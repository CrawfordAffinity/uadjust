var _bind4section={};

$.ID_1156021694_window.setTitleAttributes({
color : 'WHITE'
}
);
var _activity; 
if (OS_ANDROID) { _activity = $.ID_1156021694.activity; var abx = require('com.alcoapps.actionbarextras'); }
var _my_events = {}, _out_vars = {}, $item = {}, args = arguments[0] || {}; if ('__args' in args && '__id' in args['__args']) args.__id=args['__args'].__id; if ('__modelo' in args && _.keys(args.__modelo).length>0 && 'item' in $) { $.item.set(args.__modelo); $item = $.item.toJSON(); }
if (_.isUndefined(require(WPATH('vars'))[args.__id])) require(WPATH('vars'))[args.__id]={};
if (OS_ANDROID) {
   $.ID_1156021694_window.addEventListener('open', function(e) {
   abx.setStatusbarColor("#FFFFFF");
   abx.setBackgroundColor("#2d9edb");
   });
}


$.ID_1231086207.init({
titulo : '¿EL ASEGURADO CONFIRMA ESTOS DATOS?',
__id : 'ALL1231086207',
si : 'SI, Están correctos',
texto : 'El asegurado debe confirmar que los datos de esta sección están correctos',
onno : no_ID_1047971969,
onsi : si_ID_2119445427,
top : 25,
no : 'NO, Hay que modificar algo'
}
);

function si_ID_2119445427(e) {

var evento=e;
if (false) console.log('presiono si',{});
$.ID_1156021694.close();

if ('__args' in args) {
	args['__args'].onsi({});
} else {
	args.onsi({});
}

}
function no_ID_1047971969(e) {

var evento=e;
if (false) console.log('presiono NO',{});
$.ID_1156021694.close();

if ('__args' in args) {
	args['__args'].onno({});
} else {
	args.onno({});
}

}

(function() {
var params = ('params' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['params']:'';
if (!_.isUndefined(params.pantalla)) {
var ID_1156021694_titulo = params.pantalla;

									var setTitle2 = function(valor) {
										if (OS_ANDROID) {
											abx.title = valor;
										} else {
											$.ID_1156021694_window.setTitle(valor);
										}
									};
									var getTitle2 = function() {
										if (OS_ANDROID) {
											return abx.title;
										} else {
											return $.ID_1156021694_window.getTitle();
										}
									};
									setTitle2(ID_1156021694_titulo);

}
if (!_.isUndefined(params.header)) {
if (false) console.log('modificando barcolor a fondo'+params.header,{});
var ID_1156021694_barcolor = 'fondo'+params.header;

									var setColorBarra = function(estilo) {
										if ('styles' in require(WPATH('a4w')) && estilo in require(WPATH('a4w')).styles['classes']) {
											if (OS_ANDROID) {
												abx.setBackgroundColor(require(WPATH('a4w')).styles['classes'][estilo]['backgroundColor']);
											} else {
												$.ID_1156021694_window.setBarColor(require(WPATH('a4w')).styles['classes'][estilo]['backgroundColor']);
											}
										} else {
											if (OS_ANDROID) {
												abx.setBackgroundColor(estilo);
											} else {
												$.ID_1156021694_window.setBarColor(estilo);
											}
										}
									}
									setColorBarra(ID_1156021694_barcolor);

}
if (!_.isUndefined(params.titulo)) {

$.ID_1231086207.update({titulo : params.titulo});
}
if (!_.isUndefined(params.texto)) {

$.ID_1231086207.update({texto : params.texto});
}
if (!_.isUndefined(params.si)) {

$.ID_1231086207.update({si : params.si});
}
if (!_.isUndefined(params.no)) {

$.ID_1231086207.update({no : params.no});
}
})();

function Open_ID_2121138919(e) {

e.cancelBubble=true;
var elemento=e.source;
if (false) console.log('evento open llamado en widget esta de acuerdo',{});

}
if (OS_IOS || OS_ANDROID) {
$.ID_1156021694.addEventListener('close',function(){
   $.destroy(); // cleanup bindings
   $.off(); //remove backbone events of this widget controller
   try {
      //require(WPATH('vars'))[args.__id]=null;
      args = null;
      if (OS_ANDROID) {
         abx = null;
      }
      if ($item) $item = null;
      if (_my_events) {
         for(_ev_tmp in _my_events) { 
            try {
               if (_ev_tmp.indexOf('_web')!=-1) {
                  Ti.App.removeEventListener(_ev_tmp.split(',')[0],_my_events[_ev_tmp]);
               } else {
                  Alloy.Events.off(_ev_tmp.split(',')[0],_my_events[_ev_tmp]);
               }
            } catch(err10) {
            }
         }
         _my_events = null;
         //delete _my_events;
      }
   } catch(err10) {
   }
   if (_out_vars) {
      var _ev_tmp;
      for(_ev_tmp in _out_vars) { 
         for (_ev_rem in _out_vars[_ev_tmp]._remove) {
            try {
 eval(_out_vars[_ev_tmp]._remove[_ev_rem]); 
 } catch(_errt) {}
         }
         _out_vars[_ev_tmp] = null;
      }
      _ev_tmp = null;
      _out_vars = null;
      //delete _out_vars;
   }
});
}