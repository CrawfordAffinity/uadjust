/** 
* Widget Modal
* Representa un combobox que abre una pantalla en forma de modal con items para seleccionar
* 
* @param data Variable con datos para modal
* @param campo Define el texto del campo
* @param titulo Define el titulo del modal
* @param guardar Define si mostrar o no el boton guardar
* @param color Define el color del modal
* @param seleccione Modifica el hint del combobox
* @param left Modifica el margen izquierdo del campo del selector
* @param right Modifica el margen derecho del campo del selector
* @param top Modifica el margen superior del campo del selector
* @param activo Si true, permite seleccionar un valor. En false, esta desactivado
* @param cargando Texto a mostrar mientras se carga en memoria datos para selector 
*/
var _bind4section={};

var args = arguments[0] || {};

function Click_ID_984251325(e) {

e.cancelBubble=true;
var elemento=e.source;
var data = ('data' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['data']:'';
var activo = ('activo' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['activo']:'';
if (activo==true||activo=='true') {

if ('__args' in args) {
	args['__args'].onabrir({});
} else {
	args.onabrir({});
}
if (_.isArray(data)) {
Widget.createController("tipo_modal",{'__args' : (typeof args!=='undefined')?args:__args}).getView().open();
}
 else {
var ID_1530241800_opts=['Aceptar'];
var ID_1530241800 = Ti.UI.createAlertDialog({
   title: 'Error',
   message: 'Faltan los datos para modal',
   buttonNames: ID_1530241800_opts
});
ID_1530241800.addEventListener('click', function(e) {
   var nulo=ID_1530241800_opts[e.index];
nulo = null;

   e.source.removeEventListener("click", arguments.callee);
});
ID_1530241800.show();
}}

}
/** 
* Funcion que inicializa el widget 
*/

$.init = function(params) {
   for (var tobe in params) args[tobe] = params[tobe];
   if ('__id' in params) require(WPATH('vars'))[params.__id]={};
/** 
* guardamos variable con valor por defecto 
*/
require(WPATH('vars'))[args.__id]['data']='';
/** 
* guardamos variable con valor por defecto 
*/
require(WPATH('vars'))[args.__id]['campo']='no definido';
/** 
* guardamos variable con valor por defecto 
*/
require(WPATH('vars'))[args.__id]['titulo']='no definido';
/** 
* guardamos variable con valor por defecto 
*/
require(WPATH('vars'))[args.__id]['guardar']='false';
/** 
* guardamos variable con valor por defecto 
*/
require(WPATH('vars'))[args.__id]['color']='';
/** 
* guardamos variable con valor por defecto 
*/
require(WPATH('vars'))[args.__id]['activo']='true';
/** 
* guardamos variable con valor por defecto 
*/
require(WPATH('vars'))[args.__id]['cargando']='cargando ..';
/** 
* guardamos variable con valor por defecto 
*/
require(WPATH('vars'))[args.__id]['params']=params;
if (!_.isUndefined(params.data)) {
/** 
* verifica si params.data existe 
*/
require(WPATH('vars'))[args.__id]['data']=params.data;
}
if (!_.isUndefined(params.campo)) {
/** 
* verifica si params.campo existe 
*/
require(WPATH('vars'))[args.__id]['campo']=params.campo;
$.ID_813529883.setText(params.campo);

}
if (!_.isUndefined(params.titulo)) {
/** 
* verifica si params.titulo existe 
*/
require(WPATH('vars'))[args.__id]['titulo']=params.titulo;
}
if (!_.isUndefined(params.guardar)) {
/** 
* verifica si params.guardar existe 
*/
require(WPATH('vars'))[args.__id]['guardar']=params.guardar;
}
if (!_.isUndefined(params.color)) {
/** 
* verifica si params.color existe 
*/
require(WPATH('vars'))[args.__id]['color']=params.color;
}
if (!_.isUndefined(params.seleccione)) {
/** 
* verifica si params.seleccione existe 
*/
$.ID_460439112.setText(params.seleccione);

require(WPATH('vars'))[args.__id]['seleccione']=params.seleccione;
}
if (!_.isUndefined(params.left)) {
/** 
* verifica si params.left existe 
*/
$.ID_813529883.setLeft(params.left);

$.ID_809844812.setLeft(params.left);

}
if (!_.isUndefined(params.right)) {
/** 
* verifica si params.right existe 
*/
$.ID_809844812.setRight(params.right);

}
if (!_.isUndefined(params.top)) {
/** 
* verifica si params.top existe 
*/
$.ID_1309803634.setTop(params.top);

}
if (!_.isUndefined(params.activo)) {
/** 
* verifica si params.activo existe 
*/
require(WPATH('vars'))[args.__id]['activo']=params.activo;
}
if (!_.isUndefined(params.cargando)) {
/** 
* verifica si params.cargando existe 
*/
$.ID_460439112.setText(params.cargando);

require(WPATH('vars'))[args.__id]['cargando']=params.cargando;
}
/** 
* este nodo se ejecuta luego de 0.2 segundos 
*/
var ID_346372225_func = function() {

if ('__args' in args) {
	args['__args'].onafterinit({});
} else {
	args.onafterinit({});
}
};
var ID_346372225 = setTimeout(ID_346372225_func, 1000*0.2);
};

/** 
* Funcion que modifica el contenido del selector.
* 
* @param data Array con valores a mostrar en selector 
*/

$.data = function(params) {
var cargando = ('cargando' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['cargando']:'';
var seleccione = ('seleccione' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['seleccione']:'';
if ((_.isObject(cargando) || (_.isString(cargando)) &&  !_.isEmpty(cargando)) || _.isNumber(cargando) || _.isBoolean(cargando)) {
if ((_.isObject(seleccione) || (_.isString(seleccione)) &&  !_.isEmpty(seleccione)) || _.isNumber(seleccione) || _.isBoolean(seleccione)) {
/** 
* verifica si variable &quot;seleccione&quot; tiene algun valor. 
*/
$.ID_460439112.setText(seleccione);

}
}
if (!_.isUndefined(params.data)) {
require(WPATH('vars'))[args.__id]['data']=params.data;
}
 else {

if ('__args' in args) {
	args['__args'].onafterinit({});
} else {
	args.onafterinit({});
}
}};

/** 
* Funcion que modifica los textos del selector segun sus parametros
* 
* @param campo Modifica el texto mostrado al lado del combobox
* @param titulo Modifica el titulo del modal
* @param seleccione Modifica el hint del combobox
* @param valor Modifica el valor mostrado por defecto en el combobox 
*/

$.labels = function(params) {
if (!_.isUndefined(params.campo)) {
/** 
* verifica si params.campo existe. 
*/
$.ID_813529883.setText(params.campo);

}
if (!_.isUndefined(params.titulo)) {
/** 
* verifica si params.titulo existe. 
*/
require(WPATH('vars'))[args.__id]['titulo']=params.titulo;
}
if (!_.isUndefined(params.seleccione)) {
/** 
* verifica si params.seleccione existe. 
*/
$.ID_460439112.setText(params.seleccione);

var ID_460439112_estilo = 'estilo5_1';

						if (typeof WPATH != 'undefined') {
							var _tmp_a4w = require(WPATH('a4w'));
						} else {
							var _tmp_a4w = require('a4w');
						}
						if ((typeof ID_460439112_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (ID_460439112_estilo in _tmp_a4w.styles['classes'])) {
							try {
								ID_460439112_estilo = _tmp_a4w.styles['classes'][ID_460439112_estilo];
							} catch (st_val_err) {
							}
						}
						$.ID_460439112.applyProperties(ID_460439112_estilo);

}
if (!_.isUndefined(params.valor)) {
/** 
* verifica si params.valor existe. 
*/
$.ID_460439112.setText(params.valor);

var ID_460439112_estilo = 'estilo12';

						if (typeof WPATH != 'undefined') {
							var _tmp_a4w = require(WPATH('a4w'));
						} else {
							var _tmp_a4w = require('a4w');
						}
						if ((typeof ID_460439112_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (ID_460439112_estilo in _tmp_a4w.styles['classes'])) {
							try {
								ID_460439112_estilo = _tmp_a4w.styles['classes'][ID_460439112_estilo];
							} catch (st_val_err) {
							}
						}
						$.ID_460439112.applyProperties(ID_460439112_estilo);

}
};

/** 
* Funcion que desactiva este combobox 
*/

$.desactivar = function(params) {
require(WPATH('vars'))[args.__id]['activo']='false';
var ID_460439112_estilo = 'estilo5_1';

						if (typeof WPATH != 'undefined') {
							var _tmp_a4w = require(WPATH('a4w'));
						} else {
							var _tmp_a4w = require('a4w');
						}
						if ((typeof ID_460439112_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (ID_460439112_estilo in _tmp_a4w.styles['classes'])) {
							try {
								ID_460439112_estilo = _tmp_a4w.styles['classes'][ID_460439112_estilo];
							} catch (st_val_err) {
							}
						}
						$.ID_460439112.applyProperties(ID_460439112_estilo);

};

/** 
* Funcion que activa este combobox 
*/

$.activar = function(params) {
require(WPATH('vars'))[args.__id]['activo']='true';
var seleccione = ('seleccione' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['seleccione']:'';
var textosel;
textosel = $.ID_460439112.getText();

if (seleccione!=textosel) {
/** 
* verifica si el valor de variable &quot;seleccione&quot; es distinto al valor de la variable &quot;textosel&quot; 
*/
var ID_460439112_estilo = 'estilo12';

						if (typeof WPATH != 'undefined') {
							var _tmp_a4w = require(WPATH('a4w'));
						} else {
							var _tmp_a4w = require('a4w');
						}
						if ((typeof ID_460439112_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (ID_460439112_estilo in _tmp_a4w.styles['classes'])) {
							try {
								ID_460439112_estilo = _tmp_a4w.styles['classes'][ID_460439112_estilo];
							} catch (st_val_err) {
							}
						}
						$.ID_460439112.applyProperties(ID_460439112_estilo);

}
};

/** 
* Funcion que elimina los datos cargados en combobox, para liberar recursos de memoria 
*/

$.limpiar = function(params) {
};