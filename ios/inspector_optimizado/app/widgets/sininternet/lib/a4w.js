exports.events = _.clone(Backbone.Events);
exports.ajaxkeys = {};
exports.variables = {};
exports.styles = {"images":{"mono_7":"/images/i3ECE8B0251AA8C358DD86D279774A6FE.png","mono_3":"/images/i66166742DB624694FEA59FCCAF583FB7.png"},"classes":{"#ID_497503827":{"image":"WPATH('images/i3ECE8B0251AA8C358DD86D279774A6FE.png')"},"estilo8":{"color":"#ee7f7e","font":{"fontSize":"12dp"}},"#ID_1037140748":{"image":"WPATH('images/i66166742DB624694FEA59FCCAF583FB7.png')"},"estilo2_1":{"color":"#a0a1a3","font":{"fontFamily":"SFUIText-Light","fontSize":"15dp"}}}};
exports.fontello = {};
