/** 
* Widget Pregunta
* Este control representa una vista que permite escoger entre dos opciones: si y no.
* 
* @param top Indica el margen superior de este widget
* @param titulo Indica el titulo mostrado sobre el personaje
* @param texto Indica el texto de la pregunta
* @param si Indica el texto para el boton SI
* @param no Indica el texto para el boton NO 
*/
var _bind4section={};

var args = arguments[0] || {};


$.ID_1300949542.init({
titulo : '¿PUEDE CONTINUAR CON LA INSPECCION?',
__id : 'ALL1300949542',
texto : 'Si está el asegurado en el domicilio presione SI para continuar',
alto : '-',
top : 0,
ancho : '90%',
tipo : '_info'
}
);

function Touchstart_ID_479156976(e) {

e.cancelBubble=true;
var elemento=e.source;
$.ID_1746056874.setBackgroundColor('#f6fdfa');

}
function Touchend_ID_1911965702(e) {

e.cancelBubble=true;
var elemento=e.source;
$.ID_1746056874.setBackgroundColor('#ffffff');

if ('__args' in args) {
	args['__args'].onsi({});
} else {
	args.onsi({});
}

}
function Touchstart_ID_979464485(e) {

e.cancelBubble=true;
var elemento=e.source;
$.ID_1824685387.setBackgroundColor('#fdf5f5');

}
function Touchend_ID_988414676(e) {

e.cancelBubble=true;
var elemento=e.source;
$.ID_1824685387.setBackgroundColor('#ffffff');

if ('__args' in args) {
	args['__args'].onno({});
} else {
	args.onno({});
}

}
/** 
* Funcion que inicializa widget 
*/

$.init = function(params) {
   for (var tobe in params) args[tobe] = params[tobe];
   if ('__id' in params) require(WPATH('vars'))[params.__id]={};
if (!_.isUndefined(params.top)) {
/** 
* Verifica que existe params.top 
*/
$.ID_169908798.setTop(params.top);

}
if (!_.isUndefined(params.titulo)) {
/** 
* Verifica que exista params.titulo 
*/

$.ID_1300949542.update({titulo : params.titulo});
}
if (!_.isUndefined(params.texto)) {
/** 
* Verifica que exista params.texto 
*/

$.ID_1300949542.update({texto : params.texto});
}
if (!_.isUndefined(params.si)) {
/** 
* Verifica que exista params.si 
*/
$.ID_853778123.setText(params.si);

}
if (!_.isUndefined(params.no)) {
/** 
* Verifica que exista params.no 
*/
$.ID_1376957032.setText(params.no);

if (_.isNumber((params.no.length)) && _.isNumber(28) && (params.no.length) < 28) {
$.ID_1376957032.setTop(25);

}
 else {
$.ID_1376957032.setTop(18);

}}
};

/** 
* Funcion que actualiza los datos mostrados en la vista de este widget
* 
* @param titulo Modifica el titulo sobre el personaje
* @param texto Modifica el texto de detalle de la pregunta
* @param si Modifica el texto del boton SI
* @param no Modifica el texto del boton NO 
*/

$.update = function(params) {
if (!_.isUndefined(params.titulo)) {
/** 
* Verifica que exista params.titulo 
*/

$.ID_1300949542.update({titulo : params.titulo});
}
if (!_.isUndefined(params.texto)) {
/** 
* Verifica que exista params.texto 
*/

$.ID_1300949542.update({texto : params.texto});
}
if (!_.isUndefined(params.si)) {
/** 
* Verifica que exista params.si 
*/
$.ID_853778123.setText(params.si);

}
if (!_.isUndefined(params.no)) {
/** 
* Verifica que exista params.no 
*/
$.ID_1376957032.setText(params.no);

if (_.isNumber((params.no.length)) && _.isNumber(28) && (params.no.length) < 28) {
/** 
* Consulta si la cantidad de caracteres del valor de params.no es menor a 28 
*/
$.ID_1376957032.setTop(25);

}
 else {
$.ID_1376957032.setTop(18);

}}
};