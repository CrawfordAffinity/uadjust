/** 
* Widget Boton Largo
* Representa un boton con progreso opcional y diferentes estilos
* 
* @param titulo Define el texto del boton
* @param color Define el color del boton: amarillo, rojo, verde, lila, naranjo, morado, rosado, celeste, cafe
* @param icono Define el alias del icono a mostrar junto al texto 
*/
var _bind4section={};

var args = arguments[0] || {};

function Touchstart_ID_1950876699(e) {
/** 
* Este evento se ejecuta al presionar el boton. 
*/

e.cancelBubble=true;
var elemento=e.source;
var color = ('color' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['color']:'';
if (color=='amarillo') {
var ID_281086116_estilo = 'fd_amarillo_on';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_281086116.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_281086116_estilo);

}
 else if (color=='rojo') {
var ID_281086116_estilo = 'fd_rojo_on';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_281086116.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_281086116_estilo);

} else if (color=='verde') {
var ID_281086116_estilo = 'fd_verde_on';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_281086116.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_281086116_estilo);

} else if (color=='lila') {
var ID_281086116_estilo = 'fd_lila_on';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_281086116.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_281086116_estilo);

} else if (color=='naranjo') {
var ID_281086116_estilo = 'fd_naranjo_on';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_281086116.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_281086116_estilo);

} else if (color=='morado') {
var ID_281086116_estilo = 'fd_morado_on';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_281086116.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_281086116_estilo);

} else if (color=='rosado') {
var ID_281086116_estilo = 'fd_rosado_on';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_281086116.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_281086116_estilo);

} else if (color=='celeste') {
var ID_281086116_estilo = 'fd_celeste_on';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_281086116.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_281086116_estilo);

} else if (color=='cafe') {
var ID_281086116_estilo = 'fd_cafe_on';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_281086116.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_281086116_estilo);

}
}
function Touchend_ID_1022085204(e) {
/** 
* Este evento se ejecuta al dejar de presionar el boton. 
*/

e.cancelBubble=true;
var elemento=e.source;
var color = ('color' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['color']:'';
if (color=='amarillo') {
var ID_281086116_estilo = 'fd_amarillo';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_281086116.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_281086116_estilo);

}
 else if (color=='rojo') {
var ID_281086116_estilo = 'fd_rojo';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_281086116.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_281086116_estilo);

} else if (color=='verde') {
var ID_281086116_estilo = 'fd_verde';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_281086116.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_281086116_estilo);

} else if (color=='lila') {
var ID_281086116_estilo = 'fd_lila';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_281086116.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_281086116_estilo);

} else if (color=='naranjo') {
var ID_281086116_estilo = 'fd_naranjo';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_281086116.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_281086116_estilo);

} else if (color=='morado') {
var ID_281086116_estilo = 'fd_morado';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_281086116.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_281086116_estilo);

} else if (color=='rosado') {
var ID_281086116_estilo = 'fd_rosado';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_281086116.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_281086116_estilo);

} else if (color=='celeste') {
var ID_281086116_estilo = 'fd_celeste';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_281086116.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_281086116_estilo);

} else if (color=='cafe') {
var ID_281086116_estilo = 'fd_cafe';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_281086116.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_281086116_estilo);

}
if ('__args' in args) {
	args['__args'].onclick({});
} else {
	args.onclick({});
}

}
/** 
* Funcion que inicializa widget 
*/

$.init = function(params) {
   for (var tobe in params) args[tobe] = params[tobe];
   if ('__id' in params) require(WPATH('vars'))[params.__id]={};
require(WPATH('vars'))[args.__id]['color']='amarillo';
if (!_.isUndefined(params.titulo)) {
/** 
* Comprueba que exista el argumento params.titulo 
*/
$.ID_675293245.setText(params.titulo);

}
if (!_.isUndefined(params.color)) {
/** 
* Comprueba que exista el argumento params.color 
*/
require(WPATH('vars'))[args.__id]['color']=params.color;
}
if (!_.isUndefined(params.icono)) {
/** 
* Comprueba que exista el argumento params.icono 
*/
require(WPATH('vars'))[args.__id]['icono']=params.icono;
var ID_1525222491_icono = params.icono;

				  if ('fontello' in require(WPATH('a4w')) && 'adjust' in require(WPATH('a4w')).fontello && ID_1525222491_icono in require(WPATH('a4w')).fontello['adjust'].CODES) {
				  		ID_1525222491_icono = require(WPATH('a4w')).fontello['adjust'].CODES[ID_1525222491_icono];
				  } else {
				  		console.log('live/modificar -> error setting new svg icon alias');
				  }
				  $.ID_1525222491.setText(ID_1525222491_icono);

var ID_1525222491_visible = true;

										  if (ID_1525222491_visible=='si') {
											  ID_1525222491_visible=true;
										  } else if (ID_1525222491_visible=='no') {
											  ID_1525222491_visible=false;
										  }
										  $.ID_1525222491.setVisible(ID_1525222491_visible);

}
var color = ('color' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['color']:'';
if (color=='amarillo') {
/** 
* Comprueba que el valor de la variable color sea amarillo 
*/
var ID_281086116_estilo = 'fd_amarillo';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_281086116.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_281086116_estilo);

}
 else if (color=='rojo') {
/** 
* Comprueba que el valor de la variable color sea rojo 
*/
var ID_281086116_estilo = 'fd_rojo';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_281086116.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_281086116_estilo);

} else if (color=='verde') {
/** 
* Comprueba que el valor de la variable color sea verde 
*/
var ID_281086116_estilo = 'fd_verde';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_281086116.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_281086116_estilo);

} else if (color=='lila') {
/** 
* Comprueba que el valor de la variable color sea lila 
*/
var ID_281086116_estilo = 'fd_lila';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_281086116.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_281086116_estilo);

} else if (color=='naranjo') {
/** 
* Comprueba que el valor de la variable color sea naranjo 
*/
var ID_281086116_estilo = 'fd_naranjo';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_281086116.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_281086116_estilo);

} else if (color=='morado') {
/** 
* Comprueba que el valor de la variable color sea morado 
*/
var ID_281086116_estilo = 'fd_morado';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_281086116.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_281086116_estilo);

} else if (color=='rosado') {
/** 
* Comprueba que el valor de la variable color sea rosado. 
*/
var ID_281086116_estilo = 'fd_rosado';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_281086116.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_281086116_estilo);

} else if (color=='celeste') {
/** 
* Comprueba que el valor de la variable color sea celeste. 
*/
var ID_281086116_estilo = 'fd_celeste';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_281086116.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_281086116_estilo);

} else if (color=='cafe') {
/** 
* Comprueba que el valor de la variable color sea cafe. 
*/
var ID_281086116_estilo = 'fd_cafe';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_281086116.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_281086116_estilo);

}};

/** 
* Funcion que muestra animacion de progreso y oculta texto de boton 
*/

$.iniciar_progreso = function(params) {
/** 
* oculta el bot&#243;n continuar y muestra la animaci&#243;n de progreso. 
*/
var ID_235307810_visible = false;

										  if (ID_235307810_visible=='si') {
											  ID_235307810_visible=true;
										  } else if (ID_235307810_visible=='no') {
											  ID_235307810_visible=false;
										  }
										  $.ID_235307810.setVisible(ID_235307810_visible);

$.ID_1972029426.show();
};

/** 
* Funcion que oculta animacion de progreso y muestra texto de boton 
*/

$.detener_progreso = function(params) {
var ID_235307810_visible = true;

										  if (ID_235307810_visible=='si') {
											  ID_235307810_visible=true;
										  } else if (ID_235307810_visible=='no') {
											  ID_235307810_visible=false;
										  }
										  $.ID_235307810.setVisible(ID_235307810_visible);

$.ID_1972029426.hide();
};