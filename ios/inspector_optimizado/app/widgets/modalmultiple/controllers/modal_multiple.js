var _bind4section={};

$.ID_80372271_window.setTitleAttributes({
color : 'WHITE'
}
);
var _activity; 
if (OS_ANDROID) { _activity = $.ID_80372271.activity; var abx = require('com.alcoapps.actionbarextras'); }
var _my_events = {}, _out_vars = {}, $item = {}, args = arguments[0] || {}; if ('__args' in args && '__id' in args['__args']) args.__id=args['__args'].__id; if ('__modelo' in args && _.keys(args.__modelo).length>0 && 'item' in $) { $.item.set(args.__modelo); $item = $.item.toJSON(); }
if (_.isUndefined(require(WPATH('vars'))[args.__id])) require(WPATH('vars'))[args.__id]={};
if (OS_ANDROID) {
   $.ID_80372271_window.addEventListener('open', function(e) {
   abx.setStatusbarColor("#FFFFFF");
   abx.setBackgroundColor("#ffffff");
   });
}

/** 
* Genera una consulta al modelo temp_multiple filtrando por id_instancia, la respuesta a la consulta se guarda en una variable llamada lista 
*/
var ID_89308563_like = function(search) {
  if (typeof search !== 'string' || this === null) {return false; }
  search = search.replace(new RegExp("([\\.\\\\\\+\\*\\?\\[\\^\\]\\$\\(\\)\\{\\}\\=\\!\\<\\>\\|\\:\\-])", "g"), "\\$1");
  search = search.replace(/%/g, '.*').replace(/_/g, '.');
  return RegExp('^' + search + '$', 'gi').test(this);
};
var ID_89308563_filter = function(coll) {
var filtered = coll.filter(function(m) {
	var _tests = [], _all_true = false, model = m.toJSON();
_tests.push((model.idinstancia == '0'));
	var _all_true_s = _.uniq(_tests);
	_all_true = (_all_true_s.length==1 && _all_true_s[0]==true)?true:false;
	return _all_true;
	});
filtered = _.toArray(filtered);
return filtered;
};
var ID_89308563_update = function(e) {
};
_.defer(function() { 
 Widget.Collections.temp_multiple.fetch(); 
 });
Widget.Collections.temp_multiple.on('add change delete', function(ee) { ID_89308563_update(ee); });
var ID_89308563_transform = function(model) {
var fila = model.toJSON();
if (fila.estado==1||fila.estado=='1') {
 fila.activo=true;
}
 else {
 fila.activo=false;
}var elcolor;
elcolor = $.ID_1202779477.getColor();

 fila.color=elcolor;
return fila;
};
Widget.Collections.temp_multiple.fetch();

function Click_ID_1957476363(e) {

e.cancelBubble=true;
var elemento=e.source;
/** 
* Recupera la variable id_instacia con el id del widget 
*/
var id_instancia = ('id_instancia' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['id_instancia']:'';
/** 
* Consulta el modelo temp_multiple filtrando por idinstancia y estado en 1 
*/
if (Widget.Collections.temp_multiple.models.length==0) Widget.Collections.temp_multiple.fetch();
var ID_1313560106_i=Widget.Collections.temp_multiple;
//filtramos modelos segun consulta (where futuro linkeado) y armamos respuesta
var ID_1313560106_i_where=[], activos=[], struct={}
for (var reg_ID_1313560106=0;reg_ID_1313560106<ID_1313560106_i.models.length;reg_ID_1313560106++) {
  if ('idinstancia' in ID_1313560106_i.models[reg_ID_1313560106].attributes && ID_1313560106_i.models[reg_ID_1313560106].attributes['idinstancia'] == id_instancia && 'estado' in ID_1313560106_i.models[reg_ID_1313560106].attributes && ID_1313560106_i.models[reg_ID_1313560106].attributes['estado'] == 1) {
    ID_1313560106_i_where.push(Widget.Collections.temp_multiple.at(reg_ID_1313560106));
    struct={};
    for (var key in ID_1313560106_i.models[reg_ID_1313560106].attributes) {
      struct[key]=ID_1313560106_i.models[reg_ID_1313560106].attributes[key];
    }
    activos.push(struct);
  }
}
/** 
* Convierte el campo amostrar por label 
*/
var resp=[];
_.each(activos, function(fila, pos) {
  var new_row={};
  _.each(fila, function(x, llave) {
    var newkey='';
    if (llave=='amostrar') newkey='label';
    if (llave=='valor') newkey='valor';
    if (newkey!='') new_row[newkey] = fila[llave];
  });
  resp.push(new_row);
});
/** 
* Obtiene el campo valor que esta en la variable resp y los separa con una coma 
*/
var lista = _.pluck(resp, 'valor').join(',');
/** 
* Retorna los valores y la data a la pantalla que utiliza el widget 
*/
if ('__args' in args) {
	args['__args'].oncerrar({valores : lista,data : resp});
} else {
	args.oncerrar({valores : lista,data : resp});
}
/** 
* Cierra la pantall a 
*/
$.ID_80372271.close();

}
function Change_ID_655299005(e) {

e.cancelBubble=true;
var elemento=e.value;
/** 
* Hace una consulta al modelo temp_multiple, filtrando con el id de la fila 
*/
if (Widget.Collections.temp_multiple.models.length==0) Widget.Collections.temp_multiple.fetch();
var ID_250780886_i=Widget.Collections.temp_multiple;
//filtramos modelos segun consulta (where futuro linkeado) y armamos respuesta
var ID_250780886_i_where=[], actual=[], struct={}
for (var reg_ID_250780886=0;reg_ID_250780886<ID_250780886_i.models.length;reg_ID_250780886++) {
  if ('id' in ID_250780886_i.models[reg_ID_250780886].attributes && ID_250780886_i.models[reg_ID_250780886].attributes['id'] == e.source.parent._idfila) {
    ID_250780886_i_where.push(Widget.Collections.temp_multiple.at(reg_ID_250780886));
    struct={};
    for (var key in ID_250780886_i.models[reg_ID_250780886].attributes) {
      struct[key]=ID_250780886_i.models[reg_ID_250780886].attributes[key];
    }
    actual.push(struct);
  }
}
if (false) console.log('switch source cambiado dice (padre)',{"basedatos":actual,"elemento":e.source.parent._idfila});
if (elemento==true||elemento=='true') {
/** 
* en el caso que el switch sea true, modifica el modelo en estado 1 
*/
/** 
* Modifica el estado del id seleccionado 
*/
var db = Widget.Collections.temp_multiple;
if (ID_250780886_i_where.length>0) {
  for (var reg_ID_925942059=0;reg_ID_925942059<ID_250780886_i_where.length;reg_ID_925942059++) {
    ID_250780886_i_where[reg_ID_925942059].set({ estado : '1' },{ silent:true });
    ID_250780886_i_where[reg_ID_925942059].save();
  }
}
}
 else {
/** 
* Modifica el estado del id seleccionado 
*/
var db = Widget.Collections.temp_multiple;
if (ID_250780886_i_where.length>0) {
  for (var reg_ID_1461974181=0;reg_ID_1461974181<ID_250780886_i_where.length;reg_ID_1461974181++) {
    ID_250780886_i_where[reg_ID_1461974181].set({ estado : '0' },{ silent:true });
    ID_250780886_i_where[reg_ID_1461974181].save();
  }
}
}elemento=null;

}

(function() {
/** 
* recupera la variable params creada en la pantalla inicio de este widget 
*/
var params = ('params' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['params']:'';
/** 
* Modifica el valor de la idinstacia en el modelo que se esta usando, y lo ordena con amostrar 
*/
ID_89308563_filter = function(coll) {
var filtered = coll.filter(function(m) {
	var _tests = [], _all_true = false, model = m.toJSON();
_tests.push((model.idinstancia == params.__id));
	var _all_true_s = _.uniq(_tests);
	_all_true = (_all_true_s.length==1 && _all_true_s[0]==true)?true:false;
	return _all_true;
	});
filtered = _.toArray(filtered);
var ordered = _.sortBy(filtered,'amostrar');
return ordered;
};
_.defer(function() { 
 Widget.Collections.temp_multiple.fetch(); 
 });
if (!_.isUndefined(params.titulo)) {
/** 
* Modifica el titulo de la pantalla modal segun el valor de params.titulo 
*/
var ID_80372271_titulo = params.titulo;

									var setTitle2 = function(valor) {
										if (OS_ANDROID) {
											abx.title = valor;
										} else {
											$.ID_80372271_window.setTitle(valor);
										}
									};
									var getTitle2 = function() {
										if (OS_ANDROID) {
											return abx.title;
										} else {
											return $.ID_80372271_window.getTitle();
										}
									};
									setTitle2(ID_80372271_titulo);

/** 
* Actualiza el titulo del header en el caso de que se le haya pasado el valor params.titulo 
*/
$.ID_1819032593.setText(params.titulo.toUpperCase());

}
if (!_.isUndefined(params.subtitulo)) {
/** 
* Cambia el texto en el caso de que el parametro subtitulo exista 
*/
$.ID_168035718.setText(params.subtitulo);

}
if (!_.isUndefined(params.color)) {
/** 
* Verifica si params.color existe 
*/
if (params.color=='blanco') {
/** 
* Verifica el valor de params.color y modifica el color del boton cerrar pantalla segun su valor 
*/
/** 
* Modifica el estilo del boton cerrar pantalla. 
*/
var ID_1202779477_estilo = 'color_blanco';

					if (typeof WPATH != 'undefined') {
						var _tmp_a4w = require(WPATH('a4w'));
					} else {
						var _tmp_a4w = require('a4w');
					}
					if ((typeof ID_1202779477_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (ID_1202779477_estilo in _tmp_a4w.styles['classes'])) {
						try {
							ID_1202779477_estilo = _tmp_a4w.styles['classes'][ID_1202779477_estilo]['color'];
						} catch (st_val_err) {
						}
					}
					$.ID_1202779477.setColor(ID_1202779477_estilo);

}
 else if (params.color=='negro') {
var ID_1202779477_estilo = 'color_negro';

					if (typeof WPATH != 'undefined') {
						var _tmp_a4w = require(WPATH('a4w'));
					} else {
						var _tmp_a4w = require('a4w');
					}
					if ((typeof ID_1202779477_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (ID_1202779477_estilo in _tmp_a4w.styles['classes'])) {
						try {
							ID_1202779477_estilo = _tmp_a4w.styles['classes'][ID_1202779477_estilo]['color'];
						} catch (st_val_err) {
						}
					}
					$.ID_1202779477.setColor(ID_1202779477_estilo);

} else if (params.color=='azul') {
var ID_1202779477_estilo = 'color_azul';

					if (typeof WPATH != 'undefined') {
						var _tmp_a4w = require(WPATH('a4w'));
					} else {
						var _tmp_a4w = require('a4w');
					}
					if ((typeof ID_1202779477_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (ID_1202779477_estilo in _tmp_a4w.styles['classes'])) {
						try {
							ID_1202779477_estilo = _tmp_a4w.styles['classes'][ID_1202779477_estilo]['color'];
						} catch (st_val_err) {
						}
					}
					$.ID_1202779477.setColor(ID_1202779477_estilo);

} else if (params.color=='plomo') {
var ID_1202779477_estilo = 'color_plomo';

					if (typeof WPATH != 'undefined') {
						var _tmp_a4w = require(WPATH('a4w'));
					} else {
						var _tmp_a4w = require('a4w');
					}
					if ((typeof ID_1202779477_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (ID_1202779477_estilo in _tmp_a4w.styles['classes'])) {
						try {
							ID_1202779477_estilo = _tmp_a4w.styles['classes'][ID_1202779477_estilo]['color'];
						} catch (st_val_err) {
						}
					}
					$.ID_1202779477.setColor(ID_1202779477_estilo);

} else if (params.color=='rojo') {
var ID_1202779477_estilo = 'color_rojo';

					if (typeof WPATH != 'undefined') {
						var _tmp_a4w = require(WPATH('a4w'));
					} else {
						var _tmp_a4w = require('a4w');
					}
					if ((typeof ID_1202779477_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (ID_1202779477_estilo in _tmp_a4w.styles['classes'])) {
						try {
							ID_1202779477_estilo = _tmp_a4w.styles['classes'][ID_1202779477_estilo]['color'];
						} catch (st_val_err) {
						}
					}
					$.ID_1202779477.setColor(ID_1202779477_estilo);

} else if (params.color=='lila') {
var ID_1202779477_estilo = 'color_lila';

					if (typeof WPATH != 'undefined') {
						var _tmp_a4w = require(WPATH('a4w'));
					} else {
						var _tmp_a4w = require('a4w');
					}
					if ((typeof ID_1202779477_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (ID_1202779477_estilo in _tmp_a4w.styles['classes'])) {
						try {
							ID_1202779477_estilo = _tmp_a4w.styles['classes'][ID_1202779477_estilo]['color'];
						} catch (st_val_err) {
						}
					}
					$.ID_1202779477.setColor(ID_1202779477_estilo);

} else if (params.color=='naranjo') {
var ID_1202779477_estilo = 'color_naranjo';

					if (typeof WPATH != 'undefined') {
						var _tmp_a4w = require(WPATH('a4w'));
					} else {
						var _tmp_a4w = require('a4w');
					}
					if ((typeof ID_1202779477_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (ID_1202779477_estilo in _tmp_a4w.styles['classes'])) {
						try {
							ID_1202779477_estilo = _tmp_a4w.styles['classes'][ID_1202779477_estilo]['color'];
						} catch (st_val_err) {
						}
					}
					$.ID_1202779477.setColor(ID_1202779477_estilo);

} else if (params.color=='amarillo') {
var ID_1202779477_estilo = 'color_amarillo';

					if (typeof WPATH != 'undefined') {
						var _tmp_a4w = require(WPATH('a4w'));
					} else {
						var _tmp_a4w = require('a4w');
					}
					if ((typeof ID_1202779477_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (ID_1202779477_estilo in _tmp_a4w.styles['classes'])) {
						try {
							ID_1202779477_estilo = _tmp_a4w.styles['classes'][ID_1202779477_estilo]['color'];
						} catch (st_val_err) {
						}
					}
					$.ID_1202779477.setColor(ID_1202779477_estilo);

} else if (params.color=='morado') {
var ID_1202779477_estilo = 'color_morado';

					if (typeof WPATH != 'undefined') {
						var _tmp_a4w = require(WPATH('a4w'));
					} else {
						var _tmp_a4w = require('a4w');
					}
					if ((typeof ID_1202779477_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (ID_1202779477_estilo in _tmp_a4w.styles['classes'])) {
						try {
							ID_1202779477_estilo = _tmp_a4w.styles['classes'][ID_1202779477_estilo]['color'];
						} catch (st_val_err) {
						}
					}
					$.ID_1202779477.setColor(ID_1202779477_estilo);

} else if (params.color=='rosado') {
var ID_1202779477_estilo = 'color_rosado';

					if (typeof WPATH != 'undefined') {
						var _tmp_a4w = require(WPATH('a4w'));
					} else {
						var _tmp_a4w = require('a4w');
					}
					if ((typeof ID_1202779477_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (ID_1202779477_estilo in _tmp_a4w.styles['classes'])) {
						try {
							ID_1202779477_estilo = _tmp_a4w.styles['classes'][ID_1202779477_estilo]['color'];
						} catch (st_val_err) {
						}
					}
					$.ID_1202779477.setColor(ID_1202779477_estilo);

} else if (params.color=='celeste') {
var ID_1202779477_estilo = 'color_celeste';

					if (typeof WPATH != 'undefined') {
						var _tmp_a4w = require(WPATH('a4w'));
					} else {
						var _tmp_a4w = require('a4w');
					}
					if ((typeof ID_1202779477_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (ID_1202779477_estilo in _tmp_a4w.styles['classes'])) {
						try {
							ID_1202779477_estilo = _tmp_a4w.styles['classes'][ID_1202779477_estilo]['color'];
						} catch (st_val_err) {
						}
					}
					$.ID_1202779477.setColor(ID_1202779477_estilo);

} else if (params.color=='cafe') {
var ID_1202779477_estilo = 'color_cafe';

					if (typeof WPATH != 'undefined') {
						var _tmp_a4w = require(WPATH('a4w'));
					} else {
						var _tmp_a4w = require('a4w');
					}
					if ((typeof ID_1202779477_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (ID_1202779477_estilo in _tmp_a4w.styles['classes'])) {
						try {
							ID_1202779477_estilo = _tmp_a4w.styles['classes'][ID_1202779477_estilo]['color'];
						} catch (st_val_err) {
						}
					}
					$.ID_1202779477.setColor(ID_1202779477_estilo);

} else if (params.color=='verde') {
var ID_1202779477_estilo = 'color_verde';

					if (typeof WPATH != 'undefined') {
						var _tmp_a4w = require(WPATH('a4w'));
					} else {
						var _tmp_a4w = require('a4w');
					}
					if ((typeof ID_1202779477_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (ID_1202779477_estilo in _tmp_a4w.styles['classes'])) {
						try {
							ID_1202779477_estilo = _tmp_a4w.styles['classes'][ID_1202779477_estilo]['color'];
						} catch (st_val_err) {
						}
					}
					$.ID_1202779477.setColor(ID_1202779477_estilo);

}}
})();

if (OS_IOS || OS_ANDROID) {
$.ID_80372271.addEventListener('close',function(){
   $.destroy(); // cleanup bindings
   $.off(); //remove backbone events of this widget controller
   try {
      //require(WPATH('vars'))[args.__id]=null;
      args = null;
      if (OS_ANDROID) {
         abx = null;
      }
      if ($item) $item = null;
      if (_my_events) {
         for(_ev_tmp in _my_events) { 
            try {
               if (_ev_tmp.indexOf('_web')!=-1) {
                  Ti.App.removeEventListener(_ev_tmp.split(',')[0],_my_events[_ev_tmp]);
               } else {
                  Alloy.Events.off(_ev_tmp.split(',')[0],_my_events[_ev_tmp]);
               }
            } catch(err10) {
            }
         }
         _my_events = null;
         //delete _my_events;
      }
   } catch(err10) {
   }
   if (_out_vars) {
      var _ev_tmp;
      for(_ev_tmp in _out_vars) { 
         for (_ev_rem in _out_vars[_ev_tmp]._remove) {
            try {
 eval(_out_vars[_ev_tmp]._remove[_ev_rem]); 
 } catch(_errt) {}
         }
         _out_vars[_ev_tmp] = null;
      }
      _ev_tmp = null;
      _out_vars = null;
      //delete _out_vars;
   }
});
}