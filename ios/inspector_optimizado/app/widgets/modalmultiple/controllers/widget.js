/** 
* Widget Modal Multiple
* Control que muestra un selector tipo combobox con mas un valor simultaneo seleccionable a traves de una sub pantalla con los items y switches.
* 
* @param titulo Indica el titulo del campo del combobox (label)
* @param hint Indica el texto para cuando aun no se han seleccionado items en el combobox
* @param top Indica el margen superior para el contenedor del combobox
* @param left Indica el margen izquierdo para el contenedor del combobox
* @param right Indica el margen derecho para el contenedor del combobox
* @param bottom Indica el margen inferior para el contenedor del combobox
* @param ancho Indica el ancho del contenedor del combobox
* @param alto Indica el alto del contenedor del combobox
* @param cargando Mensaje a mostrar mienstras se carga contenido de items en memoria 
*/
var _bind4section={};

var args = arguments[0] || {};

/** 
* Genera una consulta al modelo temp_multiple filtrando por id_instancia, la respuesta a la consulta se guarda en una variable llamada lista 
*/
var ID_1270126810_like = function(search) {
  if (typeof search !== 'string' || this === null) {return false; }
  search = search.replace(new RegExp("([\\.\\\\\\+\\*\\?\\[\\^\\]\\$\\(\\)\\{\\}\\=\\!\\<\\>\\|\\:\\-])", "g"), "\\$1");
  search = search.replace(/%/g, '.*').replace(/_/g, '.');
  return RegExp('^' + search + '$', 'gi').test(this);
};
var ID_1270126810_filter = function(coll) {
var filtered = coll.filter(function(m) {
	var _tests = [], _all_true = false, model = m.toJSON();
_tests.push((model.idinstancia == '0'));
	var _all_true_s = _.uniq(_tests);
	_all_true = (_all_true_s.length==1 && _all_true_s[0]==true)?true:false;
	return _all_true;
	});
filtered = _.toArray(filtered);
return filtered;
};
var ID_1270126810_transform = function(model) {
  var fila = model.toJSON();
  return fila;
};
var ID_1270126810_update = function(e) {
};
_.defer(function() { 
 Widget.Collections.temp_multiple.fetch(); 
 });
Widget.Collections.temp_multiple.on('add change delete', function(ee) { ID_1270126810_update(ee); });
Widget.Collections.temp_multiple.fetch();

function Click_ID_926713238(e) {

e.cancelBubble=true;
var elemento=e.source;
var ID_611664224_trycatch = { error: function(e) { if (false) console.log('error en comando probar: recuerda poner evento ?error como primer hijo.',e); } };
try {
ID_611664224_trycatch.error = function(evento) {
};
if ('__args' in args) {
	args['__args'].onclick({});
} else {
	args.onclick({});
}
} catch (e) {
   ID_611664224_trycatch.error(e);
}
Widget.createController("modal_multiple",{'__args' : (typeof args!=='undefined')?args:__args}).getView().open();

}
/** 
* Funcion que inicializa widget 
*/
$.init = function(params) {
   for (var tobe in params) args[tobe] = params[tobe];
   if ('__id' in params) require(WPATH('vars'))[params.__id]={};
require(WPATH('vars'))[args.__id]['params']=params;
if (!_.isUndefined(params.titulo)) {
/** 
* Revisamos los parametros al iniciar el widget y modificamos lo que sea pertinente 
*/
/** 
* Cambia el texto del titulo con el valor que se envia desde la pantalla que utiliza el widget. 
*/
$.ID_372511936.setText(params.titulo);

}
if (!_.isUndefined(params.hint)) {
/** 
* Cambia el texto de hint con el valor que se envia desde la pantalla que utiliza el widget 
*/
$.ID_1041790471.setText(params.hint);

/** 
* Mostramos hint 
*/
var ID_1041790471_visible = true;

								  if (ID_1041790471_visible=='si') {
									  ID_1041790471_visible=true;
								  } else if (ID_1041790471_visible=='no') {
									  ID_1041790471_visible=false;
								  }
								  $.ID_1041790471.setVisible(ID_1041790471_visible);

/** 
* Ocultamos valor 
*/
var ID_952750652_visible = false;

								  if (ID_952750652_visible=='si') {
									  ID_952750652_visible=true;
								  } else if (ID_952750652_visible=='no') {
									  ID_952750652_visible=false;
								  }
								  $.ID_952750652.setVisible(ID_952750652_visible);

require(WPATH('vars'))[args.__id]['hint']=params.hint;
}
if (!_.isUndefined(params.top)) {
/** 
* Define el top que tendra la vista 
*/
$.ID_1935998114.setTop(params.top);

}
if (!_.isUndefined(params.left)) {
/** 
* Define el left que tendra la vista 
*/
$.ID_1935998114.setLeft(params.left);

}
if (!_.isUndefined(params.subleft)) {
/** 
* Define el left que tendra que tendra el titulo 
*/
$.ID_372511936.setLeft(params.subleft);

}
if (!_.isUndefined(params.right)) {
/** 
* Define el right que tendra la vista 
*/
$.ID_1935998114.setRight(params.right);

}
if (!_.isUndefined(params.bottom)) {
/** 
* Define el bottom que tendra la vista 
*/
$.ID_1935998114.setBottom(params.bottom);

}
if (!_.isUndefined(params.ancho)) {
/** 
* Define el ancho que tendra la vista 
*/
var ID_1935998114_ancho = params.ancho;

								  if (ID_1935998114_ancho=='*') {
									  ID_1935998114_ancho=Ti.UI.FILL;
								  } else if (ID_1935998114_ancho=='-') {
									  ID_1935998114_ancho=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1935998114_ancho)) {
									  ID_1935998114_ancho=ID_1935998114_ancho+'dp';
								  }
								  $.ID_1935998114.setWidth(ID_1935998114_ancho);

}
if (!_.isUndefined(params.alto)) {
/** 
* Define el alto que tendra la vista 
*/
var ID_1935998114_alto = params.alto;

								  if (ID_1935998114_alto=='*') {
									  ID_1935998114_alto=Ti.UI.FILL;
								  } else if (ID_1935998114_alto=='-') {
									  ID_1935998114_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1935998114_alto)) {
									  ID_1935998114_alto=ID_1935998114_alto+'dp';
								  }
								  $.ID_1935998114.setHeight(ID_1935998114_alto);

}
if (!_.isUndefined(params.cargando)) {
$.ID_1041790471.setText(params.cargando);

var ID_1041790471_visible = true;

								  if (ID_1041790471_visible=='si') {
									  ID_1041790471_visible=true;
								  } else if (ID_1041790471_visible=='no') {
									  ID_1041790471_visible=false;
								  }
								  $.ID_1041790471.setVisible(ID_1041790471_visible);

var ID_952750652_visible = false;

								  if (ID_952750652_visible=='si') {
									  ID_952750652_visible=true;
								  } else if (ID_952750652_visible=='no') {
									  ID_952750652_visible=false;
								  }
								  $.ID_952750652.setVisible(ID_952750652_visible);

require(WPATH('vars'))[args.__id]['cargando']=params.cargando;
}
if (!_.isUndefined(params.__id)) {
/** 
* Si tenemos id de instancia de widget, guardamos id de instancia en var.&#160; Establecemos scope para datos de modelo local 
*/
/** 
* Guarda la variable id_instacia con el id del widget 
*/
require(WPATH('vars'))[args.__id]['id_instancia']=params.__id;
}
if ('__args' in args) {
	args['__args'].onafterinit({});
} else {
	args.onafterinit({});
}
/** 
* Modifica el valor de la idinstacia en el modelo que se esta usando, y lo ordena con amostrar 
*/
ID_1270126810_filter = function(coll) {
var filtered = coll.filter(function(m) {
	var _tests = [], _all_true = false, model = m.toJSON();
_tests.push((model.idinstancia == params.__id));
	var _all_true_s = _.uniq(_tests);
	_all_true = (_all_true_s.length==1 && _all_true_s[0]==true)?true:false;
	return _all_true;
	});
filtered = _.toArray(filtered);
var ordered = _.sortBy(filtered,'amostrar');
return ordered;
};
_.defer(function() { 
 Widget.Collections.temp_multiple.fetch(); 
 });
};

/** 
* Funcion que actualiza los datos del selector
* 
* @param data Recibe array de estructuras con campos: label (string), valor (any) y estado (int 0,1) para seleccionar
* @param valor Si se define, oculta hint y muestra 
*/
$.update = function(params) {
if (!_.isUndefined(params.data)) {
/** 
* Recibimos datos en caso de que se hayan mandado datos desde la pantalla que ocupa el widget 
*/
/** 
* Se guardan los datos que trae lo que venga de la pantalla que utiliza el widget 
*/
require(WPATH('vars'))[args.__id]['data']=params.data;
if (_.isArray(params.data)) {
/** 
* Consulta si los datos que se traen de la pantalla que utiliza el widget es un array 
*/
/** 
* recuperar variable &quot;id_instancia&quot; 
*/
var id_instancia = ('id_instancia' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['id_instancia']:'';
/** 
* Se eliminan los datos que esten cargados en el modelo temp_multiple 
*/
/** 
* Se eliminan los datos que esten cargados en el modelo temp_multiple 
*/
Widget.Collections.temp_multiple.fetch();
var ID_1380315445_i=Widget.Collections.temp_multiple;
//filtramos a borrar
var to_delete_ID_1380315445=[];
for (var reg_ID_1380315445=0;reg_ID_1380315445<ID_1380315445_i.models.length;reg_ID_1380315445++) {
  if ('idinstancia' in ID_1380315445_i.models[reg_ID_1380315445].attributes && ID_1380315445_i.models[reg_ID_1380315445].attributes['idinstancia'] === id_instancia) {
    to_delete_ID_1380315445.push(Widget.Collections.temp_multiple.at(reg_ID_1380315445));
  }
}
//borramos marcados
for (var ii_ID_1380315445 in to_delete_ID_1380315445) {
  to_delete_ID_1380315445[ii_ID_1380315445].destroy();
  Widget.Collections.temp_multiple.remove(to_delete_ID_1380315445[ii_ID_1380315445], { silent:true });
  Widget.Collections.temp_multiple.remove(to_delete_ID_1380315445[ii_ID_1380315445]);
}
ID_1380315445_i.trigger('remove');
var params_data = params.data;
var ID_1293594803_m=Widget.Collections.instance("temp_multiple");
_.each(params_data, function(ID_1293594803_fila,pos) {
   var ID_1293594803_modelo = Widget.createModel('temp_multiple', { valor : ID_1293594803_fila.valor, 
idinstancia : id_instancia, 
estado : ID_1293594803_fila.estado, 
amostrar : ID_1293594803_fila.label });
   ID_1293594803_m.add(ID_1293594803_modelo, { silent: true });
   ID_1293594803_modelo.save();
});
_.defer(function() { 
   Widget.Collections.instance("temp_multiple").fetch();
});
/** 
* recuperar variable &quot;cargando&quot; 
*/
var cargando = ('cargando' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['cargando']:'';
var hint = ('hint' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['hint']:'';
if ((_.isObject(cargando) || (_.isString(cargando)) &&  !_.isEmpty(cargando)) || _.isNumber(cargando) || _.isBoolean(cargando)) {
/** 
* Consulta si variable &quot;cargando&quot; tiene algun valor 
*/
/** 
* Modifica el texto del hint del selector por el valor de la variable &quot;hint&quot; 
*/
$.ID_1041790471.setText(hint);

/** 
* Mostramos hint 
*/
var ID_1041790471_visible = true;

								  if (ID_1041790471_visible=='si') {
									  ID_1041790471_visible=true;
								  } else if (ID_1041790471_visible=='no') {
									  ID_1041790471_visible=false;
								  }
								  $.ID_1041790471.setVisible(ID_1041790471_visible);

/** 
* Ocultamos el valor que haya estado en el selector. 
*/
var ID_952750652_visible = false;

								  if (ID_952750652_visible=='si') {
									  ID_952750652_visible=true;
								  } else if (ID_952750652_visible=='no') {
									  ID_952750652_visible=false;
								  }
								  $.ID_952750652.setVisible(ID_952750652_visible);

}
}
}
if (!_.isUndefined(params.valor)) {
/** 
* Establecemos valor pre-seleccionado 
*/
if ((_.isObject(params.valor) ||_.isString(params.valor)) &&  _.isEmpty(params.valor)) {
/** 
* Revisamos si el string de valor esta vacio 
*/
/** 
* Mostramos hint 
*/
var ID_1041790471_visible = true;

								  if (ID_1041790471_visible=='si') {
									  ID_1041790471_visible=true;
								  } else if (ID_1041790471_visible=='no') {
									  ID_1041790471_visible=false;
								  }
								  $.ID_1041790471.setVisible(ID_1041790471_visible);

/** 
* Ocultamos valor 
*/
var ID_952750652_visible = false;

								  if (ID_952750652_visible=='si') {
									  ID_952750652_visible=true;
								  } else if (ID_952750652_visible=='no') {
									  ID_952750652_visible=false;
								  }
								  $.ID_952750652.setVisible(ID_952750652_visible);

}
 else {
/** 
* Mostramos valor indicado 
*/
$.ID_952750652.setText(params.valor);

var ID_952750652_visible = true;

								  if (ID_952750652_visible=='si') {
									  ID_952750652_visible=true;
								  } else if (ID_952750652_visible=='no') {
									  ID_952750652_visible=false;
								  }
								  $.ID_952750652.setVisible(ID_952750652_visible);

/** 
* Ocultamos hint 
*/
var ID_1041790471_visible = false;

								  if (ID_1041790471_visible=='si') {
									  ID_1041790471_visible=true;
								  } else if (ID_1041790471_visible=='no') {
									  ID_1041790471_visible=false;
								  }
								  $.ID_1041790471.setVisible(ID_1041790471_visible);

}}
 else {
/** 
* Recuperamos los datos que fueron mandados desde la pantalla que utiliza el widget 
*/
var data = ('data' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['data']:'';
/** 
* Creamos una variable local que guardara los valores escogidos 
*/
require(WPATH('vars'))[args.__id]['elegidos']='';
/** 
* Recupera la variable id_instacia con el id del widget 
*/
var id_instancia = ('id_instancia' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['id_instancia']:'';
/** 
* Consultamos el modelo temp_multiple, filtrando por la id_instancia del widget y ordenados por el valor de amostrar de manera ascendente. Y el resultadoes almacenado en una variable llamada datos 
*/
if (Widget.Collections.temp_multiple.models.length==0) Widget.Collections.temp_multiple.fetch();
var ID_389555799_i=Widget.Collections.temp_multiple;
//filtramos modelos segun consulta (where futuro linkeado) y armamos respuesta
var ID_389555799_i_where=[], datos=[], struct={}
for (var reg_ID_389555799=0;reg_ID_389555799<ID_389555799_i.models.length;reg_ID_389555799++) {
  if ('idinstancia' in ID_389555799_i.models[reg_ID_389555799].attributes && ID_389555799_i.models[reg_ID_389555799].attributes['idinstancia'] == id_instancia) {
    ID_389555799_i_where.push(Widget.Collections.temp_multiple.at(reg_ID_389555799));
    struct={};
    for (var key in ID_389555799_i.models[reg_ID_389555799].attributes) {
      struct[key]=ID_389555799_i.models[reg_ID_389555799].attributes[key];
    }
    datos.push(struct);
  }
}
/** 
* Recuperamos la variable elegidos recien creada 
*/
var elegidos = ('elegidos' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['elegidos']:'';
var item_index = 0;
_.each(datos, function(item, item_pos, item_list) {
item_index += 1;
if ((_.isObject(elegidos) ||_.isString(elegidos)) &&  _.isEmpty(elegidos)) {
/** 
* Revisamos si la variable elegidos esta vacia 
*/
if (item.estado==1||item.estado=='1') {
/** 
* Revisamos cada item que este en la variable datos 
*/
/** 
* Almacena el primer nombre del item 
*/
 elegidos = item.amostrar;
}
}
 else {
if (item.estado==1||item.estado=='1') {
/** 
* En caso de que exista mas de un valor en elegidos, se va concatenando los nombres 
*/
 elegidos += ' + ' + item.amostrar;
}
}});
/** 
* Se actualiza la variable con el/los nombres que estaban activos en los datos 
*/
require(WPATH('vars'))[args.__id]['elegidos']=elegidos;
/** 
* Mostramos valor procesado. Cambiando el texto del valor seleccionado con la variable de elegidos (que traia todos los items activos) y dejando el texto visible 
*/
$.ID_952750652.setText(elegidos);

var ID_952750652_visible = true;

								  if (ID_952750652_visible=='si') {
									  ID_952750652_visible=true;
								  } else if (ID_952750652_visible=='no') {
									  ID_952750652_visible=false;
								  }
								  $.ID_952750652.setVisible(ID_952750652_visible);

if ((_.isObject(elegidos) ||_.isString(elegidos)) &&  _.isEmpty(elegidos)) {
/** 
* En el caso de que no hayan items elegidos se muestra el hint y se oculta el valor 
*/
/** 
* Mostramos hint 
*/
var ID_1041790471_visible = true;

								  if (ID_1041790471_visible=='si') {
									  ID_1041790471_visible=true;
								  } else if (ID_1041790471_visible=='no') {
									  ID_1041790471_visible=false;
								  }
								  $.ID_1041790471.setVisible(ID_1041790471_visible);

/** 
* Ocultamos valor 
*/
var ID_952750652_visible = false;

								  if (ID_952750652_visible=='si') {
									  ID_952750652_visible=true;
								  } else if (ID_952750652_visible=='no') {
									  ID_952750652_visible=false;
								  }
								  $.ID_952750652.setVisible(ID_952750652_visible);

}
 else {
/** 
* &#160;&#160; 
*/
/** 
* Ocultamos hint 
*/
var ID_1041790471_visible = false;

								  if (ID_1041790471_visible=='si') {
									  ID_1041790471_visible=true;
								  } else if (ID_1041790471_visible=='no') {
									  ID_1041790471_visible=false;
								  }
								  $.ID_1041790471.setVisible(ID_1041790471_visible);

/** 
* Mostramos valor 
*/
var ID_952750652_visible = true;

								  if (ID_952750652_visible=='si') {
									  ID_952750652_visible=true;
								  } else if (ID_952750652_visible=='no') {
									  ID_952750652_visible=false;
								  }
								  $.ID_952750652.setVisible(ID_952750652_visible);

}}};

/** 
* Funcion que limpia las variables y contenidos de la memoria luego de utilizar widget 
*/
$.limpiar = function(params) {
/** 
* recuperar variable &quot;params&quot; 
*/
var params = ('params' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['params']:'';
Widget.Collections.temp_multiple.fetch();
var ID_1314983014_i=Widget.Collections.temp_multiple;
//filtramos a borrar
var to_delete_ID_1314983014=[];
for (var reg_ID_1314983014=0;reg_ID_1314983014<ID_1314983014_i.models.length;reg_ID_1314983014++) {
  if ('idinstancia' in ID_1314983014_i.models[reg_ID_1314983014].attributes && ID_1314983014_i.models[reg_ID_1314983014].attributes['idinstancia'] === params.__id) {
    to_delete_ID_1314983014.push(Widget.Collections.temp_multiple.at(reg_ID_1314983014));
  }
}
//borramos marcados
for (var ii_ID_1314983014 in to_delete_ID_1314983014) {
  to_delete_ID_1314983014[ii_ID_1314983014].destroy();
  Widget.Collections.temp_multiple.remove(to_delete_ID_1314983014[ii_ID_1314983014], { silent:true });
  Widget.Collections.temp_multiple.remove(to_delete_ID_1314983014[ii_ID_1314983014]);
}
ID_1314983014_i.trigger('remove');
};