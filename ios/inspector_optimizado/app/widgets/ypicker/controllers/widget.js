/** 
* Widget Selector de Anio
* Representa un control para elegir anios
* 
* @param aceptar Indica el texto del boton aceptar
* @param cancelar Indica el texto del boton cancelar
* @param desde Indica desde que anio se puede elegir
* @param hasta Indica hasta que anio se puede elegir 
*/
var _bind4section={};

var args = arguments[0] || {};

function Click_ID_1609782075(e) {

e.cancelBubble=true;
var elemento=e.source;
require(WPATH('vars'))[args.__id]['flag']='cerrado';
$.ID_1894636949.animate({
bottom: -265,
duration: 300
});

if ('__args' in args) {
	args['__args'].oncancelar({});
} else {
	args.oncancelar({});
}

}
function Click_ID_1138708313(e) {

e.cancelBubble=true;
var elemento=e.source;
require(WPATH('vars'))[args.__id]['flag']='cerrado';
$.ID_1894636949.animate({
bottom: -265,
duration: 300
});
var ano_elegido = ('ano_elegido' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['ano_elegido']:'';

if ('__args' in args) {
	args['__args'].onaceptar({valor : ano_elegido});
} else {
	args.onaceptar({valor : ano_elegido});
}

}


function Change_ID_1698185812(e) {

e.cancelBubble=true;
var elemento=e;
var _columna=e.columnIndex;
var columna=e.columnIndex+1;
var _fila=e.rowIndex;
var fila=e.rowIndex+1;
if (_.isArray(elemento.selectedValue)) {
if (_.isObject(elemento.selectedValue[0]) && !_.isArray(elemento.selectedValue[0]) && !_.isFunction(elemento.selectedValue[0])) {
require(WPATH('vars'))[args.__id]['ano_elegido']=elemento.selectedValue[0].value;
}
 else {
require(WPATH('vars'))[args.__id]['ano_elegido']=elemento.selectedValue[0];
}}
 else {
require(WPATH('vars'))[args.__id]['ano_elegido']=elemento.value;
}if (false) console.log('debug picker change',{"elemento":elemento});

}
/** 
* Funcion que inicializa widget 
*/

$.init = function(params) {
   for (var tobe in params) args[tobe] = params[tobe];
   if ('__id' in params) require(WPATH('vars'))[params.__id]={};
/** 
* Iniciamos variable 
*/
require(WPATH('vars'))[args.__id]['flag']='cerrado';
if (!_.isUndefined(params.aceptar)) {
/** 
* verifica que exista params.aceptar 
*/
$.ID_1112469673.setText(params.aceptar);

}
if (!_.isUndefined(params.cancelar)) {
/** 
* verifica que exista params.cancelar 
*/
$.ID_1794507818.setText(params.cancelar);

}
/** 
* declaramos e instanciamos variable ano_actual con el a&#241;o actual 
*/
 ano_actual = new Date().getFullYear();
/** 
* iniciamos variable desde con el valor de ano_actual menos 100 
*/
require(WPATH('vars'))[args.__id]['desde']=ano_actual-100;
/** 
* iniciamos variable hasta con el valor de ano_actual mas 1 
*/
require(WPATH('vars'))[args.__id]['hasta']=ano_actual;
if (!_.isUndefined(params.desde)) {
/** 
* verifica que exista params.desde 
*/
require(WPATH('vars'))[args.__id]['desde']=params.desde;
}
if (!_.isUndefined(params.hasta)) {
/** 
* verifica que exista params.hasta 
*/
require(WPATH('vars'))[args.__id]['hasta']=params.hasta;
}
var desde = ('desde' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['desde']:'';
var hasta = ('hasta' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['hasta']:'';
 rango = _.range(hasta,desde,-1);
var ano_index = 0;
_.each(rango, function(ano, ano_pos, ano_list) {
ano_index += 1;
var ID_869828797 = Titanium.UI.createPickerRow({
item : ano,
value : ano
}
);
var ID_752984564 = Titanium.UI.createLabel({
text : ano,
color : '#000000',
touchEnabled : false
}
);
ID_869828797.add(ID_752984564);

$.ID_884559890.addRow(ID_869828797);
});
};

/** 
* Funcion que abre control 
*/

$.abrir = function(params) {
var flag = ('flag' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['flag']:'';
if (flag=='cerrado') {
require(WPATH('vars'))[args.__id]['flag']='abierto';
$.ID_1894636949.animate({
bottom: 0,
duration: 300
}, function() {

var ID_1417749630_trycatch = { error: function(e) { if (false) console.log('error en comando probar: recuerda poner evento ?error como primer hijo.',e); } };
try {
ID_1417749630_trycatch.error = function(evento) {
};

if ('__args' in args) {
	args['__args'].onabierto({});
} else {
	args.onabierto({});
}
} catch (e) {
   ID_1417749630_trycatch.error(e);
}
});
}
};

/** 
* Funcion que oculta control 
*/

$.cerrar = function(params) {
var flag = ('flag' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['flag']:'';
if (flag=='abierto') {
require(WPATH('vars'))[args.__id]['flag']='cerrado';
$.ID_1894636949.animate({
bottom: -265,
duration: 300
});
}
};