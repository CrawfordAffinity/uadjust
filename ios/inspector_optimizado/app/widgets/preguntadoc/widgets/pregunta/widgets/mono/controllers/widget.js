/** 
* Widget Mono-Personaje
* Muestra diferentes tipos de personajes con un mensaje de texto personalizable
* 
* @param visible Indica visibilidad de widget
* @param tipo Indica tipo de personaje: tip, info, sorry 
*/
var _bind4section={};

var args = arguments[0] || {};

/** 
* Funcion que inicializa widget 
*/

$.init = function(params) {
   for (var tobe in params) args[tobe] = params[tobe];
   if ('__id' in params) require(WPATH('vars'))[params.__id]={};
require(WPATH('vars'))[args.__id]['visible']='true';
if (!_.isUndefined(params.visible)) {
require(WPATH('vars'))[args.__id]['visible']=params.visible;
}
var visible = ('visible' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['visible']:'';
if (!_.isUndefined(params.tipo)) {
if (params.tipo=='_tip') {
require(WPATH('vars'))[args.__id]['alto']=120;
require(WPATH('vars'))[args.__id]['tipo']='_tip';
var ID_165089027_visible = visible;

										  if (ID_165089027_visible=='si') {
											  ID_165089027_visible=true;
										  } else if (ID_165089027_visible=='no') {
											  ID_165089027_visible=false;
										  }
										  $.ID_165089027.setVisible(ID_165089027_visible);

var ID_165089027_alto = 120;

								  if (ID_165089027_alto=='*') {
									  ID_165089027_alto=Ti.UI.FILL;
								  } else if (ID_165089027_alto=='-') {
									  ID_165089027_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_165089027_alto)) {
									  ID_165089027_alto=ID_165089027_alto+'dp';
								  }
								  $.ID_165089027.setHeight(ID_165089027_alto);

var ID_1845899524_visible = false;

										  if (ID_1845899524_visible=='si') {
											  ID_1845899524_visible=true;
										  } else if (ID_1845899524_visible=='no') {
											  ID_1845899524_visible=false;
										  }
										  $.ID_1845899524.setVisible(ID_1845899524_visible);

var ID_1845899524_alto = 0;

								  if (ID_1845899524_alto=='*') {
									  ID_1845899524_alto=Ti.UI.FILL;
								  } else if (ID_1845899524_alto=='-') {
									  ID_1845899524_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1845899524_alto)) {
									  ID_1845899524_alto=ID_1845899524_alto+'dp';
								  }
								  $.ID_1845899524.setHeight(ID_1845899524_alto);

var ID_1015068704_visible = false;

										  if (ID_1015068704_visible=='si') {
											  ID_1015068704_visible=true;
										  } else if (ID_1015068704_visible=='no') {
											  ID_1015068704_visible=false;
										  }
										  $.ID_1015068704.setVisible(ID_1015068704_visible);

var ID_1015068704_alto = 0;

								  if (ID_1015068704_alto=='*') {
									  ID_1015068704_alto=Ti.UI.FILL;
								  } else if (ID_1015068704_alto=='-') {
									  ID_1015068704_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1015068704_alto)) {
									  ID_1015068704_alto=ID_1015068704_alto+'dp';
								  }
								  $.ID_1015068704.setHeight(ID_1015068704_alto);

if (!_.isUndefined(params.texto)) {
$.ID_1715188451.setText(params.texto);

}
if (!_.isUndefined(params.top)) {
$.ID_165089027.setTop(params.top);

}
if (!_.isUndefined(params.bottom)) {
$.ID_165089027.setBottom(params.bottom);

}
if (!_.isUndefined(params.ancho)) {
var ID_165089027_ancho = params.ancho;

								  if (ID_165089027_ancho=='*') {
									  ID_165089027_ancho=Ti.UI.FILL;
								  } else if (ID_165089027_ancho=='-') {
									  ID_165089027_ancho=Ti.UI.SIZE;
								  } else if (!isNaN(ID_165089027_ancho)) {
									  ID_165089027_ancho=ID_165089027_ancho+'dp';
								  }
								  $.ID_165089027.setWidth(ID_165089027_ancho);

}
if (!_.isUndefined(params.alto)) {
var ID_165089027_alto = params.alto;

								  if (ID_165089027_alto=='*') {
									  ID_165089027_alto=Ti.UI.FILL;
								  } else if (ID_165089027_alto=='-') {
									  ID_165089027_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_165089027_alto)) {
									  ID_165089027_alto=ID_165089027_alto+'dp';
								  }
								  $.ID_165089027.setHeight(ID_165089027_alto);

require(WPATH('vars'))[args.__id]['alto']=params.alto;
}
if (!_.isUndefined(params.titulo)) {
$.ID_424033862.setText(params.titulo);

var ID_424033862_visible = true;

								  if (ID_424033862_visible=='si') {
									  ID_424033862_visible=true;
								  } else if (ID_424033862_visible=='no') {
									  ID_424033862_visible=false;
								  }
								  $.ID_424033862.setVisible(ID_424033862_visible);

}
}
 else if (params.tipo=='_sorry') {
require(WPATH('vars'))[args.__id]['tipo']='_sorry';
require(WPATH('vars'))[args.__id]['alto']='-';
var ID_1845899524_visible = visible;

										  if (ID_1845899524_visible=='si') {
											  ID_1845899524_visible=true;
										  } else if (ID_1845899524_visible=='no') {
											  ID_1845899524_visible=false;
										  }
										  $.ID_1845899524.setVisible(ID_1845899524_visible);

var ID_1845899524_alto = '-';

								  if (ID_1845899524_alto=='*') {
									  ID_1845899524_alto=Ti.UI.FILL;
								  } else if (ID_1845899524_alto=='-') {
									  ID_1845899524_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1845899524_alto)) {
									  ID_1845899524_alto=ID_1845899524_alto+'dp';
								  }
								  $.ID_1845899524.setHeight(ID_1845899524_alto);

var ID_165089027_visible = false;

										  if (ID_165089027_visible=='si') {
											  ID_165089027_visible=true;
										  } else if (ID_165089027_visible=='no') {
											  ID_165089027_visible=false;
										  }
										  $.ID_165089027.setVisible(ID_165089027_visible);

var ID_165089027_alto = 0;

								  if (ID_165089027_alto=='*') {
									  ID_165089027_alto=Ti.UI.FILL;
								  } else if (ID_165089027_alto=='-') {
									  ID_165089027_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_165089027_alto)) {
									  ID_165089027_alto=ID_165089027_alto+'dp';
								  }
								  $.ID_165089027.setHeight(ID_165089027_alto);

var ID_1015068704_visible = false;

										  if (ID_1015068704_visible=='si') {
											  ID_1015068704_visible=true;
										  } else if (ID_1015068704_visible=='no') {
											  ID_1015068704_visible=false;
										  }
										  $.ID_1015068704.setVisible(ID_1015068704_visible);

var ID_1015068704_alto = 0;

								  if (ID_1015068704_alto=='*') {
									  ID_1015068704_alto=Ti.UI.FILL;
								  } else if (ID_1015068704_alto=='-') {
									  ID_1015068704_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1015068704_alto)) {
									  ID_1015068704_alto=ID_1015068704_alto+'dp';
								  }
								  $.ID_1015068704.setHeight(ID_1015068704_alto);

if (!_.isUndefined(params.titulo)) {
$.ID_344364396.setText(params.titulo);

}
if (!_.isUndefined(params.texto)) {
$.ID_760973822.setText(params.texto);

}
if (!_.isUndefined(params.top)) {
$.ID_1845899524.setTop(params.top);

}
if (!_.isUndefined(params.bottom)) {
$.ID_1845899524.setBottom(params.bottom);

}
if (!_.isUndefined(params.ancho)) {
var ID_1845899524_ancho = params.ancho;

								  if (ID_1845899524_ancho=='*') {
									  ID_1845899524_ancho=Ti.UI.FILL;
								  } else if (ID_1845899524_ancho=='-') {
									  ID_1845899524_ancho=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1845899524_ancho)) {
									  ID_1845899524_ancho=ID_1845899524_ancho+'dp';
								  }
								  $.ID_1845899524.setWidth(ID_1845899524_ancho);

}
if (!_.isUndefined(params.alto)) {
var ID_1845899524_alto = params.alto;

								  if (ID_1845899524_alto=='*') {
									  ID_1845899524_alto=Ti.UI.FILL;
								  } else if (ID_1845899524_alto=='-') {
									  ID_1845899524_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1845899524_alto)) {
									  ID_1845899524_alto=ID_1845899524_alto+'dp';
								  }
								  $.ID_1845899524.setHeight(ID_1845899524_alto);

require(WPATH('vars'))[args.__id]['alto']=params.alto;
}
} else if (params.tipo=='_info') {
require(WPATH('vars'))[args.__id]['tipo']='_info';
require(WPATH('vars'))[args.__id]['alto']='-';
var ID_1015068704_visible = visible;

										  if (ID_1015068704_visible=='si') {
											  ID_1015068704_visible=true;
										  } else if (ID_1015068704_visible=='no') {
											  ID_1015068704_visible=false;
										  }
										  $.ID_1015068704.setVisible(ID_1015068704_visible);

var ID_1015068704_alto = '-';

								  if (ID_1015068704_alto=='*') {
									  ID_1015068704_alto=Ti.UI.FILL;
								  } else if (ID_1015068704_alto=='-') {
									  ID_1015068704_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1015068704_alto)) {
									  ID_1015068704_alto=ID_1015068704_alto+'dp';
								  }
								  $.ID_1015068704.setHeight(ID_1015068704_alto);

var ID_165089027_visible = false;

										  if (ID_165089027_visible=='si') {
											  ID_165089027_visible=true;
										  } else if (ID_165089027_visible=='no') {
											  ID_165089027_visible=false;
										  }
										  $.ID_165089027.setVisible(ID_165089027_visible);

var ID_165089027_alto = 0;

								  if (ID_165089027_alto=='*') {
									  ID_165089027_alto=Ti.UI.FILL;
								  } else if (ID_165089027_alto=='-') {
									  ID_165089027_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_165089027_alto)) {
									  ID_165089027_alto=ID_165089027_alto+'dp';
								  }
								  $.ID_165089027.setHeight(ID_165089027_alto);

var ID_1845899524_visible = false;

										  if (ID_1845899524_visible=='si') {
											  ID_1845899524_visible=true;
										  } else if (ID_1845899524_visible=='no') {
											  ID_1845899524_visible=false;
										  }
										  $.ID_1845899524.setVisible(ID_1845899524_visible);

var ID_1845899524_alto = 0;

								  if (ID_1845899524_alto=='*') {
									  ID_1845899524_alto=Ti.UI.FILL;
								  } else if (ID_1845899524_alto=='-') {
									  ID_1845899524_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1845899524_alto)) {
									  ID_1845899524_alto=ID_1845899524_alto+'dp';
								  }
								  $.ID_1845899524.setHeight(ID_1845899524_alto);

if (!_.isUndefined(params.titulo)) {
$.ID_184880256.setText(params.titulo);

}
if (!_.isUndefined(params.texto)) {
$.ID_1906529925.setText(params.texto);

}
if (!_.isUndefined(params.top)) {
$.ID_1015068704.setTop(params.top);

}
if (!_.isUndefined(params.bottom)) {
$.ID_1015068704.setBottom(params.bottom);

}
if (!_.isUndefined(params.ancho)) {
var ID_1015068704_ancho = params.ancho;

								  if (ID_1015068704_ancho=='*') {
									  ID_1015068704_ancho=Ti.UI.FILL;
								  } else if (ID_1015068704_ancho=='-') {
									  ID_1015068704_ancho=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1015068704_ancho)) {
									  ID_1015068704_ancho=ID_1015068704_ancho+'dp';
								  }
								  $.ID_1015068704.setWidth(ID_1015068704_ancho);

}
if (!_.isUndefined(params.alto)) {
var ID_1015068704_alto = params.alto;

								  if (ID_1015068704_alto=='*') {
									  ID_1015068704_alto=Ti.UI.FILL;
								  } else if (ID_1015068704_alto=='-') {
									  ID_1015068704_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1015068704_alto)) {
									  ID_1015068704_alto=ID_1015068704_alto+'dp';
								  }
								  $.ID_1015068704.setHeight(ID_1015068704_alto);

require(WPATH('vars'))[args.__id]['alto']=params.alto;
}
}}
};

/** 
* Funcion que actualiza texto y/o personaje
* 
* @param visible Indica si mostrar o no este widget
* @param tipo Tipo de personaje: tip, info, sorry 
*/

$.update = function(params) {
if (!_.isUndefined(params.tipo)) {
if (params.tipo=='_tip') {
require(WPATH('vars'))[args.__id]['tipo']='_tip';
}
 else if (params.tipo=='_sorry') {
require(WPATH('vars'))[args.__id]['tipo']='_sorry';
} else if (params.tipo=='_info') {
require(WPATH('vars'))[args.__id]['tipo']='_info';
}}
if (!_.isUndefined(params.visible)) {
require(WPATH('vars'))[args.__id]['visible']=params.visible;
}
var tipo = ('tipo' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['tipo']:'';
var visible = ('visible' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['visible']:'';
var alto = ('alto' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['alto']:'';
if (tipo=='_tip') {
var ID_165089027_visible = visible;

										  if (ID_165089027_visible=='si') {
											  ID_165089027_visible=true;
										  } else if (ID_165089027_visible=='no') {
											  ID_165089027_visible=false;
										  }
										  $.ID_165089027.setVisible(ID_165089027_visible);

var ID_165089027_alto = alto;

								  if (ID_165089027_alto=='*') {
									  ID_165089027_alto=Ti.UI.FILL;
								  } else if (ID_165089027_alto=='-') {
									  ID_165089027_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_165089027_alto)) {
									  ID_165089027_alto=ID_165089027_alto+'dp';
								  }
								  $.ID_165089027.setHeight(ID_165089027_alto);

var ID_1845899524_visible = false;

										  if (ID_1845899524_visible=='si') {
											  ID_1845899524_visible=true;
										  } else if (ID_1845899524_visible=='no') {
											  ID_1845899524_visible=false;
										  }
										  $.ID_1845899524.setVisible(ID_1845899524_visible);

var ID_1845899524_alto = 0;

								  if (ID_1845899524_alto=='*') {
									  ID_1845899524_alto=Ti.UI.FILL;
								  } else if (ID_1845899524_alto=='-') {
									  ID_1845899524_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1845899524_alto)) {
									  ID_1845899524_alto=ID_1845899524_alto+'dp';
								  }
								  $.ID_1845899524.setHeight(ID_1845899524_alto);

var ID_1015068704_visible = false;

										  if (ID_1015068704_visible=='si') {
											  ID_1015068704_visible=true;
										  } else if (ID_1015068704_visible=='no') {
											  ID_1015068704_visible=false;
										  }
										  $.ID_1015068704.setVisible(ID_1015068704_visible);

var ID_1015068704_alto = 0;

								  if (ID_1015068704_alto=='*') {
									  ID_1015068704_alto=Ti.UI.FILL;
								  } else if (ID_1015068704_alto=='-') {
									  ID_1015068704_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1015068704_alto)) {
									  ID_1015068704_alto=ID_1015068704_alto+'dp';
								  }
								  $.ID_1015068704.setHeight(ID_1015068704_alto);

if (!_.isUndefined(params.texto)) {
$.ID_1715188451.setText(params.texto);

}
if (!_.isUndefined(params.top)) {
$.ID_165089027.setTop(params.top);

}
if (!_.isUndefined(params.bottom)) {
$.ID_165089027.setBottom(params.bottom);

}
if (!_.isUndefined(params.ancho)) {
var ID_165089027_ancho = params.ancho;

								  if (ID_165089027_ancho=='*') {
									  ID_165089027_ancho=Ti.UI.FILL;
								  } else if (ID_165089027_ancho=='-') {
									  ID_165089027_ancho=Ti.UI.SIZE;
								  } else if (!isNaN(ID_165089027_ancho)) {
									  ID_165089027_ancho=ID_165089027_ancho+'dp';
								  }
								  $.ID_165089027.setWidth(ID_165089027_ancho);

}
if (!_.isUndefined(params.alto)) {
var ID_165089027_alto = params.alto;

								  if (ID_165089027_alto=='*') {
									  ID_165089027_alto=Ti.UI.FILL;
								  } else if (ID_165089027_alto=='-') {
									  ID_165089027_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_165089027_alto)) {
									  ID_165089027_alto=ID_165089027_alto+'dp';
								  }
								  $.ID_165089027.setHeight(ID_165089027_alto);

require(WPATH('vars'))[args.__id]['alto']=params.alto;
}
if (!_.isUndefined(params.titulo)) {
if ((_.isObject(params.titulo) || (_.isString(params.titulo)) &&  !_.isEmpty(params.titulo)) || _.isNumber(params.titulo) || _.isBoolean(params.titulo)) {
$.ID_424033862.setText(params.titulo);

var ID_424033862_visible = true;

								  if (ID_424033862_visible=='si') {
									  ID_424033862_visible=true;
								  } else if (ID_424033862_visible=='no') {
									  ID_424033862_visible=false;
								  }
								  $.ID_424033862.setVisible(ID_424033862_visible);

}
 else {
$.ID_424033862.setText(params.titulo);

var ID_424033862_visible = false;

								  if (ID_424033862_visible=='si') {
									  ID_424033862_visible=true;
								  } else if (ID_424033862_visible=='no') {
									  ID_424033862_visible=false;
								  }
								  $.ID_424033862.setVisible(ID_424033862_visible);

}}
}
 else if (tipo=='_sorry') {
var ID_1845899524_visible = visible;

										  if (ID_1845899524_visible=='si') {
											  ID_1845899524_visible=true;
										  } else if (ID_1845899524_visible=='no') {
											  ID_1845899524_visible=false;
										  }
										  $.ID_1845899524.setVisible(ID_1845899524_visible);

var ID_1845899524_alto = alto;

								  if (ID_1845899524_alto=='*') {
									  ID_1845899524_alto=Ti.UI.FILL;
								  } else if (ID_1845899524_alto=='-') {
									  ID_1845899524_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1845899524_alto)) {
									  ID_1845899524_alto=ID_1845899524_alto+'dp';
								  }
								  $.ID_1845899524.setHeight(ID_1845899524_alto);

var ID_165089027_visible = false;

										  if (ID_165089027_visible=='si') {
											  ID_165089027_visible=true;
										  } else if (ID_165089027_visible=='no') {
											  ID_165089027_visible=false;
										  }
										  $.ID_165089027.setVisible(ID_165089027_visible);

var ID_165089027_alto = 0;

								  if (ID_165089027_alto=='*') {
									  ID_165089027_alto=Ti.UI.FILL;
								  } else if (ID_165089027_alto=='-') {
									  ID_165089027_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_165089027_alto)) {
									  ID_165089027_alto=ID_165089027_alto+'dp';
								  }
								  $.ID_165089027.setHeight(ID_165089027_alto);

var ID_1015068704_visible = false;

										  if (ID_1015068704_visible=='si') {
											  ID_1015068704_visible=true;
										  } else if (ID_1015068704_visible=='no') {
											  ID_1015068704_visible=false;
										  }
										  $.ID_1015068704.setVisible(ID_1015068704_visible);

var ID_1015068704_alto = 0;

								  if (ID_1015068704_alto=='*') {
									  ID_1015068704_alto=Ti.UI.FILL;
								  } else if (ID_1015068704_alto=='-') {
									  ID_1015068704_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1015068704_alto)) {
									  ID_1015068704_alto=ID_1015068704_alto+'dp';
								  }
								  $.ID_1015068704.setHeight(ID_1015068704_alto);

if (!_.isUndefined(params.titulo)) {
$.ID_344364396.setText(params.titulo);

}
if (!_.isUndefined(params.texto)) {
$.ID_760973822.setText(params.texto);

}
if (!_.isUndefined(params.top)) {
$.ID_1845899524.setTop(params.top);

}
if (!_.isUndefined(params.bottom)) {
$.ID_1845899524.setBottom(params.bottom);

}
if (!_.isUndefined(params.ancho)) {
var ID_1845899524_ancho = params.ancho;

								  if (ID_1845899524_ancho=='*') {
									  ID_1845899524_ancho=Ti.UI.FILL;
								  } else if (ID_1845899524_ancho=='-') {
									  ID_1845899524_ancho=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1845899524_ancho)) {
									  ID_1845899524_ancho=ID_1845899524_ancho+'dp';
								  }
								  $.ID_1845899524.setWidth(ID_1845899524_ancho);

}
if (!_.isUndefined(params.alto)) {
var ID_1845899524_alto = params.alto;

								  if (ID_1845899524_alto=='*') {
									  ID_1845899524_alto=Ti.UI.FILL;
								  } else if (ID_1845899524_alto=='-') {
									  ID_1845899524_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1845899524_alto)) {
									  ID_1845899524_alto=ID_1845899524_alto+'dp';
								  }
								  $.ID_1845899524.setHeight(ID_1845899524_alto);

require(WPATH('vars'))[args.__id]['alto']=params.alto;
}
} else if (tipo=='_info') {
var ID_1015068704_visible = visible;

										  if (ID_1015068704_visible=='si') {
											  ID_1015068704_visible=true;
										  } else if (ID_1015068704_visible=='no') {
											  ID_1015068704_visible=false;
										  }
										  $.ID_1015068704.setVisible(ID_1015068704_visible);

var ID_1015068704_alto = alto;

								  if (ID_1015068704_alto=='*') {
									  ID_1015068704_alto=Ti.UI.FILL;
								  } else if (ID_1015068704_alto=='-') {
									  ID_1015068704_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1015068704_alto)) {
									  ID_1015068704_alto=ID_1015068704_alto+'dp';
								  }
								  $.ID_1015068704.setHeight(ID_1015068704_alto);

var ID_165089027_visible = false;

										  if (ID_165089027_visible=='si') {
											  ID_165089027_visible=true;
										  } else if (ID_165089027_visible=='no') {
											  ID_165089027_visible=false;
										  }
										  $.ID_165089027.setVisible(ID_165089027_visible);

var ID_165089027_alto = 0;

								  if (ID_165089027_alto=='*') {
									  ID_165089027_alto=Ti.UI.FILL;
								  } else if (ID_165089027_alto=='-') {
									  ID_165089027_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_165089027_alto)) {
									  ID_165089027_alto=ID_165089027_alto+'dp';
								  }
								  $.ID_165089027.setHeight(ID_165089027_alto);

var ID_1845899524_visible = false;

										  if (ID_1845899524_visible=='si') {
											  ID_1845899524_visible=true;
										  } else if (ID_1845899524_visible=='no') {
											  ID_1845899524_visible=false;
										  }
										  $.ID_1845899524.setVisible(ID_1845899524_visible);

var ID_1845899524_alto = 0;

								  if (ID_1845899524_alto=='*') {
									  ID_1845899524_alto=Ti.UI.FILL;
								  } else if (ID_1845899524_alto=='-') {
									  ID_1845899524_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1845899524_alto)) {
									  ID_1845899524_alto=ID_1845899524_alto+'dp';
								  }
								  $.ID_1845899524.setHeight(ID_1845899524_alto);

if (!_.isUndefined(params.titulo)) {
$.ID_184880256.setText(params.titulo);

}
if (!_.isUndefined(params.texto)) {
$.ID_1906529925.setText(params.texto);

}
if (!_.isUndefined(params.top)) {
$.ID_1015068704.setTop(params.top);

}
if (!_.isUndefined(params.bottom)) {
$.ID_1015068704.setBottom(params.bottom);

}
if (!_.isUndefined(params.ancho)) {
var ID_1015068704_ancho = params.ancho;

								  if (ID_1015068704_ancho=='*') {
									  ID_1015068704_ancho=Ti.UI.FILL;
								  } else if (ID_1015068704_ancho=='-') {
									  ID_1015068704_ancho=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1015068704_ancho)) {
									  ID_1015068704_ancho=ID_1015068704_ancho+'dp';
								  }
								  $.ID_1015068704.setWidth(ID_1015068704_ancho);

}
if (!_.isUndefined(params.alto)) {
var ID_1015068704_alto = params.alto;

								  if (ID_1015068704_alto=='*') {
									  ID_1015068704_alto=Ti.UI.FILL;
								  } else if (ID_1015068704_alto=='-') {
									  ID_1015068704_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1015068704_alto)) {
									  ID_1015068704_alto=ID_1015068704_alto+'dp';
								  }
								  $.ID_1015068704.setHeight(ID_1015068704_alto);

require(WPATH('vars'))[args.__id]['alto']=params.alto;
}
}};

/** 
* Funcion que oculta widget 
*/

$.ocultar = function(params) {
var ID_165089027_visible = false;

										  if (ID_165089027_visible=='si') {
											  ID_165089027_visible=true;
										  } else if (ID_165089027_visible=='no') {
											  ID_165089027_visible=false;
										  }
										  $.ID_165089027.setVisible(ID_165089027_visible);

var ID_1845899524_visible = false;

										  if (ID_1845899524_visible=='si') {
											  ID_1845899524_visible=true;
										  } else if (ID_1845899524_visible=='no') {
											  ID_1845899524_visible=false;
										  }
										  $.ID_1845899524.setVisible(ID_1845899524_visible);

var ID_1015068704_visible = false;

										  if (ID_1015068704_visible=='si') {
											  ID_1015068704_visible=true;
										  } else if (ID_1015068704_visible=='no') {
											  ID_1015068704_visible=false;
										  }
										  $.ID_1015068704.setVisible(ID_1015068704_visible);

};

/** 
* Funcion que muestra widget 
*/

$.mostrar = function(params) {
var tipo = ('tipo' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['tipo']:'';
var visible = ('visible' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['visible']:'';
if (tipo=='_tip') {
var ID_165089027_visible = visible;

										  if (ID_165089027_visible=='si') {
											  ID_165089027_visible=true;
										  } else if (ID_165089027_visible=='no') {
											  ID_165089027_visible=false;
										  }
										  $.ID_165089027.setVisible(ID_165089027_visible);

var ID_1845899524_visible = false;

										  if (ID_1845899524_visible=='si') {
											  ID_1845899524_visible=true;
										  } else if (ID_1845899524_visible=='no') {
											  ID_1845899524_visible=false;
										  }
										  $.ID_1845899524.setVisible(ID_1845899524_visible);

}
 else if (tipo=='_sorry') {
var ID_165089027_visible = false;

										  if (ID_165089027_visible=='si') {
											  ID_165089027_visible=true;
										  } else if (ID_165089027_visible=='no') {
											  ID_165089027_visible=false;
										  }
										  $.ID_165089027.setVisible(ID_165089027_visible);

var ID_1845899524_visible = visible;

										  if (ID_1845899524_visible=='si') {
											  ID_1845899524_visible=true;
										  } else if (ID_1845899524_visible=='no') {
											  ID_1845899524_visible=false;
										  }
										  $.ID_1845899524.setVisible(ID_1845899524_visible);

} else if (tipo=='_info') {
var ID_165089027_visible = false;

										  if (ID_165089027_visible=='si') {
											  ID_165089027_visible=true;
										  } else if (ID_165089027_visible=='no') {
											  ID_165089027_visible=false;
										  }
										  $.ID_165089027.setVisible(ID_165089027_visible);

var ID_1845899524_visible = false;

										  if (ID_1845899524_visible=='si') {
											  ID_1845899524_visible=true;
										  } else if (ID_1845899524_visible=='no') {
											  ID_1845899524_visible=false;
										  }
										  $.ID_1845899524.setVisible(ID_1845899524_visible);

var ID_1015068704_visible = visible;

										  if (ID_1015068704_visible=='si') {
											  ID_1015068704_visible=true;
										  } else if (ID_1015068704_visible=='no') {
											  ID_1015068704_visible=false;
										  }
										  $.ID_1015068704.setVisible(ID_1015068704_visible);

}};