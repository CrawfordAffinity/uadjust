/** 
* Widget Esta de Acuerdo
* Representa un boton continuar que al ser presionado abre una pantalla
* que contiene el widget Pregunta para decidir si continua con a la siguiente
* pantalla o vuelve a la que estaba para editar los datos.
* Parametros:
* Recupera cualquier parametro que se le este pasando en la pantalla donde se llama al widget. 
*/
var _bind4section={};

var args = arguments[0] || {};

function Click_ID_1776886742(e) {

e.cancelBubble=true;
var elemento=e.source;

if ('__args' in args) {
	args['__args'].onclick({});
} else {
	args.onclick({});
}

}

$.init = function(params) {
   for (var tobe in params) args[tobe] = params[tobe];
   if ('__id' in params) require(WPATH('vars'))[params.__id]={};
if (false) console.log('params obtenidos en init de esta de acuerdo',{"params":params});
require(WPATH('vars'))[args.__id]['params']=params;
require(WPATH('vars'))[args.__id]['mi_id']=params.__id;
};


$.enviar = function(params) {
var nulo = Widget.createController("pregunta_esta_de_acuerdo",{'__args' : (typeof args!=='undefined')?args:__args}).getView();
nulo.open({ modal:true, modalTransitionStyle: Ti.UI.iOS.MODAL_TRANSITION_STYLE_PARTIAL_CURL });

nulo = null;
};