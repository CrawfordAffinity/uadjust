/** 
* Widget Intermedio Esta de Acuerdo
* Representa un control oculto que al ser ejecutado su funcion abrir, abre una pantalla que contiene el widget pregunta para decidir si continua con a la siguiente pantalla o se mantiene donde esta el control, para continuar editando los datos.
* No requiere parametros. 
*/
var _bind4section={};

var args = arguments[0] || {};

function Click_ID_2048697988(e) {

e.cancelBubble=true;
var elemento=e.source;

if ('__args' in args) {
	args['__args'].onclick({});
} else {
	args.onclick({});
}

}
/** 
* Funcion que inicializa widget 
*/

$.init = function(params) {
   for (var tobe in params) args[tobe] = params[tobe];
   if ('__id' in params) require(WPATH('vars'))[params.__id]={};
if (false) console.log('params obtenidos en init de esta de acuerdo',{"params":params});
require(WPATH('vars'))[args.__id]['params']=params;
require(WPATH('vars'))[args.__id]['mi_id']=params.__id;
};

/** 
* Funcion que fuerza apertura de pantalla
* 
* @param color Indica el color del header para la pantalla 
*/

$.enviar = function(params) {
var nulo = Widget.createController("pregunta_esta_de_acuerdo",{'__args' : (typeof args!=='undefined')?args:__args}).getView();
nulo.open({ modal:true, modalTransitionStyle: Ti.UI.iOS.MODAL_TRANSITION_STYLE_PARTIAL_CURL });

nulo = null;
};