/** 
* Widget foto_chica
* Control para centralizar la compresion y escalamiento de las imagenes capturadas, en una caja
* 
* @param caja Indica el ancho-alto de este widget
* @param label Indica el texto que se muestra sobre la imagen de este widget; solo se muestra si esta definido
* @param opcional Indica el subtitulo del texto label para mostrar que imagen es opcional; solo se muestra si esta definido
* @param id Identifica esta instancia para su uso en conjunto con otras en una misma pantalla 
*/
var _bind4section={};

var args = arguments[0] || {};

function Load_ID_891945745(e) {

e.cancelBubble=true;
var elemento=e.source;
var evento=e;
 elemento.start();

}
function Change_ID_573947256(e) {

e.cancelBubble=true;
var elemento=e.source;
var evento=e;
var conteo_end = ('conteo_end' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['conteo_end']:'';
if (evento.index==conteo_end) {
require(WPATH('vars'))[args.__id]['conteo_end']='';
 elemento.pause();
}

}
function Click_ID_1011662708(e) {

e.cancelBubble=true;
var elemento=e.source;
var estado = ('estado' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['estado']:'';
if (false) console.log('estado actual en click',{"estado":estado});
if (estado==0||estado=='0') {
var ID_1352627270_visible = false;

										  if (ID_1352627270_visible=='si') {
											  ID_1352627270_visible=true;
										  } else if (ID_1352627270_visible=='no') {
											  ID_1352627270_visible=false;
										  }
										  $.ID_1352627270.setVisible(ID_1352627270_visible);

var ID_1822037338_visible = true;

										  if (ID_1822037338_visible=='si') {
											  ID_1822037338_visible=true;
										  } else if (ID_1822037338_visible=='no') {
											  ID_1822037338_visible=false;
										  }
										  $.ID_1822037338.setVisible(ID_1822037338_visible);

$.ID_1860721359.start();
var ID_1109012109_estilo = 'destacado';

						if (typeof WPATH != 'undefined') {
							var _tmp_a4w = require(WPATH('a4w'));
						} else {
							var _tmp_a4w = require('a4w');
						}
						if ((typeof ID_1109012109_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (ID_1109012109_estilo in _tmp_a4w.styles['classes'])) {
							try {
								ID_1109012109_estilo = _tmp_a4w.styles['classes'][ID_1109012109_estilo];
							} catch (st_val_err) {
							}
						}
						$.ID_1109012109.applyProperties(ID_1109012109_estilo);

require(WPATH('vars'))[args.__id]['estado']='1';

if ('__args' in args) {
	args['__args'].onclick({});
} else {
	args.onclick({});
}
}
 else if (estado==2) {
var ID_1109012109_estilo = 'destacado';

						if (typeof WPATH != 'undefined') {
							var _tmp_a4w = require(WPATH('a4w'));
						} else {
							var _tmp_a4w = require('a4w');
						}
						if ((typeof ID_1109012109_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (ID_1109012109_estilo in _tmp_a4w.styles['classes'])) {
							try {
								ID_1109012109_estilo = _tmp_a4w.styles['classes'][ID_1109012109_estilo];
							} catch (st_val_err) {
							}
						}
						$.ID_1109012109.applyProperties(ID_1109012109_estilo);


if ('__args' in args) {
	args['__args'].onclick({});
} else {
	args.onclick({});
}
}
}
/** 
* Funcion que inicializa el widget 
*/

$.init = function(params) {
   for (var tobe in params) args[tobe] = params[tobe];
   if ('__id' in params) require(WPATH('vars'))[params.__id]={};
/** 
* Declaramos e instanciamos la variable &quot;estado&quot; 
*/
require(WPATH('vars'))[args.__id]['estado']='0';
/** 
* Declaramos e instanciamos la variable &quot;caja&quot; 
*/
require(WPATH('vars'))[args.__id]['caja']=55;
if (!_.isUndefined(params.caja)) {
/** 
* Verifica si params.caja existe 
*/
/** 
* modifica el ancho y alto de la vista que contiene la foto con el valor de params.caja 
*/
var ID_1498362955_ancho = params.caja;

								  if (ID_1498362955_ancho=='*') {
									  ID_1498362955_ancho=Ti.UI.FILL;
								  } else if (ID_1498362955_ancho=='-') {
									  ID_1498362955_ancho=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1498362955_ancho)) {
									  ID_1498362955_ancho=ID_1498362955_ancho+'dp';
								  }
								  $.ID_1498362955.setWidth(ID_1498362955_ancho);

var ID_1498362955_alto = params.caja;

								  if (ID_1498362955_alto=='*') {
									  ID_1498362955_alto=Ti.UI.FILL;
								  } else if (ID_1498362955_alto=='-') {
									  ID_1498362955_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1498362955_alto)) {
									  ID_1498362955_alto=ID_1498362955_alto+'dp';
								  }
								  $.ID_1498362955.setHeight(ID_1498362955_alto);

require(WPATH('vars'))[args.__id]['caja']=params.caja;
}
if (!_.isUndefined(params.label)) {
/** 
* Verifica si params.label existe 
*/
/** 
* Modifica el texto del label que actua como titulo y ademas lo muestra si esque estuviera oculto. 
*/
$.ID_1109012109.setText(params.label);

var ID_1109012109_visible = true;

								  if (ID_1109012109_visible=='si') {
									  ID_1109012109_visible=true;
								  } else if (ID_1109012109_visible=='no') {
									  ID_1109012109_visible=false;
								  }
								  $.ID_1109012109.setVisible(ID_1109012109_visible);

}
if (!_.isUndefined(params.opcional)) {
/** 
* Verifica si params.opcional existe 
*/
/** 
* Modifica el texto del label que actua como titulo opcional y ademas lo muestra si esque estuviera oculto.. 
*/
$.ID_965641207.setText(params.opcional);

var ID_965641207_visible = true;

								  if (ID_965641207_visible=='si') {
									  ID_965641207_visible=true;
								  } else if (ID_965641207_visible=='no') {
									  ID_965641207_visible=false;
								  }
								  $.ID_965641207.setVisible(ID_965641207_visible);

}
if (!_.isUndefined(params.id)) {
/** 
* Verifica si params.id existe 
*/
require(WPATH('vars'))[args.__id]['id']=params.id;
}
};

/** 
* Funcion que recibe una imagen, la escala y comprime segun sus parametros y retorna una respuesta al padre de la instancia
* 
* @param imagen Imagen BLOB a ser procesada
* @param nueva Pixeles para definir nuevo ancho-alto de imagen escalada
* @param calidad Si definida, comprime imagen entregada al porcentaje indicado (solo enteros). 
*/

$.procesar = function(params) {
if (!_.isUndefined(params.imagen)) {
/** 
* Verifica si params.imagen existe 
*/
/** 
* Ocultamos wiggle 
*/
var ID_1822037338_visible = false;

										  if (ID_1822037338_visible=='si') {
											  ID_1822037338_visible=true;
										  } else if (ID_1822037338_visible=='no') {
											  ID_1822037338_visible=false;
										  }
										  $.ID_1822037338.setVisible(ID_1822037338_visible);

/** 
* Mostramos anim1 
*/
var ID_602677766_visible = true;

										  if (ID_602677766_visible=='si') {
											  ID_602677766_visible=true;
										  } else if (ID_602677766_visible=='no') {
											  ID_602677766_visible=false;
										  }
										  $.ID_602677766.setVisible(ID_602677766_visible);

require(WPATH('vars'))[args.__id]['conteo_end']=30;
/** 
* Comenzamos la animacion 
*/
$.ID_965642494.start();
var caja = ('caja' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['caja']:'';
 var mini = params.imagen;
/** 
* Escalamos imagen al tama&#241;o requerido 
*/
if (OS_ANDROID) {
   var ID_1808522135_imagefactory = require('ti.imagefactory');
   var mini = ID_1808522135_imagefactory.imageAsResized(params.imagen, { width: caja, height: caja, quality: 0.7 });
} else {
   var mini = params.imagen.imageAsResized(caja, caja);
}
if (!_.isUndefined(params.nueva)) {
/** 
* Verifica si existe params.nueva 
*/
var fotofinal = params.imagen;
/** 
* Escala imagen al tama&#241;o requerido 
*/
if (OS_ANDROID) {
   var ID_431277259_imagefactory = require('ti.imagefactory');
   var fotofinal = ID_431277259_imagefactory.imageAsResized(params.imagen, { width: params.nueva, height: params.nueva, quality: 0.7 });
} else {
   var fotofinal = params.imagen.imageAsResized(params.nueva, params.nueva);
}
require(WPATH('vars'))[args.__id]['conteo_end']='';
/** 
* Finalizamos la animacion 
*/
$.ID_965642494.resume();
if (!_.isUndefined(params.calidad)) {
/** 
* Verifica si existe params.calidad 
*/
var comprimida = fotofinal;
/** 
* Comprime imagen escalada en la calidad indicada 
*/
if (OS_ANDROID) {
  var ID_744309819_imagefactory = require('ti.imagefactory');
  var comprimida = ID_744309819_imagefactory.compress(fotofinal, params.calidad/100);
} else if (OS_IOS) {
  var ID_744309819_imagefactory = require('ti.imagefactory');
  var comprimida = ID_744309819_imagefactory.compress(fotofinal, params.calidad/100);
}
require(WPATH('vars'))[args.__id]['estado']=2;
var ID_602677766_visible = false;

										  if (ID_602677766_visible=='si') {
											  ID_602677766_visible=true;
										  } else if (ID_602677766_visible=='no') {
											  ID_602677766_visible=false;
										  }
										  $.ID_602677766.setVisible(ID_602677766_visible);

/** 
* Ocultamos wiggle 
*/
var ID_1822037338_visible = false;

										  if (ID_1822037338_visible=='si') {
											  ID_1822037338_visible=true;
										  } else if (ID_1822037338_visible=='no') {
											  ID_1822037338_visible=false;
										  }
										  $.ID_1822037338.setVisible(ID_1822037338_visible);

/** 
* mostramos thumbnail 
*/
var ID_1206598763_visible = true;

										  if (ID_1206598763_visible=='si') {
											  ID_1206598763_visible=true;
										  } else if (ID_1206598763_visible=='no') {
											  ID_1206598763_visible=false;
										  }
										  $.ID_1206598763.setVisible(ID_1206598763_visible);

/** 
* cambiamos la imagen de la caja que contiene lafoto por la imagen escalada a mini 
*/
var ID_687034887_imagen = mini;

													if (typeof ID_687034887_imagen == 'string' && 'styles' in require(WPATH('a4w')) && ID_687034887_imagen in require(WPATH('a4w')).styles['images']) {
														ID_687034887_imagen = WPATH(require(WPATH('a4w')).styles['images'][ID_687034887_imagen]);
													}
													$.ID_687034887.setImage(ID_687034887_imagen);


if ('__args' in args) {
	args['__args'].onlisto({escalada : fotofinal,mini : mini,comprimida : comprimida});
} else {
	args.onlisto({escalada : fotofinal,mini : mini,comprimida : comprimida});
}
}
 else {
require(WPATH('vars'))[args.__id]['estado']=2;
var ID_602677766_visible = false;

										  if (ID_602677766_visible=='si') {
											  ID_602677766_visible=true;
										  } else if (ID_602677766_visible=='no') {
											  ID_602677766_visible=false;
										  }
										  $.ID_602677766.setVisible(ID_602677766_visible);

var ID_1822037338_visible = false;

										  if (ID_1822037338_visible=='si') {
											  ID_1822037338_visible=true;
										  } else if (ID_1822037338_visible=='no') {
											  ID_1822037338_visible=false;
										  }
										  $.ID_1822037338.setVisible(ID_1822037338_visible);

var ID_1206598763_visible = true;

										  if (ID_1206598763_visible=='si') {
											  ID_1206598763_visible=true;
										  } else if (ID_1206598763_visible=='no') {
											  ID_1206598763_visible=false;
										  }
										  $.ID_1206598763.setVisible(ID_1206598763_visible);

var ID_687034887_imagen = mini;

													if (typeof ID_687034887_imagen == 'string' && 'styles' in require(WPATH('a4w')) && ID_687034887_imagen in require(WPATH('a4w')).styles['images']) {
														ID_687034887_imagen = WPATH(require(WPATH('a4w')).styles['images'][ID_687034887_imagen]);
													}
													$.ID_687034887.setImage(ID_687034887_imagen);


if ('__args' in args) {
	args['__args'].onlisto({escalada : fotofinal,mini : mini});
} else {
	args.onlisto({escalada : fotofinal,mini : mini});
}
}}
 else {
require(WPATH('vars'))[args.__id]['estado']=2;
var ID_602677766_visible = false;

										  if (ID_602677766_visible=='si') {
											  ID_602677766_visible=true;
										  } else if (ID_602677766_visible=='no') {
											  ID_602677766_visible=false;
										  }
										  $.ID_602677766.setVisible(ID_602677766_visible);

var ID_1822037338_visible = false;

										  if (ID_1822037338_visible=='si') {
											  ID_1822037338_visible=true;
										  } else if (ID_1822037338_visible=='no') {
											  ID_1822037338_visible=false;
										  }
										  $.ID_1822037338.setVisible(ID_1822037338_visible);

var ID_1206598763_visible = true;

										  if (ID_1206598763_visible=='si') {
											  ID_1206598763_visible=true;
										  } else if (ID_1206598763_visible=='no') {
											  ID_1206598763_visible=false;
										  }
										  $.ID_1206598763.setVisible(ID_1206598763_visible);

var ID_687034887_imagen = mini;

													if (typeof ID_687034887_imagen == 'string' && 'styles' in require(WPATH('a4w')) && ID_687034887_imagen in require(WPATH('a4w')).styles['images']) {
														ID_687034887_imagen = WPATH(require(WPATH('a4w')).styles['images'][ID_687034887_imagen]);
													}
													$.ID_687034887.setImage(ID_687034887_imagen);


if ('__args' in args) {
	args['__args'].onlisto({mini : mini});
} else {
	args.onlisto({mini : mini});
}
}}
};

/** 
* Detiene todas las animaciones y muestra la imagen estatica miniatura 
*/

$.detener = function(params) {
var estado = ('estado' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['estado']:'';
if (estado==1||estado=='1') {
/** 
* Verifica si la variable &quot;estado&quot; es igual a 1 
*/
/** 
* Ocultamos wiggle 
*/
var ID_1822037338_visible = false;

										  if (ID_1822037338_visible=='si') {
											  ID_1822037338_visible=true;
										  } else if (ID_1822037338_visible=='no') {
											  ID_1822037338_visible=false;
										  }
										  $.ID_1822037338.setVisible(ID_1822037338_visible);

/** 
* Ocultamos conteo 
*/
var ID_602677766_visible = false;

										  if (ID_602677766_visible=='si') {
											  ID_602677766_visible=true;
										  } else if (ID_602677766_visible=='no') {
											  ID_602677766_visible=false;
										  }
										  $.ID_602677766.setVisible(ID_602677766_visible);

/** 
* Ocultamos minifoto en caso de se estuviera mostrando 
*/
var ID_1206598763_visible = false;

										  if (ID_1206598763_visible=='si') {
											  ID_1206598763_visible=true;
										  } else if (ID_1206598763_visible=='no') {
											  ID_1206598763_visible=false;
										  }
										  $.ID_1206598763.setVisible(ID_1206598763_visible);

/** 
* Mostramos imagen estatica 
*/
var ID_1352627270_visible = true;

										  if (ID_1352627270_visible=='si') {
											  ID_1352627270_visible=true;
										  } else if (ID_1352627270_visible=='no') {
											  ID_1352627270_visible=false;
										  }
										  $.ID_1352627270.setVisible(ID_1352627270_visible);

/** 
* Modifica el estilo del label que actua como titulo del widget 
*/
var ID_1109012109_estilo = 'normal';

						if (typeof WPATH != 'undefined') {
							var _tmp_a4w = require(WPATH('a4w'));
						} else {
							var _tmp_a4w = require('a4w');
						}
						if ((typeof ID_1109012109_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (ID_1109012109_estilo in _tmp_a4w.styles['classes'])) {
							try {
								ID_1109012109_estilo = _tmp_a4w.styles['classes'][ID_1109012109_estilo];
							} catch (st_val_err) {
							}
						}
						$.ID_1109012109.applyProperties(ID_1109012109_estilo);

/** 
* Guarda variable &quot;estado&quot; con valor 0 
*/
require(WPATH('vars'))[args.__id]['estado']='0';
}
 else if (estado==2) {
/** 
* Verifica si la variable &quot;estado&quot; es igual a 2 
*/
/** 
* Modifica estilo del label que actua como titulo del widget 
*/
var ID_1109012109_estilo = 'normal';

						if (typeof WPATH != 'undefined') {
							var _tmp_a4w = require(WPATH('a4w'));
						} else {
							var _tmp_a4w = require('a4w');
						}
						if ((typeof ID_1109012109_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (ID_1109012109_estilo in _tmp_a4w.styles['classes'])) {
							try {
								ID_1109012109_estilo = _tmp_a4w.styles['classes'][ID_1109012109_estilo];
							} catch (st_val_err) {
							}
						}
						$.ID_1109012109.applyProperties(ID_1109012109_estilo);

}};

/** 
* Funcion que asigna imagen mini en vista de control
* 
* @param mini Imagen miniatura a asignar 
*/

$.mini = function(params) {
if (!_.isUndefined(params.mini)) {
/** 
* Verifica si existe params.mini 
*/
/** 
* Ocultamos wiggle 
*/
var ID_1822037338_visible = false;

										  if (ID_1822037338_visible=='si') {
											  ID_1822037338_visible=true;
										  } else if (ID_1822037338_visible=='no') {
											  ID_1822037338_visible=false;
										  }
										  $.ID_1822037338.setVisible(ID_1822037338_visible);

/** 
* Mostramos thumbnail 
*/
var ID_1206598763_visible = true;

										  if (ID_1206598763_visible=='si') {
											  ID_1206598763_visible=true;
										  } else if (ID_1206598763_visible=='no') {
											  ID_1206598763_visible=false;
										  }
										  $.ID_1206598763.setVisible(ID_1206598763_visible);

/** 
* Modificamos imagen de la caja por la que viene en params.mini 
*/
var ID_687034887_imagen = params.mini;

													if (typeof ID_687034887_imagen == 'string' && 'styles' in require(WPATH('a4w')) && ID_687034887_imagen in require(WPATH('a4w')).styles['images']) {
														ID_687034887_imagen = WPATH(require(WPATH('a4w')).styles['images'][ID_687034887_imagen]);
													}
													$.ID_687034887.setImage(ID_687034887_imagen);

}
};