/** 
* Widget 4 Fotos Chicas
* Este control representa una vista con 3-4 cajas que contienen version mini de una foto y las procesa.
* 
* @param caja Indica el ancho y alto de cada mini caja
* @param label1 Indica el texto a mostrar bajo la primera mini caja
* @param label2 Indica el texto a mostrar bajo la segunda mini caja
* @param label3 Indica el texto a mostrar bajo la tercera mini caja
* @param label4 Indica el texto a mostrar bajo la cuerta mini caja
* @param ancho Indica el ancho de este widget, como contenedor de las 4 mini cajas
* @param alto Indica el alto de este widget, como contenedor de las 4 mini cajas 
*/
var _bind4section={};

var args = arguments[0] || {};


$.ID_187601693.init({
caja : 55,
__id : 'ALL187601693',
onlisto : Listo_ID_1021188106,
label : 'Fachada',
onclick : Click_ID_1781808913
}
);

function Click_ID_1781808913(e) {

var evento=e;

$.ID_1901394969.detener({});
$.ID_1364996196.detener({});
$.ID_1437730300.detener({});

if ('__args' in args) {
	args['__args'].onclick({pos : 1});
} else {
	args.onclick({pos : 1});
}

}
function Listo_ID_1021188106(e) {

var evento=e;
if (!_.isUndefined(evento.comprimida)) {

if ('__args' in args) {
	args['__args'].onlisto({pos : 1,mini : evento.mini,nueva : evento.comprimida});
} else {
	args.onlisto({pos : 1,mini : evento.mini,nueva : evento.comprimida});
}
}
 else if (!_.isUndefined(evento.escalada)) {

if ('__args' in args) {
	args['__args'].onlisto({pos : 1,mini : evento.mini,nueva : evento.escalada});
} else {
	args.onlisto({pos : 1,mini : evento.mini,nueva : evento.escalada});
}
} else {

if ('__args' in args) {
	args['__args'].onlisto({pos : 1,mini : evento.mini});
} else {
	args.onlisto({pos : 1,mini : evento.mini});
}
}
}

$.ID_1901394969.init({
caja : 55,
__id : 'ALL1901394969',
onlisto : Listo_ID_490762142,
label : 'Barrio',
onclick : Click_ID_416941178
}
);

function Click_ID_416941178(e) {

var evento=e;

$.ID_187601693.detener({});
$.ID_1364996196.detener({});
$.ID_1437730300.detener({});

if ('__args' in args) {
	args['__args'].onclick({pos : 2});
} else {
	args.onclick({pos : 2});
}

}
function Listo_ID_490762142(e) {

var evento=e;
if (!_.isUndefined(evento.comprimida)) {

if ('__args' in args) {
	args['__args'].onlisto({pos : 2,mini : evento.mini,nueva : evento.comprimida});
} else {
	args.onlisto({pos : 2,mini : evento.mini,nueva : evento.comprimida});
}
}
 else if (!_.isUndefined(evento.escalada)) {

if ('__args' in args) {
	args['__args'].onlisto({pos : 2,mini : evento.mini,nueva : evento.escalada});
} else {
	args.onlisto({pos : 2,mini : evento.mini,nueva : evento.escalada});
}
} else {

if ('__args' in args) {
	args['__args'].onlisto({pos : 2,mini : evento.mini});
} else {
	args.onlisto({pos : 2,mini : evento.mini});
}
}
}

$.ID_1364996196.init({
caja : 55,
__id : 'ALL1364996196',
onlisto : Listo_ID_1893986936,
label : 'Numero',
onclick : Click_ID_1918189988
}
);

function Click_ID_1918189988(e) {

var evento=e;

$.ID_187601693.detener({});
$.ID_1901394969.detener({});
$.ID_1437730300.detener({});

if ('__args' in args) {
	args['__args'].onclick({pos : 3});
} else {
	args.onclick({pos : 3});
}

}
function Listo_ID_1893986936(e) {

var evento=e;
if (!_.isUndefined(evento.comprimida)) {

if ('__args' in args) {
	args['__args'].onlisto({pos : 3,mini : evento.mini,nueva : evento.comprimida});
} else {
	args.onlisto({pos : 3,mini : evento.mini,nueva : evento.comprimida});
}
}
 else if (!_.isUndefined(evento.escalada)) {

if ('__args' in args) {
	args['__args'].onlisto({pos : 3,mini : evento.mini,nueva : evento.escalada});
} else {
	args.onlisto({pos : 3,mini : evento.mini,nueva : evento.escalada});
}
} else {

if ('__args' in args) {
	args['__args'].onlisto({pos : 3,mini : evento.mini});
} else {
	args.onlisto({pos : 3,mini : evento.mini});
}
}
}

$.ID_1437730300.init({
caja : 55,
__id : 'ALL1437730300',
onlisto : Listo_ID_1977539798,
opcional : 'opcional',
label : 'Nº Depto',
onclick : Click_ID_357755274
}
);

function Click_ID_357755274(e) {

var evento=e;

$.ID_187601693.detener({});
$.ID_1901394969.detener({});
$.ID_1364996196.detener({});

if ('__args' in args) {
	args['__args'].onclick({pos : 4});
} else {
	args.onclick({pos : 4});
}

}
function Listo_ID_1977539798(e) {

var evento=e;
if (!_.isUndefined(evento.comprimida)) {

if ('__args' in args) {
	args['__args'].onlisto({pos : 4,mini : evento.mini,nueva : evento.comprimida});
} else {
	args.onlisto({pos : 4,mini : evento.mini,nueva : evento.comprimida});
}
}
 else if (!_.isUndefined(evento.escalada)) {

if ('__args' in args) {
	args['__args'].onlisto({pos : 4,mini : evento.mini,nueva : evento.escalada});
} else {
	args.onlisto({pos : 4,mini : evento.mini,nueva : evento.escalada});
}
} else {

if ('__args' in args) {
	args['__args'].onlisto({pos : 4,mini : evento.mini});
} else {
	args.onlisto({pos : 4,mini : evento.mini});
}
}
}
/** 
* Funcion que inicializa el widget 
*/

$.init = function(params) {
   for (var tobe in params) args[tobe] = params[tobe];
   if ('__id' in params) require(WPATH('vars'))[params.__id]={};
if (!_.isUndefined(params.caja)) {
/** 
* Verifica si existe params.caja 
*/
/** 
* llama al evento init del widget foto chica y le pasa por parametro params.caja 
*/

$.ID_187601693.init({caja : params.caja});
$.ID_1901394969.init({caja : params.caja});
$.ID_1364996196.init({caja : params.caja});
$.ID_1437730300.init({caja : params.caja});
}
if (!_.isUndefined(params.label1)) {
/** 
* Verifica si exsite params.label1 
*/
/** 
* llama al evento init del widget foto chica y le pasa por parametro params.label1 
*/

$.ID_187601693.init({label : params.label1});
}
if (!_.isUndefined(params.label2)) {
/** 
* Verifica si existe params.label2 
*/
/** 
* llama al evento init del widget foto chica y le pasa por parametro params.label2 
*/

$.ID_1901394969.init({label : params.label2});
}
if (!_.isUndefined(params.label3)) {
/** 
* Verifica si existe params.label3 
*/
/** 
* llama al evento init del widget foto chica y le pasa por parametro params.label3 
*/

$.ID_1364996196.init({label : params.label3});
}
if (!_.isUndefined(params.label4)) {
/** 
* Verifica si existe params.label4 
*/
/** 
* llama al evento init del widget foto chica y le pasa por parametro params.label4 
*/

$.ID_1437730300.init({label : params.label4});
}
if (!_.isUndefined(params.ancho)) {
/** 
* Verifica si existe params.ancho 
*/
/** 
* Modifica el ancho de la vista principal del widget con el valor de params.ancho 
*/
var ID_265479629_ancho = params.ancho;

								  if (ID_265479629_ancho=='*') {
									  ID_265479629_ancho=Ti.UI.FILL;
								  } else if (ID_265479629_ancho=='-') {
									  ID_265479629_ancho=Ti.UI.SIZE;
								  } else if (!isNaN(ID_265479629_ancho)) {
									  ID_265479629_ancho=ID_265479629_ancho+'dp';
								  }
								  $.ID_265479629.setWidth(ID_265479629_ancho);

}
if (!_.isUndefined(params.alto)) {
/** 
* Verifica si existe params.alto 
*/
/** 
* Modifica el alto de la la vista principal del widget con el valor de params.alto 
*/
var ID_265479629_alto = params.alto;

								  if (ID_265479629_alto=='*') {
									  ID_265479629_alto=Ti.UI.FILL;
								  } else if (ID_265479629_alto=='-') {
									  ID_265479629_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_265479629_alto)) {
									  ID_265479629_alto=ID_265479629_alto+'dp';
								  }
								  $.ID_265479629.setHeight(ID_265479629_alto);

}
};

/** 
* Funcion que recibe la imagen a ser procesada segun la posicion definida en la llamada click del widget fotochica
* 
* @param imagen1 Imagen para ser procesada en la primera caja (instancia 1 de widget fotochica)
* @param imagen2 Imagen para ser procesada en la segunda caja (instancia 2 de widget fotochica)
* @param imagen3 Imagen para ser procesada en la tercera caja (instancia 3 de widget fotochica)
* @param imagen4 Imagen para ser procesada en la cuarta caja (instancia 4 de widget fotochica)
* @param camara Traspasa valor a widgets fotochica en las 4 cajas
* @param nueva Indica resolucion de nueva imagen capturada para las 4 cajas 
*/

$.procesar = function(params) {
if (!_.isUndefined(params.imagen1)) {
/** 
* Verifica si existe params.imagen1 
*/
if (!_.isUndefined(params.nueva)) {
/** 
* Llama al evento procesar del widget foto chica y le pasa la imagen por parametro 
*/

$.ID_187601693.procesar({imagen : params.imagen1,nueva : params.nueva,calidad : params.calidad});
}
 else {

$.ID_187601693.procesar({imagen : params.imagen1});
}}
if (!_.isUndefined(params.imagen2)) {
/** 
* Verifica si existe params.imagen2 
*/
if (!_.isUndefined(params.nueva)) {
/** 
* Llama al evento procesar del widget foto chica y le pasa la imagen por parametro 
*/

$.ID_1901394969.procesar({imagen : params.imagen2,nueva : params.nueva,calidad : params.calidad});
}
 else {

$.ID_1901394969.procesar({imagen : params.imagen2});
}}
if (!_.isUndefined(params.imagen3)) {
/** 
* Verifica si existe params.imagen3 
*/
if (!_.isUndefined(params.nueva)) {
/** 
* Llama al evento procesar del widget foto chica y le pasa la imagen por parametro 
*/

$.ID_1364996196.procesar({imagen : params.imagen3,nueva : params.nueva,calidad : params.calidad});
}
 else {

$.ID_1364996196.procesar({imagen : params.imagen3});
}}
if (!_.isUndefined(params.imagen4)) {
/** 
* Verifica si existe params.imagen4 
*/
if (!_.isUndefined(params.nueva)) {
/** 
* Llama al evento procesar del widget foto chica y le pasa la imagen por parametro 
*/

$.ID_1437730300.procesar({imagen : params.imagen4,nueva : params.nueva,calidad : params.calidad});
}
 else {

$.ID_1437730300.procesar({imagen : params.imagen4});
}}
};