/** 
* Widget Boton
* Boton amarillo utilizado para tomar una tarea
* 
* @param texto Texto a definir en boton
* @param verprogreso Si true, muestra animacion de progreso
* @param vertexto Si true, muestra texto en boton 
*/
var _bind4section={};

var args = arguments[0] || {};

function Click_ID_556842754(e) {

e.cancelBubble=true;
var elemento=e.source;
var estado = ('estado' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['estado']:'';

if ('__args' in args) {
	args['__args'].onpresiono({estado : estado});
} else {
	args.onpresiono({estado : estado});
}

}
/** 
* Funcion que inicializa widget 
*/

$.init = function(params) {
   for (var tobe in params) args[tobe] = params[tobe];
   if ('__id' in params) require(WPATH('vars'))[params.__id]={};
if (!_.isUndefined(params.texto)) {
$.ID_690999687.setText(params.texto);

}
if (!_.isUndefined(params.verprogreso)) {
if (params.verprogreso==true||params.verprogreso=='true') {
$.ID_544688334.show();
}
 else if (params.verprogreso==false||params.verprogreso=='false') {
$.ID_544688334.hide();
}}
if (!_.isUndefined(params.vertexto)) {
var ID_690999687_visible = params.vertexto;

								  if (ID_690999687_visible=='si') {
									  ID_690999687_visible=true;
								  } else if (ID_690999687_visible=='no') {
									  ID_690999687_visible=false;
								  }
								  $.ID_690999687.setVisible(ID_690999687_visible);

}
if (!_.isUndefined(params.estilo)) {
var ID_1955206092_estilo = params.estilo;

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_1955206092.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_1955206092_estilo);

}
require(WPATH('vars'))[args.__id]['estado']='0';
};

/** 
* Funcion que actualiza boton
* 
* @param texto Modifica el texto del boton
* @param verprogreso Si true, muestra animacion de progreso
* @param vertexto Si true, muestra texto en boton 
*/

$.datos = function(params) {
if (!_.isUndefined(params.texto)) {
$.ID_690999687.setText(params.texto);

}
if (!_.isUndefined(params.verprogreso)) {
if (params.verprogreso==true||params.verprogreso=='true') {
$.ID_544688334.show();
}
 else if (params.verprogreso==false||params.verprogreso=='false') {
$.ID_544688334.hide();
}}
if (!_.isUndefined(params.vertexto)) {
var ID_690999687_visible = params.vertexto;

								  if (ID_690999687_visible=='si') {
									  ID_690999687_visible=true;
								  } else if (ID_690999687_visible=='no') {
									  ID_690999687_visible=false;
								  }
								  $.ID_690999687.setVisible(ID_690999687_visible);

}
if (!_.isUndefined(params.estilo)) {
var ID_1955206092_estilo = params.estilo;

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_1955206092.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_1955206092_estilo);

}
require(WPATH('vars'))[args.__id]['estado']=params.estado;
};