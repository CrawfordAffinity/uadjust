exports.events = _.clone(Backbone.Events);
exports.ajaxkeys = {};
exports.variables = {};
exports.styles = {"images":{},"classes":{"bt_aceptar":{"color":"#2d9edb","font":{"fontWeight":"bold","fontSize":"15dp"}},"bt_cancelar":{"color":"#a0a1a3","font":{"fontFamily":"SFUIText-Light","fontSize":"15dp"}}}};
exports.fontello = {};
