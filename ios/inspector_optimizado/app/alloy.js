Alloy.Events = _.clone(Backbone.Events);
Alloy.Collections.marcas = Alloy.createCollection('marcas');
Alloy.Collections.entidad_financiera = Alloy.createCollection('entidad_financiera');
Alloy.Collections.emergencia_ubicacion = Alloy.createCollection('emergencia_ubicacion');
Alloy.Collections.estructura_cubierta = Alloy.createCollection('estructura_cubierta');
Alloy.Collections.pavimento = Alloy.createCollection('pavimento');
Alloy.Collections.compania = Alloy.createCollection('compania');
Alloy.Collections.insp_itemdanos = Alloy.createCollection('insp_itemdanos');
Alloy.Collections.numero_unico = Alloy.createCollection('numero_unico');
Alloy.Collections.insp_firma = Alloy.createCollection('insp_firma');
Alloy.Collections.muros_tabiques = Alloy.createCollection('muros_tabiques');
Alloy.Collections.insp_niveles = Alloy.createCollection('insp_niveles');
Alloy.Collections.insp_contenido = Alloy.createCollection('insp_contenido');
Alloy.Collections.inspectores = Alloy.createCollection('inspectores');
Alloy.Collections.emergencia_perfil = Alloy.createCollection('emergencia_perfil');
Alloy.Collections.pais = Alloy.createCollection('pais');
Alloy.Collections.tipo_accion = Alloy.createCollection('tipo_accion');
Alloy.Collections.estructura_soportante = Alloy.createCollection('estructura_soportante');
Alloy.Collections.entrepisos = Alloy.createCollection('entrepisos');
Alloy.Collections.insp_recintos = Alloy.createCollection('insp_recintos');
Alloy.Collections.insp_datosbasicos = Alloy.createCollection('insp_datosbasicos');
Alloy.Collections.tipo_partida = Alloy.createCollection('tipo_partida');
Alloy.Collections.unidad_medida = Alloy.createCollection('unidad_medida');
Alloy.Collections.tipo_dano = Alloy.createCollection('tipo_dano');
Alloy.Collections.emergencia = Alloy.createCollection('emergencia');
Alloy.Collections.asegurado = Alloy.createCollection('asegurado');
Alloy.Collections.inspecciones = Alloy.createCollection('inspecciones');
Alloy.Collections.monedas = Alloy.createCollection('monedas');
Alloy.Collections.insp_documentos = Alloy.createCollection('insp_documentos');
Alloy.Collections.insp_siniestro = Alloy.createCollection('insp_siniestro');
Alloy.Collections.cubierta = Alloy.createCollection('cubierta');
Alloy.Collections.historial_tareas = Alloy.createCollection('historial_tareas');
Alloy.Collections.tipo_siniestro = Alloy.createCollection('tipo_siniestro');
Alloy.Collections.nivel1 = Alloy.createCollection('nivel1');
Alloy.Collections.insp_fotosrequeridas = Alloy.createCollection('insp_fotosrequeridas');
Alloy.Collections.tareas = Alloy.createCollection('tareas');
Alloy.Collections.experiencia_oficio = Alloy.createCollection('experiencia_oficio');
Alloy.Collections.insp_caracteristicas = Alloy.createCollection('insp_caracteristicas');
Alloy.Collections.destino = Alloy.createCollection('destino');
Alloy.Collections.bienes = Alloy.createCollection('bienes');
Alloy.Collections.tareas_entrantes = Alloy.createCollection('tareas_entrantes');
Alloy.Collections.cola = Alloy.createCollection('cola');
Alloy.Globals.mod_ti_map = require('ti.map');
//contenido de nodo global..
(function() {
    var _my_events = {},
        _out_vars = {},
        _var_scopekey = 'ID_1771290876';
    require('vars')[_var_scopekey] = {};
    require('vars')['url_server'] = L('x2575237648', 'http://api.uadjust.com:9999/api/');
    /** 
     * Lo siguiente actualiza solicita al sistema operativo que indique su posicion mas precisa con mejor consumo de bateria 
     */
    require('vars')['gps_error'] = L('x4261170317', 'true');

    var ID_1258954520 = function(evento) {
        if (evento.error == false || evento.error == 'false') {
            require('vars')['gps_error'] = L('x734881840_traducir', 'false');
            require('vars')['gps_latitud'] = evento.latitude;
            require('vars')['gps_longitud'] = evento.longitude;
            require('vars')['gps_coords'] = evento.coords;
            var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
            var inspeccion_encurso = ('inspeccion_encurso' in require('vars')) ? require('vars')['inspeccion_encurso'] : '';
            if (inspeccion_encurso == false || inspeccion_encurso == 'false') {
                if (_.isObject(inspector)) {
                    var seguir_tarea = ('seguir_tarea' in require('vars')) ? require('vars')['seguir_tarea'] : '';
                    var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
                    if (_.isNumber(seguir_tarea)) {
                        var ID_82591610 = {};

                        ID_82591610.success = function(e) {
                            var elemento = e,
                                valor = e;
                            if (false) console.log('respuesta exitosa de guardarUbicacion', {
                                "datos": elemento
                            });
                            elemento = null, valor = null;
                        };

                        ID_82591610.error = function(e) {
                            var elemento = e,
                                valor = e;
                            if (false) console.log('respuesta fallida de guardarUbicacion', {
                                "datos": elemento
                            });
                            elemento = null, valor = null;
                        };
                        require('helper').ajaxUnico('ID_82591610', '' + String.format(L('x4011689324', '%1$sguardarUbicacion'), url_server.toString()) + '', 'POST', {
                            id_inspector: inspector.id_server,
                            id_tarea: seguir_tarea,
                            lat: evento.latitude,
                            lon: evento.longitude
                        }, 15000, ID_82591610);
                    }
                    var ID_398815612 = {};

                    ID_398815612.success = function(e) {
                        var elemento = e,
                            valor = e;
                        if (elemento.error == 0 || elemento.error == '0') {
                            var ID_1787502895_i = Alloy.Collections.tareas;
                            var sql = "DELETE FROM " + ID_1787502895_i.config.adapter.collection_name;
                            var db = Ti.Database.open(ID_1787502895_i.config.adapter.db_name);
                            db.execute(sql);
                            db.close();
                            ID_1787502895_i.trigger('remove');
                            var elemento_mistareas = elemento.mistareas;
                            var ID_643651375_m = Alloy.Collections.tareas;
                            var db_ID_643651375 = Ti.Database.open(ID_643651375_m.config.adapter.db_name);
                            db_ID_643651375.execute('BEGIN');
                            _.each(elemento_mistareas, function(ID_643651375_fila, pos) {
                                db_ID_643651375.execute('INSERT INTO tareas (fecha_tarea, id_inspeccion, id_asegurado, nivel_2, comentario_can_o_rech, asegurado_tel_fijo, estado_tarea, bono, evento, id_inspector, asegurado_codigo_identificador, lat, nivel_1, asegurado_nombre, pais, direccion, asegurador, fecha_ingreso, fecha_siniestro, nivel_1_texto, distance, nivel_4, perfil, asegurado_id, pais_texto, id_server, categoria, nivel_3, asegurado_correo, num_caso, lon, asegurado_tel_movil, nivel_5, tipo_tarea) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', ID_643651375_fila.fecha_tarea, ID_643651375_fila.id_inspeccion, ID_643651375_fila.id_asegurado, ID_643651375_fila.nivel_2, ID_643651375_fila.comentario_can_o_rech, ID_643651375_fila.asegurado_tel_fijo, ID_643651375_fila.estado_tarea, ID_643651375_fila.bono, ID_643651375_fila.evento, ID_643651375_fila.id_inspector, ID_643651375_fila.asegurado_codigo_identificador, ID_643651375_fila.lat, ID_643651375_fila.nivel_1, ID_643651375_fila.asegurado_nombre, ID_643651375_fila.pais, ID_643651375_fila.direccion, ID_643651375_fila.asegurador, ID_643651375_fila.fecha_ingreso, ID_643651375_fila.fecha_siniestro, ID_643651375_fila.nivel_1_, ID_643651375_fila.distancia, ID_643651375_fila.nivel_4, 'ubicacion', ID_643651375_fila.asegurado_id, ID_643651375_fila.pais, ID_643651375_fila.id, ID_643651375_fila.categoria, ID_643651375_fila.nivel_3, ID_643651375_fila.asegurado_correo, ID_643651375_fila.num_caso, ID_643651375_fila.lon, ID_643651375_fila.asegurado_tel_movil, ID_643651375_fila.nivel_5, ID_643651375_fila.tipo_tarea);
                            });
                            db_ID_643651375.execute('COMMIT');
                            db_ID_643651375.close();
                            db_ID_643651375 = null;
                            ID_643651375_m.trigger('change');
                            if (false) console.log('psb: refrescando mistareas', {});
                            var ID_1089888790_func = function() {

                                Alloy.Events.trigger('_refrescar_tareas_mistareas');

                                Alloy.Events.trigger('_calcular_ruta');
                            };
                            var ID_1089888790 = setTimeout(ID_1089888790_func, 1000 * 0.1);
                        }
                        elemento = null, valor = null;
                    };

                    ID_398815612.error = function(e) {
                        var elemento = e,
                            valor = e;
                        if (false) console.log('respuesta fallida de obtenerMisTareas', {
                            "datos": elemento
                        });
                        elemento = null, valor = null;
                    };
                    require('helper').ajaxUnico('ID_398815612', '' + String.format(L('x2334609226', '%1$sobtenerMisTareas'), url_server.toString()) + '', 'POST', {
                        id_inspector: inspector.id_server,
                        lat: evento.latitude,
                        lon: evento.longitude
                    }, 15000, ID_398815612);
                }
            }
        } else {
            if (false) console.log('psb ha ocurrido error localizando por equipo a usuario', {
                "evento": evento
            });
            require('vars')['gps_error'] = L('x4261170317', 'true');
        }
    };
    var ID_1258954520_locupd = function() {
        Titanium.Geolocation.purpose = 'Requerido para aplicacion.';
        Titanium.Geolocation.accuracy = Ti.Geolocation.ACCURACY_HUNDRED_METERS;
        Titanium.Geolocation.distanceFilter = 100;
        Titanium.Geolocation.preferredProvider = Ti.Geolocation.PROVIDER_GPS;
    };
    ID_1258954520_moment = require('alloy/moment');
    if (OS_ANDROID) {
        var providerGps = Ti.Geolocation.Android.createLocationProvider({
            name: Ti.Geolocation.PROVIDER_GPS,
            minUpdateDistance: 100.0,
            minUpdateTime: 0
        });
        Ti.Geolocation.Android.addLocationProvider(providerGps);
        Ti.Geolocation.Android.manualMode = true;
        Titanium.Geolocation.addEventListener('location', function(ee) {
            if (ee && ee.coords) {
                ID_1258954520({
                    error: false,
                    error_data: {},
                    latitude: ('latitude' in ee.coords) ? ee.coords.latitude : -1,
                    longitude: ('longitude' in ee.coords) ? ee.coords.longitude : -1,
                    coords: ee.coords,
                    date: ID_1258954520_moment(ee.coords.timestamp).format()
                });
            }
        });
    } else if (OS_IOS) {
        if (Ti.Geolocation.hasLocationPermissions(Ti.Geolocation.AUTHORIZATION_WHEN_IN_USE)) {
            if (Ti.Geolocation.locationServicesEnabled) {
                ID_1258954520_locupd();
                Titanium.Geolocation.addEventListener('location', function(ee) {
                    if (ee && ee.coords) {
                        ID_1258954520({
                            error: false,
                            error_data: {},
                            latitude: ('latitude' in ee.coords) ? ee.coords.latitude : -1,
                            longitude: ('longitude' in ee.coords) ? ee.coords.longitude : -1,
                            coords: ee.coords,
                            date: ID_1258954520_moment(ee.coords.timestamp).format()
                        });
                    }
                });
            }
        } else {
            Ti.Geolocation.requestLocationPermissions(Ti.Geolocation.AUTHORIZATION_WHEN_IN_USE, function(u) {
                if (u.success) {
                    ID_1258954520_locupd();
                    Titanium.Geolocation.addEventListener('location', function(ee) {
                        if (ee && ee.coords) {
                            ID_1258954520({
                                error: false,
                                error_data: {},
                                latitude: ('latitude' in ee.coords) ? ee.coords.latitude : -1,
                                longitude: ('longitude' in ee.coords) ? ee.coords.longitude : -1,
                                coords: ee.coords,
                                date: ID_1258954520_moment(ee.coords.timestamp).format()
                            });
                        }
                    });
                } else {
                    ID_1258954520({
                        error: true,
                        error_data: u,
                        latitude: -1,
                        longitude: -1,
                        coords: {},
                        date: ID_1258954520_moment().format()
                    });
                }
            });
        }
    }
    var ID_781143015 = Ti.UI.createView({});
    var ID_781143015_cloud = require('ti.cloud');
    var ID_781143015_register = function(_meta) {
        var elemento = _meta.value;
        if (false) console.log('registrado', {
            "datos": elemento
        });
        require('vars')['devicetoken'] = elemento;
        Ti.App.Properties.setString('devicetoken', JSON.stringify(elemento));
        elemento = null;
    };
    var ID_781143015_message = function(_meta) {
        var elemento = _meta.value;
        if (false) console.log('llego elemento', {
            "datos": elemento
        });
        /** 
         * Se usa para mantener similitud con el código de android 
         */
        mensaje = elemento.data;
        if (false) console.log('llego mensaje', {
            "datos": mensaje
        });
        var ID_1574481652_opts = [L('x1518866076_traducir', 'Aceptar')];
        var ID_1574481652 = Ti.UI.createAlertDialog({
            title: L('x1789641236_traducir', 'Notificacion'),
            message: '' + elemento.data.aps.alert + '',
            buttonNames: ID_1574481652_opts
        });
        ID_1574481652.addEventListener('click', function(e) {
            var cosa = ID_1574481652_opts[e.index];
            cosa = null;

            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1574481652.show();
        if (mensaje.confirmacion == true || mensaje.confirmacion == 'true') {
            Ti.App.Properties.setString('confirmarpush', JSON.stringify(true));
            var confirmarpush = JSON.parse(Ti.App.Properties.getString('confirmarpush'));
            if (false) console.log('mirame we', {
                "asd": confirmarpush
            });
            var fechaactual = new Date();
            var fecha_hoy = null;
            if ('formatear_fecha' in require('funciones')) {
                fecha_hoy = require('funciones').formatear_fecha({
                    'fecha': fechaactual,
                    'formato': L('x591439515_traducir', 'YYYY-MM-DD')
                });
            } else {
                try {
                    fecha_hoy = f_formatear_fecha({
                        'fecha': fechaactual,
                        'formato': L('x591439515_traducir', 'YYYY-MM-DD')
                    });
                } catch (ee) {}
            }
            if (false) console.log('pasando por confirmacion true', {
                "asd": fecha_hoy
            });
            var ID_672833222_i = Alloy.Collections.consultarpush;
            var sql = "DELETE FROM " + ID_672833222_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_672833222_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            ID_672833222_i.trigger('remove');
            if (false) console.log('eliminando modelo', {});
            var ID_1644726123_m = Alloy.Collections.consultarpush;
            var ID_1644726123_fila = Alloy.createModel('consultarpush', {
                fecha: fecha_hoy
            });
            ID_1644726123_m.add(ID_1644726123_fila);
            ID_1644726123_fila.save();
            var ID_1931461855_i = Alloy.createCollection('consultarpush');
            var ID_1931461855_i_where = '';
            ID_1931461855_i.fetch();
            var consulta = require('helper').query2array(ID_1931461855_i);
            if (false) console.log('consultando modelo', {
                "confecha": consulta
            });
        }
        Alloy.Events.trigger('_refrescar_tareas_hoy');

        Alloy.Events.trigger('_calcular_ruta');
        elemento = null;
    };
    var ID_781143015_error = function(_meta) {
        var elemento = _meta.value;
        if (false) console.log('error en aceptarPush', {
            "datos": elemento
        });
        elemento = null;
    };
    if (OS_ANDROID) {
        var ID_781143015_cloudpush = require('ti.cloudpush');
        ID_781143015_cloudpush.retrieveDeviceToken({
            success: function(e) {
                ID_781143015_cloud.PushNotifications.subscribeToken({
                    device_token: e.deviceToken,
                    channel: 'generico',
                    type: 'android'
                }, function(ee) {
                    if (ee.success) {
                        if (typeof ID_781143015_register != 'undefined') ID_781143015_register({
                            value: e.deviceToken
                        });
                    } else {
                        if (typeof ID_781143015_error != 'undefined') ID_781143015_error({
                            value: ee.message,
                            code: ee.error
                        });
                    }
                });
            },
            error: function(e) {
                if (typeof ID_781143015_error != 'undefined') ID_781143015_error({
                    value: e.error,
                    code: 'token'
                });
            },
        });
        ID_781143015_cloudpush.addEventListener('callback', function(evt) {
            if (typeof ID_781143015_message != 'undefined') ID_781143015_message({
                value: evt.payload
            });
        });
        ID_781143015_cloudpush.addEventListener('trayClick', function(evt) {
            if (typeof ID_781143015_message != 'undefined') ID_781143015_message({
                value: evt.payload
            });
        });
    } else if (OS_IOS) {
        if (Ti.Platform.name == 'iPhone OS' && parseInt(Ti.Platform.version.split('.')[0]) >= 8) {
            Ti.App.iOS.addEventListener('usernotificationsettings', function registerForPush() {
                Ti.App.iOS.removeEventListener('usernotificationsettings', registerForPush);
                Ti.Network.registerForPushNotifications({
                    success: function(e) {
                        ID_781143015_cloud.PushNotifications.subscribeToken({
                            device_token: e.deviceToken,
                            channel: 'generico',
                            type: 'ios'
                        }, function(ee) {
                            if (ee.success) {
                                if (typeof ID_781143015_register != 'undefined') ID_781143015_register({
                                    value: e.deviceToken
                                });
                            } else {
                                if (typeof ID_781143015_error != 'undefined') ID_781143015_error({
                                    value: ee.message,
                                    code: ee.error
                                });
                            }
                        });
                    },
                    error: function(e) {
                        if (typeof ID_781143015_error != 'undefined') ID_781143015_error({
                            value: e.error
                        });
                    },
                    callback: function(e) {
                        if (typeof ID_781143015_message != 'undefined') ID_781143015_message({
                            value: e
                        });
                    }
                });
            });
            Ti.App.iOS.registerUserNotificationSettings({
                types: [Ti.App.iOS.USER_NOTIFICATION_TYPE_ALERT, Ti.App.iOS.USER_NOTIFICATION_TYPE_SOUND, Ti.App.iOS.USER_NOTIFICATION_TYPE_BADGE]
            });
        } else {
            Ti.Network.registerForPushNotifications({
                types: [Ti.App.iOS.USER_NOTIFICATION_TYPE_ALERT, Ti.App.iOS.USER_NOTIFICATION_TYPE_SOUND, Ti.App.iOS.USER_NOTIFICATION_TYPE_BADGE],
                success: function(e) {
                    ID_781143015_cloud.PushNotifications.subscribeToken({
                        device_token: e.deviceToken,
                        channel: 'generico',
                        type: 'ios'
                    }, function(ee) {
                        if (ee.success) {
                            if (typeof ID_781143015_register != 'undefined') ID_781143015_register({
                                value: e.deviceToken
                            });
                        } else {
                            if (typeof ID_781143015_error != 'undefined') ID_781143015_error({
                                value: ee.message,
                                code: ee.error
                            });
                        }
                    });
                },
                error: function(e) {
                    if (typeof ID_781143015_error != 'undefined') ID_781143015_error({
                        value: e.error
                    });
                },
                callback: function(e) {
                    if (typeof ID_781143015_message != 'undefined') ID_781143015_message({
                        value: e
                    });
                }
            });
        }
    }
    /** 
     * Ciclo de notificaciones 
     */
    var hola = 0;
    var ID_1182510359_continuar = true;
    _out_vars['ID_1182510359'] = {
        _remove: ["clearTimeout(_out_vars['ID_1182510359']._run)"] 
    };
    var ID_1182510359_func = function() {
        hola = hola + 1;
        if (OS_IOS) {
            console.log('DEBUG: memoria disponible en IOS: ' + Ti.Platform.availableMemory * 1024 * 1024 + ' bytes');
        } else {
            console.log('DEBUG: memoria disponible en Android: ' + Ti.Platform.availableMemory + ' bytes');
        }
        if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
            var ID_209803946 = null;
            if ('consumir_cola' in require('funciones')) {
                ID_209803946 = require('funciones').consumir_cola({});
            } else {
                try {
                    ID_209803946 = f_consumir_cola({});
                } catch (ee) {}
            }
        }
        if (ID_1182510359_continuar == true) {
            _out_vars['ID_1182510359']._run = setTimeout(ID_1182510359_func, 1000 * 30);
        }
    };
    _out_vars['ID_1182510359']._run = setTimeout(ID_1182510359_func, 1000 * 30);
})();