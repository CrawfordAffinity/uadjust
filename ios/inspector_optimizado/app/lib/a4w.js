exports.events = _.clone(Backbone.Events);
exports.ajaxkeys = {};
exports.variables = {};
exports.styles = {
    "images": {
        "id_164841395492": "/images/i7F73828276786D80F13715BB5BFB4CCC.png",
        "id_164841395410": "/images/iE9355204576B1FD1BB3F1F0E435BFF6D.png",
        "id_164841395471": "/images/iDFEBE4EC019BCE94F6EC5853A3BFABF5.png",
        "id_164841395442": "/images/iE50DF32C64581FCE5856C9454B2D58E2.png",
        "tareas_on": "/images/iF50519A47DC67364BC4E523D4F9D7B6C.png",
        "id_499822526": "/images/i4ECB85A2AE41ACB15653139F35BFA8C7.png",
        "id_164841395419": "/images/i988F7B1016AF1A4927CD7ED9AE8AB990.png",
        "id_164841395430": "/images/i9AC6E8C97F63FEB0A46CCD655AC44431.png",
        "id_17304885114": "/images/i0E5D79CD99D2BBA09F0BEAA30A79A3D9.png",
        "id_17304885111": "/images/iE67E9F2E19A329505444E7465B5FD7D0.png",
        "id_17304885144": "/images/i87DE3A07ABFD59B0F96861DEC12DDF8F.png",
        "id_17304885149": "/images/i47F6C47BCA8C1146589A6899C5016EA7.png",
        "id_164841395454": "/images/i20769EDB2A387AD7E63BFEE1ADB6E01A.png",
        "id_17304885150": "/images/iF37AFC4AE87333385D84EF93517E2F25.png",
        "hoy_on": "/images/iAEC3B644E80683643754D43C54627375.png",
        "pincho10": "/images/i71C6F623091942D94C46CC378343ABF6.png",
        "id_1648413954110": "/images/iDD2C93752E1169F8D71892A477C85A11.png",
        "id_17304885140": "/images/iE1BAB1BE32595937A03D896936E17846.png",
        "id_17304885122": "/images/iF6C82CEBB73C3F50FC5AC08E6FA50DF2.png",
        "id_1648413954118": "/images/iE02AFE3817F8B969AEAACF7456E25AB5.png",
        "id_1648413954120": "/images/i870DC5E31CA825DAEE1AD00C6FB1D68A.png",
        "id_17304885138": "/images/iF746C788F42D49A6AD715DC319BFF2B7.png",
        "id_1887826074": "/images/iB1F901AEFA79EE1F3A05C41650B368A4.png",
        "id_1648413954115": "/images/i6AF96FB60F20F611E16F44E4A4C366B4.png",
        "id_1648413954100": "/images/iF086F6FE7A1868188FD09EE6FA03485B.png",
        "id_164841395411": "/images/i0E4650BFDDB114F81A51C600521AC2D7.png",
        "id_1730488511": "/images/iBC783890F4B9774991803EB6ECA81549.png",
        "id_164841395433": "/images/i388C5A5B6FEC1AAA9E0FED2BE6842A35.png",
        "id_164841395441": "/images/i10BF85AEDE9F2E6087E8EECF917665A8.png",
        "id_1648413954102": "/images/i47EFAC4DEC72BFED1EAD8D5621ECF913.png",
        "sin_imagen": "/images/i7C39FFD7014473CB54CF601D7555AA02.png",
        "id_164841395489": "/images/iECBF5EDE5417D57D71BD067290909C22.png",
        "id_17304885161": "/images/iD07A2DFA41B1AF2BAD9EC1879066DBE8.png",
        "id_1648413954113": "/images/i7DF846739C1FD71098DE3902990DA8A9.png",
        "id_1648413954104": "/images/i6394513CA228CCB57ED602297C9422BC.png",
        "id_164841395472": "/images/iAC27C6E0D5FAF36E89B0E75BD07AEB87.png",
        "id_17304885152": "/images/i49BA5221BED2176E506AA26FABE14E75.png",
        "id_17304885141": "/images/i09B77564ECA69742FF6926701ADCC788.png",
        "id_17304885169": "/images/i7C907FDBDA8C040F234553C55D821A45.png",
        "id_16484139547": "/images/i77B96376A21E9E530BF99DEF0E191F24.png",
        "id_164841395414": "/images/i19F89DC20905F566DA7CCDBE900B1828.png",
        "id_17304885159": "/images/i771C97B5058ACE91E7A66AD6D47C1934.png",
        "id_17304885116": "/images/iD464C126D5638562F447AE472C2BD133.png",
        "id_17304885127": "/images/i574DB1641BB1F722625EB07223F436E0.png",
        "id_16484139542": "/images/iEB558FE382408A66B62F79BCD08E87DE.png",
        "id_17304885162": "/images/iB32E3B863CF1AC37F2816A7BCD60A554.png",
        "id_17304885156": "/images/i98CA10AE81168639ED3D051D83F9FF4A.png",
        "id_17304885148": "/images/i77F111A1F32961B1F386B1DC7377767D.png",
        "id_164841395438": "/images/i54701C93E11CA7BAF12DF89AABF2AE04.png",
        "id_164841395497": "/images/iA7B0C6D62FD940727B9D5DBBD4165324.png",
        "id_17304885143": "/images/iC35735FF5486867B522A8ADFAFC3B6C7.png",
        "id_402471964": "/images/i4ECB85A2AE41ACB15653139F35BFA8C7.png",
        "id_17304885155": "/images/i94E3A2CA38A4F73C31EF4B2C5D5BE94B.png",
        "id_164841395445": "/images/i0BEBD7212FCEADD8E81C2812FD68C36D.png",
        "pincho2": "/images/iAF9569EFAB97FCE93FE3DAD449050784.png",
        "id_17304885129": "/images/i97EEC684C8F34E49FB30501C8417541F.png",
        "seguir": "/images/i1434B8CFE569C191FCC8DE95E5A1BC96.png",
        "id_164841395481": "/images/i15B71869941D9D056D2771715265227F.png",
        "id_164841395434": "/images/iBB18FBADFC2D527AD2709E8652AAFD5A.png",
        "id_17304885139": "/images/i570A2B785DF574495DD66D6D91299947.png",
        "id_164841395476": "/images/i691DBFC540A724998A2353C7D6DC199F.png",
        "emergencia_on": "/images/i7E92861F463166F66224942F55FA69B0.png",
        "id_164841395478": "/images/i35A05BBCDD161E2A6C0C478F703901C0.png",
        "id_164841395436": "/images/i359C4AC08A6FB6D5912E5ECA8ABBB00A.png",
        "id_1730488517": "/images/i5C878AB139179D2864AE95A2540738B3.png",
        "id_17304885158": "/images/iAADD9B59546B4EABA48E369D34ED0C21.png",
        "id_164841395465": "/images/i396F93C0F37490C405609068F53808DE.png",
        "pincho5": "/images/i0B10A526AD8D5B25BC5BAC5C2C06F119.png",
        "id_1648413954117": "/images/i2F569FBCA8D42EB735DED2831286DC66.png",
        "id_164841395444": "/images/i89096AB8DBE818E384384C292C89811F.png",
        "id_17304885151": "/images/i093B3780EEFCFADC8E24C0D54540C0B5.png",
        "id_17304885125": "/images/iCB3B5FFBFA8DF33CB29C50BC26542162.png",
        "pincho_normal": "/images/i1EC4F2338BB0785F7A6208CC19D16FA3.png",
        "id_17304885166": "/images/i0DE5EA646AA925B3530150BF5F230AA5.png",
        "id_1648413954103": "/images/i96D645D648D27F12B2E4F54E9503E8DF.png",
        "id_164841395467": "/images/iF3F5D57B22CF688DF19FE14CCD909402.png",
        "hoy_off": "/images/iEE7B77BF68990BD6E87F22F782B6F317.png",
        "id_164841395470": "/images/i6EA1D3660FE93F229A4DD0E034A728EB.png",
        "id_164841395468": "/images/i399EE8FDF222FDA603DF0C82E0BCCFAC.png",
        "id_164841395423": "/images/i42D15D48B2ED837F7903FE534A06494B.png",
        "entrada_off": "/images/i3DA1303A718E3AD93FF5A8CA452A055D.png",
        "id_17304885120": "/images/i907B21FBECE83B7799349F9FAA0D472A.png",
        "id_164841395459": "/images/iA152F49A26EC92286520C265AABD8CFC.png",
        "mono_6": "/images/i1EE8695ED4E44BD032E4C96194C31486.png",
        "tip_extra": "/images/i07526F3913A2AE7CCEDF61A779050CF9.png",
        "id_164841395429": "/images/i9BDA9EE2558DFE3F2125992BECA4278D.png",
        "id_164841395440": "/images/i585BBC1586441528C908AD41EB8EBDBD.png",
        "id_164841395498": "/images/i51313033EE92F99494C0CEF93F77F8D9.png",
        "id_16484139549": "/images/i88E908455A225B9390E3678E9C19FBA2.png",
        "id_164841395415": "/images/i6755383116D1A5AD862D47B6D5554641.png",
        "id_16484139546": "/images/iD7DACAF52A6FE787F42E337F896ED7E5.png",
        "perfil_off": "/images/i56ADB5CB2BA30CF0527FC3963E4B5673.png",
        "id_164841395488": "/images/iDAA2A0A70B3960D02C55DDF038216353.png",
        "id_164841395435": "/images/i10063715BD30D221415EF53335926168.png",
        "id_17304885147": "/images/i7F291EBC02606240803647C45234D01B.png",
        "id_17304885112": "/images/i3915CB7F49F97487FF413D7B39ED0B37.png",
        "id_17304885126": "/images/i220830766E53497CDAC422E9AAE22296.png",
        "id_17304885165": "/images/i8C849BCE5D5B003A5C94BC535D7C65EC.png",
        "id_164841395426": "/images/i077D23D9EA5866B0EF5E25614919665F.png",
        "id_17304885153": "/images/iF065FCB3FEA5C1EBC4123E4F1156C63D.png",
        "id_164841395494": "/images/i18ACDDC89C81835BFB74D3A0E71D8C10.png",
        "id_17304885118": "/images/i37DC047D2355AA99AF3408D44781C90B.png",
        "id_1648413954111": "/images/i661E5F5296ABB7AB4B0E5D236E6C7630.png",
        "id_164841395421": "/images/i46769D88018418464C9F4B24D384CB8B.png",
        "id_1648413954119": "/images/iAE28EA1830BF41F55AD84813E8F1D056.png",
        "id_1648413954116": "/images/iBBA0A82C21EA239F79E171AC51EA7B95.png",
        "id_164841395460": "/images/i154CF2D0DF49584435B369E991104CD0.png",
        "id_164841395449": "/images/iF714D356682197276705B90C40BA8A03.png",
        "id_164841395418": "/images/i38E6B9D378262B4EF35387DC47BBF31D.png",
        "id_17304885121": "/images/i901A29B5913762D207E0BEBD505A9A5F.png",
        "id_164841395446": "/images/i1B954A4FDCFE0860F6D3EBC4D01D2F19.png",
        "id_164841395499": "/images/i26C8BDFC1DEA0C67358C293A353C327B.png",
        "id_1648413954101": "/images/i9C4EAA87C9F69FE64BE240DAE158CF0E.png",
        "pincho9": "/images/i8893A1F7922FA64C321EAD4A6119E30D.png",
        "id_1648413954105": "/images/i6CD686DC5A11FB94CBB2A09FC90DB6EF.png",
        "id_164841395495": "/images/i5A8E6E8D34ABB47533165D506B7E1462.png",
        "pincho1": "/images/i016B8B08EE4DEC0313B1A3082A84D2F2.png",
        "id_1648413954112": "/images/iCCB062F45E0D9D917C617FDB2EA1E357.png",
        "id_17304885157": "/images/i89E93536F928FB549C7EEE44EF370DCE.png",
        "pincho3": "/images/i56ABF76585C9B6A661090A44C21595F2.png",
        "id_164841395448": "/images/iBEB7E7D434C1667889663859EAE75D92.png",
        "id_164841395424": "/images/iBC7C1AA388608962A1F7B8C61BD6DA82.png",
        "id_17304885168": "/images/i8B825AD136827F17DB6D478184C85301.png",
        "mono_3": "/images/i66166742DB624694FEA59FCCAF583FB7.png",
        "id_1648413954114": "/images/iAD45960BC2841EC6E2D86283A2F33E96.png",
        "id_164841395458": "/images/i380DF6EF85EB2185DE1A501FEE66371A.png",
        "id_164841395457": "/images/i0DE060270629150E8A87173EDB8B79F7.png",
        "tareas_off": "/images/iA1C4E28A42E2B254D31FB3A96A742031.png",
        "id_164841395417": "/images/i40A27D206CA23815C331AB71422629F2.png",
        "id_164841395484": "/images/i7FDF4568F230AA5A4D3B701708D1E8D6.png",
        "id_1648413954108": "/images/i5907CCF7C26ECC19EAD231C90A4FAB9D.png",
        "id_17304885131": "/images/i829FF9A81D8C1537788109FCE55BA372.png",
        "id_164841395493": "/images/i68421A3D08C3B75227181E0EC9EBCE42.png",
        "id_17304885163": "/images/iF77E81CC3C9BE873EB33BA0AAD43D591.png",
        "id_17304885164": "/images/i79D6CE5169F51E74EF1FEA39DB07CEB9.png",
        "id_164841395477": "/images/iD182D4C677A7FEF7B7F9D2021F901A9C.png",
        "id_164841395491": "/images/iF8B6DC3340FEC020058F7C743ADA0098.png",
        "id_164841395456": "/images/i9B1B0BD5A88BDAB459D66F68B80309F5.png",
        "id_164841395483": "/images/i88CB92AA112DA9B4D3913374D31E5489.png",
        "id_164841395466": "/images/i28D1EA7CF68179295028D9F1DC787292.png",
        "id_1730488514": "/images/iCD6AA874B07BD726ABF7364ED6851236.png",
        "id_164841395453": "/images/i3CD6AA1281B7B4ED5F37ED76FCC2A808.png",
        "id_164841395475": "/images/i7E06C884017083155A6857A40DCF4DE3.png",
        "id_17304885110": "/images/i5CF917F99E733C468810E2F5D08CC88D.png",
        "id_164841395425": "/images/iF09D8144E898AB6F35D2FF363EE98F9E.png",
        "id_164841395428": "/images/i21932839113ADBF836C5D8C8B4B11AEB.png",
        "id_17304885167": "/images/i3137CC503DA91A0688C5F38A0095A244.png",
        "pincho8": "/images/iCF8339009327869FD10EC12DA520DC5A.png",
        "id_164841395431": "/images/iE28ABABF9CABE7255711922BBF55B983.png",
        "id_17304885130": "/images/iEA1F5243153E7D22D525312FDFAE5A90.png",
        "id_1648413954106": "/images/iEBD4EE843BEEB6E225C25387B7DCF30E.png",
        "id_17304885154": "/images/iA06FB0DE96CD4315F67874E0FBDB4EE1.png",
        "id_17304885123": "/images/i5D746AFA0C7F3FCE385618C403086A21.png",
        "id_17304885160": "/images/i749E117F5781F156EF8FE76959C4144F.png",
        "emergencia_off": "/images/iCF94090E508927BED4CDC1EEA6432A46.png",
        "id_17304885145": "/images/iEDCBD1534AE3D23AD5893D34EEA1BB5C.png",
        "id_16484139548": "/images/i1A8982F638B1931C2A37ED5E826C17C0.png",
        "id_17304885137": "/images/i77C1E7F3FBBE442E1B12C53126B13B1B.png",
        "id_164841395452": "/images/iBAAC1AF775CEE1B716B575455DB68FDC.png",
        "mono_7": "/images/i3ECE8B0251AA8C358DD86D279774A6FE.png",
        "perfil_on": "/images/iAD127855BF1E2CC6C2F28479754B79C0.png",
        "id_17304885115": "/images/i1B011A35F1C8D4708DA9E83D549EA011.png",
        "id_164841395439": "/images/i9AE08E1C51AF160F89D84F868261FCFD.png",
        "id_17304885142": "/images/iF0674BDE61F66D48C2CA74C15C3AC823.png",
        "id_164841395487": "/images/i57C1B3043D0424531659720F9C32B1C2.png",
        "id_164841395473": "/images/i273522DAAED644BC07219E8653954EE3.png",
        "id_17304885124": "/images/i21006680354E2478937ED256C47F0D6F.png",
        "id_164841395482": "/images/i8A0B707451CCBE122A5B28F640716AF1.png",
        "id_164841395437": "/images/i0E59AE058F689EF3F4FAD9FDDF470974.png",
        "id_1361874287": "/images/i8E0D4613C253E68B59FED20BB17AEEF9.png",
        "linea": "/images/iA56941C357BC902627770A6229AC94A1.png",
        "pinchocasa": "/images/i31D884D83D4B504A668DECEFFAE7EA43.png",
        "id_1730488513": "/images/i8454D88E5895192B0770F08DB2F4C7D6.png",
        "id_17304885136": "/images/iCA6997D4EA75B7A7EA52E0BBCF58089A.png",
        "id_164841395474": "/images/i86B7E95443CEBED206372766EAF4F59C.png",
        "id_1852819254": "/images/i8E0D4613C253E68B59FED20BB17AEEF9.png",
        "id_17304885171": "/images/i9136DCA319DE76A1A2437B6415790BB9.png",
        "id_164841395413": "/images/i2175C7E985066E9DBB5FD1E204BDED5D.png",
        "id_1730488515": "/images/iAB265FAD2F6EA6964A2299BAF0375F29.png",
        "id_164841395447": "/images/i304ED678B67AD49378DB6228C609BE45.png",
        "id_1730488518": "/images/iFDDDAFE794E6B3D1640B628D4327FCD4.png",
        "id_17304885119": "/images/i400DF90467135851598103A438FA2CBE.png",
        "id_164841395463": "/images/iF0D35F95D8CF565D84F4B2EABC07C0C3.png",
        "pincho6": "/images/i15970AF9F27BB88EC2DC1EE088D2551E.png",
        "id_164841395412": "/images/i867483CAEDC8D77255A7F25A15625CA5.png",
        "id_164841395464": "/images/iAC8C1B1E912288D6EF52DC4C71B79944.png",
        "id_164841395416": "/images/i1391131C784CF99686D19D65AC20986E.png",
        "id_164841395485": "/images/i69B8A04A79F91820581E4D3708B7D003.png",
        "id_164841395461": "/images/iB6A8B660E26087802754C713C196B739.png",
        "id_164841395443": "/images/iBFDCFF2749FF3ACE5A75BF05C2C1E25C.png",
        "id_164841395432": "/images/i0A41ED8B1E443429769FFB49137003C2.png",
        "id_164841395422": "/images/i3F6BAF33AC1E4011205917185EA98CAD.png",
        "id_1730488519": "/images/i21A429706C7934A4774996F025B3B1AF.png",
        "id_164841395420": "/images/i405EC5CB58F536363712327BE6376BB1.png",
        "id_1730488512": "/images/iC91EEFE37F9302D5B31812F55B1658CC.png",
        "id_17304885135": "/images/iE289D15EF298A75107CE5D8C53D93DFA.png",
        "pincho7": "/images/iA6C17CF225E89F54DE02BF67B8FCA6A0.png",
        "logo": "/images/i31ABAC96411E678886E8E42DD5A4F520.png",
        "id_16484139545": "/images/iC844F1FD37D49F099C8E19CDD3168A41.png",
        "id_164841395469": "/images/i09C63C089B2E569672618ED5FC69504C.png",
        "id_16484139541": "/images/i21578740E736331F7BDC9D452BD712C9.png",
        "id_164841395496": "/images/i9AD38C8643DBCC036D0D226508F2B971.png",
        "id_17304885117": "/images/i8E921D2D9D8781FB98CDAC70A1508CD5.png",
        "id_17304885113": "/images/iAED1FDE3F57D021F59389F23987A43CC.png",
        "id_1648413954107": "/images/iB0DCEF19023ADDE9B38C97AF31540D4D.png",
        "id_164841395455": "/images/iDAED2AF86C310E481C7409143758ECB6.png",
        "id_164841395427": "/images/iA83DD447D387E7698AB46331B8A8D019.png",
        "id_17304885128": "/images/i46787C084B30E2C588FF57C442127DBE.png",
        "entrada_on": "/images/i93E2F664C26053BA732526F3E38C6FD3.png",
        "id_17304885146": "/images/i4D19A43D79882DDD81ADC3FEE88FC5E0.png",
        "id_16484139543": "/images/iE4617B7080958F45BF2C192CC042382D.png",
        "id_1648413954109": "/images/i8143DE4F0A77BC1F4111FF6A02EED95E.png",
        "id_164841395451": "/images/i00760138B01D3550976723B1B225C943.png",
        "id_17304885170": "/images/i7926A4414B70F4E087DBB9DC20D34D84.png",
        "id_17304885132": "/images/i72E9850BB987DCF26DD0CC778C372383.png",
        "id_164841395486": "/images/i55288AB1A85C7038BCA8AD843ADF586F.png",
        "id_17304885172": "/images/i19277EEC1CB9EB9A319964D895656ECE.png",
        "id_17304885133": "/images/i14318836CA86ECEF8C2D9F7ADD5EABAE.png",
        "id_1046919308": "/images/i8E0D4613C253E68B59FED20BB17AEEF9.png",
        "pincho4": "/images/i999E1B96A6FB0BFB9034B067828F4A68.png",
        "id_164841395462": "/images/iD0A8DD7F8257D87CADD4557D2DDE27A0.png",
        "id_164841395490": "/images/i10CCB6F0BFED76841F1607FA34C062B1.png",
        "id_333012614": "/images/i8E0D4613C253E68B59FED20BB17AEEF9.png",
        "id_16484139544": "/images/i9290A0A895DCA701657AA3A2701BC5B1.png",
        "id_164841395480": "/images/iE34B2EECFDC74FC20A5DEA25E204DB7A.png",
        "id_164841395450": "/images/iA1B6778946A3A0358E419143E8081D2B.png",
        "id_1730488516": "/images/i9ABB2FA1BACD1BC4DAFC6D59EDE7E61F.png",
        "id_164841395479": "/images/iCF6F841564C2B00839FA0338556EE25E.png",
        "id_17304885134": "/images/i52476D8DE05175CC624D3B1F13DB9411.png"
    },
    "classes": {
        "#ID_1632444658": {
            "text": "L('x3376426101_traducir','NO')"
        },
        "#ID_1413046510": {
            "text": "L('x3746555228_traducir','SI')"
        },
        "#ID_1420945673": {
            "aceptar": "L('x1518866076_traducir','Aceptar')",
            "cancelar": "L('x2353348866_traducir','Cancelar')"
        },
        "#ID_111998300": {
            "text": "L('x2760387473_traducir','Alto (mt)')"
        },
        "#ID_2116871682": {
            "letra": "L('x1141589763','J')"
        },
        "#ID_523805973": {
            "_section": "L('x1916403066_traducir','hoy')"
        },
        "#ID_1989764705": {
            "text": "L('x3354371722_traducir','Recupero Material')"
        },
        "#ID_1975428302": {
            "titulo": "L('x1219835481_traducir','Muros / Tabiques')",
            "cargando": "L('x1740321226_traducir','cargando ..')",
            "hint": "L('x2879998099_traducir','Seleccione muros')",
            "subtitulo": "L('x4011106049_traducir','Indique los tipos')"
        },
        "#ID_708633473": {
            "text": "L('x4117795806_traducir','Superficie (m2)')"
        },
        "#ID_1891010907": {
            "pantalla": "L('x3294048930_traducir','documentos')"
        },
        "fondolila": {
            "backgroundColor": "#8383db",
            "font": {
                "fontSize": "12dp"
            }
        },
        "#ID_1191344867": {
            "text": "L('x426093682_traducir','Domicilio')"
        },
        "#ID_1334182915": {
            "title": "L('x1452485313_traducir','FINALIZADO')"
        },
        "#ID_25505023": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "14dp"
            }
        },
        "bg_progreso_celeste": {
            "backgroundColor": "#60bde2",
            "font": {
                "fontSize": "12dp"
            }
        },
        "#ID_915854835": {
            "on": "L('x1532004165','registro.d2')"
        },
        "#ID_1792932375": {
            "label_a": "L('x3904355907_traducir','a')",
            "_bono": "L('x2764662954_traducir','Bono adicional:')",
            "bt_cancelar": "L('x1030930307_traducir','Cerrar')"
        },
        "btheader": {
            "color": "#2d9edb",
            "font": {
                "fontWeight": "bold",
                "fontSize": "18dp"
            }
        },
        "#ID_975882591": {
            "text": "L('x2010049695_traducir','Presupuesto de Reparaciones del edificio en formato Excel, detallado por recintos a través de precios unitarios, según la siguiente descripcion: Partida, Unidad, Cantidad, Valor Unitario y Valor Total, incluyendo adicionalmente los Gastos Generales, Utilidad e IVA. (Se entrega formato completo).')"
        },
        "#ID_1465595034": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "9dp"
            }
        },
        "#ID_1577364445": {
            "text": "L('x2793345159_traducir','Fecha inspeccion')"
        },
        "estilo8": {
            "color": "#ee7f7e",
            "font": {
                "fontSize": "12dp"
            }
        },
        "#ID_509734203": {
            "text": "L('x3731894882_traducir','Se iniciará el proceso de inspección')"
        },
        "#ID_941354109": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "14dp"
            }
        },
        "#ID_1568547162": {
            "mins": "L('x4261170317','true')",
            "a": "L('x3904355907_traducir','a')"
        },
        "#ID_173048851": {
            "images": ["/images/iBC783890F4B9774991803EB6ECA81549.png", "/images/iC91EEFE37F9302D5B31812F55B1658CC.png", "/images/i8454D88E5895192B0770F08DB2F4C7D6.png", "/images/iCD6AA874B07BD726ABF7364ED6851236.png", "/images/iAB265FAD2F6EA6964A2299BAF0375F29.png", "/images/i9ABB2FA1BACD1BC4DAFC6D59EDE7E61F.png", "/images/i5C878AB139179D2864AE95A2540738B3.png", "/images/iFDDDAFE794E6B3D1640B628D4327FCD4.png", "/images/i21A429706C7934A4774996F025B3B1AF.png", "/images/i5CF917F99E733C468810E2F5D08CC88D.png", "/images/iE67E9F2E19A329505444E7465B5FD7D0.png", "/images/i3915CB7F49F97487FF413D7B39ED0B37.png", "/images/iAED1FDE3F57D021F59389F23987A43CC.png", "/images/i0E5D79CD99D2BBA09F0BEAA30A79A3D9.png", "/images/i1B011A35F1C8D4708DA9E83D549EA011.png", "/images/iD464C126D5638562F447AE472C2BD133.png", "/images/i8E921D2D9D8781FB98CDAC70A1508CD5.png", "/images/i37DC047D2355AA99AF3408D44781C90B.png", "/images/i400DF90467135851598103A438FA2CBE.png", "/images/i907B21FBECE83B7799349F9FAA0D472A.png", "/images/i901A29B5913762D207E0BEBD505A9A5F.png", "/images/iF6C82CEBB73C3F50FC5AC08E6FA50DF2.png", "/images/i5D746AFA0C7F3FCE385618C403086A21.png", "/images/i21006680354E2478937ED256C47F0D6F.png", "/images/iCB3B5FFBFA8DF33CB29C50BC26542162.png", "/images/i220830766E53497CDAC422E9AAE22296.png", "/images/i574DB1641BB1F722625EB07223F436E0.png", "/images/i46787C084B30E2C588FF57C442127DBE.png", "/images/i97EEC684C8F34E49FB30501C8417541F.png", "/images/iEA1F5243153E7D22D525312FDFAE5A90.png", "/images/i829FF9A81D8C1537788109FCE55BA372.png", "/images/i72E9850BB987DCF26DD0CC778C372383.png", "/images/i14318836CA86ECEF8C2D9F7ADD5EABAE.png", "/images/i52476D8DE05175CC624D3B1F13DB9411.png", "/images/iE289D15EF298A75107CE5D8C53D93DFA.png", "/images/iCA6997D4EA75B7A7EA52E0BBCF58089A.png", "/images/i77C1E7F3FBBE442E1B12C53126B13B1B.png", "/images/iF746C788F42D49A6AD715DC319BFF2B7.png", "/images/i570A2B785DF574495DD66D6D91299947.png", "/images/iE1BAB1BE32595937A03D896936E17846.png", "/images/i09B77564ECA69742FF6926701ADCC788.png", "/images/iF0674BDE61F66D48C2CA74C15C3AC823.png", "/images/iC35735FF5486867B522A8ADFAFC3B6C7.png", "/images/i87DE3A07ABFD59B0F96861DEC12DDF8F.png", "/images/iEDCBD1534AE3D23AD5893D34EEA1BB5C.png", "/images/i4D19A43D79882DDD81ADC3FEE88FC5E0.png", "/images/i7F291EBC02606240803647C45234D01B.png", "/images/i77F111A1F32961B1F386B1DC7377767D.png", "/images/i47F6C47BCA8C1146589A6899C5016EA7.png", "/images/iF37AFC4AE87333385D84EF93517E2F25.png", "/images/i093B3780EEFCFADC8E24C0D54540C0B5.png", "/images/i49BA5221BED2176E506AA26FABE14E75.png", "/images/iF065FCB3FEA5C1EBC4123E4F1156C63D.png", "/images/iA06FB0DE96CD4315F67874E0FBDB4EE1.png", "/images/i94E3A2CA38A4F73C31EF4B2C5D5BE94B.png", "/images/i98CA10AE81168639ED3D051D83F9FF4A.png", "/images/i89E93536F928FB549C7EEE44EF370DCE.png", "/images/iAADD9B59546B4EABA48E369D34ED0C21.png", "/images/i771C97B5058ACE91E7A66AD6D47C1934.png", "/images/i749E117F5781F156EF8FE76959C4144F.png", "/images/iD07A2DFA41B1AF2BAD9EC1879066DBE8.png", "/images/iB32E3B863CF1AC37F2816A7BCD60A554.png", "/images/iF77E81CC3C9BE873EB33BA0AAD43D591.png", "/images/i79D6CE5169F51E74EF1FEA39DB07CEB9.png", "/images/i8C849BCE5D5B003A5C94BC535D7C65EC.png", "/images/i0DE5EA646AA925B3530150BF5F230AA5.png", "/images/i3137CC503DA91A0688C5F38A0095A244.png", "/images/i8B825AD136827F17DB6D478184C85301.png", "/images/i7C907FDBDA8C040F234553C55D821A45.png", "/images/i7926A4414B70F4E087DBB9DC20D34D84.png", "/images/i9136DCA319DE76A1A2437B6415790BB9.png", "/images/i19277EEC1CB9EB9A319964D895656ECE.png"]
        },
        "#ID_1950131219": {
            "text": "L('x2722748660_traducir','Año de construccion')"
        },
        "#ID_924691181": {
            "text": "L('x1885200639_traducir','Tomar fotos del daño')"
        },
        "fondorosado": {
            "backgroundColor": "#ffacaa",
            "font": {
                "fontSize": "12dp"
            }
        },
        "#ID_592273440": {
            "seleccione": "L('x3621251639_traducir','unidad')"
        },
        "#ID_622180894": {
            "text": "L('x2188845716_traducir','Listado de Contenidos Afectados Valorizados, detallado por: Tipo de bien, Marca, Modelo, Edad y Valor Estimado (se entrega formato ejemplo).')"
        },
        "#ID_2042040375": {
            "text": "L('x2760387473_traducir','Alto (mt)')"
        },
        "#ID_1532603534": {
            "text": "L('x607758518_traducir','¿Existen otros seguros por la misma materia siniestrada?')"
        },
        "#ID_500930588": {
            "text": "L('x3861721694_traducir','Contraseña')"
        },
        "#ID_2142229625": {
            "pantalla": "L('x1805186499_traducir','contenidos')"
        },
        "#ID_82327069": {
            "pantalla": "L('x1828862638_traducir','basicos')"
        },
        "#ID_1028422273": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "9dp"
            }
        },
        "#ID_581833647": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "18dp"
            }
        },
        "#ID_2036600033": {
            "text": "L('x366125934_traducir','Tomar fotos del contenido')"
        },
        "#ID_663682001": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "9dp"
            }
        },
        "#ID_1594858833": {
            "hintText": "L('x314662607_traducir','Ingrese nombre')"
        },
        "#ID_1587413219": {
            "title": "L('x3952572309_traducir','SINIESTRO')"
        },
        "#ID_1377834092": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "9dp"
            }
        },
        "#ID_947148270": {
            "letra": "L('x2746444292','D')"
        },
        "#ID_863076866": {
            "hasta": "L('x2081216563','hasta_horas[0]')"
        },
        "#ID_1960650091": {
            "title": "L('x1489379407_traducir','¿HAY ALGUIEN?')"
        },
        "#ID_636596927": {
            "hintText": "L('x1001594672_traducir','RUT')"
        },
        "#ID_505684609": {
            "title": "L('x1769798096_traducir','Cercanos')"
        },
        "#ID_677272162": {
            "title": "L('x2146494644_traducir','ENROLAMIENTO')"
        },
        "#ID_334050739": {
            "title": "L('x2146494644_traducir','ENROLAMIENTO')"
        },
        "#ID_1296819022": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "20dp"
            }
        },
        "#ID_2027587518": {
            "titulo": "L('x258069688_traducir','Editar domicilio')"
        },
        "#ID_879344999": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "12dp"
            }
        },
        "#ID_624539553": {
            "title": "L('x3683967382_traducir','Siguiente')"
        },
        "#ID_2005131739": {
            "headerTitle": "L('x3424088795_traducir','ref1')"
        },
        "#ID_1234171100": {
            "hintText": "L('x314662607_traducir','Ingrese nombre')"
        },
        "#ID_1653129180": {
            "text": "L('x3976272881_traducir','¿ Esta listo ?')"
        },
        "#ID_1725006625": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "20dp"
            }
        },
        "#ID_1612935041": {
            "letra": "L('x1342839628','V')"
        },
        "#ID_1947416026": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "9dp"
            }
        },
        "#ID_343670043": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "18dp"
            }
        },
        "#ID_1034095675": {
            "text": "L('x4067701965_traducir','Dirección Riesgo')"
        },
        "#ID_228731410": {
            "title": "L('x2943883035_traducir','Guardar')"
        },
        "#ID_95735942": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "9dp"
            }
        },
        "#ID_1904383038": {
            "titulo": "L('x86212863_traducir','COMPAÑIA')",
            "cargando": "L('x1740321226_traducir','cargando ..')",
            "campo": "L('x3363644313_traducir','Compañía')",
            "seleccione": "L('x3859572977_traducir','seleccione compañía')"
        },
        "#ID_1448626648": {
            "text": "L('x435194361_traducir','Descripcion del siniestro')"
        },
        "#ID_1086440326": {
            "valor": "L('x4264937424','evento.valor')"
        },
        "#ID_1534880474": {
            "title": "L('x2726960050_traducir','EDITAR')"
        },
        "#ID_982792988": {
            "text": "L('x2043784063_traducir','Comuna')"
        },
        "#ID_995335318": {
            "text": "L('x1523866503_traducir','Valor del Objeto')"
        },
        "fondomorado": {
            "backgroundColor": "#b9aaf3",
            "font": {
                "fontSize": "12dp"
            }
        },
        "#ID_1326186190": {
            "letra": "L('x185522819_traducir','MI')"
        },
        "#ID_1565575511": {
            "title": "L('x3719714762_traducir','inicio')"
        },
        "#ID_201283538": {
            "valor": "L('x4264937424','evento.valor')"
        },
        "#ID_1558901197": {
            "text": "L('x2536760995_traducir','ASEGURADO SE COMPROMETE EN ENVIAR LOS ANTECEDENTES SOLICITADOS EN:')"
        },
        "estilo9": {
            "font": {
                "fontSize": "16dp"
            }
        },
        "#ID_651351303": {
            "title": "L('x2146494644_traducir','ENROLAMIENTO')"
        },
        "#ID_1605312581": {
            "text": "L('x2973460117_traducir','Año del Nivel')"
        },
        "#ID_1259303976": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "9dp"
            }
        },
        "#ID_21955821": {
            "texto": "L('x3839284819_traducir','Tip: Al iniciar seguimiento, le avisaremos al cliente que vas en camino')"
        },
        "#ID_1150092109": {
            "pantalla": "L('x1828862638_traducir','basicos')"
        },
        "#ID_966843289": {
            "text": "L('x2509905523_traducir','Seleccione días disponibles')"
        },
        "#ID_121244764": {
            "title": "L('x582013978_traducir','DATOS DEL ASEGURADO')"
        },
        "#ID_1262428321": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "18dp"
            }
        },
        "#ID_871051365": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "14dp"
            }
        },
        "prioridad_otro": {
            "color": "#a5876d",
            "font": {
                "fontSize": "15dp"
            }
        },
        "#ID_1704296253": {
            "headerTitle": "L('x27834329_traducir','fecha')"
        },
        "fondoverde": {
            "backgroundColor": "#8ce5bd",
            "font": {
                "fontSize": "12dp"
            }
        },
        "#ID_1342029941": {
            "text": "L('x2764946924_traducir','Asegurado')"
        },
        "#ID_1098579444": {
            "on": "L('x743946195','registro.d3')"
        },
        "#ID_1696312604": {
            "text": "L('x2324757213_traducir','Asegurador/Corredor')"
        },
        "#ID_1703221543": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "18dp"
            }
        },
        "#ID_1185934609": {
            "text": "L('x3172022095_traducir','Coloque rango de horas')"
        },
        "#ID_461587198": {
            "text": "L('x1035217420_traducir','Recinto')"
        },
        "#ID_1837940774": {
            "pantalla": "L('x2234318590_traducir','firma')"
        },
        "#ID_1154897587": {
            "text": "L('x1375988868_traducir','?%')"
        },
        "#ID_620197319": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "9dp"
            }
        },
        "#ID_959212917": {
            "text": "L('x173701603_traducir','Fecha de Compra')"
        },
        "#ID_1753840856": {
            "text": "L('x2467432966_traducir','Evaluacion de daños generales')"
        },
        "#ID_23912785": {
            "titulo": "L('x1524107289_traducir','CONTINUAR')"
        },
        "#ID_1544703483": {
            "title": "L('x3683967382_traducir','Siguiente')"
        },
        "#ID_1328468903": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "9dp"
            }
        },
        "#ID_252896651": {
            "titulo": "L('x1866523485_traducir','Estruct. cubierta')",
            "cargando": "L('x1740321226_traducir','cargando ..')",
            "hint": "L('x2460890829_traducir','Seleccione e.cubiertas')",
            "subtitulo": "L('x4011106049_traducir','Indique los tipos')"
        },
        "estilo8_2": {
            "color": "#ee7f7e",
            "font": {
                "fontFamily": "SFUIText-Medium",
                "fontSize": "10dp"
            }
        },
        "#ID_1385093082": {
            "title": "L('x2726960050_traducir','EDITAR')"
        },
        "#ID_28953735": {
            "text": "L('x4000049319_traducir','Pedro Herrera Silva')"
        },
        "#ID_1747370698": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "9dp"
            }
        },
        "#ID_662931237": {
            "hintText": "L('x742992573_traducir','meses')"
        },
        "#ID_1014684330": {
            "valor": "L('x1367393739','dato_n1[0].id_label')"
        },
        "#ID_1919630060": {
            "text": "L('x2558236558_traducir','Largo (mt)')"
        },
        "#ID_500305974": {
            "text": "L('x4106122489_traducir','ENVIAR')"
        },
        "#ID_1724275107": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "18dp"
            }
        },
        "#ID_1517641115": {
            "mins": "L('x4261170317','true')",
            "a": "L('x3904355907_traducir','a')"
        },
        "#ID_1086853652": {
            "pantalla": "L('x3681655494_traducir','caracteristicas')"
        },
        "#ID_1776516445": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "20dp"
            }
        },
        "#ID_1864949772": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "18dp"
            }
        },
        "#ID_673432332": {
            "titulo": "L('x587441556_traducir','PARTE 1: País de residencia')",
            "avance": "L('x122616641_traducir','1/6')"
        },
        "#ID_59957637": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "15dp"
            }
        },
        "#ID_786813367": {
            "title": "L('x3567030427_traducir','TAREAS ENTRANTES')"
        },
        "#ID_1146042904": {
            "titulo": "L('x118417065_traducir','PARTE 2: Datos personales')",
            "avance": "L('x84428056_traducir','2/6')"
        },
        "#ID_572364594": {
            "hintText": "L('x4108050209','0')"
        },
        "#ID_1383074449": {
            "text": "L('x1283592374_traducir','Niveles')"
        },
        "#ID_1594192805": {
            "text": "L('x2067448595_traducir','Cancelando Inspección, espere ..')"
        },
        "#ID_768820897": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "20dp"
            }
        },
        "#ID_887257458": {
            "headerTitle": "L('x2345059420','fecha_entrantes')"
        },
        "#ID_1950087430": {
            "text": "L('x267051884_traducir','nivel 2')"
        },
        "#ID_1414823663": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "9dp"
            }
        },
        "#ID_1512817053": {
            "headerTitle": "L('x3424088795_traducir','ref1')"
        },
        "#ID_1795647174": {
            "text": "L('x3867756121_traducir','nivel 4')"
        },
        "#ID_1852315924": {
            "texto": "L('x1488972008_traducir','Tip: No puedes iniciar inspecciones para tareas que no son para el dia')"
        },
        "#ID_1490565172": {
            "text": "L('x1951430381_traducir','Piso')"
        },
        "#ID_1991643897": {
            "text": "L('x483197933_traducir','Seleccione año')"
        },
        "#ID_1291362588": {
            "text": "L('x4137765611_traducir','¿El seguro está asociado a un Crédito Hipotecario?')"
        },
        "#ID_1279618182": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "9dp"
            }
        },
        "#ID_1252165328": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "9dp"
            }
        },
        "porcentaje": {
            "color": "#333333",
            "font": {
                "fontWeight": "bold",
                "fontSize": "18dp"
            }
        },
        "#ID_947985444": {
            "text": "L('x2474326379_traducir','TERMINACIONES')"
        },
        "#ID_281307344": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "9dp"
            }
        },
        "#ID_878607546": {
            "text": "L('x2354316101_traducir','Teléfono')"
        },
        "#ID_1466872410": {
            "text": "L('x3376426101_traducir','NO')"
        },
        "#ID_811245783": {
            "title": "L('x2398692269_traducir','Tu casa')"
        },
        "#ID_1277998911": {
            "title": "L('x2464850483_traducir','FIRMA CLIENTE')"
        },
        "#ID_1386430374": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "16dp"
            }
        },
        "#ID_1387187998": {
            "text": "L('x2028852218_traducir','nivel 3')"
        },
        "#ID_931569721": {
            "text": "L('x2151727540_traducir','Nro caso sistema')"
        },
        "#ID_1423732331": {
            "title": "L('x2726960050_traducir','EDITAR')"
        },
        "#ID_862545319": {
            "aceptar": "L('x1518866076_traducir','Aceptar')",
            "cancelar": "L('x2353348866_traducir','Cancelar')"
        },
        "#ID_1280934004": {
            "text": "L('x962007463_traducir','Certificado de Hipotecas y Gravámenes Actualizado')"
        },
        "#ID_7804354": {
            "text": "L('x2421821072_traducir','Datos')"
        },
        "#ID_446037089": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "9dp"
            }
        },
        "#ID_1536081210": {
            "titulo": "L('x1866523485_traducir','Estruct. cubierta')",
            "cargando": "L('x1740321226_traducir','cargando ..')",
            "hint": "L('x2460890829_traducir','Seleccione e.cubiertas')",
            "subtitulo": "L('x4011106049_traducir','Indique los tipos')"
        },
        "#ID_1501498971": {
            "pantalla": "L('x3681655494_traducir','caracteristicas')"
        },
        "#ID_517891806": {
            "title": "L('x2745628043_traducir','REGISTRARSE')"
        },
        "#ID_399652068": {
            "titulo": "L('x3327059844_traducir','Entrepisos')",
            "cargando": "L('x1740321226_traducir','cargando ..')",
            "hint": "L('x2146928948_traducir','Seleccione entrepisos')",
            "subtitulo": "L('x4011106049_traducir','Indique los tipos')"
        },
        "#ID_1759947970": {
            "hintText": "L('x3619315722_traducir','Escribir nivel 3')"
        },
        "#ID_1908023149": {
            "titulo": "L('x52303785_traducir','UNIDAD')",
            "cargando": "L('x1740321226_traducir','cargando ..')",
            "campo": "L('x2054670809_traducir','Unidad de medida')",
            "seleccione": "L('x4091990063_traducir','unidad')"
        },
        "#ID_228046760": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "9dp"
            }
        },
        "#ID_859578408": {
            "texto": "L('x2523383595_traducir','Tip: No aceptes tareas si no estás seguro que puedes llegar a tiempo')"
        },
        "#ID_1777835372": {
            "titulo": "L('x3327059844_traducir','Entrepisos')",
            "cargando": "L('x1740321226_traducir','cargando ..')",
            "hint": "L('x2146928948_traducir','Seleccione entrepisos')",
            "subtitulo": "L('x4011106049_traducir','Indique los tipos')"
        },
        "estilo14": {
            "color": "#fcbd83",
            "font": {
                "fontWeight": "bold",
                "fontSize": "15dp"
            }
        },
        "#ID_204298172": {
            "title": "L('x1471097171_traducir','NUEVO RECINTO')"
        },
        "#ID_1357980546": {
            "hintText": "L('x714216034_traducir','escribir')"
        },
        "#ID_132277174": {
            "text": "L('x2249118711_traducir','Disponibilidad')"
        },
        "#ID_1943547704": {
            "text": "L('x4117795806_traducir','Superficie (m2)')"
        },
        "#ID_511044183": {
            "titulo": "L('x2059554513_traducir','ENTIDAD')",
            "cargando": "L('x1740321226_traducir','cargando ..')",
            "campo": "L('x2538351238_traducir','Entidad Financiera')",
            "seleccione": "L('x3782298892_traducir','seleccione entidad')"
        },
        "#ID_1991898351": {
            "hintText": "L('x647322985_traducir','Repita su teléfono')"
        },
        "#ID_1988142494": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "9dp"
            }
        },
        "#ID_246047384": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "20dp"
            }
        },
        "#ID_1964190745": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "17dp"
            }
        },
        "#ID_1266631229": {
            "hintText": "L('x3505400086_traducir','Máximo 140 caracteres')"
        },
        "#ID_658148125": {
            "label_a": "L('x3904355907_traducir','a')",
            "_bono": "L('x2764662954_traducir','Bono adicional:')"
        },
        "#ID_1766077739": {
            "text": "L('x2547889144','-')"
        },
        "#ID_628891891": {
            "pantalla": "L('x2627320454','insp_cancelada')"
        },
        "#ID_499000080": {
            "text": "L('x2434817046_traducir','Entra para editar tus datos')"
        },
        "#ID_1167199251": {
            "text": "L('x1375988868_traducir','?%')"
        },
        "#ID_1995173537": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "18dp"
            }
        },
        "#ID_705299174": {
            "text": "L('x2442032847_traducir','nivel 5')"
        },
        "#ID_1120273894": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "14dp"
            }
        },
        "#ID_1503332472": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "36dp"
            }
        },
        "estilo7": {
            "color": "#999999",
            "font": {
                "fontFamily": "SFUIText-Medium",
                "fontSize": "14dp"
            }
        },
        "#ID_848195789": {
            "on": "L('x725251018','registro.d7')"
        },
        "#ID_1240322270": {
            "text": "L('x2520471236_traducir','DD-MM-YYYY')"
        },
        "#ID_1189525823": {
            "letra": "L('x3664761504','M')"
        },
        "#ID_1226671661": {
            "text": "L('x1396136993_traducir','Dirección')"
        },
        "#ID_1616529374": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "14dp"
            }
        },
        "#ID_1312331480": {
            "titulo": "L('x3904602325_traducir','Editar país')"
        },
        "#ID_1740801603": {
            "letra": "L('x2909332022','L')"
        },
        "fondorojo": {
            "backgroundColor": "#ee7f7e",
            "font": {
                "fontSize": "12dp"
            }
        },
        "#ID_1562239072": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "14dp"
            }
        },
        "#ID_1399189593": {
            "titulo": "L('x233750493_traducir','PARTE 3: Domicilio')",
            "avance": "L('x80359215_traducir','3/6')"
        },
        "#ID_39376060": {
            "title": "L('x4216663563_traducir','Emergencias')"
        },
        "#ID_687588341": {
            "text": "L('x529917922_traducir','¿Estás dispuesto a viajar fuera de tu país?')"
        },
        "titulo_recinto": {
            "color": "#8383db",
            "font": {
                "fontSize": "16dp"
            }
        },
        "#ID_971947162": {
            "hintText": "L('x1935574480_traducir','ingrese usuario')"
        },
        "#ID_1001083957": {
            "text": "L('x3172022095_traducir','Coloque rango de horas')"
        },
        "#ID_30774663": {
            "text": "L('x3136609939_traducir','Fono fijo')"
        },
        "#ID_1018168074": {
            "text": "L('x2438444121_traducir','Descripción del contenido')"
        },
        "#ID_795688903": {
            "text": "L('x4100297395_traducir','Ancho (mt)')"
        },
        "#ID_1167075018": {
            "text": "L('x3920081714_traducir','Repetir correo')"
        },
        "estilo11": {
            "color": "#2d9edb",
            "font": {
                "fontWeight": "bold",
                "fontSize": "15dp"
            }
        },
        "#ID_1674875520": {
            "headerTitle": "L('x1916403066_traducir','hoy')"
        },
        "#ID_1396533677": {
            "letra": "L('x3664761504','M')"
        },
        "#ID_1661890898": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "20dp"
            }
        },
        "#ID_862619153": {
            "title": "L('x2435611700_traducir','NUEVO DAÑO')"
        },
        "#ID_232106170": {
            "titulo": "L('x2266302645_traducir','Cubierta')",
            "cargando": "L('x1740321226_traducir','cargando ..')",
            "hint": "L('x2134385782_traducir','Seleccione cubiertas')",
            "subtitulo": "L('x4011106049_traducir','Indique los tipos')"
        },
        "#ID_2081717098": {
            "titulo": "L('x404129330_traducir','¿EL ASEGURADO CONFIRMA ESTOS DATOS?')",
            "si": "L('x1723413441_traducir','SI, Están correctos')",
            "texto": "L('x2083765231_traducir','El asegurado debe confirmar que los datos de esta sección están correctos')",
            "pantalla": "L('x2851883104_traducir','¿ESTA DE ACUERDO?')",
            "no": "L('x55492959_traducir','NO, Hay que modificar algo')"
        },
        "#ID_1771733970": {
            "title": "L('x1486275980_traducir','NUEVO CONTENIDO')"
        },
        "#ID_1402555963": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "8dp"
            }
        },
        "#ID_1689623114": {
            "titulo": "L('x1219835481_traducir','Muros / Tabiques')",
            "cargando": "L('x1740321226_traducir','cargando ..')",
            "hint": "L('x2879998099_traducir','Seleccione muros')",
            "subtitulo": "L('x4011106049_traducir','Indique los tipos')"
        },
        "#ID_179299802": {
            "text": "L('x1692519842_traducir','Primero debemos verificar tu disponibilidad')"
        },
        "#ID_993907842": {
            "letra": "L('x543223747','S')"
        },
        "#ID_66128070": {
            "text": "L('x2973460117_traducir','Año del Nivel')"
        },
        "#ID_1327360331": {
            "pantalla": "L('x3772634211_traducir','siniestro')"
        },
        "estilo5_1": {
            "color": "#c0c0c7",
            "font": {
                "fontSize": "17dp"
            }
        },
        "#ID_983090064": {
            "hintText": "L('x2452733176_traducir','ingrese correo')"
        },
        "#ID_1560093094": {
            "title": "L('x3096182311_traducir','TOMAR TAREA')"
        },
        "#ID_180477701": {
            "text": "L('x1890062079_traducir','Correo')"
        },
        "#ID_1045476414": {
            "text": "L('x3376426101_traducir','NO')"
        },
        "#ID_1590673691": {
            "title": "L('x2605545184_traducir','Mis tareas')"
        },
        "fondoceleste": {
            "backgroundColor": "#8bc9e8",
            "font": {
                "fontSize": "12dp"
            }
        },
        "estilo13": {
            "color": "#ffffff",
            "font": {
                "fontSize": "12dp"
            }
        },
        "#ID_1161659083": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "15dp"
            }
        },
        "#ID_1261706953": {
            "text": "L('x2067448595_traducir','Cancelando Inspección, espere ..')"
        },
        "#ID_1962514889": {
            "text": "L('x2067448595_traducir','Cancelando Inspección, espere ..')"
        },
        "#ID_518154422": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "9dp"
            }
        },
        "#ID_2016929633": {
            "text": "L('x3523939734_traducir','Asegúrate de haber ingresado todos los daños')"
        },
        "#ID_71757881": {
            "text": "L('x3964605301_traducir','Part time')"
        },
        "#ID_1183320931": {
            "hintText": "L('x1239348649_traducir','Escribir nivel 4')"
        },
        "#ID_392594798": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "15dp"
            }
        },
        "#ID_1719306939": {
            "hintText": "L('x859125505_traducir','Escriba')"
        },
        "#ID_2073052255": {
            "text": "L('x2509905523_traducir','Seleccione días disponibles')"
        },
        "#ID_1939219122": {
            "hintText": "L('x300188755_traducir','año')"
        },
        "#ID_516906299": {
            "titulo": "L('x404129330_traducir','¿EL ASEGURADO CONFIRMA ESTOS DATOS?')",
            "si": "L('x1723413441_traducir','SI, Están correctos')",
            "texto": "L('x2083765231_traducir','El asegurado debe confirmar que los datos de esta sección están correctos')",
            "pantalla": "L('x2851883104_traducir','¿ESTA DE ACUERDO?')",
            "no": "L('x55492959_traducir','NO, Hay que modificar algo')"
        },
        "#ID_1772078359": {
            "title": "L('x1563580555_traducir','DAÑOS')"
        },
        "#ID_1634717783": {
            "title": "L('x2943883035_traducir','Guardar')"
        },
        "#ID_335577466": {
            "font": {
                "fontFamily": "",
                "fontSize": "18dp"
            }
        },
        "#ID_1119875032": {
            "title": "L('x2146494644_traducir','ENROLAMIENTO')"
        },
        "#ID_1255090666": {
            "text": "L('x318795834_traducir','Full time')"
        },
        "#ID_1016162150": {
            "text": "L('x3307875748_traducir','¿Cuántos días?')"
        },
        "#ID_1405369085": {
            "text": "L('x3867756121_traducir','nivel 4')"
        },
        "#ID_371989095": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "8dp"
            }
        },
        "#ID_674889091": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "9dp"
            }
        },
        "#ID_602473581": {
            "text": "L('x2903793082_traducir','rut')"
        },
        "#ID_593401253": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "9dp"
            }
        },
        "#ID_1675410232": {
            "texto": "L('x2695983055_traducir','PRESIONE PARA ACEPTAR')"
        },
        "#ID_497149076": {
            "data": "L('x2347414356','data_oficios')"
        },
        "#ID_397255117": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "16dp"
            }
        },
        "#ID_1694932226": {
            "text": "L('x664979134_traducir','Edificio')"
        },
        "#ID_1601253540": {
            "text": "L('x246910460_traducir','Agregue cada contenido movible dañado')"
        },
        "#ID_1664480644": {
            "headerTitle": "L('x3424088795_traducir','ref1')"
        },
        "#ID_1634370902": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "9dp"
            }
        },
        "bg_progreso_rosado": {
            "backgroundColor": "#ff9292",
            "font": {
                "fontSize": "12dp"
            }
        },
        "#ID_1454083959": {
            "text": "L('x1001594672_traducir','RUT')"
        },
        "#ID_1283878755": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "14dp"
            }
        },
        "#ID_1052786642": {
            "text": "L('x1323451372_traducir','Planos y Memoria de Cálculo Estructural')"
        },
        "#ID_342918202": {
            "text": "L('x1344045403_traducir','Planos de Arquitectura (Plantas, Elevaciones y Cortes)')"
        },
        "#ID_91435429": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "15dp"
            }
        },
        "#ID_2045024898": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "18dp"
            }
        },
        "#ID_363463535": {
            "titulo": "L('x2266302645_traducir','Cubierta')",
            "cargando": "L('x1740321226_traducir','cargando ..')",
            "hint": "L('x2134385782_traducir','Seleccione cubiertas')",
            "subtitulo": "L('x4011106049_traducir','Indique los tipos')"
        },
        "#ID_466094334": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "16dp"
            }
        },
        "#ID_1489499488": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "20dp"
            }
        },
        "#ID_459685228": {
            "text": "L('x1350052795_traducir','Definir % de daños')"
        },
        "#ID_362822250": {
            "on": "L('x2989695600','registro.d4')"
        },
        "#ID_409457031": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "9dp"
            }
        },
        "#ID_1303818490": {
            "titulo": "L('x2353066504_traducir','LLAMAR ASEGURADO')"
        },
        "#ID_1863508631": {
            "title": "L('x2943883035_traducir','Guardar')"
        },
        "#ID_1726261643": {
            "hintText": "L('x93906838_traducir','ingrese su teléfono')"
        },
        "#ID_1511661336": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "9dp"
            }
        },
        "#ID_1747880333": {
            "seleccione": "L('x2404293600_traducir','seleccione tipo')"
        },
        "#ID_2025638983": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "9dp"
            }
        },
        "#ID_180804538": {
            "letra": "L('x1141589763','J')"
        },
        "#ID_1976570418": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "14dp"
            }
        },
        "#ID_1910964691": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "18dp"
            }
        },
        "#ID_627845946": {
            "hintText": "L('x2070103557_traducir','repita su correo')"
        },
        "#ID_1398127839": {
            "text": "L('x1027380240_traducir','Nombre')"
        },
        "#ID_831544830": {
            "text": "L('x2611428876_traducir','desde:')"
        },
        "#ID_252175981": {
            "texto": "L('x886429039_traducir','Tip: No puedes iniciar inspecciones sin confirmar tus tareas del dia')"
        },
        "#ID_1585064981": {
            "aceptar": "L('x1518866076_traducir','Aceptar')",
            "cancelar": "L('x2353348866_traducir','Cancelar')"
        },
        "#ID_1199623369": {
            "text": "L('x1903508534_traducir','Direccion')"
        },
        "titulo_niveles": {
            "color": "#8ce5bd",
            "font": {
                "fontWeight": "bold",
                "fontFamily": "SFUIText-Medium",
                "fontSize": "16dp"
            }
        },
        "#ID_1379951202": {
            "_section": "L('x4074624282_traducir','manana')"
        },
        "fondooscuro": {
            "color": "#878787",
            "backgroundColor": "#878787",
            "font": {
                "fontSize": "12dp"
            }
        },
        "#ID_277265460": {
            "desde": "L('x828196665','desde_horas[0]')"
        },
        "#ID_361801881": {
            "text": "L('x22218330_traducir','Confirma tus tareas de hoy')"
        },
        "#ID_1494646298": {
            "titulo": "L('x1571349115_traducir','NIVEL')",
            "cargando": "L('x1740321226_traducir','cargando ..')",
            "campo": "L('x2771220443_traducir','Nivel del Recinto')",
            "seleccione": "L('x2434264250_traducir','seleccione nivel')"
        },
        "#ID_612389820": {
            "title": "L('x2943883035_traducir','Guardar')"
        },
        "#ID_1846458254": {
            "title": "L('x197973218_traducir','Con Peaje')"
        },
        "#ID_1092739853": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "18dp"
            }
        },
        "#ID_1731330558": {
            "hintText": "L('x180503380_traducir','Apellido materno')"
        },
        "#ID_355178971": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "18dp"
            }
        },
        "#ID_1253308863": {
            "text": "L('x3210826773_traducir','Fecha Siniestro')"
        },
        "#ID_1025871597": {
            "text": "L('x1110802308_traducir','¿Estás dispuesto a viajar fuera de tu ciudad?')"
        },
        "#ID_1569098078": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "18dp"
            }
        },
        "fondoamarillo": {
            "backgroundColor": "#f8da54",
            "font": {
                "fontSize": "12dp"
            }
        },
        "#ID_1516441729": {
            "text": "L('x221750731_traducir','seleccione partida')"
        },
        "#ID_300054435": {
            "text": "L('x1045762689_traducir','Hora de inspeccion')"
        },
        "#ID_1877621755": {
            "text": "L('x3376426101_traducir','NO')"
        },
        "#ID_1860370347": {
            "titulo": "L('x1524107289_traducir','CONTINUAR')"
        },
        "#ID_1088806219": {
            "titulo": "L('x404129330_traducir','¿EL ASEGURADO CONFIRMA ESTOS DATOS?')",
            "si": "L('x1723413441_traducir','SI, Están correctos')",
            "texto": "L('x2083765231_traducir','El asegurado debe confirmar que los datos de esta sección están correctos')",
            "pantalla": "L('x2851883104_traducir','¿ESTA DE ACUERDO?')",
            "no": "L('x55492959_traducir','NO, Hay que modificar algo')"
        },
        "#ID_1324277593": {
            "titulo": "L('x591862035_traducir','Pavimentos')",
            "cargando": "L('x1740321226_traducir','cargando ..')",
            "hint": "L('x2600368035_traducir','Seleccione pavimentos')",
            "subtitulo": "L('x4011106049_traducir','Indique los tipos')"
        },
        "#ID_374340032": {
            "titulo": "L('x2828751865_traducir','¡ESTAS SIN CONEXION!')",
            "mensaje": "L('x3528440528_traducir','No puedes ver las tareas entrantes')"
        },
        "#ID_609299033": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "9dp"
            }
        },
        "#ID_192107377": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "9dp"
            }
        },
        "#ID_598691919": {
            "on": "L('x3308524262','registro.d5')"
        },
        "#ID_1573327234": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "15dp"
            }
        },
        "#ID_1345051066": {
            "titulo": "L('x2266302645_traducir','Cubierta')",
            "cargando": "L('x1740321226_traducir','cargando ..')",
            "hint": "L('x2134385782_traducir','Seleccione cubiertas')",
            "subtitulo": "L('x4011106049_traducir','Indique los tipos')"
        },
        "#ID_154930557": {
            "hintText": "L('x1731734927_traducir','Escribir nombre')"
        },
        "#ID_1599217179": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "18dp"
            }
        },
        "#ID_1056584802": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "18dp"
            }
        },
        "#ID_1944128176": {
            "title": "L('x2004545330_traducir','CARACTERISTICAS')"
        },
        "#ID_363605705": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "20dp"
            }
        },
        "#ID_1734985585": {
            "text": "L('x4100297395_traducir','Ancho (mt)')"
        },
        "#ID_271213064": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "16dp"
            }
        },
        "#ID_1996770915": {
            "titulo": "L('x1629775439_traducir','MONEDAS')",
            "cargando": "L('x1740321226_traducir','cargando ..')",
            "campo": "L('x3081186843_traducir','Moneda')",
            "seleccione": "L('x2547889144','-')"
        },
        "#ID_1019357907": {
            "text": "L('x3376426101_traducir','NO')"
        },
        "#ID_1379917834": {
            "titulo": "L('x3952572309_traducir','SINIESTRO')",
            "cargando": "L('x1740321226_traducir','cargando ..')",
            "campo": "L('x2004291629_traducir','Tipo de siniestro')",
            "seleccione": "L('x4123108455_traducir','seleccione tipo')"
        },
        "estilo1": {
            "color": "#4d4d4d",
            "font": {
                "fontSize": "15dp"
            }
        },
        "#ID_1213816900": {
            "title": "L('x2943883035_traducir','Guardar')"
        },
        "#ID_1264274795": {
            "headerTitle": "L('x4074624282_traducir','manana')"
        },
        "#ID_1642423363": {
            "hintText": "L('x3229948791_traducir','escribir nivel 3')"
        },
        "#ID_1741978890": {
            "titulo": "L('x1291961897_traducir','PARTE 5: Contactos')",
            "avance": "L('x4674461_traducir','5/6')"
        },
        "#ID_458987331": {
            "hintText": "L('x702987842_traducir','escribir nivel 5')"
        },
        "#ID_492538423": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "9dp"
            }
        },
        "#ID_1167041456": {
            "titulo": "L('x8960895_traducir','GUARDAR DAÑOS')"
        },
        "#ID_1494714000": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "15dp"
            }
        },
        "#ID_1311514463": {
            "data": "L('x1872543855','data_region')"
        },
        "#ID_1900942475": {
            "text": "L('x3534566262_traducir','Respaldos de Adquisición de Bienes Afectados (Facturas, Boletas, Cartolas de Casas Comerciales, Catálogos, Fotos)')"
        },
        "#ID_1448197759": {
            "texto": "L('x1422333761_traducir','Tienes una capacidad maxima de tareas por dia pero aun puedes tomar en otros dias')"
        },
        "#ID_1889836297": {
            "letra": "L('x543223747','S')"
        },
        "#ID_908004102": {
            "text": "L('x2451619465_traducir','status: esperando')"
        },
        "#ID_454616829": {
            "text": "L('x2721945877_traducir','ENVIANDO INSPECCIONES')"
        },
        "#ID_304530978": {
            "title": "L('x271733139_traducir','MIS TAREAS')"
        },
        "estilo4": {
            "color": "#4d4d4d",
            "font": {
                "fontSize": "16dp"
            }
        },
        "#ID_1133039142": {
            "titulo": "L('x3969906465_traducir','ESPERA TU LLAMADA')",
            "texto": "L('x1275032235_traducir','Recuerda llevar a la entrevista tu CV, tu certificado de título y referencias')"
        },
        "#ID_1529557361": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "9dp"
            }
        },
        "#ID_360177492": {
            "text": "L('x2458576127_traducir','Especificaciones Técnicas de la Construcción')"
        },
        "#ID_986864643": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "15dp"
            }
        },
        "#ID_77208369": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "9dp"
            }
        },
        "#ID_1643788099": {
            "title": "L('x2146494644_traducir','ENROLAMIENTO')"
        },
        "#ID_2075141137": {
            "text": "L('x3376426101_traducir','NO')"
        },
        "#ID_1470293663": {
            "titulo": "L('x767609104_traducir','RECINTOS')",
            "cargando": "L('x1740321226_traducir','cargando ..')",
            "campo": "L('x382177638_traducir','Ubicación')",
            "seleccione": "L('x4060681104_traducir','recinto')"
        },
        "#ID_105146259": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "9dp"
            }
        },
        "#ID_1402173038": {
            "text": "L('x939625677_traducir','Otros (Especificar)')"
        },
        "#ID_1698705043": {
            "pantalla": "L('x1805186499_traducir','contenidos')"
        },
        "#ID_1164137706": {
            "pantalla": "L('x3174879261_traducir','recintos')"
        },
        "#ID_1277888221": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "8dp"
            }
        },
        "#ID_2071361111": {
            "pantalla": "L('x1828862638_traducir','basicos')"
        },
        "#ID_1255658089": {
            "text": "L('x2087500331_traducir','Edita cada item independientemente')"
        },
        "#ID_1744942265": {
            "text": "L('x3964605301_traducir','Part time')"
        },
        "#ID_1968209485": {
            "text": "L('x1820953783_traducir','ASEGURATE DE ENVIAR LA INSPECCION TERMINADA EN TU PERFIL')"
        },
        "desc": {
            "color": "#838383",
            "font": {
                "fontFamily": "SFUIText-Medium",
                "fontSize": "11dp"
            }
        },
        "#ID_1011874534": {
            "text": "L('x2451619465_traducir','status: esperando')"
        },
        "#ID_2107155773": {
            "titulo": "L('x1867465554_traducir','MARCAS')",
            "cargando": "L('x1740321226_traducir','cargando ..')",
            "campo": "L('x3837495059_traducir','Marca del Item')",
            "seleccione": "L('x1021431138_traducir','seleccione marca')"
        },
        "#ID_824073784": {
            "text": "L('x4144438243_traducir','Acepta desistir la realización de esta inspección')"
        },
        "#ID_802982462": {
            "title": "L('x2943883035_traducir','Guardar')"
        },
        "#ID_549278296": {
            "title": "L('x2943883035_traducir','Guardar')"
        },
        "#ID_670811732": {
            "title": "L('x1633918740_traducir','Sin Peaje')"
        },
        "#ID_1245501080": {
            "text": "L('x2760387473_traducir','Alto (mt)')"
        },
        "#ID_860719784": {
            "title": "L('x2726960050_traducir','EDITAR')"
        },
        "#ID_1507772706": {
            "titulo": "L('x404129330_traducir','¿EL ASEGURADO CONFIRMA ESTOS DATOS?')",
            "si": "L('x1723413441_traducir','SI, Están correctos')",
            "texto": "L('x2083765231_traducir','El asegurado debe confirmar que los datos de esta sección están correctos')",
            "pantalla": "L('x2851883104_traducir','¿ESTA DE ACUERDO?')",
            "no": "L('x55492959_traducir','NO, Hay que modificar algo')"
        },
        "#ID_1458019245": {
            "text": "L('x3573009323_traducir','Repetir teléfono')"
        },
        "#ID_966235729": {
            "headerTitle": "L('x3424088795_traducir','ref1')"
        },
        "#ID_1346364327": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "9dp"
            }
        },
        "#ID_1189437781": {
            "label1": "L('x724488137_traducir','Fachada')",
            "label4": "L('x676841594_traducir','Nº Depto')",
            "label3": "L('x4076266664_traducir','Numero')",
            "label2": "L('x2825989175_traducir','Barrio')"
        },
        "#ID_142790127": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "9dp"
            }
        },
        "#ID_1139316662": {
            "hintText": "L('x1599456288_traducir','Ingrese correo')"
        },
        "estilo8_1": {
            "color": "#ee7f7e",
            "font": {
                "fontFamily": "SFUIText-Medium",
                "fontSize": "15dp"
            }
        },
        "#ID_117825709": {
            "title": "L('x902724175_traducir','EDITAR NIVEL')"
        },
        "#ID_1557692345": {
            "text": "L('x3253144191_traducir','telefono')"
        },
        "#ID_49063077": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "20dp"
            }
        },
        "fondoplomo": {
            "backgroundColor": "#f7f7f7",
            "font": {
                "fontSize": "12dp"
            }
        },
        "#ID_1168497178": {
            "text": "L('x4060777286_traducir','INSTALACIONES')"
        },
        "#ID_1195415339": {
            "titulo": "L('x3752633607_traducir','PARTE 4: Disponibilidad de trabajo')",
            "avance": "L('x25508266_traducir','4/6')"
        },
        "#ID_1521020064": {
            "hintText": "L('x1054459199_traducir','Escribir nivel 5')"
        },
        "bt_guardar": {
            "color": "#ffffff",
            "font": {
                "fontSize": "18dp"
            }
        },
        "#ID_459542007": {
            "text": "L('x1046761583_traducir','12-03-1989')"
        },
        "#ID_1199702717": {
            "hintText": "L('x4108050209','0')"
        },
        "#ID_1474425081": {
            "text": "L('x1242435453_traducir','La direccion es correcta?')"
        },
        "#ID_1935931444": {
            "text": "L('x2564648986_traducir','caso')"
        },
        "#ID_1911446078": {
            "titulo": "L('x3679463181_traducir','FINALIZAR')"
        },
        "#ID_809722670": {
            "text": "L('x1951430381_traducir','Piso')"
        },
        "#ID_1377483554": {
            "hintText": "L('x1125245_traducir','repita su teléfono')"
        },
        "#ID_664324972": {
            "text": "L('x3652084882_traducir','RUT del presente')"
        },
        "#ID_1132173110": {
            "letra": "L('x2909332022','L')"
        },
        "#ID_1547626400": {
            "text": "L('x1411564115_traducir','Agrega cada nivel')"
        },
        "#ID_1108738654": {
            "text": "L('x4117795806_traducir','Superficie (m2)')"
        },
        "#ID_1082890408": {
            "title": "L('x3835609072_traducir','HOY')"
        },
        "bg_progreso_verde": {
            "backgroundColor": "#33cc7f",
            "font": {
                "fontSize": "12dp"
            }
        },
        "#ID_411389157": {
            "title": "L('x3218612972_traducir','DETALLES DE TAREA')"
        },
        "#ID_800868967": {
            "text": "L('x2719983611_traducir','seleccione producto')"
        },
        "#ID_1012442242": {
            "titulo": "L('x1197985134_traducir','SALIR')"
        },
        "#ID_1430936766": {
            "text": "L('x1380747035_traducir','Construcción Anexa')"
        },
        "#ID_1524450495": {
            "headerTitle": "L('x3424088795_traducir','ref1')"
        },
        "estilo6": {
            "color": "#4d4d4d",
            "font": {
                "fontFamily": "SFUIText-Medium",
                "fontSize": "16dp"
            }
        },
        "#ID_1085005246": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "9dp"
            }
        },
        "#ID_475316297": {
            "text": "L('x1375988868_traducir','?%')"
        },
        "fondoazul": {
            "backgroundColor": "#2d9edb",
            "font": {
                "fontSize": "12dp"
            }
        },
        "#ID_1986537543": {
            "text": "L('x2270754326_traducir','RUT del asegurado')"
        },
        "#ID_218695237": {
            "titulo": "L('x1524107289_traducir','CONTINUAR')"
        },
        "#ID_2066895374": {
            "titulo": "L('x3180818344_traducir','SELECCIONE REGION')",
            "campo": "L('x147780672_traducir','Region')",
            "seleccione": "L('x677075728_traducir','seleccione region')"
        },
        "#ID_1716158756": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "18dp"
            }
        },
        "#ID_392712350": {
            "text": "L('x3920081714_traducir','Repetir correo')"
        },
        "estilo12": {
            "color": "#000000",
            "font": {
                "fontSize": "18dp"
            }
        },
        "#ID_310021856": {
            "title": "L('x1649875133_traducir','HISTORIAL')"
        },
        "#ID_1425483843": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "12dp"
            }
        },
        "#ID_381163": {
            "text": "L('x123080099_traducir','E-Mail')"
        },
        "#ID_782701911": {
            "text": "L('x1890062079_traducir','Correo')"
        },
        "#ID_1692919316": {
            "text": "L('x2558236558_traducir','Largo (mt)')"
        },
        "#ID_653609177": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "15dp"
            }
        },
        "#ID_102543267": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "18dp"
            }
        },
        "#ID_59157151": {
            "headerTitle": "L('x3424088795_traducir','ref1')"
        },
        "#ID_1900075988": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "20dp"
            }
        },
        "#ID_1785851628": {
            "pantalla": "L('x1828862638_traducir','basicos')"
        },
        "#ID_1570885865": {
            "text": "L('x3399771609','17.765.374-8')"
        },
        "#ID_1965774674": {
            "text": "L('x2354316101_traducir','Teléfono')"
        },
        "#ID_1024942406": {
            "hintText": "L('x397976520_traducir','Describa el daño')"
        },
        "#ID_1427451570": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "20dp"
            }
        },
        "#ID_2094008726": {
            "text": "L('x2443017383_traducir','Fecha de nacimiento')"
        },
        "#ID_1240816567": {
            "valor": "L('x342344098','evento.valores')"
        },
        "#ID_1079731371": {
            "_section": "L('x2345059420','fecha_entrantes')"
        },
        "#ID_564060634": {
            "text": "L('x649484684_traducir','Items de daño')"
        },
        "rojo": {
            "color": "#ee7f7e",
            "font": {
                "fontWeight": "bold",
                "fontFamily": "SFUIText-Medium",
                "fontSize": "16dp"
            }
        },
        "#ID_1648413954": {
            "images": ["/images/i21578740E736331F7BDC9D452BD712C9.png", "/images/iEB558FE382408A66B62F79BCD08E87DE.png", "/images/iE4617B7080958F45BF2C192CC042382D.png", "/images/i9290A0A895DCA701657AA3A2701BC5B1.png", "/images/iC844F1FD37D49F099C8E19CDD3168A41.png", "/images/iD7DACAF52A6FE787F42E337F896ED7E5.png", "/images/i77B96376A21E9E530BF99DEF0E191F24.png", "/images/i1A8982F638B1931C2A37ED5E826C17C0.png", "/images/i88E908455A225B9390E3678E9C19FBA2.png", "/images/iE9355204576B1FD1BB3F1F0E435BFF6D.png", "/images/i0E4650BFDDB114F81A51C600521AC2D7.png", "/images/i867483CAEDC8D77255A7F25A15625CA5.png", "/images/i2175C7E985066E9DBB5FD1E204BDED5D.png", "/images/i19F89DC20905F566DA7CCDBE900B1828.png", "/images/i6755383116D1A5AD862D47B6D5554641.png", "/images/i1391131C784CF99686D19D65AC20986E.png", "/images/i40A27D206CA23815C331AB71422629F2.png", "/images/i38E6B9D378262B4EF35387DC47BBF31D.png", "/images/i988F7B1016AF1A4927CD7ED9AE8AB990.png", "/images/i405EC5CB58F536363712327BE6376BB1.png", "/images/i46769D88018418464C9F4B24D384CB8B.png", "/images/i3F6BAF33AC1E4011205917185EA98CAD.png", "/images/i42D15D48B2ED837F7903FE534A06494B.png", "/images/iBC7C1AA388608962A1F7B8C61BD6DA82.png", "/images/iF09D8144E898AB6F35D2FF363EE98F9E.png", "/images/i077D23D9EA5866B0EF5E25614919665F.png", "/images/iA83DD447D387E7698AB46331B8A8D019.png", "/images/i21932839113ADBF836C5D8C8B4B11AEB.png", "/images/i9BDA9EE2558DFE3F2125992BECA4278D.png", "/images/i9AC6E8C97F63FEB0A46CCD655AC44431.png", "/images/iE28ABABF9CABE7255711922BBF55B983.png", "/images/i0A41ED8B1E443429769FFB49137003C2.png", "/images/i388C5A5B6FEC1AAA9E0FED2BE6842A35.png", "/images/iBB18FBADFC2D527AD2709E8652AAFD5A.png", "/images/i10063715BD30D221415EF53335926168.png", "/images/i359C4AC08A6FB6D5912E5ECA8ABBB00A.png", "/images/i0E59AE058F689EF3F4FAD9FDDF470974.png", "/images/i54701C93E11CA7BAF12DF89AABF2AE04.png", "/images/i9AE08E1C51AF160F89D84F868261FCFD.png", "/images/i585BBC1586441528C908AD41EB8EBDBD.png", "/images/i10BF85AEDE9F2E6087E8EECF917665A8.png", "/images/iE50DF32C64581FCE5856C9454B2D58E2.png", "/images/iBFDCFF2749FF3ACE5A75BF05C2C1E25C.png", "/images/i89096AB8DBE818E384384C292C89811F.png", "/images/i0BEBD7212FCEADD8E81C2812FD68C36D.png", "/images/i1B954A4FDCFE0860F6D3EBC4D01D2F19.png", "/images/i304ED678B67AD49378DB6228C609BE45.png", "/images/iBEB7E7D434C1667889663859EAE75D92.png", "/images/iF714D356682197276705B90C40BA8A03.png", "/images/iA1B6778946A3A0358E419143E8081D2B.png", "/images/i00760138B01D3550976723B1B225C943.png", "/images/iBAAC1AF775CEE1B716B575455DB68FDC.png", "/images/i3CD6AA1281B7B4ED5F37ED76FCC2A808.png", "/images/i20769EDB2A387AD7E63BFEE1ADB6E01A.png", "/images/iDAED2AF86C310E481C7409143758ECB6.png", "/images/i9B1B0BD5A88BDAB459D66F68B80309F5.png", "/images/i0DE060270629150E8A87173EDB8B79F7.png", "/images/i380DF6EF85EB2185DE1A501FEE66371A.png", "/images/iA152F49A26EC92286520C265AABD8CFC.png", "/images/i154CF2D0DF49584435B369E991104CD0.png", "/images/iB6A8B660E26087802754C713C196B739.png", "/images/iD0A8DD7F8257D87CADD4557D2DDE27A0.png", "/images/iF0D35F95D8CF565D84F4B2EABC07C0C3.png", "/images/iAC8C1B1E912288D6EF52DC4C71B79944.png", "/images/i396F93C0F37490C405609068F53808DE.png", "/images/i28D1EA7CF68179295028D9F1DC787292.png", "/images/iF3F5D57B22CF688DF19FE14CCD909402.png", "/images/i399EE8FDF222FDA603DF0C82E0BCCFAC.png", "/images/i09C63C089B2E569672618ED5FC69504C.png", "/images/i6EA1D3660FE93F229A4DD0E034A728EB.png", "/images/iDFEBE4EC019BCE94F6EC5853A3BFABF5.png", "/images/iAC27C6E0D5FAF36E89B0E75BD07AEB87.png", "/images/i273522DAAED644BC07219E8653954EE3.png", "/images/i86B7E95443CEBED206372766EAF4F59C.png", "/images/i7E06C884017083155A6857A40DCF4DE3.png", "/images/i691DBFC540A724998A2353C7D6DC199F.png", "/images/iD182D4C677A7FEF7B7F9D2021F901A9C.png", "/images/i35A05BBCDD161E2A6C0C478F703901C0.png", "/images/iCF6F841564C2B00839FA0338556EE25E.png", "/images/iE34B2EECFDC74FC20A5DEA25E204DB7A.png", "/images/i15B71869941D9D056D2771715265227F.png", "/images/i8A0B707451CCBE122A5B28F640716AF1.png", "/images/i88CB92AA112DA9B4D3913374D31E5489.png", "/images/i7FDF4568F230AA5A4D3B701708D1E8D6.png", "/images/i69B8A04A79F91820581E4D3708B7D003.png", "/images/i55288AB1A85C7038BCA8AD843ADF586F.png", "/images/i57C1B3043D0424531659720F9C32B1C2.png", "/images/iDAA2A0A70B3960D02C55DDF038216353.png", "/images/iECBF5EDE5417D57D71BD067290909C22.png", "/images/i10CCB6F0BFED76841F1607FA34C062B1.png", "/images/iF8B6DC3340FEC020058F7C743ADA0098.png", "/images/i7F73828276786D80F13715BB5BFB4CCC.png", "/images/i68421A3D08C3B75227181E0EC9EBCE42.png", "/images/i18ACDDC89C81835BFB74D3A0E71D8C10.png", "/images/i5A8E6E8D34ABB47533165D506B7E1462.png", "/images/i9AD38C8643DBCC036D0D226508F2B971.png", "/images/iA7B0C6D62FD940727B9D5DBBD4165324.png", "/images/i51313033EE92F99494C0CEF93F77F8D9.png", "/images/i26C8BDFC1DEA0C67358C293A353C327B.png", "/images/iF086F6FE7A1868188FD09EE6FA03485B.png", "/images/i9C4EAA87C9F69FE64BE240DAE158CF0E.png", "/images/i47EFAC4DEC72BFED1EAD8D5621ECF913.png", "/images/i96D645D648D27F12B2E4F54E9503E8DF.png", "/images/i6394513CA228CCB57ED602297C9422BC.png", "/images/i6CD686DC5A11FB94CBB2A09FC90DB6EF.png", "/images/iEBD4EE843BEEB6E225C25387B7DCF30E.png", "/images/iB0DCEF19023ADDE9B38C97AF31540D4D.png", "/images/i5907CCF7C26ECC19EAD231C90A4FAB9D.png", "/images/i8143DE4F0A77BC1F4111FF6A02EED95E.png", "/images/iDD2C93752E1169F8D71892A477C85A11.png", "/images/i661E5F5296ABB7AB4B0E5D236E6C7630.png", "/images/iCCB062F45E0D9D917C617FDB2EA1E357.png", "/images/i7DF846739C1FD71098DE3902990DA8A9.png", "/images/iAD45960BC2841EC6E2D86283A2F33E96.png", "/images/i6AF96FB60F20F611E16F44E4A4C366B4.png", "/images/iBBA0A82C21EA239F79E171AC51EA7B95.png", "/images/i2F569FBCA8D42EB735DED2831286DC66.png", "/images/iE02AFE3817F8B969AEAACF7456E25AB5.png", "/images/iAE28EA1830BF41F55AD84813E8F1D056.png", "/images/i870DC5E31CA825DAEE1AD00C6FB1D68A.png"]
        },
        "#ID_1108427052": {
            "pantalla": "L('x1828862638_traducir','basicos')"
        },
        "#ID_784741595": {
            "headerTitle": "L('x1916403066_traducir','hoy')"
        },
        "#ID_1282712073": {
            "pantalla": "L('x3287362058_traducir','frequeridas')"
        },
        "#ID_1637360044": {
            "text": "L('x2067448595_traducir','Cancelando Inspección, espere ..')"
        },
        "#ID_1954632612": {
            "text": "L('x2442032847_traducir','nivel 5')"
        },
        "#ID_1893667930": {
            "titulo": "L('x1219835481_traducir','Muros / Tabiques')",
            "cargando": "L('x1740321226_traducir','cargando ..')",
            "hint": "L('x2879998099_traducir','Seleccione muros')",
            "subtitulo": "L('x4011106049_traducir','Indique los tipos')"
        },
        "#ID_687615275": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "14dp"
            }
        },
        "#ID_854180539": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "20dp"
            }
        },
        "#ID_1929710970": {
            "titulo": "L('x1524107289_traducir','CONTINUAR')"
        },
        "#ID_1280279961": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "20dp"
            }
        },
        "#ID_395419113": {
            "titulo": "L('x542998687_traducir','¡FELICIDADES EN TERMINAR CON EXITO TU INSPECCION!')",
            "texto": "L('x1072203252_traducir','Tu inspección será recibida por nosotros, gracias por tu trabajo.')"
        },
        "#ID_557564254": {
            "text": "L('x2301231272_traducir','Ciudad')"
        },
        "#ID_723947171": {
            "_section": "L('x27834329_traducir','fecha')"
        },
        "#ID_1562025293": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "14dp"
            }
        },
        "#ID_1380638210": {
            "titulo": "L('x1975271086_traducir','Estructura Soportante')",
            "cargando": "L('x1740321226_traducir','cargando ..')",
            "hint": "L('x2898603391_traducir','Seleccione estructura')",
            "subtitulo": "L('x4011106049_traducir','Indique los tipos')"
        },
        "#ID_1882867077": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "18dp"
            }
        },
        "#ID_1089146397": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "15dp"
            }
        },
        "#ID_1042589932": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "18dp"
            }
        },
        "#ID_2133014266": {
            "pantalla": "L('x3294048930_traducir','documentos')"
        },
        "#ID_1183134912": {
            "headerTitle": "L('x2819214932_traducir','holi1')"
        },
        "#ID_1831593863": {
            "text": "L('x3381833431_traducir','Documentos solicitados al asegurado')"
        },
        "#ID_414434092": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "14dp"
            }
        },
        "#ID_813584692": {
            "text": "L('x1878155044_traducir','Escritura de Compraventa')"
        },
        "#ID_790501879": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "18dp"
            }
        },
        "#ID_1990255213": {
            "titulo": "L('x591862035_traducir','Pavimentos')",
            "cargando": "L('x1740321226_traducir','cargando ..')",
            "hint": "L('x2600368035_traducir','Seleccione pavimentos')",
            "subtitulo": "L('x4011106049_traducir','Indique los tipos')"
        },
        "#ID_1992645203": {
            "text": "L('x267051884_traducir','nivel 2')"
        },
        "#ID_887479914": {
            "text": "L('x2854409003_traducir','días')"
        },
        "#ID_1537130957": {
            "text": "L('x1756503588_traducir','Descripción del daño')"
        },
        "#ID_1966667414": {
            "pantalla": "L('x3294048930_traducir','documentos')"
        },
        "#ID_175663411": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "9dp"
            }
        },
        "#ID_1415795757": {
            "titulo": "L('x1524107289_traducir','CONTINUAR')"
        },
        "#ID_1565023908": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "16dp"
            }
        },
        "#ID_1471609178": {
            "aceptar": "L('x1518866076_traducir','Aceptar')",
            "cancelar": "L('x2353348866_traducir','Cancelar')"
        },
        "#ID_1165483591": {
            "text": "L('x3999754113_traducir','Fono movil')"
        },
        "estilo10": {
            "color": "#ffffff",
            "font": {
                "fontWeight": "bold",
                "fontSize": "16dp"
            }
        },
        "#ID_783513222": {
            "texto": "L('x2695983055_traducir','PRESIONE PARA ACEPTAR')"
        },
        "#ID_1751881857": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "18dp"
            }
        },
        "#ID_1308259195": {
            "hintText": "L('x526694301_traducir','Ingrese su teléfono')"
        },
        "mini": {
            "color": "#333333",
            "font": {
                "fontFamily": "SFUIText-Medium",
                "fontSize": "10dp"
            }
        },
        "#ID_192697022": {
            "text": "L('x574786063_traducir','Editar Contactos')"
        },
        "#ID_198296088": {
            "title": "L('x3046757478_traducir','DATOS BASICOS')"
        },
        "#ID_238268657": {
            "titulo": "L('x4266405801_traducir','Profesion')",
            "campo": "L('x419737788_traducir','Oficio / Profesion')",
            "seleccione": "L('x1243610977_traducir','Seleccione su oficio/profesión')"
        },
        "#ID_1553498091": {
            "text": "L('x3065475174_traducir','Contactos')"
        },
        "#ID_1125701114": {
            "texto": "L('x1739881219_traducir','Tip: Nunca dejes una tarea sin inspeccionar. El no hacerlo te afecta directamente')",
            "bottom": "L('x3693793700_traducir','40')",
            "tipo": "L('x99131830','_tip')"
        },
        "#ID_1393871432": {
            "title": "L('x767609104_traducir','RECINTOS')"
        },
        "fondoblanco": {
            "backgroundColor": "#ffffff",
            "font": {
                "fontSize": "12dp"
            }
        },
        "estilo8_3": {
            "color": "#ee7f7e",
            "font": {
                "fontFamily": "SFUIText-Medium",
                "fontSize": "9dp"
            }
        },
        "#ID_1904184926": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "9dp"
            }
        },
        "#ID_1375870021": {
            "text": "L('x2486765391_traducir','Contenidos')"
        },
        "#ID_1407323156": {
            "title": "L('x2578278336_traducir','NUEVO NIVEL')"
        },
        "#ID_296073555": {
            "titulo": "L('x4058536315_traducir','ENVIAR FORMULARIO')"
        },
        "#ID_432985265": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "9dp"
            }
        },
        "#ID_770156022": {
            "headerTitle": "L('x3424088795_traducir','ref1')"
        },
        "#ID_1289130430": {
            "text": "L('x2905032041_traducir','MANTENER PARA INICIAR')"
        },
        "#ID_1023458367": {
            "hintText": "L('x1591840468_traducir','escribir nivel 4')"
        },
        "#ID_582453330": {
            "pantalla": "L('x3181120844_traducir','hayalguien')"
        },
        "#ID_1863770628": {
            "text": "L('x1350052795_traducir','Definir % de daños')"
        },
        "#ID_1512316616": {
            "titulo": "L('x2828751865_traducir','¡ESTAS SIN CONEXION!')",
            "mensaje": "L('x1855928898_traducir','No puedes ver las tareas de emergencias de hoy')"
        },
        "#ID_1736335037": {
            "text": "L('x1626050066_traducir','Asegúrate de haber ingresado todos los recintos')"
        },
        "#ID_1622772489": {
            "text": "L('x4100297395_traducir','Ancho (mt)')"
        },
        "#ID_1015210318": {
            "hintText": "L('x3041923657_traducir','Escribir direccion')"
        },
        "#ID_1631664749": {
            "text": "L('x318795834_traducir','Full time')"
        },
        "estilo14n": {
            "color": "#666666",
            "font": {
                "fontWeight": "bold",
                "fontSize": "15dp"
            }
        },
        "titulo_danos": {
            "color": "#b9aaf3",
            "font": {
                "fontWeight": "bold",
                "fontFamily": "SFUIText-Medium",
                "fontSize": "16dp"
            }
        },
        "#ID_1401951081": {
            "_section": "L('x3690817388','manana_entrantes')"
        },
        "#ID_416876314": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "18dp"
            }
        },
        "#ID_375599278": {
            "title": "L('x2726960050_traducir','EDITAR')"
        },
        "#ID_1477085419": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "14dp"
            }
        },
        "#ID_1550780670": {
            "text": "L('x742992573_traducir','meses')"
        },
        "#ID_1871422952": {
            "titulo": "L('x1866523485_traducir','Estruct. cubierta')",
            "cargando": "L('x1740321226_traducir','cargando ..')",
            "hint": "L('x2460890829_traducir','Seleccione e.cubiertas')",
            "subtitulo": "L('x4011106049_traducir','Indique los tipos')"
        },
        "#ID_22055519": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "9dp"
            }
        },
        "#ID_463934795": {
            "titulo": "L('x4235921061_traducir','PARTE 6: Experiencia de trabajo')",
            "avance": "L('x33680836_traducir','6/6')"
        },
        "#ID_676840445": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "12dp"
            }
        },
        "#ID_886082434": {
            "texto": "L('x990554165_traducir','PRESIONE PARA VERIFICAR')"
        },
        "#ID_1059906457": {
            "title": "L('x2146494644_traducir','ENROLAMIENTO')"
        },
        "#ID_1948652003": {
            "text": "L('x3279325126_traducir','Apellido Paterno')"
        },
        "#ID_1352818624": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "9dp"
            }
        },
        "#ID_2143248160": {
            "titulo": "L('x1524107289_traducir','CONTINUAR')"
        },
        "#ID_1614118096": {
            "titulo": "L('x31606107_traducir','Editar Disponibilidad de trabajo')"
        },
        "#ID_763798704": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "9dp"
            }
        },
        "#ID_1413999243": {
            "titulo": "L('x1384515306_traducir','TIPO DAÑO')",
            "cargando": "L('x1740321226_traducir','cargando ..')",
            "campo": "L('x953397154_traducir','Tipo de Daño')",
            "seleccione": "L('x4123108455_traducir','seleccione tipo')"
        },
        "#ID_25124347": {
            "title": "L('x2728236766_traducir','PORTADA')"
        },
        "#ID_1007469789": {
            "titulo": "L('x1678967761_traducir','GUARDAR RECINTOS')"
        },
        "#ID_1066820610": {
            "text": "L('x3313466056_traducir','Apellido Materno')"
        },
        "#ID_1523341755": {
            "titulo": "L('x2349703141_traducir','NO TIENES TAREAS')",
            "texto": "L('x2279881512_traducir','Asegurate de tomar tareas para hoy y revisar tu ruta')"
        },
        "#ID_1581748382": {
            "text": "L('x1110802308_traducir','¿Estás dispuesto a viajar fuera de tu ciudad?')"
        },
        "bg_progreso_naranjo": {
            "backgroundColor": "#f2a566",
            "font": {
                "fontSize": "12dp"
            }
        },
        "fondonaranjo": {
            "backgroundColor": "#fcbd83",
            "font": {
                "fontSize": "12dp"
            }
        },
        "#ID_648162266": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "15dp"
            }
        },
        "#ID_194255918": {
            "text": "L('x3376426101_traducir','NO')"
        },
        "#ID_1473909169": {
            "hintText": "L('x4108050209','0')"
        },
        "#ID_1488618388": {
            "title": "L('x2146494644_traducir','ENROLAMIENTO')"
        },
        "#ID_1424138904": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "9dp"
            }
        },
        "#ID_961321407": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "15dp"
            }
        },
        "#ID_1804952842": {
            "text": "L('x1960877763_traducir','NombreAsegurado')"
        },
        "#ID_181885186": {
            "text": "L('x3990391233_traducir','Usuario')"
        },
        "#ID_2107803798": {
            "text": "L('x2067448595_traducir','Cancelando Inspección, espere ..')"
        },
        "#ID_1770730105": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "14dp"
            }
        },
        "#ID_735641899": {
            "text": "L('x1431676631_traducir','Certificado de Dominio Vigente Actualizado')"
        },
        "#ID_1398061098": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "18dp"
            }
        },
        "#ID_1673669563": {
            "hintText": "L('x2113698382_traducir','ingrese contraseña')"
        },
        "#ID_1777301047": {
            "text": "L('x3327316334_traducir','PRESIONA PARA CONFIRMAR')"
        },
        "#ID_160905390": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "9dp"
            }
        },
        "#ID_132453779": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "9dp"
            }
        },
        "#ID_1048127024": {
            "text": "L('x1719427472_traducir','Partida')"
        },
        "#ID_132596446": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "9dp"
            }
        },
        "#ID_1548647422": {
            "text": "L('x503985650_traducir','Cantidad Items')"
        },
        "#ID_2059092140": {
            "text": "L('x529917922_traducir','¿Estás dispuesto a viajar fuera de tu país?')"
        },
        "#ID_1792027403": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "9dp"
            }
        },
        "#ID_468683248": {
            "text": "L('x3109184006_traducir','Nombre del item')"
        },
        "#ID_1271284106": {
            "headerTitle": "L('x3162342996_traducir','ref4')"
        },
        "#ID_1590250895": {
            "pantalla": "L('x887781275_traducir','detalle')"
        },
        "#ID_1481453019": {
            "nivel1": "L('x3264699902','pais.label_nivel1')"
        },
        "progreso": {
            "color": "#ffffff",
            "font": {
                "fontWeight": "bold",
                "fontSize": "14dp"
            }
        },
        "#ID_321122430": {
            "titulo": "L('x1975271086_traducir','Estructura Soportante')",
            "cargando": "L('x1740321226_traducir','cargando ..')",
            "hint": "L('x2898603391_traducir','Seleccione estructura')",
            "subtitulo": "L('x4011106049_traducir','Indique los tipos')"
        },
        "bg_progreso_morado": {
            "backgroundColor": "#9e8ff2",
            "font": {
                "fontSize": "12dp"
            }
        },
        "#ID_1528282890": {
            "hintText": "L('x1817984376_traducir','Repita su correo')"
        },
        "fondoplomo2": {
            "backgroundColor": "#e6e6e6",
            "font": {
                "fontSize": "12dp"
            }
        },
        "#ID_1907563700": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "15dp"
            }
        },
        "#ID_823445728": {
            "text": "L('x2089188151_traducir','Detallar experiencia (Quedan 140 caracteres)')"
        },
        "#ID_1712023120": {
            "hintText": "L('x3329282304_traducir','Escriba nombre del recinto')"
        },
        "#ID_2036699456": {
            "title": "L('x354890761_traducir','DOCUMENTOS')"
        },
        "#ID_2142873717": {
            "letra": "L('x2746444292','D')"
        },
        "#ID_108765079": {
            "texto": "L('x1693196576_traducir','Tip: No llames a horas imprudentes y mantén siempre una buena actitud')"
        },
        "#ID_1582745917": {
            "text": "L('x982552870_traducir','nombre')"
        },
        "#ID_1376055740": {
            "text": "L('x993337110_traducir','ENVIAR TODOS')"
        },
        "#ID_1029806018": {
            "text": "L('x3376426101_traducir','NO')"
        },
        "#ID_1453220671": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "9dp"
            }
        },
        "estilo2": {
            "color": "#a0a1a3",
            "font": {
                "fontFamily": "SFUIText-Light",
                "fontSize": "12dp"
            }
        },
        "#ID_211809593": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "14dp"
            }
        },
        "#ID_1816083179": {
            "text": "L('x2558236558_traducir','Largo (mt)')"
        },
        "#ID_717638193": {
            "on": "L('x3260667647','registro.d1')"
        },
        "#ID_409036750": {
            "titulo": "L('x1975271086_traducir','Estructura Soportante')",
            "cargando": "L('x1740321226_traducir','cargando ..')",
            "hint": "L('x2898603391_traducir','Seleccione estructura')",
            "subtitulo": "L('x4011106049_traducir','Indique los tipos')"
        },
        "#ID_1507036956": {
            "text": "L('x2067448595_traducir','Cancelando Inspección, espere ..')"
        },
        "#ID_1712815186": {
            "text": "L('x2028852218_traducir','nivel 3')"
        },
        "#ID_1655547577": {
            "title": "L('x1249164186_traducir','Hoy')"
        },
        "#ID_662764184": {
            "text": "L('x2877523867_traducir','Superficie')"
        },
        "#ID_667285497": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "14dp"
            }
        },
        "#ID_327747054": {
            "texto": "L('x2315614667_traducir','Tip. Nunca dejes una tarea sin inspeccionar. El no hacerlo te afecta directamente')"
        },
        "#ID_1516027765": {
            "text": "L('x483197933_traducir','Seleccione año')"
        },
        "#ID_1002217127": {
            "hintText": "L('x714216034_traducir','escribir')"
        },
        "#ID_813613027": {
            "headerTitle": "L('x3690817388','manana_entrantes')"
        },
        "#ID_1689556322": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "14dp"
            }
        },
        "#ID_1227729639": {
            "title": "L('x850824713_traducir','PRODUCTOS')"
        },
        "#ID_1823288107": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "9dp"
            }
        },
        "#ID_1897376880": {
            "pantalla": "L('x1805186499_traducir','contenidos')"
        },
        "desistirtexto": {
            "color": "#ffffff",
            "font": {
                "fontWeight": "bold",
                "fontSize": "14dp"
            }
        },
        "estilo2_1": {
            "color": "#a0a1a3",
            "font": {
                "fontFamily": "SFUIText-Light",
                "fontSize": "15dp"
            }
        },
        "#ID_1162506155": {
            "text": "L('x2443017383_traducir','Fecha de nacimiento')"
        },
        "#ID_1168052393": {
            "titulo": "L('x3327059844_traducir','Entrepisos')",
            "cargando": "L('x1740321226_traducir','cargando ..')",
            "hint": "L('x2146928948_traducir','Seleccione entrepisos')",
            "subtitulo": "L('x4011106049_traducir','Indique los tipos')"
        },
        "#ID_1663369926": {
            "hintText": "L('x214679130_traducir','Apellido paterno')"
        },
        "#ID_514443896": {
            "_section": "L('x1916403066_traducir','hoy')"
        },
        "#ID_568446375": {
            "text": "L('x2452838785_traducir','¿Inhabitable?')"
        },
        "#ID_744073039": {
            "text": "L('x1350052795_traducir','Definir % de daños')"
        },
        "#ID_1547243391": {
            "hintText": "L('x3041923657_traducir','Escribir direccion')"
        },
        "fondocafe": {
            "backgroundColor": "#a5876d",
            "font": {
                "fontSize": "12dp"
            }
        },
        "#ID_1222663545": {
            "titulo": "L('x1574293037_traducir','Editar Disponibilidad')"
        },
        "#ID_1252159636": {
            "text": "L('x2770200964_traducir','Estimación de Meses')"
        },
        "#ID_831455466": {
            "_section": "L('x910537343_traducir','otra')"
        },
        "#ID_611022613": {
            "pantalla": "L('x3772634211_traducir','siniestro')"
        },
        "#ID_861542679": {
            "nivel1": "L('x3264699902','pais.label_nivel1')"
        },
        "#ID_1356979064": {
            "text": "L('x4108050209','0')"
        },
        "#ID_1122036551": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "12dp"
            }
        },
        "#ID_1613236955": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "9dp"
            }
        },
        "#ID_1980527066": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "8dp"
            }
        },
        "#ID_1921077056": {
            "titulo": "L('x404129330_traducir','¿EL ASEGURADO CONFIRMA ESTOS DATOS?')",
            "si": "L('x1723413441_traducir','SI, Están correctos')",
            "texto": "L('x2083765231_traducir','El asegurado debe confirmar que los datos de esta sección están correctos')",
            "pantalla": "L('x2851883104_traducir','¿ESTA DE ACUERDO?')",
            "no": "L('x55492959_traducir','NO, Hay que modificar algo')"
        },
        "#ID_1706780543": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "12dp"
            }
        },
        "#ID_1382661801": {
            "data": "L('x1872543855','data_region')"
        },
        "#ID_1139905442": {
            "text": "L('x2016219634_traducir','En presencia de')"
        },
        "#ID_1227389373": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "24dp"
            }
        },
        "estilo6_1": {
            "color": "#848484",
            "font": {
                "fontFamily": "SFUIText-Bold",
                "fontSize": "14dp"
            }
        },
        "#ID_2010596466": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "8dp"
            }
        },
        "#ID_2107305926": {
            "titulo": "L('x3752633607_traducir','PARTE 4: Disponibilidad de trabajo')",
            "avance": "L('x25508266_traducir','4/6')"
        },
        "#ID_449023935": {
            "text": "L('x3573009323_traducir','Repetir teléfono')"
        },
        "#ID_1709668154": {
            "text": "L('x2601646010_traducir','Tomar fotos de Recinto')"
        },
        "#ID_905374707": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "36dp"
            }
        },
        "#ID_1398654311": {
            "valor": "L('x4264937424','evento.valor')"
        },
        "estilo5": {
            "color": "#808080",
            "font": {
                "fontSize": "16dp"
            }
        },
        "#ID_1342034597": {
            "title": "L('x2943883035_traducir','Guardar')"
        },
        "#ID_1151684903": {
            "titulo": "L('x1313568614_traducir','Destino')",
            "hint": "L('x2017856269_traducir','Seleccione destino')",
            "subtitulo": "L('x1857779775_traducir','Indique los destinos')"
        },
        "#ID_1252140689": {
            "text": "L('x1996753865_traducir','correo')"
        },
        "#ID_1645691087": {
            "headerTitle": "L('x910537343_traducir','otra')"
        },
        "#ID_336997000": {
            "headerTitle": "L('x1602196311_traducir','ayer')"
        },
        "desistir": {
            "backgroundColor": "#adadad",
            "font": {
                "fontSize": "12dp"
            }
        },
        "#ID_1098001258": {
            "text": "L('x3376426101_traducir','NO')"
        },
        "#ID_225919236": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "14dp"
            }
        },
        "estilo3": {
            "color": "#838383",
            "font": {
                "fontFamily": "SFUIText-Medium",
                "fontSize": "12dp"
            }
        },
        "#ID_7263178": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "9dp"
            }
        },
        "#ID_1694039928": {
            "title": "L('x1721610689_traducir','PERFIL')"
        },
        "#ID_400889681": {
            "titulo": "L('x591862035_traducir','Pavimentos')",
            "cargando": "L('x1740321226_traducir','cargando ..')",
            "hint": "L('x2600368035_traducir','Seleccione pavimentos')",
            "subtitulo": "L('x4011106049_traducir','Indique los tipos')"
        },
        "#ID_1474596824": {
            "pantalla": "L('x3174879261_traducir','recintos')"
        },
        "#ID_1361811016": {
            "titulo": "L('x4288296688_traducir','REGION')",
            "campo": "L('x147780672_traducir','Region')",
            "seleccione": "L('x116943338_traducir','Seleccione region')"
        },
        "#ID_1386974827": {
            "_section": "L('x2819214932_traducir','holi1')"
        },
        "#ID_364580565": {
            "letra": "L('x185522819_traducir','MI')"
        },
        "#ID_41461518": {
            "headerTitle": "L('x3166667470','hoy_entrantes')"
        },
        "#ID_324967777": {
            "seleccione": "L('x3621251639_traducir','unidad')"
        },
        "#ID_1532353833": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "20dp"
            }
        },
        "#ID_1094491373": {
            "title": "L('x3976038131_traducir','DAÑOS EN CONTENIDO')"
        },
        "#ID_1729175210": {
            "text": "L('x1027380240_traducir','Nombre')"
        },
        "#ID_337746182": {
            "titulo": "L('x1321529571_traducir','ENTRAR')"
        },
        "#ID_1090265816": {
            "text": "L('x2439138629_traducir','¿Se recomienda analisis estructural por especialista?')"
        },
        "#ID_481046448": {
            "_section": "L('x3166667470','hoy_entrantes')"
        },
        "#ID_1092968272": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "20dp"
            }
        },
        "#ID_584016677": {
            "text": "L('x3376426101_traducir','NO')"
        },
        "#ID_2139168046": {
            "text": "L('x2067448595_traducir','Cancelando Inspección, espere ..')"
        },
        "#ID_1052014443": {
            "font": {
                "fontFamily": "beta1",
                "fontSize": "20dp"
            }
        },
        "#ID_127741422": {
            "text": "L('x1110261063_traducir','MEJOR RUTA')"
        },
        "#ID_1551530631": {
            "titulo": "L('x1951816285_traducir','¿PUEDE CONTINUAR CON LA INSPECCION?')",
            "si": "L('x369557195_traducir','SI puedo continuar')",
            "texto": "L('x2522333127_traducir','Si está el asegurado en el domicilio presione SI para continuar')",
            "no": "L('x3892244486_traducir','NO se pudo realizar la inspección')"
        },
        "#ID_1836760008": {
            "titulo": "L('x404129330_traducir','¿EL ASEGURADO CONFIRMA ESTOS DATOS?')",
            "si": "L('x1723413441_traducir','SI, Están correctos')",
            "texto": "L('x2083765231_traducir','El asegurado debe confirmar que los datos de esta sección están correctos')",
            "pantalla": "L('x2851883104_traducir','¿ESTA DE ACUERDO?')",
            "no": "L('x55492959_traducir','NO, Hay que modificar algo')"
        },
        "#ID_397435405": {
            "text": "L('x1027380240_traducir','Nombre')"
        },
        "#ID_229344222": {
            "title": "L('x2445898609_traducir','Perfil')"
        },
        "#ID_864340655": {
            "text": "L('x2268209570_traducir','ESTRUCTURA SOPORTANTE')"
        },
        "#ID_1282535710": {
            "texto": "L('x3287250645_traducir','CERRAR')"
        },
        "#ID_1688501184": {
            "pantalla": "L('x3174879261_traducir','recintos')"
        },
        "#ID_457319757": {
            "letra": "L('x1342839628','V')"
        },
        "#ID_1514050575": {
            "font": {
                "fontFamily": "",
                "fontSize": "18dp"
            }
        },
        "#ID_1344963939": {
            "text": "L('x1187778027_traducir','No cierre la aplicacion y asegurese de estar conectado a internet')"
        },
        "#ID_1465840618": {
            "title": "L('x810026296_traducir','FOTOS REQUERIDAS')"
        },
        "#ID_1479491977": {
            "text": "L('x1886674359_traducir','Agregue recintos para indicar daños')"
        },
        "#ID_1428215143": {
            "_section": "L('x1602196311_traducir','ayer')"
        },
        "#ID_564931174": {
            "on": "L('x1547518812','registro.d6')"
        }
    }
};
exports.fontello = {
    "adjust": {
        "CODES": {
            "auto": "\uE80b",
            "entrar": "\uE81b",
            "emergencias": "\uE819",
            "registrarse": "\uE827",
            "continuar": "\uE812",
            "info": "\uE821",
            "cerrar": "\uE80e",
            "salir_insp": "\uE828",
            "hoy": "\uE820",
            "check": "\uE80f",
            "agregar_item": "\uE80a",
            "9": "\uE808",
            "girar_camara": "\uE81d",
            "llamar": "\uE822",
            "volver": "\uE82a",
            "cruz": "\uE814",
            "1": "\uE800",
            "hora": "\uE81f",
            "ciudad": "\uE810",
            "3": "\uE802",
            "flecha": "\uE82b",
            "7": "\uE806",
            "4": "\uE803",
            "perfil": "\uE817",
            "cancelar_tarea": "\uE80d",
            "6": "\uE805",
            "mistareas": "\uE823",
            "historial": "\uE81e",
            "8": "\uE807",
            "entradas": "\uE81a",
            "caso": "\uE815",
            "ubicacion": "\uE829",
            "2": "\uE801",
            "refresh": "\uE826",
            "enviar_insp": "\uE81c",
            "telefono": "\uE818",
            "nuevo_dano": "\uE824",
            "comuna": "\uE811",
            "critico": "\uE813",
            "5": "\uE804",
            "camara": "\uE80c",
            "10": "\uE809",
            "correo": "\uE816"
        },
        "POSTSCRIPT": "beta1",
        "TTF": "/Users/eliasbaeza/Documents/Creador_Open_Workspace/ios/psb-uadjust-iphone/menu/sub/perfil/sub/contacto/dsl_cache//fontello-85cc3601/font/beta1.ttf"
    }
};