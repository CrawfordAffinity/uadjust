exports.definition = {
	config: {
		columns: {
			"label_nivel2": "TEXT",
			"moneda": "TEXT",
			"nombre": "TEXT",
			"label_nivel4": "TEXT",
			"label_codigo_identificador": "TEXT",
			"label_nivel3": "TEXT",
			"id_server": "INTEGER",
			"label_nivel1": "TEXT",
			"iso": "TEXT",
			"sis_metrico": "TEXT",
			"id": "INTEGER PRIMARY KEY AUTOINCREMENT",
			"niveles_pais": "INTEGER",
			"id_pais": "INTEGER",
			"label_nivel5": "TEXT",
			"lenguaje": "TEXT",
		},
		adapter: {
			"type": "sql",
			"collection_name": "pais",
			"idAttribute": "id",
			"remoteBackup": "false",
		}
	},
	extendModel: function(Model) {
		_.extend(Model.prototype, {
			transform: function transform() {
				var transformed = this.toJSON();
				return transformed;
			}
		});
		return Model;
	},
	extendCollection: function(Collection) {
		// helper functions
		function S4() {
			return (0 | 65536 * (1 + Math.random())).toString(16).substring(1);
		}
		function guid() {
			return S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4();
		}
		// start extend
		_.extend(Collection.prototype, {
			deleteAll : function() {
				var collection = this;
				var sql = "DELETE FROM " + collection.config.adapter.collection_name;
				db = Ti.Database.open(collection.config.adapter.db_name);
				db.execute(sql);
				db.close();
				collection.trigger('sync');
			}
		});
		// end extend
		return Collection;
	}
};
