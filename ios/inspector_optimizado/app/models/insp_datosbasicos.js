exports.definition = {
	config: {
		columns: {
			"id_inspeccion": "INTEGER",
			"hora_inspeccion": "TEXT",
			"fono_movil": "TEXT",
			"direccion_ciudad": "TEXT",
			"presente_nombre": "TEXT",
			"fono_fijo": "TEXT",
			"rut_asegurado": "TEXT",
			"fecha_inspeccion": "TEXT",
			"direccion_riesgo": "TEXT",
			"direccion_correcta": "INTEGER",
			"id": "INTEGER PRIMARY KEY AUTOINCREMENT",
			"direccion_comuna": "TEXT",
			"fecha_full_inspeccion": "TEXT",
			"asegurador": "TEXT",
			"nro_caso": "TEXT",
			"asegurado": "TEXT",
			"fecha_siniestro": "TEXT",
			"presente_rut": "TEXT",
			"email": "TEXT",
		},
		adapter: {
			"type": "sql",
			"collection_name": "insp_datosbasicos",
			"idAttribute": "id",
			"remoteBackup": "false",
		}
	},
	extendModel: function(Model) {
		_.extend(Model.prototype, {
			transform: function transform() {
				var transformed = this.toJSON();
				return transformed;
			}
		});
		return Model;
	},
	extendCollection: function(Collection) {
		// helper functions
		function S4() {
			return (0 | 65536 * (1 + Math.random())).toString(16).substring(1);
		}
		function guid() {
			return S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4();
		}
		// start extend
		_.extend(Collection.prototype, {
			deleteAll : function() {
				var collection = this;
				var sql = "DELETE FROM " + collection.config.adapter.collection_name;
				db = Ti.Database.open(collection.config.adapter.db_name);
				db.execute(sql);
				db.close();
				collection.trigger('sync');
			}
		});
		// end extend
		return Collection;
	}
};
