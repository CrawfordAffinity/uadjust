var _bind4section = {};
var _list_templates = {};
var $firma = $.firma.toJSON();

$.ID_1277998911_window.setTitleAttributes({
    color: 'WHITE'
});
var _activity;
if (OS_ANDROID) {
    _activity = $.ID_1277998911.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_1277998911';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_1277998911_window.addEventListener('open', function(e) {
        abx.setStatusbarColor("#FFFFFF");
        abx.setBackgroundColor("#2d9edb");
    });
}


_my_events['app:canvasGetImageResponse,ID_951380292_web'] = function(evento) {
    if (false) console.log('recibi resupesta en la web', {});
    if (evento.accion == L('x3719714762_traducir', 'inicio')) {
        if (false) console.log('procesando firma', {});
    } else if (evento.accion == L('x2199507635_traducir', 'imagen')) {
        var firma_accion = ('firma_accion' in require('vars')) ? require('vars')['firma_accion'] : '';
        if (firma_accion == L('x1623535239_traducir', 'guardar')) {
            var ID_1601301257_m = Alloy.Collections.numero_unico;
            var ID_1601301257_fila = Alloy.createModel('numero_unico', {
                comentario: 'firma'
            });
            ID_1601301257_m.add(ID_1601301257_fila);
            ID_1601301257_fila.save();
            var nuevoid = require('helper').model2object(ID_1601301257_m.last());
            var firma_png = Ti.Utils.base64decode(evento.image);
            if (OS_ANDROID) {
                var ID_375516339_imagefactory = require('ti.imagefactory');
                var firma_jpg = ID_375516339_imagefactory.compress(firma_png, 0.9);
            } else if (OS_IOS) {
                var ID_375516339_imagefactory = require('ti.imagefactory');
                var firma_jpg = ID_375516339_imagefactory.compress(firma_png, 0.9);
            }
            if (false) console.log(String.format(L('x2459914461', 'firma capturada, y asignada como firma%1$s.jpg'), nuevoid.id.toString()), {});
            var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
            if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
                var ID_1153703423_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
                if (ID_1153703423_d.exists() == false) ID_1153703423_d.createDirectory();
                var ID_1153703423_f = Ti.Filesystem.getFile(ID_1153703423_d.resolve(), 'firma' + nuevoid.id + '.jpg');
                if (ID_1153703423_f.exists() == true) ID_1153703423_f.deleteFile();
                ID_1153703423_f.write(firma_jpg);
                ID_1153703423_d = null;
                ID_1153703423_f = null;
            } else {
                var ID_1633907683_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + 0);
                if (ID_1633907683_d.exists() == false) ID_1633907683_d.createDirectory();
                var ID_1633907683_f = Ti.Filesystem.getFile(ID_1633907683_d.resolve(), 'firma' + nuevoid.id + '.jpg');
                if (ID_1633907683_f.exists() == true) ID_1633907683_f.deleteFile();
                ID_1633907683_f.write(firma_jpg);
                ID_1633907683_d = null;
                ID_1633907683_f = null;
            }
            $.firma.set({
                firma64: String.format(L('x2450709990', 'firma%1$s.jpg'), nuevoid.id.toString())
            });
            if ('firma' in $) $firma = $.firma.toJSON();
            var seltarea = null,
                firma_jpg = null,
                firma_png = null,
                evento = null;
            if (false) console.log('guardando firma', {});
            Alloy.Collections[$.firma.config.adapter.collection_name].add($.firma);
            $.firma.save();
            Alloy.Collections[$.firma.config.adapter.collection_name].fetch();
            require('vars')['firma_accion'] = '';
            var ID_1042874148_trycatch = {
                error: function(e) {
                    if (false) console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
                }
            };
            try {
                ID_1042874148_trycatch.error = function(evento) {};
                Alloy.Events.trigger('_cerrar_insp');
            } catch (e) {
                ID_1042874148_trycatch.error(e);
            }
            Alloy.createController("fin_index", {}).getView().open();
        } else if (firma_accion == L('x1912016452_traducir', 'limpiar')) {
            require('vars')['firma_accion'] = '';
            if (false) console.log('firma limpiada', {});
        } else {
            require('vars')['firma_accion'] = '';
        }
    }
};
Ti.App.addEventListener('app:canvasGetImageResponse', _my_events['app:canvasGetImageResponse,ID_951380292_web']);


$.ID_1911446078.init({
    titulo: L('x3679463181_traducir', 'FINALIZAR'),
    __id: 'ALL1911446078',
    color: 'verde',
    onclick: Click_ID_1575422634
});

function Click_ID_1575422634(e) {

    var evento = e;
    ID_1929834437.show();

}

function Postlayout_ID_1981850400(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var anteriores = ('anteriores' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['anteriores'] : '';
    if (anteriores == false || anteriores == 'false') {
        var insp_cancelada = ('insp_cancelada' in require('vars')) ? require('vars')['insp_cancelada'] : '';
        if ((_.isObject(insp_cancelada) || (_.isString(insp_cancelada)) && !_.isEmpty(insp_cancelada)) || _.isNumber(insp_cancelada) || _.isBoolean(insp_cancelada)) {
            /** 
             * Dependiendo del flag (para saber si se cancelo la inspeccion) cerramos pantalla de insp cancelada o documentos 
             */
            if (false) console.log(String.format(L('x2708040700_traducir', 'llamando cierre insp_cancelada %1$s'), insp_cancelada.toString()), {});
            Alloy.Events.trigger('_cerrar_insp', {
                pantalla: insp_cancelada
            });
        } else {
            Alloy.Events.trigger('_cerrar_insp', {
                pantalla: L('x3294048930_traducir', 'documentos')
            });
        }
        require('vars')[_var_scopekey]['anteriores'] = L('x4261170317', 'true');
    }

}

/** 
 * Completamos datos de persona presente 
 */
var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
if (false) console.log('datos de seltarea en firma', {
    "seltarea": seltarea
});
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
    $.firma.set({
        id_inspeccion: seltarea.id_server
    });
    if ('firma' in $) $firma = $.firma.toJSON();
    var ID_1405240957_i = Alloy.createCollection('insp_datosbasicos');
    var ID_1405240957_i_where = 'id_inspeccion=\'' + seltarea.id_server + '\'';
    ID_1405240957_i.fetch({
        query: 'SELECT * FROM insp_datosbasicos WHERE id_inspeccion=\'' + seltarea.id_server + '\''
    });
    var previos = require('helper').query2array(ID_1405240957_i);
    if (false) console.log('datos basicos en firma', {
        "datos": previos
    });
    if (previos && previos.length) {
        $.ID_1582745917.setText(previos[0].presente_nombre);

        $.ID_602473581.setText(previos[0].presente_rut);

    } else {
        if (false) console.log('ERROR FIRMA: insp_datosbasicos no tiene datos de inspeccion (ej. si cancelo en hayalguien, antes de datosbasicos)', {});
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        if ((_.isObject(seltarea.asegurado_nombre) || (_.isString(seltarea.asegurado_nombre)) && !_.isEmpty(seltarea.asegurado_nombre)) || _.isNumber(seltarea.asegurado_nombre) || _.isBoolean(seltarea.asegurado_nombre)) {
            $.ID_1582745917.setText(seltarea.asegurado_nombre);

        } else {
            $.ID_1582745917.setText('No definido');

        }
        if ((_.isObject(seltarea.asegurado_codigo_identificador) || (_.isString(seltarea.asegurado_codigo_identificador)) && !_.isEmpty(seltarea.asegurado_codigo_identificador)) || _.isNumber(seltarea.asegurado_codigo_identificador) || _.isBoolean(seltarea.asegurado_codigo_identificador)) {
            $.ID_602473581.setText(seltarea.asegurado_codigo_identificador);

        } else {
            $.ID_602473581.setText('-');

        }
        var ID_455319011_i = Alloy.createCollection('insp_datosbasicos');
        var ID_455319011_i_where = '';
        ID_455319011_i.fetch();
        var ptodos = require('helper').query2array(ID_455319011_i);
        if (false) console.log('ERROR FIRMA: debug insp_datosbasicos full', {
            "full": ptodos
        });
    }
} else {
    $.ID_1582745917.setText('Pablo Martinez Herrera');

    $.ID_602473581.setText('5.555.555-5');

}
require('vars')['firma_accion'] = L('x2290876758_traducir', 'nada');
var ID_1929834437_opts = [L('x3827418516_traducir', 'Si'), L('x3211941273_traducir', ' Limpiar'), L('x2376009830_traducir', ' Cancelar')];
var ID_1929834437 = Ti.UI.createAlertDialog({
    title: L('x2308473092_traducir', 'Por favor, confirme'),
    message: L('x652596190_traducir', '¿ Esta conforme con la firma ?'),
    buttonNames: ID_1929834437_opts
});
ID_1929834437.addEventListener('click', function(e) {
    var resp = ID_1929834437_opts[e.index];
    if (resp == L('x3827418516_traducir', 'Si')) {
        require('vars')['firma_accion'] = L('x1623535239_traducir', 'guardar');
        Ti.App.fireEvent('app:canvasGetImage');
    } else if (resp == L('x3192535000_traducir', 'Limpiar')) {
        require('vars')['firma_accion'] = L('x1912016452_traducir', 'limpiar');
        Ti.App.fireEvent('app:canvasGetImage');
    } else {
        require('vars')['firma_accion'] = L('x2290876758_traducir', 'nada');
    }
    resp = null;
});
/** 
 * Cerramos esta pantalla cuando se haya llamado el evento desde pantalla finalizado 
 */
_my_events['_cerrar_insp,ID_1618628689'] = function(evento) {
    if (!_.isUndefined(evento.pantalla)) {
        if (evento.pantalla == L('x736965987_traducir', 'firma')) {
            if (false) console.log('debug cerrando firma', {});
            var ID_2044076676_trycatch = {
                error: function(e) {
                    if (false) console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
                }
            };
            try {
                ID_2044076676_trycatch.error = function(evento) {
                    if (false) console.log('error cerrando firma', {});
                };
                $.ID_1277998911.close();
            } catch (e) {
                ID_2044076676_trycatch.error(e);
            }
        }
    } else {
        if (false) console.log('debug cerrando (todas) firma', {});
        var ID_1408754065_trycatch = {
            error: function(e) {
                if (false) console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
            }
        };
        try {
            ID_1408754065_trycatch.error = function(evento) {
                if (false) console.log('error cerrando firma', {});
            };
            $.ID_1277998911.close();
        } catch (e) {
            ID_1408754065_trycatch.error(e);
        }
    }
};
Alloy.Events.on('_cerrar_insp', _my_events['_cerrar_insp,ID_1618628689']);
/** 
 * Revisamos flag para saber si el inspector cancelo la inspeccion 
 */
var insp_cancelada = ('insp_cancelada' in require('vars')) ? require('vars')['insp_cancelada'] : '';
if ((_.isObject(insp_cancelada) || (_.isString(insp_cancelada)) && !_.isEmpty(insp_cancelada)) || _.isNumber(insp_cancelada) || _.isBoolean(insp_cancelada)) {
    var ID_1410235970_visible = true;

    if (ID_1410235970_visible == 'si') {
        ID_1410235970_visible = true;
    } else if (ID_1410235970_visible == 'no') {
        ID_1410235970_visible = false;
    }
    $.ID_1410235970.setVisible(ID_1410235970_visible);

} else {
    var ID_1410235970_visible = false;

    if (ID_1410235970_visible == 'si') {
        ID_1410235970_visible = true;
    } else if (ID_1410235970_visible == 'no') {
        ID_1410235970_visible = false;
    }
    $.ID_1410235970.setVisible(ID_1410235970_visible);

}
if (OS_IOS || OS_ANDROID) {
    $.ID_1277998911.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}
//$.ID_1277998911.open();