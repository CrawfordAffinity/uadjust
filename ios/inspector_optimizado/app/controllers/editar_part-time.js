var _bind4section = {};
var _list_templates = {};

var _activity;
if (OS_ANDROID) {
    _activity = $.ID_375599278.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_375599278';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_375599278.addEventListener('open', function(e) {
        abx.setStatusbarColor("#000000");
        abx.setBackgroundColor("white");
    });
}

function Click_ID_1337435494(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    $.ID_375599278.close();

    Alloy.Events.trigger('_close_editar');

}

function Click_ID_1948273248(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var desde = ('desde' in require('vars')) ? require('vars')['desde'] : '';
    var hasta = ('hasta' in require('vars')) ? require('vars')['hasta'] : '';
    var dias = ('dias' in require('vars')) ? require('vars')['dias'] : '';
    if (false) console.log('detalle de dias al clickear', {
        "datos": dias
    });

    //validaciones
    var al_menos_uno = dias.d1 || dias.d2 || dias.d3 || dias.d4 || dias.d5 || dias.d6 || dias.d7;
    desde = parseInt(desde);
    hasta = parseInt(hasta)
    if (al_menos_uno == false || al_menos_uno == 'false') {
        var ID_1232101512_opts = [L('x1518866076_traducir', 'Aceptar')];
        var ID_1232101512 = Ti.UI.createAlertDialog({
            title: L('x57652245_traducir', 'Alerta'),
            message: L('x2430147855_traducir', 'Seleccione al menos un día'),
            buttonNames: ID_1232101512_opts
        });
        ID_1232101512.addEventListener('click', function(e) {
            var suu = ID_1232101512_opts[e.index];
            suu = null;

            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1232101512.show();
    } else if (_.isNumber(desde) && _.isNumber(hasta) && desde > hasta) {
        var ID_594317031_opts = [L('x1518866076_traducir', 'Aceptar')];
        var ID_594317031 = Ti.UI.createAlertDialog({
            title: L('x57652245_traducir', 'Alerta'),
            message: L('x4068516320_traducir', 'El rango de horas está mal ingresado'),
            buttonNames: ID_594317031_opts
        });
        ID_594317031.addEventListener('click', function(e) {
            var suu = ID_594317031_opts[e.index];
            suu = null;

            e.source.removeEventListener("click", arguments.callee);
        });
        ID_594317031.show();
    } else if (desde == hasta) {
        var ID_981623759_opts = [L('x1518866076_traducir', 'Aceptar')];
        var ID_981623759 = Ti.UI.createAlertDialog({
            title: L('x57652245_traducir', 'Alerta'),
            message: L('x178692076_traducir', 'El rango debe ser superior a una hora'),
            buttonNames: ID_981623759_opts
        });
        ID_981623759.addEventListener('click', function(e) {
            var suu = ID_981623759_opts[e.index];
            suu = null;

            e.source.removeEventListener("click", arguments.callee);
        });
        ID_981623759.show();
    } else {
        var registro = _.extend(registro, {
            disp_horas: String.format(L('x1576037006_traducir', '%1$s:00 %2$s:00'), (desde) ? desde.toString() : '', (hasta) ? hasta.toString() : '')
        });
        require('vars')['registro'] = registro;
        if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
            var ID_1214115693_i = Alloy.createCollection('inspectores');
            var ID_1214115693_i_where = '';
            ID_1214115693_i.fetch();
            var inspector_list = require('helper').query2array(ID_1214115693_i);
            inspector = inspector_list[0];
            var fueraciudad = ('fueraciudad' in require('vars')) ? require('vars')['fueraciudad'] : '';
            var fuerapais = ('fuerapais' in require('vars')) ? require('vars')['fuerapais'] : '';
            var dias = ('dias' in require('vars')) ? require('vars')['dias'] : '';
            var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
            var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
            var desde = ('desde' in require('vars')) ? require('vars')['desde'] : '';
            var hasta = ('hasta' in require('vars')) ? require('vars')['hasta'] : '';
            var ID_1114361496 = {};

            ID_1114361496.success = function(e) {
                var elemento = e,
                    valor = e;
                if (false) console.log('Mi resultado es', {
                    "elemento": elemento
                });
                if (elemento.error == 0 || elemento.error == '0') {
                    var ID_790725687_i = Alloy.createCollection('inspectores');
                    var ID_790725687_i_where = '';
                    ID_790725687_i.fetch();
                    var inspector_list = require('helper').query2array(ID_790725687_i);
                    var fueraciudad = ('fueraciudad' in require('vars')) ? require('vars')['fueraciudad'] : '';
                    var fuerapais = ('fuerapais' in require('vars')) ? require('vars')['fuerapais'] : '';
                    var dias = ('dias' in require('vars')) ? require('vars')['dias'] : '';
                    var desde = ('desde' in require('vars')) ? require('vars')['desde'] : '';
                    var hasta = ('hasta' in require('vars')) ? require('vars')['hasta'] : '';
                    if (false) console.log('modificando disponibilidad en BD local', {
                        "registro": dias
                    });
                    var db = Ti.Database.open(ID_790725687_i.config.adapter.db_name);
                    if (ID_790725687_i_where == '') {
                        var sql = 'UPDATE ' + ID_790725687_i.config.adapter.collection_name + ' SET disponibilidad_viajar_pais=\'' + fuerapais + '\'';
                    } else {
                        var sql = 'UPDATE ' + ID_790725687_i.config.adapter.collection_name + ' SET disponibilidad_viajar_pais=\'' + fuerapais + '\' WHERE ' + ID_790725687_i_where;
                    }
                    db.execute(sql);
                    if (ID_790725687_i_where == '') {
                        var sql = 'UPDATE ' + ID_790725687_i.config.adapter.collection_name + ' SET disponibilidad_viajar_ciudad=\'' + fueraciudad + '\'';
                    } else {
                        var sql = 'UPDATE ' + ID_790725687_i.config.adapter.collection_name + ' SET disponibilidad_viajar_ciudad=\'' + fueraciudad + '\' WHERE ' + ID_790725687_i_where;
                    }
                    db.execute(sql);
                    if (ID_790725687_i_where == '') {
                        var sql = 'UPDATE ' + ID_790725687_i.config.adapter.collection_name + ' SET disponibilidad=\'' + 0 + '\'';
                    } else {
                        var sql = 'UPDATE ' + ID_790725687_i.config.adapter.collection_name + ' SET disponibilidad=\'' + 0 + '\' WHERE ' + ID_790725687_i_where;
                    }
                    db.execute(sql);
                    if (ID_790725687_i_where == '') {
                        var sql = 'UPDATE ' + ID_790725687_i.config.adapter.collection_name + ' SET disponibilidad_horas=\'' + desde + ':00 ' + hasta + ':00' + '\'';
                    } else {
                        var sql = 'UPDATE ' + ID_790725687_i.config.adapter.collection_name + ' SET disponibilidad_horas=\'' + desde + ':00 ' + hasta + ':00' + '\' WHERE ' + ID_790725687_i_where;
                    }
                    db.execute(sql);
                    if (ID_790725687_i_where == '') {
                        var sql = 'UPDATE ' + ID_790725687_i.config.adapter.collection_name + ' SET d1=\'' + dias.d1 + '\'';
                    } else {
                        var sql = 'UPDATE ' + ID_790725687_i.config.adapter.collection_name + ' SET d1=\'' + dias.d1 + '\' WHERE ' + ID_790725687_i_where;
                    }
                    db.execute(sql);
                    if (ID_790725687_i_where == '') {
                        var sql = 'UPDATE ' + ID_790725687_i.config.adapter.collection_name + ' SET d2=\'' + dias.d2 + '\'';
                    } else {
                        var sql = 'UPDATE ' + ID_790725687_i.config.adapter.collection_name + ' SET d2=\'' + dias.d2 + '\' WHERE ' + ID_790725687_i_where;
                    }
                    db.execute(sql);
                    if (ID_790725687_i_where == '') {
                        var sql = 'UPDATE ' + ID_790725687_i.config.adapter.collection_name + ' SET d3=\'' + dias.d3 + '\'';
                    } else {
                        var sql = 'UPDATE ' + ID_790725687_i.config.adapter.collection_name + ' SET d3=\'' + dias.d3 + '\' WHERE ' + ID_790725687_i_where;
                    }
                    db.execute(sql);
                    if (ID_790725687_i_where == '') {
                        var sql = 'UPDATE ' + ID_790725687_i.config.adapter.collection_name + ' SET d4=\'' + dias.d4 + '\'';
                    } else {
                        var sql = 'UPDATE ' + ID_790725687_i.config.adapter.collection_name + ' SET d4=\'' + dias.d4 + '\' WHERE ' + ID_790725687_i_where;
                    }
                    db.execute(sql);
                    if (ID_790725687_i_where == '') {
                        var sql = 'UPDATE ' + ID_790725687_i.config.adapter.collection_name + ' SET d5=\'' + dias.d5 + '\'';
                    } else {
                        var sql = 'UPDATE ' + ID_790725687_i.config.adapter.collection_name + ' SET d5=\'' + dias.d5 + '\' WHERE ' + ID_790725687_i_where;
                    }
                    db.execute(sql);
                    if (ID_790725687_i_where == '') {
                        var sql = 'UPDATE ' + ID_790725687_i.config.adapter.collection_name + ' SET d6=\'' + dias.d6 + '\'';
                    } else {
                        var sql = 'UPDATE ' + ID_790725687_i.config.adapter.collection_name + ' SET d6=\'' + dias.d6 + '\' WHERE ' + ID_790725687_i_where;
                    }
                    db.execute(sql);
                    if (ID_790725687_i_where == '') {
                        var sql = 'UPDATE ' + ID_790725687_i.config.adapter.collection_name + ' SET d7=\'' + dias.d7 + '\'';
                    } else {
                        var sql = 'UPDATE ' + ID_790725687_i.config.adapter.collection_name + ' SET d7=\'' + dias.d7 + '\' WHERE ' + ID_790725687_i_where;
                    }
                    db.execute(sql);
                    db.close();
                }
                elemento = null, valor = null;
            };

            ID_1114361496.error = function(e) {
                var elemento = e,
                    valor = e;
                if (false) console.log('Hubo un error', {
                    "elemento": elemento
                });
                elemento = null, valor = null;
            };
            require('helper').ajaxUnico('ID_1114361496', '' + String.format(L('x2304081372', '%1$seditarPerfilTipo2'), url_server.toString()) + '', 'POST', {
                id_inspector: inspector.id_server,
                disponibilidad_viajar_pais: fuerapais,
                disponibilidad_viajar_ciudad: fueraciudad,
                disponibilidad_horas: desde + ':00 ' + hasta + ':00',
                disponibilidad: 0,
                d1: dias.d1,
                d2: dias.d2,
                d3: dias.d3,
                d4: dias.d4,
                d5: dias.d5,
                d6: dias.d6,
                d7: dias.d7
            }, 15000, ID_1114361496);
            $.ID_375599278.close();

            Alloy.Events.trigger('_close_editar');
        } else {
            var ID_922158419_opts = [L('x1518866076_traducir', 'Aceptar')];
            var ID_922158419 = Ti.UI.createAlertDialog({
                title: L('x3071602690_traducir', 'No hay internet'),
                message: L('x2846685350_traducir', 'No se pueden efectuar los cambios sin conexion.'),
                buttonNames: ID_922158419_opts
            });
            ID_922158419.addEventListener('click', function(e) {
                var suu = ID_922158419_opts[e.index];
                suu = null;

                e.source.removeEventListener("click", arguments.callee);
            });
            ID_922158419.show();
        }
    }
}

$.ID_1614118096.init({
    titulo: L('x31606107_traducir', 'Editar Disponibilidad de trabajo'),
    __id: 'ALL1614118096',
    avance: L('', '')
});


$.ID_1132173110.init({
    __id: 'ALL1132173110',
    letra: L('x2909332022', 'L'),
    onon: on_ID_1237590678,
    onoff: Off_ID_1150929050
});

function on_ID_1237590678(e) {

    var evento = e;
    var dias = ('dias' in require('vars')) ? require('vars')['dias'] : '';
    var dias = _.extend(dias, {
        d1: 1
    });
    if (false) console.log('estado de dias', {
        "data": dias
    });
    require('vars')['dias'] = dias;

}

function Off_ID_1150929050(e) {

    var evento = e;
    var dias = ('dias' in require('vars')) ? require('vars')['dias'] : '';
    var dias = _.extend(dias, {
        d1: 0
    });
    require('vars')['dias'] = dias;

}

$.ID_1396533677.init({
    __id: 'ALL1396533677',
    letra: L('x3664761504', 'M'),
    onon: on_ID_727785297,
    onoff: Off_ID_610801192
});

function on_ID_727785297(e) {

    var evento = e;
    var dias = ('dias' in require('vars')) ? require('vars')['dias'] : '';
    var dias = _.extend(dias, {
        d2: 1
    });
    require('vars')['dias'] = dias;

}

function Off_ID_610801192(e) {

    var evento = e;
    var dias = ('dias' in require('vars')) ? require('vars')['dias'] : '';
    var dias = _.extend(dias, {
        d2: 0
    });
    require('vars')['dias'] = dias;

}

$.ID_364580565.init({
    __id: 'ALL364580565',
    letra: L('x185522819_traducir', 'MI'),
    onon: on_ID_1272243659,
    onoff: Off_ID_1236282682
});

function on_ID_1272243659(e) {

    var evento = e;
    var dias = ('dias' in require('vars')) ? require('vars')['dias'] : '';
    var dias = _.extend(dias, {
        d3: 1
    });
    require('vars')['dias'] = dias;

}

function Off_ID_1236282682(e) {

    var evento = e;
    var dias = ('dias' in require('vars')) ? require('vars')['dias'] : '';
    var dias = _.extend(dias, {
        d3: 0
    });
    require('vars')['dias'] = dias;

}

$.ID_180804538.init({
    __id: 'ALL180804538',
    letra: L('x1141589763', 'J'),
    onon: on_ID_151645229,
    onoff: Off_ID_768817667
});

function on_ID_151645229(e) {

    var evento = e;
    var dias = ('dias' in require('vars')) ? require('vars')['dias'] : '';
    var dias = _.extend(dias, {
        d4: 1
    });
    require('vars')['dias'] = dias;

}

function Off_ID_768817667(e) {

    var evento = e;
    var dias = ('dias' in require('vars')) ? require('vars')['dias'] : '';
    var dias = _.extend(dias, {
        d4: 0
    });
    require('vars')['dias'] = dias;

}

$.ID_457319757.init({
    __id: 'ALL457319757',
    letra: L('x1342839628', 'V'),
    onon: on_ID_1948647970,
    onoff: Off_ID_119088936
});

function on_ID_1948647970(e) {

    var evento = e;
    var dias = ('dias' in require('vars')) ? require('vars')['dias'] : '';
    var dias = _.extend(dias, {
        d5: 1
    });
    require('vars')['dias'] = dias;

}

function Off_ID_119088936(e) {

    var evento = e;
    var dias = ('dias' in require('vars')) ? require('vars')['dias'] : '';
    var dias = _.extend(dias, {
        d5: 0
    });
    require('vars')['dias'] = dias;

}

$.ID_993907842.init({
    __id: 'ALL993907842',
    letra: L('x543223747', 'S'),
    onon: on_ID_1501368637,
    onoff: Off_ID_1685362431
});

function on_ID_1501368637(e) {

    var evento = e;
    var dias = ('dias' in require('vars')) ? require('vars')['dias'] : '';
    var dias = _.extend(dias, {
        d6: 1
    });
    require('vars')['dias'] = dias;

}

function Off_ID_1685362431(e) {

    var evento = e;
    var dias = ('dias' in require('vars')) ? require('vars')['dias'] : '';
    var dias = _.extend(dias, {
        d6: 0
    });
    require('vars')['dias'] = dias;

}

$.ID_947148270.init({
    __id: 'ALL947148270',
    letra: L('x2746444292', 'D'),
    onon: on_ID_6265224,
    onoff: Off_ID_689881562
});

function on_ID_6265224(e) {

    var evento = e;
    var dias = ('dias' in require('vars')) ? require('vars')['dias'] : '';
    var dias = _.extend(dias, {
        d7: 1
    });
    require('vars')['dias'] = dias;

}

function Off_ID_689881562(e) {

    var evento = e;
    var dias = ('dias' in require('vars')) ? require('vars')['dias'] : '';
    var dias = _.extend(dias, {
        d7: 0
    });
    require('vars')['dias'] = dias;

}

$.ID_1517641115.init({
    __id: 'ALL1517641115',
    onchange: Change_ID_267735168,
    mins: L('x4261170317', 'true'),
    a: L('x3904355907_traducir', 'a')
});

function Change_ID_267735168(e) {

    var evento = e;
    require('vars')['desde'] = evento.desde;
    require('vars')['hasta'] = evento.hasta;

}

(function() {
    var ID_352358512_i = Alloy.createCollection('inspectores');
    var ID_352358512_i_where = '';
    ID_352358512_i.fetch();
    var inspector_list = require('helper').query2array(ID_352358512_i);
    var dias = {
        d1: '',
        d2: '',
        d3: '',
        d4: '',
        d5: '',
        d6: '',
        d7: ''
    };
    var dias = _.extend(dias, {
        d1: inspector.d1,
        d2: inspector.d2,
        d3: inspector.d3,
        d4: inspector.d4,
        d5: inspector.d5,
        d6: inspector.d6,
        d7: inspector.d7
    });
    if (false) console.log('detalle de los dias al consultar', {
        "datos": dias
    });
    inspector = inspector_list[0];
    var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
    if (_.isObject(registro) && !_.isArray(registro) && !_.isFunction(registro)) {} else {
        require('vars')['registro'] = inspector;
        var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
    }
    if (inspector_list && inspector_list.length == 0) {
        var dias = _.extend(dias, {
            d1: 0,
            d2: 0,
            d3: 0,
            d4: 0,
            d5: 0,
            d6: 0,
            d7: 0
        });
        require('vars')['desde'] = L('x2212294583', '1');
        require('vars')['hasta'] = L('x2212294583', '1');
    } else {
        var dias = _.extend(dias, {
            d1: inspector.d1,
            d2: inspector.d2,
            d3: inspector.d3,
            d4: inspector.d4,
            d5: inspector.d5,
            d6: inspector.d6,
            d7: inspector.d7
        });
        if (false) console.log('editar-disponibilidad: info inspector dice', {
            "inspector": inspector
        });
        if (!_.isNull(inspector.disponibilidad_horas)) {
            if ((_.isObject(inspector.disponibilidad_horas) || (_.isString(inspector.disponibilidad_horas)) && !_.isEmpty(inspector.disponibilidad_horas)) || _.isNumber(inspector.disponibilidad_horas) || _.isBoolean(inspector.disponibilidad_horas)) {

                horas = inspector.disponibilidad_horas.split(" ");
                desde_horas = horas[0].split(":");
                hasta_horas = horas[1].split(":")
                require('vars')['desde'] = desde_horas[0];
                require('vars')['hasta'] = hasta_horas[0];
                if (false) console.log('aqui vamos a evaluar las horas', {
                    "desde": desde_horas[0],
                    "hasta": hasta_horas[0]
                });
                /*
                $.ID_1517641115.set({
                    desde: desde_horas[0]
                });

                $.ID_1517641115.set({
                    hasta: hasta_horas[0]
                });
                */
            } else {
                require('vars')['desde'] = L('x2212294583', '1');
                require('vars')['hasta'] = L('x2212294583', '1');
            }
        } else {
            require('vars')['desde'] = L('x2212294583', '1');
            require('vars')['hasta'] = L('x2212294583', '1');
        }
    }
    require('vars')['dias'] = dias;
    require('vars')['registro'] = registro;
    var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';

    $.ID_1132173110.set({
        on: inspector.d1
    });

    $.ID_1396533677.set({
        on: inspector.d2
    });

    $.ID_364580565.set({
        on: inspector.d3
    });

    $.ID_180804538.set({
        on: inspector.d4
    });

    $.ID_457319757.set({
        on: inspector.d5
    });

    $.ID_993907842.set({
        on: inspector.d6
    });

    $.ID_947148270.set({
        on: inspector.d7
    });
})();

function Postlayout_ID_835780064(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var ID_1482413520_func = function() {
        /** 
         * Una vez ya cargada la tarea, dejamos la variable limpia para poder abrir otras pantallas 
         */
        require('vars')['var_abriendo'] = '';
    };
    var ID_1482413520 = setTimeout(ID_1482413520_func, 1000 * 0.2);

}
if (OS_IOS || OS_ANDROID) {
    $.ID_375599278.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}