var _bind4section = {};
var _list_templates = {};
var $inspector = $.inspector.toJSON();

var _activity;
if (OS_ANDROID) {
    _activity = $.ID_1385093082.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_1385093082';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_1385093082_window.addEventListener('open', function(e) {
        abx.setStatusbarColor("#000000");
        abx.setBackgroundColor("white");
    });
}


var ID_2115698781_like = function(search) {
    if (typeof search !== 'string' || this === null) {
        return false;
    }
    search = search.replace(new RegExp("([\\.\\\\\\+\\*\\?\\[\\^\\]\\$\\(\\)\\{\\}\\=\\!\\<\\>\\|\\:\\-])", "g"), "\\$1");
    search = search.replace(/%/g, '.*').replace(/_/g, '.');
    return RegExp('^' + search + '$', 'gi').test(this);
};
var ID_2115698781_filter = function(coll) {
    var filtered = _.toArray(coll.filter(function(m) {
        return true;
    }));
    return filtered;
};
var ID_2115698781_transform = function(model) {
    var fila = model.toJSON();
    return fila;
};
var ID_2115698781_update = function(e) {};
_.defer(function() {
    Alloy.Collections.pais.fetch();
});
Alloy.Collections.pais.on('add change delete', function(ee) {
    ID_2115698781_update(ee);
});
Alloy.Collections.pais.fetch();

function Click_ID_370401986(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    $.ID_1385093082.close();

}

function Click_ID_1231082455(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var pais_seleccionado = ('pais_seleccionado' in require('vars')) ? require('vars')['pais_seleccionado'] : '';
    if (Ti.App.deployType != 'production') console.log('el pais a guardar es', {
        "datos": pais_seleccionado
    });
    /** 
     * Creamos un flag para evitar que la pantalla se abra dos veces 
     */
    var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
    if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
        require('vars')['var_abriendo'] = L('x4261170317', 'true');
        Alloy.createController("editar_domicilio", {}).getView().open();
    }

}

$.ID_1312331480.init({
    titulo: L('x3904602325_traducir', 'Editar país'),
    __id: 'ALL1312331480',
    avance: '',
    onclick: Click_ID_2857115
});

function Click_ID_2857115(e) {

    var evento = e;

}


function Change_ID_1359202645(e) {

    e.cancelBubble = true;
    var elemento = e;
    var _columna = e.columnIndex;
    var columna = e.columnIndex + 1;
    var _fila = e.rowIndex;
    var fila = e.rowIndex + 1;
    var modelo = require('helper').query2array(Alloy.Collections.pais)[e.rowIndex];
    _.defer(function(modelo) {
        /** 
         * Guardamos el pais seleccionado en el picker 
         */
        require('vars')['pais_seleccionado'] = modelo;
    }, modelo);

}

(function() {
    _my_events['_close_editar,ID_1370588202'] = function(evento) {
        if (Ti.App.deployType != 'production') console.log('estamos en el _close_editar', {});
        $.ID_1385093082.close();
    };
    Alloy.Events.on('_close_editar', _my_events['_close_editar,ID_1370588202']);
    var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
    var ID_1595206756_i = Alloy.createCollection('pais');
    var ID_1595206756_i_where = 'id_server=1';
    ID_1595206756_i.fetch({
        query: 'SELECT * FROM pais WHERE id_server=1'
    });
    var pais_detalle = require('helper').query2array(ID_1595206756_i);
    /** 
     * Guardamos variable con el dato previo del pais del inspector y cargamos los datos en binding 
     */
    require('vars')['pais_seleccionado'] = pais_detalle[0];
    if (Ti.App.deployType != 'production') console.log('detalle del pais antiguo', {
        "datos": pais_detalle[0]
    });
    var ID_244825631 = {};
    ID_244825631.success = function(e) {
        var elemento = e,
            valor = e;
        if (elemento == false || elemento == 'false') {
            /** 
             * Si en la consulta los datos obtenidos no existen, mostramos mensaje para decir que hubo problemas al obtener los datos. Caso contrario, limpiamos tablas y cargamos los datos obtenidos desde el servidor en las tablas 
             */
            var ID_433094214_opts = [L('x1518866076_traducir', 'Aceptar')];
            var ID_433094214 = Ti.UI.createAlertDialog({
                title: L('x57652245_traducir', 'Alerta'),
                message: L('x1693828390_traducir', 'Problema de conexión por favor intentar después'),
                buttonNames: ID_433094214_opts
            });
            ID_433094214.addEventListener('click', function(e) {
                var suu = ID_433094214_opts[e.index];
                suu = null;
                e.source.removeEventListener("click", arguments.callee);
            });
            ID_433094214.show();
        } else {
            var ID_1183843717_i = Alloy.Collections.pais;
            var sql = "DELETE FROM " + ID_1183843717_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_1183843717_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_1183843717_i.trigger('delete');
            var ID_900784705_i = Alloy.Collections.nivel1;
            var sql = "DELETE FROM " + ID_900784705_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_900784705_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_900784705_i.trigger('delete');
            var ID_524627090_i = Alloy.Collections.experiencia_oficio;
            var sql = "DELETE FROM " + ID_524627090_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_524627090_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_524627090_i.trigger('delete');
            var elemento_regiones = elemento.regiones;
            var ID_1042041424_m = Alloy.Collections.nivel1;
            var db_ID_1042041424 = Ti.Database.open(ID_1042041424_m.config.adapter.db_name);
            db_ID_1042041424.execute('BEGIN');
            _.each(elemento_regiones, function(ID_1042041424_fila, pos) {
                db_ID_1042041424.execute('INSERT INTO nivel1 (id_label, id_server, dif_horaria, id_pais) VALUES (?,?,?,?)', ID_1042041424_fila.subdivision_name, ID_1042041424_fila.id, 2, ID_1042041424_fila.id_pais);
            });
            db_ID_1042041424.execute('COMMIT');
            db_ID_1042041424.close();
            db_ID_1042041424 = null;
            ID_1042041424_m.trigger('change');
            var elemento_paises = elemento.paises;
            var ID_711357707_m = Alloy.Collections.pais;
            var db_ID_711357707 = Ti.Database.open(ID_711357707_m.config.adapter.db_name);
            db_ID_711357707.execute('BEGIN');
            _.each(elemento_paises, function(ID_711357707_fila, pos) {
                db_ID_711357707.execute('INSERT INTO pais (label_nivel2, moneda, nombre, label_nivel4, label_codigo_identificador, label_nivel3, id_server, label_nivel1, iso, sis_metrico, niveles_pais, id_pais, label_nivel5, lenguaje) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)', ID_711357707_fila.nivel_2, ID_711357707_fila.moneda, ID_711357707_fila.nombre, ID_711357707_fila.nivel_4, ID_711357707_fila.label_codigo_identificador, ID_711357707_fila.nivel_3, ID_711357707_fila.id, ID_711357707_fila.nivel_1, ID_711357707_fila.iso, ID_711357707_fila.sis_metrico, ID_711357707_fila.niveles_pais, ID_711357707_fila.idpais, ID_711357707_fila.nivel_5, ID_711357707_fila.lenguaje);
            });
            db_ID_711357707.execute('COMMIT');
            db_ID_711357707.close();
            db_ID_711357707 = null;
            ID_711357707_m.trigger('change');
            var elemento_experiencia_oficio = elemento.experiencia_oficio;
            var ID_1581092805_m = Alloy.Collections.experiencia_oficio;
            var db_ID_1581092805 = Ti.Database.open(ID_1581092805_m.config.adapter.db_name);
            db_ID_1581092805.execute('BEGIN');
            _.each(elemento_experiencia_oficio, function(ID_1581092805_fila, pos) {
                db_ID_1581092805.execute('INSERT INTO experiencia_oficio (nombre, id_server, id_pais) VALUES (?,?,?)', ID_1581092805_fila.nombre, ID_1581092805_fila.id, ID_1581092805_fila.idpais);
            });
            db_ID_1581092805.execute('COMMIT');
            db_ID_1581092805.close();
            db_ID_1581092805 = null;
            ID_1581092805_m.trigger('change');
        }
        elemento = null, valor = null;
    };
    ID_244825631.error = function(e) {
        var elemento = e,
            valor = e;
        var ID_370190194_opts = [L('x1518866076_traducir', 'Aceptar')];
        var ID_370190194 = Ti.UI.createAlertDialog({
            title: L('x57652245_traducir', 'Alerta'),
            message: L('x1693828390_traducir', 'Problema de conexión por favor intentar después'),
            buttonNames: ID_370190194_opts
        });
        ID_370190194.addEventListener('click', function(e) {
            var suu = ID_370190194_opts[e.index];
            suu = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_370190194.show();
        elemento = null, valor = null;
    };
    require('helper').ajaxUnico('ID_244825631', '' + String.format(L('x2963862531', '%1$sobtenerPais'), url_server.toString()) + '', 'POST', {}, 15000, ID_244825631);
})();

function Postlayout_ID_1488112593(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    /** 
     * Una vez ya cargada la tarea, dejamos la variable limpia para poder abrir otras pantallas 
     */
    require('vars')['var_abriendo'] = '';
    var ID_1268946757_i = Alloy.createCollection('inspectores');
    var ID_1268946757_i_where = '';
    ID_1268946757_i.fetch();
    var inspec_pais = require('helper').query2array(ID_1268946757_i);
    if (_.isNumber(inspec_pais[0].pais)) {
        if (Ti.App.deployType != 'production') console.log('estamos en condicion', {
            "datos": inspec_pais[0].pais,
        });
        var picker;
        picker = $.ID_1260310979;
        picker.setSelectedRow(0, (inspec_pais[0].pais - 1));
    }

}
if (OS_IOS || OS_ANDROID) {
    $.ID_1385093082.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}
//$.ID_1385093082.open();