var _bind4section = {};
var _list_templates = {
    "tarea_historia": {
        "ID_1680574104": {
            "text": "{comuna}"
        },
        "ID_1304747472": {},
        "ID_676600495": {},
        "ID_1147033032": {},
        "ID_1810186930": {
            "text": "{hora_termino}"
        },
        "ID_911340427": {},
        "ID_1680180472": {
            "visible": "{bt_enviartarea}"
        },
        "ID_452289613": {},
        "ID_930474704": {
            "text": "{direccion}"
        },
        "ID_428063592": {
            "idlocal": "{id}",
            "estado": "{estado_tarea}"
        },
        "ID_1404040740": {},
        "ID_1184366498": {},
        "ID_459591478": {
            "text": "{ciudad}, {pais}"
        },
        "ID_362564056": {
            "visible": "{enviando_tarea}"
        },
        "ID_1887826074": {},
        "ID_165401298": {},
        "ID_1987088054": {},
        "ID_500305974": {},
        "ID_404048727": {}
    }
};

var _activity;
if (OS_ANDROID) {
    _activity = $.ID_310021856.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_310021856';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_310021856_window.addEventListener('open', function(e) {
        abx.setStatusbarColor("#FFFFFF");
        abx.setBackgroundColor("#ffffff");
    });
}

function Touchstart_ID_1520008950(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    $.ID_343670043.setColor('#ff0033');


}

function Touchend_ID_1184881792(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    $.ID_343670043.setColor('#2d9edb');
    if (false) console.log('agregado refrescar el badge historial', {});
    var ID_1814440833 = null;
    Alloy.Events.trigger('_refrescar_tareas');
    $.ID_310021856.close();

}

function Click_ID_855562006(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var ID_8660388_visible = true;

    if (ID_8660388_visible == 'si') {
        ID_8660388_visible = true;
    } else if (ID_8660388_visible == 'no') {
        ID_8660388_visible = false;
    }
    $.ID_8660388.setVisible(ID_8660388_visible);

    require('vars')['enviando_inspecciones'] = L('x4261170317', 'true');
    /** 
     * Ocultamos enviar todos 
     */
    var ID_609693366_visible = false;

    if (ID_609693366_visible == 'si') {
        ID_609693366_visible = true;
    } else if (ID_609693366_visible == 'no') {
        ID_609693366_visible = false;
    }
    $.ID_609693366.setVisible(ID_609693366_visible);

    /** 
     * Ocultamos cerrar historial 
     */
    var ID_743962856_visible = false;

    if (ID_743962856_visible == 'si') {
        ID_743962856_visible = true;
    } else if (ID_743962856_visible == 'no') {
        ID_743962856_visible = false;
    }
    $.ID_743962856.setVisible(ID_743962856_visible);


}

function Itemclick_ID_795143143(e) {

    e.cancelBubble = true;
    var objeto = e.section.getItemAt(e.itemIndex);
    var modelo = {},
        _modelo = [];
    var fila = {},
        fila_bak = {},
        info = {
            _template: objeto.template,
            _what: [],
            _seccion_ref: e.section.getHeaderTitle(),
            _model_id: -1
        },
        _tmp = {
            objmap: {}
        };
    if ('itemId' in e) {
        info._model_id = e.itemId;
        modelo._id = info._model_id;
        if (info._seccion_ref != '' && info._seccion_ref in _bind4section) {
            modelo._collection = _bind4section[info._seccion_ref];
            _tmp._coll = modelo._collection;
        }
    }
    if (info._seccion_ref != '' && info._seccion_ref in _bind4section) {
        if (Alloy.Collections[_tmp._coll].config.adapter.type == 'sql') {
            _tmp._inst = Alloy.Collections[_tmp._coll];
            _tmp._id = Alloy.Collections[_tmp._coll].config.adapter.idAttribute;
            _tmp._dbsql = 'SELECT * FROM ' + Alloy.Collections[_tmp._coll].config.adapter.collection_name + ' WHERE ' + _tmp._id + ' = ' + e.itemId;
            _tmp._db = Ti.Database.open(Alloy.Collections[_tmp._coll].config.adapter.db_name);
            _modelo = _tmp._db.execute(_tmp._dbsql);
            var values = [],
                fieldNames = [];
            var fieldCount = _.isFunction(_modelo.fieldCount) ? _modelo.fieldCount() : _modelo.fieldCount;
            var getField = _modelo.field;
            var i = 0;
            for (; fieldCount > i; i++) fieldNames.push(_modelo.fieldName(i));
            while (_modelo.isValidRow()) {
                var o = {};
                for (i = 0; fieldCount > i; i++) o[fieldNames[i]] = getField(i);
                values.push(o);
                _modelo.next();
            }
            _modelo = values;
            _tmp._db.close();
        } else {
            _tmp._search = {};
            _tmp._search[_tmp._id] = e.itemId + '';
            _modelo = Alloy.Collections[_tmp._coll].fetch(_tmp._search);
        }
    }
    var findVariables = require('fvariables');
    _.each(_list_templates[info._template], function(obj_id, id) {
        _.each(obj_id, function(valor, prop) {
            var llaves = findVariables(valor, '{', '}');
            _.each(llaves, function(llave) {
                _tmp.objmap[llave] = {
                    id: id,
                    prop: prop
                };
                fila[llave] = objeto[id][prop];
                if (id == e.bindId) info._what.push(llave);
            });
        });
    });
    info._what = info._what.join(',');
    fila_bak = JSON.parse(JSON.stringify(fila));
    if (fila.bt_enviartarea == true || fila.bt_enviartarea == 'true') {
        /** 
         * Solo ejecutamos si esta el boton 
         */
        var fila = _.extend(fila, {
            enviando_tarea: L('x4261170317', 'true'),
            bt_enviartarea: L('x734881840_traducir', 'false')
        });
        require('vars')[_var_scopekey]['enviar_id'] = fila.id;
        if (false) console.log('fila tarea por enviar pinchada', {
            "fila": fila
        });
        /** 
         * Modificamos registro en tabla por enviando tarea 
         */
        var ID_356907424_i = Alloy.createCollection('historial_tareas');
        var ID_356907424_i_where = 'id=\'' + fila.id + '\'';
        ID_356907424_i.fetch({
            query: 'SELECT * FROM historial_tareas WHERE id=\'' + fila.id + '\''
        });
        var tareaclick = require('helper').query2array(ID_356907424_i);
        var db = Ti.Database.open(ID_356907424_i.config.adapter.db_name);
        if (ID_356907424_i_where == '') {
            var sql = 'UPDATE ' + ID_356907424_i.config.adapter.collection_name + ' SET estado_envio=0';
        } else {
            var sql = 'UPDATE ' + ID_356907424_i.config.adapter.collection_name + ' SET estado_envio=0 WHERE ' + ID_356907424_i_where;
        }
        db.execute(sql);
        db.close();
        require('vars')['enviando_inspecciones'] = L('x4261170317', 'true');
        /** 
         * Mostramos popup 
         */
        var ID_8660388_visible = true;

        if (ID_8660388_visible == 'si') {
            ID_8660388_visible = true;
        } else if (ID_8660388_visible == 'no') {
            ID_8660388_visible = false;
        }
        $.ID_8660388.setVisible(ID_8660388_visible);

        var ID_609693366_visible = false;

        if (ID_609693366_visible == 'si') {
            ID_609693366_visible = true;
        } else if (ID_609693366_visible == 'no') {
            ID_609693366_visible = false;
        }
        $.ID_609693366.setVisible(ID_609693366_visible);

        var ID_743962856_visible = false;

        if (ID_743962856_visible == 'si') {
            ID_743962856_visible = true;
        } else if (ID_743962856_visible == 'no') {
            ID_743962856_visible = false;
        }
        $.ID_743962856.setVisible(ID_743962856_visible);

    }
    _tmp.changed = false;
    _tmp.diff_keys = [];
    _.each(fila, function(value1, prop) {
        var had_samekey = false;
        _.each(fila_bak, function(value2, prop2) {
            if (prop == prop2 && value1 == value2) {
                had_samekey = true;
            } else if (!_.has(fila_bak, prop) || !_.has(fila, prop2)) {
                has_samekey = true;
            }
        });
        if (!had_samekey) _tmp.diff_keys.push(prop);
    });
    if (_tmp.diff_keys.length > 0) _tmp.changed = true;
    if (_tmp.changed == true) {
        _.each(_tmp.diff_keys, function(llave) {
            objeto[_tmp.objmap[llave].id][_tmp.objmap[llave].prop] = fila[llave];
        });
        e.section.updateItemAt(e.itemIndex, objeto);
    }

}

function Load_ID_1020345449(e) {
    /** 
     * Evento que se ejecuta una vez cargada la vista 
     */

    e.cancelBubble = true;
    var elemento = e.source;
    var evento = e;
    /** 
     * Hace que la animacion se ejecute 
     */
    elemento.start();

}

function Longpress_ID_1959077469(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var ID_757532626_opts = [L('x1518866076_traducir', 'Aceptar'), L('x2376009830_traducir', ' Cancelar')];
    var ID_757532626 = Ti.UI.createAlertDialog({
        title: L('x3836149088_traducir', 'Abortar envio'),
        message: L('x4264692327_traducir', '¿ Esta seguro que desea cancelar los envios ?'),
        buttonNames: ID_757532626_opts
    });
    ID_757532626.addEventListener('click', function(e) {
        var seguro = ID_757532626_opts[e.index];
        if (seguro == L('x1518866076_traducir', 'Aceptar')) {
            require('vars')['enviando_inspecciones'] = L('x734881840_traducir', 'false');
            var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
            /** 
             * Desactivamos estado_envio de todas las tareas de este inspector 
             */
            var ID_198256704_i = Alloy.createCollection('historial_tareas');
            var ID_198256704_i_where = 'id_inspector=\'' + inspector.id_server + '\'';
            ID_198256704_i.fetch({
                query: 'SELECT * FROM historial_tareas WHERE id_inspector=\'' + inspector.id_server + '\''
            });
            var cancelar_envios = require('helper').query2array(ID_198256704_i);
            var db = Ti.Database.open(ID_198256704_i.config.adapter.db_name);
            if (ID_198256704_i_where == '') {
                var sql = 'UPDATE ' + ID_198256704_i.config.adapter.collection_name + ' SET estado_envio=0';
            } else {
                var sql = 'UPDATE ' + ID_198256704_i.config.adapter.collection_name + ' SET estado_envio=0 WHERE ' + ID_198256704_i_where;
            }
            db.execute(sql);
            db.close();
            var ID_1279375049 = null;
            if ('refrescar_tareas' in require('funciones')) {
                ID_1279375049 = require('funciones').refrescar_tareas({});
            } else {
                try {
                    ID_1279375049 = f_refrescar_tareas({});
                } catch (ee) {}
            }
            /** 
             * Ocultamos popup 
             */
            var ID_8660388_visible = false;

            if (ID_8660388_visible == 'si') {
                ID_8660388_visible = true;
            } else if (ID_8660388_visible == 'no') {
                ID_8660388_visible = false;
            }
            $.ID_8660388.setVisible(ID_8660388_visible);

            var ID_609693366_visible = true;

            if (ID_609693366_visible == 'si') {
                ID_609693366_visible = true;
            } else if (ID_609693366_visible == 'no') {
                ID_609693366_visible = false;
            }
            $.ID_609693366.setVisible(ID_609693366_visible);

            var ID_743962856_visible = true;

            if (ID_743962856_visible == 'si') {
                ID_743962856_visible = true;
            } else if (ID_743962856_visible == 'no') {
                ID_743962856_visible = false;
            }
            $.ID_743962856.setVisible(ID_743962856_visible);

            var porce = 0;

        }
        seguro = null;

        e.source.removeEventListener("click", arguments.callee);
    });
    ID_757532626.show();

}

var f_refrescar_tareas = function(x_params) {
    var xyz = x_params['xyz'];
    /** 
     * Limpiamos la tabla de tareas 
     */
    var ID_513906812_borrar = false;

    var limpiarListado = function(animar) {
        var a_nimar = (typeof animar == 'undefined') ? false : animar;
        if (OS_IOS && a_nimar == true) {
            var s_ecciones = $.ID_513906812.getSections();
            _.each(s_ecciones, function(obj_id, pos) {
                $.ID_513906812.deleteSectionAt(0, {
                    animated: true
                });
            });
        } else {
            $.ID_513906812.setSections([]);
        }
    };
    limpiarListado(ID_513906812_borrar);

    var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
    /** 
     * Consultamos esta tabla para ir ingresando las tareas que estan pendientes de envio, por orden de fecha descendiente 
     */
    var ID_1217690775_i = Alloy.createCollection('historial_tareas');
    var ID_1217690775_i_where = 'ORDER BY FECHA_TERMINO DESC';
    ID_1217690775_i.fetch({
        query: 'SELECT * FROM historial_tareas ORDER BY FECHA_TERMINO DESC'
    });
    var tareas = require('helper').query2array(ID_1217690775_i);
    /** 
     * Inicializamos la variable ultima_fecha en vacio, esto servira por si encontramos una tarea en la misma fecha y la podremos guardar en la misma seccion que la tarea anterior (si fueron realizadas el mismo dia) 
     */
    require('vars')[_var_scopekey]['ultima_fecha'] = '';
    var tarea_index = 0;
    _.each(tareas, function(tarea, tarea_pos, tarea_list) {
        tarea_index += 1;
        /** 
         * Recuperamos la variable para ver si la tarea anterior tiene la misma fecha que la tarea que estamos recorriendo 
         */
        var ultima_fecha = ('ultima_fecha' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['ultima_fecha'] : '';
        /** 
         * Formateamos fecha para mostrar en pantalla 
         */
        var moment = require('alloy/moment');
        var fecha_hoy = moment(new Date()).format('YYYY-MM-DD');
        /** 
         * Creamos variable ayer con la fecha del dia de ayer (segun la que tenga el smartphone) 
         */
        var ayer = new Date();
        ayer.setDate(ayer.getDate() - 1);;
        /** 
         * Formateamos la fecha de ayer para mostrar en pantalla 
         */
        var moment = require('alloy/moment');
        var ID_1033057419 = ayer;
        var fecha_ayer = moment(ID_1033057419).format('YYYY-MM-DD');
        if (ultima_fecha != tarea.fecha_termino) {
            /** 
             * Revisamos la fecha de la tarea, si es la misma que la anterior, no nos cambiamos de seccion, caso contrario vemos de que dia es y dejamos esa tarea en la fecha que corresponde 
             */
            if (tarea.fecha_termino == fecha_hoy) {
                var ID_1674875520 = Titanium.UI.createListSection({
                    headerTitle: L('x1916403066_traducir', 'hoy')
                });
                var ID_1014132452 = Titanium.UI.createView({
                    height: '25dp',
                    width: Ti.UI.FILL
                });
                var ID_852194385 = Titanium.UI.createView({
                    height: '25dp',
                    layout: 'vertical',
                    width: Ti.UI.FILL,
                    backgroundColor: '#F7F7F7'
                });
                var ID_1501484936 = Titanium.UI.createLabel({
                    text: L('x3835609072_traducir', 'HOY'),
                    color: '#999999',
                    touchEnabled: false,
                    font: {
                        fontFamily: 'SFUIText-Medium',
                        fontSize: '14dp'
                    }

                });
                ID_852194385.add(ID_1501484936);

                ID_1014132452.add(ID_852194385);
                ID_1674875520.setHeaderView(ID_1014132452);
                $.ID_513906812.appendSection(ID_1674875520);
                require('vars')[_var_scopekey]['ultima_fecha'] = tarea.fecha_termino;
            } else if (tarea.fecha_termino == fecha_ayer) {
                var ID_336997000 = Titanium.UI.createListSection({
                    headerTitle: L('x1602196311_traducir', 'ayer')
                });
                var ID_223075789 = Titanium.UI.createView({
                    height: '25dp',
                    width: Ti.UI.FILL
                });
                var ID_937521382 = Titanium.UI.createView({
                    height: '25dp',
                    layout: 'vertical',
                    width: Ti.UI.FILL,
                    backgroundColor: '#F7F7F7'
                });
                var ID_474569346 = Titanium.UI.createLabel({
                    text: L('x1776975587_traducir', 'AYER'),
                    color: '#999999',
                    touchEnabled: false,
                    font: {
                        fontFamily: 'SFUIText-Medium',
                        fontSize: '14dp'
                    }

                });
                ID_937521382.add(ID_474569346);

                ID_223075789.add(ID_937521382);
                ID_336997000.setHeaderView(ID_223075789);
                $.ID_513906812.appendSection(ID_336997000);
                require('vars')[_var_scopekey]['ultima_fecha'] = tarea.fecha_termino;
            } else {
                var ID_1645691087 = Titanium.UI.createListSection({
                    headerTitle: L('x910537343_traducir', 'otra')
                });
                var ID_801056720 = Titanium.UI.createView({
                    height: '25dp',
                    width: Ti.UI.FILL
                });
                var ID_1373657437 = Titanium.UI.createView({
                    height: '25dp',
                    layout: 'vertical',
                    width: Ti.UI.FILL,
                    backgroundColor: '#F7F7F7'
                });
                var ID_1072597236 = Titanium.UI.createLabel({
                    text: tarea.fecha_termino,
                    color: '#999999',
                    touchEnabled: false,
                    font: {
                        fontFamily: 'SFUIText-Medium',
                        fontSize: '14dp'
                    }

                });
                ID_1373657437.add(ID_1072597236);

                ID_801056720.add(ID_1373657437);
                ID_1645691087.setHeaderView(ID_801056720);
                $.ID_513906812.appendSection(ID_1645691087);
                require('vars')[_var_scopekey]['ultima_fecha'] = tarea.fecha_termino;
            }
        }
        if ((_.isObject(tarea.nivel_2) || _.isString(tarea.nivel_2)) && _.isEmpty(tarea.nivel_2)) {
            /** 
             * Define ultimo nivel de la tarea 
             */
            var tarea = _.extend(tarea, {
                ultimo_nivel: tarea.nivel_1
            });
        } else if ((_.isObject(tarea.nivel_3) || _.isString(tarea.nivel_3)) && _.isEmpty(tarea.nivel_3)) {
            var tarea = _.extend(tarea, {
                ultimo_nivel: tarea.nivel_2
            });
        } else if ((_.isObject(tarea.nivel_4) || _.isString(tarea.nivel_4)) && _.isEmpty(tarea.nivel_4)) {
            var tarea = _.extend(tarea, {
                ultimo_nivel: tarea.nivel_3
            });
        } else if ((_.isObject(tarea.nivel_5) || _.isString(tarea.nivel_5)) && _.isEmpty(tarea.nivel_5)) {
            var tarea = _.extend(tarea, {
                ultimo_nivel: tarea.nivel_4
            });
        } else {
            var tarea = _.extend(tarea, {
                ultimo_nivel: tarea.nivel_5
            });
        }
        /** 
         * Formateamos hora_termino de hh:mm:ss a HH:MM hrs en la variable de tarea 
         */
        var hora_t = tarea.hora_termino.split(':');
        /** 
         * Actualizamos la variable de la tarea con el estado de bt_enviartarea, enviando_tarea y la hora de termino 
         */
        var tarea = _.extend(tarea, {
            bt_enviartarea: L('x734881840_traducir', 'false'),
            enviando_tarea: L('x734881840_traducir', 'false'),
            hora_termino: String.format(L('x1815173162_traducir', '%1$s:%2$shrs'), (hora_t[0]) ? hora_t[0].toString() : '', (hora_t[1]) ? hora_t[1].toString() : '')
        });
        if (tarea.estado_tarea == 8) {
            /** 
             * Revisamos el estado de la tarea 
             */
            /** 
             * Revisamos que el directorio de la inspeccion exista en el telefono y tenga archivos 
             */
            var tarea = _.extend(tarea, {
                bt_enviartarea: L('x4261170317', 'true'),
                enviando_tarea: L('x734881840_traducir', 'false')
            });
        } else if (tarea.estado_tarea == 9) {
            /** 
             * Revisamos que el directorio de la inspeccion exista en el telefono y tenga archivos 
             */
            var tarea = _.extend(tarea, {
                bt_enviartarea: L('x4261170317', 'true'),
                enviando_tarea: L('x734881840_traducir', 'false')
            });
        }
        if (tarea.bt_enviartarea == true || tarea.bt_enviartarea == 'true') {
            if (tarea.estado_envio == 0 || tarea.estado_envio == '0') {
                /** 
                 * Revisamos que el equipo tenga imagenes guardadas para la tarea actual 
                 */

                var fotos = [];
                var ID_1688832049_f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + tarea.id_server + '/');
                if (ID_1688832049_f.exists() == true) {
                    fotos = ID_1688832049_f.getDirectoryListing();
                }
                if (false) console.log(String.format(L('x2358582387_traducir', 'probando si hay fotos para id %1$s'), tarea.id_server.toString()), {
                    "fotos": fotos
                });
                if (fotos && fotos.length) {
                    /** 
                     * Cambiamos el estado de bt_enviartarea y enviando_tarea 
                     */
                    var tarea = _.extend(tarea, {
                        bt_enviartarea: L('x4261170317', 'true'),
                        enviando_tarea: L('x734881840_traducir', 'false')
                    });
                } else {
                    var tarea = _.extend(tarea, {
                        bt_enviartarea: L('x734881840_traducir', 'false'),
                        enviando_tarea: L('x734881840_traducir', 'false')
                    });
                }
            } else if (tarea.estado_envio == 1 || tarea.estado_envio == '1') {
                /** 
                 * Cambiamos el estado de bt_enviartarea y enviando_tarea 
                 */
                var tarea = _.extend(tarea, {
                    bt_enviartarea: L('x734881840_traducir', 'false'),
                    enviando_tarea: L('x4261170317', 'true')
                });
            } else {
                /** 
                 * Caso estado:null 
                 */
                /** 
                 * Revisamos que el equipo tenga imagenes guardadas para la tarea actual 
                 */

                var fotos = [];
                var ID_1025235198_f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + tarea.id_server + '/');
                if (ID_1025235198_f.exists() == true) {
                    fotos = ID_1025235198_f.getDirectoryListing();
                }
                if (false) console.log(String.format(L('x2358582387_traducir', 'probando si hay fotos para id %1$s'), tarea.id_server.toString()), {
                    "fotos": fotos
                });
                if (fotos && fotos.length) {
                    /** 
                     * Cambiamos el estado de bt_enviartarea y enviando_tarea 
                     */
                    var tarea = _.extend(tarea, {
                        bt_enviartarea: L('x4261170317', 'true'),
                        enviando_tarea: L('x734881840_traducir', 'false')
                    });
                } else {
                    var tarea = _.extend(tarea, {
                        bt_enviartarea: L('x734881840_traducir', 'false'),
                        enviando_tarea: L('x734881840_traducir', 'false')
                    });
                }
            }
        }
        if (tarea.fecha_termino == fecha_hoy) {
            /** 
             * Agregamos items tareas a la seccion de las tareas del dia de hoy, ayer u otra fecha 
             */
            var ID_523805973 = [{
                ID_1680574104: {
                    text: tarea.ultimo_nivel
                },
                ID_1304747472: {},
                ID_676600495: {},
                ID_1147033032: {},
                ID_1810186930: {
                    text: tarea.hora_termino
                },
                ID_911340427: {},
                ID_1680180472: {
                    visible: tarea.bt_enviartarea
                },
                ID_452289613: {},
                ID_930474704: {
                    text: tarea.direccion
                },
                ID_428063592: {
                    idlocal: tarea.id_server,
                    estado: tarea.estado_tarea
                },
                ID_1404040740: {},
                template: 'tarea_historia',
                ID_1184366498: {},
                ID_459591478: {
                    text: String.format(L('x1487588533', '%1$s, %2$s'), (tarea.nivel_2) ? tarea.nivel_2.toString() : '', (tarea.pais) ? tarea.pais.toString() : '')
                },
                ID_362564056: {
                    visible: tarea.enviando_tarea
                },
                ID_1887826074: {},
                ID_165401298: {},
                ID_1987088054: {},
                ID_500305974: {},
                ID_404048727: {}

            }];
            var ID_523805973_secs = {};
            _.map($.ID_513906812.getSections(), function(ID_523805973_valor, ID_523805973_indice) {
                ID_523805973_secs[ID_523805973_valor.getHeaderTitle()] = ID_523805973_indice;
                return ID_523805973_valor;
            });
            if ('' + L('x1916403066_traducir', 'hoy') + '' in ID_523805973_secs) {
                $.ID_513906812.sections[ID_523805973_secs['' + L('x1916403066_traducir', 'hoy') + '']].appendItems(ID_523805973);
            } else {
                console.log('DEBUG:dynamic listItem points to a invalid section id: check attribute _seccion.');
            }
        } else if (tarea.fecha_termino == fecha_ayer) {
            var ID_1428215143 = [{
                ID_1680574104: {
                    text: tarea.ultimo_nivel
                },
                ID_1304747472: {},
                ID_676600495: {},
                ID_1147033032: {},
                ID_1810186930: {
                    text: tarea.hora_termino
                },
                ID_911340427: {},
                ID_1680180472: {
                    visible: tarea.bt_enviartarea
                },
                ID_452289613: {},
                ID_930474704: {
                    text: tarea.direccion
                },
                ID_428063592: {
                    idlocal: tarea.id_server,
                    estado: tarea.estado_tarea
                },
                ID_1404040740: {},
                template: 'tarea_historia',
                ID_1184366498: {},
                ID_459591478: {
                    text: String.format(L('x1487588533', '%1$s, %2$s'), (tarea.nivel_2) ? tarea.nivel_2.toString() : '', (tarea.pais) ? tarea.pais.toString() : '')
                },
                ID_362564056: {
                    visible: tarea.enviando_tarea
                },
                ID_1887826074: {},
                ID_165401298: {},
                ID_1987088054: {},
                ID_500305974: {},
                ID_404048727: {}

            }];
            var ID_1428215143_secs = {};
            _.map($.ID_513906812.getSections(), function(ID_1428215143_valor, ID_1428215143_indice) {
                ID_1428215143_secs[ID_1428215143_valor.getHeaderTitle()] = ID_1428215143_indice;
                return ID_1428215143_valor;
            });
            if ('' + L('x1602196311_traducir', 'ayer') + '' in ID_1428215143_secs) {
                $.ID_513906812.sections[ID_1428215143_secs['' + L('x1602196311_traducir', 'ayer') + '']].appendItems(ID_1428215143);
            } else {
                console.log('DEBUG:dynamic listItem points to a invalid section id: check attribute _seccion.');
            }
        } else {
            var ID_831455466 = [{
                ID_1680574104: {
                    text: tarea.ultimo_nivel
                },
                ID_1304747472: {},
                ID_676600495: {},
                ID_1147033032: {},
                ID_1810186930: {
                    text: tarea.hora_termino
                },
                ID_911340427: {},
                ID_1680180472: {
                    visible: tarea.bt_enviartarea
                },
                ID_452289613: {},
                ID_930474704: {
                    text: tarea.direccion
                },
                ID_428063592: {
                    idlocal: tarea.id_server,
                    estado: tarea.estado_tarea
                },
                ID_1404040740: {},
                template: 'tarea_historia',
                ID_1184366498: {},
                ID_459591478: {
                    text: String.format(L('x1487588533', '%1$s, %2$s'), (tarea.nivel_2) ? tarea.nivel_2.toString() : '', (tarea.pais) ? tarea.pais.toString() : '')
                },
                ID_362564056: {
                    visible: tarea.enviando_tarea
                },
                ID_1887826074: {},
                ID_165401298: {},
                ID_1987088054: {},
                ID_500305974: {},
                ID_404048727: {}

            }];
            var ID_831455466_secs = {};
            _.map($.ID_513906812.getSections(), function(ID_831455466_valor, ID_831455466_indice) {
                ID_831455466_secs[ID_831455466_valor.getHeaderTitle()] = ID_831455466_indice;
                return ID_831455466_valor;
            });
            if ('' + L('x910537343_traducir', 'otra') + '' in ID_831455466_secs) {
                $.ID_513906812.sections[ID_831455466_secs['' + L('x910537343_traducir', 'otra') + '']].appendItems(ID_831455466);
            } else {
                console.log('DEBUG:dynamic listItem points to a invalid section id: check attribute _seccion.');
            }
        }
        /** 
         * Limpiamos memoria 
         */
        fechahoy = null, ayer = null, fecha_ayer = null, hora_t = null;
    });
    /** 
     * Limpiar memoria 
     */
    tarea = null, tareas = null;
    return null;
};
var ep_enviarFoto = function(fotos, enviar_id, url_server) {
    qfoto = fotos[qfoto_index];
    var enviar_id = ('enviar_id' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['enviar_id'] : '';
    var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
    console.log('psb intentando enviar con desfase foto ' + qfoto_index + ' con datos', qfoto);
    var porce = Math.round(((qfoto_index) * 100) / fotos.length);
    $.ID_1344963939.setText('No cierre la aplicacion y asegurese de estar conectado a internet\nProgreso ' + porce + ' %');
    qfoto_index += 1;
    if (false) console.log('leyendo binario', {});
    /* Leemos la imagen y la almacenamos en variable */
    var firmabin = '';
    var ID_502219582_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + enviar_id);
    if (ID_502219582_d.exists() == true) {
        var ID_502219582_f = Ti.Filesystem.getFile(ID_502219582_d.resolve(), qfoto);
        if (ID_502219582_f.exists() == true) {
            firmabin = ID_502219582_f.read();
        }
        ID_502219582_f = null;
    }
    ID_502219582_d = null;
    if (false) console.log('enviando a servidor', {});
    /* Hacemos consulta al servidor mandando las fotos de la inspeccion */
    var resp = null;
    if (false) console.log('DEBUG WEB: requesting url:' + url_server + 'subirImagenes' + ' with data:', {
        _method: 'POST',
        _params: {
            id_tarea: enviar_id,
            archivo: qfoto,
            imagen: firmabin,
            app_id: qfoto + ',' + enviar_id
        },
        _timeout: '20000'
    });
    require('helper').ajaxUnico(qfoto + ',' + enviar_id, '' + url_server + 'subirImagenes' + '', 'POST', {
        id_tarea: enviar_id,
        archivo: qfoto,
        imagen: firmabin,
        app_id: qfoto + ',' + enviar_id
    }, 600000, {
        abort: function(elid) {
            console.log('psb se esta solicitando cancelar el envio del id ' + elid + ', porque aun no llega su exito previo, y re-llamando');
            return true;
        },
        success: function(x9) {
            resp = x9;
            console.log('psb respuesta bruta de subirImagenes recibida', resp);
            if (resp.error != 0 && resp.error != '0') {
                /* Si el servidor responde con error */
                if (false) console.log('fallo envio de imagen (en servidor)', {
                    "qfoto": qfoto,
                    "resp": resp
                });
                /* Recuperamos variable */
                var enviar_id = ('enviar_id' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['enviar_id'] : '';
                var pendientes = ('pendientes' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['pendientes'] : '';
                if (resp.mensaje.toLowerCase().indexOf('insertar la imagen'.toLowerCase()) != -1 ||
                    resp.mensaje.toLowerCase().indexOf('imagen no existe'.toLowerCase()) != -1) {
                    /* Revisamos la respuesta del servidor */
                    console.log('psb recibido error (ya existente o no registrada) al subirImagenes (enviar_id=' + enviar_id + '), borrando de equipo y marcando como enviada.');
                    /* imagen ya existe en servidor, borramos de equipo y marcamos como enviada */
                    var info = resp.app_id.split(',');
                    /* Borramos archivo */
                    var ID_1001674794_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + info[1]);
                    var ID_1001674794_f = Ti.Filesystem.getFile(ID_1001674794_d.resolve(), info[0]);
                    if (ID_1001674794_f.exists() == true) ID_1001674794_f.deleteFile();
                    if (false) console.log('foto enviada borrada de equipo', {
                        "info": info
                    });
                    /* Modificamos la tarea actualizando el estado de envio */
                    var ID_1936135963_i = Alloy.createCollection('historial_tareas');
                    var ID_1936135963_i_where = 'id=\'' + enviar_id + '\'';
                    ID_1936135963_i.fetch({
                        query: 'SELECT * FROM historial_tareas WHERE id=\'' + enviar_id + '\''
                    });
                    var tareaclick = require('helper').query2array(ID_1936135963_i);
                    var db = Ti.Database.open(ID_1936135963_i.config.adapter.db_name);
                    if (ID_1936135963_i_where == '') {
                        var sql = 'UPDATE ' + ID_1936135963_i.config.adapter.collection_name + ' SET estado_envio=0';
                    } else {
                        var sql = 'UPDATE ' + ID_1936135963_i.config.adapter.collection_name + ' SET estado_envio=0 WHERE ' + ID_1936135963_i_where;
                    }
                    db.execute(sql);
                    if (ID_1936135963_i_where == '') {
                        var sql = 'UPDATE ' + ID_1936135963_i.config.adapter.collection_name + ' SET estado_tarea=10';
                    } else {
                        var sql = 'UPDATE ' + ID_1936135963_i.config.adapter.collection_name + ' SET estado_tarea=10 WHERE ' + ID_1936135963_i_where;
                    }
                    db.execute(sql);
                    db.close();
                } else {
                    /* otro error, modificamos la tarea actualizando el estado de envio */
                    var ID_1283904867_i = Alloy.createCollection('historial_tareas');
                    var ID_1283904867_i_where = 'id=\'' + enviar_id + '\'';
                    ID_1283904867_i.fetch({
                        query: 'SELECT * FROM historial_tareas WHERE id=\'' + enviar_id + '\''
                    });
                    var tareaclick = require('helper').query2array(ID_1283904867_i);
                    var db = Ti.Database.open(ID_1283904867_i.config.adapter.db_name);
                    if (ID_1283904867_i_where == '') {
                        var sql = 'UPDATE ' + ID_1283904867_i.config.adapter.collection_name + ' SET estado_envio=0';
                    } else {
                        var sql = 'UPDATE ' + ID_1283904867_i.config.adapter.collection_name + ' SET estado_envio=0 WHERE ' + ID_1283904867_i_where;
                    }
                    db.execute(sql);
                    db.close();
                    // borra enviar_id (tarea) cuya foto fallo.
                    require('vars')[_var_scopekey]['enviar_id'] = '';
                }
                if ((_.has(pendientes, enviar_id)) == true || (_.has(pendientes, enviar_id)) == 'true') {
                    /* Revisamos que el objeto pendientes contenga la propiedad enviar_id, si contiene, borramos la propiedad enviar_id del objeto y actualizamos */
                    /* Eliminamos la propiedad enviar_id del objeto pendientes y actualizamos la variable */
                    delete pendientes[enviar_id];
                    require('vars')[_var_scopekey]['pendientes'] = pendientes;
                }
                var pendientes = ('pendientes' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['pendientes'] : '';
                if ((Object.keys(pendientes).length) == 0 || (Object.keys(pendientes).length) == '0') {
                    /* Revisamos si el objeto tiene propiedades */
                    /* Tratamos de cancelar envio y quitamos popup y refrescamos listado */
                    require('vars')['enviando_inspecciones'] = 'false';
                    var ID_1867246625 = null;
                    if ('refrescar_tareas' in require('funciones')) {
                        ID_1867246625 = require('funciones').refrescar_tareas({});
                    } else {
                        try {
                            ID_1867246625 = f_refrescar_tareas({});
                        } catch (ee) {}
                    }

                    /* Ocultamos popup */
                    var ID_8660388_visible = false;

                    if (ID_8660388_visible == 'si') {
                        ID_8660388_visible = true;
                    } else if (ID_8660388_visible == 'no') {
                        ID_8660388_visible = false;
                    }
                    $.ID_8660388.setVisible(ID_8660388_visible);

                    /** 
                     * Mostramos cerrar historial 
                     */
                    var ID_743962856_visible = true;

                    if (ID_743962856_visible == 'si') {
                        ID_743962856_visible = true;
                    } else if (ID_743962856_visible == 'no') {
                        ID_743962856_visible = false;
                    }
                    $.ID_743962856.setVisible(ID_743962856_visible);
                    /* rervisamos si no queda ninguna imagen por enviar (desactivamos enviar_todos) */
                    var faltan = false;
                    /* 27-feb esto se podria mejorar, agregando soporte para filtrar por estado_tarea = 8,9 y contar los registros. */
                    var ID_1390575608_i = Alloy.createCollection('historial_tareas');
                    var ID_1390575608_i_where = '';
                    ID_1390575608_i.fetch();
                    var tareastest = require('helper').query2array(ID_1390575608_i);
                    var test_index = 0;
                    _.each(tareastest, function(test, test_pos, test_list) {
                        test_index += 1;
                        if (require('helper').arr_contains('8,9'.toLowerCase().split(','), test.estado_tarea.toString().toLowerCase())) {
                            /* Revisamos que el equipo tenga imagenes guardadas para la tarea actual y actualizamos la variable faltan */
                            var fotos = [];
                            var ID_668109677_f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + test.id_server + '/');
                            if (ID_668109677_f.exists() == true) {
                                fotos = ID_668109677_f.getDirectoryListing();
                            }

                            if (fotos && fotos.length == 0) {
                                faltan = true;
                            }
                        }
                    });
                    if (faltan == false || faltan == 'false') {
                        /** 
                         * Ocultamos enviar todos 
                         */
                        var ID_609693366_visible = false;

                        if (ID_609693366_visible == 'si') {
                            ID_609693366_visible = true;
                        } else if (ID_609693366_visible == 'no') {
                            ID_609693366_visible = false;
                        }
                        $.ID_609693366.setVisible(ID_609693366_visible);
                        if (false) console.log('esconder en la consulta web', {});
                    }
                    /* esto es para refrescar badge de pantalla perfil */
                    Alloy.Events.trigger('refrescar_historial');
                }
            } else if (_.has(resp, "app_id")) {
                var pendientes = ('pendientes' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['pendientes'] : '';
                var info = resp.app_id.split(',');
                var ID_1203936130_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + info[1]);
                var ID_1203936130_f = Ti.Filesystem.getFile(ID_1203936130_d.resolve(), info[0]);
                if (ID_1203936130_f.exists() == true) ID_1203936130_f.deleteFile();
                if (false) console.log('foto enviada borrada de equipo', {
                    "info": info
                });
                var testmas = [];
                var ID_1036625800_f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + info[1] + '/');
                if (ID_1036625800_f.exists() == true) {
                    testmas = ID_1036625800_f.getDirectoryListing();
                }
                var ID_985199740_i = Alloy.createCollection('historial_tareas');
                var ID_985199740_i_where = 'id=\'' + info[1] + '\'';
                ID_985199740_i.fetch({
                    query: 'SELECT * FROM historial_tareas WHERE id=\'' + info[1] + '\''
                });
                var tareaclick = require('helper').query2array(ID_985199740_i);
                var db = Ti.Database.open(ID_985199740_i.config.adapter.db_name);
                if (ID_985199740_i_where == '') {
                    var sql = 'UPDATE ' + ID_985199740_i.config.adapter.collection_name + ' SET estado_envio=0';
                } else {
                    var sql = 'UPDATE ' + ID_985199740_i.config.adapter.collection_name + ' SET estado_envio=0 WHERE ' + ID_985199740_i_where;
                }
                db.execute(sql);
                if (ID_985199740_i_where == '') {
                    var sql = 'UPDATE ' + ID_985199740_i.config.adapter.collection_name + ' SET estado_tarea=10';
                } else {
                    var sql = 'UPDATE ' + ID_985199740_i.config.adapter.collection_name + ' SET estado_tarea=10 WHERE ' + ID_985199740_i_where;
                }
                db.execute(sql);
                db.close();
                info = null;
                if ((_.has(pendientes, enviar_id)) == true || (_.has(pendientes, enviar_id)) == 'true') {
                    delete pendientes[enviar_id];
                    require('vars')[_var_scopekey]['pendientes'] = pendientes;
                }
                var pendientes = ('pendientes' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['pendientes'] : '';
                if ((Object.keys(pendientes).length) == 0 || (Object.keys(pendientes).length) == '0') {
                    /* Revisamos que pendientes no tenga mas registros pendientes */
                    if (testmas && testmas.length == 0) {
                        if (false) console.log('no hay mas imagenes para tarea activa, desactivamos popup', {});
                        /* Recuperamos variables */
                        require('vars')[_var_scopekey]['enviar_id'] = '';
                        require('vars')['enviando_inspecciones'] = 'false';
                        /* Refrescamos la lista de tareas */
                        var ID_1985286020 = null;
                        if ('refrescar_tareas' in require('funciones')) {
                            ID_1985286020 = require('funciones').refrescar_tareas({});
                        } else {
                            try {
                                ID_1985286020 = f_refrescar_tareas({});
                            } catch (ee) {}
                        }
                        /* Ocultamos popup */
                        var ID_8660388_visible = false;

                        if (ID_8660388_visible == 'si') {
                            ID_8660388_visible = true;
                        } else if (ID_8660388_visible == 'no') {
                            ID_8660388_visible = false;
                        }
                        $.ID_8660388.setVisible(ID_8660388_visible);

                        /** 
                         * Mostramos cerrar historial 
                         */
                        var ID_743962856_visible = true;

                        if (ID_743962856_visible == 'si') {
                            ID_743962856_visible = true;
                        } else if (ID_743962856_visible == 'no') {
                            ID_743962856_visible = false;
                        }
                        $.ID_743962856.setVisible(ID_743962856_visible);
                        /* Revisamos si no queda ninguna imagen por enviar (desactivamos enviar_todos) */
                        var faltan = false;
                        /* Consultamos la tabla de historial tareas, revisamos que cada item tenga el estado_tarea en 8 o 9, revisamos que exista un directorio con la tarea. Esto es cuando mandamos todas las inspecciones pendientes */
                        var ID_828104227_i = Alloy.createCollection('historial_tareas');
                        var ID_828104227_i_where = '';
                        ID_828104227_i.fetch();
                        var tareastest = require('helper').query2array(ID_828104227_i);
                        var test_index = 0;
                        _.each(tareastest, function(test, test_pos, test_list) {
                            test_index += 1;
                            if (require('helper').arr_contains('8,9'.toLowerCase().split(','), test.estado_tarea.toString().toLowerCase())) {
                                /* Revisamos que el equipo tenga imagenes guardadas para la tarea actual */
                                var fotos = [];
                                var ID_1873863856_f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + test.id_server + '/');
                                if (ID_1873863856_f.exists() == true) {
                                    fotos = ID_1873863856_f.getDirectoryListing();
                                }
                                if (fotos && fotos.length == 0) {}
                            }
                        });
                        if (faltan == false || faltan == 'false') {
                            /** 
                             * Ocultamos enviar todos 
                             */
                            var ID_609693366_visible = false;

                            if (ID_609693366_visible == 'si') {
                                ID_609693366_visible = true;
                            } else if (ID_609693366_visible == 'no') {
                                ID_609693366_visible = false;
                            }
                            $.ID_609693366.setVisible(ID_609693366_visible);
                            if (false) console.log('esconder en la otra consulta web', {});
                        }
                        /* esto es para refrescar badge de pantalla perfil */
                        Alloy.Events.trigger('refrescar_historial');
                    }
                } else {
                    if (testmas && testmas.length == 0) {
                        /* Esta tarea no tiene mas fotos, borramos enviar_id para continuar con pendientes */
                        require('vars')[_var_scopekey]['enviar_id'] = '';
                        var ID_213243253 = null;
                        if ('refrescar_tareas' in require('funciones')) {
                            ID_213243253 = require('funciones').refrescar_tareas({});
                        } else {
                            try {
                                ID_213243253 = f_refrescar_tareas({});
                            } catch (ee) {}
                        }
                    }
                }
            }
            if (fotos.length >= qfoto_index && require('vars')[_var_scopekey]['enviar_id'] != '') {
                var pidio_cancelar = ('pidio_cancelar' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['pidio_cancelar'] : '';
                if (pidio_cancelar != '') {
                    require('vars')[_var_scopekey]['pidio_cancelar'] = '';
                    console.log('PSB se pidio cancelar, por lo que no seguimos enviando imagenes de tarea actual');
                } else {
                    ep_enviarFoto(fotos);
                }
            } else if (require('vars')[_var_scopekey]['enviar_id'] == '') {
                var pidio_cancelar = ('pidio_cancelar' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['pidio_cancelar'] : '';
                if (pidio_cancelar != '') {
                    require('vars')[_var_scopekey]['pidio_cancelar'] = '';
                    console.log('PSB se pidio cancelar, por lo que hacemos nada');
                } else {
                    console.log('PSB enviar_id en blanco, significa que se quiso todas si hay pendientes');
                    var pendientes = ('pendientes' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['pendientes'] : '';
                    if ((Object.keys(pendientes).length) == 0 || (Object.keys(pendientes).length) == '0') {
                        // si no hay nada pendiente y no hay enviar_id ni fotos actuales
                        // escondemos dialogo
                        require('vars')['enviando_inspecciones'] = false;
                        var ID_1867246625 = null;
                        f_refrescar_tareas({});
                        /* Ocultamos popup */
                        $.ID_8660388.setVisible(false);
                        /** 
                         * Mostramos cerrar historial 
                         */
                        var ID_743962856_visible = true;

                        if (ID_743962856_visible == 'si') {
                            ID_743962856_visible = true;
                        } else if (ID_743962856_visible == 'no') {
                            ID_743962856_visible = false;
                        }
                        $.ID_743962856.setVisible(ID_743962856_visible);
                        /* esto es para refrescar badge de pantalla perfil */
                        Alloy.Events.trigger('refrescar_historial');
                    } else {
                        console.log('PSB hay pendientes, continuamos intervalo general', pendientes);
                        require('vars')['enviando_inspecciones'] = true; // activamos para que intervalo de chequeo interrumpa prosiga.
                    }
                }
            } else {
                // se acabaron fotos de tarea actual
                console.log('PSB continuando con intervalo normal');
                require('vars')['enviando_inspecciones'] = true; // activamos para que intervalo de chequeo interrumpa prosiga.
            }
        }
    });
    if (false) console.log('debug qfoto', {
        "ciclo": ciclo,
        "qfoto": qfoto
    });
    /* Limpiamos memoria */
    firma64 = null;
};
var ciclo = 0;
var ID_1443564091_continuar = true;
_out_vars['ID_1443564091'] = {
    _remove: ["clearTimeout(_out_vars['ID_1443564091']._run)"] 
};
var ID_1443564091_func = function() {
    ciclo = ciclo + 1;
    var enviando_inspecciones = ('enviando_inspecciones' in require('vars')) ? require('vars')['enviando_inspecciones'] : '';
    if (enviando_inspecciones == true || enviando_inspecciones == 'true') {
        if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
            /** 
             * Tratamos de enviar imagenes 
             */
            var enviar_id = ('enviar_id' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['enviar_id'] : '';
            if ((_.isObject(enviar_id) || (_.isString(enviar_id)) && !_.isEmpty(enviar_id)) || _.isNumber(enviar_id) || _.isBoolean(enviar_id)) {
                /** 
                 * si enviar_id esta definido es porque se solicito un envio en particular. 
                 */
                /** 
                 * Revisamos que la inspeccion tenga archivos en el directorio 
                 */

                var fotos = [];
                var ID_8053671_f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + enviar_id + '/');
                if (ID_8053671_f.exists() == true) {
                    fotos = ID_8053671_f.getDirectoryListing();
                }
                if (false) console.log(String.format(L('x3071089469_traducir', 'fotos de %1$s'), enviar_id.toString()), {
                    "fotos": fotos
                });
                qfoto_index = 0;
                var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
                /** 
                 * Actualizamos el estado de envio de la inspeccion en la tabla 
                 */
                var ID_917031794_i = Alloy.createCollection('historial_tareas');
                var ID_917031794_i_where = 'id=\'' + enviar_id + '\'';
                ID_917031794_i.fetch({
                    query: 'SELECT * FROM historial_tareas WHERE id=\'' + enviar_id + '\''
                });
                var tareaclick = require('helper').query2array(ID_917031794_i);
                var db = Ti.Database.open(ID_917031794_i.config.adapter.db_name);
                if (ID_917031794_i_where == '') {
                    var sql = 'UPDATE ' + ID_917031794_i.config.adapter.collection_name + ' SET estado_envio=1';
                } else {
                    var sql = 'UPDATE ' + ID_917031794_i.config.adapter.collection_name + ' SET estado_envio=1 WHERE ' + ID_917031794_i_where;
                }
                db.execute(sql);
                db.close();
                require('vars')['enviando_inspecciones'] = L('x734881840_traducir', 'false');
                var envios = {
                    fotos: fotos,
                    enviar_id: L('x3904355907_traducir', 'a'),
                    url_server: L('x1908338681', 'b')
                };

                ep_enviarFoto(fotos);
            } else {
                /** 
                 * si enviar_id no esta definido, se solicito enviar_todas 
                 */
                /** 
                 * Enviamos todas las fotos disponibles 
                 */
                var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
                if (false) console.log('enviamos las fotos de todas las inspecciones con fotos', {});
                /** 
                 * Consultamos la tabla de historial tareas, revisamos que cada item tenga el estado_tarea en 8 o 9, revisamos que exista un directorio con la tarea. Esto es cuando mandamos todas las inspecciones pendientes 
                 */
                var ID_1538383137_i = Alloy.createCollection('historial_tareas');
                var ID_1538383137_i_where = 'ORDER BY FECHA_TERMINO DESC';
                ID_1538383137_i.fetch({
                    query: 'SELECT * FROM historial_tareas ORDER BY FECHA_TERMINO DESC'
                });
                var tareas = require('helper').query2array(ID_1538383137_i);
                var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
                var lista = {};
                var tarea_index = 0;
                _.each(tareas, function(tarea, tarea_pos, tarea_list) {
                    tarea_index += 1;
                    if (require('helper').arr_contains('8,9'.toLowerCase().split(','), tarea.estado_tarea.toString().toLowerCase())) {
                        /** 
                         * Revisamos que cada tarea tenga un estado_tarea en 8 o 9 
                         */
                        var fotos = [];
                        var ID_1053507135_f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + tarea.id_server + '/');
                        if (ID_1053507135_f.exists() == true) {
                            fotos = ID_1053507135_f.getDirectoryListing();
                        }
                        qfoto_index = 0;
                        if (fotos && fotos.length) {
                            /** 
                             * Revisamos que el directorio tenga archivos 
                             */
                            /** 
                             * Actualizamos la variable enviar_id con el id de la tarea que estamos enviando 
                             */
                            var enviar_id = ('enviar_id' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['enviar_id'] : '';
                            if ((_.isObject(enviar_id) || _.isString(enviar_id)) && _.isEmpty(enviar_id)) {
                                require('vars')[_var_scopekey]['enviar_id'] = tarea.id_server;
                                lista[tarea.id_server] = true;
                            } else {
                                lista[enviar_id] = true;
                            }
                        }
                    }
                });
                if (false) console.log('detalle de la lista', {
                    "datos": lista
                });
                /** 
                 * Actualizamos variable pendientes con la lista de tareas 
                 */
                require('vars')[_var_scopekey]['pendientes'] = lista;
                if ((Object.keys(lista).length) == 0 || (Object.keys(lista).length) == '0') {
                    /** 
                     * Revisamos que la variable de lista este vacia 
                     */
                    /** 
                     * Detenemos popup enviando 
                     */
                    require('vars')['enviando_inspecciones'] = L('x734881840_traducir', 'false');
                    /** 
                     * Limpiamos variable de enviar_id 
                     */
                    require('vars')[_var_scopekey]['enviar_id'] = '';
                    /** 
                     * Llamamos la funcion de refrescar_tareas 
                     */
                    var ID_1111108741 = null;
                    if ('refrescar_tareas' in require('funciones')) {
                        ID_1111108741 = require('funciones').refrescar_tareas({});
                    } else {
                        try {
                            ID_1111108741 = f_refrescar_tareas({});
                        } catch (ee) {}
                    }
                    /** 
                     * Ocultamos popup 
                     */
                    var ID_8660388_visible = false;

                    if (ID_8660388_visible == 'si') {
                        ID_8660388_visible = true;
                    } else if (ID_8660388_visible == 'no') {
                        ID_8660388_visible = false;
                    }
                    $.ID_8660388.setVisible(ID_8660388_visible);

                    /** 
                     * Mostramos enviar todos 
                     */
                    var ID_609693366_visible = true;

                    if (ID_609693366_visible == 'si') {
                        ID_609693366_visible = true;
                    } else if (ID_609693366_visible == 'no') {
                        ID_609693366_visible = false;
                    }
                    $.ID_609693366.setVisible(ID_609693366_visible);

                    /** 
                     * Mostramos cerrar historial 
                     */
                    var ID_743962856_visible = true;

                    if (ID_743962856_visible == 'si') {
                        ID_743962856_visible = true;
                    } else if (ID_743962856_visible == 'no') {
                        ID_743962856_visible = false;
                    }
                    $.ID_743962856.setVisible(ID_743962856_visible);

                    /** 
                     * Revisamos si no queda ninguna imagen por enviar (desactivamos enviar_todos) 
                     */
                    var faltan = false;
                    /** 
                     * Consultamos la tabla de historial tareas, revisamos que cada item tenga el estado_tarea en 8 o 9, revisamos que exista un directorio con la tarea 
                     */
                    var ID_621139247_i = Alloy.createCollection('historial_tareas');
                    var ID_621139247_i_where = '';
                    ID_621139247_i.fetch();
                    var tareastest = require('helper').query2array(ID_621139247_i);
                    var test_index = 0;
                    _.each(tareastest, function(test, test_pos, test_list) {
                        test_index += 1;
                        if (require('helper').arr_contains('8,9'.toLowerCase().split(','), test.estado_tarea.toString().toLowerCase())) {
                            /** 
                             * Revisamos que el equipo tenga imagenes guardadas para la tarea actual 
                             */

                            var fotos = [];
                            var ID_1405073794_f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + test.id_server + '/');
                            if (ID_1405073794_f.exists() == true) {
                                fotos = ID_1405073794_f.getDirectoryListing();
                            }
                            qfoto_index = 0;
                            if (fotos && fotos.length == 0) {
                                /** 
                                 * Si no existen fotos en el equipo, actualizamos la variable de que no hay fotos en el equipo 
                                 */
                                require('vars')[_var_scopekey]['faltan'] = L('x4261170317', 'true');
                                faltan = true;
                            }
                        }
                    });
                    if (faltan == false || faltan == 'false') {
                        /** 
                         * Ocultamos boton enviar todos 
                         */
                        if (false) console.log('esconder en la consulta web', {});
                    }
                }
            }
        }
    }
    if (ID_1443564091_continuar == true) {
        _out_vars['ID_1443564091']._run = setTimeout(ID_1443564091_func, 1000 * 5);
    }
};
_out_vars['ID_1443564091']._run = setTimeout(ID_1443564091_func, 1000 * 5);


(function() {
    require('vars')['enviando_inspecciones'] = L('x734881840_traducir', 'false');
    require('vars')['pendientes'] = {};
    /** 
     * Recuperamos variable inspector 
     */
    var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
    if ((_.isObject(inspector) || _.isString(inspector)) && _.isEmpty(inspector)) {
        /** 
         * Revisamos que la variable inspector tenga datos, si no hacemos una consulta a la tabla y actualizamos la variable con los datos 
         */
        var ID_79962037_i = Alloy.createCollection('inspectores');
        var ID_79962037_i_where = '';
        ID_79962037_i.fetch();
        var inspector_list = require('helper').query2array(ID_79962037_i);
        require('vars')['inspector'] = inspector_list[0];
        var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
    }
    require('vars')[_var_scopekey]['faltan'] = L('x734881840_traducir', 'false');
    /** 
     * Consultamos la tabla de historial tareas, revisamos que cada item tenga el estado_tarea en 8 o 9, revisamos que exista un directorio con la tarea 
     */
    var ID_1629439457_i = Alloy.createCollection('historial_tareas');
    var ID_1629439457_i_where = '';
    ID_1629439457_i.fetch();
    var tareastest = require('helper').query2array(ID_1629439457_i);
    var test_index = 0;
    _.each(tareastest, function(test, test_pos, test_list) {
        test_index += 1;
        if (require('helper').arr_contains('8,9'.toLowerCase().split(','), test.estado_tarea.toString().toLowerCase())) {
            /** 
             * Revisamos que el equipo tenga imagenes guardadas para la tarea actual 
             */

            var fotos = [];
            var ID_694905693_f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + test.id_server + '/');
            if (ID_694905693_f.exists() == true) {
                fotos = ID_694905693_f.getDirectoryListing();
            }
            if (fotos && fotos.length == 0) {
                /** 
                 * Si no existen fotos en el equipo, actualizamos la variable de que no hay fotos en el equipo 
                 */
            } else {
                require('vars')[_var_scopekey]['faltan'] = L('x4261170317', 'true');
            }
        }
    });
    var ID_1268939712_func = function() {
        var faltan = ('faltan' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['faltan'] : '';
        if (faltan == true || faltan == 'true') {
            /** 
             * Revisamos si no queda ninguna imagen por enviar (desactivamos enviar_todos) 
             */
            if (false) console.log('faltan es true', {});
            /** 
             * Mostramos enviar todos 
             */
            var ID_609693366_visible = true;

            if (ID_609693366_visible == 'si') {
                ID_609693366_visible = true;
            } else if (ID_609693366_visible == 'no') {
                ID_609693366_visible = false;
            }
            $.ID_609693366.setVisible(ID_609693366_visible);

            /** 
             * Mostramos cerrar historial 
             */
            var ID_743962856_visible = true;

            if (ID_743962856_visible == 'si') {
                ID_743962856_visible = true;
            } else if (ID_743962856_visible == 'no') {
                ID_743962856_visible = false;
            }
            $.ID_743962856.setVisible(ID_743962856_visible);

        } else {
            if (false) console.log('faltan es falso', {});
        }
        var ID_1407376368 = null;
        if ('refrescar_tareas' in require('funciones')) {
            ID_1407376368 = require('funciones').refrescar_tareas({});
        } else {
            try {
                ID_1407376368 = f_refrescar_tareas({});
            } catch (ee) {}
        }
    };
    var ID_1268939712 = setTimeout(ID_1268939712_func, 1000 * 0.2);
})();

if (OS_IOS || OS_ANDROID) {
    $.ID_310021856.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}
//$.ID_310021856.open();