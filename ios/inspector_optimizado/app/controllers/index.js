var _bind4section = {};
var _list_templates = {};

var _activity;
if (OS_ANDROID) {
    _activity = $.ID_25124347.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_25124347';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_25124347.addEventListener('open', function(e) {
        abx.setStatusbarColor("#000000");
    });
}

function Load_ID_487912265(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var evento = e;
    elemento.start();

}

function Click_ID_1284362860(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    /** 
     * Creamos una estructura para usarla en el enrolamiento 
     */
    var vacio = {};
    /** 
     * Guardamos la variable registro para que sea de acceso permanente, (que no se pierda despues de una vez cerrado) 
     */
    Ti.App.Properties.setString('registro', JSON.stringify(vacio));
    var mi_pais = ('mi_pais' in require('vars')) ? require('vars')['mi_pais'] : '';
    var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
    var ID_1674696970 = null;
    if ('ocultar_botones' in require('funciones')) {
        ID_1674696970 = require('funciones').ocultar_botones({});
    } else {
        try {
            ID_1674696970 = f_ocultar_botones({});
        } catch (ee) {}
    }
    $.ID_908004102.setText('status: obteniendo info');

    var ID_362822929 = {};

    ID_362822929.success = function(e) {
        var elemento = e,
            valor = e;
        if (elemento == false || elemento == 'false') {
            $.ID_908004102.setText('status: esperando');

            var ID_244136950_opts = [L('x1518866076_traducir', 'Aceptar')];
            var ID_244136950 = Ti.UI.createAlertDialog({
                title: L('x57652245_traducir', 'Alerta'),
                message: L('x1693828390_traducir', 'Problema de conexión por favor intentar después'),
                buttonNames: ID_244136950_opts
            });
            ID_244136950.addEventListener('click', function(e) {
                var suu = ID_244136950_opts[e.index];
                suu = null;

                e.source.removeEventListener("click", arguments.callee);
            });
            ID_244136950.show();
        } else {
            /** 
             * En el caso de obtener un resultado desde el servidor, limpiamos y cargamos las tablas 
             */
            $.ID_908004102.setText('status: cargando datos');

            var ID_821317784_i = Alloy.Collections.pais;
            var sql = "DELETE FROM " + ID_821317784_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_821317784_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            ID_821317784_i.trigger('remove');
            var ID_1557444604_i = Alloy.Collections.nivel1;
            var sql = "DELETE FROM " + ID_1557444604_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_1557444604_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            ID_1557444604_i.trigger('remove');
            var ID_325623147_i = Alloy.Collections.experiencia_oficio;
            var sql = "DELETE FROM " + ID_325623147_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_325623147_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            ID_325623147_i.trigger('remove');
            var elemento_regiones = elemento.regiones;
            var ID_1376954920_m = Alloy.Collections.nivel1;
            var db_ID_1376954920 = Ti.Database.open(ID_1376954920_m.config.adapter.db_name);
            db_ID_1376954920.execute('BEGIN');
            _.each(elemento_regiones, function(ID_1376954920_fila, pos) {
                db_ID_1376954920.execute('INSERT INTO nivel1 (id_label, id_server, dif_horaria, id_pais) VALUES (?,?,?,?)', ID_1376954920_fila.subdivision_name, ID_1376954920_fila.id, 2, ID_1376954920_fila.id_pais);
            });
            db_ID_1376954920.execute('COMMIT');
            db_ID_1376954920.close();
            db_ID_1376954920 = null;
            ID_1376954920_m.trigger('change');
            var elemento_paises = elemento.paises;
            var ID_518549359_m = Alloy.Collections.pais;
            var db_ID_518549359 = Ti.Database.open(ID_518549359_m.config.adapter.db_name);
            db_ID_518549359.execute('BEGIN');
            _.each(elemento_paises, function(ID_518549359_fila, pos) {
                db_ID_518549359.execute('INSERT INTO pais (label_nivel2, moneda, nombre, label_nivel4, label_codigo_identificador, label_nivel3, id_server, label_nivel1, iso, sis_metrico, niveles_pais, id_pais, label_nivel5, lenguaje) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)', ID_518549359_fila.nivel_2, ID_518549359_fila.moneda, ID_518549359_fila.nombre, ID_518549359_fila.nivel_4, ID_518549359_fila.label_codigo_identificador, ID_518549359_fila.nivel_3, ID_518549359_fila.id, ID_518549359_fila.nivel_1, ID_518549359_fila.iso, ID_518549359_fila.sis_metrico, ID_518549359_fila.niveles_pais, ID_518549359_fila.idpais, ID_518549359_fila.nivel_5, ID_518549359_fila.lenguaje);
            });
            db_ID_518549359.execute('COMMIT');
            db_ID_518549359.close();
            db_ID_518549359 = null;
            ID_518549359_m.trigger('change');
            var elemento_experiencia_oficio = elemento.experiencia_oficio;
            var ID_1594927226_m = Alloy.Collections.experiencia_oficio;
            var db_ID_1594927226 = Ti.Database.open(ID_1594927226_m.config.adapter.db_name);
            db_ID_1594927226.execute('BEGIN');
            _.each(elemento_experiencia_oficio, function(ID_1594927226_fila, pos) {
                db_ID_1594927226.execute('INSERT INTO experiencia_oficio (nombre, id_server, id_pais) VALUES (?,?,?)', ID_1594927226_fila.nombre, ID_1594927226_fila.id, ID_1594927226_fila.idpais);
            });
            db_ID_1594927226.execute('COMMIT');
            db_ID_1594927226.close();
            db_ID_1594927226 = null;
            ID_1594927226_m.trigger('change');
            $.ID_908004102.setText('status: esperando');

            /** 
             * Y cargamos la primera pantalla del enrolamiento 
             */
            Alloy.createController("registro_index", {}).getView().open();
        }
        var ID_1865449898 = null;
        if ('mostrar_botones' in require('funciones')) {
            ID_1865449898 = require('funciones').mostrar_botones({});
        } else {
            try {
                ID_1865449898 = f_mostrar_botones({});
            } catch (ee) {}
        }
        elemento = null, valor = null;
    };

    ID_362822929.error = function(e) {
        var elemento = e,
            valor = e;
        var ID_6513015_opts = [L('x1518866076_traducir', 'Aceptar')];
        var ID_6513015 = Ti.UI.createAlertDialog({
            title: L('x57652245_traducir', 'Alerta'),
            message: L('x1693828390_traducir', 'Problema de conexión por favor intentar después'),
            buttonNames: ID_6513015_opts
        });
        ID_6513015.addEventListener('click', function(e) {
            var suu = ID_6513015_opts[e.index];
            suu = null;

            e.source.removeEventListener("click", arguments.callee);
        });
        ID_6513015.show();
        var ID_758659505 = null;
        if ('mostrar_botones' in require('funciones')) {
            ID_758659505 = require('funciones').mostrar_botones({});
        } else {
            try {
                ID_758659505 = f_mostrar_botones({});
            } catch (ee) {}
        }
        elemento = null, valor = null;
    };
    require('helper').ajaxUnico('ID_362822929', '' + String.format(L('x2963862531', '%1$sobtenerPais'), url_server.toString()) + '', 'POST', {}, 15000, ID_362822929);

}

$.ID_337746182.init({
    titulo: L('x1321529571_traducir', 'ENTRAR'),
    __id: 'ALL337746182',
    color: 'amarillo',
    onclick: Click_ID_1788736104
});

function Click_ID_1788736104(e) {

    var evento = e;
    if (false) console.log('LLamando al servidor por login', {});
    var correo;
    correo = $.ID_971947162.getValue();

    var password;
    password = $.ID_1673669563.getValue();

    if ((_.isObject(correo) || _.isString(correo)) && _.isEmpty(correo)) {
        var ID_147104875_opts = [L('x1518866076_traducir', 'Aceptar')];
        var ID_147104875 = Ti.UI.createAlertDialog({
            title: L('x57652245_traducir', 'Alerta'),
            message: L('x2435587168_traducir', 'Debe ingresar su nombre de usuario'),
            buttonNames: ID_147104875_opts
        });
        ID_147104875.addEventListener('click', function(e) {
            var x = ID_147104875_opts[e.index];
            $.ID_971947162.focus();
            x = null;

            e.source.removeEventListener("click", arguments.callee);
        });
        ID_147104875.show();
    } else if ((_.isObject(password) || _.isString(password)) && _.isEmpty(password)) {
        var ID_1539382908_opts = [L('x1518866076_traducir', 'Aceptar')];
        var ID_1539382908 = Ti.UI.createAlertDialog({
            title: L('x57652245_traducir', 'Alerta'),
            message: L('x2082354442_traducir', 'Debe ingresar su clave'),
            buttonNames: ID_1539382908_opts
        });
        ID_1539382908.addEventListener('click', function(e) {
            var x = ID_1539382908_opts[e.index];
            $.ID_1673669563.focus();
            x = null;

            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1539382908.show();
    } else {
        /** 
         * Verificamos que los campos de email y password no esten vacios, en caso que haya datos, proseguimos con la carga de paises 
         */
        var ID_1791823265 = null;
        if ('ocultar_botones' in require('funciones')) {
            ID_1791823265 = require('funciones').ocultar_botones({});
        } else {
            try {
                ID_1791823265 = f_ocultar_botones({});
            } catch (ee) {}
        }
        require('vars')['correo'] = correo;
        require('vars')['password'] = password;
        $.ID_971947162.blur();
        $.ID_1673669563.blur();
        if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
            var ID_988851127 = null;
            if ('cargar_paises' in require('funciones')) {
                ID_988851127 = require('funciones').cargar_paises({});
            } else {
                try {
                    ID_988851127 = f_cargar_paises({});
                } catch (ee) {}
            }
        } else {
            var ID_1947630548 = null;
            if ('mostrar_botones' in require('funciones')) {
                ID_1947630548 = require('funciones').mostrar_botones({});
            } else {
                try {
                    ID_1947630548 = f_mostrar_botones({});
                } catch (ee) {}
            }
            var ID_674689339_opts = [L('x1518866076_traducir', 'Aceptar')];
            var ID_674689339 = Ti.UI.createAlertDialog({
                title: L('x57652245_traducir', 'Alerta'),
                message: L('x954934616_traducir', 'Problema de conexion por favor intentar despues'),
                buttonNames: ID_674689339_opts
            });
            ID_674689339.addEventListener('click', function(e) {
                var suu = ID_674689339_opts[e.index];
                suu = null;

                e.source.removeEventListener("click", arguments.callee);
            });
            ID_674689339.show();
        }
    }
}

/** 
 * Modificamos el ancho de la vista del registro para que se ajuste a su contenido y no tome la totalidad 
 */
var ID_1632570648_ancho = '-';

if (ID_1632570648_ancho == '*') {
    ID_1632570648_ancho = Ti.UI.FILL;
} else if (ID_1632570648_ancho == '-') {
    ID_1632570648_ancho = Ti.UI.SIZE;
} else if (!isNaN(ID_1632570648_ancho)) {
    ID_1632570648_ancho = ID_1632570648_ancho + 'dp';
}
$.ID_1632570648.setWidth(ID_1632570648_ancho);

/** 
 * Definimos en una variable el lenguaje que tiene el telefono 
 */
var pais_codigo = Titanium.Locale.currentCountry;
/** 
 * Guardamos en una variable global el codigo de pais que tiene el telefono 
 */
require('vars')['mi_pais'] = pais_codigo;
/** 
 * Recuperamos la url del servidor de uadjust 
 */
var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
/** 
 * Partimos sin inspeccion desde login 
 */
require('vars')['inspeccion_encurso'] = L('x734881840_traducir', 'false');
/** 
 * Obtenemos una primera ubicaci&#243;n para el login 
 */
var _ifunc_res_ID_713480223 = function(e) {
    if (e.error) {
        var geopos = {
            error: true,
            latitude: -1,
            longitude: -1,
            reason: (e.reason) ? e.reason : 'unknown',
            accuracy: 0,
            speed: 0,
            error_compass: false
        };
    } else {
        var geopos = e;
    }
    if (geopos.error == false || geopos.error == 'false') {
        /** 
         * Revisamos que no haya error al obtener la ubicacion 
         */
        require('vars')['gps_error'] = L('x734881840_traducir', 'false');
        require('vars')['gps_latitud'] = geopos.latitude;
        require('vars')['gps_longitud'] = geopos.longitude;
    }
};
Ti.Geolocation.setAccuracy(Ti.Geolocation.ACCURACY_HIGH);
if (Ti.Geolocation.hasLocationPermissions(Ti.Geolocation.AUTHORIZATION_WHEN_IN_USE)) {
    Ti.Geolocation.getCurrentPosition(function(ee) {
        if (Ti.Geolocation.getHasCompass() == false) {
            var pgeopos = ('coords' in ee) ? ee.coords : {};
            pgeopos.error = ('coords' in ee) ? false : true;
            pgeopos.reason = '';
            pgeopos.speed = 0;
            pgeopos.compass = -1;
            pgeopos.compass_accuracy = -1;
            pgeopos.error_compass = true;
            _ifunc_res_ID_713480223(pgeopos);
        } else {
            Ti.Geolocation.getCurrentHeading(function(yy) {
                var pgeopos = ('coords' in ee) ? ee.coords : {};
                pgeopos.error = ('coords' in ee) ? false : true;
                pgeopos.reason = ('error' in ee) ? ee.error : '';
                if (yy.error) {
                    pgeopos.error_compass = true;
                } else {
                    pgeopos.compass = yy.heading;
                    pgeopos.compass_accuracy = yy.heading.accuracy;
                }
                _ifunc_res_ID_713480223(pgeopos);
            });
        }
    });
} else {
    Ti.Geolocation.requestLocationPermissions(Ti.Geolocation.AUTHORIZATION_WHEN_IN_USE, function(u) {
        if (u.success) {
            Ti.Geolocation.getCurrentPosition(function(ee) {
                if (Ti.Geolocation.getHasCompass() == false) {
                    var pgeopos = ('coords' in ee) ? ee.coords : {};
                    pgeopos.error = ('coords' in ee) ? false : true;
                    pgeopos.reason = '';
                    pgeopos.speed = 0;
                    pgeopos.compass = -1;
                    pgeopos.compass_accuracy = -1;
                    pgeopos.error_compass = true;
                    _ifunc_res_ID_713480223(pgeopos);
                } else {
                    Ti.Geolocation.getCurrentHeading(function(yy) {
                        var pgeopos = ('coords' in ee) ? ee.coords : {};
                        pgeopos.error = ('coords' in ee) ? false : true;
                        pgeopos.reason = '';
                        if (yy.error) {
                            pgeopos.error_compass = true;
                        } else {
                            pgeopos.compass = yy.heading;
                            pgeopos.compass_accuracy = yy.heading.accuracy;
                        }
                        _ifunc_res_ID_713480223(pgeopos);
                    });
                }
            });
        } else {
            _ifunc_res_ID_713480223({
                error: true,
                latitude: -1,
                longitude: -1,
                reason: 'permission_denied',
                accuracy: 0,
                speed: 0,
                error_compass: false
            });
        }
    });
}
var f_cargar_paises = function(x_params) {
    var item = x_params['item'];
    $.ID_908004102.setText('status: cargando paises');

    /** 
     * Hacemos una consulta al servidor para obtener los paises 
     */
    var ID_1506536448 = {};

    ID_1506536448.success = function(e) {
        var elemento = e,
            valor = e;
        if (_.isObject(elemento)) {
            if (elemento.error == 0 || elemento.error == '0') {
                /** 
                 * En el caso que el servidor retorne un resultado 0 es porque esta todo bien, caso contrario, se mostrara mensaje de error y vuelve a mostrar los errores 
                 */
                if (false) console.log('registrando paises', {});
                /** 
                 * Si no hubo problema al obtener los paises, limpiamos los modelos y cargamos los datos 
                 */
                /** 
                 * Si no hubo problema al obtener los paises, limpiamos los modelos y cargamos los datos 
                 */

                var ID_1512376271_i = Alloy.Collections.pais;
                var sql = "DELETE FROM " + ID_1512376271_i.config.adapter.collection_name;
                var db = Ti.Database.open(ID_1512376271_i.config.adapter.db_name);
                db.execute(sql);
                db.close();
                ID_1512376271_i.trigger('remove');
                var ID_1612987487_i = Alloy.Collections.nivel1;
                var sql = "DELETE FROM " + ID_1612987487_i.config.adapter.collection_name;
                var db = Ti.Database.open(ID_1612987487_i.config.adapter.db_name);
                db.execute(sql);
                db.close();
                ID_1612987487_i.trigger('remove');
                var ID_794595785_i = Alloy.Collections.experiencia_oficio;
                var sql = "DELETE FROM " + ID_794595785_i.config.adapter.collection_name;
                var db = Ti.Database.open(ID_794595785_i.config.adapter.db_name);
                db.execute(sql);
                db.close();
                ID_794595785_i.trigger('remove');
                var elemento_regiones = elemento.regiones;
                var ID_1643530140_m = Alloy.Collections.nivel1;
                var db_ID_1643530140 = Ti.Database.open(ID_1643530140_m.config.adapter.db_name);
                db_ID_1643530140.execute('BEGIN');
                _.each(elemento_regiones, function(ID_1643530140_fila, pos) {
                    db_ID_1643530140.execute('INSERT INTO nivel1 (id_label, id_server, dif_horaria, id_pais) VALUES (?,?,?,?)', ID_1643530140_fila.subdivision_name, ID_1643530140_fila.id, 2, ID_1643530140_fila.id_pais);
                });
                db_ID_1643530140.execute('COMMIT');
                db_ID_1643530140.close();
                db_ID_1643530140 = null;
                ID_1643530140_m.trigger('change');
                var elemento_paises = elemento.paises;
                var ID_1034882584_m = Alloy.Collections.pais;
                var db_ID_1034882584 = Ti.Database.open(ID_1034882584_m.config.adapter.db_name);
                db_ID_1034882584.execute('BEGIN');
                _.each(elemento_paises, function(ID_1034882584_fila, pos) {
                    db_ID_1034882584.execute('INSERT INTO pais (label_nivel2, nombre, label_nivel4, label_codigo_identificador, label_nivel3, id_server, label_nivel1, iso, sis_metrico, id_pais, label_nivel5, lenguaje) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)', ID_1034882584_fila.nivel_2, ID_1034882584_fila.nombre, ID_1034882584_fila.nivel_4, ID_1034882584_fila.label_codigo_identificador, ID_1034882584_fila.nivel_3, ID_1034882584_fila.id, ID_1034882584_fila.nivel_1, ID_1034882584_fila.iso, ID_1034882584_fila.sis_metrico, ID_1034882584_fila.idpais, ID_1034882584_fila.nivel_5, ID_1034882584_fila.lenguaje);
                });
                db_ID_1034882584.execute('COMMIT');
                db_ID_1034882584.close();
                db_ID_1034882584 = null;
                ID_1034882584_m.trigger('change');
                var elemento_experiencia_oficio = elemento.experiencia_oficio;
                var ID_636641228_m = Alloy.Collections.experiencia_oficio;
                var db_ID_636641228 = Ti.Database.open(ID_636641228_m.config.adapter.db_name);
                db_ID_636641228.execute('BEGIN');
                _.each(elemento_experiencia_oficio, function(ID_636641228_fila, pos) {
                    db_ID_636641228.execute('INSERT INTO experiencia_oficio (nombre, id_server, id_pais) VALUES (?,?,?)', ID_636641228_fila.nombre, ID_636641228_fila.id, ID_636641228_fila.idpais);
                });
                db_ID_636641228.execute('COMMIT');
                db_ID_636641228.close();
                db_ID_636641228 = null;
                ID_636641228_m.trigger('change');
                /** 
                 * Al terminar la carga de datos de la base de datos, llamamos una funcion para continuar con el proceso 
                 */
                var ID_203502002 = null;
                if ('llamar_login' in require('funciones')) {
                    ID_203502002 = require('funciones').llamar_login({});
                } else {
                    try {
                        ID_203502002 = f_llamar_login({});
                    } catch (ee) {}
                }
            } else {
                var ID_452613796 = null;
                if ('mostrar_botones' in require('funciones')) {
                    ID_452613796 = require('funciones').mostrar_botones({});
                } else {
                    try {
                        ID_452613796 = f_mostrar_botones({});
                    } catch (ee) {}
                }
                var ID_606943387_opts = [L('x1518866076_traducir', 'Aceptar')];
                var ID_606943387 = Ti.UI.createAlertDialog({
                    title: L('x3237162386_traducir', 'Atencion'),
                    message: '' + String.format(L('x2630571488_traducir', 'Hubo un problema con el servidor (%1$s)'), elemento.error.toString()) + '',
                    buttonNames: ID_606943387_opts
                });
                ID_606943387.addEventListener('click', function(e) {
                    var suu = ID_606943387_opts[e.index];
                    suu = null;

                    e.source.removeEventListener("click", arguments.callee);
                });
                ID_606943387.show();
                $.ID_908004102.setText('status: esperando');

            }
        } else {
            var ID_790527593 = null;
            if ('mostrar_botones' in require('funciones')) {
                ID_790527593 = require('funciones').mostrar_botones({});
            } else {
                try {
                    ID_790527593 = f_mostrar_botones({});
                } catch (ee) {}
            }
            var ID_475986631_opts = [L('x1518866076_traducir', 'Aceptar')];
            var ID_475986631 = Ti.UI.createAlertDialog({
                title: L('x3237162386_traducir', 'Atencion'),
                message: L('x1364461137_traducir', 'Hubo un problema con el servidor'),
                buttonNames: ID_475986631_opts
            });
            ID_475986631.addEventListener('click', function(e) {
                var suu = ID_475986631_opts[e.index];
                suu = null;

                e.source.removeEventListener("click", arguments.callee);
            });
            ID_475986631.show();
            $.ID_908004102.setText('status: esperando');

        }
        elemento = null, valor = null;
    };

    ID_1506536448.error = function(e) {
        var elemento = e,
            valor = e;
        var ID_930600987 = null;
        if ('mostrar_botones' in require('funciones')) {
            ID_930600987 = require('funciones').mostrar_botones({});
        } else {
            try {
                ID_930600987 = f_mostrar_botones({});
            } catch (ee) {}
        }
        var ID_641150474_opts = [L('x1518866076_traducir', 'Aceptar')];
        var ID_641150474 = Ti.UI.createAlertDialog({
            title: L('x3237162386_traducir', 'Atencion'),
            message: L('x954934616_traducir', 'Problema de conexion por favor intentar despues'),
            buttonNames: ID_641150474_opts
        });
        ID_641150474.addEventListener('click', function(e) {
            var suu = ID_641150474_opts[e.index];
            suu = null;

            e.source.removeEventListener("click", arguments.callee);
        });
        ID_641150474.show();
        $.ID_908004102.setText('status: esperando');

        elemento = null, valor = null;
    };
    require('helper').ajaxUnico('ID_1506536448', '' + String.format(L('x2963862531', '%1$sobtenerPais'), url_server.toString()) + '', 'POST', {}, 15000, ID_1506536448);
    return null;
};
var f_llamar_login = function(x_params) {
    var item = x_params['item'];
    if (false) console.log('capturando ubicacion', {});
    $.ID_908004102.setText('status: verificando credenciales');

    /** 
     * Recuperamos la variable gps_error 
     */
    var gps_error = ('gps_error' in require('vars')) ? require('vars')['gps_error'] : '';
    if (gps_error == true || gps_error == 'true') {
        /** 
         * En el caso que gps_error sea verdadero, mostramos mensaje de problema. Caso contrario, seguimos con la validacion de usuario 
         */
        var ID_1222397500_opts = [L('x1518866076_traducir', 'Aceptar')];
        var ID_1222397500 = Ti.UI.createAlertDialog({
            title: L('x57652245_traducir', 'Alerta'),
            message: L('x3873304135_traducir', 'No se pudo obtener la ubicación'),
            buttonNames: ID_1222397500_opts
        });
        ID_1222397500.addEventListener('click', function(e) {
            var errori = ID_1222397500_opts[e.index];
            errori = null;

        });
        ID_1222397500.show();
        var ID_1353705667 = null;
        if ('mostrar_botones' in require('funciones')) {
            ID_1353705667 = require('funciones').mostrar_botones({});
        } else {
            try {
                ID_1353705667 = f_mostrar_botones({});
            } catch (ee) {}
        }
    } else {
        var correo = ('correo' in require('vars')) ? require('vars')['correo'] : '';
        var password = ('password' in require('vars')) ? require('vars')['password'] : '';
        var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
        /** 
         * La variable devicetoken es el token obtenido cuando se solicita obtener el push 
         */
        var devicetoken = ('devicetoken' in require('vars')) ? require('vars')['devicetoken'] : '';
        if ((_.isObject(devicetoken) || _.isString(devicetoken)) && _.isEmpty(devicetoken)) {
            /** 
             * Se aplica cuando se compila en iOS, ya que los push no funcionan en simuladores 
             */
            /** 
             * Agregamos este devicetoken simulado porque en el emulador no hay mensajes push 
             */
            require('vars')['devicetoken'] = L('x1803495209_traducir', 'emulador-iphone6-creador');
            Ti.App.Properties.setString('devicetoken', JSON.stringify('emulador-iphone6-creador'));
            var devicetoken = ('devicetoken' in require('vars')) ? require('vars')['devicetoken'] : '';
        }
        if (false) console.log('llamando servicio login', {});
        /** 
         * Obtenemos el id unico de cada equipo 
         */
        var uidd = Ti.Platform.id;
        var gps_latitud = ('gps_latitud' in require('vars')) ? require('vars')['gps_latitud'] : '';
        var gps_longitud = ('gps_longitud' in require('vars')) ? require('vars')['gps_longitud'] : '';
        /** 
         * Obtenemos la zona horaria 
         */
        var moment = require('alloy/moment');
        var reloj_celular = moment(new Date()).format('ZZ');
        /** 
         * Obtenemos la zona horaria del equipo 
         */
        var dif = +reloj_celular * 0.01;
        if (false) console.log('psb dato para sincronizacion horaria (solo gmt)', {
            "fecha": dif
        });
        var ID_822595707 = {};
        if (false) console.log('DEBUG WEB: requesting url:' + String.format(L('x796226125', '%1$slogin'), url_server.toString()) + ' with data:', {
            _method: 'POST',
            _params: {
                correo: correo,
                password: password,
                device: Ti.Platform.name,
                lat: gps_latitud,
                lon: gps_longitud,
                device_token: devicetoken,
                uidd: uidd,
                fecha: dif
            },
            _timeout: '15000'
        });

        ID_822595707.success = function(e) {
            var elemento = e,
                valor = e;
            if (false) console.log('respuesta de servidor: login', {
                "elemento": elemento
            });
            if (elemento.error == 401) {
                /** 
                 * Manejamos los distintos tipos de mensaje de error que puede decir el servidor. 
                 */
                if (false) console.log('login error 401', {});
                var ID_1463949108 = null;
                if ('mostrar_botones' in require('funciones')) {
                    ID_1463949108 = require('funciones').mostrar_botones({});
                } else {
                    try {
                        ID_1463949108 = f_mostrar_botones({});
                    } catch (ee) {}
                }

                $.ID_337746182.detener_progreso({});
                var ID_1837050684_opts = [L('x1518866076_traducir', 'Aceptar')];
                var ID_1837050684 = Ti.UI.createAlertDialog({
                    title: L('x57652245_traducir', 'Alerta'),
                    message: L('x2082395189_traducir', 'Clave o usuario incorrectos.'),
                    buttonNames: ID_1837050684_opts
                });
                ID_1837050684.addEventListener('click', function(e) {
                    var res = ID_1837050684_opts[e.index];
                    res = null;

                    e.source.removeEventListener("click", arguments.callee);
                });
                ID_1837050684.show();
            } else if (elemento.error == 402) {
                if (false) console.log('login error 402', {});
                var ID_1281427820 = null;
                if ('mostrar_botones' in require('funciones')) {
                    ID_1281427820 = require('funciones').mostrar_botones({});
                } else {
                    try {
                        ID_1281427820 = f_mostrar_botones({});
                    } catch (ee) {}
                }

                $.ID_337746182.detener_progreso({});
                var ID_218946890_opts = [L('x1518866076_traducir', 'Aceptar')];
                var ID_218946890 = Ti.UI.createAlertDialog({
                    title: L('x57652245_traducir', 'Alerta'),
                    message: L('x2797729566_traducir', 'Usuario bloqueado'),
                    buttonNames: ID_218946890_opts
                });
                ID_218946890.addEventListener('click', function(e) {
                    var res = ID_218946890_opts[e.index];
                    res = null;

                    e.source.removeEventListener("click", arguments.callee);
                });
                ID_218946890.show();
            } else if (elemento.error == 0 || elemento.error == '0') {
                /** 
                 * En el caso que no exista error (O sea, error 0), procedemos con la limpieza y carga de datos en la base de datos 
                 */
                if (false) console.log('login error 0: todo bien, accediendo', {});
                if (false) console.log('limpiando tablas locales asociadas a login previo', {});
                var ID_1229479795_i = Alloy.Collections.inspectores;
                var sql = "DELETE FROM " + ID_1229479795_i.config.adapter.collection_name;
                var db = Ti.Database.open(ID_1229479795_i.config.adapter.db_name);
                db.execute(sql);
                db.close();
                ID_1229479795_i.trigger('remove');
                var ID_644557694_i = Alloy.Collections.tareas_entrantes;
                var sql = "DELETE FROM " + ID_644557694_i.config.adapter.collection_name;
                var db = Ti.Database.open(ID_644557694_i.config.adapter.db_name);
                db.execute(sql);
                db.close();
                ID_644557694_i.trigger('remove');
                var ID_217861599_i = Alloy.Collections.tareas;
                var sql = "DELETE FROM " + ID_217861599_i.config.adapter.collection_name;
                var db = Ti.Database.open(ID_217861599_i.config.adapter.db_name);
                db.execute(sql);
                db.close();
                ID_217861599_i.trigger('remove');
                var ID_95859824_i = Alloy.Collections.emergencia;
                var sql = "DELETE FROM " + ID_95859824_i.config.adapter.collection_name;
                var db = Ti.Database.open(ID_95859824_i.config.adapter.db_name);
                db.execute(sql);
                db.close();
                ID_95859824_i.trigger('remove');
                var ID_548050463_i = Alloy.Collections.historial_tareas;
                var sql = "DELETE FROM " + ID_548050463_i.config.adapter.collection_name;
                var db = Ti.Database.open(ID_548050463_i.config.adapter.db_name);
                db.execute(sql);
                db.close();
                ID_548050463_i.trigger('remove');
                /** 
                 * alias para simplificar campos de inspector. 
                 */
                inspector = elemento.inspector;
                var ID_911234606_m = Alloy.Collections.inspectores;
                var ID_911234606_fila = Alloy.createModel('inspectores', {
                    apellido_materno: inspector.apellido_materno,
                    id_nivel1: inspector.nivel_1,
                    lat_dir: inspector.lat_dir,
                    disponibilidad_viajar_pais: inspector.disponibilidad_viajar_pais,
                    uuid: inspector.uidd,
                    fecha_nacimiento: inspector.fecha_nacimiento,
                    d1: inspector.d1,
                    d2: inspector.d2,
                    password: inspector.password,
                    estado: inspector.estado,
                    pais: inspector.pais,
                    direccion: inspector.direccion,
                    d3: inspector.d3,
                    nivel3: inspector.nivel_3,
                    d5: inspector.d5,
                    d4: inspector.d4,
                    disponibilidad_fechas: inspector.disponibilidad_fechas,
                    d7: inspector.d7,
                    nivel4: inspector.nivel_4,
                    nombre: inspector.nombre,
                    nivel5: inspector.nivel_5,
                    disponibilidad_horas: inspector.disponibilidad_horas,
                    nivel2: inspector.nivel_2,
                    lon_dir: inspector.lon_dir,
                    disponibilidad_viajar_ciudad: inspector.disponibilidad_viajar_ciudad,
                    id_server: inspector.id,
                    d6: inspector.d6,
                    telefono: inspector.telefono,
                    experiencia_detalle: inspector.experiencia_detalle,
                    disponibilidad: inspector.disponibilidad,
                    experiencia_oficio: inspector.id_experencia_oficio,
                    codigo_identificador: inspector.codigo_identificador,
                    apellido_paterno: inspector.apellido_paterno,
                    correo: inspector.correo
                });
                ID_911234606_m.add(ID_911234606_fila);
                ID_911234606_fila.save();
                var elemento_tareas_historial_tareas = elemento.tareas.historial_tareas;
                var ID_352267062_m = Alloy.Collections.historial_tareas;
                var db_ID_352267062 = Ti.Database.open(ID_352267062_m.config.adapter.db_name);
                db_ID_352267062.execute('BEGIN');
                _.each(elemento_tareas_historial_tareas, function(ID_352267062_fila, pos) {
                    db_ID_352267062.execute('INSERT INTO historial_tareas (fecha_tarea, id_inspeccion, id_asegurado, nivel_2, comentario_can_o_rech, asegurado_tel_fijo, estado_tarea, bono, evento, id_inspector, asegurado_codigo_identificador, lat, nivel_1, asegurado_nombre, pais, direccion, asegurador, fecha_ingreso, fecha_siniestro, nivel_1_texto, distance, fecha_termino, nivel_4, perfil, asegurado_id, id_server, categoria, nivel_3, asegurado_correo, num_caso, lon, asegurado_tel_movil, nivel_5, tipo_tarea, hora_termino) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', ID_352267062_fila.fecha_tarea, ID_352267062_fila.id_inspeccion, ID_352267062_fila.id_asegurado, ID_352267062_fila.nivel_2, ID_352267062_fila.comentario_can_o_rech, ID_352267062_fila.asegurado_tel_fijo, ID_352267062_fila.estado_tarea, ID_352267062_fila.bono, ID_352267062_fila.evento, ID_352267062_fila.id_inspector, ID_352267062_fila.asegurado_codigo_identificador, ID_352267062_fila.lat, ID_352267062_fila.nivel_1, ID_352267062_fila.asegurado_nombre, ID_352267062_fila.pais, ID_352267062_fila.direccion, ID_352267062_fila.asegurador, ID_352267062_fila.fecha_ingreso, ID_352267062_fila.fecha_siniestro, ID_352267062_fila.nivel_1_, ID_352267062_fila.distancia, ID_352267062_fila.fecha_finalizacion, ID_352267062_fila.nivel_4, 'ubicacion', ID_352267062_fila.asegurado_id, ID_352267062_fila.id, ID_352267062_fila.categoria, ID_352267062_fila.nivel_3, ID_352267062_fila.asegurado_correo, ID_352267062_fila.num_caso, ID_352267062_fila.lon, ID_352267062_fila.asegurado_tel_movil, ID_352267062_fila.nivel_5, ID_352267062_fila.tipo_tarea, ID_352267062_fila.hora_finalizacion);
                });
                db_ID_352267062.execute('COMMIT');
                db_ID_352267062.close();
                db_ID_352267062 = null;
                ID_352267062_m.trigger('change');
                var elemento_tareas_mistareas = elemento.tareas.mistareas;
                if (false) console.log('detalle mistareas...', {
                    "datos": elemento_tareas_mistareas
                });
                var ID_1484445609_m = Alloy.Collections.tareas;
                var db_ID_1484445609 = Ti.Database.open(ID_1484445609_m.config.adapter.db_name);
                db_ID_1484445609.execute('BEGIN');
                _.each(elemento_tareas_mistareas, function(ID_1484445609_fila, pos) {
                    db_ID_1484445609.execute('INSERT INTO tareas (fecha_tarea, id_inspeccion, id_asegurado, nivel_2, comentario_can_o_rech, asegurado_tel_fijo, estado_tarea, bono, evento, id_inspector, asegurado_codigo_identificador, lat, nivel_1, asegurado_nombre, pais, direccion, asegurador, fecha_ingreso, fecha_siniestro, nivel_1_texto, distance, nivel_4, perfil, asegurado_id, pais_texto, id_server, categoria, nivel_3, asegurado_correo, num_caso, lon, asegurado_tel_movil, nivel_5, tipo_tarea) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', ID_1484445609_fila.fecha_tarea, ID_1484445609_fila.id_inspeccion, ID_1484445609_fila.id_asegurado, ID_1484445609_fila.nivel_2, ID_1484445609_fila.comentario_can_o_rech, ID_1484445609_fila.asegurado_tel_fijo, ID_1484445609_fila.estado_tarea, ID_1484445609_fila.bono, ID_1484445609_fila.evento, ID_1484445609_fila.id_inspector, ID_1484445609_fila.asegurado_codigo_identificador, ID_1484445609_fila.lat, ID_1484445609_fila.nivel_1, ID_1484445609_fila.asegurado_nombre, ID_1484445609_fila.pais, ID_1484445609_fila.direccion, ID_1484445609_fila.asegurador, ID_1484445609_fila.fecha_ingreso, ID_1484445609_fila.fecha_siniestro, ID_1484445609_fila.nivel_1_, ID_1484445609_fila.distancia, ID_1484445609_fila.nivel_4, 'ubicacion', ID_1484445609_fila.asegurado_id, ID_1484445609_fila.pais, ID_1484445609_fila.id, ID_1484445609_fila.categoria, ID_1484445609_fila.nivel_3, ID_1484445609_fila.asegurado_correo, ID_1484445609_fila.num_caso, ID_1484445609_fila.lon, ID_1484445609_fila.asegurado_tel_movil, ID_1484445609_fila.nivel_5, ID_1484445609_fila.tipo_tarea);
                });
                db_ID_1484445609.execute('COMMIT');
                db_ID_1484445609.close();
                db_ID_1484445609 = null;
                ID_1484445609_m.trigger('change');
                var elemento_tareas_emergencias_perfil = elemento.tareas.emergencias.perfil;
                var ID_550736104_m = Alloy.Collections.emergencia;
                var db_ID_550736104 = Ti.Database.open(ID_550736104_m.config.adapter.db_name);
                db_ID_550736104.execute('BEGIN');
                _.each(elemento_tareas_emergencias_perfil, function(ID_550736104_fila, pos) {
                    db_ID_550736104.execute('INSERT INTO emergencia (fecha_tarea, id_inspeccion, id_asegurado, nivel_2, comentario_can_o_rech, asegurado_tel_fijo, estado_tarea, bono, evento, id_inspector, asegurado_codigo_identificador, lat, nivel_1, asegurado_nombre, pais, direccion, asegurador, fecha_ingreso, fecha_siniestro, nivel_1_texto, distance, nivel_4, perfil, asegurado_id, pais_texto, id_server, categoria, nivel_3, asegurado_correo, num_caso, lon, asegurado_tel_movil, distancia_2, nivel_5, tipo_tarea) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', ID_550736104_fila.fecha_tarea, ID_550736104_fila.id_inspeccion, ID_550736104_fila.id_asegurado, ID_550736104_fila.nivel_2, ID_550736104_fila.comentario_can_o_rech, ID_550736104_fila.asegurado_tel_fijo, ID_550736104_fila.estado_tarea, ID_550736104_fila.bono, ID_550736104_fila.evento, ID_550736104_fila.id_inspector, ID_550736104_fila.asegurado_codigo_identificador, ID_550736104_fila.lat, ID_550736104_fila.nivel_1, ID_550736104_fila.asegurado_nombre, ID_550736104_fila.pais, ID_550736104_fila.direccion, ID_550736104_fila.asegurador, ID_550736104_fila.fecha_ingreso, ID_550736104_fila.fecha_siniestro, ID_550736104_fila.nivel_1_, ID_550736104_fila.distancia, ID_550736104_fila.nivel_4, 'casa', ID_550736104_fila.asegurado_id, ID_550736104_fila.pais, ID_550736104_fila.id, ID_550736104_fila.categoria, ID_550736104_fila.nivel_3, ID_550736104_fila.asegurado_correo, ID_550736104_fila.num_caso, ID_550736104_fila.lon, ID_550736104_fila.asegurado_tel_movil, ID_550736104_fila.distancia_2, ID_550736104_fila.nivel_5, ID_550736104_fila.tipo_tarea);
                });
                db_ID_550736104.execute('COMMIT');
                db_ID_550736104.close();
                db_ID_550736104 = null;
                ID_550736104_m.trigger('change');
                var elemento_tareas_emergencias_ubicacion = elemento.tareas.emergencias.ubicacion;
                var ID_465294225_m = Alloy.Collections.emergencia;
                var db_ID_465294225 = Ti.Database.open(ID_465294225_m.config.adapter.db_name);
                db_ID_465294225.execute('BEGIN');
                _.each(elemento_tareas_emergencias_ubicacion, function(ID_465294225_fila, pos) {
                    db_ID_465294225.execute('INSERT INTO emergencia (fecha_tarea, id_inspeccion, id_asegurado, nivel_2, comentario_can_o_rech, asegurado_tel_fijo, estado_tarea, bono, evento, id_inspector, asegurado_codigo_identificador, lat, nivel_1, asegurado_nombre, pais, direccion, asegurador, fecha_ingreso, fecha_siniestro, nivel_1_texto, distance, nivel_4, perfil, asegurado_id, pais_texto, id_server, categoria, nivel_3, asegurado_correo, num_caso, lon, asegurado_tel_movil, distancia_2, nivel_5, tipo_tarea) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', ID_465294225_fila.fecha_tarea, ID_465294225_fila.id_inspeccion, ID_465294225_fila.id_asegurado, ID_465294225_fila.nivel_2, ID_465294225_fila.comentario_can_o_rech, ID_465294225_fila.asegurado_tel_fijo, ID_465294225_fila.estado_tarea, ID_465294225_fila.bono, ID_465294225_fila.evento, ID_465294225_fila.id_inspector, ID_465294225_fila.asegurado_codigo_identificador, ID_465294225_fila.lat, ID_465294225_fila.nivel_1, ID_465294225_fila.asegurado_nombre, ID_465294225_fila.pais, ID_465294225_fila.direccion, ID_465294225_fila.asegurador, ID_465294225_fila.fecha_ingreso, ID_465294225_fila.fecha_siniestro, ID_465294225_fila.nivel_1_, ID_465294225_fila.distancia, ID_465294225_fila.nivel_4, 'ubicacion', ID_465294225_fila.asegurado_id, ID_465294225_fila.pais, ID_465294225_fila.id, ID_465294225_fila.categoria, ID_465294225_fila.nivel_3, ID_465294225_fila.asegurado_correo, ID_465294225_fila.num_caso, ID_465294225_fila.lon, ID_465294225_fila.asegurado_tel_movil, ID_465294225_fila.distancia_2, ID_465294225_fila.nivel_5, ID_465294225_fila.tipo_tarea);
                });
                db_ID_465294225.execute('COMMIT');
                db_ID_465294225.close();
                db_ID_465294225 = null;
                ID_465294225_m.trigger('change');
                var elemento_tareas_entrantes_critica = elemento.tareas.entrantes.critica;
                var ID_1863452207_m = Alloy.Collections.tareas_entrantes;
                var db_ID_1863452207 = Ti.Database.open(ID_1863452207_m.config.adapter.db_name);
                db_ID_1863452207.execute('BEGIN');
                _.each(elemento_tareas_entrantes_critica, function(ID_1863452207_fila, pos) {
                    db_ID_1863452207.execute('INSERT INTO tareas_entrantes (fecha_tarea, id_inspeccion, id_asegurado, nivel_2, comentario_can_o_rech, asegurado_tel_fijo, estado_tarea, bono, evento, id_inspector, asegurado_codigo_identificador, lat, nivel_1, asegurado_nombre, pais, direccion, asegurador, fecha_ingreso, fecha_siniestro, nivel_1_texto, distance, nivel_4, perfil, asegurado_id, pais_texto, id_server, categoria, nivel_3, asegurado_correo, num_caso, lon, asegurado_tel_movil, tipo_tarea, nivel_5) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', ID_1863452207_fila.fecha_tarea, ID_1863452207_fila.id_inspeccion, ID_1863452207_fila.id_asegurado, ID_1863452207_fila.nivel_2, ID_1863452207_fila.comentario_can_o_rech, ID_1863452207_fila.asegurado_tel_fijo, ID_1863452207_fila.estado_tarea, ID_1863452207_fila.bono, ID_1863452207_fila.evento, ID_1863452207_fila.id_inspector, ID_1863452207_fila.asegurado_codigo_identificador, ID_1863452207_fila.lat, ID_1863452207_fila.nivel_1, ID_1863452207_fila.asegurado_nombre, ID_1863452207_fila.pais, ID_1863452207_fila.direccion, ID_1863452207_fila.asegurador, ID_1863452207_fila.fecha_ingreso, ID_1863452207_fila.fecha_siniestro, ID_1863452207_fila.nivel_1_, ID_1863452207_fila.distancia, ID_1863452207_fila.nivel_4, 'casa', ID_1863452207_fila.asegurado_id, ID_1863452207_fila.pais, ID_1863452207_fila.id, ID_1863452207_fila.categoria, ID_1863452207_fila.nivel_3, ID_1863452207_fila.asegurado_correo, ID_1863452207_fila.num_caso, ID_1863452207_fila.lon, ID_1863452207_fila.asegurado_tel_movil, ID_1863452207_fila.tipo_tarea, ID_1863452207_fila.nivel_5);
                });
                db_ID_1863452207.execute('COMMIT');
                db_ID_1863452207.close();
                db_ID_1863452207 = null;
                ID_1863452207_m.trigger('change');
                var elemento_tareas_entrantes_normal = elemento.tareas.entrantes.normal;
                var ID_1099285299_m = Alloy.Collections.tareas_entrantes;
                var db_ID_1099285299 = Ti.Database.open(ID_1099285299_m.config.adapter.db_name);
                db_ID_1099285299.execute('BEGIN');
                _.each(elemento_tareas_entrantes_normal, function(ID_1099285299_fila, pos) {
                    db_ID_1099285299.execute('INSERT INTO tareas_entrantes (fecha_tarea, id_inspeccion, id_asegurado, nivel_2, comentario_can_o_rech, asegurado_tel_fijo, estado_tarea, bono, evento, id_inspector, asegurado_codigo_identificador, lat, nivel_1, asegurado_nombre, pais, direccion, asegurador, fecha_ingreso, fecha_siniestro, nivel_1_texto, distance, nivel_4, perfil, asegurado_id, pais_texto, id_server, categoria, nivel_3, asegurado_correo, num_caso, lon, asegurado_tel_movil, tipo_tarea, nivel_5) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', ID_1099285299_fila.fecha_tarea, ID_1099285299_fila.id_inspeccion, ID_1099285299_fila.id_asegurado, ID_1099285299_fila.nivel_2, ID_1099285299_fila.comentario_can_o_rech, ID_1099285299_fila.asegurado_tel_fijo, ID_1099285299_fila.estado_tarea, ID_1099285299_fila.bono, ID_1099285299_fila.evento, ID_1099285299_fila.id_inspector, ID_1099285299_fila.asegurado_codigo_identificador, ID_1099285299_fila.lat, ID_1099285299_fila.nivel_1, ID_1099285299_fila.asegurado_nombre, ID_1099285299_fila.pais, ID_1099285299_fila.direccion, ID_1099285299_fila.asegurador, ID_1099285299_fila.fecha_ingreso, ID_1099285299_fila.fecha_siniestro, ID_1099285299_fila.nivel_1_, ID_1099285299_fila.distancia, ID_1099285299_fila.nivel_4, 'casa', ID_1099285299_fila.asegurado_id, ID_1099285299_fila.pais, ID_1099285299_fila.id, ID_1099285299_fila.categoria, ID_1099285299_fila.nivel_3, ID_1099285299_fila.asegurado_correo, ID_1099285299_fila.num_caso, ID_1099285299_fila.lon, ID_1099285299_fila.asegurado_tel_movil, ID_1099285299_fila.tipo_tarea, ID_1099285299_fila.nivel_5);
                });
                db_ID_1099285299.execute('COMMIT');
                db_ID_1099285299.close();
                db_ID_1099285299 = null;
                ID_1099285299_m.trigger('change');
                /** 
                 * Al terminar la carga de datos de la base de datos, llamamos una funcion para continuar con el proceso 
                 */
                var ID_1038535132 = null;
                if ('cargar_selectores' in require('funciones')) {
                    ID_1038535132 = require('funciones').cargar_selectores({});
                } else {
                    try {
                        ID_1038535132 = f_cargar_selectores({});
                    } catch (ee) {}
                }
            } else {
                /** 
                 * Y si es un error que no tenemos registrado, mostramos el mensaje directamente en una alerta 
                 */
                var ID_1626215927 = null;
                if ('mostrar_botones' in require('funciones')) {
                    ID_1626215927 = require('funciones').mostrar_botones({});
                } else {
                    try {
                        ID_1626215927 = f_mostrar_botones({});
                    } catch (ee) {}
                }
                var ID_927055317_opts = [L('x1518866076_traducir', 'Aceptar')];
                var ID_927055317 = Ti.UI.createAlertDialog({
                    title: L('x57652245_traducir', 'Alerta'),
                    message: '' + String.format(L('x921749426_traducir', 'Error %1$s'), elemento.mensaje.toString()) + '',
                    buttonNames: ID_927055317_opts
                });
                ID_927055317.addEventListener('click', function(e) {
                    var res = ID_927055317_opts[e.index];
                    res = null;

                    e.source.removeEventListener("click", arguments.callee);
                });
                ID_927055317.show();
                $.ID_908004102.setText('status: esperando');

            }
            elemento = null, valor = null;
        };

        ID_822595707.error = function(e) {
            var elemento = e,
                valor = e;
            var ID_697710795 = null;
            if ('mostrar_botones' in require('funciones')) {
                ID_697710795 = require('funciones').mostrar_botones({});
            } else {
                try {
                    ID_697710795 = f_mostrar_botones({});
                } catch (ee) {}
            }
            var ID_552165355_opts = [L('x1518866076_traducir', 'Aceptar')];
            var ID_552165355 = Ti.UI.createAlertDialog({
                title: L('x57652245_traducir', 'Alerta'),
                message: L('x3632753785_traducir', 'Problema de conexion, favor intentar más tarde'),
                buttonNames: ID_552165355_opts
            });
            ID_552165355.addEventListener('click', function(e) {
                var suu = ID_552165355_opts[e.index];
                suu = null;

                e.source.removeEventListener("click", arguments.callee);
            });
            ID_552165355.show();
            $.ID_908004102.setText('status: esperando');

            elemento = null, valor = null;
        };
        require('helper').ajaxUnico('ID_822595707', '' + String.format(L('x796226125', '%1$slogin'), url_server.toString()) + '', 'POST', {
            correo: correo,
            password: password,
            device: Ti.Platform.name,
            lat: gps_latitud,
            lon: gps_longitud,
            device_token: devicetoken,
            uidd: uidd,
            fecha: dif
        }, 15000, ID_822595707);
    }
    return null;
};
var f_cargar_selectores = function(x_params) {
    var item = x_params['item'];
    $.ID_908004102.setText('status: cargando selectores');

    /** 
     * Consultamos la tabla de inspectores y su resultado lo guardamos en la variable inspector 
     */
    var ID_568274345_i = Alloy.createCollection('inspectores');
    var ID_568274345_i_where = '';
    ID_568274345_i.fetch();
    var inspector = require('helper').query2array(ID_568274345_i);
    var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
    var selectores_fechas = JSON.parse(Ti.App.Properties.getString('selectores_fechas'));
    if (inspector && inspector.length) {
        /** 
         * guardamos la variable: inspector con los datos del inspector activo para no tener que consultarla cada vez dentro del menu. 
         */
        require('vars')['inspector'] = inspector[0];
        if (false) console.log('psb inspector cargado', {
            "inspector": inspector[0]
        });
        /** 
         * Consultamos la tabla de pais, filtrando por el pais del inspector, guardamos la consulta en una variable pais 
         */
        var ID_499816964_i = Alloy.createCollection('pais');
        var ID_499816964_i_where = 'id_server=\'' + inspector[0].pais + '\'';
        ID_499816964_i.fetch({
            query: 'SELECT * FROM pais WHERE id_server=\'' + inspector[0].pais + '\''
        });
        var pais = require('helper').query2array(ID_499816964_i);
        if (pais && pais.length) {
            require('vars')['pais'] = pais;
            var defecto = {
                fecha: ''
            };
            /** 
             * Generamos una estructura con los datos de los selectores y guardando la fecha (de consulta) en vacio 
             */
            var selectores_fechas = {
                destino: defecto,
                compania: defecto,
                entidad_financiera: defecto,
                tipo_siniestro: defecto,
                estructura_soportante: defecto,
                muros_tabiques: defecto,
                entrepisos: defecto,
                pavimento: defecto,
                estructura_cubierta: defecto,
                cubierta: defecto,
                partida: defecto,
                bienes: defecto,
                tipo_dano: defecto,
                marcas: defecto,
                monedas: defecto
            };
            if (false) console.log('consultando selectores', {});
            if (false) console.log('detalle pais', {
                "asd": pais
            });
            var ID_925164431 = {};

            ID_925164431.success = function(e) {
                var elemento = e,
                    valor = e;
                if (elemento.error == 0 || elemento.error == '0') {
                    var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
                    if (false) console.log('Mi lenguage a ocupar es', {
                        "leng": pais[0].lenguaje
                    });
                    var selectores_fechas = JSON.parse(Ti.App.Properties.getString('selectores_fechas'));
                    /** 
                     * Creamos una variable selectores_list y agregamos un campo nuevo, que sera la fecha. Despues los ordenamos segun el nombre del selector 
                     */

                    selectores_list = _.map(elemento.selectores,
                        function(num) {
                            var object = {
                                selector: num.selector,
                                fecha: num.fecha
                            };
                            return object;
                        });
                    selectores_fechas = _.indexBy(selectores_list, 'selector');
                    /** 
                     * Guardamos la variable selectores_fecha para que sea de acceso permanente, (que no se pierda despues de una vez cerrado) 
                     */
                    Ti.App.Properties.setString('selectores_fechas', JSON.stringify(selectores_fechas));
                    /** 
                     * Limpiamos las tablas, insertamos los datos desde el servidor y vamos modificando el texto que muestra en pantalla el status de los datos que estan siendo almacenados 
                     */
                    if (false) console.log('Ingresando datos de pais actual', {});
                    if (false) console.log(String.format(L('x869747243_traducir', 'eliminando e ingresando: destino (%1$s)'), elemento.destino.length.toString()), {});
                    var ID_1468372734_i = Alloy.Collections.destino;
                    var sql = 'DELETE FROM ' + ID_1468372734_i.config.adapter.collection_name + ' WHERE pais_texto=\'' + pais[0].nombre + '\'';
                    var db = Ti.Database.open(ID_1468372734_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_1468372734_i.trigger('remove');
                    var elemento_destino = elemento.destino;
                    var ID_596906264_m = Alloy.Collections.destino;
                    var db_ID_596906264 = Ti.Database.open(ID_596906264_m.config.adapter.db_name);
                    db_ID_596906264.execute('BEGIN');
                    _.each(elemento_destino, function(ID_596906264_fila, pos) {
                        db_ID_596906264.execute('INSERT INTO destino (nombre, fecha, pais_texto, id_server, id_segured, pais) VALUES (?,?,?,?,?,?)', ID_596906264_fila.valor, ID_596906264_fila.fecha, ID_596906264_fila.pais_texto, ID_596906264_fila.id, ID_596906264_fila.id_segured, ID_596906264_fila.pais);
                    });
                    db_ID_596906264.execute('COMMIT');
                    db_ID_596906264.close();
                    db_ID_596906264 = null;
                    ID_596906264_m.trigger('change');
                    $.ID_908004102.setText('status: insertando destinos');

                    if (false) console.log(String.format(L('x3307041009_traducir', 'eliminando e ingresando: compania (%1$s)'), elemento.compania.length.toString()), {});
                    var ID_1095926658_i = Alloy.Collections.compania;
                    var sql = 'DELETE FROM ' + ID_1095926658_i.config.adapter.collection_name + ' WHERE pais_texto=\'' + pais[0].nombre + '\'';
                    var db = Ti.Database.open(ID_1095926658_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_1095926658_i.trigger('remove');
                    var elemento_compania = elemento.compania;
                    var ID_1990870660_m = Alloy.Collections.compania;
                    var db_ID_1990870660 = Ti.Database.open(ID_1990870660_m.config.adapter.db_name);
                    db_ID_1990870660.execute('BEGIN');
                    _.each(elemento_compania, function(ID_1990870660_fila, pos) {
                        db_ID_1990870660.execute('INSERT INTO compania (nombre, fecha, id_server, pais_texto, id_segured, pais) VALUES (?,?,?,?,?,?)', ID_1990870660_fila.valor, ID_1990870660_fila.fecha, ID_1990870660_fila.id, ID_1990870660_fila.pais_texto, ID_1990870660_fila.id_segured, ID_1990870660_fila.pais);
                    });
                    db_ID_1990870660.execute('COMMIT');
                    db_ID_1990870660.close();
                    db_ID_1990870660 = null;
                    ID_1990870660_m.trigger('change');
                    $.ID_908004102.setText('status: insertando companias');

                    if (false) console.log(String.format(L('x1259104841_traducir', 'eliminando e ingresando: entidad_financiera (%1$s)'), elemento.entidad_financiera.length.toString()), {});
                    var ID_600453898_i = Alloy.Collections.entidad_financiera;
                    var sql = 'DELETE FROM ' + ID_600453898_i.config.adapter.collection_name + ' WHERE pais_texto=\'' + pais[0].nombre + '\'';
                    var db = Ti.Database.open(ID_600453898_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_600453898_i.trigger('remove');
                    var elemento_entidad_financiera = elemento.entidad_financiera;
                    var ID_118910868_m = Alloy.Collections.entidad_financiera;
                    var db_ID_118910868 = Ti.Database.open(ID_118910868_m.config.adapter.db_name);
                    db_ID_118910868.execute('BEGIN');
                    _.each(elemento_entidad_financiera, function(ID_118910868_fila, pos) {
                        db_ID_118910868.execute('INSERT INTO entidad_financiera (nombre, fecha, id_server, pais_texto, id_segured, pais) VALUES (?,?,?,?,?,?)', ID_118910868_fila.valor, ID_118910868_fila.fecha, ID_118910868_fila.id, ID_118910868_fila.pais_texto, ID_118910868_fila.id_segured, ID_118910868_fila.pais);
                    });
                    db_ID_118910868.execute('COMMIT');
                    db_ID_118910868.close();
                    db_ID_118910868 = null;
                    ID_118910868_m.trigger('change');
                    $.ID_908004102.setText('status: insertando entidades');

                    if (false) console.log(String.format(L('x2170187839_traducir', 'eliminando e ingresando: tipo_siniestro (%1$s)'), elemento.tipo_siniestro.length.toString()), {});
                    var ID_589980159_i = Alloy.Collections.tipo_siniestro;
                    var sql = 'DELETE FROM ' + ID_589980159_i.config.adapter.collection_name + ' WHERE pais_texto=\'' + pais[0].nombre + '\'';
                    var db = Ti.Database.open(ID_589980159_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_589980159_i.trigger('remove');
                    var elemento_tipo_siniestro = elemento.tipo_siniestro;
                    var ID_529131790_m = Alloy.Collections.tipo_siniestro;
                    var db_ID_529131790 = Ti.Database.open(ID_529131790_m.config.adapter.db_name);
                    db_ID_529131790.execute('BEGIN');
                    _.each(elemento_tipo_siniestro, function(ID_529131790_fila, pos) {
                        db_ID_529131790.execute('INSERT INTO tipo_siniestro (nombre, fecha, id_server, pais_texto, id_segured, pais) VALUES (?,?,?,?,?,?)', ID_529131790_fila.valor, ID_529131790_fila.fecha, ID_529131790_fila.id, ID_529131790_fila.pais_texto, ID_529131790_fila.id_segured, ID_529131790_fila.pais);
                    });
                    db_ID_529131790.execute('COMMIT');
                    db_ID_529131790.close();
                    db_ID_529131790 = null;
                    ID_529131790_m.trigger('change');
                    $.ID_908004102.setText(String.format(L('x4131184409_traducir', 'status: insertando %1$s tipo siniestros'), elemento.tipo_siniestro.length.toString()));

                    $.ID_908004102.setText('status: insertando tipo siniestro');

                    if (false) console.log(String.format(L('x1439237550_traducir', 'eliminando e ingresando: estructura_soportante (%1$s)'), elemento.estructura_soportante.length.toString()), {});
                    var ID_1284610492_i = Alloy.Collections.estructura_soportante;
                    var sql = 'DELETE FROM ' + ID_1284610492_i.config.adapter.collection_name + ' WHERE pais_texto=\'' + pais[0].nombre + '\'';
                    var db = Ti.Database.open(ID_1284610492_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_1284610492_i.trigger('remove');
                    var elemento_estructura_soportante = elemento.estructura_soportante;
                    var ID_444252329_m = Alloy.Collections.estructura_soportante;
                    var db_ID_444252329 = Ti.Database.open(ID_444252329_m.config.adapter.db_name);
                    db_ID_444252329.execute('BEGIN');
                    _.each(elemento_estructura_soportante, function(ID_444252329_fila, pos) {
                        db_ID_444252329.execute('INSERT INTO estructura_soportante (nombre, fecha, id_server, pais_texto, id_segured, pais) VALUES (?,?,?,?,?,?)', ID_444252329_fila.valor, ID_444252329_fila.fecha, ID_444252329_fila.id, ID_444252329_fila.pais_texto, ID_444252329_fila.id_segured, ID_444252329_fila.pais);
                    });
                    db_ID_444252329.execute('COMMIT');
                    db_ID_444252329.close();
                    db_ID_444252329 = null;
                    ID_444252329_m.trigger('change');
                    $.ID_908004102.setText(String.format(L('x1654897630_traducir', 'status: insertando %1$s'), elemento.estructura_soportante.length.toString()));

                    $.ID_908004102.setText('status: insertando tipo estructuras soportantes');

                    if (false) console.log(String.format(L('x112713061_traducir', 'eliminando e ingresando: muros_tabiques (%1$s)'), elemento.muros_tabiques.length.toString()), {});
                    var ID_939743662_i = Alloy.Collections.muros_tabiques;
                    var sql = 'DELETE FROM ' + ID_939743662_i.config.adapter.collection_name + ' WHERE pais_texto=\'' + pais[0].nombre + '\'';
                    var db = Ti.Database.open(ID_939743662_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_939743662_i.trigger('remove');
                    var elemento_muros_tabiques = elemento.muros_tabiques;
                    var ID_1624016418_m = Alloy.Collections.muros_tabiques;
                    var db_ID_1624016418 = Ti.Database.open(ID_1624016418_m.config.adapter.db_name);
                    db_ID_1624016418.execute('BEGIN');
                    _.each(elemento_muros_tabiques, function(ID_1624016418_fila, pos) {
                        db_ID_1624016418.execute('INSERT INTO muros_tabiques (nombre, fecha, id_server, pais_texto, id_segured, pais) VALUES (?,?,?,?,?,?)', ID_1624016418_fila.valor, ID_1624016418_fila.fecha, ID_1624016418_fila.id, ID_1624016418_fila.pais_texto, ID_1624016418_fila.id_segured, ID_1624016418_fila.pais);
                    });
                    db_ID_1624016418.execute('COMMIT');
                    db_ID_1624016418.close();
                    db_ID_1624016418 = null;
                    ID_1624016418_m.trigger('change');
                    $.ID_908004102.setText('status: insertando tipo muro tabiques');

                    if (false) console.log(String.format(L('x322969612_traducir', 'eliminando e ingresando: entrepisos (%1$s)'), elemento.entrepisos.length.toString()), {});
                    var ID_1066270229_i = Alloy.Collections.entrepisos;
                    var sql = 'DELETE FROM ' + ID_1066270229_i.config.adapter.collection_name + ' WHERE pais_texto=\'' + pais[0].nombre + '\'';
                    var db = Ti.Database.open(ID_1066270229_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_1066270229_i.trigger('remove');
                    var elemento_entrepisos = elemento.entrepisos;
                    var ID_1971500432_m = Alloy.Collections.entrepisos;
                    var db_ID_1971500432 = Ti.Database.open(ID_1971500432_m.config.adapter.db_name);
                    db_ID_1971500432.execute('BEGIN');
                    _.each(elemento_entrepisos, function(ID_1971500432_fila, pos) {
                        db_ID_1971500432.execute('INSERT INTO entrepisos (nombre, fecha, id_server, pais_texto, id_segured, pais) VALUES (?,?,?,?,?,?)', ID_1971500432_fila.valor, ID_1971500432_fila.fecha, ID_1971500432_fila.id, ID_1971500432_fila.pais_texto, ID_1971500432_fila.id_segured, ID_1971500432_fila.pais);
                    });
                    db_ID_1971500432.execute('COMMIT');
                    db_ID_1971500432.close();
                    db_ID_1971500432 = null;
                    ID_1971500432_m.trigger('change');
                    $.ID_908004102.setText('status: insertando entrepisos');

                    if (false) console.log(String.format(L('x2733921830_traducir', 'eliminando e ingresando: pavimento (%1$s)'), elemento.pavimento.length.toString()), {});
                    var ID_1710719702_i = Alloy.Collections.pavimento;
                    var sql = 'DELETE FROM ' + ID_1710719702_i.config.adapter.collection_name + ' WHERE pais_texto=\'' + pais[0].nombre + '\'';
                    var db = Ti.Database.open(ID_1710719702_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_1710719702_i.trigger('remove');
                    var elemento_pavimento = elemento.pavimento;
                    var ID_1003942204_m = Alloy.Collections.pavimento;
                    var db_ID_1003942204 = Ti.Database.open(ID_1003942204_m.config.adapter.db_name);
                    db_ID_1003942204.execute('BEGIN');
                    _.each(elemento_pavimento, function(ID_1003942204_fila, pos) {
                        db_ID_1003942204.execute('INSERT INTO pavimento (nombre, fecha, id_server, pais_texto, id_segured, pais) VALUES (?,?,?,?,?,?)', ID_1003942204_fila.valor, ID_1003942204_fila.fecha, ID_1003942204_fila.id, ID_1003942204_fila.pais_texto, ID_1003942204_fila.id_segured, ID_1003942204_fila.pais);
                    });
                    db_ID_1003942204.execute('COMMIT');
                    db_ID_1003942204.close();
                    db_ID_1003942204 = null;
                    ID_1003942204_m.trigger('change');
                    $.ID_908004102.setText('status: insertando tipo pavimento');

                    if (false) console.log(String.format(L('x4260007882_traducir', 'eliminando e ingresando: estructura_cubierta (%1$s)'), elemento.estructura_cubierta.length.toString()), {});
                    var ID_1397564298_i = Alloy.Collections.estructura_cubierta;
                    var sql = 'DELETE FROM ' + ID_1397564298_i.config.adapter.collection_name + ' WHERE pais_texto=\'' + pais[0].nombre + '\'';
                    var db = Ti.Database.open(ID_1397564298_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_1397564298_i.trigger('remove');
                    var elemento_estructura_cubierta = elemento.estructura_cubierta;
                    var ID_417351518_m = Alloy.Collections.estructura_cubierta;
                    var db_ID_417351518 = Ti.Database.open(ID_417351518_m.config.adapter.db_name);
                    db_ID_417351518.execute('BEGIN');
                    _.each(elemento_estructura_cubierta, function(ID_417351518_fila, pos) {
                        db_ID_417351518.execute('INSERT INTO estructura_cubierta (nombre, fecha, id_server, pais_texto, id_segured, pais) VALUES (?,?,?,?,?,?)', ID_417351518_fila.valor, ID_417351518_fila.fecha, ID_417351518_fila.id, ID_417351518_fila.pais_texto, ID_417351518_fila.id_segured, ID_417351518_fila.pais);
                    });
                    db_ID_417351518.execute('COMMIT');
                    db_ID_417351518.close();
                    db_ID_417351518 = null;
                    ID_417351518_m.trigger('change');
                    $.ID_908004102.setText('status: insertando tipo estructuras cubierta');

                    if (false) console.log(String.format(L('x4140625223_traducir', 'eliminando e ingresando: cubierta (%1$s)'), elemento.cubierta.length.toString()), {});
                    var ID_941658551_i = Alloy.Collections.cubierta;
                    var sql = 'DELETE FROM ' + ID_941658551_i.config.adapter.collection_name + ' WHERE pais_texto=\'' + pais[0].nombre + '\'';
                    var db = Ti.Database.open(ID_941658551_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_941658551_i.trigger('remove');
                    var elemento_cubierta = elemento.cubierta;
                    var ID_69140717_m = Alloy.Collections.cubierta;
                    var db_ID_69140717 = Ti.Database.open(ID_69140717_m.config.adapter.db_name);
                    db_ID_69140717.execute('BEGIN');
                    _.each(elemento_cubierta, function(ID_69140717_fila, pos) {
                        db_ID_69140717.execute('INSERT INTO cubierta (nombre, fecha, id_server, pais_texto, id_segured, pais) VALUES (?,?,?,?,?,?)', ID_69140717_fila.valor, ID_69140717_fila.fecha, ID_69140717_fila.id, ID_69140717_fila.pais_texto, ID_69140717_fila.id_segured, ID_69140717_fila.pais);
                    });
                    db_ID_69140717.execute('COMMIT');
                    db_ID_69140717.close();
                    db_ID_69140717 = null;
                    ID_69140717_m.trigger('change');
                    $.ID_908004102.setText('status: insertando cubiertas');

                    if (false) console.log(String.format(L('x56040633_traducir', 'eliminando e ingresando: tipo_partida (%1$s)'), elemento.partida.length.toString()), {});
                    var ID_559466842_i = Alloy.Collections.tipo_partida;
                    var sql = 'DELETE FROM ' + ID_559466842_i.config.adapter.collection_name + ' WHERE pais_texto=\'' + pais[0].nombre + '\'';
                    var db = Ti.Database.open(ID_559466842_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_559466842_i.trigger('remove');
                    var elemento_partida = elemento.partida;
                    var ID_1665005058_m = Alloy.Collections.tipo_partida;
                    var db_ID_1665005058 = Ti.Database.open(ID_1665005058_m.config.adapter.db_name);
                    db_ID_1665005058.execute('BEGIN');
                    _.each(elemento_partida, function(ID_1665005058_fila, pos) {
                        db_ID_1665005058.execute('INSERT INTO tipo_partida (nombre, fecha, id_server, pais_texto, id_segured, pais) VALUES (?,?,?,?,?,?)', ID_1665005058_fila.valor, ID_1665005058_fila.fecha, ID_1665005058_fila.id, ID_1665005058_fila.pais_texto, ID_1665005058_fila.id_segured, ID_1665005058_fila.pais);
                    });
                    db_ID_1665005058.execute('COMMIT');
                    db_ID_1665005058.close();
                    db_ID_1665005058 = null;
                    ID_1665005058_m.trigger('change');
                    $.ID_908004102.setText('status: insertando tipo partida');

                    if (false) console.log(String.format(L('x318662177_traducir', 'eliminando e ingresando: bienes (%1$s)'), elemento.bienes.length.toString()), {});
                    var ID_179036360_i = Alloy.Collections.bienes;
                    var sql = 'DELETE FROM ' + ID_179036360_i.config.adapter.collection_name + ' WHERE pais_texto=\'' + pais[0].nombre + '\'';
                    var db = Ti.Database.open(ID_179036360_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_179036360_i.trigger('remove');
                    var elemento_bienes = elemento.bienes;
                    var ID_722513862_m = Alloy.Collections.bienes;
                    var db_ID_722513862 = Ti.Database.open(ID_722513862_m.config.adapter.db_name);
                    db_ID_722513862.execute('BEGIN');
                    _.each(elemento_bienes, function(ID_722513862_fila, pos) {
                        db_ID_722513862.execute('INSERT INTO bienes (nombre, fecha, id_server, pais_texto, id_segured, pais) VALUES (?,?,?,?,?,?)', ID_722513862_fila.valor, ID_722513862_fila.fecha, ID_722513862_fila.id, ID_722513862_fila.pais_texto, ID_722513862_fila.id_segured, ID_722513862_fila.pais);
                    });
                    db_ID_722513862.execute('COMMIT');
                    db_ID_722513862.close();
                    db_ID_722513862 = null;
                    ID_722513862_m.trigger('change');
                    $.ID_908004102.setText('status: insertando bienes');

                    if (false) console.log(String.format(L('x2761353396_traducir', 'eliminando e ingresando: marcas (%1$s)'), elemento.marcas.length.toString()), {});
                    var ID_1181734088_i = Alloy.Collections.marcas;
                    var sql = 'DELETE FROM ' + ID_1181734088_i.config.adapter.collection_name + ' WHERE pais_texto=\'' + pais[0].nombre + '\'';
                    var db = Ti.Database.open(ID_1181734088_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_1181734088_i.trigger('remove');
                    var elemento_marcas = elemento.marcas;
                    var ID_1746983975_m = Alloy.Collections.marcas;
                    var db_ID_1746983975 = Ti.Database.open(ID_1746983975_m.config.adapter.db_name);
                    db_ID_1746983975.execute('BEGIN');
                    _.each(elemento_marcas, function(ID_1746983975_fila, pos) {
                        db_ID_1746983975.execute('INSERT INTO marcas (nombre, fecha, id_server, pais_texto, id_segured, pais) VALUES (?,?,?,?,?,?)', ID_1746983975_fila.valor, ID_1746983975_fila.fecha, ID_1746983975_fila.id, ID_1746983975_fila.pais_texto, ID_1746983975_fila.id_segured, ID_1746983975_fila.pais);
                    });
                    db_ID_1746983975.execute('COMMIT');
                    db_ID_1746983975.close();
                    db_ID_1746983975 = null;
                    ID_1746983975_m.trigger('change');
                    $.ID_908004102.setText('status: insertando marcas');

                    if (false) console.log(String.format(L('x2205107975_traducir', 'eliminando e ingresando: monedas (%1$s)'), elemento.monedas.length.toString()), {});
                    var ID_146080128_i = Alloy.Collections.monedas;
                    var sql = 'DELETE FROM ' + ID_146080128_i.config.adapter.collection_name + ' WHERE pais_texto=\'' + pais[0].nombre + '\'';
                    var db = Ti.Database.open(ID_146080128_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_146080128_i.trigger('remove');
                    var elemento_monedas = elemento.monedas;
                    var ID_809684354_m = Alloy.Collections.monedas;
                    var db_ID_809684354 = Ti.Database.open(ID_809684354_m.config.adapter.db_name);
                    db_ID_809684354.execute('BEGIN');
                    _.each(elemento_monedas, function(ID_809684354_fila, pos) {
                        db_ID_809684354.execute('INSERT INTO monedas (nombre, fecha, id_server, pais_texto, id_segured, pais) VALUES (?,?,?,?,?,?)', ID_809684354_fila.valor, ID_809684354_fila.fecha, ID_809684354_fila.id, ID_809684354_fila.pais_texto, ID_809684354_fila.id_segured, ID_809684354_fila.pais);
                    });
                    db_ID_809684354.execute('COMMIT');
                    db_ID_809684354.close();
                    db_ID_809684354 = null;
                    ID_809684354_m.trigger('change');
                    $.ID_908004102.setText('status: insertando monedas');

                    if (false) console.log(String.format(L('x656637966_traducir', 'eliminando e ingresando: tipo_dano (%1$s)'), elemento.tipo_dano.length.toString()), {});
                    var ID_418951995_i = Alloy.Collections.tipo_dano;
                    var sql = 'DELETE FROM ' + ID_418951995_i.config.adapter.collection_name + ' WHERE pais_texto=\'' + pais[0].nombre + '\'';
                    var db = Ti.Database.open(ID_418951995_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_418951995_i.trigger('remove');
                    var elemento_tipo_dano = elemento.tipo_dano;
                    var ID_164256080_m = Alloy.Collections.tipo_dano;
                    var db_ID_164256080 = Ti.Database.open(ID_164256080_m.config.adapter.db_name);
                    db_ID_164256080.execute('BEGIN');
                    _.each(elemento_tipo_dano, function(ID_164256080_fila, pos) {
                        db_ID_164256080.execute('INSERT INTO tipo_dano (nombre, fecha, id_partida, id_server, pais_texto, id_segured, pais) VALUES (?,?,?,?,?,?,?)', ID_164256080_fila.valor, ID_164256080_fila.fecha, ID_164256080_fila.id_partida, ID_164256080_fila.id, ID_164256080_fila.pais_texto, ID_164256080_fila.id_segured, ID_164256080_fila.pais);
                    });
                    db_ID_164256080.execute('COMMIT');
                    db_ID_164256080.close();
                    db_ID_164256080 = null;
                    ID_164256080_m.trigger('change');
                    $.ID_908004102.setText('status: insertando tipo danos');

                    if (false) console.log(String.format(L('x1231345309_traducir', 'eliminando e ingresando: tipo_accion (%1$s)'), elemento.accion.length.toString()), {});
                    var ID_431865586_i = Alloy.Collections.tipo_accion;
                    var sql = 'DELETE FROM ' + ID_431865586_i.config.adapter.collection_name + ' WHERE pais_texto=\'' + pais[0].nombre + '\'';
                    var db = Ti.Database.open(ID_431865586_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_431865586_i.trigger('remove');
                    var elemento_accion = elemento.accion;
                    var ID_933459760_m = Alloy.Collections.tipo_accion;
                    var db_ID_933459760 = Ti.Database.open(ID_933459760_m.config.adapter.db_name);
                    db_ID_933459760.execute('BEGIN');
                    _.each(elemento_accion, function(ID_933459760_fila, pos) {
                        db_ID_933459760.execute('INSERT INTO tipo_accion (nombre, id_tipo_dano, fecha, id_server, pais_texto, id_segured, pais) VALUES (?,?,?,?,?,?,?)', ID_933459760_fila.valor, ID_933459760_fila.id_tipo_dano, ID_933459760_fila.fecha, ID_933459760_fila.id, ID_933459760_fila.pais_texto, ID_933459760_fila.id_segured, ID_933459760_fila.pais);
                    });
                    db_ID_933459760.execute('COMMIT');
                    db_ID_933459760.close();
                    db_ID_933459760 = null;
                    ID_933459760_m.trigger('change');
                    $.ID_908004102.setText('status: insertando tipo accion');

                    if (false) console.log(String.format(L('x3902904863_traducir', 'eliminando e ingresando: unidad_medida (%1$s)'), elemento.unidad_medida.length.toString()), {});
                    var ID_1008136239_i = Alloy.Collections.unidad_medida;
                    var sql = 'DELETE FROM ' + ID_1008136239_i.config.adapter.collection_name + ' WHERE pais_texto=\'' + pais[0].nombre + '\'';
                    var db = Ti.Database.open(ID_1008136239_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_1008136239_i.trigger('remove');
                    var elemento_unidad_medida = elemento.unidad_medida;
                    var ID_49749114_m = Alloy.Collections.unidad_medida;
                    var db_ID_49749114 = Ti.Database.open(ID_49749114_m.config.adapter.db_name);
                    db_ID_49749114.execute('BEGIN');
                    _.each(elemento_unidad_medida, function(ID_49749114_fila, pos) {
                        db_ID_49749114.execute('INSERT INTO unidad_medida (id_accion, nombre, fecha, id_server, pais_texto, id_segured, pais, abrev) VALUES (?,?,?,?,?,?,?,?)', ID_49749114_fila.id_accion, ID_49749114_fila.valor, ID_49749114_fila.fecha, ID_49749114_fila.id, ID_49749114_fila.pais_texto, ID_49749114_fila.id_segured, ID_49749114_fila.pais, ID_49749114_fila.valor_s);
                    });
                    db_ID_49749114.execute('COMMIT');
                    db_ID_49749114.close();
                    db_ID_49749114 = null;
                    ID_49749114_m.trigger('change');
                    $.ID_908004102.setText('status: insertando unidades de medida');

                    /** 
                     * Volvemos a mostrar los botones presentes en el login 
                     */
                    var ID_283126324 = null;
                    if ('mostrar_botones' in require('funciones')) {
                        ID_283126324 = require('funciones').mostrar_botones({});
                    } else {
                        try {
                            ID_283126324 = f_mostrar_botones({});
                        } catch (ee) {}
                    }
                    /** 
                     * Detenemos la animacion de carga en el boton de login 
                     */

                    $.ID_337746182.detener_progreso({});
                    /** 
                     * Enviamos a la pantalla de menu principal 
                     */
                    Alloy.createController("menu_index", {}).getView().open();
                } else {
                    var ID_747900914 = null;
                    if ('mostrar_botones' in require('funciones')) {
                        ID_747900914 = require('funciones').mostrar_botones({});
                    } else {
                        try {
                            ID_747900914 = f_mostrar_botones({});
                        } catch (ee) {}
                    }
                    var ID_692941064_opts = [L('x1518866076_traducir', 'Aceptar')];
                    var ID_692941064 = Ti.UI.createAlertDialog({
                        title: L('x57652245_traducir', 'Alerta'),
                        message: '' + String.format(L('x921749426_traducir', 'Error %1$s'), elemento.mensaje.toString()) + '',
                        buttonNames: ID_692941064_opts
                    });
                    ID_692941064.addEventListener('click', function(e) {
                        var res = ID_692941064_opts[e.index];
                        res = null;

                        e.source.removeEventListener("click", arguments.callee);
                    });
                    ID_692941064.show();
                    $.ID_908004102.setText('status: esperando');

                }
                elemento = null, valor = null;
            };

            ID_925164431.error = function(e) {
                var elemento = e,
                    valor = e;
                var ID_1573500326 = null;
                if ('mostrar_botones' in require('funciones')) {
                    ID_1573500326 = require('funciones').mostrar_botones({});
                } else {
                    try {
                        ID_1573500326 = f_mostrar_botones({});
                    } catch (ee) {}
                }
                var ID_622160546_opts = [L('x1518866076_traducir', 'Aceptar')];
                var ID_622160546 = Ti.UI.createAlertDialog({
                    title: L('x57652245_traducir', 'Alerta'),
                    message: L('x954934616_traducir', 'Problema de conexion por favor intentar despues'),
                    buttonNames: ID_622160546_opts
                });
                ID_622160546.addEventListener('click', function(e) {
                    var suu = ID_622160546_opts[e.index];
                    suu = null;

                    e.source.removeEventListener("click", arguments.callee);
                });
                ID_622160546.show();
                if (false) console.log('error selectores', {
                    "asd": elemento
                });
                $.ID_908004102.setText('status: esperando');

                elemento = null, valor = null;
            };
            require('helper').ajaxUnico('ID_925164431', '' + String.format(L('x3323641845', '%1$sselectores'), url_server.toString()) + '', 'POST', {
                pais: pais[0].nombre,
                destino: selectores_fechas.destino.fecha,
                compania: selectores_fechas.compania.fecha,
                entidad_financiera: selectores_fechas.entidad_financiera.fecha,
                tipo_siniestro: selectores_fechas.tipo_siniestro.fecha,
                estructura_soportante: selectores_fechas.estructura_soportante.fecha,
                muros_tabiques: selectores_fechas.muros_tabiques.fecha,
                entrepisos: selectores_fechas.entrepisos.fecha,
                pavimento: selectores_fechas.pavimento.fecha,
                estructura_cubierta: selectores_fechas.estructura_cubierta.fecha,
                cubierta: selectores_fechas.cubierta.fecha,
                partida: selectores_fechas.partida.fecha,
                bienes: selectores_fechas.bienes.fecha,
                monedas: selectores_fechas.monedas.fecha,
                marcas: selectores_fechas.marcas.fecha
            }, 15000, ID_925164431);
        } else {
            var ID_43157519_i = Alloy.createCollection('pais');
            var ID_43157519_i_where = '';
            ID_43157519_i.fetch();
            var paises = require('helper').query2array(ID_43157519_i);
            if (false) console.log('hubo un error, no se encontro pais, dump dice', {
                "paises": paises
            });
        }
    }
    return null;
};
var f_mostrar_botones = function(x_params) {
    /** 
     * Mostramos el boton registrar 
     */
    var ID_1632570648_visible = true;

    if (ID_1632570648_visible == 'si') {
        ID_1632570648_visible = true;
    } else if (ID_1632570648_visible == 'no') {
        ID_1632570648_visible = false;
    }
    $.ID_1632570648.setVisible(ID_1632570648_visible);

    /** 
     * Detenemos animacion progreso de registro 
     */
    $.ID_418095586.hide();
    /** 
     * Mostramos boton entrar 
     */

    $.ID_337746182.detener_progreso({});
    return null;
};
var f_ocultar_botones = function(x_params) {
    /** 
     * Ocultamos boton registrar 
     */
    var ID_1632570648_visible = false;

    if (ID_1632570648_visible == 'si') {
        ID_1632570648_visible = true;
    } else if (ID_1632570648_visible == 'no') {
        ID_1632570648_visible = false;
    }
    $.ID_1632570648.setVisible(ID_1632570648_visible);

    /** 
     * Iniciamos animacion progreso de registro 
     */
    $.ID_418095586.show();
    /** 
     * Ocultamos boton entrar 
     */

    $.ID_337746182.iniciar_progreso({});
    return null;
};

if (OS_IOS || OS_ANDROID) {
    $.ID_25124347.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}
$.ID_25124347.open();