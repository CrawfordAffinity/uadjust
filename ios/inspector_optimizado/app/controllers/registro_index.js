var _bind4section = {};
var _list_templates = {};

var _activity;
if (OS_ANDROID) {
    _activity = $.ID_1119875032.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_1119875032';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_1119875032_window.addEventListener('open', function(e) {
        abx.setStatusbarColor("#000000");
        abx.setBackgroundColor("white");
    });
}

function Click_ID_1240338329(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    $.ID_1119875032.close();

}

$.ID_673432332.init({
    titulo: L('x587441556_traducir', 'PARTE 1: País de residencia'),
    __id: 'ALL673432332',
    avance: L('x122616641_traducir', '1/6')
});


var ID_833187137_like = function(search) {
    if (typeof search !== 'string' || this === null) {
        return false;
    }
    search = search.replace(new RegExp("([\\.\\\\\\+\\*\\?\\[\\^\\]\\$\\(\\)\\{\\}\\=\\!\\<\\>\\|\\:\\-])", "g"), "\\$1");
    search = search.replace(/%/g, '.*').replace(/_/g, '.');
    return RegExp('^' + search + '$', 'gi').test(this);
};
var ID_833187137_filter = function(coll) {
    var filtered = _.toArray(coll.filter(function(m) {
        return true;
    }));
    return filtered;
};
var ID_833187137_transform = function(model) {
    var fila = model.toJSON();
    return fila;
};
var ID_833187137_update = function(e) {};
_.defer(function() {
    Alloy.Collections.pais.fetch();
});
Alloy.Collections.pais.on('add change delete', function(ee) {
    ID_833187137_update(ee);
});
Alloy.Collections.pais.fetch();



function Change_ID_1493410550(e) {

    e.cancelBubble = true;
    var elemento = e;
    var _columna = e.columnIndex;
    var columna = e.columnIndex + 1;
    var _fila = e.rowIndex;
    var fila = e.rowIndex + 1;
    var modelo = require('helper').query2array(Alloy.Collections.pais)[e.rowIndex];
    _.defer(function(modelo) {
        require('vars')['pais_seleccionado'] = modelo;
    }, modelo);

}

$.ID_2143248160.init({
    titulo: L('x1524107289_traducir', 'CONTINUAR'),
    __id: 'ALL2143248160',
    onclick: Click_ID_381969225
});

function Click_ID_381969225(e) {

    var evento = e;
    var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
    var pais_seleccionado = ('pais_seleccionado' in require('vars')) ? require('vars')['pais_seleccionado'] : '';
    if (pais_seleccionado == -1) {
        var ID_1103996902_i = Alloy.createCollection('pais');
        var ID_1103996902_i_where = '';
        ID_1103996902_i.fetch();
        var predet = require('helper').query2array(ID_1103996902_i);
        require('vars')['pais_seleccionado'] = predet[0];
        var pais_seleccionado = ('pais_seleccionado' in require('vars')) ? require('vars')['pais_seleccionado'] : '';
    }
    if (false) console.log('cambiando idioma a', {
        "lenguaje": pais_seleccionado.lenguaje
    });
    /** 
     * Cambiamos el idioma de la app segun sea el pais seleccionado por el inspector 
     */
    if (OS_ANDROID) {
        Ti.Locale.setLanguage(pais_seleccionado.lenguaje.split('_').join('-'));
        var intent = Ti.Android.createIntent({
            flags: Ti.Android.FLAG_ACTIVITY_CLEAR_TOP | Ti.Android.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED | Ti.Android.FLAG_ACTIVITY_NEW_TASK,
            action: Ti.Android.ACTION_MAIN,
            url: 'index.js'
        });
    } else {
        Ti.Locale.setLanguage(pais_seleccionado.lenguaje.split('_').join('-'));
    }
    /** 
     * Agregamos campo pais al objeto registro 
     */
    var registro = _.extend(registro, {
        pais: pais_seleccionado.id_server
    });
    /** 
     * Y lo guardamos en una variable global 
     */
    require('vars')['registro'] = registro;
    if ("ID_1119875032" in Alloy.Globals) {
        Alloy.Globals["ID_1119875032"].openWindow(Alloy.createController("datos_personales", {}).getView());
    } else {
        Alloy.Globals["ID_1119875032"] = $.ID_1119875032;
        Alloy.Globals["ID_1119875032"].openWindow(Alloy.createController("datos_personales", {}).getView());
    }

}

(function() {
    /** 
     * Inicializamos de registro 
     */
    var registro = {
        pais: 1
    };
    /** 
     * Actualizamos la variable de registro 
     */
    require('vars')['registro'] = registro;
    /** 
     * Predefinimos un pais nulo por defecto 
     */
    require('vars')['pais_seleccionado'] = -1;
    /** 
     * Creamos evento que al cerrar desde la ultima pantalla, esta tambi&#233;n se cierre. Evitamos el uso excesivo de memoria ram 
     */
    _my_events['_close_enrolamiento,ID_1674487459'] = function(evento) {
        if (false) console.log('escuchando cerrar enrolamiento paisresidencia', {});
        $.ID_1119875032.close();
    };
    Alloy.Events.on('_close_enrolamiento', _my_events['_close_enrolamiento,ID_1674487459']);
})();

if (OS_IOS || OS_ANDROID) {
    $.ID_1119875032.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}
//$.ID_1119875032.open();