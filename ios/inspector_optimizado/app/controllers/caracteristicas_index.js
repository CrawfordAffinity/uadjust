var _bind4section = {
    "ref1": "insp_niveles"
};
var _list_templates = {
    "pborrar": {
        "ID_1883915455": {
            "text": "{id}"
        },
        "ID_2120109018": {
            "text": "{nombre}"
        },
        "ID_1046919308": {},
        "ID_1735369570": {},
        "ID_1777579448": {},
        "ID_1254685949": {},
        "ID_1795050796": {},
        "ID_1752901998": {},
        "ID_1914367022": {
            "text": "{nombre}"
        },
        "ID_1249074865": {
            "text": "{id}"
        },
        "ID_1131557960": {},
        "ID_1361874287": {},
        "ID_1421561173": {},
        "ID_1142337614": {},
        "ID_1057712478": {
            "text": "{nombre}"
        },
        "ID_1942170367": {
            "text": "{id}"
        },
        "ID_1502687579": {},
        "ID_1852819254": {}
    },
    "contenido": {
        "ID_630800887": {},
        "ID_862352109": {
            "text": "{id}"
        },
        "ID_560452274": {
            "text": "{nombre}"
        }
    },
    "elemento": {
        "ID_432412973": {},
        "ID_291370846": {
            "text": "{nombre}"
        },
        "ID_1925487537": {
            "text": "{id_server}"
        },
        "ID_975786586": {
            "text": "{id_segured}"
        },
        "ID_1546190443": {}
    },
    "recinto": {
        "ID_1960582596": {},
        "ID_871581291": {
            "text": "{nombre}"
        },
        "ID_310297848": {
            "text": "{id}"
        }
    },
    "nivel": {
        "ID_1021786928": {
            "text": "{id}"
        },
        "ID_1241655463": {
            "text": "{nombre}"
        },
        "ID_599581692": {}
    }
};
var $datos = $.datos.toJSON();

$.ID_1944128176_window.setTitleAttributes({
    color: 'WHITE'
});
var _activity;
if (OS_ANDROID) {
    _activity = $.ID_1944128176.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_1944128176';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_1944128176_window.addEventListener('open', function(e) {
        abx.setStatusbarColor("#FFFFFF");
        abx.setBackgroundColor("#8ce5bd");
    });
}

/** 
 * Consulta los datos ingresados en la pantalla de niveles y permite desplegarlo en un listado de esta pantalla 
 */
var ID_1214523675_like = function(search) {
    if (typeof search !== 'string' || this === null) {
        return false;
    }
    search = search.replace(new RegExp("([\\.\\\\\\+\\*\\?\\[\\^\\]\\$\\(\\)\\{\\}\\=\\!\\<\\>\\|\\:\\-])", "g"), "\\$1");
    search = search.replace(/%/g, '.*').replace(/_/g, '.');
    return RegExp('^' + search + '$', 'gi').test(this);
};
var ID_1214523675_filter = function(coll) {
    var filtered = _.toArray(coll.filter(function(m) {
        return true;
    }));
    return filtered;
};
var ID_1214523675_update = function(e) {};
_.defer(function() {
    Alloy.Collections.insp_niveles.fetch();
});
Alloy.Collections.insp_niveles.on('add change delete', function(ee) {
    ID_1214523675_update(ee);
});
var ID_1214523675_update = function(evento) {
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    var ID_1564229758_i = Alloy.createCollection('insp_niveles');
    var ID_1564229758_i_where = 'id_inspeccion=\'' + seltarea.id_server + '\'';
    ID_1564229758_i.fetch({
        query: 'SELECT * FROM insp_niveles WHERE id_inspeccion=\'' + seltarea.id_server + '\''
    });
    var insp_n = require('helper').query2array(ID_1564229758_i);
    var alto = 50 * insp_n.length;
    var ID_1774266909_alto = alto;

    if (ID_1774266909_alto == '*') {
        ID_1774266909_alto = Ti.UI.FILL;
    } else if (!isNaN(ID_1774266909_alto)) {
        ID_1774266909_alto = ID_1774266909_alto + 'dp';
    }
    $.ID_1774266909.setHeight(ID_1774266909_alto);

};
ID_1214523675_filter = function(coll) {
    var filtered = _.toArray(coll.filter(function(filax) {
        var fila = filax.toJSON();
        var test = true;
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            if (seltarea.id_server != fila.id_inspeccion) {
                test = false;
            }
        }
        fila = null;
        return test;
    }));
    return filtered;
};
var ID_1214523675_transform = function(model) {
    var fila = model.toJSON();
    return fila;
};
Alloy.Collections.insp_niveles.fetch();

function Click_ID_2119040220(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    /** 
     * Muestra pantalla preguntas 
     */
    ID_1795613283.show();

}

$.ID_2081717098.init({
    titulo: L('x404129330_traducir', '¿EL ASEGURADO CONFIRMA ESTOS DATOS?'),
    __id: 'ALL2081717098',
    si: L('x1723413441_traducir', 'SI, Están correctos'),
    texto: L('x2083765231_traducir', 'El asegurado debe confirmar que los datos de esta sección están correctos'),
    pantalla: L('x2851883104_traducir', '¿ESTA DE ACUERDO?'),
    onno: no_ID_1493775158,
    onsi: si_ID_1832277789,
    no: L('x55492959_traducir', 'NO, Hay que modificar algo'),
    header: 'verde',
    onclick: Click_ID_1078224024
});

function Click_ID_1078224024(e) {

    var evento = e;
    var moment = require('alloy/moment');
    var anoactual = moment(new Date()).format('YYYY');
    /** 
     * Flags para verificar que todos los datos ingresados esten correctos 
     */
    require('vars')[_var_scopekey]['otro_valor'] = L('x734881840_traducir', 'false');
    require('vars')[_var_scopekey]['sin_niveles'] = L('x734881840_traducir', 'false');
    require('vars')[_var_scopekey]['todobien'] = L('x4261170317', 'true');
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
        /** 
         * Comprobamos si no hay niveles definidos, para mostrar posteriormente dialogo luego de finalizar resto de validaciones. 
         */
        var ID_1398605413_i = Alloy.createCollection('insp_niveles');
        var ID_1398605413_i_where = 'id_inspeccion=\'' + seltarea.id_server + '\'';
        ID_1398605413_i.fetch({
            query: 'SELECT * FROM insp_niveles WHERE id_inspeccion=\'' + seltarea.id_server + '\''
        });
        var niveles = require('helper').query2array(ID_1398605413_i);
        if (niveles && niveles.length == 0) {
            require('vars')[_var_scopekey]['sin_niveles'] = L('x4261170317', 'true');
        }
    } else {
        var ID_1874669027_i = Alloy.createCollection('insp_niveles');
        var ID_1874669027_i_where = '';
        ID_1874669027_i.fetch();
        var niveles = require('helper').query2array(ID_1874669027_i);
        if (niveles && niveles.length == 0) {
            require('vars')[_var_scopekey]['sin_niveles'] = L('x4261170317', 'true');
        }
    }
    if(false) console.log('info datos', {
        "daot": $.datos.toJSON()
    });
    if (_.isUndefined($datos.destinos)) {
        require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
        var ID_2002693953_opts = [L('x1518866076_traducir', 'Aceptar')];
        var ID_2002693953 = Ti.UI.createAlertDialog({
            title: L('x3237162386_traducir', 'Atencion'),
            message: L('x3814056646_traducir', 'Seleccione destino de la vivienda'),
            buttonNames: ID_2002693953_opts
        });
        ID_2002693953.addEventListener('click', function(e) {
            var nulo = ID_2002693953_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_2002693953.show();
    } else if ((_.isObject($datos.destinos) || _.isString($datos.destinos)) && _.isEmpty($datos.destinos)) {
        require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
        var ID_503144617_opts = [L('x1518866076_traducir', 'Aceptar')];
        var ID_503144617 = Ti.UI.createAlertDialog({
            title: L('x3237162386_traducir', 'Atencion'),
            message: L('x3814056646_traducir', 'Seleccione destino de la vivienda'),
            buttonNames: ID_503144617_opts
        });
        ID_503144617.addEventListener('click', function(e) {
            var nulo = ID_503144617_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_503144617.show();
    }
    if (_.isUndefined($datos.construccion_ano)) {
        require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
        var ID_242352338_opts = [L('x1518866076_traducir', 'Aceptar')];
        var ID_242352338 = Ti.UI.createAlertDialog({
            title: L('x3237162386_traducir', 'Atencion'),
            message: L('x2674242197_traducir', 'Ingrese año de construcción de la vivienda'),
            buttonNames: ID_242352338_opts
        });
        ID_242352338.addEventListener('click', function(e) {
            var nulo = ID_242352338_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_242352338.show();
    } else if ((_.isObject($datos.construccion_ano) || _.isString($datos.construccion_ano)) && _.isEmpty($datos.construccion_ano)) {
        require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
        var ID_522693828_opts = [L('x1518866076_traducir', 'Aceptar')];
        var ID_522693828 = Ti.UI.createAlertDialog({
            title: L('x3237162386_traducir', 'Atencion'),
            message: L('x2674242197_traducir', 'Ingrese año de construcción de la vivienda'),
            buttonNames: ID_522693828_opts
        });
        ID_522693828.addEventListener('click', function(e) {
            var nulo = ID_522693828_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_522693828.show();
    } else if ($datos.construccion_ano > anoactual == true || $datos.construccion_ano > anoactual == 'true') {
        require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
        var ID_194242927_opts = [L('x1518866076_traducir', 'Aceptar')];
        var ID_194242927 = Ti.UI.createAlertDialog({
            title: L('x3237162386_traducir', 'Atencion'),
            message: L('x2118147629_traducir', 'El año tiene que ser menor al año actual'),
            buttonNames: ID_194242927_opts
        });
        ID_194242927.addEventListener('click', function(e) {
            var nulo = ID_194242927_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_194242927.show();
    } else if ($datos.construccion_ano < (anoactual - 100) == true || $datos.construccion_ano < (anoactual - 100) == 'true') {
        require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
        var ID_804350675_opts = [L('x1518866076_traducir', 'Aceptar')];
        var ID_804350675 = Ti.UI.createAlertDialog({
            title: L('x3237162386_traducir', 'Atencion'),
            message: L('x624168147_traducir', 'Tiene que tener máximo 100 años de antigüedad'),
            buttonNames: ID_804350675_opts
        });
        ID_804350675.addEventListener('click', function(e) {
            var nulo = ID_804350675_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_804350675.show();
    } else if ($datos.otros_seguros_enlugar == 1 || $datos.otros_seguros_enlugar == '1') {
        if (_.isUndefined($datos.id_compania)) {
            require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
            var ID_24469666_opts = [L('x1518866076_traducir', 'Aceptar')];
            var ID_24469666 = Ti.UI.createAlertDialog({
                title: L('x3237162386_traducir', 'Atencion'),
                message: L('x1200064248_traducir', 'Seleccione compañía de seguros'),
                buttonNames: ID_24469666_opts
            });
            ID_24469666.addEventListener('click', function(e) {
                var nulo = ID_24469666_opts[e.index];
                nulo = null;
                e.source.removeEventListener("click", arguments.callee);
            });
            ID_24469666.show();
        } else if ((_.isObject($datos.id_compania) || _.isString($datos.id_compania)) && _.isEmpty($datos.id_compania)) {
            require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
            var ID_1387306774_opts = [L('x1518866076_traducir', 'Aceptar')];
            var ID_1387306774 = Ti.UI.createAlertDialog({
                title: L('x3237162386_traducir', 'Atencion'),
                message: L('x1200064248_traducir', 'Seleccione compañía de seguros'),
                buttonNames: ID_1387306774_opts
            });
            ID_1387306774.addEventListener('click', function(e) {
                var nulo = ID_1387306774_opts[e.index];
                nulo = null;
                e.source.removeEventListener("click", arguments.callee);
            });
            ID_1387306774.show();
        } else {
            require('vars')[_var_scopekey]['otro_valor'] = L('x4261170317', 'true');
        }
    }
    if ($datos.asociado_hipotecario == 1 || $datos.asociado_hipotecario == '1') {
        if (_.isUndefined($datos.id_entidad_financiera)) {
            require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
            var ID_725720385_opts = [L('x1518866076_traducir', 'Aceptar')];
            var ID_725720385 = Ti.UI.createAlertDialog({
                title: L('x3237162386_traducir', 'Atencion'),
                message: L('x4118601873_traducir', 'Seleccione entidad financiera'),
                buttonNames: ID_725720385_opts
            });
            ID_725720385.addEventListener('click', function(e) {
                var nulo = ID_725720385_opts[e.index];
                nulo = null;
                e.source.removeEventListener("click", arguments.callee);
            });
            ID_725720385.show();
        } else if ((_.isObject($datos.id_entidad_financiera) || _.isString($datos.id_entidad_financiera)) && _.isEmpty($datos.id_entidad_financiera)) {
            require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
            var ID_1365952444_opts = [L('x1518866076_traducir', 'Aceptar')];
            var ID_1365952444 = Ti.UI.createAlertDialog({
                title: L('x3237162386_traducir', 'Atencion'),
                message: L('x4118601873_traducir', 'Seleccione entidad financiera'),
                buttonNames: ID_1365952444_opts
            });
            ID_1365952444.addEventListener('click', function(e) {
                var nulo = ID_1365952444_opts[e.index];
                nulo = null;
                e.source.removeEventListener("click", arguments.callee);
            });
            ID_1365952444.show();
        } else {
            require('vars')[_var_scopekey]['otro_valor'] = L('x4261170317', 'true');
        }
    }
    if ($datos.inhabitable == 1 || $datos.inhabitable == '1') {
        if (_.isUndefined($datos.estimacion_meses)) {
            require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
            var ID_1083180607_opts = [L('x1518866076_traducir', 'Aceptar')];
            var ID_1083180607 = Ti.UI.createAlertDialog({
                title: L('x3237162386_traducir', 'Atencion'),
                message: L('x2127433848_traducir', 'Ingrese estimación de meses'),
                buttonNames: ID_1083180607_opts
            });
            ID_1083180607.addEventListener('click', function(e) {
                var nulo = ID_1083180607_opts[e.index];
                nulo = null;
                e.source.removeEventListener("click", arguments.callee);
            });
            ID_1083180607.show();
        } else if ((_.isObject($datos.estimacion_meses) || _.isString($datos.estimacion_meses)) && _.isEmpty($datos.estimacion_meses)) {
            require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
            var ID_271339535_opts = [L('x1518866076_traducir', 'Aceptar')];
            var ID_271339535 = Ti.UI.createAlertDialog({
                title: L('x3237162386_traducir', 'Atencion'),
                message: L('x2127433848_traducir', 'Ingrese estimación de meses'),
                buttonNames: ID_271339535_opts
            });
            ID_271339535.addEventListener('click', function(e) {
                var nulo = ID_271339535_opts[e.index];
                nulo = null;
                e.source.removeEventListener("click", arguments.callee);
            });
            ID_271339535.show();
        } else {
            require('vars')[_var_scopekey]['otro_valor'] = L('x4261170317', 'true');
        }
    }
    var todobien = ('todobien' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['todobien'] : '';
    if (todobien == true || todobien == 'true') {
        var otro_valor = ('otro_valor' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['otro_valor'] : '';
        if (otro_valor == true || otro_valor == 'true') {
            var sin_niveles = ('sin_niveles' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['sin_niveles'] : '';
            if (sin_niveles == true || sin_niveles == 'true') {
                var ID_1570013819_opts = [L('x1518866076_traducir', 'Aceptar')];
                var ID_1570013819 = Ti.UI.createAlertDialog({
                    title: L('x3237162386_traducir', 'Atencion'),
                    message: L('x3117604322_traducir', 'Ingrese al menos un nivel de la vivienda'),
                    buttonNames: ID_1570013819_opts
                });
                ID_1570013819.addEventListener('click', function(e) {
                    var nulo = ID_1570013819_opts[e.index];
                    nulo = null;
                    e.source.removeEventListener("click", arguments.callee);
                });
                ID_1570013819.show();
            } else {
                /** 
                 * Abrimos pantalla esta de acuerdo 
                 */
                $.ID_2081717098.enviar({});
            }
        } else {
            var sin_niveles = ('sin_niveles' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['sin_niveles'] : '';
            if (sin_niveles == true || sin_niveles == 'true') {
                var ID_837114221_opts = [L('x1518866076_traducir', 'Aceptar')];
                var ID_837114221 = Ti.UI.createAlertDialog({
                    title: L('x3237162386_traducir', 'Atencion'),
                    message: L('x3117604322_traducir', 'Ingrese al menos un nivel de la vivienda'),
                    buttonNames: ID_837114221_opts
                });
                ID_837114221.addEventListener('click', function(e) {
                    var nulo = ID_837114221_opts[e.index];
                    nulo = null;
                    e.source.removeEventListener("click", arguments.callee);
                });
                ID_837114221.show();
            } else {
                $.ID_2081717098.enviar({});
            }
        }
    }

}

function si_ID_1832277789(e) {

    var evento = e;
    /** 
     * Guardamos lo ingresado en el modelo 
     */
    Alloy.Collections[$.datos.config.adapter.collection_name].add($.datos);
    $.datos.save();
    Alloy.Collections[$.datos.config.adapter.collection_name].fetch();
    anoactual = null;
    otro_valor = null;
    sin_niveles = null;
    todo_bien = null;
    seltarea = null;
    niveles = null
        /** 
         * Limpiamos widget para liberar memoria ram 
         */
    $.ID_1151684903.limpiar({});
    $.ID_1904383038.limpiar({});
    $.ID_511044183.limpiar({});
    /** 
     * Enviamos a pantalla siniestro 
     */
    Alloy.createController("siniestro_index", {}).getView().open();

}

function no_ID_1493775158(e) {

    var evento = e;

}

$.ID_1151684903.init({
    titulo: L('x1313568614_traducir', 'Destino'),
    __id: 'ALL1151684903',
    oncerrar: Cerrar_ID_1061002829,
    hint: L('x2017856269_traducir', 'Seleccione destino'),
    color: 'verde',
    subtitulo: L('x1857779775_traducir', 'Indique los destinos'),
    top: 20,
    onafterinit: Afterinit_ID_1514337892
});

function Afterinit_ID_1514337892(e) {

    var evento = e;
    var ID_1600749054_func = function() {
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {} else {
            var ID_1376679078_i = Alloy.Collections.destino;
            var sql = "DELETE FROM " + ID_1376679078_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_1376679078_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_1376679078_i.trigger('delete');
            var item_index = 0;
            _.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
                item_index += 1;
                var ID_1565849607_m = Alloy.Collections.destino;
                var ID_1565849607_fila = Alloy.createModel('destino', {
                    nombre: String.format(L('x1956100003_traducir', 'Casa%1$s'), item.toString()),
                    id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
                    id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item.toString()),
                    pais: 1
                });
                ID_1565849607_m.add(ID_1565849607_fila);
                ID_1565849607_fila.save();
                _.defer(function() {});
            });
        }
        /** 
         * Obtenemos datos para selectores 
         */
        var ID_1603996103_i = Alloy.createCollection('destino');
        ID_1603996103_i.fetch();
        var ID_1603996103_src = require('helper').query2array(ID_1603996103_i);
        var datos = [];
        _.each(ID_1603996103_src, function(fila, pos) {
            var new_row = {};
            _.each(fila, function(x, llave) {
                var newkey = '';
                if (llave == 'nombre') newkey = 'label';
                if (llave == 'id_segured') newkey = 'valor';
                if (newkey != '') new_row[newkey] = fila[llave];
            });
            new_row['estado'] = 0;
            datos.push(new_row);
        });
        /** 
         * Enviamos los datos al widget 
         */
        $.ID_1151684903.update({
            data: datos
        });
        datos = null;
    };
    var ID_1600749054 = setTimeout(ID_1600749054_func, 1000 * 0.2);

}

function Cerrar_ID_1061002829(e) {

    var evento = e;
    $.ID_1151684903.update({});
    $.datos.set({
        destinos: evento.valores
    });
    if ('datos' in $) $datos = $.datos.toJSON();

}

function Change_ID_1755832330(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    $.datos.set({
        construccion_ano: elemento
    });
    if ('datos' in $) $datos = $.datos.toJSON();
    elemento = null, source = null;

}

function Change_ID_1228010089(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    if (elemento == true || elemento == 'true') {
        $.ID_2075141137.setText('SI');

        $.datos.set({
            construccion_anexa: 1
        });
        if ('datos' in $) $datos = $.datos.toJSON();
    } else {
        $.ID_2075141137.setText('NO');

        $.datos.set({
            construccion_anexa: 0
        });
        if ('datos' in $) $datos = $.datos.toJSON();
    }
    elemento = null;

}

function Change_ID_1855201829(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    if (elemento == true || elemento == 'true') {
        $.ID_1029806018.setText('SI');

        $.datos.set({
            otros_seguros_enlugar: 1
        });
        if ('datos' in $) $datos = $.datos.toJSON();
        $.ID_1904383038.activar({});
    } else {
        $.ID_1029806018.setText('NO');

        $.datos.set({
            otros_seguros_enlugar: 0
        });
        if ('datos' in $) $datos = $.datos.toJSON();
        $.ID_1904383038.desactivar({});
    }
    elemento = null;

}

$.ID_1904383038.init({
    titulo: L('x86212863_traducir', 'COMPAÑIA'),
    cargando: L('x1740321226_traducir', 'cargando ..'),
    __id: 'ALL1904383038',
    onrespuesta: Respuesta_ID_1453999143,
    campo: L('x3363644313_traducir', 'Compañía'),
    onabrir: Abrir_ID_2047404562,
    color: 'verde',
    top: 10,
    seleccione: L('x3859572977_traducir', 'seleccione compañía'),
    activo: false,
    onafterinit: Afterinit_ID_1781077047
});

function Afterinit_ID_1781077047(e) {

    var evento = e;
    var ID_1948046515_func = function() {
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {} else {
            var ID_1496395729_i = Alloy.Collections.compania;
            var sql = "DELETE FROM " + ID_1496395729_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_1496395729_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_1496395729_i.trigger('delete');
            var item_index = 0;
            _.each('A,A,B,C,D,E,F,F,G'.split(','), function(item, item_pos, item_list) {
                item_index += 1;
                var ID_1279485209_m = Alloy.Collections.compania;
                var ID_1279485209_fila = Alloy.createModel('compania', {
                    nombre: String.format(L('x1904871496', '%1$s Empresa'), item.toString()),
                    id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
                    id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item_pos.toString()),
                    pais: 1
                });
                ID_1279485209_m.add(ID_1279485209_fila);
                ID_1279485209_fila.save();
                _.defer(function() {});
            });
        }
        var ID_1661207591_i = Alloy.createCollection('compania');
        ID_1661207591_i.fetch();
        var ID_1661207591_src = require('helper').query2array(ID_1661207591_i);
        var datos = [];
        _.each(ID_1661207591_src, function(fila, pos) {
            var new_row = {};
            _.each(fila, function(x, llave) {
                var newkey = '';
                if (llave == 'nombre') newkey = 'valor';
                if (llave == 'id_segured') newkey = 'id_segured';
                if (newkey != '') new_row[newkey] = fila[llave];
            });
            datos.push(new_row);
        });
        $.ID_1904383038.data({
            data: datos
        });
        datos = null;
    };
    var ID_1948046515 = setTimeout(ID_1948046515_func, 1000 * 0.2);

}

function Abrir_ID_2047404562(e) {

    var evento = e;

}

function Respuesta_ID_1453999143(e) {

    var evento = e;
    $.ID_1904383038.labels({
        valor: evento.valor
    });
    $.datos.set({
        id_compania: evento.item.id_segured
    });
    if ('datos' in $) $datos = $.datos.toJSON();

}

function Change_ID_266856704(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    if (elemento == true || elemento == 'true') {
        $.ID_584016677.setText('SI');

        $.datos.set({
            asociado_hipotecario: 1
        });
        if ('datos' in $) $datos = $.datos.toJSON();
        $.ID_511044183.activar({});
    } else {
        $.ID_584016677.setText('NO');

        $.datos.set({
            asociado_hipotecario: 0
        });
        if ('datos' in $) $datos = $.datos.toJSON();
        $.ID_511044183.desactivar({});
    }
    elemento = null;

}

$.ID_511044183.init({
    titulo: L('x2059554513_traducir', 'ENTIDAD'),
    cargando: L('x1740321226_traducir', 'cargando ..'),
    __id: 'ALL511044183',
    onrespuesta: Respuesta_ID_864977697,
    campo: L('x2538351238_traducir', 'Entidad Financiera'),
    onabrir: Abrir_ID_129099414,
    color: 'verde',
    seleccione: L('x3782298892_traducir', 'seleccione entidad'),
    activo: false,
    onafterinit: Afterinit_ID_1747714774
});

function Afterinit_ID_1747714774(e) {

    var evento = e;
    var ID_779848452_func = function() {
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {} else {
            var ID_116885447_i = Alloy.Collections.entidad_financiera;
            var sql = "DELETE FROM " + ID_116885447_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_116885447_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_116885447_i.trigger('delete');
            var item_index = 0;
            _.each('A,A,B,C,D,E,F,F,G'.split(','), function(item, item_pos, item_list) {
                item_index += 1;
                var ID_1604887491_m = Alloy.Collections.entidad_financiera;
                var ID_1604887491_fila = Alloy.createModel('entidad_financiera', {
                    nombre: String.format(L('x380244590', '%1$s Entidad'), item.toString()),
                    id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
                    id_segured: String.format(L('x1929201954_traducir', '20%1$s'), item_pos.toString()),
                    pais: 1
                });
                ID_1604887491_m.add(ID_1604887491_fila);
                ID_1604887491_fila.save();
                _.defer(function() {});
            });
        }
        var ID_822123413_i = Alloy.createCollection('entidad_financiera');
        ID_822123413_i.fetch();
        var ID_822123413_src = require('helper').query2array(ID_822123413_i);
        var datos = [];
        _.each(ID_822123413_src, function(fila, pos) {
            var new_row = {};
            _.each(fila, function(x, llave) {
                var newkey = '';
                if (llave == 'nombre') newkey = 'valor';
                if (llave == 'id_segured') newkey = 'id_segured';
                if (newkey != '') new_row[newkey] = fila[llave];
            });
            datos.push(new_row);
        });
        $.ID_511044183.data({
            data: datos
        });
        datos = null;
    };
    var ID_779848452 = setTimeout(ID_779848452_func, 1000 * 0.2);

}

function Abrir_ID_129099414(e) {

    var evento = e;

}

function Respuesta_ID_864977697(e) {

    var evento = e;
    $.ID_511044183.labels({
        valor: evento.valor
    });
    $.datos.set({
        id_entidad_financiera: evento.item.id_segured
    });
    if ('datos' in $) $datos = $.datos.toJSON();

}

function Change_ID_872181670(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    if (elemento == true || elemento == 'true') {
        $.ID_1466872410.setText('SI');

        $.ID_662931237.setEditable(true);

        $.datos.set({
            inhabitable: 1
        });
        if ('datos' in $) $datos = $.datos.toJSON();
    } else {
        $.ID_1466872410.setText('NO');

        $.ID_662931237.setEditable('false');

        $.datos.set({
            inhabitable: 0
        });
        if ('datos' in $) $datos = $.datos.toJSON();
    }
    elemento = null;

}

function Change_ID_1529362900(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    $.datos.set({
        estimacion_meses: elemento
    });
    if ('datos' in $) $datos = $.datos.toJSON();
    elemento = null, source = null;

}

function Click_ID_1417853738(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    Alloy.createController("nuevo_nivel", {}).getView().open();

}

function Swipe_ID_1902064617(e) {

    e.cancelBubble = true;
    var elemento = e.section.getItemAt(e.itemIndex);
    var findVariables = require('fvariables');
    var fila = {};
    _.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
        _.each(obj_id, function(valor, prop) {
            fila[prop] = elemento[id_field][prop];
            var llaves = findVariables(valor, '{', '}');
            _.each(llaves, function(llave) {
                fila[llave] = elemento[id_field][prop];
            });
        });
    });
    var flag_borrar = ('flag_borrar' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['flag_borrar'] : '';
    if (flag_borrar == false || flag_borrar == 'false') {
        if (e.direction == L('x2053629800_traducir', 'left')) {
            var findVariables = require('fvariables');
            elemento.template = 'pborrar';
            _.each(_list_templates['pborrar'], function(obj_id, id_field) {
                _.each(obj_id, function(valor, prop) {
                    var llaves = findVariables(valor, '{', '}');
                    _.each(llaves, function(llave) {
                        elemento[id_field] = {};
                        elemento[id_field][prop] = fila[llave];
                    });
                });
            });
            if (OS_IOS) {
                e.section.updateItemAt(e.itemIndex, elemento, {
                    animated: true
                });
            } else if (OS_ANDROID) {
                e.section.updateItemAt(e.itemIndex, elemento);
            }
            require('vars')[_var_scopekey]['flag_borrar'] = L('x4261170317', 'true');
        }
    } else {
        if(false) console.log('esta en true', {});
        require('vars')[_var_scopekey]['flag_borrar'] = L('x734881840_traducir', 'false');
        _.defer(function() {
            Alloy.Collections.insp_niveles.fetch();
        });
    }
}

function Click_ID_1710059938(e) {

    e.cancelBubble = true;
    var elemento = e.section.getItemAt(e.itemIndex);
    var findVariables = require('fvariables');
    var fila = {};
    _.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
        _.each(obj_id, function(valor, prop) {
            fila[prop] = elemento[id_field][prop];
            var llaves = findVariables(valor, '{', '}');
            _.each(llaves, function(llave) {
                fila[llave] = elemento[id_field][prop];
            });
        });
    });
    var flag_borrar = ('flag_borrar' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['flag_borrar'] : '';
    if (flag_borrar == false || flag_borrar == 'false') {
        /** 
         * Ocupamos esta variable para impedir que la pantalla siguente se cargue dos veces 
         */
        var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
        if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
            require('vars')['var_abriendo'] = L('x4261170317', 'true');
            /** 
             * Enviamos el parametro _dato para indicar cual sera el id del nivel a editar 
             */
            Alloy.createController("editar_nivel", {
                '_fila': fila
            }).getView().open();
        }
    } else {
        if(false) console.log('no puedes entrar... desmarca el resto', {});
        _.defer(function() {
            Alloy.Collections.insp_niveles.fetch();
        });
        require('vars')[_var_scopekey]['flag_borrar'] = L('x734881840_traducir', 'false');
    }
}

function Click_ID_1977890478(e) {

    e.cancelBubble = true;
    var elemento = e.section.getItemAt(e.itemIndex);
    var findVariables = require('fvariables');
    var fila = {};
    _.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
        _.each(obj_id, function(valor, prop) {
            fila[prop] = elemento[id_field][prop];
            var llaves = findVariables(valor, '{', '}');
            _.each(llaves, function(llave) {
                fila[llave] = elemento[id_field][prop];
            });
        });
    });
    var ID_2057711963_opts = [L('x3827418516_traducir', 'Si'), L('x1962639792_traducir', ' No')];
    var ID_2057711963 = Ti.UI.createAlertDialog({
        title: L('x2185084353_traducir', 'Atención'),
        message: '' + String.format(L('x4127782287_traducir', '¿ Seguro desea eliminar: %1$s ?'), fila.nombre.toString()) + '',
        buttonNames: ID_2057711963_opts
    });
    ID_2057711963.addEventListener('click', function(e) {
        var xd = ID_2057711963_opts[e.index];
        if (xd == L('x3827418516_traducir', 'Si')) {
            var ID_1931990785_i = Alloy.Collections.insp_niveles;
            var sql = 'DELETE FROM ' + ID_1931990785_i.config.adapter.collection_name + ' WHERE id=\'' + fila.id + '\'';
            var db = Ti.Database.open(ID_1931990785_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            ID_1931990785_i.trigger('delete');
            _.defer(function() {
                Alloy.Collections.insp_niveles.fetch();
            });
            require('vars')[_var_scopekey]['flag_borrar'] = L('x734881840_traducir', 'false');
        } else {
            require('vars')[_var_scopekey]['flag_borrar'] = L('x734881840_traducir', 'false');
            _.defer(function() {
                Alloy.Collections.insp_niveles.fetch();
            });
        }
        xd = null;
        e.source.removeEventListener("click", arguments.callee);
    });
    ID_2057711963.show();

}

function Swipe_ID_1398167337(e) {

    e.cancelBubble = true;
    var elemento = e.section.getItemAt(e.itemIndex);
    var findVariables = require('fvariables');
    var fila = {};
    _.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
        _.each(obj_id, function(valor, prop) {
            fila[prop] = elemento[id_field][prop];
            var llaves = findVariables(valor, '{', '}');
            _.each(llaves, function(llave) {
                fila[llave] = elemento[id_field][prop];
            });
        });
    });
    if (e.direction == L('x3033167124_traducir', 'right')) {
        var findVariables = require('fvariables');
        elemento.template = 'nivel';
        _.each(_list_templates['nivel'], function(obj_id, id_field) {
            _.each(obj_id, function(valor, prop) {
                var llaves = findVariables(valor, '{', '}');
                _.each(llaves, function(llave) {
                    elemento[id_field] = {};
                    elemento[id_field][prop] = fila[llave];
                });
            });
        });
        if (OS_IOS) {
            e.section.updateItemAt(e.itemIndex, elemento, {
                animated: true
            });
        } else if (OS_ANDROID) {
            e.section.updateItemAt(e.itemIndex, elemento);
        }
        require('vars')[_var_scopekey]['flag_borrar'] = L('x734881840_traducir', 'false');
    }

}

function Click_ID_1661414996(e) {

    e.cancelBubble = true;
    var elemento = e.section.getItemAt(e.itemIndex);
    var findVariables = require('fvariables');
    var fila = {};
    _.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
        _.each(obj_id, function(valor, prop) {
            fila[prop] = elemento[id_field][prop];
            var llaves = findVariables(valor, '{', '}');
            _.each(llaves, function(llave) {
                fila[llave] = elemento[id_field][prop];
            });
        });
    });
    require('vars')[_var_scopekey]['flag_borrar'] = L('x734881840_traducir', 'false');
    _.defer(function() {
        Alloy.Collections.insp_niveles.fetch();
    });

}

function Postlayout_ID_1885676713(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    $.ID_1790546014.show();

}

function Postlayout_ID_1912604635(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var anteriores = ('anteriores' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['anteriores'] : '';
    if (anteriores == false || anteriores == 'false') {
        /** 
         * Cerramos pantalla datos basicos 
         */
        Alloy.Events.trigger('_cerrar_insp', {
            pantalla: L('x1828862638_traducir', 'basicos')
        });
        require('vars')[_var_scopekey]['anteriores'] = L('x4261170317', 'true');
    }

}

var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
var ID_1795613283_opts = [L('x3481285698_traducir', 'Asegurado no puede seguir'), L('x3077268071_traducir', ' Se me acabo la bateria'), L('x479989047_traducir', ' Tuve un accidente'), L('x2376009830_traducir', ' Cancelar')];
var ID_1795613283 = Ti.UI.createOptionDialog({
    title: L('x1670176792_traducir', 'RAZON PARA CANCELAR INSPECCION ACTUAL'),
    options: ID_1795613283_opts
});
ID_1795613283.addEventListener('click', function(e) {
    var resp = ID_1795613283_opts[e.index];
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
    if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
        var razon = "";
        if (resp == L('x3481285698_traducir', 'Asegurado no puede seguir')) {
            razon = "Asegurado no puede seguir";
        }
        if (resp == L('x1862794075_traducir', 'Se me acabo la bateria')) {
            razon = "Se me acabo la bateria";
        }
        if (resp == L('x4012805111_traducir', 'Tuve un accidente')) {
            razon = "Tuve un accidente";
        }
        if ((_.isObject(razon) || (_.isString(razon)) && !_.isEmpty(razon)) || _.isNumber(razon) || _.isBoolean(razon)) {
            require('vars')['insp_cancelada'] = L('x2941610362_traducir', 'siniestro');
            Alloy.Collections[$.datos.config.adapter.collection_name].add($.datos);
            $.datos.save();
            Alloy.Collections[$.datos.config.adapter.collection_name].fetch();
            var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
            var ID_1576645881_visible = true;

            if (ID_1576645881_visible == 'si') {
                ID_1576645881_visible = true;
            } else if (ID_1576645881_visible == 'no') {
                ID_1576645881_visible = false;
            }
            $.ID_1576645881.setVisible(ID_1576645881_visible);

            var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
            var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
            var datos = {
                id_inspector: inspector.id_server,
                codigo_identificador: inspector.codigo_identificador,
                id_server: seltarea.id_server,
                num_caso: seltarea.num_caso,
                razon: razon
            };
            require('vars')[_var_scopekey]['datos'] = datos;
            var ID_1260514301 = {};
            ID_1260514301.success = function(e) {
                var elemento = e,
                    valor = e;
                var ID_1576645881_visible = false;

                if (ID_1576645881_visible == 'si') {
                    ID_1576645881_visible = true;
                } else if (ID_1576645881_visible == 'no') {
                    ID_1576645881_visible = false;
                }
                $.ID_1576645881.setVisible(ID_1576645881_visible);

                Alloy.createController("firma_index", {}).getView().open();
                var ID_1613198065_func = function() {
                    /** 
                     * Cerramos pantalla datos basicos 
                     */
                    Alloy.Events.trigger('_cerrar_insp', {
                        pantalla: L('x3681655494_traducir', 'caracteristicas')
                    });
                };
                var ID_1613198065 = setTimeout(ID_1613198065_func, 1000 * 0.5);
                elemento = null, valor = null;
            };
            ID_1260514301.error = function(e) {
                var elemento = e,
                    valor = e;
                var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
                var datos = ('datos' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['datos'] : '';
                var ID_1502144321_m = Alloy.Collections.cola;
                var ID_1502144321_fila = Alloy.createModel('cola', {
                    data: JSON.stringify(datos),
                    id_tarea: seltarea.id_server,
                    tipo: 'cancelar'
                });
                ID_1502144321_m.add(ID_1502144321_fila);
                ID_1502144321_fila.save();
                _.defer(function() {});
                var ID_1576645881_visible = false;

                if (ID_1576645881_visible == 'si') {
                    ID_1576645881_visible = true;
                } else if (ID_1576645881_visible == 'no') {
                    ID_1576645881_visible = false;
                }
                $.ID_1576645881.setVisible(ID_1576645881_visible);

                Alloy.createController("firma_index", {}).getView().open();
                /** 
                 * Cerramos pantalla datos basicos 
                 */
                Alloy.Events.trigger('_cerrar_insp', {
                    pantalla: L('x3681655494_traducir', 'caracteristicas')
                });
                elemento = null, valor = null;
            };
            require('helper').ajaxUnico('ID_1260514301', '' + String.format(L('x3244141284', '%1$scancelarTarea'), url_server.toString()) + '', 'POST', {
                id_inspector: seltarea.id_inspector,
                codigo_identificador: inspector.codigo_identificador,
                id_tarea: seltarea.id_server,
                num_caso: seltarea.num_caso,
                mensaje: razon,
                opcion: 0,
                tipo: 1
            }, 15000, ID_1260514301);
        }
    }
    resp = null;
});
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
    $.datos.set({
        id_inspeccion: seltarea.id_server
    });
    if ('datos' in $) $datos = $.datos.toJSON();
    /** 
     * Definimos campos defaults 
     */
    $.datos.set({
        construccion_anexa: 0,
        otros_seguros_enlugar: 0,
        asociado_hipotecario: 0,
        inhabitable: 0
    });
    if ('datos' in $) $datos = $.datos.toJSON();
}
/** 
 * Cerramos esta pantalla cuando es llamada en la pantalla siguente (Siniestro) 
 */
_my_events['_cerrar_insp,ID_1613718747'] = function(evento) {
    if (!_.isUndefined(evento.pantalla)) {
        if (evento.pantalla == L('x345484177_traducir', 'caracteristicas')) {
            if(false) console.log('debug cerrando caracteristicas', {});
            var ID_1826657610_trycatch = {
                error: function(e) {
                    if(false) console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
                }
            };
            try {
                ID_1826657610_trycatch.error = function(evento) {
                    if(false) console.log('error cerrando caracteristicas', {});
                };
                $.ID_1151684903.limpiar({});
                $.ID_1904383038.limpiar({});
                $.ID_511044183.limpiar({});
                $.ID_1944128176.close();
            } catch (e) {
                ID_1826657610_trycatch.error(e);
            }
        }
    } else {
        if(false) console.log('debug cerrando (todas) caracteristicas', {});
        var ID_1309566017_trycatch = {
            error: function(e) {
                if(false) console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
            }
        };
        try {
            ID_1309566017_trycatch.error = function(evento) {
                if(false) console.log('error cerrando caracteristicas', {});
            };
            $.ID_1151684903.limpiar({});
            $.ID_1904383038.limpiar({});
            $.ID_511044183.limpiar({});
            $.ID_1944128176.close();
        } catch (e) {
            ID_1309566017_trycatch.error(e);
        }
    }
};
Alloy.Events.on('_cerrar_insp', _my_events['_cerrar_insp,ID_1613718747']);
require('vars')[_var_scopekey]['anteriores'] = L('x734881840_traducir', 'false');
require('vars')[_var_scopekey]['flag_borrar'] = L('x734881840_traducir', 'false');

if (OS_IOS || OS_ANDROID) {
    $.ID_1944128176.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}
//$.ID_1944128176.open();