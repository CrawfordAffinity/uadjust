var _bind4section={};
var _list_templates={};
var $nivel = $.nivel.toJSON();

$.ID_117825709_window.setTitleAttributes({
color : 'WHITE'
}
);
var _activity; 
if (OS_ANDROID) { _activity = $.ID_117825709.activity; var abx = require('com.alcoapps.actionbarextras'); }
var _my_events = {}, _out_vars = {}, $item = {}, args = arguments[0] || {}; if ('__args' in args && '__id' in args['__args']) args.__id=args['__args'].__id; if ('__modelo' in args && _.keys(args.__modelo).length>0 && 'item' in $) { $.item.set(args.__modelo); $item = $.item.toJSON(); }
var _var_scopekey = 'ID_117825709';
require('vars')[_var_scopekey]={};
if (OS_ANDROID) {
   $.ID_117825709_window.addEventListener('open', function(e) {
   abx.setStatusbarColor("#FFFFFF");
   abx.setBackgroundColor("#8ce5bd");
   });
}

function Click_ID_1661434606(e) {

e.cancelBubble=true;
var elemento=e.source;
/** 
* Limpiamos widget para liberar memoria ram 
*/
$.ID_409036750.limpiar({});
$.ID_1893667930.limpiar({});
$.ID_1168052393.limpiar({});
$.ID_400889681.limpiar({});
$.ID_252896651.limpiar({});
$.ID_232106170.limpiar({});
$.ID_117825709.close();

}
function Click_ID_932299554(e) {

e.cancelBubble=true;
var elemento=e.source;
var seltarea = ('seltarea' in require('vars'))?require('vars')['seltarea']:'';
var moment = require('alloy/moment');
var anoactual = moment(new Date()).format('YYYY');
if (_.isUndefined($nivel.nombre)) {
/** 
* Verificamos que los campos ingresados esten correctos 
*/
var ID_123843463_opts=[L('x1518866076_traducir','Aceptar')];
var ID_123843463 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x1734272038_traducir','Ingrese nombre del nivel'),
   buttonNames: ID_123843463_opts
});
ID_123843463.addEventListener('click', function(e) {
   var nulo=ID_123843463_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_123843463.show();
}
 else if ((_.isObject($nivel.nombre) ||_.isString($nivel.nombre)) &&  _.isEmpty($nivel.nombre)) {
var ID_923380823_opts=[L('x1518866076_traducir','Aceptar')];
var ID_923380823 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x1734272038_traducir','Ingrese nombre del nivel'),
   buttonNames: ID_923380823_opts
});
ID_923380823.addEventListener('click', function(e) {
   var nulo=ID_923380823_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_923380823.show();
} else if (_.isUndefined($nivel.piso)) {
var ID_1997216173_opts=[L('x1518866076_traducir','Aceptar')];
var ID_1997216173 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x2669747147_traducir','Ingrese Nº de piso'),
   buttonNames: ID_1997216173_opts
});
ID_1997216173.addEventListener('click', function(e) {
   var nulo=ID_1997216173_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_1997216173.show();
} else if (_.isUndefined($nivel.ano)) {
var ID_305245323_opts=[L('x1518866076_traducir','Aceptar')];
var ID_305245323 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x853726930_traducir','Ingrese año de construcción del nivel'),
   buttonNames: ID_305245323_opts
});
ID_305245323.addEventListener('click', function(e) {
   var nulo=ID_305245323_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_305245323.show();
} else if ($nivel.ano < (anoactual-100)==true||$nivel.ano < (anoactual-100)=='true') {
if (false) console.log('ano mayor',{});
require('vars')[_var_scopekey]['todobien']=L('x734881840_traducir','false');
var ID_1733670242_opts=[L('x1518866076_traducir','Aceptar')];
var ID_1733670242 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x624168147_traducir','Tiene que tener máximo 100 años de antigüedad'),
   buttonNames: ID_1733670242_opts
});
ID_1733670242.addEventListener('click', function(e) {
   var nulo=ID_1733670242_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_1733670242.show();
} else if ($nivel.ano > anoactual==true||$nivel.ano > anoactual=='true') {
if (false) console.log('ano mayor',{});
require('vars')[_var_scopekey]['todobien']=L('x734881840_traducir','false');
var ID_1137798309_opts=[L('x1518866076_traducir','Aceptar')];
var ID_1137798309 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x2118147629_traducir','El año tiene que ser menor al año actual'),
   buttonNames: ID_1137798309_opts
});
ID_1137798309.addEventListener('click', function(e) {
   var nulo=ID_1137798309_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_1137798309.show();
} else if (_.isUndefined($nivel.largo)) {
var ID_1834915317_opts=[L('x1518866076_traducir','Aceptar')];
var ID_1834915317 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x779559340_traducir','Ingrese largo del nivel'),
   buttonNames: ID_1834915317_opts
});
ID_1834915317.addEventListener('click', function(e) {
   var nulo=ID_1834915317_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_1834915317.show();
} else if (_.isUndefined($nivel.ancho)) {
var ID_1647611653_opts=[L('x1518866076_traducir','Aceptar')];
var ID_1647611653 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x2682783608_traducir','Ingrese ancho del nivel'),
   buttonNames: ID_1647611653_opts
});
ID_1647611653.addEventListener('click', function(e) {
   var nulo=ID_1647611653_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_1647611653.show();
} else if (_.isUndefined($nivel.alto)) {
var ID_1479572282_opts=[L('x1518866076_traducir','Aceptar')];
var ID_1479572282 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x4120715490_traducir','Ingrese la altura del nivel'),
   buttonNames: ID_1479572282_opts
});
ID_1479572282.addEventListener('click', function(e) {
   var nulo=ID_1479572282_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_1479572282.show();
} else if (_.isUndefined($nivel.ids_estructuras_soportantes)) {
var ID_330615560_opts=[L('x1518866076_traducir','Aceptar')];
var ID_330615560 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x3045996758_traducir','Seleccione la estructura soportante del nivel'),
   buttonNames: ID_330615560_opts
});
ID_330615560.addEventListener('click', function(e) {
   var nulo=ID_330615560_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_330615560.show();
} else if (_.isUndefined($nivel.ids_estructuras_soportantes)) {
var ID_1332168764_opts=[L('x1518866076_traducir','Aceptar')];
var ID_1332168764 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x3045996758_traducir','Seleccione la estructura soportante del nivel'),
   buttonNames: ID_1332168764_opts
});
ID_1332168764.addEventListener('click', function(e) {
   var nulo=ID_1332168764_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_1332168764.show();
} else if ((_.isObject($nivel.ids_estructuras_soportantes) ||_.isString($nivel.ids_estructuras_soportantes)) &&  _.isEmpty($nivel.ids_estructuras_soportantes)) {
var ID_1937287793_opts=[L('x1518866076_traducir','Aceptar')];
var ID_1937287793 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x3045996758_traducir','Seleccione la estructura soportante del nivel'),
   buttonNames: ID_1937287793_opts
});
ID_1937287793.addEventListener('click', function(e) {
   var nulo=ID_1937287793_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_1937287793.show();
} else {
var argumentos = ('argumentos' in require('vars')[_var_scopekey])?require('vars')[_var_scopekey]['argumentos']:'';
if (!_.isUndefined(argumentos._fila)) {
/** 
* Eliminamos el nivel que estamos editando de la tabla y reemplazamos por el que tenemos actualmente con datos editados 
*/
/** 
* Eliminamos el nivel que estamos editando de la tabla y reemplazamos por el que tenemos actualmente con datos editados 
*/
var ID_533766911_i=Alloy.Collections.insp_niveles;
var sql = 'DELETE FROM ' + ID_533766911_i.config.adapter.collection_name + ' WHERE id=\''+argumentos._fila.id+'\'';
var db = Ti.Database.open(ID_533766911_i.config.adapter.db_name);
db.execute(sql);
db.close();
ID_533766911_i.trigger('delete');
}
Alloy.Collections[$.nivel.config.adapter.collection_name].add($.nivel);
$.nivel.save();
Alloy.Collections[$.nivel.config.adapter.collection_name].fetch();
var ID_373252409_func = function() {
/** 
* Limpiamos widget para liberar memoria ram 
*/
$.ID_409036750.limpiar({});
$.ID_1893667930.limpiar({});
$.ID_1168052393.limpiar({});
$.ID_400889681.limpiar({});
$.ID_252896651.limpiar({});
$.ID_232106170.limpiar({});
$.ID_117825709.close();
 anoactual  = null;
};
var ID_373252409 = setTimeout(ID_373252409_func, 1000*0.1);
}
}
function Change_ID_892505517(e) {

e.cancelBubble=true;
var elemento=e.value;
var source=e.source;
$.nivel.set({
nombre : elemento
}
);
if ('nivel' in $) $nivel=$.nivel.toJSON();
elemento=null, source=null;

}
function Change_ID_1745847263(e) {

e.cancelBubble=true;
var elemento=e.value;
var source=e.source;
/** 
* Actualizamos el modelo con el piso ingresado 
*/
$.nivel.set({
piso : elemento
}
);
if ('nivel' in $) $nivel=$.nivel.toJSON();
elemento=null, source=null;

}
function Change_ID_378855596(e) {

e.cancelBubble=true;
var elemento=e.value;
var source=e.source;
$.nivel.set({
ano : elemento
}
);
if ('nivel' in $) $nivel=$.nivel.toJSON();
elemento=null, source=null;

}
function Change_ID_1183068520(e) {

e.cancelBubble=true;
var elemento=e.value;
var source=e.source;
/** 
* Obtenemos el valor escrito en el campo de ancho 
*/
var ancho;
ancho = $.ID_816034727.getValue();

if ((_.isObject(ancho) || (_.isString(ancho)) &&  !_.isEmpty(ancho)) || _.isNumber(ancho) || _.isBoolean(ancho)) {
/** 
* Calculamos la superficie 
*/
if ((_.isObject(elemento) || (_.isString(elemento)) &&  !_.isEmpty(elemento)) || _.isNumber(elemento) || _.isBoolean(elemento)) {
/** 
* nos aseguramos que ancho y valor de campo sean decimales con puntos (flotante), y no comas (espa&#241;ol) 
*/
 var nuevo=parseFloat(ancho.split(',').join('.'))*parseFloat(elemento.split(',').join('.'));
$.nivel.set({
superficie : nuevo
}
);
if ('nivel' in $) $nivel=$.nivel.toJSON();
}
}
/** 
* Actualizamos el valor del largo del recinto 
*/
$.nivel.set({
largo : elemento
}
);
if ('nivel' in $) $nivel=$.nivel.toJSON();
elemento=null, source=null;

}
function Change_ID_557467702(e) {

e.cancelBubble=true;
var elemento=e.value;
var source=e.source;
var largo;
largo = $.ID_12130731.getValue();

if ((_.isObject(largo) || (_.isString(largo)) &&  !_.isEmpty(largo)) || _.isNumber(largo) || _.isBoolean(largo)) {
if ((_.isObject(elemento) || (_.isString(elemento)) &&  !_.isEmpty(elemento)) || _.isNumber(elemento) || _.isBoolean(elemento)) {
/** 
* nos aseguramos que largo y valor de campo sean decimales con puntos (flotante), y no comas (espa&#241;ol) 
*/
 var nuevo=parseFloat(largo.split(',').join('.'))*parseFloat(elemento.split(',').join('.'));
$.nivel.set({
superficie : nuevo
}
);
if ('nivel' in $) $nivel=$.nivel.toJSON();
}
}
$.nivel.set({
ancho : elemento
}
);
if ('nivel' in $) $nivel=$.nivel.toJSON();
elemento=null, source=null;

}
function Change_ID_676455181(e) {

e.cancelBubble=true;
var elemento=e.value;
var source=e.source;
$.nivel.set({
alto : elemento
}
);
if ('nivel' in $) $nivel=$.nivel.toJSON();
elemento=null, source=null;

}

$.ID_409036750.init({
titulo : L('x1975271086_traducir','Estructura Soportante'),
cargando : L('x1740321226_traducir','cargando ..'),
__id : 'ALL409036750',
oncerrar : Cerrar_ID_1583373204,
hint : L('x2898603391_traducir','Seleccione estructura'),
color : 'verde',
subtitulo : L('x4011106049_traducir','Indique los tipos'),
top : 5,
onclick : Click_ID_92545813,
onafterinit : Afterinit_ID_675647005
}
);

function Click_ID_92545813(e) {

var evento=e;

}
function Cerrar_ID_1583373204(e) {

var evento=e;
$.ID_409036750.update({});
$.nivel.set({
ids_estructuras_soportantes : evento.valores
}
);
if ('nivel' in $) $nivel=$.nivel.toJSON();

}
function Afterinit_ID_675647005(e) {

var evento=e;
/** 
* ejecutamos desfasado para no congelar el hilo. 
*/
var ID_764832750_func = function() {
var seltarea = ('seltarea' in require('vars'))?require('vars')['seltarea']:'';
var nivel_editado = ('nivel_editado' in require('vars')[_var_scopekey])?require('vars')[_var_scopekey]['nivel_editado']:'';
if (false) console.log(String.format(L('x1614712856_traducir','contenido de nivel editado %1$s'), nivel_editado.toString()),{});
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
var pais = ('pais' in require('vars'))?require('vars')['pais']:'';
var ID_1721238000_i=Alloy.createCollection('estructura_soportante');
var ID_1721238000_i_where='pais_texto=\''+pais[0].nombre+'\'';
ID_1721238000_i.fetch({ query: 'SELECT * FROM estructura_soportante WHERE pais_texto=\''+pais[0].nombre+'\'' });
var estructura=require('helper').query2array(ID_1721238000_i);
var padre_index = 0;
_.each(estructura, function(padre, padre_pos, padre_list) {
padre_index += 1;
if (_.isNull(nivel_editado.ids_estructuras_soportantes)) {
/** 
* esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
*/
 padre._estado=0;
}
 else if (nivel_editado.ids_estructuras_soportantes.indexOf(padre.id_segured)!=-1) {
 padre._estado=1;
} else {
 padre._estado=0;
}});
var datos=[];
_.each(estructura, function(fila, pos) {
  var new_row={};
  _.each(fila, function(x, llave) {
    var newkey='';
    if (llave=='nombre') newkey='label';
    if (llave=='id_segured') newkey='valor';
    if (llave=='_estado') newkey='estado';
    if (newkey!='') new_row[newkey] = fila[llave];
  });
  datos.push(new_row);
});
$.ID_409036750.update({data : datos});
}
 else {
/** 
* cargamos datos dummy para cuando compilamos de forma individual. 
*/
var ID_980499542_i=Alloy.Collections.estructura_soportante;
var sql = "DELETE FROM " + ID_980499542_i.config.adapter.collection_name;
var db = Ti.Database.open(ID_980499542_i.config.adapter.db_name);
db.execute(sql);
db.close();
sql=null;
db=null;
ID_980499542_i.trigger('delete');
var item_index = 0;
_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
item_index += 1;
var ID_45571712_m=Alloy.Collections.estructura_soportante;
var ID_45571712_fila = Alloy.createModel('estructura_soportante', {
nombre : String.format(L('x1088195980_traducir','Estructura%1$s'), item.toString()),
id_server : String.format(L('x1290178835_traducir','0%1$s'), item.toString()),
pais_texto : 'Chile',
id_segured : String.format(L('x1125471167_traducir','10%1$s'), item.toString()),
pais : 1
}
 );
ID_45571712_m.add(ID_45571712_fila);
ID_45571712_fila.save();
});
var ID_364675137_i=Alloy.createCollection('estructura_soportante');
var ID_364675137_i_where='';
ID_364675137_i.fetch();
var estructura=require('helper').query2array(ID_364675137_i);
var padre_index = 0;
_.each(estructura, function(padre, padre_pos, padre_list) {
padre_index += 1;
if (_.isNull(nivel_editado.ids_estructuras_soportantes)) {
/** 
* esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
*/
 padre._estado=0;
}
 else if (nivel_editado.ids_estructuras_soportantes.indexOf(padre.id_segured)!=-1) {
 padre._estado=1;
} else {
 padre._estado=0;
}});
var datos=[];
_.each(estructura, function(fila, pos) {
  var new_row={};
  _.each(fila, function(x, llave) {
    var newkey='';
    if (llave=='nombre') newkey='label';
    if (llave=='id_segured') newkey='valor';
    if (llave=='_estado') newkey='estado';
    if (newkey!='') new_row[newkey] = fila[llave];
  });
  datos.push(new_row);
});
$.ID_409036750.update({data : datos});
}};
var ID_764832750 = setTimeout(ID_764832750_func, 1000*0.2);

}

$.ID_1893667930.init({
titulo : L('x1219835481_traducir','Muros / Tabiques'),
cargando : L('x1740321226_traducir','cargando ..'),
__id : 'ALL1893667930',
oncerrar : Cerrar_ID_33360826,
hint : L('x2879998099_traducir','Seleccione muros'),
color : 'verde',
subtitulo : L('x4011106049_traducir','Indique los tipos'),
top : 5,
onclick : Click_ID_232903056,
onafterinit : Afterinit_ID_1566481815
}
);

function Click_ID_232903056(e) {

var evento=e;

}
function Cerrar_ID_33360826(e) {

var evento=e;
$.ID_1893667930.update({});
$.nivel.set({
ids_muros : evento.valores
}
);
if ('nivel' in $) $nivel=$.nivel.toJSON();

}
function Afterinit_ID_1566481815(e) {

var evento=e;
/** 
* ejecutamos desfasado para no congelar el hilo. 
*/
var ID_34250152_func = function() {
var seltarea = ('seltarea' in require('vars'))?require('vars')['seltarea']:'';
var nivel_editado = ('nivel_editado' in require('vars')[_var_scopekey])?require('vars')[_var_scopekey]['nivel_editado']:'';
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
var pais = ('pais' in require('vars'))?require('vars')['pais']:'';
var ID_526747553_i=Alloy.createCollection('muros_tabiques');
var ID_526747553_i_where='pais_texto=\''+pais[0].nombre+'\'';
ID_526747553_i.fetch({ query: 'SELECT * FROM muros_tabiques WHERE pais_texto=\''+pais[0].nombre+'\'' });
var tabiques=require('helper').query2array(ID_526747553_i);
var padre_index = 0;
_.each(tabiques, function(padre, padre_pos, padre_list) {
padre_index += 1;
if (_.isNull(nivel_editado.ids_muros)) {
/** 
* esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
*/
 padre._estado=0;
}
 else if (nivel_editado.ids_muros.indexOf(padre.id_segured)!=-1) {
 padre._estado=1;
} else {
 padre._estado=0;
}});
var datos=[];
_.each(tabiques, function(fila, pos) {
  var new_row={};
  _.each(fila, function(x, llave) {
    var newkey='';
    if (llave=='nombre') newkey='label';
    if (llave=='id_segured') newkey='valor';
    if (llave=='_estado') newkey='estado';
    if (newkey!='') new_row[newkey] = fila[llave];
  });
  datos.push(new_row);
});
$.ID_1893667930.update({data : datos});
}
 else {
/** 
* cargamos datos dummy para cuando compilamos de forma individual. 
*/
var ID_859753571_i=Alloy.Collections.muros_tabiques;
var sql = "DELETE FROM " + ID_859753571_i.config.adapter.collection_name;
var db = Ti.Database.open(ID_859753571_i.config.adapter.db_name);
db.execute(sql);
db.close();
sql=null;
db=null;
ID_859753571_i.trigger('delete');
var item_index = 0;
_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
item_index += 1;
var ID_288844918_m=Alloy.Collections.muros_tabiques;
var ID_288844918_fila = Alloy.createModel('muros_tabiques', {
nombre : String.format(L('x3565664878_traducir','Muros%1$s'), item.toString()),
id_server : String.format(L('x1290178835_traducir','0%1$s'), item.toString()),
pais_texto : 'Chile',
id_segured : String.format(L('x1125471167_traducir','10%1$s'), item.toString()),
pais : 1
}
 );
ID_288844918_m.add(ID_288844918_fila);
ID_288844918_fila.save();
});
var ID_1313813375_i=Alloy.createCollection('muros_tabiques');
var ID_1313813375_i_where='';
ID_1313813375_i.fetch();
var muros=require('helper').query2array(ID_1313813375_i);
var padre_index = 0;
_.each(muros, function(padre, padre_pos, padre_list) {
padre_index += 1;
if (_.isNull(nivel_editado.ids_muros)) {
/** 
* esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
*/
 padre._estado=0;
}
 else if (nivel_editado.ids_muros.indexOf(padre.id_segured)!=-1) {
 padre._estado=1;
} else {
 padre._estado=0;
}});
var datos=[];
_.each(muros, function(fila, pos) {
  var new_row={};
  _.each(fila, function(x, llave) {
    var newkey='';
    if (llave=='nombre') newkey='label';
    if (llave=='id_segured') newkey='valor';
    if (llave=='_estado') newkey='estado';
    if (newkey!='') new_row[newkey] = fila[llave];
  });
  datos.push(new_row);
});
$.ID_1893667930.update({data : datos});
}};
var ID_34250152 = setTimeout(ID_34250152_func, 1000*0.2);

}

$.ID_1168052393.init({
titulo : L('x3327059844_traducir','Entrepisos'),
cargando : L('x1740321226_traducir','cargando ..'),
__id : 'ALL1168052393',
oncerrar : Cerrar_ID_1161250667,
hint : L('x2146928948_traducir','Seleccione entrepisos'),
color : 'verde',
subtitulo : L('x4011106049_traducir','Indique los tipos'),
top : 5,
onclick : Click_ID_1116777412,
onafterinit : Afterinit_ID_1223959243
}
);

function Click_ID_1116777412(e) {

var evento=e;

}
function Cerrar_ID_1161250667(e) {

var evento=e;
$.ID_1168052393.update({});
$.nivel.set({
ids_entrepisos : evento.valores
}
);
if ('nivel' in $) $nivel=$.nivel.toJSON();

}
function Afterinit_ID_1223959243(e) {

var evento=e;
var ID_1681776986_func = function() {
var seltarea = ('seltarea' in require('vars'))?require('vars')['seltarea']:'';
var nivel_editado = ('nivel_editado' in require('vars')[_var_scopekey])?require('vars')[_var_scopekey]['nivel_editado']:'';
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
var pais = ('pais' in require('vars'))?require('vars')['pais']:'';
var ID_624023785_i=Alloy.createCollection('entrepisos');
var ID_624023785_i_where='pais_texto=\''+pais[0].nombre+'\'';
ID_624023785_i.fetch({ query: 'SELECT * FROM entrepisos WHERE pais_texto=\''+pais[0].nombre+'\'' });
var entrepisos=require('helper').query2array(ID_624023785_i);
var padre_index = 0;
_.each(entrepisos, function(padre, padre_pos, padre_list) {
padre_index += 1;
if (_.isNull(nivel_editado.ids_entrepisos)) {
/** 
* esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
*/
 padre._estado=0;
}
 else if (nivel_editado.ids_entrepisos.indexOf(padre.id_segured)!=-1) {
 padre._estado=1;
} else {
 padre._estado=0;
}});
var datos=[];
_.each(entrepisos, function(fila, pos) {
  var new_row={};
  _.each(fila, function(x, llave) {
    var newkey='';
    if (llave=='nombre') newkey='label';
    if (llave=='id_segured') newkey='valor';
    if (llave=='_estado') newkey='estado';
    if (newkey!='') new_row[newkey] = fila[llave];
  });
  datos.push(new_row);
});
$.ID_1168052393.update({data : datos});
}
 else {
var ID_1511918291_i=Alloy.Collections.entrepisos;
var sql = "DELETE FROM " + ID_1511918291_i.config.adapter.collection_name;
var db = Ti.Database.open(ID_1511918291_i.config.adapter.db_name);
db.execute(sql);
db.close();
sql=null;
db=null;
ID_1511918291_i.trigger('delete');
var item_index = 0;
_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
item_index += 1;
var ID_1065204712_m=Alloy.Collections.entrepisos;
var ID_1065204712_fila = Alloy.createModel('entrepisos', {
nombre : String.format(L('x2266735154_traducir','Entrepiso%1$s'), item.toString()),
id_server : String.format(L('x1290178835_traducir','0%1$s'), item.toString()),
pais_texto : 'Chile',
id_segured : String.format(L('x1125471167_traducir','10%1$s'), item.toString()),
pais : 1
}
 );
ID_1065204712_m.add(ID_1065204712_fila);
ID_1065204712_fila.save();
});
var ID_263386618_i=Alloy.createCollection('entrepisos');
var ID_263386618_i_where='';
ID_263386618_i.fetch();
var entrepisos=require('helper').query2array(ID_263386618_i);
var padre_index = 0;
_.each(entrepisos, function(padre, padre_pos, padre_list) {
padre_index += 1;
if (_.isNull(nivel_editado.ids_entrepisos)) {
/** 
* esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
*/
 padre._estado=0;
}
 else if (nivel_editado.ids_entrepisos.indexOf(padre.id_segured)!=-1) {
 padre._estado=1;
} else {
 padre._estado=0;
}});
var datos=[];
_.each(entrepisos, function(fila, pos) {
  var new_row={};
  _.each(fila, function(x, llave) {
    var newkey='';
    if (llave=='nombre') newkey='label';
    if (llave=='id_segured') newkey='valor';
    if (llave=='_estado') newkey='estado';
    if (newkey!='') new_row[newkey] = fila[llave];
  });
  datos.push(new_row);
});
$.ID_1168052393.update({data : datos});
}};
var ID_1681776986 = setTimeout(ID_1681776986_func, 1000*0.2);

}

$.ID_400889681.init({
titulo : L('x591862035_traducir','Pavimentos'),
cargando : L('x1740321226_traducir','cargando ..'),
__id : 'ALL400889681',
oncerrar : Cerrar_ID_558314786,
hint : L('x2600368035_traducir','Seleccione pavimentos'),
color : 'verde',
subtitulo : L('x4011106049_traducir','Indique los tipos'),
top : 5,
onclick : Click_ID_1252176153,
onafterinit : Afterinit_ID_889514014
}
);

function Click_ID_1252176153(e) {

var evento=e;

}
function Cerrar_ID_558314786(e) {

var evento=e;
$.ID_400889681.update({});
$.nivel.set({
ids_pavimentos : evento.valores
}
);
if ('nivel' in $) $nivel=$.nivel.toJSON();

}
function Afterinit_ID_889514014(e) {

var evento=e;
var ID_1696478889_func = function() {
var seltarea = ('seltarea' in require('vars'))?require('vars')['seltarea']:'';
var nivel_editado = ('nivel_editado' in require('vars')[_var_scopekey])?require('vars')[_var_scopekey]['nivel_editado']:'';
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
var pais = ('pais' in require('vars'))?require('vars')['pais']:'';
var ID_1259044934_i=Alloy.createCollection('pavimento');
var ID_1259044934_i_where='pais_texto=\''+pais[0].nombre+'\'';
ID_1259044934_i.fetch({ query: 'SELECT * FROM pavimento WHERE pais_texto=\''+pais[0].nombre+'\'' });
var pavimentos=require('helper').query2array(ID_1259044934_i);
var padre_index = 0;
_.each(pavimentos, function(padre, padre_pos, padre_list) {
padre_index += 1;
if (_.isNull(nivel_editado.ids_pavimentos)) {
/** 
* esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
*/
 padre._estado=0;
}
 else if (nivel_editado.ids_pavimentos.indexOf(padre.id_segured)!=-1) {
 padre._estado=1;
} else {
 padre._estado=0;
}});
var datos=[];
_.each(pavimentos, function(fila, pos) {
  var new_row={};
  _.each(fila, function(x, llave) {
    var newkey='';
    if (llave=='nombre') newkey='label';
    if (llave=='id_segured') newkey='valor';
    if (llave=='_estado') newkey='estado';
    if (newkey!='') new_row[newkey] = fila[llave];
  });
  datos.push(new_row);
});
$.ID_400889681.update({data : datos});
}
 else {
var ID_23233507_i=Alloy.Collections.pavimento;
var sql = "DELETE FROM " + ID_23233507_i.config.adapter.collection_name;
var db = Ti.Database.open(ID_23233507_i.config.adapter.db_name);
db.execute(sql);
db.close();
sql=null;
db=null;
ID_23233507_i.trigger('delete');
var item_index = 0;
_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
item_index += 1;
var ID_1660891143_m=Alloy.Collections.pavimento;
var ID_1660891143_fila = Alloy.createModel('pavimento', {
nombre : String.format(L('x427067467_traducir','Pavimento%1$s'), item.toString()),
id_server : String.format(L('x1290178835_traducir','0%1$s'), item.toString()),
pais_texto : 'Chile',
id_segured : String.format(L('x1125471167_traducir','10%1$s'), item.toString()),
pais : 1
}
 );
ID_1660891143_m.add(ID_1660891143_fila);
ID_1660891143_fila.save();
});
var ID_102779869_i=Alloy.createCollection('pavimento');
var ID_102779869_i_where='';
ID_102779869_i.fetch();
var pavimentos=require('helper').query2array(ID_102779869_i);
var padre_index = 0;
_.each(pavimentos, function(padre, padre_pos, padre_list) {
padre_index += 1;
if (_.isNull(nivel_editado.ids_pavimentos)) {
/** 
* esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
*/
 padre._estado=0;
}
 else if (nivel_editado.ids_pavimentos.indexOf(padre.id_segured)!=-1) {
 padre._estado=1;
} else {
 padre._estado=0;
}});
var datos=[];
_.each(pavimentos, function(fila, pos) {
  var new_row={};
  _.each(fila, function(x, llave) {
    var newkey='';
    if (llave=='nombre') newkey='label';
    if (llave=='id_segured') newkey='valor';
    if (llave=='_estado') newkey='estado';
    if (newkey!='') new_row[newkey] = fila[llave];
  });
  datos.push(new_row);
});
$.ID_400889681.update({data : datos});
}};
var ID_1696478889 = setTimeout(ID_1696478889_func, 1000*0.2);

}

$.ID_252896651.init({
titulo : L('x1866523485_traducir','Estruct. cubierta'),
cargando : L('x1740321226_traducir','cargando ..'),
__id : 'ALL252896651',
oncerrar : Cerrar_ID_765440106,
hint : L('x2460890829_traducir','Seleccione e.cubiertas'),
color : 'verde',
subtitulo : L('x4011106049_traducir','Indique los tipos'),
top : 5,
onclick : Click_ID_340314450,
onafterinit : Afterinit_ID_149901219
}
);

function Click_ID_340314450(e) {

var evento=e;

}
function Cerrar_ID_765440106(e) {

var evento=e;
$.ID_252896651.update({});
$.nivel.set({
ids_estructura_cubiera : evento.valores
}
);
if ('nivel' in $) $nivel=$.nivel.toJSON();

}
function Afterinit_ID_149901219(e) {

var evento=e;
var ID_1080255621_func = function() {
var seltarea = ('seltarea' in require('vars'))?require('vars')['seltarea']:'';
var nivel_editado = ('nivel_editado' in require('vars')[_var_scopekey])?require('vars')[_var_scopekey]['nivel_editado']:'';
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
var pais = ('pais' in require('vars'))?require('vars')['pais']:'';
var ID_26056136_i=Alloy.createCollection('estructura_cubierta');
var ID_26056136_i_where='pais_texto=\''+pais[0].nombre+'\'';
ID_26056136_i.fetch({ query: 'SELECT * FROM estructura_cubierta WHERE pais_texto=\''+pais[0].nombre+'\'' });
var ecubiertas=require('helper').query2array(ID_26056136_i);
var padre_index = 0;
_.each(ecubiertas, function(padre, padre_pos, padre_list) {
padre_index += 1;
if (_.isNull(nivel_editado.ids_estructura_cubiera)) {
/** 
* esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
*/
 padre._estado=0;
}
 else if (nivel_editado.ids_estructura_cubiera.indexOf(padre.id_segured)!=-1) {
 padre._estado=1;
} else {
 padre._estado=0;
}});
var datos=[];
_.each(ecubiertas, function(fila, pos) {
  var new_row={};
  _.each(fila, function(x, llave) {
    var newkey='';
    if (llave=='nombre') newkey='label';
    if (llave=='id_segured') newkey='valor';
    if (llave=='_estado') newkey='estado';
    if (newkey!='') new_row[newkey] = fila[llave];
  });
  datos.push(new_row);
});
$.ID_252896651.update({data : datos});
}
 else {
var ID_966893307_i=Alloy.Collections.estructura_cubierta;
var sql = "DELETE FROM " + ID_966893307_i.config.adapter.collection_name;
var db = Ti.Database.open(ID_966893307_i.config.adapter.db_name);
db.execute(sql);
db.close();
sql=null;
db=null;
ID_966893307_i.trigger('delete');
var item_index = 0;
_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
item_index += 1;
var ID_1339188299_m=Alloy.Collections.estructura_cubierta;
var ID_1339188299_fila = Alloy.createModel('estructura_cubierta', {
nombre : String.format(L('x1686539481_traducir','Estru Cubierta %1$s'), item.toString()),
id_server : String.format(L('x1290178835_traducir','0%1$s'), item.toString()),
pais_texto : 'Chile',
id_segured : String.format(L('x1125471167_traducir','10%1$s'), item.toString()),
pais : 1
}
 );
ID_1339188299_m.add(ID_1339188299_fila);
ID_1339188299_fila.save();
});
var ID_1027463354_i=Alloy.createCollection('estructura_cubierta');
var ID_1027463354_i_where='';
ID_1027463354_i.fetch();
var ecubiertas=require('helper').query2array(ID_1027463354_i);
var padre_index = 0;
_.each(ecubiertas, function(padre, padre_pos, padre_list) {
padre_index += 1;
if (_.isNull(nivel_editado.ids_estructura_cubiera)) {
/** 
* esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
*/
 padre._estado=0;
}
 else if (nivel_editado.ids_estructura_cubiera.indexOf(padre.id_segured)!=-1) {
 padre._estado=1;
} else {
 padre._estado=0;
}});
var datos=[];
_.each(ecubiertas, function(fila, pos) {
  var new_row={};
  _.each(fila, function(x, llave) {
    var newkey='';
    if (llave=='nombre') newkey='label';
    if (llave=='id_segured') newkey='valor';
    if (llave=='_estado') newkey='estado';
    if (newkey!='') new_row[newkey] = fila[llave];
  });
  datos.push(new_row);
});
$.ID_252896651.update({data : datos});
}};
var ID_1080255621 = setTimeout(ID_1080255621_func, 1000*0.2);

}

$.ID_232106170.init({
titulo : L('x2266302645_traducir','Cubierta'),
cargando : L('x1740321226_traducir','cargando ..'),
__id : 'ALL232106170',
oncerrar : Cerrar_ID_726265531,
hint : L('x2134385782_traducir','Seleccione cubiertas'),
color : 'verde',
subtitulo : L('x4011106049_traducir','Indique los tipos'),
top : 5,
onclick : Click_ID_1025697304,
onafterinit : Afterinit_ID_1630648550
}
);

function Click_ID_1025697304(e) {

var evento=e;

}
function Cerrar_ID_726265531(e) {

var evento=e;
$.ID_232106170.update({});
$.nivel.set({
ids_cubierta : evento.valores
}
);
if ('nivel' in $) $nivel=$.nivel.toJSON();

}
function Afterinit_ID_1630648550(e) {

var evento=e;
var ID_1214955522_func = function() {
var seltarea = ('seltarea' in require('vars'))?require('vars')['seltarea']:'';
var nivel_editado = ('nivel_editado' in require('vars')[_var_scopekey])?require('vars')[_var_scopekey]['nivel_editado']:'';
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
var pais = ('pais' in require('vars'))?require('vars')['pais']:'';
var ID_1026349358_i=Alloy.createCollection('cubierta');
var ID_1026349358_i_where='pais_texto=\''+pais[0].nombre+'\'';
ID_1026349358_i.fetch({ query: 'SELECT * FROM cubierta WHERE pais_texto=\''+pais[0].nombre+'\'' });
var cubiertas=require('helper').query2array(ID_1026349358_i);
var padre_index = 0;
_.each(cubiertas, function(padre, padre_pos, padre_list) {
padre_index += 1;
if (_.isNull(nivel_editado.ids_cubierta)) {
/** 
* esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
*/
 padre._estado=0;
}
 else if (nivel_editado.ids_cubierta.indexOf(padre.id_segured)!=-1) {
 padre._estado=1;
} else {
 padre._estado=0;
}});
var datos=[];
_.each(cubiertas, function(fila, pos) {
  var new_row={};
  _.each(fila, function(x, llave) {
    var newkey='';
    if (llave=='nombre') newkey='label';
    if (llave=='id_segured') newkey='valor';
    if (llave=='_estado') newkey='estado';
    if (newkey!='') new_row[newkey] = fila[llave];
  });
  datos.push(new_row);
});
$.ID_232106170.update({data : datos});
}
 else {
var ID_1907440301_i=Alloy.Collections.cubierta;
var sql = "DELETE FROM " + ID_1907440301_i.config.adapter.collection_name;
var db = Ti.Database.open(ID_1907440301_i.config.adapter.db_name);
db.execute(sql);
db.close();
sql=null;
db=null;
ID_1907440301_i.trigger('delete');
var item_index = 0;
_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
item_index += 1;
var ID_815704640_m=Alloy.Collections.cubierta;
var ID_815704640_fila = Alloy.createModel('cubierta', {
nombre : String.format(L('x2246230604_traducir','Cubierta %1$s'), item.toString()),
id_server : String.format(L('x1290178835_traducir','0%1$s'), item.toString()),
pais_texto : 'Chile',
id_segured : String.format(L('x1125471167_traducir','10%1$s'), item.toString()),
pais : 1
}
 );
ID_815704640_m.add(ID_815704640_fila);
ID_815704640_fila.save();
});
var ID_858615674_i=Alloy.createCollection('cubierta');
var ID_858615674_i_where='';
ID_858615674_i.fetch();
var cubiertas=require('helper').query2array(ID_858615674_i);
var padre_index = 0;
_.each(cubiertas, function(padre, padre_pos, padre_list) {
padre_index += 1;
if (_.isNull(nivel_editado.ids_cubierta)) {
/** 
* esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
*/
 padre._estado=0;
}
 else if (nivel_editado.ids_cubierta.indexOf(padre.id_segured)!=-1) {
 padre._estado=1;
} else {
 padre._estado=0;
}});
var datos=[];
_.each(cubiertas, function(fila, pos) {
  var new_row={};
  _.each(fila, function(x, llave) {
    var newkey='';
    if (llave=='nombre') newkey='label';
    if (llave=='id_segured') newkey='valor';
    if (llave=='_estado') newkey='estado';
    if (newkey!='') new_row[newkey] = fila[llave];
  });
  datos.push(new_row);
});
$.ID_232106170.update({data : datos});
}};
var ID_1214955522 = setTimeout(ID_1214955522_func, 1000*0.2);

}

(function() {
var seltarea = ('seltarea' in require('vars'))?require('vars')['seltarea']:'';
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
$.nivel.set({
id_inspeccion : seltarea.id_server
}
);
if ('nivel' in $) $nivel=$.nivel.toJSON();
}
if (!_.isUndefined(args._fila)) {
require('vars')[_var_scopekey]['argumentos']=args;
/** 
* heredamos datos previos de nivel en modelo nuevo con binding de esta pantalla 
*/
var ID_774685007_i=Alloy.createCollection('insp_niveles');
var ID_774685007_i_where='id=\''+args._fila.id+'\'';
ID_774685007_i.fetch({ query: 'SELECT * FROM insp_niveles WHERE id=\''+args._fila.id+'\'' });
var insp_n=require('helper').query2array(ID_774685007_i);
$.ID_1190114944.setValue(insp_n[0].ano);

/** 
* Cargamos los datos en la tabla 
*/
$.nivel.set({
ids_entrepisos : insp_n[0].ids_entrepisos,
id_inspeccion : insp_n[0].id_inspeccion,
nombre : insp_n[0].nombre,
superficie : insp_n[0].superficie,
largo : insp_n[0].largo,
ids_pavimentos : insp_n[0].ids_pavimentos,
alto : insp_n[0].alto,
ano : insp_n[0].ano,
ids_muros : insp_n[0].ids_muros,
ancho : insp_n[0].ancho,
ids_estructura_cubiera : insp_n[0].ids_estructura_cubiera,
piso : insp_n[0].piso,
ids_cubierta : insp_n[0].ids_cubierta,
ids_estructuras_soportantes : insp_n[0].ids_estructuras_soportantes
}
);
if ('nivel' in $) $nivel=$.nivel.toJSON();
require('vars')[_var_scopekey]['nivel_editado']=insp_n[0];
}
})();

function Postlayout_ID_1943624936(e) {

e.cancelBubble=true;
var elemento=e.source;
require('vars')['var_abriendo']='';

}
if (OS_IOS || OS_ANDROID) {
$.ID_117825709.addEventListener('close',function(){
   $.destroy(); // cleanup bindings
   $.off(); //remove backbone events of this controller
   var _ev_tmp = null, _ev_rem = null;
   if (_my_events) {
      for(_ev_tmp in _my_events) { 
   		try {
   		    if (_ev_tmp.indexOf('_web')!=-1) {
   			   Ti.App.removeEventListener(_ev_tmp.split(',')[0],_my_events[_ev_tmp]);
   		    } else {
   			   Alloy.Events.off(_ev_tmp.split(',')[0],_my_events[_ev_tmp]);
   		    }
   		} catch(err10) {
   		}
      }
      _my_events = null;
      //delete _my_events;
   }
   if (_out_vars) {
      for(_ev_tmp in _out_vars) { 
         for (_ev_rem in _out_vars[_ev_tmp]._remove) {
            try {
 eval(_out_vars[_ev_tmp]._remove[_ev_rem]); 
 } catch(_errt) {}
         }
         _out_vars[_ev_tmp] = null;
      }
      _ev_tmp = null;
      //delete _out_vars;
   }
});
}