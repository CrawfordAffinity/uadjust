var _bind4section = {};
var _list_templates = {};

$.ID_1960650091_window.setTitleAttributes({
    color: 'WHITE'
});
var _activity;
if (OS_ANDROID) {
    _activity = $.ID_1960650091.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_1960650091';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_1960650091_window.addEventListener('open', function(e) {
        abx.setStatusbarColor("#FFFFFF");
        abx.setBackgroundColor("#2d9edb");
    });
}


$.ID_1551530631.init({
    titulo: L('x1951816285_traducir', '¿PUEDE CONTINUAR CON LA INSPECCION?'),
    __id: 'ALL1551530631',
    si: L('x369557195_traducir', 'SI puedo continuar'),
    texto: L('x2522333127_traducir', 'Si está el asegurado en el domicilio presione SI para continuar'),
    onno: no_ID_1232992295,
    onsi: si_ID_1873407735,
    no: L('x3892244486_traducir', 'NO se pudo realizar la inspección'),
    top: 25
});

function si_ID_1873407735(e) {

    var evento = e;
    if (false) console.log('presiono si', {});
    Alloy.createController("datosbasicos_index", {}).getView().open();

}

function no_ID_1232992295(e) {

    var evento = e;
    if (false) console.log('presiono NO', {});
    ID_1475506831.show();

}

function Postlayout_ID_1019114801(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    $.ID_1624238230.show();

}

(function() {
    /** 
     * Llamamos servicio iniciarInspeccion aqui, no hacemos nada con respuesta 
     */
    var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    /** 
     * api de iniciar tarea: solo si estamos en compilacion full 
     */
    var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
    /** 
     * Esto es para indicar en firma ult pantalla a cerrar en cancelada 
     */
    require('vars')['insp_cancelada'] = '';
    if ((_.isObject(url_server) || (_.isString(url_server)) && !_.isEmpty(url_server)) || _.isNumber(url_server) || _.isBoolean(url_server)) {
        /** 
         * Revisamos si la variable url_server contiene datos para poder avisar al servidor que iniciaremos la inspeccion 
         */
        var ID_2080488937 = {};
        if (false) console.log('DEBUG WEB: requesting url:' + String.format(L('x1627917957', '%1$siniciarTarea'), url_server.toString()) + ' with data:', {
            _method: 'POST',
            _params: {
                id_inspector: inspector.id_server,
                codigo_identificador: inspector.codigo_identificador,
                id_tarea: seltarea.id_server,
                num_caso: seltarea.num_caso
            },
            _timeout: '15000'
        });

        ID_2080488937.success = function(e) {
            var elemento = e,
                valor = e;
            /** 
             * Inicializamos inspeccion 
             */
            require('vars')['inspeccion_encurso'] = L('x4261170317', 'true');
            var moment = require('alloy/moment');
            var hora = moment(new Date()).format('HH:mm');
            var moment = require('alloy/moment');
            var fecha = moment(new Date()).format('DD-MM-YYYY');
            var ID_1648086850_m = Alloy.Collections.inspecciones;
            var ID_1648086850_fila = Alloy.createModel('inspecciones', {
                hora: hora,
                fecha: fecha,
                id_server: seltarea.id_server,
                fecha_inspeccion_inicio: new Date()
            });
            ID_1648086850_m.add(ID_1648086850_fila);
            ID_1648086850_fila.save();
            elemento = null, valor = null;
        };

        ID_2080488937.error = function(e) {
            var elemento = e,
                valor = e;
            if (false) console.log('agregado el 13ago EBH', {});
            /** 
             * Inicializamos inspeccion 
             */
            require('vars')['inspeccion_encurso'] = L('x4261170317', 'true');
            var moment = require('alloy/moment');
            var hora = moment(new Date()).format('HH:mm');
            var moment = require('alloy/moment');
            var fecha = moment(new Date()).format('DD-MM-YYYY');
            var ID_1905098930_m = Alloy.Collections.inspecciones;
            var ID_1905098930_fila = Alloy.createModel('inspecciones', {
                hora: hora,
                fecha: fecha,
                id_server: seltarea.id_server,
                fecha_inspeccion_inicio: new Date()
            });
            ID_1905098930_m.add(ID_1905098930_fila);
            ID_1905098930_fila.save();
            elemento = null, valor = null;
        };
        require('helper').ajaxUnico('ID_2080488937', '' + String.format(L('x1627917957', '%1$siniciarTarea'), url_server.toString()) + '', 'POST', {
            id_inspector: inspector.id_server,
            codigo_identificador: inspector.codigo_identificador,
            id_tarea: seltarea.id_server,
            num_caso: seltarea.num_caso
        }, 15000, ID_2080488937);
    }
    /** 
     * Desactivamos seguimiento de tarea 
     */
    require('vars')['seguir_tarea'] = '';
})();


var ID_1475506831_opts = [L('x3481285698_traducir', 'Asegurado no puede seguir'), L('x3077268071_traducir', ' Se me acabo la bateria'), L('x479989047_traducir', ' Tuve un accidente'), L('x2376009830_traducir', ' Cancelar')];
var ID_1475506831 = Ti.UI.createOptionDialog({
    title: L('x1670176792_traducir', 'RAZON PARA CANCELAR INSPECCION ACTUAL'),
    options: ID_1475506831_opts
});
ID_1475506831.addEventListener('click', function(e) {
    var resp = ID_1475506831_opts[e.index];
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
    var razon = "";
    if (resp == L('x3481285698_traducir', 'Asegurado no puede seguir')) {
        /** 
         * Esto parece redundante, pero es para escapar de i18n el texto 
         */
        razon = "Asegurado no puede seguir";
    }
    if (resp == L('x1862794075_traducir', 'Se me acabo la bateria')) {
        razon = "Se me acabo la bateria";
    }
    if (resp == L('x4012805111_traducir', 'Tuve un accidente')) {
        razon = "Tuve un accidente";
    }
    if ((_.isObject(razon) || (_.isString(razon)) && !_.isEmpty(razon)) || _.isNumber(razon) || _.isBoolean(razon)) {
        require('vars')['insp_cancelada'] = L('x155149119_traducir', 'hayalguien');
        if (false) console.log('llamando servicio cancelarTarea', {
            "dettarea": seltarea,
            "inspec": inspector
        });
        var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
        /** 
         * Mostramos popup avisando que se esta cancelando la inspeccion 
         */
        var ID_1698971817_visible = true;

        if (ID_1698971817_visible == 'si') {
            ID_1698971817_visible = true;
        } else if (ID_1698971817_visible == 'no') {
            ID_1698971817_visible = false;
        }
        $.ID_1698971817.setVisible(ID_1698971817_visible);

        var ID_1484602297 = {};
        if (false) console.log('DEBUG WEB: requesting url:' + String.format(L('x3244141284', '%1$scancelarTarea'), url_server.toString()) + ' with data:', {
            _method: 'POST',
            _params: {
                id_inspector: inspector.id_server,
                codigo_identificador: inspector.codigo_identificador,
                id_tarea: seltarea.id_server,
                num_caso: seltarea.num_caso,
                mensaje: razon,
                opcion: 0,
                tipo: 1
            },
            _timeout: '15000'
        });

        ID_1484602297.success = function(e) {
            var elemento = e,
                valor = e;
            if (false) console.log('Mi resultado es', {
                "elemento": elemento
            });
            var ID_1698971817_visible = false;

            if (ID_1698971817_visible == 'si') {
                ID_1698971817_visible = true;
            } else if (ID_1698971817_visible == 'no') {
                ID_1698971817_visible = false;
            }
            $.ID_1698971817.setVisible(ID_1698971817_visible);

            /** 
             * Enviamos a firma 
             */
            Alloy.createController("firma_index", {}).getView().open();
            elemento = null, valor = null;
        };

        ID_1484602297.error = function(e) {
            var elemento = e,
                valor = e;
            if (false) console.log('hubo error llamando cancelarTarea, la agregamos a cola de salida', {
                "elemento": elemento
            });
            if (false) console.log('agregando servicio cancelarTarea a cola', {});
            /** 
             * En el caso que haya habido un problema llamando al servicio cancelarTarea, guardamos los datos en un objeto 
             */
            var datos = {
                id_inspector: seltarea.id_inspector,
                codigo_identificador: inspector.codigo_identificador,
                id_server: seltarea.id_server,
                num_caso: seltarea.num_caso,
                razon: razon
            };
            var ID_1794329393_m = Alloy.Collections.cola;
            var ID_1794329393_fila = Alloy.createModel('cola', {
                data: JSON.stringify(datos),
                id_tarea: seltarea.id_server,
                tipo: 'cancelar'
            });
            ID_1794329393_m.add(ID_1794329393_fila);
            ID_1794329393_fila.save();
            /** 
             * Ocultamos popup avisando que se esta cancelando la inspeccion 
             */
            var ID_1698971817_visible = false;

            if (ID_1698971817_visible == 'si') {
                ID_1698971817_visible = true;
            } else if (ID_1698971817_visible == 'no') {
                ID_1698971817_visible = false;
            }
            $.ID_1698971817.setVisible(ID_1698971817_visible);

            /** 
             * Enviamos a firma 
             */
            Alloy.createController("firma_index", {}).getView().open();
            elemento = null, valor = null;
        };
        require('helper').ajaxUnico('ID_1484602297', '' + String.format(L('x3244141284', '%1$scancelarTarea'), url_server.toString()) + '', 'POST', {
            id_inspector: inspector.id_server,
            codigo_identificador: inspector.codigo_identificador,
            id_tarea: seltarea.id_server,
            num_caso: seltarea.num_caso,
            mensaje: razon,
            opcion: 0,
            tipo: 1
        }, 15000, ID_1484602297);
    }
    resp = null;

});
/** 
 * Cerramos esta pantalla cuando es llamada en la pantalla siguente (datosbasicos) 
 */

_my_events['_cerrar_insp,ID_1503299089'] = function(evento) {

    if (false) console.log('Recibido el evento en HAY ALGUIEN', {
        "contenido del evento": evento.pantalla
    });

    if (!_.isUndefined(evento.pantalla)) {
        if (evento.pantalla == L('x155149119_traducir', 'hayalguien')) {
            if (false) console.log('debug cerrando hayalguien', {});

            var ID_1436370236_trycatch = {
                error: function(e) {
                    if (false) console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
                }
            };
            try {
                ID_1436370236_trycatch.error = function(evento) {
                    if (false) console.log('error cerrando hayalguien', {});
                };
                $.ID_1960650091.close();
            } catch (e) {
                ID_1436370236_trycatch.error(e);
            }
        }
    } else {
        if (false) console.log('debug cerrando (todas) hayalguien', {});

        var ID_1280164765_trycatch = {
            error: function(e) {
                if (false) console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
            }
        };
        try {
            ID_1280164765_trycatch.error = function(evento) {
                if (false) console.log('error cerrando hayalguien', {});
            };
            $.ID_1960650091.close();
        } catch (e) {
            ID_1280164765_trycatch.error(e);
        }
    }
};
Alloy.Events.on('_cerrar_insp', _my_events['_cerrar_insp,ID_1503299089']);

if (OS_IOS || OS_ANDROID) {
    $.ID_1960650091.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}
//$.ID_1960650091.open();