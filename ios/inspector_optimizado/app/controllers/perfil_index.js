var _bind4section = {};
var _list_templates = {
    "tarea_historia": {
        "ID_1680574104": {
            "text": "{comuna}"
        },
        "ID_1304747472": {},
        "ID_676600495": {},
        "ID_1147033032": {},
        "ID_1810186930": {
            "text": "{hora_termino}"
        },
        "ID_911340427": {},
        "ID_1680180472": {
            "visible": "{bt_enviartarea}"
        },
        "ID_452289613": {},
        "ID_930474704": {
            "text": "{direccion}"
        },
        "ID_428063592": {
            "idlocal": "{id}",
            "estado": "{estado_tarea}"
        },
        "ID_1404040740": {},
        "ID_1184366498": {},
        "ID_459591478": {
            "text": "{ciudad}, {pais}"
        },
        "ID_362564056": {
            "visible": "{enviando_tarea}"
        },
        "ID_1887826074": {},
        "ID_165401298": {},
        "ID_1987088054": {},
        "ID_500305974": {},
        "ID_404048727": {}
    }
};

var _activity;
if (OS_ANDROID) {
    _activity = $.ID_1694039928.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_1694039928';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_1694039928.addEventListener('open', function(e) {
        abx.setStatusbarColor("#000000");
        abx.setBackgroundColor("white");
    });
}

function Click_ID_1630309856(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    Alloy.createController("historial_index", {}).getView().open();

}

function Click_ID_892455856(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    Alloy.createController("domicilio_index", {}).getView().open();

}

function Click_ID_1779279805(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    Alloy.createController("disponibilidad_index", {}).getView().open();

}

function Click_ID_24201420(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    Alloy.createController("contacto_index", {}).getView().open();

}

var f_badge_historial = function(x_params) {
    var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
    if (_.isObject(inspector) && !_.isArray(inspector) && !_.isFunction(inspector)) {
        /** 
         * Revisamos que la variable de inspector tenga datos 
         */
        /** 
         * Creamos una variable para el contador de inspecciones por mandar, consultamos el historial de tareas para incrementar el contador en caso de que se encuentren tareas por enviar 
         */
        var cuantas = 0;
        var ID_1115245295_i = Alloy.createCollection('historial_tareas');
        var ID_1115245295_i_where = 'id_inspector=\'' + inspector.id_server + '\' AND estado_tarea=8';
        ID_1115245295_i.fetch({
            query: 'SELECT * FROM historial_tareas WHERE id_inspector=\'' + inspector.id_server + '\' AND estado_tarea=8'
        });
        var inspa = require('helper').query2array(ID_1115245295_i);
        var tarea_index = 0;
        _.each(inspa, function(tarea, tarea_pos, tarea_list) {
            tarea_index += 1;
            var fotos = [];
            var ID_1411350683_f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + tarea.id_server + '/');
            if (ID_1411350683_f.exists() == true) {
                fotos = ID_1411350683_f.getDirectoryListing();
            }
            if (fotos && fotos.length) {
                cuantas = cuantas + 1;
            }
        });
        var ID_1755607697_i = Alloy.createCollection('historial_tareas');
        var ID_1755607697_i_where = 'id_inspector=\'' + inspector.id_server + '\' AND estado_tarea=9';
        ID_1755607697_i.fetch({
            query: 'SELECT * FROM historial_tareas WHERE id_inspector=\'' + inspector.id_server + '\' AND estado_tarea=9'
        });
        var inspb = require('helper').query2array(ID_1755607697_i);
        var tarea_index = 0;
        _.each(inspb, function(tarea, tarea_pos, tarea_list) {
            tarea_index += 1;
            var fotos = [];
            var ID_1560028605_f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + tarea.id_server + '/');
            if (ID_1560028605_f.exists() == true) {
                fotos = ID_1560028605_f.getDirectoryListing();
            }
            if (fotos && fotos.length) {
                cuantas = cuantas + 1;
            }
        });
        $.ID_1356979064.setText(cuantas);
        if (cuantas == 0 || cuantas == '0') {
            /** 
             * Si no hay tareas pendientes para ser enviadas, ocultamos el texto de las inspecciones pendientes por enviar 
             */
            var ID_1271753422_visible = false;

            if (ID_1271753422_visible == 'si') {
                ID_1271753422_visible = true;
            } else if (ID_1271753422_visible == 'no') {
                ID_1271753422_visible = false;
            }
            $.ID_1271753422.setVisible(ID_1271753422_visible);

        } else {
            var ID_1271753422_visible = true;

            if (ID_1271753422_visible == 'si') {
                ID_1271753422_visible = true;
            } else if (ID_1271753422_visible == 'no') {
                ID_1271753422_visible = false;
            }
            $.ID_1271753422.setVisible(ID_1271753422_visible);

        }
        $.ID_28953735.setText(String.format(L('x1445533071', '%1$s %2$s %3$s'), (inspector.nombre) ? inspector.nombre.toString() : '', (inspector.apellido_paterno) ? inspector.apellido_paterno.toString() : '', (inspector.apellido_materno) ? inspector.apellido_materno.toString() : ''));

        $.ID_1570885865.setText(inspector.codigo_identificador);

        $.ID_459542007.setText(inspector.fecha_nacimiento);

        /** 
         * Limpiamos la memoria 
         */

        inspa = null, inspb = null
    }
    return null;
};


(function() {
    var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
    if ((_.isObject(inspector) || _.isString(inspector)) && _.isEmpty(inspector)) {
        /** 
         * Revisamos que la variable del inspector (la que tiene el detalle del inspector) contenga datos, si esta vacio, hacemos una consulta a la tabla, cargamos los datos en la variable y editamos los textos en pantalla 
         */
        var ID_1428163031_i = Alloy.createCollection('inspectores');
        var ID_1428163031_i_where = '';
        ID_1428163031_i.fetch();
        var inspector_list = require('helper').query2array(ID_1428163031_i);
        require('vars')['inspector'] = inspector_list[0];
        var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
        $.ID_28953735.setText(String.format(L('x1445533071', '%1$s %2$s %3$s'), (inspector.nombre) ? inspector.nombre.toString() : '', (inspector.apellido_paterno) ? inspector.apellido_paterno.toString() : '', (inspector.apellido_materno) ? inspector.apellido_materno.toString() : ''));

        $.ID_1570885865.setText(inspector.codigo_identificador);

        $.ID_459542007.setText(inspector.fecha_nacimiento);

    } else {
        var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
        $.ID_28953735.setText(String.format(L('x1445533071', '%1$s %2$s %3$s'), (inspector.nombre) ? inspector.nombre.toString() : '', (inspector.apellido_paterno) ? inspector.apellido_paterno.toString() : '', (inspector.apellido_materno) ? inspector.apellido_materno.toString() : ''));

        $.ID_1570885865.setText(inspector.codigo_identificador);

        $.ID_459542007.setText(inspector.fecha_nacimiento);

    }
    var ID_1814440833 = null;
    if ('badge_historial' in require('funciones')) {
        ID_1814440833 = require('funciones').badge_historial({});
    } else {
        try {
            ID_1814440833 = f_badge_historial({});
        } catch (ee) {}
    }
    /** 
     * 26-feb-2018, cambiado de _refrescar_tareas_mistareas, para que cuando finalice inspeccion tambien se actualice el badge de historial. 
     */

    _my_events['_refrescar_tareas,ID_1843162009'] = function(evento) {
        var ID_1885832003 = null;
        if ('badge_historial' in require('funciones')) {
            ID_1885832003 = require('funciones').badge_historial({});
        } else {
            try {
                ID_1885832003 = f_badge_historial({});
            } catch (ee) {}
        }
    };
    Alloy.Events.on('_refrescar_tareas', _my_events['_refrescar_tareas,ID_1843162009']);
})();

if (OS_IOS || OS_ANDROID) {
    $.ID_1694039928.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}
//$.ID_1694039928.open();