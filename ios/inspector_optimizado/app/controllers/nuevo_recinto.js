var _bind4section={"ref1":"insp_itemdanos"};
var _list_templates={"pborrar":{"ID_911129691":{"text":"{id}"},"ID_123385123":{},"ID_1325519397":{"text":"{nombre}"},"ID_1575624962":{},"ID_1839254956":{},"ID_333012614":{}},"dano":{"ID_1569764493":{"text":"{id}"},"ID_304218376":{},"ID_1912165224":{"text":"{nombre}"}}};
var $recinto = $.recinto.toJSON();

$.ID_204298172_window.setTitleAttributes({
color : 'WHITE'
}
);
var _activity; 
if (OS_ANDROID) { _activity = $.ID_204298172.activity; var abx = require('com.alcoapps.actionbarextras'); }
var _my_events = {}, _out_vars = {}, $item = {}, args = arguments[0] || {}; if ('__args' in args && '__id' in args['__args']) args.__id=args['__args'].__id; if ('__modelo' in args && _.keys(args.__modelo).length>0 && 'item' in $) { $.item.set(args.__modelo); $item = $.item.toJSON(); }
var _var_scopekey = 'ID_204298172';
require('vars')[_var_scopekey]={};
if (OS_ANDROID) {
   $.ID_204298172_window.addEventListener('open', function(e) {
   abx.setStatusbarColor("#FFFFFF");
   abx.setBackgroundColor("#b9aaf3");
   });
}


var ID_595379136_like = function(search) {
  if (typeof search !== 'string' || this === null) {return false; }
  search = search.replace(new RegExp("([\\.\\\\\\+\\*\\?\\[\\^\\]\\$\\(\\)\\{\\}\\=\\!\\<\\>\\|\\:\\-])", "g"), "\\$1");
  search = search.replace(/%/g, '.*').replace(/_/g, '.');
  return RegExp('^' + search + '$', 'gi').test(this);
};
var ID_595379136_filter = function(coll) {
var filtered = coll.filter(function(m) {
	var _tests = [], _all_true = false, model = m.toJSON();
_tests.push((model.id_recinto == '0'));
	var _all_true_s = _.uniq(_tests);
	_all_true = (_all_true_s.length==1 && _all_true_s[0]==true)?true:false;
	return _all_true;
	});
filtered = _.toArray(filtered);
return filtered;
};
var ID_595379136_transform = function(model) {
  var modelo = model.toJSON();
  return modelo;
};
_.defer(function() { 
 Alloy.Collections.insp_itemdanos.fetch(); 
 });
Alloy.Collections.insp_itemdanos.on('add change delete', function(ee) { ID_595379136_update(ee); });
var ID_595379136_update = function(evento) {
var temp_idrecinto = ('temp_idrecinto' in require('vars'))?require('vars')['temp_idrecinto']:'';
if ((_.isObject(temp_idrecinto) || (_.isString(temp_idrecinto)) &&  !_.isEmpty(temp_idrecinto)) || _.isNumber(temp_idrecinto) || _.isBoolean(temp_idrecinto)) {
var ID_915441220_i=Alloy.createCollection('insp_itemdanos');
var ID_915441220_i_where='id_recinto=\''+temp_idrecinto+'\'';
ID_915441220_i.fetch({ query: 'SELECT * FROM insp_itemdanos WHERE id_recinto=\''+temp_idrecinto+'\'' });
var insp_n=require('helper').query2array(ID_915441220_i);
 var alto = 50*insp_n.length;
var ID_232714694_alto = alto;

								  if (ID_232714694_alto=='*') {
									  ID_232714694_alto=Ti.UI.FILL;
								  } else if (!isNaN(ID_232714694_alto)) {
									  ID_232714694_alto=ID_232714694_alto+'dp';
								  }
								  $.ID_232714694.setHeight(ID_232714694_alto);

}
 else {
var seltarea = ('seltarea' in require('vars'))?require('vars')['seltarea']:'';
var ID_918658580_i=Alloy.createCollection('insp_itemdanos');
var ID_918658580_i_where='id_inspeccion=\''+seltarea.id_server+'\'';
ID_918658580_i.fetch({ query: 'SELECT * FROM insp_itemdanos WHERE id_inspeccion=\''+seltarea.id_server+'\'' });
var insp_n=require('helper').query2array(ID_918658580_i);
 var alto = 50*insp_n.length;
var ID_232714694_alto = alto;

								  if (ID_232714694_alto=='*') {
									  ID_232714694_alto=Ti.UI.FILL;
								  } else if (!isNaN(ID_232714694_alto)) {
									  ID_232714694_alto=ID_232714694_alto+'dp';
								  }
								  $.ID_232714694.setHeight(ID_232714694_alto);

}};
Alloy.Collections.insp_itemdanos.fetch();

function Click_ID_194617435(e) {

e.cancelBubble=true;
var elemento=e.source;
/** 
* Limpiamos widgets multiples (memoria) 
*/
$.ID_321122430.limpiar({});
$.ID_1975428302.limpiar({});
$.ID_399652068.limpiar({});
$.ID_1990255213.limpiar({});
$.ID_1536081210.limpiar({});
$.ID_363463535.limpiar({});
$.ID_204298172.close();

}
function Click_ID_1450940879(e) {

e.cancelBubble=true;
var elemento=e.source;
var temp_idrecinto = ('temp_idrecinto' in require('vars'))?require('vars')['temp_idrecinto']:'';
/** 
* Consultamos la tabla de danos, filtrando por el id del recinto y saber si tiene danos 
*/
var ID_1357411979_i=Alloy.createCollection('insp_itemdanos');
var ID_1357411979_i_where='id_recinto=\''+temp_idrecinto+'\'';
ID_1357411979_i.fetch({ query: 'SELECT * FROM insp_itemdanos WHERE id_recinto=\''+temp_idrecinto+'\'' });
var danos_verificacion=require('helper').query2array(ID_1357411979_i);
 var test = $.recinto.toJSON();
if (Ti.App.deployType != 'production') console.log('info datos',{"datos":test});
if (_.isUndefined(test.nombre)) {
/** 
* Validamos que existan los datos ingresados y que esten bien 
*/
var ID_1905058147_opts=[L('x1518866076_traducir','Aceptar')];
var ID_1905058147 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x995453488_traducir','Ingrese el nombre del recinto'),
   buttonNames: ID_1905058147_opts
});
ID_1905058147.addEventListener('click', function(e) {
   var nulo=ID_1905058147_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_1905058147.show();
}
 else if ((_.isObject(test.nombre) ||_.isString(test.nombre)) &&  _.isEmpty(test.nombre)) {
var ID_1044287971_opts=[L('x1518866076_traducir','Aceptar')];
var ID_1044287971 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x995453488_traducir','Ingrese el nombre del recinto'),
   buttonNames: ID_1044287971_opts
});
ID_1044287971.addEventListener('click', function(e) {
   var nulo=ID_1044287971_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_1044287971.show();
} else if (_.isUndefined(test.id_nivel)) {
var ID_1086831580_opts=[L('x1518866076_traducir','Aceptar')];
var ID_1086831580 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x2221207910_traducir','Seleccione el nivel al cual pertenece el nuevo recinto'),
   buttonNames: ID_1086831580_opts
});
ID_1086831580.addEventListener('click', function(e) {
   var nulo=ID_1086831580_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_1086831580.show();
} else if (_.isUndefined(test.largo)) {
var ID_1162705651_opts=[L('x1518866076_traducir','Aceptar')];
var ID_1162705651 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x2947701485_traducir','Ingrese largo del recinto'),
   buttonNames: ID_1162705651_opts
});
ID_1162705651.addEventListener('click', function(e) {
   var nulo=ID_1162705651_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_1162705651.show();
} else if ((_.isObject(test.largo) ||_.isString(test.largo)) &&  _.isEmpty(test.largo)) {
var ID_926089611_opts=[L('x1518866076_traducir','Aceptar')];
var ID_926089611 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x2947701485_traducir','Ingrese largo del recinto'),
   buttonNames: ID_926089611_opts
});
ID_926089611.addEventListener('click', function(e) {
   var nulo=ID_926089611_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_926089611.show();
} else if (_.isUndefined(test.ancho)) {
var ID_1853057559_opts=[L('x1518866076_traducir','Aceptar')];
var ID_1853057559 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x2565566100_traducir','Ingrese ancho del recinto'),
   buttonNames: ID_1853057559_opts
});
ID_1853057559.addEventListener('click', function(e) {
   var nulo=ID_1853057559_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_1853057559.show();
} else if ((_.isObject(test.ancho) ||_.isString(test.ancho)) &&  _.isEmpty(test.ancho)) {
var ID_395309859_opts=[L('x1518866076_traducir','Aceptar')];
var ID_395309859 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x2565566100_traducir','Ingrese ancho del recinto'),
   buttonNames: ID_395309859_opts
});
ID_395309859.addEventListener('click', function(e) {
   var nulo=ID_395309859_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_395309859.show();
} else if (_.isUndefined(test.alto)) {
var ID_1547824742_opts=[L('x1518866076_traducir','Aceptar')];
var ID_1547824742 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x2365115278_traducir','Ingrese alto del recinto'),
   buttonNames: ID_1547824742_opts
});
ID_1547824742.addEventListener('click', function(e) {
   var nulo=ID_1547824742_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_1547824742.show();
} else if ((_.isObject(test.alto) ||_.isString(test.alto)) &&  _.isEmpty(test.alto)) {
var ID_697194687_opts=[L('x1518866076_traducir','Aceptar')];
var ID_697194687 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x2365115278_traducir','Ingrese alto del recinto'),
   buttonNames: ID_697194687_opts
});
ID_697194687.addEventListener('click', function(e) {
   var nulo=ID_697194687_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_697194687.show();
} else if (_.isUndefined(test.ids_estructuras_soportantes)) {
var ID_1277073507_opts=[L('x1518866076_traducir','Aceptar')];
var ID_1277073507 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x503969960_traducir','Seleccione la estructura soportante del recinto'),
   buttonNames: ID_1277073507_opts
});
ID_1277073507.addEventListener('click', function(e) {
   var nulo=ID_1277073507_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_1277073507.show();
} else if ((_.isObject(test.ids_estructuras_soportantes) ||_.isString(test.ids_estructuras_soportantes)) &&  _.isEmpty(test.ids_estructuras_soportantes)) {
var ID_452581767_opts=[L('x1518866076_traducir','Aceptar')];
var ID_452581767 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x503969960_traducir','Seleccione la estructura soportante del recinto'),
   buttonNames: ID_452581767_opts
});
ID_452581767.addEventListener('click', function(e) {
   var nulo=ID_452581767_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_452581767.show();
} else if (danos_verificacion && danos_verificacion.length==0) {
var ID_287335839_opts=[L('x4151998846_traducir','Volver'),L('x2923459930_traducir',' Guardar')];
var ID_287335839 = Ti.UI.createAlertDialog({
   title: L('x1927199828_traducir','¿Guardar sin daños?'),
   message: L('x2099678332_traducir','Estas guardando el recinto sin daños asociados, si esta correcto presiona guardar'),
   buttonNames: ID_287335839_opts
});
ID_287335839.addEventListener('click', function(e) {
   var msj=ID_287335839_opts[e.index];
if (msj==L('x2943883035_traducir','Guardar')) {
if (!_.isUndefined(args._dato)) {
/** 
* Si estamos editando, debemos borrar modelo previo. 
*/
var idrecintoactual = ('idrecintoactual' in require('vars')[_var_scopekey])?require('vars')[_var_scopekey]['idrecintoactual']:'';
var ID_1266820232_i=Alloy.Collections.insp_recintos;
var sql = 'DELETE FROM ' + ID_1266820232_i.config.adapter.collection_name + ' WHERE id=\''+idrecintoactual+'\'';
var db = Ti.Database.open(ID_1266820232_i.config.adapter.db_name);
db.execute(sql);
db.close();
ID_1266820232_i.trigger('delete');
}
Alloy.Collections[$.recinto.config.adapter.collection_name].add($.recinto);
$.recinto.save();
Alloy.Collections[$.recinto.config.adapter.collection_name].fetch();
 test = null;
$.ID_321122430.limpiar({});
$.ID_1975428302.limpiar({});
$.ID_399652068.limpiar({});
$.ID_1990255213.limpiar({});
$.ID_1536081210.limpiar({});
$.ID_363463535.limpiar({});
$.ID_204298172.close();
}
msj = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_287335839.show();
} else {
if (!_.isUndefined(args._dato)) {
/** 
* Si estamos editando, debemos borrar modelo previo. 
*/
var idrecintoactual = ('idrecintoactual' in require('vars')[_var_scopekey])?require('vars')[_var_scopekey]['idrecintoactual']:'';
var ID_1280961897_i=Alloy.Collections.insp_recintos;
var sql = 'DELETE FROM ' + ID_1280961897_i.config.adapter.collection_name + ' WHERE id=\''+idrecintoactual+'\'';
var db = Ti.Database.open(ID_1280961897_i.config.adapter.db_name);
db.execute(sql);
db.close();
ID_1280961897_i.trigger('delete');
}
/** 
* Guardamos el modelo con los datos ingresado 
*/
Alloy.Collections[$.recinto.config.adapter.collection_name].add($.recinto);
$.recinto.save();
Alloy.Collections[$.recinto.config.adapter.collection_name].fetch();
/** 
* Limpiamos widgets multiples (memoria) 
*/
$.ID_321122430.limpiar({});
$.ID_1975428302.limpiar({});
$.ID_399652068.limpiar({});
$.ID_1990255213.limpiar({});
$.ID_1536081210.limpiar({});
$.ID_363463535.limpiar({});
$.ID_204298172.close();
} 
danos_verificacion = null;
temp_id_recinto = null;
test = null

}
function Change_ID_2068855580(e) {

e.cancelBubble=true;
var elemento=e.value;
var source=e.source;
/** 
* Guardamos el nombre del recinto en el modelo 
*/
$.recinto.set({
nombre : elemento
}
);
if ('recinto' in $) $recinto=$.recinto.toJSON();
elemento=null, source=null;

}

$.ID_1494646298.init({
titulo : L('x1571349115_traducir','NIVEL'),
cargando : L('x1740321226_traducir','cargando ..'),
__id : 'ALL1494646298',
left : 0,
onrespuesta : Respuesta_ID_1610688475,
campo : L('x2771220443_traducir','Nivel del Recinto'),
onabrir : Abrir_ID_1372563788,
color : 'morado',
right : 0,
top : 20,
seleccione : L('x2434264250_traducir','seleccione nivel'),
activo : true,
onafterinit : Afterinit_ID_1639978348
}
);

function Afterinit_ID_1639978348(e) {

var evento=e;
var ID_1130215393_func = function() {
var seltarea = ('seltarea' in require('vars'))?require('vars')['seltarea']:'';
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
/** 
* Revisamos si estamos haciendo una inspeccion o es un dummy 
*/
var ID_1333606337_i=Alloy.createCollection('insp_niveles');
var ID_1333606337_i_where='id_inspeccion=\''+seltarea.id_server+'\'';
ID_1333606337_i.fetch({ query: 'SELECT * FROM insp_niveles WHERE id_inspeccion=\''+seltarea.id_server+'\'' });
var datosniveles=require('helper').query2array(ID_1333606337_i);
/** 
* Obtenemos datos para selectores (solo los de esta inspeccion) 
*/
var datos=[];
_.each(datosniveles, function(fila, pos) {
  var new_row={};
  _.each(fila, function(x, llave) {
    var newkey='';
    if (llave=='nombre') newkey='valor';
    if (llave=='id') newkey='id_interno';
    if (newkey!='') new_row[newkey] = fila[llave];
  });
  datos.push(new_row);
});
}
 else {
/** 
* Eliminamos modelo insp_niveles 
*/
/** 
* Eliminamos modelo insp_niveles 
*/
var ID_1348077060_i=Alloy.Collections.insp_niveles;
var sql = "DELETE FROM " + ID_1348077060_i.config.adapter.collection_name;
var db = Ti.Database.open(ID_1348077060_i.config.adapter.db_name);
db.execute(sql);
db.close();
sql=null;
db=null;
ID_1348077060_i.trigger('delete');
var item_index = 0;
_.each('1,2,3,4,5'.split(','), function(item, item_pos, item_list) {
item_index += 1;
var ID_1509751481_m=Alloy.Collections.insp_niveles;
var ID_1509751481_fila = Alloy.createModel('insp_niveles', {
id_inspeccion : -1,
nombre : String.format(L('x2818462194_traducir','Nivel%1$s'), item.toString()),
piso : item
}
 );
ID_1509751481_m.add(ID_1509751481_fila);
ID_1509751481_fila.save();
_.defer(function() { 
});
});
var ID_1952292214_i=Alloy.createCollection('insp_niveles');
ID_1952292214_i.fetch();
var ID_1952292214_src=require('helper').query2array(ID_1952292214_i);
var datos=[];
_.each(ID_1952292214_src, function(fila, pos) {
  var new_row={};
  _.each(fila, function(x, llave) {
    var newkey='';
    if (llave=='nombre') newkey='valor';
    if (llave=='id') newkey='id_interno';
    if (newkey!='') new_row[newkey] = fila[llave];
  });
  datos.push(new_row);
});
}if (!_.isUndefined(args._dato)) {
/** 
* Si estamos editando contenido, cargamos dato previo seleccionado en base de datos. 
*/
var ID_1476542468_i=Alloy.createCollection('insp_recintos');
var ID_1476542468_i_where='id=\''+args._dato.id+'\'';
ID_1476542468_i.fetch({ query: 'SELECT * FROM insp_recintos WHERE id=\''+args._dato.id+'\'' });
var recinto=require('helper').query2array(ID_1476542468_i);
if (Ti.App.deployType != 'production') console.log('detalle del recinto a editar',{"datos":recinto});
if (recinto && recinto.length) {
/** 
* obtenemos valor seleccionado previamente y lo mostramos en selector. 
*/
var ID_791661424_i=Alloy.createCollection('insp_niveles');
var ID_791661424_i_where='id=\''+recinto[0].id_nivel+'\'';
ID_791661424_i.fetch({ query: 'SELECT * FROM insp_niveles WHERE id=\''+recinto[0].id_nivel+'\'' });
var nivel=require('helper').query2array(ID_791661424_i);
if (nivel && nivel.length) {
require('vars')[_var_scopekey]['nivel1']=nivel;
var ID_52538855_func = function() {
var nivel1 = ('nivel1' in require('vars')[_var_scopekey])?require('vars')[_var_scopekey]['nivel1']:'';
$.ID_1494646298.labels({valor : nivel1[0].nombre});
if (Ti.App.deployType != 'production') console.log('el nombre del recinto es',{"datos":nivel1[0].nombre});
 nivel = null;
};
var ID_52538855 = setTimeout(ID_52538855_func, 1000*0.21);
}
}
}
$.ID_1494646298.data({data : datos});
};
var ID_1130215393 = setTimeout(ID_1130215393_func, 1000*0.2);

}
function Abrir_ID_1372563788(e) {

var evento=e;

}
function Respuesta_ID_1610688475(e) {

var evento=e;
/** 
* Ponemos el texto del valor escogido en el widget 
*/
$.ID_1494646298.labels({valor : evento.valor});
/** 
* Guardamos el id del nivel que seleccionamos 
*/
$.recinto.set({
id_nivel : evento.item.id_interno
}
);
if ('recinto' in $) $recinto=$.recinto.toJSON();
if (Ti.App.deployType != 'production') console.log('datos recibidos de modal seleccione nivel',{"datos":evento});

}

$.ID_62888976.init({
caja : 45,
__id : 'ALL62888976',
onlisto : Listo_ID_984349368,
onclick : Click_ID_1339144188
}
);

function Click_ID_1339144188(e) {

var evento=e;
/** 
* Capturamos foto 
*/
var ID_1268991261_result = function(e) {
   var foto_full = e.valor;
   var foto = e.valor.data;
if (foto_full.error==true||foto_full.error=='true') {
/** 
* Si hay error al capturar foto muestra alerta 
*/
var ID_859250702_opts=[L('x1518866076_traducir','Aceptar')];
var ID_859250702 = Ti.UI.createAlertDialog({
   title: L('x57652245_traducir','Alerta'),
   message: L('x1068520423_traducir','Existe un problema con la camara'),
   buttonNames: ID_859250702_opts
});
ID_859250702.addEventListener('click', function(e) {
   var suu=ID_859250702_opts[e.index];
suu = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_859250702.show();
}
 else if (foto_full.cancel==true||foto_full.cancel=='true') {
if (Ti.App.deployType != 'production') console.log('cancelada',{});
$.ID_62888976.detener({});
} else {
var seltarea = ('seltarea' in require('vars'))?require('vars')['seltarea']:'';
/** 
* Procesa la foto 
*/
$.ID_62888976.procesar({imagen : foto,nueva : 640,calidad : 87});
} foto = null;
};
function camara_ID_1268991261() {
   Ti.Media.showCamera({
      success: function(event) {
         if (event.mediaType==Ti.Media.MEDIA_TYPE_PHOTO) {
            ID_1268991261_result({ valor:{ error:false, cancel:false, data:event.media, reason:'' } }); 
         } else {
            ID_1268991261_result({ valor:{ error:true, cancel:false, data:'', reason:'not image' } }); 
         }
      },
      cancel: function() {
         ID_1268991261_result({ valor:{ error:false, cancel:true, data:'', reason:'cancelled' } }); 
      },
      error: function(error) {
         ID_1268991261_result({ valor:{ error:true, cancel:false, data:error, reason:error.error } }); 
      },
      saveToPhotoGallery:false,
      allowImageEditing:true,
      mediaTypes:[Ti.Media.MEDIA_TYPE_PHOTO]
   });
}
require('vars')['_estadoflash_']='auto';
require('vars')['_tipocamara_']='trasera';
require('vars')['_hayflash_']=true;
if (Ti.Media.hasCameraPermissions()) {
   camara_ID_1268991261();
} else {
   Ti.Media.requestCameraPermissions(function(ercp) {
      if (ercp.success) {
         camara_ID_1268991261();
      } else {
         ID_1268991261_result({ valor:{ error:true, cancel:false, data:'nopermission', type:'permission', reason:'camera doesnt have permissions' } });
      }
   });
}

}
function Listo_ID_984349368(e) {

var evento=e;
var seltarea = ('seltarea' in require('vars'))?require('vars')['seltarea']:'';
var ID_1050157153_m=Alloy.Collections.numero_unico;
var ID_1050157153_fila = Alloy.createModel('numero_unico', {
comentario : 'nuevo recinto foto1'
}
 );
ID_1050157153_m.add(ID_1050157153_fila);
ID_1050157153_fila.save();
var nuevoid = require('helper').model2object(ID_1050157153_m.last());
_.defer(function() { 
});
if (!_.isUndefined(evento.comprimida)) {
if (Ti.App.deployType != 'production') console.log(String.format(L('x2721240094','imagen capturada, comprimida y asignada como cap%1$s.jpg'), nuevoid.id.toString()),{});
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
var ID_870492955_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'inspeccion'+seltarea.id_server);
if (ID_870492955_d.exists()==false) ID_870492955_d.createDirectory();
var ID_870492955_f = Ti.Filesystem.getFile(ID_870492955_d.resolve(),'cap'+nuevoid.id+'.jpg');
if (ID_870492955_f.exists()==true) ID_870492955_f.deleteFile();
ID_870492955_f.write(evento.comprimida);
ID_870492955_d = null; ID_870492955_f = null;
var ID_1805445107_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'mini'+seltarea.id_server);
if (ID_1805445107_d.exists()==false) ID_1805445107_d.createDirectory();
var ID_1805445107_f = Ti.Filesystem.getFile(ID_1805445107_d.resolve(),'cap'+nuevoid.id+'.jpg');
if (ID_1805445107_f.exists()==true) ID_1805445107_f.deleteFile();
ID_1805445107_f.write(evento.mini);
ID_1805445107_d = null; ID_1805445107_f = null;
}
 else {
var ID_1705924032_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'inspeccion'+0);
if (ID_1705924032_d.exists()==false) ID_1705924032_d.createDirectory();
var ID_1705924032_f = Ti.Filesystem.getFile(ID_1705924032_d.resolve(),'cap'+nuevoid.id+'.jpg');
if (ID_1705924032_f.exists()==true) ID_1705924032_f.deleteFile();
ID_1705924032_f.write(evento.comprimida);
ID_1705924032_d = null; ID_1705924032_f = null;
var ID_1114793737_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'mini'+0);
if (ID_1114793737_d.exists()==false) ID_1114793737_d.createDirectory();
var ID_1114793737_f = Ti.Filesystem.getFile(ID_1114793737_d.resolve(),'cap'+nuevoid.id+'.jpg');
if (ID_1114793737_f.exists()==true) ID_1114793737_f.deleteFile();
ID_1114793737_f.write(evento.mini);
ID_1114793737_d = null; ID_1114793737_f = null;
}$.recinto.set({
foto1 : String.format(L('x1070475521','cap%1$s.jpg'), nuevoid.id.toString())
}
);
if ('recinto' in $) $recinto=$.recinto.toJSON();
}

}

$.ID_1386533233.init({
caja : 45,
__id : 'ALL1386533233',
onlisto : Listo_ID_1183511394,
onclick : Click_ID_635180829
}
);

function Click_ID_635180829(e) {

var evento=e;
var ID_1132645588_result = function(e) {
   var foto_full = e.valor;
   var foto = e.valor.data;
if (foto_full.error==true||foto_full.error=='true') {
var ID_1608964149_opts=[L('x1518866076_traducir','Aceptar')];
var ID_1608964149 = Ti.UI.createAlertDialog({
   title: L('x57652245_traducir','Alerta'),
   message: L('x1068520423_traducir','Existe un problema con la camara'),
   buttonNames: ID_1608964149_opts
});
ID_1608964149.addEventListener('click', function(e) {
   var suu=ID_1608964149_opts[e.index];
suu = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_1608964149.show();
}
 else if (foto_full.cancel==true||foto_full.cancel=='true') {
if (Ti.App.deployType != 'production') console.log('cancelada',{});
$.ID_1386533233.detener({});
} else {
$.ID_1386533233.procesar({imagen : foto,nueva : 640,calidad : 87});
}};
function camara_ID_1132645588() {
   Ti.Media.showCamera({
      success: function(event) {
         if (event.mediaType==Ti.Media.MEDIA_TYPE_PHOTO) {
            ID_1132645588_result({ valor:{ error:false, cancel:false, data:event.media, reason:'' } }); 
         } else {
            ID_1132645588_result({ valor:{ error:true, cancel:false, data:'', reason:'not image' } }); 
         }
      },
      cancel: function() {
         ID_1132645588_result({ valor:{ error:false, cancel:true, data:'', reason:'cancelled' } }); 
      },
      error: function(error) {
         ID_1132645588_result({ valor:{ error:true, cancel:false, data:error, reason:error.error } }); 
      },
      saveToPhotoGallery:false,
      allowImageEditing:true,
      mediaTypes:[Ti.Media.MEDIA_TYPE_PHOTO]
   });
}
require('vars')['_estadoflash_']='auto';
require('vars')['_tipocamara_']='trasera';
require('vars')['_hayflash_']=true;
if (Ti.Media.hasCameraPermissions()) {
   camara_ID_1132645588();
} else {
   Ti.Media.requestCameraPermissions(function(ercp) {
      if (ercp.success) {
         camara_ID_1132645588();
      } else {
         ID_1132645588_result({ valor:{ error:true, cancel:false, data:'nopermission', type:'permission', reason:'camera doesnt have permissions' } });
      }
   });
}

}
function Listo_ID_1183511394(e) {

var evento=e;
var seltarea = ('seltarea' in require('vars'))?require('vars')['seltarea']:'';
var ID_784045145_m=Alloy.Collections.numero_unico;
var ID_784045145_fila = Alloy.createModel('numero_unico', {
comentario : 'nuevo recinto foto2'
}
 );
ID_784045145_m.add(ID_784045145_fila);
ID_784045145_fila.save();
var nuevoid = require('helper').model2object(ID_784045145_m.last());
_.defer(function() { 
});
if (!_.isUndefined(evento.comprimida)) {
if (Ti.App.deployType != 'production') console.log(String.format(L('x2721240094','imagen capturada, comprimida y asignada como cap%1$s.jpg'), nuevoid.id.toString()),{});
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
var ID_394616061_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'inspeccion'+seltarea.id_server);
if (ID_394616061_d.exists()==false) ID_394616061_d.createDirectory();
var ID_394616061_f = Ti.Filesystem.getFile(ID_394616061_d.resolve(),'cap'+nuevoid.id+'.jpg');
if (ID_394616061_f.exists()==true) ID_394616061_f.deleteFile();
ID_394616061_f.write(evento.comprimida);
ID_394616061_d = null; ID_394616061_f = null;
var ID_515170206_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'mini'+seltarea.id_server);
if (ID_515170206_d.exists()==false) ID_515170206_d.createDirectory();
var ID_515170206_f = Ti.Filesystem.getFile(ID_515170206_d.resolve(),'cap'+nuevoid.id+'.jpg');
if (ID_515170206_f.exists()==true) ID_515170206_f.deleteFile();
ID_515170206_f.write(evento.mini);
ID_515170206_d = null; ID_515170206_f = null;
}
 else {
var ID_870090468_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'inspeccion'+0);
if (ID_870090468_d.exists()==false) ID_870090468_d.createDirectory();
var ID_870090468_f = Ti.Filesystem.getFile(ID_870090468_d.resolve(),'cap'+nuevoid.id+'.jpg');
if (ID_870090468_f.exists()==true) ID_870090468_f.deleteFile();
ID_870090468_f.write(evento.comprimida);
ID_870090468_d = null; ID_870090468_f = null;
var ID_1119169584_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'mini'+0);
if (ID_1119169584_d.exists()==false) ID_1119169584_d.createDirectory();
var ID_1119169584_f = Ti.Filesystem.getFile(ID_1119169584_d.resolve(),'cap'+nuevoid.id+'.jpg');
if (ID_1119169584_f.exists()==true) ID_1119169584_f.deleteFile();
ID_1119169584_f.write(evento.mini);
ID_1119169584_d = null; ID_1119169584_f = null;
}$.recinto.set({
foto2 : String.format(L('x1070475521','cap%1$s.jpg'), nuevoid.id.toString())
}
);
if ('recinto' in $) $recinto=$.recinto.toJSON();
}
 foto = null;

}

$.ID_389922953.init({
caja : 45,
__id : 'ALL389922953',
onlisto : Listo_ID_548361579,
onclick : Click_ID_800379991
}
);

function Click_ID_800379991(e) {

var evento=e;
var ID_357299186_result = function(e) {
   var foto_full = e.valor;
   var foto = e.valor.data;
if (foto_full.error==true||foto_full.error=='true') {
var ID_133336205_opts=[L('x1518866076_traducir','Aceptar')];
var ID_133336205 = Ti.UI.createAlertDialog({
   title: L('x57652245_traducir','Alerta'),
   message: L('x1068520423_traducir','Existe un problema con la camara'),
   buttonNames: ID_133336205_opts
});
ID_133336205.addEventListener('click', function(e) {
   var suu=ID_133336205_opts[e.index];
suu = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_133336205.show();
}
 else if (foto_full.cancel==true||foto_full.cancel=='true') {
if (Ti.App.deployType != 'production') console.log('cancelada',{});
$.ID_389922953.detener({});
} else {
$.ID_389922953.procesar({imagen : foto,nueva : 640,calidad : 87});
} foto = null;
};
function camara_ID_357299186() {
   Ti.Media.showCamera({
      success: function(event) {
         if (event.mediaType==Ti.Media.MEDIA_TYPE_PHOTO) {
            ID_357299186_result({ valor:{ error:false, cancel:false, data:event.media, reason:'' } }); 
         } else {
            ID_357299186_result({ valor:{ error:true, cancel:false, data:'', reason:'not image' } }); 
         }
      },
      cancel: function() {
         ID_357299186_result({ valor:{ error:false, cancel:true, data:'', reason:'cancelled' } }); 
      },
      error: function(error) {
         ID_357299186_result({ valor:{ error:true, cancel:false, data:error, reason:error.error } }); 
      },
      saveToPhotoGallery:false,
      allowImageEditing:true,
      mediaTypes:[Ti.Media.MEDIA_TYPE_PHOTO]
   });
}
require('vars')['_estadoflash_']='auto';
require('vars')['_tipocamara_']='trasera';
require('vars')['_hayflash_']=true;
if (Ti.Media.hasCameraPermissions()) {
   camara_ID_357299186();
} else {
   Ti.Media.requestCameraPermissions(function(ercp) {
      if (ercp.success) {
         camara_ID_357299186();
      } else {
         ID_357299186_result({ valor:{ error:true, cancel:false, data:'nopermission', type:'permission', reason:'camera doesnt have permissions' } });
      }
   });
}

}
function Listo_ID_548361579(e) {

var evento=e;
var seltarea = ('seltarea' in require('vars'))?require('vars')['seltarea']:'';
var ID_1973934436_m=Alloy.Collections.numero_unico;
var ID_1973934436_fila = Alloy.createModel('numero_unico', {
comentario : 'nuevo recinto foto3'
}
 );
ID_1973934436_m.add(ID_1973934436_fila);
ID_1973934436_fila.save();
var nuevoid = require('helper').model2object(ID_1973934436_m.last());
_.defer(function() { 
});
if (!_.isUndefined(evento.comprimida)) {
if (Ti.App.deployType != 'production') console.log(String.format(L('x2721240094','imagen capturada, comprimida y asignada como cap%1$s.jpg'), nuevoid.id.toString()),{});
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
var ID_695920476_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'inspeccion'+seltarea.id_server);
if (ID_695920476_d.exists()==false) ID_695920476_d.createDirectory();
var ID_695920476_f = Ti.Filesystem.getFile(ID_695920476_d.resolve(),'cap'+nuevoid.id+'.jpg');
if (ID_695920476_f.exists()==true) ID_695920476_f.deleteFile();
ID_695920476_f.write(evento.comprimida);
ID_695920476_d = null; ID_695920476_f = null;
var ID_1792806264_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'mini'+seltarea.id_server);
if (ID_1792806264_d.exists()==false) ID_1792806264_d.createDirectory();
var ID_1792806264_f = Ti.Filesystem.getFile(ID_1792806264_d.resolve(),'cap'+nuevoid.id+'.jpg');
if (ID_1792806264_f.exists()==true) ID_1792806264_f.deleteFile();
ID_1792806264_f.write(evento.mini);
ID_1792806264_d = null; ID_1792806264_f = null;
}
 else {
var ID_1374218651_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'inspeccion'+0);
if (ID_1374218651_d.exists()==false) ID_1374218651_d.createDirectory();
var ID_1374218651_f = Ti.Filesystem.getFile(ID_1374218651_d.resolve(),'cap'+nuevoid.id+'.jpg');
if (ID_1374218651_f.exists()==true) ID_1374218651_f.deleteFile();
ID_1374218651_f.write(evento.comprimida);
ID_1374218651_d = null; ID_1374218651_f = null;
var ID_1330076367_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'mini'+0);
if (ID_1330076367_d.exists()==false) ID_1330076367_d.createDirectory();
var ID_1330076367_f = Ti.Filesystem.getFile(ID_1330076367_d.resolve(),'cap'+nuevoid.id+'.jpg');
if (ID_1330076367_f.exists()==true) ID_1330076367_f.deleteFile();
ID_1330076367_f.write(evento.mini);
ID_1330076367_d = null; ID_1330076367_f = null;
}$.recinto.set({
foto3 : String.format(L('x1070475521','cap%1$s.jpg'), nuevoid.id.toString())
}
);
if ('recinto' in $) $recinto=$.recinto.toJSON();
}

}
function Change_ID_1514893135(e) {

e.cancelBubble=true;
var elemento=e.value;
var source=e.source;
/** 
* Obtenemos el valor ingresado en campo de ancho, para poder calcular la superficie 
*/
var ancho;
ancho = $.ID_1051579768.getValue();

if ((_.isObject(ancho) || (_.isString(ancho)) &&  !_.isEmpty(ancho)) || _.isNumber(ancho) || _.isBoolean(ancho)) {
if ((_.isObject(elemento) || (_.isString(elemento)) &&  !_.isEmpty(elemento)) || _.isNumber(elemento) || _.isBoolean(elemento)) {
/** 
* nos aseguramos que ancho y valor de campo sean decimales con puntos (flotante), y no comas (espa&#241;ol) 
*/
 var nuevo=parseFloat(ancho.split(',').join('.'))*parseFloat(elemento.split(',').join('.'));
$.recinto.set({
superficie : nuevo
}
);
if ('recinto' in $) $recinto=$.recinto.toJSON();
}
}
/** 
* Guardamos el largo del recinto 
*/
$.recinto.set({
largo : elemento
}
);
if ('recinto' in $) $recinto=$.recinto.toJSON();
elemento=null, source=null;

}
function Change_ID_473892946(e) {

e.cancelBubble=true;
var elemento=e.value;
var source=e.source;
var largo;
largo = $.ID_1253568597.getValue();

if ((_.isObject(largo) || (_.isString(largo)) &&  !_.isEmpty(largo)) || _.isNumber(largo) || _.isBoolean(largo)) {
if ((_.isObject(elemento) || (_.isString(elemento)) &&  !_.isEmpty(elemento)) || _.isNumber(elemento) || _.isBoolean(elemento)) {
/** 
* nos aseguramos que largo y valor de campo sean decimales con puntos (flotante), y no comas (espa&#241;ol) 
*/
 var nuevo=parseFloat(largo.split(',').join('.'))*parseFloat(elemento.split(',').join('.'));
$.recinto.set({
superficie : nuevo
}
);
if ('recinto' in $) $recinto=$.recinto.toJSON();
}
}
$.recinto.set({
ancho : elemento
}
);
if ('recinto' in $) $recinto=$.recinto.toJSON();
elemento=null, source=null;

}
function Change_ID_740070738(e) {

e.cancelBubble=true;
var elemento=e.value;
var source=e.source;
$.recinto.set({
alto : elemento
}
);
if ('recinto' in $) $recinto=$.recinto.toJSON();
elemento=null, source=null;

}

$.ID_321122430.init({
titulo : L('x1975271086_traducir','Estructura Soportante'),
cargando : L('x1740321226_traducir','cargando ..'),
__id : 'ALL321122430',
oncerrar : Cerrar_ID_866581192,
left : 0,
hint : L('x2898603391_traducir','Seleccione estructura'),
color : 'morado',
subtitulo : L('x4011106049_traducir','Indique los tipos'),
right : 0,
top : 5,
onclick : Click_ID_547046452,
onafterinit : Afterinit_ID_1719673259
}
);

function Click_ID_547046452(e) {

var evento=e;

}
function Cerrar_ID_866581192(e) {

var evento=e;
$.ID_321122430.update({});
$.recinto.set({
ids_estructuras_soportantes : evento.valores
}
);
if ('recinto' in $) $recinto=$.recinto.toJSON();

}
function Afterinit_ID_1719673259(e) {

var evento=e;
var ID_1260507929_func = function() {
var seltarea = ('seltarea' in require('vars'))?require('vars')['seltarea']:'';
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
var pais = ('pais' in require('vars'))?require('vars')['pais']:'';
var ID_1428751800_i=Alloy.createCollection('estructura_soportante');
var ID_1428751800_i_where='pais_texto=\''+pais[0].nombre+'\'';
ID_1428751800_i.fetch({ query: 'SELECT * FROM estructura_soportante WHERE pais_texto=\''+pais[0].nombre+'\'' });
var estructura=require('helper').query2array(ID_1428751800_i);
var datos=[];
_.each(estructura, function(fila, pos) {
  var new_row={};
  _.each(fila, function(x, llave) {
    var newkey='';
    if (llave=='nombre') newkey='label';
    if (llave=='id_segured') newkey='valor';
    if (newkey!='') new_row[newkey] = fila[llave];
  });
  new_row['estado'] = 0;
  datos.push(new_row);
});
$.ID_321122430.update({data : datos});
}
 else {
var ID_965895026_i=Alloy.Collections.estructura_soportante;
var sql = "DELETE FROM " + ID_965895026_i.config.adapter.collection_name;
var db = Ti.Database.open(ID_965895026_i.config.adapter.db_name);
db.execute(sql);
db.close();
sql=null;
db=null;
ID_965895026_i.trigger('delete');
var item_index = 0;
_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
item_index += 1;
var ID_1326868287_m=Alloy.Collections.estructura_soportante;
var ID_1326868287_fila = Alloy.createModel('estructura_soportante', {
nombre : String.format(L('x1088195980_traducir','Estructura%1$s'), item.toString()),
id_server : String.format(L('x1290178835_traducir','0%1$s'), item.toString()),
id_segured : String.format(L('x1125471167_traducir','10%1$s'), item.toString()),
pais : 1
}
 );
ID_1326868287_m.add(ID_1326868287_fila);
ID_1326868287_fila.save();
_.defer(function() { 
});
});
var ID_1560607667_i=Alloy.createCollection('estructura_soportante');
ID_1560607667_i.fetch();
var ID_1560607667_src=require('helper').query2array(ID_1560607667_i);
var datos=[];
_.each(ID_1560607667_src, function(fila, pos) {
  var new_row={};
  _.each(fila, function(x, llave) {
    var newkey='';
    if (llave=='nombre') newkey='label';
    if (llave=='id_segured') newkey='valor';
    if (newkey!='') new_row[newkey] = fila[llave];
  });
  new_row['estado'] = 0;
  datos.push(new_row);
});
$.ID_321122430.update({data : datos});
}
datos = null
if (!_.isUndefined(args._dato)) {
var ID_1163494656_i=Alloy.createCollection('insp_recintos');
var ID_1163494656_i_where='id=\''+args._dato.id+'\'';
ID_1163494656_i.fetch({ query: 'SELECT * FROM insp_recintos WHERE id=\''+args._dato.id+'\'' });
var recinto=require('helper').query2array(ID_1163494656_i);
if (recinto && recinto.length) {
/** 
* obtenemos valor seleccionado previamente y lo mostramos en selector. 
*/
if ((_.isObject(recinto[0].ids_estructuras_soportantes) || (_.isString(recinto[0].ids_estructuras_soportantes)) &&  !_.isEmpty(recinto[0].ids_estructuras_soportantes)) || _.isNumber(recinto[0].ids_estructuras_soportantes) || _.isBoolean(recinto[0].ids_estructuras_soportantes)) {
 var estructuras = recinto[0].ids_estructuras_soportantes;
 var estructurasSeparadas = estructuras.split(",");
 var estructurasElegidos = '';
var a_index = 0;
_.each(estructurasSeparadas, function(a, a_pos, a_list) {
a_index += 1;
var ID_1409972512_i=Alloy.createCollection('estructura_soportante');
var ID_1409972512_i_where='id_segured=\''+a+'\'';
ID_1409972512_i.fetch({ query: 'SELECT * FROM estructura_soportante WHERE id_segured=\''+a+'\'' });
var estructura=require('helper').query2array(ID_1409972512_i);
if ((_.isObject(estructurasElegidos) ||_.isString(estructurasElegidos)) &&  _.isEmpty(estructurasElegidos)) {
 estructurasElegidos += estructura[0].nombre;
}
 else {
 estructurasElegidos += ' + ' + estructura[0].nombre;
}if (estructura && estructura.length) {
var ID_991230781_func = function() {
if (Ti.App.deployType != 'production') console.log('UPDATE',{"val":estructurasElegidos});
$.ID_321122430.update({valor : estructurasElegidos});
a = null; estructurasSeparadas = null; estructurasElegidos = null; recinto = null, estructuras = null;
};
var ID_991230781 = setTimeout(ID_991230781_func, 1000*0.21);
}
});
}
 else {
if (Ti.App.deployType != 'production') console.log('no hay estructuras soportantes',{});
}}
}
};
var ID_1260507929 = setTimeout(ID_1260507929_func, 1000*0.2);

}

$.ID_1975428302.init({
titulo : L('x1219835481_traducir','Muros / Tabiques'),
cargando : L('x1740321226_traducir','cargando ..'),
__id : 'ALL1975428302',
oncerrar : Cerrar_ID_430468783,
left : 0,
hint : L('x2879998099_traducir','Seleccione muros'),
color : 'morado',
subtitulo : L('x4011106049_traducir','Indique los tipos'),
right : 0,
top : 5,
onclick : Click_ID_38762254,
onafterinit : Afterinit_ID_1217341344
}
);

function Click_ID_38762254(e) {

var evento=e;

}
function Cerrar_ID_430468783(e) {

var evento=e;
$.ID_1975428302.update({});
$.recinto.set({
ids_muros : evento.valores
}
);
if ('recinto' in $) $recinto=$.recinto.toJSON();

}
function Afterinit_ID_1217341344(e) {

var evento=e;
var ID_93083937_func = function() {
var seltarea = ('seltarea' in require('vars'))?require('vars')['seltarea']:'';
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
var pais = ('pais' in require('vars'))?require('vars')['pais']:'';
var ID_226882694_i=Alloy.createCollection('muros_tabiques');
var ID_226882694_i_where='pais_texto=\''+pais[0].nombre+'\'';
ID_226882694_i.fetch({ query: 'SELECT * FROM muros_tabiques WHERE pais_texto=\''+pais[0].nombre+'\'' });
var tabiques=require('helper').query2array(ID_226882694_i);
var datos=[];
_.each(tabiques, function(fila, pos) {
  var new_row={};
  _.each(fila, function(x, llave) {
    var newkey='';
    if (llave=='nombre') newkey='label';
    if (llave=='id_segured') newkey='valor';
    if (newkey!='') new_row[newkey] = fila[llave];
  });
  new_row['estado'] = 0;
  datos.push(new_row);
});
$.ID_1975428302.update({data : datos});
}
 else {
var ID_1632798819_i=Alloy.Collections.muros_tabiques;
var sql = "DELETE FROM " + ID_1632798819_i.config.adapter.collection_name;
var db = Ti.Database.open(ID_1632798819_i.config.adapter.db_name);
db.execute(sql);
db.close();
sql=null;
db=null;
ID_1632798819_i.trigger('delete');
var item_index = 0;
_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
item_index += 1;
var ID_1716569383_m=Alloy.Collections.muros_tabiques;
var ID_1716569383_fila = Alloy.createModel('muros_tabiques', {
nombre : String.format(L('x3565664878_traducir','Muros%1$s'), item.toString()),
id_server : String.format(L('x1290178835_traducir','0%1$s'), item.toString()),
id_segured : String.format(L('x1125471167_traducir','10%1$s'), item.toString()),
pais : 1
}
 );
ID_1716569383_m.add(ID_1716569383_fila);
ID_1716569383_fila.save();
_.defer(function() { 
});
});
var ID_1768795363_i=Alloy.createCollection('muros_tabiques');
ID_1768795363_i.fetch();
var ID_1768795363_src=require('helper').query2array(ID_1768795363_i);
var datos=[];
_.each(ID_1768795363_src, function(fila, pos) {
  var new_row={};
  _.each(fila, function(x, llave) {
    var newkey='';
    if (llave=='nombre') newkey='label';
    if (llave=='id_segured') newkey='valor';
    if (newkey!='') new_row[newkey] = fila[llave];
  });
  new_row['estado'] = 0;
  datos.push(new_row);
});
$.ID_1975428302.update({data : datos});
}
tabiques = null,
datos = null
if (!_.isUndefined(args._dato)) {
var ID_1402735265_i=Alloy.createCollection('insp_recintos');
var ID_1402735265_i_where='id=\''+args._dato.id+'\'';
ID_1402735265_i.fetch({ query: 'SELECT * FROM insp_recintos WHERE id=\''+args._dato.id+'\'' });
var recinto=require('helper').query2array(ID_1402735265_i);
if (recinto && recinto.length) {
/** 
* obtenemos valor seleccionado previamente y lo mostramos en selector. 
*/
if ((_.isObject(recinto[0].ids_muros) || (_.isString(recinto[0].ids_muros)) &&  !_.isEmpty(recinto[0].ids_muros)) || _.isNumber(recinto[0].ids_muros) || _.isBoolean(recinto[0].ids_muros)) {
 var muros = recinto[0].ids_muros;
 var murosSeparados = muros.split(",");
 var muroElegidos = '';
var b_index = 0;
_.each(murosSeparados, function(b, b_pos, b_list) {
b_index += 1;
var ID_1396159465_i=Alloy.createCollection('muros_tabiques');
var ID_1396159465_i_where='id_segured=\''+b+'\'';
ID_1396159465_i.fetch({ query: 'SELECT * FROM muros_tabiques WHERE id_segured=\''+b+'\'' });
var muro=require('helper').query2array(ID_1396159465_i);
if ((_.isObject(muroElegidos) ||_.isString(muroElegidos)) &&  _.isEmpty(muroElegidos)) {
 muroElegidos += muro[0].nombre;
}
 else {
 muroElegidos += ' + ' + muro[0].nombre;
}if (muro && muro.length) {
var ID_1135464224_func = function() {
$.ID_1975428302.update({valor : muroElegidos});
recinto = null; muros = null; murosSeparadas = null, murosElegidos = null; b = null; ;
};
var ID_1135464224 = setTimeout(ID_1135464224_func, 1000*0.21);
}
});
}
 else {
if (Ti.App.deployType != 'production') console.log('no hay muros',{});
}}
}
};
var ID_93083937 = setTimeout(ID_93083937_func, 1000*0.2);

}

$.ID_399652068.init({
titulo : L('x3327059844_traducir','Entrepisos'),
cargando : L('x1740321226_traducir','cargando ..'),
__id : 'ALL399652068',
oncerrar : Cerrar_ID_1223292786,
left : 0,
hint : L('x2146928948_traducir','Seleccione entrepisos'),
color : 'morado',
subtitulo : L('x4011106049_traducir','Indique los tipos'),
right : 0,
top : 5,
onclick : Click_ID_1631472609,
onafterinit : Afterinit_ID_537546359
}
);

function Click_ID_1631472609(e) {

var evento=e;

}
function Cerrar_ID_1223292786(e) {

var evento=e;
$.ID_399652068.update({});
$.recinto.set({
ids_entrepisos : evento.valores
}
);
if ('recinto' in $) $recinto=$.recinto.toJSON();

}
function Afterinit_ID_537546359(e) {

var evento=e;
var ID_1667308270_func = function() {
var seltarea = ('seltarea' in require('vars'))?require('vars')['seltarea']:'';
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
var pais = ('pais' in require('vars'))?require('vars')['pais']:'';
var ID_166482715_i=Alloy.createCollection('entrepisos');
var ID_166482715_i_where='pais_texto=\''+pais[0].nombre+'\'';
ID_166482715_i.fetch({ query: 'SELECT * FROM entrepisos WHERE pais_texto=\''+pais[0].nombre+'\'' });
var entrepisos=require('helper').query2array(ID_166482715_i);
var datos=[];
_.each(entrepisos, function(fila, pos) {
  var new_row={};
  _.each(fila, function(x, llave) {
    var newkey='';
    if (llave=='nombre') newkey='label';
    if (llave=='id_segured') newkey='valor';
    if (newkey!='') new_row[newkey] = fila[llave];
  });
  new_row['estado'] = 0;
  datos.push(new_row);
});
$.ID_399652068.update({data : datos});
}
 else {
var ID_1944345357_i=Alloy.Collections.entrepisos;
var sql = "DELETE FROM " + ID_1944345357_i.config.adapter.collection_name;
var db = Ti.Database.open(ID_1944345357_i.config.adapter.db_name);
db.execute(sql);
db.close();
sql=null;
db=null;
ID_1944345357_i.trigger('delete');
var item_index = 0;
_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
item_index += 1;
var ID_1499058541_m=Alloy.Collections.entrepisos;
var ID_1499058541_fila = Alloy.createModel('entrepisos', {
nombre : String.format(L('x2266735154_traducir','Entrepiso%1$s'), item.toString()),
id_server : String.format(L('x1290178835_traducir','0%1$s'), item.toString()),
id_segured : String.format(L('x1125471167_traducir','10%1$s'), item.toString()),
pais : 1
}
 );
ID_1499058541_m.add(ID_1499058541_fila);
ID_1499058541_fila.save();
_.defer(function() { 
});
});
var ID_44835984_i=Alloy.createCollection('entrepisos');
ID_44835984_i.fetch();
var ID_44835984_src=require('helper').query2array(ID_44835984_i);
var datos=[];
_.each(ID_44835984_src, function(fila, pos) {
  var new_row={};
  _.each(fila, function(x, llave) {
    var newkey='';
    if (llave=='nombre') newkey='label';
    if (llave=='id_segured') newkey='valor';
    if (newkey!='') new_row[newkey] = fila[llave];
  });
  new_row['estado'] = 0;
  datos.push(new_row);
});
$.ID_399652068.update({data : datos});
 datos = null;
}
pais = null,
entrepisos = null,
datos = null
if (!_.isUndefined(args._dato)) {
var ID_572295686_i=Alloy.createCollection('insp_recintos');
var ID_572295686_i_where='id=\''+args._dato.id+'\'';
ID_572295686_i.fetch({ query: 'SELECT * FROM insp_recintos WHERE id=\''+args._dato.id+'\'' });
var recinto=require('helper').query2array(ID_572295686_i);
if (recinto && recinto.length) {
/** 
* obtenemos valor seleccionado previamente y lo mostramos en selector. 
*/
if ((_.isObject(recinto[0].ids_entrepisos) || (_.isString(recinto[0].ids_entrepisos)) &&  !_.isEmpty(recinto[0].ids_entrepisos)) || _.isNumber(recinto[0].ids_entrepisos) || _.isBoolean(recinto[0].ids_entrepisos)) {
 var entrepisos = recinto[0].ids_entrepisos;
 var entrepisosSeparadas = entrepisos.split(",");
 var entrepisoElegidos = '';
var c_index = 0;
_.each(entrepisosSeparadas, function(c, c_pos, c_list) {
c_index += 1;
var ID_823869703_i=Alloy.createCollection('entrepisos');
var ID_823869703_i_where='id_segured=\''+c+'\'';
ID_823869703_i.fetch({ query: 'SELECT * FROM entrepisos WHERE id_segured=\''+c+'\'' });
var entrepiso=require('helper').query2array(ID_823869703_i);
if ((_.isObject(entrepisoElegidos) ||_.isString(entrepisoElegidos)) &&  _.isEmpty(entrepisoElegidos)) {
 entrepisoElegidos += entrepiso[0].nombre;
}
 else {
 entrepisoElegidos += ' + ' + entrepiso[0].nombre;
}if (entrepiso && entrepiso.length) {
var ID_1953689051_func = function() {
$.ID_399652068.update({valor : entrepisoElegidos});
recinto = null; entrepisos = null; entrepisosSeparadas = null; entrepisoElegidos = null; c = null;
};
var ID_1953689051 = setTimeout(ID_1953689051_func, 1000*0.21);
}
});
}
 else {
if (Ti.App.deployType != 'production') console.log('no hay entrepisos',{});
}}
}
};
var ID_1667308270 = setTimeout(ID_1667308270_func, 1000*0.2);

}

$.ID_1990255213.init({
titulo : L('x591862035_traducir','Pavimentos'),
cargando : L('x1740321226_traducir','cargando ..'),
__id : 'ALL1990255213',
oncerrar : Cerrar_ID_1979957644,
left : 0,
hint : L('x2600368035_traducir','Seleccione pavimentos'),
color : 'morado',
subtitulo : L('x4011106049_traducir','Indique los tipos'),
right : 0,
top : 5,
onclick : Click_ID_1433183121,
onafterinit : Afterinit_ID_594392290
}
);

function Click_ID_1433183121(e) {

var evento=e;

}
function Cerrar_ID_1979957644(e) {

var evento=e;
$.ID_1990255213.update({});
$.recinto.set({
ids_pavimentos : evento.valores
}
);
if ('recinto' in $) $recinto=$.recinto.toJSON();

}
function Afterinit_ID_594392290(e) {

var evento=e;
var ID_11528857_func = function() {
var seltarea = ('seltarea' in require('vars'))?require('vars')['seltarea']:'';
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
var pais = ('pais' in require('vars'))?require('vars')['pais']:'';
var ID_1111305011_i=Alloy.createCollection('pavimento');
var ID_1111305011_i_where='pais_texto=\''+pais[0].nombre+'\'';
ID_1111305011_i.fetch({ query: 'SELECT * FROM pavimento WHERE pais_texto=\''+pais[0].nombre+'\'' });
var pavimentos=require('helper').query2array(ID_1111305011_i);
var datos=[];
_.each(pavimentos, function(fila, pos) {
  var new_row={};
  _.each(fila, function(x, llave) {
    var newkey='';
    if (llave=='nombre') newkey='label';
    if (llave=='id_segured') newkey='valor';
    if (newkey!='') new_row[newkey] = fila[llave];
  });
  new_row['estado'] = 0;
  datos.push(new_row);
});
$.ID_1990255213.update({data : datos});
}
 else {
var ID_1152477302_i=Alloy.Collections.pavimento;
var sql = "DELETE FROM " + ID_1152477302_i.config.adapter.collection_name;
var db = Ti.Database.open(ID_1152477302_i.config.adapter.db_name);
db.execute(sql);
db.close();
sql=null;
db=null;
ID_1152477302_i.trigger('delete');
var item_index = 0;
_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
item_index += 1;
var ID_1812019101_m=Alloy.Collections.pavimento;
var ID_1812019101_fila = Alloy.createModel('pavimento', {
nombre : String.format(L('x427067467_traducir','Pavimento%1$s'), item.toString()),
id_server : String.format(L('x1290178835_traducir','0%1$s'), item.toString()),
id_segured : String.format(L('x1125471167_traducir','10%1$s'), item.toString()),
pais : 1
}
 );
ID_1812019101_m.add(ID_1812019101_fila);
ID_1812019101_fila.save();
_.defer(function() { 
});
});
var ID_888785974_i=Alloy.createCollection('pavimento');
ID_888785974_i.fetch();
var ID_888785974_src=require('helper').query2array(ID_888785974_i);
var datos=[];
_.each(ID_888785974_src, function(fila, pos) {
  var new_row={};
  _.each(fila, function(x, llave) {
    var newkey='';
    if (llave=='nombre') newkey='label';
    if (llave=='id_segured') newkey='valor';
    if (newkey!='') new_row[newkey] = fila[llave];
  });
  new_row['estado'] = 0;
  datos.push(new_row);
});
$.ID_1990255213.update({data : datos});
}
pavimentos = null,
datos = null
if (!_.isUndefined(args._dato)) {
var ID_891162546_i=Alloy.createCollection('insp_recintos');
var ID_891162546_i_where='id=\''+args._dato.id+'\'';
ID_891162546_i.fetch({ query: 'SELECT * FROM insp_recintos WHERE id=\''+args._dato.id+'\'' });
var recinto=require('helper').query2array(ID_891162546_i);
if (recinto && recinto.length) {
/** 
* obtenemos valor seleccionado previamente y lo mostramos en selector. 
*/
if ((_.isObject(recinto[0].ids_pavimentos) || (_.isString(recinto[0].ids_pavimentos)) &&  !_.isEmpty(recinto[0].ids_pavimentos)) || _.isNumber(recinto[0].ids_pavimentos) || _.isBoolean(recinto[0].ids_pavimentos)) {
 var pavimentos = recinto[0].ids_pavimentos;
 var pavimentosSeparadas = pavimentos.split(",");
 var pavimentoElegidos = '';
var d_index = 0;
_.each(pavimentosSeparadas, function(d, d_pos, d_list) {
d_index += 1;
var ID_1965938728_i=Alloy.createCollection('pavimento');
var ID_1965938728_i_where='id_segured=\''+d+'\'';
ID_1965938728_i.fetch({ query: 'SELECT * FROM pavimento WHERE id_segured=\''+d+'\'' });
var pavimento=require('helper').query2array(ID_1965938728_i);
if ((_.isObject(pavimentoElegidos) ||_.isString(pavimentoElegidos)) &&  _.isEmpty(pavimentoElegidos)) {
 pavimentoElegidos += pavimento[0].nombre;
}
 else {
 pavimentoElegidos += ' + ' + pavimento[0].nombre;
}if (pavimento && pavimento.length) {
var ID_961862621_func = function() {
$.ID_1990255213.update({valor : pavimentoElegidos});
 recinto =null; pavimentos =null; pavimentosSeparadas =null; pavimentoElegidos =null; d =null;
};
var ID_961862621 = setTimeout(ID_961862621_func, 1000*0.21);
}
});
}
 else {
if (Ti.App.deployType != 'production') console.log('no hay pavimentos',{});
}}
}
};
var ID_11528857 = setTimeout(ID_11528857_func, 1000*0.2);

}

$.ID_1536081210.init({
titulo : L('x1866523485_traducir','Estruct. cubierta'),
cargando : L('x1740321226_traducir','cargando ..'),
__id : 'ALL1536081210',
oncerrar : Cerrar_ID_307547788,
left : 0,
hint : L('x2460890829_traducir','Seleccione e.cubiertas'),
color : 'morado',
subtitulo : L('x4011106049_traducir','Indique los tipos'),
right : 0,
top : 5,
onclick : Click_ID_1479345319,
onafterinit : Afterinit_ID_301236740
}
);

function Click_ID_1479345319(e) {

var evento=e;

}
function Cerrar_ID_307547788(e) {

var evento=e;
$.ID_1536081210.update({});
$.recinto.set({
ids_estructura_cubiera : evento.valores
}
);
if ('recinto' in $) $recinto=$.recinto.toJSON();

}
function Afterinit_ID_301236740(e) {

var evento=e;
var ID_93562171_func = function() {
var seltarea = ('seltarea' in require('vars'))?require('vars')['seltarea']:'';
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
var pais = ('pais' in require('vars'))?require('vars')['pais']:'';
var ID_602189889_i=Alloy.createCollection('estructura_cubierta');
var ID_602189889_i_where='pais_texto=\''+pais[0].nombre+'\'';
ID_602189889_i.fetch({ query: 'SELECT * FROM estructura_cubierta WHERE pais_texto=\''+pais[0].nombre+'\'' });
var ecubiertas=require('helper').query2array(ID_602189889_i);
var datos=[];
_.each(ecubiertas, function(fila, pos) {
  var new_row={};
  _.each(fila, function(x, llave) {
    var newkey='';
    if (llave=='nombre') newkey='label';
    if (llave=='id_segured') newkey='valor';
    if (newkey!='') new_row[newkey] = fila[llave];
  });
  new_row['estado'] = 0;
  datos.push(new_row);
});
$.ID_1536081210.update({data : datos});
}
 else {
var ID_465022339_i=Alloy.Collections.estructura_cubierta;
var sql = "DELETE FROM " + ID_465022339_i.config.adapter.collection_name;
var db = Ti.Database.open(ID_465022339_i.config.adapter.db_name);
db.execute(sql);
db.close();
sql=null;
db=null;
ID_465022339_i.trigger('delete');
var item_index = 0;
_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
item_index += 1;
var ID_289810078_m=Alloy.Collections.estructura_cubierta;
var ID_289810078_fila = Alloy.createModel('estructura_cubierta', {
nombre : String.format(L('x1686539481_traducir','Estru Cubierta %1$s'), item.toString()),
id_server : String.format(L('x1290178835_traducir','0%1$s'), item.toString()),
id_segured : String.format(L('x1125471167_traducir','10%1$s'), item.toString()),
pais : 1
}
 );
ID_289810078_m.add(ID_289810078_fila);
ID_289810078_fila.save();
_.defer(function() { 
});
});
var ID_1472411243_i=Alloy.createCollection('estructura_cubierta');
ID_1472411243_i.fetch();
var ID_1472411243_src=require('helper').query2array(ID_1472411243_i);
var datos=[];
_.each(ID_1472411243_src, function(fila, pos) {
  var new_row={};
  _.each(fila, function(x, llave) {
    var newkey='';
    if (llave=='nombre') newkey='label';
    if (llave=='id_segured') newkey='valor';
    if (newkey!='') new_row[newkey] = fila[llave];
  });
  new_row['estado'] = 0;
  datos.push(new_row);
});
$.ID_1536081210.update({data : datos});
}
ecubiertas  = null,
datos = null
if (!_.isUndefined(args._dato)) {
var ID_1158582208_i=Alloy.createCollection('insp_recintos');
var ID_1158582208_i_where='id=\''+args._dato.id+'\'';
ID_1158582208_i.fetch({ query: 'SELECT * FROM insp_recintos WHERE id=\''+args._dato.id+'\'' });
var recinto=require('helper').query2array(ID_1158582208_i);
if (recinto && recinto.length) {
/** 
* obtenemos valor seleccionado previamente y lo mostramos en selector. 
*/
if ((_.isObject(recinto[0].ids_estructura_cubiera) || (_.isString(recinto[0].ids_estructura_cubiera)) &&  !_.isEmpty(recinto[0].ids_estructura_cubiera)) || _.isNumber(recinto[0].ids_estructura_cubiera) || _.isBoolean(recinto[0].ids_estructura_cubiera)) {
 var esCubierta = recinto[0].ids_estructura_cubiera;
 var esCubiertasSeparadas = esCubierta.split(",");
 var esCubiertaElegidos = '';
var e_index = 0;
_.each(esCubiertasSeparadas, function(e, e_pos, e_list) {
e_index += 1;
var ID_1572079154_i=Alloy.createCollection('estructura_cubierta');
var ID_1572079154_i_where='id_segured=\''+e+'\'';
ID_1572079154_i.fetch({ query: 'SELECT * FROM estructura_cubierta WHERE id_segured=\''+e+'\'' });
var es_cubierta=require('helper').query2array(ID_1572079154_i);
if ((_.isObject(esCubiertaElegidos) ||_.isString(esCubiertaElegidos)) &&  _.isEmpty(esCubiertaElegidos)) {
 esCubiertaElegidos += es_cubierta[0].nombre;
}
 else {
 esCubiertaElegidos += ' + ' + es_cubierta[0].nombre;
}if (es_cubierta && es_cubierta.length) {
var ID_127517620_func = function() {
$.ID_1536081210.update({valor : esCubiertaElegidos});
 recinto = null; esCubierta = null; esCubiertasSeparadas = null; esCubiertaElegidos = null; e = null;
;
};
var ID_127517620 = setTimeout(ID_127517620_func, 1000*0.21);
}
});
}
 else {
if (Ti.App.deployType != 'production') console.log('no hay estructura cubierta',{});
}}
}
};
var ID_93562171 = setTimeout(ID_93562171_func, 1000*0.2);

}

$.ID_363463535.init({
titulo : L('x2266302645_traducir','Cubierta'),
cargando : L('x1740321226_traducir','cargando ..'),
__id : 'ALL363463535',
oncerrar : Cerrar_ID_443341336,
left : 0,
hint : L('x2134385782_traducir','Seleccione cubiertas'),
color : 'morado',
subtitulo : L('x4011106049_traducir','Indique los tipos'),
right : 0,
top : 5,
onclick : Click_ID_157900844,
onafterinit : Afterinit_ID_1701721686
}
);

function Click_ID_157900844(e) {

var evento=e;

}
function Cerrar_ID_443341336(e) {

var evento=e;
$.ID_363463535.update({});
$.recinto.set({
ids_cubierta : evento.valores
}
);
if ('recinto' in $) $recinto=$.recinto.toJSON();

}
function Afterinit_ID_1701721686(e) {

var evento=e;
var ID_566132131_func = function() {
var seltarea = ('seltarea' in require('vars'))?require('vars')['seltarea']:'';
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
var pais = ('pais' in require('vars'))?require('vars')['pais']:'';
var ID_233184011_i=Alloy.createCollection('cubierta');
var ID_233184011_i_where='pais_texto=\''+pais[0].nombre+'\'';
ID_233184011_i.fetch({ query: 'SELECT * FROM cubierta WHERE pais_texto=\''+pais[0].nombre+'\'' });
var cubiertas=require('helper').query2array(ID_233184011_i);
var datos=[];
_.each(cubiertas, function(fila, pos) {
  var new_row={};
  _.each(fila, function(x, llave) {
    var newkey='';
    if (llave=='nombre') newkey='label';
    if (llave=='id_segured') newkey='valor';
    if (newkey!='') new_row[newkey] = fila[llave];
  });
  new_row['estado'] = 0;
  datos.push(new_row);
});
$.ID_363463535.update({data : datos});
}
 else {
var ID_1032916351_i=Alloy.Collections.cubierta;
var sql = "DELETE FROM " + ID_1032916351_i.config.adapter.collection_name;
var db = Ti.Database.open(ID_1032916351_i.config.adapter.db_name);
db.execute(sql);
db.close();
sql=null;
db=null;
ID_1032916351_i.trigger('delete');
var item_index = 0;
_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
item_index += 1;
var ID_1913652567_m=Alloy.Collections.cubierta;
var ID_1913652567_fila = Alloy.createModel('cubierta', {
nombre : String.format(L('x2246230604_traducir','Cubierta %1$s'), item.toString()),
id_server : String.format(L('x1290178835_traducir','0%1$s'), item.toString()),
id_segured : String.format(L('x1125471167_traducir','10%1$s'), item.toString()),
pais : 1
}
 );
ID_1913652567_m.add(ID_1913652567_fila);
ID_1913652567_fila.save();
_.defer(function() { 
});
});
var ID_259258747_i=Alloy.createCollection('cubierta');
ID_259258747_i.fetch();
var ID_259258747_src=require('helper').query2array(ID_259258747_i);
var datos=[];
_.each(ID_259258747_src, function(fila, pos) {
  var new_row={};
  _.each(fila, function(x, llave) {
    var newkey='';
    if (llave=='nombre') newkey='label';
    if (llave=='id_segured') newkey='valor';
    if (newkey!='') new_row[newkey] = fila[llave];
  });
  new_row['estado'] = 0;
  datos.push(new_row);
});
$.ID_363463535.update({data : datos});
}
pais = null,
cubiertas = null,
datos = null
if (!_.isUndefined(args._dato)) {
var ID_1274666045_i=Alloy.createCollection('insp_recintos');
var ID_1274666045_i_where='id=\''+args._dato.id+'\'';
ID_1274666045_i.fetch({ query: 'SELECT * FROM insp_recintos WHERE id=\''+args._dato.id+'\'' });
var recinto=require('helper').query2array(ID_1274666045_i);
if (recinto && recinto.length) {
/** 
* obtenemos valor seleccionado previamente y lo mostramos en selector. 
*/
if ((_.isObject(recinto[0].ids_cubierta) || (_.isString(recinto[0].ids_cubierta)) &&  !_.isEmpty(recinto[0].ids_cubierta)) || _.isNumber(recinto[0].ids_cubierta) || _.isBoolean(recinto[0].ids_cubierta)) {
 var cubierta = recinto[0].ids_cubierta;
 var cubiertasSeparadas = cubierta.split(",");
 var cubiertaElegidos = '';
var f_index = 0;
_.each(cubiertasSeparadas, function(f, f_pos, f_list) {
f_index += 1;
var ID_1831750360_i=Alloy.createCollection('cubierta');
var ID_1831750360_i_where='id_segured=\''+f+'\'';
ID_1831750360_i.fetch({ query: 'SELECT * FROM cubierta WHERE id_segured=\''+f+'\'' });
var cubierta=require('helper').query2array(ID_1831750360_i);
if ((_.isObject(cubiertaElegidos) ||_.isString(cubiertaElegidos)) &&  _.isEmpty(cubiertaElegidos)) {
 cubiertaElegidos += cubierta[0].nombre;
}
 else {
 cubiertaElegidos += ' + ' + cubierta[0].nombre;
}if (cubierta && cubierta.length) {
var ID_1131253245_func = function() {
$.ID_363463535.update({valor : cubiertaElegidos});
  recinto = null; cubierta = null; cubiertasSeparadas = null; cubiertaElegidos = null; f = null;
};
var ID_1131253245 = setTimeout(ID_1131253245_func, 1000*0.21);
}
});
}
 else {
if (Ti.App.deployType != 'production') console.log('no hay cubiertas',{});
}}
}
};
var ID_566132131 = setTimeout(ID_566132131_func, 1000*0.2);

}
function Click_ID_1190507160(e) {

e.cancelBubble=true;
var elemento=e.source;
Alloy.createController("nuevo_dano",{}).getView().open();

}
function Swipe_ID_616394471(e) {

e.cancelBubble=true;
var elemento=e.section.getItemAt(e.itemIndex);
var findVariables = require('fvariables');
var fila = {};
_.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
   _.each(obj_id, function(valor, prop) {
      fila[prop] = elemento[id_field][prop];
      var llaves = findVariables(valor,'{','}');
      _.each(llaves, function(llave) {
         fila[llave] = elemento[id_field][prop];
      });
   });
});
var flag_borrar = ('flag_borrar' in require('vars')[_var_scopekey])?require('vars')[_var_scopekey]['flag_borrar']:'';
if (flag_borrar==false||flag_borrar=='false') {
if (e.direction==L('x2053629800_traducir','left')) {
var findVariables = require('fvariables');
elemento.template = 'pborrar';
_.each(_list_templates['pborrar'], function(obj_id, id_field) {
   _.each(obj_id, function(valor, prop) {
      var llaves = findVariables(valor,'{','}');
      _.each(llaves, function(llave) {
         elemento[id_field] = {};
         elemento[id_field][prop] = fila[llave];
      });
   });
});
if (OS_IOS) {
   e.section.updateItemAt(e.itemIndex, elemento, { animated: true });
} else if (OS_ANDROID) {
   e.section.updateItemAt(e.itemIndex, elemento);
}
require('vars')[_var_scopekey]['flag_borrar']=L('x4261170317','true');
}
}
 else {
require('vars')[_var_scopekey]['flag_borrar']=L('x734881840_traducir','false');
_.defer(function() { 
 Alloy.Collections.insp_itemdanos.fetch(); 
 });
}
}
function Click_ID_1244881555(e) {

e.cancelBubble=true;
var elemento=e.section.getItemAt(e.itemIndex);
var findVariables = require('fvariables');
var fila = {};
_.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
   _.each(obj_id, function(valor, prop) {
      fila[prop] = elemento[id_field][prop];
      var llaves = findVariables(valor,'{','}');
      _.each(llaves, function(llave) {
         fila[llave] = elemento[id_field][prop];
      });
   });
});
var flag_borrar = ('flag_borrar' in require('vars')[_var_scopekey])?require('vars')[_var_scopekey]['flag_borrar']:'';
if (flag_borrar==false||flag_borrar=='false') {
/** 
* Ocupamos esta variable para impedir que la pantalla siguente se cargue dos veces 
*/
var var_abriendo = ('var_abriendo' in require('vars'))?require('vars')['var_abriendo']:'';
if ((_.isObject(var_abriendo) ||_.isString(var_abriendo)) &&  _.isEmpty(var_abriendo)) {
require('vars')['var_abriendo']=L('x4261170317','true');
/** 
* Enviamos el parametro _dato para indicar cual sera el id del dano a editar 
*/
Alloy.createController("nuevo_dano",{'_dato' : fila}).getView().open();
}
}
 else {
_.defer(function() { 
 Alloy.Collections.insp_itemdanos.fetch(); 
 });
require('vars')[_var_scopekey]['flag_borrar']=L('x734881840_traducir','false');
}
}
function Click_ID_1260268716(e) {

e.cancelBubble=true;
var elemento=e.section.getItemAt(e.itemIndex);
var findVariables = require('fvariables');
var fila = {};
_.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
   _.each(obj_id, function(valor, prop) {
      fila[prop] = elemento[id_field][prop];
      var llaves = findVariables(valor,'{','}');
      _.each(llaves, function(llave) {
         fila[llave] = elemento[id_field][prop];
      });
   });
});
var ID_1455191021_opts=[L('x3827418516_traducir','Si'),L('x1962639792_traducir',' No')];
var ID_1455191021 = Ti.UI.createAlertDialog({
   title: L('x4097537701_traducir','ALERTA'),
   message: ''+String.format(L('x614696086_traducir','Seguro quiere borrar %1$s ?'), fila.nombre.toString())+'',
   buttonNames: ID_1455191021_opts
});
ID_1455191021.addEventListener('click', function(e) {
   var xd=ID_1455191021_opts[e.index];
if (xd==L('x3827418516_traducir','Si')) {
var ID_570776821_i=Alloy.Collections.insp_itemdanos;
var sql = 'DELETE FROM ' + ID_570776821_i.config.adapter.collection_name + ' WHERE id=\''+fila.id+'\'';
var db = Ti.Database.open(ID_570776821_i.config.adapter.db_name);
db.execute(sql);
db.close();
ID_570776821_i.trigger('delete');
_.defer(function() { 
 Alloy.Collections.insp_itemdanos.fetch(); 
 });
require('vars')[_var_scopekey]['flag_borrar']=L('x734881840_traducir','false');
}
 else {
require('vars')[_var_scopekey]['flag_borrar']=L('x734881840_traducir','false');
_.defer(function() { 
 Alloy.Collections.insp_itemdanos.fetch(); 
 });
}xd = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_1455191021.show();

}
function Swipe_ID_892281804(e) {

e.cancelBubble=true;
var elemento=e.section.getItemAt(e.itemIndex);
var findVariables = require('fvariables');
var fila = {};
_.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
   _.each(obj_id, function(valor, prop) {
      fila[prop] = elemento[id_field][prop];
      var llaves = findVariables(valor,'{','}');
      _.each(llaves, function(llave) {
         fila[llave] = elemento[id_field][prop];
      });
   });
});
if (e.direction==L('x3033167124_traducir','right')) {
var findVariables = require('fvariables');
elemento.template = 'dano';
_.each(_list_templates['dano'], function(obj_id, id_field) {
   _.each(obj_id, function(valor, prop) {
      var llaves = findVariables(valor,'{','}');
      _.each(llaves, function(llave) {
         elemento[id_field] = {};
         elemento[id_field][prop] = fila[llave];
      });
   });
});
if (OS_IOS) {
   e.section.updateItemAt(e.itemIndex, elemento, { animated: true });
} else if (OS_ANDROID) {
   e.section.updateItemAt(e.itemIndex, elemento);
}
require('vars')[_var_scopekey]['flag_borrar']=L('x734881840_traducir','false');
}

}
function Click_ID_1661414996(e) {

e.cancelBubble=true;
var elemento=e.section.getItemAt(e.itemIndex);
var findVariables = require('fvariables');
var fila = {};
_.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
   _.each(obj_id, function(valor, prop) {
      fila[prop] = elemento[id_field][prop];
      var llaves = findVariables(valor,'{','}');
      _.each(llaves, function(llave) {
         fila[llave] = elemento[id_field][prop];
      });
   });
});
require('vars')[_var_scopekey]['flag_borrar']=L('x734881840_traducir','false');
_.defer(function() { 
 Alloy.Collections.insp_itemdanos.fetch(); 
 });

}

(function() {
var seltarea = ('seltarea' in require('vars'))?require('vars')['seltarea']:'';
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
/** 
* Modificamos la idinspeccion con el idserver de la tarea 
*/
$.recinto.set({
id_inspeccion : seltarea.id_server
}
);
if ('recinto' in $) $recinto=$.recinto.toJSON();
}
if (!_.isUndefined(args._dato)) {
/** 
* Si existe args._dato estamos editando recinto 
*/
/** 
* Modificamos titulo de header en caso de estar editando recinto 
*/
var ID_204298172_titulo = 'EDITAR RECINTO';

									var setTitle2 = function(valor) {
										if (OS_ANDROID) {
											abx.title = valor;
										} else {
											$.ID_204298172_window.setTitle(valor);
										}
									};
									var getTitle2 = function() {
										if (OS_ANDROID) {
											return abx.title;
										} else {
											return $.ID_204298172_window.getTitle();
										}
									};
									setTitle2(ID_204298172_titulo);

require('vars')[_var_scopekey]['idrecintoactual']=args._dato.id;
/** 
* heredamos datos previos de nivel en modelo nuevo con binding de esta pantalla 
*/
var ID_506371344_i=Alloy.createCollection('insp_recintos');
var ID_506371344_i_where='id=\''+args._dato.id+'\'';
ID_506371344_i.fetch({ query: 'SELECT * FROM insp_recintos WHERE id=\''+args._dato.id+'\'' });
var insp_r=require('helper').query2array(ID_506371344_i);
/** 
* filtramos items de dano, para el id_recinto activo. 
*/
ID_595379136_filter = function(coll) {
var filtered = coll.filter(function(m) {
	var _tests = [], _all_true = false, model = m.toJSON();
_tests.push((model.id_recinto == insp_r[0].id_recinto));
	var _all_true_s = _.uniq(_tests);
	_all_true = (_all_true_s.length==1 && _all_true_s[0]==true)?true:false;
	return _all_true;
	});
filtered = _.toArray(filtered);
return filtered;
};
_.defer(function() { 
 Alloy.Collections.insp_itemdanos.fetch(); 
 });
/** 
* Cargamos el modelo con los datos obtenidos desde la consulta 
*/
$.recinto.set({
ids_entrepisos : insp_r[0].ids_entrepisos,
id_inspeccion : insp_r[0].id_inspeccion,
nombre : insp_r[0].nombre,
superficie : insp_r[0].superficie,
largo : insp_r[0].largo,
foto1 : insp_r[0].foto1,
foto2 : insp_r[0].foto2,
id_recinto : insp_r[0].id_recinto,
foto3 : insp_r[0].foto3,
ids_pavimentos : insp_r[0].ids_pavimentos,
alto : insp_r[0].alto,
ids_muros : insp_r[0].ids_muros,
ancho : insp_r[0].ancho,
ids_estructura_cubiera : insp_r[0].ids_estructura_cubiera,
ids_cubierta : insp_r[0].ids_cubierta,
id_nivel : insp_r[0].id_nivel,
ids_estructuras_soportantes : insp_r[0].ids_estructuras_soportantes
}
);
if ('recinto' in $) $recinto=$.recinto.toJSON();
/** 
* guardamos variable pq aun no existe este recinto al agregar da&#241;os 
*/
require('vars')['temp_idrecinto']=insp_r[0].id_recinto;
/** 
* Cargamos los textos en los campos de texto 
*/
$.ID_1712023120.setValue(insp_r[0].nombre);

$.ID_1253568597.setValue(insp_r[0].largo);

$.ID_1051579768.setValue(insp_r[0].ancho);

$.ID_1174191051.setValue(insp_r[0].alto);

if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
/** 
* Revisamos si existe variable seltarea para saber si las fotos son desde la inspeccion o son datos dummy 
*/
if (Ti.App.deployType != 'production') console.log('es parte de la compilacion inspeccion completa',{});
if ((_.isObject(insp_r[0].foto1) || (_.isString(insp_r[0].foto1)) &&  !_.isEmpty(insp_r[0].foto1)) || _.isNumber(insp_r[0].foto1) || _.isBoolean(insp_r[0].foto1)) {
var ID_1218924586_trycatch = { error: function(e) { if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.',e); } };
try {
ID_1218924586_trycatch.error = function(evento) {
};
var foto1 = '';
var ID_1743680972_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'mini'+insp_r[0].id_inspeccion);
if (ID_1743680972_d.exists()==true) {
  var ID_1743680972_f = Ti.Filesystem.getFile(ID_1743680972_d.resolve(),insp_r[0].foto1);
  if (ID_1743680972_f.exists()==true) {
    foto1 = ID_1743680972_f.read();
  }
  ID_1743680972_f = null;
}
ID_1743680972_d = null;
} catch (e) {
   ID_1218924586_trycatch.error(e);
}
}
if ((_.isObject(insp_r[0].foto2) || (_.isString(insp_r[0].foto2)) &&  !_.isEmpty(insp_r[0].foto2)) || _.isNumber(insp_r[0].foto2) || _.isBoolean(insp_r[0].foto2)) {
var ID_1324389329_trycatch = { error: function(e) { if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.',e); } };
try {
ID_1324389329_trycatch.error = function(evento) {
};
var foto2 = '';
var ID_1116016580_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'mini'+insp_r[0].id_inspeccion);
if (ID_1116016580_d.exists()==true) {
  var ID_1116016580_f = Ti.Filesystem.getFile(ID_1116016580_d.resolve(),insp_r[0].foto2);
  if (ID_1116016580_f.exists()==true) {
    foto2 = ID_1116016580_f.read();
  }
  ID_1116016580_f = null;
}
ID_1116016580_d = null;
} catch (e) {
   ID_1324389329_trycatch.error(e);
}
}
if ((_.isObject(insp_r[0].foto3) || (_.isString(insp_r[0].foto3)) &&  !_.isEmpty(insp_r[0].foto3)) || _.isNumber(insp_r[0].foto3) || _.isBoolean(insp_r[0].foto3)) {
var ID_55032749_trycatch = { error: function(e) { if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.',e); } };
try {
ID_55032749_trycatch.error = function(evento) {
};
var foto3 = '';
var ID_1411782814_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'mini'+insp_r[0].id_inspeccion);
if (ID_1411782814_d.exists()==true) {
  var ID_1411782814_f = Ti.Filesystem.getFile(ID_1411782814_d.resolve(),insp_r[0].foto3);
  if (ID_1411782814_f.exists()==true) {
    foto3 = ID_1411782814_f.read();
  }
  ID_1411782814_f = null;
}
ID_1411782814_d = null;
} catch (e) {
   ID_55032749_trycatch.error(e);
}
}
}
 else {
if (Ti.App.deployType != 'production') console.log('esta compilacion esta sola, simulamos carpetas',{});
if ((_.isObject(insp_r[0].foto1) || (_.isString(insp_r[0].foto1)) &&  !_.isEmpty(insp_r[0].foto1)) || _.isNumber(insp_r[0].foto1) || _.isBoolean(insp_r[0].foto1)) {
var ID_547629173_trycatch = { error: function(e) { if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.',e); } };
try {
ID_547629173_trycatch.error = function(evento) {
};
var foto1 = '';
var ID_315896947_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'mini'+0);
if (ID_315896947_d.exists()==true) {
  var ID_315896947_f = Ti.Filesystem.getFile(ID_315896947_d.resolve(),insp_r[0].foto1);
  if (ID_315896947_f.exists()==true) {
    foto1 = ID_315896947_f.read();
  }
  ID_315896947_f = null;
}
ID_315896947_d = null;
} catch (e) {
   ID_547629173_trycatch.error(e);
}
}
if ((_.isObject(insp_r[0].foto2) || (_.isString(insp_r[0].foto2)) &&  !_.isEmpty(insp_r[0].foto2)) || _.isNumber(insp_r[0].foto2) || _.isBoolean(insp_r[0].foto2)) {
var ID_1616909749_trycatch = { error: function(e) { if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.',e); } };
try {
ID_1616909749_trycatch.error = function(evento) {
};
var foto2 = '';
var ID_33999265_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'mini'+0);
if (ID_33999265_d.exists()==true) {
  var ID_33999265_f = Ti.Filesystem.getFile(ID_33999265_d.resolve(),insp_r[0].foto2);
  if (ID_33999265_f.exists()==true) {
    foto2 = ID_33999265_f.read();
  }
  ID_33999265_f = null;
}
ID_33999265_d = null;
} catch (e) {
   ID_1616909749_trycatch.error(e);
}
}
if ((_.isObject(insp_r[0].foto3) || (_.isString(insp_r[0].foto3)) &&  !_.isEmpty(insp_r[0].foto3)) || _.isNumber(insp_r[0].foto3) || _.isBoolean(insp_r[0].foto3)) {
var ID_1025504743_trycatch = { error: function(e) { if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.',e); } };
try {
ID_1025504743_trycatch.error = function(evento) {
};
var foto3 = '';
var ID_4931431_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'mini'+0);
if (ID_4931431_d.exists()==true) {
  var ID_4931431_f = Ti.Filesystem.getFile(ID_4931431_d.resolve(),insp_r[0].foto3);
  if (ID_4931431_f.exists()==true) {
    foto3 = ID_4931431_f.read();
  }
  ID_4931431_f = null;
}
ID_4931431_d = null;
} catch (e) {
   ID_1025504743_trycatch.error(e);
}
}
}if ((_.isObject(foto1) || (_.isString(foto1)) &&  !_.isEmpty(foto1)) || _.isNumber(foto1) || _.isBoolean(foto1)) {
/** 
* Revisamos que foto1, foto2 y foto3 contengan texto, por lo que si es asi, modificamos la imagen previa y ponemos la foto miniatura obtenida desde la memoria 
*/
if (Ti.App.deployType != 'production') console.log('llamamos evento mini',{});
$.ID_62888976.mini({mini : foto1});
}
if ((_.isObject(foto2) || (_.isString(foto2)) &&  !_.isEmpty(foto2)) || _.isNumber(foto2) || _.isBoolean(foto2)) {
if (Ti.App.deployType != 'production') console.log('llamamos evento mini',{});
$.ID_1386533233.mini({mini : foto2});
}
if ((_.isObject(foto3) || (_.isString(foto3)) &&  !_.isEmpty(foto3)) || _.isNumber(foto3) || _.isBoolean(foto3)) {
if (Ti.App.deployType != 'production') console.log('llamamos evento mini',{});
$.ID_389922953.mini({mini : foto3});
}
var ID_1825155490_i=Alloy.createCollection('insp_itemdanos');
var ID_1825155490_i_where='id_inspeccion=\''+seltarea.id_server+'\'';
ID_1825155490_i.fetch({ query: 'SELECT * FROM insp_itemdanos WHERE id_inspeccion=\''+seltarea.id_server+'\'' });
var insp_n=require('helper').query2array(ID_1825155490_i);
 var alto = 50*insp_n.length;
var ID_232714694_alto = alto;

								  if (ID_232714694_alto=='*') {
									  ID_232714694_alto=Ti.UI.FILL;
								  } else if (!isNaN(ID_232714694_alto)) {
									  ID_232714694_alto=ID_232714694_alto+'dp';
								  }
								  $.ID_232714694.setHeight(ID_232714694_alto);

}
 else if (_.isUndefined(args._dato)) {
var ID_1918205425_m=Alloy.Collections.numero_unico;
var ID_1918205425_fila = Alloy.createModel('numero_unico', {
comentario : 'nuevo recinto id'
}
 );
ID_1918205425_m.add(ID_1918205425_fila);
ID_1918205425_fila.save();
var nuevoid = require('helper').model2object(ID_1918205425_m.last());
_.defer(function() { 
});
/** 
* guardamos variable pq aun no existe este recinto al agregar da&#241;os 
*/
require('vars')['temp_idrecinto']=nuevoid.id;
$.recinto.set({
id_recinto : nuevoid.id
}
);
if ('recinto' in $) $recinto=$.recinto.toJSON();
/** 
* filtramos items de dano, para el id_recinto recien creado. 
*/
ID_595379136_filter = function(coll) {
var filtered = coll.filter(function(m) {
	var _tests = [], _all_true = false, model = m.toJSON();
_tests.push((model.id_recinto == nuevoid.id));
	var _all_true_s = _.uniq(_tests);
	_all_true = (_all_true_s.length==1 && _all_true_s[0]==true)?true:false;
	return _all_true;
	});
filtered = _.toArray(filtered);
return filtered;
};
_.defer(function() { 
 Alloy.Collections.insp_itemdanos.fetch(); 
 });
}
foto1 = null;
foto2 = null;
foto3 = null
require('vars')[_var_scopekey]['flag_borrar']=L('x734881840_traducir','false');
})();

function Postlayout_ID_57529384(e) {

e.cancelBubble=true;
var elemento=e.source;
require('vars')['var_abriendo']='';

}
if (OS_IOS || OS_ANDROID) {
$.ID_204298172.addEventListener('close',function(){
   $.destroy(); // cleanup bindings
   $.off(); //remove backbone events of this controller
   var _ev_tmp = null, _ev_rem = null;
   if (_my_events) {
      for(_ev_tmp in _my_events) { 
   		try {
   		    if (_ev_tmp.indexOf('_web')!=-1) {
   			   Ti.App.removeEventListener(_ev_tmp.split(',')[0],_my_events[_ev_tmp]);
   		    } else {
   			   Alloy.Events.off(_ev_tmp.split(',')[0],_my_events[_ev_tmp]);
   		    }
   		} catch(err10) {
   		}
      }
      _my_events = null;
      //delete _my_events;
   }
   if (_out_vars) {
      for(_ev_tmp in _out_vars) { 
         for (_ev_rem in _out_vars[_ev_tmp]._remove) {
            try {
 eval(_out_vars[_ev_tmp]._remove[_ev_rem]); 
 } catch(_errt) {}
         }
         _out_vars[_ev_tmp] = null;
      }
      _ev_tmp = null;
      //delete _out_vars;
   }
});
}