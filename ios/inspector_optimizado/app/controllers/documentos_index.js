var _bind4section = {};
var _list_templates = {};
var $documentos = $.documentos.toJSON();

$.ID_2036699456_window.setTitleAttributes({
    color: 'WHITE'
});
var _activity;
if (OS_ANDROID) {
    _activity = $.ID_2036699456.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_2036699456';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_2036699456_window.addEventListener('open', function(e) {
        abx.setStatusbarColor("#FFFFFF");
        abx.setBackgroundColor("#ffacaa");
    });
}

function Click_ID_1638350386(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    ID_1160550620.show();

}

$.ID_1921077056.init({
    titulo: L('x404129330_traducir', '¿EL ASEGURADO CONFIRMA ESTOS DATOS?'),
    __id: 'ALL1921077056',
    si: L('x1723413441_traducir', 'SI, Están correctos'),
    texto: L('x2083765231_traducir', 'El asegurado debe confirmar que los datos de esta sección están correctos'),
    pantalla: L('x2851883104_traducir', '¿ESTA DE ACUERDO?'),
    onno: no_ID_1982968930,
    onsi: si_ID_1333561605,
    no: L('x55492959_traducir', 'NO, Hay que modificar algo'),
    header: 'rosado',
    onclick: Click_ID_908226810
});

function Click_ID_908226810(e) {

    var evento = e;
    require('vars')[_var_scopekey]['alguno_on'] = L('x734881840_traducir', 'false');
    if ($documentos.doc1 == 1 || $documentos.doc1 == '1') {
        require('vars')[_var_scopekey]['alguno_on'] = L('x4261170317', 'true');
    } else if ($documentos.doc2 == 1 || $documentos.doc2 == '1') {
        require('vars')[_var_scopekey]['alguno_on'] = L('x4261170317', 'true');
    } else if ($documentos.doc3 == 1 || $documentos.doc3 == '1') {
        require('vars')[_var_scopekey]['alguno_on'] = L('x4261170317', 'true');
    } else if ($documentos.doc4 == 1 || $documentos.doc4 == '1') {
        require('vars')[_var_scopekey]['alguno_on'] = L('x4261170317', 'true');
    } else if ($documentos.doc5 == 1 || $documentos.doc5 == '1') {
        require('vars')[_var_scopekey]['alguno_on'] = L('x4261170317', 'true');
    } else if ($documentos.doc6 == 1 || $documentos.doc6 == '1') {
        require('vars')[_var_scopekey]['alguno_on'] = L('x4261170317', 'true');
    } else if ($documentos.doc7 == 1 || $documentos.doc7 == '1') {
        require('vars')[_var_scopekey]['alguno_on'] = L('x4261170317', 'true');
    } else if ($documentos.doc8 == 1 || $documentos.doc8 == '1') {
        require('vars')[_var_scopekey]['alguno_on'] = L('x4261170317', 'true');
    } else if ($documentos.doc9 == 1 || $documentos.doc9 == '1') {
        require('vars')[_var_scopekey]['alguno_on'] = L('x4261170317', 'true');
    } else if ($documentos.doc10 == 1 || $documentos.doc10 == '1') {
        require('vars')[_var_scopekey]['alguno_on'] = L('x4261170317', 'true');
    }
    var alguno_on = ('alguno_on' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['alguno_on'] : '';
    if (false) console.log('PROBANDO1', {
        "prueba": alguno_on
    });
    if (alguno_on == true || alguno_on == 'true') {
        if (false) console.log('PROBANDO2', {
            "prueba": alguno_on,
            "los dias": $documentos.dias
        });
        if (_.isUndefined($documentos.dias)) {
            var ID_1096626793_opts = [L('x1518866076_traducir', 'Aceptar')];
            var ID_1096626793 = Ti.UI.createAlertDialog({
                title: L('x3237162386_traducir', 'Atencion'),
                message: L('x1870732722_traducir', 'Ingrese días de compromiso'),
                buttonNames: ID_1096626793_opts
            });
            ID_1096626793.addEventListener('click', function(e) {
                var nulo = ID_1096626793_opts[e.index];
                nulo = null;
                e.source.removeEventListener("click", arguments.callee);
            });
            ID_1096626793.show();
            if (false) console.log('los dias no existen', {});
        } else if ((_.isObject($documentos.dias) || _.isString($documentos.dias)) && _.isEmpty($documentos.dias)) {
            var ID_1393433961_opts = [L('x1518866076_traducir', 'Aceptar')];
            var ID_1393433961 = Ti.UI.createAlertDialog({
                title: L('x3237162386_traducir', 'Atencion'),
                message: L('x1870732722_traducir', 'Ingrese días de compromiso'),
                buttonNames: ID_1393433961_opts
            });
            ID_1393433961.addEventListener('click', function(e) {
                var nulo = ID_1393433961_opts[e.index];
                nulo = null;
                e.source.removeEventListener("click", arguments.callee);
            });
            ID_1393433961.show();
        } else {
            $.ID_1921077056.enviar({});
        }
    } else {
        $.ID_1921077056.enviar({});
    }
}

function si_ID_1333561605(e) {

    var evento = e;
    Alloy.Collections[$.documentos.config.adapter.collection_name].add($.documentos);
    $.documentos.save();
    Alloy.Collections[$.documentos.config.adapter.collection_name].fetch();
    Alloy.createController("firma_index", {}).getView().open();

}

function no_ID_1982968930(e) {

    var evento = e;

}

function Change_ID_1856855136(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    if (elemento == true || elemento == 'true') {
        /** 
         * Dependiendo del estado del switch, actualizamos el estado del documento 
         */
        $.documentos.set({
            doc1: 1
        });
        if ('documentos' in $) $documentos = $.documentos.toJSON();
    } else {
        $.documentos.set({
            doc1: 0
        });
        if ('documentos' in $) $documentos = $.documentos.toJSON();
    }
    elemento = null;

}

function Change_ID_534984469(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    if (elemento == true || elemento == 'true') {
        $.documentos.set({
            doc2: 1
        });
        if ('documentos' in $) $documentos = $.documentos.toJSON();
    } else {
        $.documentos.set({
            doc2: 0
        });
        if ('documentos' in $) $documentos = $.documentos.toJSON();
    }
    elemento = null;

}

function Change_ID_753425820(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    if (elemento == true || elemento == 'true') {
        $.documentos.set({
            doc3: 1
        });
        if ('documentos' in $) $documentos = $.documentos.toJSON();
    } else {
        $.documentos.set({
            doc3: 0
        });
        if ('documentos' in $) $documentos = $.documentos.toJSON();
    }
    elemento = null;

}

function Change_ID_71890785(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    if (elemento == true || elemento == 'true') {
        $.documentos.set({
            doc4: 1
        });
        if ('documentos' in $) $documentos = $.documentos.toJSON();
    } else {
        $.documentos.set({
            doc4: 0
        });
        if ('documentos' in $) $documentos = $.documentos.toJSON();
    }
    elemento = null;

}

function Change_ID_1965912974(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    if (elemento == true || elemento == 'true') {
        $.documentos.set({
            doc5: 1
        });
        if ('documentos' in $) $documentos = $.documentos.toJSON();
    } else {
        $.documentos.set({
            doc5: 0
        });
        if ('documentos' in $) $documentos = $.documentos.toJSON();
    }
    elemento = null;

}

function Change_ID_525528314(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    if (elemento == true || elemento == 'true') {
        $.documentos.set({
            doc6: 1
        });
        if ('documentos' in $) $documentos = $.documentos.toJSON();
    } else {
        $.documentos.set({
            doc6: 0
        });
        if ('documentos' in $) $documentos = $.documentos.toJSON();
    }
    elemento = null;

}

function Change_ID_1996805994(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    if (elemento == true || elemento == 'true') {
        $.documentos.set({
            doc7: 1
        });
        if ('documentos' in $) $documentos = $.documentos.toJSON();
    } else {
        $.documentos.set({
            doc7: 0
        });
        if ('documentos' in $) $documentos = $.documentos.toJSON();
    }
    elemento = null;

}

function Change_ID_1729341498(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    if (elemento == true || elemento == 'true') {
        $.documentos.set({
            doc8: 1
        });
        if ('documentos' in $) $documentos = $.documentos.toJSON();
    } else {
        $.documentos.set({
            doc8: 0
        });
        if ('documentos' in $) $documentos = $.documentos.toJSON();
    }
    elemento = null;

}

function Change_ID_243127124(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    if (elemento == true || elemento == 'true') {
        $.documentos.set({
            doc9: 1
        });
        if ('documentos' in $) $documentos = $.documentos.toJSON();
    } else {
        $.documentos.set({
            doc9: 0
        });
        if ('documentos' in $) $documentos = $.documentos.toJSON();
    }
    elemento = null;

}

function Change_ID_1106870128(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    if (elemento == true || elemento == 'true') {
        /** 
         * Actualizamos doc10 
         */
        $.documentos.set({
            doc10: 1
        });
        if ('documentos' in $) $documentos = $.documentos.toJSON();
        /** 
         * Actualizamos edificio 
         */
        $.documentos.set({
            edificio: 1
        });
        if ('documentos' in $) $documentos = $.documentos.toJSON();
        require('vars')[_var_scopekey]['s1'] = L('x2212294583', '1');
    } else {
        $.documentos.set({
            edificio: 0
        });
        if ('documentos' in $) $documentos = $.documentos.toJSON();
        require('vars')[_var_scopekey]['s1'] = L('x4108050209', '0');
        /** 
         * Recuperamos variable s2 para saber si el doc10 debe estar en 0 o 1 
         */
        var s2 = ('s2' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['s2'] : '';
        if (s2 == 0 || s2 == '0') {
            $.documentos.set({
                doc10: 0
            });
            if ('documentos' in $) $documentos = $.documentos.toJSON();
        }
    }
    elemento = null;

}

function Change_ID_1880893238(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    if (elemento == true || elemento == 'true') {
        /** 
         * Actualizamos el doc10 
         */
        $.documentos.set({
            doc10: 1
        });
        if ('documentos' in $) $documentos = $.documentos.toJSON();
        /** 
         * Actualizamos los contenidos 
         */
        $.documentos.set({
            contenidos: 1
        });
        if ('documentos' in $) $documentos = $.documentos.toJSON();
        require('vars')[_var_scopekey]['s2'] = L('x2212294583', '1');
    } else {
        $.documentos.set({
            contenidos: 0
        });
        if ('documentos' in $) $documentos = $.documentos.toJSON();
        require('vars')[_var_scopekey]['s2'] = L('x4108050209', '0');
        /** 
         * Recuperamos variable s1 para saber si el doc10 debe estar en 0 o 1 
         */
        var s1 = ('s1' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['s1'] : '';
        if (s1 == 0 || s1 == '0') {
            $.documentos.set({
                doc10: 0
            });
            if ('documentos' in $) $documentos = $.documentos.toJSON();
        }
    }
    elemento = null;

}

function Change_ID_1432956192(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    $.documentos.set({
        otros: elemento
    });
    if ('documentos' in $) $documentos = $.documentos.toJSON();
    elemento = null, source = null;

}

function Change_ID_503279996(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    /** 
     * Actualizamos la tabla con los dias para enviar los documentos 
     */
    $.documentos.set({
        dias: elemento
    });
    if ('documentos' in $) $documentos = $.documentos.toJSON();
    elemento = null, source = null;

}

function Return_ID_286897182(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    /** 
     * Al presionar enter en el teclado del equipo, desenfocamos la caja 
     */
    $.ID_744186086.blur();
    elemento = null, source = null;

}

function Click_ID_1587934315(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var evento = e;
    /** 
     * Desenfocamos las cajas de texto 
     */
    $.ID_1807021265.blur();
    $.ID_744186086.blur();

}

function Postlayout_ID_1559231534(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    $.ID_1308881787.show();

}

function Postlayout_ID_2124089436(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var anteriores = ('anteriores' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['anteriores'] : '';
    if (anteriores == false || anteriores == 'false') {
        /** 
         * Cerramos pantalla contenidos 
         */
        Alloy.Events.trigger('_cerrar_insp', {
            pantalla: L('x1805186499_traducir', 'contenidos')
        });
        require('vars')[_var_scopekey]['anteriores'] = L('x4261170317', 'true');
    }

}

var ID_1160550620_opts = [L('x3481285698_traducir', 'Asegurado no puede seguir'), L('x3077268071_traducir', ' Se me acabo la bateria'), L('x479989047_traducir', ' Tuve un accidente'), L('x2376009830_traducir', ' Cancelar')];
var ID_1160550620 = Ti.UI.createOptionDialog({
    title: L('x1670176792_traducir', 'RAZON PARA CANCELAR INSPECCION ACTUAL'),
    options: ID_1160550620_opts
});
ID_1160550620.addEventListener('click', function(e) {
    var resp = ID_1160550620_opts[e.index];
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
    if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
        var razon = "";
        if (resp == L('x3481285698_traducir', 'Asegurado no puede seguir')) {
            razon = "Asegurado no puede seguir";
        }
        if (resp == L('x1862794075_traducir', 'Se me acabo la bateria')) {
            razon = "Se me acabo la bateria";
        }
        if (resp == L('x4012805111_traducir', 'Tuve un accidente')) {
            razon = "Tuve un accidente";
        }
        if (false) console.log('mi razon es', {
            "datos": razon
        });
        if ((_.isObject(razon) || (_.isString(razon)) && !_.isEmpty(razon)) || _.isNumber(razon) || _.isBoolean(razon)) {
            if (false) console.log('llamando servicio cancelarTarea', {});
            require('vars')['insp_cancelada'] = L('x2941610362_traducir', 'siniestro');
            Alloy.Collections[$.datos.config.adapter.collection_name].add($.datos);
            $.datos.save();
            Alloy.Collections[$.datos.config.adapter.collection_name].fetch();
            var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
            var ID_1624731232_visible = true;

            if (ID_1624731232_visible == 'si') {
                ID_1624731232_visible = true;
            } else if (ID_1624731232_visible == 'no') {
                ID_1624731232_visible = false;
            }
            $.ID_1624731232.setVisible(ID_1624731232_visible);

            var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
            var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
            if (false) console.log('detalle de seltarea', {
                "data": seltarea
            });
            var datos = {
                id_inspector: inspector.id_server,
                codigo_identificador: inspector.codigo_identificador,
                id_server: seltarea.id_server,
                num_caso: seltarea.num_caso,
                razon: razon
            };
            require('vars')[_var_scopekey]['datos'] = datos;
            var ID_2113401140 = {};
            ID_2113401140.success = function(e) {
                var elemento = e,
                    valor = e;
                var ID_1624731232_visible = false;

                if (ID_1624731232_visible == 'si') {
                    ID_1624731232_visible = true;
                } else if (ID_1624731232_visible == 'no') {
                    ID_1624731232_visible = false;
                }
                $.ID_1624731232.setVisible(ID_1624731232_visible);

                Alloy.createController("firma_index", {}).getView().open();
                var ID_1174066682_func = function() {
                    /** 
                     * Cerramos pantalla datos basicos 
                     */
                    Alloy.Events.trigger('_cerrar_insp', {
                        pantalla: L('x3294048930_traducir', 'documentos')
                    });
                };
                var ID_1174066682 = setTimeout(ID_1174066682_func, 1000 * 0.5);
                elemento = null, valor = null;
            };
            ID_2113401140.error = function(e) {
                var elemento = e,
                    valor = e;
                if (false) console.log('hubo error llamando cancelarTarea, la agregamos a cola de salida', {
                    "elemento": elemento
                });
                if (false) console.log('agregando servicio cancelarTarea a cola', {});
                var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
                var datos = ('datos' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['datos'] : '';
                var ID_1353258075_m = Alloy.Collections.cola;
                var ID_1353258075_fila = Alloy.createModel('cola', {
                    data: JSON.stringify(datos),
                    id_tarea: seltarea.id_server,
                    tipo: 'cancelar'
                });
                ID_1353258075_m.add(ID_1353258075_fila);
                ID_1353258075_fila.save();
                var ID_1624731232_visible = false;

                if (ID_1624731232_visible == 'si') {
                    ID_1624731232_visible = true;
                } else if (ID_1624731232_visible == 'no') {
                    ID_1624731232_visible = false;
                }
                $.ID_1624731232.setVisible(ID_1624731232_visible);

                Alloy.createController("firma_index", {}).getView().open();
                /** 
                 * Cerramos pantalla datos basicos 
                 */
                Alloy.Events.trigger('_cerrar_insp', {
                    pantalla: L('x3294048930_traducir', 'documentos')
                });
                elemento = null, valor = null;
            };
            require('helper').ajaxUnico('ID_2113401140', '' + String.format(L('x3244141284', '%1$scancelarTarea'), url_server.toString()) + '', 'POST', {
                id_inspector: seltarea.id_inspector,
                codigo_identificador: inspector.codigo_identificador,
                id_tarea: seltarea.id_server,
                num_caso: seltarea.num_caso,
                mensaje: razon,
                opcion: 0,
                tipo: 1
            }, 15000, ID_2113401140);
        }
    }
    resp = null;
});
/** 
 * Cerramos esta pantalla cuando la firma se ha ejecutado 
 */
_my_events['_cerrar_insp,ID_1803619828'] = function(evento) {
    if (!_.isUndefined(evento.pantalla)) {
        if (evento.pantalla == L('x515385654_traducir', 'documentos')) {
            if (false) console.log('debug cerrando documentos', {});
            var ID_1274846699_trycatch = {
                error: function(e) {
                    if (false) console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
                }
            };
            try {
                ID_1274846699_trycatch.error = function(evento) {
                    if (false) console.log('error cerrando documentos', {});
                };
                $.ID_2036699456.close();
            } catch (e) {
                ID_1274846699_trycatch.error(e);
            }
        }
    } else {
        if (false) console.log('debug cerrando (todas) documentos', {});
        var ID_1176043715_trycatch = {
            error: function(e) {
                if (false) console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
            }
        };
        try {
            ID_1176043715_trycatch.error = function(evento) {
                if (false) console.log('error cerrando documentos', {});
            };
            $.ID_2036699456.close();
        } catch (e) {
            ID_1176043715_trycatch.error(e);
        }
    }
};
Alloy.Events.on('_cerrar_insp', _my_events['_cerrar_insp,ID_1803619828']);
var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
/** 
 * Inicializamos la tabla con datos default 
 */
$.documentos.set({
    id_inspeccion: seltarea.id_server,
    doc10: 0,
    doc6: 0,
    doc4: 0,
    doc7: 0,
    doc3: 0,
    edificio: 0,
    doc9: 0,
    dias: '',
    doc1: 0,
    doc5: 0,
    contenidos: 0,
    otros: '',
    doc2: 0,
    doc8: 0
});
if ('documentos' in $) $documentos = $.documentos.toJSON();
/** 
 * Inicializamos flag para determinar si el doc10 tiene que estar en 0 o 1 
 */
require('vars')[_var_scopekey]['s1'] = L('x4108050209', '0');
require('vars')[_var_scopekey]['s2'] = L('x4108050209', '0');
/** 
 * Avisamos que la inspeccion no fue cancelada 
 */
require('vars')['insp_cancelada'] = '';
require('vars')[_var_scopekey]['anteriores'] = L('x734881840_traducir', 'false');

if (OS_IOS || OS_ANDROID) {
    $.ID_2036699456.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}
//$.ID_2036699456.open();