var _bind4section = {};
var _list_templates = {};

var _activity;
if (OS_ANDROID) {
    _activity = $.ID_1560093094.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_1560093094';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_1560093094_window.addEventListener('open', function(e) {
        abx.setStatusbarColor("#FFFFFF");
        abx.setBackgroundColor("#ffffff");
    });
}

function Touchstart_ID_572988685(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    $.ID_1910964691.setColor('#ff0033');


}

function Touchend_ID_1357970929(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    $.ID_1910964691.setColor('#2d9edb');

    if (false) console.log('cerrando tomartarea', {});
    $.ID_1560093094.close();

}

$.ID_658148125.init({
    __id: 'ALL658148125',
    onlisto: Listo_ID_557189579,
    label_a: L('x3904355907_traducir', 'a'),
    onerror: Error_ID_934220026,
    _bono: L('x2764662954_traducir', 'Bono adicional:'),
    bono: L('', ''),
    ondatos: Datos_ID_1762014414
});

function Listo_ID_557189579(e) {

    var evento = e;
    if (false) console.log('widget mapa ha llamado evento \'listo\'', {});

}

function Error_ID_934220026(e) {

    var evento = e;
    if (false) console.log('ha ocurrido un error con el mapa', {
        "evento": evento
    });
    var ID_634942182_opts = [L('x1518866076_traducir', 'Aceptar')];
    var ID_634942182 = Ti.UI.createAlertDialog({
        title: L('x57652245_traducir', 'Alerta'),
        message: L('x1774080321_traducir', 'Ha ocurrido un error con el mapa'),
        buttonNames: ID_634942182_opts
    });
    ID_634942182.addEventListener('click', function(e) {
        var x = ID_634942182_opts[e.index];
        x = null;

        e.source.removeEventListener("click", arguments.callee);
    });
    ID_634942182.show();

}

function Datos_ID_1762014414(e) {

    var evento = e;
    if (false) console.log(String.format(L('x3211431303_traducir', 'datos recibidos desde widget mapa: %1$s'), evento.ruta_distancia.toString()), {
        "evento": evento
    });

}

$.ID_886082434.init({
    __id: 'ALL886082434',
    texto: L('x990554165_traducir', 'PRESIONE PARA VERIFICAR'),
    vertexto: true,
    verprogreso: false,
    onpresiono: Presiono_ID_1016935004
});

function Presiono_ID_1016935004(e) {

    var evento = e;

    $.ID_886082434.datos({
        verprogreso: true,
        vertexto: false
    });
    if (evento.estado == 1 || evento.estado == '1') {
        /** 
         * Llamamos servicios obtenerMisTareas al aceptarTarea para reordenar posici&#243;n seg&#250;n distancias y tener rutas correctas. 
         */
        $.ID_1560093094.close();
        /** 
         * Necesitamos saber la posicion del inspector para poder avisarle al servidor que vamos a tomar una tarea 
         */
        var _ifunc_res_ID_514536379 = function(e) {
            if (e.error) {
                var posicion = {
                    error: true,
                    latitude: -1,
                    longitude: -1,
                    reason: (e.reason) ? e.reason : 'unknown',
                    accuracy: 0,
                    speed: 0,
                    error_compass: false
                };
            } else {
                var posicion = e;
            }
            if (posicion.error == true || posicion.error == 'true') {
                var ID_1582610104_opts = [L('x1518866076_traducir', 'Aceptar')];
                var ID_1582610104 = Ti.UI.createAlertDialog({
                    title: L('x3237162386_traducir', 'Atencion'),
                    message: L('x3873304135_traducir', 'No se pudo obtener la ubicación'),
                    buttonNames: ID_1582610104_opts
                });
                ID_1582610104.addEventListener('click', function(e) {
                    var suu = ID_1582610104_opts[e.index];
                    suu = null;

                    e.source.removeEventListener("click", arguments.callee);
                });
                ID_1582610104.show();
            } else {
                if (Ti.Network.networkType == Ti.Network.NETWORK_NONE) {
                    var ID_1249812917_opts = [L('x1518866076_traducir', 'Aceptar')];
                    var ID_1249812917 = Ti.UI.createAlertDialog({
                        title: L('x3237162386_traducir', 'Atencion'),
                        message: L('x78115213_traducir', 'No tiene internet, por favor conectese a una red para realizar esta accion.'),
                        buttonNames: ID_1249812917_opts
                    });
                    ID_1249812917.addEventListener('click', function(e) {
                        var suu = ID_1249812917_opts[e.index];
                        suu = null;

                        e.source.removeEventListener("click", arguments.callee);
                    });
                    ID_1249812917.show();
                } else {
                    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
                    var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
                    var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
                    if (false) console.log('consultando servicio aceptarTarea', {
                        "inspector": inspector,
                        "seltarea": seltarea
                    });
                    var ID_875148326 = {};

                    ID_875148326.success = function(e) {
                        var elemento = e,
                            valor = e;
                        if (false) console.log('estoy en success pero fuera de la condicion', {
                            "datos": elemento
                        });
                        if (elemento.error == 0 || elemento.error == '0') {

                            $.ID_886082434.datos({
                                verprogreso: false,
                                vertexto: true
                            });

                            $.ID_886082434.datos({
                                verprogreso: false,
                                vertexto: true,
                                texto: L('x2695983055_traducir', 'PRESIONE PARA ACEPTAR'),
                                estilo: 'fondoverde',
                                estado: 1
                            });
                            var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
                            var ID_528553300_i = Alloy.Collections.tareas_entrantes;
                            var sql = 'DELETE FROM ' + ID_528553300_i.config.adapter.collection_name + ' WHERE id_server=\'' + seltarea.id_server + '\'';
                            var db = Ti.Database.open(ID_528553300_i.config.adapter.db_name);
                            db.execute(sql);
                            db.close();
                            ID_528553300_i.trigger('remove');
                            var ID_1918940496_i = Alloy.Collections.emergencia;
                            var sql = 'DELETE FROM ' + ID_1918940496_i.config.adapter.collection_name + ' WHERE id_server=\'' + seltarea.id_server + '\'';
                            var db = Ti.Database.open(ID_1918940496_i.config.adapter.db_name);
                            db.execute(sql);
                            db.close();
                            ID_1918940496_i.trigger('remove');
                            if (_.isNull(seltarea.fecha_tarea)) {
                                if (false) console.log('Agregando fecha', {});
                                var moment = require('alloy/moment');
                                var fecha_hoy = moment(new Date()).format('YYYY-MM-DD');
                                var seltarea = _.extend(seltarea, {
                                    fecha_tarea: fecha_hoy
                                });
                            }
                            var ID_1696588353_m = Alloy.Collections.tareas;
                            var ID_1696588353_fila = Alloy.createModel('tareas', {
                                fecha_tarea: seltarea.fecha_tarea,
                                id_inspeccion: seltarea.id_inspeccion,
                                nivel_2: seltarea.nivel_2,
                                id_asegurado: seltarea.id_asegurado,
                                comentario_can_o_rech: seltarea.comentario_can_o_rech,
                                estado_tarea: seltarea.estado_tarea,
                                bono: seltarea.bono,
                                id_inspector: seltarea.id_inspector,
                                lat: seltarea.lat,
                                nivel_1: seltarea.nivel_1,
                                pais: seltarea.pais,
                                direccion: seltarea.direccion,
                                asegurador: seltarea.asegurador,
                                fecha_ingreso: seltarea.fecha_ingreso,
                                fecha_siniestro: seltarea.fecha_siniestro,
                                nivel_1_texto: seltarea.nivel_1_texto,
                                distance: seltarea.distance,
                                nivel_4: seltarea.nivel_4,
                                id_server: seltarea.id_server,
                                pais_texto: seltarea.pais_texto,
                                categoria: seltarea.categoria,
                                nivel_3: seltarea.nivel_3,
                                num_caso: seltarea.num_caso,
                                lon: seltarea.lon,
                                tipo_tarea: seltarea.tipo_tarea,
                                nivel_5: seltarea.nivel_5
                            });
                            ID_1696588353_m.add(ID_1696588353_fila);
                            ID_1696588353_fila.save();
                            var ID_720251023_func = function() {

                                Alloy.Events.trigger('_refrescar_tareas');
                            };
                            var ID_720251023 = setTimeout(ID_720251023_func, 1000 * 0.2);
                        } else {
                            if (false) console.log('estoy en la otra condicion', {});
                            var ID_584509628_opts = [L('x1518866076_traducir', 'Aceptar')];
                            var ID_584509628 = Ti.UI.createAlertDialog({
                                title: '' + String.format(L('x921749426_traducir', 'Error %1$s'), elemento.error.toString()) + '',
                                message: '' + elemento.mensaje + '',
                                buttonNames: ID_584509628_opts
                            });
                            ID_584509628.addEventListener('click', function(e) {
                                var suu = ID_584509628_opts[e.index];
                                suu = null;

                                e.source.removeEventListener("click", arguments.callee);
                            });
                            ID_584509628.show();
                        }
                        elemento = null, valor = null;
                    };

                    ID_875148326.error = function(e) {
                        var elemento = e,
                            valor = e;
                        if (false) console.log('Respuesta servicio', {
                            "datos": elemento
                        });
                        var ID_560916429_opts = [L('x1518866076_traducir', 'Aceptar')];
                        var ID_560916429 = Ti.UI.createAlertDialog({
                            title: L('x2619118453_traducir', 'Error'),
                            message: L('x41636219_traducir', 'Error al conectarse al servidor'),
                            buttonNames: ID_560916429_opts
                        });
                        ID_560916429.addEventListener('click', function(e) {
                            var suu = ID_560916429_opts[e.index];
                            suu = null;

                            e.source.removeEventListener("click", arguments.callee);
                        });
                        ID_560916429.show();

                        $.ID_886082434.datos({
                            verprogreso: false,
                            vertexto: true
                        });
                        elemento = null, valor = null;
                    };
                    require('helper').ajaxUnico('ID_875148326', '' + String.format(L('x1051250956', '%1$saceptarTarea'), url_server.toString()) + '', 'POST', {
                        id_inspector: inspector.id_server,
                        codigo_identificador: inspector.codigo_identificador,
                        id_tarea: seltarea.id_server,
                        num_caso: seltarea.num_caso,
                        categoria: seltarea.categoria,
                        tipo_tarea: seltarea.tipo_tarea
                    }, 15000, ID_875148326);
                }
            }
        };
        Ti.Geolocation.setAccuracy(Ti.Geolocation.ACCURACY_HIGH);
        if (Ti.Geolocation.hasLocationPermissions(Ti.Geolocation.AUTHORIZATION_WHEN_IN_USE)) {
            Ti.Geolocation.getCurrentPosition(function(ee) {
                if (Ti.Geolocation.getHasCompass() == false) {
                    var pposicion = ('coords' in ee) ? ee.coords : {};
                    pposicion.error = ('coords' in ee) ? false : true;
                    pposicion.reason = '';
                    pposicion.speed = 0;
                    pposicion.compass = -1;
                    pposicion.compass_accuracy = -1;
                    pposicion.error_compass = true;
                    _ifunc_res_ID_514536379(pposicion);
                } else {
                    Ti.Geolocation.getCurrentHeading(function(yy) {
                        var pposicion = ('coords' in ee) ? ee.coords : {};
                        pposicion.error = ('coords' in ee) ? false : true;
                        pposicion.reason = ('error' in ee) ? ee.error : '';
                        if (yy.error) {
                            pposicion.error_compass = true;
                        } else {
                            pposicion.compass = yy.heading;
                            pposicion.compass_accuracy = yy.heading.accuracy;
                        }
                        _ifunc_res_ID_514536379(pposicion);
                    });
                }
            });
        } else {
            Ti.Geolocation.requestLocationPermissions(Ti.Geolocation.AUTHORIZATION_WHEN_IN_USE, function(u) {
                if (u.success) {
                    Ti.Geolocation.getCurrentPosition(function(ee) {
                        if (Ti.Geolocation.getHasCompass() == false) {
                            var pposicion = ('coords' in ee) ? ee.coords : {};
                            pposicion.error = ('coords' in ee) ? false : true;
                            pposicion.reason = '';
                            pposicion.speed = 0;
                            pposicion.compass = -1;
                            pposicion.compass_accuracy = -1;
                            pposicion.error_compass = true;
                            _ifunc_res_ID_514536379(pposicion);
                        } else {
                            Ti.Geolocation.getCurrentHeading(function(yy) {
                                var pposicion = ('coords' in ee) ? ee.coords : {};
                                pposicion.error = ('coords' in ee) ? false : true;
                                pposicion.reason = '';
                                if (yy.error) {
                                    pposicion.error_compass = true;
                                } else {
                                    pposicion.compass = yy.heading;
                                    pposicion.compass_accuracy = yy.heading.accuracy;
                                }
                                _ifunc_res_ID_514536379(pposicion);
                            });
                        }
                    });
                } else {
                    _ifunc_res_ID_514536379({
                        error: true,
                        latitude: -1,
                        longitude: -1,
                        reason: 'permission_denied',
                        accuracy: 0,
                        speed: 0,
                        error_compass: false
                    });
                }
            });
        }
    } else if (evento.estado == 2) {
        var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
        var gps_error = ('gps_error' in require('vars')) ? require('vars')['gps_error'] : '';
        var gps_latitud = ('gps_latitud' in require('vars')) ? require('vars')['gps_latitud'] : '';
        var gps_longitud = ('gps_longitud' in require('vars')) ? require('vars')['gps_longitud'] : '';
        var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
        var ID_2070551769_func = function() {

            Alloy.Events.trigger('_refrescar_tareas');
        };
        var ID_2070551769 = setTimeout(ID_2070551769_func, 1000 * 0.2);
        $.ID_1560093094.close();
    } else {
        var _ifunc_res_ID_1305405132 = function(e) {
            if (e.error) {
                var posicion = {
                    error: true,
                    latitude: -1,
                    longitude: -1,
                    reason: (e.reason) ? e.reason : 'unknown',
                    accuracy: 0,
                    speed: 0,
                    error_compass: false
                };
            } else {
                var posicion = e;
            }
            if (posicion.error == true || posicion.error == 'true') {
                /** 
                 * Si se puede obtener la posicion gps, hacemos una consulta al servidor para ver la disponibilidad de tarea, caso contrario avisamos al inspector que no se pudo obtener la ubicacion 
                 */
                var ID_1960458123_opts = [L('x1518866076_traducir', 'Aceptar')];
                var ID_1960458123 = Ti.UI.createAlertDialog({
                    title: L('x57652245_traducir', 'Alerta'),
                    message: L('x3873304135_traducir', 'No se pudo obtener la ubicación'),
                    buttonNames: ID_1960458123_opts
                });
                ID_1960458123.addEventListener('click', function(e) {
                    var suu = ID_1960458123_opts[e.index];
                    suu = null;

                    e.source.removeEventListener("click", arguments.callee);
                });
                ID_1960458123.show();
            } else {
                if (Ti.Network.networkType == Ti.Network.NETWORK_NONE) {
                    var ID_1347206068_opts = [L('x1518866076_traducir', 'Aceptar'), L('x2376009830_traducir', ' Cancelar')];
                    var ID_1347206068 = Ti.UI.createAlertDialog({
                        title: L('x57652245_traducir', 'Alerta'),
                        message: L('x78115213_traducir', 'No tiene internet, por favor conectese a una red para realizar esta accion.'),
                        buttonNames: ID_1347206068_opts
                    });
                    ID_1347206068.addEventListener('click', function(e) {
                        var suu = ID_1347206068_opts[e.index];
                        suu = null;

                        e.source.removeEventListener("click", arguments.callee);
                    });
                    ID_1347206068.show();
                } else {
                    /** 
                     * Recuperamos variable 
                     */
                    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
                    var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
                    if (false) console.log('llamando disponiblidad de tarea', {});
                    var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
                    /** 
                     * Consultamos al servidor si el inspector tiene disponibilidad para tomar la tarea. 
                     */
                    var ID_43439598 = {};

                    ID_43439598.success = function(e) {
                        var elemento = e,
                            valor = e;
                        if (false) console.log('estoy en success pero fuera de la condicion', {
                            "datos": elemento
                        });
                        /** 
                         * Escondemos la animacion de progreso y mostramos texto en el boton 
                         */

                        $.ID_886082434.datos({
                            verprogreso: false,
                            vertexto: true
                        });
                        if (elemento.error == 0 || elemento.error == '0') {
                            /** 
                             * Si la respuesta del servidor es correcta (o sea, error 0). Si la disponibilidad es 0, no se puede tomar por exceso de tareas tomadas, disponibilidad es 1, la tarea es tomada y registrada en el servidor 
                             */
                            if (elemento.disponibilidad == 0 || elemento.disponibilidad == '0') {

                                $.ID_886082434.datos({
                                    verprogreso: false,
                                    vertexto: true,
                                    texto: L('x3287250645_traducir', 'CERRAR'),
                                    estilo: 'fondorojo',
                                    estado: 2
                                });
                                $.ID_179299802.setText('SUPERASTE TU TOMA MAXIMA DE TAREAS');


                                $.ID_859578408.update({
                                    texto: L('x1422333761_traducir', 'Tienes una capacidad maxima de tareas por dia pero aun puedes tomar en otros dias')
                                });
                            } else if (elemento.disponibilidad == 1 || elemento.disponibilidad == '1') {

                                $.ID_886082434.datos({
                                    verprogreso: false,
                                    vertexto: true
                                });
                                $.ID_179299802.setText('Quedara guardada en Mis Tareas');


                                $.ID_886082434.datos({
                                    verprogreso: false,
                                    vertexto: true,
                                    texto: L('x2695983055_traducir', 'PRESIONE PARA ACEPTAR'),
                                    estilo: 'fondoverde',
                                    estado: 1
                                });
                            }
                        } else {
                            var ID_1288759381_opts = [L('x1518866076_traducir', 'Aceptar')];
                            var ID_1288759381 = Ti.UI.createAlertDialog({
                                title: L('x2619118453_traducir', 'Error'),
                                message: L('x41636219_traducir', 'Error al conectarse al servidor'),
                                buttonNames: ID_1288759381_opts
                            });
                            ID_1288759381.addEventListener('click', function(e) {
                                var suu = ID_1288759381_opts[e.index];
                                suu = null;

                                e.source.removeEventListener("click", arguments.callee);
                            });
                            ID_1288759381.show();
                        }
                        elemento = null, valor = null;
                    };

                    ID_43439598.error = function(e) {
                        var elemento = e,
                            valor = e;
                        if (false) console.log('Respuesta servicio', {
                            "datos": elemento
                        });
                        var ID_752104211_opts = [L('x1518866076_traducir', 'Aceptar')];
                        var ID_752104211 = Ti.UI.createAlertDialog({
                            title: L('x2619118453_traducir', 'Error'),
                            message: L('x41636219_traducir', 'Error al conectarse al servidor'),
                            buttonNames: ID_752104211_opts
                        });
                        ID_752104211.addEventListener('click', function(e) {
                            var suu = ID_752104211_opts[e.index];
                            suu = null;

                            e.source.removeEventListener("click", arguments.callee);
                        });
                        ID_752104211.show();

                        $.ID_886082434.datos({
                            verprogreso: false,
                            vertexto: true
                        });
                        elemento = null, valor = null;
                    };
                    require('helper').ajaxUnico('ID_43439598', '' + String.format(L('x1968401862', '%1$sdispInspector'), url_server.toString()) + '', 'POST', {
                        id_inspector: inspector.id_server,
                        codigo_identificador: inspector.codigo_identificador,
                        id_tarea: seltarea.id_server,
                        num_caso: seltarea.num_caso
                    }, 15000, ID_43439598);
                }
            }
        };
        Ti.Geolocation.setAccuracy(Ti.Geolocation.ACCURACY_HIGH);
        if (Ti.Geolocation.hasLocationPermissions(Ti.Geolocation.AUTHORIZATION_WHEN_IN_USE)) {
            Ti.Geolocation.getCurrentPosition(function(ee) {
                if (Ti.Geolocation.getHasCompass() == false) {
                    var pposicion = ('coords' in ee) ? ee.coords : {};
                    pposicion.error = ('coords' in ee) ? false : true;
                    pposicion.reason = '';
                    pposicion.speed = 0;
                    pposicion.compass = -1;
                    pposicion.compass_accuracy = -1;
                    pposicion.error_compass = true;
                    _ifunc_res_ID_1305405132(pposicion);
                } else {
                    Ti.Geolocation.getCurrentHeading(function(yy) {
                        var pposicion = ('coords' in ee) ? ee.coords : {};
                        pposicion.error = ('coords' in ee) ? false : true;
                        pposicion.reason = ('error' in ee) ? ee.error : '';
                        if (yy.error) {
                            pposicion.error_compass = true;
                        } else {
                            pposicion.compass = yy.heading;
                            pposicion.compass_accuracy = yy.heading.accuracy;
                        }
                        _ifunc_res_ID_1305405132(pposicion);
                    });
                }
            });
        } else {
            Ti.Geolocation.requestLocationPermissions(Ti.Geolocation.AUTHORIZATION_WHEN_IN_USE, function(u) {
                if (u.success) {
                    Ti.Geolocation.getCurrentPosition(function(ee) {
                        if (Ti.Geolocation.getHasCompass() == false) {
                            var pposicion = ('coords' in ee) ? ee.coords : {};
                            pposicion.error = ('coords' in ee) ? false : true;
                            pposicion.reason = '';
                            pposicion.speed = 0;
                            pposicion.compass = -1;
                            pposicion.compass_accuracy = -1;
                            pposicion.error_compass = true;
                            _ifunc_res_ID_1305405132(pposicion);
                        } else {
                            Ti.Geolocation.getCurrentHeading(function(yy) {
                                var pposicion = ('coords' in ee) ? ee.coords : {};
                                pposicion.error = ('coords' in ee) ? false : true;
                                pposicion.reason = '';
                                if (yy.error) {
                                    pposicion.error_compass = true;
                                } else {
                                    pposicion.compass = yy.heading;
                                    pposicion.compass_accuracy = yy.heading.accuracy;
                                }
                                _ifunc_res_ID_1305405132(pposicion);
                            });
                        }
                    });
                } else {
                    _ifunc_res_ID_1305405132({
                        error: true,
                        latitude: -1,
                        longitude: -1,
                        reason: 'permission_denied',
                        accuracy: 0,
                        speed: 0,
                        error_compass: false
                    });
                }
            });
        }
    }
}

$.ID_859578408.init({
    __id: 'ALL859578408',
    texto: L('x2523383595_traducir', 'Tip: No aceptes tareas si no estás seguro que puedes llegar a tiempo'),
    top: 10,
    tipo: '_tip'
});


(function() {
    if (false) console.log('argumentos tomartarea', {
        "modelo": args
    });
    if (!_.isUndefined(args._tipo)) {
        /** 
         * Dependiendo de donde venga la tarea, guardamos una variable con el detalle de la tarea 
         */
        if (args._tipo == L('x2110366172_traducir', 'emergencias')) {
            var ID_954492358_i = Alloy.createCollection('emergencia');
            var ID_954492358_i_where = 'id=\'' + args._id + '\'';
            ID_954492358_i.fetch({
                query: 'SELECT * FROM emergencia WHERE id=\'' + args._id + '\''
            });
            var tareas = require('helper').query2array(ID_954492358_i);
            require('vars')['tareas'] = tareas;
        } else if (args._tipo == L('x2525272859_traducir', 'entrantes')) {
            var ID_622206359_i = Alloy.createCollection('tareas_entrantes');
            var ID_622206359_i_where = 'id=\'' + args._id + '\'';
            ID_622206359_i.fetch({
                query: 'SELECT * FROM tareas_entrantes WHERE id=\'' + args._id + '\''
            });
            var tareas = require('helper').query2array(ID_622206359_i);
            require('vars')['tareas'] = tareas;
        }
    }
    var tareas = ('tareas' in require('vars')) ? require('vars')['tareas'] : '';
    if (tareas && tareas.length) {
        /** 
         * Revisamos si tareas contiene datos 
         */
        if (false) console.log('datos de tarea consultados', {
            "tabla": tareas[0]
        });
        /** 
         * Guardamos en una variable los detalles de la tarea. Servira tambien para el resto de la inspeccion 
         */
        require('vars')['seltarea'] = tareas[0];
        if (false) console.log('adaptamos datos', {});
        /** 
         * Creamos estructura con datos de la tarea 
         */
        var info = {
            direccion: tareas[0].direccion,
            ciudad_pais: String.format(L('x1487588533', '%1$s, %2$s'), (tareas[0].nivel_2) ? tareas[0].nivel_2.toString() : '', (tareas[0].pais_texto) ? tareas[0].pais_texto.toString() : ''),
            ciudad: tareas[0].nivel_1,
            distancia: tareas[0].distance
        };
        if ((_.isObject(tareas[0].nivel_2) || _.isString(tareas[0].nivel_2)) && _.isEmpty(tareas[0].nivel_2)) {
            /** 
             * Agregamos el valor de la comuna como ultimo nivel 
             */
            var info = _.extend(info, {
                comuna: tareas[0].nivel_1
            });
        } else if ((_.isObject(tareas[0].nivel_3) || _.isString(tareas[0].nivel_3)) && _.isEmpty(tareas[0].nivel_3)) {
            var info = _.extend(info, {
                comuna: tareas[0].nivel_2
            });
        } else if ((_.isObject(tareas[0].nivel_4) || _.isString(tareas[0].nivel_4)) && _.isEmpty(tareas[0].nivel_4)) {
            var info = _.extend(info, {
                comuna: tareas[0].nivel_3
            });
        } else if ((_.isObject(tareas[0].nivel_5) || _.isString(tareas[0].nivel_5)) && _.isEmpty(tareas[0].nivel_5)) {
            var info = _.extend(info, {
                comuna: tareas[0].nivel_4
            });
        } else {
            var info = _.extend(info, {
                comuna: tareas[0].nivel_5
            });
        }
        if (false) console.log('actualizamos mapa con direccion', {});
        if (tareas[0].perfil == L('x2137349405_traducir', 'casa')) {
            /** 
             * Dependiendo del donde venga la tarea, desde la casa registrada o ubicacion gps se actualiza el widget del mapa 
             */
            var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';

            $.ID_658148125.update({
                tipo: 'origen',
                origen_latitud: inspector.lat_dir,
                origen_longitud: inspector.lon_dir,
                destino_latitud: tareas[0].lat,
                destino_longitud: tareas[0].lon,
                ruta: true,
                direccion: info.direccion,
                comuna: info.comuna,
                ciudad: info.ciudad_pais,
                distancia: info.distancia
            });
        } else {

            $.ID_658148125.update({
                tipo: 'ubicacion',
                latitud: tareas[0].lat,
                longitud: tareas[0].lon,
                ruta: true,
                direccion: info.direccion,
                comuna: info.comuna,
                ciudad: info.ciudad_pais,
                distancia: info.distancia
            });
        }
        /** 
         * transforma valor decimal de bono a numero entero al dividirlo por 1. 
         */
        tareas[0].bono = (tareas[0].bono / 1);
        if (tareas[0].bono != 0.0 && tareas[0].bono != '0.0') {
            /** 
             * Actualizamos el widget de los bonos en caso de que exista algo 
             */

            $.ID_658148125.update({
                bono: tareas[0].bono
            });
        } else {

            $.ID_658148125.update({
                bono: ''
            });
        }
    }
})();

function Postlayout_ID_1156648359(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var ID_1294585966_func = function() {
        /** 
         * Una vez ya cargada la tarea, dejamos la variable limpia para poder abrir otras pantallas 
         */
        require('vars')['var_abriendo'] = '';
    };
    var ID_1294585966 = setTimeout(ID_1294585966_func, 1000 * 0.2);

}
if (OS_IOS || OS_ANDROID) {
    $.ID_1560093094.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}
//$.ID_1560093094.open();