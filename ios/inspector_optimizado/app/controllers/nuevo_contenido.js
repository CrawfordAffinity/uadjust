var _bind4section={};
var _list_templates={};
var $contenido = $.contenido.toJSON();

$.ID_1771733970_window.setTitleAttributes({
color : 'WHITE'
}
);
var _activity; 
if (OS_ANDROID) { _activity = $.ID_1771733970.activity; var abx = require('com.alcoapps.actionbarextras'); }
var _my_events = {}, _out_vars = {}, $item = {}, args = arguments[0] || {}; if ('__args' in args && '__id' in args['__args']) args.__id=args['__args'].__id; if ('__modelo' in args && _.keys(args.__modelo).length>0 && 'item' in $) { $.item.set(args.__modelo); $item = $.item.toJSON(); }
var _var_scopekey = 'ID_1771733970';
require('vars')[_var_scopekey]={};
if (OS_ANDROID) {
   $.ID_1771733970_window.addEventListener('open', function(e) {
   abx.setStatusbarColor("#FFFFFF");
   abx.setBackgroundColor("#8bc9e8");
   });
}

function Click_ID_1030292705(e) {

e.cancelBubble=true;
var elemento=e.source;
/** 
* Limpiamos widgets multiples (memoria ram) 
*/
$.ID_2107155773.limpiar({});
$.ID_1470293663.limpiar({});
$.ID_1996770915.limpiar({});
$.ID_1771733970.close();

}
function Click_ID_1768639277(e) {

e.cancelBubble=true;
var elemento=e.source;
/** 
* Obtenemos la fecha actual 
*/
 
var hoy = new Date();
if ($contenido.fecha_compra > hoy==true||$contenido.fecha_compra > hoy=='true') {
/** 
* Validamos que los campos ingresados sean correctos 
*/
var ID_1492161658_opts=[L('x1518866076_traducir','Aceptar')];
var ID_1492161658 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x2327084729_traducir','La fecha de compra no puede ser superior a hoy'),
   buttonNames: ID_1492161658_opts
});
ID_1492161658.addEventListener('click', function(e) {
   var nulo=ID_1492161658_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_1492161658.show();
}
 else if (_.isUndefined($contenido.nombre)) {
var ID_1272791922_opts=[L('x1518866076_traducir','Aceptar')];
var ID_1272791922 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x2224804335_traducir','Seleccione producto'),
   buttonNames: ID_1272791922_opts
});
ID_1272791922.addEventListener('click', function(e) {
   var nulo=ID_1272791922_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_1272791922.show();
} else if (_.isUndefined($contenido.id_marca)) {
var ID_153336346_opts=[L('x1518866076_traducir','Aceptar')];
var ID_153336346 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x3747506986_traducir','Seleccione marca del producto'),
   buttonNames: ID_153336346_opts
});
ID_153336346.addEventListener('click', function(e) {
   var nulo=ID_153336346_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_153336346.show();
} else if (_.isUndefined($contenido.id_recinto)) {
var ID_693653122_opts=[L('x1518866076_traducir','Aceptar')];
var ID_693653122 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x2405532746_traducir','Seleccione el recinto donde estaba el producto'),
   buttonNames: ID_693653122_opts
});
ID_693653122.addEventListener('click', function(e) {
   var nulo=ID_693653122_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_693653122.show();
} else if (_.isUndefined($contenido.cantidad)) {
var ID_555055808_opts=[L('x1518866076_traducir','Aceptar')];
var ID_555055808 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x4257958740_traducir','Ingrese cantidad de productos'),
   buttonNames: ID_555055808_opts
});
ID_555055808.addEventListener('click', function(e) {
   var nulo=ID_555055808_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_555055808.show();
} else if ((_.isObject($contenido.cantidad) ||_.isString($contenido.cantidad)) &&  _.isEmpty($contenido.cantidad)) {
var ID_597377093_opts=[L('x1518866076_traducir','Aceptar')];
var ID_597377093 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x4257958740_traducir','Ingrese cantidad de productos'),
   buttonNames: ID_597377093_opts
});
ID_597377093.addEventListener('click', function(e) {
   var nulo=ID_597377093_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_597377093.show();
} else if (_.isUndefined($contenido.fecha_compra)) {
var ID_759817000_opts=[L('x1518866076_traducir','Aceptar')];
var ID_759817000 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x1056100694_traducir','Ingrese fecha de compra aproximada (año)'),
   buttonNames: ID_759817000_opts
});
ID_759817000.addEventListener('click', function(e) {
   var nulo=ID_759817000_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_759817000.show();
} else if (_.isUndefined($contenido.id_moneda)) {
var ID_1568938720_opts=[L('x1518866076_traducir','Aceptar')];
var ID_1568938720 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x2624129841_traducir','Seleccione el tipo de moneda del producto'),
   buttonNames: ID_1568938720_opts
});
ID_1568938720.addEventListener('click', function(e) {
   var nulo=ID_1568938720_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_1568938720.show();
} else if (_.isUndefined($contenido.valor)) {
var ID_1322743803_opts=[L('x1518866076_traducir','Aceptar')];
var ID_1322743803 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x1829790159_traducir','Ingrese valor del producto'),
   buttonNames: ID_1322743803_opts
});
ID_1322743803.addEventListener('click', function(e) {
   var nulo=ID_1322743803_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_1322743803.show();
} else if ((_.isObject($contenido.valor) ||_.isString($contenido.valor)) &&  _.isEmpty($contenido.valor)) {
var ID_5058228_opts=[L('x1518866076_traducir','Aceptar')];
var ID_5058228 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x1829790159_traducir','Ingrese valor del producto'),
   buttonNames: ID_5058228_opts
});
ID_5058228.addEventListener('click', function(e) {
   var nulo=ID_5058228_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_5058228.show();
} else if (_.isUndefined($contenido.descripcion)) {
var ID_15109800_opts=[L('x1518866076_traducir','Aceptar')];
var ID_15109800 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x417344283_traducir','Describa el producto brevemente, con un mínimo de 30 caracteres'),
   buttonNames: ID_15109800_opts
});
ID_15109800.addEventListener('click', function(e) {
   var nulo=ID_15109800_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_15109800.show();
} else if (_.isNumber($contenido.descripcion.length) && _.isNumber(29) && $contenido.descripcion.length <= 29) {
var ID_1758996172_opts=[L('x1518866076_traducir','Aceptar')];
var ID_1758996172 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x417344283_traducir','Describa el producto brevemente, con un mínimo de 30 caracteres'),
   buttonNames: ID_1758996172_opts
});
ID_1758996172.addEventListener('click', function(e) {
   var nulo=ID_1758996172_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_1758996172.show();
} else {
if (!_.isUndefined(args._dato)) {
/** 
* Si estamos editando, debemos borrar modelo previo. 
*/
var id_contenido = ('id_contenido' in require('vars')[_var_scopekey])?require('vars')[_var_scopekey]['id_contenido']:'';
var ID_1826424068_i=Alloy.Collections.insp_contenido;
var sql = 'DELETE FROM ' + ID_1826424068_i.config.adapter.collection_name + ' WHERE id=\''+id_contenido+'\'';
var db = Ti.Database.open(ID_1826424068_i.config.adapter.db_name);
db.execute(sql);
db.close();
ID_1826424068_i.trigger('delete');
}
/** 
* Guardamos modelo nuevo 
*/
Alloy.Collections[$.contenido.config.adapter.collection_name].add($.contenido);
$.contenido.save();
Alloy.Collections[$.contenido.config.adapter.collection_name].fetch();
/** 
* limpiamos widgets multiples (memoria ram) 
*/
$.ID_2107155773.limpiar({});
$.ID_1470293663.limpiar({});
$.ID_1996770915.limpiar({});
$.ID_1771733970.close();
}
}

$.ID_1585064981.init({
__id : 'ALL1585064981',
aceptar : L('x1518866076_traducir','Aceptar'),
cancelar : L('x2353348866_traducir','Cancelar'),
onaceptar : Aceptar_ID_1254289041,
oncancelar : Cancelar_ID_873546370
}
);

function Aceptar_ID_1254289041(e) {

var evento=e;
/** 
* Formateamos la fecha que responde el widget, mostramos en pantalla y actualizamos la fecha de compra 
*/
var moment = require('alloy/moment');
var ID_1410548628 = evento.valor;
var resp = moment(ID_1410548628).format('DD-MM-YYYY');
$.ID_1240322270.setText(resp);

var ID_1240322270_estilo = 'estilo12';

											var _tmp_a4w = require('a4w');
											if ((typeof ID_1240322270_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (ID_1240322270_estilo in _tmp_a4w.styles['classes'])) {
												try {
													ID_1240322270_estilo = _tmp_a4w.styles['classes'][ID_1240322270_estilo];
												} catch (st_val_err) {
												}
											}
											_tmp_a4w = null;
											$.ID_1240322270.applyProperties(ID_1240322270_estilo);

$.contenido.set({
fecha_compra : evento.valor
}
);
if ('contenido' in $) $contenido=$.contenido.toJSON();
if(false) console.log('FECHA SELECCIONADA',{"fecha":evento.valor});

}
function Cancelar_ID_873546370(e) {

var evento=e;
if(false) console.log('selector de fecha compra cancelado',{});

}
function Click_ID_847020488(e) {

e.cancelBubble=true;
var elemento=e.source;
Alloy.createController("listadocontenidos",{}).getView().open();

}

$.ID_2107155773.init({
titulo : L('x1867465554_traducir','MARCAS'),
cargando : L('x1740321226_traducir','cargando ..'),
__id : 'ALL2107155773',
left : 0,
onrespuesta : Respuesta_ID_1994295755,
campo : L('x3837495059_traducir','Marca del Item'),
onabrir : Abrir_ID_2125987014,
color : 'celeste',
right : 0,
top : 20,
seleccione : L('x1021431138_traducir','seleccione marca'),
activo : true,
onafterinit : Afterinit_ID_2082187664
}
);

function Afterinit_ID_2082187664(e) {

var evento=e;
var ID_1012106951_func = function() {
var seltarea = ('seltarea' in require('vars'))?require('vars')['seltarea']:'';
if (!_.isUndefined(args._dato)) {
/** 
* Si estamos editando contenido, cargamos dato previo seleccionado en base de datos. 
*/
var ID_271288324_i=Alloy.createCollection('insp_contenido');
var ID_271288324_i_where='id=\''+args._dato.id+'\'';
ID_271288324_i.fetch({ query: 'SELECT * FROM insp_contenido WHERE id=\''+args._dato.id+'\'' });
var insp_dato=require('helper').query2array(ID_271288324_i);
if (insp_dato && insp_dato.length) {
/** 
* obtenemos valor seleccionado previamente y lo mostramos en selector. 
*/
var ID_1024154925_i=Alloy.createCollection('marcas');
var ID_1024154925_i_where='id_segured=\''+insp_dato[0].id_marca+'\'';
ID_1024154925_i.fetch({ query: 'SELECT * FROM marcas WHERE id_segured=\''+insp_dato[0].id_marca+'\'' });
var marcas=require('helper').query2array(ID_1024154925_i);
if (marcas && marcas.length) {
var ID_81923934_func = function() {
$.ID_2107155773.labels({valor : marcas[0].nombre});
if(false) console.log('el nombre de la marca es',{"datos":marcas[0].nombre});
};
var ID_81923934 = setTimeout(ID_81923934_func, 1000*0.1);
}
}
}
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
var pais = ('pais' in require('vars'))?require('vars')['pais']:'';
var ID_1053778709_i=Alloy.createCollection('marcas');
var ID_1053778709_i_where='pais_texto=\''+pais[0].nombre+'\'';
ID_1053778709_i.fetch({ query: 'SELECT * FROM marcas WHERE pais_texto=\''+pais[0].nombre+'\'' });
var marcas=require('helper').query2array(ID_1053778709_i);
var datos=[];
_.each(marcas, function(fila, pos) {
  var new_row={};
  _.each(fila, function(x, llave) {
    var newkey='';
    if (llave=='nombre') newkey='valor';
    if (llave=='id_segured') newkey='id_interno';
    if (newkey!='') new_row[newkey] = fila[llave];
  });
  datos.push(new_row);
});
}
 else {
var ID_2140732469_i=Alloy.Collections.marcas;
var sql = "DELETE FROM " + ID_2140732469_i.config.adapter.collection_name;
var db = Ti.Database.open(ID_2140732469_i.config.adapter.db_name);
db.execute(sql);
db.close();
sql=null;
db=null;
ID_2140732469_i.trigger('delete');
var item_index = 0;
_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
item_index += 1;
var ID_1910323716_m=Alloy.Collections.marcas;
var ID_1910323716_fila = Alloy.createModel('marcas', {
nombre : String.format(L('x391662843_traducir','HomeDepot%1$s'), item.toString()),
id_server : String.format(L('x1290178835_traducir','0%1$s'), item.toString()),
id_segured : String.format(L('x1125471167_traducir','10%1$s'), item.toString()),
pais : 1
}
 );
ID_1910323716_m.add(ID_1910323716_fila);
ID_1910323716_fila.save();
});
var ID_1089667732_i=Alloy.createCollection('marcas');
ID_1089667732_i.fetch();
var ID_1089667732_src=require('helper').query2array(ID_1089667732_i);
var datos=[];
_.each(ID_1089667732_src, function(fila, pos) {
  var new_row={};
  _.each(fila, function(x, llave) {
    var newkey='';
    if (llave=='nombre') newkey='valor';
    if (llave=='id_segured') newkey='id_interno';
    if (newkey!='') new_row[newkey] = fila[llave];
  });
  datos.push(new_row);
});
}
$.ID_2107155773.data({data : datos});
};
var ID_1012106951 = setTimeout(ID_1012106951_func, 1000*0.2);

}
function Abrir_ID_2125987014(e) {

var evento=e;

}
function Respuesta_ID_1994295755(e) {

var evento=e;
$.ID_2107155773.labels({valor : evento.valor});
$.contenido.set({
id_marca : evento.item.id_interno
}
);
if ('contenido' in $) $contenido=$.contenido.toJSON();
if(false) console.log('datos recibidos de modal marca item',{"datos":evento});

}

$.ID_1066022242.init({
caja : 45,
__id : 'ALL1066022242',
onlisto : Listo_ID_1240649615,
onclick : Click_ID_1367021253
}
);

function Click_ID_1367021253(e) {

var evento=e;
/** 
* Capturamos foto 
*/
var ID_1528459684_result = function(e) {
   var foto_full = e.valor;
   var foto = e.valor.data;
if (foto_full.error==true||foto_full.error=='true') {
/** 
* Mostramos alerta indicando que ha ocurrido un error al sacar la foto 
*/
var ID_1934757973_opts=[L('x1518866076_traducir','Aceptar')];
var ID_1934757973 = Ti.UI.createAlertDialog({
   title: L('x57652245_traducir','Alerta'),
   message: L('x1068520423_traducir','Existe un problema con la camara'),
   buttonNames: ID_1934757973_opts
});
ID_1934757973.addEventListener('click', function(e) {
   var suu=ID_1934757973_opts[e.index];
suu = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_1934757973.show();
}
 else if (foto_full.cancel==true||foto_full.cancel=='true') {
if(false) console.log('cancele la foto',{});
$.ID_1066022242.detener({});
} else {
/** 
* Enviamos a procesar la foto al widget fotochica 
*/
$.ID_1066022242.procesar({imagen : foto,nueva : 640,calidad : 91});
 foto = null;
}};
function camara_ID_1528459684() {
   Ti.Media.showCamera({
      success: function(event) {
         if (event.mediaType==Ti.Media.MEDIA_TYPE_PHOTO) {
            ID_1528459684_result({ valor:{ error:false, cancel:false, data:event.media, reason:'' } }); 
         } else {
            ID_1528459684_result({ valor:{ error:true, cancel:false, data:'', reason:'not image' } }); 
         }
      },
      cancel: function() {
         ID_1528459684_result({ valor:{ error:false, cancel:true, data:'', reason:'cancelled' } }); 
      },
      error: function(error) {
         ID_1528459684_result({ valor:{ error:true, cancel:false, data:error, reason:error.error } }); 
      },
      saveToPhotoGallery:false,
      allowImageEditing:true,
      mediaTypes:[Ti.Media.MEDIA_TYPE_PHOTO]
   });
}
require('vars')['_estadoflash_']='auto';
require('vars')['_tipocamara_']='trasera';
require('vars')['_hayflash_']=true;
if (Ti.Media.hasCameraPermissions()) {
   camara_ID_1528459684();
} else {
   Ti.Media.requestCameraPermissions(function(ercp) {
      if (ercp.success) {
         camara_ID_1528459684();
      } else {
         ID_1528459684_result({ valor:{ error:true, cancel:false, data:'nopermission', type:'permission', reason:'camera doesnt have permissions' } });
      }
   });
}

}
function Listo_ID_1240649615(e) {

var evento=e;
var seltarea = ('seltarea' in require('vars'))?require('vars')['seltarea']:'';
var ID_1324041810_m=Alloy.Collections.numero_unico;
var ID_1324041810_fila = Alloy.createModel('numero_unico', {
comentario : 'nuevo danocontenido foto1'
}
 );
ID_1324041810_m.add(ID_1324041810_fila);
ID_1324041810_fila.save();
var nuevoid = require('helper').model2object(ID_1324041810_m.last());
if (!_.isUndefined(evento.comprimida)) {
if(false) console.log(String.format(L('x2721240094','imagen capturada, comprimida y asignada como cap%1$s.jpg'), nuevoid.id.toString()),{});
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
var ID_921903760_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'inspeccion'+seltarea.id_server);
if (ID_921903760_d.exists()==false) ID_921903760_d.createDirectory();
var ID_921903760_f = Ti.Filesystem.getFile(ID_921903760_d.resolve(),'cap'+nuevoid.id+'.jpg');
if (ID_921903760_f.exists()==true) ID_921903760_f.deleteFile();
ID_921903760_f.write(evento.comprimida);
ID_921903760_d = null; ID_921903760_f = null;
var ID_1150065702_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'mini'+seltarea.id_server);
if (ID_1150065702_d.exists()==false) ID_1150065702_d.createDirectory();
var ID_1150065702_f = Ti.Filesystem.getFile(ID_1150065702_d.resolve(),'cap'+nuevoid.id+'.jpg');
if (ID_1150065702_f.exists()==true) ID_1150065702_f.deleteFile();
ID_1150065702_f.write(evento.mini);
ID_1150065702_d = null; ID_1150065702_f = null;
}
 else {
var ID_632543922_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'inspeccion'+0);
if (ID_632543922_d.exists()==false) ID_632543922_d.createDirectory();
var ID_632543922_f = Ti.Filesystem.getFile(ID_632543922_d.resolve(),'cap'+nuevoid.id+'.jpg');
if (ID_632543922_f.exists()==true) ID_632543922_f.deleteFile();
ID_632543922_f.write(evento.comprimida);
ID_632543922_d = null; ID_632543922_f = null;
var ID_697608180_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'mini'+0);
if (ID_697608180_d.exists()==false) ID_697608180_d.createDirectory();
var ID_697608180_f = Ti.Filesystem.getFile(ID_697608180_d.resolve(),'cap'+nuevoid.id+'.jpg');
if (ID_697608180_f.exists()==true) ID_697608180_f.deleteFile();
ID_697608180_f.write(evento.mini);
ID_697608180_d = null; ID_697608180_f = null;
}$.contenido.set({
foto1 : String.format(L('x1070475521','cap%1$s.jpg'), nuevoid.id.toString())
}
);
if ('contenido' in $) $contenido=$.contenido.toJSON();
}

}

$.ID_2045468281.init({
caja : 45,
__id : 'ALL2045468281',
onlisto : Listo_ID_1191010010,
onclick : Click_ID_1588435437
}
);

function Click_ID_1588435437(e) {

var evento=e;
var ID_1235407225_result = function(e) {
   var foto_full = e.valor;
   var foto = e.valor.data;
if (foto_full.error==true||foto_full.error=='true') {
var ID_1158255625_opts=[L('x1518866076_traducir','Aceptar')];
var ID_1158255625 = Ti.UI.createAlertDialog({
   title: L('x57652245_traducir','Alerta'),
   message: L('x1068520423_traducir','Existe un problema con la camara'),
   buttonNames: ID_1158255625_opts
});
ID_1158255625.addEventListener('click', function(e) {
   var suu=ID_1158255625_opts[e.index];
suu = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_1158255625.show();
}
 else if (foto_full.cancel==true||foto_full.cancel=='true') {
if(false) console.log('cancelada',{});
$.ID_2045468281.detener({});
} else {
$.ID_2045468281.procesar({imagen : foto,nueva : 640,calidad : 91});
 foto = null;
}};
function camara_ID_1235407225() {
   Ti.Media.showCamera({
      success: function(event) {
         if (event.mediaType==Ti.Media.MEDIA_TYPE_PHOTO) {
            ID_1235407225_result({ valor:{ error:false, cancel:false, data:event.media, reason:'' } }); 
         } else {
            ID_1235407225_result({ valor:{ error:true, cancel:false, data:'', reason:'not image' } }); 
         }
      },
      cancel: function() {
         ID_1235407225_result({ valor:{ error:false, cancel:true, data:'', reason:'cancelled' } }); 
      },
      error: function(error) {
         ID_1235407225_result({ valor:{ error:true, cancel:false, data:error, reason:error.error } }); 
      },
      saveToPhotoGallery:false,
      allowImageEditing:true,
      mediaTypes:[Ti.Media.MEDIA_TYPE_PHOTO]
   });
}
require('vars')['_estadoflash_']='auto';
require('vars')['_tipocamara_']='trasera';
require('vars')['_hayflash_']=true;
if (Ti.Media.hasCameraPermissions()) {
   camara_ID_1235407225();
} else {
   Ti.Media.requestCameraPermissions(function(ercp) {
      if (ercp.success) {
         camara_ID_1235407225();
      } else {
         ID_1235407225_result({ valor:{ error:true, cancel:false, data:'nopermission', type:'permission', reason:'camera doesnt have permissions' } });
      }
   });
}

}
function Listo_ID_1191010010(e) {

var evento=e;
var seltarea = ('seltarea' in require('vars'))?require('vars')['seltarea']:'';
var ID_1974906574_m=Alloy.Collections.numero_unico;
var ID_1974906574_fila = Alloy.createModel('numero_unico', {
comentario : 'nuevo danocontenido foto2'
}
 );
ID_1974906574_m.add(ID_1974906574_fila);
ID_1974906574_fila.save();
var nuevoid = require('helper').model2object(ID_1974906574_m.last());
if (!_.isUndefined(evento.comprimida)) {
if(false) console.log(String.format(L('x2721240094','imagen capturada, comprimida y asignada como cap%1$s.jpg'), nuevoid.id.toString()),{});
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
var ID_434647999_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'inspeccion'+seltarea.id_server);
if (ID_434647999_d.exists()==false) ID_434647999_d.createDirectory();
var ID_434647999_f = Ti.Filesystem.getFile(ID_434647999_d.resolve(),'cap'+nuevoid.id+'.jpg');
if (ID_434647999_f.exists()==true) ID_434647999_f.deleteFile();
ID_434647999_f.write(evento.comprimida);
ID_434647999_d = null; ID_434647999_f = null;
var ID_710898845_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'mini'+seltarea.id_server);
if (ID_710898845_d.exists()==false) ID_710898845_d.createDirectory();
var ID_710898845_f = Ti.Filesystem.getFile(ID_710898845_d.resolve(),'cap'+nuevoid.id+'.jpg');
if (ID_710898845_f.exists()==true) ID_710898845_f.deleteFile();
ID_710898845_f.write(evento.mini);
ID_710898845_d = null; ID_710898845_f = null;
}
 else {
var ID_1308965600_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'inspeccion'+0);
if (ID_1308965600_d.exists()==false) ID_1308965600_d.createDirectory();
var ID_1308965600_f = Ti.Filesystem.getFile(ID_1308965600_d.resolve(),'cap'+nuevoid.id+'.jpg');
if (ID_1308965600_f.exists()==true) ID_1308965600_f.deleteFile();
ID_1308965600_f.write(evento.comprimida);
ID_1308965600_d = null; ID_1308965600_f = null;
var ID_1473417320_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'mini'+0);
if (ID_1473417320_d.exists()==false) ID_1473417320_d.createDirectory();
var ID_1473417320_f = Ti.Filesystem.getFile(ID_1473417320_d.resolve(),'cap'+nuevoid.id+'.jpg');
if (ID_1473417320_f.exists()==true) ID_1473417320_f.deleteFile();
ID_1473417320_f.write(evento.mini);
ID_1473417320_d = null; ID_1473417320_f = null;
}$.contenido.set({
foto2 : String.format(L('x1070475521','cap%1$s.jpg'), nuevoid.id.toString())
}
);
if ('contenido' in $) $contenido=$.contenido.toJSON();
}

}

$.ID_2081910510.init({
caja : 45,
__id : 'ALL2081910510',
onlisto : Listo_ID_1465685741,
onclick : Click_ID_1304165222
}
);

function Click_ID_1304165222(e) {

var evento=e;
var ID_1197430713_result = function(e) {
   var foto_full = e.valor;
   var foto = e.valor.data;
if (foto_full.error==true||foto_full.error=='true') {
var ID_1628588649_opts=[L('x1518866076_traducir','Aceptar')];
var ID_1628588649 = Ti.UI.createAlertDialog({
   title: L('x57652245_traducir','Alerta'),
   message: L('x1068520423_traducir','Existe un problema con la camara'),
   buttonNames: ID_1628588649_opts
});
ID_1628588649.addEventListener('click', function(e) {
   var suu=ID_1628588649_opts[e.index];
suu = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_1628588649.show();
}
 else if (foto_full.cancel==true||foto_full.cancel=='true') {
if(false) console.log('cancelada',{});
$.ID_2081910510.detener({});
} else {
$.ID_2081910510.procesar({imagen : foto,nueva : 640,calidad : 91});
 foto = null;
}};
function camara_ID_1197430713() {
   Ti.Media.showCamera({
      success: function(event) {
         if (event.mediaType==Ti.Media.MEDIA_TYPE_PHOTO) {
            ID_1197430713_result({ valor:{ error:false, cancel:false, data:event.media, reason:'' } }); 
         } else {
            ID_1197430713_result({ valor:{ error:true, cancel:false, data:'', reason:'not image' } }); 
         }
      },
      cancel: function() {
         ID_1197430713_result({ valor:{ error:false, cancel:true, data:'', reason:'cancelled' } }); 
      },
      error: function(error) {
         ID_1197430713_result({ valor:{ error:true, cancel:false, data:error, reason:error.error } }); 
      },
      saveToPhotoGallery:false,
      allowImageEditing:true,
      mediaTypes:[Ti.Media.MEDIA_TYPE_PHOTO]
   });
}
require('vars')['_estadoflash_']='auto';
require('vars')['_tipocamara_']='trasera';
require('vars')['_hayflash_']=true;
if (Ti.Media.hasCameraPermissions()) {
   camara_ID_1197430713();
} else {
   Ti.Media.requestCameraPermissions(function(ercp) {
      if (ercp.success) {
         camara_ID_1197430713();
      } else {
         ID_1197430713_result({ valor:{ error:true, cancel:false, data:'nopermission', type:'permission', reason:'camera doesnt have permissions' } });
      }
   });
}

}
function Listo_ID_1465685741(e) {

var evento=e;
var seltarea = ('seltarea' in require('vars'))?require('vars')['seltarea']:'';
var ID_1249366738_m=Alloy.Collections.numero_unico;
var ID_1249366738_fila = Alloy.createModel('numero_unico', {
comentario : 'nuevo danocontenido foto3'
}
 );
ID_1249366738_m.add(ID_1249366738_fila);
ID_1249366738_fila.save();
var nuevoid = require('helper').model2object(ID_1249366738_m.last());
if (!_.isUndefined(evento.comprimida)) {
if(false) console.log(String.format(L('x2721240094','imagen capturada, comprimida y asignada como cap%1$s.jpg'), nuevoid.id.toString()),{});
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
var ID_1791446374_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'inspeccion'+seltarea.id_server);
if (ID_1791446374_d.exists()==false) ID_1791446374_d.createDirectory();
var ID_1791446374_f = Ti.Filesystem.getFile(ID_1791446374_d.resolve(),'cap'+nuevoid.id+'.jpg');
if (ID_1791446374_f.exists()==true) ID_1791446374_f.deleteFile();
ID_1791446374_f.write(evento.comprimida);
ID_1791446374_d = null; ID_1791446374_f = null;
var ID_531307954_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'mini'+seltarea.id_server);
if (ID_531307954_d.exists()==false) ID_531307954_d.createDirectory();
var ID_531307954_f = Ti.Filesystem.getFile(ID_531307954_d.resolve(),'cap'+nuevoid.id+'.jpg');
if (ID_531307954_f.exists()==true) ID_531307954_f.deleteFile();
ID_531307954_f.write(evento.mini);
ID_531307954_d = null; ID_531307954_f = null;
}
 else {
var ID_1791226483_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'inspeccion'+0);
if (ID_1791226483_d.exists()==false) ID_1791226483_d.createDirectory();
var ID_1791226483_f = Ti.Filesystem.getFile(ID_1791226483_d.resolve(),'cap'+nuevoid.id+'.jpg');
if (ID_1791226483_f.exists()==true) ID_1791226483_f.deleteFile();
ID_1791226483_f.write(evento.comprimida);
ID_1791226483_d = null; ID_1791226483_f = null;
var ID_41835150_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'mini'+0);
if (ID_41835150_d.exists()==false) ID_41835150_d.createDirectory();
var ID_41835150_f = Ti.Filesystem.getFile(ID_41835150_d.resolve(),'cap'+nuevoid.id+'.jpg');
if (ID_41835150_f.exists()==true) ID_41835150_f.deleteFile();
ID_41835150_f.write(evento.mini);
ID_41835150_d = null; ID_41835150_f = null;
}$.contenido.set({
foto3 : String.format(L('x1070475521','cap%1$s.jpg'), nuevoid.id.toString())
}
);
if ('contenido' in $) $contenido=$.contenido.toJSON();
}

}

$.ID_1470293663.init({
titulo : L('x767609104_traducir','RECINTOS'),
cargando : L('x1740321226_traducir','cargando ..'),
__id : 'ALL1470293663',
left : 0,
onrespuesta : Respuesta_ID_1886346402,
campo : L('x382177638_traducir','Ubicación'),
onabrir : Abrir_ID_1976039665,
color : 'celeste',
right : 5,
top : 3,
seleccione : L('x4060681104_traducir','recinto'),
activo : true,
onafterinit : Afterinit_ID_1055030771
}
);

function Afterinit_ID_1055030771(e) {

var evento=e;
var ID_940344009_func = function() {
var seltarea = ('seltarea' in require('vars'))?require('vars')['seltarea']:'';
if (!_.isUndefined(args._dato)) {
/** 
* Si estamos editando contenido, cargamos dato previo seleccionado en base de datos. 
*/
var ID_1492812132_i=Alloy.createCollection('insp_contenido');
var ID_1492812132_i_where='id=\''+args._dato.id+'\'';
ID_1492812132_i.fetch({ query: 'SELECT * FROM insp_contenido WHERE id=\''+args._dato.id+'\'' });
var insp_dato=require('helper').query2array(ID_1492812132_i);
if (insp_dato && insp_dato.length) {
/** 
* obtenemos valor seleccionado previamente y lo mostramos en selector. 
*/
var ID_894786196_i=Alloy.createCollection('insp_recintos');
var ID_894786196_i_where='id_recinto=\''+insp_dato[0].id_recinto+'\'';
ID_894786196_i.fetch({ query: 'SELECT * FROM insp_recintos WHERE id_recinto=\''+insp_dato[0].id_recinto+'\'' });
var recintos=require('helper').query2array(ID_894786196_i);
if (recintos && recintos.length) {
var ID_1331976723_func = function() {
$.ID_1470293663.labels({valor : recintos[0].nombre});
if(false) console.log('el nombre del recinto es',{"datos":recintos[0].nombre});
};
var ID_1331976723 = setTimeout(ID_1331976723_func, 1000*0.1);
}
}
}
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
var pais = ('pais' in require('vars'))?require('vars')['pais']:'';
var ID_1047457479_i=Alloy.createCollection('insp_recintos');
var ID_1047457479_i_where='id_inspeccion=\''+seltarea.id_server+'\'';
ID_1047457479_i.fetch({ query: 'SELECT * FROM insp_recintos WHERE id_inspeccion=\''+seltarea.id_server+'\'' });
var recintos=require('helper').query2array(ID_1047457479_i);
var datos=[];
_.each(recintos, function(fila, pos) {
  var new_row={};
  _.each(fila, function(x, llave) {
    var newkey='';
    if (llave=='nombre') newkey='valor';
    if (llave=='id_recinto') newkey='id_interno';
    if (newkey!='') new_row[newkey] = fila[llave];
  });
  datos.push(new_row);
});
}
 else {
var ID_1362133342_i=Alloy.Collections.insp_recintos;
var sql = "DELETE FROM " + ID_1362133342_i.config.adapter.collection_name;
var db = Ti.Database.open(ID_1362133342_i.config.adapter.db_name);
db.execute(sql);
db.close();
sql=null;
db=null;
ID_1362133342_i.trigger('delete');
var item_index = 0;
_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
item_index += 1;
var ID_1201510257_m=Alloy.Collections.insp_recintos;
var ID_1201510257_fila = Alloy.createModel('insp_recintos', {
nombre : String.format(L('x1101343128_traducir','Pieza%1$s'), item.toString()),
id_recinto : item
}
 );
ID_1201510257_m.add(ID_1201510257_fila);
ID_1201510257_fila.save();
});
var ID_1933965016_i=Alloy.createCollection('insp_recintos');
ID_1933965016_i.fetch();
var ID_1933965016_src=require('helper').query2array(ID_1933965016_i);
var datos=[];
_.each(ID_1933965016_src, function(fila, pos) {
  var new_row={};
  _.each(fila, function(x, llave) {
    var newkey='';
    if (llave=='nombre') newkey='valor';
    if (llave=='id_recinto') newkey='id_interno';
    if (newkey!='') new_row[newkey] = fila[llave];
  });
  datos.push(new_row);
});
}
$.ID_1470293663.data({data : datos});
};
var ID_940344009 = setTimeout(ID_940344009_func, 1000*0.1);

}
function Abrir_ID_1976039665(e) {

var evento=e;

}
function Respuesta_ID_1886346402(e) {

var evento=e;
$.ID_1470293663.labels({valor : evento.valor});
$.contenido.set({
id_recinto : evento.item.id_interno
}
);
if ('contenido' in $) $contenido=$.contenido.toJSON();
if(false) console.log('datos recibidos de modal ubicacion',{"datos":evento});

}
function Change_ID_1814475507(e) {

e.cancelBubble=true;
var elemento=e.value;
var source=e.source;
$.contenido.set({
cantidad : elemento
}
);
if ('contenido' in $) $contenido=$.contenido.toJSON();
elemento=null, source=null;

}
function Click_ID_514427405(e) {

e.cancelBubble=true;
var elemento=e.source;
$.ID_1552329821.fireEvent('click');

}
function Click_ID_920294639(e) {

e.cancelBubble=true;
var elemento=e.source;
$.ID_1552329821.fireEvent('click');

}
function Click_ID_1779921517(e) {

e.cancelBubble=true;
var elemento=e.source;
$.ID_1473909169.blur();
$.ID_1199702717.blur();
$.ID_1579875228.blur();
$.ID_1585064981.abrir({});

}

$.ID_1996770915.init({
titulo : L('x1629775439_traducir','MONEDAS'),
cargando : L('x1740321226_traducir','cargando ..'),
__id : 'ALL1996770915',
left : 0,
onrespuesta : Respuesta_ID_450691452,
campo : L('x3081186843_traducir','Moneda'),
onabrir : Abrir_ID_774637536,
color : 'celeste',
right : 5,
top : 3,
seleccione : L('x2547889144','-'),
activo : true,
onafterinit : Afterinit_ID_1097145720
}
);

function Afterinit_ID_1097145720(e) {

var evento=e;
var ID_961160588_func = function() {
var seltarea = ('seltarea' in require('vars'))?require('vars')['seltarea']:'';
if (!_.isUndefined(args._dato)) {
/** 
* Si estamos editando contenido, cargamos dato previo seleccionado en base de datos. 
*/
var ID_1366978194_i=Alloy.createCollection('insp_contenido');
var ID_1366978194_i_where='id=\''+args._dato.id+'\'';
ID_1366978194_i.fetch({ query: 'SELECT * FROM insp_contenido WHERE id=\''+args._dato.id+'\'' });
var insp_dato=require('helper').query2array(ID_1366978194_i);
if (insp_dato && insp_dato.length) {
/** 
* obtenemos valor seleccionado previamente y lo mostramos en selector. 
*/
var ID_1269157798_i=Alloy.createCollection('monedas');
var ID_1269157798_i_where='id_segured=\''+insp_dato[0].id_moneda+'\'';
ID_1269157798_i.fetch({ query: 'SELECT * FROM monedas WHERE id_segured=\''+insp_dato[0].id_moneda+'\'' });
var monedas=require('helper').query2array(ID_1269157798_i);
if (monedas && monedas.length) {
require('vars')[_var_scopekey]['moneda_texto']=monedas[0].nombre;
if(false) console.log('detalle de todas las monedas',{"datos":monedas});
var ID_528261129_func = function() {
var moneda_texto = ('moneda_texto' in require('vars')[_var_scopekey])?require('vars')[_var_scopekey]['moneda_texto']:'';
$.ID_1996770915.labels({valor : moneda_texto});
if(false) console.log('el nombre de la moneda es',{"datos":moneda_texto});
};
var ID_528261129 = setTimeout(ID_528261129_func, 1000*0.5);
}
}
}
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
var pais = ('pais' in require('vars'))?require('vars')['pais']:'';
var ID_520270707_i=Alloy.createCollection('monedas');
var ID_520270707_i_where='pais_texto=\''+pais[0].nombre+'\'';
ID_520270707_i.fetch({ query: 'SELECT * FROM monedas WHERE pais_texto=\''+pais[0].nombre+'\'' });
var monedas=require('helper').query2array(ID_520270707_i);
var datos=[];
_.each(monedas, function(fila, pos) {
  var new_row={};
  _.each(fila, function(x, llave) {
    var newkey='';
    if (llave=='nombre') newkey='valor';
    if (llave=='id_segured') newkey='id_interno';
    if (newkey!='') new_row[newkey] = fila[llave];
  });
  datos.push(new_row);
});
}
 else {
var ID_781747413_i=Alloy.Collections.monedas;
var sql = "DELETE FROM " + ID_781747413_i.config.adapter.collection_name;
var db = Ti.Database.open(ID_781747413_i.config.adapter.db_name);
db.execute(sql);
db.close();
sql=null;
db=null;
ID_781747413_i.trigger('delete');
var item_index = 0;
_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
item_index += 1;
var ID_1313163701_m=Alloy.Collections.monedas;
var ID_1313163701_fila = Alloy.createModel('monedas', {
nombre : String.format(L('x4148455394_traducir','CLP%1$s'), item.toString()),
id_segured : String.format(L('x1125471167_traducir','10%1$s'), item.toString())
}
 );
ID_1313163701_m.add(ID_1313163701_fila);
ID_1313163701_fila.save();
});
var ID_1532241386_i=Alloy.createCollection('monedas');
ID_1532241386_i.fetch();
var ID_1532241386_src=require('helper').query2array(ID_1532241386_i);
var datos=[];
_.each(ID_1532241386_src, function(fila, pos) {
  var new_row={};
  _.each(fila, function(x, llave) {
    var newkey='';
    if (llave=='nombre') newkey='valor';
    if (llave=='id_segured') newkey='id_interno';
    if (newkey!='') new_row[newkey] = fila[llave];
  });
  datos.push(new_row);
});
}
$.ID_1996770915.data({data : datos});
};
var ID_961160588 = setTimeout(ID_961160588_func, 1000*0.1);

}
function Abrir_ID_774637536(e) {

var evento=e;
$.ID_1585064981.cerrar({});

}
function Respuesta_ID_450691452(e) {

var evento=e;
$.ID_1996770915.labels({valor : evento.valor});
$.contenido.set({
id_moneda : evento.item.id_interno
}
);
if ('contenido' in $) $contenido=$.contenido.toJSON();
if(false) console.log('datos recibidos de modal monedas',{"datos":evento});

}
function Change_ID_7259157(e) {

e.cancelBubble=true;
var elemento=e.value;
var source=e.source;
$.contenido.set({
valor : elemento
}
);
if ('contenido' in $) $contenido=$.contenido.toJSON();
elemento=null, source=null;

}
function Change_ID_31855985(e) {

e.cancelBubble=true;
var elemento=e.value;
if (elemento==true||elemento=='true') {
$.ID_1632444658.setText('SI');

$.contenido.set({
recupero : 1
}
);
if ('contenido' in $) $contenido=$.contenido.toJSON();
}
 else {
$.ID_1632444658.setText('NO');

$.contenido.set({
recupero : 0
}
);
if ('contenido' in $) $contenido=$.contenido.toJSON();
}elemento=null;

}
function Change_ID_1252909136(e) {

e.cancelBubble=true;
var elemento=e.value;
var source=e.source;
$.contenido.set({
descripcion : elemento
}
);
if ('contenido' in $) $contenido=$.contenido.toJSON();
elemento=null, source=null;

}

(function() {
var seltarea = ('seltarea' in require('vars'))?require('vars')['seltarea']:'';
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
/** 
* Actualizamos el id de la inspeccion en la tabla de contenidos 
*/
$.contenido.set({
id_inspeccion : seltarea.id_server
}
);
if ('contenido' in $) $contenido=$.contenido.toJSON();
$.contenido.set({
recupero : 0
}
);
if ('contenido' in $) $contenido=$.contenido.toJSON();
}
if (!_.isUndefined(args._dato)) {
/** 
* si existe args._dato es porque estamos editando el contenido 
*/
require('vars')[_var_scopekey]['id_contenido']=args._dato.id;
/** 
* Modificamos el titulo del header cuando estamos editando contenido 
*/
var ID_1771733970_titulo = 'EDITAR CONTENIDO';

									var setTitle2 = function(valor) {
										if (OS_ANDROID) {
											abx.title = valor;
										} else {
											$.ID_1771733970_window.setTitle(valor);
										}
									};
									var getTitle2 = function() {
										if (OS_ANDROID) {
											return abx.title;
										} else {
											return $.ID_1771733970_window.getTitle();
										}
									};
									setTitle2(ID_1771733970_titulo);

/** 
* Modificamos estilo del selector de fecha 
*/
var ID_1240322270_estilo = 'estilo12';

											var _tmp_a4w = require('a4w');
											if ((typeof ID_1240322270_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (ID_1240322270_estilo in _tmp_a4w.styles['classes'])) {
												try {
													ID_1240322270_estilo = _tmp_a4w.styles['classes'][ID_1240322270_estilo];
												} catch (st_val_err) {
												}
											}
											_tmp_a4w = null;
											$.ID_1240322270.applyProperties(ID_1240322270_estilo);

/** 
* heredamos datos previos de nivel en modelo nuevo con binding de esta pantalla 
*/
var ID_523657741_i=Alloy.createCollection('insp_contenido');
var ID_523657741_i_where='id=\''+args._dato.id+'\'';
ID_523657741_i.fetch({ query: 'SELECT * FROM insp_contenido WHERE id=\''+args._dato.id+'\'' });
var insp_c=require('helper').query2array(ID_523657741_i);
/** 
* Seteamos los valor del modelo contenido con los datos del contenido a editar 
*/
$.contenido.set({
id_moneda : insp_c[0].id_moneda,
fecha_compra : insp_c[0].fecha_compra,
id_inspeccion : insp_c[0].id_inspeccion,
nombre : insp_c[0].nombre,
cantidad : insp_c[0].cantidad,
foto1 : insp_c[0].foto1,
id_nombre : insp_c[0].id_nombre,
foto2 : insp_c[0].foto2,
id_recinto : insp_c[0].id_recinto,
id_marca : insp_c[0].id_marca,
foto3 : insp_c[0].foto3,
recupero : insp_c[0].recupero,
valor : insp_c[0].valor,
descripcion : insp_c[0].descripcion
}
);
if ('contenido' in $) $contenido=$.contenido.toJSON();
/** 
* Formateamos fecha con formato Unix a formato &quot;DD-MM-YYYY&quot; y la asignamos a la variable fecha_compra 
*/
var moment = require('alloy/moment');
var ID_1471867220 = insp_c[0].fecha_compra;
if (typeof ID_1471867220 === 'string' || typeof ID_1471867220 === 'number') {
  var fecha_compra = moment(ID_1471867220,'"X"').format('DD-MM-YYYY');
} else {
  var fecha_compra = moment(ID_1471867220).format('DD-MM-YYYY');
}
$.ID_800868967.setText(insp_c[0].nombre);

$.ID_800868967.setColor('#000000');

/** 
* Modifica el valor del texto de la fecha 
*/
$.ID_1240322270.setText(fecha_compra);

/** 
* Modificamos valor del texto de cantidad de items 
*/
$.ID_1473909169.setValue(insp_c[0].cantidad);

/** 
* Modificamos valor del texto de valor del objeto 
*/
$.ID_1199702717.setValue(insp_c[0].valor);

/** 
* Modificamos valor de texto del campo descripcion de contenido 
*/
$.ID_1579875228.setValue(insp_c[0].descripcion);

if (insp_c[0].recupero==1||insp_c[0].recupero=='1') {
/** 
* Si switch recupero material esta encendido se modifica el valor y texto del switch recupero material 
*/
$.ID_1568773770.setValue(true);

$.ID_1632444658.setText('SI');

}
 else {
$.ID_1568773770.setValue('false');

$.ID_1632444658.setText('NO');

}if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
/** 
* Verificamos si las fotos estan en la carpeta de inspeccion o es un dummy 
*/
if(false) console.log('es parte de la compilacion inspeccion completa',{});
if ((_.isObject(insp_c[0].foto1) || (_.isString(insp_c[0].foto1)) &&  !_.isEmpty(insp_c[0].foto1)) || _.isNumber(insp_c[0].foto1) || _.isBoolean(insp_c[0].foto1)) {
var ID_1107478240_trycatch = { error: function(e) { if(false) console.log('error en comando probar: recuerda poner evento ?error como primer hijo.',e); } };
try {
ID_1107478240_trycatch.error = function(evento) {
};
var foto1 = '';
var ID_1660735328_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'mini'+insp_c[0].id_inspeccion);
if (ID_1660735328_d.exists()==true) {
  var ID_1660735328_f = Ti.Filesystem.getFile(ID_1660735328_d.resolve(),insp_c[0].foto1);
  if (ID_1660735328_f.exists()==true) {
    foto1 = ID_1660735328_f.read();
  }
  ID_1660735328_f = null;
}
ID_1660735328_d = null;
} catch (e) {
   ID_1107478240_trycatch.error(e);
}
}
if ((_.isObject(insp_c[0].foto2) || (_.isString(insp_c[0].foto2)) &&  !_.isEmpty(insp_c[0].foto2)) || _.isNumber(insp_c[0].foto2) || _.isBoolean(insp_c[0].foto2)) {
var ID_1766127056_trycatch = { error: function(e) { if(false) console.log('error en comando probar: recuerda poner evento ?error como primer hijo.',e); } };
try {
ID_1766127056_trycatch.error = function(evento) {
};
var foto2 = '';
var ID_1545713341_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'mini'+insp_c[0].id_inspeccion);
if (ID_1545713341_d.exists()==true) {
  var ID_1545713341_f = Ti.Filesystem.getFile(ID_1545713341_d.resolve(),insp_c[0].foto2);
  if (ID_1545713341_f.exists()==true) {
    foto2 = ID_1545713341_f.read();
  }
  ID_1545713341_f = null;
}
ID_1545713341_d = null;
} catch (e) {
   ID_1766127056_trycatch.error(e);
}
}
if ((_.isObject(insp_c[0].foto3) || (_.isString(insp_c[0].foto3)) &&  !_.isEmpty(insp_c[0].foto3)) || _.isNumber(insp_c[0].foto3) || _.isBoolean(insp_c[0].foto3)) {
var ID_1292397272_trycatch = { error: function(e) { if(false) console.log('error en comando probar: recuerda poner evento ?error como primer hijo.',e); } };
try {
ID_1292397272_trycatch.error = function(evento) {
};
var foto3 = '';
var ID_1433525051_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'mini'+insp_c[0].id_inspeccion);
if (ID_1433525051_d.exists()==true) {
  var ID_1433525051_f = Ti.Filesystem.getFile(ID_1433525051_d.resolve(),insp_c[0].foto3);
  if (ID_1433525051_f.exists()==true) {
    foto3 = ID_1433525051_f.read();
  }
  ID_1433525051_f = null;
}
ID_1433525051_d = null;
} catch (e) {
   ID_1292397272_trycatch.error(e);
}
}
}
 else {
if(false) console.log('esta compilacion esta sola, simulamos carpetas',{});
if ((_.isObject(insp_c[0].foto1) || (_.isString(insp_c[0].foto1)) &&  !_.isEmpty(insp_c[0].foto1)) || _.isNumber(insp_c[0].foto1) || _.isBoolean(insp_c[0].foto1)) {
var ID_1543759328_trycatch = { error: function(e) { if(false) console.log('error en comando probar: recuerda poner evento ?error como primer hijo.',e); } };
try {
ID_1543759328_trycatch.error = function(evento) {
};
var foto1 = '';
var ID_1028443953_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'mini'+0);
if (ID_1028443953_d.exists()==true) {
  var ID_1028443953_f = Ti.Filesystem.getFile(ID_1028443953_d.resolve(),insp_c[0].foto1);
  if (ID_1028443953_f.exists()==true) {
    foto1 = ID_1028443953_f.read();
  }
  ID_1028443953_f = null;
}
ID_1028443953_d = null;
if(false) console.log('leyo el archivo',{});
} catch (e) {
   ID_1543759328_trycatch.error(e);
}
}
if ((_.isObject(insp_c[0].foto2) || (_.isString(insp_c[0].foto2)) &&  !_.isEmpty(insp_c[0].foto2)) || _.isNumber(insp_c[0].foto2) || _.isBoolean(insp_c[0].foto2)) {
var ID_1667637967_trycatch = { error: function(e) { if(false) console.log('error en comando probar: recuerda poner evento ?error como primer hijo.',e); } };
try {
ID_1667637967_trycatch.error = function(evento) {
};
var foto2 = '';
var ID_1317591968_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'mini'+0);
if (ID_1317591968_d.exists()==true) {
  var ID_1317591968_f = Ti.Filesystem.getFile(ID_1317591968_d.resolve(),insp_c[0].foto2);
  if (ID_1317591968_f.exists()==true) {
    foto2 = ID_1317591968_f.read();
  }
  ID_1317591968_f = null;
}
ID_1317591968_d = null;
} catch (e) {
   ID_1667637967_trycatch.error(e);
}
}
if ((_.isObject(insp_c[0].foto3) || (_.isString(insp_c[0].foto3)) &&  !_.isEmpty(insp_c[0].foto3)) || _.isNumber(insp_c[0].foto3) || _.isBoolean(insp_c[0].foto3)) {
var ID_1576185760_trycatch = { error: function(e) { if(false) console.log('error en comando probar: recuerda poner evento ?error como primer hijo.',e); } };
try {
ID_1576185760_trycatch.error = function(evento) {
};
var foto3 = '';
var ID_1387696862_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'mini'+0);
if (ID_1387696862_d.exists()==true) {
  var ID_1387696862_f = Ti.Filesystem.getFile(ID_1387696862_d.resolve(),insp_c[0].foto3);
  if (ID_1387696862_f.exists()==true) {
    foto3 = ID_1387696862_f.read();
  }
  ID_1387696862_f = null;
}
ID_1387696862_d = null;
} catch (e) {
   ID_1576185760_trycatch.error(e);
}
}
}if ((_.isObject(foto1) || (_.isString(foto1)) &&  !_.isEmpty(foto1)) || _.isNumber(foto1) || _.isBoolean(foto1)) {
/** 
* Revisamos si las fotos existen, y modificamos la imagen en la vista para cargar la que esta en la memoria del equipo 
*/
if(false) console.log('llamamos evento mini',{"datos":"**foto1**"});
$.ID_1066022242.mini({mini : foto1});
}
if ((_.isObject(foto2) || (_.isString(foto2)) &&  !_.isEmpty(foto2)) || _.isNumber(foto2) || _.isBoolean(foto2)) {
if(false) console.log('llamamos evento mini',{});
$.ID_2045468281.mini({mini : foto2});
}
if ((_.isObject(foto3) || (_.isString(foto3)) &&  !_.isEmpty(foto3)) || _.isNumber(foto3) || _.isBoolean(foto3)) {
if(false) console.log('llamamos evento mini',{});
$.ID_2081910510.mini({mini : foto3});
}
 
foto1 = null;
foto2 = null;
foto3 = null
}
_my_events['resp_dato1,ID_1543134261'] = function(evento) {
if(false) console.log('detalle de la respuesta',{"datos":evento});
$.ID_800868967.setText(evento.nombre);

$.ID_800868967.setColor('#000000');

/** 
* Asumimos que el valor mostrado seleccionado es el nombre de la fila dano, ya que no existe el campo nombre en esta pantalla. 
*/
$.contenido.set({
nombre : evento.nombre,
id_nombre : evento.id_segured
}
);
if ('contenido' in $) $contenido=$.contenido.toJSON();
};
Alloy.Events.on('resp_dato1', _my_events['resp_dato1,ID_1543134261']);
})();

function Postlayout_ID_514877393(e) {

e.cancelBubble=true;
var elemento=e.source;
/** 
* Una vez ya cargada la tarea, dejamos la variable limpia para poder abrir otras pantallas 
*/
require('vars')['var_abriendo']='';

}