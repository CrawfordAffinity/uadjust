var _bind4section = {};
var _list_templates = {};

var _activity;
if (OS_ANDROID) {
    _activity = $.ID_1059906457.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_1059906457';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_1059906457.addEventListener('open', function(e) {
        abx.setStatusbarColor("#000000");
        abx.setBackgroundColor("white");
    });
}
$.ID_1059906457.orientationModes = [Titanium.UI.PORTRAIT];

function Click_ID_356194219(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    $.ID_1059906457.close();

}

$.ID_1195415339.init({
    titulo: L('x3752633607_traducir', 'PARTE 4: Disponibilidad de trabajo'),
    __id: 'ALL1195415339',
    avance: L('x25508266_traducir', '4/6')
});


$.ID_1740801603.init({
    __id: 'ALL1740801603',
    letra: L('x2909332022', 'L'),
    onon: on_ID_1320471689,
    onoff: Off_ID_1531247929
});

function on_ID_1320471689(e) {
    /** 
     * Dependiendo de la seleccion (encendido o apagado) se actualiza el registro para definir los dias disponibles para trabajar 
     */

    var evento = e;
    var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
    var registro = _.extend(registro, {
        d1: 1
    });

}

function Off_ID_1531247929(e) {

    var evento = e;
    var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
    var registro = _.extend(registro, {
        d1: 0
    });

}

$.ID_1189525823.init({
    __id: 'ALL1189525823',
    letra: L('x3664761504', 'M'),
    onon: on_ID_1662873377,
    onoff: Off_ID_1692278182
});

function on_ID_1662873377(e) {
    /** 
     * Dependiendo de la seleccion (encendido o apagado) se actualiza el registro para definir los dias disponibles para trabajar 
     */

    var evento = e;
    var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
    var registro = _.extend(registro, {
        d2: 1
    });

}

function Off_ID_1692278182(e) {

    var evento = e;
    var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
    var registro = _.extend(registro, {
        d2: 0
    });

}

$.ID_1326186190.init({
    __id: 'ALL1326186190',
    letra: L('x185522819_traducir', 'MI'),
    onon: on_ID_1748992889,
    onoff: Off_ID_1322403275
});

function on_ID_1748992889(e) {
    /** 
     * Dependiendo de la seleccion (encendido o apagado) se actualiza el registro para definir los dias disponibles para trabajar 
     */

    var evento = e;
    var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
    var registro = _.extend(registro, {
        d3: 1
    });

}

function Off_ID_1322403275(e) {

    var evento = e;
    var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
    var registro = _.extend(registro, {
        d3: 0
    });

}

$.ID_2116871682.init({
    __id: 'ALL2116871682',
    letra: L('x1141589763', 'J'),
    onon: on_ID_1662017492,
    onoff: Off_ID_2135218477
});

function on_ID_1662017492(e) {
    /** 
     * Dependiendo de la seleccion (encendido o apagado) se actualiza el registro para definir los dias disponibles para trabajar 
     */

    var evento = e;
    var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
    var registro = _.extend(registro, {
        d4: 1
    });

}

function Off_ID_2135218477(e) {

    var evento = e;
    var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
    var registro = _.extend(registro, {
        d4: 0
    });

}

$.ID_1612935041.init({
    __id: 'ALL1612935041',
    letra: L('x1342839628', 'V'),
    onon: on_ID_1060414253,
    onoff: Off_ID_2021691535
});

function on_ID_1060414253(e) {
    /** 
     * Dependiendo de la seleccion (encendido o apagado) se actualiza el registro para definir los dias disponibles para trabajar 
     */

    var evento = e;
    var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
    var registro = _.extend(registro, {
        d5: 1
    });

}

function Off_ID_2021691535(e) {

    var evento = e;
    var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
    var registro = _.extend(registro, {
        d5: 0
    });

}

$.ID_1889836297.init({
    __id: 'ALL1889836297',
    letra: L('x543223747', 'S'),
    onon: on_ID_1034703838,
    onoff: Off_ID_1087043002
});

function on_ID_1034703838(e) {
    /** 
     * Dependiendo de la seleccion (encendido o apagado) se actualiza el registro para definir los dias disponibles para trabajar 
     */

    var evento = e;
    var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
    var registro = _.extend(registro, {
        d6: 1
    });

}

function Off_ID_1087043002(e) {

    var evento = e;
    var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
    var registro = _.extend(registro, {
        d6: 0
    });

}

$.ID_2142873717.init({
    __id: 'ALL2142873717',
    letra: L('x2746444292', 'D'),
    onon: on_ID_1308100098,
    onoff: Off_ID_1897759022
});

function on_ID_1308100098(e) {
    /** 
     * Dependiendo de la seleccion (encendido o apagado) se actualiza el registro para definir los dias disponibles para trabajar 
     */

    var evento = e;
    var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
    var registro = _.extend(registro, {
        d7: 1
    });

}

function Off_ID_1897759022(e) {

    var evento = e;
    var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
    var registro = _.extend(registro, {
        d7: 0
    });

}

$.ID_1568547162.init({
    __id: 'ALL1568547162',
    onchange: Change_ID_1197194019,
    mins: L('x4261170317', 'true'),
    a: L('x3904355907_traducir', 'a')
});

function Change_ID_1197194019(e) {

    var evento = e;
    require('vars')['desde'] = evento.desde;
    require('vars')['hasta'] = evento.hasta;

}

$.ID_1929710970.init({
    titulo: L('x1524107289_traducir', 'CONTINUAR'),
    __id: 'ALL1929710970',
    onclick: Click_ID_145378811
});

function Click_ID_145378811(e) {

    var evento = e;
    /** 
     * Recuperamos variables, creamos una variable para identificar que alguna opcion este siendo elegida, y que la hora de fin sea mayor a la de inicio 
     */
    var desde = ('desde' in require('vars')) ? require('vars')['desde'] : '';
    var hasta = ('hasta' in require('vars')) ? require('vars')['hasta'] : '';
    var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';

    //validaciones
    var al_menos_uno = registro.d1 || registro.d2 || registro.d3 || registro.d4 || registro.d5 || registro.d6 || registro.d7;
    desde = parseInt(desde);
    hasta = parseInt(hasta)
    if (al_menos_uno == false || al_menos_uno == 'false') {
        var ID_1910769611_opts = [L('x1518866076_traducir', 'Aceptar')];
        var ID_1910769611 = Ti.UI.createAlertDialog({
            title: L('x2185084353_traducir', 'Atención'),
            message: L('x3562924320_traducir', 'Seleccione a lo menos un día'),
            buttonNames: ID_1910769611_opts
        });
        ID_1910769611.addEventListener('click', function(e) {
            var suu = ID_1910769611_opts[e.index];
            suu = null;

            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1910769611.show();
    } else if (_.isNumber(desde) && _.isNumber(hasta) && desde > hasta) {
        var ID_2060663906_opts = [L('x1518866076_traducir', 'Aceptar')];
        var ID_2060663906 = Ti.UI.createAlertDialog({
            title: L('x2185084353_traducir', 'Atención'),
            message: L('x458766715_traducir', 'Ingrese su horario de trabajo'),
            buttonNames: ID_2060663906_opts
        });
        ID_2060663906.addEventListener('click', function(e) {
            var suu = ID_2060663906_opts[e.index];
            suu = null;

            e.source.removeEventListener("click", arguments.callee);
        });
        ID_2060663906.show();
    } else if (desde == hasta) {
        var ID_1167942014_opts = [L('x1518866076_traducir', 'Aceptar')];
        var ID_1167942014 = Ti.UI.createAlertDialog({
            title: L('x2185084353_traducir', 'Atención'),
            message: L('x178692076_traducir', 'El rango debe ser superior a una hora'),
            buttonNames: ID_1167942014_opts
        });
        ID_1167942014.addEventListener('click', function(e) {
            var suu = ID_1167942014_opts[e.index];
            suu = null;

            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1167942014.show();
    } else {
        /** 
         * Actualizamos la variable del registro 
         */
        var registro = _.extend(registro, {
            disp_horas: String.format(L('x1576037006_traducir', '%1$s:00 %2$s:00'), (desde) ? desde.toString() : '', (hasta) ? hasta.toString() : '')
        });
        require('vars')['registro'] = registro;
        if ("ID_1119875032" in Alloy.Globals) {
            Alloy.Globals["ID_1119875032"].openWindow(Alloy.createController("contactos", {}).getView());
        } else {
            Alloy.Globals["ID_1119875032"] = $.ID_1119875032;
            Alloy.Globals["ID_1119875032"].openWindow(Alloy.createController("contactos", {}).getView());
        }

    }
}

(function() {
    var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
    /** 
     * Cuando se recupera la variable de registro, cargamos la disponibilidad en 0 para que el usuario deba escoger un dia al menos de disponibilidad 
     */
    var registro = _.extend(registro, {
        d1: 0,
        d2: 0,
        d3: 0,
        d4: 0,
        d5: 0,
        d6: 0,
        d7: 0
    });
    require('vars')['registro'] = registro;
    /** 
     * Guardamos las horas disponibles en 1 para que el usuario deba escoger un horario 
     */
    require('vars')['desde'] = L('x2212294583', '1');
    require('vars')['hasta'] = L('x2212294583', '1');
    /** 
     * Creamos evento que al cerrar desde la ultima pantalla, esta tambi&#233;n se cierre. Evitamos el uso excesivo de memoria ram 
     */

    _my_events['_close_enrolamiento,ID_1368401318'] = function(evento) {
        if (false) console.log('escuchando cerrar enrolamiento parttime', {});
        $.ID_1059906457.close();
    };
    Alloy.Events.on('_close_enrolamiento', _my_events['_close_enrolamiento,ID_1368401318']);
})();

if (OS_IOS || OS_ANDROID) {
    $.ID_1059906457.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}