var _bind4section = {};
var _list_templates = {
    "pborrar": {
        "ID_1883915455": {
            "text": "{id}"
        },
        "ID_2120109018": {
            "text": "{nombre}"
        },
        "ID_1046919308": {},
        "ID_1735369570": {},
        "ID_1777579448": {},
        "ID_1254685949": {},
        "ID_1795050796": {},
        "ID_1752901998": {},
        "ID_1914367022": {
            "text": "{nombre}"
        },
        "ID_1249074865": {
            "text": "{id}"
        },
        "ID_1131557960": {},
        "ID_1361874287": {}
    },
    "contenido": {
        "ID_630800887": {},
        "ID_862352109": {
            "text": "{id}"
        },
        "ID_560452274": {
            "text": "{nombre}"
        }
    },
    "elemento": {
        "ID_432412973": {},
        "ID_291370846": {
            "text": "{nombre}"
        },
        "ID_1925487537": {
            "text": "{id_server}"
        },
        "ID_975786586": {
            "text": "{id_segured}"
        },
        "ID_1546190443": {}
    },
    "recinto": {
        "ID_1960582596": {},
        "ID_871581291": {
            "text": "{nombre}"
        },
        "ID_310297848": {
            "text": "{id}"
        }
    }
};
var $datos = $.datos.toJSON();

$.ID_1587413219_window.setTitleAttributes({
    color: 'WHITE'
});
var _activity;
if (OS_ANDROID) {
    _activity = $.ID_1587413219.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_1587413219';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_1587413219_window.addEventListener('open', function(e) {
        abx.setStatusbarColor("#FFFFFF");
        abx.setBackgroundColor("#fcbd83");
    });
}

function Click_ID_997512846(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    ID_662151190.show();

}

$.ID_516906299.init({
    titulo: L('x404129330_traducir', '¿EL ASEGURADO CONFIRMA ESTOS DATOS?'),
    __id: 'ALL516906299',
    si: L('x1723413441_traducir', 'SI, Están correctos'),
    texto: L('x2083765231_traducir', 'El asegurado debe confirmar que los datos de esta sección están correctos'),
    pantalla: L('x2851883104_traducir', '¿ESTA DE ACUERDO?'),
    onno: no_ID_312598518,
    onsi: si_ID_587901510,
    no: L('x55492959_traducir', 'NO, Hay que modificar algo'),
    header: 'naranjo',
    onclick: Click_ID_1238820637
});

function Click_ID_1238820637(e) {

    var evento = e;
    if (_.isUndefined($datos.id_tipo_siniestro)) {
        /** 
         * Validamos que los campos ingresados sean correctos 
         */
        var ID_204137070_opts = [L('x1518866076_traducir', 'Aceptar')];
        var ID_204137070 = Ti.UI.createAlertDialog({
            title: L('x3237162386_traducir', 'Atencion'),
            message: L('x407984850_traducir', 'Seleccione tipo de siniestro'),
            buttonNames: ID_204137070_opts
        });
        ID_204137070.addEventListener('click', function(e) {
            var nulo = ID_204137070_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_204137070.show();
    } else if (_.isUndefined($datos.descripcion)) {
        var ID_26478533_opts = [L('x1518866076_traducir', 'Aceptar')];
        var ID_26478533 = Ti.UI.createAlertDialog({
            title: L('x3237162386_traducir', 'Atencion'),
            message: L('x2617454703_traducir', 'Describa el siniestro brevemente, mínimo 30 caracteres'),
            buttonNames: ID_26478533_opts
        });
        ID_26478533.addEventListener('click', function(e) {
            var nulo = ID_26478533_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_26478533.show();
    } else if (_.isNumber($datos.descripcion.length) && _.isNumber(29) && $datos.descripcion.length <= 29) {
        var ID_855578299_opts = [L('x1518866076_traducir', 'Aceptar')];
        var ID_855578299 = Ti.UI.createAlertDialog({
            title: L('x3237162386_traducir', 'Atencion'),
            message: L('x2617454703_traducir', 'Describa el siniestro brevemente, mínimo 30 caracteres'),
            buttonNames: ID_855578299_opts
        });
        ID_855578299.addEventListener('click', function(e) {
            var nulo = ID_855578299_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_855578299.show();
    } else if (_.isUndefined($datos.porcentaje_danos_estructura)) {
        var ID_1123886876_opts = [L('x1518866076_traducir', 'Aceptar')];
        var ID_1123886876 = Ti.UI.createAlertDialog({
            title: L('x3237162386_traducir', 'Atencion'),
            message: L('x2749688656_traducir', 'Indique porcentaje de daños en estructura'),
            buttonNames: ID_1123886876_opts
        });
        ID_1123886876.addEventListener('click', function(e) {
            var nulo = ID_1123886876_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1123886876.show();
    } else if (_.isUndefined($datos.porcentaje_danos_terminaciones)) {
        var ID_1698458597_opts = [L('x1518866076_traducir', 'Aceptar')];
        var ID_1698458597 = Ti.UI.createAlertDialog({
            title: L('x3237162386_traducir', 'Atencion'),
            message: L('x1745658519_traducir', 'Indique porcentaje de daños en terminaciones'),
            buttonNames: ID_1698458597_opts
        });
        ID_1698458597.addEventListener('click', function(e) {
            var nulo = ID_1698458597_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1698458597.show();
    } else if (_.isUndefined($datos.porcentaje_danos_instalaciones)) {
        var ID_1822123630_opts = [L('x1518866076_traducir', 'Aceptar')];
        var ID_1822123630 = Ti.UI.createAlertDialog({
            title: L('x3237162386_traducir', 'Atencion'),
            message: L('x159191226_traducir', 'Indique porcentaje de daños en instalaciones'),
            buttonNames: ID_1822123630_opts
        });
        ID_1822123630.addEventListener('click', function(e) {
            var nulo = ID_1822123630_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1822123630.show();
    } else {
        test = null;
        $.ID_516906299.enviar({});
    }
}

function si_ID_587901510(e) {

    var evento = e;
    /** 
     * Guardamos los datos agregados/modificados en el modelo de insp_siniestro 
     */
    Alloy.Collections[$.datos.config.adapter.collection_name].add($.datos);
    $.datos.save();
    Alloy.Collections[$.datos.config.adapter.collection_name].fetch();
    /** 
     * Limpiamos widgets 
     */
    $.ID_1379917834.limpiar({});
    Alloy.createController("recintos_index", {}).getView().open();

}

function no_ID_312598518(e) {

    var evento = e;

}

$.ID_1379917834.init({
    titulo: L('x3952572309_traducir', 'SINIESTRO'),
    cargando: L('x1740321226_traducir', 'cargando ..'),
    __id: 'ALL1379917834',
    left: 16,
    onrespuesta: Respuesta_ID_1827220738,
    campo: L('x2004291629_traducir', 'Tipo de siniestro'),
    onabrir: Abrir_ID_593866641,
    color: 'naranjo',
    right: 16,
    top: 20,
    seleccione: L('x4123108455_traducir', 'seleccione tipo'),
    activo: true,
    onafterinit: Afterinit_ID_795893003
});

function Afterinit_ID_795893003(e) {

    var evento = e;
    var ID_1088992865_func = function() {
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {} else {
            /** 
             * Insertamos dummies 
             */
            /** 
             * Insertamos dummies 
             */
            var ID_1826787656_i = Alloy.Collections.tipo_siniestro;
            var sql = "DELETE FROM " + ID_1826787656_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_1826787656_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            ID_1826787656_i.trigger('remove');
            var item_index = 0;
            _.each('1,2,3,4,5'.split(','), function(item, item_pos, item_list) {
                item_index += 1;
                var ID_925670234_m = Alloy.Collections.tipo_siniestro;
                var ID_925670234_fila = Alloy.createModel('tipo_siniestro', {
                    nombre: String.format(L('x1187638321', '%1$s Siniestro'), item.toString()),
                    id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
                    pais_texto: 'Chile',
                    id_segured: String.format(L('x2286518298_traducir', '11%1$s'), item_pos.toString()),
                    pais: 1
                });
                ID_925670234_m.add(ID_925670234_fila);
                ID_925670234_fila.save();
            });
        }
        if (false) console.log('transformando datos de modelo siniestro para modal', {});
        /** 
         * obtenemos datos para selectores 
         */
        var ID_985447822_i = Alloy.createCollection('tipo_siniestro');
        ID_985447822_i.fetch();
        var ID_985447822_src = require('helper').query2array(ID_985447822_i);
        var datos = [];
        _.each(ID_985447822_src, function(fila, pos) {
            var new_row = {};
            _.each(fila, function(x, llave) {
                var newkey = '';
                if (llave == 'nombre') newkey = 'valor';
                if (llave == 'id_segured') newkey = 'id_segured';
                if (newkey != '') new_row[newkey] = fila[llave];
            });
            datos.push(new_row);
        });
        if (false) console.log('enviando data transformada a modal', {});
        /** 
         * Cargamos el widget con los datos de tipo de siniestro 
         */
        $.ID_1379917834.data({
            data: datos
        });
    };
    var ID_1088992865 = setTimeout(ID_1088992865_func, 1000 * 0.2);

}

function Abrir_ID_593866641(e) {

    var evento = e;

}

function Respuesta_ID_1827220738(e) {

    var evento = e;
    /** 
     * Mostramos en pantalla el tipo de siniestro seleccionado, y guardamos en la tabla el id del tipo de siniestro 
     */
    $.ID_1379917834.labels({
        valor: evento.valor
    });
    $.datos.set({
        id_tipo_siniestro: evento.item.id_segured
    });
    if ('datos' in $) $datos = $.datos.toJSON();

}

function Change_ID_838048861(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    /** 
     * Cuando el campo de texto es desenfocado, actualizamos el modelo con la descripcion del siniestro 
     */
    $.datos.set({
        descripcion: elemento
    });
    if ('datos' in $) $datos = $.datos.toJSON();
    elemento = null, source = null;

}

function Change_ID_1834583380(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var valor = e.value;
    /** 
     * Dejamos el valor del slider entre 0 y 100, y actualizamos el valor del porcentaje de danos en la estructura en pantalla y tabla 
     */
    var redondo = Math.round(elemento);
    $.ID_1154897587.setText(redondo + '%');

    $.datos.set({
        porcentaje_danos_estructura: redondo
    });
    if ('datos' in $) $datos = $.datos.toJSON();
    elemento = null, valor = null;

}

function Change_ID_1449385557(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    if (elemento == true || elemento == 'true') {
        $.ID_1098001258.setText('SI');

        $.datos.set({
            analisis_especialista: 1
        });
        if ('datos' in $) $datos = $.datos.toJSON();
    } else {
        $.ID_1098001258.setText('NO');

        $.datos.set({
            analisis_especialista: 0
        });
        if ('datos' in $) $datos = $.datos.toJSON();
    }
    elemento = null;

}

function Change_ID_1186826631(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var valor = e.value;
    var redondo = Math.round(elemento);
    $.ID_475316297.setText(redondo + '%');

    $.datos.set({
        porcentaje_danos_terminaciones: redondo
    });
    if ('datos' in $) $datos = $.datos.toJSON();
    elemento = null, valor = null;

}

function Change_ID_982978399(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var valor = e.value;
    var redondo = Math.round(elemento);
    $.ID_1167199251.setText(redondo + '%');

    $.datos.set({
        porcentaje_danos_instalaciones: redondo
    });
    if ('datos' in $) $datos = $.datos.toJSON();
    elemento = null, valor = null;

}

function Click_ID_1971343681(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    $.ID_1512241661.blur();

}

function Postlayout_ID_944831471(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    $.ID_459111696.show();

}

function Postlayout_ID_1988656029(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var anteriores = ('anteriores' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['anteriores'] : '';
    if (anteriores == false || anteriores == 'false') {
        /** 
         * Avisamos a la aplicacion que cierre la pantalla que estaba antes que esta... Pantalla caracteristicas 
         */
        Alloy.Events.trigger('_cerrar_insp', {
            pantalla: L('x3681655494_traducir', 'caracteristicas')
        });
    }

}

require('vars')[_var_scopekey]['anteriores'] = L('x734881840_traducir', 'false');
var ID_662151190_opts = [L('x3481285698_traducir', 'Asegurado no puede seguir'), L('x3077268071_traducir', ' Se me acabo la bateria'), L('x479989047_traducir', ' Tuve un accidente'), L('x2376009830_traducir', ' Cancelar')];
var ID_662151190 = Ti.UI.createOptionDialog({
    title: L('x1670176792_traducir', 'RAZON PARA CANCELAR INSPECCION ACTUAL'),
    options: ID_662151190_opts
});
ID_662151190.addEventListener('click', function(e) {
    var resp = ID_662151190_opts[e.index];
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
    if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
        var razon = "";
        if (resp == L('x3481285698_traducir', 'Asegurado no puede seguir')) {
            razon = "Asegurado no puede seguir";
        }
        if (resp == L('x1862794075_traducir', 'Se me acabo la bateria')) {
            razon = "Se me acabo la bateria";
        }
        if (resp == L('x4012805111_traducir', 'Tuve un accidente')) {
            razon = "Tuve un accidente";
        }
        if (false) console.log('mi razon es', {
            "datos": razon
        });
        if ((_.isObject(razon) || (_.isString(razon)) && !_.isEmpty(razon)) || _.isNumber(razon) || _.isBoolean(razon)) {
            if (false) console.log('llamando servicio cancelarTarea', {});
            require('vars')['insp_cancelada'] = L('x2941610362_traducir', 'siniestro');
            Alloy.Collections[$.datos.config.adapter.collection_name].add($.datos);
            $.datos.save();
            Alloy.Collections[$.datos.config.adapter.collection_name].fetch();
            var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
            var ID_198695387_visible = true;

            if (ID_198695387_visible == 'si') {
                ID_198695387_visible = true;
            } else if (ID_198695387_visible == 'no') {
                ID_198695387_visible = false;
            }
            $.ID_198695387.setVisible(ID_198695387_visible);

            var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
            var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
            if (false) console.log('detalle de seltarea', {
                "data": seltarea
            });
            var datos = {
                id_inspector: inspector.id_server,
                codigo_identificador: inspector.codigo_identificador,
                id_server: seltarea.id_server,
                num_caso: seltarea.num_caso,
                razon: razon
            };
            require('vars')[_var_scopekey]['datos'] = datos;
            var ID_1502339310 = {};
            ID_1502339310.success = function(e) {
                var elemento = e,
                    valor = e;
                var ID_198695387_visible = false;

                if (ID_198695387_visible == 'si') {
                    ID_198695387_visible = true;
                } else if (ID_198695387_visible == 'no') {
                    ID_198695387_visible = false;
                }
                $.ID_198695387.setVisible(ID_198695387_visible);

                Alloy.createController("firma_index", {}).getView().open();
                var ID_1502479695_func = function() {
                    /** 
                     * Cerramos pantalla datos basicos 
                     */
                    Alloy.Events.trigger('_cerrar_insp', {
                        pantalla: L('x3772634211_traducir', 'siniestro')
                    });
                };
                var ID_1502479695 = setTimeout(ID_1502479695_func, 1000 * 0.5);
                elemento = null, valor = null;
            };
            ID_1502339310.error = function(e) {
                var elemento = e,
                    valor = e;
                if (false) console.log('hubo error llamando cancelarTarea, la agregamos a cola de salida', {
                    "elemento": elemento
                });
                if (false) console.log('agregando servicio cancelarTarea a cola', {});
                var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
                var datos = ('datos' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['datos'] : '';
                var ID_62284646_m = Alloy.Collections.cola;
                var ID_62284646_fila = Alloy.createModel('cola', {
                    data: JSON.stringify(datos),
                    id_tarea: seltarea.id_server,
                    tipo: 'cancelar'
                });
                ID_62284646_m.add(ID_62284646_fila);
                ID_62284646_fila.save();
                var ID_198695387_visible = false;

                if (ID_198695387_visible == 'si') {
                    ID_198695387_visible = true;
                } else if (ID_198695387_visible == 'no') {
                    ID_198695387_visible = false;
                }
                $.ID_198695387.setVisible(ID_198695387_visible);

                Alloy.createController("firma_index", {}).getView().open();
                /** 
                 * Cerramos pantalla datos basicos 
                 */
                Alloy.Events.trigger('_cerrar_insp', {
                    pantalla: L('x3772634211_traducir', 'siniestro')
                });
                elemento = null, valor = null;
            };
            require('helper').ajaxUnico('ID_1502339310', '' + String.format(L('x3244141284', '%1$scancelarTarea'), url_server.toString()) + '', 'POST', {
                id_inspector: seltarea.id_inspector,
                codigo_identificador: inspector.codigo_identificador,
                id_tarea: seltarea.id_server,
                num_caso: seltarea.num_caso,
                mensaje: razon,
                opcion: 0,
                tipo: 1
            }, 15000, ID_1502339310);
        }
    }
    resp = null;
});
var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
    $.datos.set({
        id_inspeccion: seltarea.id_server
    });
    if ('datos' in $) $datos = $.datos.toJSON();
    $.datos.set({
        analisis_especialista: 0
    });
    if ('datos' in $) $datos = $.datos.toJSON();
}
/** 
 * Escuchamos el evento para poder cerrar pantallas a medida que ya no la estamos utilizando 
 */
_my_events['_cerrar_insp,ID_231485652'] = function(evento) {
    if (!_.isUndefined(evento.pantalla)) {
        if (evento.pantalla == L('x2941610362_traducir', 'siniestro')) {
            if (false) console.log('debug cerrando siniestro', {});
            var ID_1781710521_trycatch = {
                error: function(e) {
                    if (false) console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
                }
            };
            try {
                ID_1781710521_trycatch.error = function(evento) {
                    if (false) console.log('error cerrando siniestro', {});
                };
                $.ID_1379917834.limpiar({});
                $.ID_1587413219.close();
            } catch (e) {
                ID_1781710521_trycatch.error(e);
            }
        }
    } else {
        if (false) console.log('debug cerrando (todas) siniestro', {});
        var ID_1962200830_trycatch = {
            error: function(e) {
                if (false) console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
            }
        };
        try {
            ID_1962200830_trycatch.error = function(evento) {
                if (false) console.log('error cerrando siniestro', {});
            };
            $.ID_1379917834.limpiar({});
            $.ID_1587413219.close();
        } catch (e) {
            ID_1962200830_trycatch.error(e);
        }
    }
};
Alloy.Events.on('_cerrar_insp', _my_events['_cerrar_insp,ID_231485652']);

if (OS_IOS || OS_ANDROID) {
    $.ID_1587413219.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}
//$.ID_1587413219.open();