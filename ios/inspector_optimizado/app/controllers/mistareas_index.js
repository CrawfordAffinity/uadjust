var _bind4section = {};
var _list_templates = {
    "pborrar": {
        "ID_1883915455": {
            "text": "{id}"
        },
        "ID_2120109018": {
            "text": "{nombre}"
        },
        "ID_1046919308": {},
        "ID_1735369570": {},
        "ID_1777579448": {},
        "ID_1254685949": {},
        "ID_1795050796": {},
        "ID_1752901998": {},
        "ID_1914367022": {
            "text": "{nombre}"
        },
        "ID_1249074865": {
            "text": "{id}"
        },
        "ID_1131557960": {},
        "ID_1361874287": {},
        "ID_1421561173": {},
        "ID_1142337614": {},
        "ID_1057712478": {
            "text": "{nombre}"
        },
        "ID_1942170367": {
            "text": "{id}"
        },
        "ID_1502687579": {},
        "ID_1852819254": {}
    },
    "contenido": {
        "ID_630800887": {},
        "ID_862352109": {
            "text": "{id}"
        },
        "ID_560452274": {
            "text": "{nombre}"
        }
    },
    "elemento": {
        "ID_432412973": {},
        "ID_291370846": {
            "text": "{nombre}"
        },
        "ID_1925487537": {
            "text": "{id_server}"
        },
        "ID_975786586": {
            "text": "{id_segured}"
        },
        "ID_1546190443": {}
    },
    "recinto": {
        "ID_1960582596": {},
        "ID_871581291": {
            "text": "{nombre}"
        },
        "ID_310297848": {
            "text": "{id}"
        }
    },
    "nivel": {
        "ID_1021786928": {
            "text": "{id}"
        },
        "ID_1241655463": {
            "text": "{nombre}"
        },
        "ID_599581692": {}
    },
    "tareas_mistareas": {
        "ID_1046985988": {},
        "ID_1046084950": {
            "text": "a {distancia} km"
        },
        "ID_1123407966": {},
        "ID_1939012117": {
            "text": "{direccion}"
        },
        "ID_1583266618": {
            "text": "{ciudad}, {pais}"
        },
        "ID_1700186260": {},
        "ID_402471964": {},
        "ID_1877209258": {},
        "ID_1110389999": {},
        "ID_1420292892": {},
        "ID_2059816558": {},
        "ID_1418045386": {
            "text": "{comuna}"
        },
        "ID_1501564469": {
            "idlocal": "{idlocal}"
        },
        "ID_1355864420": {
            "visible": "{seguirvisible}"
        },
        "ID_1174588565": {},
        "ID_1753287381": {}
    }
};

var _activity;
if (OS_ANDROID) {
    _activity = $.ID_304530978.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_304530978';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_304530978.addEventListener('open', function(e) {
        abx.setBackgroundColor("white");
    });
}

function Click_ID_1997385993(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var gps_error = ('gps_error' in require('vars')) ? require('vars')['gps_error'] : '';
    if (gps_error == true || gps_error == 'true') {
        var ID_2004580725_opts = [L('x1518866076_traducir', 'Aceptar')];
        var ID_2004580725 = Ti.UI.createAlertDialog({
            title: L('x3661800039_traducir', 'Error geolocalizando'),
            message: L('x942436043_traducir', 'Ha ocurrido un error al geolocalizarlo'),
            buttonNames: ID_2004580725_opts
        });
        ID_2004580725.addEventListener('click', function(e) {
            var errori = ID_2004580725_opts[e.index];
            errori = null;

            e.source.removeEventListener("click", arguments.callee);
        });
        ID_2004580725.show();
    }
    var obtenerentrantes = ('obtenerentrantes' in require('vars')) ? require('vars')['obtenerentrantes'] : '';
    var obtenermistareas = ('obtenermistareas' in require('vars')) ? require('vars')['obtenermistareas'] : '';
    var obteneremergencias = ('obteneremergencias' in require('vars')) ? require('vars')['obteneremergencias'] : '';
    if (obteneremergencias == true || obteneremergencias == 'true') {
        if (false) console.log('esta actualizando', {});
    } else if (obtenerentrantes == true || obtenerentrantes == 'true') {
        if (false) console.log('actualizando tareas entrantes', {});
    } else if (obtenermistareas == true || obtenermistareas == 'true') {
        if (false) console.log('esta actualizando mistareas', {});
    } else {
        require('vars')['obtenermistareas'] = L('x4261170317', 'true');
        require('vars')['obtenerentrantes'] = L('x4261170317', 'true');
        var gps_latitud = ('gps_latitud' in require('vars')) ? require('vars')['gps_latitud'] : '';
        var gps_longitud = ('gps_longitud' in require('vars')) ? require('vars')['gps_longitud'] : '';
        var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
        var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
        if (false) console.log('Actualizando', {});
        var ID_1348100052 = {};

        ID_1348100052.success = function(e) {
            var elemento = e,
                valor = e;
            if (elemento.error == 0 || elemento.error == '0') {
                var ID_1257896348_i = Alloy.Collections.tareas;
                var sql = "DELETE FROM " + ID_1257896348_i.config.adapter.collection_name;
                var db = Ti.Database.open(ID_1257896348_i.config.adapter.db_name);
                db.execute(sql);
                db.close();
                ID_1257896348_i.trigger('remove');
                /** 
                 * guardamos elemento.mistareas en var local, para ejecutar desfasada la insercion a la base de datos. 
                 */
                require('vars')['mistareas'] = elemento.mistareas;
                var ID_1591812203_func = function() {
                    var mistareas = ('mistareas' in require('vars')) ? require('vars')['mistareas'] : '';
                    var ID_672779146_m = Alloy.Collections.tareas;
                    var db_ID_672779146 = Ti.Database.open(ID_672779146_m.config.adapter.db_name);
                    db_ID_672779146.execute('BEGIN');
                    _.each(mistareas, function(ID_672779146_fila, pos) {
                        db_ID_672779146.execute('INSERT INTO tareas (fecha_tarea, id_inspeccion, id_asegurado, nivel_2, comentario_can_o_rech, asegurado_tel_fijo, estado_tarea, bono, evento, id_inspector, asegurado_codigo_identificador, lat, nivel_1, asegurado_nombre, pais, direccion, asegurador, fecha_ingreso, fecha_siniestro, nivel_1_texto, distance, nivel_4, perfil, asegurado_id, pais_texto, id_server, categoria, nivel_3, asegurado_correo, num_caso, lon, asegurado_tel_movil, nivel_5, tipo_tarea) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', ID_672779146_fila.fecha_tarea, ID_672779146_fila.id_inspeccion, ID_672779146_fila.id_asegurado, ID_672779146_fila.nivel_2, ID_672779146_fila.comentario_can_o_rech, ID_672779146_fila.asegurado_tel_fijo, ID_672779146_fila.estado_tarea, ID_672779146_fila.bono, ID_672779146_fila.evento, ID_672779146_fila.id_inspector, ID_672779146_fila.asegurado_codigo_identificador, ID_672779146_fila.lat, ID_672779146_fila.nivel_1, ID_672779146_fila.asegurado_nombre, ID_672779146_fila.pais, ID_672779146_fila.direccion, ID_672779146_fila.asegurador, ID_672779146_fila.fecha_ingreso, ID_672779146_fila.fecha_siniestro, ID_672779146_fila.nivel_1_, ID_672779146_fila.distancia, ID_672779146_fila.nivel_4, 'ubicacion', ID_672779146_fila.asegurado_id, ID_672779146_fila.pais, ID_672779146_fila.id, ID_672779146_fila.categoria, ID_672779146_fila.nivel_3, ID_672779146_fila.asegurado_correo, ID_672779146_fila.num_caso, ID_672779146_fila.lon, ID_672779146_fila.asegurado_tel_movil, ID_672779146_fila.nivel_5, ID_672779146_fila.tipo_tarea);
                    });
                    db_ID_672779146.execute('COMMIT');
                    db_ID_672779146.close();
                    db_ID_672779146 = null;
                    ID_672779146_m.trigger('change');
                    require('vars')['obtenermistareas'] = L('x734881840_traducir', 'false');
                    var ID_1919001476_func = function() {
                        if (false) console.log('psb: refrescandpsb: refrescando pantallas mistareas o mistareas', {});

                        var ID_1241004686_trycatch = {
                            error: function(e) {
                                if (false) console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
                            }
                        };
                        try {
                            ID_1241004686_trycatch.error = function(evento) {};

                            Alloy.Events.trigger('_refrescar_tareas_mistareas');

                            Alloy.Events.trigger('_calcular_ruta');
                        } catch (e) {
                            ID_1241004686_trycatch.error(e);
                        }
                    };
                    var ID_1919001476 = setTimeout(ID_1919001476_func, 1000 * 0.1);
                };
                var ID_1591812203 = setTimeout(ID_1591812203_func, 1000 * 0.1);
            } else {
                if (false) console.log('error al recibir mistareas', {
                    "elemento": elemento
                });
            }
            elemento = null, valor = null;
        };

        ID_1348100052.error = function(e) {
            var elemento = e,
                valor = e;
            if (false) console.log('respuesta fallida de obtenerMisTareas', {
                "datos": elemento
            });
            elemento = null, valor = null;
        };
        require('helper').ajaxUnico('ID_1348100052', '' + String.format(L('x2334609226', '%1$sobtenerMisTareas'), url_server.toString()) + '', 'POST', {
            id_inspector: inspector.id_server,
            lat: gps_latitud,
            lon: gps_longitud
        }, 15000, ID_1348100052);
        var ID_1477541591 = {};
        if (false) console.log('DEBUG WEB: requesting url:' + String.format(L('x1654875529', '%1$sobtenerTareasEntrantes'), url_server.toString()) + ' with data:', {
            _method: 'POST',
            _params: {
                id_inspector: inspector.id_server,
                lat: gps_latitud,
                lon: gps_longitud
            },
            _timeout: '15000'
        });

        ID_1477541591.success = function(e) {
            var elemento = e,
                valor = e;
            if (elemento.error != 0 && elemento.error != '0') {
                if (false) console.log('error de servidor en obtenerTareasEntrntes', {
                    "elemento": elemento
                });
                var ID_984540120_opts = [L('x1518866076_traducir', 'Aceptar')];
                var ID_984540120 = Ti.UI.createAlertDialog({
                    title: L('x57652245_traducir', 'Alerta'),
                    message: L('x344160560_traducir', 'Error con el servidor'),
                    buttonNames: ID_984540120_opts
                });
                ID_984540120.addEventListener('click', function(e) {
                    var errori = ID_984540120_opts[e.index];
                    errori = null;

                    e.source.removeEventListener("click", arguments.callee);
                });
                ID_984540120.show();
            } else {
                var ID_1355973319_i = Alloy.Collections.tareas_entrantes;
                var sql = "DELETE FROM " + ID_1355973319_i.config.adapter.collection_name;
                var db = Ti.Database.open(ID_1355973319_i.config.adapter.db_name);
                db.execute(sql);
                db.close();
                ID_1355973319_i.trigger('remove');
                if (false) console.log('Respuesta de servidor TareasEntrantes (sin error)', {
                    "elemento": elemento
                });
                var elemento_critica = elemento.critica;
                var ID_1376170820_m = Alloy.Collections.tareas_entrantes;
                var db_ID_1376170820 = Ti.Database.open(ID_1376170820_m.config.adapter.db_name);
                db_ID_1376170820.execute('BEGIN');
                _.each(elemento_critica, function(ID_1376170820_fila, pos) {
                    db_ID_1376170820.execute('INSERT INTO tareas_entrantes (fecha_tarea, id_inspeccion, id_asegurado, nivel_2, comentario_can_o_rech, asegurado_tel_fijo, estado_tarea, bono, evento, id_inspector, asegurado_codigo_identificador, lat, nivel_1, asegurado_nombre, pais, direccion, asegurador, fecha_ingreso, fecha_siniestro, nivel_1_texto, distance, nivel_4, perfil, asegurado_id, pais_texto, id_server, categoria, nivel_3, asegurado_correo, num_caso, lon, asegurado_tel_movil, tipo_tarea, nivel_5) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', ID_1376170820_fila.fecha_tarea, ID_1376170820_fila.id_inspeccion, ID_1376170820_fila.id_asegurado, ID_1376170820_fila.nivel_2, '-', ID_1376170820_fila.asegurado_tel_fijo, ID_1376170820_fila.estado_tarea, ID_1376170820_fila.bono, ID_1376170820_fila.evento, ID_1376170820_fila.id_inspector, ID_1376170820_fila.asegurado_codigo_identificador, ID_1376170820_fila.lat, ID_1376170820_fila.nivel_1, ID_1376170820_fila.asegurado_nombre, -1, ID_1376170820_fila.direccion, ID_1376170820_fila.asegurador, ID_1376170820_fila.fecha_ingreso, ID_1376170820_fila.fecha_siniestro, ID_1376170820_fila.nivel_1_, ID_1376170820_fila.distancia, ID_1376170820_fila.nivel_4, 'casa', ID_1376170820_fila.asegurado_id, ID_1376170820_fila.pais_, ID_1376170820_fila.id, ID_1376170820_fila.categoria, ID_1376170820_fila.nivel_3, ID_1376170820_fila.asegurado_correo, ID_1376170820_fila.num_caso, ID_1376170820_fila.lon, ID_1376170820_fila.asegurado_tel_movil, ID_1376170820_fila.tipo_tarea, ID_1376170820_fila.nivel_5);
                });
                db_ID_1376170820.execute('COMMIT');
                db_ID_1376170820.close();
                db_ID_1376170820 = null;
                ID_1376170820_m.trigger('change');
                var elemento_normal = elemento.normal;
                var ID_164163396_m = Alloy.Collections.tareas_entrantes;
                var db_ID_164163396 = Ti.Database.open(ID_164163396_m.config.adapter.db_name);
                db_ID_164163396.execute('BEGIN');
                _.each(elemento_normal, function(ID_164163396_fila, pos) {
                    db_ID_164163396.execute('INSERT INTO tareas_entrantes (fecha_tarea, id_inspeccion, id_asegurado, nivel_2, comentario_can_o_rech, asegurado_tel_fijo, estado_tarea, bono, evento, id_inspector, asegurado_codigo_identificador, lat, nivel_1, asegurado_nombre, pais, direccion, asegurador, fecha_ingreso, fecha_siniestro, nivel_1_texto, distance, nivel_4, perfil, asegurado_id, pais_texto, id_server, categoria, nivel_3, asegurado_correo, num_caso, lon, asegurado_tel_movil, tipo_tarea, nivel_5) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', ID_164163396_fila.fecha_tarea, ID_164163396_fila.id_inspeccion, ID_164163396_fila.id_asegurado, ID_164163396_fila.nivel_2, '-', ID_164163396_fila.asegurado_tel_fijo, ID_164163396_fila.estado_tarea, ID_164163396_fila.bono, ID_164163396_fila.evento, ID_164163396_fila.id_inspector, ID_164163396_fila.asegurado_codigo_identificador, ID_164163396_fila.lat, ID_164163396_fila.nivel_1, ID_164163396_fila.asegurado_nombre, -1, ID_164163396_fila.direccion, ID_164163396_fila.asegurador, ID_164163396_fila.fecha_ingreso, ID_164163396_fila.fecha_siniestro, ID_164163396_fila.nivel_1_, ID_164163396_fila.distancia, ID_164163396_fila.nivel_4, 'casa', ID_164163396_fila.asegurado_id, ID_164163396_fila.pais_, ID_164163396_fila.id, ID_164163396_fila.categoria, ID_164163396_fila.nivel_3, ID_164163396_fila.asegurado_correo, ID_164163396_fila.num_caso, ID_164163396_fila.lon, ID_164163396_fila.asegurado_tel_movil, ID_164163396_fila.tipo_tarea, ID_164163396_fila.nivel_5);
                });
                db_ID_164163396.execute('COMMIT');
                db_ID_164163396.close();
                db_ID_164163396 = null;
                ID_164163396_m.trigger('change');

                Alloy.Events.trigger('_refrescar_tareas_entrantes');
                require('vars')['obtenerentrantes'] = L('x734881840_traducir', 'false');
            }
            elemento = null, valor = null;
        };

        ID_1477541591.error = function(e) {
            var elemento = e,
                valor = e;
            if (false) console.log('Hubo una falla al llamar servicio obtenerTareasEntrantes', {});
            var ID_1412699298_opts = [L('x1518866076_traducir', 'Aceptar')];
            var ID_1412699298 = Ti.UI.createAlertDialog({
                title: L('x57652245_traducir', 'Alerta'),
                message: L('x3389756565_traducir', 'No se pudo conectar con el servidor'),
                buttonNames: ID_1412699298_opts
            });
            ID_1412699298.addEventListener('click', function(e) {
                var errori = ID_1412699298_opts[e.index];
                errori = null;

                e.source.removeEventListener("click", arguments.callee);
            });
            ID_1412699298.show();
            elemento = null, valor = null;
        };
        require('helper').ajaxUnico('ID_1477541591', '' + String.format(L('x1654875529', '%1$sobtenerTareasEntrantes'), url_server.toString()) + '', 'POST', {
            id_inspector: inspector.id_server,
            lat: gps_latitud,
            lon: gps_longitud
        }, 15000, ID_1477541591);
        var ID_1772162362 = {};

        ID_1772162362.success = function(e) {
            var elemento = e,
                valor = e;
            var ID_1827806984_i = Alloy.Collections.emergencia;
            var sql = "DELETE FROM " + ID_1827806984_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_1827806984_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            ID_1827806984_i.trigger('remove');
            if (elemento.error != 0 && elemento.error != '0') {
                var ID_1578589616_opts = [L('x1518866076_traducir', 'Aceptar')];
                var ID_1578589616 = Ti.UI.createAlertDialog({
                    title: L('x57652245_traducir', 'Alerta'),
                    message: L('x344160560_traducir', 'Error con el servidor'),
                    buttonNames: ID_1578589616_opts
                });
                ID_1578589616.addEventListener('click', function(e) {
                    var errori = ID_1578589616_opts[e.index];
                    errori = null;

                    e.source.removeEventListener("click", arguments.callee);
                });
                ID_1578589616.show();
            } else {
                if (false) console.log('Insertando emergencias', {
                    "elemento": elemento
                });
                var elemento_perfil = elemento.perfil;
                var ID_1937077707_m = Alloy.Collections.emergencia;
                var db_ID_1937077707 = Ti.Database.open(ID_1937077707_m.config.adapter.db_name);
                db_ID_1937077707.execute('BEGIN');
                _.each(elemento_perfil, function(ID_1937077707_fila, pos) {
                    db_ID_1937077707.execute('INSERT INTO emergencia (fecha_tarea, id_inspeccion, nivel_2, id_asegurado, comentario_can_o_rech, asegurado_tel_fijo, estado_tarea, bono, id_inspector, asegurado_codigo_identificador, lat, nivel_1, asegurado_nombre, pais, direccion, asegurador, fecha_ingreso, fecha_siniestro, nivel_1_texto, distance, nivel_4, perfil, asegurado_id, pais_texto, id_server, categoria, nivel_3, asegurado_correo, num_caso, lon, asegurado_tel_movil, distancia_2, nivel_5, tipo_tarea) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', ID_1937077707_fila.fecha_tarea, ID_1937077707_fila.id_inspeccion, ID_1937077707_fila.nivel_2, ID_1937077707_fila.id_asegurado, ID_1937077707_fila.comentario_can_o_rech, ID_1937077707_fila.asegurado_tel_fijo, ID_1937077707_fila.estado_tarea, ID_1937077707_fila.bono, ID_1937077707_fila.id_inspector, ID_1937077707_fila.asegurado_codigo_identificador, ID_1937077707_fila.lat, ID_1937077707_fila.nivel_1, ID_1937077707_fila.asegurado_nombre, ID_1937077707_fila.pais, ID_1937077707_fila.direccion, ID_1937077707_fila.asegurador, ID_1937077707_fila.fecha_ingreso, ID_1937077707_fila.fecha_siniestro, ID_1937077707_fila.nivel_1_google, ID_1937077707_fila.distancia, ID_1937077707_fila.nivel_4, 'casa', ID_1937077707_fila.asegurado_id, ID_1937077707_fila.pais_, ID_1937077707_fila.id, ID_1937077707_fila.categoria, ID_1937077707_fila.nivel_3, ID_1937077707_fila.asegurado_correo, ID_1937077707_fila.num_caso, ID_1937077707_fila.lon, ID_1937077707_fila.asegurado_tel_movil, ID_1937077707_fila.distancia_2, ID_1937077707_fila.nivel_5, ID_1937077707_fila.tipo_tarea);
                });
                db_ID_1937077707.execute('COMMIT');
                db_ID_1937077707.close();
                db_ID_1937077707 = null;
                ID_1937077707_m.trigger('change');
                var elemento_ubicacion = elemento.ubicacion;
                var ID_507245659_m = Alloy.Collections.emergencia;
                var db_ID_507245659 = Ti.Database.open(ID_507245659_m.config.adapter.db_name);
                db_ID_507245659.execute('BEGIN');
                _.each(elemento_ubicacion, function(ID_507245659_fila, pos) {
                    db_ID_507245659.execute('INSERT INTO emergencia (fecha_tarea, id_inspeccion, id_asegurado, nivel_2, comentario_can_o_rech, asegurado_tel_fijo, estado_tarea, bono, id_inspector, asegurado_codigo_identificador, lat, nivel_1, asegurado_nombre, pais, direccion, asegurador, fecha_ingreso, fecha_siniestro, nivel_1_texto, distance, nivel_4, perfil, asegurado_id, pais_texto, id_server, categoria, nivel_3, asegurado_correo, num_caso, lon, asegurado_tel_movil, distancia_2, tipo_tarea, nivel_5) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', ID_507245659_fila.fecha_tarea, ID_507245659_fila.id_inspeccion, ID_507245659_fila.id_asegurado, ID_507245659_fila.nivel_2, ID_507245659_fila.comentario_can_o_rech, ID_507245659_fila.asegurado_tel_fijo, ID_507245659_fila.estado_tarea, ID_507245659_fila.bono, ID_507245659_fila.id_inspector, ID_507245659_fila.asegurado_codigo_identificador, ID_507245659_fila.lat, ID_507245659_fila.nivel_1, ID_507245659_fila.asegurado_nombre, ID_507245659_fila.pais, ID_507245659_fila.direccion, ID_507245659_fila.asegurador, ID_507245659_fila.fecha_ingreso, ID_507245659_fila.fecha_siniestro, ID_507245659_fila.nivel_1_google, ID_507245659_fila.distancia, ID_507245659_fila.nivel_4, 'ubicacion', ID_507245659_fila.asegurado_id, ID_507245659_fila.pais_, ID_507245659_fila.id, ID_507245659_fila.categoria, ID_507245659_fila.nivel_3, ID_507245659_fila.asegurado_correo, ID_507245659_fila.num_caso, ID_507245659_fila.lon, ID_507245659_fila.asegurado_tel_movil, ID_507245659_fila.distancia_2, ID_507245659_fila.tipo_tarea, ID_507245659_fila.nivel_5);
                });
                db_ID_507245659.execute('COMMIT');
                db_ID_507245659.close();
                db_ID_507245659 = null;
                ID_507245659_m.trigger('change');
                require('vars')['obteneremergencias'] = L('x734881840_traducir', 'false');

                var ID_1412464248 = function(evento) {};
                Alloy.Events.trigger('_refrescar_tareas');
            }
            elemento = null, valor = null;
        };

        ID_1772162362.error = function(e) {
            var elemento = e,
                valor = e;
            if (false) console.log('Hubo un error', {});
            var ID_801575173_opts = [L('x1518866076_traducir', 'Aceptar')];
            var ID_801575173 = Ti.UI.createAlertDialog({
                title: L('x57652245_traducir', 'Alerta'),
                message: L('x3389756565_traducir', 'No se pudo conectar con el servidor'),
                buttonNames: ID_801575173_opts
            });
            ID_801575173.addEventListener('click', function(e) {
                var errori = ID_801575173_opts[e.index];
                errori = null;

                e.source.removeEventListener("click", arguments.callee);
            });
            ID_801575173.show();
            elemento = null, valor = null;
        };
        require('helper').ajaxUnico('ID_1772162362', '' + String.format(L('x1578679327', '%1$sobtenerEmergencias'), url_server.toString()) + '', 'POST', {
            id_inspector: inspector.id_server,
            lat: gps_latitud,
            lon: gps_longitud
        }, 15000, ID_1772162362);
    }
}

function Itemclick_ID_1224423571(e) {

    e.cancelBubble = true;
    var objeto = e.section.getItemAt(e.itemIndex);
    var modelo = {},
        _modelo = [];
    var fila = {},
        fila_bak = {},
        info = {
            _template: objeto.template,
            _what: [],
            _seccion_ref: e.section.getHeaderTitle(),
            _model_id: -1
        },
        _tmp = {
            objmap: {}
        };
    if ('itemId' in e) {
        info._model_id = e.itemId;
        modelo._id = info._model_id;
        if (info._seccion_ref != '' && info._seccion_ref in _bind4section) {
            modelo._collection = _bind4section[info._seccion_ref];
            _tmp._coll = modelo._collection;
        }
    }
    if (info._seccion_ref != '' && info._seccion_ref in _bind4section) {
        if (Alloy.Collections[_tmp._coll].config.adapter.type == 'sql') {
            _tmp._inst = Alloy.Collections[_tmp._coll];
            _tmp._id = Alloy.Collections[_tmp._coll].config.adapter.idAttribute;
            _tmp._dbsql = 'SELECT * FROM ' + Alloy.Collections[_tmp._coll].config.adapter.collection_name + ' WHERE ' + _tmp._id + ' = ' + e.itemId;
            _tmp._db = Ti.Database.open(Alloy.Collections[_tmp._coll].config.adapter.db_name);
            _modelo = _tmp._db.execute(_tmp._dbsql);
            var values = [],
                fieldNames = [];
            var fieldCount = _.isFunction(_modelo.fieldCount) ? _modelo.fieldCount() : _modelo.fieldCount;
            var getField = _modelo.field;
            var i = 0;
            for (; fieldCount > i; i++) fieldNames.push(_modelo.fieldName(i));
            while (_modelo.isValidRow()) {
                var o = {};
                for (i = 0; fieldCount > i; i++) o[fieldNames[i]] = getField(i);
                values.push(o);
                _modelo.next();
            }
            _modelo = values;
            _tmp._db.close();
        } else {
            _tmp._search = {};
            _tmp._search[_tmp._id] = e.itemId + '';
            _modelo = Alloy.Collections[_tmp._coll].fetch(_tmp._search);
        }
    }
    var findVariables = require('fvariables');
    _.each(_list_templates[info._template], function(obj_id, id) {
        _.each(obj_id, function(valor, prop) {
            var llaves = findVariables(valor, '{', '}');
            _.each(llaves, function(llave) {
                _tmp.objmap[llave] = {
                    id: id,
                    prop: prop
                };
                fila[llave] = objeto[id][prop];
                if (id == e.bindId) info._what.push(llave);
            });
        });
    });
    info._what = info._what.join(',');
    fila_bak = JSON.parse(JSON.stringify(fila));
    /** 
     * Usamos este flag para evitar que la pantalla se abra en mas de una oportunidad 
     */
    var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
    if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
        require('vars')['var_abriendo'] = L('x4261170317', 'true');
        var nulo = Alloy.createController("detalletarea_index", {
            '_id': fila.idlocal,
            '__master_model': (typeof modelo !== 'undefined') ? modelo : {},
            '__modelo': (typeof _modelo !== 'undefined' && _modelo.length > 0) ? _modelo[0] : {}
        }).getView();
        nulo.open({
            modal: true
        });

        nulo = null;
    }
    _tmp.changed = false;
    _tmp.diff_keys = [];
    _.each(fila, function(value1, prop) {
        var had_samekey = false;
        _.each(fila_bak, function(value2, prop2) {
            if (prop == prop2 && value1 == value2) {
                had_samekey = true;
            } else if (!_.has(fila_bak, prop) || !_.has(fila, prop2)) {
                has_samekey = true;
            }
        });
        if (!had_samekey) _tmp.diff_keys.push(prop);
    });
    if (_tmp.diff_keys.length > 0) _tmp.changed = true;
    if (_tmp.changed == true) {
        _.each(_tmp.diff_keys, function(llave) {
            objeto[_tmp.objmap[llave].id][_tmp.objmap[llave].prop] = fila[llave];
        });
        e.section.updateItemAt(e.itemIndex, objeto);
    }

}

(function() {

    _my_events['_refrescar_tareas_mistareas,ID_199664348'] = function(evento) {

        var ID_521921285_trycatch = {
            error: function(e) {
                if (false) console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
            }
        };
        try {
            ID_521921285_trycatch.error = function(evento) {
                if (false) console.log('error refrescando mis tareas (pantalla mistareas)', {});
            };
            var inspeccion_encurso = ('inspeccion_encurso' in require('vars')) ? require('vars')['inspeccion_encurso'] : '';
            if (false) console.log('esta es el valor de la inspeccion en curso', {
                "inspeccion en curso": inspeccion_encurso
            });
            if (inspeccion_encurso == false || inspeccion_encurso == 'false') {
                /** 
                 * Si no hay inspeccion en curso, limpiamos la lista de tareas, crear secciones para hoy, manana y otro dia. Consultamos a la tabla e ingresamos las tareas segun fecha 
                 */
                var ID_308810445_borrar = false;

                var limpiarListado = function(animar) {
                    var a_nimar = (typeof animar == 'undefined') ? false : animar;
                    if (OS_IOS && a_nimar == true) {
                        var s_ecciones = $.ID_308810445.getSections();
                        _.each(s_ecciones, function(obj_id, pos) {
                            $.ID_308810445.deleteSectionAt(0, {
                                animated: true
                            });
                        });
                    } else {
                        $.ID_308810445.setSections([]);
                    }
                };
                limpiarListado(ID_308810445_borrar);

                var ID_784741595 = Titanium.UI.createListSection({
                    headerTitle: L('x1916403066_traducir', 'hoy')
                });
                var ID_1009847172 = Titanium.UI.createView({
                    height: Ti.UI.FILL,
                    width: Ti.UI.FILL
                });
                var ID_1453353504 = Titanium.UI.createView({
                    height: '25dp',
                    layout: 'composite',
                    width: Ti.UI.FILL,
                    backgroundColor: '#F7F7F7'
                });
                var ID_620812511 = Titanium.UI.createLabel({
                    text: L('x1296057275_traducir', 'PARA HOY'),
                    color: '#999999',
                    touchEnabled: false,
                    font: {
                        fontFamily: 'SFUIText-Medium',
                        fontSize: '14dp'
                    }

                });
                ID_1453353504.add(ID_620812511);

                ID_1009847172.add(ID_1453353504);
                ID_784741595.setHeaderView(ID_1009847172);
                $.ID_308810445.appendSection(ID_784741595);
                var ID_1264274795 = Titanium.UI.createListSection({
                    headerTitle: L('x4074624282_traducir', 'manana')
                });
                var ID_285270039 = Titanium.UI.createView({
                    height: Ti.UI.FILL,
                    width: Ti.UI.FILL
                });
                var ID_1152615331 = Titanium.UI.createView({
                    height: '25dp',
                    layout: 'composite',
                    width: Ti.UI.FILL,
                    backgroundColor: '#F7F7F7'
                });
                var ID_1204908311 = Titanium.UI.createLabel({
                    text: L('x3380933310_traducir', 'MAÑANA'),
                    color: '#999999',
                    touchEnabled: false,
                    font: {
                        fontFamily: 'SFUIText-Medium',
                        fontSize: '14dp'
                    }

                });
                ID_1152615331.add(ID_1204908311);

                ID_285270039.add(ID_1152615331);
                ID_1264274795.setHeaderView(ID_285270039);
                $.ID_308810445.appendSection(ID_1264274795);
                var ID_378876316_i = Alloy.createCollection('tareas');
                var ID_378876316_i_where = 'ORDER BY FECHA_TAREA DESC';
                ID_378876316_i.fetch({
                    query: 'SELECT * FROM tareas ORDER BY FECHA_TAREA DESC'
                });
                var tareas = require('helper').query2array(ID_378876316_i);
                /** 
                 * Inicializamos la variable de ultima fecha en vacio para usarla en el recorrido de tareas 
                 */
                require('vars')['ultima_fecha'] = '';
                /** 
                 * Formateamos fecha de hoy y manana para cargar la tarea dependiendo de la fecha de realizacion 
                 */
                var moment = require('alloy/moment');
                var fecha_hoy = moment(new Date()).format('YYYY-MM-DD');
                var manana = new Date();
                manana.setDate(manana.getDate() + 1);;
                var moment = require('alloy/moment');
                var ID_348062720 = manana;
                var fecha_manana = moment(ID_348062720).format('YYYY-MM-DD');
                var tarea_index = 0;
                _.each(tareas, function(tarea, tarea_pos, tarea_list) {
                    tarea_index += 1;
                    if (tarea.estado_tarea == 4) {
                        /** 
                         * Si estado_tarea es 4 seteamos variable para enviar_ubicacion de inspector 
                         */
                        require('vars')['seguir_tarea'] = tarea.id_server;
                    }
                    /** 
                     * Sector de otras fechas 
                     */
                    var ultima_fecha = ('ultima_fecha' in require('vars')) ? require('vars')['ultima_fecha'] : '';
                    if (ultima_fecha != tarea.fecha_tarea) {
                        /** 
                         * Generamos una seccion nueva para las fechas que no sean de hoy, ni de manana 
                         */
                        if (tarea.fecha_tarea == fecha_hoy) {} else if (tarea.fecha_tarea == fecha_manana) {} else {
                            var formatfecha = ('formatfecha' in require('vars')) ? require('vars')['formatfecha'] : '';
                            var moment = require('alloy/moment');
                            var ID_1065996523 = tarea.fecha_tarea;
                            if (typeof ID_1065996523 === 'string' || typeof ID_1065996523 === 'number') {
                                var fecha_titulo = moment(ID_1065996523, 'YYYY-MM-DD').format('DD/MM/YYYY');
                            } else {
                                var fecha_titulo = moment(ID_1065996523).format('DD/MM/YYYY');
                            }
                            var ID_1704296253 = Titanium.UI.createListSection({
                                headerTitle: L('x27834329_traducir', 'fecha')
                            });
                            var ID_1652158889 = Titanium.UI.createView({
                                height: Ti.UI.FILL,
                                width: Ti.UI.FILL
                            });
                            var ID_1644970999 = Titanium.UI.createView({
                                height: '25dp',
                                layout: 'composite',
                                width: Ti.UI.FILL,
                                backgroundColor: '#F7F7F7'
                            });
                            var ID_948739880 = Titanium.UI.createLabel({
                                text: fecha_titulo,
                                color: '#999999',
                                touchEnabled: false,
                                font: {
                                    fontFamily: 'SFUIText-Medium',
                                    fontSize: '14dp'
                                }

                            });
                            ID_1644970999.add(ID_948739880);

                            ID_1652158889.add(ID_1644970999);
                            ID_1704296253.setHeaderView(ID_1652158889);
                            $.ID_308810445.appendSection(ID_1704296253);
                        }
                        /** 
                         * Actualizamos la variable con&#160;&#160;la ultima fecha de la tarea 
                         */
                        require('vars')['ultima_fecha'] = tarea.fecha_tarea;
                    }
                    if ((_.isObject(tarea.nivel_2) || _.isString(tarea.nivel_2)) && _.isEmpty(tarea.nivel_2)) {
                        /** 
                         * Revisamos cual es el ultimo nivel disponible y lo fijamos dentro de la variable tarea como ultimo_nivel 
                         */
                        var tarea = _.extend(tarea, {
                            ultimo_nivel: tarea.nivel_1
                        });
                    } else if ((_.isObject(tarea.nivel_3) || _.isString(tarea.nivel_3)) && _.isEmpty(tarea.nivel_3)) {
                        var tarea = _.extend(tarea, {
                            ultimo_nivel: tarea.nivel_2
                        });
                    } else if ((_.isObject(tarea.nivel_4) || _.isString(tarea.nivel_4)) && _.isEmpty(tarea.nivel_4)) {
                        var tarea = _.extend(tarea, {
                            ultimo_nivel: tarea.nivel_3
                        });
                    } else if ((_.isObject(tarea.nivel_5) || _.isString(tarea.nivel_5)) && _.isEmpty(tarea.nivel_5)) {
                        var tarea = _.extend(tarea, {
                            ultimo_nivel: tarea.nivel_4
                        });
                    } else {
                        var tarea = _.extend(tarea, {
                            ultimo_nivel: tarea.nivel_5
                        });
                    }
                    if (tarea.estado_tarea == 4) {
                        /** 
                         * Preguntamos si esta en seguimiento (estado es 4) 
                         */
                        require('vars')['seguimiento'] = L('x4261170317', 'true');
                    } else {
                        require('vars')['seguimiento'] = L('x734881840_traducir', 'false');
                    }
                    var seguimiento = ('seguimiento' in require('vars')) ? require('vars')['seguimiento'] : '';
                    if (tarea.fecha_tarea == fecha_hoy) {
                        /** 
                         * Dependiendo de la fecha de la tarea, es donde apuntaremos a la seccion que corresponde (segun fecha) 
                         */
                        var ID_514443896 = [{
                            ID_1046985988: {},
                            ID_1046084950: {
                                text: String.format(L('x1959850688_traducir', 'a %1$s km'), tarea.distance.toString())
                            },
                            ID_1123407966: {},
                            ID_1939012117: {
                                text: tarea.direccion
                            },
                            ID_1583266618: {
                                text: String.format(L('x1487588533', '%1$s, %2$s'), (tarea.nivel_2) ? tarea.nivel_2.toString() : '', (tarea.pais) ? tarea.pais.toString() : '')
                            },
                            ID_1700186260: {},
                            ID_402471964: {},
                            ID_1877209258: {},
                            ID_1110389999: {},
                            ID_1420292892: {},
                            ID_2059816558: {},
                            template: 'tareas_mistareas',
                            ID_1418045386: {
                                text: tarea.ultimo_nivel
                            },
                            ID_1501564469: {
                                idlocal: tarea.id
                            },
                            ID_1355864420: {
                                visible: seguimiento
                            },
                            ID_1174588565: {},
                            ID_1753287381: {}

                        }];
                        var ID_514443896_secs = {};
                        _.map($.ID_308810445.getSections(), function(ID_514443896_valor, ID_514443896_indice) {
                            ID_514443896_secs[ID_514443896_valor.getHeaderTitle()] = ID_514443896_indice;
                            return ID_514443896_valor;
                        });
                        if ('' + L('x1916403066_traducir', 'hoy') + '' in ID_514443896_secs) {
                            $.ID_308810445.sections[ID_514443896_secs['' + L('x1916403066_traducir', 'hoy') + '']].appendItems(ID_514443896);
                        } else {
                            console.log('DEBUG:dynamic listItem points to a invalid section id: check attribute _seccion.');
                        }
                    } else if (tarea.fecha_tarea == fecha_manana) {
                        var ID_1379951202 = [{
                            ID_1046985988: {},
                            ID_1046084950: {
                                text: String.format(L('x1959850688_traducir', 'a %1$s km'), tarea.distance.toString())
                            },
                            ID_1123407966: {},
                            ID_1939012117: {
                                text: tarea.direccion
                            },
                            ID_1583266618: {
                                text: String.format(L('x1487588533', '%1$s, %2$s'), (tarea.nivel_2) ? tarea.nivel_2.toString() : '', (tarea.pais) ? tarea.pais.toString() : '')
                            },
                            ID_1700186260: {},
                            ID_402471964: {},
                            ID_1877209258: {},
                            ID_1110389999: {},
                            ID_1420292892: {},
                            ID_2059816558: {},
                            template: 'tareas_mistareas',
                            ID_1418045386: {
                                text: tarea.ultimo_nivel
                            },
                            ID_1501564469: {
                                idlocal: tarea.id
                            },
                            ID_1355864420: {
                                visible: seguimiento
                            },
                            ID_1174588565: {},
                            ID_1753287381: {}

                        }];
                        var ID_1379951202_secs = {};
                        _.map($.ID_308810445.getSections(), function(ID_1379951202_valor, ID_1379951202_indice) {
                            ID_1379951202_secs[ID_1379951202_valor.getHeaderTitle()] = ID_1379951202_indice;
                            return ID_1379951202_valor;
                        });
                        if ('' + L('x4074624282_traducir', 'manana') + '' in ID_1379951202_secs) {
                            $.ID_308810445.sections[ID_1379951202_secs['' + L('x4074624282_traducir', 'manana') + '']].appendItems(ID_1379951202);
                        } else {
                            console.log('DEBUG:dynamic listItem points to a invalid section id: check attribute _seccion.');
                        }
                    } else {
                        var ID_723947171 = [{
                            ID_1046985988: {},
                            ID_1046084950: {
                                text: String.format(L('x1959850688_traducir', 'a %1$s km'), tarea.distance.toString())
                            },
                            ID_1123407966: {},
                            ID_1939012117: {
                                text: tarea.direccion
                            },
                            ID_1583266618: {
                                text: String.format(L('x1487588533', '%1$s, %2$s'), (tarea.nivel_2) ? tarea.nivel_2.toString() : '', (tarea.pais) ? tarea.pais.toString() : '')
                            },
                            ID_1700186260: {},
                            ID_402471964: {},
                            ID_1877209258: {},
                            ID_1110389999: {},
                            ID_1420292892: {},
                            ID_2059816558: {},
                            template: 'tareas_mistareas',
                            ID_1418045386: {
                                text: tarea.ultimo_nivel
                            },
                            ID_1501564469: {
                                idlocal: tarea.id
                            },
                            ID_1355864420: {
                                visible: seguimiento
                            },
                            ID_1174588565: {},
                            ID_1753287381: {}

                        }];
                        var ID_723947171_secs = {};
                        _.map($.ID_308810445.getSections(), function(ID_723947171_valor, ID_723947171_indice) {
                            ID_723947171_secs[ID_723947171_valor.getHeaderTitle()] = ID_723947171_indice;
                            return ID_723947171_valor;
                        });
                        if ('' + L('x27834329_traducir', 'fecha') + '' in ID_723947171_secs) {
                            $.ID_308810445.sections[ID_723947171_secs['' + L('x27834329_traducir', 'fecha') + '']].appendItems(ID_723947171);
                        } else {
                            console.log('DEBUG:dynamic listItem points to a invalid section id: check attribute _seccion.');
                        }
                    }
                });
                /** 
                 * Limpieza memoria 
                 */
                fecha_hoy = null, manana = null, tarea = null;
            }
        } catch (e) {
            ID_521921285_trycatch.error(e);
        }
    };
    Alloy.Events.on('_refrescar_tareas_mistareas', _my_events['_refrescar_tareas_mistareas,ID_199664348']);

    _my_events['_refrescar_tareas,ID_268924730'] = function(evento) {
        var inspeccion_encurso = ('inspeccion_encurso' in require('vars')) ? require('vars')['inspeccion_encurso'] : '';
        if (inspeccion_encurso == false || inspeccion_encurso == 'false') {

            Alloy.Events.trigger('_refrescar_tareas_mistareas');
        }
    };
    Alloy.Events.on('_refrescar_tareas', _my_events['_refrescar_tareas,ID_268924730']);
    /** 
     * Esto se llama al logearse 
     */
    var ID_1359791285_func = function() {

        Alloy.Events.trigger('_refrescar_tareas_mistareas');
    };
    var ID_1359791285 = setTimeout(ID_1359791285_func, 1000 * 0.1);
})();

if (OS_IOS || OS_ANDROID) {
    $.ID_304530978.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}
//$.ID_304530978.open();