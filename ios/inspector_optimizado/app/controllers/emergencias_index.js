var _bind4section = {
    "ref1": "emergencia"
};
var _list_templates = {
    "tarea": {
        "ID_1385647528": {},
        "ID_880073047": {},
        "ID_1894672826": {},
        "ID_1964919225": {
            "id_server": "{id_server}"
        },
        "ID_1857546850": {
            "text": "{direccion}"
        },
        "ID_524834655": {
            "text": "a {distance} km"
        },
        "ID_1057702246": {},
        "ID_1216288924": {},
        "ID_1821123282": {},
        "ID_1740224129": {
            "text": "{id}"
        },
        "ID_386275312": {
            "text": "{nivel_2}, {pais}"
        },
        "ID_551115843": {},
        "ID_1454204399": {},
        "ID_138536799": {
            "text": "{comuna}"
        }
    }
};

var _activity;
if (OS_ANDROID) {
    _activity = $.ID_1565575511.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_1565575511';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_1565575511.addEventListener('open', function(e) {});
}


var ID_466128971_like = function(search) {
    if (typeof search !== 'string' || this === null) {
        return false;
    }
    search = search.replace(new RegExp("([\\.\\\\\\+\\*\\?\\[\\^\\]\\$\\(\\)\\{\\}\\=\\!\\<\\>\\|\\:\\-])", "g"), "\\$1");
    search = search.replace(/%/g, '.*').replace(/_/g, '.');
    return RegExp('^' + search + '$', 'gi').test(this);
};
var ID_466128971_filter = function(coll) {
    var filtered = coll.filter(function(m) {
        var _tests = [],
            _all_true = false,
            model = m.toJSON();
        _tests.push((model.perfil == 'casa'));
        var _all_true_s = _.uniq(_tests);
        _all_true = (_all_true_s.length == 1 && _all_true_s[0] == true) ? true : false;
        return _all_true;
    });
    filtered = _.toArray(filtered);
    var ordered = _.sortBy(filtered, 'distancia_2');
    return ordered;
};
var ID_466128971_update = function(e) {};
_.defer(function() {
    Alloy.Collections.emergencia.fetch();
});
Alloy.Collections.emergencia.on('add change delete', function(ee) {
    ID_466128971_update(ee);
});
var ID_466128971_transform = function(model) {
    var fila = model.toJSON();
    if ((_.isObject(fila.nivel_2) || _.isString(fila.nivel_2)) && _.isEmpty(fila.nivel_2)) {
        /** 
         * El nivel_2 (region) no deber&#237;a estar nunca en blanco, por lo que marcamos los campos comuna y ciudad con guiones para marcar falla en datos. 
         */
        var fila = _.extend(fila, {
            comuna: fila.nivel_1
        });
    } else if ((_.isObject(fila.nivel_3) || _.isString(fila.nivel_3)) && _.isEmpty(fila.nivel_3)) {
        /** 
         * El nivel_3 (ciudad) tampoco deberia estar nunca en blanco. 
         */
        var fila = _.extend(fila, {
            comuna: fila.nivel_2
        });
    } else if ((_.isObject(fila.nivel_4) || _.isString(fila.nivel_4)) && _.isEmpty(fila.nivel_4)) {
        var fila = _.extend(fila, {
            comuna: fila.nivel_3
        });
    } else if ((_.isObject(fila.nivel_5) || _.isString(fila.nivel_5)) && _.isEmpty(fila.nivel_5)) {
        var fila = _.extend(fila, {
            comuna: fila.nivel_4
        });
    } else {
        var fila = _.extend(fila, {
            comuna: fila.nivel_5
        });
    }
    return fila;
};
Alloy.Collections.emergencia.fetch();

function Click_ID_1808918144(e) {

    e.cancelBubble = true;
    if ('index' in e) {
        var elemento = e.source.getLabels()[e.index];
        var valor = elemento.title;
        if (false) console.log('Cambiado', {
            "evento": e,
            "elemento": elemento
        });
        if (e.index == 0 || e.index == '0') {
            ID_466128971_filter = function(coll) {
                var filtered = coll.filter(function(m) {
                    var _tests = [],
                        _all_true = false,
                        model = m.toJSON();
                    _tests.push((model.perfil == 'casa'));
                    var _all_true_s = _.uniq(_tests);
                    _all_true = (_all_true_s.length == 1 && _all_true_s[0] == true) ? true : false;
                    return _all_true;
                });
                filtered = _.toArray(filtered);
                var ordered = _.sortBy(filtered, 'distancia_2');
                return ordered;
            };
            _.defer(function() {
                Alloy.Collections.emergencia.fetch();
            });
        } else {
            ID_466128971_filter = function(coll) {
                var filtered = coll.filter(function(m) {
                    var _tests = [],
                        _all_true = false,
                        model = m.toJSON();
                    _tests.push((model.perfil == 'ubicacion'));
                    var _all_true_s = _.uniq(_tests);
                    _all_true = (_all_true_s.length == 1 && _all_true_s[0] == true) ? true : false;
                    return _all_true;
                });
                filtered = _.toArray(filtered);
                var ordered = _.sortBy(filtered, 'distancia_2');
                return ordered;
            };
            _.defer(function() {
                Alloy.Collections.emergencia.fetch();
            });
        }
        elemento = null, valor = null;
    }

}

function Click_ID_537120378(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var gps_error = ('gps_error' in require('vars')) ? require('vars')['gps_error'] : '';
    if (gps_error == true || gps_error == 'true') {
        var ID_1818330899_opts = [L('x1518866076_traducir', 'Aceptar')];
        var ID_1818330899 = Ti.UI.createAlertDialog({
            title: L('x57652245_traducir', 'Alerta'),
            message: L('x3873304135_traducir', 'No se pudo obtener la ubicación'),
            buttonNames: ID_1818330899_opts
        });
        ID_1818330899.addEventListener('click', function(e) {
            var errori = ID_1818330899_opts[e.index];
            errori = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1818330899.show();
    }
    var obtenerentrantes = ('obtenerentrantes' in require('vars')) ? require('vars')['obtenerentrantes'] : '';
    var obtenermistareas = ('obtenermistareas' in require('vars')) ? require('vars')['obtenermistareas'] : '';
    var obteneremergencias = ('obteneremergencias' in require('vars')) ? require('vars')['obteneremergencias'] : '';
    if (obteneremergencias == true || obteneremergencias == 'true') {
        if (false) console.log('esta actualizando', {});
    } else if (obtenerentrantes == true || obtenerentrantes == 'true') {
        if (false) console.log('actualizando tareas entrantes', {});
    } else if (obtenermistareas == true || obtenermistareas == 'true') {
        if (false) console.log('esta actualizando mistareas', {});
    } else {
        require('vars')['obtenermistareas'] = L('x4261170317', 'true');
        require('vars')['obtenerentrantes'] = L('x4261170317', 'true');
        var gps_latitud = ('gps_latitud' in require('vars')) ? require('vars')['gps_latitud'] : '';
        var gps_longitud = ('gps_longitud' in require('vars')) ? require('vars')['gps_longitud'] : '';
        var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
        var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
        if (false) console.log('Actualizando', {});
        var ID_1348100052 = {};
        ID_1348100052.success = function(e) {
            var elemento = e,
                valor = e;
            if (elemento.error == 0 || elemento.error == '0') {
                var ID_1257896348_i = Alloy.Collections.tareas;
                var sql = "DELETE FROM " + ID_1257896348_i.config.adapter.collection_name;
                var db = Ti.Database.open(ID_1257896348_i.config.adapter.db_name);
                db.execute(sql);
                db.close();
                ID_1257896348_i.trigger('remove');
                /** 
                 * guardamos elemento.mistareas en var local, para ejecutar desfasada la insercion a la base de datos. 
                 */
                require('vars')['mistareas'] = elemento.mistareas;
                var ID_1591812203_func = function() {
                    var mistareas = ('mistareas' in require('vars')) ? require('vars')['mistareas'] : '';
                    var ID_672779146_m = Alloy.Collections.tareas;
                    var db_ID_672779146 = Ti.Database.open(ID_672779146_m.config.adapter.db_name);
                    db_ID_672779146.execute('BEGIN');
                    _.each(mistareas, function(ID_672779146_fila, pos) {
                        db_ID_672779146.execute('INSERT INTO tareas (fecha_tarea, id_inspeccion, id_asegurado, nivel_2, comentario_can_o_rech, asegurado_tel_fijo, estado_tarea, bono, evento, id_inspector, asegurado_codigo_identificador, lat, nivel_1, asegurado_nombre, pais, direccion, asegurador, fecha_ingreso, fecha_siniestro, nivel_1_texto, distance, nivel_4, perfil, asegurado_id, pais_texto, id_server, categoria, nivel_3, asegurado_correo, num_caso, lon, asegurado_tel_movil, nivel_5, tipo_tarea) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', ID_672779146_fila.fecha_tarea, ID_672779146_fila.id_inspeccion, ID_672779146_fila.id_asegurado, ID_672779146_fila.nivel_2, ID_672779146_fila.comentario_can_o_rech, ID_672779146_fila.asegurado_tel_fijo, ID_672779146_fila.estado_tarea, ID_672779146_fila.bono, ID_672779146_fila.evento, ID_672779146_fila.id_inspector, ID_672779146_fila.asegurado_codigo_identificador, ID_672779146_fila.lat, ID_672779146_fila.nivel_1, ID_672779146_fila.asegurado_nombre, ID_672779146_fila.pais, ID_672779146_fila.direccion, ID_672779146_fila.asegurador, ID_672779146_fila.fecha_ingreso, ID_672779146_fila.fecha_siniestro, ID_672779146_fila.nivel_1_, ID_672779146_fila.distancia, ID_672779146_fila.nivel_4, 'ubicacion', ID_672779146_fila.asegurado_id, ID_672779146_fila.pais, ID_672779146_fila.id, ID_672779146_fila.categoria, ID_672779146_fila.nivel_3, ID_672779146_fila.asegurado_correo, ID_672779146_fila.num_caso, ID_672779146_fila.lon, ID_672779146_fila.asegurado_tel_movil, ID_672779146_fila.nivel_5, ID_672779146_fila.tipo_tarea);
                    });
                    db_ID_672779146.execute('COMMIT');
                    db_ID_672779146.close();
                    db_ID_672779146 = null;
                    ID_672779146_m.trigger('change');
                    require('vars')['obtenermistareas'] = L('x734881840_traducir', 'false');
                    var ID_1919001476_func = function() {
                        if (false) console.log('psb: refrescandpsb: refrescando pantallas mistareas o mistareas', {});
                        var ID_1241004686_trycatch = {
                            error: function(e) {
                                if (false) console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
                            }
                        };
                        try {
                            ID_1241004686_trycatch.error = function(evento) {};
                            Alloy.Events.trigger('_refrescar_tareas_mistareas');
                            Alloy.Events.trigger('_calcular_ruta');
                        } catch (e) {
                            ID_1241004686_trycatch.error(e);
                        }
                    };
                    var ID_1919001476 = setTimeout(ID_1919001476_func, 1000 * 0.1);
                };
                var ID_1591812203 = setTimeout(ID_1591812203_func, 1000 * 0.1);
            } else {
                if (false) console.log('error al recibir mistareas', {
                    "elemento": elemento
                });
            }
            elemento = null, valor = null;
        };
        ID_1348100052.error = function(e) {
            var elemento = e,
                valor = e;
            if (false) console.log('respuesta fallida de obtenerMisTareas', {
                "datos": elemento
            });
            elemento = null, valor = null;
        };
        require('helper').ajaxUnico('ID_1348100052', '' + String.format(L('x2334609226', '%1$sobtenerMisTareas'), url_server.toString()) + '', 'POST', {
            id_inspector: inspector.id_server,
            lat: gps_latitud,
            lon: gps_longitud
        }, 15000, ID_1348100052);
        var ID_1477541591 = {};
        if (false) console.log('DEBUG WEB: requesting url:' + String.format(L('x1654875529', '%1$sobtenerTareasEntrantes'), url_server.toString()) + ' with data:', {
            _method: 'POST',
            _params: {
                id_inspector: inspector.id_server,
                lat: gps_latitud,
                lon: gps_longitud
            },
            _timeout: '15000'
        });
        ID_1477541591.success = function(e) {
            var elemento = e,
                valor = e;
            if (elemento.error != 0 && elemento.error != '0') {
                if (false) console.log('error de servidor en obtenerTareasEntrntes', {
                    "elemento": elemento
                });
                var ID_984540120_opts = [L('x1518866076_traducir', 'Aceptar')];
                var ID_984540120 = Ti.UI.createAlertDialog({
                    title: L('x57652245_traducir', 'Alerta'),
                    message: L('x344160560_traducir', 'Error con el servidor'),
                    buttonNames: ID_984540120_opts
                });
                ID_984540120.addEventListener('click', function(e) {
                    var errori = ID_984540120_opts[e.index];
                    errori = null;
                    e.source.removeEventListener("click", arguments.callee);
                });
                ID_984540120.show();
            } else {
                var ID_1355973319_i = Alloy.Collections.tareas_entrantes;
                var sql = "DELETE FROM " + ID_1355973319_i.config.adapter.collection_name;
                var db = Ti.Database.open(ID_1355973319_i.config.adapter.db_name);
                db.execute(sql);
                db.close();
                ID_1355973319_i.trigger('remove');
                if (false) console.log('Respuesta de servidor TareasEntrantes (sin error)', {
                    "elemento": elemento
                });
                var elemento_critica = elemento.critica;
                var ID_1376170820_m = Alloy.Collections.tareas_entrantes;
                var db_ID_1376170820 = Ti.Database.open(ID_1376170820_m.config.adapter.db_name);
                db_ID_1376170820.execute('BEGIN');
                _.each(elemento_critica, function(ID_1376170820_fila, pos) {
                    db_ID_1376170820.execute('INSERT INTO tareas_entrantes (fecha_tarea, id_inspeccion, id_asegurado, nivel_2, comentario_can_o_rech, asegurado_tel_fijo, estado_tarea, bono, evento, id_inspector, asegurado_codigo_identificador, lat, nivel_1, asegurado_nombre, pais, direccion, asegurador, fecha_ingreso, fecha_siniestro, nivel_1_texto, distance, nivel_4, perfil, asegurado_id, pais_texto, id_server, categoria, nivel_3, asegurado_correo, num_caso, lon, asegurado_tel_movil, tipo_tarea, nivel_5) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', ID_1376170820_fila.fecha_tarea, ID_1376170820_fila.id_inspeccion, ID_1376170820_fila.id_asegurado, ID_1376170820_fila.nivel_2, '-', ID_1376170820_fila.asegurado_tel_fijo, ID_1376170820_fila.estado_tarea, ID_1376170820_fila.bono, ID_1376170820_fila.evento, ID_1376170820_fila.id_inspector, ID_1376170820_fila.asegurado_codigo_identificador, ID_1376170820_fila.lat, ID_1376170820_fila.nivel_1, ID_1376170820_fila.asegurado_nombre, -1, ID_1376170820_fila.direccion, ID_1376170820_fila.asegurador, ID_1376170820_fila.fecha_ingreso, ID_1376170820_fila.fecha_siniestro, ID_1376170820_fila.nivel_1_, ID_1376170820_fila.distancia, ID_1376170820_fila.nivel_4, 'casa', ID_1376170820_fila.asegurado_id, ID_1376170820_fila.pais_, ID_1376170820_fila.id, ID_1376170820_fila.categoria, ID_1376170820_fila.nivel_3, ID_1376170820_fila.asegurado_correo, ID_1376170820_fila.num_caso, ID_1376170820_fila.lon, ID_1376170820_fila.asegurado_tel_movil, ID_1376170820_fila.tipo_tarea, ID_1376170820_fila.nivel_5);
                });
                db_ID_1376170820.execute('COMMIT');
                db_ID_1376170820.close();
                db_ID_1376170820 = null;
                ID_1376170820_m.trigger('change');
                var elemento_normal = elemento.normal;
                var ID_164163396_m = Alloy.Collections.tareas_entrantes;
                var db_ID_164163396 = Ti.Database.open(ID_164163396_m.config.adapter.db_name);
                db_ID_164163396.execute('BEGIN');
                _.each(elemento_normal, function(ID_164163396_fila, pos) {
                    db_ID_164163396.execute('INSERT INTO tareas_entrantes (fecha_tarea, id_inspeccion, id_asegurado, nivel_2, comentario_can_o_rech, asegurado_tel_fijo, estado_tarea, bono, evento, id_inspector, asegurado_codigo_identificador, lat, nivel_1, asegurado_nombre, pais, direccion, asegurador, fecha_ingreso, fecha_siniestro, nivel_1_texto, distance, nivel_4, perfil, asegurado_id, pais_texto, id_server, categoria, nivel_3, asegurado_correo, num_caso, lon, asegurado_tel_movil, tipo_tarea, nivel_5) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', ID_164163396_fila.fecha_tarea, ID_164163396_fila.id_inspeccion, ID_164163396_fila.id_asegurado, ID_164163396_fila.nivel_2, '-', ID_164163396_fila.asegurado_tel_fijo, ID_164163396_fila.estado_tarea, ID_164163396_fila.bono, ID_164163396_fila.evento, ID_164163396_fila.id_inspector, ID_164163396_fila.asegurado_codigo_identificador, ID_164163396_fila.lat, ID_164163396_fila.nivel_1, ID_164163396_fila.asegurado_nombre, -1, ID_164163396_fila.direccion, ID_164163396_fila.asegurador, ID_164163396_fila.fecha_ingreso, ID_164163396_fila.fecha_siniestro, ID_164163396_fila.nivel_1_, ID_164163396_fila.distancia, ID_164163396_fila.nivel_4, 'casa', ID_164163396_fila.asegurado_id, ID_164163396_fila.pais_, ID_164163396_fila.id, ID_164163396_fila.categoria, ID_164163396_fila.nivel_3, ID_164163396_fila.asegurado_correo, ID_164163396_fila.num_caso, ID_164163396_fila.lon, ID_164163396_fila.asegurado_tel_movil, ID_164163396_fila.tipo_tarea, ID_164163396_fila.nivel_5);
                });
                db_ID_164163396.execute('COMMIT');
                db_ID_164163396.close();
                db_ID_164163396 = null;
                ID_164163396_m.trigger('change');
                Alloy.Events.trigger('_refrescar_tareas_entrantes');
                require('vars')['obtenerentrantes'] = L('x734881840_traducir', 'false');
            }
            elemento = null, valor = null;
        };
        ID_1477541591.error = function(e) {
            var elemento = e,
                valor = e;
            if (false) console.log('Hubo una falla al llamar servicio obtenerTareasEntrantes', {});
            var ID_1412699298_opts = [L('x1518866076_traducir', 'Aceptar')];
            var ID_1412699298 = Ti.UI.createAlertDialog({
                title: L('x57652245_traducir', 'Alerta'),
                message: L('x3389756565_traducir', 'No se pudo conectar con el servidor'),
                buttonNames: ID_1412699298_opts
            });
            ID_1412699298.addEventListener('click', function(e) {
                var errori = ID_1412699298_opts[e.index];
                errori = null;
                e.source.removeEventListener("click", arguments.callee);
            });
            ID_1412699298.show();
            elemento = null, valor = null;
        };
        require('helper').ajaxUnico('ID_1477541591', '' + String.format(L('x1654875529', '%1$sobtenerTareasEntrantes'), url_server.toString()) + '', 'POST', {
            id_inspector: inspector.id_server,
            lat: gps_latitud,
            lon: gps_longitud
        }, 15000, ID_1477541591);
        var ID_1772162362 = {};
        ID_1772162362.success = function(e) {
            var elemento = e,
                valor = e;
            var ID_1827806984_i = Alloy.Collections.emergencia;
            var sql = "DELETE FROM " + ID_1827806984_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_1827806984_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            ID_1827806984_i.trigger('remove');
            if (elemento.error != 0 && elemento.error != '0') {
                var ID_1578589616_opts = [L('x1518866076_traducir', 'Aceptar')];
                var ID_1578589616 = Ti.UI.createAlertDialog({
                    title: L('x57652245_traducir', 'Alerta'),
                    message: L('x344160560_traducir', 'Error con el servidor'),
                    buttonNames: ID_1578589616_opts
                });
                ID_1578589616.addEventListener('click', function(e) {
                    var errori = ID_1578589616_opts[e.index];
                    errori = null;
                    e.source.removeEventListener("click", arguments.callee);
                });
                ID_1578589616.show();
            } else {
                if (false) console.log('Insertando emergencias', {
                    "elemento": elemento
                });
                var elemento_perfil = elemento.perfil;
                var ID_1937077707_m = Alloy.Collections.emergencia;
                var db_ID_1937077707 = Ti.Database.open(ID_1937077707_m.config.adapter.db_name);
                db_ID_1937077707.execute('BEGIN');
                _.each(elemento_perfil, function(ID_1937077707_fila, pos) {
                    db_ID_1937077707.execute('INSERT INTO emergencia (fecha_tarea, id_inspeccion, nivel_2, id_asegurado, comentario_can_o_rech, asegurado_tel_fijo, estado_tarea, bono, id_inspector, asegurado_codigo_identificador, lat, nivel_1, asegurado_nombre, pais, direccion, asegurador, fecha_ingreso, fecha_siniestro, nivel_1_texto, distance, nivel_4, perfil, asegurado_id, pais_texto, id_server, categoria, nivel_3, asegurado_correo, num_caso, lon, asegurado_tel_movil, distancia_2, nivel_5, tipo_tarea) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', ID_1937077707_fila.fecha_tarea, ID_1937077707_fila.id_inspeccion, ID_1937077707_fila.nivel_2, ID_1937077707_fila.id_asegurado, ID_1937077707_fila.comentario_can_o_rech, ID_1937077707_fila.asegurado_tel_fijo, ID_1937077707_fila.estado_tarea, ID_1937077707_fila.bono, ID_1937077707_fila.id_inspector, ID_1937077707_fila.asegurado_codigo_identificador, ID_1937077707_fila.lat, ID_1937077707_fila.nivel_1, ID_1937077707_fila.asegurado_nombre, ID_1937077707_fila.pais, ID_1937077707_fila.direccion, ID_1937077707_fila.asegurador, ID_1937077707_fila.fecha_ingreso, ID_1937077707_fila.fecha_siniestro, ID_1937077707_fila.nivel_1_google, ID_1937077707_fila.distancia, ID_1937077707_fila.nivel_4, 'casa', ID_1937077707_fila.asegurado_id, ID_1937077707_fila.pais_, ID_1937077707_fila.id, ID_1937077707_fila.categoria, ID_1937077707_fila.nivel_3, ID_1937077707_fila.asegurado_correo, ID_1937077707_fila.num_caso, ID_1937077707_fila.lon, ID_1937077707_fila.asegurado_tel_movil, ID_1937077707_fila.distancia_2, ID_1937077707_fila.nivel_5, ID_1937077707_fila.tipo_tarea);
                });
                db_ID_1937077707.execute('COMMIT');
                db_ID_1937077707.close();
                db_ID_1937077707 = null;
                ID_1937077707_m.trigger('change');
                var elemento_ubicacion = elemento.ubicacion;
                var ID_507245659_m = Alloy.Collections.emergencia;
                var db_ID_507245659 = Ti.Database.open(ID_507245659_m.config.adapter.db_name);
                db_ID_507245659.execute('BEGIN');
                _.each(elemento_ubicacion, function(ID_507245659_fila, pos) {
                    db_ID_507245659.execute('INSERT INTO emergencia (fecha_tarea, id_inspeccion, id_asegurado, nivel_2, comentario_can_o_rech, asegurado_tel_fijo, estado_tarea, bono, id_inspector, asegurado_codigo_identificador, lat, nivel_1, asegurado_nombre, pais, direccion, asegurador, fecha_ingreso, fecha_siniestro, nivel_1_texto, distance, nivel_4, perfil, asegurado_id, pais_texto, id_server, categoria, nivel_3, asegurado_correo, num_caso, lon, asegurado_tel_movil, distancia_2, tipo_tarea, nivel_5) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', ID_507245659_fila.fecha_tarea, ID_507245659_fila.id_inspeccion, ID_507245659_fila.id_asegurado, ID_507245659_fila.nivel_2, ID_507245659_fila.comentario_can_o_rech, ID_507245659_fila.asegurado_tel_fijo, ID_507245659_fila.estado_tarea, ID_507245659_fila.bono, ID_507245659_fila.id_inspector, ID_507245659_fila.asegurado_codigo_identificador, ID_507245659_fila.lat, ID_507245659_fila.nivel_1, ID_507245659_fila.asegurado_nombre, ID_507245659_fila.pais, ID_507245659_fila.direccion, ID_507245659_fila.asegurador, ID_507245659_fila.fecha_ingreso, ID_507245659_fila.fecha_siniestro, ID_507245659_fila.nivel_1_google, ID_507245659_fila.distancia, ID_507245659_fila.nivel_4, 'ubicacion', ID_507245659_fila.asegurado_id, ID_507245659_fila.pais_, ID_507245659_fila.id, ID_507245659_fila.categoria, ID_507245659_fila.nivel_3, ID_507245659_fila.asegurado_correo, ID_507245659_fila.num_caso, ID_507245659_fila.lon, ID_507245659_fila.asegurado_tel_movil, ID_507245659_fila.distancia_2, ID_507245659_fila.tipo_tarea, ID_507245659_fila.nivel_5);
                });
                db_ID_507245659.execute('COMMIT');
                db_ID_507245659.close();
                db_ID_507245659 = null;
                ID_507245659_m.trigger('change');
                require('vars')['obteneremergencias'] = L('x734881840_traducir', 'false');
                Alloy.Events.trigger('_refrescar_tareas');
            }
            elemento = null, valor = null;
        };
        ID_1772162362.error = function(e) {
            var elemento = e,
                valor = e;
            if (false) console.log('Hubo un error', {});
            var ID_801575173_opts = [L('x1518866076_traducir', 'Aceptar')];
            var ID_801575173 = Ti.UI.createAlertDialog({
                title: L('x57652245_traducir', 'Alerta'),
                message: L('x3389756565_traducir', 'No se pudo conectar con el servidor'),
                buttonNames: ID_801575173_opts
            });
            ID_801575173.addEventListener('click', function(e) {
                var errori = ID_801575173_opts[e.index];
                errori = null;
                e.source.removeEventListener("click", arguments.callee);
            });
            ID_801575173.show();
            elemento = null, valor = null;
        };
        require('helper').ajaxUnico('ID_1772162362', '' + String.format(L('x1578679327', '%1$sobtenerEmergencias'), url_server.toString()) + '', 'POST', {
            id_inspector: inspector.id_server,
            lat: gps_latitud,
            lon: gps_longitud
        }, 15000, ID_1772162362);
    }
}

$.ID_1512316616.init({
    titulo: L('x2828751865_traducir', '¡ESTAS SIN CONEXION!'),
    __id: 'ALL1512316616',
    mensaje: L('x1855928898_traducir', 'No puedes ver las tareas de emergencias de hoy'),
    onon: on_ID_1122135172,
    onoff: Off_ID_317316563
});

function on_ID_1122135172(e) {

    var evento = e;
    var ID_4076993_visible = true;

    if (ID_4076993_visible == 'si') {
        ID_4076993_visible = true;
    } else if (ID_4076993_visible == 'no') {
        ID_4076993_visible = false;
    }
    $.ID_4076993.setVisible(ID_4076993_visible);


}

function Off_ID_317316563(e) {

    var evento = e;
    var ID_4076993_visible = false;

    if (ID_4076993_visible == 'si') {
        ID_4076993_visible = true;
    } else if (ID_4076993_visible == 'no') {
        ID_4076993_visible = false;
    }
    $.ID_4076993.setVisible(ID_4076993_visible);


}

function Itemclick_ID_1569686606(e) {

    e.cancelBubble = true;
    var objeto = e.section.getItemAt(e.itemIndex);
    var modelo = {},
        _modelo = [];
    var fila = {},
        fila_bak = {},
        info = {
            _template: objeto.template,
            _what: [],
            _seccion_ref: e.section.getHeaderTitle(),
            _model_id: -1
        },
        _tmp = {
            objmap: {}
        };
    if ('itemId' in e) {
        info._model_id = e.itemId;
        modelo._id = info._model_id;
        if (info._seccion_ref != '' && info._seccion_ref in _bind4section) {
            modelo._collection = _bind4section[info._seccion_ref];
            _tmp._coll = modelo._collection;
        }
    }
    var findVariables = require('fvariables');
    _.each(_list_templates[info._template], function(obj_id, id) {
        _.each(obj_id, function(valor, prop) {
            var llaves = findVariables(valor, '{', '}');
            _.each(llaves, function(llave) {
                _tmp.objmap[llave] = {
                    id: id,
                    prop: prop
                };
                fila[llave] = objeto[id][prop];
                if (id == e.bindId) info._what.push(llave);
            });
        });
    });
    info._what = info._what.join(',');
    fila_bak = JSON.parse(JSON.stringify(fila));
    if (false) console.log('Mi objeto emergencias es', {
        "info": info,
        "objeto": fila
    });
    /** 
     * Ocupamos esta variable para impedir que la pantalla siguente se cargue dos veces 
     */
    var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
    if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
        require('vars')['var_abriendo'] = L('x4261170317', 'true');
        var nulo = Alloy.createController("tomartarea_index", {
            '_objeto': fila,
            '_id': fila.id,
            '_tipo': 'emergencias',
            '__master_model': (typeof modelo !== 'undefined') ? modelo : {},
            '__modelo': (typeof _modelo !== 'undefined' && _modelo.length > 0) ? _modelo[0] : {}
        }).getView();
        nulo.open({
            modal: true
        });
        nulo = null;
    }
    _tmp.changed = false;
    _tmp.diff_keys = [];
    _.each(fila, function(value1, prop) {
        var had_samekey = false;
        _.each(fila_bak, function(value2, prop2) {
            if (prop == prop2 && value1 == value2) {
                had_samekey = true;
            } else if (!_.has(fila_bak, prop) || !_.has(fila, prop2)) {
                has_samekey = true;
            }
        });
        if (!had_samekey) _tmp.diff_keys.push(prop);
    });
    if (_tmp.diff_keys.length > 0) _tmp.changed = true;
    if (_tmp.changed == true) {
        _.each(_tmp.diff_keys, function(llave) {
            objeto[_tmp.objmap[llave].id][_tmp.objmap[llave].prop] = fila[llave];
        });
        e.section.updateItemAt(e.itemIndex, objeto);
    }

}

(function() {
    _my_events['_refrescar_tareas,ID_1146039108'] = function(evento) {
        /** 
         * Revisamos si hay inspecciones en curso, si no hay ninguna, se refresca la lista 
         */
        var inspeccion_encurso = ('inspeccion_encurso' in require('vars')) ? require('vars')['inspeccion_encurso'] : '';
        if (inspeccion_encurso == false || inspeccion_encurso == 'false') {
            _.defer(function() {
                Alloy.Collections.emergencia.fetch();
            });
        }
    };
    Alloy.Events.on('_refrescar_tareas', _my_events['_refrescar_tareas,ID_1146039108']);
})();

if (OS_IOS || OS_ANDROID) {
    $.ID_1565575511.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}
//$.ID_1565575511.open();