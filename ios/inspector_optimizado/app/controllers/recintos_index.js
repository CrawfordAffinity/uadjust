var _bind4section={"ref1":"insp_recintos"};
var _list_templates={"pborrar":{"ID_1883915455":{"text":"{id}"},"ID_2120109018":{"text":"{nombre}"},"ID_1046919308":{},"ID_1735369570":{},"ID_1777579448":{},"ID_1254685949":{},"ID_1795050796":{},"ID_1752901998":{},"ID_1914367022":{"text":"{nombre}"},"ID_1249074865":{"text":"{id}"},"ID_1131557960":{},"ID_1361874287":{}},"contenido":{"ID_630800887":{},"ID_862352109":{"text":"{id}"},"ID_560452274":{"text":"{nombre}"}},"elemento":{"ID_432412973":{},"ID_291370846":{"text":"{nombre}"},"ID_1925487537":{"text":"{id_server}"},"ID_975786586":{"text":"{id_segured}"},"ID_1546190443":{}},"recinto":{"ID_1960582596":{},"ID_871581291":{"text":"{nombre}"},"ID_310297848":{"text":"{id}"}}};

$.ID_1393871432_window.setTitleAttributes({
color : 'WHITE'
}
);
var _activity; 
if (OS_ANDROID) { _activity = $.ID_1393871432.activity; var abx = require('com.alcoapps.actionbarextras'); }
var _my_events = {}, _out_vars = {}, $item = {}, args = arguments[0] || {}; if ('__args' in args && '__id' in args['__args']) args.__id=args['__args'].__id; if ('__modelo' in args && _.keys(args.__modelo).length>0 && 'item' in $) { $.item.set(args.__modelo); $item = $.item.toJSON(); }
var _var_scopekey = 'ID_1393871432';
require('vars')[_var_scopekey]={};
if (OS_ANDROID) {
   $.ID_1393871432_window.addEventListener('open', function(e) {
   abx.setStatusbarColor("#FFFFFF");
   abx.setBackgroundColor("#b9aaf3");
   });
}


var ID_553809963_like = function(search) {
  if (typeof search !== 'string' || this === null) {return false; }
  search = search.replace(new RegExp("([\\.\\\\\\+\\*\\?\\[\\^\\]\\$\\(\\)\\{\\}\\=\\!\\<\\>\\|\\:\\-])", "g"), "\\$1");
  search = search.replace(/%/g, '.*').replace(/_/g, '.');
  return RegExp('^' + search + '$', 'gi').test(this);
};
var ID_553809963_filter = function(coll) {
var filtered = _.toArray(coll.filter(function(m) { return true; }));
return filtered;
};
var ID_553809963_transform = function(model) {
  var fila = model.toJSON();
  return fila;
};
var ID_553809963_update = function(e) {
};
_.defer(function() { 
 Alloy.Collections.insp_recintos.fetch(); 
 });
Alloy.Collections.insp_recintos.on('add change delete', function(ee) { ID_553809963_update(ee); });
Alloy.Collections.insp_recintos.fetch();

function Click_ID_1231352015(e) {

e.cancelBubble=true;
var elemento=e.source;
ID_1119642221.show();

}
function Click_ID_808324098(e) {

e.cancelBubble=true;
var elemento=e.source;
Alloy.createController("nuevo_recinto",{}).getView().open();

}
function Swipe_ID_1361471894(e) {

e.cancelBubble=true;
var elemento=e.section.getItemAt(e.itemIndex);
var findVariables = require('fvariables');
var fila = {};
_.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
   _.each(obj_id, function(valor, prop) {
      fila[prop] = elemento[id_field][prop];
      var llaves = findVariables(valor,'{','}');
      _.each(llaves, function(llave) {
         fila[llave] = elemento[id_field][prop];
      });
   });
});
var flag_borrar = ('flag_borrar' in require('vars')[_var_scopekey])?require('vars')[_var_scopekey]['flag_borrar']:'';
if (flag_borrar==false||flag_borrar=='false') {
if (e.direction==L('x2053629800_traducir','left')) {
var findVariables = require('fvariables');
elemento.template = 'pborrar';
_.each(_list_templates['pborrar'], function(obj_id, id_field) {
   _.each(obj_id, function(valor, prop) {
      var llaves = findVariables(valor,'{','}');
      _.each(llaves, function(llave) {
         elemento[id_field] = {};
         elemento[id_field][prop] = fila[llave];
      });
   });
});
if (OS_IOS) {
   e.section.updateItemAt(e.itemIndex, elemento, { animated: true });
} else if (OS_ANDROID) {
   e.section.updateItemAt(e.itemIndex, elemento);
}
require('vars')[_var_scopekey]['flag_borrar']=L('x4261170317','true');
}
}
 else {
require('vars')[_var_scopekey]['flag_borrar']=L('x734881840_traducir','false');
_.defer(function() { 
 Alloy.Collections.insp_recintos.fetch(); 
 });
}
}
function Click_ID_404942004(e) {

e.cancelBubble=true;
var elemento=e.section.getItemAt(e.itemIndex);
var findVariables = require('fvariables');
var fila = {};
_.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
   _.each(obj_id, function(valor, prop) {
      fila[prop] = elemento[id_field][prop];
      var llaves = findVariables(valor,'{','}');
      _.each(llaves, function(llave) {
         fila[llave] = elemento[id_field][prop];
      });
   });
});
var flag_borrar = ('flag_borrar' in require('vars')[_var_scopekey])?require('vars')[_var_scopekey]['flag_borrar']:'';
if (flag_borrar==false||flag_borrar=='false') {
/** 
* Ocupamos esta variable para impedir que la pantalla siguente se cargue dos veces 
*/
var var_abriendo = ('var_abriendo' in require('vars'))?require('vars')['var_abriendo']:'';
if ((_.isObject(var_abriendo) ||_.isString(var_abriendo)) &&  _.isEmpty(var_abriendo)) {
require('vars')['var_abriendo']=L('x4261170317','true');
Alloy.createController("nuevo_recinto",{'_dato' : fila}).getView().open();
}
}
 else {
_.defer(function() { 
 Alloy.Collections.insp_recintos.fetch(); 
 });
require('vars')[_var_scopekey]['flag_borrar']=L('x734881840_traducir','false');
}
}
function Click_ID_678725846(e) {

e.cancelBubble=true;
var elemento=e.section.getItemAt(e.itemIndex);
var findVariables = require('fvariables');
var fila = {};
_.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
   _.each(obj_id, function(valor, prop) {
      fila[prop] = elemento[id_field][prop];
      var llaves = findVariables(valor,'{','}');
      _.each(llaves, function(llave) {
         fila[llave] = elemento[id_field][prop];
      });
   });
});
var ID_1942302623_opts=[L('x3827418516_traducir','Si'),L('x1962639792_traducir',' No')];
var ID_1942302623 = Ti.UI.createAlertDialog({
   title: L('x4097537701_traducir','ALERTA'),
   message: ''+String.format(L('x4127782287_traducir','¿ Seguro desea eliminar: %1$s ?'), fila.nombre.toString())+'',
   buttonNames: ID_1942302623_opts
});
ID_1942302623.addEventListener('click', function(e) {
   var xd=ID_1942302623_opts[e.index];
if (xd==L('x3827418516_traducir','Si')) {
var ID_1192637905_i=Alloy.Collections.insp_recintos;
var sql = 'DELETE FROM ' + ID_1192637905_i.config.adapter.collection_name + ' WHERE id=\''+fila.id+'\'';
var db = Ti.Database.open(ID_1192637905_i.config.adapter.db_name);
db.execute(sql);
db.close();
ID_1192637905_i.trigger('delete');
require('vars')[_var_scopekey]['flag_borrar']=L('x734881840_traducir','false');
_.defer(function() { 
 Alloy.Collections.insp_recintos.fetch(); 
 });
}
 else {
require('vars')[_var_scopekey]['flag_borrar']=L('x734881840_traducir','false');
_.defer(function() { 
 Alloy.Collections.insp_recintos.fetch(); 
 });
}xd = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_1942302623.show();

}
function Swipe_ID_746041771(e) {

e.cancelBubble=true;
var elemento=e.section.getItemAt(e.itemIndex);
var findVariables = require('fvariables');
var fila = {};
_.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
   _.each(obj_id, function(valor, prop) {
      fila[prop] = elemento[id_field][prop];
      var llaves = findVariables(valor,'{','}');
      _.each(llaves, function(llave) {
         fila[llave] = elemento[id_field][prop];
      });
   });
});
if (e.direction==L('x3033167124_traducir','right')) {
var findVariables = require('fvariables');
elemento.template = 'recinto';
_.each(_list_templates['recinto'], function(obj_id, id_field) {
   _.each(obj_id, function(valor, prop) {
      var llaves = findVariables(valor,'{','}');
      _.each(llaves, function(llave) {
         elemento[id_field] = {};
         elemento[id_field][prop] = fila[llave];
      });
   });
});
if (OS_IOS) {
   e.section.updateItemAt(e.itemIndex, elemento, { animated: true });
} else if (OS_ANDROID) {
   e.section.updateItemAt(e.itemIndex, elemento);
}
require('vars')[_var_scopekey]['flag_borrar']=L('x734881840_traducir','false');
}

}
function Click_ID_1533715671(e) {

e.cancelBubble=true;
var elemento=e.section.getItemAt(e.itemIndex);
var findVariables = require('fvariables');
var fila = {};
_.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
   _.each(obj_id, function(valor, prop) {
      fila[prop] = elemento[id_field][prop];
      var llaves = findVariables(valor,'{','}');
      _.each(llaves, function(llave) {
         fila[llave] = elemento[id_field][prop];
      });
   });
});
require('vars')[_var_scopekey]['flag_borrar']=L('x734881840_traducir','false');
_.defer(function() { 
 Alloy.Collections.insp_recintos.trigger('change'); 
 });

}

$.ID_1007469789.init({
titulo : L('x1678967761_traducir','GUARDAR RECINTOS'),
__id : 'ALL1007469789',
color : 'verde',
onclick : Click_ID_1166599192
}
);

function Click_ID_1166599192(e) {

var evento=e;
require('vars')[_var_scopekey]['todobien']=L('x4261170317','true');
var seltarea = ('seltarea' in require('vars'))?require('vars')['seltarea']:'';
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
/** 
* Revisamos que haya al menos un registro de recinto ingresado 
*/
var ID_1829089847_i=Alloy.createCollection('insp_recintos');
var ID_1829089847_i_where='id_inspeccion=\''+seltarea.id_server+'\'';
ID_1829089847_i.fetch({ query: 'SELECT * FROM insp_recintos WHERE id_inspeccion=\''+seltarea.id_server+'\'' });
var cantidad=require('helper').query2array(ID_1829089847_i);
if (cantidad && cantidad.length==0) {
require('vars')[_var_scopekey]['todobien']=L('x734881840_traducir','false');
var ID_367898160_opts=[L('x1518866076_traducir','Aceptar')];
var ID_367898160 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x1841578175_traducir','Debe ingresar al menos un recinto'),
   buttonNames: ID_367898160_opts
});
ID_367898160.addEventListener('click', function(e) {
   var nulo=ID_367898160_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_367898160.show();
}
}
 else {
var ID_1995064223_i=Alloy.createCollection('insp_recintos');
var ID_1995064223_i_where='id_inspeccion=\''+seltarea.id_server+'\'';
ID_1995064223_i.fetch({ query: 'SELECT * FROM insp_recintos WHERE id_inspeccion=\''+seltarea.id_server+'\'' });
var cantidad=require('helper').query2array(ID_1995064223_i);
if (cantidad && cantidad.length==0) {
require('vars')[_var_scopekey]['todobien']=L('x734881840_traducir','false');
var ID_1849801000_opts=[L('x1518866076_traducir','Aceptar')];
var ID_1849801000 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x1841578175_traducir','Debe ingresar al menos un recinto'),
   buttonNames: ID_1849801000_opts
});
ID_1849801000.addEventListener('click', function(e) {
   var nulo=ID_1849801000_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_1849801000.show();
}
}var todobien = ('todobien' in require('vars')[_var_scopekey])?require('vars')[_var_scopekey]['todobien']:'';
if (todobien==true||todobien=='true') {
$.ID_1088806219.ejecutar({});
}

}

$.ID_1088806219.init({
titulo : L('x404129330_traducir','¿EL ASEGURADO CONFIRMA ESTOS DATOS?'),
__id : 'ALL1088806219',
si : L('x1723413441_traducir','SI, Están correctos'),
texto : L('x2083765231_traducir','El asegurado debe confirmar que los datos de esta sección están correctos'),
pantalla : L('x2851883104_traducir','¿ESTA DE ACUERDO?'),
onno : no_ID_766006031,
onsi : si_ID_1721647550,
no : L('x55492959_traducir','NO, Hay que modificar algo'),
header : 'morado'
}
);

function si_ID_1721647550(e) {

var evento=e;
Alloy.createController("contenidos_index",{}).getView().open();

}
function no_ID_766006031(e) {

var evento=e;

}
function Postlayout_ID_1660577326(e) {

e.cancelBubble=true;
var elemento=e.source;
$.ID_1382223177.show();

}
function Postlayout_ID_910543596(e) {

e.cancelBubble=true;
var elemento=e.source;
var anteriores = ('anteriores' in require('vars')[_var_scopekey])?require('vars')[_var_scopekey]['anteriores']:'';
if (anteriores==false||anteriores=='false') {
/** 
* Avisamos a la pantalla anterior que debe cerrarse (siniestro) 
*/
Alloy.Events.trigger('_cerrar_insp', {pantalla : L('x3772634211_traducir', 'siniestro')});
require('vars')[_var_scopekey]['anteriores']=L('x4261170317','true');
}

}

var ID_1119642221_opts=[L('x3481285698_traducir','Asegurado no puede seguir'),L('x3077268071_traducir',' Se me acabo la bateria'),L('x479989047_traducir',' Tuve un accidente'),L('x2376009830_traducir',' Cancelar')];
var ID_1119642221 = Ti.UI.createOptionDialog({
   title: L('x1670176792_traducir','RAZON PARA CANCELAR INSPECCION ACTUAL'),
   options: ID_1119642221_opts
});
ID_1119642221.addEventListener('click', function(e) {
   var resp=ID_1119642221_opts[e.index];
var seltarea = ('seltarea' in require('vars'))?require('vars')['seltarea']:'';
var inspector = ('inspector' in require('vars'))?require('vars')['inspector']:'';
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
 var razon = "";
if (resp==L('x3481285698_traducir','Asegurado no puede seguir')) {
 razon = "Asegurado no puede seguir";
}
if (resp==L('x1862794075_traducir','Se me acabo la bateria')) {
 razon = "Se me acabo la bateria";
}
if (resp==L('x4012805111_traducir','Tuve un accidente')) {
 razon = "Tuve un accidente";
}
if ((_.isObject(razon) || (_.isString(razon)) &&  !_.isEmpty(razon)) || _.isNumber(razon) || _.isBoolean(razon)) {
if(false) console.log('llamando servicio cancelarTarea',{});
require('vars')['insp_cancelada']=L('x2941610362_traducir','siniestro');
var url_server = ('url_server' in require('vars'))?require('vars')['url_server']:'';
var ID_1243651816_visible = true;

										  if (ID_1243651816_visible=='si') {
											  ID_1243651816_visible=true;
										  } else if (ID_1243651816_visible=='no') {
											  ID_1243651816_visible=false;
										  }
										  $.ID_1243651816.setVisible(ID_1243651816_visible);

var seltarea = ('seltarea' in require('vars'))?require('vars')['seltarea']:'';
var inspector = ('inspector' in require('vars'))?require('vars')['inspector']:'';
if(false) console.log('detalle de seltarea',{"data":seltarea});
var datos = {
id_inspector : inspector.id_server,
codigo_identificador : inspector.codigo_identificador,
id_server : seltarea.id_server,
num_caso : seltarea.num_caso,
razon : razon};
require('vars')[_var_scopekey]['datos']=datos;
var ID_1401462126={};
ID_1401462126.success = function(e) {
var elemento=e, valor=e;
var ID_1243651816_visible = false;

										  if (ID_1243651816_visible=='si') {
											  ID_1243651816_visible=true;
										  } else if (ID_1243651816_visible=='no') {
											  ID_1243651816_visible=false;
										  }
										  $.ID_1243651816.setVisible(ID_1243651816_visible);

Alloy.createController("firma_index",{}).getView().open();
var ID_1871897266_func = function() {
/** 
* Cerramos pantalla datos basicos 
*/
Alloy.Events.trigger('_cerrar_insp', {pantalla : L('x3174879261_traducir', 'recintos')});
};
var ID_1871897266 = setTimeout(ID_1871897266_func, 1000*0.5);
elemento=null, valor=null;
};
ID_1401462126.error = function(e) {
var elemento=e, valor=e;
if(false) console.log('hubo error llamando cancelarTarea, la agregamos a cola de salida',{"elemento":elemento});
if(false) console.log('agregando servicio cancelarTarea a cola',{});
var seltarea = ('seltarea' in require('vars'))?require('vars')['seltarea']:'';
var datos = ('datos' in require('vars')[_var_scopekey])?require('vars')[_var_scopekey]['datos']:'';
var ID_1406654726_m=Alloy.Collections.cola;
var ID_1406654726_fila = Alloy.createModel('cola', {
data : JSON.stringify(datos),
id_tarea : seltarea.id_server,
tipo : 'cancelar'
}
 );
ID_1406654726_m.add(ID_1406654726_fila);
ID_1406654726_fila.save();
_.defer(function() { 
});
var ID_1243651816_visible = false;

										  if (ID_1243651816_visible=='si') {
											  ID_1243651816_visible=true;
										  } else if (ID_1243651816_visible=='no') {
											  ID_1243651816_visible=false;
										  }
										  $.ID_1243651816.setVisible(ID_1243651816_visible);

Alloy.createController("firma_index",{}).getView().open();
/** 
* Cerramos pantalla datos basicos 
*/
Alloy.Events.trigger('_cerrar_insp', {pantalla : L('x3174879261_traducir', 'recintos')});
elemento=null, valor=null;
};
require('helper').ajaxUnico('ID_1401462126', ''+String.format(L('x3244141284','%1$scancelarTarea'), url_server.toString())+'', 'POST', { id_inspector:seltarea.id_inspector,codigo_identificador:inspector.codigo_identificador,id_tarea:seltarea.id_server,num_caso:seltarea.num_caso,mensaje:razon,opcion:0,tipo:1 }, 15000, ID_1401462126);
}
}
resp = null;
});
/** 
* Cerramos esta pantalla cuando es llamada en la pantalla siguente (contenidos) 
*/
_my_events['_cerrar_insp,ID_1905764132'] = function(evento) {
if (!_.isUndefined(evento.pantalla)) {
if (evento.pantalla==L('x3958592860_traducir','recintos')) {
if(false) console.log('debug cerrando recintos',{});
var ID_1477415526_trycatch = { error: function(e) { if(false) console.log('error en comando probar: recuerda poner evento ?error como primer hijo.',e); } };
try {
ID_1477415526_trycatch.error = function(evento) {
if(false) console.log('error cerrando recintos',{});
};
$.ID_1393871432.close();
} catch (e) {
   ID_1477415526_trycatch.error(e);
}
}
}
 else {
if(false) console.log('debug cerrando (todas) recintos',{});
var ID_1356770390_trycatch = { error: function(e) { if(false) console.log('error en comando probar: recuerda poner evento ?error como primer hijo.',e); } };
try {
ID_1356770390_trycatch.error = function(evento) {
if(false) console.log('error cerrando recintos',{});
};
$.ID_1393871432.close();
} catch (e) {
   ID_1356770390_trycatch.error(e);
}
}};
Alloy.Events.on('_cerrar_insp', _my_events['_cerrar_insp,ID_1905764132']);
var seltarea = ('seltarea' in require('vars'))?require('vars')['seltarea']:'';
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
/** 
* restringimos recintos mostrados a la inspeccion actual (por si los temporales aun tienen datos) 
*/
ID_553809963_filter = function(coll) {
var filtered = coll.filter(function(m) {
	var _tests = [], _all_true = false, model = m.toJSON();
_tests.push((model.id_inspeccion == seltarea.id_server));
	var _all_true_s = _.uniq(_tests);
	_all_true = (_all_true_s.length==1 && _all_true_s[0]==true)?true:false;
	return _all_true;
	});
filtered = _.toArray(filtered);
return filtered;
};
_.defer(function() { 
 Alloy.Collections.insp_recintos.fetch(); 
 });
}
require('vars')[_var_scopekey]['anteriores']=L('x734881840_traducir','false');
require('vars')[_var_scopekey]['flag_borrar']=L('x734881840_traducir','false');

if (OS_IOS || OS_ANDROID) {
$.ID_1393871432.addEventListener('close',function(){
   $.destroy(); // cleanup bindings
   $.off(); //remove backbone events of this controller
   var _ev_tmp = null, _ev_rem = null;
   if (_my_events) {
      for(_ev_tmp in _my_events) { 
   		try {
   		    if (_ev_tmp.indexOf('_web')!=-1) {
   			   Ti.App.removeEventListener(_ev_tmp.split(',')[0],_my_events[_ev_tmp]);
   		    } else {
   			   Alloy.Events.off(_ev_tmp.split(',')[0],_my_events[_ev_tmp]);
   		    }
   		} catch(err10) {
   		}
      }
      _my_events = null;
      //delete _my_events;
   }
   if (_out_vars) {
      for(_ev_tmp in _out_vars) { 
         for (_ev_rem in _out_vars[_ev_tmp]._remove) {
            try {
 eval(_out_vars[_ev_tmp]._remove[_ev_rem]); 
 } catch(_errt) {}
         }
         _out_vars[_ev_tmp] = null;
      }
      _ev_tmp = null;
      //delete _out_vars;
   }
});
}
//$.ID_1393871432.open();