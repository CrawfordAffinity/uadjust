var _bind4section = {};
var _list_templates = {
    "pborrar": {
        "ID_1883915455": {
            "text": "{id}"
        },
        "ID_2120109018": {
            "text": "{nombre}"
        },
        "ID_1046919308": {},
        "ID_1735369570": {},
        "ID_1777579448": {},
        "ID_1254685949": {},
        "ID_1795050796": {},
        "ID_1752901998": {},
        "ID_1914367022": {
            "text": "{nombre}"
        },
        "ID_1249074865": {
            "text": "{id}"
        },
        "ID_1131557960": {},
        "ID_1361874287": {},
        "ID_1421561173": {},
        "ID_1142337614": {},
        "ID_1057712478": {
            "text": "{nombre}"
        },
        "ID_1942170367": {
            "text": "{id}"
        },
        "ID_1502687579": {},
        "ID_1852819254": {}
    },
    "contenido": {
        "ID_630800887": {},
        "ID_862352109": {
            "text": "{id}"
        },
        "ID_560452274": {
            "text": "{nombre}"
        }
    },
    "elemento": {
        "ID_432412973": {},
        "ID_291370846": {
            "text": "{nombre}"
        },
        "ID_1925487537": {
            "text": "{id_server}"
        },
        "ID_975786586": {
            "text": "{id_segured}"
        },
        "ID_1546190443": {}
    },
    "recinto": {
        "ID_1960582596": {},
        "ID_871581291": {
            "text": "{nombre}"
        },
        "ID_310297848": {
            "text": "{id}"
        }
    },
    "nivel": {
        "ID_1021786928": {
            "text": "{id}"
        },
        "ID_1241655463": {
            "text": "{nombre}"
        },
        "ID_599581692": {}
    }
};
var $fotosr = $.fotosr.toJSON();

$.ID_1465840618_window.setTitleAttributes({
    color: 'WHITE'
});
var _activity;
if (OS_ANDROID) {
    _activity = $.ID_1465840618.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_1465840618';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_1465840618_window.addEventListener('open', function(e) {
        abx.setStatusbarColor("#FFFFFF");
        abx.setBackgroundColor("#2d9edb");
    });
}

function Click_ID_1741724357(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    /** 
     * Escondemos camara 
     */
    ID_1869868607.hide();
    /** 
     * Cerramos pantalla actual 
     */
    $.ID_1465840618.close();

}

function Click_ID_794018501(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    /** 
     * Recuperamos variable de el objeto para saber si las fotos minimas fueron capturadas, si no estan, avisamos al usuario que foto falta 
     */
    var requeridas = ('requeridas' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['requeridas'] : '';
    if (requeridas.foto1 == true || requeridas.foto1 == 'true') {
        var ID_1967487035_opts = [L('x1518866076_traducir', 'Aceptar')];
        var ID_1967487035 = Ti.UI.createAlertDialog({
            title: L('x57652245_traducir', 'Alerta'),
            message: L('x305784584_traducir', 'Falta la foto de fachada'),
            buttonNames: ID_1967487035_opts
        });
        ID_1967487035.addEventListener('click', function(e) {
            var pre = ID_1967487035_opts[e.index];
            pre = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1967487035.show();
    } else if (requeridas.foto2 == true || requeridas.foto2 == 'true') {
        var ID_662469891_opts = [L('x1518866076_traducir', 'Aceptar')];
        var ID_662469891 = Ti.UI.createAlertDialog({
            title: L('x57652245_traducir', 'Alerta'),
            message: L('x3294013631_traducir', 'Falta la foto del barrio'),
            buttonNames: ID_662469891_opts
        });
        ID_662469891.addEventListener('click', function(e) {
            var pre = ID_662469891_opts[e.index];
            pre = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_662469891.show();
    } else if (requeridas.foto3 == true || requeridas.foto3 == 'true') {
        var ID_317679681_opts = [L('x1518866076_traducir', 'Aceptar')];
        var ID_317679681 = Ti.UI.createAlertDialog({
            title: L('x57652245_traducir', 'Alerta'),
            message: L('x2286900918_traducir', 'Falta la foto de número de domicilio'),
            buttonNames: ID_317679681_opts
        });
        ID_317679681.addEventListener('click', function(e) {
            var pre = ID_317679681_opts[e.index];
            pre = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_317679681.show();
    } else {
        /** 
         * Escondemos camara 
         */
        ID_1869868607.hide();
        /** 
         * Guardamos el modelo 
         */
        Alloy.Collections[$.fotosr.config.adapter.collection_name].add($.fotosr);
        $.fotosr.save();
        Alloy.Collections[$.fotosr.config.adapter.collection_name].fetch();
        var nulo = Alloy.createController("hayalguien_index", {}).getView();
        nulo.open({
            modal: true,
            modalTransitionStyle: Ti.UI.iOS.MODAL_TRANSITION_STYLE_PARTIAL_CURL
        });
        nulo = null;
    }
}

if (OS_IOS) {
    var SquareCamera = require('com.mfogg.squarecamera');
    var ID_1869868607 = SquareCamera.createView({
        height: Ti.Platform.displayCaps.platformWidth,
        left: 0,
        frameDuration: 16,
        width: Ti.Platform.displayCaps.platformWidth,
        detectCodes: true,
        top: '64dp'
    });
} else if (OS_ANDROID) {
    var ID_1869868607 = Titanium.UI.createView({
        height: Ti.UI.SIZE,
        left: 0,
        width: Ti.UI.SIZE,
        top: '64dp'
    });
    var ID_1869868607_camera = {};
    var ID_1869868607_addcam = function() {
        var SquareCamera = require('pw.custom.androidcamera');
        ID_1869868607_camera = SquareCamera.createCameraView({
            height: Ti.UI.SIZE,
            pictureTimeout: 200,
            width: Ti.UI.SIZE,
            top: '64dp',
            save_location: 'camera'
        });
        ID_1869868607.add(ID_1869868607_camera);
        ID_1869868607_camera.addEventListener('picture_taken', function(evt) {
            ID_1869868607.fireEvent('success', {
                media: evt.path
            });
        });
    };
    if (Ti.Media.isCameraSupported) {
        if (Ti.Media.hasCameraPermissions()) {
            ID_1869868607.addcam();
        } else {
            Ti.Media.requestCameraPermissions(function(e) {
                if (e.success === true) {
                    ID_1869868607.addcam();
                } else {
                    ID_1869868607.fireEvent('error', {
                        type: 'permission',
                        message: 'permission denied'
                    });
                }
            });
        }
    };
}
ID_1869868607.addEventListener('success', function(e) {
    e.cancelBubble = true;
    var elemento = e.media;
    var valor = e.media;
    if (false) console.log('foto exitosa', {
        "elemento": elemento
    });
    if (OS_ANDROID) {
        var ID_83804430_imagefactory = require('ti.imagefactory');
        var comprimida = ID_83804430_imagefactory.compress(elemento, 0.9);
    } else if (OS_IOS) {
        var ID_83804430_imagefactory = require('ti.imagefactory');
        var comprimida = ID_83804430_imagefactory.compress(elemento, 0.9);
    }
    var foto = ('foto' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['foto'] : '';
    if (foto == 1 || foto == '1') {
        $.ID_1189437781.procesar({
            imagen1: comprimida,
            nueva: 640,
            calidad: 91
        });
    } else if (foto == 2) {
        $.ID_1189437781.procesar({
            imagen2: comprimida,
            nueva: 640,
            calidad: 91
        });
    } else if (foto == 3) {
        $.ID_1189437781.procesar({
            imagen3: comprimida,
            nueva: 640,
            calidad: 91
        });
    } else if (foto == 4) {
        $.ID_1189437781.procesar({
            imagen4: comprimida,
            nueva: 640,
            calidad: 91
        });
    }
});
ID_1869868607.addEventListener('code', function(e) {
    e.cancelBubble = true;
    var elemento = {
        tipo: e.codeType,
        valor: e.value
    };
    var valor = e.value;
    if (false) console.log('evento codigo', {
        "elemento": elemento
    });
});
$.ID_1465840618.add(ID_1869868607);

function Click_ID_1759745862(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    /** 
     * Capturamos la foto 
     */
    var ID_1869868607_capturar = 'foto';

    var capturar = function(tipo) {
        if (tipo == 'foto' || tipo == 'imagen' || tipo == 'fotografia') {
            if (OS_IOS) {
                ID_1869868607.takePhoto();
            } else {
                ID_1869868607_camera.snapPicture();
            }
        } else if (tipo == 'video') {
            //ID_1869868607.startRecording();
        }
    };
    capturar(ID_1869868607_capturar);

    /** 
     * Escondemos la vista para prevenir que el usuario tome multiples fotos a la vez, y evitar que la aplicacion se cierre 
     */
    var ID_1135315933_visible = false;

    if (ID_1135315933_visible == 'si') {
        ID_1135315933_visible = true;
    } else if (ID_1135315933_visible == 'no') {
        ID_1135315933_visible = false;
    }
    $.ID_1135315933.setVisible(ID_1135315933_visible);


}

function Click_ID_654224617(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    /** 
     * Revisamos el flag de posicion de camara (frontal o trasera) y cambiamos la camara 
     */
    var camara = ('camara' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['camara'] : '';
    if (camara == L('x2010748274_traducir', 'trasera')) {
        if (false) console.log('estoy en trasera', {});
        /** 
         * Flag para saber que tiene que cambiar a frontal 
         */
        require('vars')[_var_scopekey]['camara'] = L('x2556550622_traducir', 'frontal');
        var ID_1869868607_camara = 'frontal';

        var modificarCamara = function(tipo) {
            if (OS_IOS) {
                if (tipo == 'frontal') {
                    ID_1869868607.setCamera('front');
                } else {
                    ID_1869868607.setCamera('back');
                }
            }
        };
        modificarCamara(ID_1869868607_camara);

    }
    if (camara == L('x2556550622_traducir', 'frontal')) {
        if (false) console.log('estoy en frontal', {});
        require('vars')[_var_scopekey]['camara'] = L('x2010748274_traducir', 'trasera');
        var ID_1869868607_camara = 'trasera';

        var modificarCamara = function(tipo) {
            if (OS_IOS) {
                if (tipo == 'frontal') {
                    ID_1869868607.setCamera('front');
                } else {
                    ID_1869868607.setCamera('back');
                }
            }
        };
        modificarCamara(ID_1869868607_camara);

    }

}

$.ID_1189437781.init({
    __id: 'ALL1189437781',
    onlisto: Listo_ID_572098029,
    label1: L('x724488137_traducir', 'Fachada'),
    label4: L('x676841594_traducir', 'Nº Depto'),
    label3: L('x4076266664_traducir', 'Numero'),
    onclick: Click_ID_1657845092,
    label2: L('x2825989175_traducir', 'Barrio')
});

function Click_ID_1657845092(e) {

    var evento = e;
    require('vars')[_var_scopekey]['foto'] = evento.pos;
    /** 
     * Habilitamos boton tomar foto 
     */
    var ID_1135315933_visible = true;

    if (ID_1135315933_visible == 'si') {
        ID_1135315933_visible = true;
    } else if (ID_1135315933_visible == 'no') {
        ID_1135315933_visible = false;
    }
    $.ID_1135315933.setVisible(ID_1135315933_visible);


}

function Listo_ID_572098029(e) {

    var evento = e;
    if (false) console.log('imagen procesada obtenida 4fotos', {
        "evento": evento
    });
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    if (_.isString(seltarea)) {
        /** 
         * Definimos valor inventado id_server para testing 
         */
        var seltarea = {
            id_server: 123
        };
    }
    var previos = [];
    var ID_587641544_f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
    if (ID_587641544_f.exists() == true) {
        previos = ID_587641544_f.getDirectoryListing();
    }
    if (false) console.log('archivos previos existentes en telefono', {
        "previos": previos
    });
    var requeridas = ('requeridas' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['requeridas'] : '';
    if (evento.pos == 1 || evento.pos == '1') {
        /** 
         * Cambiamos estado de foto1 para avisar que tenemos foto capturada 
         */
        var requeridas = _.extend(requeridas, {
            foto1: L('x734881840_traducir', 'false')
        });
        /** 
         * Guardamos la foto en la memoria del celular 
         */
        var ID_1478411726_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
        if (ID_1478411726_d.exists() == false) ID_1478411726_d.createDirectory();
        var ID_1478411726_f = Ti.Filesystem.getFile(ID_1478411726_d.resolve(), 'foto1.jpg');
        if (ID_1478411726_f.exists() == true) ID_1478411726_f.deleteFile();
        ID_1478411726_f.write(evento.nueva);
        ID_1478411726_d = null;
        ID_1478411726_f = null;
        /** 
         * Actualizamos el modelo avisando que la foto de fachada, el archivo se llama foto1 
         */
        $.fotosr.set({
            fachada: 'foto1.jpg'
        });
        if ('fotosr' in $) $fotosr = $.fotosr.toJSON();
    } else if (evento.pos == 2) {
        var requeridas = _.extend(requeridas, {
            foto2: L('x734881840_traducir', 'false')
        });
        var ID_1597663409_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
        if (ID_1597663409_d.exists() == false) ID_1597663409_d.createDirectory();
        var ID_1597663409_f = Ti.Filesystem.getFile(ID_1597663409_d.resolve(), 'foto2.jpg');
        if (ID_1597663409_f.exists() == true) ID_1597663409_f.deleteFile();
        ID_1597663409_f.write(evento.nueva);
        ID_1597663409_d = null;
        ID_1597663409_f = null;
        $.fotosr.set({
            barrio: 'foto2.jpg'
        });
        if ('fotosr' in $) $fotosr = $.fotosr.toJSON();
    } else if (evento.pos == 3) {
        var requeridas = _.extend(requeridas, {
            foto3: L('x734881840_traducir', 'false')
        });
        var ID_372013953_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
        if (ID_372013953_d.exists() == false) ID_372013953_d.createDirectory();
        var ID_372013953_f = Ti.Filesystem.getFile(ID_372013953_d.resolve(), 'foto3.jpg');
        if (ID_372013953_f.exists() == true) ID_372013953_f.deleteFile();
        ID_372013953_f.write(evento.nueva);
        ID_372013953_d = null;
        ID_372013953_f = null;
        $.fotosr.set({
            numero: 'foto3.jpg'
        });
        if ('fotosr' in $) $fotosr = $.fotosr.toJSON();
    } else if (evento.pos == 4) {
        var requeridas = _.extend(requeridas, {
            foto4: L('x734881840_traducir', 'false')
        });
        var ID_562700504_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
        if (ID_562700504_d.exists() == false) ID_562700504_d.createDirectory();
        var ID_562700504_f = Ti.Filesystem.getFile(ID_562700504_d.resolve(), 'foto4.jpg');
        if (ID_562700504_f.exists() == true) ID_562700504_f.deleteFile();
        ID_562700504_f.write(evento.nueva);
        ID_562700504_d = null;
        ID_562700504_f = null;
        $.fotosr.set({
            depto: 'foto4.jpg'
        });
        if ('fotosr' in $) $fotosr = $.fotosr.toJSON();
    }
    require('vars')[_var_scopekey]['requeridas'] = requeridas;
    /** 
     * Habilitamos boton tomar foto 
     */
    var ID_1135315933_visible = true;

    if (ID_1135315933_visible == 'si') {
        ID_1135315933_visible = true;
    } else if (ID_1135315933_visible == 'no') {
        ID_1135315933_visible = false;
    }
    $.ID_1135315933.setVisible(ID_1135315933_visible);


}

function Postlayout_ID_1832570476(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    /** 
     * Avisamos que se debe cerrar pantalla detalle de tarea 
     */
    Alloy.Events.trigger('_cerrar_insp', {
        pantalla: L('x887781275_traducir', 'detalle')
    });

}

(function() {
    /** 
     * Seteamos flag para saber que se inicia en la camara trasera 
     */
    require('vars')[_var_scopekey]['camara'] = L('x2010748274_traducir', 'trasera');
    /** 
     * Creamos objeto para poder saber si el minimo de fotos obligatorias existen 
     */
    var requeridas = {
        foto1: L('x4261170317', 'true'),
        foto2: L('x4261170317', 'true'),
        foto3: L('x4261170317', 'true'),
        foto4: L('x734881840_traducir', 'false')
    };
    require('vars')[_var_scopekey]['requeridas'] = requeridas;
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
        /** 
         * Verificamos que estemos en una inspeccion para poder modificar el id_inspeccion del modelo de fotos requeridas 
         */
        $.fotosr.set({
            id_inspeccion: seltarea.id_server
        });
        if ('fotosr' in $) $fotosr = $.fotosr.toJSON();
    }
})();


/** 
 * Cerramos esta pantalla cuando es llamada en la pantalla siguente (hayalguien) 
 */
_my_events['_cerrar_insp,ID_1749393921'] = function(evento) {
    if (!_.isUndefined(evento.pantalla)) {
        if (evento.pantalla == L('x2385878536_traducir', 'frequeridas')) {
            if (false) console.log('debug cerrando fotos requeridas', {});
            var ID_1772266102_trycatch = {
                error: function(e) {
                    if (false) console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
                }
            };
            try {
                ID_1772266102_trycatch.error = function(evento) {
                    if (false) console.log('error cerrando fotos requeridas', {});
                };
                $.ID_1465840618.close();
            } catch (e) {
                ID_1772266102_trycatch.error(e);
            }
        }
    } else {
        if (false) console.log('debug cerrando (todas) fotos requeridas', {});
        var ID_2088992205_trycatch = {
            error: function(e) {
                if (false) console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
            }
        };
        try {
            ID_2088992205_trycatch.error = function(evento) {
                if (false) console.log('error cerrando fotos requeridas', {});
            };
            $.ID_1465840618.close();
        } catch (e) {
            ID_2088992205_trycatch.error(e);
        }
    }
};
Alloy.Events.on('_cerrar_insp', _my_events['_cerrar_insp,ID_1749393921']);

if (OS_IOS || OS_ANDROID) {
    $.ID_1465840618.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}
//$.ID_1465840618.open();