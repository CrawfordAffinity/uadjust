var _bind4section = {
    "ref1": "tareas"
};
var _list_templates = {
    "pborrar": {
        "ID_1883915455": {
            "text": "{id}"
        },
        "ID_2120109018": {
            "text": "{nombre}"
        },
        "ID_1046919308": {},
        "ID_1735369570": {},
        "ID_1777579448": {},
        "ID_1254685949": {},
        "ID_1795050796": {},
        "ID_1752901998": {},
        "ID_1914367022": {
            "text": "{nombre}"
        },
        "ID_1249074865": {
            "text": "{id}"
        },
        "ID_1131557960": {},
        "ID_1361874287": {},
        "ID_1421561173": {},
        "ID_1142337614": {},
        "ID_1057712478": {
            "text": "{nombre}"
        },
        "ID_1942170367": {
            "text": "{id}"
        },
        "ID_1502687579": {},
        "ID_1852819254": {}
    },
    "contenido": {
        "ID_630800887": {},
        "ID_862352109": {
            "text": "{id}"
        },
        "ID_560452274": {
            "text": "{nombre}"
        }
    },
    "elemento": {
        "ID_432412973": {},
        "ID_291370846": {
            "text": "{nombre}"
        },
        "ID_1925487537": {
            "text": "{id_server}"
        },
        "ID_975786586": {
            "text": "{id_segured}"
        },
        "ID_1546190443": {}
    },
    "recinto": {
        "ID_1960582596": {},
        "ID_871581291": {
            "text": "{nombre}"
        },
        "ID_310297848": {
            "text": "{id}"
        }
    },
    "nivel": {
        "ID_1021786928": {
            "text": "{id}"
        },
        "ID_1241655463": {
            "text": "{nombre}"
        },
        "ID_599581692": {}
    },
    "tarea": {
        "ID_700219020": {},
        "ID_135101015": {
            "text": "{direccion}"
        },
        "ID_499822526": {},
        "ID_1113843363": {},
        "ID_184328649": {},
        "ID_1320539277": {},
        "ID_260293613": {
            "text": "a {distance} km"
        },
        "ID_1225246470": {},
        "ID_1933295287": {},
        "ID_1400639334": {
            "visible": "{seguir}"
        },
        "ID_1301734233": {
            "text": "{nivel_2}, {pais_texto}"
        },
        "ID_634115674": {
            "text": "{comuna}"
        },
        "ID_1129660084": {
            "text": "{nivel_2}, {pais_texto}"
        },
        "ID_986890171": {
            "text": "a {distance} km"
        },
        "ID_75175": {},
        "ID_1124829140": {},
        "ID_22021502": {},
        "ID_1188474916": {},
        "ID_1043900892": {},
        "ID_380606864": {},
        "ID_375542781": {},
        "ID_1653124778": {},
        "ID_821554929": {},
        "ID_1021365371": {
            "text": "{nivel_2}, {pais_texto}"
        },
        "ID_839820231": {
            "text": "{comuna}"
        },
        "ID_904588626": {},
        "ID_1247657077": {},
        "ID_1634484552": {
            "text": "{comuna}"
        },
        "ID_1642626696": {},
        "ID_282912275": {},
        "ID_148052687": {
            "backgroundColor": "{bgcolor}"
        },
        "ID_1469752290": {
            "visible": "{vis_tipo9}"
        },
        "ID_1082463670": {},
        "ID_1903120505": {
            "text": "{comuna}"
        },
        "ID_23206496": {
            "text": "{direccion}"
        },
        "ID_1436234452": {
            "backgroundColor": "{bgcolor}"
        },
        "ID_1262877465": {
            "text": "{comuna}"
        },
        "ID_1680788099": {},
        "ID_1848781491": {
            "text": "{comuna}"
        },
        "ID_532471930": {},
        "ID_1336684321": {},
        "ID_466372042": {
            "visible": "{vis_tipo6}"
        },
        "ID_598257926": {
            "text": "{direccion}"
        },
        "ID_1401335837": {},
        "ID_915725625": {},
        "ID_976141257": {},
        "ID_639687765": {
            "text": "{direccion}"
        },
        "ID_73578511": {},
        "ID_295209697": {},
        "ID_1109750739": {
            "text": "a {distance} km"
        },
        "ID_940047385": {
            "text": "a {distance} km"
        },
        "ID_586914833": {
            "text": "a {distance} km"
        },
        "ID_1670613628": {},
        "ID_1008671603": {
            "backgroundColor": "{bgcolor}"
        },
        "ID_640168083": {},
        "ID_134526248": {},
        "ID_1453555994": {
            "text": "{nivel_2}, {pais_texto}"
        },
        "ID_675490908": {},
        "ID_483618405": {},
        "ID_902659266": {
            "visible": "{vis_tipo4}"
        },
        "ID_148699692": {
            "text": "{direccion}"
        },
        "ID_177113531": {},
        "ID_348289297": {},
        "ID_1685866714": {},
        "ID_905199894": {},
        "ID_995067518": {
            "text": "{direccion}"
        },
        "ID_1654146588": {
            "text": "{nivel_2}, {pais_texto}"
        },
        "ID_732636877": {},
        "ID_653858230": {},
        "ID_1639540465": {
            "text": "{nivel_2}, {pais_texto}"
        },
        "ID_1157133699": {},
        "ID_579741095": {},
        "ID_1749522495": {
            "backgroundColor": "{bgcolor}"
        },
        "ID_1834569554": {},
        "ID_1983604525": {},
        "ID_1148296760": {},
        "ID_1779783039": {
            "backgroundColor": "{bgcolor}"
        },
        "ID_1935971476": {
            "text": "{nivel_2}, {pais_texto}"
        },
        "ID_1225247169": {
            "backgroundColor": "{bgcolor}"
        },
        "ID_708673244": {
            "text": "{comuna}"
        },
        "ID_1121279285": {
            "text": "{comuna}"
        },
        "ID_1224500518": {},
        "ID_1187902687": {
            "text": "{nivel_2}, {pais_texto}"
        },
        "ID_1363673994": {},
        "ID_1829606280": {},
        "ID_644610354": {},
        "ID_135376617": {
            "text": "{nivel_2}, {pais_texto}"
        },
        "ID_1696532710": {
            "backgroundColor": "{bgcolor}"
        },
        "ID_26705836": {},
        "ID_1275917191": {
            "text": "{direccion}"
        },
        "ID_261159715": {},
        "ID_149472566": {
            "text": "{comuna}"
        },
        "ID_780703879": {},
        "ID_609601012": {
            "text": "a {distance} km"
        },
        "ID_767590028": {},
        "ID_826069369": {},
        "ID_574730052": {},
        "ID_364330445": {},
        "ID_1379816612": {
            "visible": "{vis_tipo10}"
        },
        "ID_480945686": {},
        "ID_1862215651": {
            "text": "{comuna}"
        },
        "ID_309328842": {},
        "ID_720678066": {},
        "ID_1588457992": {},
        "ID_710734301": {},
        "ID_647346064": {},
        "ID_1281228744": {},
        "ID_1991358719": {},
        "ID_1366590855": {},
        "ID_511362493": {
            "visible": "{vis_tipo8}"
        },
        "ID_1297800494": {
            "text": "{nivel_2}, {pais_texto}"
        },
        "ID_828476175": {},
        "ID_1244150292": {
            "backgroundColor": "{bgcolor}"
        },
        "ID_1292045533": {
            "backgroundColor": "{bgcolor}"
        },
        "ID_1433100065": {
            "text": "{prioridad_tiempo}"
        },
        "ID_1683440013": {
            "visible": "{vis_otro}"
        },
        "ID_1739221934": {},
        "ID_1854942384": {
            "text": "{comuna}"
        },
        "ID_870734514": {},
        "ID_1343703596": {},
        "ID_1502072898": {
            "visible": "{vis_tipo5}"
        },
        "ID_1688585922": {
            "text": "a {distance} km"
        },
        "ID_987598921": {},
        "ID_1939249430": {
            "text": "{direccion}"
        },
        "ID_1498624444": {
            "backgroundColor": "{bgcolor}"
        },
        "ID_1094583180": {},
        "ID_101815200": {},
        "ID_1417402594": {
            "visible": "{vis_tipo2}"
        },
        "ID_13800216": {
            "text": "{nivel_2}, {pais_texto}"
        },
        "ID_1002253527": {
            "visible": "{vis_tipo1}"
        },
        "ID_1556692359": {},
        "ID_1800022425": {
            "text": "{id}"
        },
        "ID_1615120356": {},
        "ID_682930065": {
            "text": "a {distance} km"
        },
        "ID_1586191918": {},
        "ID_733552058": {},
        "ID_231988172": {
            "text": "a {distance} km"
        },
        "ID_1565528410": {
            "visible": "{vis_tipo7}"
        },
        "ID_412416512": {},
        "ID_1787687658": {
            "text": "{direccion}"
        },
        "ID_1526803322": {},
        "ID_1655100522": {
            "backgroundColor": "{bgcolor}"
        },
        "ID_1376011318": {
            "text": "a {distance} km"
        },
        "ID_789021647": {
            "text": "a {distance} km"
        },
        "ID_1412225440": {},
        "ID_794184744": {
            "text": "{direccion}"
        },
        "ID_1149338911": {
            "text": "{direccion}"
        },
        "ID_1642833684": {
            "visible": "{vis_tipo3}"
        }
    }
};

var _activity;
if (OS_ANDROID) {
    _activity = $.ID_1082890408.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_1082890408';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_1082890408.addEventListener('open', function(e) {
        abx.setStatusbarColor("#000000");
        abx.setBackgroundColor("white");
    });
}


var ID_290063042_like = function(search) {
    if (typeof search !== 'string' || this === null) {
        return false;
    }
    search = search.replace(new RegExp("([\\.\\\\\\+\\*\\?\\[\\^\\]\\$\\(\\)\\{\\}\\=\\!\\<\\>\\|\\:\\-])", "g"), "\\$1");
    search = search.replace(/%/g, '.*').replace(/_/g, '.');
    return RegExp('^' + search + '$', 'gi').test(this);
};
var ID_290063042_filter = function(coll) {
    var filtered = _.toArray(coll.filter(function(m) {
        return true;
    }));
    return filtered;
};
var ID_290063042_update = function(e) {};
_.defer(function() {
    Alloy.Collections.tareas.fetch();
});
Alloy.Collections.tareas.on('add change delete', function(ee) {
    ID_290063042_update(ee);
});
var ID_290063042_transform = function(model) {
    var fila = model.toJSON();
    /** 
     * Valores extra default 
     */
    var fila = _.extend(fila, {
        vis_tipo1: L('x734881840_traducir', 'false'),
        vis_tipo2: L('x734881840_traducir', 'false'),
        vis_tipo3: L('x734881840_traducir', 'false'),
        vis_tipo4: L('x734881840_traducir', 'false'),
        vis_tipo5: L('x734881840_traducir', 'false'),
        vis_tipo6: L('x734881840_traducir', 'false'),
        vis_tipo7: L('x734881840_traducir', 'false'),
        vis_tipo8: L('x734881840_traducir', 'false'),
        vis_tipo9: L('x734881840_traducir', 'false'),
        vis_tipo10: L('x734881840_traducir', 'false'),
        vis_otro: L('x734881840_traducir', 'false'),
        bgcolor: L('x1492402853', '#CECECE'),
        seguir: L('x734881840_traducir', 'false')
    });
    if (fila.prioridad_tiempo == 1 || fila.prioridad_tiempo == '1') {
        /** 
         * Movemos pinchos de mapa segun posicion de tareas de hoy 
         */
        $.ID_1155797055.setLatitude(fila.lat);

        $.ID_1155797055.setLongitude(fila.lon);

        var fila = _.extend(fila, {
            vis_tipo1: L('x4261170317', 'true'),
            bgcolor: L('x1884157677', '#2D9EDB')
        });
    } else if (fila.prioridad_tiempo == 2) {
        $.ID_1189721672.setLatitude(fila.lat);

        $.ID_1189721672.setLongitude(fila.lon);

        var fila = _.extend(fila, {
            vis_tipo2: L('x4261170317', 'true'),
            bgcolor: L('x602248408', '#EE7F7E')
        });
    } else if (fila.prioridad_tiempo == 3) {
        $.ID_1438326163.setLatitude(fila.lat);

        $.ID_1438326163.setLongitude(fila.lon);

        var fila = _.extend(fila, {
            vis_tipo3: L('x4261170317', 'true'),
            bgcolor: L('x1927584467', '#8383DB')
        });
    } else if (fila.prioridad_tiempo == 4) {
        $.ID_57429076.setLatitude(fila.lat);

        $.ID_57429076.setLongitude(fila.lon);

        var fila = _.extend(fila, {
            vis_tipo4: L('x4261170317', 'true'),
            bgcolor: L('x1269456848', '#FCBD83')
        });
    } else if (fila.prioridad_tiempo == 5) {
        $.ID_344679634.setLatitude(fila.lat);

        $.ID_344679634.setLongitude(fila.lon);

        var fila = _.extend(fila, {
            vis_tipo5: L('x4261170317', 'true'),
            bgcolor: L('x2849471580', '#8CE5BD')
        });
    } else if (fila.prioridad_tiempo == 6) {
        $.ID_1118399852.setLatitude(fila.lat);

        $.ID_1118399852.setLongitude(fila.lon);

        var fila = _.extend(fila, {
            vis_tipo6: L('x4261170317', 'true'),
            bgcolor: L('x3452106967', '#F8DA54')
        });
    } else if (fila.prioridad_tiempo == 7) {
        $.ID_1570710884.setLatitude(fila.lat);

        $.ID_1570710884.setLongitude(fila.lon);

        var fila = _.extend(fila, {
            vis_tipo7: L('x4261170317', 'true'),
            bgcolor: L('x3332103381', '#B9AAF3')
        });
    } else if (fila.prioridad_tiempo == 8) {
        $.ID_1941929915.setLatitude(fila.lat);

        $.ID_1941929915.setLongitude(fila.lon);

        var fila = _.extend(fila, {
            vis_tipo8: L('x4261170317', 'true'),
            bgcolor: L('x3561405284', '#FFACAA')
        });
    } else if (fila.prioridad_tiempo == 9) {
        $.ID_29735867.setLatitude(fila.lat);

        $.ID_29735867.setLongitude(fila.lon);

        var fila = _.extend(fila, {
            vis_tipo9: L('x4261170317', 'true'),
            bgcolor: L('x2922666116', '#8BC9E8')
        });
    } else if (fila.prioridad_tiempo == 10) {
        $.ID_1330735480.setLatitude(fila.lat);

        $.ID_1330735480.setLongitude(fila.lon);

        var fila = _.extend(fila, {
            vis_tipo10: L('x4261170317', 'true'),
            bgcolor: L('x585770471', '#A5876D')
        });
    } else {
        $.ID_1330735480.setLatitude(fila.lat);

        $.ID_1330735480.setLongitude(fila.lon);

        var fila = _.extend(fila, {
            vis_otro: L('x4261170317', 'true')
        });
    }
    if ((_.isObject(fila.nivel_2) || _.isString(fila.nivel_2)) && _.isEmpty(fila.nivel_2)) {
        /** 
         * Mapeamos campos de niveles 
         */
        fila.comuna = fila.nivel_1;
    } else if ((_.isObject(fila.nivel_3) || _.isString(fila.nivel_3)) && _.isEmpty(fila.nivel_3)) {
        fila.comuna = fila.nivel_2;
    } else if ((_.isObject(fila.nivel_4) || _.isString(fila.nivel_4)) && _.isEmpty(fila.nivel_4)) {
        fila.comuna = fila.nivel_3;
    } else if ((_.isObject(fila.nivel_5) || _.isString(fila.nivel_5)) && _.isEmpty(fila.nivel_5)) {
        fila.comuna = fila.nivel_4;
    } else {
        fila.comuna = fila.nivel_5;
    }
    if (fila.estado_tarea == 4) {
        /** 
         * Revisamos si el estado de la tarea esta en seguimiento 
         */
        fila.seguir = true;
    }
    return fila;
};
ID_290063042_filter = function(coll) {
    var filtered = _.toArray(coll.filter(function(filax) {
        var fila = filax.toJSON();
        var test = true;
        var moment = require('alloy/moment');
        var fecha_hoy = moment(new Date()).format('YYYY-MM-DD');
        if (fila.fecha_tarea == fecha_hoy) {
            test = true;
        } else {
            test = false;
        }
        fila = null;
        return test;
    }));
    var ordered = _.sortBy(filtered, 'prioridad_tiempo');
    return ordered;
};
Alloy.Collections.tareas.fetch();


$.ID_1722441626.applyProperties({
    latitude: parseFloat('0.0'),
    longitude: parseFloat('0.0'),
    image: '/images/i31D884D83D4B504A668DECEFFAE7EA43.png'
});


$.ID_1155797055.applyProperties({
    latitude: parseFloat('0.0'),
    longitude: parseFloat('0.0'),
    image: '/images/i016B8B08EE4DEC0313B1A3082A84D2F2.png'
});


$.ID_1189721672.applyProperties({
    latitude: parseFloat('0.0'),
    longitude: parseFloat('0.0'),
    image: '/images/iAF9569EFAB97FCE93FE3DAD449050784.png'
});


$.ID_1438326163.applyProperties({
    latitude: parseFloat('0.0'),
    longitude: parseFloat('0.0'),
    image: '/images/i56ABF76585C9B6A661090A44C21595F2.png'
});


$.ID_57429076.applyProperties({
    latitude: parseFloat('0.0'),
    longitude: parseFloat('0.0'),
    image: '/images/i999E1B96A6FB0BFB9034B067828F4A68.png'
});


$.ID_344679634.applyProperties({
    latitude: parseFloat('0.0'),
    longitude: parseFloat('0.0'),
    image: '/images/i0B10A526AD8D5B25BC5BAC5C2C06F119.png'
});


$.ID_1118399852.applyProperties({
    latitude: parseFloat('0.0'),
    longitude: parseFloat('0.0'),
    image: '/images/i15970AF9F27BB88EC2DC1EE088D2551E.png'
});


$.ID_1570710884.applyProperties({
    latitude: parseFloat('0.0'),
    longitude: parseFloat('0.0'),
    image: '/images/iA6C17CF225E89F54DE02BF67B8FCA6A0.png'
});


$.ID_1941929915.applyProperties({
    latitude: parseFloat('0.0'),
    longitude: parseFloat('0.0'),
    image: '/images/iCF8339009327869FD10EC12DA520DC5A.png'
});


$.ID_29735867.applyProperties({
    latitude: parseFloat('0.0'),
    longitude: parseFloat('0.0'),
    image: '/images/i8893A1F7922FA64C321EAD4A6119E30D.png'
});


$.ID_1330735480.applyProperties({
    latitude: parseFloat('0.0'),
    longitude: parseFloat('0.0'),
    image: '/images/i71C6F623091942D94C46CC378343ABF6.png'
});


$.ID_31019924.setRegion({
    latitude: -33.392047,
    longitude: -70.542939,
    latitudeDelta: 0.005,
    longitudeDelta: 0.005
});
$.ID_31019924.applyProperties({});
if (OS_IOS) {
    var ID_31019924_camara = Alloy.Globals.mod_ti_map.createCamera({
        pitch: 0,
        heading: 0
    });
    $.ID_31019924.setCamera(ID_31019924_camara);
}


$.ID_1523341755.init({
    titulo: L('x2349703141_traducir', 'NO TIENES TAREAS'),
    __id: 'ALL1523341755',
    texto: L('x2279881512_traducir', 'Asegurate de tomar tareas para hoy y revisar tu ruta'),
    alto: '-',
    top: '10%',
    ancho: '*',
    tipo: '_sorry'
});

function Click_ID_68350663(e) {

    e.cancelBubble = true;
    if ('index' in e) {
        var elemento = e.source.getLabels()[e.index];
        var valor = elemento.title;
        if (false) console.log('cambiado tipo de calculo de ruta', {
            "elemento": elemento
        });
        if (e.index == 0 || e.index == '0') {
            require('vars')['avoid'] = '';
        } else {
            require('vars')['avoid'] = L('x1287501108_traducir', 'highways');
        }
        Alloy.Events.trigger('_calcular_ruta');
        elemento = null, valor = null;
    }

}

function Itemclick_ID_63790287(e) {

    e.cancelBubble = true;
    var objeto = e.section.getItemAt(e.itemIndex);
    var modelo = {},
        _modelo = [];
    var fila = {},
        fila_bak = {},
        info = {
            _template: objeto.template,
            _what: [],
            _seccion_ref: e.section.getHeaderTitle(),
            _model_id: -1
        },
        _tmp = {
            objmap: {}
        };
    if ('itemId' in e) {
        info._model_id = e.itemId;
        modelo._id = info._model_id;
        if (info._seccion_ref != '' && info._seccion_ref in _bind4section) {
            modelo._collection = _bind4section[info._seccion_ref];
            _tmp._coll = modelo._collection;
        }
    }
    var findVariables = require('fvariables');
    _.each(_list_templates[info._template], function(obj_id, id) {
        _.each(obj_id, function(valor, prop) {
            var llaves = findVariables(valor, '{', '}');
            _.each(llaves, function(llave) {
                _tmp.objmap[llave] = {
                    id: id,
                    prop: prop
                };
                fila[llave] = objeto[id][prop];
                if (id == e.bindId) info._what.push(llave);
            });
        });
    });
    info._what = info._what.join(',');
    fila_bak = JSON.parse(JSON.stringify(fila));
    /** 
     * Ocupamos variable para impedir que la proxima pantalla se abra dos veces 
     */
    var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
    if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
        require('vars')['var_abriendo'] = L('x4261170317', 'true');
        var nulo = Alloy.createController("detalletarea_index", {
            '_id': fila.id,
            '__master_model': (typeof modelo !== 'undefined') ? modelo : {},
            '__modelo': (typeof _modelo !== 'undefined' && _modelo.length > 0) ? _modelo[0] : {}
        }).getView();
        nulo.open({
            modal: true
        });
        nulo = null;
    }
    _tmp.changed = false;
    _tmp.diff_keys = [];
    _.each(fila, function(value1, prop) {
        var had_samekey = false;
        _.each(fila_bak, function(value2, prop2) {
            if (prop == prop2 && value1 == value2) {
                had_samekey = true;
            } else if (!_.has(fila_bak, prop) || !_.has(fila, prop2)) {
                has_samekey = true;
            }
        });
        if (!had_samekey) _tmp.diff_keys.push(prop);
    });
    if (_tmp.diff_keys.length > 0) _tmp.changed = true;
    if (_tmp.changed == true) {
        _.each(_tmp.diff_keys, function(llave) {
            objeto[_tmp.objmap[llave].id][_tmp.objmap[llave].prop] = fila[llave];
        });
        e.section.updateItemAt(e.itemIndex, objeto);
    }

}

function Longpress_ID_459065561(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    /** 
     * Armamos listado de casos para confirmar ruta 
     */
    var moment = require('alloy/moment');
    var hoy_date = moment(new Date()).format('YYYY-MM-DD');
    var ID_159436951_i = Alloy.createCollection('tareas');
    var ID_159436951_i_where = 'fecha_tarea=\'' + hoy_date + '\'';
    ID_159436951_i.fetch({
        query: 'SELECT * FROM tareas WHERE fecha_tarea=\'' + hoy_date + '\''
    });
    var tarea_lista = require('helper').query2array(ID_159436951_i);
    var listacasos = _.pluck(tarea_lista, 'num_caso').join(',');
    var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
    var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
    Ti.App.Properties.setString('confirmarpush', JSON.stringify(false));
    require('vars')['rutaconfirmada'] = L('x734881840_traducir', 'false');
    var jsonfinal = {
        _method: L('x1814004025_traducir', 'POST'),
        _url: String.format(L('x2181838308', '%1$sconfirmarRuta'), url_server.toString()),
        id_inspector: inspector[0].id_server,
        codigo_identificador: inspector[0].codigo_identificador,
        tareas: listacasos
    };
    var jsonfinal = JSON.stringify(jsonfinal);
    var ID_711778937_m = Alloy.Collections.cola;
    var ID_711778937_fila = Alloy.createModel('cola', {
        data: jsonfinal,
        tipo: 'confirmar_ruta'
    });
    ID_711778937_m.add(ID_711778937_fila);
    ID_711778937_fila.save();
    _.defer(function() {});
    /** 
     * Oculta boton mantener para iniciar 
     */
    var ID_855327767_visible = false;

    if (ID_855327767_visible == 'si') {
        ID_855327767_visible = true;
    } else if (ID_855327767_visible == 'no') {
        ID_855327767_visible = false;
    }
    $.ID_855327767.setVisible(ID_855327767_visible);

    /** 
     * Limpieza de memoria 
     */
    jsonfinal = null, listacasos = null;

}

(function() {
    $.ID_31019924.setTraffic(true);

    /** 
     * cuando se gatilla este evento, la app se esta cerrando. 30-dic-2017. Revisar 
     */
    _my_events['_refrescar_tareas_hoy,ID_1168607634'] = function(evento) {
        var inspeccion_encurso = ('inspeccion_encurso' in require('vars')) ? require('vars')['inspeccion_encurso'] : '';
        if (inspeccion_encurso == false || inspeccion_encurso == 'false') {
            $.ID_1155797055.setLatitude('0.0');

            $.ID_1155797055.setLongitude('0.0');

            $.ID_1189721672.setLatitude('0.0');

            $.ID_1189721672.setLongitude('0.0');

            $.ID_1438326163.setLatitude('0.0');

            $.ID_1438326163.setLongitude('0.0');

            $.ID_57429076.setLatitude('0.0');

            $.ID_57429076.setLongitude('0.0');

            $.ID_344679634.setLatitude('0.0');

            $.ID_344679634.setLongitude('0.0');

            $.ID_1118399852.setLatitude('0.0');

            $.ID_1118399852.setLongitude('0.0');

            $.ID_1570710884.setLatitude('0.0');

            $.ID_1570710884.setLongitude('0.0');

            $.ID_1941929915.setLatitude('0.0');

            $.ID_1941929915.setLongitude('0.0');

            $.ID_29735867.setLatitude('0.0');

            $.ID_29735867.setLongitude('0.0');

            $.ID_1330735480.setLatitude('0.0');

            $.ID_1330735480.setLongitude('0.0');

            $.ID_1330735480.setLatitude('0.0');

            $.ID_1330735480.setLongitude('0.0');

            var ID_568119611_func = function() {
                _.defer(function() {
                    Alloy.Collections.tareas.fetch();
                });
            };
            var ID_568119611 = setTimeout(ID_568119611_func, 1000 * 0.2);
            var moment = require('alloy/moment');
            var hoy_date = moment(new Date()).format('YYYY-MM-DD');
            var ID_277943757_i = Alloy.createCollection('tareas');
            var ID_277943757_i_where = 'fecha_tarea=\'' + hoy_date + '\' ORDER BY PRIORIDAD_TIEMPO ASC';
            ID_277943757_i.fetch({
                query: 'SELECT * FROM tareas WHERE fecha_tarea=\'' + hoy_date + '\' ORDER BY PRIORIDAD_TIEMPO ASC'
            });
            var tarea_lista = require('helper').query2array(ID_277943757_i);
            if (tarea_lista && tarea_lista.length == 0) {
                $.ID_1523341755.mostrar({});
                var ID_1295211740_visible = false;

                if (ID_1295211740_visible == 'si') {
                    ID_1295211740_visible = true;
                } else if (ID_1295211740_visible == 'no') {
                    ID_1295211740_visible = false;
                }
                $.ID_1295211740.setVisible(ID_1295211740_visible);

                var gps_error = ('gps_error' in require('vars')) ? require('vars')['gps_error'] : '';
                if (gps_error == false || gps_error == 'false') {
                    var inspeccion_encurso = ('inspeccion_encurso' in require('vars')) ? require('vars')['inspeccion_encurso'] : '';
                    if (inspeccion_encurso == false || inspeccion_encurso == 'false') {
                        if (false) console.log('limpiando datos...', {});
                        /** 
                         * Borramos ruta previa si existe 
                         */
                        var old_ruta = ('old_ruta' in require('vars')) ? require('vars')['old_ruta'] : '';
                        if (old_ruta && old_ruta.length) {
                            var item_rutas_index = 0;
                            _.each(old_ruta, function(item_rutas, item_rutas_pos, item_rutas_list) {
                                item_rutas_index += 1;
                                $.ID_31019924.removeRoute(item_rutas);

                            });
                            /** 
                             * Limpieza de memoria 
                             */
                            item_rutas = null;
                        }
                    }
                }
            } else {
                $.ID_1523341755.ocultar({});
                var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
                $.ID_1722441626.setLatitude(inspector.lat_dir);

                $.ID_1722441626.setLongitude(inspector.lon_dir);

                var ID_1295211740_visible = true;

                if (ID_1295211740_visible == 'si') {
                    ID_1295211740_visible = true;
                } else if (ID_1295211740_visible == 'no') {
                    ID_1295211740_visible = false;
                }
                $.ID_1295211740.setVisible(ID_1295211740_visible);

            }
            var confirmarpush = JSON.parse(Ti.App.Properties.getString('confirmarpush'));
            if (confirmarpush == true || confirmarpush == 'true') {
                var ID_855327767_visible = true;

                if (ID_855327767_visible == 'si') {
                    ID_855327767_visible = true;
                } else if (ID_855327767_visible == 'no') {
                    ID_855327767_visible = false;
                }
                $.ID_855327767.setVisible(ID_855327767_visible);

            } else {
                var ID_855327767_visible = false;

                if (ID_855327767_visible == 'si') {
                    ID_855327767_visible = true;
                } else if (ID_855327767_visible == 'no') {
                    ID_855327767_visible = false;
                }
                $.ID_855327767.setVisible(ID_855327767_visible);

            }
            tarea_lista = null, hoy_date = null, confirmarpush = null, inspeccion_encurso = null, evento = null;
        }
    };
    Alloy.Events.on('_refrescar_tareas_hoy', _my_events['_refrescar_tareas_hoy,ID_1168607634']);
    _my_events['_calcular_ruta,ID_598459730'] = function(evento) {
        var gps_error = ('gps_error' in require('vars')) ? require('vars')['gps_error'] : '';
        if (gps_error == false || gps_error == 'false') {
            var inspeccion_encurso = ('inspeccion_encurso' in require('vars')) ? require('vars')['inspeccion_encurso'] : '';
            if (inspeccion_encurso == false || inspeccion_encurso == 'false') {
                if (false) console.log('calculando ruta optima para el dia de hoy', {});
                var moment = require('alloy/moment');
                var hoy_date = moment(new Date()).format('YYYY-MM-DD');
                var ID_907052044_i = Alloy.createCollection('tareas');
                var ID_907052044_i_where = 'fecha_tarea=\'' + hoy_date + '\'';
                ID_907052044_i.fetch({
                    query: 'SELECT * FROM tareas WHERE fecha_tarea=\'' + hoy_date + '\''
                });
                var tarea_lista = require('helper').query2array(ID_907052044_i);
                if (tarea_lista && tarea_lista.length == 0) {
                    Alloy.Events.trigger('_refrescar_tareas_hoy');
                    Alloy.Events.trigger('_refrescar_tareas_hoy');
                } else {
                    var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
                    var gps_latitud = ('gps_latitud' in require('vars')) ? require('vars')['gps_latitud'] : '';
                    var gps_longitud = ('gps_longitud' in require('vars')) ? require('vars')['gps_longitud'] : '';
                    /** 
                     * Recorremos tareas para armar waypoints, considerando tarea seguida como inicial 
                     */

                    //mapea el array de tarea_lista considerando las columnas lat y lon en un array de string tipo 'lat,lon'
                    var waypoints = _.map(tarea_lista,
                        function(num) {
                            var item = num.lat + "," + num.lon;
                            return item;
                        });
                    var waypoints = waypoints.join("|");
                    /*
                    var destino = _.max(tarea_lista, function(tarea){ return tarea.distancia_2; });*/
                    var origen = gps_latitud + ',' + gps_longitud;
                    var destino = inspector.lat_dir + ',' + inspector.lon_dir;
                    //agregamos destino como ultimo waypoint, porque no lo marca como ruta.
                    waypoints = waypoints + '|' + destino
                    var avoid = ('avoid' in require('vars')) ? require('vars')['avoid'] : '';
                    var googlekeymap = ('googlekeymap' in require('vars')) ? require('vars')['googlekeymap'] : '';
                    if (false) console.log('consultando a google rutas para waypoints', {
                        "waypoints": waypoints
                    });
                    /** 
                     * API de Google Directions 
                     */
                    var ID_876987309 = {};
                    ID_876987309.success = function(e) {
                        var elemento = e,
                            valor = e;
                        if (elemento.status == L('x3610695981_traducir', 'OK')) {
                            /** 
                             * Borramos ruta previa si existe 
                             */
                            var old_ruta = ('old_ruta' in require('vars')) ? require('vars')['old_ruta'] : '';
                            if (old_ruta && old_ruta.length) {
                                var item_rutas_index = 0;
                                _.each(old_ruta, function(item_rutas, item_rutas_pos, item_rutas_list) {
                                    item_rutas_index += 1;
                                    $.ID_31019924.removeRoute(item_rutas);

                                });
                                /** 
                                 * Limpieza de memoria 
                                 */
                                item_rutas = null;
                            }
                            /** 
                             * Dibujamos nuevas rutas 
                             */
                            var ruta = elemento.routes[0];
                            var rutas = [];
                            var pincho_index = 0;
                            _.each(ruta.legs, function(pincho, pincho_pos, pincho_list) {
                                pincho_index += 1;
                                var tramo_index = 0;
                                _.each(pincho.steps, function(tramo, tramo_pos, tramo_list) {
                                    tramo_index += 1;
                                    if (pincho_index == ruta.legs.length) {
                                        var ID_1209313831_poly = require('polyline').decode(tramo.polyline.points);
                                        var nuevaruta = Alloy.Globals.mod_ti_map.createRoute({
                                            width: 6,
                                            color: '#f7d952',
                                            points: ID_1209313831_poly
                                        });
                                    } else if (pincho_index == 1 || pincho_index == '1') {
                                        var ID_891367039_poly = require('polyline').decode(tramo.polyline.points);
                                        var nuevaruta = Alloy.Globals.mod_ti_map.createRoute({
                                            width: 6,
                                            color: '#2d9edb',
                                            points: ID_891367039_poly
                                        });
                                    } else if (pincho_index == 2) {
                                        var ID_183027178_poly = require('polyline').decode(tramo.polyline.points);
                                        var nuevaruta = Alloy.Globals.mod_ti_map.createRoute({
                                            width: 6,
                                            color: '#ee7f7e',
                                            points: ID_183027178_poly
                                        });
                                    } else if (pincho_index == 3) {
                                        var ID_600929630_poly = require('polyline').decode(tramo.polyline.points);
                                        var nuevaruta = Alloy.Globals.mod_ti_map.createRoute({
                                            width: 6,
                                            color: '#b9aaf3',
                                            points: ID_600929630_poly
                                        });
                                    } else if (pincho_index == 4) {
                                        var ID_348344451_poly = require('polyline').decode(tramo.polyline.points);
                                        var nuevaruta = Alloy.Globals.mod_ti_map.createRoute({
                                            width: 6,
                                            color: '#efb275',
                                            points: ID_348344451_poly
                                        });
                                    } else if (pincho_index == 5) {
                                        var ID_1277001386_poly = require('polyline').decode(tramo.polyline.points);
                                        var nuevaruta = Alloy.Globals.mod_ti_map.createRoute({
                                            width: 6,
                                            color: '#8ce5bd',
                                            points: ID_1277001386_poly
                                        });
                                    } else if (pincho_index == 6) {
                                        var ID_183460208_poly = require('polyline').decode(tramo.polyline.points);
                                        var nuevaruta = Alloy.Globals.mod_ti_map.createRoute({
                                            width: 6,
                                            color: '#f8da54',
                                            points: ID_183460208_poly
                                        });
                                    } else if (pincho_index == 7) {
                                        var ID_1863245855_poly = require('polyline').decode(tramo.polyline.points);
                                        var nuevaruta = Alloy.Globals.mod_ti_map.createRoute({
                                            width: 6,
                                            color: '#8383db',
                                            points: ID_1863245855_poly
                                        });
                                    } else if (pincho_index == 8) {
                                        var ID_1984215290_poly = require('polyline').decode(tramo.polyline.points);
                                        var nuevaruta = Alloy.Globals.mod_ti_map.createRoute({
                                            width: 6,
                                            color: '#ffacaa',
                                            points: ID_1984215290_poly
                                        });
                                    } else if (pincho_index == 9) {
                                        var ID_513893132_poly = require('polyline').decode(tramo.polyline.points);
                                        var nuevaruta = Alloy.Globals.mod_ti_map.createRoute({
                                            width: 6,
                                            color: '#8bc9e8',
                                            points: ID_513893132_poly
                                        });
                                    } else if (pincho_index == 10) {
                                        var ID_795731645_poly = require('polyline').decode(tramo.polyline.points);
                                        var nuevaruta = Alloy.Globals.mod_ti_map.createRoute({
                                            width: 6,
                                            color: '#a5876d',
                                            points: ID_795731645_poly
                                        });
                                    } else {
                                        var ID_1726394920_poly = require('polyline').decode(tramo.polyline.points);
                                        var nuevaruta = Alloy.Globals.mod_ti_map.createRoute({
                                            width: 6,
                                            color: '#2d9edb',
                                            points: ID_1726394920_poly
                                        });
                                    }
                                    /** 
                                     * Agregamos la ruta al mapa 
                                     */
                                    $.ID_31019924.addRoute(nuevaruta);

                                    /** 
                                     * guardamos la ruta en una variable 
                                     */
                                    rutas.push(nuevaruta);
                                    require('vars')['old_ruta'] = rutas;
                                });
                            });
                            /** 
                             * Limpieza de memoria 
                             */
                            rutas = null;
                            /** 
                             * Ordenamos las tareas 
                             */
                            var moment = require('alloy/moment');
                            var hoy_date = moment(new Date()).format('YYYY-MM-DD');
                            var ID_858281831_i = Alloy.createCollection('tareas');
                            var ID_858281831_i_where = 'fecha_tarea=\'' + hoy_date + '\'';
                            ID_858281831_i.fetch({
                                query: 'SELECT * FROM tareas WHERE fecha_tarea=\'' + hoy_date + '\''
                            });
                            var tarea_lista = require('helper').query2array(ID_858281831_i);
                            var item_index = 0;
                            _.each(ruta.waypoint_order, function(item, item_pos, item_list) {
                                item_index += 1;
                                var seguir_tarea = ('seguir_tarea' in require('vars')) ? require('vars')['seguir_tarea'] : '';
                                if (item_index == ruta.waypoint_order.length) {
                                    /** 
                                     * el ultimo valor lo omitimos porque el camino a casa no es una tarea 
                                     */
                                } else {
                                    var tareaid = tarea_lista[item].id;
                                    var ID_18555137_i = Alloy.createCollection('tareas');
                                    var ID_18555137_i_where = 'id=\'' + tareaid + '\'';
                                    ID_18555137_i.fetch({
                                        query: 'SELECT * FROM tareas WHERE id=\'' + tareaid + '\''
                                    });
                                    var tarea_lista_aux = require('helper').query2array(ID_18555137_i);
                                    var db = Ti.Database.open(ID_18555137_i.config.adapter.db_name);
                                    if (ID_18555137_i_where == '') {
                                        var sql = 'UPDATE ' + ID_18555137_i.config.adapter.collection_name + ' SET prioridad_tiempo=\'' + item_index + '\'';
                                    } else {
                                        var sql = 'UPDATE ' + ID_18555137_i.config.adapter.collection_name + ' SET prioridad_tiempo=\'' + item_index + '\' WHERE ' + ID_18555137_i_where;
                                    }
                                    db.execute(sql);
                                    db.close();
                                    /** 
                                     * Limpieza de memoria 
                                     */
                                    tarea_lista_aux = null, tareaid = null;
                                }
                            });
                            /** 
                             * Limpieza de memoria 
                             */
                            ruta = null, rutas = null, tarea_lista = null, hoy_date = null, old_ruta = null, pincho = null, item = null;
                        }
                        /** 
                         * Limpieza de memoria 
                         */
                        elemento = null;
                        if (false) console.log('refrescando con ruta', {});
                        Alloy.Events.trigger('_refrescar_tareas_hoy');
                        elemento = null, valor = null;
                    };
                    ID_876987309.error = function(e) {
                        var elemento = e,
                            valor = e;
                        if (false) console.log('refrescando con error', {});
                        Alloy.Events.trigger('_refrescar_tareas_hoy');
                        elemento = null, valor = null;
                    };
                    require('helper').ajaxUnico('ID_876987309', 'https://maps.googleapis.com/maps/api/directions/json?origin=' + origen + '&destination=' + destino + '&waypoints=' + waypoints + '&key=AIzaSyAJoATeoDjsJ3fNSe5q7eIKZ7VjT2AojqY&avoid=' + avoid + '&alternatives=false', 'get', {}, 15000, ID_876987309);
                    /** 
                     * Evaluar, elemento=null; 
                     */
                    waypoints = null, destino = null, origen = null, avoid = null, googlekeymap = null, tarea_lista = null;
                }
            }
        }
    };
    Alloy.Events.on('_calcular_ruta', _my_events['_calcular_ruta,ID_598459730']);
    _my_events['_refrescar_tareas,ID_106592695'] = function(evento) {
        var inspeccion_encurso = ('inspeccion_encurso' in require('vars')) ? require('vars')['inspeccion_encurso'] : '';
        if (inspeccion_encurso == false || inspeccion_encurso == 'false') {
            /** 
             * solo refrescamos si no hay una inspecci&#243;n en curso 
             */
            if (false) console.log('llamado refrescar ruta de hoy', {});
            Alloy.Events.trigger('_calcular_ruta');
        }
    };
    Alloy.Events.on('_refrescar_tareas', _my_events['_refrescar_tareas,ID_106592695']);
    /** 
     * Esto se llama para que parta con informacion si tiene ya tareas para hoy 
     */
    var ID_599015300_func = function() {
        Alloy.Events.trigger('_calcular_ruta');
    };
    var ID_599015300 = setTimeout(ID_599015300_func, 1000 * 0.1);
})();

if (OS_IOS || OS_ANDROID) {
    $.ID_1082890408.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}
//$.ID_1082890408.open();