var _bind4section = {
    "ref1": "insp_contenido"
};
var _list_templates = {
    "pborrar": {
        "ID_1883915455": {
            "text": "{id}"
        },
        "ID_2120109018": {
            "text": "{nombre}"
        },
        "ID_1046919308": {},
        "ID_1735369570": {},
        "ID_1777579448": {},
        "ID_1254685949": {}
    },
    "contenido": {
        "ID_630800887": {},
        "ID_862352109": {
            "text": "{id}"
        },
        "ID_560452274": {
            "text": "{nombre}"
        }
    }
};

$.ID_1094491373_window.setTitleAttributes({
    color: 'WHITE'
});
var _activity;
if (OS_ANDROID) {
    _activity = $.ID_1094491373.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_1094491373';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_1094491373_window.addEventListener('open', function(e) {
        abx.setStatusbarColor("#FFFFFF");
        abx.setBackgroundColor("#8bc9e8");
    });
}


var ID_1504721941_like = function(search) {
    if (typeof search !== 'string' || this === null) {
        return false;
    }
    search = search.replace(new RegExp("([\\.\\\\\\+\\*\\?\\[\\^\\]\\$\\(\\)\\{\\}\\=\\!\\<\\>\\|\\:\\-])", "g"), "\\$1");
    search = search.replace(/%/g, '.*').replace(/_/g, '.');
    return RegExp('^' + search + '$', 'gi').test(this);
};
var ID_1504721941_filter = function(coll) {
    var filtered = _.toArray(coll.filter(function(m) {
        return true;
    }));
    return filtered;
};
var ID_1504721941_transform = function(model) {
    var fila = model.toJSON();
    return fila;
};
var ID_1504721941_update = function(e) {};
_.defer(function() {
    Alloy.Collections.insp_contenido.fetch();
});
Alloy.Collections.insp_contenido.on('add change delete', function(ee) {
    ID_1504721941_update(ee);
});
Alloy.Collections.insp_contenido.fetch();

function Click_ID_2023668748(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    ID_1846643595.show();

}

function Click_ID_1601060400(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    Alloy.createController("nuevo_contenido", {}).getView().open();

}

function Swipe_ID_1270983353(e) {

    e.cancelBubble = true;
    var elemento = e.section.getItemAt(e.itemIndex);
    var findVariables = require('fvariables');
    var fila = {};
    _.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
        _.each(obj_id, function(valor, prop) {
            fila[prop] = elemento[id_field][prop];
            var llaves = findVariables(valor, '{', '}');
            _.each(llaves, function(llave) {
                fila[llave] = elemento[id_field][prop];
            });
        });
    });
    var flag_borrar = ('flag_borrar' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['flag_borrar'] : '';
    if (flag_borrar == false || flag_borrar == 'false') {
        if (e.direction == L('x2053629800_traducir', 'left')) {
            var findVariables = require('fvariables');
            elemento.template = 'pborrar';
            _.each(_list_templates['pborrar'], function(obj_id, id_field) {
                _.each(obj_id, function(valor, prop) {
                    var llaves = findVariables(valor, '{', '}');
                    _.each(llaves, function(llave) {
                        elemento[id_field] = {};
                        elemento[id_field][prop] = fila[llave];
                    });
                });
            });
            if (OS_IOS) {
                e.section.updateItemAt(e.itemIndex, elemento, {
                    animated: true
                });
            } else if (OS_ANDROID) {
                e.section.updateItemAt(e.itemIndex, elemento);
            }
            require('vars')[_var_scopekey]['flag_borrar'] = L('x4261170317', 'true');
        }
    } else {
        require('vars')[_var_scopekey]['flag_borrar'] = L('x734881840_traducir', 'false');
        _.defer(function() {
            Alloy.Collections.insp_contenido.fetch();
        });
    }
}

function Click_ID_172215600(e) {

    e.cancelBubble = true;
    var elemento = e.section.getItemAt(e.itemIndex);
    var findVariables = require('fvariables');
    var fila = {};
    _.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
        _.each(obj_id, function(valor, prop) {
            fila[prop] = elemento[id_field][prop];
            var llaves = findVariables(valor, '{', '}');
            _.each(llaves, function(llave) {
                fila[llave] = elemento[id_field][prop];
            });
        });
    });
    var flag_borrar = ('flag_borrar' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['flag_borrar'] : '';
    if (flag_borrar == false || flag_borrar == 'false') {
        /** 
         * Ocupamos esta variable para impedir que la pantalla siguente se cargue dos veces 
         */
        var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
        if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
            require('vars')['var_abriendo'] = L('x4261170317', 'true');
            /** 
             * Enviamos a editar contenido, y pasamos parametro _dato para enviarle el id del contenido a editar 
             */
            Alloy.createController("nuevo_contenido", {
                '_dato': fila
            }).getView().open();
        }
    } else {
        _.defer(function() {
            Alloy.Collections.insp_contenido.fetch();
        });
        require('vars')[_var_scopekey]['flag_borrar'] = L('x734881840_traducir', 'false');
    }
}

function Click_ID_1044558454(e) {

    e.cancelBubble = true;
    var elemento = e.section.getItemAt(e.itemIndex);
    var findVariables = require('fvariables');
    var fila = {};
    _.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
        _.each(obj_id, function(valor, prop) {
            fila[prop] = elemento[id_field][prop];
            var llaves = findVariables(valor, '{', '}');
            _.each(llaves, function(llave) {
                fila[llave] = elemento[id_field][prop];
            });
        });
    });
    var ID_1032801196_opts = [L('x3827418516_traducir', 'Si'), L('x1962639792_traducir', ' No')];
    var ID_1032801196 = Ti.UI.createAlertDialog({
        title: L('x4097537701_traducir', 'ALERTA'),
        message: '' + String.format(L('x4127782287_traducir', '¿ Seguro desea eliminar: %1$s ?'), fila.nombre.toString()) + '',
        buttonNames: ID_1032801196_opts
    });
    ID_1032801196.addEventListener('click', function(e) {
        var xd = ID_1032801196_opts[e.index];
        if (xd == L('x3827418516_traducir', 'Si')) {
            var ID_1863360950_i = Alloy.Collections.insp_contenido;
            var sql = 'DELETE FROM ' + ID_1863360950_i.config.adapter.collection_name + ' WHERE id=\'' + fila.id + '\'';
            var db = Ti.Database.open(ID_1863360950_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            ID_1863360950_i.trigger('delete');
            require('vars')[_var_scopekey]['flag_borrar'] = L('x734881840_traducir', 'false');
            if(false) console.log('tiene un delay de mediosegundo', {});
            _.defer(function() {
                Alloy.Collections.insp_contenido.fetch();
            });
        } else {
            _.defer(function() {
                Alloy.Collections.insp_contenido.fetch();
            });
        }
        xd = null;
        e.source.removeEventListener("click", arguments.callee);
    });
    ID_1032801196.show();

}

function Swipe_ID_1624967636(e) {

    e.cancelBubble = true;
    var elemento = e.section.getItemAt(e.itemIndex);
    var findVariables = require('fvariables');
    var fila = {};
    _.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
        _.each(obj_id, function(valor, prop) {
            fila[prop] = elemento[id_field][prop];
            var llaves = findVariables(valor, '{', '}');
            _.each(llaves, function(llave) {
                fila[llave] = elemento[id_field][prop];
            });
        });
    });
    if (e.direction == L('x3033167124_traducir', 'right')) {
        var findVariables = require('fvariables');
        elemento.template = 'contenido';
        _.each(_list_templates['contenido'], function(obj_id, id_field) {
            _.each(obj_id, function(valor, prop) {
                var llaves = findVariables(valor, '{', '}');
                _.each(llaves, function(llave) {
                    elemento[id_field] = {};
                    elemento[id_field][prop] = fila[llave];
                });
            });
        });
        if (OS_IOS) {
            e.section.updateItemAt(e.itemIndex, elemento, {
                animated: true
            });
        } else if (OS_ANDROID) {
            e.section.updateItemAt(e.itemIndex, elemento);
        }
        require('vars')[_var_scopekey]['flag_borrar'] = L('x734881840_traducir', 'false');
    }

}

function Click_ID_1939618938(e) {

    e.cancelBubble = true;
    var elemento = e.section.getItemAt(e.itemIndex);
    var findVariables = require('fvariables');
    var fila = {};
    _.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
        _.each(obj_id, function(valor, prop) {
            fila[prop] = elemento[id_field][prop];
            var llaves = findVariables(valor, '{', '}');
            _.each(llaves, function(llave) {
                fila[llave] = elemento[id_field][prop];
            });
        });
    });
    var findVariables = require('fvariables');
    elemento.template = 'contenido';
    _.each(_list_templates['contenido'], function(obj_id, id_field) {
        _.each(obj_id, function(valor, prop) {
            var llaves = findVariables(valor, '{', '}');
            _.each(llaves, function(llave) {
                elemento[id_field] = {};
                elemento[id_field][prop] = fila[llave];
            });
        });
    });
    if (OS_IOS) {
        e.section.updateItemAt(e.itemIndex, elemento, {
            animated: true
        });
    } else if (OS_ANDROID) {
        e.section.updateItemAt(e.itemIndex, elemento);
    }
    require('vars')[_var_scopekey]['flag_borrar'] = L('x734881840_traducir', 'false');

}

$.ID_1167041456.init({
    titulo: L('x8960895_traducir', 'GUARDAR DAÑOS'),
    __id: 'ALL1167041456',
    color: 'verde',
    onclick: Click_ID_1405657175
});

function Click_ID_1405657175(e) {

    var evento = e;
    $.ID_1507772706.ejecutar({});

}

$.ID_1507772706.init({
    titulo: L('x404129330_traducir', '¿EL ASEGURADO CONFIRMA ESTOS DATOS?'),
    __id: 'ALL1507772706',
    si: L('x1723413441_traducir', 'SI, Están correctos'),
    texto: L('x2083765231_traducir', 'El asegurado debe confirmar que los datos de esta sección están correctos'),
    pantalla: L('x2851883104_traducir', '¿ESTA DE ACUERDO?'),
    onno: no_ID_1559802673,
    onsi: si_ID_1740705700,
    no: L('x55492959_traducir', 'NO, Hay que modificar algo'),
    header: 'celeste'
});

function si_ID_1740705700(e) {

    var evento = e;
    if(false) console.log('enviamos a documentos', {});
    Alloy.createController("documentos_index", {}).getView().open();

}

function no_ID_1559802673(e) {

    var evento = e;

}

function Postlayout_ID_1498237322(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    $.ID_1099563253.show();

}

var ID_1846643595_opts = [L('x3481285698_traducir', 'Asegurado no puede seguir'), L('x3077268071_traducir', ' Se me acabo la bateria'), L('x479989047_traducir', ' Tuve un accidente'), L('x2376009830_traducir', ' Cancelar')];
var ID_1846643595 = Ti.UI.createOptionDialog({
    title: L('x1670176792_traducir', 'RAZON PARA CANCELAR INSPECCION ACTUAL'),
    options: ID_1846643595_opts
});
ID_1846643595.addEventListener('click', function(e) {
    var resp = ID_1846643595_opts[e.index];
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
    if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
        var razon = "";
        if (resp == L('x3481285698_traducir', 'Asegurado no puede seguir')) {
            razon = "Asegurado no puede seguir";
        }
        if (resp == L('x1862794075_traducir', 'Se me acabo la bateria')) {
            razon = "Se me acabo la bateria";
        }
        if (resp == L('x4012805111_traducir', 'Tuve un accidente')) {
            razon = "Tuve un accidente";
        }
        if(false) console.log('mi razon es', {
            "datos": razon
        });
        if ((_.isObject(razon) || (_.isString(razon)) && !_.isEmpty(razon)) || _.isNumber(razon) || _.isBoolean(razon)) {
            if(false) console.log('llamando servicio cancelarTarea', {});
            require('vars')['insp_cancelada'] = L('x2941610362_traducir', 'siniestro');
            Alloy.Collections[$.contenidos.config.adapter.collection_name].add($.contenidos);
            $.contenidos.save();
            Alloy.Collections[$.contenidos.config.adapter.collection_name].fetch();
            var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
            var ID_2062032128_visible = true;

            if (ID_2062032128_visible == 'si') {
                ID_2062032128_visible = true;
            } else if (ID_2062032128_visible == 'no') {
                ID_2062032128_visible = false;
            }
            $.ID_2062032128.setVisible(ID_2062032128_visible);

            var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
            var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
            if(false) console.log('detalle de seltarea', {
                "data": seltarea
            });
            var datos = {
                id_inspector: inspector.id_server,
                codigo_identificador: inspector.codigo_identificador,
                id_server: seltarea.id_server,
                num_caso: seltarea.num_caso,
                razon: razon
            };
            require('vars')[_var_scopekey]['datos'] = datos;
            var ID_2053502953 = {};
            ID_2053502953.success = function(e) {
                var elemento = e,
                    valor = e;
                var ID_2062032128_visible = false;

                if (ID_2062032128_visible == 'si') {
                    ID_2062032128_visible = true;
                } else if (ID_2062032128_visible == 'no') {
                    ID_2062032128_visible = false;
                }
                $.ID_2062032128.setVisible(ID_2062032128_visible);

                Alloy.createController("firma_index", {}).getView().open();
                var ID_1820528991_func = function() {
                    /** 
                     * Cerramos pantalla datos basicos 
                     */
                    Alloy.Events.trigger('_cerrar_insp', {
                        pantalla: L('x1805186499_traducir', 'contenidos')
                    });
                };
                var ID_1820528991 = setTimeout(ID_1820528991_func, 1000 * 0.5);
                elemento = null, valor = null;
            };
            ID_2053502953.error = function(e) {
                var elemento = e,
                    valor = e;
                if(false) console.log('hubo error llamando cancelarTarea, la agregamos a cola de salida', {
                    "elemento": elemento
                });
                if(false) console.log('agregando servicio cancelarTarea a cola', {});
                var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
                var datos = ('datos' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['datos'] : '';
                var ID_1048734726_m = Alloy.Collections.cola;
                var ID_1048734726_fila = Alloy.createModel('cola', {
                    data: JSON.stringify(datos),
                    id_tarea: seltarea.id_server,
                    tipo: 'cancelar'
                });
                ID_1048734726_m.add(ID_1048734726_fila);
                ID_1048734726_fila.save();
                _.defer(function() {});
                var ID_2062032128_visible = false;

                if (ID_2062032128_visible == 'si') {
                    ID_2062032128_visible = true;
                } else if (ID_2062032128_visible == 'no') {
                    ID_2062032128_visible = false;
                }
                $.ID_2062032128.setVisible(ID_2062032128_visible);

                Alloy.createController("firma_index", {}).getView().open();
                /** 
                 * Cerramos pantalla datos basicos 
                 */
                Alloy.Events.trigger('_cerrar_insp', {
                    pantalla: L('x1805186499_traducir', 'contenidos')
                });
                elemento = null, valor = null;
            };
            require('helper').ajaxUnico('ID_2053502953', '' + String.format(L('x3244141284', '%1$scancelarTarea'), url_server.toString()) + '', 'POST', {
                id_inspector: seltarea.id_inspector,
                codigo_identificador: inspector.codigo_identificador,
                id_tarea: seltarea.id_server,
                num_caso: seltarea.num_caso,
                mensaje: razon,
                opcion: 0,
                tipo: 1
            }, 15000, ID_2053502953);
        }
    }
    resp = null;
});
_my_events['_cerrar_insp,ID_1127730346'] = function(evento) {
    if (!_.isUndefined(evento.pantalla)) {
        if (evento.pantalla == L('x3680765087_traducir', 'contenidos')) {
            if(false) console.log('debug cerrando contenidos', {});
            var ID_1259369118_trycatch = {
                error: function(e) {
                    if(false) console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
                }
            };
            try {
                ID_1259369118_trycatch.error = function(evento) {
                    if(false) console.log('error cerrando contenidos', {});
                };
                $.ID_1094491373.close();
            } catch (e) {
                ID_1259369118_trycatch.error(e);
            }
        }
    } else {
        if(false) console.log('debug cerrando (todas) contenidos', {});
        var ID_1175164350_trycatch = {
            error: function(e) {
                if(false) console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
            }
        };
        try {
            ID_1175164350_trycatch.error = function(evento) {
                if(false) console.log('error cerrando contenidos', {});
            };
            $.ID_1094491373.close();
        } catch (e) {
            ID_1175164350_trycatch.error(e);
        }
    }
};
Alloy.Events.on('_cerrar_insp', _my_events['_cerrar_insp,ID_1127730346']);
var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
    /** 
     * restringimos contenidos mostrados a la inspeccion actual (por si los temporales aun tienen datos) 
     */
    ID_1504721941_filter = function(coll) {
        var filtered = coll.filter(function(m) {
            var _tests = [],
                _all_true = false,
                model = m.toJSON();
            _tests.push((model.id_inspeccion == seltarea.id_server));
            var _all_true_s = _.uniq(_tests);
            _all_true = (_all_true_s.length == 1 && _all_true_s[0] == true) ? true : false;
            return _all_true;
        });
        filtered = _.toArray(filtered);
        return filtered;
    };
    _.defer(function() {
        Alloy.Collections.insp_contenido.fetch();
    });
} else {
    /** 
     * Borramos dummies 
     */
    /** 
     * Borramos dummies 
     */
    var ID_325451425_i = Alloy.Collections.insp_contenido;
    var sql = "DELETE FROM " + ID_325451425_i.config.adapter.collection_name;
    var db = Ti.Database.open(ID_325451425_i.config.adapter.db_name);
    db.execute(sql);
    db.close();
    sql = null;
    db = null;
    ID_325451425_i.trigger('delete');
    _.defer(function() {
        Alloy.Collections.insp_contenido.fetch();
    });
}
require('vars')[_var_scopekey]['anteriores'] = L('x734881840_traducir', 'false');
require('vars')[_var_scopekey]['flag_borrar'] = L('x734881840_traducir', 'false');

function Postlayout_ID_1942514203(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var anteriores = ('anteriores' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['anteriores'] : '';
    /** 
     * Una vez ya cargada la tarea, dejamos la variable limpia para poder abrir otras pantallas 
     */
    require('vars')['var_abriendo'] = '';
    if (anteriores == false || anteriores == 'false') {
        /** 
         * Limpieza memoria ram 
         */
        Alloy.Events.trigger('_cerrar_insp', {
            pantalla: L('x3174879261_traducir', 'recintos')
        });
        require('vars')[_var_scopekey]['anteriores'] = L('x4261170317', 'true');
    }

}
if (OS_IOS || OS_ANDROID) {
    $.ID_1094491373.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}
//$.ID_1094491373.open();