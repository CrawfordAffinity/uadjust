var _bind4section={};
var _list_templates={};
var $dano1 = $.dano1.toJSON();

$.ID_862619153_window.setTitleAttributes({
color : 'WHITE'
}
);
var _activity; 
if (OS_ANDROID) { _activity = $.ID_862619153.activity; var abx = require('com.alcoapps.actionbarextras'); }
var _my_events = {}, _out_vars = {}, $item = {}, args = arguments[0] || {}; if ('__args' in args && '__id' in args['__args']) args.__id=args['__args'].__id; if ('__modelo' in args && _.keys(args.__modelo).length>0 && 'item' in $) { $.item.set(args.__modelo); $item = $.item.toJSON(); }
var _var_scopekey = 'ID_862619153';
require('vars')[_var_scopekey]={};
if (OS_ANDROID) {
   $.ID_862619153_window.addEventListener('open', function(e) {
   abx.setStatusbarColor("#FFFFFF");
   abx.setBackgroundColor("#b9aaf3");
   });
}

function Click_ID_918039475(e) {

e.cancelBubble=true;
var elemento=e.source;
/** 
* Limpiamos widget 
*/
$.ID_1413999243.limpiar({});
$.ID_1908023149.limpiar({});
$.ID_862619153.close();

}
function Click_ID_1489314930(e) {

e.cancelBubble=true;
var elemento=e.source;
 var test = $.dano1.toJSON();
if (_.isUndefined(test.id_partida)) {
/** 
* Validamos que lo ingresado sea correcto 
*/
var ID_1904813210_opts=[L('x1518866076_traducir','Aceptar')];
var ID_1904813210 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x1349590523_traducir','Seleccione partida'),
   buttonNames: ID_1904813210_opts
});
ID_1904813210.addEventListener('click', function(e) {
   var nulo=ID_1904813210_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_1904813210.show();
}
 else if ((_.isObject(test.id_partida) ||_.isString(test.id_partida)) &&  _.isEmpty(test.id_partida)) {
/** 
* Validamos que lo ingresado sea correcto 
*/
var ID_1998310583_opts=[L('x1518866076_traducir','Aceptar')];
var ID_1998310583 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x1349590523_traducir','Seleccione partida'),
   buttonNames: ID_1998310583_opts
});
ID_1998310583.addEventListener('click', function(e) {
   var nulo=ID_1998310583_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_1998310583.show();
} else if (_.isUndefined(test.id_tipodano)) {
var ID_1676427987_opts=[L('x1518866076_traducir','Aceptar')];
var ID_1676427987 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x2702600033_traducir','Seleccione el tipo de daño'),
   buttonNames: ID_1676427987_opts
});
ID_1676427987.addEventListener('click', function(e) {
   var nulo=ID_1676427987_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_1676427987.show();
} else if ((_.isObject(test.id_tipodano) ||_.isString(test.id_tipodano)) &&  _.isEmpty(test.id_tipodano)) {
var ID_6736920_opts=[L('x1518866076_traducir','Aceptar')];
var ID_6736920 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x2702600033_traducir','Seleccione el tipo de daño'),
   buttonNames: ID_6736920_opts
});
ID_6736920.addEventListener('click', function(e) {
   var nulo=ID_6736920_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_6736920.show();
} else if (_.isUndefined(test.id_unidadmedida)) {
var ID_1970097224_opts=[L('x1518866076_traducir','Aceptar')];
var ID_1970097224 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x4202932850_traducir','Seleccione la unidad de medida del daño'),
   buttonNames: ID_1970097224_opts
});
ID_1970097224.addEventListener('click', function(e) {
   var nulo=ID_1970097224_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_1970097224.show();
} else if ((_.isObject(test.id_unidadmedida) ||_.isString(test.id_unidadmedida)) &&  _.isEmpty(test.id_unidadmedida)) {
var ID_1025312344_opts=[L('x1518866076_traducir','Aceptar')];
var ID_1025312344 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x4202932850_traducir','Seleccione la unidad de medida del daño'),
   buttonNames: ID_1025312344_opts
});
ID_1025312344.addEventListener('click', function(e) {
   var nulo=ID_1025312344_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_1025312344.show();
} else if (_.isUndefined(test.superficie)) {
var ID_749050934_opts=[L('x1518866076_traducir','Aceptar')];
var ID_749050934 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x1057642944_traducir','Ingrese la cubicación del daño'),
   buttonNames: ID_749050934_opts
});
ID_749050934.addEventListener('click', function(e) {
   var nulo=ID_749050934_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_749050934.show();
} else if ((_.isObject(test.superficie) ||_.isString(test.superficie)) &&  _.isEmpty(test.superficie)) {
var ID_1769741478_opts=[L('x1518866076_traducir','Aceptar')];
var ID_1769741478 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x1057642944_traducir','Ingrese la cubicación del daño'),
   buttonNames: ID_1769741478_opts
});
ID_1769741478.addEventListener('click', function(e) {
   var nulo=ID_1769741478_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_1769741478.show();
} else {
if (!_.isUndefined(args._dato)) {
/** 
* Si estamos editando, debemos borrar modelo previo. 
*/
/** 
* Eliminamos registro del dano que estamos editando para guardar el nuevo 
*/
/** 
* Eliminamos registro del dano que estamos editando para guardar el nuevo 
*/
var ID_563872808_i=Alloy.Collections.insp_itemdanos;
var sql = 'DELETE FROM ' + ID_563872808_i.config.adapter.collection_name + ' WHERE id=\''+args._dato.id+'\'';
var db = Ti.Database.open(ID_563872808_i.config.adapter.db_name);
db.execute(sql);
db.close();
ID_563872808_i.trigger('delete');
}
/** 
* Guardamos en modelo el dano ingresado 
*/
Alloy.Collections[$.dano1.config.adapter.collection_name].add($.dano1);
$.dano1.save();
Alloy.Collections[$.dano1.config.adapter.collection_name].fetch();
/** 
* Limpiamos widget 
*/
$.ID_1413999243.limpiar({});
$.ID_1908023149.limpiar({});
$.ID_862619153.close();
} test = null;

}
function Click_ID_1189625414(e) {

e.cancelBubble=true;
var elemento=e.source;
if (Ti.App.deployType != 'production') console.log('enviando a pantalla listadodanos_index',{});
Alloy.createController("",{}).getView().open();

}

$.ID_1413999243.init({
titulo : L('x1384515306_traducir','TIPO DAÑO'),
cargando : L('x1740321226_traducir','cargando ..'),
__id : 'ALL1413999243',
left : 0,
onrespuesta : Respuesta_ID_661260707,
campo : L('x953397154_traducir','Tipo de Daño'),
onabrir : Abrir_ID_1926204680,
color : 'morado',
right : 0,
top : 20,
seleccione : L('x4123108455_traducir','seleccione tipo'),
activo : true,
onafterinit : Afterinit_ID_1843803247
}
);

function Afterinit_ID_1843803247(e) {

var evento=e;
var ID_1686389527_func = function() {
if (!_.isUndefined(args._dato)) {
var ID_478830922_i=Alloy.createCollection('insp_itemdanos');
var ID_478830922_i_where='id=\''+args._dato.id+'\'';
ID_478830922_i.fetch({ query: 'SELECT * FROM insp_itemdanos WHERE id=\''+args._dato.id+'\'' });
var danos=require('helper').query2array(ID_478830922_i);
if (danos && danos.length) {
/** 
* obtenemos valor seleccionado previamente y lo mostramos en selector. 
*/
var ID_1464067895_i=Alloy.createCollection('tipo_dano');
var ID_1464067895_i_where='id_segured=\''+danos[0].id_tipodano+'\'';
ID_1464067895_i.fetch({ query: 'SELECT * FROM tipo_dano WHERE id_segured=\''+danos[0].id_tipodano+'\'' });
var t_dano=require('helper').query2array(ID_1464067895_i);
if (t_dano && t_dano.length) {
var ID_1044639882_func = function() {
$.ID_1413999243.labels({valor : t_dano[0].nombre});
tdano = null;
danos = null
};
var ID_1044639882 = setTimeout(ID_1044639882_func, 1000*0.21);
}
}
}
 else {
var seltarea = ('seltarea' in require('vars'))?require('vars')['seltarea']:'';
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
var pais = ('pais' in require('vars'))?require('vars')['pais']:'';
var ID_1416587127_i=Alloy.createCollection('tipo_dano');
var ID_1416587127_i_where='pais_texto=\''+pais[0].nombre+'\' AND id_partida=0';
ID_1416587127_i.fetch({ query: 'SELECT * FROM tipo_dano WHERE pais_texto=\''+pais[0].nombre+'\' AND id_partida=0' });
var danos=require('helper').query2array(ID_1416587127_i);
var datos=[];
_.each(danos, function(fila, pos) {
  var new_row={};
  _.each(fila, function(x, llave) {
    var newkey='';
    if (llave=='nombre') newkey='valor';
    if (llave=='id_segured') newkey='id_interno';
    if (llave=='id_server') newkey='id_gerardo';
    if (newkey!='') new_row[newkey] = fila[llave];
  });
  datos.push(new_row);
});
}
 else {
/** 
* Insertamos dummies 
*/
/** 
* Insertamos dummies 
*/
var ID_91489196_i=Alloy.Collections.tipo_dano;
var sql = "DELETE FROM " + ID_91489196_i.config.adapter.collection_name;
var db = Ti.Database.open(ID_91489196_i.config.adapter.db_name);
db.execute(sql);
db.close();
sql=null;
db=null;
ID_91489196_i.trigger('delete');
var item_index = 0;
_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
item_index += 1;
var ID_652253309_m=Alloy.Collections.tipo_dano;
var ID_652253309_fila = Alloy.createModel('tipo_dano', {
nombre : String.format(L('x1478811929_traducir','Picada%1$s'), item.toString()),
id_partida : 0,
id_server : String.format(L('x1290178835_traducir','0%1$s'), item.toString()),
id_segured : String.format(L('x1125471167_traducir','10%1$s'), item.toString()),
pais : 1
}
 );
ID_652253309_m.add(ID_652253309_fila);
ID_652253309_fila.save();
});
var ID_1756640692_i=Alloy.createCollection('tipo_dano');
ID_1756640692_i.fetch();
var ID_1756640692_src=require('helper').query2array(ID_1756640692_i);
var datos=[];
_.each(ID_1756640692_src, function(fila, pos) {
  var new_row={};
  _.each(fila, function(x, llave) {
    var newkey='';
    if (llave=='nombre') newkey='valor';
    if (llave=='id_segured') newkey='id_interno';
    if (newkey!='') new_row[newkey] = fila[llave];
  });
  datos.push(new_row);
});
}
$.ID_1413999243.data({data : datos});
t_dano = null,
pais = null,
datos = null
}};
var ID_1686389527 = setTimeout(ID_1686389527_func, 1000*0.2);

}
function Abrir_ID_1926204680(e) {

var evento=e;

}
function Respuesta_ID_661260707(e) {

var evento=e;
/** 
* Mostramos el valor seleccionado desde el widget 
*/
$.ID_1413999243.labels({valor : evento.valor});
/** 
* Actualizamos el id_tipodano e id_unidadmedida 
*/
$.dano1.set({
id_tipodano : evento.item.id_interno
}
);
if ('dano1' in $) $dano1=$.dano1.toJSON();
var seltarea = ('seltarea' in require('vars'))?require('vars')['seltarea']:'';
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
/** 
* Consultamos id_gerardo en tipo_accion, para obtener unidades de medida 
*/
var ID_1752700999_i=Alloy.createCollection('tipo_accion');
var ID_1752700999_i_where='id_tipo_dano=\''+evento.item.id_gerardo+'\'';
ID_1752700999_i.fetch({ query: 'SELECT * FROM tipo_accion WHERE id_tipo_dano=\''+evento.item.id_gerardo+'\'' });
var acciones=require('helper').query2array(ID_1752700999_i);
if (acciones && acciones.length) {
var pais = ('pais' in require('vars'))?require('vars')['pais']:'';
var ID_1138954688_i=Alloy.createCollection('unidad_medida');
var ID_1138954688_i_where='pais_texto=\''+pais[0].nombre+'\' AND id_accion=\''+acciones[0].id_server+'\'';
ID_1138954688_i.fetch({ query: 'SELECT * FROM unidad_medida WHERE pais_texto=\''+pais[0].nombre+'\' AND id_accion=\''+acciones[0].id_server+'\'' });
var unidad=require('helper').query2array(ID_1138954688_i);
var datos=[];
_.each(unidad, function(fila, pos) {
  var new_row={};
  _.each(fila, function(x, llave) {
    var newkey='';
    if (llave=='nombre') newkey='valor';
    if (llave=='id_segured') newkey='id_interno';
    if (llave=='id_server') newkey='id_gerardo';
    if (llave=='abrev') newkey='abrev';
    if (newkey!='') new_row[newkey] = fila[llave];
  });
  datos.push(new_row);
});
if (Ti.App.deployType != 'production') console.log('los datos enviados',{"asd":datos});
$.ID_1908023149.data({data : datos});
}
}
/** 
* Actualizamos texto de widget unidad de medida 
*/
$.ID_1908023149.labels({seleccione : L('x3621251639_traducir', 'unidad')});
var superficie2;
superficie2 = $.ID_572364594;
 superficie2.setValue("");
$.ID_1766077739.setText('-');

unidad = null,
datos = null

}

$.ID_1420842518.init({
caja : 45,
__id : 'ALL1420842518',
onlisto : Listo_ID_1216425867,
onclick : Click_ID_639064828
}
);

function Click_ID_639064828(e) {

var evento=e;
/** 
* Capturar foto 
*/
var ID_1567787361_result = function(e) {
   var foto_full = e.valor;
   var foto = e.valor.data;
if (foto_full.error==true||foto_full.error=='true') {
/** 
* Si existe error al capturar foto mostramos alerta 
*/
var ID_1243820738_opts=[L('x1518866076_traducir','Aceptar')];
var ID_1243820738 = Ti.UI.createAlertDialog({
   title: L('x57652245_traducir','Alerta'),
   message: L('x1068520423_traducir','Existe un problema con la camara'),
   buttonNames: ID_1243820738_opts
});
ID_1243820738.addEventListener('click', function(e) {
   var suu=ID_1243820738_opts[e.index];
suu = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_1243820738.show();
}
 else if (foto_full.cancel==true||foto_full.cancel=='true') {
if (Ti.App.deployType != 'production') console.log('cancelada',{});
$.ID_1420842518.detener({});
} else {
$.ID_1420842518.procesar({imagen : foto,nueva : 640,calidad : 87});
} foto = null;
};
function camara_ID_1567787361() {
   Ti.Media.showCamera({
      success: function(event) {
         if (event.mediaType==Ti.Media.MEDIA_TYPE_PHOTO) {
            ID_1567787361_result({ valor:{ error:false, cancel:false, data:event.media, reason:'' } }); 
         } else {
            ID_1567787361_result({ valor:{ error:true, cancel:false, data:'', reason:'not image' } }); 
         }
      },
      cancel: function() {
         ID_1567787361_result({ valor:{ error:false, cancel:true, data:'', reason:'cancelled' } }); 
      },
      error: function(error) {
         ID_1567787361_result({ valor:{ error:true, cancel:false, data:error, reason:error.error } }); 
      },
      saveToPhotoGallery:false,
      allowImageEditing:true,
      mediaTypes:[Ti.Media.MEDIA_TYPE_PHOTO]
   });
}
require('vars')['_estadoflash_']='auto';
require('vars')['_tipocamara_']='trasera';
require('vars')['_hayflash_']=true;
if (Ti.Media.hasCameraPermissions()) {
   camara_ID_1567787361();
} else {
   Ti.Media.requestCameraPermissions(function(ercp) {
      if (ercp.success) {
         camara_ID_1567787361();
      } else {
         ID_1567787361_result({ valor:{ error:true, cancel:false, data:'nopermission', type:'permission', reason:'camera doesnt have permissions' } });
      }
   });
}

}
function Listo_ID_1216425867(e) {

var evento=e;
var seltarea = ('seltarea' in require('vars'))?require('vars')['seltarea']:'';
var ID_614759559_m=Alloy.Collections.numero_unico;
var ID_614759559_fila = Alloy.createModel('numero_unico', {
comentario : 'nuevo itemdano foto1'
}
 );
ID_614759559_m.add(ID_614759559_fila);
ID_614759559_fila.save();
var nuevoid = require('helper').model2object(ID_614759559_m.last());
if (!_.isUndefined(evento.comprimida)) {
if (Ti.App.deployType != 'production') console.log(String.format(L('x2721240094','imagen capturada, comprimida y asignada como cap%1$s.jpg'), nuevoid.id.toString()),{});
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
var ID_1157857198_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'inspeccion'+seltarea.id_server);
if (ID_1157857198_d.exists()==false) ID_1157857198_d.createDirectory();
var ID_1157857198_f = Ti.Filesystem.getFile(ID_1157857198_d.resolve(),'cap'+nuevoid.id+'.jpg');
if (ID_1157857198_f.exists()==true) ID_1157857198_f.deleteFile();
ID_1157857198_f.write(evento.comprimida);
ID_1157857198_d = null; ID_1157857198_f = null;
var ID_1712680165_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'mini'+seltarea.id_server);
if (ID_1712680165_d.exists()==false) ID_1712680165_d.createDirectory();
var ID_1712680165_f = Ti.Filesystem.getFile(ID_1712680165_d.resolve(),'cap'+nuevoid.id+'.jpg');
if (ID_1712680165_f.exists()==true) ID_1712680165_f.deleteFile();
ID_1712680165_f.write(evento.mini);
ID_1712680165_d = null; ID_1712680165_f = null;
}
 else {
var ID_320260826_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'inspeccion'+0);
if (ID_320260826_d.exists()==false) ID_320260826_d.createDirectory();
var ID_320260826_f = Ti.Filesystem.getFile(ID_320260826_d.resolve(),'cap'+nuevoid.id+'.jpg');
if (ID_320260826_f.exists()==true) ID_320260826_f.deleteFile();
ID_320260826_f.write(evento.comprimida);
ID_320260826_d = null; ID_320260826_f = null;
var ID_1120725152_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'mini'+0);
if (ID_1120725152_d.exists()==false) ID_1120725152_d.createDirectory();
var ID_1120725152_f = Ti.Filesystem.getFile(ID_1120725152_d.resolve(),'cap'+nuevoid.id+'.jpg');
if (ID_1120725152_f.exists()==true) ID_1120725152_f.deleteFile();
ID_1120725152_f.write(evento.mini);
ID_1120725152_d = null; ID_1120725152_f = null;
}$.dano1.set({
foto1 : String.format(L('x1070475521','cap%1$s.jpg'), nuevoid.id.toString())
}
);
if ('dano1' in $) $dano1=$.dano1.toJSON();
}

}

$.ID_677343075.init({
caja : 45,
__id : 'ALL677343075',
onlisto : Listo_ID_493184970,
onclick : Click_ID_838952427
}
);

function Click_ID_838952427(e) {

var evento=e;
var ID_703857420_result = function(e) {
   var foto_full = e.valor;
   var foto = e.valor.data;
if (foto_full.error==true||foto_full.error=='true') {
var ID_505859193_opts=[L('x1518866076_traducir','Aceptar')];
var ID_505859193 = Ti.UI.createAlertDialog({
   title: L('x57652245_traducir','Alerta'),
   message: L('x1068520423_traducir','Existe un problema con la camara'),
   buttonNames: ID_505859193_opts
});
ID_505859193.addEventListener('click', function(e) {
   var suu=ID_505859193_opts[e.index];
suu = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_505859193.show();
}
 else if (foto_full.cancel==true||foto_full.cancel=='true') {
if (Ti.App.deployType != 'production') console.log('cancelada',{});
$.ID_677343075.detener({});
} else {
$.ID_677343075.procesar({imagen : foto,nueva : 640,calidad : 87});
} foto = null;
};
function camara_ID_703857420() {
   Ti.Media.showCamera({
      success: function(event) {
         if (event.mediaType==Ti.Media.MEDIA_TYPE_PHOTO) {
            ID_703857420_result({ valor:{ error:false, cancel:false, data:event.media, reason:'' } }); 
         } else {
            ID_703857420_result({ valor:{ error:true, cancel:false, data:'', reason:'not image' } }); 
         }
      },
      cancel: function() {
         ID_703857420_result({ valor:{ error:false, cancel:true, data:'', reason:'cancelled' } }); 
      },
      error: function(error) {
         ID_703857420_result({ valor:{ error:true, cancel:false, data:error, reason:error.error } }); 
      },
      saveToPhotoGallery:false,
      allowImageEditing:true,
      mediaTypes:[Ti.Media.MEDIA_TYPE_PHOTO]
   });
}
require('vars')['_estadoflash_']='auto';
require('vars')['_tipocamara_']='trasera';
require('vars')['_hayflash_']=true;
if (Ti.Media.hasCameraPermissions()) {
   camara_ID_703857420();
} else {
   Ti.Media.requestCameraPermissions(function(ercp) {
      if (ercp.success) {
         camara_ID_703857420();
      } else {
         ID_703857420_result({ valor:{ error:true, cancel:false, data:'nopermission', type:'permission', reason:'camera doesnt have permissions' } });
      }
   });
}

}
function Listo_ID_493184970(e) {

var evento=e;
var seltarea = ('seltarea' in require('vars'))?require('vars')['seltarea']:'';
var ID_46080488_m=Alloy.Collections.numero_unico;
var ID_46080488_fila = Alloy.createModel('numero_unico', {
comentario : 'nuevo itemdano foto2'
}
 );
ID_46080488_m.add(ID_46080488_fila);
ID_46080488_fila.save();
var nuevoid = require('helper').model2object(ID_46080488_m.last());
if (!_.isUndefined(evento.comprimida)) {
if (Ti.App.deployType != 'production') console.log(String.format(L('x2721240094','imagen capturada, comprimida y asignada como cap%1$s.jpg'), nuevoid.id.toString()),{});
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
var ID_1283075062_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'inspeccion'+seltarea.id_server);
if (ID_1283075062_d.exists()==false) ID_1283075062_d.createDirectory();
var ID_1283075062_f = Ti.Filesystem.getFile(ID_1283075062_d.resolve(),'cap'+nuevoid.id+'.jpg');
if (ID_1283075062_f.exists()==true) ID_1283075062_f.deleteFile();
ID_1283075062_f.write(evento.comprimida);
ID_1283075062_d = null; ID_1283075062_f = null;
var ID_1960146281_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'mini'+seltarea.id_server);
if (ID_1960146281_d.exists()==false) ID_1960146281_d.createDirectory();
var ID_1960146281_f = Ti.Filesystem.getFile(ID_1960146281_d.resolve(),'cap'+nuevoid.id+'.jpg');
if (ID_1960146281_f.exists()==true) ID_1960146281_f.deleteFile();
ID_1960146281_f.write(evento.mini);
ID_1960146281_d = null; ID_1960146281_f = null;
}
 else {
var ID_1870012101_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'inspeccion'+0);
if (ID_1870012101_d.exists()==false) ID_1870012101_d.createDirectory();
var ID_1870012101_f = Ti.Filesystem.getFile(ID_1870012101_d.resolve(),'cap'+nuevoid.id+'.jpg');
if (ID_1870012101_f.exists()==true) ID_1870012101_f.deleteFile();
ID_1870012101_f.write(evento.comprimida);
ID_1870012101_d = null; ID_1870012101_f = null;
var ID_1060399557_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'mini'+0);
if (ID_1060399557_d.exists()==false) ID_1060399557_d.createDirectory();
var ID_1060399557_f = Ti.Filesystem.getFile(ID_1060399557_d.resolve(),'cap'+nuevoid.id+'.jpg');
if (ID_1060399557_f.exists()==true) ID_1060399557_f.deleteFile();
ID_1060399557_f.write(evento.mini);
ID_1060399557_d = null; ID_1060399557_f = null;
}$.dano1.set({
foto2 : String.format(L('x1070475521','cap%1$s.jpg'), nuevoid.id.toString())
}
);
if ('dano1' in $) $dano1=$.dano1.toJSON();
}

}

$.ID_333724151.init({
caja : 45,
__id : 'ALL333724151',
onlisto : Listo_ID_890378561,
onclick : Click_ID_1715857076
}
);

function Click_ID_1715857076(e) {

var evento=e;
var ID_272858313_result = function(e) {
   var foto_full = e.valor;
   var foto = e.valor.data;
if (foto_full.error==true||foto_full.error=='true') {
var ID_1557270757_opts=[L('x1518866076_traducir','Aceptar')];
var ID_1557270757 = Ti.UI.createAlertDialog({
   title: L('x57652245_traducir','Alerta'),
   message: L('x1068520423_traducir','Existe un problema con la camara'),
   buttonNames: ID_1557270757_opts
});
ID_1557270757.addEventListener('click', function(e) {
   var suu=ID_1557270757_opts[e.index];
suu = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_1557270757.show();
}
 else if (foto_full.cancel==true||foto_full.cancel=='true') {
if (Ti.App.deployType != 'production') console.log('cancelar',{});
$.ID_333724151.detener({});
} else {
$.ID_333724151.procesar({imagen : foto,nueva : 640,calidad : 87});
} foto = null;
};
function camara_ID_272858313() {
   Ti.Media.showCamera({
      success: function(event) {
         if (event.mediaType==Ti.Media.MEDIA_TYPE_PHOTO) {
            ID_272858313_result({ valor:{ error:false, cancel:false, data:event.media, reason:'' } }); 
         } else {
            ID_272858313_result({ valor:{ error:true, cancel:false, data:'', reason:'not image' } }); 
         }
      },
      cancel: function() {
         ID_272858313_result({ valor:{ error:false, cancel:true, data:'', reason:'cancelled' } }); 
      },
      error: function(error) {
         ID_272858313_result({ valor:{ error:true, cancel:false, data:error, reason:error.error } }); 
      },
      saveToPhotoGallery:false,
      allowImageEditing:true,
      mediaTypes:[Ti.Media.MEDIA_TYPE_PHOTO]
   });
}
require('vars')['_estadoflash_']='auto';
require('vars')['_tipocamara_']='trasera';
require('vars')['_hayflash_']=true;
if (Ti.Media.hasCameraPermissions()) {
   camara_ID_272858313();
} else {
   Ti.Media.requestCameraPermissions(function(ercp) {
      if (ercp.success) {
         camara_ID_272858313();
      } else {
         ID_272858313_result({ valor:{ error:true, cancel:false, data:'nopermission', type:'permission', reason:'camera doesnt have permissions' } });
      }
   });
}

}
function Listo_ID_890378561(e) {

var evento=e;
var seltarea = ('seltarea' in require('vars'))?require('vars')['seltarea']:'';
var ID_1934265279_m=Alloy.Collections.numero_unico;
var ID_1934265279_fila = Alloy.createModel('numero_unico', {
comentario : 'nuevo itemdano foto3'
}
 );
ID_1934265279_m.add(ID_1934265279_fila);
ID_1934265279_fila.save();
var nuevoid = require('helper').model2object(ID_1934265279_m.last());
if (!_.isUndefined(evento.comprimida)) {
if (Ti.App.deployType != 'production') console.log(String.format(L('x2721240094','imagen capturada, comprimida y asignada como cap%1$s.jpg'), nuevoid.id.toString()),{});
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
var ID_900222749_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'inspeccion'+seltarea.id_server);
if (ID_900222749_d.exists()==false) ID_900222749_d.createDirectory();
var ID_900222749_f = Ti.Filesystem.getFile(ID_900222749_d.resolve(),'cap'+nuevoid.id+'.jpg');
if (ID_900222749_f.exists()==true) ID_900222749_f.deleteFile();
ID_900222749_f.write(evento.comprimida);
ID_900222749_d = null; ID_900222749_f = null;
var ID_218311107_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'mini'+seltarea.id_server);
if (ID_218311107_d.exists()==false) ID_218311107_d.createDirectory();
var ID_218311107_f = Ti.Filesystem.getFile(ID_218311107_d.resolve(),'cap'+nuevoid.id+'.jpg');
if (ID_218311107_f.exists()==true) ID_218311107_f.deleteFile();
ID_218311107_f.write(evento.mini);
ID_218311107_d = null; ID_218311107_f = null;
}
 else {
var ID_1306087304_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'inspeccion'+0);
if (ID_1306087304_d.exists()==false) ID_1306087304_d.createDirectory();
var ID_1306087304_f = Ti.Filesystem.getFile(ID_1306087304_d.resolve(),'cap'+nuevoid.id+'.jpg');
if (ID_1306087304_f.exists()==true) ID_1306087304_f.deleteFile();
ID_1306087304_f.write(evento.comprimida);
ID_1306087304_d = null; ID_1306087304_f = null;
var ID_1408182389_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'mini'+0);
if (ID_1408182389_d.exists()==false) ID_1408182389_d.createDirectory();
var ID_1408182389_f = Ti.Filesystem.getFile(ID_1408182389_d.resolve(),'cap'+nuevoid.id+'.jpg');
if (ID_1408182389_f.exists()==true) ID_1408182389_f.deleteFile();
ID_1408182389_f.write(evento.mini);
ID_1408182389_d = null; ID_1408182389_f = null;
}$.dano1.set({
foto3 : String.format(L('x1070475521','cap%1$s.jpg'), nuevoid.id.toString())
}
);
if ('dano1' in $) $dano1=$.dano1.toJSON();
}

}

$.ID_1908023149.init({
titulo : L('x52303785_traducir','UNIDAD'),
cargando : L('x1740321226_traducir','cargando ..'),
__id : 'ALL1908023149',
left : 0,
onrespuesta : Respuesta_ID_1901278964,
campo : L('x2054670809_traducir','Unidad de medida'),
onabrir : Abrir_ID_202281478,
color : 'morado',
right : 5,
top : 3,
seleccione : L('x4091990063_traducir','unidad'),
activo : true,
onafterinit : Afterinit_ID_1459788538
}
);

function Afterinit_ID_1459788538(e) {

var evento=e;
if (!_.isUndefined(args._dato)) {
/** 
* Si args._dato existe estamos editando dano 
*/
/** 
* Consulta model insp_itemdanos 
*/
var ID_923688167_i=Alloy.createCollection('insp_itemdanos');
var ID_923688167_i_where='id=\''+args._dato.id+'\'';
ID_923688167_i.fetch({ query: 'SELECT * FROM insp_itemdanos WHERE id=\''+args._dato.id+'\'' });
var danos=require('helper').query2array(ID_923688167_i);
if (danos && danos.length) {
/** 
* obtenemos valor seleccionado previamente y lo mostramos en selector. 
*/
var ID_697363968_i=Alloy.createCollection('unidad_medida');
var ID_697363968_i_where='id_segured=\''+danos[0].id_unidadmedida+'\'';
ID_697363968_i.fetch({ query: 'SELECT * FROM unidad_medida WHERE id_segured=\''+danos[0].id_unidadmedida+'\'' });
var u_medida=require('helper').query2array(ID_697363968_i);
if (u_medida && u_medida.length) {
require('vars')[_var_scopekey]['umedida_texto']=u_medida[0];
var ID_1174927578_func = function() {
var umedida_texto = ('umedida_texto' in require('vars')[_var_scopekey])?require('vars')[_var_scopekey]['umedida_texto']:'';
$.ID_1908023149.labels({valor : umedida_texto.nombre});
$.ID_1766077739.setText(umedida_texto.abrev);

};
var ID_1174927578 = setTimeout(ID_1174927578_func, 1000*0.5);
var seltarea = ('seltarea' in require('vars'))?require('vars')['seltarea']:'';
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
 var uu_medida = [u_medida[0]];
/** 
* obtenemos datos para selectores (solo los de este pais) 
*/
var datos_um=[];
_.each(uu_medida, function(fila, pos) {
  var new_row={};
  _.each(fila, function(x, llave) {
    var newkey='';
    if (llave=='nombre') newkey='valor';
    if (llave=='id_segured') newkey='id_interno';
    if (llave=='id_server') newkey='id_gerardo';
    if (llave=='abrev') newkey='abrev';
    if (newkey!='') new_row[newkey] = fila[llave];
  });
  datos_um.push(new_row);
});
/** 
* Cargamos el widget con los datos de tipo de unidad de medida 
*/
$.ID_1908023149.data({data : datos_um});
}
}
}
}
 else {
var seltarea = ('seltarea' in require('vars'))?require('vars')['seltarea']:'';
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
/** 
* Viene de login 
*/
var pais = ('pais' in require('vars'))?require('vars')['pais']:'';
var ID_17383129_i=Alloy.createCollection('unidad_medida');
var ID_17383129_i_where='pais_texto=\''+pais[0].nombre+'\' AND id_accion=0';
ID_17383129_i.fetch({ query: 'SELECT * FROM unidad_medida WHERE pais_texto=\''+pais[0].nombre+'\' AND id_accion=0' });
var unidad=require('helper').query2array(ID_17383129_i);
/** 
* obtenemos datos para selectores (solo los de este pais) 
*/
var datos=[];
_.each(unidad, function(fila, pos) {
  var new_row={};
  _.each(fila, function(x, llave) {
    var newkey='';
    if (llave=='nombre') newkey='valor';
    if (llave=='id_segured') newkey='id_interno';
    if (llave=='id_server') newkey='id_gerardo';
    if (llave=='abrev') newkey='abrev';
    if (newkey!='') new_row[newkey] = fila[llave];
  });
  datos.push(new_row);
});
}
 else {
/** 
* Insertamos dummies 
*/
/** 
* Insertamos dummies 
*/
var ID_727668450_i=Alloy.Collections.unidad_medida;
var sql = "DELETE FROM " + ID_727668450_i.config.adapter.collection_name;
var db = Ti.Database.open(ID_727668450_i.config.adapter.db_name);
db.execute(sql);
db.close();
sql=null;
db=null;
ID_727668450_i.trigger('delete');
var item_index = 0;
_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
item_index += 1;
var ID_1452773782_m=Alloy.Collections.unidad_medida;
var ID_1452773782_fila = Alloy.createModel('unidad_medida', {
nombre : String.format(L('x2542530260_traducir','Metro lineal%1$s'), item.toString()),
id_server : String.format(L('x1290178835_traducir','0%1$s'), item.toString()),
id_segured : String.format(L('x1125471167_traducir','10%1$s'), item.toString()),
pais : 1,
abrev : String.format(L('x339551322_traducir','ML%1$s'), item.toString())
}
 );
ID_1452773782_m.add(ID_1452773782_fila);
ID_1452773782_fila.save();
});
/** 
* Transformamos nombres de tablas 
*/
var ID_134991602_i=Alloy.createCollection('unidad_medida');
ID_134991602_i.fetch();
var ID_134991602_src=require('helper').query2array(ID_134991602_i);
var datos=[];
_.each(ID_134991602_src, function(fila, pos) {
  var new_row={};
  _.each(fila, function(x, llave) {
    var newkey='';
    if (llave=='nombre') newkey='valor';
    if (llave=='id_segured') newkey='id_interno';
    if (llave=='id_server') newkey='id_gerardo';
    if (llave=='abrev') newkey='abrev';
    if (newkey!='') new_row[newkey] = fila[llave];
  });
  datos.push(new_row);
});
}/** 
* Cargamos el widget con los datos de tipo de unidad de medida 
*/
$.ID_1908023149.data({data : datos});
}
}
function Abrir_ID_202281478(e) {

var evento=e;

}
function Respuesta_ID_1901278964(e) {

var evento=e;
/** 
* Mostramos el valor seleccionado desde el widget 
*/
$.ID_1908023149.labels({valor : evento.valor});
/** 
* Actualizamos el id de la unidad de medida 
*/
$.dano1.set({
id_unidadmedida : evento.item.id_interno
}
);
if ('dano1' in $) $dano1=$.dano1.toJSON();
/** 
* modifica el label con la abreviatura de la unidad de medida seleccionada. 
*/
$.ID_1766077739.setText(evento.item.abrev);

if (Ti.App.deployType != 'production') console.log('datos recibidos de modal tipo partidas',{"datos":evento});

}
function Change_ID_1171433432(e) {

e.cancelBubble=true;
var elemento=e.value;
var source=e.source;
/** 
* Al escribir la descripcion, vamos actualizando en la tabla de danos la superficie 
*/
$.dano1.set({
superficie : elemento
}
);
if ('dano1' in $) $dano1=$.dano1.toJSON();
elemento=null, source=null;

}
function Change_ID_440233612(e) {

e.cancelBubble=true;
var elemento=e.value;
var source=e.source;
$.dano1.set({
descripcion_dano : elemento
}
);
if ('dano1' in $) $dano1=$.dano1.toJSON();
elemento=null, source=null;

}

(function() {
var seltarea = ('seltarea' in require('vars'))?require('vars')['seltarea']:'';
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
/** 
* Modificamos la idinspeccion con el idserver de la tarea 
*/
var seltarea = ('seltarea' in require('vars'))?require('vars')['seltarea']:'';
$.dano1.set({
id_inspeccion : seltarea.id_server
}
);
if ('dano1' in $) $dano1=$.dano1.toJSON();
}
if (!_.isUndefined(args._dato)) {
/** 
* Si existe args._dato estamos editando dano 
*/
/** 
* Modificamos titulo de header si estamos editando 
*/
var ID_862619153_titulo = 'EDITAR DAÑO';

									var setTitle2 = function(valor) {
										if (OS_ANDROID) {
											abx.title = valor;
										} else {
											$.ID_862619153_window.setTitle(valor);
										}
									};
									var getTitle2 = function() {
										if (OS_ANDROID) {
											return abx.title;
										} else {
											return $.ID_862619153_window.getTitle();
										}
									};
									setTitle2(ID_862619153_titulo);

/** 
* heredamos datos previos de nivel en modelo nuevo con binding de esta pantalla 
*/
var ID_297123514_i=Alloy.createCollection('insp_itemdanos');
var ID_297123514_i_where='id=\''+args._dato.id+'\'';
ID_297123514_i.fetch({ query: 'SELECT * FROM insp_itemdanos WHERE id=\''+args._dato.id+'\'' });
var insp_danos=require('helper').query2array(ID_297123514_i);
/** 
* Cargamos el modelo con los datos obtenidos desde la consulta 
*/
$.dano1.set({
id_inspeccion : insp_danos[0].id_inspeccion,
nombre : insp_danos[0].nombre,
superficie : insp_danos[0].superficie,
foto1 : insp_danos[0].foto1,
id_partida : insp_danos[0].id_partida,
foto2 : insp_danos[0].foto2,
descripcion_dano : insp_danos[0].descripcion_dano,
id_recinto : temp_idrecinto,
foto3 : insp_danos[0].foto3,
id_tipodano : insp_danos[0].id_tipodano,
id_unidadmedida : insp_danos[0].id_unidadmedida
}
);
if ('dano1' in $) $dano1=$.dano1.toJSON();
$.ID_1516441729.setText(insp_danos[0].nombre);

$.ID_1516441729.setColor('#000000');

/** 
* Cargamos los textos en los campos de texto 
*/
$.ID_572364594.setValue(insp_danos[0].superficie);

$.ID_1024942406.setValue(insp_danos[0].descripcion_dano);

if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
/** 
* Revisamos si existe variable seltarea para saber si las fotos son desde la inspeccion o son datos dummy 
*/
if (Ti.App.deployType != 'production') console.log('es parte de la compilacion inspeccion completa',{});
if ((_.isObject(insp_danos[0].foto1) || (_.isString(insp_danos[0].foto1)) &&  !_.isEmpty(insp_danos[0].foto1)) || _.isNumber(insp_danos[0].foto1) || _.isBoolean(insp_danos[0].foto1)) {
var ID_204536747_trycatch = { error: function(e) { if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.',e); } };
try {
ID_204536747_trycatch.error = function(evento) {
};
var foto1 = '';
var ID_79340998_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'mini'+insp_danos[0].id_inspeccion);
if (ID_79340998_d.exists()==true) {
  var ID_79340998_f = Ti.Filesystem.getFile(ID_79340998_d.resolve(),insp_danos[0].foto1);
  if (ID_79340998_f.exists()==true) {
    foto1 = ID_79340998_f.read();
  }
  ID_79340998_f = null;
}
ID_79340998_d = null;
} catch (e) {
   ID_204536747_trycatch.error(e);
}
}
if ((_.isObject(insp_danos[0].foto2) || (_.isString(insp_danos[0].foto2)) &&  !_.isEmpty(insp_danos[0].foto2)) || _.isNumber(insp_danos[0].foto2) || _.isBoolean(insp_danos[0].foto2)) {
var ID_592855513_trycatch = { error: function(e) { if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.',e); } };
try {
ID_592855513_trycatch.error = function(evento) {
};
var foto2 = '';
var ID_218718946_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'mini'+insp_danos[0].id_inspeccion);
if (ID_218718946_d.exists()==true) {
  var ID_218718946_f = Ti.Filesystem.getFile(ID_218718946_d.resolve(),insp_danos[0].foto2);
  if (ID_218718946_f.exists()==true) {
    foto2 = ID_218718946_f.read();
  }
  ID_218718946_f = null;
}
ID_218718946_d = null;
} catch (e) {
   ID_592855513_trycatch.error(e);
}
}
if ((_.isObject(insp_danos[0].foto3) || (_.isString(insp_danos[0].foto3)) &&  !_.isEmpty(insp_danos[0].foto3)) || _.isNumber(insp_danos[0].foto3) || _.isBoolean(insp_danos[0].foto3)) {
var ID_1128915382_trycatch = { error: function(e) { if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.',e); } };
try {
ID_1128915382_trycatch.error = function(evento) {
};
var foto3 = '';
var ID_1844717500_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'mini'+insp_danos[0].id_inspeccion);
if (ID_1844717500_d.exists()==true) {
  var ID_1844717500_f = Ti.Filesystem.getFile(ID_1844717500_d.resolve(),insp_danos[0].foto3);
  if (ID_1844717500_f.exists()==true) {
    foto3 = ID_1844717500_f.read();
  }
  ID_1844717500_f = null;
}
ID_1844717500_d = null;
} catch (e) {
   ID_1128915382_trycatch.error(e);
}
}
}
 else {
if (Ti.App.deployType != 'production') console.log('esta compilacion esta sola, simulamos carpetas',{});
if ((_.isObject(insp_danos[0].foto1) || (_.isString(insp_danos[0].foto1)) &&  !_.isEmpty(insp_danos[0].foto1)) || _.isNumber(insp_danos[0].foto1) || _.isBoolean(insp_danos[0].foto1)) {
var ID_496864751_trycatch = { error: function(e) { if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.',e); } };
try {
ID_496864751_trycatch.error = function(evento) {
};
var foto1 = '';
var ID_1412673214_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'mini'+0);
if (ID_1412673214_d.exists()==true) {
  var ID_1412673214_f = Ti.Filesystem.getFile(ID_1412673214_d.resolve(),insp_danos[0].foto1);
  if (ID_1412673214_f.exists()==true) {
    foto1 = ID_1412673214_f.read();
  }
  ID_1412673214_f = null;
}
ID_1412673214_d = null;
} catch (e) {
   ID_496864751_trycatch.error(e);
}
}
if ((_.isObject(insp_danos[0].foto2) || (_.isString(insp_danos[0].foto2)) &&  !_.isEmpty(insp_danos[0].foto2)) || _.isNumber(insp_danos[0].foto2) || _.isBoolean(insp_danos[0].foto2)) {
var ID_1979336114_trycatch = { error: function(e) { if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.',e); } };
try {
ID_1979336114_trycatch.error = function(evento) {
};
var foto2 = '';
var ID_771669433_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'mini'+0);
if (ID_771669433_d.exists()==true) {
  var ID_771669433_f = Ti.Filesystem.getFile(ID_771669433_d.resolve(),insp_danos[0].foto2);
  if (ID_771669433_f.exists()==true) {
    foto2 = ID_771669433_f.read();
  }
  ID_771669433_f = null;
}
ID_771669433_d = null;
} catch (e) {
   ID_1979336114_trycatch.error(e);
}
}
if ((_.isObject(insp_danos[0].foto3) || (_.isString(insp_danos[0].foto3)) &&  !_.isEmpty(insp_danos[0].foto3)) || _.isNumber(insp_danos[0].foto3) || _.isBoolean(insp_danos[0].foto3)) {
var ID_1042713336_trycatch = { error: function(e) { if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.',e); } };
try {
ID_1042713336_trycatch.error = function(evento) {
};
var foto3 = '';
var ID_998439173_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'mini'+0);
if (ID_998439173_d.exists()==true) {
  var ID_998439173_f = Ti.Filesystem.getFile(ID_998439173_d.resolve(),insp_danos[0].foto3);
  if (ID_998439173_f.exists()==true) {
    foto3 = ID_998439173_f.read();
  }
  ID_998439173_f = null;
}
ID_998439173_d = null;
} catch (e) {
   ID_1042713336_trycatch.error(e);
}
}
}if ((_.isObject(foto1) || (_.isString(foto1)) &&  !_.isEmpty(foto1)) || _.isNumber(foto1) || _.isBoolean(foto1)) {
/** 
* Revisamos que foto1, foto2 y foto3 contengan texto, por lo que si es asi, modificamos la imagen previa y ponemos la foto miniatura obtenida desde la memoria 
*/
if (Ti.App.deployType != 'production') console.log('llamamos evento mini',{});
$.ID_1420842518.mini({mini : foto1});
}
if ((_.isObject(foto2) || (_.isString(foto2)) &&  !_.isEmpty(foto2)) || _.isNumber(foto2) || _.isBoolean(foto2)) {
if (Ti.App.deployType != 'production') console.log('llamamos evento mini',{});
$.ID_677343075.mini({mini : foto2});
}
if ((_.isObject(foto3) || (_.isString(foto3)) &&  !_.isEmpty(foto3)) || _.isNumber(foto3) || _.isBoolean(foto3)) {
if (Ti.App.deployType != 'production') console.log('llamamos evento mini',{});
$.ID_333724151.mini({mini : foto3});
}
var ID_1464733672_i=Alloy.createCollection('tipo_partida');
var ID_1464733672_i_where='id_segured=\''+insp_danos[0].id_partida+'\'';
ID_1464733672_i.fetch({ query: 'SELECT * FROM tipo_partida WHERE id_segured=\''+insp_danos[0].id_partida+'\'' });
var t_partida=require('helper').query2array(ID_1464733672_i);
var pais = ('pais' in require('vars'))?require('vars')['pais']:'';
var ID_115150102_i=Alloy.createCollection('tipo_dano');
var ID_115150102_i_where='pais_texto=\''+pais[0].nombre+'\' AND id_partida=\''+t_partida[0].id_server+'\'';
ID_115150102_i.fetch({ query: 'SELECT * FROM tipo_dano WHERE pais_texto=\''+pais[0].nombre+'\' AND id_partida=\''+t_partida[0].id_server+'\'' });
var danos=require('helper').query2array(ID_115150102_i);
/** 
* Obtenemos datos para selectores (solo los de este pais y este tipo de partida) 
*/
var datos=[];
_.each(danos, function(fila, pos) {
  var new_row={};
  _.each(fila, function(x, llave) {
    var newkey='';
    if (llave=='nombre') newkey='valor';
    if (llave=='id_segured') newkey='id_interno';
    if (llave=='id_server') newkey='id_gerardo';
    if (newkey!='') new_row[newkey] = fila[llave];
  });
  datos.push(new_row);
});
$.ID_1413999243.data({data : datos});
var ID_1899132949_i=Alloy.createCollection('tipo_accion');
var ID_1899132949_i_where='id_segured=\''+insp_danos[0].id_tipodano+'\'';
ID_1899132949_i.fetch({ query: 'SELECT * FROM tipo_accion WHERE id_segured=\''+insp_danos[0].id_tipodano+'\'' });
var acciones=require('helper').query2array(ID_1899132949_i);
if (acciones && acciones.length) {
var pais = ('pais' in require('vars'))?require('vars')['pais']:'';
var ID_211084460_i=Alloy.createCollection('unidad_medida');
var ID_211084460_i_where='pais_texto=\''+pais[0].nombre+'\' AND id_accion=\''+acciones[0].id_server+'\'';
ID_211084460_i.fetch({ query: 'SELECT * FROM unidad_medida WHERE pais_texto=\''+pais[0].nombre+'\' AND id_accion=\''+acciones[0].id_server+'\'' });
var unidad=require('helper').query2array(ID_211084460_i);
var datos_um=[];
_.each(unidad, function(fila, pos) {
  var new_row={};
  _.each(fila, function(x, llave) {
    var newkey='';
    if (llave=='nombre') newkey='valor';
    if (llave=='id_segured') newkey='id_interno';
    if (llave=='id_server') newkey='id_gerardo';
    if (llave=='abrev') newkey='abrev';
    if (newkey!='') new_row[newkey] = fila[llave];
  });
  datos_um.push(new_row);
});
$.ID_1908023149.data({data : datos_um});
}
}
/** 
* Recuperamos la variable del id_recinto que tiene danos relacionados y actualizamos el modelo 
*/
var temp_idrecinto = ('temp_idrecinto' in require('vars'))?require('vars')['temp_idrecinto']:'';
if ((_.isObject(temp_idrecinto) || (_.isString(temp_idrecinto)) &&  !_.isEmpty(temp_idrecinto)) || _.isNumber(temp_idrecinto) || _.isBoolean(temp_idrecinto)) {
$.dano1.set({
id_recinto : temp_idrecinto
}
);
if ('dano1' in $) $dano1=$.dano1.toJSON();
}
 
insp_danos=null;
foto1=null;
foto2=null; 
foto3=null; 
seltarea=null; 
temp_idrecinto=null; 
datos=null, 
danos=null, 
t_partida = null;
datos_um=null, 
unidad=null, 
acciones=null
_my_events['resp_dato,ID_1243902372'] = function(evento) {
$.ID_1516441729.setText(evento.nombre);

$.ID_1516441729.setColor('#000000');

/** 
* Asumimos que el valor mostrado seleccionado es el nombre de la fila dano, ya que no existe el campo nombre en esta pantalla. 
*/
$.dano1.set({
nombre : evento.nombre,
id_partida : evento.id_partida,
id_tipodano : '',
id_unidadmedida : ''
}
);
if ('dano1' in $) $dano1=$.dano1.toJSON();
var seltarea = ('seltarea' in require('vars'))?require('vars')['seltarea']:'';
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
var pais = ('pais' in require('vars'))?require('vars')['pais']:'';
var ID_325613042_i=Alloy.createCollection('tipo_dano');
var ID_325613042_i_where='pais_texto=\''+pais[0].nombre+'\' AND id_partida=\''+evento.id_server+'\'';
ID_325613042_i.fetch({ query: 'SELECT * FROM tipo_dano WHERE pais_texto=\''+pais[0].nombre+'\' AND id_partida=\''+evento.id_server+'\'' });
var danos=require('helper').query2array(ID_325613042_i);
/** 
* Obtenemos datos para selectores (solo los de este pais y este tipo de partida) 
*/
var datos=[];
_.each(danos, function(fila, pos) {
  var new_row={};
  _.each(fila, function(x, llave) {
    var newkey='';
    if (llave=='nombre') newkey='valor';
    if (llave=='id_segured') newkey='id_interno';
    if (llave=='id_server') newkey='id_gerardo';
    if (newkey!='') new_row[newkey] = fila[llave];
  });
  datos.push(new_row);
});
$.ID_1413999243.data({data : datos});
}
/** 
* Editamos y limpiamos los campos que estan siendo filtrados con este campo 
*/
$.ID_1413999243.labels({seleccione : L('x2404293600_traducir', 'seleccione tipo')});
$.ID_1908023149.labels({seleccione : L('x3621251639_traducir', 'unidad')});
$.ID_572364594.setValue('');

$.ID_1766077739.setText('-');

};
Alloy.Events.on('resp_dato', _my_events['resp_dato,ID_1243902372']);
})();

function Postlayout_ID_154957096(e) {

e.cancelBubble=true;
var elemento=e.source;
require('vars')['var_abriendo']='';

}
if (OS_IOS || OS_ANDROID) {
$.ID_862619153.addEventListener('close',function(){
   $.destroy(); // cleanup bindings
   $.off(); //remove backbone events of this controller
   var _ev_tmp = null, _ev_rem = null;
   if (_my_events) {
      for(_ev_tmp in _my_events) { 
   		try {
   		    if (_ev_tmp.indexOf('_web')!=-1) {
   			   Ti.App.removeEventListener(_ev_tmp.split(',')[0],_my_events[_ev_tmp]);
   		    } else {
   			   Alloy.Events.off(_ev_tmp.split(',')[0],_my_events[_ev_tmp]);
   		    }
   		} catch(err10) {
   		}
      }
      _my_events = null;
      //delete _my_events;
   }
   if (_out_vars) {
      for(_ev_tmp in _out_vars) { 
         for (_ev_rem in _out_vars[_ev_tmp]._remove) {
            try {
 eval(_out_vars[_ev_tmp]._remove[_ev_rem]); 
 } catch(_errt) {}
         }
         _out_vars[_ev_tmp] = null;
      }
      _ev_tmp = null;
      //delete _out_vars;
   }
});
}