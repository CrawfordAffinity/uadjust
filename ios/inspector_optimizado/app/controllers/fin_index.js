var _bind4section = {};
var _list_templates = {};

$.ID_1334182915_window.setTitleAttributes({
    color: 'WHITE'
});
var _activity;
if (OS_ANDROID) {
    _activity = $.ID_1334182915.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_1334182915';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_1334182915_window.addEventListener('open', function(e) {
        abx.setStatusbarColor("#FFFFFF");
        abx.setBackgroundColor("#2d9edb");
    });
}


$.ID_395419113.init({
    titulo: L('x542998687_traducir', '¡FELICIDADES EN TERMINAR CON EXITO TU INSPECCION!'),
    __id: 'ALL395419113',
    texto: L('x1072203252_traducir', 'Tu inspección será recibida por nosotros, gracias por tu trabajo.'),
    top: 40,
    tipo: '_info'
});


$.ID_1012442242.init({
    titulo: L('x1197985134_traducir', 'SALIR'),
    __id: 'ALL1012442242',
    color: 'verde',
    onclick: Click_ID_1879821333
});

function Click_ID_1879821333(e) {

    var evento = e;
    /** 
     * Avisamos que ya no estamos en inspeccion 
     */
    require('vars')['inspeccion_encurso'] = L('x734881840_traducir', 'false');
    var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
    var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    var ID_2038341941_i = Alloy.Collections.tareas;
    var sql = 'DELETE FROM ' + ID_2038341941_i.config.adapter.collection_name + ' WHERE id_server=\'' + seltarea.id_server + '\'';
    var db = Ti.Database.open(ID_2038341941_i.config.adapter.db_name);
    db.execute(sql);
    db.close();
    ID_2038341941_i.trigger('remove');
    if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
        if (false) console.log('detectamos internet', {});
        var ID_1957389242 = {};
        if (false) console.log('DEBUG WEB: requesting url:' + String.format(L('x4082712253', '%1$sobtenerHistorialTareas'), url_server.toString()) + ' with data:', {
            _method: 'POST',
            _params: {
                id_inspector: inspector.id_server
            },
            _timeout: '15000'
        });

        ID_1957389242.success = function(e) {
            var elemento = e,
                valor = e;
            if (false) console.log('obtuvimos el historial', {});
            var ID_1380101211_i = Alloy.Collections.historial_tareas;
            var sql = "DELETE FROM " + ID_1380101211_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_1380101211_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            ID_1380101211_i.trigger('remove');
            var elemento_historial_tareas = elemento.historial_tareas;
            var ID_1545467650_m = Alloy.Collections.historial_tareas;
            var db_ID_1545467650 = Ti.Database.open(ID_1545467650_m.config.adapter.db_name);
            db_ID_1545467650.execute('BEGIN');
            _.each(elemento_historial_tareas, function(ID_1545467650_fila, pos) {
                db_ID_1545467650.execute('INSERT INTO historial_tareas (fecha_tarea, id_inspeccion, id_asegurado, nivel_2, comentario_can_o_rech, asegurado_tel_fijo, estado_tarea, bono, evento, id_inspector, asegurado_codigo_identificador, lat, nivel_1, asegurado_nombre, pais, direccion, asegurador, fecha_ingreso, fecha_siniestro, nivel_1_texto, distance, fecha_termino, nivel_4, perfil, asegurado_id, id_server, categoria, nivel_3, asegurado_correo, num_caso, lon, asegurado_tel_movil, nivel_5, tipo_tarea, hora_termino) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', ID_1545467650_fila.fecha_tarea, ID_1545467650_fila.id_inspeccion, ID_1545467650_fila.id_asegurado, ID_1545467650_fila.nivel_2, ID_1545467650_fila.comentario_can_o_rech, ID_1545467650_fila.asegurado_tel_fijo, ID_1545467650_fila.estado_tarea, ID_1545467650_fila.bono, ID_1545467650_fila.evento, ID_1545467650_fila.id_inspector, ID_1545467650_fila.asegurado_codigo_identificador, ID_1545467650_fila.lat, ID_1545467650_fila.nivel_1, ID_1545467650_fila.asegurado_nombre, ID_1545467650_fila.pais, ID_1545467650_fila.direccion, ID_1545467650_fila.asegurador, ID_1545467650_fila.fecha_ingreso, ID_1545467650_fila.fecha_siniestro, ID_1545467650_fila.nivel_1_, ID_1545467650_fila.distancia, ID_1545467650_fila.fecha_finalizacion, ID_1545467650_fila.nivel_4, 'ubicacion', ID_1545467650_fila.asegurado_id, ID_1545467650_fila.id, ID_1545467650_fila.categoria, ID_1545467650_fila.nivel_3, ID_1545467650_fila.asegurado_correo, ID_1545467650_fila.num_caso, ID_1545467650_fila.lon, ID_1545467650_fila.asegurado_tel_movil, ID_1545467650_fila.nivel_5, ID_1545467650_fila.tipo_tarea, ID_1545467650_fila.hora_finalizacion);
            });
            db_ID_1545467650.execute('COMMIT');
            db_ID_1545467650.close();
            db_ID_1545467650 = null;
            ID_1545467650_m.trigger('change');
            /** 
             * Llamo refresco tareas 
             */

            Alloy.Events.trigger('_refrescar_tareas');
            var ID_983129480_func = function() {
                /** 
                 * Para refrescar mistareas y otras ventanas 
                 */

                Alloy.Events.trigger('_refrescar_tareas_mistareas');
                /** 
                 * Refrescar pantalla hoy 
                 */

                Alloy.Events.trigger('_calcular_ruta');
                /** 
                 * Cerramos pantalla fin 
                 */
                $.ID_1334182915.close();
            };
            var ID_983129480 = setTimeout(ID_983129480_func, 1000 * 0.2);
            elemento = null, valor = null;
        };

        ID_1957389242.error = function(e) {
            var elemento = e,
                valor = e;
            if (false) console.log('hubo un problema para obtener el historial', {});
            var ID_1938634339_func = function() {
                /** 
                 * Para refrescar mistareas y otras ventanas 
                 */

                Alloy.Events.trigger('_refrescar_tareas_mistareas');
                /** 
                 * Refrescar pantalla hoy 
                 */

                Alloy.Events.trigger('_calcular_ruta');
                /** 
                 * Cerramos pantalla fin 
                 */
                $.ID_1334182915.close();
            };
            var ID_1938634339 = setTimeout(ID_1938634339_func, 1000 * 0.2);
            elemento = null, valor = null;
        };
        require('helper').ajaxUnico('ID_1957389242', '' + String.format(L('x4082712253', '%1$sobtenerHistorialTareas'), url_server.toString()) + '', 'POST', {
            id_inspector: inspector.id_server
        }, 15000, ID_1957389242);
    } else {
        if (false) console.log('no detectamos internet', {});
        var ID_1195852335_func = function() {
            /** 
             * Para refrescar mistareas y otras ventanas 
             */

            Alloy.Events.trigger('_refrescar_tareas_mistareas');
            /** 
             * Refrescar pantalla hoy 
             */

            Alloy.Events.trigger('_calcular_ruta');
            /** 
             * Cerramos pantalla fin 
             */
            $.ID_1334182915.close();
        };
        var ID_1195852335 = setTimeout(ID_1195852335_func, 1000 * 0.2);
    }
}

function Postlayout_ID_1671267202(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var anteriores = ('anteriores' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['anteriores'] : '';
    if (anteriores == false || anteriores == 'false') {
        /** 
         * Limpieza memoria 
         */

        Alloy.Events.trigger('_cerrar_insp', {
            pantalla: L('x2234318590_traducir', 'firma')
        });
        require('vars')[_var_scopekey]['anteriores'] = L('x4261170317', 'true');
    }

}

(function() {
    require('vars')[_var_scopekey]['anteriores'] = L('x734881840_traducir', 'false');
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
        /** 
         * Enviamos datos de inspeccion a servidor 
         */
        /** 
         * Enviamos datos 
         */

        $.ID_1012442242.iniciar_progreso({});
        var sta_consultando = L('x4157762361_traducir', 'status: consultando datos...');
        $.ID_1011874534.setText(sta_consultando);

        /** 
         * Consultamos todas las tablas de la inspeccion 
         */
        var ID_725448763_i = Alloy.createCollection('insp_fotosrequeridas');
        var ID_725448763_i_where = 'id_inspeccion=\'' + seltarea.id_server + '\'';
        ID_725448763_i.fetch({
            query: 'SELECT * FROM insp_fotosrequeridas WHERE id_inspeccion=\'' + seltarea.id_server + '\''
        });
        var fotos = require('helper').query2array(ID_725448763_i);
        var ID_1507288746_i = Alloy.createCollection('insp_datosbasicos');
        var ID_1507288746_i_where = 'id_inspeccion=\'' + seltarea.id_server + '\'';
        ID_1507288746_i.fetch({
            query: 'SELECT * FROM insp_datosbasicos WHERE id_inspeccion=\'' + seltarea.id_server + '\''
        });
        var basicos = require('helper').query2array(ID_1507288746_i);
        var ID_1624449420_i = Alloy.createCollection('insp_caracteristicas');
        var ID_1624449420_i_where = 'id_inspeccion=\'' + seltarea.id_server + '\'';
        ID_1624449420_i.fetch({
            query: 'SELECT * FROM insp_caracteristicas WHERE id_inspeccion=\'' + seltarea.id_server + '\''
        });
        var cara = require('helper').query2array(ID_1624449420_i);
        var ID_147765861_i = Alloy.createCollection('insp_niveles');
        var ID_147765861_i_where = 'id_inspeccion=\'' + seltarea.id_server + '\'';
        ID_147765861_i.fetch({
            query: 'SELECT * FROM insp_niveles WHERE id_inspeccion=\'' + seltarea.id_server + '\''
        });
        var niveles = require('helper').query2array(ID_147765861_i);
        var ID_264751747_i = Alloy.createCollection('insp_siniestro');
        var ID_264751747_i_where = 'id_inspeccion=\'' + seltarea.id_server + '\'';
        ID_264751747_i.fetch({
            query: 'SELECT * FROM insp_siniestro WHERE id_inspeccion=\'' + seltarea.id_server + '\''
        });
        var siniestro = require('helper').query2array(ID_264751747_i);
        var ID_429212272_i = Alloy.createCollection('insp_recintos');
        var ID_429212272_i_where = 'id_inspeccion=\'' + seltarea.id_server + '\'';
        ID_429212272_i.fetch({
            query: 'SELECT * FROM insp_recintos WHERE id_inspeccion=\'' + seltarea.id_server + '\''
        });
        var recintos = require('helper').query2array(ID_429212272_i);
        var ID_696972891_i = Alloy.createCollection('insp_itemdanos');
        var ID_696972891_i_where = 'id_inspeccion=\'' + seltarea.id_server + '\'';
        ID_696972891_i.fetch({
            query: 'SELECT * FROM insp_itemdanos WHERE id_inspeccion=\'' + seltarea.id_server + '\''
        });
        var danos = require('helper').query2array(ID_696972891_i);
        var ID_1764335302_i = Alloy.createCollection('insp_contenido');
        var ID_1764335302_i_where = 'id_inspeccion=\'' + seltarea.id_server + '\'';
        ID_1764335302_i.fetch({
            query: 'SELECT * FROM insp_contenido WHERE id_inspeccion=\'' + seltarea.id_server + '\''
        });
        var contenidos = require('helper').query2array(ID_1764335302_i);
        var ID_444387381_i = Alloy.createCollection('insp_documentos');
        var ID_444387381_i_where = 'id_inspeccion=\'' + seltarea.id_server + '\'';
        ID_444387381_i.fetch({
            query: 'SELECT * FROM insp_documentos WHERE id_inspeccion=\'' + seltarea.id_server + '\''
        });
        var documentos = require('helper').query2array(ID_444387381_i);
        var ID_1498779234_i = Alloy.createCollection('insp_firma');
        var ID_1498779234_i_where = 'id_inspeccion=\'' + seltarea.id_server + '\'';
        ID_1498779234_i.fetch({
            query: 'SELECT * FROM insp_firma WHERE id_inspeccion=\'' + seltarea.id_server + '\''
        });
        var firma = require('helper').query2array(ID_1498779234_i);
        /** 
         * Actualizamos fecha fin de inspeccion actual 
         */
        var ID_1320362209_i = Alloy.createCollection('inspecciones');
        var ID_1320362209_i_where = 'id_server=\'' + seltarea.id_server + '\'';
        ID_1320362209_i.fetch({
            query: 'SELECT * FROM inspecciones WHERE id_server=\'' + seltarea.id_server + '\''
        });
        var nula = require('helper').query2array(ID_1320362209_i);
        var db = Ti.Database.open(ID_1320362209_i.config.adapter.db_name);
        if (ID_1320362209_i_where == '') {
            var sql = 'UPDATE ' + ID_1320362209_i.config.adapter.collection_name + ' SET fecha_inspeccion_fin=\'' + new Date() + '\'';
        } else {
            var sql = 'UPDATE ' + ID_1320362209_i.config.adapter.collection_name + ' SET fecha_inspeccion_fin=\'' + new Date() + '\' WHERE ' + ID_1320362209_i_where;
        }
        db.execute(sql);
        db.close();
        var ID_912472445_i = Alloy.createCollection('inspecciones');
        var ID_912472445_i_where = 'id_server=\'' + seltarea.id_server + '\'';
        ID_912472445_i.fetch({
            query: 'SELECT * FROM inspecciones WHERE id_server=\'' + seltarea.id_server + '\''
        });
        var inspecciones = require('helper').query2array(ID_912472445_i);
        var sta_enviando = L('x3992720377_traducir', 'status: enviando...');
        $.ID_1011874534.setText(sta_enviando);

        var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
        if ((_.isObject(url_server) || (_.isString(url_server)) && !_.isEmpty(url_server)) || _.isNumber(url_server) || _.isBoolean(url_server)) {
            /** 
             * Se crea una estructura con los datos que iban a ser enviados al servidor, notificando que la inspeccion fue cancelada por algun motivo 
             */
            var datos = {
                fotosrequeridas: fotos,
                datosbasicos: basicos,
                caracteristicas: cara,
                niveles: niveles,
                siniestro: siniestro,
                recintos: recintos,
                itemdanos: danos,
                contenido: contenidos,
                documentos: documentos,
                inspecciones: inspecciones,
                firma: firma
            };
            require('vars')[_var_scopekey]['datos'] = datos;
            if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
                var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
                if (false) console.log('tenemos internet para enviar la tarea', {});
                var ID_1455003612 = {};
                if (false) console.log('DEBUG WEB: requesting url:' + String.format(L('x3471857721', '%1$sfinalizarTarea'), url_server.toString()) + ' with JSON data:', {
                    _method: 'POSTJSON',
                    _params: {
                        fotosrequeridas: fotos,
                        datosbasicos: basicos,
                        caracteristicas: cara,
                        niveles: niveles,
                        siniestro: siniestro,
                        recintos: recintos,
                        itemdanos: danos,
                        contenido: contenidos,
                        documentos: documentos,
                        inspecciones: inspecciones,
                        firma: firma,
                        id_app: seltarea.id_server
                    },
                    _timeout: '15000'
                });

                ID_1455003612.success = function(e) {
                    var elemento = e,
                        valor = e;
                    if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
                        /** 
                         * Llamamos funcion enviarFirmas 
                         */
                        var ID_234143345 = null;
                        if ('enviarfirmas' in require('funciones')) {
                            ID_234143345 = require('funciones').enviarfirmas({});
                        } else {
                            try {
                                ID_234143345 = f_enviarfirmas({});
                            } catch (ee) {}
                        }
                    } else {}
                    if (false) console.log('success de finalizar tarea EBH', {});
                    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
                    var sta_limpiando = L('x2911704577_traducir', 'status: limpiando temporales');
                    $.ID_1011874534.setText(sta_limpiando);

                    var ID_1876659041_i = Alloy.Collections.insp_datosbasicos;
                    var sql = 'DELETE FROM ' + ID_1876659041_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
                    var db = Ti.Database.open(ID_1876659041_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_1876659041_i.trigger('remove');
                    var ID_857284691_i = Alloy.Collections.insp_caracteristicas;
                    var sql = 'DELETE FROM ' + ID_857284691_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
                    var db = Ti.Database.open(ID_857284691_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_857284691_i.trigger('remove');
                    var ID_847124238_i = Alloy.Collections.insp_niveles;
                    var sql = 'DELETE FROM ' + ID_847124238_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
                    var db = Ti.Database.open(ID_847124238_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_847124238_i.trigger('remove');
                    var ID_1576424448_i = Alloy.Collections.insp_siniestro;
                    var sql = 'DELETE FROM ' + ID_1576424448_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
                    var db = Ti.Database.open(ID_1576424448_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_1576424448_i.trigger('remove');
                    var ID_1173498101_i = Alloy.Collections.insp_recintos;
                    var sql = 'DELETE FROM ' + ID_1173498101_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
                    var db = Ti.Database.open(ID_1173498101_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_1173498101_i.trigger('remove');
                    var ID_1380739516_i = Alloy.Collections.insp_itemdanos;
                    var sql = 'DELETE FROM ' + ID_1380739516_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
                    var db = Ti.Database.open(ID_1380739516_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_1380739516_i.trigger('remove');
                    var ID_1602887330_i = Alloy.Collections.insp_contenido;
                    var sql = 'DELETE FROM ' + ID_1602887330_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
                    var db = Ti.Database.open(ID_1602887330_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_1602887330_i.trigger('remove');
                    var ID_1008090978_i = Alloy.Collections.insp_documentos;
                    var sql = 'DELETE FROM ' + ID_1008090978_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
                    var db = Ti.Database.open(ID_1008090978_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_1008090978_i.trigger('remove');
                    var ID_602296366_i = Alloy.Collections.inspecciones;
                    var sql = 'DELETE FROM ' + ID_602296366_i.config.adapter.collection_name + ' WHERE id_server=\'' + seltarea.id_server + '\'';
                    var db = Ti.Database.open(ID_602296366_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_602296366_i.trigger('remove');
                    /** 
                     * Eliminando datos enviados 
                     */
                    /** 
                     * Eliminando datos enviados 
                     */

                    var ID_1376668677_i = Alloy.Collections.insp_fotosrequeridas;
                    var sql = "DELETE FROM " + ID_1376668677_i.config.adapter.collection_name;
                    var db = Ti.Database.open(ID_1376668677_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_1376668677_i.trigger('remove');
                    /** 
                     * Limpieza de memoria 
                     */
                    insp_fotosrequeridas = null, insp_datosbasicos = null, insp_caracteristicas = null, insp_niveles = null, insp_siniestro = null, insp_recintos = null, insp_itemdanos = null, insp_contenido = null, insp_documentos = null, insp_firma = null, inspecciones = null;
                    var ID_1546591205_func = function() {
                        var sta_listo = L('x671448501_traducir', 'status: listos');
                        $.ID_1011874534.setText(sta_listo);


                        $.ID_1012442242.detener_progreso({});
                    };
                    var ID_1546591205 = setTimeout(ID_1546591205_func, 1000 * 0.5);
                    elemento = null, valor = null;
                };

                ID_1455003612.error = function(e) {
                    var elemento = e,
                        valor = e;
                    if (false) console.log('hubo error enviando inspeccion', {
                        "elemento": elemento
                    });
                    if (false) console.log('agregando servicio finalizarTarea a cola', {});
                    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
                    var datos = ('datos' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['datos'] : '';
                    var ID_2024691365_m = Alloy.Collections.cola;
                    var ID_2024691365_fila = Alloy.createModel('cola', {
                        data: JSON.stringify(datos),
                        id_tarea: seltarea.id_server,
                        tipo: 'enviar'
                    });
                    ID_2024691365_m.add(ID_2024691365_fila);
                    ID_2024691365_fila.save();
                    var ID_701254128_i = Alloy.Collections.insp_datosbasicos;
                    var sql = 'DELETE FROM ' + ID_701254128_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
                    var db = Ti.Database.open(ID_701254128_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_701254128_i.trigger('remove');
                    var ID_375317958_i = Alloy.Collections.insp_caracteristicas;
                    var sql = 'DELETE FROM ' + ID_375317958_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
                    var db = Ti.Database.open(ID_375317958_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_375317958_i.trigger('remove');
                    var ID_686059277_i = Alloy.Collections.insp_niveles;
                    var sql = 'DELETE FROM ' + ID_686059277_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
                    var db = Ti.Database.open(ID_686059277_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_686059277_i.trigger('remove');
                    var ID_1509613562_i = Alloy.Collections.insp_siniestro;
                    var sql = 'DELETE FROM ' + ID_1509613562_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
                    var db = Ti.Database.open(ID_1509613562_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_1509613562_i.trigger('remove');
                    var ID_875757008_i = Alloy.Collections.insp_recintos;
                    var sql = 'DELETE FROM ' + ID_875757008_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
                    var db = Ti.Database.open(ID_875757008_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_875757008_i.trigger('remove');
                    var ID_933984122_i = Alloy.Collections.insp_itemdanos;
                    var sql = 'DELETE FROM ' + ID_933984122_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
                    var db = Ti.Database.open(ID_933984122_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_933984122_i.trigger('remove');
                    var ID_890921391_i = Alloy.Collections.insp_contenido;
                    var sql = 'DELETE FROM ' + ID_890921391_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
                    var db = Ti.Database.open(ID_890921391_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_890921391_i.trigger('remove');
                    var ID_580716423_i = Alloy.Collections.insp_documentos;
                    var sql = 'DELETE FROM ' + ID_580716423_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
                    var db = Ti.Database.open(ID_580716423_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_580716423_i.trigger('remove');
                    var ID_269948483_i = Alloy.Collections.inspecciones;
                    var sql = 'DELETE FROM ' + ID_269948483_i.config.adapter.collection_name + ' WHERE id_server=\'' + seltarea.id_server + '\'';
                    var db = Ti.Database.open(ID_269948483_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_269948483_i.trigger('remove');
                    /** 
                     * Limpieza de memoria 
                     */
                    insp_fotosrequeridas = null, insp_datosbasicos = null, insp_caracteristicas = null, insp_niveles = null, insp_siniestro = null, insp_recintos = null, insp_itemdanos = null, insp_contenido = null, insp_documentos = null, insp_firma = null, inspecciones = null;
                    var ID_1164055122_func = function() {
                        var sta_sininternet = L('x749992161_traducir', 'status: sin internet... agregado a cola de salida');
                        $.ID_1011874534.setText(sta_sininternet);


                        $.ID_1012442242.detener_progreso({});
                    };
                    var ID_1164055122 = setTimeout(ID_1164055122_func, 1000 * 0.5);
                    elemento = null, valor = null;
                };
                require('helper').ajaxUnico('ID_1455003612', '' + String.format(L('x3471857721', '%1$sfinalizarTarea'), url_server.toString()) + '', 'POSTJSON', {
                    fotosrequeridas: fotos,
                    datosbasicos: basicos,
                    caracteristicas: cara,
                    niveles: niveles,
                    siniestro: siniestro,
                    recintos: recintos,
                    itemdanos: danos,
                    contenido: contenidos,
                    documentos: documentos,
                    inspecciones: inspecciones,
                    firma: firma,
                    id_app: seltarea.id_server
                }, 15000, ID_1455003612);
            } else {
                if (false) console.log('El celular no detecto internet... agregando a la cola de envio', {});
                var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
                var datos = ('datos' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['datos'] : '';
                var ID_459750892_m = Alloy.Collections.cola;
                var ID_459750892_fila = Alloy.createModel('cola', {
                    data: JSON.stringify(datos),
                    id_tarea: seltarea.id_server,
                    tipo: 'enviar'
                });
                ID_459750892_m.add(ID_459750892_fila);
                ID_459750892_fila.save();
                var ID_816473526_i = Alloy.Collections.insp_datosbasicos;
                var sql = 'DELETE FROM ' + ID_816473526_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
                var db = Ti.Database.open(ID_816473526_i.config.adapter.db_name);
                db.execute(sql);
                db.close();
                ID_816473526_i.trigger('remove');
                var ID_1666320191_i = Alloy.Collections.insp_caracteristicas;
                var sql = 'DELETE FROM ' + ID_1666320191_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
                var db = Ti.Database.open(ID_1666320191_i.config.adapter.db_name);
                db.execute(sql);
                db.close();
                ID_1666320191_i.trigger('remove');
                var ID_1306810181_i = Alloy.Collections.insp_niveles;
                var sql = 'DELETE FROM ' + ID_1306810181_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
                var db = Ti.Database.open(ID_1306810181_i.config.adapter.db_name);
                db.execute(sql);
                db.close();
                ID_1306810181_i.trigger('remove');
                var ID_1775946550_i = Alloy.Collections.insp_siniestro;
                var sql = 'DELETE FROM ' + ID_1775946550_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
                var db = Ti.Database.open(ID_1775946550_i.config.adapter.db_name);
                db.execute(sql);
                db.close();
                ID_1775946550_i.trigger('remove');
                var ID_1498318955_i = Alloy.Collections.insp_recintos;
                var sql = 'DELETE FROM ' + ID_1498318955_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
                var db = Ti.Database.open(ID_1498318955_i.config.adapter.db_name);
                db.execute(sql);
                db.close();
                ID_1498318955_i.trigger('remove');
                var ID_1986858185_i = Alloy.Collections.insp_itemdanos;
                var sql = 'DELETE FROM ' + ID_1986858185_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
                var db = Ti.Database.open(ID_1986858185_i.config.adapter.db_name);
                db.execute(sql);
                db.close();
                ID_1986858185_i.trigger('remove');
                var ID_1644471654_i = Alloy.Collections.insp_contenido;
                var sql = 'DELETE FROM ' + ID_1644471654_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
                var db = Ti.Database.open(ID_1644471654_i.config.adapter.db_name);
                db.execute(sql);
                db.close();
                ID_1644471654_i.trigger('remove');
                var ID_1633227210_i = Alloy.Collections.insp_documentos;
                var sql = 'DELETE FROM ' + ID_1633227210_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
                var db = Ti.Database.open(ID_1633227210_i.config.adapter.db_name);
                db.execute(sql);
                db.close();
                ID_1633227210_i.trigger('remove');
                var ID_1743814137_i = Alloy.Collections.inspecciones;
                var sql = 'DELETE FROM ' + ID_1743814137_i.config.adapter.collection_name + ' WHERE id_server=\'' + seltarea.id_server + '\'';
                var db = Ti.Database.open(ID_1743814137_i.config.adapter.db_name);
                db.execute(sql);
                db.close();
                ID_1743814137_i.trigger('remove');
                /** 
                 * Limpieza de memoria 
                 */
                insp_fotosrequeridas = null, insp_datosbasicos = null, insp_caracteristicas = null, insp_niveles = null, insp_siniestro = null, insp_recintos = null, insp_itemdanos = null, insp_contenido = null, insp_documentos = null, insp_firma = null, inspecciones = null;
                var ID_93492704_func = function() {
                    var sta_sininternet = L('x749992161_traducir', 'status: sin internet... agregado a cola de salida');
                    $.ID_1011874534.setText(sta_sininternet);


                    $.ID_1012442242.detener_progreso({});
                };
                var ID_93492704 = setTimeout(ID_93492704_func, 1000 * 0.5);
            }
        } else {
            var sta_simulador = L('x4159393315_traducir', 'status: simulador');
            $.ID_1011874534.setText(sta_simulador);


            $.ID_1012442242.detener_progreso({});
        }
    } else {
        var sta_simulador = L('x4159393315_traducir', 'status: simulador');
        $.ID_1011874534.setText(sta_simulador);

    }
})();

if (OS_IOS || OS_ANDROID) {
    $.ID_1334182915.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}
//$.ID_1334182915.open();