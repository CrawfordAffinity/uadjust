var _bind4section = {};
var _list_templates = {
    "criticas": {
        "ID_202820149": {},
        "ID_1737707475": {
            "text": "{ciudadcritica}, {paiscritica}"
        },
        "ID_422128048": {},
        "ID_403273726": {},
        "ID_620140124": {
            "text": "{direccioncritica}"
        },
        "ID_1438432281": {},
        "ID_1119172715": {},
        "ID_1977608142": {
            "text": "{comunacritica}"
        },
        "ID_1622346619": {},
        "ID_1445023664": {},
        "ID_521530515": {
            "text": "a {ubicacioncritica} km"
        },
        "ID_704055711": {
            "myid": "{myid}"
        },
        "ID_715095938": {}
    },
    "tareas": {
        "ID_1271969677": {},
        "ID_628062793": {
            "text": "{direcciontarea}"
        },
        "ID_568454310": {},
        "ID_926529622": {},
        "ID_1388054861": {
            "text": "{ciudadtarea}, {paistarea} "
        },
        "ID_369983217": {},
        "ID_75895559": {},
        "ID_360694343": {
            "text": "{comunatarea}"
        },
        "ID_1217465633": {
            "text": "a {ubicaciontarea} km"
        },
        "ID_477443044": {},
        "ID_1228388310": {},
        "ID_871033850": {},
        "ID_422941248": {
            "myid": "{myid}"
        }
    }
};
Alloy.Globals["ID_216174848"] = $.ID_216174848;
var _activity;
if (OS_ANDROID) {
    _activity = $.ID_216174848.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    args = arguments[0] || {};


var _activity;
if (OS_ANDROID) {
    _activity = $.ID_786813367.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
if (OS_ANDROID) {
    $.ID_786813367.addEventListener('open', function(e) {});
}

function Click_ID_1747085017(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    if (false) console.log('clickeo el refresh', {});
    var gps_error = ('gps_error' in require('vars')) ? require('vars')['gps_error'] : '';
    if (gps_error == true || gps_error == 'true') {
        var ID_325203441_opts = [L('x1518866076_traducir', 'Aceptar')];
        var ID_325203441 = Ti.UI.createAlertDialog({
            title: L('x3661800039_traducir', 'Error geolocalizando'),
            message: L('x942436043_traducir', 'Ha ocurrido un error al geolocalizarlo'),
            buttonNames: ID_325203441_opts
        });
        ID_325203441.addEventListener('click', function(e) {
            var errori = ID_325203441_opts[e.index];
            errori = null;

            e.source.removeEventListener("click", arguments.callee);
        });
        ID_325203441.show();
    }
    var obtenerentrantes = ('obtenerentrantes' in require('vars')) ? require('vars')['obtenerentrantes'] : '';
    var obtenermistareas = ('obtenermistareas' in require('vars')) ? require('vars')['obtenermistareas'] : '';
    var obteneremergencias = ('obteneremergencias' in require('vars')) ? require('vars')['obteneremergencias'] : '';
    if (obteneremergencias == true || obteneremergencias == 'true') {
        if (false) console.log('esta actualizando', {});
    } else if (obtenerentrantes == true || obtenerentrantes == 'true') {
        if (false) console.log('actualizando tareas entrantes', {});
    } else if (obtenermistareas == true || obtenermistareas == 'true') {
        if (false) console.log('esta actualizando mistareas', {});
    } if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
            require('vars')['obtenermistareas'] = L('x4261170317', 'true');
            require('vars')['obtenerentrantes'] = L('x4261170317', 'true');
            var gps_latitud = ('gps_latitud' in require('vars')) ? require('vars')['gps_latitud'] : '';
            var gps_longitud = ('gps_longitud' in require('vars')) ? require('vars')['gps_longitud'] : '';
            var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
            var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
            if (false) console.log('Actualizando', {});
            var ID_1348100052 = {};

            ID_1348100052.success = function(e) {
                var elemento = e,
                    valor = e;
                if (elemento.error == 0 || elemento.error == '0') {
                    var ID_1257896348_i = Alloy.Collections.tareas;
                    var sql = "DELETE FROM " + ID_1257896348_i.config.adapter.collection_name;
                    var db = Ti.Database.open(ID_1257896348_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_1257896348_i.trigger('remove');
                    /** 
                     * guardamos elemento.mistareas en var local, para ejecutar desfasada la insercion a la base de datos. 
                     */
                    require('vars')['mistareas'] = elemento.mistareas;
                    var ID_1591812203_func = function() {
                        var mistareas = ('mistareas' in require('vars')) ? require('vars')['mistareas'] : '';
                        var ID_672779146_m = Alloy.Collections.tareas;
                        var db_ID_672779146 = Ti.Database.open(ID_672779146_m.config.adapter.db_name);
                        db_ID_672779146.execute('BEGIN');
                        _.each(mistareas, function(ID_672779146_fila, pos) {
                            db_ID_672779146.execute('INSERT INTO tareas (fecha_tarea, id_inspeccion, id_asegurado, nivel_2, comentario_can_o_rech, asegurado_tel_fijo, estado_tarea, bono, evento, id_inspector, asegurado_codigo_identificador, lat, nivel_1, asegurado_nombre, pais, direccion, asegurador, fecha_ingreso, fecha_siniestro, nivel_1_texto, distance, nivel_4, perfil, asegurado_id, pais_texto, id_server, categoria, nivel_3, asegurado_correo, num_caso, lon, asegurado_tel_movil, nivel_5, tipo_tarea) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', ID_672779146_fila.fecha_tarea, ID_672779146_fila.id_inspeccion, ID_672779146_fila.id_asegurado, ID_672779146_fila.nivel_2, ID_672779146_fila.comentario_can_o_rech, ID_672779146_fila.asegurado_tel_fijo, ID_672779146_fila.estado_tarea, ID_672779146_fila.bono, ID_672779146_fila.evento, ID_672779146_fila.id_inspector, ID_672779146_fila.asegurado_codigo_identificador, ID_672779146_fila.lat, ID_672779146_fila.nivel_1, ID_672779146_fila.asegurado_nombre, ID_672779146_fila.pais, ID_672779146_fila.direccion, ID_672779146_fila.asegurador, ID_672779146_fila.fecha_ingreso, ID_672779146_fila.fecha_siniestro, ID_672779146_fila.nivel_1_, ID_672779146_fila.distancia, ID_672779146_fila.nivel_4, 'ubicacion', ID_672779146_fila.asegurado_id, ID_672779146_fila.pais, ID_672779146_fila.id, ID_672779146_fila.categoria, ID_672779146_fila.nivel_3, ID_672779146_fila.asegurado_correo, ID_672779146_fila.num_caso, ID_672779146_fila.lon, ID_672779146_fila.asegurado_tel_movil, ID_672779146_fila.nivel_5, ID_672779146_fila.tipo_tarea);
                        });
                        db_ID_672779146.execute('COMMIT');
                        db_ID_672779146.close();
                        db_ID_672779146 = null;
                        ID_672779146_m.trigger('change');
                        require('vars')['obtenermistareas'] = L('x734881840_traducir', 'false');
                        var ID_1919001476_func = function() {
                            if (false) console.log('psb: refrescandpsb: refrescando pantallas mistareas o mistareas', {});

                            var ID_1241004686_trycatch = {
                                error: function(e) {
                                    if (false) console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
                                }
                            };
                            try {
                                ID_1241004686_trycatch.error = function(evento) {};

                                Alloy.Events.trigger('_refrescar_tareas_mistareas');

                                Alloy.Events.trigger('_calcular_ruta');
                            } catch (e) {
                                ID_1241004686_trycatch.error(e);
                            }
                        };
                        var ID_1919001476 = setTimeout(ID_1919001476_func, 1000 * 0.1);
                    };
                    var ID_1591812203 = setTimeout(ID_1591812203_func, 1000 * 0.1);
                } else {
                    if (false) console.log('error al recibir mistareas', {
                        "elemento": elemento
                    });
                }
                elemento = null, valor = null;
            };

            ID_1348100052.error = function(e) {
                var elemento = e,
                    valor = e;
                if (false) console.log('respuesta fallida de obtenerMisTareas', {
                    "datos": elemento
                });
                elemento = null, valor = null;
            };
            require('helper').ajaxUnico('ID_1348100052', '' + String.format(L('x2334609226', '%1$sobtenerMisTareas'), url_server.toString()) + '', 'POST', {
                id_inspector: inspector.id_server,
                lat: gps_latitud,
                lon: gps_longitud
            }, 15000, ID_1348100052);
            var ID_1477541591 = {};
            console.log('DEBUG WEB: requesting url:' + String.format(L('x1654875529', '%1$sobtenerTareasEntrantes'), url_server.toString()) + ' with data:', {
                _method: 'POST',
                _params: {
                    id_inspector: inspector.id_server,
                    lat: gps_latitud,
                    lon: gps_longitud
                },
                _timeout: '15000'
            });

            ID_1477541591.success = function(e) {
                var elemento = e,
                    valor = e;
                if (elemento.error != 0 && elemento.error != '0') {
                    if (false) console.log('error de servidor en obtenerTareasEntrntes', {
                        "elemento": elemento
                    });
                    var ID_984540120_opts = [L('x1518866076_traducir', 'Aceptar')];
                    var ID_984540120 = Ti.UI.createAlertDialog({
                        title: L('x57652245_traducir', 'Alerta'),
                        message: L('x344160560_traducir', 'Error con el servidor'),
                        buttonNames: ID_984540120_opts
                    });
                    ID_984540120.addEventListener('click', function(e) {
                        var errori = ID_984540120_opts[e.index];
                        errori = null;

                        e.source.removeEventListener("click", arguments.callee);
                    });
                    ID_984540120.show();
                } else {
                    var ID_1355973319_i = Alloy.Collections.tareas_entrantes;
                    var sql = "DELETE FROM " + ID_1355973319_i.config.adapter.collection_name;
                    var db = Ti.Database.open(ID_1355973319_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_1355973319_i.trigger('remove');
                    if (false) console.log('Respuesta de servidor TareasEntrantes (sin error)', {
                        "elemento": elemento
                    });
                    var elemento_critica = elemento.critica;
                    var ID_1376170820_m = Alloy.Collections.tareas_entrantes;
                    var db_ID_1376170820 = Ti.Database.open(ID_1376170820_m.config.adapter.db_name);
                    db_ID_1376170820.execute('BEGIN');
                    _.each(elemento_critica, function(ID_1376170820_fila, pos) {
                        db_ID_1376170820.execute('INSERT INTO tareas_entrantes (fecha_tarea, id_inspeccion, id_asegurado, nivel_2, comentario_can_o_rech, asegurado_tel_fijo, estado_tarea, bono, evento, id_inspector, asegurado_codigo_identificador, lat, nivel_1, asegurado_nombre, pais, direccion, asegurador, fecha_ingreso, fecha_siniestro, nivel_1_texto, distance, nivel_4, perfil, asegurado_id, pais_texto, id_server, categoria, nivel_3, asegurado_correo, num_caso, lon, asegurado_tel_movil, tipo_tarea, nivel_5) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', ID_1376170820_fila.fecha_tarea, ID_1376170820_fila.id_inspeccion, ID_1376170820_fila.id_asegurado, ID_1376170820_fila.nivel_2, '-', ID_1376170820_fila.asegurado_tel_fijo, ID_1376170820_fila.estado_tarea, ID_1376170820_fila.bono, ID_1376170820_fila.evento, ID_1376170820_fila.id_inspector, ID_1376170820_fila.asegurado_codigo_identificador, ID_1376170820_fila.lat, ID_1376170820_fila.nivel_1, ID_1376170820_fila.asegurado_nombre, -1, ID_1376170820_fila.direccion, ID_1376170820_fila.asegurador, ID_1376170820_fila.fecha_ingreso, ID_1376170820_fila.fecha_siniestro, ID_1376170820_fila.nivel_1_, ID_1376170820_fila.distancia, ID_1376170820_fila.nivel_4, 'casa', ID_1376170820_fila.asegurado_id, ID_1376170820_fila.pais_, ID_1376170820_fila.id, ID_1376170820_fila.categoria, ID_1376170820_fila.nivel_3, ID_1376170820_fila.asegurado_correo, ID_1376170820_fila.num_caso, ID_1376170820_fila.lon, ID_1376170820_fila.asegurado_tel_movil, ID_1376170820_fila.tipo_tarea, ID_1376170820_fila.nivel_5);
                    });
                    db_ID_1376170820.execute('COMMIT');
                    db_ID_1376170820.close();
                    db_ID_1376170820 = null;
                    ID_1376170820_m.trigger('change');
                    var elemento_normal = elemento.normal;
                    var ID_164163396_m = Alloy.Collections.tareas_entrantes;
                    var db_ID_164163396 = Ti.Database.open(ID_164163396_m.config.adapter.db_name);
                    db_ID_164163396.execute('BEGIN');
                    _.each(elemento_normal, function(ID_164163396_fila, pos) {
                        db_ID_164163396.execute('INSERT INTO tareas_entrantes (fecha_tarea, id_inspeccion, id_asegurado, nivel_2, comentario_can_o_rech, asegurado_tel_fijo, estado_tarea, bono, evento, id_inspector, asegurado_codigo_identificador, lat, nivel_1, asegurado_nombre, pais, direccion, asegurador, fecha_ingreso, fecha_siniestro, nivel_1_texto, distance, nivel_4, perfil, asegurado_id, pais_texto, id_server, categoria, nivel_3, asegurado_correo, num_caso, lon, asegurado_tel_movil, tipo_tarea, nivel_5) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', ID_164163396_fila.fecha_tarea, ID_164163396_fila.id_inspeccion, ID_164163396_fila.id_asegurado, ID_164163396_fila.nivel_2, '-', ID_164163396_fila.asegurado_tel_fijo, ID_164163396_fila.estado_tarea, ID_164163396_fila.bono, ID_164163396_fila.evento, ID_164163396_fila.id_inspector, ID_164163396_fila.asegurado_codigo_identificador, ID_164163396_fila.lat, ID_164163396_fila.nivel_1, ID_164163396_fila.asegurado_nombre, -1, ID_164163396_fila.direccion, ID_164163396_fila.asegurador, ID_164163396_fila.fecha_ingreso, ID_164163396_fila.fecha_siniestro, ID_164163396_fila.nivel_1_, ID_164163396_fila.distancia, ID_164163396_fila.nivel_4, 'casa', ID_164163396_fila.asegurado_id, ID_164163396_fila.pais_, ID_164163396_fila.id, ID_164163396_fila.categoria, ID_164163396_fila.nivel_3, ID_164163396_fila.asegurado_correo, ID_164163396_fila.num_caso, ID_164163396_fila.lon, ID_164163396_fila.asegurado_tel_movil, ID_164163396_fila.tipo_tarea, ID_164163396_fila.nivel_5);
                    });
                    db_ID_164163396.execute('COMMIT');
                    db_ID_164163396.close();
                    db_ID_164163396 = null;
                    ID_164163396_m.trigger('change');

                    Alloy.Events.trigger('_refrescar_tareas_entrantes');
                    require('vars')['obtenerentrantes'] = L('x734881840_traducir', 'false');
                }
                elemento = null, valor = null;
            };

            ID_1477541591.error = function(e) {
                var elemento = e,
                    valor = e;
                if (false) console.log('Hubo una falla al llamar servicio obtenerTareasEntrantes', {});
                var ID_1412699298_opts = [L('x1518866076_traducir', 'Aceptar')];
                var ID_1412699298 = Ti.UI.createAlertDialog({
                    title: L('x57652245_traducir', 'Alerta'),
                    message: L('x3389756565_traducir', 'No se pudo conectar con el servidor'),
                    buttonNames: ID_1412699298_opts
                });
                ID_1412699298.addEventListener('click', function(e) {
                    var errori = ID_1412699298_opts[e.index];
                    errori = null;

                    e.source.removeEventListener("click", arguments.callee);
                });
                ID_1412699298.show();
                elemento = null, valor = null;
            };
            require('helper').ajaxUnico('ID_1477541591', '' + String.format(L('x1654875529', '%1$sobtenerTareasEntrantes'), url_server.toString()) + '', 'POST', {
                id_inspector: inspector.id_server,
                lat: gps_latitud,
                lon: gps_longitud
            }, 15000, ID_1477541591);
            var ID_1772162362 = {};

            ID_1772162362.success = function(e) {
                var elemento = e,
                    valor = e;
                var ID_1827806984_i = Alloy.Collections.emergencia;
                var sql = "DELETE FROM " + ID_1827806984_i.config.adapter.collection_name;
                var db = Ti.Database.open(ID_1827806984_i.config.adapter.db_name);
                db.execute(sql);
                db.close();
                ID_1827806984_i.trigger('remove');
                if (elemento.error != 0 && elemento.error != '0') {
                    var ID_1578589616_opts = [L('x1518866076_traducir', 'Aceptar')];
                    var ID_1578589616 = Ti.UI.createAlertDialog({
                        title: L('x57652245_traducir', 'Alerta'),
                        message: L('x344160560_traducir', 'Error con el servidor'),
                        buttonNames: ID_1578589616_opts
                    });
                    ID_1578589616.addEventListener('click', function(e) {
                        var errori = ID_1578589616_opts[e.index];
                        errori = null;

                        e.source.removeEventListener("click", arguments.callee);
                    });
                    ID_1578589616.show();
                } else {
                    if (false) console.log('Insertando emergencias', {
                        "elemento": elemento
                    });
                    var elemento_perfil = elemento.perfil;
                    var ID_1937077707_m = Alloy.Collections.emergencia;
                    var db_ID_1937077707 = Ti.Database.open(ID_1937077707_m.config.adapter.db_name);
                    db_ID_1937077707.execute('BEGIN');
                    _.each(elemento_perfil, function(ID_1937077707_fila, pos) {
                        db_ID_1937077707.execute('INSERT INTO emergencia (fecha_tarea, id_inspeccion, nivel_2, id_asegurado, comentario_can_o_rech, asegurado_tel_fijo, estado_tarea, bono, id_inspector, asegurado_codigo_identificador, lat, nivel_1, asegurado_nombre, pais, direccion, asegurador, fecha_ingreso, fecha_siniestro, nivel_1_texto, distance, nivel_4, perfil, asegurado_id, pais_texto, id_server, categoria, nivel_3, asegurado_correo, num_caso, lon, asegurado_tel_movil, distancia_2, nivel_5, tipo_tarea) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', ID_1937077707_fila.fecha_tarea, ID_1937077707_fila.id_inspeccion, ID_1937077707_fila.nivel_2, ID_1937077707_fila.id_asegurado, ID_1937077707_fila.comentario_can_o_rech, ID_1937077707_fila.asegurado_tel_fijo, ID_1937077707_fila.estado_tarea, ID_1937077707_fila.bono, ID_1937077707_fila.id_inspector, ID_1937077707_fila.asegurado_codigo_identificador, ID_1937077707_fila.lat, ID_1937077707_fila.nivel_1, ID_1937077707_fila.asegurado_nombre, ID_1937077707_fila.pais, ID_1937077707_fila.direccion, ID_1937077707_fila.asegurador, ID_1937077707_fila.fecha_ingreso, ID_1937077707_fila.fecha_siniestro, ID_1937077707_fila.nivel_1_google, ID_1937077707_fila.distancia, ID_1937077707_fila.nivel_4, 'casa', ID_1937077707_fila.asegurado_id, ID_1937077707_fila.pais_, ID_1937077707_fila.id, ID_1937077707_fila.categoria, ID_1937077707_fila.nivel_3, ID_1937077707_fila.asegurado_correo, ID_1937077707_fila.num_caso, ID_1937077707_fila.lon, ID_1937077707_fila.asegurado_tel_movil, ID_1937077707_fila.distancia_2, ID_1937077707_fila.nivel_5, ID_1937077707_fila.tipo_tarea);
                    });
                    db_ID_1937077707.execute('COMMIT');
                    db_ID_1937077707.close();
                    db_ID_1937077707 = null;
                    ID_1937077707_m.trigger('change');
                    var elemento_ubicacion = elemento.ubicacion;
                    var ID_507245659_m = Alloy.Collections.emergencia;
                    var db_ID_507245659 = Ti.Database.open(ID_507245659_m.config.adapter.db_name);
                    db_ID_507245659.execute('BEGIN');
                    _.each(elemento_ubicacion, function(ID_507245659_fila, pos) {
                        db_ID_507245659.execute('INSERT INTO emergencia (fecha_tarea, id_inspeccion, id_asegurado, nivel_2, comentario_can_o_rech, asegurado_tel_fijo, estado_tarea, bono, id_inspector, asegurado_codigo_identificador, lat, nivel_1, asegurado_nombre, pais, direccion, asegurador, fecha_ingreso, fecha_siniestro, nivel_1_texto, distance, nivel_4, perfil, asegurado_id, pais_texto, id_server, categoria, nivel_3, asegurado_correo, num_caso, lon, asegurado_tel_movil, distancia_2, tipo_tarea, nivel_5) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', ID_507245659_fila.fecha_tarea, ID_507245659_fila.id_inspeccion, ID_507245659_fila.id_asegurado, ID_507245659_fila.nivel_2, ID_507245659_fila.comentario_can_o_rech, ID_507245659_fila.asegurado_tel_fijo, ID_507245659_fila.estado_tarea, ID_507245659_fila.bono, ID_507245659_fila.id_inspector, ID_507245659_fila.asegurado_codigo_identificador, ID_507245659_fila.lat, ID_507245659_fila.nivel_1, ID_507245659_fila.asegurado_nombre, ID_507245659_fila.pais, ID_507245659_fila.direccion, ID_507245659_fila.asegurador, ID_507245659_fila.fecha_ingreso, ID_507245659_fila.fecha_siniestro, ID_507245659_fila.nivel_1_google, ID_507245659_fila.distancia, ID_507245659_fila.nivel_4, 'ubicacion', ID_507245659_fila.asegurado_id, ID_507245659_fila.pais_, ID_507245659_fila.id, ID_507245659_fila.categoria, ID_507245659_fila.nivel_3, ID_507245659_fila.asegurado_correo, ID_507245659_fila.num_caso, ID_507245659_fila.lon, ID_507245659_fila.asegurado_tel_movil, ID_507245659_fila.distancia_2, ID_507245659_fila.tipo_tarea, ID_507245659_fila.nivel_5);
                    });
                    db_ID_507245659.execute('COMMIT');
                    db_ID_507245659.close();
                    db_ID_507245659 = null;
                    ID_507245659_m.trigger('change');
                    require('vars')['obteneremergencias'] = L('x734881840_traducir', 'false');

                    var ID_1412464248 = function(evento) {};
                    Alloy.Events.trigger('_refrescar_tareas');
                }
                elemento = null, valor = null;
            };

            ID_1772162362.error = function(e) {
                var elemento = e,
                    valor = e;
                if (false) console.log('Hubo un error', {});
                var ID_801575173_opts = [L('x1518866076_traducir', 'Aceptar')];
                var ID_801575173 = Ti.UI.createAlertDialog({
                    title: L('x57652245_traducir', 'Alerta'),
                    message: L('x3389756565_traducir', 'No se pudo conectar con el servidor'),
                    buttonNames: ID_801575173_opts
                });
                ID_801575173.addEventListener('click', function(e) {
                    var errori = ID_801575173_opts[e.index];
                    errori = null;

                    e.source.removeEventListener("click", arguments.callee);
                });
                ID_801575173.show();
                elemento = null, valor = null;
            };
            require('helper').ajaxUnico('ID_1772162362', '' + String.format(L('x1578679327', '%1$sobtenerEmergencias'), url_server.toString()) + '', 'POST', {
                id_inspector: inspector.id_server,
                lat: gps_latitud,
                lon: gps_longitud
            }, 15000, ID_1772162362);
        } else {
            var ID_1496773501_opts = [L('x3610695981_traducir', 'OK')];
            var ID_1496773501 = Ti.UI.createAlertDialog({
                title: L('x57652245_traducir', 'Alerta'),
                message: L('x3389756565_traducir', 'No se pudo conectar con el servidor'),
                buttonNames: ID_1496773501_opts
            });
            ID_1496773501.addEventListener('click', function(e) {
                var errori = ID_1496773501_opts[e.index];
                errori = null;

                e.source.removeEventListener("click", arguments.callee);
            });
            ID_1496773501.show();
        }
}

$.ID_374340032.init({
    titulo: L('x2828751865_traducir', '¡ESTAS SIN CONEXION!'),
    __id: 'ALL374340032',
    mensaje: L('x3528440528_traducir', 'No puedes ver las tareas entrantes'),
    onon: on_ID_1500851550,
    onoff: Off_ID_1153942941
});

function on_ID_1500851550(e) {

    var evento = e;
    var ID_1619877339_visible = true;

    if (ID_1619877339_visible == 'si') {
        ID_1619877339_visible = true;
    } else if (ID_1619877339_visible == 'no') {
        ID_1619877339_visible = false;
    }
    $.ID_1619877339.setVisible(ID_1619877339_visible);


    Alloy.Events.trigger('_refrescar_tareas_entrantes');

}

function Off_ID_1153942941(e) {

    var evento = e;
    var ID_1619877339_visible = false;

    if (ID_1619877339_visible == 'si') {
        ID_1619877339_visible = true;
    } else if (ID_1619877339_visible == 'no') {
        ID_1619877339_visible = false;
    }
    $.ID_1619877339.setVisible(ID_1619877339_visible);


}

function Itemclick_ID_1107448930(e) {

    e.cancelBubble = true;
    var objeto = e.section.getItemAt(e.itemIndex);
    var modelo = {},
        _modelo = [];
    var fila = {},
        fila_bak = {},
        info = {
            _template: objeto.template,
            _what: [],
            _seccion_ref: e.section.getHeaderTitle(),
            _model_id: -1
        },
        _tmp = {
            objmap: {}
        };
    if ('itemId' in e) {
        info._model_id = e.itemId;
        modelo._id = info._model_id;
        if (info._seccion_ref != '' && info._seccion_ref in _bind4section) {
            modelo._collection = _bind4section[info._seccion_ref];
            _tmp._coll = modelo._collection;
        }
    }
    if (info._seccion_ref != '' && info._seccion_ref in _bind4section) {
        if (Alloy.Collections[_tmp._coll].config.adapter.type == 'sql') {
            _tmp._inst = Alloy.Collections[_tmp._coll];
            _tmp._id = Alloy.Collections[_tmp._coll].config.adapter.idAttribute;
            _tmp._dbsql = 'SELECT * FROM ' + Alloy.Collections[_tmp._coll].config.adapter.collection_name + ' WHERE ' + _tmp._id + ' = ' + e.itemId;
            _tmp._db = Ti.Database.open(Alloy.Collections[_tmp._coll].config.adapter.db_name);
            _modelo = _tmp._db.execute(_tmp._dbsql);
            var values = [],
                fieldNames = [];
            var fieldCount = _.isFunction(_modelo.fieldCount) ? _modelo.fieldCount() : _modelo.fieldCount;
            var getField = _modelo.field;
            var i = 0;
            for (; fieldCount > i; i++) fieldNames.push(_modelo.fieldName(i));
            while (_modelo.isValidRow()) {
                var o = {};
                for (i = 0; fieldCount > i; i++) o[fieldNames[i]] = getField(i);
                values.push(o);
                _modelo.next();
            }
            _modelo = values;
            _tmp._db.close();
        } else {
            _tmp._search = {};
            _tmp._search[_tmp._id] = e.itemId + '';
            _modelo = Alloy.Collections[_tmp._coll].fetch(_tmp._search);
        }
    }
    var findVariables = require('fvariables');
    _.each(_list_templates[info._template], function(obj_id, id) {
        _.each(obj_id, function(valor, prop) {
            var llaves = findVariables(valor, '{', '}');
            _.each(llaves, function(llave) {
                _tmp.objmap[llave] = {
                    id: id,
                    prop: prop
                };
                fila[llave] = objeto[id][prop];
                if (id == e.bindId) info._what.push(llave);
            });
        });
    });
    info._what = info._what.join(',');
    fila_bak = JSON.parse(JSON.stringify(fila));
    if (false) console.log('Mi objecto es', {
        "info": info,
        "objeto": fila
    });
    var nulo = Alloy.createController("tomartarea_index", {
        '_objeto': fila,
        '_id': fila.myid,
        '_tipo': 'entrantes',
        '__master_model': (typeof modelo !== 'undefined') ? modelo : {},
        '__modelo': (typeof _modelo !== 'undefined' && _modelo.length > 0) ? _modelo[0] : {}
    }).getView();
    nulo.open({
        modal: true
    });

    nulo = null;
    _tmp.changed = false;
    _tmp.diff_keys = [];
    _.each(fila, function(value1, prop) {
        var had_samekey = false;
        _.each(fila_bak, function(value2, prop2) {
            if (prop == prop2 && value1 == value2) {
                had_samekey = true;
            } else if (!_.has(fila_bak, prop) || !_.has(fila, prop2)) {
                has_samekey = true;
            }
        });
        if (!had_samekey) _tmp.diff_keys.push(prop);
    });
    if (_tmp.diff_keys.length > 0) _tmp.changed = true;
    if (_tmp.changed == true) {
        _.each(_tmp.diff_keys, function(llave) {
            objeto[_tmp.objmap[llave].id][_tmp.objmap[llave].prop] = fila[llave];
        });
        e.section.updateItemAt(e.itemIndex, objeto);
    }

}

if (false) console.log('holaaaa', {});

_my_events['_refrescar_tareas_entrantes,ID_1636319035'] = function(evento) {
    /** 
     * Limpiamos el listado para ingresar las tareas refrescadas 
     */
    var ID_879692163_borrar = false;

    var limpiarListado = function(animar) {
        var a_nimar = (typeof animar == 'undefined') ? false : animar;
        if (OS_IOS && a_nimar == true) {
            var s_ecciones = $.ID_879692163.getSections();
            _.each(s_ecciones, function(obj_id, pos) {
                $.ID_879692163.deleteSectionAt(0, {
                    animated: true
                });
            });
        } else {
            $.ID_879692163.setSections([]);
        }
    };
    limpiarListado(ID_879692163_borrar);

    /** 
     * Creamos las distintas secciones y header para indicar la fecha de la tarea 
     */
    var ID_1183134912 = Titanium.UI.createListSection({
        headerTitle: L('x2819214932_traducir', 'holi1')
    });
    var ID_1086923144 = Titanium.UI.createView({
        height: Ti.UI.FILL,
        width: Ti.UI.FILL
    });
    var ID_1216108758 = Titanium.UI.createView({
        height: '25dp',
        layout: 'composite',
        width: Ti.UI.FILL,
        backgroundColor: '#FFDCDC'
    });
    var ID_1376396582 = Titanium.UI.createLabel({
        text: L('x478055026_traducir', 'CRITICAS'),
        color: '#EE7F7E',
        touchEnabled: false,
        font: {
            fontFamily: 'SFUIText-Medium',
            fontSize: '15dp'
        }

    });
    ID_1216108758.add(ID_1376396582);

    ID_1086923144.add(ID_1216108758);
    ID_1183134912.setHeaderView(ID_1086923144);
    $.ID_879692163.appendSection(ID_1183134912);
    var ID_41461518 = Titanium.UI.createListSection({
        headerTitle: L('x3166667470', 'hoy_entrantes')
    });
    var ID_639457848 = Titanium.UI.createView({
        height: Ti.UI.FILL,
        width: Ti.UI.FILL
    });
    var ID_1235937068 = Titanium.UI.createView({
        height: '25dp',
        layout: 'composite',
        width: Ti.UI.FILL,
        backgroundColor: '#F7F7F7'
    });
    var ID_1680499516 = Titanium.UI.createLabel({
        text: L('x1296057275_traducir', 'PARA HOY'),
        color: '#999999',
        touchEnabled: false,
        font: {
            fontFamily: 'SFUIText-Medium',
            fontSize: '14dp'
        }

    });
    ID_1235937068.add(ID_1680499516);

    ID_639457848.add(ID_1235937068);
    ID_41461518.setHeaderView(ID_639457848);
    $.ID_879692163.appendSection(ID_41461518);
    var ID_813613027 = Titanium.UI.createListSection({
        headerTitle: L('x3690817388', 'manana_entrantes')
    });
    var ID_1297791130 = Titanium.UI.createView({
        height: Ti.UI.FILL,
        width: Ti.UI.FILL
    });
    var ID_825962635 = Titanium.UI.createView({
        height: '25dp',
        layout: 'composite',
        width: Ti.UI.FILL,
        backgroundColor: '#F7F7F7'
    });
    var ID_1184933740 = Titanium.UI.createLabel({
        text: L('x3380933310_traducir', 'MAÑANA'),
        color: '#999999',
        touchEnabled: false,
        font: {
            fontFamily: 'SFUIText-Medium',
            fontSize: '14dp'
        }

    });
    ID_825962635.add(ID_1184933740);

    ID_1297791130.add(ID_825962635);
    ID_813613027.setHeaderView(ID_1297791130);
    $.ID_879692163.appendSection(ID_813613027);
    /** 
     * Consultamos la tabla de las tareas criticas y guardamos en variable criticas 
     */
    var ID_583256963_i = Alloy.createCollection('tareas_entrantes');
    var ID_583256963_i_where = 'tipo_tarea=1 ORDER BY TIPO_TAREA ASC';
    ID_583256963_i.fetch({
        query: 'SELECT * FROM tareas_entrantes WHERE tipo_tarea=1 ORDER BY TIPO_TAREA ASC'
    });
    var criticas = require('helper').query2array(ID_583256963_i);
    if (false) console.log('tareas criticas existentes', {
        "criticas": criticas
    });
    var tarea_index = 0;
    _.each(criticas, function(tarea, tarea_pos, tarea_list) {
        tarea_index += 1;
        if ((_.isObject(tarea.nivel_2) || _.isString(tarea.nivel_2)) && _.isEmpty(tarea.nivel_2)) {
            /** 
             * Revisamos cual es el ultimo nivel disponible y lo fijamos dentro de la variable tarea como ultimo_nivel 
             */
            var tarea = _.extend(tarea, {
                ultimo_nivel: tarea.nivel_1
            });
        } else if ((_.isObject(tarea.nivel_3) || _.isString(tarea.nivel_3)) && _.isEmpty(tarea.nivel_3)) {
            var tarea = _.extend(tarea, {
                ultimo_nivel: tarea.nivel_2
            });
        } else if ((_.isObject(tarea.nivel_4) || _.isString(tarea.nivel_4)) && _.isEmpty(tarea.nivel_4)) {
            var tarea = _.extend(tarea, {
                ultimo_nivel: tarea.nivel_3
            });
        } else if ((_.isObject(tarea.nivel_5) || _.isString(tarea.nivel_5)) && _.isEmpty(tarea.nivel_5)) {
            var tarea = _.extend(tarea, {
                ultimo_nivel: tarea.nivel_4
            });
        } else {
            var tarea = _.extend(tarea, {
                ultimo_nivel: tarea.nivel_5
            });
        }
        /** 
         * Insertamos la variable tarea con los atributos en el listado, seccion criticas 
         */
        var ID_1386974827 = [{
            ID_202820149: {},
            ID_1737707475: {
                text: String.format(L('x1487588533', '%1$s, %2$s'), (tarea.nivel_2) ? tarea.nivel_2.toString() : '', (tarea.pais_texto) ? tarea.pais_texto.toString() : '')
            },
            ID_422128048: {},
            ID_403273726: {},
            ID_620140124: {
                text: tarea.direccion
            },
            ID_1438432281: {},
            ID_1119172715: {},
            ID_1977608142: {
                text: tarea.ultimo_nivel
            },
            ID_1622346619: {},
            ID_1445023664: {},
            template: 'criticas',
            ID_521530515: {
                text: String.format(L('x1959850688_traducir', 'a %1$s km'), tarea.distance.toString())
            },
            ID_704055711: {
                myid: tarea.id
            },
            ID_715095938: {}

        }];
        var ID_1386974827_secs = {};
        _.map($.ID_879692163.getSections(), function(ID_1386974827_valor, ID_1386974827_indice) {
            ID_1386974827_secs[ID_1386974827_valor.getHeaderTitle()] = ID_1386974827_indice;
            return ID_1386974827_valor;
        });
        if ('' + L('x2819214932_traducir', 'holi1') + '' in ID_1386974827_secs) {
            $.ID_879692163.sections[ID_1386974827_secs['' + L('x2819214932_traducir', 'holi1') + '']].appendItems(ID_1386974827);
        } else {
            console.log('DEBUG:dynamic listItem points to a invalid section id: check attribute _seccion.');
        }
    });
    if (criticas && criticas.length) {
        /** 
         * Revisamos si la variable criticas contiene tareas, siendo true, contamos el largo del arreglo y las definimos en el badge, si no tiene tareas, no mostramos el badge 
         */
        var ID_786813367_badge = criticas.length;

        var set_Badge = function(valor) {
            if (OS_IOS) {
                if (valor == 'null') {
                    $.tab_ID_786813367.setBadge(null);
                } else {
                    $.tab_ID_786813367.setBadge(valor);
                }
            } else if (OS_ANDROID) {
                if (valor == 'null') {
                    $.tab_ID_786813367.setIcon(null);
                } else if ('images' in require('a4w').styles && 'androidbadge' + ID_786813367_badge in require('a4w').styles['images']) {
                    $.tab_ID_786813367.setIcon(require('a4w').styles['images']['androidbadge' + ID_786813367_badge]);
                    if (typeof WPATH != 'undefined') {
                        require(WPATH('vars'))['_android_badge'] = ID_786813367_badge;
                    } else {
                        require('vars')['_android_badge'] = ID_786813367_badge;
                    }
                }
            }
        };
        var get_Badge = function() {
            if (OS_IOS) {
                return $.tab_ID_786813367.getBadge();
            } else if (OS_ANDROID) {
                if (typeof WPATH != 'undefined') {
                    return require(WPATH('vars'))['_android_badge'];
                } else {
                    return require('vars')['_android_badge'];
                }
            } else {
                return 0;
            }
        };
        set_Badge(ID_786813367_badge);

    } else {
        var ID_786813367_badge = 'null';

        var set_Badge = function(valor) {
            if (OS_IOS) {
                if (valor == 'null') {
                    $.tab_ID_786813367.setBadge(null);
                } else {
                    $.tab_ID_786813367.setBadge(valor);
                }
            } else if (OS_ANDROID) {
                if (valor == 'null') {
                    $.tab_ID_786813367.setIcon(null);
                } else if ('images' in require('a4w').styles && 'androidbadge' + ID_786813367_badge in require('a4w').styles['images']) {
                    $.tab_ID_786813367.setIcon(require('a4w').styles['images']['androidbadge' + ID_786813367_badge]);
                    if (typeof WPATH != 'undefined') {
                        require(WPATH('vars'))['_android_badge'] = ID_786813367_badge;
                    } else {
                        require('vars')['_android_badge'] = ID_786813367_badge;
                    }
                }
            }
        };
        var get_Badge = function() {
            if (OS_IOS) {
                return $.tab_ID_786813367.getBadge();
            } else if (OS_ANDROID) {
                if (typeof WPATH != 'undefined') {
                    return require(WPATH('vars'))['_android_badge'];
                } else {
                    return require('vars')['_android_badge'];
                }
            } else {
                return 0;
            }
        };
        set_Badge(ID_786813367_badge);

    }
    /** 
     * Consultamos la tabla de las tareas criticas y guardamos en variable normales 
     */
    var ID_893838287_i = Alloy.createCollection('tareas_entrantes');
    var ID_893838287_i_where = 'tipo_tarea=0 ORDER BY FECHA_TAREA ASC';
    ID_893838287_i.fetch({
        query: 'SELECT * FROM tareas_entrantes WHERE tipo_tarea=0 ORDER BY FECHA_TAREA ASC'
    });
    var normales = require('helper').query2array(ID_893838287_i);
    if (normales && normales.length) {
        if (false) console.log('tareas normales existentes', {
            "normales": normales
        });
        var moment = require('alloy/moment');
        var fecha_hoy = moment(new Date()).format('YYYY-MM-DD');
        manana = new Date();
        manana.setDate(manana.getDate() + 1);;
        var moment = require('alloy/moment');
        var ID_298723346 = manana;
        var fecha_manana = moment(ID_298723346).format('YYYY-MM-DD');
        /** 
         * Inicializamos la variable de ultima fecha en vacio para usarla en el recorrido de tareas 
         */
        require('vars')['ultima_fecha'] = '';
        var tarea_index = 0;
        _.each(normales, function(tarea, tarea_pos, tarea_list) {
            tarea_index += 1;
            /** 
             * Variable usada para poder filtrar las tareas segun la fecha y no se vayan todas a una sola seccion 
             */
            var ultima_fecha = ('ultima_fecha' in require('vars')) ? require('vars')['ultima_fecha'] : '';
            if (ultima_fecha != tarea.fecha_tarea) {
                /** 
                 * Generamos una seccion nueva para las fechas que no sean de hoy, ni de manana 
                 */
                if (false) console.log('recorrien3', {
                    "fmanana": fecha_manana,
                    "fhoy": fecha_hoy,
                    "ufecha": ultima_fecha
                });
                if (tarea.fecha_tarea == fecha_hoy) {} else if (tarea.fecha_tarea == fecha_manana) {} else {
                    var fecha_titulo = null;
                    if ('formatear_fecha' in require('funciones')) {
                        fecha_titulo = require('funciones').formatear_fecha({
                            'fecha': tarea.fecha_tarea,
                            'formato': L('x2933785305_traducir', 'DD/MM/YYYY'),
                            'formato_entrada': L('x591439515_traducir', 'YYYY-MM-DD')
                        });
                    } else {
                        try {
                            fecha_titulo = f_formatear_fecha({
                                'fecha': tarea.fecha_tarea,
                                'formato': L('x2933785305_traducir', 'DD/MM/YYYY'),
                                'formato_entrada': L('x591439515_traducir', 'YYYY-MM-DD')
                            });
                        } catch (ee) {}
                    }
                    var ID_887257458 = Titanium.UI.createListSection({
                        headerTitle: L('x2345059420', 'fecha_entrantes')
                    });
                    var ID_819042770 = Titanium.UI.createView({
                        height: Ti.UI.FILL,
                        width: Ti.UI.FILL
                    });
                    var ID_1319827803 = Titanium.UI.createView({
                        height: '25dp',
                        layout: 'composite',
                        width: Ti.UI.FILL,
                        backgroundColor: '#F7F7F7'
                    });
                    var ID_1955950352 = Titanium.UI.createLabel({
                        text: fecha_titulo,
                        color: '#999999',
                        touchEnabled: false,
                        font: {
                            fontFamily: 'SFUIText-Medium',
                            fontSize: '14dp'
                        }

                    });
                    ID_1319827803.add(ID_1955950352);

                    ID_819042770.add(ID_1319827803);
                    ID_887257458.setHeaderView(ID_819042770);
                    $.ID_879692163.appendSection(ID_887257458);
                }
                /** 
                 * Actualizamos la variable con&#160;la ultima fecha de la tarea 
                 */
                require('vars')['ultima_fecha'] = tarea.fecha_tarea;
            }
            if ((_.isObject(tarea.nivel_2) || _.isString(tarea.nivel_2)) && _.isEmpty(tarea.nivel_2)) {
                /** 
                 * Revisamos cual es el ultimo nivel disponible y lo fijamos dentro de la variable tarea como ultimo_nivel 
                 */
                var tarea = _.extend(tarea, {
                    ultimo_nivel: tarea.nivel_1
                });
            } else if ((_.isObject(tarea.nivel_3) || _.isString(tarea.nivel_3)) && _.isEmpty(tarea.nivel_3)) {
                var tarea = _.extend(tarea, {
                    ultimo_nivel: tarea.nivel_2
                });
            } else if ((_.isObject(tarea.nivel_4) || _.isString(tarea.nivel_4)) && _.isEmpty(tarea.nivel_4)) {
                var tarea = _.extend(tarea, {
                    ultimo_nivel: tarea.nivel_3
                });
            } else if ((_.isObject(tarea.nivel_5) || _.isString(tarea.nivel_5)) && _.isEmpty(tarea.nivel_5)) {
                var tarea = _.extend(tarea, {
                    ultimo_nivel: tarea.nivel_4
                });
            } else {
                var tarea = _.extend(tarea, {
                    ultimo_nivel: tarea.nivel_5
                });
            }
            if (tarea.fecha_tarea == fecha_hoy) {
                /** 
                 * Dependiendo de la fecha de la tarea, es donde apuntaremos a la seccion que corresponde (segun fecha) 
                 */
                var ID_481046448 = [{
                    ID_1271969677: {},
                    ID_628062793: {
                        text: tarea.direccion
                    },
                    ID_568454310: {},
                    ID_926529622: {},
                    ID_1388054861: {
                        text: String.format(L('x1487588533', '%1$s, %2$s'), (tarea.nivel_2) ? tarea.nivel_2.toString() : '', (tarea.pais_texto) ? tarea.pais_texto.toString() : '')
                    },
                    ID_369983217: {},
                    ID_75895559: {},
                    ID_360694343: {
                        text: tarea.ultimo_nivel
                    },
                    ID_1217465633: {
                        text: String.format(L('x1959850688_traducir', 'a %1$s km'), tarea.distance.toString())
                    },
                    template: 'tareas',
                    ID_477443044: {},
                    ID_1228388310: {},
                    ID_871033850: {},
                    ID_422941248: {
                        myid: tarea.id
                    }

                }];
                var ID_481046448_secs = {};
                _.map($.ID_879692163.getSections(), function(ID_481046448_valor, ID_481046448_indice) {
                    ID_481046448_secs[ID_481046448_valor.getHeaderTitle()] = ID_481046448_indice;
                    return ID_481046448_valor;
                });
                if ('' + L('x3166667470', 'hoy_entrantes') + '' in ID_481046448_secs) {
                    $.ID_879692163.sections[ID_481046448_secs['' + L('x3166667470', 'hoy_entrantes') + '']].appendItems(ID_481046448);
                } else {
                    console.log('DEBUG:dynamic listItem points to a invalid section id: check attribute _seccion.');
                }
            } else if (tarea.fecha_tarea == fecha_manana) {
                var ID_1401951081 = [{
                    ID_1271969677: {},
                    ID_628062793: {
                        text: tarea.direccion
                    },
                    ID_568454310: {},
                    ID_926529622: {},
                    ID_1388054861: {
                        text: String.format(L('x1487588533', '%1$s, %2$s'), (tarea.nivel_2) ? tarea.nivel_2.toString() : '', (tarea.pais_texto) ? tarea.pais_texto.toString() : '')
                    },
                    ID_369983217: {},
                    ID_75895559: {},
                    ID_360694343: {
                        text: tarea.ultimo_nivel
                    },
                    ID_1217465633: {
                        text: String.format(L('x1959850688_traducir', 'a %1$s km'), tarea.distance.toString())
                    },
                    template: 'tareas',
                    ID_477443044: {},
                    ID_1228388310: {},
                    ID_871033850: {},
                    ID_422941248: {
                        myid: tarea.id
                    }

                }];
                var ID_1401951081_secs = {};
                _.map($.ID_879692163.getSections(), function(ID_1401951081_valor, ID_1401951081_indice) {
                    ID_1401951081_secs[ID_1401951081_valor.getHeaderTitle()] = ID_1401951081_indice;
                    return ID_1401951081_valor;
                });
                if ('' + L('x3690817388', 'manana_entrantes') + '' in ID_1401951081_secs) {
                    $.ID_879692163.sections[ID_1401951081_secs['' + L('x3690817388', 'manana_entrantes') + '']].appendItems(ID_1401951081);
                } else {
                    console.log('DEBUG:dynamic listItem points to a invalid section id: check attribute _seccion.');
                }
            } else {
                var ID_1079731371 = [{
                    ID_1271969677: {},
                    ID_628062793: {
                        text: tarea.direccion
                    },
                    ID_568454310: {},
                    ID_926529622: {},
                    ID_1388054861: {
                        text: String.format(L('x1487588533', '%1$s, %2$s'), (tarea.nivel_2) ? tarea.nivel_2.toString() : '', (tarea.pais_texto) ? tarea.pais_texto.toString() : '')
                    },
                    ID_369983217: {},
                    ID_75895559: {},
                    ID_360694343: {
                        text: tarea.ultimo_nivel
                    },
                    ID_1217465633: {
                        text: String.format(L('x1959850688_traducir', 'a %1$s km'), tarea.distance.toString())
                    },
                    template: 'tareas',
                    ID_477443044: {},
                    ID_1228388310: {},
                    ID_871033850: {},
                    ID_422941248: {
                        myid: tarea.id
                    }

                }];
                var ID_1079731371_secs = {};
                _.map($.ID_879692163.getSections(), function(ID_1079731371_valor, ID_1079731371_indice) {
                    ID_1079731371_secs[ID_1079731371_valor.getHeaderTitle()] = ID_1079731371_indice;
                    return ID_1079731371_valor;
                });
                if ('' + L('x2345059420', 'fecha_entrantes') + '' in ID_1079731371_secs) {
                    $.ID_879692163.sections[ID_1079731371_secs['' + L('x2345059420', 'fecha_entrantes') + '']].appendItems(ID_1079731371);
                } else {
                    console.log('DEBUG:dynamic listItem points to a invalid section id: check attribute _seccion.');
                }
            }
        });
    }
};
Alloy.Events.on('_refrescar_tareas_entrantes', _my_events['_refrescar_tareas_entrantes,ID_1636319035']);

_my_events['_refrescar_tareas,ID_411683779'] = function(evento) {

    Alloy.Events.trigger('_refrescar_tareas_entrantes');
};
Alloy.Events.on('_refrescar_tareas', _my_events['_refrescar_tareas,ID_411683779']);
var ID_170851302_func = function() {

    Alloy.Events.trigger('_refrescar_tareas_entrantes');
};
var ID_170851302 = setTimeout(ID_170851302_func, 1000 * 0.2);

//$.ID_216174848.open();