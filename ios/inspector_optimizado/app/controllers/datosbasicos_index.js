var _bind4section = {};
var _list_templates = {
    "pborrar": {
        "ID_1883915455": {
            "text": "{id}"
        },
        "ID_2120109018": {
            "text": "{nombre}"
        },
        "ID_1046919308": {},
        "ID_1735369570": {},
        "ID_1777579448": {},
        "ID_1254685949": {},
        "ID_1795050796": {},
        "ID_1752901998": {},
        "ID_1914367022": {
            "text": "{nombre}"
        },
        "ID_1249074865": {
            "text": "{id}"
        },
        "ID_1131557960": {},
        "ID_1361874287": {},
        "ID_1421561173": {},
        "ID_1142337614": {},
        "ID_1057712478": {
            "text": "{nombre}"
        },
        "ID_1942170367": {
            "text": "{id}"
        },
        "ID_1502687579": {},
        "ID_1852819254": {}
    },
    "contenido": {
        "ID_630800887": {},
        "ID_862352109": {
            "text": "{id}"
        },
        "ID_560452274": {
            "text": "{nombre}"
        }
    },
    "elemento": {
        "ID_432412973": {},
        "ID_291370846": {
            "text": "{nombre}"
        },
        "ID_1925487537": {
            "text": "{id_server}"
        },
        "ID_975786586": {
            "text": "{id_segured}"
        },
        "ID_1546190443": {}
    },
    "recinto": {
        "ID_1960582596": {},
        "ID_871581291": {
            "text": "{nombre}"
        },
        "ID_310297848": {
            "text": "{id}"
        }
    },
    "nivel": {
        "ID_1021786928": {
            "text": "{id}"
        },
        "ID_1241655463": {
            "text": "{nombre}"
        },
        "ID_599581692": {}
    }
};
var $datos = $.datos.toJSON();

$.ID_198296088_window.setTitleAttributes({
    color: 'WHITE'
});
var _activity;
if (OS_ANDROID) {
    _activity = $.ID_198296088.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_198296088';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_198296088_window.addEventListener('open', function(e) {
        abx.setStatusbarColor("#FFFFFF");
        abx.setBackgroundColor("#2d9edb");
    });
}

function Click_ID_1116577415(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    ID_1618331425.show();

}

$.ID_1836760008.init({
    titulo: L('x404129330_traducir', '¿EL ASEGURADO CONFIRMA ESTOS DATOS?'),
    __id: 'ALL1836760008',
    si: L('x1723413441_traducir', 'SI, Están correctos'),
    texto: L('x2083765231_traducir', 'El asegurado debe confirmar que los datos de esta sección están correctos'),
    pantalla: L('x2851883104_traducir', '¿ESTA DE ACUERDO?'),
    onno: no_ID_1976559498,
    onsi: si_ID_1142766943,
    no: L('x55492959_traducir', 'NO, Hay que modificar algo'),
    header: 'azul',
    onclick: Click_ID_518729539
});

function Click_ID_518729539(e) {

    var evento = e;
    var test = $.datos.toJSON();
    require('vars')[_var_scopekey]['todobien'] = L('x4261170317', 'true');
    if (_.isUndefined(test.presente_nombre)) {
        require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
        var ID_378680658_opts = [L('x1518866076_traducir', 'Aceptar')];
        var ID_378680658 = Ti.UI.createAlertDialog({
            title: L('x3237162386_traducir', 'Atencion'),
            message: L('x3689875543_traducir', 'Ingrese nombre de la persona presente'),
            buttonNames: ID_378680658_opts
        });
        ID_378680658.addEventListener('click', function(e) {
            var nulo = ID_378680658_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_378680658.show();
    } else if (test.presente_nombre.length == 0 || test.presente_nombre.length == '0') {
        require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
        var ID_880914267_opts = [L('x1518866076_traducir', 'Aceptar')];
        var ID_880914267 = Ti.UI.createAlertDialog({
            title: L('x3237162386_traducir', 'Atencion'),
            message: L('x3689875543_traducir', 'Ingrese nombre de la persona presente'),
            buttonNames: ID_880914267_opts
        });
        ID_880914267.addEventListener('click', function(e) {
            var nulo = ID_880914267_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_880914267.show();
    } else if (_.isUndefined(test.presente_rut)) {
        require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
        var ID_1829926154_opts = [L('x1518866076_traducir', 'Aceptar')];
        var ID_1829926154 = Ti.UI.createAlertDialog({
            title: L('x3237162386_traducir', 'Atencion'),
            message: L('x2263964467_traducir', 'Ingrese Rut de la persona presente'),
            buttonNames: ID_1829926154_opts
        });
        ID_1829926154.addEventListener('click', function(e) {
            var nulo = ID_1829926154_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1829926154.show();
    } else if (test.presente_rut.length == 0 || test.presente_rut.length == '0') {
        require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
        var ID_1135862644_opts = [L('x1518866076_traducir', 'Aceptar')];
        var ID_1135862644 = Ti.UI.createAlertDialog({
            title: L('x3237162386_traducir', 'Atencion'),
            message: L('x2263964467_traducir', 'Ingrese Rut de la persona presente'),
            buttonNames: ID_1135862644_opts
        });
        ID_1135862644.addEventListener('click', function(e) {
            var nulo = ID_1135862644_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1135862644.show();
    } else if (!_.isUndefined(test.direccion_correcta)) {
        if (test.direccion_correcta == false || test.direccion_correcta == 'false') {
            if (_.isUndefined(test.direccion_riesgo)) {
                require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
                var ID_176720367_opts = [L('x1518866076_traducir', 'Aceptar')];
                var ID_176720367 = Ti.UI.createAlertDialog({
                    title: L('x3237162386_traducir', 'Atencion'),
                    message: L('x2359666818_traducir', 'Debe indicar la dirección correcta'),
                    buttonNames: ID_176720367_opts
                });
                ID_176720367.addEventListener('click', function(e) {
                    var nulo = ID_176720367_opts[e.index];
                    nulo = null;
                    e.source.removeEventListener("click", arguments.callee);
                });
                ID_176720367.show();
            } else if (_.isUndefined(test.direccion_comuna)) {
                require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
                var ID_584210670_opts = [L('x1518866076_traducir', 'Aceptar')];
                var ID_584210670 = Ti.UI.createAlertDialog({
                    title: L('x3237162386_traducir', 'Atencion'),
                    message: L('x4270715747_traducir', 'Debe indicar la comuna de la dirección'),
                    buttonNames: ID_584210670_opts
                });
                ID_584210670.addEventListener('click', function(e) {
                    var nulo = ID_584210670_opts[e.index];
                    nulo = null;
                    e.source.removeEventListener("click", arguments.callee);
                });
                ID_584210670.show();
            } else if (_.isUndefined(test.direccion_ciudad)) {
                require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
                var ID_1689138946_opts = [L('x1518866076_traducir', 'Aceptar')];
                var ID_1689138946 = Ti.UI.createAlertDialog({
                    title: L('x3237162386_traducir', 'Atencion'),
                    message: L('x3232683315_traducir', 'Debe indicar la ciudad de la dirección'),
                    buttonNames: ID_1689138946_opts
                });
                ID_1689138946.addEventListener('click', function(e) {
                    var nulo = ID_1689138946_opts[e.index];
                    nulo = null;
                    e.source.removeEventListener("click", arguments.callee);
                });
                ID_1689138946.show();
            }
        }
    }
    var todobien = ('todobien' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['todobien'] : '';
    if (todobien == true || todobien == 'true') {
        test = null;
        $.ID_1836760008.enviar({});
    }

}

function si_ID_1142766943(e) {

    var evento = e;
    /** 
     * Guardamos los datos de la tabla en el modelo 
     */
    Alloy.Collections[$.datos.config.adapter.collection_name].add($.datos);
    $.datos.save();
    Alloy.Collections[$.datos.config.adapter.collection_name].fetch();
    Alloy.createController("caracteristicas_index", {}).getView().open();

}

function no_ID_1976559498(e) {

    var evento = e;

}

function Change_ID_1908333488(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    $.datos.set({
        fono_fijo: elemento
    });
    if ('datos' in $) $datos = $.datos.toJSON();
    elemento = null, source = null;

}

function Change_ID_1416588784(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    $.datos.set({
        fono_movil: elemento
    });
    if ('datos' in $) $datos = $.datos.toJSON();
    elemento = null, source = null;

}

function Change_ID_845378117(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    $.datos.set({
        email: elemento
    });
    if ('datos' in $) $datos = $.datos.toJSON();
    elemento = null, source = null;

}

function Change_ID_1520521827(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    $.datos.set({
        presente_nombre: elemento
    });
    if ('datos' in $) $datos = $.datos.toJSON();
    elemento = null, source = null;

}

function Change_ID_1495593922(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    $.datos.set({
        presente_rut: elemento
    });
    if ('datos' in $) $datos = $.datos.toJSON();
    elemento = null, source = null;

}

function Change_ID_1255411964(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    if (elemento == true || elemento == 'true') {
        $.ID_1413046510.setText('SI');

        $.ID_1993726132.setEditable('false');

        $.ID_926493919.setEditable('false');

        $.ID_301031689.setEditable('false');

        require('vars')[_var_scopekey]['direccioncorrecta'] = L('x2212294583', '1');
    } else {
        $.ID_1413046510.setText('NO');

        require('vars')[_var_scopekey]['direccioncorrecta'] = L('x4108050209', '0');
        $.ID_1993726132.setEditable(true);

        $.ID_926493919.setEditable(true);

        $.ID_301031689.setEditable(true);

    }
    $.datos.set({
        direccion_correcta: elemento
    });
    if ('datos' in $) $datos = $.datos.toJSON();
    elemento = null;

}

function Change_ID_298318017(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    $.datos.set({
        direccion_riesgo: elemento
    });
    if ('datos' in $) $datos = $.datos.toJSON();
    elemento = null, source = null;

}

function Change_ID_1898891136(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    $.datos.set({
        direccion_comuna: elemento
    });
    if ('datos' in $) $datos = $.datos.toJSON();
    elemento = null, source = null;

}

function Change_ID_1478554101(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    $.datos.set({
        direccion_ciudad: elemento
    });
    if ('datos' in $) $datos = $.datos.toJSON();
    elemento = null, source = null;

}

function Click_ID_454606560(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    /** 
     * desenfoca campos de texto para ocultar teclado al presionar en partes blancas. 
     */
    $.ID_1130272126.blur();
    $.ID_1267847770.blur();
    $.ID_494849736.blur();
    $.ID_1066684589.blur();
    $.ID_636596927.blur();
    $.ID_1993726132.blur();
    $.ID_926493919.blur();
    $.ID_301031689.blur();

}

function Postlayout_ID_1880739227(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    $.ID_1771180057.show();

}

function Postlayout_ID_1924806703(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var anteriores = ('anteriores' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['anteriores'] : '';
    if (anteriores == false || anteriores == 'false') {
        /** 
         * Cerramos fotosrequeridas (limpieza memoria) 
         */
        Alloy.Events.trigger('_cerrar_insp', {
            pantalla: L('x3287362058_traducir', 'frequeridas')
        });
        /** 
         * Cerramos pantalla hayalguien 
         */
        Alloy.Events.trigger('_cerrar_insp', {
            pantalla: L('x3181120844_traducir', 'hayalguien')
        });
        require('vars')[_var_scopekey]['anteriores'] = L('x4261170317', 'true');
    }

}

(function() {
    /** 
     * Recuperamos variable para verificar si tiene datos, siendo asi, cargamos los datos en la tabla de datosbasicos 
     */
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    require('vars')[_var_scopekey]['anteriores'] = L('x734881840_traducir', 'false');
    if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
        /** 
         * Seteamos datos a vista 
         */
        $.datos.set({
            id_inspeccion: seltarea.id_server,
            fono_movil: seltarea.asegurado_tel_movil,
            fono_fijo: seltarea.asegurado_tel_fijo,
            direccion_ciudad: seltarea.nivel_2,
            rut_asegurado: seltarea.asegurado_codigo_identificador,
            direccion_riesgo: seltarea.direccion,
            direccion_correcta: true,
            direccion_comuna: seltarea.nivel_3,
            asegurador: seltarea.asegurador,
            nro_caso: seltarea.num_caso,
            asegurado: seltarea.asegurado_nombre,
            fecha_siniestro: seltarea.fecha_siniestro,
            email: seltarea.asegurado_correo
        });
        if ('datos' in $) $datos = $.datos.toJSON();
        /** 
         * Consultamos fecha/hora inicio de inspeccion de inspecciones 
         */
        var ID_412711721_i = Alloy.createCollection('inspecciones');
        var ID_412711721_i_where = 'id_server=\'' + seltarea.id_server + '\'';
        ID_412711721_i.fetch({
            query: 'SELECT * FROM inspecciones WHERE id_server=\'' + seltarea.id_server + '\''
        });
        var insp = require('helper').query2array(ID_412711721_i);
        if (insp && insp.length) {
            /** 
             * Traspasamos informacion a datosbasicos 
             */
            $.datos.set({
                hora_inspeccion: insp[0].hora,
                fecha_inspeccion: insp[0].fecha,
                fecha_full_inspeccion: insp[0].fecha_inspeccion_inicio
            });
            if ('datos' in $) $datos = $.datos.toJSON();
        } else {
            /** 
             * Formateamos hora/fecha inspeccion 
             */
            var moment = require('alloy/moment');
            var hora = moment(new Date()).format('HH:mm');
            var moment = require('alloy/moment');
            var fecha = moment(new Date()).format('DD-MM-YYYY');
            $.datos.set({
                hora_inspeccion: hora,
                fecha_inspeccion: fecha,
                fecha_full_inspeccion: new Date()
            });
            if ('datos' in $) $datos = $.datos.toJSON();
        }
    } else {
        /** 
         * Datos dummies para test 
         */
        $.datos.set({
            id_inspeccion: 112233,
            fono_movil: 56999990070,
            fono_fijo: 56280908070,
            direccion_riesgo: 'Av. Las Condes 9460',
            direccion_correcta: true,
            asegurador: 'Falabella',
            nro_caso: 99999,
            asegurado: 'Pepito',
            fecha_siniestro: '17-01-2017',
            email: 'pepito@gmail.com'
        });
        if ('datos' in $) $datos = $.datos.toJSON();
        /** 
         * Formateamos hora/fecha inspeccion 
         */
        var moment = require('alloy/moment');
        var hora = moment(new Date()).format('HH:mm');
        var moment = require('alloy/moment');
        var fecha = moment(new Date()).format('DD-MM-YYYY');
        /** 
         * Traspasamos informacion a datosbasicos 
         */
        $.datos.set({
            hora_inspeccion: hora,
            fecha_inspeccion: fecha,
            fecha_full_inspeccion: new Date()
        });
        if ('datos' in $) $datos = $.datos.toJSON();
    }
})();


/** 
 * Cerramos esta pantalla cuando es llamada en la pantalla siguente (siniestro) 
 */
_my_events['_cerrar_insp,ID_1699439202'] = function(evento) {
    if (!_.isUndefined(evento.pantalla)) {
        if (evento.pantalla == L('x3679018572_traducir', 'basicos')) {
            if (false) console.log('debug cerrando datosbasicos', {});
            var ID_878785134_trycatch = {
                error: function(e) {
                    if (false) console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
                }
            };
            try {
                ID_878785134_trycatch.error = function(evento) {
                    if (false) console.log('error cerrando datosbasicos', {});
                };
                $.ID_198296088.close();
            } catch (e) {
                ID_878785134_trycatch.error(e);
            }
        }
    } else {
        if (false) console.log('debug cerrando (todas) datosbasicos', {});
        var ID_1233793887_trycatch = {
            error: function(e) {
                if (false) console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
            }
        };
        try {
            ID_1233793887_trycatch.error = function(evento) {
                if (false) console.log('error cerrando datosbasicos', {});
            };
            $.ID_198296088.close();
        } catch (e) {
            ID_1233793887_trycatch.error(e);
        }
    }
};
Alloy.Events.on('_cerrar_insp', _my_events['_cerrar_insp,ID_1699439202']);
var ID_1618331425_opts = [L('x3481285698_traducir', 'Asegurado no puede seguir'), L('x3077268071_traducir', ' Se me acabo la bateria'), L('x479989047_traducir', ' Tuve un accidente'), L('x2376009830_traducir', ' Cancelar')];
var ID_1618331425 = Ti.UI.createOptionDialog({
    title: L('x1670176792_traducir', 'RAZON PARA CANCELAR INSPECCION ACTUAL'),
    options: ID_1618331425_opts
});
ID_1618331425.addEventListener('click', function(e) {
    var resp = ID_1618331425_opts[e.index];
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
    if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
        var razon = "";
        if (resp == L('x3481285698_traducir', 'Asegurado no puede seguir')) {
            razon = "Asegurado no puede seguir";
        }
        if (resp == L('x1862794075_traducir', 'Se me acabo la bateria')) {
            razon = "Se me acabo la bateria";
        }
        if (resp == L('x4012805111_traducir', 'Tuve un accidente')) {
            razon = "Tuve un accidente";
        }
        if (false) console.log('mi razon es', {
            "datos": razon
        });
        if ((_.isObject(razon) || (_.isString(razon)) && !_.isEmpty(razon)) || _.isNumber(razon) || _.isBoolean(razon)) {
            if (false) console.log('llamando servicio cancelarTarea', {});
            require('vars')['insp_cancelada'] = L('x2941610362_traducir', 'siniestro');
            Alloy.Collections[$.datos.config.adapter.collection_name].add($.datos);
            $.datos.save();
            Alloy.Collections[$.datos.config.adapter.collection_name].fetch();
            var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
            var ID_2092947148_visible = true;

            if (ID_2092947148_visible == 'si') {
                ID_2092947148_visible = true;
            } else if (ID_2092947148_visible == 'no') {
                ID_2092947148_visible = false;
            }
            $.ID_2092947148.setVisible(ID_2092947148_visible);

            var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
            var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
            if (false) console.log('detalle de seltarea', {
                "data": seltarea
            });
            var datos = {
                id_inspector: inspector.id_server,
                codigo_identificador: inspector.codigo_identificador,
                id_server: seltarea.id_server,
                num_caso: seltarea.num_caso,
                razon: razon
            };
            require('vars')[_var_scopekey]['datos'] = datos;
            var ID_1414718499 = {};
            ID_1414718499.success = function(e) {
                var elemento = e,
                    valor = e;
                var ID_2092947148_visible = false;

                if (ID_2092947148_visible == 'si') {
                    ID_2092947148_visible = true;
                } else if (ID_2092947148_visible == 'no') {
                    ID_2092947148_visible = false;
                }
                $.ID_2092947148.setVisible(ID_2092947148_visible);

                Alloy.createController("firma_index", {}).getView().open();
                var ID_1744588117_func = function() {
                    /** 
                     * Cerramos pantalla datos basicos 
                     */
                    Alloy.Events.trigger('_cerrar_insp', {
                        pantalla: L('x1828862638_traducir', 'basicos')
                    });
                };
                var ID_1744588117 = setTimeout(ID_1744588117_func, 1000 * 0.5);
                elemento = null, valor = null;
            };
            ID_1414718499.error = function(e) {
                var elemento = e,
                    valor = e;
                if (false) console.log('hubo error llamando cancelarTarea, la agregamos a cola de salida', {
                    "elemento": elemento
                });
                if (false) console.log('agregando servicio cancelarTarea a cola', {});
                var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
                var datos = ('datos' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['datos'] : '';
                var ID_1855711966_m = Alloy.Collections.cola;
                var ID_1855711966_fila = Alloy.createModel('cola', {
                    data: JSON.stringify(datos),
                    id_tarea: seltarea.id_server,
                    tipo: 'cancelar'
                });
                ID_1855711966_m.add(ID_1855711966_fila);
                ID_1855711966_fila.save();
                var ID_2092947148_visible = false;

                if (ID_2092947148_visible == 'si') {
                    ID_2092947148_visible = true;
                } else if (ID_2092947148_visible == 'no') {
                    ID_2092947148_visible = false;
                }
                $.ID_2092947148.setVisible(ID_2092947148_visible);

                Alloy.createController("firma_index", {}).getView().open();
                /** 
                 * Cerramos pantalla datos basicos 
                 */
                Alloy.Events.trigger('_cerrar_insp', {
                    pantalla: L('x1828862638_traducir', 'basicos')
                });
                elemento = null, valor = null;
            };
            require('helper').ajaxUnico('ID_1414718499', '' + String.format(L('x3244141284', '%1$scancelarTarea'), url_server.toString()) + '', 'POST', {
                id_inspector: seltarea.id_inspector,
                codigo_identificador: inspector.codigo_identificador,
                id_tarea: seltarea.id_server,
                num_caso: seltarea.num_caso,
                mensaje: razon,
                opcion: 0,
                tipo: 1
            }, 15000, ID_1414718499);
        }
    }
    resp = null;
});

if (OS_IOS || OS_ANDROID) {
    $.ID_198296088.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}
//$.ID_198296088.open();