var _bind4section = {};
var _list_templates = {
    "pborrar": {
        "ID_1883915455": {
            "text": "{id}"
        },
        "ID_2120109018": {
            "text": "{nombre}"
        },
        "ID_1046919308": {},
        "ID_1735369570": {},
        "ID_1777579448": {},
        "ID_1254685949": {},
        "ID_1795050796": {},
        "ID_1752901998": {},
        "ID_1914367022": {
            "text": "{nombre}"
        },
        "ID_1249074865": {
            "text": "{id}"
        },
        "ID_1131557960": {},
        "ID_1361874287": {},
        "ID_1421561173": {},
        "ID_1142337614": {},
        "ID_1057712478": {
            "text": "{nombre}"
        },
        "ID_1942170367": {
            "text": "{id}"
        },
        "ID_1502687579": {},
        "ID_1852819254": {}
    },
    "contenido": {
        "ID_630800887": {},
        "ID_862352109": {
            "text": "{id}"
        },
        "ID_560452274": {
            "text": "{nombre}"
        }
    },
    "elemento": {
        "ID_432412973": {},
        "ID_291370846": {
            "text": "{nombre}"
        },
        "ID_1925487537": {
            "text": "{id_server}"
        },
        "ID_975786586": {
            "text": "{id_segured}"
        },
        "ID_1546190443": {}
    },
    "recinto": {
        "ID_1960582596": {},
        "ID_871581291": {
            "text": "{nombre}"
        },
        "ID_310297848": {
            "text": "{id}"
        }
    },
    "nivel": {
        "ID_1021786928": {
            "text": "{id}"
        },
        "ID_1241655463": {
            "text": "{nombre}"
        },
        "ID_599581692": {}
    }
};

var _activity;
if (OS_ANDROID) {
    _activity = $.ID_411389157.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_411389157';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_411389157_window.addEventListener('open', function(e) {
        abx.setStatusbarColor("#FFFFFF");
        abx.setBackgroundColor("#ffffff");
    });
}

function Touchstart_ID_1090731328(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    $.ID_1056584802.setColor('#ff0033');


}

function Touchend_ID_1470694348(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    $.ID_1056584802.setColor('#2d9edb');

    $.ID_411389157.close();

}

function Click_ID_1766042303(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    /** 
     * Eenviamos a ver datos del asegurado 
     */
    var nulo = Alloy.createController("datosasegurado_index", {}).getView();
    nulo.open({
        modal: true
    });

    nulo = null;

}

$.ID_1792932375.init({
    __id: 'ALL1792932375',
    onlisto: Listo_ID_1799325527,
    externo: true,
    label_a: L('x3904355907_traducir', 'a'),
    onerror: Error_ID_1068050163,
    _bono: L('x2764662954_traducir', 'Bono adicional:'),
    bono: L('', ''),
    bt_cancelar: L('x1030930307_traducir', 'Cerrar'),
    ondatos: Datos_ID_1536779017
});

function Listo_ID_1799325527(e) {

    var evento = e;
    if (false) console.log('widget mapa ha llamado evento \'listo\'', {});

}

function Error_ID_1068050163(e) {

    var evento = e;
    if (false) console.log('ha ocurrido un error con el mapa', {
        "evento": evento
    });
    var ID_1066587408_opts = [L('x1518866076_traducir', 'Aceptar')];
    var ID_1066587408 = Ti.UI.createAlertDialog({
        title: L('x57652245_traducir', 'Alerta'),
        message: L('x1774080321_traducir', 'Ha ocurrido un error con el mapa'),
        buttonNames: ID_1066587408_opts
    });
    ID_1066587408.addEventListener('click', function(e) {
        var x = ID_1066587408_opts[e.index];
        x = null;

        e.source.removeEventListener("click", arguments.callee);
    });
    ID_1066587408.show();

}

function Datos_ID_1536779017(e) {

    var evento = e;
    if (false) console.log(String.format(L('x3211431303_traducir', 'datos recibidos desde widget mapa: %1$s'), evento.ruta_distancia.toString()), {
        "evento": evento
    });

}

function Longpress_ID_1611955468(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var bloquear_inspeccion = ('bloquear_inspeccion' in require('vars')) ? require('vars')['bloquear_inspeccion'] : '';
    if (false) console.log('psb longpress en boton', {
        "bloquear": bloquear_inspeccion
    });
    if (bloquear_inspeccion == false || bloquear_inspeccion == 'false') {
        /** 
         * Recuperamos variables. seltarea contiene el detalle de la tarea seleccionada, url_server contiene la url de uadjust, inspector contiene el detalle del inspector, iniciar_seguimiento flag para cambiar el estado del seguimiento, long_activo flag para el cambio de color del boton 
         */
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
        var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
        var iniciar_seguimiento = ('iniciar_seguimiento' in require('vars')) ? require('vars')['iniciar_seguimiento'] : '';
        require('vars')['long_activo'] = L('x4261170317', 'true');
        if (false) console.log('psb longpress en boton insp-false', {
            "bloquear": bloquear_inspeccion
        });
        if (iniciar_seguimiento == true || iniciar_seguimiento == 'true') {
            if (false) console.log('psb boton dice iniciar_seguimiento en true, cambiamos cosas', {
                "bloquear": bloquear_inspeccion
            });
            /** 
             * Informamos que debemos seguir esta tarea 
             */
            require('vars')['seguir_tarea'] = seltarea.id_server;
            /** 
             * Necesario para refrescar tareas en cierre de ventana 
             */
            require('vars')['seguimiento_cambiado'] = L('x4261170317', 'true');
            /** 
             * Actualizamos tabla local, refrescamos tareas y transformamos en mantener para iniciar. Cambiamos todas las tareas nuestras que tienen estado 4 a estado 3 (por si otra tarea tenia seguimiento antes) 
             */
            var ID_892467957_i = Alloy.createCollection('tareas');
            var ID_892467957_i_where = 'estado_tarea=4';
            ID_892467957_i.fetch({
                query: 'SELECT * FROM tareas WHERE estado_tarea=4'
            });
            var tareas = require('helper').query2array(ID_892467957_i);
            var db = Ti.Database.open(ID_892467957_i.config.adapter.db_name);
            if (ID_892467957_i_where == '') {
                var sql = 'UPDATE ' + ID_892467957_i.config.adapter.collection_name + ' SET estado_tarea=3';
            } else {
                var sql = 'UPDATE ' + ID_892467957_i.config.adapter.collection_name + ' SET estado_tarea=3 WHERE ' + ID_892467957_i_where;
            }
            db.execute(sql);
            db.close();
            var ID_247745160_i = Alloy.createCollection('tareas');
            var ID_247745160_i_where = 'id=\'' + seltarea.id + '\'';
            ID_247745160_i.fetch({
                query: 'SELECT * FROM tareas WHERE id=\'' + seltarea.id + '\''
            });
            var tareas = require('helper').query2array(ID_247745160_i);
            var db = Ti.Database.open(ID_247745160_i.config.adapter.db_name);
            if (ID_247745160_i_where == '') {
                var sql = 'UPDATE ' + ID_247745160_i.config.adapter.collection_name + ' SET estado_tarea=4';
            } else {
                var sql = 'UPDATE ' + ID_247745160_i.config.adapter.collection_name + ' SET estado_tarea=4 WHERE ' + ID_247745160_i_where;
            }
            db.execute(sql);
            db.close();
            var ID_1293237312_func = function() {

                Alloy.Events.trigger('_refrescar_tareas');
            };
            var ID_1293237312 = setTimeout(ID_1293237312_func, 1000 * 0.2);
            /** 
             * Mostramos monito 
             */

            $.ID_1792932375.update({
                monito: true
            });
            require('vars')['long_activo'] = L('x734881840_traducir', 'false');
            /** 
             * transformamos en boton azul de mantener para iniciar 
             */
            require('vars')['iniciar_seguimiento'] = L('x734881840_traducir', 'false');
            var ID_1935223870_estilo = 'fondoazul';

            var setEstilo = function(clase) {
                if ('styles' in require('a4w') && clase in require('a4w').styles['classes']) {
                    try {
                        $.ID_1935223870.applyProperties(require('a4w').styles['classes'][clase]);
                    } catch (sete_err) {}
                }
            };
            setEstilo(ID_1935223870_estilo);

            $.ID_1289130430.setText('MANTENER PARA INICIAR');


            $.ID_1125701114.update({
                texto: L('x2315614667_traducir', 'Tip. Nunca dejes una tarea sin inspeccionar. El no hacerlo te afecta directamente')
            });
            $.ID_509734203.setText('Se iniciara el proceso de inspeccion');

            var gps_error = ('gps_error' in require('vars')) ? require('vars')['gps_error'] : '';
            if (gps_error == true || gps_error == 'true') {
                /** 
                 * Avisamos a backend la ubicacion del inspector 
                 */
                var ID_122407932 = {};

                ID_122407932.success = function(e) {
                    var elemento = e,
                        valor = e;
                    if (false) console.log('respuesta exitosa de guardarUbicacion en iniciarSeguimiento', {
                        "datos": elemento
                    });
                    elemento = null, valor = null;
                };

                ID_122407932.error = function(e) {
                    var elemento = e,
                        valor = e;
                    if (false) console.log('respuesta fallida de guardarUbicacion en iniciarSeguimiento', {
                        "datos": elemento
                    });
                    elemento = null, valor = null;
                };
                require('helper').ajaxUnico('ID_122407932', '' + String.format(L('x4011689324', '%1$sguardarUbicacion'), url_server.toString()) + '', 'POST', {
                    id_inspector: inspector.id_server,
                    id_tarea: seltarea.id_server,
                    lat: -1,
                    lon: -1
                }, 15000, ID_122407932);
            } else {
                var gps_latitud = ('gps_latitud' in require('vars')) ? require('vars')['gps_latitud'] : '';
                var gps_longitud = ('gps_longitud' in require('vars')) ? require('vars')['gps_longitud'] : '';
                var ID_1054961310 = {};

                ID_1054961310.success = function(e) {
                    var elemento = e,
                        valor = e;
                    if (false) console.log('respuesta exitosa de guardarUbicacion en iniciarSeguimiento', {
                        "datos": elemento
                    });
                    elemento = null, valor = null;
                };

                ID_1054961310.error = function(e) {
                    var elemento = e,
                        valor = e;
                    if (false) console.log('respuesta fallida de guardarUbicacion en iniciarSeguimiento', {
                        "datos": elemento
                    });
                    elemento = null, valor = null;
                };
                require('helper').ajaxUnico('ID_1054961310', '' + String.format(L('x4011689324', '%1$sguardarUbicacion'), url_server.toString()) + '', 'POST', {
                    id_inspector: inspector.id_server,
                    id_tarea: seltarea.id_server,
                    lat: gps_latitud,
                    lon: gps_longitud
                }, 15000, ID_1054961310);
            }
        } else {
            var ID_1935223870_estilo = 'fondoazul';

            var setEstilo = function(clase) {
                if ('styles' in require('a4w') && clase in require('a4w').styles['classes']) {
                    try {
                        $.ID_1935223870.applyProperties(require('a4w').styles['classes'][clase]);
                    } catch (sete_err) {}
                }
            };
            setEstilo(ID_1935223870_estilo);

            Alloy.createController("fotosrequeridas_index", {}).getView().open();
        }
    }

}

function Touchstart_ID_774746224(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var bloquear_inspeccion = ('bloquear_inspeccion' in require('vars')) ? require('vars')['bloquear_inspeccion'] : '';
    if (bloquear_inspeccion == false || bloquear_inspeccion == 'false') {
        var iniciar_seguimiento = ('iniciar_seguimiento' in require('vars')) ? require('vars')['iniciar_seguimiento'] : '';
        if (iniciar_seguimiento == true || iniciar_seguimiento == 'true') {
            var ID_1935223870_estilo = 'fondonaranjo';

            var setEstilo = function(clase) {
                if ('styles' in require('a4w') && clase in require('a4w').styles['classes']) {
                    try {
                        $.ID_1935223870.applyProperties(require('a4w').styles['classes'][clase]);
                    } catch (sete_err) {}
                }
            };
            setEstilo(ID_1935223870_estilo);

        } else {
            var ID_1935223870_estilo = 'fondoceleste';

            var setEstilo = function(clase) {
                if ('styles' in require('a4w') && clase in require('a4w').styles['classes']) {
                    try {
                        $.ID_1935223870.applyProperties(require('a4w').styles['classes'][clase]);
                    } catch (sete_err) {}
                }
            };
            setEstilo(ID_1935223870_estilo);

        }
    }

}

function Touchend_ID_1434380729(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    /** 
     * Dejamos boton como estaba 
     */
    var bloquear_inspeccion = ('bloquear_inspeccion' in require('vars')) ? require('vars')['bloquear_inspeccion'] : '';
    var long_activo = ('long_activo' in require('vars')) ? require('vars')['long_activo'] : '';
    if (bloquear_inspeccion == false || bloquear_inspeccion == 'false') {
        if (long_activo == false || long_activo == 'false') {
            var iniciar_seguimiento = ('iniciar_seguimiento' in require('vars')) ? require('vars')['iniciar_seguimiento'] : '';
            if (iniciar_seguimiento == true || iniciar_seguimiento == 'true') {
                var ID_1935223870_estilo = 'fondoamarillo';

                var setEstilo = function(clase) {
                    if ('styles' in require('a4w') && clase in require('a4w').styles['classes']) {
                        try {
                            $.ID_1935223870.applyProperties(require('a4w').styles['classes'][clase]);
                        } catch (sete_err) {}
                    }
                };
                setEstilo(ID_1935223870_estilo);

            } else {
                var ID_1935223870_estilo = 'fondoazul';

                var setEstilo = function(clase) {
                    if ('styles' in require('a4w') && clase in require('a4w').styles['classes']) {
                        try {
                            $.ID_1935223870.applyProperties(require('a4w').styles['classes'][clase]);
                        } catch (sete_err) {}
                    }
                };
                setEstilo(ID_1935223870_estilo);

            }
        }
    }

}

$.ID_1125701114.init({
    __id: 'ALL1125701114',
    texto: L('x1739881219_traducir', 'Tip: Nunca dejes una tarea sin inspeccionar. El no hacerlo te afecta directamente'),
    bottom: L('x3693793700_traducir', '40'),
    tipo: L('x99131830', '_tip')
});


(function() {
    if (false) console.log('argumentos detalletarea', {
        "modelo": args
    });
    require('vars')['long_activo'] = L('x734881840_traducir', 'false');
    require('vars')['seguimiento_cambiado'] = L('x734881840_traducir', 'false');
    /** 
     * Filtramos el id de la tarea con los parameros desde la pantalla anterior 
     */
    var ID_509195037_i = Alloy.createCollection('tareas');
    var ID_509195037_i_where = 'id=\'' + args._id + '\'';
    ID_509195037_i.fetch({
        query: 'SELECT * FROM tareas WHERE id=\'' + args._id + '\''
    });
    var tareas = require('helper').query2array(ID_509195037_i);
    if (tareas && tareas.length) {
        /** 
         * Esto es util para la sub-pantalla datos del asegurado 
         */
        require('vars')['seltarea'] = tareas[0];
        /** 
         * Recuperamos los valores defaults 
         */
        var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
        require('vars')['bloquear_inspeccion'] = L('x734881840_traducir', 'false');
        if (false) console.log('adaptamos datos', {});
        /** 
         * Creamos variable de estructura para cargar datos de la tarea 
         */
        var info = {
            direccion: tareas[0].direccion,
            ciudad_pais: String.format(L('x1487588533', '%1$s, %2$s'), (tareas[0].nivel_2) ? tareas[0].nivel_2.toString() : '', (tareas[0].pais_texto) ? tareas[0].pais_texto.toString() : ''),
            distancia: tareas[0].distance
        };
        if ((_.isObject(tareas[0].nivel_2) || _.isString(tareas[0].nivel_2)) && _.isEmpty(tareas[0].nivel_2)) {
            /** 
             * Definimos el ultimo nivel de la tarea y lo guardamos en la estructura como comuna 
             */
            var info = _.extend(info, {
                comuna: tareas[0].nivel_1
            });
        } else if ((_.isObject(tareas[0].nivel_3) || _.isString(tareas[0].nivel_3)) && _.isEmpty(tareas[0].nivel_3)) {
            var info = _.extend(info, {
                comuna: tareas[0].nivel_2
            });
        } else if ((_.isObject(tareas[0].nivel_4) || _.isString(tareas[0].nivel_4)) && _.isEmpty(tareas[0].nivel_4)) {
            var info = _.extend(info, {
                comuna: tareas[0].nivel_3
            });
        } else if ((_.isObject(tareas[0].nivel_5) || _.isString(tareas[0].nivel_5)) && _.isEmpty(tareas[0].nivel_5)) {
            var info = _.extend(info, {
                comuna: tareas[0].nivel_4
            });
        } else {
            var info = _.extend(info, {
                comuna: tareas[0].nivel_5
            });
        }
        if (false) console.log('actualizamos mapa con direccion', {});
        /** 
         * Actualizamos widget de mapa con los datos de la tarea 
         */

        $.ID_1792932375.update({
            tipo: 'ubicacion',
            latitud: tareas[0].lat,
            longitud: tareas[0].lon,
            ruta: true,
            direccion: info.direccion,
            comuna: info.comuna,
            ciudad: info.ciudad_pais,
            distancia: info.distancia,
            externo: true
        });
        /** 
         * Si seguir_tarea no es igual a nuestro id_server: significa que no estamos siguiendola por lo que boton debe decir iniciar seguimiento, en caso contrario, debemos mostrar monito porque se esta siguiendo. 
         */
        var seguir_tarea = ('seguir_tarea' in require('vars')) ? require('vars')['seguir_tarea'] : '';
        if (seguir_tarea != tareas[0].id_server) {
            var ID_1935223870_estilo = 'fondoamarillo';

            var setEstilo = function(clase) {
                if ('styles' in require('a4w') && clase in require('a4w').styles['classes']) {
                    try {
                        $.ID_1935223870.applyProperties(require('a4w').styles['classes'][clase]);
                    } catch (sete_err) {}
                }
            };
            setEstilo(ID_1935223870_estilo);

            $.ID_1289130430.setText('INICIAR SEGUIMIENTO');


            $.ID_1125701114.update({
                texto: L('x3839284819_traducir', 'Tip: Al iniciar seguimiento, le avisaremos al cliente que vas en camino')
            });
            $.ID_509734203.setText('Mantenga presionado para iniciar el seguimiento');

            require('vars')['iniciar_seguimiento'] = L('x4261170317', 'true');

            $.ID_1792932375.update({
                monito: false
            });
        } else {
            /** 
             * esta tarea esta actualmente siendo seguida, en este estado debe decir mantener para iniciar (default) 
             */
            require('vars')['iniciar_seguimiento'] = L('x734881840_traducir', 'false');
            /** 
             * Mostramos monito de caminando en la pantalla 
             */

            $.ID_1792932375.update({
                monito: true
            });
        }
        var moment = require('alloy/moment');
        var fecha_hoy = moment(new Date()).format('YYYY-MM-DD');
        if (tareas[0].fecha_tarea != fecha_hoy) {
            if (false) console.log('la fecha no es de hoy, se bloquea boton y se indica razon', {});
            /** 
             * La fecha no es de hoy, se bloquea boton y se indica razon 
             */
            var ID_1935223870_estilo = 'fondoplomo2';

            var setEstilo = function(clase) {
                if ('styles' in require('a4w') && clase in require('a4w').styles['classes']) {
                    try {
                        $.ID_1935223870.applyProperties(require('a4w').styles['classes'][clase]);
                    } catch (sete_err) {}
                }
            };
            setEstilo(ID_1935223870_estilo);

            $.ID_1289130430.setText('BLOQUEADA POR FECHA');


            $.ID_1125701114.update({
                texto: L('x1488972008_traducir', 'Tip: No puedes iniciar inspecciones para tareas que no son para el dia')
            });
            var moment = require('alloy/moment');
            var ID_805159815 = tareas[0].fecha_tarea;
            if (typeof ID_805159815 === 'string' || typeof ID_805159815 === 'number') {
                var fecha_mostrar = moment(ID_805159815, 'YYYY-MM-DD').format('DD-MM-YYYY');
            } else {
                var fecha_mostrar = moment(ID_805159815).format('DD-MM-YYYY');
            }
            $.ID_509734203.setText(String.format(L('x1019215239_traducir', 'Esta tarea se debe realizar el %1$s'), fecha_mostrar.toString()));

            require('vars')['bloquear_inspeccion'] = L('x4261170317', 'true');
        }
        var confirmarpush = JSON.parse(Ti.App.Properties.getString('confirmarpush'));
        if (false) console.log('detalle tarea: confirmar push dice', {
            "confirmar_push": confirmarpush
        });
        if (confirmarpush == true || confirmarpush == 'true') {
            require('vars')['bloquear_inspeccion'] = L('x4261170317', 'true');
            var ID_1935223870_estilo = 'fondoplomo2';

            var setEstilo = function(clase) {
                if ('styles' in require('a4w') && clase in require('a4w').styles['classes']) {
                    try {
                        $.ID_1935223870.applyProperties(require('a4w').styles['classes'][clase]);
                    } catch (sete_err) {}
                }
            };
            setEstilo(ID_1935223870_estilo);

            $.ID_1289130430.setText('BLOQUEADA POR CONFIRMACION');


            $.ID_1125701114.update({
                texto: L('x886429039_traducir', 'Tip: No puedes iniciar inspecciones sin confirmar tus tareas del dia')
            });
            $.ID_509734203.setText('Se debe confirmar primero las tareas');

        }
        if (_.isNumber(tareas[0].bono) && _.isNumber(0) && tareas[0].bono > 0) {

            $.ID_1792932375.update({
                bono: tareas[0].bono
            });
        }
    }
})();



_my_events['_cerrar_insp,ID_1805632763'] = function(evento) {
    if (!_.isUndefined(evento.pantalla)) {
        if (evento.pantalla == L('x2151250992_traducir', 'detalle')) {
            if (false) console.log('debug cerrando detalle tarea', {});

            var ID_962933245_trycatch = {
                error: function(e) {
                    if (false) console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
                }
            };
            try {
                ID_962933245_trycatch.error = function(evento) {
                    if (false) console.log('error cerrando detalle tarea', {});
                };
                $.ID_411389157.close();
            } catch (e) {
                ID_962933245_trycatch.error(e);
            }
        }
    } else {
        if (false) console.log('debug cerrando (todas) detalle tarea', {});

        var ID_1827570313_trycatch = {
            error: function(e) {
                if (false) console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
            }
        };
        try {
            ID_1827570313_trycatch.error = function(evento) {
                if (false) console.log('error cerrando detalle tarea', {});
            };
            $.ID_411389157.close();
        } catch (e) {
            ID_1827570313_trycatch.error(e);
        }
    }
};
Alloy.Events.on('_cerrar_insp', _my_events['_cerrar_insp,ID_1805632763']);

function Postlayout_ID_25820778(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var ID_2007442586_func = function() {
        require('vars')['var_abriendo'] = '';
    };
    var ID_2007442586 = setTimeout(ID_2007442586_func, 1000 * 0.2);

}
if (OS_IOS || OS_ANDROID) {
    $.ID_411389157.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}
//$.ID_411389157.open();