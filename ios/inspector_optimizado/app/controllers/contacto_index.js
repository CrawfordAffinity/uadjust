var _bind4section = {};
var _list_templates = {};

var _activity;
if (OS_ANDROID) {
    _activity = $.ID_1534880474.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_1534880474';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_1534880474_window.addEventListener('open', function(e) {
        abx.setStatusbarColor("#000000");
        abx.setBackgroundColor("white");
    });
}

function Click_ID_1923478739(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    $.ID_1534880474.close();

}

function Click_ID_1838269158(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
        /** 
         * Obtenemos los valores de los campos ingresados en la pantalla 
         */
        var correo;
        correo = $.ID_983090064.getValue();

        var correo_aux;
        correo_aux = $.ID_627845946.getValue();

        var telefono;
        telefono = $.ID_1726261643.getValue();

        var telefono_aux;
        telefono_aux = $.ID_1377483554.getValue();

        if ((_.isObject(correo) || _.isString(correo)) && _.isEmpty(correo)) {
            /** 
             * Revisamos que los campos no esten vacios o que no coincidan 
             */
            var ID_1862856768_opts = [L('x1518866076_traducir', 'Aceptar')];
            var ID_1862856768 = Ti.UI.createAlertDialog({
                title: L('x57652245_traducir', 'Alerta'),
                message: L('x3211270885_traducir', 'Debe ingresar un correo'),
                buttonNames: ID_1862856768_opts
            });
            ID_1862856768.addEventListener('click', function(e) {
                var suu = ID_1862856768_opts[e.index];
                suu = null;

                e.source.removeEventListener("click", arguments.callee);
            });
            ID_1862856768.show();
        } else if (!(_.isString(correo) && /\S+@\S+\.\S+/.test(correo))) {
            var ID_1857379989_opts = [L('x1518866076_traducir', 'Aceptar')];
            var ID_1857379989 = Ti.UI.createAlertDialog({
                title: L('x57652245_traducir', 'Alerta'),
                message: L('x3725915824_traducir', 'Debe ingresar un correo valido'),
                buttonNames: ID_1857379989_opts
            });
            ID_1857379989.addEventListener('click', function(e) {
                var suu = ID_1857379989_opts[e.index];
                suu = null;

                e.source.removeEventListener("click", arguments.callee);
            });
            ID_1857379989.show();
        } else if (correo != correo_aux) {
            var ID_676801709_opts = [L('x1518866076_traducir', 'Aceptar')];
            var ID_676801709 = Ti.UI.createAlertDialog({
                title: L('x57652245_traducir', 'Alerta'),
                message: L('x3338569552_traducir', 'Los correos no coinciden'),
                buttonNames: ID_676801709_opts
            });
            ID_676801709.addEventListener('click', function(e) {
                var suu = ID_676801709_opts[e.index];
                suu = null;

                e.source.removeEventListener("click", arguments.callee);
            });
            ID_676801709.show();
        } else if ((_.isObject(telefono) || _.isString(telefono)) && _.isEmpty(telefono)) {
            var ID_1146941147_opts = [L('x1518866076_traducir', 'Aceptar')];
            var ID_1146941147 = Ti.UI.createAlertDialog({
                title: L('x57652245_traducir', 'Alerta'),
                message: L('x674181321_traducir', 'Debe ingresar un teléfono'),
                buttonNames: ID_1146941147_opts
            });
            ID_1146941147.addEventListener('click', function(e) {
                var suu = ID_1146941147_opts[e.index];
                suu = null;

                e.source.removeEventListener("click", arguments.callee);
            });
            ID_1146941147.show();
        } else if (_.isNumber((telefono.length)) && _.isNumber(3) && (telefono.length) < 3) {
            var ID_870968368_opts = [L('x1518866076_traducir', 'Aceptar')];
            var ID_870968368 = Ti.UI.createAlertDialog({
                title: L('x57652245_traducir', 'Alerta'),
                message: L('x770664310_traducir', 'El telefono debe tener más digitos'),
                buttonNames: ID_870968368_opts
            });
            ID_870968368.addEventListener('click', function(e) {
                var suu = ID_870968368_opts[e.index];
                suu = null;

                e.source.removeEventListener("click", arguments.callee);
            });
            ID_870968368.show();
        } else if (telefono != telefono_aux) {
            var ID_444442991_opts = [L('x1518866076_traducir', 'Aceptar')];
            var ID_444442991 = Ti.UI.createAlertDialog({
                title: L('x57652245_traducir', 'Alerta'),
                message: L('x2467132173_traducir', 'Los teléfonos no coinciden'),
                buttonNames: ID_444442991_opts
            });
            ID_444442991.addEventListener('click', function(e) {
                var suu = ID_444442991_opts[e.index];
                suu = null;

                e.source.removeEventListener("click", arguments.callee);
            });
            ID_444442991.show();
        } else {
            var ID_1760765424_i = Alloy.createCollection('inspectores');
            var ID_1760765424_i_where = '';
            ID_1760765424_i.fetch();
            var inspector_list = require('helper').query2array(ID_1760765424_i);
            /** 
             * Guardamos correo y telefono en caso de que la consulta al servidor sea exitosa 
             */
            require('vars')['correo'] = correo;
            require('vars')['telefono'] = telefono;
            /** 
             * Recuperamos variable url de uadjust 
             */
            var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
            var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
            var ID_752769319 = {};

            ID_752769319.success = function(e) {
                var elemento = e,
                    valor = e;
                if (false) console.log('Servidor responde editarPerfilTipo3', {
                    "elemento": elemento
                });
                /** 
                 * no me hace sentido que esto se repita al responder el servidor, ya que ya fue escrito antes de llamar al servidor tambien. 
                 */
                var ID_684142441_i = Alloy.createCollection('inspectores');
                var ID_684142441_i_where = '';
                ID_684142441_i.fetch();
                var inspector_list = require('helper').query2array(ID_684142441_i);
                var correo = ('correo' in require('vars')) ? require('vars')['correo'] : '';
                var telefono = ('telefono' in require('vars')) ? require('vars')['telefono'] : '';
                if (false) console.log('hare modificacion', {
                    "datos": String.format(L('x1843559429_traducir', '%1$s y %2$s'), (correo) ? correo.toString() : '', (telefono) ? telefono.toString() : '')
                });
                var db = Ti.Database.open(ID_684142441_i.config.adapter.db_name);
                if (ID_684142441_i_where == '') {
                    var sql = 'UPDATE ' + ID_684142441_i.config.adapter.collection_name + ' SET correo=\'' + correo + '\'';
                } else {
                    var sql = 'UPDATE ' + ID_684142441_i.config.adapter.collection_name + ' SET correo=\'' + correo + '\' WHERE ' + ID_684142441_i_where;
                }
                db.execute(sql);
                if (ID_684142441_i_where == '') {
                    var sql = 'UPDATE ' + ID_684142441_i.config.adapter.collection_name + ' SET telefono=\'' + telefono + '\'';
                } else {
                    var sql = 'UPDATE ' + ID_684142441_i.config.adapter.collection_name + ' SET telefono=\'' + telefono + '\' WHERE ' + ID_684142441_i_where;
                }
                db.execute(sql);
                db.close();
                /** 
                 * Mostramos mensaje de exito de cambio 
                 */
                var ID_710102279_opts = [L('x2859518234_traducir', 'ACEPTAR')];
                var ID_710102279 = Ti.UI.createAlertDialog({
                    title: L('x2185084353_traducir', 'Atención'),
                    message: L('x134968077_traducir', 'Cambios realizados'),
                    buttonNames: ID_710102279_opts
                });
                ID_710102279.addEventListener('click', function(e) {
                    var abc = ID_710102279_opts[e.index];
                    $.ID_1534880474.close();
                    abc = null;

                    e.source.removeEventListener("click", arguments.callee);
                });
                ID_710102279.show();
                elemento = null, valor = null;
            };

            ID_752769319.error = function(e) {
                var elemento = e,
                    valor = e;
                if (false) console.log('Hubo un error', {
                    "elemento": elemento
                });
                elemento = null, valor = null;
            };
            require('helper').ajaxUnico('ID_752769319', '' + String.format(L('x2421050525', '%1$seditarPerfilTipo3'), url_server.toString()) + '', 'POST', {
                id_inspector: inspector.id_server,
                correo: correo,
                telefono: telefono
            }, 15000, ID_752769319);
        }
    } else {
        var ID_1554441340_opts = [L('x1518866076_traducir', 'Aceptar')];
        var ID_1554441340 = Ti.UI.createAlertDialog({
            title: L('x57652245_traducir', 'Alerta'),
            message: L('x3970757340_traducir', 'No se pueden efectuar los cambios, ya que no hay internet'),
            buttonNames: ID_1554441340_opts
        });
        ID_1554441340.addEventListener('click', function(e) {
            var suu = ID_1554441340_opts[e.index];
            suu = null;

            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1554441340.show();
    }
}

function Click_ID_1372066254(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    $.ID_983090064.blur();
    $.ID_627845946.blur();
    $.ID_1726261643.blur();
    $.ID_1377483554.blur();

}

(function() {
    var ID_1228108884_i = Alloy.createCollection('inspectores');
    var ID_1228108884_i_where = '';
    ID_1228108884_i.fetch();
    var inspector_list = require('helper').query2array(ID_1228108884_i);
    if (false) console.log('Mi inspector es', {
        "inspector": inspector_list[0]
    });
    if (inspector_list && inspector_list.length) {
        $.ID_983090064.setValue(inspector_list[0].correo);

        $.ID_1726261643.setValue(inspector_list[0].telefono);

    }
})();

function Postlayout_ID_1374908753(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var ID_1713255393_func = function() {
        /** 
         * Una vez ya cargada la tarea, dejamos la variable limpia para poder abrir otras pantallas 
         */
        require('vars')['var_abriendo'] = '';
    };
    var ID_1713255393 = setTimeout(ID_1713255393_func, 1000 * 0.2);

}
if (OS_IOS || OS_ANDROID) {
    $.ID_1534880474.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}
//$.ID_1534880474.open();