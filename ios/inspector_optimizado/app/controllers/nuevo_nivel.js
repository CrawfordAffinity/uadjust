var _bind4section={};
var _list_templates={};
var $nivel = $.nivel.toJSON();

$.ID_1407323156_window.setTitleAttributes({
color : 'WHITE'
}
);
var _activity; 
if (OS_ANDROID) { _activity = $.ID_1407323156.activity; var abx = require('com.alcoapps.actionbarextras'); }
var _my_events = {}, _out_vars = {}, $item = {}, args = arguments[0] || {}; if ('__args' in args && '__id' in args['__args']) args.__id=args['__args'].__id; if ('__modelo' in args && _.keys(args.__modelo).length>0 && 'item' in $) { $.item.set(args.__modelo); $item = $.item.toJSON(); }
var _var_scopekey = 'ID_1407323156';
require('vars')[_var_scopekey]={};
if (OS_ANDROID) {
   $.ID_1407323156_window.addEventListener('open', function(e) {
   abx.setStatusbarColor("#FFFFFF");
   abx.setBackgroundColor("#8ce5bd");
   });
}

function Click_ID_1742163591(e) {

e.cancelBubble=true;
var elemento=e.source;
/** 
* Limpiamos widget para liberar memoria ram 
*/
$.ID_1380638210.limpiar({});
$.ID_1689623114.limpiar({});
$.ID_1777835372.limpiar({});
$.ID_1324277593.limpiar({});
$.ID_1871422952.limpiar({});
$.ID_1345051066.limpiar({});
$.ID_1407323156.close();

}
function Click_ID_1551936741(e) {

e.cancelBubble=true;
var elemento=e.source;
var seltarea = ('seltarea' in require('vars'))?require('vars')['seltarea']:'';
var moment = require('alloy/moment');
var anoactual = moment(new Date()).format('YYYY');
if (_.isUndefined($nivel.nombre)) {
/** 
* Verificamos que los campos ingresados esten correctos 
*/
var ID_2089603283_opts=[L('x1518866076_traducir','Aceptar')];
var ID_2089603283 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x1734272038_traducir','Ingrese nombre del nivel'),
   buttonNames: ID_2089603283_opts
});
ID_2089603283.addEventListener('click', function(e) {
   var nulo=ID_2089603283_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_2089603283.show();
}
 else if ((_.isObject($nivel.nombre) ||_.isString($nivel.nombre)) &&  _.isEmpty($nivel.nombre)) {
var ID_1478130880_opts=[L('x1518866076_traducir','Aceptar')];
var ID_1478130880 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x1734272038_traducir','Ingrese nombre del nivel'),
   buttonNames: ID_1478130880_opts
});
ID_1478130880.addEventListener('click', function(e) {
   var nulo=ID_1478130880_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_1478130880.show();
} else if (_.isUndefined($nivel.piso)) {
var ID_1087460829_opts=[L('x1518866076_traducir','Aceptar')];
var ID_1087460829 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x2669747147_traducir','Ingrese Nº de piso'),
   buttonNames: ID_1087460829_opts
});
ID_1087460829.addEventListener('click', function(e) {
   var nulo=ID_1087460829_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_1087460829.show();
} else if ((_.isObject($nivel.piso) ||_.isString($nivel.piso)) &&  _.isEmpty($nivel.piso)) {
var ID_810717892_opts=[L('x1518866076_traducir','Aceptar')];
var ID_810717892 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x2669747147_traducir','Ingrese Nº de piso'),
   buttonNames: ID_810717892_opts
});
ID_810717892.addEventListener('click', function(e) {
   var nulo=ID_810717892_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_810717892.show();
} else if (_.isUndefined($nivel.ano)) {
var ID_363040379_opts=[L('x1518866076_traducir','Aceptar')];
var ID_363040379 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x853726930_traducir','Ingrese año de construcción del nivel'),
   buttonNames: ID_363040379_opts
});
ID_363040379.addEventListener('click', function(e) {
   var nulo=ID_363040379_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_363040379.show();
} else if ((_.isObject($nivel.ano) ||_.isString($nivel.ano)) &&  _.isEmpty($nivel.ano)) {
var ID_1145186859_opts=[L('x1518866076_traducir','Aceptar')];
var ID_1145186859 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x853726930_traducir','Ingrese año de construcción del nivel'),
   buttonNames: ID_1145186859_opts
});
ID_1145186859.addEventListener('click', function(e) {
   var nulo=ID_1145186859_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_1145186859.show();
} else if ($nivel.ano < (anoactual-100)==true||$nivel.ano < (anoactual-100)=='true') {
require('vars')[_var_scopekey]['todobien']=L('x734881840_traducir','false');
var ID_271729525_opts=[L('x1518866076_traducir','Aceptar')];
var ID_271729525 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x624168147_traducir','Tiene que tener máximo 100 años de antigüedad'),
   buttonNames: ID_271729525_opts
});
ID_271729525.addEventListener('click', function(e) {
   var nulo=ID_271729525_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_271729525.show();
} else if ($nivel.ano > anoactual==true||$nivel.ano > anoactual=='true') {
require('vars')[_var_scopekey]['todobien']=L('x734881840_traducir','false');
var ID_1795243898_opts=[L('x1518866076_traducir','Aceptar')];
var ID_1795243898 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x2118147629_traducir','El año tiene que ser menor al año actual'),
   buttonNames: ID_1795243898_opts
});
ID_1795243898.addEventListener('click', function(e) {
   var nulo=ID_1795243898_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_1795243898.show();
} else if (_.isUndefined($nivel.largo)) {
var ID_1178503984_opts=[L('x1518866076_traducir','Aceptar')];
var ID_1178503984 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x779559340_traducir','Ingrese largo del nivel'),
   buttonNames: ID_1178503984_opts
});
ID_1178503984.addEventListener('click', function(e) {
   var nulo=ID_1178503984_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_1178503984.show();
} else if ((_.isObject($nivel.largo) ||_.isString($nivel.largo)) &&  _.isEmpty($nivel.largo)) {
var ID_1950854283_opts=[L('x1518866076_traducir','Aceptar')];
var ID_1950854283 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x779559340_traducir','Ingrese largo del nivel'),
   buttonNames: ID_1950854283_opts
});
ID_1950854283.addEventListener('click', function(e) {
   var nulo=ID_1950854283_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_1950854283.show();
} else if (_.isUndefined($nivel.ancho)) {
var ID_173165960_opts=[L('x1518866076_traducir','Aceptar')];
var ID_173165960 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x2682783608_traducir','Ingrese ancho del nivel'),
   buttonNames: ID_173165960_opts
});
ID_173165960.addEventListener('click', function(e) {
   var nulo=ID_173165960_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_173165960.show();
} else if ((_.isObject($nivel.ancho) ||_.isString($nivel.ancho)) &&  _.isEmpty($nivel.ancho)) {
var ID_828251852_opts=[L('x1518866076_traducir','Aceptar')];
var ID_828251852 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x2682783608_traducir','Ingrese ancho del nivel'),
   buttonNames: ID_828251852_opts
});
ID_828251852.addEventListener('click', function(e) {
   var nulo=ID_828251852_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_828251852.show();
} else if (_.isUndefined($nivel.alto)) {
var ID_215888724_opts=[L('x1518866076_traducir','Aceptar')];
var ID_215888724 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x4120715490_traducir','Ingrese la altura del nivel'),
   buttonNames: ID_215888724_opts
});
ID_215888724.addEventListener('click', function(e) {
   var nulo=ID_215888724_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_215888724.show();
} else if ((_.isObject($nivel.alto) ||_.isString($nivel.alto)) &&  _.isEmpty($nivel.alto)) {
var ID_943597719_opts=[L('x1518866076_traducir','Aceptar')];
var ID_943597719 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x4120715490_traducir','Ingrese la altura del nivel'),
   buttonNames: ID_943597719_opts
});
ID_943597719.addEventListener('click', function(e) {
   var nulo=ID_943597719_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_943597719.show();
} else if (_.isUndefined($nivel.ids_estructuras_soportantes)) {
var ID_1356709795_opts=[L('x1518866076_traducir','Aceptar')];
var ID_1356709795 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x3045996758_traducir','Seleccione la estructura soportante del nivel'),
   buttonNames: ID_1356709795_opts
});
ID_1356709795.addEventListener('click', function(e) {
   var nulo=ID_1356709795_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_1356709795.show();
} else if ((_.isObject($nivel.ids_estructuras_soportantes) ||_.isString($nivel.ids_estructuras_soportantes)) &&  _.isEmpty($nivel.ids_estructuras_soportantes)) {
var ID_1973568452_opts=[L('x1518866076_traducir','Aceptar')];
var ID_1973568452 = Ti.UI.createAlertDialog({
   title: L('x3237162386_traducir','Atencion'),
   message: L('x3045996758_traducir','Seleccione la estructura soportante del nivel'),
   buttonNames: ID_1973568452_opts
});
ID_1973568452.addEventListener('click', function(e) {
   var nulo=ID_1973568452_opts[e.index];
nulo = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_1973568452.show();
} else {
/** 
* Guardamos los campos ingresados en el modelo 
*/
Alloy.Collections[$.nivel.config.adapter.collection_name].add($.nivel);
$.nivel.save();
Alloy.Collections[$.nivel.config.adapter.collection_name].fetch();
var ID_1189911720_func = function() {
/** 
* Limpiamos widget para liberar memoria ram 
*/
$.ID_1380638210.limpiar({});
$.ID_1689623114.limpiar({});
$.ID_1777835372.limpiar({});
$.ID_1324277593.limpiar({});
$.ID_1871422952.limpiar({});
$.ID_1345051066.limpiar({});
$.ID_1407323156.close();
};
var ID_1189911720 = setTimeout(ID_1189911720_func, 1000*0.1);
 anoactual  = null;
}
}
function Change_ID_1310018207(e) {

e.cancelBubble=true;
var elemento=e.value;
var source=e.source;
$.nivel.set({
nombre : elemento
}
);
if ('nivel' in $) $nivel=$.nivel.toJSON();
elemento=null, source=null;

}
function Change_ID_764031777(e) {

e.cancelBubble=true;
var elemento=e.value;
var source=e.source;
$.nivel.set({
piso : elemento
}
);
if ('nivel' in $) $nivel=$.nivel.toJSON();
elemento=null, source=null;

}
function Change_ID_1815762137(e) {

e.cancelBubble=true;
var elemento=e.value;
var source=e.source;
$.nivel.set({
ano : elemento
}
);
if ('nivel' in $) $nivel=$.nivel.toJSON();
elemento=null, source=null;

}
function Change_ID_1807338906(e) {

e.cancelBubble=true;
var elemento=e.value;
var source=e.source;
var ancho;
ancho = $.ID_1594240221.getValue();

if ((_.isObject(ancho) || (_.isString(ancho)) &&  !_.isEmpty(ancho)) || _.isNumber(ancho) || _.isBoolean(ancho)) {
if ((_.isObject(elemento) || (_.isString(elemento)) &&  !_.isEmpty(elemento)) || _.isNumber(elemento) || _.isBoolean(elemento)) {
/** 
* nos aseguramos que ancho y valor de campo sean decimales con puntos (flotante), y no comas (espa&#241;ol) 
*/
 var nuevo=parseFloat(ancho.split(',').join('.'))*parseFloat(elemento.split(',').join('.'));
$.nivel.set({
superficie : nuevo
}
);
if ('nivel' in $) $nivel=$.nivel.toJSON();
}
}
$.nivel.set({
largo : elemento
}
);
if ('nivel' in $) $nivel=$.nivel.toJSON();
elemento=null, source=null;

}
function Change_ID_1367059026(e) {

e.cancelBubble=true;
var elemento=e.value;
var source=e.source;
var largo;
largo = $.ID_1321071750.getValue();

if ((_.isObject(largo) || (_.isString(largo)) &&  !_.isEmpty(largo)) || _.isNumber(largo) || _.isBoolean(largo)) {
if ((_.isObject(elemento) || (_.isString(elemento)) &&  !_.isEmpty(elemento)) || _.isNumber(elemento) || _.isBoolean(elemento)) {
/** 
* nos aseguramos que largo y valor de campo sean decimales con puntos (flotante), y no comas (espa&#241;ol) 
*/
 var nuevo=parseFloat(largo.split(',').join('.'))*parseFloat(elemento.split(',').join('.'));
$.nivel.set({
superficie : nuevo
}
);
if ('nivel' in $) $nivel=$.nivel.toJSON();
}
}
$.nivel.set({
ancho : elemento
}
);
if ('nivel' in $) $nivel=$.nivel.toJSON();
elemento=null, source=null;

}
function Change_ID_1603734767(e) {

e.cancelBubble=true;
var elemento=e.value;
var source=e.source;
$.nivel.set({
alto : elemento
}
);
if ('nivel' in $) $nivel=$.nivel.toJSON();
elemento=null, source=null;

}

$.ID_1380638210.init({
titulo : L('x1975271086_traducir','Estructura Soportante'),
cargando : L('x1740321226_traducir','cargando ..'),
__id : 'ALL1380638210',
oncerrar : Cerrar_ID_1033128288,
hint : L('x2898603391_traducir','Seleccione estructura'),
color : 'verde',
subtitulo : L('x4011106049_traducir','Indique los tipos'),
top : 5,
onclick : Click_ID_1545325717,
onafterinit : Afterinit_ID_2002298610
}
);

function Click_ID_1545325717(e) {

var evento=e;

}
function Cerrar_ID_1033128288(e) {

var evento=e;
$.ID_1380638210.update({});
$.nivel.set({
ids_estructuras_soportantes : evento.valores
}
);
if ('nivel' in $) $nivel=$.nivel.toJSON();

}
function Afterinit_ID_2002298610(e) {

var evento=e;
var ID_848598486_func = function() {
var seltarea = ('seltarea' in require('vars'))?require('vars')['seltarea']:'';
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
var pais = ('pais' in require('vars'))?require('vars')['pais']:'';
var ID_1515066089_i=Alloy.createCollection('estructura_soportante');
var ID_1515066089_i_where='pais_texto=\''+pais[0].nombre+'\'';
ID_1515066089_i.fetch({ query: 'SELECT * FROM estructura_soportante WHERE pais_texto=\''+pais[0].nombre+'\'' });
var estructura=require('helper').query2array(ID_1515066089_i);
var datos=[];
_.each(estructura, function(fila, pos) {
  var new_row={};
  _.each(fila, function(x, llave) {
    var newkey='';
    if (llave=='nombre') newkey='label';
    if (llave=='id_segured') newkey='valor';
    if (newkey!='') new_row[newkey] = fila[llave];
  });
  new_row['estado'] = 0;
  datos.push(new_row);
});
$.ID_1380638210.update({data : datos});
}
 else {
var ID_1610487413_i=Alloy.Collections.estructura_soportante;
var sql = "DELETE FROM " + ID_1610487413_i.config.adapter.collection_name;
var db = Ti.Database.open(ID_1610487413_i.config.adapter.db_name);
db.execute(sql);
db.close();
sql=null;
db=null;
ID_1610487413_i.trigger('delete');
var item_index = 0;
_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
item_index += 1;
var ID_1632483632_m=Alloy.Collections.estructura_soportante;
var ID_1632483632_fila = Alloy.createModel('estructura_soportante', {
nombre : String.format(L('x1088195980_traducir','Estructura%1$s'), item.toString()),
id_server : String.format(L('x1290178835_traducir','0%1$s'), item.toString()),
pais_texto : 'Chile',
id_segured : String.format(L('x1125471167_traducir','10%1$s'), item.toString()),
pais : 1
}
 );
ID_1632483632_m.add(ID_1632483632_fila);
ID_1632483632_fila.save();
});
var ID_1089208465_i=Alloy.createCollection('estructura_soportante');
ID_1089208465_i.fetch();
var ID_1089208465_src=require('helper').query2array(ID_1089208465_i);
var datos=[];
_.each(ID_1089208465_src, function(fila, pos) {
  var new_row={};
  _.each(fila, function(x, llave) {
    var newkey='';
    if (llave=='nombre') newkey='label';
    if (llave=='id_segured') newkey='valor';
    if (newkey!='') new_row[newkey] = fila[llave];
  });
  new_row['estado'] = 0;
  datos.push(new_row);
});
$.ID_1380638210.update({data : datos});
}
seltarea  = null;
pais  = null;
estructura  = null;
datos  = null
};
var ID_848598486 = setTimeout(ID_848598486_func, 1000*0.2);

}

$.ID_1689623114.init({
titulo : L('x1219835481_traducir','Muros / Tabiques'),
cargando : L('x1740321226_traducir','cargando ..'),
__id : 'ALL1689623114',
oncerrar : Cerrar_ID_1009544840,
hint : L('x2879998099_traducir','Seleccione muros'),
color : 'verde',
subtitulo : L('x4011106049_traducir','Indique los tipos'),
top : 5,
onclick : Click_ID_1122567961,
onafterinit : Afterinit_ID_1306974316
}
);

function Click_ID_1122567961(e) {

var evento=e;

}
function Cerrar_ID_1009544840(e) {

var evento=e;
$.ID_1689623114.update({});
$.nivel.set({
ids_muros : evento.valores
}
);
if ('nivel' in $) $nivel=$.nivel.toJSON();

}
function Afterinit_ID_1306974316(e) {

var evento=e;
var ID_761628958_func = function() {
var seltarea = ('seltarea' in require('vars'))?require('vars')['seltarea']:'';
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
var pais = ('pais' in require('vars'))?require('vars')['pais']:'';
var ID_1691867162_i=Alloy.createCollection('muros_tabiques');
var ID_1691867162_i_where='pais_texto=\''+pais[0].nombre+'\'';
ID_1691867162_i.fetch({ query: 'SELECT * FROM muros_tabiques WHERE pais_texto=\''+pais[0].nombre+'\'' });
var tabiques=require('helper').query2array(ID_1691867162_i);
var datos=[];
_.each(tabiques, function(fila, pos) {
  var new_row={};
  _.each(fila, function(x, llave) {
    var newkey='';
    if (llave=='nombre') newkey='label';
    if (llave=='id_segured') newkey='valor';
    if (newkey!='') new_row[newkey] = fila[llave];
  });
  new_row['estado'] = 0;
  datos.push(new_row);
});
$.ID_1689623114.update({data : datos});
}
 else {
var ID_1577217385_i=Alloy.Collections.muros_tabiques;
var sql = "DELETE FROM " + ID_1577217385_i.config.adapter.collection_name;
var db = Ti.Database.open(ID_1577217385_i.config.adapter.db_name);
db.execute(sql);
db.close();
sql=null;
db=null;
ID_1577217385_i.trigger('delete');
var item_index = 0;
_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
item_index += 1;
var ID_1851548626_m=Alloy.Collections.muros_tabiques;
var ID_1851548626_fila = Alloy.createModel('muros_tabiques', {
nombre : String.format(L('x3565664878_traducir','Muros%1$s'), item.toString()),
id_server : String.format(L('x1290178835_traducir','0%1$s'), item.toString()),
pais_texto : 'Chile',
id_segured : String.format(L('x1125471167_traducir','10%1$s'), item.toString()),
pais : 1
}
 );
ID_1851548626_m.add(ID_1851548626_fila);
ID_1851548626_fila.save();
});
var ID_1091439112_i=Alloy.createCollection('muros_tabiques');
ID_1091439112_i.fetch();
var ID_1091439112_src=require('helper').query2array(ID_1091439112_i);
var datos=[];
_.each(ID_1091439112_src, function(fila, pos) {
  var new_row={};
  _.each(fila, function(x, llave) {
    var newkey='';
    if (llave=='nombre') newkey='label';
    if (llave=='id_segured') newkey='valor';
    if (newkey!='') new_row[newkey] = fila[llave];
  });
  new_row['estado'] = 0;
  datos.push(new_row);
});
$.ID_1689623114.update({data : datos});
}
seltarea  = null;
pais  = null;
tabiques  = null;
datos  = null
};
var ID_761628958 = setTimeout(ID_761628958_func, 1000*0.2);

}

$.ID_1777835372.init({
titulo : L('x3327059844_traducir','Entrepisos'),
cargando : L('x1740321226_traducir','cargando ..'),
__id : 'ALL1777835372',
oncerrar : Cerrar_ID_1698789265,
hint : L('x2146928948_traducir','Seleccione entrepisos'),
color : 'verde',
subtitulo : L('x4011106049_traducir','Indique los tipos'),
top : 5,
onclick : Click_ID_1843312914,
onafterinit : Afterinit_ID_1728051577
}
);

function Click_ID_1843312914(e) {

var evento=e;

}
function Cerrar_ID_1698789265(e) {

var evento=e;
$.ID_1777835372.update({});
$.nivel.set({
ids_entrepisos : evento.valores
}
);
if ('nivel' in $) $nivel=$.nivel.toJSON();

}
function Afterinit_ID_1728051577(e) {

var evento=e;
var ID_1534965449_func = function() {
var seltarea = ('seltarea' in require('vars'))?require('vars')['seltarea']:'';
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
var pais = ('pais' in require('vars'))?require('vars')['pais']:'';
var ID_303315958_i=Alloy.createCollection('entrepisos');
var ID_303315958_i_where='pais_texto=\''+pais[0].nombre+'\'';
ID_303315958_i.fetch({ query: 'SELECT * FROM entrepisos WHERE pais_texto=\''+pais[0].nombre+'\'' });
var entrepisos=require('helper').query2array(ID_303315958_i);
var datos=[];
_.each(entrepisos, function(fila, pos) {
  var new_row={};
  _.each(fila, function(x, llave) {
    var newkey='';
    if (llave=='nombre') newkey='label';
    if (llave=='id_segured') newkey='valor';
    if (newkey!='') new_row[newkey] = fila[llave];
  });
  new_row['estado'] = 0;
  datos.push(new_row);
});
$.ID_1777835372.update({data : datos});
}
 else {
var ID_1573705793_i=Alloy.Collections.entrepisos;
var sql = "DELETE FROM " + ID_1573705793_i.config.adapter.collection_name;
var db = Ti.Database.open(ID_1573705793_i.config.adapter.db_name);
db.execute(sql);
db.close();
sql=null;
db=null;
ID_1573705793_i.trigger('delete');
var item_index = 0;
_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
item_index += 1;
var ID_1634034273_m=Alloy.Collections.entrepisos;
var ID_1634034273_fila = Alloy.createModel('entrepisos', {
nombre : String.format(L('x2266735154_traducir','Entrepiso%1$s'), item.toString()),
id_server : String.format(L('x1290178835_traducir','0%1$s'), item.toString()),
pais_texto : 'Chile',
id_segured : String.format(L('x1125471167_traducir','10%1$s'), item.toString()),
pais : 1
}
 );
ID_1634034273_m.add(ID_1634034273_fila);
ID_1634034273_fila.save();
});
var ID_2024956307_i=Alloy.createCollection('entrepisos');
ID_2024956307_i.fetch();
var ID_2024956307_src=require('helper').query2array(ID_2024956307_i);
var datos=[];
_.each(ID_2024956307_src, function(fila, pos) {
  var new_row={};
  _.each(fila, function(x, llave) {
    var newkey='';
    if (llave=='nombre') newkey='label';
    if (llave=='id_segured') newkey='valor';
    if (newkey!='') new_row[newkey] = fila[llave];
  });
  new_row['estado'] = 0;
  datos.push(new_row);
});
$.ID_1777835372.update({data : datos});
}
seltarea  = null;
pais  = null;
datos  = null;
entrepisos  = null
};
var ID_1534965449 = setTimeout(ID_1534965449_func, 1000*0.2);

}

$.ID_1324277593.init({
titulo : L('x591862035_traducir','Pavimentos'),
cargando : L('x1740321226_traducir','cargando ..'),
__id : 'ALL1324277593',
oncerrar : Cerrar_ID_1598013809,
hint : L('x2600368035_traducir','Seleccione pavimentos'),
color : 'verde',
subtitulo : L('x4011106049_traducir','Indique los tipos'),
top : 5,
onclick : Click_ID_1420524423,
onafterinit : Afterinit_ID_1002512749
}
);

function Click_ID_1420524423(e) {

var evento=e;

}
function Cerrar_ID_1598013809(e) {

var evento=e;
$.ID_1324277593.update({});
$.nivel.set({
ids_pavimentos : evento.valores
}
);
if ('nivel' in $) $nivel=$.nivel.toJSON();

}
function Afterinit_ID_1002512749(e) {

var evento=e;
var ID_60762849_func = function() {
var seltarea = ('seltarea' in require('vars'))?require('vars')['seltarea']:'';
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
var pais = ('pais' in require('vars'))?require('vars')['pais']:'';
var ID_920912481_i=Alloy.createCollection('pavimento');
var ID_920912481_i_where='pais_texto=\''+pais[0].nombre+'\'';
ID_920912481_i.fetch({ query: 'SELECT * FROM pavimento WHERE pais_texto=\''+pais[0].nombre+'\'' });
var pavimentos=require('helper').query2array(ID_920912481_i);
var datos=[];
_.each(pavimentos, function(fila, pos) {
  var new_row={};
  _.each(fila, function(x, llave) {
    var newkey='';
    if (llave=='nombre') newkey='label';
    if (llave=='id_segured') newkey='valor';
    if (newkey!='') new_row[newkey] = fila[llave];
  });
  new_row['estado'] = 0;
  datos.push(new_row);
});
$.ID_1324277593.update({data : datos});
}
 else {
var ID_1940581846_i=Alloy.Collections.pavimento;
var sql = "DELETE FROM " + ID_1940581846_i.config.adapter.collection_name;
var db = Ti.Database.open(ID_1940581846_i.config.adapter.db_name);
db.execute(sql);
db.close();
sql=null;
db=null;
ID_1940581846_i.trigger('delete');
var item_index = 0;
_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
item_index += 1;
var ID_1873334587_m=Alloy.Collections.pavimento;
var ID_1873334587_fila = Alloy.createModel('pavimento', {
nombre : String.format(L('x427067467_traducir','Pavimento%1$s'), item.toString()),
id_server : String.format(L('x1290178835_traducir','0%1$s'), item.toString()),
pais_texto : 'Chile',
id_segured : String.format(L('x1125471167_traducir','10%1$s'), item.toString()),
pais : 1
}
 );
ID_1873334587_m.add(ID_1873334587_fila);
ID_1873334587_fila.save();
});
var ID_1797203852_i=Alloy.createCollection('pavimento');
ID_1797203852_i.fetch();
var ID_1797203852_src=require('helper').query2array(ID_1797203852_i);
var datos=[];
_.each(ID_1797203852_src, function(fila, pos) {
  var new_row={};
  _.each(fila, function(x, llave) {
    var newkey='';
    if (llave=='nombre') newkey='label';
    if (llave=='id_segured') newkey='valor';
    if (newkey!='') new_row[newkey] = fila[llave];
  });
  new_row['estado'] = 0;
  datos.push(new_row);
});
$.ID_1324277593.update({data : datos});
}
seltarea  = null;
pais  = null;
datos  = null;
pavimentos  = null
};
var ID_60762849 = setTimeout(ID_60762849_func, 1000*0.2);

}

$.ID_1871422952.init({
titulo : L('x1866523485_traducir','Estruct. cubierta'),
cargando : L('x1740321226_traducir','cargando ..'),
__id : 'ALL1871422952',
oncerrar : Cerrar_ID_1600050963,
hint : L('x2460890829_traducir','Seleccione e.cubiertas'),
color : 'verde',
subtitulo : L('x4011106049_traducir','Indique los tipos'),
top : 5,
onclick : Click_ID_1255218457,
onafterinit : Afterinit_ID_1485399435
}
);

function Click_ID_1255218457(e) {

var evento=e;

}
function Cerrar_ID_1600050963(e) {

var evento=e;
$.ID_1871422952.update({});
$.nivel.set({
ids_estructura_cubiera : evento.valores
}
);
if ('nivel' in $) $nivel=$.nivel.toJSON();

}
function Afterinit_ID_1485399435(e) {

var evento=e;
var ID_602043695_func = function() {
var seltarea = ('seltarea' in require('vars'))?require('vars')['seltarea']:'';
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
var pais = ('pais' in require('vars'))?require('vars')['pais']:'';
var ID_1623473944_i=Alloy.createCollection('estructura_cubierta');
var ID_1623473944_i_where='pais_texto=\''+pais[0].nombre+'\'';
ID_1623473944_i.fetch({ query: 'SELECT * FROM estructura_cubierta WHERE pais_texto=\''+pais[0].nombre+'\'' });
var ecubiertas=require('helper').query2array(ID_1623473944_i);
var datos=[];
_.each(ecubiertas, function(fila, pos) {
  var new_row={};
  _.each(fila, function(x, llave) {
    var newkey='';
    if (llave=='nombre') newkey='label';
    if (llave=='id_segured') newkey='valor';
    if (newkey!='') new_row[newkey] = fila[llave];
  });
  new_row['estado'] = 0;
  datos.push(new_row);
});
$.ID_1871422952.update({data : datos});
}
 else {
var ID_1763619695_i=Alloy.Collections.estructura_cubierta;
var sql = "DELETE FROM " + ID_1763619695_i.config.adapter.collection_name;
var db = Ti.Database.open(ID_1763619695_i.config.adapter.db_name);
db.execute(sql);
db.close();
sql=null;
db=null;
ID_1763619695_i.trigger('delete');
var item_index = 0;
_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
item_index += 1;
var ID_2066264646_m=Alloy.Collections.estructura_cubierta;
var ID_2066264646_fila = Alloy.createModel('estructura_cubierta', {
nombre : String.format(L('x1686539481_traducir','Estru Cubierta %1$s'), item.toString()),
id_server : String.format(L('x1290178835_traducir','0%1$s'), item.toString()),
pais_texto : 'Chile',
id_segured : String.format(L('x1125471167_traducir','10%1$s'), item.toString()),
pais : 1
}
 );
ID_2066264646_m.add(ID_2066264646_fila);
ID_2066264646_fila.save();
});
var ID_1653637134_i=Alloy.createCollection('estructura_cubierta');
ID_1653637134_i.fetch();
var ID_1653637134_src=require('helper').query2array(ID_1653637134_i);
var datos=[];
_.each(ID_1653637134_src, function(fila, pos) {
  var new_row={};
  _.each(fila, function(x, llave) {
    var newkey='';
    if (llave=='nombre') newkey='label';
    if (llave=='id_segured') newkey='valor';
    if (newkey!='') new_row[newkey] = fila[llave];
  });
  new_row['estado'] = 0;
  datos.push(new_row);
});
$.ID_1871422952.update({data : datos});
}
seltarea  = null;
pais  = null;
datos  = null;
ecubiertas  = null
};
var ID_602043695 = setTimeout(ID_602043695_func, 1000*0.2);

}

$.ID_1345051066.init({
titulo : L('x2266302645_traducir','Cubierta'),
cargando : L('x1740321226_traducir','cargando ..'),
__id : 'ALL1345051066',
oncerrar : Cerrar_ID_1417504323,
hint : L('x2134385782_traducir','Seleccione cubiertas'),
color : 'verde',
subtitulo : L('x4011106049_traducir','Indique los tipos'),
top : 5,
onclick : Click_ID_1846322759,
onafterinit : Afterinit_ID_1900675506
}
);

function Click_ID_1846322759(e) {

var evento=e;

}
function Cerrar_ID_1417504323(e) {

var evento=e;
$.ID_1345051066.update({});
$.nivel.set({
ids_cubierta : evento.valores
}
);
if ('nivel' in $) $nivel=$.nivel.toJSON();

}
function Afterinit_ID_1900675506(e) {

var evento=e;
var ID_1301596767_func = function() {
var seltarea = ('seltarea' in require('vars'))?require('vars')['seltarea']:'';
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
var pais = ('pais' in require('vars'))?require('vars')['pais']:'';
var ID_793433432_i=Alloy.createCollection('cubierta');
var ID_793433432_i_where='pais_texto=\''+pais[0].nombre+'\'';
ID_793433432_i.fetch({ query: 'SELECT * FROM cubierta WHERE pais_texto=\''+pais[0].nombre+'\'' });
var cubiertas=require('helper').query2array(ID_793433432_i);
var datos=[];
_.each(cubiertas, function(fila, pos) {
  var new_row={};
  _.each(fila, function(x, llave) {
    var newkey='';
    if (llave=='nombre') newkey='label';
    if (llave=='id_segured') newkey='valor';
    if (newkey!='') new_row[newkey] = fila[llave];
  });
  new_row['estado'] = 0;
  datos.push(new_row);
});
$.ID_1345051066.update({data : datos});
}
 else {
var ID_1248704900_i=Alloy.Collections.cubierta;
var sql = "DELETE FROM " + ID_1248704900_i.config.adapter.collection_name;
var db = Ti.Database.open(ID_1248704900_i.config.adapter.db_name);
db.execute(sql);
db.close();
sql=null;
db=null;
ID_1248704900_i.trigger('delete');
var item_index = 0;
_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
item_index += 1;
var ID_1203606113_m=Alloy.Collections.cubierta;
var ID_1203606113_fila = Alloy.createModel('cubierta', {
nombre : String.format(L('x2246230604_traducir','Cubierta %1$s'), item.toString()),
id_server : String.format(L('x1290178835_traducir','0%1$s'), item.toString()),
pais_texto : 'Chile',
id_segured : String.format(L('x1125471167_traducir','10%1$s'), item.toString()),
pais : 1
}
 );
ID_1203606113_m.add(ID_1203606113_fila);
ID_1203606113_fila.save();
});
var ID_1037644490_i=Alloy.createCollection('cubierta');
ID_1037644490_i.fetch();
var ID_1037644490_src=require('helper').query2array(ID_1037644490_i);
var datos=[];
_.each(ID_1037644490_src, function(fila, pos) {
  var new_row={};
  _.each(fila, function(x, llave) {
    var newkey='';
    if (llave=='nombre') newkey='label';
    if (llave=='id_segured') newkey='valor';
    if (newkey!='') new_row[newkey] = fila[llave];
  });
  new_row['estado'] = 0;
  datos.push(new_row);
});
$.ID_1345051066.update({data : datos});
}};
var ID_1301596767 = setTimeout(ID_1301596767_func, 1000*0.2);

}
function Click_ID_70431695(e) {

e.cancelBubble=true;
var elemento=e.source;
$.ID_1234171100.blur();
$.ID_1947669263.blur();
$.ID_1321071750.blur();
$.ID_1594240221.blur();
$.ID_1944160595.blur();

}

(function() {
if (!_.isUndefined(args._fila)) {
/** 
* heredamos datos previos de nivel en modelo nuevo con binding de esta pantalla 
*/
var ID_1232876665_i=Alloy.createCollection('insp_niveles');
var ID_1232876665_i_where='id=\''+args._fila.id+'\'';
ID_1232876665_i.fetch({ query: 'SELECT * FROM insp_niveles WHERE id=\''+args._fila.id+'\'' });
var insp_n=require('helper').query2array(ID_1232876665_i);
$.nivel.set({
ids_entrepisos : insp_n[0].ids_entrepisos,
id_inspeccion : insp_n[0].id_inspeccion,
nombre : insp_n[0].nombre,
superficie : insp_n[0].superficie,
largo : insp_n[0].largo,
ids_pavimentos : insp_n[0].ids_pavimentos,
alto : insp_n[0].alto,
ano : insp_n[0].ano,
ids_muros : insp_n[0].ids_muros,
ancho : insp_n[0].ancho,
ids_estructura_cubiera : insp_n[0].ids_estructura_cubiera,
piso : insp_n[0].piso,
ids_cubierta : insp_n[0].ids_cubierta,
ids_estructuras_soportantes : insp_n[0].ids_estructuras_soportantes
}
);
if ('nivel' in $) $nivel=$.nivel.toJSON();
require('vars')['nivel_editado']=insp_n[0];
 insp_n = null;
var ID_1407323156_titulo = 'EDITAR NIVEL';

									var setTitle2 = function(valor) {
										if (OS_ANDROID) {
											abx.title = valor;
										} else {
											$.ID_1407323156_window.setTitle(valor);
										}
									};
									var getTitle2 = function() {
										if (OS_ANDROID) {
											return abx.title;
										} else {
											return $.ID_1407323156_window.getTitle();
										}
									};
									setTitle2(ID_1407323156_titulo);

}
var seltarea = ('seltarea' in require('vars'))?require('vars')['seltarea']:'';
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
$.nivel.set({
id_inspeccion : seltarea.id_server
}
);
if ('nivel' in $) $nivel=$.nivel.toJSON();
}
})();

function Postlayout_ID_1657536879(e) {

e.cancelBubble=true;
var elemento=e.source;
require('vars')['var_abriendo']='';

}
if (OS_IOS || OS_ANDROID) {
$.ID_1407323156.addEventListener('close',function(){
   $.destroy(); // cleanup bindings
   $.off(); //remove backbone events of this controller
   var _ev_tmp = null, _ev_rem = null;
   if (_my_events) {
      for(_ev_tmp in _my_events) { 
   		try {
   		    if (_ev_tmp.indexOf('_web')!=-1) {
   			   Ti.App.removeEventListener(_ev_tmp.split(',')[0],_my_events[_ev_tmp]);
   		    } else {
   			   Alloy.Events.off(_ev_tmp.split(',')[0],_my_events[_ev_tmp]);
   		    }
   		} catch(err10) {
   		}
      }
      _my_events = null;
      //delete _my_events;
   }
   if (_out_vars) {
      for(_ev_tmp in _out_vars) { 
         for (_ev_rem in _out_vars[_ev_tmp]._remove) {
            try {
 eval(_out_vars[_ev_tmp]._remove[_ev_rem]); 
 } catch(_errt) {}
         }
         _out_vars[_ev_tmp] = null;
      }
      _ev_tmp = null;
      //delete _out_vars;
   }
});
}