var _bind4section = {};
var _list_templates = {};

var _activity;
if (OS_ANDROID) {
    _activity = $.ID_334050739.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_334050739';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_334050739.addEventListener('open', function(e) {
        abx.setStatusbarColor("#000000");
        abx.setBackgroundColor("white");
    });
}
$.ID_334050739.orientationModes = [Titanium.UI.PORTRAIT];

function Click_ID_1547618305(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    $.ID_334050739.close();

}

$.ID_1420945673.init({
    __id: 'ALL1420945673',
    aceptar: L('x1518866076_traducir', 'Aceptar'),
    cancelar: L('x2353348866_traducir', 'Cancelar'),
    onaceptar: Aceptar_ID_1000528260,
    oncancelar: Cancelar_ID_1617306473
});

function Aceptar_ID_1000528260(e) {

    var evento = e;
    var resp = null;
    if ('formatear_fecha' in require('funciones')) {
        resp = require('funciones').formatear_fecha({
            'fecha': evento.valor,
            'formato': L('x2520471236_traducir', 'DD-MM-YYYY')
        });
    } else {
        try {
            resp = f_formatear_fecha({
                'fecha': evento.valor,
                'formato': L('x2520471236_traducir', 'DD-MM-YYYY')
            });
        } catch (ee) {}
    }
    $.ID_2094008726.setText(resp);

    var ID_2094008726_estilo = 'estilo12';

    var _tmp_a4w = require('a4w');
    if ((typeof ID_2094008726_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (ID_2094008726_estilo in _tmp_a4w.styles['classes'])) {
        try {
            ID_2094008726_estilo = _tmp_a4w.styles['classes'][ID_2094008726_estilo];
        } catch (st_val_err) {}
    }
    _tmp_a4w = null;
    $.ID_2094008726.applyProperties(ID_2094008726_estilo);

    require('vars')['fecha_nacimiento'] = evento.valor;

}

function Cancelar_ID_1617306473(e) {

    var evento = e;
    if (false) console.log('selector de fecha cancelado', {});

}

$.ID_1146042904.init({
    titulo: L('x118417065_traducir', 'PARTE 2: Datos personales'),
    __id: 'ALL1146042904',
    avance: L('x84428056_traducir', '2/6'),
    onclick: Click_ID_289151780
});

function Click_ID_289151780(e) {

    var evento = e;
    $.ID_154930557.blur();
    $.ID_1663369926.blur();
    $.ID_1731330558.blur();
    $.ID_1719306939.blur();

}

function Focus_ID_38810559(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;

    $.ID_1420945673.cerrar({});
    elemento = null, source = null;

}

function Focus_ID_1159714816(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;

    $.ID_1420945673.cerrar({});
    elemento = null, source = null;

}

function Focus_ID_505244999(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;

    $.ID_1420945673.cerrar({});
    elemento = null, source = null;

}

function Focus_ID_1827264810(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;

    $.ID_1420945673.cerrar({});
    elemento = null, source = null;

}

function Click_ID_1846939075(e) {

    e.cancelBubble = true;
    var elemento = e.source;

    $.ID_1669511822.fireEvent('click');

}

function Click_ID_1970655281(e) {

    e.cancelBubble = true;
    var elemento = e.source;

    $.ID_1669511822.fireEvent('click');

}

function Click_ID_1782048328(e) {

    e.cancelBubble = true;
    var elemento = e.source;

    $.ID_1420945673.abrir({});
    $.ID_154930557.blur();
    $.ID_1663369926.blur();
    $.ID_1731330558.blur();
    $.ID_1719306939.blur();

}

$.ID_1860370347.init({
    titulo: L('x1524107289_traducir', 'CONTINUAR'),
    __id: 'ALL1860370347',
    onclick: Click_ID_275036354
});

function Click_ID_275036354(e) {

    var evento = e;
    /** 
     * Obtenemos el ano actual y guardamos en variable 
     */
    var moment = require('alloy/moment');
    var hoy_ano = moment(new Date()).format('YYYY');
    /** 
     * Recuperamos variable fecha_nacimiento para obtener el ano 
     */
    var fecha_nacimiento = ('fecha_nacimiento' in require('vars')) ? require('vars')['fecha_nacimiento'] : '';
    var moment = require('alloy/moment');
    var ID_1797744969 = fecha_nacimiento;
    var sel_ano = moment(ID_1797744969).format('YYYY');
    var fecha_nacimiento = ('fecha_nacimiento' in require('vars')) ? require('vars')['fecha_nacimiento'] : '';
    /** 
     * Obtenemos los datos ingresados en los campos 
     */
    var nombre;
    nombre = $.ID_154930557.getValue();

    var apellido_paterno;
    apellido_paterno = $.ID_1663369926.getValue();

    var apellido_materno;
    apellido_materno = $.ID_1731330558.getValue();

    var codigo_verificador;
    codigo_verificador = $.ID_1719306939.getValue();

    if ((_.isObject(nombre) || _.isString(nombre)) && _.isEmpty(nombre)) {
        /** 
         * Levantamos mensaje de error en caso de que hayan campos vacios o el inspector tenga menos de 18 anos 
         */
        var ID_7089201_opts = [L('x1518866076_traducir', 'Aceptar')];
        var ID_7089201 = Ti.UI.createAlertDialog({
            title: L('x2185084353_traducir', 'Atención'),
            message: L('x314662607_traducir', 'Ingrese nombre'),
            buttonNames: ID_7089201_opts
        });
        ID_7089201.addEventListener('click', function(e) {
            var suu = ID_7089201_opts[e.index];
            suu = null;

            e.source.removeEventListener("click", arguments.callee);
        });
        ID_7089201.show();
    } else if ((_.isObject(apellido_paterno) || _.isString(apellido_paterno)) && _.isEmpty(apellido_paterno)) {
        var ID_1438123274_opts = [L('x1518866076_traducir', 'Aceptar')];
        var ID_1438123274 = Ti.UI.createAlertDialog({
            title: L('x2185084353_traducir', 'Atención'),
            message: L('x3533941670_traducir', 'Ingrese apellido paterno'),
            buttonNames: ID_1438123274_opts
        });
        ID_1438123274.addEventListener('click', function(e) {
            var suu = ID_1438123274_opts[e.index];
            suu = null;

            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1438123274.show();
    } else if ((_.isObject(codigo_verificador) || _.isString(codigo_verificador)) && _.isEmpty(codigo_verificador)) {
        var ID_1884737053_opts = [L('x1518866076_traducir', 'Aceptar')];
        var ID_1884737053 = Ti.UI.createAlertDialog({
            title: L('x2185084353_traducir', 'Atención'),
            message: L('x3203487177_traducir', 'Ingrese RUT'),
            buttonNames: ID_1884737053_opts
        });
        ID_1884737053.addEventListener('click', function(e) {
            var suu = ID_1884737053_opts[e.index];
            suu = null;

            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1884737053.show();
    } else if (_.isNull(fecha_nacimiento)) {
        var ID_955387930_opts = [L('x1518866076_traducir', 'Aceptar')];
        var ID_955387930 = Ti.UI.createAlertDialog({
            title: L('x2185084353_traducir', 'Atención'),
            message: L('x1503656371_traducir', 'Ingrese fecha de nacimiento'),
            buttonNames: ID_955387930_opts
        });
        ID_955387930.addEventListener('click', function(e) {
            var suu = ID_955387930_opts[e.index];
            suu = null;

            e.source.removeEventListener("click", arguments.callee);
        });
        ID_955387930.show();
    } else if ((fecha_nacimiento.length) == 0 || (fecha_nacimiento.length) == '0') {
        /** 
         * &#8804; 
         */
        var ID_1140392854_opts = [L('x1518866076_traducir', 'Aceptar')];
        var ID_1140392854 = Ti.UI.createAlertDialog({
            title: L('x2185084353_traducir', 'Atención'),
            message: L('x1503656371_traducir', 'Ingrese fecha de nacimiento'),
            buttonNames: ID_1140392854_opts
        });
        ID_1140392854.addEventListener('click', function(e) {
            var suu = ID_1140392854_opts[e.index];
            suu = null;

            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1140392854.show();
    } else if (_.isNumber(hoy_ano - sel_ano) && _.isNumber(18) && hoy_ano - sel_ano < 18) {
        var ID_1255761996_opts = [L('x1518866076_traducir', 'Aceptar')];
        var ID_1255761996 = Ti.UI.createAlertDialog({
            title: L('x2185084353_traducir', 'Atención'),
            message: L('x3243799351_traducir', 'Debe ser mayor de edad'),
            buttonNames: ID_1255761996_opts
        });
        ID_1255761996.addEventListener('click', function(e) {
            var suu = ID_1255761996_opts[e.index];
            suu = null;

            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1255761996.show();
    } else {
        var moment = require('alloy/moment');
        var hoy_ano = moment(new Date()).format('YYYY');
        var moment = require('alloy/moment');
        var ID_450324445 = fecha_nacimiento;
        var sel_ano = moment(ID_450324445).format('YYYY');
        if (_.isNumber((hoy_ano - sel_ano)) && _.isNumber(18) && (hoy_ano - sel_ano) < 18) {
            var ID_1109899382_opts = [L('x1518866076_traducir', 'Aceptar')];
            var ID_1109899382 = Ti.UI.createAlertDialog({
                title: L('x2185084353_traducir', 'Atención'),
                message: L('x3243799351_traducir', 'Debe ser mayor de edad'),
                buttonNames: ID_1109899382_opts
            });
            ID_1109899382.addEventListener('click', function(e) {
                var suu = ID_1109899382_opts[e.index];
                suu = null;

                e.source.removeEventListener("click", arguments.callee);
            });
            ID_1109899382.show();
        } else {
            /** 
             * Recuperamos la variable registro y agregamos los campos de la pantalla 
             */
            var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
            var registro = _.extend(registro, {
                nombre: nombre,
                apellido_paterno: apellido_paterno,
                apellido_materno: apellido_materno,
                codigo_identificador: codigo_verificador,
                fecha_nacimiento: fecha_nacimiento
            });
            require('vars')['registro'] = registro;
            /** 
             * Enviamos a la proxima pantalla, domicilio 
             */
            if ("ID_1119875032" in Alloy.Globals) {
                Alloy.Globals["ID_1119875032"].openWindow(Alloy.createController("domicilio", {}).getView());
            } else {
                Alloy.Globals["ID_1119875032"] = $.ID_1119875032;
                Alloy.Globals["ID_1119875032"].openWindow(Alloy.createController("domicilio", {}).getView());
            }

        }
    }
}

(function() {
    var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
    var ID_1588688991_i = Alloy.createCollection('pais');
    var ID_1588688991_i_where = 'id_server=\'' + registro.pais + '\'';
    ID_1588688991_i.fetch({
        query: 'SELECT * FROM pais WHERE id_server=\'' + registro.pais + '\''
    });
    var pais_iter = require('helper').query2array(ID_1588688991_i);
    /** 
     * Modificamos el label para poner el codigo identificador de una persona 
     */
    $.ID_1454083959.setText(pais_iter[0].label_codigo_identificador);

    var hint;
    hint = $.ID_1719306939.getHintText();

    /** 
     * Modificamos el hint para poner el codigo identificador de una persona 
     */
    $.ID_1719306939.setHintText(String.format(L('x1045525933', '%1$s %2$s'), (hint) ? hint.toString() : '', (pais_iter[0].label_codigo_identificador) ? pais_iter[0].label_codigo_identificador.toString() : ''));

    /** 
     * Creamos evento que al cerrar desde la ultima pantalla, esta tambi&#233;n se cierre. Evitamos el uso excesivo de memoria ram 
     */

    _my_events['_close_enrolamiento,ID_131518285'] = function(evento) {
        if (false) console.log('escuchando cerrar enrolamiento datospersonales', {});
        /** 
         * Cerramos pantalla datos personales 
         */
        $.ID_334050739.close();
    };
    Alloy.Events.on('_close_enrolamiento', _my_events['_close_enrolamiento,ID_131518285']);
})();

if (OS_IOS || OS_ANDROID) {
    $.ID_334050739.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}