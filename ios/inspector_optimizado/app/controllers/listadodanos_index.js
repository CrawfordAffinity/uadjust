var _bind4section={"ref4":"tipo_partida"};
var _list_templates={"elemento":{"ID_432412973":{},"ID_291370846":{"text":"{nombre}"},"ID_1925487537":{"text":"{id_server}"},"ID_975786586":{"text":"{id_segured}"},"ID_1546190443":{}}};

$.ID_1772078359_window.setTitleAttributes({
color : 'WHITE'
}
);
var _activity; 
if (OS_ANDROID) { _activity = $.ID_1772078359.activity; var abx = require('com.alcoapps.actionbarextras'); }
var _my_events = {}, _out_vars = {}, $item = {}, args = arguments[0] || {}; if ('__args' in args && '__id' in args['__args']) args.__id=args['__args'].__id; if ('__modelo' in args && _.keys(args.__modelo).length>0 && 'item' in $) { $.item.set(args.__modelo); $item = $.item.toJSON(); }
var _var_scopekey = 'ID_1772078359';
require('vars')[_var_scopekey]={};
if (OS_ANDROID) {
   $.ID_1772078359_window.addEventListener('open', function(e) {
   abx.setStatusbarColor("#FFFFFF");
   abx.setBackgroundColor("#b9aaf3");
   });
}


var ID_1293541575_like = function(search) {
  if (typeof search !== 'string' || this === null) {return false; }
  search = search.replace(new RegExp("([\\.\\\\\\+\\*\\?\\[\\^\\]\\$\\(\\)\\{\\}\\=\\!\\<\\>\\|\\:\\-])", "g"), "\\$1");
  search = search.replace(/%/g, '.*').replace(/_/g, '.');
  return RegExp('^' + search + '$', 'gi').test(this);
};
var ID_1293541575_filter = function(coll) {
var filtered = _.toArray(coll.filter(function(m) { return true; }));
var ordered = _.sortBy(filtered,'nombre');
return ordered;
};
var ID_1293541575_transform = function(model) {
  var modelo = model.toJSON();
  return modelo;
};
var ID_1293541575_update = function(e) {
};
_.defer(function() { 
 Alloy.Collections.tipo_partida.fetch(); 
 });
Alloy.Collections.tipo_partida.on('add change delete', function(ee) { ID_1293541575_update(ee); });
ID_1293541575_filter = function(coll) {
var filtered = _.toArray(coll.filter(function(filax) {
   var fila=filax.toJSON();
   var test=true;
   fila=null;
   return test;
}));
return filtered;
};
Alloy.Collections.tipo_partida.fetch();

function Click_ID_918039475(e) {

e.cancelBubble=true;
var elemento=e.source;
/** 
* Cierra la pantalla tipo_modal 
*/
$.ID_1772078359.close();

}
function Itemclick_ID_1286804751(e) {

e.cancelBubble=true;
var objeto=e.section.getItemAt(e.itemIndex); var modelo={}, _modelo=[];
var fila={}, fila_bak={}, info={ _template:objeto.template, _what:[], _seccion_ref:e.section.getHeaderTitle(), _model_id:-1 }, _tmp={ objmap:{} };
if ('itemId' in e) {
   info._model_id=e.itemId;
   modelo._id=info._model_id;
   if (info._seccion_ref!='' && info._seccion_ref in _bind4section) {
      modelo._collection=_bind4section[info._seccion_ref];
      _tmp._coll=modelo._collection;
   }
}
var findVariables = require('fvariables');
_.each(_list_templates[info._template], function(obj_id, id) {
   _.each(obj_id, function(valor, prop) {
      var llaves = findVariables(valor,'{','}');
         _.each(llaves, function(llave) {
            _tmp.objmap[llave] = { id:id, prop:prop };
            fila[llave] = objeto[id][prop];
            if (id==e.bindId) info._what.push(llave);
         });
   });
});
info._what = info._what.join(',');
fila_bak = JSON.parse(JSON.stringify(fila));
if (Ti.App.deployType != 'production') console.log('detalle del item seleccionado',{"datos":fila});
Alloy.Events.trigger('resp_dato', {id_partida : fila.id_segured,nombre : fila.nombre,id_server : fila.id_server});
$.ID_1772078359.close();
_tmp.changed = false; _tmp.diff_keys = [];
_.each(fila, function(value1,prop) {
   var had_samekey = false;
   _.each(fila_bak, function(value2,prop2) {
      if (prop==prop2 && value1==value2) {
         had_samekey=true;
      } else if (!_.has(fila_bak,prop) || !_.has(fila,prop2)) {
         has_samekey=true;
      }
   });
   if (!had_samekey) _tmp.diff_keys.push(prop);
});
if (_tmp.diff_keys.length>0) _tmp.changed=true;
if (_tmp.changed==true) {
   _.each(_tmp.diff_keys, function(llave) {
      objeto[_tmp.objmap[llave].id][_tmp.objmap[llave].prop] = fila[llave];
   });
   e.section.updateItemAt(e.itemIndex, objeto);
}

}
/** 
* Recuperamos variables para editar y cargar cosas de la pantalla 
*/
(function() {
var ID_106646031_func = function() {
var seltarea = ('seltarea' in require('vars'))?require('vars')['seltarea']:'';
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
var pais = ('pais' in require('vars'))?require('vars')['pais']:'';
}
 else {
var ID_1420826415_i=Alloy.Collections.tipo_partida;
var sql = "DELETE FROM " + ID_1420826415_i.config.adapter.collection_name;
var db = Ti.Database.open(ID_1420826415_i.config.adapter.db_name);
db.execute(sql);
db.close();
ID_1420826415_i.trigger('remove');
var item_index = 0;
_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
item_index += 1;
var ID_310980047_m=Alloy.Collections.tipo_partida;
var ID_310980047_fila = Alloy.createModel('tipo_partida', {
nombre : String.format(L('x3638114596_traducir','Muro%1$s'), item.toString()),
id_server : item,
id_segured : String.format(L('x1125471167_traducir','10%1$s'), item.toString()),
pais : 1
}
 );
ID_310980047_m.add(ID_310980047_fila);
ID_310980047_fila.save();
_.defer(function() { 
Alloy.Collections.tipo_partida.fetch();
});
});
}};
var ID_106646031 = setTimeout(ID_106646031_func, 1000*0.2);
if (Ti.App.deployType != 'production') console.log('cargue pantalla listadodano',{});
})();

function Postlayout_ID_1156648359(e) {

e.cancelBubble=true;
var elemento=e.source;
var ID_1294585966_func = function() {
/** 
* Una vez ya cargada la tarea, dejamos la variable limpia para poder abrir otras pantallas 
*/
require('vars')['var_abriendo']='';
};
var ID_1294585966 = setTimeout(ID_1294585966_func, 1000*0.2);

}
if (OS_IOS || OS_ANDROID) {
$.ID_1772078359.addEventListener('close',function(){
   $.destroy(); // cleanup bindings
   $.off(); //remove backbone events of this controller
   var _ev_tmp = null, _ev_rem = null;
   if (_my_events) {
      for(_ev_tmp in _my_events) { 
   		try {
   		    if (_ev_tmp.indexOf('_web')!=-1) {
   			   Ti.App.removeEventListener(_ev_tmp.split(',')[0],_my_events[_ev_tmp]);
   		    } else {
   			   Alloy.Events.off(_ev_tmp.split(',')[0],_my_events[_ev_tmp]);
   		    }
   		} catch(err10) {
   		}
      }
      _my_events = null;
      //delete _my_events;
   }
   if (_out_vars) {
      for(_ev_tmp in _out_vars) { 
         for (_ev_rem in _out_vars[_ev_tmp]._remove) {
            try {
 eval(_out_vars[_ev_tmp]._remove[_ev_rem]); 
 } catch(_errt) {}
         }
         _out_vars[_ev_tmp] = null;
      }
      _ev_tmp = null;
      //delete _out_vars;
   }
});
}
//$.ID_1772078359.open();