Alloy.Events = _.clone(Backbone.Events);
Alloy.Collections.usuario = Alloy.createCollection('usuario');
Alloy.Collections.casos = Alloy.createCollection('casos');
//contenido de nodo global..
(function() {
	var _my_events = {},
		_out_vars = {},
		_var_scopekey = 'ID_1511373359';
	require('vars')[_var_scopekey] = {};
	require('vars')['urlcrawford'] = L('x1025326216', 'http://qa-serviciosproflow.crawfordaffinity.com/ServiceApp.svc/');
	require('vars')['urluadjust'] = L('x2575237648', 'http://api.uadjust.com:9999/api/');
	var mensajesPush = Ti.UI.createView({});
	var mensajesPush_cloud = require('ti.cloud');
	var mensajesPush_register = function(_meta) {
		var elemento = _meta.value;
		if (Ti.App.deployType != 'production') console.log('registrado', {
			"datos": elemento
		});
		require('vars')['device_token'] = elemento;
		elemento = null;
	};
	var mensajesPush_message = function(_meta) {
		var elemento = _meta.value;
		mensaje = JSON.parse(elemento);
		if (Ti.App.deployType != 'production') console.log('llego mensaje', {
			"datos": mensaje
		});
		var preguntarAlerta7_opts = [L('x1518866076_traducir', 'Aceptar'), L('x2376009830_traducir', ' Cancelar')];
		var preguntarAlerta7 = Ti.UI.createAlertDialog({
			title: L('x1789641236_traducir', 'Notificacion'),
			message: '' + mensaje.android.alert + '',
			buttonNames: preguntarAlerta7_opts
		});
		preguntarAlerta7.addEventListener('click', function(e) {
			var cosa = preguntarAlerta7_opts[e.index];
			cosa = null;

			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta7.show();
		elemento = null;
	};
	var mensajesPush_error = function(_meta) {
		var elemento = _meta.value;
		if (Ti.App.deployType != 'production') console.log('error', {
			"datos": elemento
		});
		elemento = null;
	};
	if (OS_ANDROID) {
		var mensajesPush_cloudpush = require('ti.cloudpush');
		mensajesPush_cloudpush.retrieveDeviceToken({
			success: function(e) {
				mensajesPush_cloud.PushNotifications.subscribeToken({
					device_token: e.deviceToken,
					channel: 'generico',
					type: 'android'
				}, function(ee) {
					if (ee.success) {
						if (typeof mensajesPush_register != 'undefined') mensajesPush_register({
							value: e.deviceToken
						});
					} else {
						if (typeof mensajesPush_error != 'undefined') mensajesPush_error({
							value: ee.message,
							code: ee.error
						});
					}
				});
			},
			error: function(e) {
				if (typeof mensajesPush_error != 'undefined') mensajesPush_error({
					value: e.error,
					code: 'token'
				});
			},
		});
		mensajesPush_cloudpush.addEventListener('callback', function(evt) {
			if (typeof mensajesPush_message != 'undefined') mensajesPush_message({
				value: evt.payload
			});
		});
		mensajesPush_cloudpush.addEventListener('trayClick', function(evt) {
			if (typeof mensajesPush_message != 'undefined') mensajesPush_message({
				value: evt.payload
			});
		});
	} else if (OS_IOS) {
		if (Ti.Platform.name == 'iPhone OS' && parseInt(Ti.Platform.version.split('.')[0]) >= 8) {
			Ti.App.iOS.addEventListener('usernotificationsettings', function registerForPush() {
				Ti.App.iOS.removeEventListener('usernotificationsettings', registerForPush);
				Ti.Network.registerForPushNotifications({
					success: function(e) {
						mensajesPush_cloud.PushNotifications.subscribeToken({
							device_token: e.deviceToken,
							channel: 'generico',
							type: 'ios'
						}, function(ee) {
							if (ee.success) {
								if (typeof mensajesPush_register != 'undefined') mensajesPush_register({
									value: e.deviceToken
								});
							} else {
								if (typeof mensajesPush_error != 'undefined') mensajesPush_error({
									value: ee.message,
									code: ee.error
								});
							}
						});
					},
					error: function(e) {
						if (typeof mensajesPush_error != 'undefined') mensajesPush_error({
							value: e.error
						});
					},
					callback: function(e) {
						if (typeof mensajesPush_message != 'undefined') mensajesPush_message({
							value: e
						});
					}
				});
			});
			Ti.App.iOS.registerUserNotificationSettings({
				types: [Ti.App.iOS.USER_NOTIFICATION_TYPE_ALERT, Ti.App.iOS.USER_NOTIFICATION_TYPE_SOUND, Ti.App.iOS.USER_NOTIFICATION_TYPE_BADGE]
			});
		} else {
			Ti.Network.registerForPushNotifications({
				types: [Ti.App.iOS.USER_NOTIFICATION_TYPE_ALERT, Ti.App.iOS.USER_NOTIFICATION_TYPE_SOUND, Ti.App.iOS.USER_NOTIFICATION_TYPE_BADGE],
				success: function(e) {
					mensajesPush_cloud.PushNotifications.subscribeToken({
						device_token: e.deviceToken,
						channel: 'generico',
						type: 'ios'
					}, function(ee) {
						if (ee.success) {
							if (typeof mensajesPush_register != 'undefined') mensajesPush_register({
								value: e.deviceToken
							});
						} else {
							if (typeof mensajesPush_error != 'undefined') mensajesPush_error({
								value: ee.message,
								code: ee.error
							});
						}
					});
				},
				error: function(e) {
					if (typeof mensajesPush_error != 'undefined') mensajesPush_error({
						value: e.error
					});
				},
				callback: function(e) {
					if (typeof mensajesPush_message != 'undefined') mensajesPush_message({
						value: e
					});
				}
			});
		}
	}
})();