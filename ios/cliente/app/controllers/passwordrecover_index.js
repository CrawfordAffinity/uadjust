var _bind4section = {};
var _list_templates = {};

$.PASSWORD_RECOVERY_window.setTitleAttributes({
	color: 'WHITE'
});
var _activity;
if (OS_ANDROID) {
	_activity = $.PASSWORD_RECOVERY.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'PASSWORD_RECOVERY';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.PASSWORD_RECOVERY_window.addEventListener('open', function(e) {
		abx.setStatusbarColor("#FFFFFF");
		abx.setBackgroundColor("#2d9edb");
	});
}

function Click_vista(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.PASSWORD_RECOVERY.close();

}

function Change_WriteEmail(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	var regemail = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
	var validaremail = regemail.test(elemento);
	if (validaremail == true || validaremail == 'true') {
		$.ID_269232931.setBackgroundColor('#8ce5bd');
	} else {
		$.ID_269232931.setBackgroundColor('#e6e6e6');
	}
	elemento = null, source = null;

}

function Click_vista6(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
		/** 
		 * Revisamos si el celular tiene internet 
		 */
		/** 
		 * Recuperamos el texto ingresado del correo 
		 */
		var email;
		email = $.WriteEmail.getValue();

		if ((_.isObject(email) || _.isString(email)) && _.isEmpty(email)) {
			/** 
			 * Revisamos si el campo esta vacio, si es asi mostramos mensaje de que faltan datos 
			 */
			var preguntarAlerta_opts = [L('x3610695981_traducir', 'OK')];
			var preguntarAlerta = Ti.UI.createAlertDialog({
				title: L('x2861137601_traducir', 'ERROR'),
				message: L('x1676137616_traducir', 'EMPTY EMAIL'),
				buttonNames: preguntarAlerta_opts
			});
			preguntarAlerta.addEventListener('click', function(e) {
				var xd = preguntarAlerta_opts[e.index];
				xd = null;

				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta.show();
		} else {
			/** 
			 * Revisamos si el correo cumple el formato de email con RegEx 
			 */
			var regemail = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
			var validaremail = regemail.test(email);
			if (validaremail != true && validaremail != 'true') {
				/** 
				 * Si el email cumple el formato, enviamos a crawford la solicitud de recuperar contrasena 
				 */
				var preguntarAlerta2_opts = [L('x3610695981_traducir', 'OK')];
				var preguntarAlerta2 = Ti.UI.createAlertDialog({
					title: L('x2861137601_traducir', 'ERROR'),
					message: L('x1647703686_traducir', 'INVALID EMAIL'),
					buttonNames: preguntarAlerta2_opts
				});
				preguntarAlerta2.addEventListener('click', function(e) {
					var xdd = preguntarAlerta2_opts[e.index];
					xdd = null;

					e.source.removeEventListener("click", arguments.callee);
				});
				preguntarAlerta2.show();
			} else {
				var urlcrawford = ('urlcrawford' in require('vars')) ? require('vars')['urlcrawford'] : '';
				var consultarURL = {};

				consultarURL.success = function(e) {
					var elemento = e,
						valor = e;
					if (elemento == true || elemento == 'true') {
						/** 
						 * Si el servidor responde con un true, cerramos la pantalla 
						 */
						$.PASSWORD_RECOVERY.close();
					} else {
						var preguntarAlerta3_opts = [L('x3610695981_traducir', 'OK')];
						var preguntarAlerta3 = Ti.UI.createAlertDialog({
							title: L('x3769205873_traducir', 'ALERT'),
							message: L('x3594877300_traducir', 'USER OR PASSWORD INVALID'),
							buttonNames: preguntarAlerta3_opts
						});
						preguntarAlerta3.addEventListener('click', function(e) {
							var xdx = preguntarAlerta3_opts[e.index];
							xdx = null;

							e.source.removeEventListener("click", arguments.callee);
						});
						preguntarAlerta3.show();
					}
					if (Ti.App.deployType != 'production') console.log('respuesta recuperarpassword', {
						"asd": elemento
					});
					/** 
					 * Ocultamos imagen de progreso 
					 */
					$.progreso.hide();
					var SEND_visible = true;

					if (SEND_visible == 'si') {
						SEND_visible = true;
					} else if (SEND_visible == 'no') {
						SEND_visible = false;
					}
					$.SEND.setVisible(SEND_visible);

					elemento = null, valor = null;
				};

				consultarURL.error = function(e) {
					var elemento = e,
						valor = e;
					var preguntarAlerta4_opts = [L('x3610695981_traducir', 'OK')];
					var preguntarAlerta4 = Ti.UI.createAlertDialog({
						title: L('x3769205873_traducir', 'ALERT'),
						message: L('x1755017309_traducir', 'ERROR CONNECTING TO THE SERVER'),
						buttonNames: preguntarAlerta4_opts
					});
					preguntarAlerta4.addEventListener('click', function(e) {
						var zcx = preguntarAlerta4_opts[e.index];
						zcx = null;

						e.source.removeEventListener("click", arguments.callee);
					});
					preguntarAlerta4.show();
					elemento = null, valor = null;
				};
				require('helper').ajaxUnico('consultarURL', '' + String.format(L('x1410305732', '%1$srecuperarPassword/'), urlcrawford.toString()) + '', 'POST', {
					correo: email
				}, 15000, consultarURL);
				var SEND_visible = false;

				if (SEND_visible == 'si') {
					SEND_visible = true;
				} else if (SEND_visible == 'no') {
					SEND_visible = false;
				}
				$.SEND.setVisible(SEND_visible);

				/** 
				 * Mostramos imagen de progreso 
				 */
				$.progreso.show();
			}
		}
	} else {
		var preguntarAlerta5_opts = [L('x3610695981_traducir', 'OK')];
		var preguntarAlerta5 = Ti.UI.createAlertDialog({
			title: L('x3769205873_traducir', 'ALERT'),
			message: L('x1867626468_traducir', 'YOU DONT HAVE INTERNET CONNECTION'),
			buttonNames: preguntarAlerta5_opts
		});
		preguntarAlerta5.addEventListener('click', function(e) {
			var asd = preguntarAlerta5_opts[e.index];
			asd = null;

			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta5.show();
	}
}

function Click_scroll(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.WriteEmail.blur();

}

(function() {
	$.scroll.setShowVerticalScrollIndicator(false);
})();

if (OS_IOS || OS_ANDROID) {
	$.PASSWORD_RECOVERY.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}
//$.PASSWORD_RECOVERY.open();
