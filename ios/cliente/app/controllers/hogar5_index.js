var _bind4section = {};
var _list_templates = {};

$.HOME_window.setTitleAttributes({
	color: 'WHITE'
});
var _activity;
if (OS_ANDROID) {
	_activity = $.HOME.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'HOME';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.HOME_window.addEventListener('open', function(e) {
		abx.setStatusbarColor("#FFFFFF");
		abx.setBackgroundColor("#2d9edb");
	});
}

function Click_vista(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.HOME.close();

}

$.widgetBarracasos.init({
	__id: 'ALL115378316',
	paso: L('x2226203566', '5'),
	titulo5: L('x951154001_traducir', 'End'),
	titulo1: L('x807033745_traducir', 'Received'),
	titulo4: L('x1018769216_traducir', 'Evaluating'),
	titulo3: L('x4262956787_traducir', 'Inspected'),
	titulo2: L('x807001066_traducir', 'Scheduled')
});


$.widgetCajadescripcion.init({
	titulo: L('x3950563313_traducir', 'Description'),
	__id: 'ALL1926115404'
});

function Load_imagen(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var evento = e;
	elemento.start();

}

(function() {
	var urlcrawford = ('urlcrawford' in require('vars')) ? require('vars')['urlcrawford'] : '';
	var urluadjust = ('urluadjust' in require('vars')) ? require('vars')['urluadjust'] : '';
	if (Ti.App.deployType != 'production') console.log('hogar5', {
		"datos": args
	});
	if ((_.isObject(args) || (_.isString(args)) && !_.isEmpty(args)) || _.isNumber(args) || _.isBoolean(args)) {
		var siniestro = args._data;
		var consultarModelo_i = Alloy.createCollection('casos');
		var consultarModelo_i_where = 'siniestro=\'' + siniestro + '\'';
		consultarModelo_i.fetch({
			query: 'SELECT * FROM casos WHERE siniestro=\'' + siniestro + '\''
		});
		var datos = require('helper').query2array(consultarModelo_i);
		datos = datos[0];
		if (Ti.App.deployType != 'production') console.log('carga', {
			"asd": datos
		});
		var docu = JSON.parse(datos.documentos);
		var hola_index = 0;
		_.each(docu, function(hola, hola_pos, hola_list) {
			hola_index += 1;
			if (Ti.App.deployType != 'production') console.log('cada dato es', {
				"asd": hola
			});
			var vista8 = Titanium.UI.createView({
				height: '50dp',
				bottom: '10dp',
				borderColor: '#e6e6e6',
				layout: 'composite',
				width: '90%',
				top: '10dp',
				borderWidth: 1,
				borderRadius: 5,
				backgroundColor: '#FFFFFF'
			});
			var Holanombre = Titanium.UI.createLabel({
				bottom: 15,
				text: hola.nombre,
				color: '#2d9edb',
				touchEnabled: false,
				top: 15,
				font: {
					fontFamily: 'SFUIDisplay-Regular',
					fontSize: '14dp'
				}

			});
			vista8.add(Holanombre);


			vista8.addEventListener('click', function(e) {
				e.cancelBubble = true;
				var elemento = e.source;
				Ti.Platform.openURL(hola.url);
			});
			$.scroll.add(vista8);
		});
		var days = L('x3957652582_traducir', 'days');
		var legal = L('x2139489547_traducir', 'legal:');
		var uadjust = L('x1807599243_traducir', 'uadjust:');
		$.Label3.setText(String.format(L('x3000044032', '%1$s %2$s %3$s %4$s %5$s %6$s'), (legal) ? legal.toString() : '', (datos.legal) ? datos.legal.toString() : '', (days) ? days.toString() : '', (uadjust) ? uadjust.toString() : '', (datos.estimado) ? datos.estimado.toString() : '', (days) ? days.toString() : ''));

		$.Label2.setText(datos.compania);

	}
	$.scroll.setShowVerticalScrollIndicator(false);
	var hog5 = L('x3999798477_traducir', 'deschogar5');

	$.widgetCajadescripcion.texto({
		valor: L('x3763516846_traducir', 'hog5')
	});
})();

if (OS_IOS || OS_ANDROID) {
	$.HOME.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}
//$.HOME.open();
