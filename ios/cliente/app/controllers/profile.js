var _bind4section = {};
var _list_templates = {};

try {
	$.PROFILE.setTitleAttributes({
		color: '#FFFFFF'
	});
} catch (err_ios) {};
var _activity;
if (OS_ANDROID) {
	_activity = $.PROFILE.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'PROFILE';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.PROFILE.addEventListener('open', function(e) {
		abx.setStatusbarColor("#FFFFFF");
	});
}

function Change_ID_1115208204(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	if (elemento == true || elemento == 'true') {
		var vista18_estilo = 'fondoverde';

		var setEstilo = function(clase) {
			if ('styles' in require('a4w') && clase in require('a4w').styles['classes']) {
				try {
					$.vista18.applyProperties(require('a4w').styles['classes'][clase]);
				} catch (sete_err) {}
			}
		};
		setEstilo(vista18_estilo);

	} else {
		var vista18_estilo = 'fondoplomo';

		var setEstilo = function(clase) {
			if ('styles' in require('a4w') && clase in require('a4w').styles['classes']) {
				try {
					$.vista18.applyProperties(require('a4w').styles['classes'][clase]);
				} catch (sete_err) {}
			}
		};
		setEstilo(vista18_estilo);

	}
	elemento = null;

}

function Click_vista18(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	/** 
	 * En el caso de que sea true, consultamos si hay internet 
	 */
	var agreechange;
	agreechange = $.ID_1115208204.getValue();

	if (agreechange == true || agreechange == 'true') {
		/** 
		 * Revisamos el estado de switch para cambiar el email 
		 */
		if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
			var oldemail;
			oldemail = $.WriteEmail.getValue();

			if ((_.isObject(oldemail) || (_.isString(oldemail)) && !_.isEmpty(oldemail)) || _.isNumber(oldemail) || _.isBoolean(oldemail)) {
				/** 
				 * Con RegEx comprobamos que el campo sea email 
				 */
				var regemail = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
				var validaroldemail = regemail.test(oldemail);
				if (validaroldemail == true || validaroldemail == 'true') {
					var newemail;
					newemail = $.RepeatEmail2.getValue();

					if ((_.isObject(newemail) || (_.isString(newemail)) && !_.isEmpty(newemail)) || _.isNumber(newemail) || _.isBoolean(newemail)) {
						var validarnewemail = regemail.test(newemail);
						if (validarnewemail == true || validarnewemail == 'true') {
							if (Ti.App.deployType != 'production') console.log('envio a enviarcontacto', {
								"olvd": oldemail,
								"newmai": newemail
							});
							/** 
							 * Recuperamos la url de crawford 
							 */
							var urlcrawford = ('urlcrawford' in require('vars')) ? require('vars')['urlcrawford'] : '';
							var consultarURL3 = {};

							consultarURL3.success = function(e) {
								var elemento = e,
									valor = e;
								if (Ti.App.deployType != 'production') console.log('respuesta editarcontacto', {
									"asd": elemento
								});
								if (elemento == true || elemento == 'true') {
									/** 
									 * Si el servidor responde con true, avisamos que el email fue cambiado, en caso contrario mostramos mensaje de que el correo antiguo no existe 
									 */
									var preguntarAlerta3_opts = [L('x3610695981_traducir', 'OK')];
									var preguntarAlerta3 = Ti.UI.createAlertDialog({
										title: L('x3769205873_traducir', 'ALERT'),
										message: L('x3027875514_traducir', 'EMAIL CHANGED'),
										buttonNames: preguntarAlerta3_opts
									});
									preguntarAlerta3.addEventListener('click', function(e) {
										var xdd = preguntarAlerta3_opts[e.index];
										if (xdd == L('x3610695981_traducir', 'OK')) {}
										xdd = null;

										e.source.removeEventListener("click", arguments.callee);
									});
									preguntarAlerta3.show();
								} else if (elemento == false || elemento == 'false') {
									var preguntarAlerta4_opts = [L('x3610695981_traducir', 'OK')];
									var preguntarAlerta4 = Ti.UI.createAlertDialog({
										title: L('x2861137601_traducir', 'ERROR'),
										message: L('x1617761228_traducir', 'OLD EMAIL DOESNT EXIST'),
										buttonNames: preguntarAlerta4_opts
									});
									preguntarAlerta4.addEventListener('click', function(e) {
										var xdd = preguntarAlerta4_opts[e.index];
										xdd = null;

										e.source.removeEventListener("click", arguments.callee);
									});
									preguntarAlerta4.show();
								}
								/** 
								 * Ocultamos imagen de progreso 
								 */
								$.progreso.hide();
								var SAVECHANGES_visible = true;

								if (SAVECHANGES_visible == 'si') {
									SAVECHANGES_visible = true;
								} else if (SAVECHANGES_visible == 'no') {
									SAVECHANGES_visible = false;
								}
								$.SAVECHANGES.setVisible(SAVECHANGES_visible);

								elemento = null, valor = null;
							};

							consultarURL3.error = function(e) {
								var elemento = e,
									valor = e;
								var preguntarAlerta5_opts = [L('x3610695981_traducir', 'OK')];
								var preguntarAlerta5 = Ti.UI.createAlertDialog({
									title: L('x3769205873_traducir', 'ALERT'),
									message: L('x1755017309_traducir', 'ERROR CONNECTING TO THE SERVER'),
									buttonNames: preguntarAlerta5_opts
								});
								preguntarAlerta5.addEventListener('click', function(e) {
									var xd = preguntarAlerta5_opts[e.index];
									xd = null;

									e.source.removeEventListener("click", arguments.callee);
								});
								preguntarAlerta5.show();
								elemento = null, valor = null;
							};
							require('helper').ajaxUnico('consultarURL3', '' + String.format(L('x3984023410', '%1$seditarContacto/'), urlcrawford.toString()) + '', 'POST', {
								correo: oldemail,
								correo_nuevo: newemail
							}, 15000, consultarURL3);
							/** 
							 * Mostramos imagen de progreso 
							 */
							$.progreso.show();
							var SAVECHANGES_visible = false;

							if (SAVECHANGES_visible == 'si') {
								SAVECHANGES_visible = true;
							} else if (SAVECHANGES_visible == 'no') {
								SAVECHANGES_visible = false;
							}
							$.SAVECHANGES.setVisible(SAVECHANGES_visible);

						} else {
							var preguntarAlerta6_opts = [L('x3610695981_traducir', 'OK')];
							var preguntarAlerta6 = Ti.UI.createAlertDialog({
								title: L('x2861137601_traducir', 'ERROR'),
								message: L('x1745909188_traducir', 'INVALID NEW EMAIL'),
								buttonNames: preguntarAlerta6_opts
							});
							preguntarAlerta6.addEventListener('click', function(e) {
								var xdd = preguntarAlerta6_opts[e.index];
								xdd = null;

								e.source.removeEventListener("click", arguments.callee);
							});
							preguntarAlerta6.show();
						}
					} else {
						var preguntarAlerta7_opts = [L('x3610695981_traducir', 'OK')];
						var preguntarAlerta7 = Ti.UI.createAlertDialog({
							title: L('x2861137601_traducir', 'ERROR'),
							message: L('x3802298370_traducir', 'EMPTY NEW EMAIL'),
							buttonNames: preguntarAlerta7_opts
						});
						preguntarAlerta7.addEventListener('click', function(e) {
							var xd = preguntarAlerta7_opts[e.index];
							xd = null;

							e.source.removeEventListener("click", arguments.callee);
						});
						preguntarAlerta7.show();
					}
				} else {
					var preguntarAlerta8_opts = [L('x3610695981_traducir', 'OK')];
					var preguntarAlerta8 = Ti.UI.createAlertDialog({
						title: L('x2861137601_traducir', 'ERROR'),
						message: L('x907214079_traducir', 'INVALID OLD EMAIL'),
						buttonNames: preguntarAlerta8_opts
					});
					preguntarAlerta8.addEventListener('click', function(e) {
						var xdd = preguntarAlerta8_opts[e.index];
						xdd = null;

						e.source.removeEventListener("click", arguments.callee);
					});
					preguntarAlerta8.show();
				}
			} else {
				var preguntarAlerta9_opts = [L('x3610695981_traducir', 'OK')];
				var preguntarAlerta9 = Ti.UI.createAlertDialog({
					title: L('x2861137601_traducir', 'ERROR'),
					message: L('x3164664121_traducir', 'EMPTY OLD EMAIL'),
					buttonNames: preguntarAlerta9_opts
				});
				preguntarAlerta9.addEventListener('click', function(e) {
					var xd = preguntarAlerta9_opts[e.index];
					xd = null;

					e.source.removeEventListener("click", arguments.callee);
				});
				preguntarAlerta9.show();
			}
		} else {
			var preguntarAlerta10_opts = [L('x3610695981_traducir', 'OK')];
			var preguntarAlerta10 = Ti.UI.createAlertDialog({
				title: L('x3769205873_traducir', 'ALERT'),
				message: L('x1867626468_traducir', 'YOU DONT HAVE INTERNET CONNECTION'),
				buttonNames: preguntarAlerta10_opts
			});
			preguntarAlerta10.addEventListener('click', function(e) {
				var asd = preguntarAlerta10_opts[e.index];
				asd = null;

				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta10.show();
		}
	}

}

(function() {
	/** 
	 * Consultamos la tabla de usuario para poder mostrar en pantalla los datos del usuario... Nombre, RUT, fecha nacimiento 
	 */
	var consultarModelo3_i = Alloy.createCollection('usuario');
	var consultarModelo3_i_where = '';
	consultarModelo3_i.fetch();
	var user = require('helper').query2array(consultarModelo3_i);
	$.ClientsName.setText(user[0].name);

	$.Rut.setText(user[0].rut);

	$.Birthdate.setText(user[0].birthdate);

})();

if (OS_IOS || OS_ANDROID) {
	$.PROFILE.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}