var _bind4section = {};
var _list_templates = {};

$.CAR_window.setTitleAttributes({
	color: '#FFFFFF'
});
var _activity;
if (OS_ANDROID) {
	_activity = $.CAR.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'CAR';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.CAR_window.addEventListener('open', function(e) {
		abx.setStatusbarColor("#FFFFFF");
		abx.setBackgroundColor("#2d9edb");
	});
}

function Click_vista(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.CAR.close();

}

$.widgetBarrafraude.init({
	__id: 'ALL507654472',
	paso: L('x450215437', '2'),
	titulo1: L('x807033745_traducir', 'Received'),
	titulo3: L('x951154001_traducir', 'End'),
	titulo2: L('x1018769216_traducir', 'Evaluating')
});


(function() {
	$.scroll.setShowVerticalScrollIndicator(false);
	if ((_.isObject(args) || (_.isString(args)) && !_.isEmpty(args)) || _.isNumber(args) || _.isBoolean(args)) {
		var siniestro = args._data;
		var consultarModelo_i = Alloy.createCollection('casos');
		var consultarModelo_i_where = 'siniestro=\'' + siniestro + '\'';
		consultarModelo_i.fetch({
			query: 'SELECT * FROM casos WHERE siniestro=\'' + siniestro + '\''
		});
		var datos = require('helper').query2array(consultarModelo_i);
		datos = datos[0];
		if (Ti.App.deployType != 'production') console.log('carga', {
			"asd": datos
		});
		var docu = JSON.parse(datos.documentos);
		var hola_index = 0;
		_.each(docu, function(hola, hola_pos, hola_list) {
			hola_index += 1;
			var vista10 = Titanium.UI.createView({
				height: '1dp',
				layout: 'vertical',
				width: Ti.UI.FILL,
				backgroundColor: '#E6E6E6',
				font: {
					fontSize: '12dp'
				}

			});
			$.vista8.add(vista10);
			if (Ti.App.deployType != 'production') console.log('asdasd', {
				"asd": hola
			});
			if (hola.estado == 1 || hola.estado == '1') {
				var vista11 = Titanium.UI.createView({
					height: Ti.UI.SIZE,
					bottom: '20dp',
					layout: 'horizontal',
					top: '20dp'
				});
				if (OS_IOS) {
					var imagen = Ti.UI.createImageView({
						left: 17,
						image: '/images/i2C7AF58BC23164AB82E5D62503EC1D98.png',
						id: imagen
					});
				} else {
					var imagen = Ti.UI.createImageView({
						left: 17,
						image: '/images/i2C7AF58BC23164AB82E5D62503EC1D98.png',
						id: imagen
					});
				}
				require('vars')['_imagen_original_'] = imagen.getImage();
				require('vars')['_imagen_filtro_'] = 'original';
				vista11.add(imagen);
				var Holanombre = Titanium.UI.createLabel({
					left: 10,
					text: hola.nombre,
					color: '#a0a1a3',
					touchEnabled: false,
					width: Ti.UI.FILL,
					font: {
						fontFamily: 'SFUIDisplay-Light',
						fontSize: '13dp'
					}

				});
				vista11.add(Holanombre);

				$.vista8.add(vista11);
			} else {
				var vista12 = Titanium.UI.createView({
					height: Ti.UI.SIZE,
					bottom: '20dp',
					layout: 'composite',
					top: '20dp'
				});
				var Holanombre2 = Titanium.UI.createLabel({
					left: 40,
					text: hola.nombre,
					color: '#a0a1a3',
					touchEnabled: false,
					width: Ti.UI.FILL,
					font: {
						fontFamily: 'SFUIDisplay-Light',
						fontSize: '13dp'
					}

				});
				vista12.add(Holanombre2);

				$.vista8.add(vista12);
			}
		});
		$.Label2.setText(datos.compania);

		$.Desccar.setText(datos.descripcion);

		$.Correo.setText(datos.correo);

		var days = L('x3957652582_traducir', 'days');
		var legal = L('x2139489547_traducir', 'legal:');
		var uadjust = L('x1807599243_traducir', 'uadjust:');
		$.Label3.setText(String.format(L('x3000044032', '%1$s %2$s %3$s %4$s %5$s %6$s'), (legal) ? legal.toString() : '', (datos.legal) ? datos.legal.toString() : '', (days) ? days.toString() : '', (uadjust) ? uadjust.toString() : '', (datos.estimado) ? datos.estimado.toString() : '', (days) ? days.toString() : ''));

	}
})();

if (OS_IOS || OS_ANDROID) {
	$.CAR.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}
//$.CAR.open();
