var _bind4section = {};
var _list_templates = {};

try {
	$.CASES.setTitleAttributes({
		color: '#FFFFFF'
	});
} catch (err_ios) {};
var _activity;
if (OS_ANDROID) {
	_activity = $.CASES.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'CASES';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.CASES.addEventListener('open', function(e) {
		abx.setStatusbarColor("#FFFFFF");
	});
}


(function() {
	var urlcrawford = ('urlcrawford' in require('vars')) ? require('vars')['urlcrawford'] : '';
	var consultarModelo_i = Alloy.createCollection('usuario');
	var consultarModelo_i_where = '';
	consultarModelo_i.fetch();
	var user = require('helper').query2array(consultarModelo_i);
	if (Ti.App.deployType != 'production') console.log('usuario', {
		"asd": user
	});
	var consultarURL = {};

	consultarURL.success = function(e) {
		var elemento = e,
			valor = e;
		var eliminarModelo_i = Alloy.Collections.casos;
		var sql = "DELETE FROM " + eliminarModelo_i.config.adapter.collection_name;
		var db = Ti.Database.open(eliminarModelo_i.config.adapter.db_name);
		db.execute(sql);
		db.close();
		eliminarModelo_i.trigger('remove');
		var elemento_inspecciones = elemento.inspecciones;
		var insertarModelo_m = Alloy.Collections.casos;
		var db_insertarModelo = Ti.Database.open(insertarModelo_m.config.adapter.db_name);
		db_insertarModelo.execute('BEGIN');
		_.each(elemento_inspecciones, function(insertarModelo_fila, pos) {
			db_insertarModelo.execute('INSERT INTO casos (tipotexto, siniestro, rut_inspector, documentos, calificacion, compania, legal, imagen_inspector, estimado, inspector, realizado_fecha, fecha_agendamiento, num_caso, realizado_dias, estado, estadotexto, tipo, correo, descripcion) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', insertarModelo_fila.tipo, insertarModelo_fila.siniestro, insertarModelo_fila.datos_estado.rut_inspector, insertarModelo_fila.datos_estado.documentos, insertarModelo_fila.datos_estado.calificacion, insertarModelo_fila.compania, insertarModelo_fila.legal, insertarModelo_fila.datos_estado.imagen_inspector, insertarModelo_fila.estimado, insertarModelo_fila.datos_estado.inspector, insertarModelo_fila.realizado_fecha, insertarModelo_fila.datos_estado.fecha_agendamiento, insertarModelo_fila.datos_estado.num_caso, insertarModelo_fila.realizado_dias, insertarModelo_fila.estado, insertarModelo_fila.estado, insertarModelo_fila.tipo, insertarModelo_fila.datos_estado.correo, insertarModelo_fila.datos_estado.descripcion);
		});
		db_insertarModelo.execute('COMMIT');
		db_insertarModelo.close();
		db_insertarModelo = null;
		insertarModelo_m.trigger('change');
		var consultarModelo2_i = Alloy.createCollection('casos');
		var consultarModelo2_i_where = '';
		consultarModelo2_i.fetch();
		var casitos = require('helper').query2array(consultarModelo2_i);
		if (Ti.App.deployType != 'production') console.log('carga de casitos', {
			"asd": casitos
		});
		var tarea_index = 0;
		_.each(casitos, function(tarea, tarea_pos, tarea_list) {
			tarea_index += 1;
			if (Ti.App.deployType != 'production') console.log('tareas', {
				"asd": tarea
			});
			if (tarea.tipotexto == 1 || tarea.tipotexto == '1') {
				/** 
				 * Revisamos cada caso en la tabla... Definiendo el tipo y estado, le agregamos la funcionalidad para i18n 
				 */
				var home = L('x3521422318_traducir', 'Home');
				tarea.tipotexto = home;
			} else if (tarea.tipotexto == 2) {
				var car = L('x1332781181_traducir', 'Car');
				tarea.tipotexto = car;
			} else if (tarea.tipotexto == 3) {
				var fraud = L('x2166733574_traducir', 'Fraud');
				tarea.tipotexto = fraud;
			}
			if (tarea.estadotexto == 1 || tarea.estadotexto == '1') {
				var received = L('x807033745_traducir', 'Received');
				tarea.estadotexto = received;
			} else if (tarea.estadotexto == 2) {
				var assignment = L('x2140686186_traducir', 'Assignment');
				tarea.estadotexto = assignment;
			} else if (tarea.estadotexto == 3) {
				var inspected = L('x4262956787_traducir', 'Inspected');
				tarea.estadotexto = inspected;
			} else if (tarea.estadotexto == 4) {
				var evaluating = L('x1018769216_traducir', 'Evaluating');
				tarea.estadotexto = evaluating;
			} else if (tarea.estadotexto == 5) {
				var end = L('x951154001_traducir', 'End');
				tarea.estadotexto = end;
			}
			var filaPicker = Titanium.UI.createTableViewRow({
				className: 'hackfilaPicker'
			});
			var vista = Titanium.UI.createView({
				height: '140dp',
				bottom: '5dp',
				borderColor: '#e6e6e6',
				layout: 'horizontal',
				width: '90%',
				top: '5dp',
				borderWidth: 1,
				borderRadius: 5,
				backgroundColor: '#FFFFFF'
			});
			var vista3 = Titanium.UI.createView({
				height: Ti.UI.SIZE,
				left: '10dp',
				layout: 'vertical',
				width: '90%'
			});
			var vista4 = Titanium.UI.createView({
				height: Ti.UI.SIZE,
				layout: 'composite',
				top: '10dp'
			});
			var Tareasiniestro = Titanium.UI.createLabel({
				left: 0,
				text: tarea.siniestro,
				color: '#4d4d4d',
				touchEnabled: false,
				font: {
					fontFamily: 'SFUIDisplay-Regular',
					fontSize: '16dp'
				}

			});
			vista4.add(Tareasiniestro);

			vista3.add(vista4);
			var vista5 = Titanium.UI.createView({
				height: Ti.UI.SIZE,
				layout: 'horizontal',
				top: '5dp'
			});
			if (OS_IOS) {
				var imagen = Ti.UI.createImageView({
					image: '/images/i239C58B331BACBFFC1F05A0C07342641.png',
					id: imagen
				});
			} else {
				var imagen = Ti.UI.createImageView({
					image: '/images/i239C58B331BACBFFC1F05A0C07342641.png',
					id: imagen
				});
			}
			require('vars')['_imagen_original_'] = imagen.getImage();
			require('vars')['_imagen_filtro_'] = 'original';
			vista5.add(imagen);
			var Tareaestadotexto = Titanium.UI.createLabel({
				left: 3,
				text: tarea.estadotexto,
				color: '#a0a1a3',
				touchEnabled: false,
				width: Ti.UI.SIZE,
				font: {
					fontFamily: 'SFUIDisplay-Light',
					fontSize: '13dp'
				}

			});
			vista5.add(Tareaestadotexto);

			vista3.add(vista5);
			var vista6 = Titanium.UI.createView({
				height: Ti.UI.SIZE,
				layout: 'horizontal',
				top: '5dp'
			});
			if (OS_IOS) {
				var imagen2 = Ti.UI.createImageView({
					left: 1,
					image: '/images/i8E796261F1E325976728EEA3FEC63CD2.png',
					id: imagen2
				});
			} else {
				var imagen2 = Ti.UI.createImageView({
					left: 1,
					image: '/images/i8E796261F1E325976728EEA3FEC63CD2.png',
					id: imagen2
				});
			}
			require('vars')['_imagen2_original_'] = imagen2.getImage();
			require('vars')['_imagen2_filtro_'] = 'original';
			vista6.add(imagen2);
			var Tareacompania = Titanium.UI.createLabel({
				left: 3,
				text: tarea.compania,
				color: '#a0a1a3',
				touchEnabled: false,
				width: Ti.UI.SIZE,
				font: {
					fontFamily: 'SFUIDisplay-Light',
					fontSize: '13dp'
				}

			});
			vista6.add(Tareacompania);

			vista3.add(vista6);
			var vista7 = Titanium.UI.createView({
				height: Ti.UI.SIZE,
				layout: 'horizontal',
				top: '5dp'
			});
			var LegalETA = Titanium.UI.createLabel({
				text: L('x3505507568_traducir', 'Legal ETA:'),
				color: '#838383',
				touchEnabled: false,
				width: Ti.UI.SIZE,
				font: {
					fontFamily: 'SFUIDisplay-Medium',
					fontSize: '13dp'
				}

			});
			vista7.add(LegalETA);

			var Tarealegal = Titanium.UI.createLabel({
				left: 3,
				text: String.format(L('x3860732411', '%1$s days'), tarea.legal.toString()),
				color: '#a0a1a3',
				touchEnabled: false,
				width: Ti.UI.SIZE,
				font: {
					fontFamily: 'SFUIDisplay-Light',
					fontSize: '13dp'
				}

			});
			vista7.add(Tarealegal);

			vista3.add(vista7);
			var vista8 = Titanium.UI.createView({
				height: Ti.UI.SIZE,
				bottom: '10dp',
				layout: 'horizontal',
				width: Ti.UI.FILL,
				top: '5dp'
			});
			var ETA = Titanium.UI.createLabel({
				text: L('x3377813129_traducir', 'ETA:'),
				color: '#838383',
				touchEnabled: false,
				width: Ti.UI.SIZE,
				font: {
					fontFamily: 'SFUIDisplay-Medium',
					fontSize: '13dp'
				}

			});
			vista8.add(ETA);

			var Tareaestimado = Titanium.UI.createLabel({
				left: 3,
				text: String.format(L('x3860732411', '%1$s days'), tarea.estimado.toString()),
				color: '#a0a1a3',
				touchEnabled: false,
				width: Ti.UI.SIZE,
				font: {
					fontFamily: 'SFUIDisplay-Light',
					fontSize: '13dp'
				}

			});
			vista8.add(Tareaestimado);

			vista3.add(vista8);
			vista.add(vista3);
			var vista9 = Titanium.UI.createView({
				height: Ti.UI.SIZE,
				layout: 'composite',
				width: Ti.UI.FILL
			});
			if (OS_IOS) {
				var imagen3 = Ti.UI.createImageView({
					image: '/images/iF679959773CE61B0590F6F405053072E.png',
					id: imagen3
				});
			} else {
				var imagen3 = Ti.UI.createImageView({
					image: '/images/iF679959773CE61B0590F6F405053072E.png',
					id: imagen3
				});
			}
			require('vars')['_imagen3_original_'] = imagen3.getImage();
			require('vars')['_imagen3_filtro_'] = 'original';
			vista9.add(imagen3);
			vista.add(vista9);
			filaPicker.add(vista);

			filaPicker.addEventListener('click', function(e) {
				e.cancelBubble = true;
				var evento = e;
				var fila = e.row;
				var data = e.rowData;
				var elemento = e.source;
				var siniestro;
				siniestro = Tareasiniestro;
				if (Ti.App.deployType != 'production') console.log('dato', {
					"asd": siniestro.text
				});
				if (Ti.App.deployType != 'production') console.log('tarea', {
					"asd": tarea
				});
				if (tarea.tipo == 1 || tarea.tipo == '1') {
					/** 
					 * Dependiendo del tipo de caso...
					 * 1. hogar
					 * 2. fraude
					 * 3.&#160;auto 
					 */
					if (tarea.estado == 1 || tarea.estado == '1') {
						/** 
						 * Dependiendo del estado de caso...
						 * 1. Caso recibido
						 * 2. Datos de agendamiento
						 * 3.&#160;Calificacion del inspector
						 * 4. Documentos
						 * 5. Inspeccion finalizada 
						 */
						Alloy.createController("hogar1_index", {
							'_data': siniestro.text
						}).getView().open();
						if (Ti.App.deployType != 'production') console.log('tarea11', {});
					} else if (tarea.estado == 2) {
						Alloy.createController("hogar2_index", {
							'_data': siniestro.text
						}).getView().open();
						if (Ti.App.deployType != 'production') console.log('tarea12', {});
					} else if (tarea.estado == 3) {
						Alloy.createController("hogar3_index", {
							'_data': siniestro.text
						}).getView().open();
						if (Ti.App.deployType != 'production') console.log('tarea13', {});
					} else if (tarea.estado == 4) {
						Alloy.createController("hogar4_index", {
							'_data': siniestro.text
						}).getView().open();
						if (Ti.App.deployType != 'production') console.log('tarea14', {});
					} else if (tarea.estado == 5) {
						Alloy.createController("hogar5_index", {
							'_data': siniestro.text
						}).getView().open();
						if (Ti.App.deployType != 'production') console.log('tarea15', {});
					}
				} else if (tarea.tipo == 2) {
					if (tarea.estado == 1 || tarea.estado == '1') {
						Alloy.createController("fraude1_index", {
							'_data': siniestro.text
						}).getView().open();
						if (Ti.App.deployType != 'production') console.log('tarea21', {});
					} else if (tarea.estado == 2) {
						Alloy.createController("fraude2_index", {
							'_data': siniestro.text
						}).getView().open();
						if (Ti.App.deployType != 'production') console.log('tarea22', {});
					} else if (tarea.estado == 3) {
						Alloy.createController("fraude3_index", {
							'_data': siniestro.text
						}).getView().open();
						if (Ti.App.deployType != 'production') console.log('tarea23', {});
					}
				} else if (tarea.tipo == 3) {
					if (tarea.estado == 1 || tarea.estado == '1') {
						Alloy.createController("auto1_index", {
							'_data': siniestro.text
						}).getView().open();
						if (Ti.App.deployType != 'production') console.log('tarea31', {});
					} else if (tarea.estado == 2) {
						Alloy.createController("auto2_index", {
							'_data': siniestro.text
						}).getView().open();
						if (Ti.App.deployType != 'production') console.log('tarea32', {});
					} else if (tarea.estado == 3) {
						Alloy.createController("auto3_index", {
							'_data': siniestro.text
						}).getView().open();
						if (Ti.App.deployType != 'production') console.log('tarea33', {});
					}
				}
				evento = null, fila = null, data = null, elemento = null;
			});
			$.tabla.appendRow(filaPicker, false);
		});
		if (Ti.App.deployType != 'production') console.log('obteniendo datos de siniestros', {
			"asd": elemento
		});
		var inspeccion_index = 0;
		_.each(elemento.inspecciones, function(inspeccion, inspeccion_pos, inspeccion_list) {
			inspeccion_index += 1;
			if (Ti.App.deployType != 'production') console.log('detalle de insp', {
				"asd": inspeccion
			});
		});
		elemento = null, valor = null;
	};

	consultarURL.error = function(e) {
		var elemento = e,
			valor = e;
		var preguntarAlerta_opts = [L('x3610695981_traducir', 'OK')];
		var preguntarAlerta = Ti.UI.createAlertDialog({
			title: L('x3769205873_traducir', 'ALERT'),
			message: L('x3064151243_traducir', 'ERROR OBTAINING THE CASES'),
			buttonNames: preguntarAlerta_opts
		});
		preguntarAlerta.addEventListener('click', function(e) {
			var xd = preguntarAlerta_opts[e.index];
			xd = null;

			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta.show();
		elemento = null, valor = null;
	};
	require('helper').ajaxUnico('consultarURL', '' + String.format(L('x4094662162', '%1$ssiniestrosAsegurado/'), urlcrawford.toString()) + '', 'POST', {
		correo: user[0].email
	}, 15000, consultarURL);
	var cantbanners = ('cantbanners' in require('vars')) ? require('vars')['cantbanners'] : '';
	var consultarURL2 = {};

	consultarURL2.success = function(e) {
		var elemento = e,
			valor = e;
		var hola_index = 0;
		_.each(elemento.imagenes, function(hola, hola_pos, hola_list) {
			hola_index += 1;
			if (OS_IOS) {
				var imagen4 = Ti.UI.createImageView({
					height: Ti.UI.FILL,
					width: Ti.UI.FILL,
					image: hola.url_imagen,
					id: imagen4
				});
			} else {
				var imagen4 = Ti.UI.createImageView({
					height: Ti.UI.FILL,
					width: Ti.UI.FILL,
					image: hola.url_imagen,
					id: imagen4
				});
			}
			require('vars')['_imagen4_original_'] = imagen4.getImage();
			require('vars')['_imagen4_filtro_'] = 'original';

			imagen4.addEventListener('click', function(e) {
				e.cancelBubble = true;
				var elemento = e.source;
				var evento = e;
				Ti.Platform.openURL(hola.link);
			});
			$.vistaNavegable.addView(imagen4);
			require('vars')['cantbanners'] = hola_pos + 1;
			if (Ti.App.deployType != 'production') console.log('dato banner', {
				"asd": hola
			});
		});
		elemento = null, valor = null;
	};

	consultarURL2.error = function(e) {
		var elemento = e,
			valor = e;
		var preguntarAlerta2_opts = [L('x3610695981_traducir', 'OK')];
		var preguntarAlerta2 = Ti.UI.createAlertDialog({
			title: L('x3769205873_traducir', 'ALERT'),
			message: L('x3455773058_traducir', 'ERROR OBTAINING THE BANNERS'),
			buttonNames: preguntarAlerta2_opts
		});
		preguntarAlerta2.addEventListener('click', function(e) {
			var xd = preguntarAlerta2_opts[e.index];
			xd = null;

			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta2.show();
		elemento = null, valor = null;
	};
	require('helper').ajaxUnico('consultarURL2', '' + String.format(L('x2232578092', '%1$sbanners/'), urlcrawford.toString()) + '', 'POST', {
		pais: 'Chile'
	}, 15000, consultarURL2);
	var carrusel = 0;
	var ID_1856920329_continuar = true;
	_out_vars['ID_1856920329'] = {
		_remove: ["clearTimeout(_out_vars['ID_1856920329']._run)"] 
	};
	var ID_1856920329_func = function() {
		carrusel = carrusel + 1;
		var cantbanners = ('cantbanners' in require('vars')) ? require('vars')['cantbanners'] : '';
		if (carrusel % cantbanners != 0 && carrusel % cantbanners != '0') {
			$.vistaNavegable.moveNext();
		} else {
			$.vistaNavegable.scrollToView(0);
		}
		if (ID_1856920329_continuar == true) {
			_out_vars['ID_1856920329']._run = setTimeout(ID_1856920329_func, 1000 * 5);
		}
	};
	_out_vars['ID_1856920329']._run = setTimeout(ID_1856920329_func, 1000 * 5);
})();

if (OS_IOS || OS_ANDROID) {
	$.CASES.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}