var _bind4section = {};
var _list_templates = {};

try {
	$.CREATE_ACCOUNT.setTitleAttributes({
		color: 'WHITE'
	});
} catch (err_ios) {};
var _activity;
if (OS_ANDROID) {
	_activity = $.CREATE_ACCOUNT.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'CREATE_ACCOUNT';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.CREATE_ACCOUNT.addEventListener('open', function(e) {
		abx.setStatusbarColor("#FFFFFF");
		abx.setBackgroundColor("#2d9edb");
	});
}

function Click_vista(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.CREATE_ACCOUNT.close();

}

function Touchstart_vista4(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	/** 
	 * Modificamos el color de fondo del boton 
	 */
	$.vista4.setBackgroundColor('#43dddd');

}

function Touchend_vista4(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	/** 
	 * Modificamos el color de fondo del boton 
	 */
	var vista4_estilo = 'fondoverde';

	var setEstilo = function(clase) {
		if ('styles' in require('a4w') && clase in require('a4w').styles['classes']) {
			try {
				$.vista4.applyProperties(require('a4w').styles['classes'][clase]);
			} catch (sete_err) {}
		}
	};
	setEstilo(vista4_estilo);

	if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
		/** 
		 * Revisamos si el celular tiene internet para poder enviar los datos del usuario y registrarse 
		 */
		/** 
		 * Recuperamos la variable con todos los datos de registo y la url de crawford 
		 */
		var newuser = ('newuser' in require('vars')) ? require('vars')['newuser'] : '';
		var urlcrawford = ('urlcrawford' in require('vars')) ? require('vars')['urlcrawford'] : '';
		/** 
		 * Obtenemos el id unico de cada celular 
		 */
		var uuid = Titanium.Platform.id;
		if (Ti.App.deployType != 'production') console.log('datosenviar', {
			"asd": newuser,
			"uuid": uuid
		});
		/** 
		 * Hacemos la consulta al servidor 
		 */
		var consultarURL = {};

		consultarURL.success = function(e) {
			var elemento = e,
				valor = e;
			if (Ti.App.deployType != 'production') console.log('resultado registraruser', {
				"asd": elemento
			});
			if (elemento.error == 0 || elemento.error == '0') {
				/** 
				 * Dependiendo de la respuesta del servidor podremos saber si la informacion enviada fue un registro exitoso o ya habia sido ocupado el mail 
				 */
				var preguntarAlerta_opts = [L('x3610695981_traducir', 'OK')];
				var preguntarAlerta = Ti.UI.createAlertDialog({
					title: L('x3769205873_traducir', 'ALERT'),
					message: L('x642300599_traducir', 'SUCCESFUL REGISTER'),
					buttonNames: preguntarAlerta_opts
				});
				preguntarAlerta.addEventListener('click', function(e) {
					var xdd = preguntarAlerta_opts[e.index];
					xdd = null;

					e.source.removeEventListener("click", arguments.callee);
				});
				preguntarAlerta.show();

				Alloy.Events.trigger('cerrar_registro');
			} else if (elemento.error == 1040) {
				var preguntarAlerta2_opts = [L('x3610695981_traducir', 'OK')];
				var preguntarAlerta2 = Ti.UI.createAlertDialog({
					title: L('x2861137601_traducir', 'ERROR'),
					message: L('x2719848596_traducir', 'USER ALREADY EXISTS'),
					buttonNames: preguntarAlerta2_opts
				});
				preguntarAlerta2.addEventListener('click', function(e) {
					var xdd = preguntarAlerta2_opts[e.index];
					xdd = null;

					e.source.removeEventListener("click", arguments.callee);
				});
				preguntarAlerta2.show();
			} else {
				var preguntarAlerta3_opts = [L('x3610695981_traducir', 'OK')];
				var preguntarAlerta3 = Ti.UI.createAlertDialog({
					title: L('x2861137601_traducir', 'ERROR'),
					message: L('x3266792483_traducir', 'SERVER PROBLEM'),
					buttonNames: preguntarAlerta3_opts
				});
				preguntarAlerta3.addEventListener('click', function(e) {
					var xdd = preguntarAlerta3_opts[e.index];
					xdd = null;

					e.source.removeEventListener("click", arguments.callee);
				});
				preguntarAlerta3.show();
			}
			/** 
			 * Escondemos la imagen de progreso 
			 */
			$.progreso.hide();
			var AGREE_visible = true;

			if (AGREE_visible == 'si') {
				AGREE_visible = true;
			} else if (AGREE_visible == 'no') {
				AGREE_visible = false;
			}
			$.AGREE.setVisible(AGREE_visible);

			elemento = null, valor = null;
		};

		consultarURL.error = function(e) {
			var elemento = e,
				valor = e;
			var preguntarAlerta4_opts = [L('x3610695981_traducir', 'OK')];
			var preguntarAlerta4 = Ti.UI.createAlertDialog({
				title: L('x3769205873_traducir', 'ALERT'),
				message: L('x1755017309_traducir', 'ERROR CONNECTING TO THE SERVER'),
				buttonNames: preguntarAlerta4_opts
			});
			preguntarAlerta4.addEventListener('click', function(e) {
				var zcx = preguntarAlerta4_opts[e.index];
				zcx = null;

				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta4.show();
			elemento = null, valor = null;
		};
		require('helper').ajaxUnico('consultarURL', '' + String.format(L('x2734794399', '%1$sregistrarUsuario/'), urlcrawford.toString()) + '', 'POST', {
			pais: newuser.country,
			correo: newuser.email,
			nombre: newuser.name,
			apellido_paterno: newuser.middlename,
			apellido_materno: newuser.lastname,
			rut: newuser.codeidentifier,
			fecha_nacimiento: newuser.birthdate,
			password: newuser.password,
			telefono: newuser.phonenumber,
			uuid: uuid
		}, 15000, consultarURL);
		/** 
		 * Mostramos la imagen de progreso 
		 */
		$.progreso.show();
		var AGREE_visible = false;

		if (AGREE_visible == 'si') {
			AGREE_visible = true;
		} else if (AGREE_visible == 'no') {
			AGREE_visible = false;
		}
		$.AGREE.setVisible(AGREE_visible);

	} else {
		var preguntarAlerta5_opts = [L('x3610695981_traducir', 'OK')];
		var preguntarAlerta5 = Ti.UI.createAlertDialog({
			title: L('x2861137601_traducir', 'ERROR'),
			message: L('x364799750_traducir', 'CHECK YOUR INTERNET CONNECTION'),
			buttonNames: preguntarAlerta5_opts
		});
		preguntarAlerta5.addEventListener('click', function(e) {
			var xdd = preguntarAlerta5_opts[e.index];
			xdd = null;

			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta5.show();
	}
}

(function() {

	_my_events['cerrar_registro,ID_401972574'] = function(evento) {
		$.CREATE_ACCOUNT.close();
	};
	Alloy.Events.on('cerrar_registro', _my_events['cerrar_registro,ID_401972574']);
})();

if (OS_IOS || OS_ANDROID) {
	$.CREATE_ACCOUNT.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}
//$.CREATE_ACCOUNT.open();
