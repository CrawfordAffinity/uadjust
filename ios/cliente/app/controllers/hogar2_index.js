var _bind4section = {};
var _list_templates = {};

$.HOME_window.setTitleAttributes({
	color: 'WHITE'
});
var _activity;
if (OS_ANDROID) {
	_activity = $.HOME.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'HOME';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.HOME_window.addEventListener('open', function(e) {
		abx.setStatusbarColor("#FFFFFF");
		abx.setBackgroundColor("#2d9edb");
	});
}

function Click_vista(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var ID_693507722_detener = true;

	var detenerRepetir = function(mula) {
		ID_693507722_continuar = false;
		clearTimeout(_out_vars['ID_693507722']._run);
	};
	detenerRepetir(ID_693507722_detener);

	$.HOME.close();

}

$.widgetBarracasos.init({
	__id: 'ALL1422803942',
	paso: L('x450215437', '2'),
	titulo5: L('x951154001_traducir', 'End'),
	titulo1: L('x807033745_traducir', 'Received'),
	titulo4: L('x1018769216_traducir', 'Evaluating'),
	titulo3: L('x4262956787_traducir', 'Inspected'),
	titulo2: L('x807001066_traducir', 'Scheduled')
});


$.widgetCajadescripcion.init({
	titulo: L('x3950563313_traducir', 'Description'),
	__id: 'ALL1926115404'
});

function Click_vista12(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.vista13.animate({
		bottom: 0
	});

}

$.pinchoMapa.applyProperties({
	latitude: parseFloat('0.0'),
	longitude: parseFloat('0.0'),
	image: '/images/i4044ADFFA4209653B5E04223DF297AE5.png'
});


$.pinchoMapa2.applyProperties({
	latitude: parseFloat('0.0'),
	longitude: parseFloat('0.0'),
	image: '/images/iBB546CC4D5E99A8A1B3E5A1BDEA91412.png'
});


$.mapa.setRegion({
	latitude: 0,
	longitude: 0,
	latitudeDelta: 0.005,
	longitudeDelta: 0.005
});
$.mapa.applyProperties({});
if (OS_IOS) {
	var mapa_camara = require('ti.map').createCamera({
		pitch: 0,
		heading: 0
	});
	$.mapa.setCamera(mapa_camara);
}

function Click_imagen2(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var evento = e;
	$.vista13.animate({
		bottom: '-80%'
	});

}

(function() {
	var urlcrawford = ('urlcrawford' in require('vars')) ? require('vars')['urlcrawford'] : '';
	var urluadjust = ('urluadjust' in require('vars')) ? require('vars')['urluadjust'] : '';
	if (Ti.App.deployType != 'production') console.log('hogar2', {
		"datos": args
	});
	if ((_.isObject(args) || (_.isString(args)) && !_.isEmpty(args)) || _.isNumber(args) || _.isBoolean(args)) {
		var siniestro = args._data;
		var consultarModelo_i = Alloy.createCollection('casos');
		var consultarModelo_i_where = 'siniestro=\'' + siniestro + '\'';
		consultarModelo_i.fetch({
			query: 'SELECT * FROM casos WHERE siniestro=\'' + siniestro + '\''
		});
		var datos = require('helper').query2array(consultarModelo_i);
		datos = datos[0];
		if (Ti.App.deployType != 'production') console.log('contiene datos', {
			"asd": datos
		});
		if (Ti.App.deployType != 'production') console.log('compania', {
			"asd": datos.compania
		});
		$.Label2.setText(datos.compania);

		if ((_.isObject(datos.inspector) || (_.isString(datos.inspector)) && !_.isEmpty(datos.inspector)) || _.isNumber(datos.inspector) || _.isBoolean(datos.inspector)) {
			var assigned = L('x1366852108_traducir', 'Assigned inspector');
			if (Ti.App.deployType != 'production') console.log('asigne a alguien', {
				"asd": datos.inspector
			});
			$.UnassignedInspector.setText(assigned);

			$.Assignmentin.setText(datos.inspector);

			if ((_.isObject(datos.fecha_agendamiento) || (_.isString(datos.fecha_agendamiento)) && !_.isEmpty(datos.fecha_agendamiento)) || _.isNumber(datos.fecha_agendamiento) || _.isBoolean(datos.fecha_agendamiento)) {
				$.label4.setText(datos.fecha_agendamiento);

				var vista10_visible = true;

				if (vista10_visible == 'si') {
					vista10_visible = true;
				} else if (vista10_visible == 'no') {
					vista10_visible = false;
				}
				$.vista10.setVisible(vista10_visible);

			}
			var consultarURL = {};

			consultarURL.success = function(e) {
				var elemento = e,
					valor = e;
				if (elemento.error == 0 || elemento.error == '0') {
					if ((_.isObject(elemento.inspector) || (_.isString(elemento.inspector)) && !_.isEmpty(elemento.inspector)) || _.isNumber(elemento.inspector) || _.isBoolean(elemento.inspector)) {
						var vista11_visible = true;

						if (vista11_visible == 'si') {
							vista11_visible = true;
						} else if (vista11_visible == 'no') {
							vista11_visible = false;
						}
						$.vista11.setVisible(vista11_visible);

						inspector = elemento.inspector.split(",");
						tarea = elemento.tarea.split(",");
						$.pinchoMapa2.setLatitude(inspector[0]);

						$.pinchoMapa2.setLongitude(inspector[1]);

						$.pinchoMapa.setLatitude(tarea[0]);

						$.pinchoMapa.setLongitude(tarea[1]);

						var mapa_latitud = tarea[0];

						var mapa_tmp = $.mapa.getRegion();
						mapa_tmp.latitude = mapa_latitud;
						mapa_latitud = mapa_tmp;
						$.mapa.setRegion(mapa_latitud);

						var mapa_longitud = tarea[1];

						var mapa_tmp = $.mapa.getRegion();
						mapa_tmp.longitude = mapa_longitud;
						mapa_longitud = mapa_tmp;
						$.mapa.setRegion(mapa_longitud);

						$.Min.setText(elemento.duracion);

						require('vars')['duracion_seg'] = elemento.duracion_seg;

						var rutaMapa_poly = require('polyline').decode(elemento.polyline);
						var varruta = require('ti.map').createRoute({
							width: 6,
							color: '#2d9edb',
							points: rutaMapa_poly
						});
						if (Ti.App.deployType != 'production') console.log('instanciada la ruta 1vez', {});
						$.mapa.addRoute(varruta);

						if (Ti.App.deployType != 'production') console.log('agregada la ruta 1vez', {});
						require('vars')['laruta'] = varruta;
					} else {
						if (Ti.App.deployType != 'production') console.log('los datos estan en blanco', {});
					}
				} else if (elemento.error == 400) {
					if (Ti.App.deployType != 'production') console.log('error obteniendo datos del mapa...datosfaltan', {});
				}
				if (Ti.App.deployType != 'production') console.log('resp_ubicacion', {
					"asd": elemento
				});
				elemento = null, valor = null;
			};

			consultarURL.error = function(e) {
				var elemento = e,
					valor = e;
				var preguntarAlerta_opts = [L('x3610695981_traducir', 'OK')];
				var preguntarAlerta = Ti.UI.createAlertDialog({
					title: L('x3769205873_traducir', 'ALERT'),
					message: L('x4290892430_traducir', 'ERROR OBTAINING THE MAP DATA'),
					buttonNames: preguntarAlerta_opts
				});
				preguntarAlerta.addEventListener('click', function(e) {
					var zxc = preguntarAlerta_opts[e.index];
					zxc = null;

					e.source.removeEventListener("click", arguments.callee);
				});
				preguntarAlerta.show();
				elemento = null, valor = null;
			};
			require('helper').ajaxUnico('consultarURL', '' + String.format(L('x2319149298', '%1$sobtenerUbicacion/'), urluadjust.toString()) + '', 'POST', {
				ci_inspector: datos.rut_inspector,
				num_caso: datos.num_caso
			}, 15000, consultarURL);
			var hola = 0;
			var ID_693507722_continuar = true;
			_out_vars['ID_693507722'] = {
				_remove: ["clearTimeout(_out_vars['ID_693507722']._run)"] 
			};
			var ID_693507722_func = function() {
				hola = hola + 1;
				var duracion_seg = ('duracion_seg' in require('vars')) ? require('vars')['duracion_seg'] : '';
				if (Ti.App.deployType != 'production') console.log('me wa caer', {});
				if (_.isNumber(duracion_seg) && _.isNumber(60) && duracion_seg > 60) {
					require('vars')['duracion_seg'] = duracion_seg;

					var moment = require('alloy/moment');
					duracion_seg = duracion_seg - 60;
					var now = moment.utc(duracion_seg * 1000).format('HH[h]:mm[m]');
					$.Min.setText(now);

					require('vars')['duracion'] = duracion_seg;
					var consultarURL2 = {};

					consultarURL2.success = function(e) {
						var elemento = e,
							valor = e;
						if ((_.isObject(elemento.inspector) || (_.isString(elemento.inspector)) && !_.isEmpty(elemento.inspector)) || _.isNumber(elemento.inspector) || _.isBoolean(elemento.inspector)) {
							if (Ti.App.deployType != 'production') console.log('repitientdo ubucacion', {});
							var vista11_visible = true;

							if (vista11_visible == 'si') {
								vista11_visible = true;
							} else if (vista11_visible == 'no') {
								vista11_visible = false;
							}
							$.vista11.setVisible(vista11_visible);

							inspector = elemento.inspector.split(",");
							tarea = elemento.tarea.split(",");
							$.pinchoMapa2.setLatitude(inspector[0]);

							$.pinchoMapa2.setLongitude(inspector[1]);

							$.pinchoMapa.setLatitude(tarea[0]);

							$.pinchoMapa.setLongitude(tarea[1]);

							$.Min.setText(elemento.duracion);

							var laruta = ('laruta' in require('vars')) ? require('vars')['laruta'] : '';
							if (_.isObject(laruta)) {
								if (Ti.App.deployType != 'production') console.log('es objeto', {});
								$.mapa.removeRoute(laruta);

								if (Ti.App.deployType != 'production') console.log('borrando ruta', {});
							} else {
								if (Ti.App.deployType != 'production') console.log('no es objeto', {});
							}
							var rutaMapa2_poly = require('polyline').decode(elemento.polyline);
							var varruta = require('ti.map').createRoute({
								width: 6,
								color: '#2d9edb',
								points: rutaMapa2_poly
							});
							if (Ti.App.deployType != 'production') console.log('instanciada nueva ruta', {});
							require('vars')['laruta'] = varruta;
							$.mapa.addRoute(varruta);

							if (Ti.App.deployType != 'production') console.log('agregada la ruta nueva', {});
						}
						elemento = null, valor = null;
					};

					consultarURL2.error = function(e) {
						var elemento = e,
							valor = e;
						var preguntarAlerta2_opts = [L('x3610695981_traducir', 'OK')];
						var preguntarAlerta2 = Ti.UI.createAlertDialog({
							title: L('x3769205873_traducir', 'ALERT'),
							message: L('x4290892430_traducir', 'ERROR OBTAINING THE MAP DATA'),
							buttonNames: preguntarAlerta2_opts
						});
						preguntarAlerta2.addEventListener('click', function(e) {
							var zxc = preguntarAlerta2_opts[e.index];
							zxc = null;

							e.source.removeEventListener("click", arguments.callee);
						});
						preguntarAlerta2.show();
						elemento = null, valor = null;
					};
					require('helper').ajaxUnico('consultarURL2', '' + String.format(L('x2319149298', '%1$sobtenerUbicacion/'), urluadjust.toString()) + '', 'POST', {
						ci_inspector: datos.rut_inspector,
						num_caso: datos.num_caso
					}, 15000, consultarURL2);
				} else {}
				if (ID_693507722_continuar == true) {
					_out_vars['ID_693507722']._run = setTimeout(ID_693507722_func, 1000 * 20);
				}
			};
			_out_vars['ID_693507722']._run = setTimeout(ID_693507722_func, 1000 * 20);
			$.Label5.setText(datos.rut_inspector);

			if ((_.isObject(datos.imagen_inspector) || (_.isString(datos.imagen_inspector)) && !_.isEmpty(datos.imagen_inspector)) || _.isNumber(datos.imagen_inspector) || _.isBoolean(datos.imagen_inspector)) {
				var imageBlob = Ti.Utils.base64decode(datos.imagen_inspector);
				var imagen_imagen = imageBlob;

				if (typeof imagen_imagen == 'string' && 'styles' in require('a4w') && imagen_imagen in require('a4w').styles['images']) {
					imagen_imagen = require('a4w').styles['images'][imagen_imagen];
				}
				$.imagen.setImage(imagen_imagen);

			}
		}
		var days = L('x3957652582_traducir', 'days');
		var legal = L('x2139489547_traducir', 'legal:');
		var uadjust = L('x1807599243_traducir', 'uadjust:');
		$.Label3.setText(String.format(L('x3000044032', '%1$s %2$s %3$s %4$s %5$s %6$s'), (legal) ? legal.toString() : '', (datos.legal) ? datos.legal.toString() : '', (days) ? days.toString() : '', (uadjust) ? uadjust.toString() : '', (datos.estimado) ? datos.estimado.toString() : '', (days) ? days.toString() : ''));

	}
	$.scroll.setShowVerticalScrollIndicator(false);
	var hog2 = L('x1879867758_traducir', 'deschogar2');

	$.widgetCajadescripcion.texto({
		valor: L('x2117476365_traducir', 'hog2')
	});
})();

if (OS_IOS || OS_ANDROID) {
	$.HOME.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}
//$.HOME.open();
