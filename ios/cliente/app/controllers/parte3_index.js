var _bind4section = {};
var _list_templates = {};

$.CREATE_ACCOUNT_window.setTitleAttributes({
	color: 'WHITE'
});
var _activity;
if (OS_ANDROID) {
	_activity = $.CREATE_ACCOUNT.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'CREATE_ACCOUNT';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.CREATE_ACCOUNT_window.addEventListener('open', function(e) {
		abx.setStatusbarColor("#FFFFFF");
		abx.setBackgroundColor("#2d9edb");
	});
}

function Click_vista(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.CREATE_ACCOUNT.close();

}

function Touchstart_vista9(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	/** 
	 * Modificamos el color de fondo del boton 
	 */
	$.vista9.setBackgroundColor('#f7ae57');

}

function Touchend_vista9(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	/** 
	 * Modificamos el color de fondo del boton 
	 */
	var vista9_estilo = 'fondoamarillo';

	var setEstilo = function(clase) {
		if ('styles' in require('a4w') && clase in require('a4w').styles['classes']) {
			try {
				$.vista9.applyProperties(require('a4w').styles['classes'][clase]);
			} catch (sete_err) {}
		}
	};
	setEstilo(vista9_estilo);

	var email;
	email = $.WriteEmail.getValue();

	var repeatedmail;
	repeatedmail = $.RepeatEmail2.getValue();

	var password;
	password = $.WritePassword.getValue();

	var repeatpassword;
	repeatpassword = $.RepeatPassword2.getValue();

	var phonenumber;
	phonenumber = $.Xxxxxxxx.getValue();

	var repeatphonenumber;
	repeatphonenumber = $.Xxxxxxxx2.getValue();

	/** 
	 * Agregamos RegEx para verificar que los campos son validos. En este caso validar el que el mail tenga formato correcto y que el numero de telefono es valido 
	 */
	var regemail = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
	var validaremail = regemail.test(email);
	var validarrepeatedmail = regemail.test(email);
	var regnumber = new RegExp(/[0-9]/);
	var validarphonenumber = regnumber.test(phonenumber);
	var validarrepeatedphone = regnumber.test(repeatphonenumber);
	if (Ti.App.deployType != 'production') console.log('telefono', {
		"nro": phonenumber,
		"regex": validarphonenumber
	});
	if (Ti.App.deployType != 'production') console.log('telefonorepetido', {
		"nro": repeatphonenumber,
		"regex": validarrepeatedphone
	});
	if (Ti.App.deployType != 'production') console.log('email', {
		"regex": validaremail,
		"email": email
	});
	if (Ti.App.deployType != 'production') console.log('emailrepetido', {
		"nro": repeatedmail,
		"regex": validarrepeatedmail
	});
	if (validaremail == true || validaremail == 'true') {
		/** 
		 * Revisamos que los campos esten correctos 
		 */
		if (validarrepeatedmail == true || validarrepeatedmail == 'true') {
			if (email == repeatedmail) {
				if (_.isNumber(password.length) && _.isNumber(4) && password.length >= 4) {
					if (repeatpassword == password) {
						if (validarphonenumber == true || validarphonenumber == 'true') {
							if (validarrepeatedphone == true || validarrepeatedphone == 'true') {
								if (_.isNumber(phonenumber.length) && _.isNumber(5) && phonenumber.length > 5) {
									if (repeatphonenumber == phonenumber) {
										/** 
										 * Recuperamos variable con los datos del usuario 
										 */
										var newuser = ('newuser' in require('vars')) ? require('vars')['newuser'] : '';
										if (Ti.App.deployType != 'production') console.log('pantalla3', {
											"asd": newuser
										});
										/** 
										 * Registramos nuevos campos en el objeto 
										 */

										newuser.email = email;
										newuser.password = password;
										newuser.phonenumber = phonenumber;;
										require('vars')['newuser'] = newuser;
										if ("CREATE_ACCOUNT" in Alloy.Globals) {
											Alloy.Globals["CREATE_ACCOUNT"].openWindow(Alloy.createController("parte4_index", {}).getView());
										} else {
											Alloy.Globals["CREATE_ACCOUNT"] = $.CREATE_ACCOUNT;
											Alloy.Globals["CREATE_ACCOUNT"].openWindow(Alloy.createController("parte4_index", {}).getView());
										}

									} else {
										var preguntarAlerta_opts = [L('x3610695981_traducir', 'OK')];
										var preguntarAlerta = Ti.UI.createAlertDialog({
											title: L('x2861137601_traducir', 'ERROR'),
											message: L('x3081519886_traducir', 'PHONE NUMBERS ARENT THE SAME'),
											buttonNames: preguntarAlerta_opts
										});
										preguntarAlerta.addEventListener('click', function(e) {
											var xdd = preguntarAlerta_opts[e.index];
											xdd = null;

											e.source.removeEventListener("click", arguments.callee);
										});
										preguntarAlerta.show();
									}
								} else {
									var preguntarAlerta2_opts = [L('x3610695981_traducir', 'OK')];
									var preguntarAlerta2 = Ti.UI.createAlertDialog({
										title: L('x2861137601_traducir', 'ERROR'),
										message: L('x1908977392_traducir', 'PHONE NUMBER TOO SHORT'),
										buttonNames: preguntarAlerta2_opts
									});
									preguntarAlerta2.addEventListener('click', function(e) {
										var xdd = preguntarAlerta2_opts[e.index];
										xdd = null;

										e.source.removeEventListener("click", arguments.callee);
									});
									preguntarAlerta2.show();
								}
							} else {
								var preguntarAlerta3_opts = [L('x3610695981_traducir', 'OK')];
								var preguntarAlerta3 = Ti.UI.createAlertDialog({
									title: L('x2861137601_traducir', 'ERROR'),
									message: L('x3809972048_traducir', 'INVALID REPEATED PHONE NUMBER'),
									buttonNames: preguntarAlerta3_opts
								});
								preguntarAlerta3.addEventListener('click', function(e) {
									var xdd = preguntarAlerta3_opts[e.index];
									xdd = null;

									e.source.removeEventListener("click", arguments.callee);
								});
								preguntarAlerta3.show();
							}
						} else {
							var preguntarAlerta4_opts = [L('x3610695981_traducir', 'OK')];
							var preguntarAlerta4 = Ti.UI.createAlertDialog({
								title: L('x2861137601_traducir', 'ERROR'),
								message: L('x3583230686_traducir', 'INVALID PHONE NUMBER'),
								buttonNames: preguntarAlerta4_opts
							});
							preguntarAlerta4.addEventListener('click', function(e) {
								var xdd = preguntarAlerta4_opts[e.index];
								xdd = null;

								e.source.removeEventListener("click", arguments.callee);
							});
							preguntarAlerta4.show();
						}
					} else {
						var preguntarAlerta5_opts = [L('x3610695981_traducir', 'OK')];
						var preguntarAlerta5 = Ti.UI.createAlertDialog({
							title: L('x2861137601_traducir', 'ERROR'),
							message: L('x3133968772_traducir', 'EMAILS ARENT THE SAME'),
							buttonNames: preguntarAlerta5_opts
						});
						preguntarAlerta5.addEventListener('click', function(e) {
							var xdd = preguntarAlerta5_opts[e.index];
							xdd = null;

							e.source.removeEventListener("click", arguments.callee);
						});
						preguntarAlerta5.show();
					}
				} else {
					var preguntarAlerta6_opts = [L('x3610695981_traducir', 'OK')];
					var preguntarAlerta6 = Ti.UI.createAlertDialog({
						title: L('x2861137601_traducir', 'ERROR'),
						message: L('x2728743456_traducir', 'PASSWORD TOO SHORT'),
						buttonNames: preguntarAlerta6_opts
					});
					preguntarAlerta6.addEventListener('click', function(e) {
						var xdd = preguntarAlerta6_opts[e.index];
						xdd = null;

						e.source.removeEventListener("click", arguments.callee);
					});
					preguntarAlerta6.show();
				}
			} else {
				var preguntarAlerta7_opts = [L('x3610695981_traducir', 'OK')];
				var preguntarAlerta7 = Ti.UI.createAlertDialog({
					title: L('x2861137601_traducir', 'ERROR'),
					message: L('x3133968772_traducir', 'EMAILS ARENT THE SAME'),
					buttonNames: preguntarAlerta7_opts
				});
				preguntarAlerta7.addEventListener('click', function(e) {
					var xdd = preguntarAlerta7_opts[e.index];
					xdd = null;

					e.source.removeEventListener("click", arguments.callee);
				});
				preguntarAlerta7.show();
				if (Ti.App.deployType != 'production') console.log('repetidomail', {
					"primero": email,
					"segundo": repeatedmail
				});
			}
		} else {
			var preguntarAlerta8_opts = [L('x3610695981_traducir', 'OK')];
			var preguntarAlerta8 = Ti.UI.createAlertDialog({
				title: L('x2861137601_traducir', 'ERROR'),
				message: L('x1808754769_traducir', 'INVALID REPEATED EMAIL'),
				buttonNames: preguntarAlerta8_opts
			});
			preguntarAlerta8.addEventListener('click', function(e) {
				var xdd = preguntarAlerta8_opts[e.index];
				xdd = null;

				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta8.show();
		}
	} else {
		var preguntarAlerta9_opts = [L('x3610695981_traducir', 'OK')];
		var preguntarAlerta9 = Ti.UI.createAlertDialog({
			title: L('x2861137601_traducir', 'ERROR'),
			message: L('x1647703686_traducir', 'INVALID EMAIL'),
			buttonNames: preguntarAlerta9_opts
		});
		preguntarAlerta9.addEventListener('click', function(e) {
			var xdd = preguntarAlerta9_opts[e.index];
			xdd = null;

			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta9.show();
	}
}

function Click_scroll(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.WriteEmail.blur();
	$.RepeatEmail2.blur();
	$.WritePassword.blur();
	$.RepeatPassword2.blur();
	$.Xxxxxxxx.blur();
	$.Xxxxxxxx2.blur();

}

(function() {
	$.scroll.setShowVerticalScrollIndicator(false);

	_my_events['cerrar_registro,ID_471444039'] = function(evento) {
		$.CREATE_ACCOUNT.close();
	};
	Alloy.Events.on('cerrar_registro', _my_events['cerrar_registro,ID_471444039']);
})();

if (OS_IOS || OS_ANDROID) {
	$.CREATE_ACCOUNT.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}
//$.CREATE_ACCOUNT.open();
