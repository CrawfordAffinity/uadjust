var _bind4section = {};
var _list_templates = {};

try {
	$.CREATE_ACCOUNT.setTitleAttributes({
		color: 'WHITE'
	});
} catch (err_ios) {};
var _activity;
if (OS_ANDROID) {
	_activity = $.CREATE_ACCOUNT.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'CREATE_ACCOUNT';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.CREATE_ACCOUNT.addEventListener('open', function(e) {
		abx.setStatusbarColor("#FFFFFF");
		abx.setBackgroundColor("#2d9edb");
	});
}

function Click_vista(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.CREATE_ACCOUNT.close();

}

function Click_vista8(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var flag = ('flag' in require('vars')) ? require('vars')['flag'] : '';
	if (flag == L('x2829500202_traducir', 'cerrado')) {
		$.vista10.animate({
			bottom: 0,
			duration: 300
		});
		/** 
		 * Setear un flag para evitar que se abra dos veces la vista 
		 */
		flag = 'abierto';
	}
	$.WriteName.blur();
	$.WriteMiddle.blur();
	$.WriteLastName.blur();
	$.Empty.blur();

}

function Touchstart_vista9(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	/** 
	 * Modificamos el color de fondo del boton 
	 */
	$.vista9.setBackgroundColor('#f7ae57');

}

function Touchend_vista9(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	/** 
	 * Modificamos el color de fondo del boton 
	 */
	var vista9_estilo = 'fondoamarillo';

	var setEstilo = function(clase) {
		if ('styles' in require('a4w') && clase in require('a4w').styles['classes']) {
			try {
				$.vista9.applyProperties(require('a4w').styles['classes'][clase]);
			} catch (sete_err) {}
		}
	};
	setEstilo(vista9_estilo);

	/** 
	 * Recuperamos los valores que fueron ingresado en la pantalla 
	 */
	var name;
	name = $.WriteName.getValue();

	var middlename;
	middlename = $.WriteMiddle.getValue();

	var lastname;
	lastname = $.WriteLastName.getValue();

	var codeidentifier;
	codeidentifier = $.Empty.getValue();

	var birthdate;
	birthdate = $.DDMMYYYY.getText();

	/** 
	 * Recuperamos las variables del flag y fecha nacimiento 
	 */
	var cambiofecha = ('cambiofecha' in require('vars')) ? require('vars')['cambiofecha'] : '';
	var birthdate = ('birthdate' in require('vars')) ? require('vars')['birthdate'] : '';
	if (Ti.App.deployType != 'production') console.log('asd', {
		"asdasd": name.length
	});
	if (_.isNumber(name.length) && _.isNumber(2) && name.length > 2) {
		/** 
		 * Revisamos que los campos solicitados hayan sido llenados, si no, vamos mostrando mensajes de alerta 
		 */
		if (_.isNumber(lastname.length) && _.isNumber(2) && lastname.length > 2) {
			if (_.isNumber(codeidentifier.length) && _.isNumber(2) && codeidentifier.length > 2) {
				if (cambiofecha == true || cambiofecha == 'true') {
					/** 
					 * Recuperamos variable con los datos del usuario 
					 */
					var newuser = ('newuser' in require('vars')) ? require('vars')['newuser'] : '';
					if (Ti.App.deployType != 'production') console.log('pantalla2', {
						"asd": newuser
					});
					/** 
					 * Registramos nuevos campos en el objeto 
					 */

					newuser.name = name;
					newuser.middlename = middlename;
					newuser.lastname = lastname;
					newuser.codeidentifier = codeidentifier;
					newuser.birthdate = birthdate;
					require('vars')['newuser'] = newuser;
					Alloy.createController("parte3_index", {}).getView().open();
				} else {
					var preguntarAlerta_opts = [L('x3610695981_traducir', 'OK')];
					var preguntarAlerta = Ti.UI.createAlertDialog({
						title: L('x3769205873_traducir', 'ALERT'),
						message: L('x2674993850_traducir', 'CHOOSE A DATE TO BIRTHDATE'),
						buttonNames: preguntarAlerta_opts
					});
					preguntarAlerta.addEventListener('click', function(e) {
						var zxc = preguntarAlerta_opts[e.index];
						zxc = null;

						e.source.removeEventListener("click", arguments.callee);
					});
					preguntarAlerta.show();
				}
			} else {
				var preguntarAlerta2_opts = [L('x3610695981_traducir', 'OK')];
				var preguntarAlerta2 = Ti.UI.createAlertDialog({
					title: L('x3769205873_traducir', 'ALERT'),
					message: L('x2853287514_traducir', 'CODE IDENTIFIER TOO SHORT'),
					buttonNames: preguntarAlerta2_opts
				});
				preguntarAlerta2.addEventListener('click', function(e) {
					var zxc = preguntarAlerta2_opts[e.index];
					zxc = null;

					e.source.removeEventListener("click", arguments.callee);
				});
				preguntarAlerta2.show();
			}
		} else {
			var preguntarAlerta3_opts = [L('x3610695981_traducir', 'OK')];
			var preguntarAlerta3 = Ti.UI.createAlertDialog({
				title: L('x3769205873_traducir', 'ALERT'),
				message: L('x3610468735_traducir', 'LAST NAME TOO SHORT'),
				buttonNames: preguntarAlerta3_opts
			});
			preguntarAlerta3.addEventListener('click', function(e) {
				var zxc = preguntarAlerta3_opts[e.index];
				zxc = null;

				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta3.show();
		}
	} else {
		var preguntarAlerta4_opts = [L('x3610695981_traducir', 'OK')];
		var preguntarAlerta4 = Ti.UI.createAlertDialog({
			title: L('x3769205873_traducir', 'ALERT'),
			message: L('x2847864425_traducir', 'NAME TOO SHORT'),
			buttonNames: preguntarAlerta4_opts
		});
		preguntarAlerta4.addEventListener('click', function(e) {
			var zxc = preguntarAlerta4_opts[e.index];
			zxc = null;

			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta4.show();
	}
}

function Click_vista13(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.vista10.animate({
		bottom: -265,
		duration: 300
	});
	flag = 'cerrado';
	require('vars')['flag'] = L('x2829500202_traducir', 'cerrado');

}

function Click_vista14(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.vista10.animate({
		bottom: -265,
		duration: 300
	});
	/** 
	 * Seteamos el flag para evitar que se cierre dos veces 
	 */
	flag = 'cerrado';
	require('vars')['flag'] = L('x2829500202_traducir', 'cerrado');
	/** 
	 * Obtenemos el valor que fue seleccionado en la ruleta 
	 */
	var fecha;
	fecha = $.pickerFecha.value;

	if (Ti.App.deployType != 'production') console.log('dato', {
		"asd": fecha
	});
	var newuser = ('newuser' in require('vars')) ? require('vars')['newuser'] : '';
	/** 
	 * Guardamos la fecha de nacimiento como es solicitado en el servidor 
	 */
	var para_servidor = null;
	if ('formatear_fecha' in require('funciones')) {
		para_servidor = require('funciones').formatear_fecha({
			'fecha': fecha,
			'formato': L('x2933785305_traducir', 'DD/MM/YYYY')
		});
	} else {
		try {
			para_servidor = f_formatear_fecha({
				'fecha': fecha,
				'formato': L('x2933785305_traducir', 'DD/MM/YYYY')
			});
		} catch (ee) {}
	}
	if (newuser.country == L('x1273338369_traducir', 'Estados Unidos')) {
		/** 
		 * Dependiendo del pais seleccionado se modificaran los textos para definir el id... Ejemplo RUT, SAT, SSN 
		 */
		var nuevo = null;
		if ('formatear_fecha' in require('funciones')) {
			nuevo = require('funciones').formatear_fecha({
				'fecha': fecha,
				'formato': L('x1291815763_traducir', 'MM/DD/YYYY')
			});
		} else {
			try {
				nuevo = f_formatear_fecha({
					'fecha': fecha,
					'formato': L('x1291815763_traducir', 'MM/DD/YYYY')
				});
			} catch (ee) {}
		}
		if (Ti.App.deployType != 'production') console.log('formateada', {
			"asd": nuevo
		});
		$.DDMMYYYY.setText(nuevo);

		$.DDMMYYYY.setColor('#000000');

	} else {
		var nuevo = null;
		if ('formatear_fecha' in require('funciones')) {
			nuevo = require('funciones').formatear_fecha({
				'fecha': fecha,
				'formato': L('x2933785305_traducir', 'DD/MM/YYYY')
			});
		} else {
			try {
				nuevo = f_formatear_fecha({
					'fecha': fecha,
					'formato': L('x2933785305_traducir', 'DD/MM/YYYY')
				});
			} catch (ee) {}
		}
		if (Ti.App.deployType != 'production') console.log('formateada', {
			"asd": nuevo
		});
		$.DDMMYYYY.setText(nuevo);

		$.DDMMYYYY.setColor('#000000');

	}
	require('vars')['birthdate'] = para_servidor;
	require('vars')['cambiofecha'] = L('x4261170317', 'true');

}



(function() {
	$.scroll.setShowVerticalScrollIndicator(false);
	require('vars')['cambiofecha'] = L('x734881840_traducir', 'false');
	require('vars')['flag'] = L('x2829500202_traducir', 'cerrado');
	var write = L('x2104195679_traducir', 'write');
	var newuser = ('newuser' in require('vars')) ? require('vars')['newuser'] : '';
	if (newuser.country == L('x2490715067_traducir', 'Chile')) {
		$.CodeIdentifier.setText('RUT');

		$.Empty.setHintText(String.format(L('x3357650813', '%1$s RUT'), write.toString()));
		$.DDMMYYYY.setText('DD/MM/YYYY');

	} else if (newuser.country == L('x1273338369_traducir', 'Estados Unidos')) {
		$.CodeIdentifier.setText('SSN');

		$.Empty.setHintText(String.format(L('x3292352113', '%1$s SSN'), write.toString()));
		$.DDMMYYYY.setText('MM/DD/YYYY');

	} else if (newuser.country == L('x696679517_traducir', 'Mexico')) {
		$.CodeIdentifier.setText('SAT');

		$.Empty.setHintText(String.format(L('x1806877620', '%1$s SAT'), write.toString()));
		$.DDMMYYYY.setText('DD/MM/YYYY');

	}
	_my_events['cerrar_registro,ID_471444039'] = function(evento) {
		$.CREATE_ACCOUNT.close();
	};
	Alloy.Events.on('cerrar_registro', _my_events['cerrar_registro,ID_471444039']);
})();

if (OS_IOS || OS_ANDROID) {
	$.CREATE_ACCOUNT.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}
//$.CREATE_ACCOUNT.open();
