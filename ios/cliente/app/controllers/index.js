var _bind4section = {};
var _list_templates = {};

var _activity;
if (OS_ANDROID) {
	_activity = $.LOGIN.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'LOGIN';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.LOGIN.addEventListener('open', function(e) {
		abx.setStatusbarColor("#FFFFFF");
	});
}

function Load_imagen(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var evento = e;
	elemento.start();

}

function Click_vista5(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	Alloy.createController("parte1_index", {}).getView().open();

}

function Click_vista6(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	Alloy.createController("passwordrecover_index", {}).getView().open();

}

function Touchstart_vista7(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.vista7.setBackgroundColor('#f7ae57');

}

function Touchend_vista7(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var vista7_estilo = 'fondoamarillo';

	var setEstilo = function(clase) {
		if ('styles' in require('a4w') && clase in require('a4w').styles['classes']) {
			try {
				$.vista7.applyProperties(require('a4w').styles['classes'][clase]);
			} catch (sete_err) {}
		}
	};
	setEstilo(vista7_estilo);

	if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
		/** 
		 * Revisamos si hay internet 
		 */
		var email;
		email = $.YourEmail.getValue();

		if ((_.isObject(email) || (_.isString(email)) && !_.isEmpty(email)) || _.isNumber(email) || _.isBoolean(email)) {
			/** 
			 * Validamos que el mail, y password exista, consultamos al servidor los datos y si esta bien, ingresamos al menu con los casos. Ademas de almacenar el correo, nombre rut y fecha de nacimiento en el modelo usuario 
			 */
			var regemail = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
			var validaremail = regemail.test(email);
			if (validaremail == true || validaremail == 'true') {
				var password;
				password = $.YourPassword.getValue();

				if (_.isNumber(password.length) && _.isNumber(3) && password.length > 3) {
					var urlcrawford = ('urlcrawford' in require('vars')) ? require('vars')['urlcrawford'] : '';
					var uuid = Titanium.Platform.id;
					var device_token = ('device_token' in require('vars')) ? require('vars')['device_token'] : '';
					if (Ti.App.deployType != 'production') console.log('voy a enviar', {
						"dtoken": device_token,
						"uuid": uuid,
						"password": password,
						"mail": email
					});
					var consultarURL = {};

					consultarURL.success = function(e) {
						var elemento = e,
							valor = e;
						if ((_.isObject(elemento.nombre) || (_.isString(elemento.nombre)) && !_.isEmpty(elemento.nombre)) || _.isNumber(elemento.nombre) || _.isBoolean(elemento.nombre)) {
							var eliminarModelo_i = Alloy.Collections.usuario;
							var sql = "DELETE FROM " + eliminarModelo_i.config.adapter.collection_name;
							var db = Ti.Database.open(eliminarModelo_i.config.adapter.db_name);
							db.execute(sql);
							db.close();
							eliminarModelo_i.trigger('remove');
							var insertarModelo_m = Alloy.Collections.usuario;
							var insertarModelo_fila = Alloy.createModel('usuario', {
								birthdate: elemento.fecha_nacimiento,
								name: String.format(L('x1445533071', '%1$s %2$s %3$s'), (elemento.nombre) ? elemento.nombre.toString() : '', (elemento.apellido_paterno) ? elemento.apellido_paterno.toString() : '', (elemento.apellido_materno) ? elemento.apellido_materno.toString() : ''),
								rut: elemento.rut,
								email: elemento.correo
							});
							insertarModelo_m.add(insertarModelo_fila);
							insertarModelo_fila.save();
							Alloy.createController("menu_index", {}).getView().open();
							$.progreso.hide();
							var SIGNIN_visible = true;

							if (SIGNIN_visible == 'si') {
								SIGNIN_visible = true;
							} else if (SIGNIN_visible == 'no') {
								SIGNIN_visible = false;
							}
							$.SIGNIN.setVisible(SIGNIN_visible);

						} else {
							var preguntarAlerta_opts = [L('x3610695981_traducir', 'OK')];
							var preguntarAlerta = Ti.UI.createAlertDialog({
								title: L('x3769205873_traducir', 'ALERT'),
								message: L('x3594877300_traducir', 'USER OR PASSWORD INVALID'),
								buttonNames: preguntarAlerta_opts
							});
							preguntarAlerta.addEventListener('click', function(e) {
								var xdx = preguntarAlerta_opts[e.index];
								xdx = null;

								e.source.removeEventListener("click", arguments.callee);
							});
							preguntarAlerta.show();
							$.progreso.hide();
							var SIGNIN_visible = true;

							if (SIGNIN_visible == 'si') {
								SIGNIN_visible = true;
							} else if (SIGNIN_visible == 'no') {
								SIGNIN_visible = false;
							}
							$.SIGNIN.setVisible(SIGNIN_visible);

						}
						if (Ti.App.deployType != 'production') console.log('respuesta login', {
							"asd": elemento
						});
						elemento = null, valor = null;
					};

					consultarURL.error = function(e) {
						var elemento = e,
							valor = e;
						var preguntarAlerta2_opts = [L('x3610695981_traducir', 'OK')];
						var preguntarAlerta2 = Ti.UI.createAlertDialog({
							title: L('x3769205873_traducir', 'ALERT'),
							message: L('x904051258_traducir', 'ERROR CONNECTING'),
							buttonNames: preguntarAlerta2_opts
						});
						preguntarAlerta2.addEventListener('click', function(e) {
							var zxc = preguntarAlerta2_opts[e.index];
							zxc = null;

							e.source.removeEventListener("click", arguments.callee);
						});
						preguntarAlerta2.show();
						$.progreso.hide();
						var SIGNIN_visible = true;

						if (SIGNIN_visible == 'si') {
							SIGNIN_visible = true;
						} else if (SIGNIN_visible == 'no') {
							SIGNIN_visible = false;
						}
						$.SIGNIN.setVisible(SIGNIN_visible);

						elemento = null, valor = null;
					};
					require('helper').ajaxUnico('consultarURL', '' + String.format(L('x3079903409', '%1$slogin/'), urlcrawford.toString()) + '', 'POST', {
						correo: email,
						password: password,
						uuid: uuid,
						device_token: device_token
					}, 15000, consultarURL);
					$.progreso.show();
					var SIGNIN_visible = false;

					if (SIGNIN_visible == 'si') {
						SIGNIN_visible = true;
					} else if (SIGNIN_visible == 'no') {
						SIGNIN_visible = false;
					}
					$.SIGNIN.setVisible(SIGNIN_visible);

				} else {
					var preguntarAlerta3_opts = [L('x3610695981_traducir', 'OK')];
					var preguntarAlerta3 = Ti.UI.createAlertDialog({
						title: L('x3769205873_traducir', 'ALERT'),
						message: L('x2728743456_traducir', 'PASSWORD TOO SHORT'),
						buttonNames: preguntarAlerta3_opts
					});
					preguntarAlerta3.addEventListener('click', function(e) {
						var zxc = preguntarAlerta3_opts[e.index];
						zxc = null;

						e.source.removeEventListener("click", arguments.callee);
					});
					preguntarAlerta3.show();
				}
			} else {
				var preguntarAlerta4_opts = [L('x3610695981_traducir', 'OK')];
				var preguntarAlerta4 = Ti.UI.createAlertDialog({
					title: L('x2861137601_traducir', 'ERROR'),
					message: L('x1647703686_traducir', 'INVALID EMAIL'),
					buttonNames: preguntarAlerta4_opts
				});
				preguntarAlerta4.addEventListener('click', function(e) {
					var xdd = preguntarAlerta4_opts[e.index];
					xdd = null;

					e.source.removeEventListener("click", arguments.callee);
				});
				preguntarAlerta4.show();
			}
		} else {
			var preguntarAlerta5_opts = [L('x3610695981_traducir', 'OK')];
			var preguntarAlerta5 = Ti.UI.createAlertDialog({
				title: L('x2861137601_traducir', 'ERROR'),
				message: L('x1676137616_traducir', 'EMPTY EMAIL'),
				buttonNames: preguntarAlerta5_opts
			});
			preguntarAlerta5.addEventListener('click', function(e) {
				var xd = preguntarAlerta5_opts[e.index];
				xd = null;

				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta5.show();
		}
	} else {
		var preguntarAlerta6_opts = [L('x3610695981_traducir', 'OK')];
		var preguntarAlerta6 = Ti.UI.createAlertDialog({
			title: L('x3769205873_traducir', 'ALERT'),
			message: L('x1867626468_traducir', 'YOU DONT HAVE INTERNET CONNECTION'),
			buttonNames: preguntarAlerta6_opts
		});
		preguntarAlerta6.addEventListener('click', function(e) {
			var asd = preguntarAlerta6_opts[e.index];
			asd = null;

			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta6.show();
	}
}

(function() {
	/** 
	 * Cambiamos ancho de vista de registro 
	 */
	var vista5_ancho = '-';

	if (vista5_ancho == '*') {
		vista5_ancho = Ti.UI.FILL;
	} else if (vista5_ancho == '-') {
		vista5_ancho = Ti.UI.SIZE;
	} else if (!isNaN(vista5_ancho)) {
		vista5_ancho = vista5_ancho + 'dp';
	}
	$.vista5.setWidth(vista5_ancho);

	/** 
	 * Desenfocamos campos de texto 
	 */
	$.YourEmail.blur();
	$.YourPassword.blur();
	$.scroll.setShowVerticalScrollIndicator(false);
})();

if (OS_IOS || OS_ANDROID) {
	$.LOGIN.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}
$.LOGIN.open();