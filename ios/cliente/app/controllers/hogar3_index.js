var _bind4section = {};
var _list_templates = {};

$.HOME_window.setTitleAttributes({
	color: '#FFFFFF'
});
var _activity;
if (OS_ANDROID) {
	_activity = $.HOME.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'HOME';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.HOME_window.addEventListener('open', function(e) {
		abx.setStatusbarColor("#FFFFFF");
		abx.setBackgroundColor("#2d9edb");
	});
}

function Click_vista(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.HOME.close();

}

$.widgetBarracasos.init({
	__id: 'ALL1655304175',
	paso: L('x1842515611', '3'),
	titulo5: L('x951154001_traducir', 'End'),
	titulo1: L('x807033745_traducir', 'Received'),
	titulo4: L('x1018769216_traducir', 'Evaluating'),
	titulo3: L('x4262956787_traducir', 'Inspected'),
	titulo2: L('x807001066_traducir', 'Scheduled')
});


$.widgetCajadescripcion.init({
	titulo: L('x3950563313_traducir', 'Description'),
	__id: 'ALL1926115404'
});

function Click_imagen3(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var evento = e;
	require('vars')['calificacion'] = L('x2212294583', '1');
	if (Ti.App.deployType != 'production') console.log('caja1', {});
	$.imagen3.start();
	var imagen5_visible = false;

	if (imagen5_visible == 'si') {
		imagen5_visible = true;
	} else if (imagen5_visible == 'no') {
		imagen5_visible = false;
	}
	$.imagen5.setVisible(imagen5_visible);

	var imagen7_visible = false;

	if (imagen7_visible == 'si') {
		imagen7_visible = true;
	} else if (imagen7_visible == 'no') {
		imagen7_visible = false;
	}
	$.imagen7.setVisible(imagen7_visible);

	var imagen9_visible = false;

	if (imagen9_visible == 'si') {
		imagen9_visible = true;
	} else if (imagen9_visible == 'no') {
		imagen9_visible = false;
	}
	$.imagen9.setVisible(imagen9_visible);

	var imagen11_visible = false;

	if (imagen11_visible == 'si') {
		imagen11_visible = true;
	} else if (imagen11_visible == 'no') {
		imagen11_visible = false;
	}
	$.imagen11.setVisible(imagen11_visible);


}

function Click_imagen4(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var evento = e;
	var imagen5_visible = true;

	if (imagen5_visible == 'si') {
		imagen5_visible = true;
	} else if (imagen5_visible == 'no') {
		imagen5_visible = false;
	}
	$.imagen5.setVisible(imagen5_visible);

	var imagen3_visible = true;

	if (imagen3_visible == 'si') {
		imagen3_visible = true;
	} else if (imagen3_visible == 'no') {
		imagen3_visible = false;
	}
	$.imagen3.setVisible(imagen3_visible);

	$.imagen3.start();
	$.imagen5.start();

}

function Click_imagen5(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var evento = e;
	require('vars')['calificacion'] = 2;
	if (Ti.App.deployType != 'production') console.log('caja2', {});
	var imagen5_visible = true;

	if (imagen5_visible == 'si') {
		imagen5_visible = true;
	} else if (imagen5_visible == 'no') {
		imagen5_visible = false;
	}
	$.imagen5.setVisible(imagen5_visible);

	$.imagen3.start();
	$.imagen5.start();
	var imagen7_visible = false;

	if (imagen7_visible == 'si') {
		imagen7_visible = true;
	} else if (imagen7_visible == 'no') {
		imagen7_visible = false;
	}
	$.imagen7.setVisible(imagen7_visible);

	var imagen9_visible = false;

	if (imagen9_visible == 'si') {
		imagen9_visible = true;
	} else if (imagen9_visible == 'no') {
		imagen9_visible = false;
	}
	$.imagen9.setVisible(imagen9_visible);

	var imagen11_visible = false;

	if (imagen11_visible == 'si') {
		imagen11_visible = true;
	} else if (imagen11_visible == 'no') {
		imagen11_visible = false;
	}
	$.imagen11.setVisible(imagen11_visible);


}

function Click_imagen6(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var evento = e;
	var imagen7_visible = true;

	if (imagen7_visible == 'si') {
		imagen7_visible = true;
	} else if (imagen7_visible == 'no') {
		imagen7_visible = false;
	}
	$.imagen7.setVisible(imagen7_visible);

	var imagen3_visible = true;

	if (imagen3_visible == 'si') {
		imagen3_visible = true;
	} else if (imagen3_visible == 'no') {
		imagen3_visible = false;
	}
	$.imagen3.setVisible(imagen3_visible);

	var imagen5_visible = true;

	if (imagen5_visible == 'si') {
		imagen5_visible = true;
	} else if (imagen5_visible == 'no') {
		imagen5_visible = false;
	}
	$.imagen5.setVisible(imagen5_visible);

	$.imagen3.start();
	$.imagen5.start();
	$.imagen7.start();

}

function Click_imagen7(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var evento = e;
	require('vars')['calificacion'] = 3;
	$.imagen3.start();
	$.imagen5.start();
	$.imagen7.start();
	if (Ti.App.deployType != 'production') console.log('caja3', {});
	var imagen9_visible = false;

	if (imagen9_visible == 'si') {
		imagen9_visible = true;
	} else if (imagen9_visible == 'no') {
		imagen9_visible = false;
	}
	$.imagen9.setVisible(imagen9_visible);

	var imagen11_visible = false;

	if (imagen11_visible == 'si') {
		imagen11_visible = true;
	} else if (imagen11_visible == 'no') {
		imagen11_visible = false;
	}
	$.imagen11.setVisible(imagen11_visible);


}

function Click_imagen8(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var evento = e;
	var imagen9_visible = true;

	if (imagen9_visible == 'si') {
		imagen9_visible = true;
	} else if (imagen9_visible == 'no') {
		imagen9_visible = false;
	}
	$.imagen9.setVisible(imagen9_visible);

	var imagen3_visible = true;

	if (imagen3_visible == 'si') {
		imagen3_visible = true;
	} else if (imagen3_visible == 'no') {
		imagen3_visible = false;
	}
	$.imagen3.setVisible(imagen3_visible);

	var imagen5_visible = true;

	if (imagen5_visible == 'si') {
		imagen5_visible = true;
	} else if (imagen5_visible == 'no') {
		imagen5_visible = false;
	}
	$.imagen5.setVisible(imagen5_visible);

	var imagen7_visible = true;

	if (imagen7_visible == 'si') {
		imagen7_visible = true;
	} else if (imagen7_visible == 'no') {
		imagen7_visible = false;
	}
	$.imagen7.setVisible(imagen7_visible);

	$.imagen3.start();
	$.imagen5.start();
	$.imagen7.start();
	$.imagen9.start();

}

function Click_imagen9(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var evento = e;
	if (Ti.App.deployType != 'production') console.log('caja4', {});
	require('vars')['calificacion'] = 4;
	$.imagen3.start();
	$.imagen5.start();
	$.imagen7.start();
	$.imagen9.start();
	var imagen11_visible = false;

	if (imagen11_visible == 'si') {
		imagen11_visible = true;
	} else if (imagen11_visible == 'no') {
		imagen11_visible = false;
	}
	$.imagen11.setVisible(imagen11_visible);


}

function Click_imagen10(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var evento = e;
	var imagen11_visible = true;

	if (imagen11_visible == 'si') {
		imagen11_visible = true;
	} else if (imagen11_visible == 'no') {
		imagen11_visible = false;
	}
	$.imagen11.setVisible(imagen11_visible);

	var imagen3_visible = true;

	if (imagen3_visible == 'si') {
		imagen3_visible = true;
	} else if (imagen3_visible == 'no') {
		imagen3_visible = false;
	}
	$.imagen3.setVisible(imagen3_visible);

	var imagen5_visible = true;

	if (imagen5_visible == 'si') {
		imagen5_visible = true;
	} else if (imagen5_visible == 'no') {
		imagen5_visible = false;
	}
	$.imagen5.setVisible(imagen5_visible);

	var imagen7_visible = true;

	if (imagen7_visible == 'si') {
		imagen7_visible = true;
	} else if (imagen7_visible == 'no') {
		imagen7_visible = false;
	}
	$.imagen7.setVisible(imagen7_visible);

	var imagen9_visible = true;

	if (imagen9_visible == 'si') {
		imagen9_visible = true;
	} else if (imagen9_visible == 'no') {
		imagen9_visible = false;
	}
	$.imagen9.setVisible(imagen9_visible);

	$.imagen3.start();
	$.imagen5.start();
	$.imagen7.start();
	$.imagen9.start();
	$.imagen11.start();

}

function Click_imagen11(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var evento = e;
	if (Ti.App.deployType != 'production') console.log('caja5', {});
	require('vars')['calificacion'] = 5;
	$.imagen3.start();
	$.imagen5.start();
	$.imagen7.start();
	$.imagen9.start();
	$.imagen11.start();

}

function Click_vista25(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var email = ('email' in require('vars')) ? require('vars')['email'] : '';
	var datos = ('datos' in require('vars')) ? require('vars')['datos'] : '';
	var calificacion = ('calificacion' in require('vars')) ? require('vars')['calificacion'] : '';
	var urlcrawford = ('urlcrawford' in require('vars')) ? require('vars')['urlcrawford'] : '';
	var consultarURL = {};

	consultarURL.success = function(e) {
		var elemento = e,
			valor = e;
		if (elemento == true || elemento == 'true') {
			var vista25_visible = false;

			if (vista25_visible == 'si') {
				vista25_visible = true;
			} else if (vista25_visible == 'no') {
				vista25_visible = false;
			}
			$.vista25.setVisible(vista25_visible);

		}
		elemento = null, valor = null;
	};

	consultarURL.error = function(e) {
		var elemento = e,
			valor = e;
		elemento = null, valor = null;
	};
	require('helper').ajaxUnico('consultarURL', '' + String.format(L('x554964598', '%1$scalificarInspector/'), urlcrawford.toString()) + '', 'POST', {
		correo: email,
		siniestro: datos.siniestro,
		rut_inspector: datos.rut_inspector
	}, 15000, consultarURL);
	var RATE_visible = false;

	if (RATE_visible == 'si') {
		RATE_visible = true;
	} else if (RATE_visible == 'no') {
		RATE_visible = false;
	}
	$.RATE.setVisible(RATE_visible);

	$.progreso.show();

}

(function() {
	var urlcrawford = ('urlcrawford' in require('vars')) ? require('vars')['urlcrawford'] : '';
	var urluadjust = ('urluadjust' in require('vars')) ? require('vars')['urluadjust'] : '';
	if (Ti.App.deployType != 'production') console.log('hogar3', {
		"datos": args
	});
	if ((_.isObject(args) || (_.isString(args)) && !_.isEmpty(args)) || _.isNumber(args) || _.isBoolean(args)) {
		var siniestro = args._data;
		var consultarModelo_i = Alloy.createCollection('casos');
		var consultarModelo_i_where = 'siniestro=\'' + siniestro + '\'';
		consultarModelo_i.fetch({
			query: 'SELECT * FROM casos WHERE siniestro=\'' + siniestro + '\''
		});
		var datos = require('helper').query2array(consultarModelo_i);
		datos = datos[0];
		if (Ti.App.deployType != 'production') console.log('carga', {
			"asd": datos
		});
		$.Label2.setText(datos.compania);

		if ((_.isObject(datos.imagen_inspector) || (_.isString(datos.imagen_inspector)) && !_.isEmpty(datos.imagen_inspector)) || _.isNumber(datos.imagen_inspector) || _.isBoolean(datos.imagen_inspector)) {
			var imageBlob = Ti.Utils.base64decode(datos.imagen_inspector);
			var imagen_imagen = imageBlob;

			if (typeof imagen_imagen == 'string' && 'styles' in require('a4w') && imagen_imagen in require('a4w').styles['images']) {
				imagen_imagen = require('a4w').styles['images'][imagen_imagen];
			}
			$.imagen.setImage(imagen_imagen);

		}
		$.InspectorsName.setText(datos.inspector);

		$.label4.setText(datos.rut_inspector);

		if ((_.isObject(datos.fecha_agendamiento) || (_.isString(datos.fecha_agendamiento)) && !_.isEmpty(datos.fecha_agendamiento)) || _.isNumber(datos.fecha_agendamiento) || _.isBoolean(datos.fecha_agendamiento)) {
			$.Label5.setText(datos.fecha_agendamiento);

			var vista9_visible = true;

			if (vista9_visible == 'si') {
				vista9_visible = true;
			} else if (vista9_visible == 'no') {
				vista9_visible = false;
			}
			$.vista9.setVisible(vista9_visible);

		}
		datos.calificacion = parseInt(datos.calificacion);
		if (datos.calificacion == -1) {
			var vista17_visible = false;

			if (vista17_visible == 'si') {
				vista17_visible = true;
			} else if (vista17_visible == 'no') {
				vista17_visible = false;
			}
			$.vista17.setVisible(vista17_visible);

			if (Ti.App.deployType != 'production') console.log('sincalificacion', {});
		} else if (datos.calificacion == 1 || datos.calificacion == '1') {
			$.imagen13.start();
			if (Ti.App.deployType != 'production') console.log('cali1', {});
			var vista25_visible = false;

			if (vista25_visible == 'si') {
				vista25_visible = true;
			} else if (vista25_visible == 'no') {
				vista25_visible = false;
			}
			$.vista25.setVisible(vista25_visible);

		} else if (datos.calificacion == 2) {
			$.imagen13.start();
			$.imagen15.start();
			if (Ti.App.deployType != 'production') console.log('cali2', {});
			var vista25_visible = false;

			if (vista25_visible == 'si') {
				vista25_visible = true;
			} else if (vista25_visible == 'no') {
				vista25_visible = false;
			}
			$.vista25.setVisible(vista25_visible);

		} else if (datos.calificacion == 3) {
			$.imagen13.start();
			$.imagen15.start();
			$.imagen17.start();
			if (Ti.App.deployType != 'production') console.log('cali3', {});
			var vista25_visible = false;

			if (vista25_visible == 'si') {
				vista25_visible = true;
			} else if (vista25_visible == 'no') {
				vista25_visible = false;
			}
			$.vista25.setVisible(vista25_visible);

		} else if (datos.calificacion == 4) {
			$.imagen13.start();
			$.imagen15.start();
			$.imagen17.start();
			$.imagen19.start();
			if (Ti.App.deployType != 'production') console.log('cali4', {});
			var vista25_visible = false;

			if (vista25_visible == 'si') {
				vista25_visible = true;
			} else if (vista25_visible == 'no') {
				vista25_visible = false;
			}
			$.vista25.setVisible(vista25_visible);

		} else if (datos.calificacion == 5) {
			$.imagen13.start();
			$.imagen15.start();
			$.imagen17.start();
			$.imagen19.start();
			$.imagen21.start();
			if (Ti.App.deployType != 'production') console.log('cali5', {});
			var vista25_visible = false;

			if (vista25_visible == 'si') {
				vista25_visible = true;
			} else if (vista25_visible == 'no') {
				vista25_visible = false;
			}
			$.vista25.setVisible(vista25_visible);

		}
		var days = L('x3957652582_traducir', 'days');
		var legal = L('x2139489547_traducir', 'legal:');
		var uadjust = L('x1807599243_traducir', 'uadjust:');
		$.Label3.setText(String.format(L('x3000044032', '%1$s %2$s %3$s %4$s %5$s %6$s'), (legal) ? legal.toString() : '', (datos.legal) ? datos.legal.toString() : '', (days) ? days.toString() : '', (uadjust) ? uadjust.toString() : '', (datos.estimado) ? datos.estimado.toString() : '', (days) ? days.toString() : ''));

	}
	$.scroll.setShowVerticalScrollIndicator(false);
	var hog3 = L('x118206968_traducir', 'deschogar3');

	$.widgetCajadescripcion.texto({
		valor: L('x154210459_traducir', 'hog3')
	});
})();

if (OS_IOS || OS_ANDROID) {
	$.HOME.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}
//$.HOME.open();
