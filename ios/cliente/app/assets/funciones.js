var funciones = {}
funciones.formatear_fecha = function(x_params) {
	var formato = x_params['formato'];
	var fecha = x_params['fecha'];
	var formato_entrada = x_params['formato_entrada'];
	var moment = require("alloy/moment");
	var fecha_era_texto = (typeof fecha === 'string' || typeof fecha === 'number') ? true : false;
	var nuevo = '';
	if (fecha_era_texto == true || fecha_era_texto == 'true') {
		nuevo = moment(fecha, formato_entrada).format(formato);
	} else {
		nuevo = moment(fecha).format(formato);
	}
	return nuevo;
};

module.exports = funciones;