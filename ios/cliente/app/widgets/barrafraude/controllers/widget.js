/** 
 * Widget Barra de fraude
 * Permite mostrar al usuario en que parte del proceso se encuentra actualmente
 * 
 * @params paso Para poder indicarle al usuario en que paso se encuentra actualmente
 * @params titulo1 Modifica el texto para ponerle el nombre del paso
 * @params titulo2 Modifica el texto para ponerle el nombre del paso
 * @params titulo3 Modifica el texto para ponerle el nombre del paso 
 */
var _bind4section = {};

var args = arguments[0] || {};


$.init = function(params) {
	for (var tobe in params) args[tobe] = params[tobe];
	if ('__id' in params) require(WPATH('vars'))[params.__id] = {};
	if (params.paso == 1 || params.paso == '1') {
		var imagen_imagen = '1on';

		if (typeof imagen_imagen == 'string' && 'styles' in require(WPATH('a4w')) && imagen_imagen in require(WPATH('a4w')).styles['images']) {
			imagen_imagen = WPATH(require(WPATH('a4w')).styles['images'][imagen_imagen]);
		}
		$.imagen.setImage(imagen_imagen);

		$.label.setColor('#000000');

	} else if (params.paso == 2) {
		var imagen_imagen = '1off';

		if (typeof imagen_imagen == 'string' && 'styles' in require(WPATH('a4w')) && imagen_imagen in require(WPATH('a4w')).styles['images']) {
			imagen_imagen = WPATH(require(WPATH('a4w')).styles['images'][imagen_imagen]);
		}
		$.imagen.setImage(imagen_imagen);

		var vista3_estilo = 'fondoazul';

		var setEstilo = function(clase) {
			if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
				try {
					$.vista3.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
				} catch (sete_err) {}
			}
		};
		setEstilo(vista3_estilo);

		var vista5_estilo = 'fondoazul';

		var setEstilo = function(clase) {
			if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
				try {
					$.vista5.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
				} catch (sete_err) {}
			}
		};
		setEstilo(vista5_estilo);

		var imagen2_imagen = '2on';

		if (typeof imagen2_imagen == 'string' && 'styles' in require(WPATH('a4w')) && imagen2_imagen in require(WPATH('a4w')).styles['images']) {
			imagen2_imagen = WPATH(require(WPATH('a4w')).styles['images'][imagen2_imagen]);
		}
		$.imagen2.setImage(imagen2_imagen);

		$.label2.setColor('#000000');

	} else if (params.paso == 3) {
		var imagen_imagen = '1off';

		if (typeof imagen_imagen == 'string' && 'styles' in require(WPATH('a4w')) && imagen_imagen in require(WPATH('a4w')).styles['images']) {
			imagen_imagen = WPATH(require(WPATH('a4w')).styles['images'][imagen_imagen]);
		}
		$.imagen.setImage(imagen_imagen);

		var imagen2_imagen = '2off';

		if (typeof imagen2_imagen == 'string' && 'styles' in require(WPATH('a4w')) && imagen2_imagen in require(WPATH('a4w')).styles['images']) {
			imagen2_imagen = WPATH(require(WPATH('a4w')).styles['images'][imagen2_imagen]);
		}
		$.imagen2.setImage(imagen2_imagen);

		var imagen3_imagen = '3on';

		if (typeof imagen3_imagen == 'string' && 'styles' in require(WPATH('a4w')) && imagen3_imagen in require(WPATH('a4w')).styles['images']) {
			imagen3_imagen = WPATH(require(WPATH('a4w')).styles['images'][imagen3_imagen]);
		}
		$.imagen3.setImage(imagen3_imagen);

		var vista3_estilo = 'fondoazul';

		var setEstilo = function(clase) {
			if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
				try {
					$.vista3.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
				} catch (sete_err) {}
			}
		};
		setEstilo(vista3_estilo);

		var vista5_estilo = 'fondoazul';

		var setEstilo = function(clase) {
			if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
				try {
					$.vista5.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
				} catch (sete_err) {}
			}
		};
		setEstilo(vista5_estilo);

		var vista6_estilo = 'fondoazul';

		var setEstilo = function(clase) {
			if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
				try {
					$.vista6.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
				} catch (sete_err) {}
			}
		};
		setEstilo(vista6_estilo);

		var vista8_estilo = 'fondoazul';

		var setEstilo = function(clase) {
			if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
				try {
					$.vista8.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
				} catch (sete_err) {}
			}
		};
		setEstilo(vista8_estilo);

		$.label3.setColor('#000000');

	}
	if (!_.isUndefined(params.titulo1)) {
		$.label.setText(params.titulo1);

	}
	if (!_.isUndefined(params.titulo2)) {
		$.label2.setText(params.titulo2);

	}
	if (!_.isUndefined(params.titulo3)) {
		$.label3.setText(params.titulo3);

	}
};