Widget Caja descripcion
Permite mostrar al usuario una caja con texto descriptorio

@params titulo Titulo de la descripcion
@params descripcion Texto de la descripcion en si
@params valor Cambia el texto que tiene en la descripcion
