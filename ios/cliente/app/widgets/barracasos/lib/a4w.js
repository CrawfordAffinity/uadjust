exports.events = _.clone(Backbone.Events);
exports.ajaxkeys = {};
exports.variables = {};
exports.styles = {
	"images": {
		"5on": "/images/i1B084C970EE19D9AE0428A2571FC664D.png",
		"5off": "/images/iCD23BD0471A65356FAB1DE09509F9E58.png",
		"4p": "/images/i84A926F825C2515FBD903F97F79BD214.png",
		"1on": "/images/iC01FF6BEF6CBC79D7B2C256E8755A23E.png",
		"2on": "/images/i78A03ADE2271444F6DB47A58A71CC85A.png",
		"2p": "/images/i16AD707F3A6BEE1FB6B7342B8593A039.png",
		"3p": "/images/i061894607BD8D5FED78C268E9ADF0AC1.png",
		"3on": "/images/iFD7E5C76ACA49417BD156B284ABDD078.png",
		"1off": "/images/i21BFEFF48B78A4ADA96EE3F7CA446C23.png",
		"4off": "/images/iC675CCE6FFABEE96330944DEF186D1EC.png",
		"4on": "/images/i3A2DD2B077F406774602CECF8FDF5C13.png",
		"1p": "/images/i416ABAD55A6919C3568DB5572D92FBB0.png",
		"2off": "/images/i0FB531AF32E39B989C13329409227E63.png",
		"3off": "/images/i220D6308D3FD9EE0327E08E14CDD01FF.png",
		"5p": "/images/iF57510F32636B74912E58AAE9FB220B6.png"
	},
	"classes": {
		"#imagen3": {
			"image": "WPATH('images/i061894607BD8D5FED78C268E9ADF0AC1.png')"
		},
		"est9": {
			"font": {
				"fontFamily": "SFUIDisplay-Regular",
				"fontSize": "12dp"
			}
		},
		"fondoazul": {
			"backgroundColor": "#2d9edb",
			"font": {
				"fontSize": "12dp"
			}
		},
		"fondoplomo": {
			"backgroundColor": "#e6e6e6",
			"font": {
				"fontSize": "12dp"
			}
		},
		"#imagen": {
			"image": "WPATH('images/i416ABAD55A6919C3568DB5572D92FBB0.png')"
		},
		"#imagen5": {
			"image": "WPATH('images/iF57510F32636B74912E58AAE9FB220B6.png')"
		},
		"#imagen2": {
			"image": "WPATH('images/i16AD707F3A6BEE1FB6B7342B8593A039.png')"
		},
		"#imagen4": {
			"image": "WPATH('images/i84A926F825C2515FBD903F97F79BD214.png')"
		},
		"est5": {
			"color": "#a0a1a3",
			"font": {
				"fontFamily": "SFUIDisplay-Light",
				"fontSize": "12dp"
			}
		}
	}
};
exports.fontello = {};