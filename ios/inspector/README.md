Nombre proyecto: Login





Descripcion: Proyecto principal de la aplicacion, permite al usuario poder registrarse, o en caso que tenga una cuenta, puede iniciar sesion y obtener todos los datos necesarios para poder hacer una inspeccion y ademas ver o asignarse tareas nuevas





Consume las siguientes tablas locales:


*         pais: Contiene la lista de paises registrados
*         asegurado:---------------------------
*         emergencia_perfil: Contiene todas las tareas de emergencia disponibles segun el domicilio del inspector
*         emergencia_ubicacion: Contiene todas las tareas de emergencia disponibles segun el domicilio del inspector
*         inspectores: Contiene el detalle del inspector
*         tareas_entrantes: Contiene todas las tareas disponibles para ser tomadas
*         historial_tareas: Contiene el historial de tareas que no han sido enviados, digase fotos de la inspeccion
*         tareas: Contiene el registro de las tareas que el inspector tiene asignadas
*         experiencia_oficio: Obtiene la lista de experiencias u oficios que estan disponibles
*         nivel1: Para obtener el primer nivel del pais (por ejemplo, la region del pais)
*         emergencia: ------------------
*         tipo_accion: Contiene los tipos de danos
*         tipo_partida: Contiene los tipo de partida
*         tipo_dano:
*         unidad_medida:
*         destino: Para consultar y guardar destinos de la propiedad disponibles.
*         compania: Para consultar y guardar companias de seguros disponibles.
*         entidad_financiera: Para consultar y guardar entidades financieras disponibles.
*         tipo_siniestro:
*         estructura_soportante: Para consultar y guardar estructuras soportantes disponibles.
*         muros_tabiques: Para consultar y guardar muros y tabiques disponibles.
*         entrepisos: Para consultar y guardar entrepisos disponibles.
*         pavimento: Para consultar y guardar pavimentos disponibles.
*         estructura_cubierta: Para consultar y guardar estructuras cubiertas disponibles.
*         cubierta: Para consultar y guardar cubiertas disponibles.
*         bienes: Para consultar y guardar los bienes disponibles
*         marcas: Para consultar y guardar las marcas disponibles
*         monedas: Para consultar y guardar las monedas disponibles
*         inspecciones: Se ocupa para consultar el detalle de la inspeccion que se lleva a cabo
*         insp_fotosrequeridas: Se ocupa para guardar el nombre de los archivos de las fotos requeridas
*         insp_datosbasicos: Se ocupa para consultar y guardar los datos de la pantalla datos basicos
*         insp_caracteristicas: Para consultar y guardar nuevos registros disponibles.
*         insp_niveles: Para consultar y guardar niveles disponibles.
*         insp_siniestro: Se ocupa para guardar los datos de la pantalla siniestro
*         insp_recintos:&#160;Para consultar recintos que hayan sido guardados.
*         insp_itemdanos: Para consultar y guardar danos disponibles.
*         insp_contenido: Para consultar y guardar nuevos contenidos disponibles.
*         insp_documentos: Se utiliza para consultar y guardar los documentos que fueron seleccionados
*         insp_firma: Se utiliza para guardar la firma del asegurado
*         numero_unico:&#160;&#160;Para guardar&#160;un numero unico&#160;
*         cola: Se ocupa cuando se cancela la inspeccion para guardar los datos recopilados de la inspeccion hasta ese momento





Ocupa los siguientes estilos:


*         estilo3
*         estilo6_1
*         estilo10
*         fondoamarillo


Predecesor del proyecto:


*         Este proyecto es el principal, no tiene predecesor.


Sucesor del proyecto:


*         Proyecto Registro
*         Proyecto Login





Pantallas:


*         Portada: El inspector puede iniciar sesion, o en caso que no este registado, da la posibilidad de registrarse                            Widgets:                                        Botonlargo: Vista que interactua pareciendo un boton
