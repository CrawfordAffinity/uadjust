var _bind4section = {};
var _list_templates = {
	"contenido": {
		"Label2": {
			"text": "{id}"
		},
		"vista4": {},
		"Label": {
			"text": "{nombre}"
		}
	},
	"recinto": {
		"Label2": {
			"text": "{id}"
		},
		"vista4": {},
		"Label": {
			"text": "{nombre}"
		}
	}
};
var $datos = $.datos.toJSON();

$.SINIESTRO_window.setTitleAttributes({
	color: 'WHITE'
});
var _activity;
if (OS_ANDROID) {
	_activity = $.SINIESTRO.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'SINIESTRO';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.SINIESTRO_window.addEventListener('open', function(e) {
		abx.setStatusbarColor("#FFFFFF");
		abx.setBackgroundColor("#fcbd83");
	});
}

function Click_vista(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	preguntarOpciones.show();

}

$.widgetPreguntas.init({
	titulo: L('x404129330_traducir', '¿EL ASEGURADO CONFIRMA ESTOS DATOS?'),
	__id: 'ALL516906299',
	si: L('x1723413441_traducir', 'SI, Están correctos'),
	texto: L('x2083765231_traducir', 'El asegurado debe confirmar que los datos de esta sección están correctos'),
	pantalla: L('x2851883104_traducir', '¿ESTA DE ACUERDO?'),
	onno: no_widgetPreguntas,
	onsi: si_widgetPreguntas,
	no: L('x55492959_traducir', 'NO, Hay que modificar algo'),
	header: L('x2241007166_traducir', 'naranjo'),
	onclick: Click_widgetPreguntas
});

function Click_widgetPreguntas(e) {

	var evento = e;
	if (_.isUndefined($datos.id_tipo_siniestro)) {
		/** 
		 * Validamos que los campos ingresados sean correctos 
		 */
		var preguntarAlerta_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x407984850_traducir', 'Seleccione tipo de siniestro'),
			buttonNames: preguntarAlerta_opts
		});
		preguntarAlerta.addEventListener('click', function(e) {
			var nulo = preguntarAlerta_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta.show();
	} else if (_.isUndefined($datos.descripcion)) {
		var preguntarAlerta2_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta2 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x2617454703_traducir', 'Describa el siniestro brevemente, mínimo 30 caracteres'),
			buttonNames: preguntarAlerta2_opts
		});
		preguntarAlerta2.addEventListener('click', function(e) {
			var nulo = preguntarAlerta2_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta2.show();
	} else if (_.isNumber($datos.descripcion.length) && _.isNumber(29) && $datos.descripcion.length <= 29) {
		var preguntarAlerta3_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta3 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x2617454703_traducir', 'Describa el siniestro brevemente, mínimo 30 caracteres'),
			buttonNames: preguntarAlerta3_opts
		});
		preguntarAlerta3.addEventListener('click', function(e) {
			var nulo = preguntarAlerta3_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta3.show();
	} else if (_.isUndefined($datos.porcentaje_danos_estructura)) {
		var preguntarAlerta4_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta4 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x2749688656_traducir', 'Indique porcentaje de daños en estructura'),
			buttonNames: preguntarAlerta4_opts
		});
		preguntarAlerta4.addEventListener('click', function(e) {
			var nulo = preguntarAlerta4_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta4.show();
	} else if (_.isUndefined($datos.porcentaje_danos_terminaciones)) {
		var preguntarAlerta5_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta5 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x1745658519_traducir', 'Indique porcentaje de daños en terminaciones'),
			buttonNames: preguntarAlerta5_opts
		});
		preguntarAlerta5.addEventListener('click', function(e) {
			var nulo = preguntarAlerta5_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta5.show();
	} else if (_.isUndefined($datos.porcentaje_danos_instalaciones)) {
		var preguntarAlerta6_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta6 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x159191226_traducir', 'Indique porcentaje de daños en instalaciones'),
			buttonNames: preguntarAlerta6_opts
		});
		preguntarAlerta6.addEventListener('click', function(e) {
			var nulo = preguntarAlerta6_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta6.show();
	} else {
		test = null;
		$.widgetPreguntas.enviar({});
	}
}

function si_widgetPreguntas(e) {

	var evento = e;
	/** 
	 * Guardamos los datos agregados/modificados en el modelo de insp_siniestro 
	 */
	Alloy.Collections[$.datos.config.adapter.collection_name].add($.datos);
	$.datos.save();
	Alloy.Collections[$.datos.config.adapter.collection_name].fetch();
	/** 
	 * Limpiamos widgets 
	 */
	$.widgetModal.limpiar({});
	Alloy.createController("recintos_index", {}).getView().open();

}

function no_widgetPreguntas(e) {

	var evento = e;

}

$.widgetModal.init({
	titulo: L('x3952572309_traducir', 'SINIESTRO'),
	cargando: L('x1740321226_traducir', 'cargando ..'),
	__id: 'ALL1379917834',
	left: 16,
	onrespuesta: Respuesta_widgetModal,
	campo: L('x2004291629_traducir', 'Tipo de siniestro'),
	onabrir: Abrir_widgetModal,
	color: 'naranjo',
	right: 16,
	top: 20,
	seleccione: L('x4123108455_traducir', 'seleccione tipo'),
	activo: true,
	onafterinit: Afterinit_widgetModal
});

function Afterinit_widgetModal(e) {

	var evento = e;
	var ID_1088992865_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {} else {
			/** 
			 * Insertamos dummies 
			 */
			/** 
			 * Insertamos dummies 
			 */
			var eliminarModelo_i = Alloy.Collections.tipo_siniestro;
			var sql = "DELETE FROM " + eliminarModelo_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4,5'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo_m = Alloy.Collections.tipo_siniestro;
				var insertarModelo_fila = Alloy.createModel('tipo_siniestro', {
					nombre: String.format(L('x1187638321', '%1$s Siniestro'), item.toString()),
					id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
					pais_texto: 'Chile',
					id_segured: String.format(L('x2286518298_traducir', '11%1$s'), item_pos.toString()),
					pais: 1
				});
				insertarModelo_m.add(insertarModelo_fila);
				insertarModelo_fila.save();
			});
		}
		if (Ti.App.deployType != 'production') console.log('transformando datos de modelo siniestro para modal', {});
		/** 
		 * obtenemos datos para selectores 
		 */
		var transformarModelo_i = Alloy.createCollection('tipo_siniestro');
		transformarModelo_i.fetch();
		var transformarModelo_src = require('helper').query2array(transformarModelo_i);
		var datos = [];
		_.each(transformarModelo_src, function(fila, pos) {
			var new_row = {};
			_.each(fila, function(x, llave) {
				var newkey = '';
				if (llave == 'nombre') newkey = 'valor';
				if (llave == 'id_segured') newkey = 'id_segured';
				if (newkey != '') new_row[newkey] = fila[llave];
			});
			datos.push(new_row);
		});
		if (Ti.App.deployType != 'production') console.log('enviando data transformada a modal', {});
		/** 
		 * Cargamos el widget con los datos de tipo de siniestro 
		 */
		$.widgetModal.data({
			data: datos
		});
	};
	var ID_1088992865 = setTimeout(ID_1088992865_func, 1000 * 0.2);

}

function Abrir_widgetModal(e) {

	var evento = e;

}

function Respuesta_widgetModal(e) {

	var evento = e;
	/** 
	 * Mostramos en pantalla el tipo de siniestro seleccionado, y guardamos en la tabla el id del tipo de siniestro 
	 */
	$.widgetModal.labels({
		valor: evento.valor
	});
	$.datos.set({
		id_tipo_siniestro: evento.item.id_segured
	});
	if ('datos' in $) $datos = $.datos.toJSON();

}

function Change_area(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	/** 
	 * Cuando el campo de texto es desenfocado, actualizamos el modelo con la descripcion del siniestro 
	 */
	$.datos.set({
		descripcion: elemento
	});
	if ('datos' in $) $datos = $.datos.toJSON();
	elemento = null, source = null;

}

function Change_slider(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var valor = e.value;
	/** 
	 * Dejamos el valor del slider entre 0 y 100, y actualizamos el valor del porcentaje de danos en la estructura en pantalla y tabla 
	 */
	var redondo = Math.round(elemento);
	$.label.setText(String.format(L('x1749571123_traducir', '%1$s%'), redondo.toString()));

	$.datos.set({
		porcentaje_danos_estructura: redondo
	});
	if ('datos' in $) $datos = $.datos.toJSON();
	elemento = null, valor = null;

}

function Change_ID_129828234(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	if (elemento == true || elemento == 'true') {
		$.NO.setText('SI');

		$.datos.set({
			analisis_especialista: 1
		});
		if ('datos' in $) $datos = $.datos.toJSON();
	} else {
		$.NO.setText('NO');

		$.datos.set({
			analisis_especialista: 0
		});
		if ('datos' in $) $datos = $.datos.toJSON();
	}
	elemento = null;

}

function Change_slider2(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var valor = e.value;
	var redondo = Math.round(elemento);
	$.label2.setText(String.format(L('x1749571123_traducir', '%1$s%'), redondo.toString()));

	$.datos.set({
		porcentaje_danos_terminaciones: redondo
	});
	if ('datos' in $) $datos = $.datos.toJSON();
	elemento = null, valor = null;

}

function Change_slider3(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var valor = e.value;
	var redondo = Math.round(elemento);
	$.label3.setText(String.format(L('x1749571123_traducir', '%1$s%'), redondo.toString()));

	$.datos.set({
		porcentaje_danos_instalaciones: redondo
	});
	if ('datos' in $) $datos = $.datos.toJSON();
	elemento = null, valor = null;

}

function Click_scroll(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.area.blur();

}

function Postlayout_vista13(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.progreso.show();

}

function Postlayout_SINIESTRO(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	/** 
	 * Avisamos a la aplicacion que cierre la pantalla que estaba antes que esta... Pantalla caracteristicas 
	 */
	Alloy.Events.trigger('_cerrar_insp', {
		pantalla: 'caracteristicas'
	});

}

var preguntarOpciones_opts = [L('x3481285698_traducir', 'Asegurado no puede seguir'), L('x3077268071_traducir', ' Se me acabo la bateria'), L('x479989047_traducir', ' Tuve un accidente'), L('x2376009830_traducir', ' Cancelar')];
var preguntarOpciones = Ti.UI.createOptionDialog({
	title: L('x1670176792_traducir', 'RAZON PARA CANCELAR INSPECCION ACTUAL'),
	options: preguntarOpciones_opts
});
preguntarOpciones.addEventListener('click', function(e) {
	var resp = preguntarOpciones_opts[e.index];
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
	if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
		var razon = "";
		if (resp == L('x3481285698_traducir', 'Asegurado no puede seguir')) {
			razon = "Asegurado no puede seguir";
		}
		if (resp == L('x1862794075_traducir', 'Se me acabo la bateria')) {
			razon = "Se me acabo la bateria";
		}
		if (resp == L('x4012805111_traducir', 'Tuve un accidente')) {
			razon = "Tuve un accidente";
		}
		if ((_.isObject(razon) || (_.isString(razon)) && !_.isEmpty(razon)) || _.isNumber(razon) || _.isBoolean(razon)) {
			if (Ti.App.deployType != 'production') console.log('llamando servicio cancelarTarea', {});
			require('vars')['insp_cancelada'] = L('x2941610362_traducir', 'siniestro');
			Alloy.Collections[$.datos.config.adapter.collection_name].add($.datos);
			$.datos.save();
			Alloy.Collections[$.datos.config.adapter.collection_name].fetch();
			var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
			var vista13_visible = true;

			if (vista13_visible == 'si') {
				vista13_visible = true;
			} else if (vista13_visible == 'no') {
				vista13_visible = false;
			}
			$.vista13.setVisible(vista13_visible);

			var consultarURL = {};
			consultarURL.success = function(e) {
				var elemento = e,
					valor = e;
				$.widgetModal.limpiar({});
				var vista13_visible = false;

				if (vista13_visible == 'si') {
					vista13_visible = true;
				} else if (vista13_visible == 'no') {
					vista13_visible = false;
				}
				$.vista13.setVisible(vista13_visible);

				Alloy.createController("firma_index", {}).getView().open();
				elemento = null, valor = null;
			};
			consultarURL.error = function(e) {
				var elemento = e,
					valor = e;
				if (Ti.App.deployType != 'production') console.log('hubo error llamando cancelarTarea, la agregamos a cola de salida', {
					"elemento": elemento
				});
				$.widgetModal.limpiar({});
				if (Ti.App.deployType != 'production') console.log('agregando servicio cancelarTarea a cola', {});
				var datos = {
					id_inspector: seltarea.id_inspector,
					codigo_identificador: inspector.codigo_identificador,
					id_server: seltarea.id_server,
					num_caso: seltarea.num_caso,
					razon: razon
				};
				var insertarModelo2_m = Alloy.Collections.cola;
				var insertarModelo2_fila = Alloy.createModel('cola', {
					data: JSON.stringify(datos),
					id_tarea: seltarea.id_server,
					tipo: 'cancelar'
				});
				insertarModelo2_m.add(insertarModelo2_fila);
				insertarModelo2_fila.save();
				var vista13_visible = false;

				if (vista13_visible == 'si') {
					vista13_visible = true;
				} else if (vista13_visible == 'no') {
					vista13_visible = false;
				}
				$.vista13.setVisible(vista13_visible);

				Alloy.createController("firma_index", {}).getView().open();
				elemento = null, valor = null;
			};
			require('helper').ajaxUnico('consultarURL', '' + String.format(L('x3244141284', '%1$scancelarTarea'), url_server.toString()) + '', 'POST', {
				id_inspector: seltarea.id_inspector,
				codigo_identificador: inspector.codigo_identificador,
				id_tarea: seltarea.id_server,
				num_caso: seltarea.num_caso,
				mensaje: razon,
				opcion: 0,
				tipo: 1
			}, 15000, consultarURL);
		}
	}
	resp = null;
});
var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
	$.datos.set({
		id_inspeccion: seltarea.id_server
	});
	if ('datos' in $) $datos = $.datos.toJSON();
	$.datos.set({
		analisis_especialista: 0
	});
	if ('datos' in $) $datos = $.datos.toJSON();
}
/** 
 * Escuchamos el evento para poder cerrar pantallas a medida que ya no la estamos utilizando 
 */
_my_events['_cerrar_insp,ID_231485652'] = function(evento) {
	if (!_.isUndefined(evento.pantalla)) {
		if (evento.pantalla == L('x2941610362_traducir', 'siniestro')) {
			if (Ti.App.deployType != 'production') console.log('debug cerrando siniestro', {});
			var ID_1781710521_trycatch = {
				error: function(e) {
					if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
				}
			};
			try {
				ID_1781710521_trycatch.error = function(evento) {
					if (Ti.App.deployType != 'production') console.log('error cerrando siniestro', {});
				};
				$.widgetModal.limpiar({});
				$.SINIESTRO.close();
			} catch (e) {
				ID_1781710521_trycatch.error(e);
			}
		}
	} else {
		if (Ti.App.deployType != 'production') console.log('debug cerrando (todas) siniestro', {});
		var ID_1962200830_trycatch = {
			error: function(e) {
				if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
			}
		};
		try {
			ID_1962200830_trycatch.error = function(evento) {
				if (Ti.App.deployType != 'production') console.log('error cerrando siniestro', {});
			};
			$.widgetModal.limpiar({});
			$.SINIESTRO.close();
		} catch (e) {
			ID_1962200830_trycatch.error(e);
		}
	}
};
Alloy.Events.on('_cerrar_insp', _my_events['_cerrar_insp,ID_231485652']);

if (OS_IOS || OS_ANDROID) {
	$.SINIESTRO.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}
//$.SINIESTRO.open();
