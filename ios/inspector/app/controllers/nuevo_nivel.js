var _bind4section = {};
var _list_templates = {};
var $nivel = $.nivel.toJSON();

$.NUEVO_NIVEL_window.setTitleAttributes({
	color: 'WHITE'
});
var _activity;
if (OS_ANDROID) {
	_activity = $.NUEVO_NIVEL.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'NUEVO_NIVEL';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.NUEVO_NIVEL_window.addEventListener('open', function(e) {
		abx.setStatusbarColor("#FFFFFF");
		abx.setBackgroundColor("#8ce5bd");
	});
}

function Click_vista24(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	/** 
	 * Limpiamos widget para liberar memoria ram 
	 */
	$.widgetModalmultiple2.limpiar({});
	$.widgetModalmultiple3.limpiar({});
	$.widgetModalmultiple4.limpiar({});
	$.widgetModalmultiple5.limpiar({});
	$.widgetModalmultiple6.limpiar({});
	$.widgetModalmultiple7.limpiar({});
	$.NUEVO_NIVEL.close();

}

function Click_vista25(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	/** 
	 * Obtenemos el ano actual 
	 */
	var d = new Date();
	var anoactual = d.getFullYear();
	if (_.isUndefined($nivel.nombre)) {
		/** 
		 * Verificamos que los campos ingresados esten correctos 
		 */
		var preguntarAlerta15_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta15 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x1734272038_traducir', 'Ingrese nombre del nivel'),
			buttonNames: preguntarAlerta15_opts
		});
		preguntarAlerta15.addEventListener('click', function(e) {
			var nulo = preguntarAlerta15_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta15.show();
	} else if ((_.isObject($nivel.nombre) || _.isString($nivel.nombre)) && _.isEmpty($nivel.nombre)) {
		var preguntarAlerta16_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta16 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x1734272038_traducir', 'Ingrese nombre del nivel'),
			buttonNames: preguntarAlerta16_opts
		});
		preguntarAlerta16.addEventListener('click', function(e) {
			var nulo = preguntarAlerta16_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta16.show();
	} else if (_.isUndefined($nivel.piso)) {
		var preguntarAlerta17_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta17 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x2669747147_traducir', 'Ingrese Nº de piso'),
			buttonNames: preguntarAlerta17_opts
		});
		preguntarAlerta17.addEventListener('click', function(e) {
			var nulo = preguntarAlerta17_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta17.show();
	} else if (_.isUndefined($nivel.ano)) {
		var preguntarAlerta18_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta18 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x853726930_traducir', 'Ingrese año de construcción del nivel'),
			buttonNames: preguntarAlerta18_opts
		});
		preguntarAlerta18.addEventListener('click', function(e) {
			var nulo = preguntarAlerta18_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta18.show();
	} else if ($nivel.ano < (anoactual - 100) == true || $nivel.ano < (anoactual - 100) == 'true') {
		if (Ti.App.deployType != 'production') console.log('ano mayor', {});
		require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
		var preguntarAlerta19_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta19 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x624168147_traducir', 'Tiene que tener máximo 100 años de antigüedad'),
			buttonNames: preguntarAlerta19_opts
		});
		preguntarAlerta19.addEventListener('click', function(e) {
			var nulo = preguntarAlerta19_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta19.show();
	} else if ($nivel.ano > anoactual == true || $nivel.ano > anoactual == 'true') {
		if (Ti.App.deployType != 'production') console.log('ano mayor', {});
		require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
		var preguntarAlerta20_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta20 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x2118147629_traducir', 'El año tiene que ser menor al año actual'),
			buttonNames: preguntarAlerta20_opts
		});
		preguntarAlerta20.addEventListener('click', function(e) {
			var nulo = preguntarAlerta20_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta20.show();
	} else if (_.isUndefined($nivel.largo)) {
		var preguntarAlerta21_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta21 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x779559340_traducir', 'Ingrese largo del nivel'),
			buttonNames: preguntarAlerta21_opts
		});
		preguntarAlerta21.addEventListener('click', function(e) {
			var nulo = preguntarAlerta21_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta21.show();
	} else if (_.isUndefined($nivel.ancho)) {
		var preguntarAlerta22_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta22 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x2682783608_traducir', 'Ingrese ancho del nivel'),
			buttonNames: preguntarAlerta22_opts
		});
		preguntarAlerta22.addEventListener('click', function(e) {
			var nulo = preguntarAlerta22_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta22.show();
	} else if (_.isUndefined($nivel.alto)) {
		var preguntarAlerta23_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta23 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x4120715490_traducir', 'Ingrese la altura del nivel'),
			buttonNames: preguntarAlerta23_opts
		});
		preguntarAlerta23.addEventListener('click', function(e) {
			var nulo = preguntarAlerta23_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta23.show();
	} else if (_.isUndefined($nivel.ids_estructuras_soportantes)) {
		var preguntarAlerta24_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta24 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x3045996758_traducir', 'Seleccione la estructura soportante del nivel'),
			buttonNames: preguntarAlerta24_opts
		});
		preguntarAlerta24.addEventListener('click', function(e) {
			var nulo = preguntarAlerta24_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta24.show();
	} else {
		/** 
		 * Guardamos los campos ingresados en el modelo 
		 */
		Alloy.Collections[$.nivel.config.adapter.collection_name].add($.nivel);
		$.nivel.save();
		Alloy.Collections[$.nivel.config.adapter.collection_name].fetch();
		var ID_1189911720_func = function() {
			/** 
			 * Limpiamos widget para liberar memoria ram 
			 */
			$.widgetModalmultiple2.limpiar({});
			$.widgetModalmultiple3.limpiar({});
			$.widgetModalmultiple4.limpiar({});
			$.widgetModalmultiple5.limpiar({});
			$.widgetModalmultiple6.limpiar({});
			$.widgetModalmultiple7.limpiar({});
			$.NUEVO_NIVEL.close();
		};
		var ID_1189911720 = setTimeout(ID_1189911720_func, 1000 * 0.1);
	}
}

$.widgetYpicker.init({
	__id: 'ALL1585064981',
	aceptar: L('x1518866076_traducir', 'Aceptar'),
	cancelar: L('x2353348866_traducir', 'Cancelar'),
	onabierto: Abierto_widgetYpicker,
	onaceptar: Aceptar_widgetYpicker,
	oncancelar: Cancelar_widgetYpicker
});

function Aceptar_widgetYpicker(e) {

	var evento = e;
	if (Ti.App.deployType != 'production') console.log('selector de ano aceptado', {
		"evento": evento
	});
	$.SeleccioneAo.setText(evento.valor);

	var SeleccioneAo_estilo = 'estilo12';

	var _tmp_a4w = require('a4w');
	if ((typeof SeleccioneAo_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (SeleccioneAo_estilo in _tmp_a4w.styles['classes'])) {
		try {
			SeleccioneAo_estilo = _tmp_a4w.styles['classes'][SeleccioneAo_estilo];
		} catch (st_val_err) {}
	}
	_tmp_a4w = null;
	$.SeleccioneAo.applyProperties(SeleccioneAo_estilo);

	$.nivel.set({
		ano: evento.valor
	});
	if ('nivel' in $) $nivel = $.nivel.toJSON();

}

function Cancelar_widgetYpicker(e) {

	var evento = e;
	if (Ti.App.deployType != 'production') console.log('selector de ano cancelado', {});

}

function Abierto_widgetYpicker(e) {

	var evento = e;
	$.IngreseNombre.blur();
	$.campo.blur();
	$.campo2.blur();
	$.campo3.blur();
	$.campo4.blur();

}

function Focus_IngreseNombre(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	$.widgetYpicker.cerrar({});
	elemento = null, source = null;

}

function Change_IngreseNombre(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	$.nivel.set({
		nombre: elemento
	});
	if ('nivel' in $) $nivel = $.nivel.toJSON();
	elemento = null, source = null;

}

function Focus_campo(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	$.widgetYpicker.cerrar({});
	elemento = null, source = null;

}

function Change_campo(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	$.nivel.set({
		piso: elemento
	});
	if ('nivel' in $) $nivel = $.nivel.toJSON();
	elemento = null, source = null;

}

function Click_vista29(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.IngreseNombre.fireEvent('blur');
	$.campo.fireEvent('blur');
	$.campo2.fireEvent('blur');
	$.campo3.fireEvent('blur');
	$.campo4.fireEvent('blur');
	$.widgetYpicker.abrir({});

}

function Change_campo2(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	var ancho;
	ancho = $.campo3.getValue();

	if ((_.isObject(ancho) || (_.isString(ancho)) && !_.isEmpty(ancho)) || _.isNumber(ancho) || _.isBoolean(ancho)) {
		if ((_.isObject(elemento) || (_.isString(elemento)) && !_.isEmpty(elemento)) || _.isNumber(elemento) || _.isBoolean(elemento)) {
			/** 
			 * nos aseguramos que ancho y valor de campo sean decimales con puntos (flotante), y no comas (espa&#241;ol) 
			 */
			var nuevo = parseFloat(ancho.split(',').join('.')) * parseFloat(elemento.split(',').join('.'));
			$.nivel.set({
				superficie: nuevo
			});
			if ('nivel' in $) $nivel = $.nivel.toJSON();
		}
	}
	$.nivel.set({
		largo: elemento
	});
	if ('nivel' in $) $nivel = $.nivel.toJSON();
	elemento = null, source = null;

}

function Focus_campo2(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	$.widgetYpicker.cerrar({});
	elemento = null, source = null;

}

function Change_campo3(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	var largo;
	largo = $.campo2.getValue();

	if ((_.isObject(largo) || (_.isString(largo)) && !_.isEmpty(largo)) || _.isNumber(largo) || _.isBoolean(largo)) {
		if ((_.isObject(elemento) || (_.isString(elemento)) && !_.isEmpty(elemento)) || _.isNumber(elemento) || _.isBoolean(elemento)) {
			/** 
			 * nos aseguramos que largo y valor de campo sean decimales con puntos (flotante), y no comas (espa&#241;ol) 
			 */
			var nuevo = parseFloat(largo.split(',').join('.')) * parseFloat(elemento.split(',').join('.'));
			$.nivel.set({
				superficie: nuevo
			});
			if ('nivel' in $) $nivel = $.nivel.toJSON();
		}
	}
	$.nivel.set({
		ancho: elemento
	});
	if ('nivel' in $) $nivel = $.nivel.toJSON();
	elemento = null, source = null;

}

function Focus_campo3(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	$.widgetYpicker.cerrar({});
	elemento = null, source = null;

}

function Change_campo4(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	$.nivel.set({
		alto: elemento
	});
	if ('nivel' in $) $nivel = $.nivel.toJSON();
	elemento = null, source = null;

}

function Focus_campo4(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	$.widgetYpicker.cerrar({});
	elemento = null, source = null;

}

$.widgetModalmultiple2.init({
	titulo: L('x1975271086_traducir', 'Estructura Soportante'),
	cargando: L('x1740321226_traducir', 'cargando ..'),
	__id: 'ALL321122430',
	oncerrar: Cerrar_widgetModalmultiple2,
	hint: L('x2898603391_traducir', 'Seleccione estructura'),
	color: 'verde',
	subtitulo: L('x4011106049_traducir', 'Indique los tipos'),
	top: 5,
	onclick: Click_widgetModalmultiple2,
	onafterinit: Afterinit_widgetModalmultiple2
});

function Click_widgetModalmultiple2(e) {

	var evento = e;
	$.widgetYpicker.cerrar({});

}

function Cerrar_widgetModalmultiple2(e) {

	var evento = e;
	$.widgetModalmultiple2.update({});
	$.nivel.set({
		ids_estructuras_soportantes: evento.valores
	});
	if ('nivel' in $) $nivel = $.nivel.toJSON();

}

function Afterinit_widgetModalmultiple2(e) {

	var evento = e;
	var ID_848598486_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			var consultarModelo5_i = Alloy.createCollection('estructura_soportante');
			var consultarModelo5_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
			consultarModelo5_i.fetch({
				query: 'SELECT * FROM estructura_soportante WHERE pais_texto=\'' + pais[0].nombre + '\''
			});
			var estructura = require('helper').query2array(consultarModelo5_i);
			var datos = [];
			_.each(estructura, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'label';
					if (llave == 'id_segured') newkey = 'valor';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				new_row['estado'] = 0;
				datos.push(new_row);
			});
			$.widgetModalmultiple2.update({
				data: datos
			});
		} else {
			var eliminarModelo4_i = Alloy.Collections.estructura_soportante;
			var sql = "DELETE FROM " + eliminarModelo4_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo4_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo4_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo5_m = Alloy.Collections.estructura_soportante;
				var insertarModelo5_fila = Alloy.createModel('estructura_soportante', {
					nombre: String.format(L('x1088195980_traducir', 'Estructura%1$s'), item.toString()),
					id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
					pais_texto: 'Chile',
					id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item.toString()),
					pais: 1
				});
				insertarModelo5_m.add(insertarModelo5_fila);
				insertarModelo5_fila.save();
			});
			var transformarModelo5_i = Alloy.createCollection('estructura_soportante');
			transformarModelo5_i.fetch();
			var transformarModelo5_src = require('helper').query2array(transformarModelo5_i);
			var datos = [];
			_.each(transformarModelo5_src, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'label';
					if (llave == 'id_segured') newkey = 'valor';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				new_row['estado'] = 0;
				datos.push(new_row);
			});
			$.widgetModalmultiple2.update({
				data: datos
			});
		}
	};
	var ID_848598486 = setTimeout(ID_848598486_func, 1000 * 0.2);

}

$.widgetModalmultiple3.init({
	titulo: L('x1219835481_traducir', 'Muros / Tabiques'),
	cargando: L('x1740321226_traducir', 'cargando ..'),
	__id: 'ALL1975428302',
	oncerrar: Cerrar_widgetModalmultiple3,
	hint: L('x2879998099_traducir', 'Seleccione muros'),
	color: 'verde',
	subtitulo: L('x4011106049_traducir', 'Indique los tipos'),
	top: 5,
	onclick: Click_widgetModalmultiple3,
	onafterinit: Afterinit_widgetModalmultiple3
});

function Click_widgetModalmultiple3(e) {

	var evento = e;
	$.widgetYpicker.cerrar({});

}

function Cerrar_widgetModalmultiple3(e) {

	var evento = e;
	$.widgetModalmultiple3.update({});
	$.nivel.set({
		ids_muros: evento.valores
	});
	if ('nivel' in $) $nivel = $.nivel.toJSON();

}

function Afterinit_widgetModalmultiple3(e) {

	var evento = e;
	var ID_761628958_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			var consultarModelo6_i = Alloy.createCollection('muros_tabiques');
			var consultarModelo6_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
			consultarModelo6_i.fetch({
				query: 'SELECT * FROM muros_tabiques WHERE pais_texto=\'' + pais[0].nombre + '\''
			});
			var tabiques = require('helper').query2array(consultarModelo6_i);
			var datos = [];
			_.each(tabiques, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'label';
					if (llave == 'id_segured') newkey = 'valor';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				new_row['estado'] = 0;
				datos.push(new_row);
			});
			$.widgetModalmultiple3.update({
				data: datos
			});
		} else {
			var eliminarModelo5_i = Alloy.Collections.muros_tabiques;
			var sql = "DELETE FROM " + eliminarModelo5_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo5_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo5_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo6_m = Alloy.Collections.muros_tabiques;
				var insertarModelo6_fila = Alloy.createModel('muros_tabiques', {
					nombre: String.format(L('x3565664878_traducir', 'Muros%1$s'), item.toString()),
					id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
					pais_texto: 'Chile',
					id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item.toString()),
					pais: 1
				});
				insertarModelo6_m.add(insertarModelo6_fila);
				insertarModelo6_fila.save();
			});
			var transformarModelo7_i = Alloy.createCollection('muros_tabiques');
			transformarModelo7_i.fetch();
			var transformarModelo7_src = require('helper').query2array(transformarModelo7_i);
			var datos = [];
			_.each(transformarModelo7_src, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'label';
					if (llave == 'id_segured') newkey = 'valor';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				new_row['estado'] = 0;
				datos.push(new_row);
			});
			$.widgetModalmultiple3.update({
				data: datos
			});
		}
	};
	var ID_761628958 = setTimeout(ID_761628958_func, 1000 * 0.2);

}

$.widgetModalmultiple4.init({
	titulo: L('x3327059844_traducir', 'Entrepisos'),
	cargando: L('x1740321226_traducir', 'cargando ..'),
	__id: 'ALL399652068',
	oncerrar: Cerrar_widgetModalmultiple4,
	hint: L('x2146928948_traducir', 'Seleccione entrepisos'),
	color: 'verde',
	subtitulo: L('x4011106049_traducir', 'Indique los tipos'),
	top: 5,
	onclick: Click_widgetModalmultiple4,
	onafterinit: Afterinit_widgetModalmultiple4
});

function Click_widgetModalmultiple4(e) {

	var evento = e;
	$.widgetYpicker.cerrar({});

}

function Cerrar_widgetModalmultiple4(e) {

	var evento = e;
	$.widgetModalmultiple4.update({});
	$.nivel.set({
		ids_entrepisos: evento.valores
	});
	if ('nivel' in $) $nivel = $.nivel.toJSON();

}

function Afterinit_widgetModalmultiple4(e) {

	var evento = e;
	var ID_1534965449_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			var consultarModelo7_i = Alloy.createCollection('entrepisos');
			var consultarModelo7_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
			consultarModelo7_i.fetch({
				query: 'SELECT * FROM entrepisos WHERE pais_texto=\'' + pais[0].nombre + '\''
			});
			var entrepisos = require('helper').query2array(consultarModelo7_i);
			var datos = [];
			_.each(entrepisos, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'label';
					if (llave == 'id_segured') newkey = 'valor';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				new_row['estado'] = 0;
				datos.push(new_row);
			});
			$.widgetModalmultiple4.update({
				data: datos
			});
		} else {
			var eliminarModelo6_i = Alloy.Collections.entrepisos;
			var sql = "DELETE FROM " + eliminarModelo6_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo6_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo6_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo7_m = Alloy.Collections.entrepisos;
				var insertarModelo7_fila = Alloy.createModel('entrepisos', {
					nombre: String.format(L('x2266735154_traducir', 'Entrepiso%1$s'), item.toString()),
					id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
					pais_texto: 'Chile',
					id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item.toString()),
					pais: 1
				});
				insertarModelo7_m.add(insertarModelo7_fila);
				insertarModelo7_fila.save();
			});
			var transformarModelo9_i = Alloy.createCollection('entrepisos');
			transformarModelo9_i.fetch();
			var transformarModelo9_src = require('helper').query2array(transformarModelo9_i);
			var datos = [];
			_.each(transformarModelo9_src, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'label';
					if (llave == 'id_segured') newkey = 'valor';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				new_row['estado'] = 0;
				datos.push(new_row);
			});
			$.widgetModalmultiple4.update({
				data: datos
			});
		}
	};
	var ID_1534965449 = setTimeout(ID_1534965449_func, 1000 * 0.2);

}

$.widgetModalmultiple5.init({
	titulo: L('x591862035_traducir', 'Pavimentos'),
	cargando: L('x1740321226_traducir', 'cargando ..'),
	__id: 'ALL1990255213',
	oncerrar: Cerrar_widgetModalmultiple5,
	hint: L('x2600368035_traducir', 'Seleccione pavimentos'),
	color: 'verde',
	subtitulo: L('x4011106049_traducir', 'Indique los tipos'),
	top: 5,
	onclick: Click_widgetModalmultiple5,
	onafterinit: Afterinit_widgetModalmultiple5
});

function Click_widgetModalmultiple5(e) {

	var evento = e;
	$.widgetYpicker.cerrar({});

}

function Cerrar_widgetModalmultiple5(e) {

	var evento = e;
	$.widgetModalmultiple5.update({});
	$.nivel.set({
		ids_pavimentos: evento.valores
	});
	if ('nivel' in $) $nivel = $.nivel.toJSON();

}

function Afterinit_widgetModalmultiple5(e) {

	var evento = e;
	var ID_60762849_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			var consultarModelo8_i = Alloy.createCollection('pavimento');
			var consultarModelo8_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
			consultarModelo8_i.fetch({
				query: 'SELECT * FROM pavimento WHERE pais_texto=\'' + pais[0].nombre + '\''
			});
			var pavimentos = require('helper').query2array(consultarModelo8_i);
			var datos = [];
			_.each(pavimentos, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'label';
					if (llave == 'id_segured') newkey = 'valor';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				new_row['estado'] = 0;
				datos.push(new_row);
			});
			$.widgetModalmultiple5.update({
				data: datos
			});
		} else {
			var eliminarModelo7_i = Alloy.Collections.pavimento;
			var sql = "DELETE FROM " + eliminarModelo7_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo7_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo7_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo8_m = Alloy.Collections.pavimento;
				var insertarModelo8_fila = Alloy.createModel('pavimento', {
					nombre: String.format(L('x427067467_traducir', 'Pavimento%1$s'), item.toString()),
					id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
					pais_texto: 'Chile',
					id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item.toString()),
					pais: 1
				});
				insertarModelo8_m.add(insertarModelo8_fila);
				insertarModelo8_fila.save();
			});
			var transformarModelo11_i = Alloy.createCollection('pavimento');
			transformarModelo11_i.fetch();
			var transformarModelo11_src = require('helper').query2array(transformarModelo11_i);
			var datos = [];
			_.each(transformarModelo11_src, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'label';
					if (llave == 'id_segured') newkey = 'valor';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				new_row['estado'] = 0;
				datos.push(new_row);
			});
			$.widgetModalmultiple5.update({
				data: datos
			});
		}
	};
	var ID_60762849 = setTimeout(ID_60762849_func, 1000 * 0.2);

}

$.widgetModalmultiple6.init({
	titulo: L('x1866523485_traducir', 'Estruct. cubierta'),
	cargando: L('x1740321226_traducir', 'cargando ..'),
	__id: 'ALL1536081210',
	oncerrar: Cerrar_widgetModalmultiple6,
	hint: L('x2460890829_traducir', 'Seleccione e.cubiertas'),
	color: 'verde',
	subtitulo: L('x4011106049_traducir', 'Indique los tipos'),
	top: 5,
	onclick: Click_widgetModalmultiple6,
	onafterinit: Afterinit_widgetModalmultiple6
});

function Click_widgetModalmultiple6(e) {

	var evento = e;
	$.widgetYpicker.cerrar({});

}

function Cerrar_widgetModalmultiple6(e) {

	var evento = e;
	$.widgetModalmultiple6.update({});
	$.nivel.set({
		ids_estructura_cubiera: evento.valores
	});
	if ('nivel' in $) $nivel = $.nivel.toJSON();

}

function Afterinit_widgetModalmultiple6(e) {

	var evento = e;
	var ID_602043695_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			var consultarModelo9_i = Alloy.createCollection('estructura_cubierta');
			var consultarModelo9_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
			consultarModelo9_i.fetch({
				query: 'SELECT * FROM estructura_cubierta WHERE pais_texto=\'' + pais[0].nombre + '\''
			});
			var ecubiertas = require('helper').query2array(consultarModelo9_i);
			var datos = [];
			_.each(ecubiertas, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'label';
					if (llave == 'id_segured') newkey = 'valor';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				new_row['estado'] = 0;
				datos.push(new_row);
			});
			$.widgetModalmultiple6.update({
				data: datos
			});
		} else {
			var eliminarModelo8_i = Alloy.Collections.estructura_cubierta;
			var sql = "DELETE FROM " + eliminarModelo8_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo8_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo8_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo9_m = Alloy.Collections.estructura_cubierta;
				var insertarModelo9_fila = Alloy.createModel('estructura_cubierta', {
					nombre: String.format(L('x1686539481_traducir', 'Estru Cubierta %1$s'), item.toString()),
					id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
					pais_texto: 'Chile',
					id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item.toString()),
					pais: 1
				});
				insertarModelo9_m.add(insertarModelo9_fila);
				insertarModelo9_fila.save();
			});
			var transformarModelo13_i = Alloy.createCollection('estructura_cubierta');
			transformarModelo13_i.fetch();
			var transformarModelo13_src = require('helper').query2array(transformarModelo13_i);
			var datos = [];
			_.each(transformarModelo13_src, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'label';
					if (llave == 'id_segured') newkey = 'valor';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				new_row['estado'] = 0;
				datos.push(new_row);
			});
			$.widgetModalmultiple6.update({
				data: datos
			});
		}
	};
	var ID_602043695 = setTimeout(ID_602043695_func, 1000 * 0.2);

}

$.widgetModalmultiple7.init({
	titulo: L('x2266302645_traducir', 'Cubierta'),
	cargando: L('x1740321226_traducir', 'cargando ..'),
	__id: 'ALL363463535',
	oncerrar: Cerrar_widgetModalmultiple7,
	hint: L('x2134385782_traducir', 'Seleccione cubiertas'),
	color: 'verde',
	subtitulo: L('x4011106049_traducir', 'Indique los tipos'),
	top: 5,
	onclick: Click_widgetModalmultiple7,
	onafterinit: Afterinit_widgetModalmultiple7
});

function Click_widgetModalmultiple7(e) {

	var evento = e;
	$.widgetYpicker.cerrar({});

}

function Cerrar_widgetModalmultiple7(e) {

	var evento = e;
	$.widgetModalmultiple7.update({});
	$.nivel.set({
		ids_cubierta: evento.valores
	});
	if ('nivel' in $) $nivel = $.nivel.toJSON();

}

function Afterinit_widgetModalmultiple7(e) {

	var evento = e;
	var ID_1301596767_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			var consultarModelo10_i = Alloy.createCollection('cubierta');
			var consultarModelo10_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
			consultarModelo10_i.fetch({
				query: 'SELECT * FROM cubierta WHERE pais_texto=\'' + pais[0].nombre + '\''
			});
			var cubiertas = require('helper').query2array(consultarModelo10_i);
			var datos = [];
			_.each(cubiertas, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'label';
					if (llave == 'id_segured') newkey = 'valor';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				new_row['estado'] = 0;
				datos.push(new_row);
			});
			$.widgetModalmultiple7.update({
				data: datos
			});
		} else {
			var eliminarModelo9_i = Alloy.Collections.cubierta;
			var sql = "DELETE FROM " + eliminarModelo9_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo9_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo9_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo10_m = Alloy.Collections.cubierta;
				var insertarModelo10_fila = Alloy.createModel('cubierta', {
					nombre: String.format(L('x2246230604_traducir', 'Cubierta %1$s'), item.toString()),
					id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
					pais_texto: 'Chile',
					id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item.toString()),
					pais: 1
				});
				insertarModelo10_m.add(insertarModelo10_fila);
				insertarModelo10_fila.save();
			});
			var transformarModelo15_i = Alloy.createCollection('cubierta');
			transformarModelo15_i.fetch();
			var transformarModelo15_src = require('helper').query2array(transformarModelo15_i);
			var datos = [];
			_.each(transformarModelo15_src, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'label';
					if (llave == 'id_segured') newkey = 'valor';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				new_row['estado'] = 0;
				datos.push(new_row);
			});
			$.widgetModalmultiple7.update({
				data: datos
			});
		}
	};
	var ID_1301596767 = setTimeout(ID_1301596767_func, 1000 * 0.2);

}

function Click_scroll2(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.IngreseNombre.blur();
	$.campo.blur();
	$.campo2.blur();
	$.campo3.blur();
	$.campo4.blur();

}

(function() {
	if (!_.isUndefined(args._fila)) {
		/** 
		 * heredamos datos previos de nivel en modelo nuevo con binding de esta pantalla 
		 */
		var consultarModelo11_i = Alloy.createCollection('insp_niveles');
		var consultarModelo11_i_where = 'id=\'' + args._fila.id + '\'';
		consultarModelo11_i.fetch({
			query: 'SELECT * FROM insp_niveles WHERE id=\'' + args._fila.id + '\''
		});
		var insp_n = require('helper').query2array(consultarModelo11_i);
		$.nivel.set({
			ids_entrepisos: insp_n[0].ids_entrepisos,
			id_inspeccion: insp_n[0].id_inspeccion,
			nombre: insp_n[0].nombre,
			superficie: insp_n[0].superficie,
			largo: insp_n[0].largo,
			ids_pavimentos: insp_n[0].ids_pavimentos,
			alto: insp_n[0].alto,
			ano: insp_n[0].ano,
			ids_muros: insp_n[0].ids_muros,
			ancho: insp_n[0].ancho,
			ids_estructura_cubiera: insp_n[0].ids_estructura_cubiera,
			piso: insp_n[0].piso,
			ids_cubierta: insp_n[0].ids_cubierta,
			ids_estructuras_soportantes: insp_n[0].ids_estructuras_soportantes
		});
		if ('nivel' in $) $nivel = $.nivel.toJSON();
		require('vars')['nivel_editado'] = insp_n[0];
		var NUEVO_NIVEL_titulo = 'EDITAR NIVEL';

		var setTitle2 = function(valor) {
			if (OS_ANDROID) {
				abx.title = valor;
			} else {
				$.NUEVO_NIVEL_window.setTitle(valor);
			}
		};
		var getTitle2 = function() {
			if (OS_ANDROID) {
				return abx.title;
			} else {
				return $.NUEVO_NIVEL_window.getTitle();
			}
		};
		setTitle2(NUEVO_NIVEL_titulo);

	}
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
		$.nivel.set({
			id_inspeccion: seltarea.id_server
		});
		if ('nivel' in $) $nivel = $.nivel.toJSON();
	}
})();

if (OS_IOS || OS_ANDROID) {
	$.NUEVO_NIVEL.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}