var _bind4section = {
	"ref1": "insp_itemdanos"
};
var _list_templates = {
	"dano": {
		"Label4": {
			"text": "{id}"
		},
		"vista15": {},
		"Label3": {
			"text": "{nombre}"
		}
	}
};
var $recinto = $.recinto.toJSON();

$.NUEVO_RECINTO_window.setTitleAttributes({
	color: 'WHITE'
});
var _activity;
if (OS_ANDROID) {
	_activity = $.NUEVO_RECINTO.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'NUEVO_RECINTO';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.NUEVO_RECINTO_window.addEventListener('open', function(e) {
		abx.setStatusbarColor("#FFFFFF");
		abx.setBackgroundColor("#b9aaf3");
	});
}


var consultarModelo5_like = function(search) {
	if (typeof search !== 'string' || this === null) {
		return false;
	}
	search = search.replace(new RegExp("([\\.\\\\\\+\\*\\?\\[\\^\\]\\$\\(\\)\\{\\}\\=\\!\\<\\>\\|\\:\\-])", "g"), "\\$1");
	search = search.replace(/%/g, '.*').replace(/_/g, '.');
	return RegExp('^' + search + '$', 'gi').test(this);
};
var consultarModelo5_filter = function(coll) {
	var filtered = coll.filter(function(m) {
		var _tests = [],
			_all_true = false,
			model = m.toJSON();
		_tests.push((model.id_recinto == '0'));
		var _all_true_s = _.uniq(_tests);
		_all_true = (_all_true_s.length == 1 && _all_true_s[0] == true) ? true : false;
		return _all_true;
	});
	filtered = _.toArray(filtered);
	return filtered;
};
var consultarModelo5_transform = function(model) {
	var fila = model.toJSON();
	return fila;
};
var consultarModelo5_update = function(e) {};
_.defer(function() {
	Alloy.Collections.insp_itemdanos.fetch();
});
Alloy.Collections.insp_itemdanos.on('add change delete', function(ee) {
	consultarModelo5_update(ee);
});
Alloy.Collections.insp_itemdanos.fetch();

function Click_vista8(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	/** 
	 * Limpiamos widgets multiples (memoria) 
	 */
	$.widgetModalmultiple.limpiar({});
	$.widgetModalmultiple2.limpiar({});
	$.widgetModalmultiple3.limpiar({});
	$.widgetModalmultiple4.limpiar({});
	$.widgetModalmultiple5.limpiar({});
	$.widgetModalmultiple6.limpiar({});
	$.NUEVO_RECINTO.close();

}

function Click_vista10(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var test = $.recinto.toJSON();
	var temp_idrecinto = ('temp_idrecinto' in require('vars')) ? require('vars')['temp_idrecinto'] : '';
	/** 
	 * Consultamos la tabla de danos, filtrando por el id del recinto y saber si tiene danos 
	 */
	var consultarModelo6_i = Alloy.createCollection('insp_itemdanos');
	var consultarModelo6_i_where = 'id_recinto=\'' + temp_idrecinto + '\'';
	consultarModelo6_i.fetch({
		query: 'SELECT * FROM insp_itemdanos WHERE id_recinto=\'' + temp_idrecinto + '\''
	});
	var danos_verificacion = require('helper').query2array(consultarModelo6_i);
	if (_.isUndefined(test.nombre)) {
		/** 
		 * Validamos que existan los datos ingresados y que esten bien 
		 */
		var preguntarAlerta3_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta3 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x995453488_traducir', 'Ingrese el nombre del recinto'),
			buttonNames: preguntarAlerta3_opts
		});
		preguntarAlerta3.addEventListener('click', function(e) {
			var nulo = preguntarAlerta3_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta3.show();
	} else if ((_.isObject(test.nombre) || _.isString(test.nombre)) && _.isEmpty(test.nombre)) {
		var preguntarAlerta4_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta4 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x995453488_traducir', 'Ingrese el nombre del recinto'),
			buttonNames: preguntarAlerta4_opts
		});
		preguntarAlerta4.addEventListener('click', function(e) {
			var nulo = preguntarAlerta4_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta4.show();
	} else if (_.isUndefined(test.id_nivel)) {
		var preguntarAlerta5_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta5 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x2221207910_traducir', 'Seleccione el nivel al cual pertenece el nuevo recinto'),
			buttonNames: preguntarAlerta5_opts
		});
		preguntarAlerta5.addEventListener('click', function(e) {
			var nulo = preguntarAlerta5_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta5.show();
	} else if (_.isUndefined(test.largo)) {
		var preguntarAlerta6_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta6 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x2947701485_traducir', 'Ingrese largo del recinto'),
			buttonNames: preguntarAlerta6_opts
		});
		preguntarAlerta6.addEventListener('click', function(e) {
			var nulo = preguntarAlerta6_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta6.show();
	} else if ((_.isObject(test.largo) || _.isString(test.largo)) && _.isEmpty(test.largo)) {
		var preguntarAlerta7_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta7 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x2947701485_traducir', 'Ingrese largo del recinto'),
			buttonNames: preguntarAlerta7_opts
		});
		preguntarAlerta7.addEventListener('click', function(e) {
			var nulo = preguntarAlerta7_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta7.show();
	} else if (_.isUndefined(test.ancho)) {
		var preguntarAlerta8_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta8 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x2565566100_traducir', 'Ingrese ancho del recinto'),
			buttonNames: preguntarAlerta8_opts
		});
		preguntarAlerta8.addEventListener('click', function(e) {
			var nulo = preguntarAlerta8_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta8.show();
	} else if ((_.isObject(test.ancho) || _.isString(test.ancho)) && _.isEmpty(test.ancho)) {
		var preguntarAlerta9_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta9 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x2565566100_traducir', 'Ingrese ancho del recinto'),
			buttonNames: preguntarAlerta9_opts
		});
		preguntarAlerta9.addEventListener('click', function(e) {
			var nulo = preguntarAlerta9_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta9.show();
	} else if (_.isUndefined(test.alto)) {
		var preguntarAlerta10_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta10 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x2365115278_traducir', 'Ingrese alto del recinto'),
			buttonNames: preguntarAlerta10_opts
		});
		preguntarAlerta10.addEventListener('click', function(e) {
			var nulo = preguntarAlerta10_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta10.show();
	} else if ((_.isObject(test.alto) || _.isString(test.alto)) && _.isEmpty(test.alto)) {
		var preguntarAlerta11_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta11 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x2365115278_traducir', 'Ingrese alto del recinto'),
			buttonNames: preguntarAlerta11_opts
		});
		preguntarAlerta11.addEventListener('click', function(e) {
			var nulo = preguntarAlerta11_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta11.show();
	} else if (_.isUndefined(test.ids_estructuras_soportantes)) {
		var preguntarAlerta12_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta12 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x503969960_traducir', 'Seleccione la estructura soportante del recinto'),
			buttonNames: preguntarAlerta12_opts
		});
		preguntarAlerta12.addEventListener('click', function(e) {
			var nulo = preguntarAlerta12_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta12.show();
	} else if ((_.isObject(test.ids_estructuras_soportantes) || _.isString(test.ids_estructuras_soportantes)) && _.isEmpty(test.ids_estructuras_soportantes)) {
		var preguntarAlerta13_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta13 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x503969960_traducir', 'Seleccione la estructura soportante del recinto'),
			buttonNames: preguntarAlerta13_opts
		});
		preguntarAlerta13.addEventListener('click', function(e) {
			var nulo = preguntarAlerta13_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta13.show();
	} else if (danos_verificacion && danos_verificacion.length == 0) {
		var preguntarAlerta14_opts = [L('x4151998846_traducir', 'Volver'), L('x2923459930_traducir', ' Guardar')];
		var preguntarAlerta14 = Ti.UI.createAlertDialog({
			title: L('x1927199828_traducir', '¿Guardar sin daños?'),
			message: L('x2099678332_traducir', 'Estas guardando el recinto sin daños asociados, si esta correcto presiona guardar'),
			buttonNames: preguntarAlerta14_opts
		});
		preguntarAlerta14.addEventListener('click', function(e) {
			var msj = preguntarAlerta14_opts[e.index];
			if (msj == L('x2943883035_traducir', 'Guardar')) {
				if (!_.isUndefined(args._dato)) {
					/** 
					 * Si estamos editando, debemos borrar modelo previo. 
					 */
					var idrecintoactual = ('idrecintoactual' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['idrecintoactual'] : '';
					var eliminarModelo_i = Alloy.Collections.insp_recintos;
					var sql = 'DELETE FROM ' + eliminarModelo_i.config.adapter.collection_name + ' WHERE id=\'' + idrecintoactual + '\'';
					var db = Ti.Database.open(eliminarModelo_i.config.adapter.db_name);
					db.execute(sql);
					db.close();
					eliminarModelo_i.trigger('remove');
				}
				Alloy.Collections[$.recinto.config.adapter.collection_name].add($.recinto);
				$.recinto.save();
				Alloy.Collections[$.recinto.config.adapter.collection_name].fetch();
				test = null;
				$.widgetModalmultiple.limpiar({});
				$.widgetModalmultiple2.limpiar({});
				$.widgetModalmultiple3.limpiar({});
				$.widgetModalmultiple4.limpiar({});
				$.widgetModalmultiple5.limpiar({});
				$.widgetModalmultiple6.limpiar({});
				$.NUEVO_RECINTO.close();
			}
			msj = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta14.show();
	} else {
		if (!_.isUndefined(args._dato)) {
			/** 
			 * Si estamos editando, debemos borrar modelo previo. 
			 */
			var idrecintoactual = ('idrecintoactual' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['idrecintoactual'] : '';
			var eliminarModelo2_i = Alloy.Collections.insp_recintos;
			var sql = 'DELETE FROM ' + eliminarModelo2_i.config.adapter.collection_name + ' WHERE id=\'' + idrecintoactual + '\'';
			var db = Ti.Database.open(eliminarModelo2_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo2_i.trigger('remove');
		}
		/** 
		 * Guardamos el modelo con los datos ingresado 
		 */
		Alloy.Collections[$.recinto.config.adapter.collection_name].add($.recinto);
		$.recinto.save();
		Alloy.Collections[$.recinto.config.adapter.collection_name].fetch();
		test = null;
		/** 
		 * Limpiamos widgets multiples (memoria) 
		 */
		$.widgetModalmultiple.limpiar({});
		$.widgetModalmultiple2.limpiar({});
		$.widgetModalmultiple3.limpiar({});
		$.widgetModalmultiple4.limpiar({});
		$.widgetModalmultiple5.limpiar({});
		$.widgetModalmultiple6.limpiar({});
		$.NUEVO_RECINTO.close();
	}
}

function Click_vista13(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	Alloy.createController("nuevo_dano", {}).getView().open();

}

function Itemclick_listado4(e) {

	e.cancelBubble = true;
	var objeto = e.section.getItemAt(e.itemIndex);
	var modelo = {},
		_modelo = [];
	var fila = {},
		fila_bak = {},
		info = {
			_template: objeto.template,
			_what: [],
			_seccion_ref: e.section.getHeaderTitle(),
			_model_id: -1
		},
		_tmp = {
			objmap: {}
		};
	if ('itemId' in e) {
		info._model_id = e.itemId;
		modelo._id = info._model_id;
		if (info._seccion_ref != '' && info._seccion_ref in _bind4section) {
			modelo._collection = _bind4section[info._seccion_ref];
			_tmp._coll = modelo._collection;
		}
	}
	var findVariables = require('fvariables');
	_.each(_list_templates[info._template], function(obj_id, id) {
		_.each(obj_id, function(valor, prop) {
			var llaves = findVariables(valor, '{', '}');
			_.each(llaves, function(llave) {
				_tmp.objmap[llave] = {
					id: id,
					prop: prop
				};
				fila[llave] = objeto[id][prop];
				if (id == e.bindId) info._what.push(llave);
			});
		});
	});
	info._what = info._what.join(',');
	fila_bak = JSON.parse(JSON.stringify(fila));
	if (Ti.App.deployType != 'production') console.log('click en fila dano', {});
	/** 
	 * Enviamos el parametro _dato para indicar cual sera el id del dano a editar 
	 */
	Alloy.createController("nuevo_dano", {
		'_dato': fila,
		'__master_model': (typeof modelo !== 'undefined') ? modelo : {},
		'__modelo': (typeof _modelo !== 'undefined' && _modelo.length > 0) ? _modelo[0] : {}
	}).getView().open();
	_tmp.changed = false;
	_tmp.diff_keys = [];
	_.each(fila, function(value1, prop) {
		var had_samekey = false;
		_.each(fila_bak, function(value2, prop2) {
			if (prop == prop2 && value1 == value2) {
				had_samekey = true;
			} else if (!_.has(fila_bak, prop) || !_.has(fila, prop2)) {
				has_samekey = true;
			}
		});
		if (!had_samekey) _tmp.diff_keys.push(prop);
	});
	if (_tmp.diff_keys.length > 0) _tmp.changed = true;
	if (_tmp.changed == true) {
		_.each(_tmp.diff_keys, function(llave) {
			objeto[_tmp.objmap[llave].id][_tmp.objmap[llave].prop] = fila[llave];
		});
		e.section.updateItemAt(e.itemIndex, objeto);
	}

}

function Change_EscribaNombre(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	/** 
	 * Guardamos el nombre del recinto en el modelo 
	 */
	$.recinto.set({
		nombre: elemento
	});
	if ('recinto' in $) $recinto = $.recinto.toJSON();
	elemento = null, source = null;

}

$.widgetModal.init({
	titulo: L('x1571349115_traducir', 'NIVEL'),
	cargando: L('x1740321226_traducir', 'cargando ..'),
	__id: 'ALL1379917834',
	left: 0,
	onrespuesta: Respuesta_widgetModal,
	campo: L('x2771220443_traducir', 'Nivel del Recinto'),
	onabrir: Abrir_widgetModal,
	color: 'morado',
	right: 0,
	top: 20,
	seleccione: L('x2434264250_traducir', 'seleccione nivel'),
	activo: true,
	onafterinit: Afterinit_widgetModal
});

function Afterinit_widgetModal(e) {

	var evento = e;
	var ID_1130215393_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			/** 
			 * Revisamos si estamos haciendo una inspeccion o es un dummy 
			 */
			var consultarModelo7_i = Alloy.createCollection('insp_niveles');
			var consultarModelo7_i_where = 'id_inspeccion=\'' + seltarea.id_server + '\'';
			consultarModelo7_i.fetch({
				query: 'SELECT * FROM insp_niveles WHERE id_inspeccion=\'' + seltarea.id_server + '\''
			});
			var datosniveles = require('helper').query2array(consultarModelo7_i);
			/** 
			 * Obtenemos datos para selectores (solo los de esta inspeccion) 
			 */
			var datos = [];
			_.each(datosniveles, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'valor';
					if (llave == 'id') newkey = 'id_interno';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
		} else {
			/** 
			 * Eliminamos modelo insp_niveles 
			 */
			/** 
			 * Eliminamos modelo insp_niveles 
			 */
			var eliminarModelo3_i = Alloy.Collections.insp_niveles;
			var sql = "DELETE FROM " + eliminarModelo3_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo3_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo3_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4,5'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo2_m = Alloy.Collections.insp_niveles;
				var insertarModelo2_fila = Alloy.createModel('insp_niveles', {
					id_inspeccion: -1,
					nombre: String.format(L('x2818462194_traducir', 'Nivel%1$s'), item.toString()),
					piso: item
				});
				insertarModelo2_m.add(insertarModelo2_fila);
				insertarModelo2_fila.save();
				_.defer(function() {});
			});
			var transformarModelo2_i = Alloy.createCollection('insp_niveles');
			transformarModelo2_i.fetch();
			var transformarModelo2_src = require('helper').query2array(transformarModelo2_i);
			var datos = [];
			_.each(transformarModelo2_src, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'valor';
					if (llave == 'id') newkey = 'id_interno';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
		}
		if (!_.isUndefined(args._dato)) {
			/** 
			 * Si estamos editando contenido, cargamos dato previo seleccionado en base de datos. 
			 */
			var consultarModelo8_i = Alloy.createCollection('insp_recintos');
			var consultarModelo8_i_where = 'id=\'' + args._dato.id + '\'';
			consultarModelo8_i.fetch({
				query: 'SELECT * FROM insp_recintos WHERE id=\'' + args._dato.id + '\''
			});
			var recinto = require('helper').query2array(consultarModelo8_i);
			if (Ti.App.deployType != 'production') console.log('detalle del recinto a editar', {
				"datos": recinto
			});
			if (recinto && recinto.length) {
				/** 
				 * obtenemos valor seleccionado previamente y lo mostramos en selector. 
				 */
				var consultarModelo9_i = Alloy.createCollection('insp_niveles');
				var consultarModelo9_i_where = 'id=\'' + recinto[0].id_nivel + '\'';
				consultarModelo9_i.fetch({
					query: 'SELECT * FROM insp_niveles WHERE id=\'' + recinto[0].id_nivel + '\''
				});
				var nivel = require('helper').query2array(consultarModelo9_i);
				if (nivel && nivel.length) {
					var ID_52538855_func = function() {
						$.widgetModal.labels({
							valor: nivel[0].nombre
						});
						if (Ti.App.deployType != 'production') console.log('el nombre del recinto es', {
							"datos": nivel[0].nombre
						});
					};
					var ID_52538855 = setTimeout(ID_52538855_func, 1000 * 0.21);
				}
			}
		}
		$.widgetModal.data({
			data: datos
		});
	};
	var ID_1130215393 = setTimeout(ID_1130215393_func, 1000 * 0.2);

}

function Abrir_widgetModal(e) {

	var evento = e;

}

function Respuesta_widgetModal(e) {

	var evento = e;
	/** 
	 * Ponemos el texto del valor escogido en el widget 
	 */
	$.widgetModal.labels({
		valor: evento.valor
	});
	/** 
	 * Guardamos el id del nivel que seleccionamos 
	 */
	$.recinto.set({
		id_nivel: evento.item.id_interno
	});
	if ('recinto' in $) $recinto = $.recinto.toJSON();
	if (Ti.App.deployType != 'production') console.log('datos recibidos de modal seleccione nivel', {
		"datos": evento
	});

}

$.widgetFotochica.init({
	caja: L('x2889884971_traducir', '45'),
	__id: 'ALL62888976',
	onlisto: Listo_widgetFotochica,
	onclick: Click_widgetFotochica
});

function Click_widgetFotochica(e) {

	var evento = e;
	/** 
	 * Capturamos foto 
	 */
	var capturarFoto_result = function(e) {
		var foto_full = e.valor;
		var foto = e.valor.data;
		if (foto_full.error == true || foto_full.error == 'true') {
			/** 
			 * Si hay error al capturar foto muestra alerta 
			 */
			var preguntarAlerta15_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta15 = Ti.UI.createAlertDialog({
				title: L('x57652245_traducir', 'Alerta'),
				message: L('x1068520423_traducir', 'Existe un problema con la camara'),
				buttonNames: preguntarAlerta15_opts
			});
			preguntarAlerta15.addEventListener('click', function(e) {
				var suu = preguntarAlerta15_opts[e.index];
				suu = null;
				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta15.show();
		} else {
			/** 
			 * Procesa la foto 
			 */
			$.widgetFotochica.procesar({
				imagen: foto,
				nueva: 640,
				calidad: 91
			});
		}
	};

	function camara_capturarFoto() {
		Ti.Media.showCamera({
			success: function(event) {
				if (event.mediaType == Ti.Media.MEDIA_TYPE_PHOTO) {
					capturarFoto_result({
						valor: {
							error: false,
							cancel: false,
							data: event.media,
							reason: ''
						}
					});
				} else {
					capturarFoto_result({
						valor: {
							error: true,
							cancel: false,
							data: '',
							reason: 'not image'
						}
					});
				}
			},
			cancel: function() {
				capturarFoto_result({
					valor: {
						error: false,
						cancel: true,
						data: '',
						reason: 'cancelled'
					}
				});
			},
			error: function(error) {
				capturarFoto_result({
					valor: {
						error: true,
						cancel: false,
						data: error,
						reason: error.error
					}
				});
			},
			saveToPhotoGallery: false,
			allowImageEditing: true,
			mediaTypes: [Ti.Media.MEDIA_TYPE_PHOTO]
		});
	}
	require('vars')['_estadoflash_'] = 'auto';
	require('vars')['_tipocamara_'] = 'trasera';
	require('vars')['_hayflash_'] = true;
	if (Ti.Media.hasCameraPermissions()) {
		camara_capturarFoto();
	} else {
		Ti.Media.requestCameraPermissions(function(ercp) {
			if (ercp.success) {
				camara_capturarFoto();
			} else {
				capturarFoto_result({
					valor: {
						error: true,
						cancel: false,
						data: 'nopermission',
						type: 'permission',
						reason: 'camera doesnt have permissions'
					}
				});
			}
		});
	}

}

function Listo_widgetFotochica(e) {

	var evento = e;
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	var insertarModelo3_m = Alloy.Collections.numero_unico;
	var insertarModelo3_fila = Alloy.createModel('numero_unico', {
		comentario: 'nuevo recinto foto1'
	});
	insertarModelo3_m.add(insertarModelo3_fila);
	insertarModelo3_fila.save();
	var nuevoid = require('helper').model2object(insertarModelo3_m.last());
	_.defer(function() {});
	if (!_.isUndefined(evento.comprimida)) {
		if (Ti.App.deployType != 'production') console.log(String.format(L('x2721240094', 'imagen capturada, comprimida y asignada como cap%1$s.jpg'), nuevoid.id.toString()), {});
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var ID_870492955_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
			if (ID_870492955_d.exists() == false) ID_870492955_d.createDirectory();
			var ID_870492955_f = Ti.Filesystem.getFile(ID_870492955_d.resolve(), 'cap' + nuevoid.id + '.jpg');
			if (ID_870492955_f.exists() == true) ID_870492955_f.deleteFile();
			ID_870492955_f.write(evento.comprimida);
			ID_870492955_d = null;
			ID_870492955_f = null;
			var ID_1805445107_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'mini' + seltarea.id_server);
			if (ID_1805445107_d.exists() == false) ID_1805445107_d.createDirectory();
			var ID_1805445107_f = Ti.Filesystem.getFile(ID_1805445107_d.resolve(), 'cap' + nuevoid.id + '.jpg');
			if (ID_1805445107_f.exists() == true) ID_1805445107_f.deleteFile();
			ID_1805445107_f.write(evento.mini);
			ID_1805445107_d = null;
			ID_1805445107_f = null;
		} else {
			var ID_1705924032_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + 0);
			if (ID_1705924032_d.exists() == false) ID_1705924032_d.createDirectory();
			var ID_1705924032_f = Ti.Filesystem.getFile(ID_1705924032_d.resolve(), 'cap' + nuevoid.id + '.jpg');
			if (ID_1705924032_f.exists() == true) ID_1705924032_f.deleteFile();
			ID_1705924032_f.write(evento.comprimida);
			ID_1705924032_d = null;
			ID_1705924032_f = null;
			var ID_1114793737_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'mini' + 0);
			if (ID_1114793737_d.exists() == false) ID_1114793737_d.createDirectory();
			var ID_1114793737_f = Ti.Filesystem.getFile(ID_1114793737_d.resolve(), 'cap' + nuevoid.id + '.jpg');
			if (ID_1114793737_f.exists() == true) ID_1114793737_f.deleteFile();
			ID_1114793737_f.write(evento.mini);
			ID_1114793737_d = null;
			ID_1114793737_f = null;
		}
		$.recinto.set({
			foto1: String.format(L('x1070475521', 'cap%1$s.jpg'), nuevoid.id.toString())
		});
		if ('recinto' in $) $recinto = $.recinto.toJSON();
	}

}

$.widgetFotochica2.init({
	caja: L('x2889884971_traducir', '45'),
	__id: 'ALL1386533233',
	onlisto: Listo_widgetFotochica2,
	onclick: Click_widgetFotochica2
});

function Click_widgetFotochica2(e) {

	var evento = e;
	var capturarFoto2_result = function(e) {
		var foto_full = e.valor;
		var foto = e.valor.data;
		if (foto_full.error == true || foto_full.error == 'true') {
			var preguntarAlerta16_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta16 = Ti.UI.createAlertDialog({
				title: L('x57652245_traducir', 'Alerta'),
				message: L('x1068520423_traducir', 'Existe un problema con la camara'),
				buttonNames: preguntarAlerta16_opts
			});
			preguntarAlerta16.addEventListener('click', function(e) {
				var suu = preguntarAlerta16_opts[e.index];
				suu = null;
				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta16.show();
		} else {
			$.widgetFotochica2.procesar({
				imagen: foto,
				nueva: 640,
				calidad: 91
			});
		}
	};

	function camara_capturarFoto2() {
		Ti.Media.showCamera({
			success: function(event) {
				if (event.mediaType == Ti.Media.MEDIA_TYPE_PHOTO) {
					capturarFoto2_result({
						valor: {
							error: false,
							cancel: false,
							data: event.media,
							reason: ''
						}
					});
				} else {
					capturarFoto2_result({
						valor: {
							error: true,
							cancel: false,
							data: '',
							reason: 'not image'
						}
					});
				}
			},
			cancel: function() {
				capturarFoto2_result({
					valor: {
						error: false,
						cancel: true,
						data: '',
						reason: 'cancelled'
					}
				});
			},
			error: function(error) {
				capturarFoto2_result({
					valor: {
						error: true,
						cancel: false,
						data: error,
						reason: error.error
					}
				});
			},
			saveToPhotoGallery: false,
			allowImageEditing: true,
			mediaTypes: [Ti.Media.MEDIA_TYPE_PHOTO]
		});
	}
	require('vars')['_estadoflash_'] = 'auto';
	require('vars')['_tipocamara_'] = 'trasera';
	require('vars')['_hayflash_'] = true;
	if (Ti.Media.hasCameraPermissions()) {
		camara_capturarFoto2();
	} else {
		Ti.Media.requestCameraPermissions(function(ercp) {
			if (ercp.success) {
				camara_capturarFoto2();
			} else {
				capturarFoto2_result({
					valor: {
						error: true,
						cancel: false,
						data: 'nopermission',
						type: 'permission',
						reason: 'camera doesnt have permissions'
					}
				});
			}
		});
	}

}

function Listo_widgetFotochica2(e) {

	var evento = e;
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	var insertarModelo4_m = Alloy.Collections.numero_unico;
	var insertarModelo4_fila = Alloy.createModel('numero_unico', {
		comentario: 'nuevo recinto foto2'
	});
	insertarModelo4_m.add(insertarModelo4_fila);
	insertarModelo4_fila.save();
	var nuevoid = require('helper').model2object(insertarModelo4_m.last());
	_.defer(function() {});
	if (!_.isUndefined(evento.comprimida)) {
		if (Ti.App.deployType != 'production') console.log(String.format(L('x2721240094', 'imagen capturada, comprimida y asignada como cap%1$s.jpg'), nuevoid.id.toString()), {});
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var ID_394616061_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
			if (ID_394616061_d.exists() == false) ID_394616061_d.createDirectory();
			var ID_394616061_f = Ti.Filesystem.getFile(ID_394616061_d.resolve(), 'cap' + nuevoid.id + '.jpg');
			if (ID_394616061_f.exists() == true) ID_394616061_f.deleteFile();
			ID_394616061_f.write(evento.comprimida);
			ID_394616061_d = null;
			ID_394616061_f = null;
			var ID_515170206_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'mini' + seltarea.id_server);
			if (ID_515170206_d.exists() == false) ID_515170206_d.createDirectory();
			var ID_515170206_f = Ti.Filesystem.getFile(ID_515170206_d.resolve(), 'cap' + nuevoid.id + '.jpg');
			if (ID_515170206_f.exists() == true) ID_515170206_f.deleteFile();
			ID_515170206_f.write(evento.mini);
			ID_515170206_d = null;
			ID_515170206_f = null;
		} else {
			var ID_870090468_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + 0);
			if (ID_870090468_d.exists() == false) ID_870090468_d.createDirectory();
			var ID_870090468_f = Ti.Filesystem.getFile(ID_870090468_d.resolve(), 'cap' + nuevoid.id + '.jpg');
			if (ID_870090468_f.exists() == true) ID_870090468_f.deleteFile();
			ID_870090468_f.write(evento.comprimida);
			ID_870090468_d = null;
			ID_870090468_f = null;
			var ID_1119169584_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'mini' + 0);
			if (ID_1119169584_d.exists() == false) ID_1119169584_d.createDirectory();
			var ID_1119169584_f = Ti.Filesystem.getFile(ID_1119169584_d.resolve(), 'cap' + nuevoid.id + '.jpg');
			if (ID_1119169584_f.exists() == true) ID_1119169584_f.deleteFile();
			ID_1119169584_f.write(evento.mini);
			ID_1119169584_d = null;
			ID_1119169584_f = null;
		}
		$.recinto.set({
			foto2: String.format(L('x1070475521', 'cap%1$s.jpg'), nuevoid.id.toString())
		});
		if ('recinto' in $) $recinto = $.recinto.toJSON();
	}

}

$.widgetFotochica3.init({
	caja: L('x2889884971_traducir', '45'),
	__id: 'ALL389922953',
	onlisto: Listo_widgetFotochica3,
	onclick: Click_widgetFotochica3
});

function Click_widgetFotochica3(e) {

	var evento = e;
	var capturarFoto3_result = function(e) {
		var foto_full = e.valor;
		var foto = e.valor.data;
		if (foto_full.error == true || foto_full.error == 'true') {
			var preguntarAlerta17_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta17 = Ti.UI.createAlertDialog({
				title: L('x57652245_traducir', 'Alerta'),
				message: L('x1068520423_traducir', 'Existe un problema con la camara'),
				buttonNames: preguntarAlerta17_opts
			});
			preguntarAlerta17.addEventListener('click', function(e) {
				var suu = preguntarAlerta17_opts[e.index];
				suu = null;
				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta17.show();
		} else {
			$.widgetFotochica3.procesar({
				imagen: foto,
				nueva: 640,
				calidad: 91
			});
		}
	};

	function camara_capturarFoto3() {
		Ti.Media.showCamera({
			success: function(event) {
				if (event.mediaType == Ti.Media.MEDIA_TYPE_PHOTO) {
					capturarFoto3_result({
						valor: {
							error: false,
							cancel: false,
							data: event.media,
							reason: ''
						}
					});
				} else {
					capturarFoto3_result({
						valor: {
							error: true,
							cancel: false,
							data: '',
							reason: 'not image'
						}
					});
				}
			},
			cancel: function() {
				capturarFoto3_result({
					valor: {
						error: false,
						cancel: true,
						data: '',
						reason: 'cancelled'
					}
				});
			},
			error: function(error) {
				capturarFoto3_result({
					valor: {
						error: true,
						cancel: false,
						data: error,
						reason: error.error
					}
				});
			},
			saveToPhotoGallery: false,
			allowImageEditing: true,
			mediaTypes: [Ti.Media.MEDIA_TYPE_PHOTO]
		});
	}
	require('vars')['_estadoflash_'] = 'auto';
	require('vars')['_tipocamara_'] = 'trasera';
	require('vars')['_hayflash_'] = true;
	if (Ti.Media.hasCameraPermissions()) {
		camara_capturarFoto3();
	} else {
		Ti.Media.requestCameraPermissions(function(ercp) {
			if (ercp.success) {
				camara_capturarFoto3();
			} else {
				capturarFoto3_result({
					valor: {
						error: true,
						cancel: false,
						data: 'nopermission',
						type: 'permission',
						reason: 'camera doesnt have permissions'
					}
				});
			}
		});
	}

}

function Listo_widgetFotochica3(e) {

	var evento = e;
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	var insertarModelo5_m = Alloy.Collections.numero_unico;
	var insertarModelo5_fila = Alloy.createModel('numero_unico', {
		comentario: 'nuevo recinto foto3'
	});
	insertarModelo5_m.add(insertarModelo5_fila);
	insertarModelo5_fila.save();
	var nuevoid = require('helper').model2object(insertarModelo5_m.last());
	_.defer(function() {});
	if (!_.isUndefined(evento.comprimida)) {
		if (Ti.App.deployType != 'production') console.log(String.format(L('x2721240094', 'imagen capturada, comprimida y asignada como cap%1$s.jpg'), nuevoid.id.toString()), {});
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var ID_695920476_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
			if (ID_695920476_d.exists() == false) ID_695920476_d.createDirectory();
			var ID_695920476_f = Ti.Filesystem.getFile(ID_695920476_d.resolve(), 'cap' + nuevoid.id + '.jpg');
			if (ID_695920476_f.exists() == true) ID_695920476_f.deleteFile();
			ID_695920476_f.write(evento.comprimida);
			ID_695920476_d = null;
			ID_695920476_f = null;
			var ID_1792806264_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'mini' + seltarea.id_server);
			if (ID_1792806264_d.exists() == false) ID_1792806264_d.createDirectory();
			var ID_1792806264_f = Ti.Filesystem.getFile(ID_1792806264_d.resolve(), 'cap' + nuevoid.id + '.jpg');
			if (ID_1792806264_f.exists() == true) ID_1792806264_f.deleteFile();
			ID_1792806264_f.write(evento.mini);
			ID_1792806264_d = null;
			ID_1792806264_f = null;
		} else {
			var ID_1374218651_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + 0);
			if (ID_1374218651_d.exists() == false) ID_1374218651_d.createDirectory();
			var ID_1374218651_f = Ti.Filesystem.getFile(ID_1374218651_d.resolve(), 'cap' + nuevoid.id + '.jpg');
			if (ID_1374218651_f.exists() == true) ID_1374218651_f.deleteFile();
			ID_1374218651_f.write(evento.comprimida);
			ID_1374218651_d = null;
			ID_1374218651_f = null;
			var ID_1330076367_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'mini' + 0);
			if (ID_1330076367_d.exists() == false) ID_1330076367_d.createDirectory();
			var ID_1330076367_f = Ti.Filesystem.getFile(ID_1330076367_d.resolve(), 'cap' + nuevoid.id + '.jpg');
			if (ID_1330076367_f.exists() == true) ID_1330076367_f.deleteFile();
			ID_1330076367_f.write(evento.mini);
			ID_1330076367_d = null;
			ID_1330076367_f = null;
		}
		$.recinto.set({
			foto3: String.format(L('x1070475521', 'cap%1$s.jpg'), nuevoid.id.toString())
		});
		if ('recinto' in $) $recinto = $.recinto.toJSON();
	}

}

function Change_campo(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	/** 
	 * Obtenemos el valor ingresado en campo de ancho, para poder calcular la superficie 
	 */
	var ancho;
	ancho = $.campo2.getValue();

	if ((_.isObject(ancho) || (_.isString(ancho)) && !_.isEmpty(ancho)) || _.isNumber(ancho) || _.isBoolean(ancho)) {
		if ((_.isObject(elemento) || (_.isString(elemento)) && !_.isEmpty(elemento)) || _.isNumber(elemento) || _.isBoolean(elemento)) {
			/** 
			 * nos aseguramos que ancho y valor de campo sean decimales con puntos (flotante), y no comas (espa&#241;ol) 
			 */
			var nuevo = parseFloat(ancho.split(',').join('.')) * parseFloat(elemento.split(',').join('.'));
			$.recinto.set({
				superficie: nuevo
			});
			if ('recinto' in $) $recinto = $.recinto.toJSON();
		}
	}
	/** 
	 * Guardamos el largo del recinto 
	 */
	$.recinto.set({
		largo: elemento
	});
	if ('recinto' in $) $recinto = $.recinto.toJSON();
	elemento = null, source = null;

}

function Focus_campo(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	elemento = null, source = null;

}

function Change_campo2(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	var largo;
	largo = $.campo.getValue();

	if ((_.isObject(largo) || (_.isString(largo)) && !_.isEmpty(largo)) || _.isNumber(largo) || _.isBoolean(largo)) {
		if ((_.isObject(elemento) || (_.isString(elemento)) && !_.isEmpty(elemento)) || _.isNumber(elemento) || _.isBoolean(elemento)) {
			/** 
			 * nos aseguramos que largo y valor de campo sean decimales con puntos (flotante), y no comas (espa&#241;ol) 
			 */
			var nuevo = parseFloat(largo.split(',').join('.')) * parseFloat(elemento.split(',').join('.'));
			$.recinto.set({
				superficie: nuevo
			});
			if ('recinto' in $) $recinto = $.recinto.toJSON();
		}
	}
	$.recinto.set({
		ancho: elemento
	});
	if ('recinto' in $) $recinto = $.recinto.toJSON();
	elemento = null, source = null;

}

function Focus_campo2(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	elemento = null, source = null;

}

function Change_campo3(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	$.recinto.set({
		alto: elemento
	});
	if ('recinto' in $) $recinto = $.recinto.toJSON();
	elemento = null, source = null;

}

function Focus_campo3(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	elemento = null, source = null;

}

$.widgetModalmultiple.init({
	titulo: L('x1975271086_traducir', 'Estructura Soportante'),
	cargando: L('x1740321226_traducir', 'cargando ..'),
	__id: 'ALL321122430',
	oncerrar: Cerrar_widgetModalmultiple,
	left: 0,
	hint: L('x2898603391_traducir', 'Seleccione estructura'),
	color: 'morado',
	subtitulo: L('x4011106049_traducir', 'Indique los tipos'),
	right: 0,
	top: 5,
	onclick: Click_widgetModalmultiple,
	onafterinit: Afterinit_widgetModalmultiple
});

function Click_widgetModalmultiple(e) {

	var evento = e;

}

function Cerrar_widgetModalmultiple(e) {

	var evento = e;
	$.widgetModalmultiple.update({});
	$.recinto.set({
		ids_estructuras_soportantes: evento.valores
	});
	if ('recinto' in $) $recinto = $.recinto.toJSON();

}

function Afterinit_widgetModalmultiple(e) {

	var evento = e;
	var ID_1260507929_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			var consultarModelo10_i = Alloy.createCollection('estructura_soportante');
			var consultarModelo10_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
			consultarModelo10_i.fetch({
				query: 'SELECT * FROM estructura_soportante WHERE pais_texto=\'' + pais[0].nombre + '\''
			});
			var estructura = require('helper').query2array(consultarModelo10_i);
			var datos = [];
			_.each(estructura, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'label';
					if (llave == 'id_segured') newkey = 'valor';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				new_row['estado'] = 0;
				datos.push(new_row);
			});
			$.widgetModalmultiple.update({
				data: datos
			});
		} else {
			var eliminarModelo4_i = Alloy.Collections.estructura_soportante;
			var sql = "DELETE FROM " + eliminarModelo4_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo4_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo4_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo6_m = Alloy.Collections.estructura_soportante;
				var insertarModelo6_fila = Alloy.createModel('estructura_soportante', {
					nombre: String.format(L('x1088195980_traducir', 'Estructura%1$s'), item.toString()),
					id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
					id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item.toString()),
					pais: 1
				});
				insertarModelo6_m.add(insertarModelo6_fila);
				insertarModelo6_fila.save();
				_.defer(function() {});
			});
			var transformarModelo4_i = Alloy.createCollection('estructura_soportante');
			transformarModelo4_i.fetch();
			var transformarModelo4_src = require('helper').query2array(transformarModelo4_i);
			var datos = [];
			_.each(transformarModelo4_src, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'label';
					if (llave == 'id_segured') newkey = 'valor';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				new_row['estado'] = 0;
				datos.push(new_row);
			});
			$.widgetModalmultiple.update({
				data: datos
			});
		}
		if (!_.isUndefined(args._dato)) {
			var consultarModelo11_i = Alloy.createCollection('insp_recintos');
			var consultarModelo11_i_where = 'id=\'' + args._dato.id + '\'';
			consultarModelo11_i.fetch({
				query: 'SELECT * FROM insp_recintos WHERE id=\'' + args._dato.id + '\''
			});
			var recinto = require('helper').query2array(consultarModelo11_i);
			if (recinto && recinto.length) {
				/** 
				 * obtenemos valor seleccionado previamente y lo mostramos en selector. 
				 */
				if ((_.isObject(recinto[0].ids_estructuras_soportantes) || (_.isString(recinto[0].ids_estructuras_soportantes)) && !_.isEmpty(recinto[0].ids_estructuras_soportantes)) || _.isNumber(recinto[0].ids_estructuras_soportantes) || _.isBoolean(recinto[0].ids_estructuras_soportantes)) {
					var estructuras = recinto[0].ids_estructuras_soportantes;
					var estructurasSeparadas = estructuras.split(",");
					var estructurasElegidos = '';
					var a_index = 0;
					_.each(estructurasSeparadas, function(a, a_pos, a_list) {
						a_index += 1;
						var consultarModelo12_i = Alloy.createCollection('estructura_soportante');
						var consultarModelo12_i_where = 'id_segured=\'' + a + '\'';
						consultarModelo12_i.fetch({
							query: 'SELECT * FROM estructura_soportante WHERE id_segured=\'' + a + '\''
						});
						var estructura = require('helper').query2array(consultarModelo12_i);
						if ((_.isObject(estructurasElegidos) || _.isString(estructurasElegidos)) && _.isEmpty(estructurasElegidos)) {
							estructurasElegidos += estructura[0].nombre;
						} else {
							estructurasElegidos += ' + ' + estructura[0].nombre;
						}
						if (estructura && estructura.length) {
							var ID_991230781_func = function() {
								if (Ti.App.deployType != 'production') console.log('UPDATE', {
									"val": estructurasElegidos
								});
								$.widgetModalmultiple.update({
									valor: estructurasElegidos
								});
							};
							var ID_991230781 = setTimeout(ID_991230781_func, 1000 * 0.21);
						}
					});
				} else {
					if (Ti.App.deployType != 'production') console.log('no hay estructuras soportantes', {});
				}
			}
		}
	};
	var ID_1260507929 = setTimeout(ID_1260507929_func, 1000 * 0.2);

}

$.widgetModalmultiple2.init({
	titulo: L('x1219835481_traducir', 'Muros / Tabiques'),
	cargando: L('x1740321226_traducir', 'cargando ..'),
	__id: 'ALL1975428302',
	oncerrar: Cerrar_widgetModalmultiple2,
	left: 0,
	hint: L('x2879998099_traducir', 'Seleccione muros'),
	color: 'morado',
	subtitulo: L('x4011106049_traducir', 'Indique los tipos'),
	right: 0,
	top: 5,
	onclick: Click_widgetModalmultiple2,
	onafterinit: Afterinit_widgetModalmultiple2
});

function Click_widgetModalmultiple2(e) {

	var evento = e;

}

function Cerrar_widgetModalmultiple2(e) {

	var evento = e;
	$.widgetModalmultiple2.update({});
	$.recinto.set({
		ids_muros: evento.valores
	});
	if ('recinto' in $) $recinto = $.recinto.toJSON();

}

function Afterinit_widgetModalmultiple2(e) {

	var evento = e;
	var ID_93083937_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			var consultarModelo13_i = Alloy.createCollection('muros_tabiques');
			var consultarModelo13_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
			consultarModelo13_i.fetch({
				query: 'SELECT * FROM muros_tabiques WHERE pais_texto=\'' + pais[0].nombre + '\''
			});
			var tabiques = require('helper').query2array(consultarModelo13_i);
			var datos = [];
			_.each(tabiques, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'label';
					if (llave == 'id_segured') newkey = 'valor';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				new_row['estado'] = 0;
				datos.push(new_row);
			});
			$.widgetModalmultiple2.update({
				data: datos
			});
		} else {
			var eliminarModelo5_i = Alloy.Collections.muros_tabiques;
			var sql = "DELETE FROM " + eliminarModelo5_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo5_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo5_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo7_m = Alloy.Collections.muros_tabiques;
				var insertarModelo7_fila = Alloy.createModel('muros_tabiques', {
					nombre: String.format(L('x3565664878_traducir', 'Muros%1$s'), item.toString()),
					id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
					id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item.toString()),
					pais: 1
				});
				insertarModelo7_m.add(insertarModelo7_fila);
				insertarModelo7_fila.save();
				_.defer(function() {});
			});
			var transformarModelo6_i = Alloy.createCollection('muros_tabiques');
			transformarModelo6_i.fetch();
			var transformarModelo6_src = require('helper').query2array(transformarModelo6_i);
			var datos = [];
			_.each(transformarModelo6_src, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'label';
					if (llave == 'id_segured') newkey = 'valor';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				new_row['estado'] = 0;
				datos.push(new_row);
			});
			$.widgetModalmultiple2.update({
				data: datos
			});
		}
		if (!_.isUndefined(args._dato)) {
			var consultarModelo14_i = Alloy.createCollection('insp_recintos');
			var consultarModelo14_i_where = 'id=\'' + args._dato.id + '\'';
			consultarModelo14_i.fetch({
				query: 'SELECT * FROM insp_recintos WHERE id=\'' + args._dato.id + '\''
			});
			var recinto = require('helper').query2array(consultarModelo14_i);
			if (recinto && recinto.length) {
				/** 
				 * obtenemos valor seleccionado previamente y lo mostramos en selector. 
				 */
				if ((_.isObject(recinto[0].ids_muros) || (_.isString(recinto[0].ids_muros)) && !_.isEmpty(recinto[0].ids_muros)) || _.isNumber(recinto[0].ids_muros) || _.isBoolean(recinto[0].ids_muros)) {
					var muros = recinto[0].ids_muros;
					var murosSeparados = muros.split(",");
					var muroElegidos = '';
					var b_index = 0;
					_.each(murosSeparados, function(b, b_pos, b_list) {
						b_index += 1;
						var consultarModelo15_i = Alloy.createCollection('muros_tabiques');
						var consultarModelo15_i_where = 'id_segured=\'' + b + '\'';
						consultarModelo15_i.fetch({
							query: 'SELECT * FROM muros_tabiques WHERE id_segured=\'' + b + '\''
						});
						var muro = require('helper').query2array(consultarModelo15_i);
						if ((_.isObject(muroElegidos) || _.isString(muroElegidos)) && _.isEmpty(muroElegidos)) {
							muroElegidos += muro[0].nombre;
						} else {
							muroElegidos += ' + ' + muro[0].nombre;
						}
						if (muro && muro.length) {
							var ID_1135464224_func = function() {
								$.widgetModalmultiple2.update({
									valor: muroElegidos
								});
							};
							var ID_1135464224 = setTimeout(ID_1135464224_func, 1000 * 0.21);
						}
					});
				} else {
					if (Ti.App.deployType != 'production') console.log('no hay muros', {});
				}
			}
		}
	};
	var ID_93083937 = setTimeout(ID_93083937_func, 1000 * 0.2);

}

$.widgetModalmultiple3.init({
	titulo: L('x3327059844_traducir', 'Entrepisos'),
	cargando: L('x1740321226_traducir', 'cargando ..'),
	__id: 'ALL399652068',
	oncerrar: Cerrar_widgetModalmultiple3,
	left: 0,
	hint: L('x2146928948_traducir', 'Seleccione entrepisos'),
	color: 'morado',
	subtitulo: L('x4011106049_traducir', 'Indique los tipos'),
	right: 0,
	top: 5,
	onclick: Click_widgetModalmultiple3,
	onafterinit: Afterinit_widgetModalmultiple3
});

function Click_widgetModalmultiple3(e) {

	var evento = e;

}

function Cerrar_widgetModalmultiple3(e) {

	var evento = e;
	$.widgetModalmultiple3.update({});
	$.recinto.set({
		ids_entrepisos: evento.valores
	});
	if ('recinto' in $) $recinto = $.recinto.toJSON();

}

function Afterinit_widgetModalmultiple3(e) {

	var evento = e;
	var ID_1667308270_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			var consultarModelo16_i = Alloy.createCollection('entrepisos');
			var consultarModelo16_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
			consultarModelo16_i.fetch({
				query: 'SELECT * FROM entrepisos WHERE pais_texto=\'' + pais[0].nombre + '\''
			});
			var entrepisos = require('helper').query2array(consultarModelo16_i);
			var datos = [];
			_.each(entrepisos, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'label';
					if (llave == 'id_segured') newkey = 'valor';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				new_row['estado'] = 0;
				datos.push(new_row);
			});
			$.widgetModalmultiple3.update({
				data: datos
			});
		} else {
			var eliminarModelo6_i = Alloy.Collections.entrepisos;
			var sql = "DELETE FROM " + eliminarModelo6_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo6_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo6_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo8_m = Alloy.Collections.entrepisos;
				var insertarModelo8_fila = Alloy.createModel('entrepisos', {
					nombre: String.format(L('x2266735154_traducir', 'Entrepiso%1$s'), item.toString()),
					id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
					id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item.toString()),
					pais: 1
				});
				insertarModelo8_m.add(insertarModelo8_fila);
				insertarModelo8_fila.save();
				_.defer(function() {});
			});
			var transformarModelo8_i = Alloy.createCollection('entrepisos');
			transformarModelo8_i.fetch();
			var transformarModelo8_src = require('helper').query2array(transformarModelo8_i);
			var datos = [];
			_.each(transformarModelo8_src, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'label';
					if (llave == 'id_segured') newkey = 'valor';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				new_row['estado'] = 0;
				datos.push(new_row);
			});
			$.widgetModalmultiple3.update({
				data: datos
			});
		}
		if (!_.isUndefined(args._dato)) {
			var consultarModelo17_i = Alloy.createCollection('insp_recintos');
			var consultarModelo17_i_where = 'id=\'' + args._dato.id + '\'';
			consultarModelo17_i.fetch({
				query: 'SELECT * FROM insp_recintos WHERE id=\'' + args._dato.id + '\''
			});
			var recinto = require('helper').query2array(consultarModelo17_i);
			if (recinto && recinto.length) {
				/** 
				 * obtenemos valor seleccionado previamente y lo mostramos en selector. 
				 */
				if ((_.isObject(recinto[0].ids_entrepisos) || (_.isString(recinto[0].ids_entrepisos)) && !_.isEmpty(recinto[0].ids_entrepisos)) || _.isNumber(recinto[0].ids_entrepisos) || _.isBoolean(recinto[0].ids_entrepisos)) {
					var entrepisos = recinto[0].ids_entrepisos;
					var entrepisosSeparadas = entrepisos.split(",");
					var entrepisoElegidos = '';
					var c_index = 0;
					_.each(entrepisosSeparadas, function(c, c_pos, c_list) {
						c_index += 1;
						var consultarModelo18_i = Alloy.createCollection('entrepisos');
						var consultarModelo18_i_where = 'id_segured=\'' + c + '\'';
						consultarModelo18_i.fetch({
							query: 'SELECT * FROM entrepisos WHERE id_segured=\'' + c + '\''
						});
						var entrepiso = require('helper').query2array(consultarModelo18_i);
						if ((_.isObject(entrepisoElegidos) || _.isString(entrepisoElegidos)) && _.isEmpty(entrepisoElegidos)) {
							entrepisoElegidos += entrepiso[0].nombre;
						} else {
							entrepisoElegidos += ' + ' + entrepiso[0].nombre;
						}
						if (entrepiso && entrepiso.length) {
							var ID_1953689051_func = function() {
								$.widgetModalmultiple3.update({
									valor: entrepisoElegidos
								});
							};
							var ID_1953689051 = setTimeout(ID_1953689051_func, 1000 * 0.21);
						}
					});
				} else {
					if (Ti.App.deployType != 'production') console.log('no hay entrepisos', {});
				}
			}
		}
	};
	var ID_1667308270 = setTimeout(ID_1667308270_func, 1000 * 0.2);

}

$.widgetModalmultiple4.init({
	titulo: L('x591862035_traducir', 'Pavimentos'),
	cargando: L('x1740321226_traducir', 'cargando ..'),
	__id: 'ALL1990255213',
	oncerrar: Cerrar_widgetModalmultiple4,
	left: 0,
	hint: L('x2600368035_traducir', 'Seleccione pavimentos'),
	color: 'morado',
	subtitulo: L('x4011106049_traducir', 'Indique los tipos'),
	right: 0,
	top: 5,
	onclick: Click_widgetModalmultiple4,
	onafterinit: Afterinit_widgetModalmultiple4
});

function Click_widgetModalmultiple4(e) {

	var evento = e;

}

function Cerrar_widgetModalmultiple4(e) {

	var evento = e;
	$.widgetModalmultiple4.update({});
	$.recinto.set({
		ids_pavimentos: evento.valores
	});
	if ('recinto' in $) $recinto = $.recinto.toJSON();

}

function Afterinit_widgetModalmultiple4(e) {

	var evento = e;
	var ID_11528857_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			var consultarModelo19_i = Alloy.createCollection('pavimento');
			var consultarModelo19_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
			consultarModelo19_i.fetch({
				query: 'SELECT * FROM pavimento WHERE pais_texto=\'' + pais[0].nombre + '\''
			});
			var pavimentos = require('helper').query2array(consultarModelo19_i);
			var datos = [];
			_.each(pavimentos, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'label';
					if (llave == 'id_segured') newkey = 'valor';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				new_row['estado'] = 0;
				datos.push(new_row);
			});
			$.widgetModalmultiple4.update({
				data: datos
			});
		} else {
			var eliminarModelo7_i = Alloy.Collections.pavimento;
			var sql = "DELETE FROM " + eliminarModelo7_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo7_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo7_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo9_m = Alloy.Collections.pavimento;
				var insertarModelo9_fila = Alloy.createModel('pavimento', {
					nombre: String.format(L('x427067467_traducir', 'Pavimento%1$s'), item.toString()),
					id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
					id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item.toString()),
					pais: 1
				});
				insertarModelo9_m.add(insertarModelo9_fila);
				insertarModelo9_fila.save();
				_.defer(function() {});
			});
			var transformarModelo10_i = Alloy.createCollection('pavimento');
			transformarModelo10_i.fetch();
			var transformarModelo10_src = require('helper').query2array(transformarModelo10_i);
			var datos = [];
			_.each(transformarModelo10_src, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'label';
					if (llave == 'id_segured') newkey = 'valor';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				new_row['estado'] = 0;
				datos.push(new_row);
			});
			$.widgetModalmultiple4.update({
				data: datos
			});
		}
		if (!_.isUndefined(args._dato)) {
			var consultarModelo20_i = Alloy.createCollection('insp_recintos');
			var consultarModelo20_i_where = 'id=\'' + args._dato.id + '\'';
			consultarModelo20_i.fetch({
				query: 'SELECT * FROM insp_recintos WHERE id=\'' + args._dato.id + '\''
			});
			var recinto = require('helper').query2array(consultarModelo20_i);
			if (recinto && recinto.length) {
				/** 
				 * obtenemos valor seleccionado previamente y lo mostramos en selector. 
				 */
				if ((_.isObject(recinto[0].ids_pavimentos) || (_.isString(recinto[0].ids_pavimentos)) && !_.isEmpty(recinto[0].ids_pavimentos)) || _.isNumber(recinto[0].ids_pavimentos) || _.isBoolean(recinto[0].ids_pavimentos)) {
					var pavimentos = recinto[0].ids_pavimentos;
					var pavimentosSeparadas = pavimentos.split(",");
					var pavimentoElegidos = '';
					var d_index = 0;
					_.each(pavimentosSeparadas, function(d, d_pos, d_list) {
						d_index += 1;
						var consultarModelo21_i = Alloy.createCollection('pavimento');
						var consultarModelo21_i_where = 'id_segured=\'' + d + '\'';
						consultarModelo21_i.fetch({
							query: 'SELECT * FROM pavimento WHERE id_segured=\'' + d + '\''
						});
						var pavimento = require('helper').query2array(consultarModelo21_i);
						if ((_.isObject(pavimentoElegidos) || _.isString(pavimentoElegidos)) && _.isEmpty(pavimentoElegidos)) {
							pavimentoElegidos += pavimento[0].nombre;
						} else {
							pavimentoElegidos += ' + ' + pavimento[0].nombre;
						}
						if (pavimento && pavimento.length) {
							var ID_961862621_func = function() {
								$.widgetModalmultiple4.update({
									valor: pavimentoElegidos
								});
							};
							var ID_961862621 = setTimeout(ID_961862621_func, 1000 * 0.21);
						}
					});
				} else {
					if (Ti.App.deployType != 'production') console.log('no hay pavimentos', {});
				}
			}
		}
	};
	var ID_11528857 = setTimeout(ID_11528857_func, 1000 * 0.2);

}

$.widgetModalmultiple5.init({
	titulo: L('x1866523485_traducir', 'Estruct. cubierta'),
	cargando: L('x1740321226_traducir', 'cargando ..'),
	__id: 'ALL1536081210',
	oncerrar: Cerrar_widgetModalmultiple5,
	left: 0,
	hint: L('x2460890829_traducir', 'Seleccione e.cubiertas'),
	color: 'morado',
	subtitulo: L('x4011106049_traducir', 'Indique los tipos'),
	right: 0,
	top: 5,
	onclick: Click_widgetModalmultiple5,
	onafterinit: Afterinit_widgetModalmultiple5
});

function Click_widgetModalmultiple5(e) {

	var evento = e;

}

function Cerrar_widgetModalmultiple5(e) {

	var evento = e;
	$.widgetModalmultiple5.update({});
	$.recinto.set({
		ids_estructura_cubiera: evento.valores
	});
	if ('recinto' in $) $recinto = $.recinto.toJSON();

}

function Afterinit_widgetModalmultiple5(e) {

	var evento = e;
	var ID_93562171_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			var consultarModelo22_i = Alloy.createCollection('estructura_cubierta');
			var consultarModelo22_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
			consultarModelo22_i.fetch({
				query: 'SELECT * FROM estructura_cubierta WHERE pais_texto=\'' + pais[0].nombre + '\''
			});
			var ecubiertas = require('helper').query2array(consultarModelo22_i);
			var datos = [];
			_.each(ecubiertas, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'label';
					if (llave == 'id_segured') newkey = 'valor';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				new_row['estado'] = 0;
				datos.push(new_row);
			});
			$.widgetModalmultiple5.update({
				data: datos
			});
		} else {
			var eliminarModelo8_i = Alloy.Collections.estructura_cubierta;
			var sql = "DELETE FROM " + eliminarModelo8_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo8_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo8_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo10_m = Alloy.Collections.estructura_cubierta;
				var insertarModelo10_fila = Alloy.createModel('estructura_cubierta', {
					nombre: String.format(L('x1686539481_traducir', 'Estru Cubierta %1$s'), item.toString()),
					id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
					id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item.toString()),
					pais: 1
				});
				insertarModelo10_m.add(insertarModelo10_fila);
				insertarModelo10_fila.save();
				_.defer(function() {});
			});
			var transformarModelo12_i = Alloy.createCollection('estructura_cubierta');
			transformarModelo12_i.fetch();
			var transformarModelo12_src = require('helper').query2array(transformarModelo12_i);
			var datos = [];
			_.each(transformarModelo12_src, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'label';
					if (llave == 'id_segured') newkey = 'valor';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				new_row['estado'] = 0;
				datos.push(new_row);
			});
			$.widgetModalmultiple5.update({
				data: datos
			});
		}
		if (!_.isUndefined(args._dato)) {
			var consultarModelo23_i = Alloy.createCollection('insp_recintos');
			var consultarModelo23_i_where = 'id=\'' + args._dato.id + '\'';
			consultarModelo23_i.fetch({
				query: 'SELECT * FROM insp_recintos WHERE id=\'' + args._dato.id + '\''
			});
			var recinto = require('helper').query2array(consultarModelo23_i);
			if (recinto && recinto.length) {
				/** 
				 * obtenemos valor seleccionado previamente y lo mostramos en selector. 
				 */
				if ((_.isObject(recinto[0].ids_estructura_cubiera) || (_.isString(recinto[0].ids_estructura_cubiera)) && !_.isEmpty(recinto[0].ids_estructura_cubiera)) || _.isNumber(recinto[0].ids_estructura_cubiera) || _.isBoolean(recinto[0].ids_estructura_cubiera)) {
					var esCubierta = recinto[0].ids_estructura_cubiera;
					var esCubiertasSeparadas = esCubierta.split(",");
					var esCubiertaElegidos = '';
					var e_index = 0;
					_.each(esCubiertasSeparadas, function(e, e_pos, e_list) {
						e_index += 1;
						var consultarModelo24_i = Alloy.createCollection('estructura_cubierta');
						var consultarModelo24_i_where = 'id_segured=\'' + e + '\'';
						consultarModelo24_i.fetch({
							query: 'SELECT * FROM estructura_cubierta WHERE id_segured=\'' + e + '\''
						});
						var es_cubierta = require('helper').query2array(consultarModelo24_i);
						if ((_.isObject(esCubiertaElegidos) || _.isString(esCubiertaElegidos)) && _.isEmpty(esCubiertaElegidos)) {
							esCubiertaElegidos += es_cubierta[0].nombre;
						} else {
							esCubiertaElegidos += ' + ' + es_cubierta[0].nombre;
						}
						if (es_cubierta && es_cubierta.length) {
							var ID_127517620_func = function() {
								$.widgetModalmultiple5.update({
									valor: esCubiertaElegidos
								});
							};
							var ID_127517620 = setTimeout(ID_127517620_func, 1000 * 0.21);
						}
					});
				} else {
					if (Ti.App.deployType != 'production') console.log('no hay estructura cubierta', {});
				}
			}
		}
	};
	var ID_93562171 = setTimeout(ID_93562171_func, 1000 * 0.2);

}

$.widgetModalmultiple6.init({
	titulo: L('x2266302645_traducir', 'Cubierta'),
	cargando: L('x1740321226_traducir', 'cargando ..'),
	__id: 'ALL363463535',
	oncerrar: Cerrar_widgetModalmultiple6,
	left: 0,
	hint: L('x2134385782_traducir', 'Seleccione cubiertas'),
	color: 'morado',
	subtitulo: L('x4011106049_traducir', 'Indique los tipos'),
	right: 0,
	top: 5,
	onclick: Click_widgetModalmultiple6,
	onafterinit: Afterinit_widgetModalmultiple6
});

function Click_widgetModalmultiple6(e) {

	var evento = e;

}

function Cerrar_widgetModalmultiple6(e) {

	var evento = e;
	$.widgetModalmultiple6.update({});
	$.recinto.set({
		ids_cubierta: evento.valores
	});
	if ('recinto' in $) $recinto = $.recinto.toJSON();

}

function Afterinit_widgetModalmultiple6(e) {

	var evento = e;
	var ID_566132131_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			var consultarModelo25_i = Alloy.createCollection('cubierta');
			var consultarModelo25_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
			consultarModelo25_i.fetch({
				query: 'SELECT * FROM cubierta WHERE pais_texto=\'' + pais[0].nombre + '\''
			});
			var cubiertas = require('helper').query2array(consultarModelo25_i);
			var datos = [];
			_.each(cubiertas, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'label';
					if (llave == 'id_segured') newkey = 'valor';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				new_row['estado'] = 0;
				datos.push(new_row);
			});
			$.widgetModalmultiple6.update({
				data: datos
			});
		} else {
			var eliminarModelo9_i = Alloy.Collections.cubierta;
			var sql = "DELETE FROM " + eliminarModelo9_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo9_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo9_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo11_m = Alloy.Collections.cubierta;
				var insertarModelo11_fila = Alloy.createModel('cubierta', {
					nombre: String.format(L('x2246230604_traducir', 'Cubierta %1$s'), item.toString()),
					id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
					id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item.toString()),
					pais: 1
				});
				insertarModelo11_m.add(insertarModelo11_fila);
				insertarModelo11_fila.save();
				_.defer(function() {});
			});
			var transformarModelo14_i = Alloy.createCollection('cubierta');
			transformarModelo14_i.fetch();
			var transformarModelo14_src = require('helper').query2array(transformarModelo14_i);
			var datos = [];
			_.each(transformarModelo14_src, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'label';
					if (llave == 'id_segured') newkey = 'valor';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				new_row['estado'] = 0;
				datos.push(new_row);
			});
			$.widgetModalmultiple6.update({
				data: datos
			});
		}
		if (!_.isUndefined(args._dato)) {
			var consultarModelo26_i = Alloy.createCollection('insp_recintos');
			var consultarModelo26_i_where = 'id=\'' + args._dato.id + '\'';
			consultarModelo26_i.fetch({
				query: 'SELECT * FROM insp_recintos WHERE id=\'' + args._dato.id + '\''
			});
			var recinto = require('helper').query2array(consultarModelo26_i);
			if (recinto && recinto.length) {
				/** 
				 * obtenemos valor seleccionado previamente y lo mostramos en selector. 
				 */
				if ((_.isObject(recinto[0].ids_cubierta) || (_.isString(recinto[0].ids_cubierta)) && !_.isEmpty(recinto[0].ids_cubierta)) || _.isNumber(recinto[0].ids_cubierta) || _.isBoolean(recinto[0].ids_cubierta)) {
					var cubierta = recinto[0].ids_cubierta;
					var cubiertasSeparadas = cubierta.split(",");
					var cubiertaElegidos = '';
					var f_index = 0;
					_.each(cubiertasSeparadas, function(f, f_pos, f_list) {
						f_index += 1;
						var consultarModelo27_i = Alloy.createCollection('cubierta');
						var consultarModelo27_i_where = 'id_segured=\'' + f + '\'';
						consultarModelo27_i.fetch({
							query: 'SELECT * FROM cubierta WHERE id_segured=\'' + f + '\''
						});
						var cubierta = require('helper').query2array(consultarModelo27_i);
						if ((_.isObject(cubiertaElegidos) || _.isString(cubiertaElegidos)) && _.isEmpty(cubiertaElegidos)) {
							cubiertaElegidos += cubierta[0].nombre;
						} else {
							cubiertaElegidos += ' + ' + cubierta[0].nombre;
						}
						if (cubierta && cubierta.length) {
							var ID_1131253245_func = function() {
								$.widgetModalmultiple6.update({
									valor: cubiertaElegidos
								});
							};
							var ID_1131253245 = setTimeout(ID_1131253245_func, 1000 * 0.21);
						}
					});
				} else {
					if (Ti.App.deployType != 'production') console.log('no hay cubiertas', {});
				}
			}
		}
	};
	var ID_566132131 = setTimeout(ID_566132131_func, 1000 * 0.2);

}

(function() {
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
		/** 
		 * Modificamos la idinspeccion con el idserver de la tarea 
		 */
		$.recinto.set({
			id_inspeccion: seltarea.id_server
		});
		if ('recinto' in $) $recinto = $.recinto.toJSON();
	}
	if (!_.isUndefined(args._dato)) {
		/** 
		 * Si existe args._dato estamos editando recinto 
		 */
		/** 
		 * Modificamos titulo de header en caso de estar editando recinto 
		 */
		var NUEVO_RECINTO_titulo = 'EDITAR RECINTO';

		var setTitle2 = function(valor) {
			if (OS_ANDROID) {
				abx.title = valor;
			} else {
				$.NUEVO_RECINTO_window.setTitle(valor);
			}
		};
		var getTitle2 = function() {
			if (OS_ANDROID) {
				return abx.title;
			} else {
				return $.NUEVO_RECINTO_window.getTitle();
			}
		};
		setTitle2(NUEVO_RECINTO_titulo);

		if (Ti.App.deployType != 'production') console.log('HELLO1', {
			"valor": args._dato
		});
		require('vars')[_var_scopekey]['idrecintoactual'] = args._dato.id;
		/** 
		 * heredamos datos previos de nivel en modelo nuevo con binding de esta pantalla 
		 */
		var consultarModelo28_i = Alloy.createCollection('insp_recintos');
		var consultarModelo28_i_where = 'id=\'' + args._dato.id + '\'';
		consultarModelo28_i.fetch({
			query: 'SELECT * FROM insp_recintos WHERE id=\'' + args._dato.id + '\''
		});
		var insp_r = require('helper').query2array(consultarModelo28_i);
		if (Ti.App.deployType != 'production') console.log('ESTE ES EL RECINTO1', {
			"valor": insp_r[0]
		});
		/** 
		 * filtramos items de dano, para el id_recinto activo. 
		 */
		consultarModelo5_filter = function(coll) {
			var filtered = coll.filter(function(m) {
				var _tests = [],
					_all_true = false,
					model = m.toJSON();
				_tests.push((model.id_recinto == insp_r[0].id_recinto));
				var _all_true_s = _.uniq(_tests);
				_all_true = (_all_true_s.length == 1 && _all_true_s[0] == true) ? true : false;
				return _all_true;
			});
			filtered = _.toArray(filtered);
			return filtered;
		};
		_.defer(function() {
			Alloy.Collections.insp_itemdanos.fetch();
		});
		/** 
		 * Cargamos el modelo con los datos obtenidos desde la consulta 
		 */
		$.recinto.set({
			ids_entrepisos: insp_r[0].ids_entrepisos,
			id_inspeccion: insp_r[0].id_inspeccion,
			nombre: insp_r[0].nombre,
			superficie: insp_r[0].superficie,
			largo: insp_r[0].largo,
			foto1: insp_r[0].foto1,
			foto2: insp_r[0].foto2,
			id_recinto: insp_r[0].id_recinto,
			foto3: insp_r[0].foto3,
			ids_pavimentos: insp_r[0].ids_pavimentos,
			alto: insp_r[0].alto,
			ids_muros: insp_r[0].ids_muros,
			ancho: insp_r[0].ancho,
			ids_estructura_cubiera: insp_r[0].ids_estructura_cubiera,
			ids_cubierta: insp_r[0].ids_cubierta,
			id_nivel: insp_r[0].id_nivel,
			ids_estructuras_soportantes: insp_r[0].ids_estructuras_soportantes
		});
		if ('recinto' in $) $recinto = $.recinto.toJSON();
		/** 
		 * guardamos variable pq aun no existe este recinto al agregar da&#241;os 
		 */
		require('vars')['temp_idrecinto'] = insp_r[0].id_recinto;
		/** 
		 * Cargamos los textos en los campos de texto 
		 */
		$.EscribaNombre.setValue(insp_r[0].nombre);

		$.campo.setValue(insp_r[0].largo);

		$.campo2.setValue(insp_r[0].ancho);

		$.campo3.setValue(insp_r[0].alto);

		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			/** 
			 * Revisamos si existe variable seltarea para saber si las fotos son desde la inspeccion o son datos dummy 
			 */
			if (Ti.App.deployType != 'production') console.log('es parte de la compilacion inspeccion completa', {});
			var ID_1218924586_trycatch = {
				error: function(e) {
					if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
				}
			};
			try {
				ID_1218924586_trycatch.error = function(evento) {};
				var foto1 = '';
				var ID_1743680972_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'mini' + insp_r[0].id_inspeccion);
				if (ID_1743680972_d.exists() == true) {
					var ID_1743680972_f = Ti.Filesystem.getFile(ID_1743680972_d.resolve(), insp_r[0].foto1);
					if (ID_1743680972_f.exists() == true) {
						foto1 = ID_1743680972_f.read();
					}
					ID_1743680972_f = null;
				}
				ID_1743680972_d = null;
			} catch (e) {
				ID_1218924586_trycatch.error(e);
			}
			var ID_1324389329_trycatch = {
				error: function(e) {
					if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
				}
			};
			try {
				ID_1324389329_trycatch.error = function(evento) {};
				var foto2 = '';
				var ID_1116016580_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'mini' + insp_r[0].id_inspeccion);
				if (ID_1116016580_d.exists() == true) {
					var ID_1116016580_f = Ti.Filesystem.getFile(ID_1116016580_d.resolve(), insp_r[0].foto2);
					if (ID_1116016580_f.exists() == true) {
						foto2 = ID_1116016580_f.read();
					}
					ID_1116016580_f = null;
				}
				ID_1116016580_d = null;
			} catch (e) {
				ID_1324389329_trycatch.error(e);
			}
			var ID_55032749_trycatch = {
				error: function(e) {
					if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
				}
			};
			try {
				ID_55032749_trycatch.error = function(evento) {};
				var foto3 = '';
				var ID_1411782814_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'mini' + insp_r[0].id_inspeccion);
				if (ID_1411782814_d.exists() == true) {
					var ID_1411782814_f = Ti.Filesystem.getFile(ID_1411782814_d.resolve(), insp_r[0].foto3);
					if (ID_1411782814_f.exists() == true) {
						foto3 = ID_1411782814_f.read();
					}
					ID_1411782814_f = null;
				}
				ID_1411782814_d = null;
			} catch (e) {
				ID_55032749_trycatch.error(e);
			}
		} else {
			if (Ti.App.deployType != 'production') console.log('esta compilacion esta sola, simulamos carpetas', {});
			var ID_547629173_trycatch = {
				error: function(e) {
					if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
				}
			};
			try {
				ID_547629173_trycatch.error = function(evento) {};
				var foto1 = '';
				var ID_315896947_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'mini' + 0);
				if (ID_315896947_d.exists() == true) {
					var ID_315896947_f = Ti.Filesystem.getFile(ID_315896947_d.resolve(), insp_r[0].foto1);
					if (ID_315896947_f.exists() == true) {
						foto1 = ID_315896947_f.read();
					}
					ID_315896947_f = null;
				}
				ID_315896947_d = null;
			} catch (e) {
				ID_547629173_trycatch.error(e);
			}
			var ID_1616909749_trycatch = {
				error: function(e) {
					if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
				}
			};
			try {
				ID_1616909749_trycatch.error = function(evento) {};
				var foto2 = '';
				var ID_33999265_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'mini' + 0);
				if (ID_33999265_d.exists() == true) {
					var ID_33999265_f = Ti.Filesystem.getFile(ID_33999265_d.resolve(), insp_r[0].foto2);
					if (ID_33999265_f.exists() == true) {
						foto2 = ID_33999265_f.read();
					}
					ID_33999265_f = null;
				}
				ID_33999265_d = null;
			} catch (e) {
				ID_1616909749_trycatch.error(e);
			}
			var ID_1025504743_trycatch = {
				error: function(e) {
					if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
				}
			};
			try {
				ID_1025504743_trycatch.error = function(evento) {};
				var foto3 = '';
				var ID_4931431_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'mini' + 0);
				if (ID_4931431_d.exists() == true) {
					var ID_4931431_f = Ti.Filesystem.getFile(ID_4931431_d.resolve(), insp_r[0].foto3);
					if (ID_4931431_f.exists() == true) {
						foto3 = ID_4931431_f.read();
					}
					ID_4931431_f = null;
				}
				ID_4931431_d = null;
			} catch (e) {
				ID_1025504743_trycatch.error(e);
			}
		}
		if ((_.isObject(foto1) || (_.isString(foto1)) && !_.isEmpty(foto1)) || _.isNumber(foto1) || _.isBoolean(foto1)) {
			/** 
			 * Revisamos que foto1, foto2 y foto3 contengan texto, por lo que si es asi, modificamos la imagen previa y ponemos la foto miniatura obtenida desde la memoria 
			 */
			if (Ti.App.deployType != 'production') console.log('llamamos evento mini', {});
			$.widgetFotochica.mini({
				mini: foto1
			});
		}
		if ((_.isObject(foto2) || (_.isString(foto2)) && !_.isEmpty(foto2)) || _.isNumber(foto2) || _.isBoolean(foto2)) {
			if (Ti.App.deployType != 'production') console.log('llamamos evento mini', {});
			$.widgetFotochica2.mini({
				mini: foto2
			});
		}
		if ((_.isObject(foto3) || (_.isString(foto3)) && !_.isEmpty(foto3)) || _.isNumber(foto3) || _.isBoolean(foto3)) {
			if (Ti.App.deployType != 'production') console.log('llamamos evento mini', {});
			$.widgetFotochica3.mini({
				mini: foto3
			});
		}
	} else if (_.isUndefined(args._dato)) {
		var insertarModelo12_m = Alloy.Collections.numero_unico;
		var insertarModelo12_fila = Alloy.createModel('numero_unico', {
			comentario: 'nuevo recinto id'
		});
		insertarModelo12_m.add(insertarModelo12_fila);
		insertarModelo12_fila.save();
		var nuevoid = require('helper').model2object(insertarModelo12_m.last());
		_.defer(function() {});
		/** 
		 * guardamos variable pq aun no existe este recinto al agregar da&#241;os 
		 */
		require('vars')['temp_idrecinto'] = nuevoid.id;
		$.recinto.set({
			id_recinto: nuevoid.id
		});
		if ('recinto' in $) $recinto = $.recinto.toJSON();
		/** 
		 * filtramos items de dano, para el id_recinto recien creado. 
		 */
		consultarModelo5_filter = function(coll) {
			var filtered = coll.filter(function(m) {
				var _tests = [],
					_all_true = false,
					model = m.toJSON();
				_tests.push((model.id_recinto == nuevoid.id));
				var _all_true_s = _.uniq(_tests);
				_all_true = (_all_true_s.length == 1 && _all_true_s[0] == true) ? true : false;
				return _all_true;
			});
			filtered = _.toArray(filtered);
			return filtered;
		};
		_.defer(function() {
			Alloy.Collections.insp_itemdanos.fetch();
		});
	}
})();

if (OS_IOS || OS_ANDROID) {
	$.NUEVO_RECINTO.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}