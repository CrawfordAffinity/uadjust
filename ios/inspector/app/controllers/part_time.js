var _bind4section = {};
var _list_templates = {};

var _activity;
if (OS_ANDROID) {
	_activity = $.ENROLAMIENTO5.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'ENROLAMIENTO5';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.ENROLAMIENTO5.addEventListener('open', function(e) {
		abx.setStatusbarColor("#000000");
		abx.setBackgroundColor("white");
	});
}
$.ENROLAMIENTO5.orientationModes = [Titanium.UI.PORTRAIT];

function Click_vista38(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.ENROLAMIENTO5.close();

}

$.widgetHeader5.init({
	titulo: L('x3752633607_traducir', 'PARTE 4: Disponibilidad de trabajo'),
	__id: 'ALL1614118096',
	avance: L('x25508266_traducir', '4/6')
});


$.widgetPelota.init({
	__id: 'ALL1132173110',
	letra: L('x2909332022', 'L'),
	onon: on_widgetPelota,
	onoff: Off_widgetPelota
});

function on_widgetPelota(e) {
	/** 
	 * Dependiendo de la seleccion (encendido o apagado) se actualiza el registro para definir los dias disponibles para trabajar 
	 */

	var evento = e;
	var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
	var registro = _.extend(registro, {
		d1: 1
	});

}

function Off_widgetPelota(e) {

	var evento = e;
	var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
	var registro = _.extend(registro, {
		d1: 0
	});

}

$.widgetPelota2.init({
	__id: 'ALL1396533677',
	letra: L('x3664761504', 'M'),
	onon: on_widgetPelota2,
	onoff: Off_widgetPelota2
});

function on_widgetPelota2(e) {
	/** 
	 * Dependiendo de la seleccion (encendido o apagado) se actualiza el registro para definir los dias disponibles para trabajar 
	 */

	var evento = e;
	var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
	var registro = _.extend(registro, {
		d2: 1
	});

}

function Off_widgetPelota2(e) {

	var evento = e;
	var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
	var registro = _.extend(registro, {
		d2: 0
	});

}

$.widgetPelota3.init({
	__id: 'ALL364580565',
	letra: L('x185522819_traducir', 'MI'),
	onon: on_widgetPelota3,
	onoff: Off_widgetPelota3
});

function on_widgetPelota3(e) {
	/** 
	 * Dependiendo de la seleccion (encendido o apagado) se actualiza el registro para definir los dias disponibles para trabajar 
	 */

	var evento = e;
	var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
	var registro = _.extend(registro, {
		d3: 1
	});

}

function Off_widgetPelota3(e) {

	var evento = e;
	var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
	var registro = _.extend(registro, {
		d3: 0
	});

}

$.widgetPelota4.init({
	__id: 'ALL180804538',
	letra: L('x1141589763', 'J'),
	onon: on_widgetPelota4,
	onoff: Off_widgetPelota4
});

function on_widgetPelota4(e) {
	/** 
	 * Dependiendo de la seleccion (encendido o apagado) se actualiza el registro para definir los dias disponibles para trabajar 
	 */

	var evento = e;
	var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
	var registro = _.extend(registro, {
		d4: 1
	});

}

function Off_widgetPelota4(e) {

	var evento = e;
	var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
	var registro = _.extend(registro, {
		d4: 0
	});

}

$.widgetPelota5.init({
	__id: 'ALL457319757',
	letra: L('x1342839628', 'V'),
	onon: on_widgetPelota5,
	onoff: Off_widgetPelota5
});

function on_widgetPelota5(e) {
	/** 
	 * Dependiendo de la seleccion (encendido o apagado) se actualiza el registro para definir los dias disponibles para trabajar 
	 */

	var evento = e;
	var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
	var registro = _.extend(registro, {
		d5: 1
	});

}

function Off_widgetPelota5(e) {

	var evento = e;
	var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
	var registro = _.extend(registro, {
		d5: 0
	});

}

$.widgetPelota6.init({
	__id: 'ALL993907842',
	letra: L('x543223747', 'S'),
	onon: on_widgetPelota6,
	onoff: Off_widgetPelota6
});

function on_widgetPelota6(e) {
	/** 
	 * Dependiendo de la seleccion (encendido o apagado) se actualiza el registro para definir los dias disponibles para trabajar 
	 */

	var evento = e;
	var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
	var registro = _.extend(registro, {
		d6: 1
	});

}

function Off_widgetPelota6(e) {

	var evento = e;
	var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
	var registro = _.extend(registro, {
		d6: 0
	});

}

$.widgetPelota7.init({
	__id: 'ALL947148270',
	letra: L('x2746444292', 'D'),
	onon: on_widgetPelota7,
	onoff: Off_widgetPelota7
});

function on_widgetPelota7(e) {
	/** 
	 * Dependiendo de la seleccion (encendido o apagado) se actualiza el registro para definir los dias disponibles para trabajar 
	 */

	var evento = e;
	var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
	var registro = _.extend(registro, {
		d7: 1
	});

}

function Off_widgetPelota7(e) {

	var evento = e;
	var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
	var registro = _.extend(registro, {
		d7: 0
	});

}

$.widgetPickerhoras.init({
	__id: 'ALL1517641115',
	onchange: Change_widgetPickerhoras,
	mins: L('x4261170317', 'true'),
	a: L('x3904355907', 'a')
});

function Change_widgetPickerhoras(e) {

	var evento = e;
	require('vars')['desde'] = evento.desde;
	require('vars')['hasta'] = evento.hasta;

}

$.widgetBotonlargo5.init({
	titulo: L('x1524107289_traducir', 'CONTINUAR'),
	__id: 'ALL1929710970',
	onclick: Click_widgetBotonlargo5
});

function Click_widgetBotonlargo5(e) {

	var evento = e;
	/** 
	 * Recuperamos variables, creamos una variable para identificar que alguna opcion este siendo elegida, y que la hora de fin sea mayor a la de inicio 
	 */
	var desde = ('desde' in require('vars')) ? require('vars')['desde'] : '';
	var hasta = ('hasta' in require('vars')) ? require('vars')['hasta'] : '';
	var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';

	//validaciones
	var al_menos_uno = registro.d1 || registro.d2 || registro.d3 || registro.d4 || registro.d5 || registro.d6 || registro.d7;
	desde = parseInt(desde);
	hasta = parseInt(hasta)
	if (al_menos_uno == false || al_menos_uno == 'false') {
		var preguntarAlerta13_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta13 = Ti.UI.createAlertDialog({
			title: L('x2185084353_traducir', 'Atención'),
			message: L('x3562924320_traducir', 'Seleccione a lo menos un día'),
			buttonNames: preguntarAlerta13_opts
		});
		preguntarAlerta13.addEventListener('click', function(e) {
			var suu = preguntarAlerta13_opts[e.index];
			suu = null;

			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta13.show();
	} else if (_.isNumber(desde) && _.isNumber(hasta) && desde > hasta) {
		var preguntarAlerta14_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta14 = Ti.UI.createAlertDialog({
			title: L('x2185084353_traducir', 'Atención'),
			message: L('x458766715_traducir', 'Ingrese su horario de trabajo'),
			buttonNames: preguntarAlerta14_opts
		});
		preguntarAlerta14.addEventListener('click', function(e) {
			var suu = preguntarAlerta14_opts[e.index];
			suu = null;

			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta14.show();
	} else if (desde == hasta) {
		var preguntarAlerta15_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta15 = Ti.UI.createAlertDialog({
			title: L('x2185084353_traducir', 'Atención'),
			message: L('x178692076_traducir', 'El rango debe ser superior a una hora'),
			buttonNames: preguntarAlerta15_opts
		});
		preguntarAlerta15.addEventListener('click', function(e) {
			var suu = preguntarAlerta15_opts[e.index];
			suu = null;

			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta15.show();
	} else {
		/** 
		 * Actualizamos la variable del registro 
		 */
		var registro = _.extend(registro, {
			disp_horas: String.format(L('x1576037006_traducir', '%1$s:00 %2$s:00'), (desde) ? desde.toString() : '', (hasta) ? hasta.toString() : '')
		});
		require('vars')['registro'] = registro;
		if ("ENROLAMIENTO" in Alloy.Globals) {
			Alloy.Globals["ENROLAMIENTO"].openWindow(Alloy.createController("contactos", {}).getView());
		} else {
			Alloy.Globals["ENROLAMIENTO"] = $.ENROLAMIENTO;
			Alloy.Globals["ENROLAMIENTO"].openWindow(Alloy.createController("contactos", {}).getView());
		}

	}
}

(function() {
	var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
	/** 
	 * Cuando se recupera la variable de registro, cargamos la disponibilidad en 0 para que el usuario deba escoger un dia al menos de disponibilidad 
	 */
	var registro = _.extend(registro, {
		d1: 0,
		d2: 0,
		d3: 0,
		d4: 0,
		d5: 0,
		d6: 0,
		d7: 0
	});
	require('vars')['registro'] = registro;
	/** 
	 * Guardamos las horas disponibles en 1 para que el usuario deba escoger un horario 
	 */
	require('vars')['desde'] = L('x2212294583', '1');
	require('vars')['hasta'] = L('x2212294583', '1');
	/** 
	 * Creamos evento que al cerrar desde la ultima pantalla, esta tambi&#233;n se cierre. Evitamos el uso excesivo de memoria ram 
	 */

	_my_events['_close_enrolamiento,ID_1368401318'] = function(evento) {
		if (Ti.App.deployType != 'production') console.log('escuchando cerrar enrolamiento parttime', {});
		$.ENROLAMIENTO5.close();
	};
	Alloy.Events.on('_close_enrolamiento', _my_events['_close_enrolamiento,ID_1368401318']);
})();

if (OS_IOS || OS_ANDROID) {
	$.ENROLAMIENTO5.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}