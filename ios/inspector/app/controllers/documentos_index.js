var _bind4section = {};
var _list_templates = {};
var $documentos = $.documentos.toJSON();

$.DOCUMENTOS_window.setTitleAttributes({
	color: 'WHITE'
});
var _activity;
if (OS_ANDROID) {
	_activity = $.DOCUMENTOS.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'DOCUMENTOS';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.DOCUMENTOS_window.addEventListener('open', function(e) {
		abx.setStatusbarColor("#FFFFFF");
		abx.setBackgroundColor("#ffacaa");
	});
}

function Click_vista(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	preguntarOpciones.show();

}

$.widgetPreguntadoc.init({
	titulo: L('x404129330_traducir', '¿EL ASEGURADO CONFIRMA ESTOS DATOS?'),
	__id: 'ALL516906299',
	si: L('x1723413441_traducir', 'SI, Están correctos'),
	texto: L('x2083765231_traducir', 'El asegurado debe confirmar que los datos de esta sección están correctos'),
	pantalla: L('x2851883104_traducir', '¿ESTA DE ACUERDO?'),
	onno: no_widgetPreguntadoc,
	onsi: si_widgetPreguntadoc,
	no: L('x55492959_traducir', 'NO, Hay que modificar algo'),
	header: 'rosado',
	onclick: Click_widgetPreguntadoc
});

function Click_widgetPreguntadoc(e) {

	var evento = e;
	if (Ti.App.deployType != 'production') console.log('click en guardar!!!!', {});
	var test = $.documentos.toJSON();
	if (Ti.App.deployType != 'production') console.log('test', {
		"valor": test
	});
	require('vars')[_var_scopekey]['alguno_on'] = L('x734881840_traducir', 'false');
	if (test.doc1 == 1 || test.doc1 == '1') {
		require('vars')[_var_scopekey]['alguno_on'] = L('x4261170317', 'true');
	} else if (test.doc2 == 1 || test.doc2 == '1') {
		require('vars')[_var_scopekey]['alguno_on'] = L('x4261170317', 'true');
	} else if (test.doc3 == 1 || test.doc3 == '1') {
		require('vars')[_var_scopekey]['alguno_on'] = L('x4261170317', 'true');
	} else if (test.doc4 == 1 || test.doc4 == '1') {
		require('vars')[_var_scopekey]['alguno_on'] = L('x4261170317', 'true');
	} else if (test.doc5 == 1 || test.doc5 == '1') {
		require('vars')[_var_scopekey]['alguno_on'] = L('x4261170317', 'true');
	} else if (test.doc6 == 1 || test.doc6 == '1') {
		require('vars')[_var_scopekey]['alguno_on'] = L('x4261170317', 'true');
	} else if (test.doc7 == 1 || test.doc7 == '1') {
		require('vars')[_var_scopekey]['alguno_on'] = L('x4261170317', 'true');
	} else if (test.doc8 == 1 || test.doc8 == '1') {
		require('vars')[_var_scopekey]['alguno_on'] = L('x4261170317', 'true');
	} else if (test.doc9 == 1 || test.doc9 == '1') {
		require('vars')[_var_scopekey]['alguno_on'] = L('x4261170317', 'true');
	} else if (test.doc10 == 1 || test.doc10 == '1') {
		require('vars')[_var_scopekey]['alguno_on'] = L('x4261170317', 'true');
	}
	var alguno_on = ('alguno_on' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['alguno_on'] : '';
	if (Ti.App.deployType != 'production') console.log('alguno_on', {
		"valor": alguno_on
	});
	if (alguno_on == true || alguno_on == 'true') {
		if (Ti.App.deployType != 'production') console.log('dias', {
			"valor": test.dias
		});
		if ((_.isObject(test.dias) || _.isString(test.dias)) && _.isEmpty(test.dias)) {
			var preguntarAlerta_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta = Ti.UI.createAlertDialog({
				title: L('x3237162386_traducir', 'Atencion'),
				message: L('x1870732722_traducir', 'Ingrese días de compromiso'),
				buttonNames: preguntarAlerta_opts
			});
			preguntarAlerta.addEventListener('click', function(e) {
				var nulo = preguntarAlerta_opts[e.index];
				nulo = null;
				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta.show();
		} else if (_.isUndefined(test.dias)) {
			var preguntarAlerta2_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta2 = Ti.UI.createAlertDialog({
				title: L('x3237162386_traducir', 'Atencion'),
				message: L('x1870732722_traducir', 'Ingrese días de compromiso'),
				buttonNames: preguntarAlerta2_opts
			});
			preguntarAlerta2.addEventListener('click', function(e) {
				var nulo = preguntarAlerta2_opts[e.index];
				nulo = null;
				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta2.show();
		} else if (test.dias == -1) {
			var preguntarAlerta3_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta3 = Ti.UI.createAlertDialog({
				title: L('x3237162386_traducir', 'Atencion'),
				message: L('x1870732722_traducir', 'Ingrese días de compromiso'),
				buttonNames: preguntarAlerta3_opts
			});
			preguntarAlerta3.addEventListener('click', function(e) {
				var nulo = preguntarAlerta3_opts[e.index];
				nulo = null;
				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta3.show();
		} else {
			if (Ti.App.deployType != 'production') console.log('Esta todo muy bien', {});
			test = null;
			$.widgetPreguntadoc.enviar({});
		}
	} else {
		test = null;
		$.widgetPreguntadoc.enviar({});
	}
}

function si_widgetPreguntadoc(e) {

	var evento = e;
	Alloy.Collections[$.documentos.config.adapter.collection_name].add($.documentos);
	$.documentos.save();
	Alloy.Collections[$.documentos.config.adapter.collection_name].fetch();
	Alloy.createController("firma_index", {}).getView().open();

}

function no_widgetPreguntadoc(e) {

	var evento = e;

}

function Change_ID_1174564286(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	if (elemento == true || elemento == 'true') {
		/** 
		 * Dependiendo del estado del switch, actualizamos el estado del documento 
		 */
		$.documentos.set({
			doc1: 1
		});
		if ('documentos' in $) $documentos = $.documentos.toJSON();
	} else {
		$.documentos.set({
			doc1: 0
		});
		if ('documentos' in $) $documentos = $.documentos.toJSON();
	}
	elemento = null;

}

function Change_ID_821496152(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	if (elemento == true || elemento == 'true') {
		$.documentos.set({
			doc2: 1
		});
		if ('documentos' in $) $documentos = $.documentos.toJSON();
	} else {
		$.documentos.set({
			doc2: 0
		});
		if ('documentos' in $) $documentos = $.documentos.toJSON();
	}
	elemento = null;

}

function Change_ID_1174173191(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	if (elemento == true || elemento == 'true') {
		$.documentos.set({
			doc3: 1
		});
		if ('documentos' in $) $documentos = $.documentos.toJSON();
	} else {
		$.documentos.set({
			doc3: 0
		});
		if ('documentos' in $) $documentos = $.documentos.toJSON();
	}
	elemento = null;

}

function Change_ID_1192853373(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	if (elemento == true || elemento == 'true') {
		$.documentos.set({
			doc4: 1
		});
		if ('documentos' in $) $documentos = $.documentos.toJSON();
	} else {
		$.documentos.set({
			doc4: 0
		});
		if ('documentos' in $) $documentos = $.documentos.toJSON();
	}
	elemento = null;

}

function Change_ID_1338746933(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	if (elemento == true || elemento == 'true') {
		$.documentos.set({
			doc5: 1
		});
		if ('documentos' in $) $documentos = $.documentos.toJSON();
	} else {
		$.documentos.set({
			doc5: 0
		});
		if ('documentos' in $) $documentos = $.documentos.toJSON();
	}
	elemento = null;

}

function Change_ID_592418340(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	if (elemento == true || elemento == 'true') {
		$.documentos.set({
			doc6: 1
		});
		if ('documentos' in $) $documentos = $.documentos.toJSON();
	} else {
		$.documentos.set({
			doc6: 0
		});
		if ('documentos' in $) $documentos = $.documentos.toJSON();
	}
	elemento = null;

}

function Change_ID_317508767(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	if (elemento == true || elemento == 'true') {
		$.documentos.set({
			doc7: 1
		});
		if ('documentos' in $) $documentos = $.documentos.toJSON();
	} else {
		$.documentos.set({
			doc7: 0
		});
		if ('documentos' in $) $documentos = $.documentos.toJSON();
	}
	elemento = null;

}

function Change_ID_1494372783(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	if (elemento == true || elemento == 'true') {
		$.documentos.set({
			doc8: 1
		});
		if ('documentos' in $) $documentos = $.documentos.toJSON();
	} else {
		$.documentos.set({
			doc8: 0
		});
		if ('documentos' in $) $documentos = $.documentos.toJSON();
	}
	elemento = null;

}

function Change_ID_353137507(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	if (elemento == true || elemento == 'true') {
		$.documentos.set({
			doc9: 1
		});
		if ('documentos' in $) $documentos = $.documentos.toJSON();
	} else {
		$.documentos.set({
			doc9: 0
		});
		if ('documentos' in $) $documentos = $.documentos.toJSON();
	}
	elemento = null;

}

function Change_ID_1889009100(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	if (elemento == true || elemento == 'true') {
		/** 
		 * Actualizamos doc10 
		 */
		$.documentos.set({
			doc10: 1
		});
		if ('documentos' in $) $documentos = $.documentos.toJSON();
		/** 
		 * Actualizamos edificio 
		 */
		$.documentos.set({
			edificio: 1
		});
		if ('documentos' in $) $documentos = $.documentos.toJSON();
		require('vars')[_var_scopekey]['s1'] = L('x2212294583', '1');
	} else {
		$.documentos.set({
			edificio: 0
		});
		if ('documentos' in $) $documentos = $.documentos.toJSON();
		require('vars')[_var_scopekey]['s1'] = L('x4108050209', '0');
		/** 
		 * Recuperamos variable s2 para saber si el doc10 debe estar en 0 o 1 
		 */
		var s2 = ('s2' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['s2'] : '';
		if (s2 == 0 || s2 == '0') {
			$.documentos.set({
				doc10: 0
			});
			if ('documentos' in $) $documentos = $.documentos.toJSON();
		}
	}
	elemento = null;

}

function Change_ID_1431536933(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	if (elemento == true || elemento == 'true') {
		/** 
		 * Actualizamos el doc10 
		 */
		$.documentos.set({
			doc10: 1
		});
		if ('documentos' in $) $documentos = $.documentos.toJSON();
		/** 
		 * Actualizamos los contenidos 
		 */
		$.documentos.set({
			contenidos: 1
		});
		if ('documentos' in $) $documentos = $.documentos.toJSON();
		require('vars')[_var_scopekey]['s2'] = L('x2212294583', '1');
	} else {
		$.documentos.set({
			contenidos: 0
		});
		if ('documentos' in $) $documentos = $.documentos.toJSON();
		require('vars')[_var_scopekey]['s2'] = L('x4108050209', '0');
		/** 
		 * Recuperamos variable s1 para saber si el doc10 debe estar en 0 o 1 
		 */
		var s1 = ('s1' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['s1'] : '';
		if (s1 == 0 || s1 == '0') {
			$.documentos.set({
				doc10: 0
			});
			if ('documentos' in $) $documentos = $.documentos.toJSON();
		}
	}
	elemento = null;

}

function Change_area(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	$.documentos.set({
		otros: elemento
	});
	if ('documentos' in $) $documentos = $.documentos.toJSON();
	elemento = null, source = null;

}

function Change_campo(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	/** 
	 * Actualizamos la tabla con los dias para enviar los documentos 
	 */
	$.documentos.set({
		dias: elemento
	});
	if ('documentos' in $) $documentos = $.documentos.toJSON();
	elemento = null, source = null;

}

function Return_campo(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	/** 
	 * Al presionar enter en el teclado del equipo, desenfocamos la caja 
	 */
	$.campo.blur();
	elemento = null, source = null;

}

function Click_tabla(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var evento = e;
	/** 
	 * Desenfocamos las cajas de texto 
	 */
	$.area.blur();
	$.campo.blur();

}

function Postlayout_vista43(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.progreso.show();

}

function Postlayout_DOCUMENTOS(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	/** 
	 * Cerramos pantalla contenidos 
	 */
	Alloy.Events.trigger('_cerrar_insp', {
		pantalla: 'contenidos'
	});

}

var preguntarOpciones_opts = [L('x3481285698_traducir', 'Asegurado no puede seguir'), L('x3077268071_traducir', ' Se me acabo la bateria'), L('x479989047_traducir', ' Tuve un accidente'), L('x2376009830_traducir', ' Cancelar')];
var preguntarOpciones = Ti.UI.createOptionDialog({
	title: L('x1670176792_traducir', 'RAZON PARA CANCELAR INSPECCION ACTUAL'),
	options: preguntarOpciones_opts
});
preguntarOpciones.addEventListener('click', function(e) {
	var resp = preguntarOpciones_opts[e.index];
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
	if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
		var razon = "";
		if (resp == L('x3481285698_traducir', 'Asegurado no puede seguir')) {
			/** 
			 * Esto parece redundante, pero es para escapar de i18n el texto 
			 */
			razon = "Asegurado no puede seguir";
		}
		if (resp == L('x1862794075_traducir', 'Se me acabo la bateria')) {
			razon = "Se me acabo la bateria";
		}
		if (resp == L('x4012805111_traducir', 'Tuve un accidente')) {
			razon = "Tuve un accidente";
		}
		if ((_.isObject(razon) || (_.isString(razon)) && !_.isEmpty(razon)) || _.isNumber(razon) || _.isBoolean(razon)) {
			if (Ti.App.deployType != 'production') console.log('llamando servicio cancelarTarea', {});
			require('vars')['insp_cancelada'] = L('x515385654_traducir', 'documentos');
			Alloy.Collections[$.documentos.config.adapter.collection_name].add($.documentos);
			$.documentos.save();
			Alloy.Collections[$.documentos.config.adapter.collection_name].fetch();
			var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
			/** 
			 * Mostramos popup avisando que se esta cancelando la inspeccion 
			 */
			var vista43_visible = true;

			if (vista43_visible == 'si') {
				vista43_visible = true;
			} else if (vista43_visible == 'no') {
				vista43_visible = false;
			}
			$.vista43.setVisible(vista43_visible);

			var consultarURL = {};
			consultarURL.success = function(e) {
				var elemento = e,
					valor = e;
				/** 
				 * Ocultamos popup avisando que se esta cancelando la inspeccion 
				 */
				var vista43_visible = false;

				if (vista43_visible == 'si') {
					vista43_visible = true;
				} else if (vista43_visible == 'no') {
					vista43_visible = false;
				}
				$.vista43.setVisible(vista43_visible);

				/** 
				 * Enviamos a firma 
				 */
				Alloy.createController("firma_index", {}).getView().open();
				elemento = null, valor = null;
			};
			consultarURL.error = function(e) {
				var elemento = e,
					valor = e;
				if (Ti.App.deployType != 'production') console.log('hubo error llamando cancelarTarea, la agregamos a cola de salida', {
					"elemento": elemento
				});
				if (Ti.App.deployType != 'production') console.log('agregando servicio cancelarTarea a cola', {});
				/** 
				 * En el caso que haya habido un problema llamando al servicio cancelarTarea, guardamos los datos en un objeto 
				 */
				var datos = {
					id_inspector: seltarea.id_inspector,
					codigo_identificador: inspector.codigo_identificador,
					id_server: seltarea.id_server,
					num_caso: seltarea.num_caso,
					razon: razon
				};
				var insertarModelo_m = Alloy.Collections.cola;
				var insertarModelo_fila = Alloy.createModel('cola', {
					data: JSON.stringify(datos),
					id_tarea: seltarea.id_server,
					tipo: 'cancelar'
				});
				insertarModelo_m.add(insertarModelo_fila);
				insertarModelo_fila.save();
				/** 
				 * Ocultamos popup avisando que se esta cancelando la inspeccion 
				 */
				var vista43_visible = false;

				if (vista43_visible == 'si') {
					vista43_visible = true;
				} else if (vista43_visible == 'no') {
					vista43_visible = false;
				}
				$.vista43.setVisible(vista43_visible);

				/** 
				 * Enviamos a firma 
				 */
				Alloy.createController("firma_index", {}).getView().open();
				elemento = null, valor = null;
			};
			require('helper').ajaxUnico('consultarURL', '' + String.format(L('x3244141284', '%1$scancelarTarea'), url_server.toString()) + '', 'POST', {
				id_inspector: seltarea.id_inspector,
				codigo_identificador: inspector.codigo_identificador,
				id_tarea: seltarea.id_server,
				num_caso: seltarea.num_caso,
				mensaje: razon,
				opcion: 0,
				tipo: 1
			}, 15000, consultarURL);
		}
	}
	resp = null;
});
/** 
 * Cerramos esta pantalla cuando la firma se ha ejecutado 
 */
_my_events['_cerrar_insp,ID_231485652'] = function(evento) {
	if (!_.isUndefined(evento.pantalla)) {
		if (evento.pantalla == L('x515385654_traducir', 'documentos')) {
			if (Ti.App.deployType != 'production') console.log('debug cerrando documentos', {});
			var ID_1781710521_trycatch = {
				error: function(e) {
					if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
				}
			};
			try {
				ID_1781710521_trycatch.error = function(evento) {
					if (Ti.App.deployType != 'production') console.log('error cerrando documentos', {});
				};
				$.DOCUMENTOS.close();
			} catch (e) {
				ID_1781710521_trycatch.error(e);
			}
		}
	} else {
		if (Ti.App.deployType != 'production') console.log('debug cerrando (todas) documentos', {});
		var ID_1962200830_trycatch = {
			error: function(e) {
				if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
			}
		};
		try {
			ID_1962200830_trycatch.error = function(evento) {
				if (Ti.App.deployType != 'production') console.log('error cerrando documentos', {});
			};
			$.DOCUMENTOS.close();
		} catch (e) {
			ID_1962200830_trycatch.error(e);
		}
	}
};
Alloy.Events.on('_cerrar_insp', _my_events['_cerrar_insp,ID_231485652']);
var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
	/** 
	 * Inicializamos la tabla con datos default 
	 */
	$.documentos.set({
		id_inspeccion: seltarea.id_server,
		doc10: 0,
		doc6: 0,
		doc4: 0,
		doc7: 0,
		doc3: 0,
		edificio: 0,
		doc9: 0,
		dias: -1,
		doc1: 0,
		doc5: 0,
		contenidos: 0,
		otros: '',
		doc2: 0,
		doc8: 0
	});
	if ('documentos' in $) $documentos = $.documentos.toJSON();
}


(function() {
	/** 
	 * Inicializamos flag para determinar si el doc10 tiene que estar en 0 o 1 
	 */
	require('vars')[_var_scopekey]['s1'] = L('x4108050209', '0');
	require('vars')[_var_scopekey]['s2'] = L('x4108050209', '0');
	/** 
	 * Avisamos que la inspeccion no fue cancelada 
	 */
	require('vars')['insp_cancelada'] = '';
})();

if (OS_IOS || OS_ANDROID) {
	$.DOCUMENTOS.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}
//$.DOCUMENTOS.open();
