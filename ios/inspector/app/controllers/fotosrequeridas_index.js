var _bind4section = {};
var _list_templates = {
	"contenido": {
		"Label2": {
			"text": "{id}"
		},
		"vista4": {},
		"Label": {
			"text": "{nombre}"
		}
	},
	"recinto": {
		"Label2": {
			"text": "{id}"
		},
		"vista4": {},
		"Label": {
			"text": "{nombre}"
		}
	},
	"nivel": {
		"Label2": {
			"text": "{id}"
		},
		"vista20": {},
		"Label": {
			"text": "{nombre}"
		}
	}
};
var $fotosr = $.fotosr.toJSON();

$.FOTOS_REQUERIDAS_window.setTitleAttributes({
	color: 'WHITE'
});
var _activity;
if (OS_ANDROID) {
	_activity = $.FOTOS_REQUERIDAS.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'FOTOS_REQUERIDAS';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.FOTOS_REQUERIDAS_window.addEventListener('open', function(e) {
		abx.setStatusbarColor("#FFFFFF");
		abx.setBackgroundColor("#2d9edb");
	});
}

function Click_vista(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	/** 
	 * Escondemos camara 
	 */
	camaraCuadrada.hide();
	/** 
	 * Cerramos pantalla actual 
	 */
	$.FOTOS_REQUERIDAS.close();

}

function Click_vista2(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	/** 
	 * Recuperamos variable de el objeto para saber si las fotos minimas fueron capturadas, si no estan, avisamos al usuario que foto falta 
	 */
	var requeridas = ('requeridas' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['requeridas'] : '';
	if (requeridas.foto1 == true || requeridas.foto1 == 'true') {
		var preguntarAlerta_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta = Ti.UI.createAlertDialog({
			title: L('x57652245_traducir', 'Alerta'),
			message: L('x305784584_traducir', 'Falta la foto de fachada'),
			buttonNames: preguntarAlerta_opts
		});
		preguntarAlerta.addEventListener('click', function(e) {
			var pre = preguntarAlerta_opts[e.index];
			pre = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta.show();
	} else if (requeridas.foto2 == true || requeridas.foto2 == 'true') {
		var preguntarAlerta2_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta2 = Ti.UI.createAlertDialog({
			title: L('x57652245_traducir', 'Alerta'),
			message: L('x3294013631_traducir', 'Falta la foto del barrio'),
			buttonNames: preguntarAlerta2_opts
		});
		preguntarAlerta2.addEventListener('click', function(e) {
			var pre = preguntarAlerta2_opts[e.index];
			pre = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta2.show();
	} else if (requeridas.foto3 == true || requeridas.foto3 == 'true') {
		var preguntarAlerta3_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta3 = Ti.UI.createAlertDialog({
			title: L('x57652245_traducir', 'Alerta'),
			message: L('x2286900918_traducir', 'Falta la foto de número de domicilio'),
			buttonNames: preguntarAlerta3_opts
		});
		preguntarAlerta3.addEventListener('click', function(e) {
			var pre = preguntarAlerta3_opts[e.index];
			pre = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta3.show();
	} else {
		/** 
		 * Escondemos camara 
		 */
		camaraCuadrada.hide();
		/** 
		 * Guardamos el modelo 
		 */
		Alloy.Collections[$.fotosr.config.adapter.collection_name].add($.fotosr);
		$.fotosr.save();
		Alloy.Collections[$.fotosr.config.adapter.collection_name].fetch();
		var nulo = Alloy.createController("hayalguien_index", {}).getView();
		nulo.open({
			modal: true,
			modalTransitionStyle: Ti.UI.iOS.MODAL_TRANSITION_STYLE_PARTIAL_CURL
		});
		nulo = null;
	}
}

if (OS_IOS) {
	var SquareCamera = require('com.mfogg.squarecamera');
	var camaraCuadrada = SquareCamera.createView({
		height: Ti.Platform.displayCaps.platformWidth,
		left: 0,
		frameDuration: 16,
		width: Ti.Platform.displayCaps.platformWidth,
		detectCodes: true,
		top: '64dp'
	});
} else if (OS_ANDROID) {
	var camaraCuadrada = Titanium.UI.createView({
		height: Ti.UI.SIZE,
		left: 0,
		width: Ti.UI.SIZE,
		top: '64dp'
	});
	var camaraCuadrada_camera = {};
	var camaraCuadrada_addcam = function() {
		var SquareCamera = require('pw.custom.androidcamera');
		camaraCuadrada_camera = SquareCamera.createCameraView({
			height: Ti.UI.SIZE,
			pictureTimeout: 200,
			width: Ti.UI.SIZE,
			top: '64dp',
			save_location: 'camera'
		});
		camaraCuadrada.add(camaraCuadrada_camera);
		camaraCuadrada_camera.addEventListener('picture_taken', function(evt) {
			camaraCuadrada.fireEvent('success', {
				media: evt.path
			});
		});
	};
	if (Ti.Media.isCameraSupported) {
		if (Ti.Media.hasCameraPermissions()) {
			camaraCuadrada.addcam();
		} else {
			Ti.Media.requestCameraPermissions(function(e) {
				if (e.success === true) {
					camaraCuadrada.addcam();
				} else {
					camaraCuadrada.fireEvent('error', {
						type: 'permission',
						message: 'permission denied'
					});
				}
			});
		}
	};
}
camaraCuadrada.addEventListener('success', function(e) {
	e.cancelBubble = true;
	var elemento = e.media;
	var valor = e.media;
	if (Ti.App.deployType != 'production') console.log('foto exitosa', {
		"elemento": elemento
	});
	var foto = ('foto' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['foto'] : '';
	if (foto == 1 || foto == '1') {
		$.widget4fotos.procesar({
			imagen1: elemento,
			nueva: 640
		});
	} else if (foto == 2) {
		$.widget4fotos.procesar({
			imagen2: elemento,
			nueva: 640
		});
	} else if (foto == 3) {
		$.widget4fotos.procesar({
			imagen3: elemento,
			nueva: 640
		});
	} else if (foto == 4) {
		$.widget4fotos.procesar({
			imagen4: elemento,
			nueva: 640
		});
	}
});
camaraCuadrada.addEventListener('code', function(e) {
	e.cancelBubble = true;
	var elemento = {
		tipo: e.codeType,
		valor: e.value
	};
	var valor = e.value;
	if (Ti.App.deployType != 'production') console.log('evento codigo', {
		"elemento": elemento
	});
});
$.FOTOS_REQUERIDAS.add(camaraCuadrada);

function Click_vista6(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	/** 
	 * Capturamos la foto 
	 */
	var camaraCuadrada_capturar = 'foto';

	var capturar = function(tipo) {
		if (tipo == 'foto' || tipo == 'imagen' || tipo == 'fotografia') {
			if (OS_IOS) {
				camaraCuadrada.takePhoto();
			} else {
				camaraCuadrada_camera.snapPicture();
			}
		} else if (tipo == 'video') {
			//camaraCuadrada.startRecording();
		}
	};
	capturar(camaraCuadrada_capturar);

	/** 
	 * Escondemos la vista para prevenir que el usuario tome multiples fotos a la vez, y evitar que la aplicacion se cierre 
	 */
	var vista6_visible = false;

	if (vista6_visible == 'si') {
		vista6_visible = true;
	} else if (vista6_visible == 'no') {
		vista6_visible = false;
	}
	$.vista6.setVisible(vista6_visible);


}

function Click_vista7(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	/** 
	 * Revisamos el flag de posicion de camara (frontal o trasera) y cambiamos la camara 
	 */
	var camara = ('camara' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['camara'] : '';
	if (camara == L('x2010748274_traducir', 'trasera')) {
		if (Ti.App.deployType != 'production') console.log('estoy en trasera', {});
		/** 
		 * Flag para saber que tiene que cambiar a frontal 
		 */
		require('vars')[_var_scopekey]['camara'] = L('x2556550622_traducir', 'frontal');
		var camaraCuadrada_camara = 'frontal';

		var modificarCamara = function(tipo) {
			if (OS_IOS) {
				if (tipo == 'frontal') {
					camaraCuadrada.setCamera('front');
				} else {
					camaraCuadrada.setCamera('back');
				}
			}
		};
		modificarCamara(camaraCuadrada_camara);

	}
	if (camara == L('x2556550622_traducir', 'frontal')) {
		if (Ti.App.deployType != 'production') console.log('estoy en frontal', {});
		require('vars')[_var_scopekey]['camara'] = L('x2010748274_traducir', 'trasera');
		var camaraCuadrada_camara = 'trasera';

		var modificarCamara = function(tipo) {
			if (OS_IOS) {
				if (tipo == 'frontal') {
					camaraCuadrada.setCamera('front');
				} else {
					camaraCuadrada.setCamera('back');
				}
			}
		};
		modificarCamara(camaraCuadrada_camara);

	}

}

$.widget4fotos.init({
	__id: 'ALL1189437781',
	onlisto: Listo_widget4fotos,
	label1: L('x724488137_traducir', 'Fachada'),
	label4: L('x676841594_traducir', 'Nº Depto'),
	label3: L('x4076266664_traducir', 'Numero'),
	onclick: Click_widget4fotos,
	label2: L('x2825989175_traducir', 'Barrio')
});

function Click_widget4fotos(e) {

	var evento = e;
	require('vars')[_var_scopekey]['foto'] = evento.pos;
	/** 
	 * Habilitamos boton tomar foto 
	 */
	var vista6_visible = true;

	if (vista6_visible == 'si') {
		vista6_visible = true;
	} else if (vista6_visible == 'no') {
		vista6_visible = false;
	}
	$.vista6.setVisible(vista6_visible);


}

function Listo_widget4fotos(e) {

	var evento = e;
	if (Ti.App.deployType != 'production') console.log('imagen procesada obtenida 4fotos', {
		"evento": evento
	});
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	if (_.isString(seltarea)) {
		/** 
		 * Definimos valor inventado id_server para testing 
		 */
		var seltarea = {
			id_server: 123
		};
	}
	var previos = [];
	var ID_587641544_f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
	if (ID_587641544_f.exists() == true) {
		previos = ID_587641544_f.getDirectoryListing();
	}
	if (Ti.App.deployType != 'production') console.log('archivos previos existentes en telefono', {
		"previos": previos
	});
	var requeridas = ('requeridas' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['requeridas'] : '';
	if (evento.pos == 1 || evento.pos == '1') {
		/** 
		 * Cambiamos estado de foto1 para avisar que tenemos foto capturada 
		 */
		var requeridas = _.extend(requeridas, {
			foto1: L('x734881840_traducir', 'false')
		});
		/** 
		 * Guardamos la foto en la memoria del celular 
		 */
		var ID_870492955_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
		if (ID_870492955_d.exists() == false) ID_870492955_d.createDirectory();
		var ID_870492955_f = Ti.Filesystem.getFile(ID_870492955_d.resolve(), 'foto1.jpg');
		if (ID_870492955_f.exists() == true) ID_870492955_f.deleteFile();
		ID_870492955_f.write(evento.nueva);
		ID_870492955_d = null;
		ID_870492955_f = null;
		/** 
		 * Actualizamos el modelo avisando que la foto de fachada, el archivo se llama foto1 
		 */
		$.fotosr.set({
			fachada: 'foto1.jpg'
		});
		if ('fotosr' in $) $fotosr = $.fotosr.toJSON();
	} else if (evento.pos == 2) {
		var requeridas = _.extend(requeridas, {
			foto2: L('x734881840_traducir', 'false')
		});
		var ID_1597663409_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
		if (ID_1597663409_d.exists() == false) ID_1597663409_d.createDirectory();
		var ID_1597663409_f = Ti.Filesystem.getFile(ID_1597663409_d.resolve(), 'foto2.jpg');
		if (ID_1597663409_f.exists() == true) ID_1597663409_f.deleteFile();
		ID_1597663409_f.write(evento.nueva);
		ID_1597663409_d = null;
		ID_1597663409_f = null;
		$.fotosr.set({
			barrio: 'foto2.jpg'
		});
		if ('fotosr' in $) $fotosr = $.fotosr.toJSON();
	} else if (evento.pos == 3) {
		var requeridas = _.extend(requeridas, {
			foto3: L('x734881840_traducir', 'false')
		});
		var ID_372013953_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
		if (ID_372013953_d.exists() == false) ID_372013953_d.createDirectory();
		var ID_372013953_f = Ti.Filesystem.getFile(ID_372013953_d.resolve(), 'foto3.jpg');
		if (ID_372013953_f.exists() == true) ID_372013953_f.deleteFile();
		ID_372013953_f.write(evento.nueva);
		ID_372013953_d = null;
		ID_372013953_f = null;
		$.fotosr.set({
			numero: 'foto3.jpg'
		});
		if ('fotosr' in $) $fotosr = $.fotosr.toJSON();
	} else if (evento.pos == 4) {
		var requeridas = _.extend(requeridas, {
			foto4: L('x734881840_traducir', 'false')
		});
		var ID_562700504_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
		if (ID_562700504_d.exists() == false) ID_562700504_d.createDirectory();
		var ID_562700504_f = Ti.Filesystem.getFile(ID_562700504_d.resolve(), 'foto4.jpg');
		if (ID_562700504_f.exists() == true) ID_562700504_f.deleteFile();
		ID_562700504_f.write(evento.nueva);
		ID_562700504_d = null;
		ID_562700504_f = null;
		$.fotosr.set({
			depto: 'foto4.jpg'
		});
		if ('fotosr' in $) $fotosr = $.fotosr.toJSON();
	}
	require('vars')[_var_scopekey]['requeridas'] = requeridas;
	/** 
	 * Habilitamos boton tomar foto 
	 */
	var vista6_visible = true;

	if (vista6_visible == 'si') {
		vista6_visible = true;
	} else if (vista6_visible == 'no') {
		vista6_visible = false;
	}
	$.vista6.setVisible(vista6_visible);


}

function Postlayout_FOTOS_REQUERIDAS(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	/** 
	 * Avisamos que se debe cerrar pantalla detalle de tarea 
	 */
	Alloy.Events.trigger('_cerrar_insp', {
		pantalla: 'detalle'
	});

}

(function() {
	/** 
	 * Seteamos flag para saber que se inicia en la camara trasera 
	 */
	require('vars')[_var_scopekey]['camara'] = L('x2010748274_traducir', 'trasera');
	/** 
	 * Creamos objeto para poder saber si el minimo de fotos obligatorias existen 
	 */
	var requeridas = {
		foto1: L('x4261170317', 'true'),
		foto2: L('x4261170317', 'true'),
		foto3: L('x4261170317', 'true'),
		foto4: L('x734881840_traducir', 'false')
	};
	require('vars')[_var_scopekey]['requeridas'] = requeridas;
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
		/** 
		 * Verificamos que estemos en una inspeccion para poder modificar el id_inspeccion del modelo de fotos requeridas 
		 */
		$.fotosr.set({
			id_inspeccion: seltarea.id_server
		});
		if ('fotosr' in $) $fotosr = $.fotosr.toJSON();
	}
})();


/** 
 * Cerramos esta pantalla cuando es llamada en la pantalla siguente (hayalguien) 
 */
_my_events['_cerrar_insp,ID_231485652'] = function(evento) {
	if (!_.isUndefined(evento.pantalla)) {
		if (evento.pantalla == L('x2385878536_traducir', 'frequeridas')) {
			if (Ti.App.deployType != 'production') console.log('debug cerrando fotos requeridas', {});
			var ID_962933245_trycatch = {
				error: function(e) {
					if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
				}
			};
			try {
				ID_962933245_trycatch.error = function(evento) {
					if (Ti.App.deployType != 'production') console.log('error cerrando fotos requeridas', {});
				};
				$.FOTOS_REQUERIDAS.close();
			} catch (e) {
				ID_962933245_trycatch.error(e);
			}
		}
	} else {
		if (Ti.App.deployType != 'production') console.log('debug cerrando (todas) fotos requeridas', {});
		var ID_1962200830_trycatch = {
			error: function(e) {
				if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
			}
		};
		try {
			ID_1962200830_trycatch.error = function(evento) {
				if (Ti.App.deployType != 'production') console.log('error cerrando fotos requeridas', {});
			};
			$.FOTOS_REQUERIDAS.close();
		} catch (e) {
			ID_1962200830_trycatch.error(e);
		}
	}
};
Alloy.Events.on('_cerrar_insp', _my_events['_cerrar_insp,ID_231485652']);

if (OS_IOS || OS_ANDROID) {
	$.FOTOS_REQUERIDAS.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}
//$.FOTOS_REQUERIDAS.open();
