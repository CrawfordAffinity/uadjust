var _bind4section = {};
var _list_templates = {};
var $dano = $.dano.toJSON();

$.NUEVO_DAÑO_window.setTitleAttributes({
	color: 'WHITE'
});
var _activity;
if (OS_ANDROID) {
	_activity = $.NUEVO_DAÑO.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'NUEVO_DAÑO';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.NUEVO_DAÑO_window.addEventListener('open', function(e) {
		abx.setStatusbarColor("#FFFFFF");
		abx.setBackgroundColor("#b9aaf3");
	});
}

function Click_vista33(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	/** 
	 * Limpiamos widget 
	 */
	$.widgetModal8.limpiar({});
	$.widgetModal9.limpiar({});
	$.widgetModal10.limpiar({});
	$.NUEVO_DAÑO.close();

}

function Click_vista37(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var test = $.dano.toJSON();
	if (_.isUndefined(test.id_partida)) {
		/** 
		 * Validamos que lo ingresado sea correcto 
		 */
		var preguntarAlerta18_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta18 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x1349590523_traducir', 'Seleccione partida'),
			buttonNames: preguntarAlerta18_opts
		});
		preguntarAlerta18.addEventListener('click', function(e) {
			var nulo = preguntarAlerta18_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta18.show();
	} else if (_.isUndefined(test.id_tipodano)) {
		var preguntarAlerta19_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta19 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x2702600033_traducir', 'Seleccione el tipo de daño'),
			buttonNames: preguntarAlerta19_opts
		});
		preguntarAlerta19.addEventListener('click', function(e) {
			var nulo = preguntarAlerta19_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta19.show();
	} else if (_.isUndefined(test.id_unidadmedida)) {
		var preguntarAlerta20_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta20 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x4202932850_traducir', 'Seleccione la unidad de medida del daño'),
			buttonNames: preguntarAlerta20_opts
		});
		preguntarAlerta20.addEventListener('click', function(e) {
			var nulo = preguntarAlerta20_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta20.show();
	} else if (_.isUndefined(test.superficie)) {
		var preguntarAlerta21_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta21 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x1057642944_traducir', 'Ingrese la cubicación del daño'),
			buttonNames: preguntarAlerta21_opts
		});
		preguntarAlerta21.addEventListener('click', function(e) {
			var nulo = preguntarAlerta21_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta21.show();
	} else if ((_.isObject(test.superficie) || _.isString(test.superficie)) && _.isEmpty(test.superficie)) {
		var preguntarAlerta22_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta22 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x1057642944_traducir', 'Ingrese la cubicación del daño'),
			buttonNames: preguntarAlerta22_opts
		});
		preguntarAlerta22.addEventListener('click', function(e) {
			var nulo = preguntarAlerta22_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta22.show();
	} else {
		if (!_.isUndefined(args._dato)) {
			/** 
			 * Si estamos editando, debemos borrar modelo previo. 
			 */
			/** 
			 * Eliminamos registro del dano que estamos editando para guardar el nuevo 
			 */
			/** 
			 * Eliminamos registro del dano que estamos editando para guardar el nuevo 
			 */
			var eliminarModelo10_i = Alloy.Collections.insp_itemdanos;
			var sql = 'DELETE FROM ' + eliminarModelo10_i.config.adapter.collection_name + ' WHERE id=\'' + args._dato.id + '\'';
			var db = Ti.Database.open(eliminarModelo10_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo10_i.trigger('remove');
		}
		/** 
		 * Guardamos en modelo el dano ingresado 
		 */
		Alloy.Collections[$.dano.config.adapter.collection_name].add($.dano);
		$.dano.save();
		Alloy.Collections[$.dano.config.adapter.collection_name].fetch();
		test = null;
		/** 
		 * Limpiamos widget 
		 */
		$.widgetModal8.limpiar({});
		$.widgetModal9.limpiar({});
		$.widgetModal10.limpiar({});
		$.NUEVO_DAÑO.close();
	}
}

$.widgetModal8.init({
	titulo: L('x2525275670_traducir', 'PARTIDA'),
	cargando: L('x1740321226_traducir', 'cargando ..'),
	__id: 'ALL1687673767',
	left: 0,
	onrespuesta: Respuesta_widgetModal8,
	campo: L('x1719427472_traducir', 'Partida'),
	onabrir: Abrir_widgetModal8,
	color: 'morado',
	right: 0,
	top: 15,
	seleccione: L('x221750731_traducir', 'seleccione partida'),
	activo: true,
	onafterinit: Afterinit_widgetModal8
});

function Afterinit_widgetModal8(e) {

	var evento = e;
	var ID_1331103465_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			var consultarModelo31_i = Alloy.createCollection('tipo_partida');
			var consultarModelo31_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
			consultarModelo31_i.fetch({
				query: 'SELECT * FROM tipo_partida WHERE pais_texto=\'' + pais[0].nombre + '\''
			});
			var partidas = require('helper').query2array(consultarModelo31_i);
			var datos = [];
			_.each(partidas, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'valor';
					if (llave == 'id_segured') newkey = 'id_interno';
					if (llave == 'id_server') newkey = 'id_gerardo';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
		} else {
			var eliminarModelo11_i = Alloy.Collections.tipo_partida;
			var sql = "DELETE FROM " + eliminarModelo11_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo11_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo11_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo13_m = Alloy.Collections.tipo_partida;
				var insertarModelo13_fila = Alloy.createModel('tipo_partida', {
					nombre: String.format(L('x3638114596_traducir', 'Muro%1$s'), item.toString()),
					id_server: item,
					id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item.toString()),
					pais: 1
				});
				insertarModelo13_m.add(insertarModelo13_fila);
				insertarModelo13_fila.save();
			});
			var transformarModelo16_i = Alloy.createCollection('tipo_partida');
			transformarModelo16_i.fetch();
			var transformarModelo16_src = require('helper').query2array(transformarModelo16_i);
			var datos = [];
			_.each(transformarModelo16_src, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'valor';
					if (llave == 'id_segured') newkey = 'id_interno';
					if (llave == 'id_server') newkey = 'id_gerardo';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
		}
		$.widgetModal8.data({
			data: datos
		});
		if (!_.isUndefined(args._dato)) {
			var consultarModelo32_i = Alloy.createCollection('insp_itemdanos');
			var consultarModelo32_i_where = 'id=\'' + args._dato.id + '\'';
			consultarModelo32_i.fetch({
				query: 'SELECT * FROM insp_itemdanos WHERE id=\'' + args._dato.id + '\''
			});
			var danos = require('helper').query2array(consultarModelo32_i);
			if (danos && danos.length) {
				/** 
				 * obtenemos valor seleccionado previamente y lo mostramos en selector. 
				 */
				var consultarModelo33_i = Alloy.createCollection('tipo_partida');
				var consultarModelo33_i_where = 'id_segured=\'' + danos[0].id_partida + '\'';
				consultarModelo33_i.fetch({
					query: 'SELECT * FROM tipo_partida WHERE id_segured=\'' + danos[0].id_partida + '\''
				});
				var t_partida = require('helper').query2array(consultarModelo33_i);
				if (t_partida && t_partida.length) {
					var ID_1175218658_func = function() {
						$.widgetModal8.labels({
							valor: t_partida[0].nombre
						});
					};
					var ID_1175218658 = setTimeout(ID_1175218658_func, 1000 * 0.21);
				}
			}
		}
	};
	var ID_1331103465 = setTimeout(ID_1331103465_func, 1000 * 0.2);

}

function Abrir_widgetModal8(e) {

	var evento = e;

}

function Respuesta_widgetModal8(e) {

	var evento = e;
	$.widgetModal8.labels({
		valor: evento.valor
	});
	/** 
	 * Asumimos que el valor mostrado seleccionado es el nombre de la fila dano, ya que no existe el campo nombre en esta pantalla. 
	 */
	$.dano.set({
		nombre: evento.valor,
		id_partida: evento.item.id_interno
	});
	if ('dano' in $) $dano = $.dano.toJSON();
	if (Ti.App.deployType != 'production') console.log('datos recibidos de modal tipo partidas', {
		"datos": evento
	});
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
		var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
		var consultarModelo34_i = Alloy.createCollection('tipo_dano');
		var consultarModelo34_i_where = 'pais_texto=\'' + pais[0].nombre + '\' AND id_partida=\'' + evento.item.id_gerardo + '\'';
		consultarModelo34_i.fetch({
			query: 'SELECT * FROM tipo_dano WHERE pais_texto=\'' + pais[0].nombre + '\' AND id_partida=\'' + evento.item.id_gerardo + '\''
		});
		var danos = require('helper').query2array(consultarModelo34_i);
		/** 
		 * Obtenemos datos para selectores (solo los de este pais y este tipo de partida) 
		 */
		var datos = [];
		_.each(danos, function(fila, pos) {
			var new_row = {};
			_.each(fila, function(x, llave) {
				var newkey = '';
				if (llave == 'nombre') newkey = 'valor';
				if (llave == 'id_segured') newkey = 'id_interno';
				if (llave == 'id_server') newkey = 'id_gerardo';
				if (newkey != '') new_row[newkey] = fila[llave];
			});
			datos.push(new_row);
		});
		$.widgetModal9.data({
			data: datos
		});
	}
	/** 
	 * Editamos y limpiamos los campos que estan siendo filtrados con este campo 
	 */
	$.widgetModal9.labels({
		seleccione: 'seleccione tipo'
	});
	$.widgetModal10.labels({
		seleccione: 'unidad'
	});
	var superficie;
	superficie = $.campo5;
	superficie.setValue("");
	$.label5.setText('-');


}

$.widgetModal9.init({
	titulo: L('x1384515306_traducir', 'TIPO DAÑO'),
	cargando: L('x1740321226_traducir', 'cargando ..'),
	__id: 'ALL1413999243',
	left: 0,
	onrespuesta: Respuesta_widgetModal9,
	campo: L('x953397154_traducir', 'Tipo de Daño'),
	onabrir: Abrir_widgetModal9,
	color: 'morado',
	right: 0,
	top: 20,
	seleccione: L('x4123108455_traducir', 'seleccione tipo'),
	activo: true,
	onafterinit: Afterinit_widgetModal9
});

function Afterinit_widgetModal9(e) {

	var evento = e;
	var ID_1686389527_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			var consultarModelo35_i = Alloy.createCollection('tipo_dano');
			var consultarModelo35_i_where = 'pais_texto=\'' + pais[0].nombre + '\' AND id_partida=0';
			consultarModelo35_i.fetch({
				query: 'SELECT * FROM tipo_dano WHERE pais_texto=\'' + pais[0].nombre + '\' AND id_partida=0'
			});
			var danos = require('helper').query2array(consultarModelo35_i);
			var datos = [];
			_.each(danos, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'valor';
					if (llave == 'id_segured') newkey = 'id_interno';
					if (llave == 'id_server') newkey = 'id_gerardo';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
		} else {
			/** 
			 * Insertamos dummies 
			 */
			/** 
			 * Insertamos dummies 
			 */
			var eliminarModelo12_i = Alloy.Collections.tipo_dano;
			var sql = "DELETE FROM " + eliminarModelo12_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo12_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo12_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo14_m = Alloy.Collections.tipo_dano;
				var insertarModelo14_fila = Alloy.createModel('tipo_dano', {
					nombre: String.format(L('x1478811929_traducir', 'Picada%1$s'), item.toString()),
					id_partida: 0,
					id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
					id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item.toString()),
					pais: 1
				});
				insertarModelo14_m.add(insertarModelo14_fila);
				insertarModelo14_fila.save();
			});
			var transformarModelo19_i = Alloy.createCollection('tipo_dano');
			transformarModelo19_i.fetch();
			var transformarModelo19_src = require('helper').query2array(transformarModelo19_i);
			var datos = [];
			_.each(transformarModelo19_src, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'valor';
					if (llave == 'id_segured') newkey = 'id_interno';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
		}
		$.widgetModal9.data({
			data: datos
		});
		if (!_.isUndefined(args._dato)) {
			var consultarModelo36_i = Alloy.createCollection('insp_itemdanos');
			var consultarModelo36_i_where = 'id=\'' + args._dato.id + '\'';
			consultarModelo36_i.fetch({
				query: 'SELECT * FROM insp_itemdanos WHERE id=\'' + args._dato.id + '\''
			});
			var danos = require('helper').query2array(consultarModelo36_i);
			if (danos && danos.length) {
				/** 
				 * obtenemos valor seleccionado previamente y lo mostramos en selector. 
				 */
				var consultarModelo37_i = Alloy.createCollection('tipo_dano');
				var consultarModelo37_i_where = 'id_segured=\'' + danos[0].id_tipodano + '\'';
				consultarModelo37_i.fetch({
					query: 'SELECT * FROM tipo_dano WHERE id_segured=\'' + danos[0].id_tipodano + '\''
				});
				var t_dano = require('helper').query2array(consultarModelo37_i);
				if (t_dano && t_dano.length) {
					var ID_1044639882_func = function() {
						$.widgetModal9.labels({
							valor: t_dano[0].nombre
						});
					};
					var ID_1044639882 = setTimeout(ID_1044639882_func, 1000 * 0.21);
				}
			}
		}
	};
	var ID_1686389527 = setTimeout(ID_1686389527_func, 1000 * 0.2);

}

function Abrir_widgetModal9(e) {

	var evento = e;

}

function Respuesta_widgetModal9(e) {

	var evento = e;
	/** 
	 * Mostramos el valor seleccionado desde el widget 
	 */
	$.widgetModal9.labels({
		valor: evento.valor
	});
	/** 
	 * Actualizamos el id_tipodano e id_unidadmedida 
	 */
	$.dano.set({
		id_tipodano: evento.item.id_interno
	});
	if ('dano' in $) $dano = $.dano.toJSON();
	if (Ti.App.deployType != 'production') console.log('datos recibidos de modal tipo partidas', {
		"datos": evento
	});
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
		/** 
		 * Consultamos id_gerardo en tipo_accion, para obtener unidades de medida 
		 */
		var consultarModelo38_i = Alloy.createCollection('tipo_accion');
		var consultarModelo38_i_where = 'id_tipo_dano=\'' + evento.item.id_gerardo + '\'';
		consultarModelo38_i.fetch({
			query: 'SELECT * FROM tipo_accion WHERE id_tipo_dano=\'' + evento.item.id_gerardo + '\''
		});
		var acciones = require('helper').query2array(consultarModelo38_i);
		if (acciones && acciones.length) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			var consultarModelo39_i = Alloy.createCollection('unidad_medida');
			var consultarModelo39_i_where = 'pais_texto=\'' + pais[0].nombre + '\' AND id_accion=\'' + acciones[0].id_server + '\'';
			consultarModelo39_i.fetch({
				query: 'SELECT * FROM unidad_medida WHERE pais_texto=\'' + pais[0].nombre + '\' AND id_accion=\'' + acciones[0].id_server + '\''
			});
			var unidad = require('helper').query2array(consultarModelo39_i);
			var datos = [];
			_.each(unidad, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'valor';
					if (llave == 'id_segured') newkey = 'id_interno';
					if (llave == 'id_server') newkey = 'id_gerardo';
					if (llave == 'abrev') newkey = 'abrev';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
			$.widgetModal10.data({
				data: datos
			});
		}
	}
	/** 
	 * Actualizamos texto de widget unidad de medida 
	 */
	$.widgetModal10.labels({
		seleccione: 'unidad'
	});
	var superficie2;
	superficie2 = $.campo5;
	superficie2.setValue("");
	$.label5.setText('-');


}

$.widgetFotochica4.init({
	caja: L('x2889884971_traducir', '45'),
	__id: 'ALL1420842518',
	onlisto: Listo_widgetFotochica4,
	onclick: Click_widgetFotochica4
});

function Click_widgetFotochica4(e) {

	var evento = e;
	/** 
	 * Capturar foto 
	 */
	var capturarFoto4_result = function(e) {
		var foto_full = e.valor;
		var foto = e.valor.data;
		if (foto_full.error == true || foto_full.error == 'true') {
			/** 
			 * Si existe error al capturar foto mostramos alerta 
			 */
			var preguntarAlerta23_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta23 = Ti.UI.createAlertDialog({
				title: L('x57652245_traducir', 'Alerta'),
				message: L('x1068520423_traducir', 'Existe un problema con la camara'),
				buttonNames: preguntarAlerta23_opts
			});
			preguntarAlerta23.addEventListener('click', function(e) {
				var suu = preguntarAlerta23_opts[e.index];
				suu = null;
				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta23.show();
		} else {
			$.widgetFotochica4.procesar({
				imagen: foto,
				nueva: 640,
				calidad: 91
			});
		}
	};

	function camara_capturarFoto4() {
		Ti.Media.showCamera({
			success: function(event) {
				if (event.mediaType == Ti.Media.MEDIA_TYPE_PHOTO) {
					capturarFoto4_result({
						valor: {
							error: false,
							cancel: false,
							data: event.media,
							reason: ''
						}
					});
				} else {
					capturarFoto4_result({
						valor: {
							error: true,
							cancel: false,
							data: '',
							reason: 'not image'
						}
					});
				}
			},
			cancel: function() {
				capturarFoto4_result({
					valor: {
						error: false,
						cancel: true,
						data: '',
						reason: 'cancelled'
					}
				});
			},
			error: function(error) {
				capturarFoto4_result({
					valor: {
						error: true,
						cancel: false,
						data: error,
						reason: error.error
					}
				});
			},
			saveToPhotoGallery: false,
			allowImageEditing: true,
			mediaTypes: [Ti.Media.MEDIA_TYPE_PHOTO]
		});
	}
	require('vars')['_estadoflash_'] = 'auto';
	require('vars')['_tipocamara_'] = 'trasera';
	require('vars')['_hayflash_'] = true;
	if (Ti.Media.hasCameraPermissions()) {
		camara_capturarFoto4();
	} else {
		Ti.Media.requestCameraPermissions(function(ercp) {
			if (ercp.success) {
				camara_capturarFoto4();
			} else {
				capturarFoto4_result({
					valor: {
						error: true,
						cancel: false,
						data: 'nopermission',
						type: 'permission',
						reason: 'camera doesnt have permissions'
					}
				});
			}
		});
	}

}

function Listo_widgetFotochica4(e) {

	var evento = e;
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	var insertarModelo15_m = Alloy.Collections.numero_unico;
	var insertarModelo15_fila = Alloy.createModel('numero_unico', {
		comentario: 'nuevo itemdano foto1'
	});
	insertarModelo15_m.add(insertarModelo15_fila);
	insertarModelo15_fila.save();
	var nuevoid = require('helper').model2object(insertarModelo15_m.last());
	if (!_.isUndefined(evento.comprimida)) {
		if (Ti.App.deployType != 'production') console.log(String.format(L('x2721240094', 'imagen capturada, comprimida y asignada como cap%1$s.jpg'), nuevoid.id.toString()), {});
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var ID_1157857198_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
			if (ID_1157857198_d.exists() == false) ID_1157857198_d.createDirectory();
			var ID_1157857198_f = Ti.Filesystem.getFile(ID_1157857198_d.resolve(), 'cap' + nuevoid.id + '.jpg');
			if (ID_1157857198_f.exists() == true) ID_1157857198_f.deleteFile();
			ID_1157857198_f.write(evento.comprimida);
			ID_1157857198_d = null;
			ID_1157857198_f = null;
			var ID_1712680165_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'mini' + seltarea.id_server);
			if (ID_1712680165_d.exists() == false) ID_1712680165_d.createDirectory();
			var ID_1712680165_f = Ti.Filesystem.getFile(ID_1712680165_d.resolve(), 'cap' + nuevoid.id + '.jpg');
			if (ID_1712680165_f.exists() == true) ID_1712680165_f.deleteFile();
			ID_1712680165_f.write(evento.mini);
			ID_1712680165_d = null;
			ID_1712680165_f = null;
		} else {
			var ID_320260826_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + 0);
			if (ID_320260826_d.exists() == false) ID_320260826_d.createDirectory();
			var ID_320260826_f = Ti.Filesystem.getFile(ID_320260826_d.resolve(), 'cap' + nuevoid.id + '.jpg');
			if (ID_320260826_f.exists() == true) ID_320260826_f.deleteFile();
			ID_320260826_f.write(evento.comprimida);
			ID_320260826_d = null;
			ID_320260826_f = null;
			var ID_1120725152_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'mini' + 0);
			if (ID_1120725152_d.exists() == false) ID_1120725152_d.createDirectory();
			var ID_1120725152_f = Ti.Filesystem.getFile(ID_1120725152_d.resolve(), 'cap' + nuevoid.id + '.jpg');
			if (ID_1120725152_f.exists() == true) ID_1120725152_f.deleteFile();
			ID_1120725152_f.write(evento.mini);
			ID_1120725152_d = null;
			ID_1120725152_f = null;
		}
		$.dano.set({
			foto1: String.format(L('x1070475521', 'cap%1$s.jpg'), nuevoid.id.toString())
		});
		if ('dano' in $) $dano = $.dano.toJSON();
	}

}

$.widgetFotochica5.init({
	caja: L('x2889884971_traducir', '45'),
	__id: 'ALL677343075',
	onlisto: Listo_widgetFotochica5,
	onclick: Click_widgetFotochica5
});

function Click_widgetFotochica5(e) {

	var evento = e;
	var capturarFoto5_result = function(e) {
		var foto_full = e.valor;
		var foto = e.valor.data;
		if (foto_full.error == true || foto_full.error == 'true') {
			var preguntarAlerta24_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta24 = Ti.UI.createAlertDialog({
				title: L('x57652245_traducir', 'Alerta'),
				message: L('x1068520423_traducir', 'Existe un problema con la camara'),
				buttonNames: preguntarAlerta24_opts
			});
			preguntarAlerta24.addEventListener('click', function(e) {
				var suu = preguntarAlerta24_opts[e.index];
				suu = null;
				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta24.show();
		} else {
			$.widgetFotochica5.procesar({
				imagen: foto,
				nueva: 640,
				calidad: 91
			});
		}
	};

	function camara_capturarFoto5() {
		Ti.Media.showCamera({
			success: function(event) {
				if (event.mediaType == Ti.Media.MEDIA_TYPE_PHOTO) {
					capturarFoto5_result({
						valor: {
							error: false,
							cancel: false,
							data: event.media,
							reason: ''
						}
					});
				} else {
					capturarFoto5_result({
						valor: {
							error: true,
							cancel: false,
							data: '',
							reason: 'not image'
						}
					});
				}
			},
			cancel: function() {
				capturarFoto5_result({
					valor: {
						error: false,
						cancel: true,
						data: '',
						reason: 'cancelled'
					}
				});
			},
			error: function(error) {
				capturarFoto5_result({
					valor: {
						error: true,
						cancel: false,
						data: error,
						reason: error.error
					}
				});
			},
			saveToPhotoGallery: false,
			allowImageEditing: true,
			mediaTypes: [Ti.Media.MEDIA_TYPE_PHOTO]
		});
	}
	require('vars')['_estadoflash_'] = 'auto';
	require('vars')['_tipocamara_'] = 'trasera';
	require('vars')['_hayflash_'] = true;
	if (Ti.Media.hasCameraPermissions()) {
		camara_capturarFoto5();
	} else {
		Ti.Media.requestCameraPermissions(function(ercp) {
			if (ercp.success) {
				camara_capturarFoto5();
			} else {
				capturarFoto5_result({
					valor: {
						error: true,
						cancel: false,
						data: 'nopermission',
						type: 'permission',
						reason: 'camera doesnt have permissions'
					}
				});
			}
		});
	}

}

function Listo_widgetFotochica5(e) {

	var evento = e;
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	var insertarModelo16_m = Alloy.Collections.numero_unico;
	var insertarModelo16_fila = Alloy.createModel('numero_unico', {
		comentario: 'nuevo itemdano foto2'
	});
	insertarModelo16_m.add(insertarModelo16_fila);
	insertarModelo16_fila.save();
	var nuevoid = require('helper').model2object(insertarModelo16_m.last());
	if (!_.isUndefined(evento.comprimida)) {
		if (Ti.App.deployType != 'production') console.log(String.format(L('x2721240094', 'imagen capturada, comprimida y asignada como cap%1$s.jpg'), nuevoid.id.toString()), {});
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var ID_1283075062_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
			if (ID_1283075062_d.exists() == false) ID_1283075062_d.createDirectory();
			var ID_1283075062_f = Ti.Filesystem.getFile(ID_1283075062_d.resolve(), 'cap' + nuevoid.id + '.jpg');
			if (ID_1283075062_f.exists() == true) ID_1283075062_f.deleteFile();
			ID_1283075062_f.write(evento.comprimida);
			ID_1283075062_d = null;
			ID_1283075062_f = null;
			var ID_1960146281_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'mini' + seltarea.id_server);
			if (ID_1960146281_d.exists() == false) ID_1960146281_d.createDirectory();
			var ID_1960146281_f = Ti.Filesystem.getFile(ID_1960146281_d.resolve(), 'cap' + nuevoid.id + '.jpg');
			if (ID_1960146281_f.exists() == true) ID_1960146281_f.deleteFile();
			ID_1960146281_f.write(evento.mini);
			ID_1960146281_d = null;
			ID_1960146281_f = null;
		} else {
			var ID_1870012101_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + 0);
			if (ID_1870012101_d.exists() == false) ID_1870012101_d.createDirectory();
			var ID_1870012101_f = Ti.Filesystem.getFile(ID_1870012101_d.resolve(), 'cap' + nuevoid.id + '.jpg');
			if (ID_1870012101_f.exists() == true) ID_1870012101_f.deleteFile();
			ID_1870012101_f.write(evento.comprimida);
			ID_1870012101_d = null;
			ID_1870012101_f = null;
			var ID_1060399557_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'mini' + 0);
			if (ID_1060399557_d.exists() == false) ID_1060399557_d.createDirectory();
			var ID_1060399557_f = Ti.Filesystem.getFile(ID_1060399557_d.resolve(), 'cap' + nuevoid.id + '.jpg');
			if (ID_1060399557_f.exists() == true) ID_1060399557_f.deleteFile();
			ID_1060399557_f.write(evento.mini);
			ID_1060399557_d = null;
			ID_1060399557_f = null;
		}
		$.dano.set({
			foto2: String.format(L('x1070475521', 'cap%1$s.jpg'), nuevoid.id.toString())
		});
		if ('dano' in $) $dano = $.dano.toJSON();
	}

}

$.widgetFotochica6.init({
	caja: L('x2889884971_traducir', '45'),
	__id: 'ALL333724151',
	onlisto: Listo_widgetFotochica6,
	onclick: Click_widgetFotochica6
});

function Click_widgetFotochica6(e) {

	var evento = e;
	var capturarFoto6_result = function(e) {
		var foto_full = e.valor;
		var foto = e.valor.data;
		if (foto_full.error == true || foto_full.error == 'true') {
			var preguntarAlerta25_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta25 = Ti.UI.createAlertDialog({
				title: L('x57652245_traducir', 'Alerta'),
				message: L('x1068520423_traducir', 'Existe un problema con la camara'),
				buttonNames: preguntarAlerta25_opts
			});
			preguntarAlerta25.addEventListener('click', function(e) {
				var suu = preguntarAlerta25_opts[e.index];
				suu = null;
				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta25.show();
		} else {
			$.widgetFotochica6.procesar({
				imagen: foto,
				nueva: 640,
				calidad: 91
			});
		}
	};

	function camara_capturarFoto6() {
		Ti.Media.showCamera({
			success: function(event) {
				if (event.mediaType == Ti.Media.MEDIA_TYPE_PHOTO) {
					capturarFoto6_result({
						valor: {
							error: false,
							cancel: false,
							data: event.media,
							reason: ''
						}
					});
				} else {
					capturarFoto6_result({
						valor: {
							error: true,
							cancel: false,
							data: '',
							reason: 'not image'
						}
					});
				}
			},
			cancel: function() {
				capturarFoto6_result({
					valor: {
						error: false,
						cancel: true,
						data: '',
						reason: 'cancelled'
					}
				});
			},
			error: function(error) {
				capturarFoto6_result({
					valor: {
						error: true,
						cancel: false,
						data: error,
						reason: error.error
					}
				});
			},
			saveToPhotoGallery: false,
			allowImageEditing: true,
			mediaTypes: [Ti.Media.MEDIA_TYPE_PHOTO]
		});
	}
	require('vars')['_estadoflash_'] = 'auto';
	require('vars')['_tipocamara_'] = 'trasera';
	require('vars')['_hayflash_'] = true;
	if (Ti.Media.hasCameraPermissions()) {
		camara_capturarFoto6();
	} else {
		Ti.Media.requestCameraPermissions(function(ercp) {
			if (ercp.success) {
				camara_capturarFoto6();
			} else {
				capturarFoto6_result({
					valor: {
						error: true,
						cancel: false,
						data: 'nopermission',
						type: 'permission',
						reason: 'camera doesnt have permissions'
					}
				});
			}
		});
	}

}

function Listo_widgetFotochica6(e) {

	var evento = e;
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	var insertarModelo17_m = Alloy.Collections.numero_unico;
	var insertarModelo17_fila = Alloy.createModel('numero_unico', {
		comentario: 'nuevo itemdano foto3'
	});
	insertarModelo17_m.add(insertarModelo17_fila);
	insertarModelo17_fila.save();
	var nuevoid = require('helper').model2object(insertarModelo17_m.last());
	if (!_.isUndefined(evento.comprimida)) {
		if (Ti.App.deployType != 'production') console.log(String.format(L('x2721240094', 'imagen capturada, comprimida y asignada como cap%1$s.jpg'), nuevoid.id.toString()), {});
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var ID_900222749_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
			if (ID_900222749_d.exists() == false) ID_900222749_d.createDirectory();
			var ID_900222749_f = Ti.Filesystem.getFile(ID_900222749_d.resolve(), 'cap' + nuevoid.id + '.jpg');
			if (ID_900222749_f.exists() == true) ID_900222749_f.deleteFile();
			ID_900222749_f.write(evento.comprimida);
			ID_900222749_d = null;
			ID_900222749_f = null;
			var ID_218311107_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'mini' + seltarea.id_server);
			if (ID_218311107_d.exists() == false) ID_218311107_d.createDirectory();
			var ID_218311107_f = Ti.Filesystem.getFile(ID_218311107_d.resolve(), 'cap' + nuevoid.id + '.jpg');
			if (ID_218311107_f.exists() == true) ID_218311107_f.deleteFile();
			ID_218311107_f.write(evento.mini);
			ID_218311107_d = null;
			ID_218311107_f = null;
		} else {
			var ID_1306087304_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + 0);
			if (ID_1306087304_d.exists() == false) ID_1306087304_d.createDirectory();
			var ID_1306087304_f = Ti.Filesystem.getFile(ID_1306087304_d.resolve(), 'cap' + nuevoid.id + '.jpg');
			if (ID_1306087304_f.exists() == true) ID_1306087304_f.deleteFile();
			ID_1306087304_f.write(evento.comprimida);
			ID_1306087304_d = null;
			ID_1306087304_f = null;
			var ID_1408182389_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'mini' + 0);
			if (ID_1408182389_d.exists() == false) ID_1408182389_d.createDirectory();
			var ID_1408182389_f = Ti.Filesystem.getFile(ID_1408182389_d.resolve(), 'cap' + nuevoid.id + '.jpg');
			if (ID_1408182389_f.exists() == true) ID_1408182389_f.deleteFile();
			ID_1408182389_f.write(evento.mini);
			ID_1408182389_d = null;
			ID_1408182389_f = null;
		}
		$.dano.set({
			foto3: String.format(L('x1070475521', 'cap%1$s.jpg'), nuevoid.id.toString())
		});
		if ('dano' in $) $dano = $.dano.toJSON();
	}

}

$.widgetModal10.init({
	titulo: L('x52303785_traducir', 'UNIDAD'),
	cargando: L('x1740321226_traducir', 'cargando ..'),
	__id: 'ALL1908023149',
	left: 0,
	onrespuesta: Respuesta_widgetModal10,
	campo: L('x2054670809_traducir', 'Unidad de medida'),
	onabrir: Abrir_widgetModal10,
	color: 'morado',
	right: 5,
	top: 3,
	seleccione: L('x4091990063_traducir', 'unidad'),
	activo: true,
	onafterinit: Afterinit_widgetModal10
});

function Afterinit_widgetModal10(e) {

	var evento = e;
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
		/** 
		 * Viene de login 
		 */
		var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
		var consultarModelo40_i = Alloy.createCollection('unidad_medida');
		var consultarModelo40_i_where = 'pais_texto=\'' + pais[0].nombre + '\' AND id_accion=0';
		consultarModelo40_i.fetch({
			query: 'SELECT * FROM unidad_medida WHERE pais_texto=\'' + pais[0].nombre + '\' AND id_accion=0'
		});
		var unidad = require('helper').query2array(consultarModelo40_i);
		/** 
		 * obtenemos datos para selectores (solo los de este pais) 
		 */
		var datos = [];
		_.each(unidad, function(fila, pos) {
			var new_row = {};
			_.each(fila, function(x, llave) {
				var newkey = '';
				if (llave == 'nombre') newkey = 'valor';
				if (llave == 'id_segured') newkey = 'id_interno';
				if (llave == 'id_server') newkey = 'id_gerardo';
				if (llave == 'abrev') newkey = 'abrev';
				if (newkey != '') new_row[newkey] = fila[llave];
			});
			datos.push(new_row);
		});
	} else {
		/** 
		 * Insertamos dummies 
		 */
		/** 
		 * Insertamos dummies 
		 */
		var eliminarModelo13_i = Alloy.Collections.unidad_medida;
		var sql = "DELETE FROM " + eliminarModelo13_i.config.adapter.collection_name;
		var db = Ti.Database.open(eliminarModelo13_i.config.adapter.db_name);
		db.execute(sql);
		db.close();
		eliminarModelo13_i.trigger('remove');
		var item_index = 0;
		_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
			item_index += 1;
			var insertarModelo18_m = Alloy.Collections.unidad_medida;
			var insertarModelo18_fila = Alloy.createModel('unidad_medida', {
				nombre: String.format(L('x2542530260_traducir', 'Metro lineal%1$s'), item.toString()),
				id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
				id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item.toString()),
				pais: 1,
				abrev: String.format(L('x339551322_traducir', 'ML%1$s'), item.toString())
			});
			insertarModelo18_m.add(insertarModelo18_fila);
			insertarModelo18_fila.save();
		});
		/** 
		 * Transformamos nombres de tablas 
		 */
		var transformarModelo22_i = Alloy.createCollection('unidad_medida');
		transformarModelo22_i.fetch();
		var transformarModelo22_src = require('helper').query2array(transformarModelo22_i);
		var datos = [];
		_.each(transformarModelo22_src, function(fila, pos) {
			var new_row = {};
			_.each(fila, function(x, llave) {
				var newkey = '';
				if (llave == 'nombre') newkey = 'valor';
				if (llave == 'id_segured') newkey = 'id_interno';
				if (llave == 'id_server') newkey = 'id_gerardo';
				if (llave == 'abrev') newkey = 'abrev';
				if (newkey != '') new_row[newkey] = fila[llave];
			});
			datos.push(new_row);
		});
	}
	/** 
	 * Cargamos el widget con los datos de tipo de unidad de medida 
	 */
	$.widgetModal10.data({
		data: datos
	});
	if (!_.isUndefined(args._dato)) {
		/** 
		 * Si args._dato existe estamos editando dano 
		 */
		if (Ti.App.deployType != 'production') console.log('hola1', {});
		/** 
		 * Consulta model insp_itemdanos 
		 */
		var consultarModelo41_i = Alloy.createCollection('insp_itemdanos');
		var consultarModelo41_i_where = 'id=\'' + args._dato.id + '\'';
		consultarModelo41_i.fetch({
			query: 'SELECT * FROM insp_itemdanos WHERE id=\'' + args._dato.id + '\''
		});
		var danos = require('helper').query2array(consultarModelo41_i);
		if (danos && danos.length) {
			/** 
			 * obtenemos valor seleccionado previamente y lo mostramos en selector. 
			 */
			if (Ti.App.deployType != 'production') console.log('hola2', {
				"valor": danos
			});
			var consultarModelo42_i = Alloy.createCollection('unidad_medida');
			var consultarModelo42_i_where = 'id_segured=\'' + danos[0].id_unidadmedida + '\'';
			consultarModelo42_i.fetch({
				query: 'SELECT * FROM unidad_medida WHERE id_segured=\'' + danos[0].id_unidadmedida + '\''
			});
			var u_medida = require('helper').query2array(consultarModelo42_i);
			if (u_medida && u_medida.length) {
				var ID_1530463053_func = function() {
					if (Ti.App.deployType != 'production') console.log('hola3', {});
					$.widgetModal10.labels({
						valor: u_medida[0].nombre
					});
					$.label5.setText(u_medida[0].abrev);

				};
				var ID_1530463053 = setTimeout(ID_1530463053_func, 1000 * 0.21);
			}
		}
	}

}

function Abrir_widgetModal10(e) {

	var evento = e;

}

function Respuesta_widgetModal10(e) {

	var evento = e;
	/** 
	 * Mostramos el valor seleccionado desde el widget 
	 */
	$.widgetModal10.labels({
		valor: evento.valor
	});
	/** 
	 * Actualizamos el id de la unidad de medida 
	 */
	$.dano.set({
		id_unidadmedida: evento.item.id_interno
	});
	if ('dano' in $) $dano = $.dano.toJSON();
	/** 
	 * modifica el label con la abreviatura de la unidad de medida seleccionada. 
	 */
	$.label5.setText(evento.item.abrev);

	if (Ti.App.deployType != 'production') console.log('datos recibidos de modal tipo partidas', {
		"datos": evento
	});

}

function Change_campo5(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	/** 
	 * Al escribir la descripcion, vamos actualizando en la tabla de danos la superficie 
	 */
	$.dano.set({
		superficie: elemento
	});
	if ('dano' in $) $dano = $.dano.toJSON();
	elemento = null, source = null;

}

function Change_DescribaelDao(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	$.dano.set({
		descripcion_dano: elemento
	});
	if ('dano' in $) $dano = $.dano.toJSON();
	elemento = null, source = null;

}

(function() {
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
		/** 
		 * Modificamos la idinspeccion con el idserver de la tarea 
		 */
		$.dano.set({
			id_inspeccion: seltarea.id_server
		});
		if ('dano' in $) $dano = $.dano.toJSON();
	}
	if (!_.isUndefined(args._dato)) {
		/** 
		 * Si existe args._dato estamos editando dano 
		 */
		/** 
		 * Modificamos titulo de header si estamos editando 
		 */
		var NUEVO_DAÑO_titulo = 'EDITAR DAÑO';

		var setTitle2 = function(valor) {
			if (OS_ANDROID) {
				abx.title = valor;
			} else {
				$.NUEVO_DAÑO_window.setTitle(valor);
			}
		};
		var getTitle2 = function() {
			if (OS_ANDROID) {
				return abx.title;
			} else {
				return $.NUEVO_DAÑO_window.getTitle();
			}
		};
		setTitle2(NUEVO_DAÑO_titulo);

		/** 
		 * heredamos datos previos de nivel en modelo nuevo con binding de esta pantalla 
		 */
		var consultarModelo43_i = Alloy.createCollection('insp_itemdanos');
		var consultarModelo43_i_where = 'id=\'' + args._dato.id + '\'';
		consultarModelo43_i.fetch({
			query: 'SELECT * FROM insp_itemdanos WHERE id=\'' + args._dato.id + '\''
		});
		var insp_danos = require('helper').query2array(consultarModelo43_i);
		if (Ti.App.deployType != 'production') console.log('ESTE ES EL DAÑO1', {
			"valor": insp_danos[0]
		});
		/** 
		 * Cargamos el modelo con los datos obtenidos desde la consulta 
		 */
		$.dano.set({
			id_inspeccion: insp_danos[0].id_inspeccion,
			nombre: insp_danos[0].nombre,
			superficie: insp_danos[0].superficie,
			foto1: insp_danos[0].foto1,
			id_partida: insp_danos[0].id_partida,
			foto2: insp_danos[0].foto2,
			descripcion_dano: insp_danos[0].descripcion_dano,
			id_recinto: temp_idrecinto,
			foto3: insp_danos[0].foto3,
			id_tipodano: insp_danos[0].id_tipodano,
			id_unidadmedida: insp_danos[0].id_unidadmedida
		});
		if ('dano' in $) $dano = $.dano.toJSON();
		/** 
		 * Cargamos los textos en los campos de texto 
		 */
		$.campo5.setValue(insp_danos[0].superficie);

		$.DescribaelDao.setValue(insp_danos[0].descripcion_dano);

		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			/** 
			 * Revisamos si existe variable seltarea para saber si las fotos son desde la inspeccion o son datos dummy 
			 */
			if (Ti.App.deployType != 'production') console.log('es parte de la compilacion inspeccion completa', {});
			var ID_204536747_trycatch = {
				error: function(e) {
					if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
				}
			};
			try {
				ID_204536747_trycatch.error = function(evento) {};
				var foto1 = '';
				var ID_79340998_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'mini' + insp_danos[0].id_inspeccion);
				if (ID_79340998_d.exists() == true) {
					var ID_79340998_f = Ti.Filesystem.getFile(ID_79340998_d.resolve(), insp_danos[0].foto1);
					if (ID_79340998_f.exists() == true) {
						foto1 = ID_79340998_f.read();
					}
					ID_79340998_f = null;
				}
				ID_79340998_d = null;
			} catch (e) {
				ID_204536747_trycatch.error(e);
			}
			var ID_592855513_trycatch = {
				error: function(e) {
					if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
				}
			};
			try {
				ID_592855513_trycatch.error = function(evento) {};
				var foto2 = '';
				var ID_218718946_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'mini' + insp_danos[0].id_inspeccion);
				if (ID_218718946_d.exists() == true) {
					var ID_218718946_f = Ti.Filesystem.getFile(ID_218718946_d.resolve(), insp_danos[0].foto2);
					if (ID_218718946_f.exists() == true) {
						foto2 = ID_218718946_f.read();
					}
					ID_218718946_f = null;
				}
				ID_218718946_d = null;
			} catch (e) {
				ID_592855513_trycatch.error(e);
			}
			var ID_1128915382_trycatch = {
				error: function(e) {
					if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
				}
			};
			try {
				ID_1128915382_trycatch.error = function(evento) {};
				var foto3 = '';
				var ID_1844717500_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'mini' + insp_danos[0].id_inspeccion);
				if (ID_1844717500_d.exists() == true) {
					var ID_1844717500_f = Ti.Filesystem.getFile(ID_1844717500_d.resolve(), insp_danos[0].foto3);
					if (ID_1844717500_f.exists() == true) {
						foto3 = ID_1844717500_f.read();
					}
					ID_1844717500_f = null;
				}
				ID_1844717500_d = null;
			} catch (e) {
				ID_1128915382_trycatch.error(e);
			}
		} else {
			if (Ti.App.deployType != 'production') console.log('esta compilacion esta sola, simulamos carpetas', {});
			var ID_496864751_trycatch = {
				error: function(e) {
					if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
				}
			};
			try {
				ID_496864751_trycatch.error = function(evento) {};
				var foto1 = '';
				var ID_1412673214_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'mini' + 0);
				if (ID_1412673214_d.exists() == true) {
					var ID_1412673214_f = Ti.Filesystem.getFile(ID_1412673214_d.resolve(), insp_danos[0].foto1);
					if (ID_1412673214_f.exists() == true) {
						foto1 = ID_1412673214_f.read();
					}
					ID_1412673214_f = null;
				}
				ID_1412673214_d = null;
			} catch (e) {
				ID_496864751_trycatch.error(e);
			}
			var ID_1979336114_trycatch = {
				error: function(e) {
					if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
				}
			};
			try {
				ID_1979336114_trycatch.error = function(evento) {};
				var foto2 = '';
				var ID_771669433_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'mini' + 0);
				if (ID_771669433_d.exists() == true) {
					var ID_771669433_f = Ti.Filesystem.getFile(ID_771669433_d.resolve(), insp_danos[0].foto2);
					if (ID_771669433_f.exists() == true) {
						foto2 = ID_771669433_f.read();
					}
					ID_771669433_f = null;
				}
				ID_771669433_d = null;
			} catch (e) {
				ID_1979336114_trycatch.error(e);
			}
			var ID_1042713336_trycatch = {
				error: function(e) {
					if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
				}
			};
			try {
				ID_1042713336_trycatch.error = function(evento) {};
				var foto3 = '';
				var ID_998439173_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'mini' + 0);
				if (ID_998439173_d.exists() == true) {
					var ID_998439173_f = Ti.Filesystem.getFile(ID_998439173_d.resolve(), insp_danos[0].foto3);
					if (ID_998439173_f.exists() == true) {
						foto3 = ID_998439173_f.read();
					}
					ID_998439173_f = null;
				}
				ID_998439173_d = null;
			} catch (e) {
				ID_1042713336_trycatch.error(e);
			}
		}
		if ((_.isObject(foto1) || (_.isString(foto1)) && !_.isEmpty(foto1)) || _.isNumber(foto1) || _.isBoolean(foto1)) {
			/** 
			 * Revisamos que foto1, foto2 y foto3 contengan texto, por lo que si es asi, modificamos la imagen previa y ponemos la foto miniatura obtenida desde la memoria 
			 */
			if (Ti.App.deployType != 'production') console.log('llamamos evento mini', {});
			$.widgetFotochica4.mini({
				mini: foto1
			});
		}
		if ((_.isObject(foto2) || (_.isString(foto2)) && !_.isEmpty(foto2)) || _.isNumber(foto2) || _.isBoolean(foto2)) {
			if (Ti.App.deployType != 'production') console.log('llamamos evento mini', {});
			$.widgetFotochica5.mini({
				mini: foto2
			});
		}
		if ((_.isObject(foto3) || (_.isString(foto3)) && !_.isEmpty(foto3)) || _.isNumber(foto3) || _.isBoolean(foto3)) {
			if (Ti.App.deployType != 'production') console.log('llamamos evento mini', {});
			$.widgetFotochica6.mini({
				mini: foto3
			});
		}
	}
	/** 
	 * Recuperamos la variable del id_recinto que tiene danos relacionados y actualizamos el modelo 
	 */
	var temp_idrecinto = ('temp_idrecinto' in require('vars')) ? require('vars')['temp_idrecinto'] : '';
	if ((_.isObject(temp_idrecinto) || (_.isString(temp_idrecinto)) && !_.isEmpty(temp_idrecinto)) || _.isNumber(temp_idrecinto) || _.isBoolean(temp_idrecinto)) {
		$.dano.set({
			id_recinto: temp_idrecinto
		});
		if ('dano' in $) $dano = $.dano.toJSON();
	}
})();

if (OS_IOS || OS_ANDROID) {
	$.NUEVO_DAÑO.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}