var _bind4section = {};
var _list_templates = {};
var $inspector = $.inspector.toJSON();

var _activity;
if (OS_ANDROID) {
	_activity = $.EDITAR.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'EDITAR';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.EDITAR_window.addEventListener('open', function(e) {
		abx.setStatusbarColor("#000000");
		abx.setBackgroundColor("white");
	});
}


var consultarModelo_like = function(search) {
	if (typeof search !== 'string' || this === null) {
		return false;
	}
	search = search.replace(new RegExp("([\\.\\\\\\+\\*\\?\\[\\^\\]\\$\\(\\)\\{\\}\\=\\!\\<\\>\\|\\:\\-])", "g"), "\\$1");
	search = search.replace(/%/g, '.*').replace(/_/g, '.');
	return RegExp('^' + search + '$', 'gi').test(this);
};
var consultarModelo_filter = function(coll) {
	var filtered = _.toArray(coll.filter(function(m) {
		return true;
	}));
	return filtered;
};
var consultarModelo_transform = function(model) {
	var fila = model.toJSON();
	return fila;
};
var consultarModelo_update = function(e) {};
_.defer(function() {
	Alloy.Collections.pais.fetch();
});
Alloy.Collections.pais.on('add change delete', function(ee) {
	consultarModelo_update(ee);
});
Alloy.Collections.pais.fetch();

function Click_vista(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.EDITAR.close();

}

function Click_vista2(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var pais_seleccionado = ('pais_seleccionado' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['pais_seleccionado'] : '';
	if (Ti.App.deployType != 'production') console.log('el pais seleccionado es', {
		"datos": pais_seleccionado
	});
	if ((_.isObject(pais_seleccionado) || _.isString(pais_seleccionado)) && _.isEmpty(pais_seleccionado)) {
		/** 
		 * Avisamos al usuario en el caso de que no haya seleccionado un pais, caso contrario actualizamos la tabla con el pais seleccionado y continuamos a la proxima pantalla 
		 */
		var preguntarAlerta_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x3970786515_traducir', 'Debe seleccionar un pais'),
			buttonNames: preguntarAlerta_opts
		});
		preguntarAlerta.addEventListener('click', function(e) {
			var abc = preguntarAlerta_opts[e.index];
			abc = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta.show();
	} else {
		$.inspector.set({
			pais: pais_seleccionado.id_server
		});
		if ('inspector' in $) $inspector = $.inspector.toJSON();
		if (Ti.App.deployType != 'production') console.log('el pais a guardar es', {
			"datos": pais_seleccionado.id_server
		});
		Alloy.Collections[$.inspector.config.adapter.collection_name].add($.inspector);
		$.inspector.save();
		Alloy.Collections[$.inspector.config.adapter.collection_name].fetch();
		/** 
		 * Creamos un flag para evitar que la pantalla se abra dos veces 
		 */
		var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
		if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
			require('vars')['var_abriendo'] = L('x4261170317', 'true');
			if ("EDITAR" in Alloy.Globals) {
				Alloy.Globals["EDITAR"].openWindow(Alloy.createController("editar_domicilio", {}).getView());
			} else {
				Alloy.Globals["EDITAR"] = $.EDITAR;
				Alloy.Globals["EDITAR"].openWindow(Alloy.createController("editar_domicilio", {}).getView());
			}
			nulo = null;
		}
	}
}

$.widgetHeader.init({
	titulo: L('x3904602325_traducir', 'Editar país'),
	__id: 'ALL673432332',
	avance: '',
	onclick: Click_widgetHeader
});

function Click_widgetHeader(e) {

	var evento = e;

}


function Change_picker(e) {

	e.cancelBubble = true;
	var elemento = e;
	var _columna = e.columnIndex;
	var columna = e.columnIndex + 1;
	var _fila = e.rowIndex;
	var fila = e.rowIndex + 1;
	var modelo = require('helper').query2array(Alloy.Collections.pais)[e.rowIndex];
	_.defer(function(modelo) {
		/** 
		 * Guardamos el pais seleccionado en el picker 
		 */
		require('vars')[_var_scopekey]['pais_seleccionado'] = modelo;
	}, modelo);

}

(function() {
	if (Ti.App.deployType != 'production') console.log('estamos en domicilio', {});
	_my_events['_close_editar,ID_518578093'] = function(evento) {
		if (Ti.App.deployType != 'production') console.log('estamos en el _close_editar', {});
		$.EDITAR.close();
	};
	Alloy.Events.on('_close_editar', _my_events['_close_editar,ID_518578093']);
	var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
	var consultarURL = {};
	consultarURL.success = function(e) {
		var elemento = e,
			valor = e;
		if (elemento == false || elemento == 'false') {
			/** 
			 * Si en la consulta los datos obtenidos no existen, mostramos mensaje para decir que hubo problemas al obtener los datos. Caso contrario, limpiamos tablas y cargamos los datos obtenidos desde el servidor en las tablas 
			 */
			var preguntarAlerta2_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta2 = Ti.UI.createAlertDialog({
				title: L('x57652245_traducir', 'Alerta'),
				message: L('x1693828390_traducir', 'Problema de conexión por favor intentar después'),
				buttonNames: preguntarAlerta2_opts
			});
			preguntarAlerta2.addEventListener('click', function(e) {
				var suu = preguntarAlerta2_opts[e.index];
				suu = null;
				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta2.show();
		} else {
			if (Ti.App.deployType != 'production') console.log('cargando datos', {
				"datos": elemento
			});
			var eliminarModelo_i = Alloy.Collections.pais;
			var sql = "DELETE FROM " + eliminarModelo_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo_i.trigger('remove');
			var eliminarModelo2_i = Alloy.Collections.nivel1;
			var sql = "DELETE FROM " + eliminarModelo2_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo2_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo2_i.trigger('remove');
			var eliminarModelo3_i = Alloy.Collections.experiencia_oficio;
			var sql = "DELETE FROM " + eliminarModelo3_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo3_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo3_i.trigger('remove');
			var elemento_regiones = elemento.regiones;
			var insertarModelo_m = Alloy.Collections.nivel1;
			var db_insertarModelo = Ti.Database.open(insertarModelo_m.config.adapter.db_name);
			db_insertarModelo.execute('BEGIN');
			_.each(elemento_regiones, function(insertarModelo_fila, pos) {
				db_insertarModelo.execute('INSERT INTO nivel1 (id_label, id_server, dif_horaria, id_pais) VALUES (?,?,?,?)', insertarModelo_fila.subdivision_name, insertarModelo_fila.id, 2, insertarModelo_fila.id_pais);
			});
			db_insertarModelo.execute('COMMIT');
			db_insertarModelo.close();
			db_insertarModelo = null;
			insertarModelo_m.trigger('change');
			var elemento_paises = elemento.paises;
			var insertarModelo2_m = Alloy.Collections.pais;
			var db_insertarModelo2 = Ti.Database.open(insertarModelo2_m.config.adapter.db_name);
			db_insertarModelo2.execute('BEGIN');
			_.each(elemento_paises, function(insertarModelo2_fila, pos) {
				db_insertarModelo2.execute('INSERT INTO pais (label_nivel2, moneda, nombre, label_nivel4, label_codigo_identificador, label_nivel3, id_server, label_nivel1, iso, sis_metrico, niveles_pais, id_pais, label_nivel5, lenguaje) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)', insertarModelo2_fila.nivel_2, insertarModelo2_fila.moneda, insertarModelo2_fila.nombre, insertarModelo2_fila.nivel_4, insertarModelo2_fila.label_codigo_identificador, insertarModelo2_fila.nivel_3, insertarModelo2_fila.id, insertarModelo2_fila.nivel_1, insertarModelo2_fila.iso, insertarModelo2_fila.sis_metrico, insertarModelo2_fila.niveles_pais, insertarModelo2_fila.idpais, insertarModelo2_fila.nivel_5, insertarModelo2_fila.lenguaje);
			});
			db_insertarModelo2.execute('COMMIT');
			db_insertarModelo2.close();
			db_insertarModelo2 = null;
			insertarModelo2_m.trigger('change');
			var elemento_experiencia_oficio = elemento.experiencia_oficio;
			var insertarModelo3_m = Alloy.Collections.experiencia_oficio;
			var db_insertarModelo3 = Ti.Database.open(insertarModelo3_m.config.adapter.db_name);
			db_insertarModelo3.execute('BEGIN');
			_.each(elemento_experiencia_oficio, function(insertarModelo3_fila, pos) {
				db_insertarModelo3.execute('INSERT INTO experiencia_oficio (nombre, id_server, id_pais) VALUES (?,?,?)', insertarModelo3_fila.nombre, insertarModelo3_fila.id, insertarModelo3_fila.idpais);
			});
			db_insertarModelo3.execute('COMMIT');
			db_insertarModelo3.close();
			db_insertarModelo3 = null;
			insertarModelo3_m.trigger('change');
		}
		if (Ti.App.deployType != 'production') console.log('trajo datos de obtenerpais', {});
		elemento = null, valor = null;
	};
	consultarURL.error = function(e) {
		var elemento = e,
			valor = e;
		var preguntarAlerta3_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta3 = Ti.UI.createAlertDialog({
			title: L('x57652245_traducir', 'Alerta'),
			message: L('x1693828390_traducir', 'Problema de conexión por favor intentar después'),
			buttonNames: preguntarAlerta3_opts
		});
		preguntarAlerta3.addEventListener('click', function(e) {
			var suu = preguntarAlerta3_opts[e.index];
			suu = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta3.show();
		elemento = null, valor = null;
	};
	require('helper').ajaxUnico('consultarURL', '' + String.format(L('x2963862531', '%1$sobtenerPais'), url_server.toString()) + '', 'POST', {}, 15000, consultarURL);
	/** 
	 * heredamos datos previos de nivel en modelo nuevo con binding de esta pantalla 
	 */
	var consultarModelo2_i = Alloy.createCollection('inspectores');
	var consultarModelo2_i_where = '';
	consultarModelo2_i.fetch();
	var inspector_viejo = require('helper').query2array(consultarModelo2_i);
	if (inspector_viejo && inspector_viejo.length) {
		/** 
		 * Guardamos variable con el dato previo del pais del inspector y cargamos los datos en binding 
		 */
		require('vars')[_var_scopekey]['pais_seleccionado'] = inspector_viejo[0].pais;
		$.inspector.set({
			apellido_materno: inspector_viejo[0].apellido_materno,
			id_nivel1: inspector_viejo[0].id_nivel1,
			lat_dir: inspector_viejo[0].lat_dir,
			disponibilidad_viajar_pais: inspector_viejo[0].disponibilidad_viajar_pais,
			uuid: inspector_viejo[0].uidd,
			fecha_nacimiento: inspector_viejo[0].fecha_nacimiento,
			d1: inspector_viejo[0].d1,
			d2: inspector_viejo[0].d2,
			password: inspector_viejo[0].password,
			pais: inspector_viejo[0].pais,
			direccion: inspector_viejo[0].direccion,
			d3: inspector_viejo[0].d3,
			nivel3: inspector_viejo[0].nivel3,
			d5: inspector_viejo[0].d5,
			d4: inspector_viejo[0].d4,
			disponibilidad_fechas: inspector_viejo[0].disponibilidad_fechas,
			d7: inspector_viejo[0].d7,
			nivel4: inspector_viejo[0].nivel4,
			nombre: inspector_viejo[0].nombre,
			nivel5: inspector_viejo[0].nivel5,
			nivel2: inspector_viejo[0].nivel2,
			disponibilidad_horas: inspector_viejo[0].disponibilidad_horas,
			disponibilidad_viajar_ciudad: inspector_viejo[0].disponibilidad_viajar_ciudad,
			lon_dir: inspector_viejo[0].lon_dir,
			id_server: inspector_viejo[0].id_server,
			d6: inspector_viejo[0].d6,
			telefono: inspector_viejo[0].telefono,
			experiencia_detalle: inspector_viejo[0].experiencia_detalle,
			disponibilidad: inspector_viejo[0].disponibilidad,
			codigo_identificador: inspector_viejo[0].codigo_identificador,
			experiencia_oficio: inspector_viejo[0].experiencia_oficio,
			direccion_correccion: inspector_viejo[0].direccion_correccion,
			apellido_paterno: inspector_viejo[0].apellido_paterno,
			correo: inspector_viejo[0].correo
		});
		if ('inspector' in $) $inspector = $.inspector.toJSON();
	} else {
		/** 
		 * Si no existe inspector (en el caso de que esten haciendo pruebas locales) cargamos datos dummy 
		 */
		$.inspector.set({
			apellido_materno: 'Huerta',
			id_nivel1: 21,
			lat_dir: -123123,
			disponibilidad_viajar_pais: 0,
			uuid: 'uuid-telefono-pruebas',
			fecha_nacimiento: '09-05-1994',
			d1: 1,
			d2: 1,
			password: 'test',
			pais: 1,
			direccion: 'Sta Isabel 951',
			d3: 1,
			nivel3: 'Santiago',
			d5: 1,
			d4: 1,
			disponibilidad_fechas: 1,
			d7: 1,
			nivel4: 4,
			nombre: 'Elias',
			nivel5: 5,
			nivel2: 'Santiago',
			disponibilidad_horas: 0,
			disponibilidad_viajar_ciudad: 1,
			lon_dir: -123123,
			id_server: 123,
			d6: 1,
			telefono: 950093248,
			experiencia_detalle: 'Sin experiencia',
			disponibilidad: 1,
			codigo_identificador: '18.373.278-1',
			experiencia_oficio: 1,
			direccion_correccion: 1,
			apellido_paterno: 'Baeza',
			correo: 'elias@creador.cl'
		});
		if ('inspector' in $) $inspector = $.inspector.toJSON();
	}
})();

function Postlayout_EDITAR(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var ID_1294585966_func = function() {
		/** 
		 * Una vez ya cargada la tarea, dejamos la variable limpia para poder abrir otras pantallas 
		 */
		require('vars')['var_abriendo'] = '';
	};
	var ID_1294585966 = setTimeout(ID_1294585966_func, 1000 * 0.2);

}
if (OS_IOS || OS_ANDROID) {
	$.EDITAR.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}
//$.EDITAR.open();
