var _bind4section = {};
var _list_templates = {};

var _activity;
if (OS_ANDROID) {
	_activity = $.ENROLAMIENTO7.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'ENROLAMIENTO7';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.ENROLAMIENTO7.addEventListener('open', function(e) {
		abx.setStatusbarColor("#000000");
		abx.setBackgroundColor("white");
	});
}
$.ENROLAMIENTO7.orientationModes = [Titanium.UI.PORTRAIT];

function Click_vista54(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.ENROLAMIENTO7.close();

}

$.widgetHeader7.init({
	titulo: L('x4235921061_traducir', 'PARTE 6: Experiencia de trabajo'),
	__id: 'ALL463934795',
	avance: L('x33680836_traducir', '6/6'),
	onclick: Click_widgetHeader7
});

function Click_widgetHeader7(e) {

	var evento = e;
	$.MximoCaracteres.blur();

}

$.widgetModal2.init({
	titulo: L('x4266405801_traducir', 'Profesion'),
	__id: 'ALL238268657',
	onrespuesta: Respuesta_widgetModal2,
	campo: L('x419737788_traducir', 'Oficio / Profesion'),
	onabrir: Abrir_widgetModal2,
	guardar: false,
	seleccione: L('x1243610977_traducir', 'Seleccione su oficio/profesión'),
	onafterinit: Afterinit_widgetModal2
});

function Afterinit_widgetModal2(e) {

	var evento = e;
	/** 
	 * Recuperamos las variables con los datos de oficio y profesion. Despues lo cargamos en el widget 
	 */
	var data_oficios = ('data_oficios' in require('vars')) ? require('vars')['data_oficios'] : '';

	$.widgetModal2.data({
		data: data_oficios
	});

}

function Abrir_widgetModal2(e) {

	var evento = e;
	$.MximoCaracteres.blur();

}

function Respuesta_widgetModal2(e) {

	var evento = e;
	if (Ti.App.deployType != 'production') console.log('datos recibidos de modal oficio', {
		"datos": evento
	});
	/** 
	 * Mostramos el valor seleccionado en la pantalla y guardamos el valor en una variable 
	 */

	$.widgetModal2.labels({
		valor: evento.valor
	});
	require('vars')['data_oficio'] = evento.item;

}

function Change_MximoCaracteres(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	$.DetallarExperiencia.setText(String.format(L('x1264081656_traducir', 'Detallar experiencia (Quedan %1$s caracteres)'), (140 - elemento.length).toString()));

	elemento = null, source = null;

}

function Return_MximoCaracteres(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	$.MximoCaracteres.blur();
	elemento = null, source = null;

}

$.widgetMono.init({
	titulo: L('x3969906465_traducir', 'ESPERA TU LLAMADA'),
	__id: 'ALL1300949542',
	texto: L('x1275032235_traducir', 'Recuerda llevar a la entrevista tu CV, tu certificado de título y referencias'),
	alto: '-',
	top: 25,
	ancho: '90%',
	tipo: '_tip'
});


$.widgetBotonlargo7.init({
	titulo: L('x4058536315_traducir', 'ENVIAR FORMULARIO'),
	__id: 'ALL296073555',
	color: 'verde',
	onclick: Click_widgetBotonlargo7
});

function Click_widgetBotonlargo7(e) {

	var evento = e;
	/** 
	 * Recuperamos variables y valores de los campos de texto y almacenamos en la variable de registro 
	 */
	var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
	var data_oficio = ('data_oficio' in require('vars')) ? require('vars')['data_oficio'] : '';
	var experiencia;
	experiencia = $.MximoCaracteres.getValue();

	var registro = _.extend(registro, {
		oficio: data_oficio.id_server,
		experiencia: experiencia
	});
	require('vars')['registro'] = registro;
	if (data_oficio.id_server == 0 || data_oficio.id_server == '0') {
		/** 
		 * Validamos que se haya escogido un oficio o profesion, y que la experiencia sea mayor a 30 caracteres 
		 */
		var preguntarAlerta22_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta22 = Ti.UI.createAlertDialog({
			title: L('x2185084353_traducir', 'Atención'),
			message: L('x606194025_traducir', 'Seleccione oficio o profesión'),
			buttonNames: preguntarAlerta22_opts
		});
		preguntarAlerta22.addEventListener('click', function(e) {
			var suu = preguntarAlerta22_opts[e.index];
			suu = null;

			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta22.show();
	} else if (_.isNumber((experiencia.length)) && _.isNumber(30) && (experiencia.length) < 30) {
		var preguntarAlerta23_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta23 = Ti.UI.createAlertDialog({
			title: L('x2185084353_traducir', 'Atención'),
			message: L('x1776988474_traducir', 'Describa su experiencia laboral brevemente, mínimo 30 caracteres'),
			buttonNames: preguntarAlerta23_opts
		});
		preguntarAlerta23.addEventListener('click', function(e) {
			var suu = preguntarAlerta23_opts[e.index];
			suu = null;

			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta23.show();
	} else {

		$.widgetBotonlargo7.iniciar_progreso({});
		var fecha_resp = null;
		if ('formatear_fecha' in require('funciones')) {
			fecha_resp = require('funciones').formatear_fecha({
				'fecha': registro.fecha_nacimiento,
				'formato': L('x591439515_traducir', 'YYYY-MM-DD')
			});
		} else {
			try {
				fecha_resp = f_formatear_fecha({
					'fecha': registro.fecha_nacimiento,
					'formato': L('x591439515_traducir', 'YYYY-MM-DD')
				});
			} catch (ee) {}
		}
		var elid = Ti.Platform.id;
		var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
		var consultarURL2 = {};

		consultarURL2.success = function(e) {
			var elemento = e,
				valor = e;

			$.widgetBotonlargo7.detener_progreso({});
			if (Ti.App.deployType != 'production') console.log('respuesta de servidor por enrolamiento', {
				"elemento": elemento
			});
			if (elemento.error == 0 || elemento.error == '0') {
				/** 
				 * En caso de que el registro sea exitoso, mostramos mensaje de exito y nos regresamos al login. Caso contrario, mostramos mensajes de error. 
				 */

				Alloy.Events.trigger('_close_enrolamiento');
				var preguntarAlerta24_opts = [L('x1518866076_traducir', 'Aceptar')];
				var preguntarAlerta24 = Ti.UI.createAlertDialog({
					title: L('x57652245_traducir', 'Alerta'),
					message: L('x4087789422_traducir', 'Registro exitoso'),
					buttonNames: preguntarAlerta24_opts
				});
				preguntarAlerta24.addEventListener('click', function(e) {
					var suu = preguntarAlerta24_opts[e.index];
					suu = null;

					e.source.removeEventListener("click", arguments.callee);
				});
				preguntarAlerta24.show();
				$.ENROLAMIENTO7.close();
			} else if (elemento.error == 1040) {

				Alloy.Events.trigger('_close_enrolamiento');
				var preguntarAlerta25_opts = [L('x1518866076_traducir', 'Aceptar')];
				var preguntarAlerta25 = Ti.UI.createAlertDialog({
					title: L('x2619118453_traducir', 'Error'),
					message: L('x538232794_traducir', 'El rut indicado ya estaba registrado'),
					buttonNames: preguntarAlerta25_opts
				});
				preguntarAlerta25.addEventListener('click', function(e) {
					var suu = preguntarAlerta25_opts[e.index];
					suu = null;

					e.source.removeEventListener("click", arguments.callee);
				});
				preguntarAlerta25.show();
				$.ENROLAMIENTO7.close();
			} else if (elemento.error == 1020) {

				Alloy.Events.trigger('_close_enrolamiento');
				var preguntarAlerta26_opts = [L('x1518866076_traducir', 'Aceptar')];
				var preguntarAlerta26 = Ti.UI.createAlertDialog({
					title: L('x2619118453_traducir', 'Error'),
					message: L('x3188313389_traducir', 'El correo indicado ya estaba registrado'),
					buttonNames: preguntarAlerta26_opts
				});
				preguntarAlerta26.addEventListener('click', function(e) {
					var suu = preguntarAlerta26_opts[e.index];
					suu = null;

					e.source.removeEventListener("click", arguments.callee);
				});
				preguntarAlerta26.show();
				$.ENROLAMIENTO7.close();
			} else {
				var preguntarAlerta27_opts = [L('x1518866076_traducir', 'Aceptar')];
				var preguntarAlerta27 = Ti.UI.createAlertDialog({
					title: L('x57652245_traducir', 'Alerta'),
					message: L('x3532386985_traducir', 'Hubo problemas en el servidor. Intente de nuevo más tarde'),
					buttonNames: preguntarAlerta27_opts
				});
				preguntarAlerta27.addEventListener('click', function(e) {
					var suu = preguntarAlerta27_opts[e.index];
					suu = null;

					e.source.removeEventListener("click", arguments.callee);
				});
				preguntarAlerta27.show();
			}
			elemento = null, valor = null;
		};

		consultarURL2.error = function(e) {
			var elemento = e,
				valor = e;

			$.widgetBotonlargo7.detener_progreso({});
			var preguntarAlerta28_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta28 = Ti.UI.createAlertDialog({
				title: L('x57652245_traducir', 'Alerta'),
				message: L('x2467152901_traducir', 'Hubo problemas en el registro. Intente de nuevo más tarde'),
				buttonNames: preguntarAlerta28_opts
			});
			preguntarAlerta28.addEventListener('click', function(e) {
				var suu = preguntarAlerta28_opts[e.index];
				suu = null;

				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta28.show();
			elemento = null, valor = null;
		};
		require('helper').ajaxUnico('consultarURL2', '' + String.format(L('x351097317', '%1$sregistrarInspector'), url_server.toString()) + '', 'POST', {
			pais: registro.pais,
			nombre: registro.nombre,
			apellido_paterno: registro.apellido_paterno,
			apellido_materno: registro.apellido_materno,
			codigo_identificador: registro.codigo_identificador,
			fecha_nacimiento: fecha_resp,
			nivel_1: registro.nivel_1,
			nivel_2: registro.nivel_2,
			nivel_3: registro.nivel_3,
			nivel_4: registro.nivel_4,
			nivel_5: registro.nivel_5,
			direccion: registro.direccion,
			disponibilidad: registro.disponibilidad,
			disp_viajar_ciudad: registro.disp_viajar_ciudad,
			disp_viajar_pais: registro.disp_viajar_pais,
			disp_horas: registro.disp_horas,
			correo: registro.correo,
			oficio: registro.oficio,
			experiencia: registro.experiencia,
			device: Titanium.Platform.name,
			uidd: Titanium.Platform.id,
			telefono: registro.telefono,
			correccion_direccion: registro.correccion_direccion,
			latitude: registro.latitude,
			longitude: registro.longitude,
			d1: registro.d1,
			d2: registro.d2,
			d3: registro.d3,
			d4: registro.d4,
			d5: registro.d5,
			d6: registro.d6,
			d7: registro.d7,
			google: registro.google
		}, 15000, consultarURL2);
	}
}

(function() {
	/** 
	 * Recuperamos la variable del registro, agregamos el id_server 
	 */
	var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
	var data_oficio = {
		id_server: 0
	};
	require('vars')['data_oficio'] = data_oficio;
	/** 
	 * Consultamos la tabla de experiencia y oficios y la guardamos en una variable global 
	 */
	var consultarModelo6_i = Alloy.createCollection('experiencia_oficio');
	var consultarModelo6_i_where = 'id_pais=\'' + registro.pais + '\'';
	consultarModelo6_i.fetch({
		query: 'SELECT * FROM experiencia_oficio WHERE id_pais=\'' + registro.pais + '\''
	});
	var oficio_list = require('helper').query2array(consultarModelo6_i);
	var item_index = 0;
	_.each(oficio_list, function(item, item_pos, item_list) {
		item_index += 1;
		item.valor = item.nombre;
	});
	require('vars')['data_oficios'] = oficio_list;
})();

if (OS_IOS || OS_ANDROID) {
	$.ENROLAMIENTO7.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}