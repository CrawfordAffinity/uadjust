var _bind4section = {};
var _list_templates = {
	"contenido": {
		"Label2": {
			"text": "{id}"
		},
		"vista4": {},
		"Label": {
			"text": "{nombre}"
		}
	},
	"recinto": {
		"Label2": {
			"text": "{id}"
		},
		"vista4": {},
		"Label": {
			"text": "{nombre}"
		}
	},
	"nivel": {
		"Label2": {
			"text": "{id}"
		},
		"vista20": {},
		"Label": {
			"text": "{nombre}"
		}
	}
};

$.HAY_ALGUIEN_window.setTitleAttributes({
	color: 'WHITE'
});
var _activity;
if (OS_ANDROID) {
	_activity = $.HAY_ALGUIEN.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'HAY_ALGUIEN';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.HAY_ALGUIEN_window.addEventListener('open', function(e) {
		abx.setStatusbarColor("#FFFFFF");
		abx.setBackgroundColor("#2d9edb");
	});
}


$.widgetPregunta.init({
	titulo: L('x1951816285_traducir', '¿PUEDE CONTINUAR CON LA INSPECCION?'),
	__id: 'ALL581285551',
	si: L('x369557195_traducir', 'SI puedo continuar'),
	texto: L('x2522333127_traducir', 'Si está el asegurado en el domicilio presione SI para continuar'),
	onno: no_widgetPregunta,
	onsi: si_widgetPregunta,
	no: L('x3892244486_traducir', 'NO se pudo realizar la inspección'),
	top: 25
});

function si_widgetPregunta(e) {

	var evento = e;
	if (Ti.App.deployType != 'production') console.log('presiono si', {});
	Alloy.createController("datosbasicos_index", {}).getView().open();

}

function no_widgetPregunta(e) {

	var evento = e;
	if (Ti.App.deployType != 'production') console.log('presiono NO', {});
	preguntarOpciones.show();

}

function Postlayout_vista2(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.progreso.show();

}

(function() {
	/** 
	 * Llamamos servicio iniciarInspeccion aqui, no hacemos nada con respuesta 
	 */
	var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	/** 
	 * api de iniciar tarea: solo si estamos en compilacion full 
	 */
	var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
	/** 
	 * Esto es para indicar en firma ult pantalla a cerrar en cancelada 
	 */
	require('vars')['insp_cancelada'] = '';
	if ((_.isObject(url_server) || (_.isString(url_server)) && !_.isEmpty(url_server)) || _.isNumber(url_server) || _.isBoolean(url_server)) {
		/** 
		 * Revisamos si la variable url_server contiene datos para poder avisar al servidor que iniciaremos la inspeccion 
		 */
		var consultarURL = {};
		console.log('DEBUG WEB: requesting url:' + String.format(L('x1627917957', '%1$siniciarTarea'), url_server.toString()) + ' with data:', {
			_method: 'POST',
			_params: {
				id_inspector: inspector.id_server,
				codigo_identificador: inspector.codigo_identificador,
				id_tarea: seltarea.id_server,
				num_caso: seltarea.num_caso
			},
			_timeout: '15000'
		});

		consultarURL.success = function(e) {
			var elemento = e,
				valor = e;
			/** 
			 * Inicializamos inspeccion 
			 */
			require('vars')['inspeccion_encurso'] = L('x4261170317', 'true');
			var moment = require('alloy/moment');
			var hora = moment(new Date()).format('HH:mm');
			var moment = require('alloy/moment');
			var fecha = moment(new Date()).format('DD-MM-YYYY');
			var insertarModelo_m = Alloy.Collections.inspecciones;
			var insertarModelo_fila = Alloy.createModel('inspecciones', {
				hora: hora,
				fecha: fecha,
				id_server: seltarea.id_server,
				fecha_inspeccion_inicio: new Date()
			});
			insertarModelo_m.add(insertarModelo_fila);
			insertarModelo_fila.save();
			elemento = null, valor = null;
		};

		consultarURL.error = function(e) {
			var elemento = e,
				valor = e;
			elemento = null, valor = null;
		};
		require('helper').ajaxUnico('consultarURL', '' + String.format(L('x1627917957', '%1$siniciarTarea'), url_server.toString()) + '', 'POST', {
			id_inspector: inspector.id_server,
			codigo_identificador: inspector.codigo_identificador,
			id_tarea: seltarea.id_server,
			num_caso: seltarea.num_caso
		}, 15000, consultarURL);
	}
	/** 
	 * Desactivamos seguimiento de tarea 
	 */
	require('vars')['seguir_tarea'] = '';
})();


var preguntarOpciones_opts = [L('x3481285698_traducir', 'Asegurado no puede seguir'), L('x3077268071_traducir', ' Se me acabo la bateria'), L('x479989047_traducir', ' Tuve un accidente'), L('x2376009830_traducir', ' Cancelar')];
var preguntarOpciones = Ti.UI.createOptionDialog({
	title: L('x1670176792_traducir', 'RAZON PARA CANCELAR INSPECCION ACTUAL'),
	options: preguntarOpciones_opts
});
preguntarOpciones.addEventListener('click', function(e) {
	var resp = preguntarOpciones_opts[e.index];
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
	var razon = "";
	if (resp == L('x3481285698_traducir', 'Asegurado no puede seguir')) {
		/** 
		 * Esto parece redundante, pero es para escapar de i18n el texto 
		 */
		razon = "Asegurado no puede seguir";
	}
	if (resp == L('x1862794075_traducir', 'Se me acabo la bateria')) {
		razon = "Se me acabo la bateria";
	}
	if (resp == L('x4012805111_traducir', 'Tuve un accidente')) {
		razon = "Tuve un accidente";
	}
	if ((_.isObject(razon) || (_.isString(razon)) && !_.isEmpty(razon)) || _.isNumber(razon) || _.isBoolean(razon)) {
		require('vars')['insp_cancelada'] = L('x155149119_traducir', 'hayalguien');
		if (Ti.App.deployType != 'production') console.log('llamando servicio cancelarTarea', {});
		var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
		/** 
		 * Mostramos popup avisando que se esta cancelando la inspeccion 
		 */
		var vista2_visible = true;

		if (vista2_visible == 'si') {
			vista2_visible = true;
		} else if (vista2_visible == 'no') {
			vista2_visible = false;
		}
		$.vista2.setVisible(vista2_visible);

		var consultarURL2 = {};

		consultarURL2.success = function(e) {
			var elemento = e,
				valor = e;
			if (Ti.App.deployType != 'production') console.log('Mi resultado es', {
				"elemento": elemento
			});
			var vista2_visible = false;

			if (vista2_visible == 'si') {
				vista2_visible = true;
			} else if (vista2_visible == 'no') {
				vista2_visible = false;
			}
			$.vista2.setVisible(vista2_visible);

			/** 
			 * Enviamos a firma 
			 */
			Alloy.createController("firma_index", {}).getView().open();
			elemento = null, valor = null;
		};

		consultarURL2.error = function(e) {
			var elemento = e,
				valor = e;
			if (Ti.App.deployType != 'production') console.log('hubo error llamando cancelarTarea, la agregamos a cola de salida', {
				"elemento": elemento
			});
			if (Ti.App.deployType != 'production') console.log('agregando servicio cancelarTarea a cola', {});
			/** 
			 * En el caso que haya habido un problema llamando al servicio cancelarTarea, guardamos los datos en un objeto 
			 */
			var datos = {
				id_inspector: seltarea.id_inspector,
				codigo_identificador: inspector.codigo_identificador,
				id_server: seltarea.id_server,
				num_caso: seltarea.num_caso,
				razon: razon
			};
			var insertarModelo2_m = Alloy.Collections.cola;
			var insertarModelo2_fila = Alloy.createModel('cola', {
				data: JSON.stringify(datos),
				id_tarea: seltarea.id_server,
				tipo: 'cancelar'
			});
			insertarModelo2_m.add(insertarModelo2_fila);
			insertarModelo2_fila.save();
			/** 
			 * Ocultamos popup avisando que se esta cancelando la inspeccion 
			 */
			var vista2_visible = false;

			if (vista2_visible == 'si') {
				vista2_visible = true;
			} else if (vista2_visible == 'no') {
				vista2_visible = false;
			}
			$.vista2.setVisible(vista2_visible);

			/** 
			 * Enviamos a firma 
			 */
			Alloy.createController("firma_index", {}).getView().open();
			elemento = null, valor = null;
		};
		require('helper').ajaxUnico('consultarURL2', '' + String.format(L('x3244141284', '%1$scancelarTarea'), url_server.toString()) + '', 'POST', {
			id_inspector: seltarea.id_inspector,
			codigo_identificador: inspector.codigo_identificador,
			id_tarea: seltarea.id_server,
			num_caso: seltarea.num_caso,
			mensaje: razon,
			opcion: 0,
			tipo: 1
		}, 15000, consultarURL2);
	}
	resp = null;

});
/** 
 * Cerramos esta pantalla cuando es llamada en la pantalla siguente (datosbasicos) 
 */

_my_events['_cerrar_insp,ID_231485652'] = function(evento) {
	if (!_.isUndefined(evento.pantalla)) {
		if (evento.pantalla == L('x155149119_traducir', 'hayalguien')) {
			if (Ti.App.deployType != 'production') console.log('debug cerrando hayalguien', {});

			var ID_1436370236_trycatch = {
				error: function(e) {
					if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
				}
			};
			try {
				ID_1436370236_trycatch.error = function(evento) {
					if (Ti.App.deployType != 'production') console.log('error cerrando hayalguien', {});
				};
				$.HAY_ALGUIEN.close();
			} catch (e) {
				ID_1436370236_trycatch.error(e);
			}
		}
	} else {
		if (Ti.App.deployType != 'production') console.log('debug cerrando (todas) hayalguien', {});

		var ID_1962200830_trycatch = {
			error: function(e) {
				if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
			}
		};
		try {
			ID_1962200830_trycatch.error = function(evento) {
				if (Ti.App.deployType != 'production') console.log('error cerrando hayalguien', {});
			};
			$.HAY_ALGUIEN.close();
		} catch (e) {
			ID_1962200830_trycatch.error(e);
		}
	}
};
Alloy.Events.on('_cerrar_insp', _my_events['_cerrar_insp,ID_231485652']);

if (OS_IOS || OS_ANDROID) {
	$.HAY_ALGUIEN.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}
//$.HAY_ALGUIEN.open();
