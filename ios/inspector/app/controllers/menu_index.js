var _bind4section = {};
var _list_templates = {
	"contenido": {
		"Label2": {
			"text": "{id}"
		},
		"vista4": {},
		"Label": {
			"text": "{nombre}"
		}
	},
	"recinto": {
		"Label2": {
			"text": "{id}"
		},
		"vista4": {},
		"Label": {
			"text": "{nombre}"
		}
	},
	"nivel": {
		"Label2": {
			"text": "{id}"
		},
		"vista20": {},
		"Label": {
			"text": "{nombre}"
		}
	},
	"tareas_mistareas": {
		"Label2": {
			"text": "{comuna}"
		},
		"vista5": {},
		"vista7": {},
		"vista3": {},
		"Adistancia": {
			"text": "a {distancia} km"
		},
		"vista9": {},
		"imagen": {},
		"vista11": {},
		"vista13": {
			"visible": "{seguirvisible}"
		},
		"Label": {
			"text": "{direccion}"
		},
		"vista4": {
			"idlocal": "{idlocal}"
		},
		"vista10": {},
		"Label3": {
			"text": "{ciudad}, {pais}"
		},
		"vista12": {},
		"vista6": {},
		"vista8": {}
	},
	"tareas": {
		"vista7": {},
		"Label3": {
			"text": "{direcciontarea}"
		},
		"vista8": {},
		"vista5": {},
		"Label5": {
			"text": "{ciudadtarea}, {paistarea} "
		},
		"vista4": {},
		"vista10": {},
		"Label4": {
			"text": "{comunatarea}"
		},
		"Aubicaciontarea": {
			"text": "a {ubicaciontarea} km"
		},
		"vista9": {},
		"vista6": {},
		"vista11": {},
		"vista3": {
			"myid": "{myid}"
		}
	},
	"criticas": {
		"vista15": {},
		"Label8": {
			"text": "{ciudadcritica}, {paiscritica}"
		},
		"vista14": {},
		"vista18": {},
		"Label6": {
			"text": "{direccioncritica}"
		},
		"vista20": {},
		"vista17": {},
		"Label7": {
			"text": "{comunacritica}"
		},
		"vista16": {},
		"vista19": {},
		"Aubicacioncritica": {
			"text": "a {ubicacioncritica} km"
		},
		"vista12": {
			"myid": "{myid}"
		},
		"vista13": {}
	},
	"tarea": {
		"vista86": {},
		"Label7": {
			"text": "{direccion}"
		},
		"vista97": {},
		"vista92": {},
		"vista107": {
			"visible": "{seguir}"
		},
		"vista93": {},
		"Adistance4": {
			"text": "a {distance} km"
		},
		"vista106": {},
		"vista102": {},
		"Label3": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"Label11": {
			"text": "{comuna}"
		},
		"Label21": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"Adistance7": {
			"text": "a {distance} km"
		},
		"vista38": {},
		"vista41": {},
		"vista55": {},
		"vista47": {},
		"vista19": {},
		"vista103": {},
		"vista48": {},
		"vista65": {},
		"vista7": {},
		"Label30": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"Label5": {
			"text": "{comuna}"
		},
		"vista64": {},
		"vista61": {},
		"Label14": {
			"text": "{comuna}"
		},
		"vista16": {},
		"vista15": {},
		"vista72": {
			"backgroundColor": "{bgcolor}"
		},
		"vista80": {
			"visible": "{vis_tipo9}"
		},
		"vista83": {},
		"Label23": {
			"text": "{comuna}"
		},
		"Label13": {
			"text": "{direccion}"
		},
		"vista90": {
			"backgroundColor": "{bgcolor}"
		},
		"Label33": {
			"text": "{comuna}"
		},
		"vista57": {},
		"Label8": {
			"text": "{comuna}"
		},
		"vista85": {},
		"vista68": {},
		"vista53": {
			"visible": "{vis_tipo6}"
		},
		"Label28": {
			"text": "{direccion}"
		},
		"vista76": {},
		"vista34": {},
		"vista37": {},
		"Label16": {
			"text": "{direccion}"
		},
		"vista25": {},
		"vista49": {},
		"Adistance6": {
			"text": "a {distance} km"
		},
		"Adistance": {
			"text": "a {distance} km"
		},
		"Adistance11": {
			"text": "a {distance} km"
		},
		"vista13": {},
		"vista27": {
			"backgroundColor": "{bgcolor}"
		},
		"vista79": {},
		"vista73": {},
		"Label34": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"Label35": {
			"text": "{id}"
		},
		"vista60": {},
		"vista82": {},
		"vista35": {
			"visible": "{vis_tipo4}"
		},
		"Label": {
			"text": "{direccion}"
		},
		"vista87": {},
		"vista67": {},
		"vista33": {},
		"vista70": {},
		"Label10": {
			"text": "{direccion}"
		},
		"Label12": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"vista96": {},
		"vista22": {},
		"Label24": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"vista52": {},
		"vista28": {},
		"vista99": {
			"backgroundColor": "{bgcolor}"
		},
		"vista84": {},
		"vista30": {},
		"vista78": {},
		"vista45": {
			"backgroundColor": "{bgcolor}"
		},
		"Label9": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"vista63": {
			"backgroundColor": "{bgcolor}"
		},
		"Label20": {
			"text": "{comuna}"
		},
		"Label29": {
			"text": "{comuna}"
		},
		"vista12": {},
		"Label18": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"vista29": {},
		"vista43": {},
		"vista105": {},
		"Label27": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"vista9": {
			"backgroundColor": "{bgcolor}"
		},
		"vista46": {},
		"Label22": {
			"text": "{direccion}"
		},
		"vista39": {},
		"Label26": {
			"text": "{comuna}"
		},
		"vista14": {},
		"Adistance9": {
			"text": "a {distance} km"
		},
		"vista58": {},
		"vista21": {},
		"vista24": {},
		"vista66": {},
		"vista89": {
			"visible": "{vis_tipo10}"
		},
		"vista23": {},
		"Label2": {
			"text": "{comuna}"
		},
		"vista88": {},
		"vista40": {},
		"vista74": {},
		"vista42": {},
		"vista94": {},
		"vista56": {},
		"vista91": {},
		"vista75": {},
		"vista71": {
			"visible": "{vis_tipo8}"
		},
		"Label15": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"vista95": {},
		"vista81": {
			"backgroundColor": "{bgcolor}"
		},
		"vista36": {
			"backgroundColor": "{bgcolor}"
		},
		"Label31": {
			"text": "{prioridad_tiempo}"
		},
		"vista98": {
			"visible": "{vis_otro}"
		},
		"vista10": {},
		"Label17": {
			"text": "{comuna}"
		},
		"vista100": {},
		"vista51": {},
		"vista44": {
			"visible": "{vis_tipo5}"
		},
		"Adistance8": {
			"text": "a {distance} km"
		},
		"imagen": {},
		"vista59": {},
		"Label4": {
			"text": "{direccion}"
		},
		"vista18": {
			"backgroundColor": "{bgcolor}"
		},
		"vista101": {},
		"vista31": {},
		"vista17": {
			"visible": "{vis_tipo2}"
		},
		"Label6": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"vista8": {
			"visible": "{vis_tipo1}"
		},
		"vista104": {},
		"vista11": {},
		"Adistance2": {
			"text": "a {distance} km"
		},
		"vista32": {},
		"vista69": {},
		"Adistance3": {
			"text": "a {distance} km"
		},
		"vista62": {
			"visible": "{vis_tipo7}"
		},
		"vista50": {},
		"Label25": {
			"text": "{direccion}"
		},
		"vista77": {},
		"vista54": {
			"backgroundColor": "{bgcolor}"
		},
		"Adistance5": {
			"text": "a {distance} km"
		},
		"Adistance10": {
			"text": "a {distance} km"
		},
		"vista20": {},
		"Label19": {
			"text": "{direccion}"
		},
		"Label32": {
			"text": "{direccion}"
		},
		"vista26": {
			"visible": "{vis_tipo3}"
		}
	},
	"tarea_historia": {
		"Label3": {
			"text": "{comuna}"
		},
		"vista6": {},
		"vista8": {},
		"vista4": {},
		"Label2": {
			"text": "{hora_termino}"
		},
		"vista10": {},
		"vista15": {
			"visible": "{bt_enviartarea}"
		},
		"vista12": {},
		"Label": {
			"text": "{direccion}"
		},
		"vista5": {
			"idlocal": "{id}",
			"estado": "{estado_tarea}"
		},
		"vista11": {},
		"vista14": {},
		"Label4": {
			"text": "{ciudad}, {pais}"
		},
		"vista16": {
			"visible": "{enviando_tarea}"
		},
		"vista13": {},
		"vista7": {},
		"ENVIAR": {},
		"imagen": {},
		"vista9": {}
	}
};
Alloy.Globals["menu"] = $.menu;
var _activity;
if (OS_ANDROID) {
	_activity = $.menu.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	args = arguments[0] || {};


var _activity;
if (OS_ANDROID) {
	_activity = $.Label2.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
if (OS_ANDROID) {
	$.Label2.addEventListener('open', function(e) {});
}

function Click_vista(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.iconoRefresh.setColor('#ff0033');

	var gps_error = ('gps_error' in require('vars')) ? require('vars')['gps_error'] : '';
	if (gps_error == true || gps_error == 'true') {
		$.iconoRefresh.setColor('#2d9edb');

		var preguntarAlerta_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta = Ti.UI.createAlertDialog({
			title: L('x3661800039_traducir', 'Error geolocalizando'),
			message: L('x942436043_traducir', 'Ha ocurrido un error al geolocalizarlo'),
			buttonNames: preguntarAlerta_opts
		});
		preguntarAlerta.addEventListener('click', function(e) {
			var errori = preguntarAlerta_opts[e.index];
			errori = null;

			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta.show();
	}
	var obtenertareas = ('obtenertareas' in require('vars')) ? require('vars')['obtenertareas'] : '';
	if (obtenertareas == true || obtenertareas == 'true') {
		if (Ti.App.deployType != 'production') console.log('actualizando tareas', {});
	} else {
		require('vars')['obtenerentrantes'] = L('x4261170317', 'true');
		var gps_latitud = ('gps_latitud' in require('vars')) ? require('vars')['gps_latitud'] : '';
		var gps_longitud = ('gps_longitud' in require('vars')) ? require('vars')['gps_longitud'] : '';
		var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
		if (Ti.App.deployType != 'production') console.log('Llamando servidor obtenerTareasEntrantes', {});
		var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
		var consultarURL = {};
		console.log('DEBUG WEB: requesting url:' + String.format(L('x1654875529', '%1$sobtenerTareasEntrantes'), url_server.toString()) + ' with data:', {
			_method: 'POST',
			_params: {
				id_inspector: inspector.id_server,
				lat: gps_latitud,
				lon: gps_longitud
			},
			_timeout: '15000'
		});

		consultarURL.success = function(e) {
			var elemento = e,
				valor = e;
			if (elemento.error != 0 && elemento.error != '0') {
				if (Ti.App.deployType != 'production') console.log('error de servidor en obtenerTareasEntrntes', {
					"elemento": elemento
				});
				$.iconoRefresh.setColor('#2d9edb');

				var preguntarAlerta2_opts = [L('x1518866076_traducir', 'Aceptar')];
				var preguntarAlerta2 = Ti.UI.createAlertDialog({
					title: L('x57652245_traducir', 'Alerta'),
					message: L('x344160560_traducir', 'Error con el servidor'),
					buttonNames: preguntarAlerta2_opts
				});
				preguntarAlerta2.addEventListener('click', function(e) {
					var errori = preguntarAlerta2_opts[e.index];
					errori = null;

					e.source.removeEventListener("click", arguments.callee);
				});
				preguntarAlerta2.show();
			} else {
				var eliminarModelo_i = Alloy.Collections.tareas_entrantes;
				var sql = "DELETE FROM " + eliminarModelo_i.config.adapter.collection_name;
				var db = Ti.Database.open(eliminarModelo_i.config.adapter.db_name);
				db.execute(sql);
				db.close();
				eliminarModelo_i.trigger('remove');
				if (Ti.App.deployType != 'production') console.log('Respuesta de servidor TareasEntrantes (sin error)', {
					"elemento": elemento
				});
				var elemento_critica = elemento.critica;
				var insertarModelo_m = Alloy.Collections.tareas_entrantes;
				var db_insertarModelo = Ti.Database.open(insertarModelo_m.config.adapter.db_name);
				db_insertarModelo.execute('BEGIN');
				_.each(elemento_critica, function(insertarModelo_fila, pos) {
					db_insertarModelo.execute('INSERT INTO tareas_entrantes (fecha_tarea, id_inspeccion, id_asegurado, nivel_2, comentario_can_o_rech, asegurado_tel_fijo, estado_tarea, bono, evento, id_inspector, asegurado_codigo_identificador, lat, nivel_1, asegurado_nombre, pais, direccion, asegurador, fecha_ingreso, fecha_siniestro, nivel_1_texto, distance, nivel_4, perfil, asegurado_id, pais_texto, id_server, categoria, nivel_3, asegurado_correo, num_caso, lon, asegurado_tel_movil, tipo_tarea, nivel_5) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', insertarModelo_fila.fecha_tarea, insertarModelo_fila.id_inspeccion, insertarModelo_fila.id_asegurado, insertarModelo_fila.nivel_2, '-', insertarModelo_fila.asegurado_tel_fijo, insertarModelo_fila.estado_tarea, insertarModelo_fila.bono, insertarModelo_fila.evento, insertarModelo_fila.id_inspector, insertarModelo_fila.asegurado_codigo_identificador, insertarModelo_fila.lat, insertarModelo_fila.nivel_1, insertarModelo_fila.asegurado_nombre, -1, insertarModelo_fila.direccion, insertarModelo_fila.asegurador, insertarModelo_fila.fecha_ingreso, insertarModelo_fila.fecha_siniestro, insertarModelo_fila.nivel_1_, insertarModelo_fila.distancia, insertarModelo_fila.nivel_4, 'casa', insertarModelo_fila.asegurado_id, insertarModelo_fila.pais_, insertarModelo_fila.id, insertarModelo_fila.categoria, insertarModelo_fila.nivel_3, insertarModelo_fila.asegurado_correo, insertarModelo_fila.num_caso, insertarModelo_fila.lon, insertarModelo_fila.asegurado_tel_movil, insertarModelo_fila.tipo_tarea, insertarModelo_fila.nivel_5);
				});
				db_insertarModelo.execute('COMMIT');
				db_insertarModelo.close();
				db_insertarModelo = null;
				insertarModelo_m.trigger('change');
				var elemento_normal = elemento.normal;
				var insertarModelo2_m = Alloy.Collections.tareas_entrantes;
				var db_insertarModelo2 = Ti.Database.open(insertarModelo2_m.config.adapter.db_name);
				db_insertarModelo2.execute('BEGIN');
				_.each(elemento_normal, function(insertarModelo2_fila, pos) {
					db_insertarModelo2.execute('INSERT INTO tareas_entrantes (fecha_tarea, id_inspeccion, id_asegurado, nivel_2, comentario_can_o_rech, asegurado_tel_fijo, estado_tarea, bono, evento, id_inspector, asegurado_codigo_identificador, lat, nivel_1, asegurado_nombre, pais, direccion, asegurador, fecha_ingreso, fecha_siniestro, nivel_1_texto, distance, nivel_4, perfil, asegurado_id, pais_texto, id_server, categoria, nivel_3, asegurado_correo, num_caso, lon, asegurado_tel_movil, tipo_tarea, nivel_5) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', insertarModelo2_fila.fecha_tarea, insertarModelo2_fila.id_inspeccion, insertarModelo2_fila.id_asegurado, insertarModelo2_fila.nivel_2, '-', insertarModelo2_fila.asegurado_tel_fijo, insertarModelo2_fila.estado_tarea, insertarModelo2_fila.bono, insertarModelo2_fila.evento, insertarModelo2_fila.id_inspector, insertarModelo2_fila.asegurado_codigo_identificador, insertarModelo2_fila.lat, insertarModelo2_fila.nivel_1, insertarModelo2_fila.asegurado_nombre, -1, insertarModelo2_fila.direccion, insertarModelo2_fila.asegurador, insertarModelo2_fila.fecha_ingreso, insertarModelo2_fila.fecha_siniestro, insertarModelo2_fila.nivel_1_, insertarModelo2_fila.distancia, insertarModelo2_fila.nivel_4, 'casa', insertarModelo2_fila.asegurado_id, insertarModelo2_fila.pais_, insertarModelo2_fila.id, insertarModelo2_fila.categoria, insertarModelo2_fila.nivel_3, insertarModelo2_fila.asegurado_correo, insertarModelo2_fila.num_caso, insertarModelo2_fila.lon, insertarModelo2_fila.asegurado_tel_movil, insertarModelo2_fila.tipo_tarea, insertarModelo2_fila.nivel_5);
				});
				db_insertarModelo2.execute('COMMIT');
				db_insertarModelo2.close();
				db_insertarModelo2 = null;
				insertarModelo2_m.trigger('change');
				$.iconoRefresh.setColor('#2d9edb');


				Alloy.Events.trigger('_refrescar_tareas_entrantes');
				require('vars')['obtenerentrantes'] = L('x734881840_traducir', 'false');
			}
			elemento = null, valor = null;
		};

		consultarURL.error = function(e) {
			var elemento = e,
				valor = e;
			if (Ti.App.deployType != 'production') console.log('Hubo una falla al llamar servicio obtenerTareasEntrantes', {});
			$.iconoRefresh.setColor('#2d9edb');

			var preguntarAlerta3_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta3 = Ti.UI.createAlertDialog({
				title: L('x57652245_traducir', 'Alerta'),
				message: L('x3389756565_traducir', 'No se pudo conectar con el servidor'),
				buttonNames: preguntarAlerta3_opts
			});
			preguntarAlerta3.addEventListener('click', function(e) {
				var errori = preguntarAlerta3_opts[e.index];
				errori = null;

				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta3.show();
			elemento = null, valor = null;
		};
		require('helper').ajaxUnico('consultarURL', '' + String.format(L('x1654875529', '%1$sobtenerTareasEntrantes'), url_server.toString()) + '', 'POST', {
			id_inspector: inspector.id_server,
			lat: gps_latitud,
			lon: gps_longitud
		}, 15000, consultarURL);
	}
}

$.widgetSininternet.init({
	titulo: L('x2828751865_traducir', '¡ESTAS SIN CONEXION!'),
	__id: 'ALL374340032',
	mensaje: L('x3528440528_traducir', 'No puedes ver las tareas entrantes'),
	onon: on_widgetSininternet,
	onoff: Off_widgetSininternet
});

function on_widgetSininternet(e) {

	var evento = e;
	var vista2_visible = true;

	if (vista2_visible == 'si') {
		vista2_visible = true;
	} else if (vista2_visible == 'no') {
		vista2_visible = false;
	}
	$.vista2.setVisible(vista2_visible);


	Alloy.Events.trigger('_refrescar_tareas_entrantes');

}

function Off_widgetSininternet(e) {

	var evento = e;
	var vista2_visible = false;

	if (vista2_visible == 'si') {
		vista2_visible = true;
	} else if (vista2_visible == 'no') {
		vista2_visible = false;
	}
	$.vista2.setVisible(vista2_visible);


}

function Itemclick_listado(e) {

	e.cancelBubble = true;
	var objeto = e.section.getItemAt(e.itemIndex);
	var modelo = {},
		_modelo = [];
	var fila = {},
		fila_bak = {},
		info = {
			_template: objeto.template,
			_what: [],
			_seccion_ref: e.section.getHeaderTitle(),
			_model_id: -1
		},
		_tmp = {
			objmap: {}
		};
	if ('itemId' in e) {
		info._model_id = e.itemId;
		modelo._id = info._model_id;
		if (info._seccion_ref != '' && info._seccion_ref in _bind4section) {
			modelo._collection = _bind4section[info._seccion_ref];
			_tmp._coll = modelo._collection;
		}
	}
	if (info._seccion_ref != '' && info._seccion_ref in _bind4section) {
		if (Alloy.Collections[_tmp._coll].config.adapter.type == 'sql') {
			_tmp._inst = Alloy.Collections[_tmp._coll];
			_tmp._id = Alloy.Collections[_tmp._coll].config.adapter.idAttribute;
			_tmp._dbsql = 'SELECT * FROM ' + Alloy.Collections[_tmp._coll].config.adapter.collection_name + ' WHERE ' + _tmp._id + ' = ' + e.itemId;
			_tmp._db = Ti.Database.open(Alloy.Collections[_tmp._coll].config.adapter.db_name);
			_modelo = _tmp._db.execute(_tmp._dbsql);
			var values = [],
				fieldNames = [];
			var fieldCount = _.isFunction(_modelo.fieldCount) ? _modelo.fieldCount() : _modelo.fieldCount;
			var getField = _modelo.field;
			var i = 0;
			for (; fieldCount > i; i++) fieldNames.push(_modelo.fieldName(i));
			while (_modelo.isValidRow()) {
				var o = {};
				for (i = 0; fieldCount > i; i++) o[fieldNames[i]] = getField(i);
				values.push(o);
				_modelo.next();
			}
			_modelo = values;
			_tmp._db.close();
		} else {
			_tmp._search = {};
			_tmp._search[_tmp._id] = e.itemId + '';
			_modelo = Alloy.Collections[_tmp._coll].fetch(_tmp._search);
		}
	}
	var findVariables = require('fvariables');
	_.each(_list_templates[info._template], function(obj_id, id) {
		_.each(obj_id, function(valor, prop) {
			var llaves = findVariables(valor, '{', '}');
			_.each(llaves, function(llave) {
				_tmp.objmap[llave] = {
					id: id,
					prop: prop
				};
				fila[llave] = objeto[id][prop];
				if (id == e.bindId) info._what.push(llave);
			});
		});
	});
	info._what = info._what.join(',');
	fila_bak = JSON.parse(JSON.stringify(fila));
	if (Ti.App.deployType != 'production') console.log('Mi objecto es', {
		"info": info,
		"objeto": fila
	});
	var nulo = Alloy.createController("tomartarea_index", {
		'_objeto': fila,
		'_id': fila.myid,
		'_tipo': 'entrantes',
		'__master_model': (typeof modelo !== 'undefined') ? modelo : {},
		'__modelo': (typeof _modelo !== 'undefined' && _modelo.length > 0) ? _modelo[0] : {}
	}).getView();
	nulo.open({
		modal: true
	});

	nulo = null;
	_tmp.changed = false;
	_tmp.diff_keys = [];
	_.each(fila, function(value1, prop) {
		var had_samekey = false;
		_.each(fila_bak, function(value2, prop2) {
			if (prop == prop2 && value1 == value2) {
				had_samekey = true;
			} else if (!_.has(fila_bak, prop) || !_.has(fila, prop2)) {
				has_samekey = true;
			}
		});
		if (!had_samekey) _tmp.diff_keys.push(prop);
	});
	if (_tmp.diff_keys.length > 0) _tmp.changed = true;
	if (_tmp.changed == true) {
		_.each(_tmp.diff_keys, function(llave) {
			objeto[_tmp.objmap[llave].id][_tmp.objmap[llave].prop] = fila[llave];
		});
		e.section.updateItemAt(e.itemIndex, objeto);
	}

}


_my_events['_refrescar_tareas_entrantes,ID_1636319035'] = function(evento) {
	/** 
	 * Limpiamos el listado para ingresar las tareas refrescadas 
	 */
	var listado_borrar = false;

	var limpiarListado = function(animar) {
		var a_nimar = (typeof animar == 'undefined') ? false : animar;
		if (OS_IOS && a_nimar == true) {
			var s_ecciones = $.listado.getSections();
			_.each(s_ecciones, function(obj_id, pos) {
				$.listado.deleteSectionAt(0, {
					animated: true
				});
			});
		} else {
			$.listado.setSections([]);
		}
	};
	limpiarListado(listado_borrar);

	/** 
	 * Creamos las distintas secciones y header para indicar la fecha de la tarea 
	 */
	var seccionListadoHoli = Titanium.UI.createListSection({
		headerTitle: L('x2819214932_traducir', 'holi1')
	});
	var headerListado = Titanium.UI.createView({
		height: Ti.UI.FILL,
		width: Ti.UI.FILL
	});
	var vista21 = Titanium.UI.createView({
		height: '25dp',
		layout: 'composite',
		width: Ti.UI.FILL,
		backgroundColor: '#FFDCDC'
	});
	var CRITICAS = Titanium.UI.createLabel({
		text: L('x478055026_traducir', 'CRITICAS'),
		color: '#EE7F7E',
		touchEnabled: false,
		font: {
			fontFamily: 'SFUIText-Medium',
			fontSize: '15dp'
		}

	});
	vista21.add(CRITICAS);

	headerListado.add(vista21);
	seccionListadoHoli.setHeaderView(headerListado);
	$.listado.appendSection(seccionListadoHoli);
	var seccionListadoHoyentrantes = Titanium.UI.createListSection({
		headerTitle: L('x3166667470', 'hoy_entrantes')
	});
	var headerListado2 = Titanium.UI.createView({
		height: Ti.UI.FILL,
		width: Ti.UI.FILL
	});
	var vista22 = Titanium.UI.createView({
		height: '25dp',
		layout: 'composite',
		width: Ti.UI.FILL,
		backgroundColor: '#F7F7F7'
	});
	var PARAHOY = Titanium.UI.createLabel({
		text: L('x1296057275_traducir', 'PARA HOY'),
		color: '#999999',
		touchEnabled: false,
		font: {
			fontFamily: 'SFUIText-Medium',
			fontSize: '14dp'
		}

	});
	vista22.add(PARAHOY);

	headerListado2.add(vista22);
	seccionListadoHoyentrantes.setHeaderView(headerListado2);
	$.listado.appendSection(seccionListadoHoyentrantes);
	var seccionListadoMananaentrantes = Titanium.UI.createListSection({
		headerTitle: L('x3690817388', 'manana_entrantes')
	});
	var headerListado3 = Titanium.UI.createView({
		height: Ti.UI.FILL,
		width: Ti.UI.FILL
	});
	var vista23 = Titanium.UI.createView({
		height: '25dp',
		layout: 'composite',
		width: Ti.UI.FILL,
		backgroundColor: '#F7F7F7'
	});
	var MAANA = Titanium.UI.createLabel({
		text: L('x3380933310_traducir', 'MAÑANA'),
		color: '#999999',
		touchEnabled: false,
		font: {
			fontFamily: 'SFUIText-Medium',
			fontSize: '14dp'
		}

	});
	vista23.add(MAANA);

	headerListado3.add(vista23);
	seccionListadoMananaentrantes.setHeaderView(headerListado3);
	$.listado.appendSection(seccionListadoMananaentrantes);
	/** 
	 * Consultamos la tabla de las tareas criticas y guardamos en variable criticas 
	 */
	var consultarModelo_i = Alloy.createCollection('tareas_entrantes');
	var consultarModelo_i_where = 'tipo_tarea=1 ORDER BY TIPO_TAREA ASC';
	consultarModelo_i.fetch({
		query: 'SELECT * FROM tareas_entrantes WHERE tipo_tarea=1 ORDER BY TIPO_TAREA ASC'
	});
	var criticas = require('helper').query2array(consultarModelo_i);
	if (Ti.App.deployType != 'production') console.log('tareas criticas existentes', {
		"criticas": criticas
	});
	var tarea_index = 0;
	_.each(criticas, function(tarea, tarea_pos, tarea_list) {
		tarea_index += 1;
		if ((_.isObject(tarea.nivel_2) || _.isString(tarea.nivel_2)) && _.isEmpty(tarea.nivel_2)) {
			/** 
			 * Revisamos cual es el ultimo nivel disponible y lo fijamos dentro de la variable tarea como ultimo_nivel 
			 */
			var tarea = _.extend(tarea, {
				ultimo_nivel: tarea.nivel_1
			});
		} else if ((_.isObject(tarea.nivel_3) || _.isString(tarea.nivel_3)) && _.isEmpty(tarea.nivel_3)) {
			var tarea = _.extend(tarea, {
				ultimo_nivel: tarea.nivel_2
			});
		} else if ((_.isObject(tarea.nivel_4) || _.isString(tarea.nivel_4)) && _.isEmpty(tarea.nivel_4)) {
			var tarea = _.extend(tarea, {
				ultimo_nivel: tarea.nivel_3
			});
		} else if ((_.isObject(tarea.nivel_5) || _.isString(tarea.nivel_5)) && _.isEmpty(tarea.nivel_5)) {
			var tarea = _.extend(tarea, {
				ultimo_nivel: tarea.nivel_4
			});
		} else {
			var tarea = _.extend(tarea, {
				ultimo_nivel: tarea.nivel_5
			});
		}
		/** 
		 * Insertamos la variable tarea con los atributos en el listado, seccion criticas 
		 */
		var itemListado = [{
			vista15: {},
			Label8: {
				text: String.format(L('x1487588533_traducir', '%1$s, %2$s'), (tarea.nivel_2) ? tarea.nivel_2.toString() : '', (tarea.pais_texto) ? tarea.pais_texto.toString() : '')
			},
			vista14: {},
			vista18: {},
			Label6: {
				text: tarea.direccion
			},
			vista20: {},
			vista17: {},
			Label7: {
				text: tarea.ultimo_nivel
			},
			vista16: {},
			vista19: {},
			template: 'criticas',
			Aubicacioncritica: {
				text: String.format(L('x1959850688_traducir', 'a %1$s km'), tarea.distance.toString())
			},
			vista12: {
				myid: tarea.id
			},
			vista13: {}

		}];
		var itemListado_secs = {};
		_.map($.listado.getSections(), function(itemListado_valor, itemListado_indice) {
			itemListado_secs[itemListado_valor.getHeaderTitle()] = itemListado_indice;
			return itemListado_valor;
		});
		if ('' + L('x2819214932_traducir', 'holi1') + '' in itemListado_secs) {
			$.listado.sections[itemListado_secs['' + L('x2819214932_traducir', 'holi1') + '']].appendItems(itemListado);
		} else {
			console.log('DEBUG:dynamic listItem points to a invalid section id: check attribute _seccion.');
		}
	});
	if (criticas && criticas.length) {
		/** 
		 * Revisamos si la variable criticas contiene tareas, siendo true, contamos el largo del arreglo y las definimos en el badge, si no tiene tareas, no mostramos el badge 
		 */
		var Label2_badge = criticas.length;

		var set_Badge = function(valor) {
			if (OS_IOS) {
				if (valor == 'null') {
					$.tab_Label2.setBadge(null);
				} else {
					$.tab_Label2.setBadge(valor);
				}
			} else if (OS_ANDROID) {
				if (valor == 'null') {
					$.tab_Label2.setIcon(null);
				} else if ('images' in require('a4w').styles && 'androidbadge' + Label2_badge in require('a4w').styles['images']) {
					$.tab_Label2.setIcon(require('a4w').styles['images']['androidbadge' + Label2_badge]);
					if (typeof WPATH != 'undefined') {
						require(WPATH('vars'))['_android_badge'] = Label2_badge;
					} else {
						require('vars')['_android_badge'] = Label2_badge;
					}
				}
			}
		};
		var get_Badge = function() {
			if (OS_IOS) {
				return $.tab_Label2.getBadge();
			} else if (OS_ANDROID) {
				if (typeof WPATH != 'undefined') {
					return require(WPATH('vars'))['_android_badge'];
				} else {
					return require('vars')['_android_badge'];
				}
			} else {
				return 0;
			}
		};
		set_Badge(Label2_badge);

	} else {
		var Label2_badge = 'null';

		var set_Badge = function(valor) {
			if (OS_IOS) {
				if (valor == 'null') {
					$.tab_Label2.setBadge(null);
				} else {
					$.tab_Label2.setBadge(valor);
				}
			} else if (OS_ANDROID) {
				if (valor == 'null') {
					$.tab_Label2.setIcon(null);
				} else if ('images' in require('a4w').styles && 'androidbadge' + Label2_badge in require('a4w').styles['images']) {
					$.tab_Label2.setIcon(require('a4w').styles['images']['androidbadge' + Label2_badge]);
					if (typeof WPATH != 'undefined') {
						require(WPATH('vars'))['_android_badge'] = Label2_badge;
					} else {
						require('vars')['_android_badge'] = Label2_badge;
					}
				}
			}
		};
		var get_Badge = function() {
			if (OS_IOS) {
				return $.tab_Label2.getBadge();
			} else if (OS_ANDROID) {
				if (typeof WPATH != 'undefined') {
					return require(WPATH('vars'))['_android_badge'];
				} else {
					return require('vars')['_android_badge'];
				}
			} else {
				return 0;
			}
		};
		set_Badge(Label2_badge);

	}
	/** 
	 * Consultamos la tabla de las tareas criticas y guardamos en variable normales 
	 */
	var consultarModelo2_i = Alloy.createCollection('tareas_entrantes');
	var consultarModelo2_i_where = 'tipo_tarea=0 ORDER BY FECHA_TAREA ASC';
	consultarModelo2_i.fetch({
		query: 'SELECT * FROM tareas_entrantes WHERE tipo_tarea=0 ORDER BY FECHA_TAREA ASC'
	});
	var normales = require('helper').query2array(consultarModelo2_i);
	if (normales && normales.length) {
		hoy = new Date();
		if (Ti.App.deployType != 'production') console.log('tareas normales existentes', {
			"normales": normales
		});
		/** 
		 * Formateamos fecha de hoy y manana para cargar la tarea dependiendo de la fecha de realizacion 
		 */
		var fecha_hoy = null;
		if ('formatear_fecha' in require('funciones')) {
			fecha_hoy = require('funciones').formatear_fecha({
				'fecha': hoy,
				'formato': L('x591439515_traducir', 'YYYY-MM-DD')
			});
		} else {
			try {
				fecha_hoy = f_formatear_fecha({
					'fecha': hoy,
					'formato': L('x591439515_traducir', 'YYYY-MM-DD')
				});
			} catch (ee) {}
		}
		manana = new Date();
		manana.setDate(manana.getDate() + 1);;
		var fecha_manana = null;
		if ('formatear_fecha' in require('funciones')) {
			fecha_manana = require('funciones').formatear_fecha({
				'fecha': manana,
				'formato': L('x591439515_traducir', 'YYYY-MM-DD')
			});
		} else {
			try {
				fecha_manana = f_formatear_fecha({
					'fecha': manana,
					'formato': L('x591439515_traducir', 'YYYY-MM-DD')
				});
			} catch (ee) {}
		}
		/** 
		 * Inicializamos la variable de ultima fecha en vacio para usarla en el recorrido de tareas 
		 */
		require('vars')['ultima_fecha'] = '';
		var tarea_index = 0;
		_.each(normales, function(tarea, tarea_pos, tarea_list) {
			tarea_index += 1;
			/** 
			 * Variable usada para poder filtrar las tareas segun la fecha y no se vayan todas a una sola seccion 
			 */
			var ultima_fecha = ('ultima_fecha' in require('vars')) ? require('vars')['ultima_fecha'] : '';
			if (ultima_fecha != tarea.fecha_tarea) {
				/** 
				 * Generamos una seccion nueva para las fechas que no sean de hoy, ni de manana 
				 */
				if (Ti.App.deployType != 'production') console.log('recorrien3', {
					"fmanana": fecha_manana,
					"fhoy": fecha_hoy,
					"ufecha": ultima_fecha
				});
				if (tarea.fecha_tarea == fecha_hoy) {} else if (tarea.fecha_tarea == fecha_manana) {} else {
					var fecha_titulo = null;
					if ('formatear_fecha' in require('funciones')) {
						fecha_titulo = require('funciones').formatear_fecha({
							'fecha': tarea.fecha_tarea,
							'formato': L('x2933785305_traducir', 'DD/MM/YYYY'),
							'formato_entrada': L('x591439515_traducir', 'YYYY-MM-DD')
						});
					} else {
						try {
							fecha_titulo = f_formatear_fecha({
								'fecha': tarea.fecha_tarea,
								'formato': L('x2933785305_traducir', 'DD/MM/YYYY'),
								'formato_entrada': L('x591439515_traducir', 'YYYY-MM-DD')
							});
						} catch (ee) {}
					}
					var seccionListadoFechaentrantes = Titanium.UI.createListSection({
						headerTitle: L('x2345059420', 'fecha_entrantes')
					});
					var headerListado4 = Titanium.UI.createView({
						height: Ti.UI.FILL,
						width: Ti.UI.FILL
					});
					var vista24 = Titanium.UI.createView({
						height: '25dp',
						layout: 'composite',
						width: Ti.UI.FILL,
						backgroundColor: '#F7F7F7'
					});
					var Fechatitulo = Titanium.UI.createLabel({
						text: fecha_titulo,
						color: '#999999',
						touchEnabled: false,
						font: {
							fontFamily: 'SFUIText-Medium',
							fontSize: '14dp'
						}

					});
					vista24.add(Fechatitulo);

					headerListado4.add(vista24);
					seccionListadoFechaentrantes.setHeaderView(headerListado4);
					$.listado.appendSection(seccionListadoFechaentrantes);
				}
				/** 
				 * Actualizamos la variable con&#160;la ultima fecha de la tarea 
				 */
				require('vars')['ultima_fecha'] = tarea.fecha_tarea;
			}
			if ((_.isObject(tarea.nivel_2) || _.isString(tarea.nivel_2)) && _.isEmpty(tarea.nivel_2)) {
				/** 
				 * Revisamos cual es el ultimo nivel disponible y lo fijamos dentro de la variable tarea como ultimo_nivel 
				 */
				var tarea = _.extend(tarea, {
					ultimo_nivel: tarea.nivel_1
				});
			} else if ((_.isObject(tarea.nivel_3) || _.isString(tarea.nivel_3)) && _.isEmpty(tarea.nivel_3)) {
				var tarea = _.extend(tarea, {
					ultimo_nivel: tarea.nivel_2
				});
			} else if ((_.isObject(tarea.nivel_4) || _.isString(tarea.nivel_4)) && _.isEmpty(tarea.nivel_4)) {
				var tarea = _.extend(tarea, {
					ultimo_nivel: tarea.nivel_3
				});
			} else if ((_.isObject(tarea.nivel_5) || _.isString(tarea.nivel_5)) && _.isEmpty(tarea.nivel_5)) {
				var tarea = _.extend(tarea, {
					ultimo_nivel: tarea.nivel_4
				});
			} else {
				var tarea = _.extend(tarea, {
					ultimo_nivel: tarea.nivel_5
				});
			}
			if (tarea.fecha_tarea == fecha_hoy) {
				/** 
				 * Dependiendo de la fecha de la tarea, es donde apuntaremos a la seccion que corresponde (segun fecha) 
				 */
				var itemListado2 = [{
					vista7: {},
					Label3: {
						text: tarea.direccion
					},
					vista8: {},
					vista5: {},
					Label5: {
						text: String.format(L('x1487588533_traducir', '%1$s, %2$s'), (tarea.nivel_2) ? tarea.nivel_2.toString() : '', (tarea.pais_texto) ? tarea.pais_texto.toString() : '')
					},
					vista4: {},
					vista10: {},
					Label4: {
						text: tarea.ultimo_nivel
					},
					Aubicaciontarea: {
						text: String.format(L('x1959850688_traducir', 'a %1$s km'), tarea.distance.toString())
					},
					template: 'tareas',
					vista9: {},
					vista6: {},
					vista11: {},
					vista3: {
						myid: tarea.id
					}

				}];
				var itemListado2_secs = {};
				_.map($.listado.getSections(), function(itemListado2_valor, itemListado2_indice) {
					itemListado2_secs[itemListado2_valor.getHeaderTitle()] = itemListado2_indice;
					return itemListado2_valor;
				});
				if ('' + L('x3166667470', 'hoy_entrantes') + '' in itemListado2_secs) {
					$.listado.sections[itemListado2_secs['' + L('x3166667470', 'hoy_entrantes') + '']].appendItems(itemListado2);
				} else {
					console.log('DEBUG:dynamic listItem points to a invalid section id: check attribute _seccion.');
				}
			} else if (tarea.fecha_tarea == fecha_manana) {
				var itemListado3 = [{
					vista7: {},
					Label3: {
						text: tarea.direccion
					},
					vista8: {},
					vista5: {},
					Label5: {
						text: String.format(L('x1487588533_traducir', '%1$s, %2$s'), (tarea.nivel_2) ? tarea.nivel_2.toString() : '', (tarea.pais_texto) ? tarea.pais_texto.toString() : '')
					},
					vista4: {},
					vista10: {},
					Label4: {
						text: tarea.ultimo_nivel
					},
					Aubicaciontarea: {
						text: String.format(L('x1959850688_traducir', 'a %1$s km'), tarea.distance.toString())
					},
					template: 'tareas',
					vista9: {},
					vista6: {},
					vista11: {},
					vista3: {
						myid: tarea.id
					}

				}];
				var itemListado3_secs = {};
				_.map($.listado.getSections(), function(itemListado3_valor, itemListado3_indice) {
					itemListado3_secs[itemListado3_valor.getHeaderTitle()] = itemListado3_indice;
					return itemListado3_valor;
				});
				if ('' + L('x3690817388', 'manana_entrantes') + '' in itemListado3_secs) {
					$.listado.sections[itemListado3_secs['' + L('x3690817388', 'manana_entrantes') + '']].appendItems(itemListado3);
				} else {
					console.log('DEBUG:dynamic listItem points to a invalid section id: check attribute _seccion.');
				}
			} else {
				var itemListado4 = [{
					vista7: {},
					Label3: {
						text: tarea.direccion
					},
					vista8: {},
					vista5: {},
					Label5: {
						text: String.format(L('x1487588533_traducir', '%1$s, %2$s'), (tarea.nivel_2) ? tarea.nivel_2.toString() : '', (tarea.pais_texto) ? tarea.pais_texto.toString() : '')
					},
					vista4: {},
					vista10: {},
					Label4: {
						text: tarea.ultimo_nivel
					},
					Aubicaciontarea: {
						text: String.format(L('x1959850688_traducir', 'a %1$s km'), tarea.distance.toString())
					},
					template: 'tareas',
					vista9: {},
					vista6: {},
					vista11: {},
					vista3: {
						myid: tarea.id
					}

				}];
				var itemListado4_secs = {};
				_.map($.listado.getSections(), function(itemListado4_valor, itemListado4_indice) {
					itemListado4_secs[itemListado4_valor.getHeaderTitle()] = itemListado4_indice;
					return itemListado4_valor;
				});
				if ('' + L('x2345059420', 'fecha_entrantes') + '' in itemListado4_secs) {
					$.listado.sections[itemListado4_secs['' + L('x2345059420', 'fecha_entrantes') + '']].appendItems(itemListado4);
				} else {
					console.log('DEBUG:dynamic listItem points to a invalid section id: check attribute _seccion.');
				}
			}
		});
	}
};
Alloy.Events.on('_refrescar_tareas_entrantes', _my_events['_refrescar_tareas_entrantes,ID_1636319035']);

_my_events['_refrescar_tareas,ID_411683779'] = function(evento) {

	Alloy.Events.trigger('_refrescar_tareas_entrantes');
};
Alloy.Events.on('_refrescar_tareas', _my_events['_refrescar_tareas,ID_411683779']);
var ID_170851302_func = function() {

	Alloy.Events.trigger('_refrescar_tareas_entrantes');
};
var ID_170851302 = setTimeout(ID_170851302_func, 1000 * 0.2);

//$.menu.open();
