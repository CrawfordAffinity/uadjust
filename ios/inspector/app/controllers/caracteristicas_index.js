var _bind4section = {
	"ref1": "insp_niveles"
};
var _list_templates = {
	"contenido": {
		"Label2": {
			"text": "{id}"
		},
		"vista4": {},
		"Label": {
			"text": "{nombre}"
		}
	},
	"recinto": {
		"Label2": {
			"text": "{id}"
		},
		"vista4": {},
		"Label": {
			"text": "{nombre}"
		}
	},
	"nivel": {
		"Label2": {
			"text": "{id}"
		},
		"vista20": {},
		"Label": {
			"text": "{nombre}"
		}
	}
};
var $datos = $.datos.toJSON();

$.CARACTERISTICAS_window.setTitleAttributes({
	color: 'WHITE'
});
var _activity;
if (OS_ANDROID) {
	_activity = $.CARACTERISTICAS.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'CARACTERISTICAS';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.CARACTERISTICAS_window.addEventListener('open', function(e) {
		abx.setStatusbarColor("#FFFFFF");
		abx.setBackgroundColor("#8ce5bd");
	});
}

/** 
 * Consulta los datos ingresados en la pantalla de niveles y permite desplegarlo en un listado de esta pantalla 
 */
var consultarModelo_like = function(search) {
	if (typeof search !== 'string' || this === null) {
		return false;
	}
	search = search.replace(new RegExp("([\\.\\\\\\+\\*\\?\\[\\^\\]\\$\\(\\)\\{\\}\\=\\!\\<\\>\\|\\:\\-])", "g"), "\\$1");
	search = search.replace(/%/g, '.*').replace(/_/g, '.');
	return RegExp('^' + search + '$', 'gi').test(this);
};
var consultarModelo_filter = function(coll) {
	var filtered = _.toArray(coll.filter(function(m) {
		return true;
	}));
	return filtered;
};
var consultarModelo_transform = function(model) {
	var fila = model.toJSON();
	return fila;
};
var consultarModelo_update = function(e) {};
_.defer(function() {
	Alloy.Collections.insp_niveles.fetch();
});
Alloy.Collections.insp_niveles.on('add change delete', function(ee) {
	consultarModelo_update(ee);
});
Alloy.Collections.insp_niveles.fetch();

function Click_vista(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	/** 
	 * Muestra pantalla preguntas 
	 */
	preguntarOpciones.show();

}

$.widgetPreguntac.init({
	titulo: L('x404129330_traducir', '¿EL ASEGURADO CONFIRMA ESTOS DATOS?'),
	__id: 'ALL516906299',
	si: L('x1723413441_traducir', 'SI, Están correctos'),
	texto: L('x2083765231_traducir', 'El asegurado debe confirmar que los datos de esta sección están correctos'),
	pantalla: L('x2851883104_traducir', '¿ESTA DE ACUERDO?'),
	onno: no_widgetPreguntac,
	onsi: si_widgetPreguntac,
	no: L('x55492959_traducir', 'NO, Hay que modificar algo'),
	header: L('x3614728713_traducir', 'verde'),
	onclick: Click_widgetPreguntac
});

function Click_widgetPreguntac(e) {

	var evento = e;
	/** 
	 * Se obtiene el a&#241;o actual para comparar que el a&#241;o ingresado no sea superior 
	 */
	var d = new Date();
	var anoactual = d.getFullYear();
	/** 
	 * Flags para verificar que todos los datos ingresados esten correctos 
	 */
	require('vars')[_var_scopekey]['otro_valor'] = L('x734881840_traducir', 'false');
	require('vars')[_var_scopekey]['sin_niveles'] = L('x734881840_traducir', 'false');
	require('vars')[_var_scopekey]['todobien'] = L('x4261170317', 'true');
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
		/** 
		 * Comprobamos si no hay niveles definidos, para mostrar posteriormente dialogo luego de finalizar resto de validaciones. 
		 */
		var consultarModelo2_i = Alloy.createCollection('insp_niveles');
		var consultarModelo2_i_where = 'id_inspeccion=\'' + seltarea.id_server + '\'';
		consultarModelo2_i.fetch({
			query: 'SELECT * FROM insp_niveles WHERE id_inspeccion=\'' + seltarea.id_server + '\''
		});
		var niveles = require('helper').query2array(consultarModelo2_i);
		if (niveles && niveles.length == 0) {
			require('vars')[_var_scopekey]['sin_niveles'] = L('x4261170317', 'true');
		}
	} else {
		var consultarModelo3_i = Alloy.createCollection('insp_niveles');
		var consultarModelo3_i_where = '';
		consultarModelo3_i.fetch();
		var niveles = require('helper').query2array(consultarModelo3_i);
		if (niveles && niveles.length == 0) {
			require('vars')[_var_scopekey]['sin_niveles'] = L('x4261170317', 'true');
		}
	}
	if (Ti.App.deployType != 'production') console.log('info datos', {
		"daot": $.datos.toJSON()
	});
	if (_.isUndefined($datos.destinos)) {
		require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
		var preguntarAlerta_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x3814056646_traducir', 'Seleccione destino de la vivienda'),
			buttonNames: preguntarAlerta_opts
		});
		preguntarAlerta.addEventListener('click', function(e) {
			var nulo = preguntarAlerta_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta.show();
		if (Ti.App.deployType != 'production') console.log('hola1', {});
	} else if ((_.isObject($datos.destinos) || _.isString($datos.destinos)) && _.isEmpty($datos.destinos)) {
		require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
		var preguntarAlerta2_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta2 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x3814056646_traducir', 'Seleccione destino de la vivienda'),
			buttonNames: preguntarAlerta2_opts
		});
		preguntarAlerta2.addEventListener('click', function(e) {
			var nulo = preguntarAlerta2_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta2.show();
		if (Ti.App.deployType != 'production') console.log('hola2', {});
	} else if (_.isUndefined($datos.construccion_ano)) {
		require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
		var preguntarAlerta3_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta3 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x2674242197_traducir', 'Ingrese año de construcción de la vivienda'),
			buttonNames: preguntarAlerta3_opts
		});
		preguntarAlerta3.addEventListener('click', function(e) {
			var nulo = preguntarAlerta3_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta3.show();
		if (Ti.App.deployType != 'production') console.log('hola3', {});
	} else if ((_.isObject($datos.construccion_ano) || _.isString($datos.construccion_ano)) && _.isEmpty($datos.construccion_ano)) {
		require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
		var preguntarAlerta4_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta4 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x2674242197_traducir', 'Ingrese año de construcción de la vivienda'),
			buttonNames: preguntarAlerta4_opts
		});
		preguntarAlerta4.addEventListener('click', function(e) {
			var nulo = preguntarAlerta4_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta4.show();
		if (Ti.App.deployType != 'production') console.log('hola4', {});
	} else if ($datos.construccion_ano > anoactual == true || $datos.construccion_ano > anoactual == 'true') {
		require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
		var preguntarAlerta5_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta5 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x2118147629_traducir', 'El año tiene que ser menor al año actual'),
			buttonNames: preguntarAlerta5_opts
		});
		preguntarAlerta5.addEventListener('click', function(e) {
			var nulo = preguntarAlerta5_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta5.show();
		if (Ti.App.deployType != 'production') console.log('hola5', {});
	} else if ($datos.construccion_ano < (anoactual - 100) == true || $datos.construccion_ano < (anoactual - 100) == 'true') {
		require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
		var preguntarAlerta6_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta6 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x624168147_traducir', 'Tiene que tener máximo 100 años de antigüedad'),
			buttonNames: preguntarAlerta6_opts
		});
		preguntarAlerta6.addEventListener('click', function(e) {
			var nulo = preguntarAlerta6_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta6.show();
		if (Ti.App.deployType != 'production') console.log('hola6', {});
	} else if ($datos.otros_seguros_enlugar == 1 || $datos.otros_seguros_enlugar == '1') {
		if (Ti.App.deployType != 'production') console.log('hola7', {});
		if (_.isUndefined($datos.id_compania)) {
			require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
			var preguntarAlerta7_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta7 = Ti.UI.createAlertDialog({
				title: L('x3237162386_traducir', 'Atencion'),
				message: L('x1200064248_traducir', 'Seleccione compañía de seguros'),
				buttonNames: preguntarAlerta7_opts
			});
			preguntarAlerta7.addEventListener('click', function(e) {
				var nulo = preguntarAlerta7_opts[e.index];
				nulo = null;
				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta7.show();
			if (Ti.App.deployType != 'production') console.log('hola8', {});
		} else if ((_.isObject($datos.id_compania) || _.isString($datos.id_compania)) && _.isEmpty($datos.id_compania)) {
			require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
			var preguntarAlerta8_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta8 = Ti.UI.createAlertDialog({
				title: L('x3237162386_traducir', 'Atencion'),
				message: L('x1200064248_traducir', 'Seleccione compañía de seguros'),
				buttonNames: preguntarAlerta8_opts
			});
			preguntarAlerta8.addEventListener('click', function(e) {
				var nulo = preguntarAlerta8_opts[e.index];
				nulo = null;
				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta8.show();
		} else {
			require('vars')[_var_scopekey]['otro_valor'] = L('x4261170317', 'true');
			if (Ti.App.deployType != 'production') console.log('hola9', {});
		}
	} else {}
	if ($datos.asociado_hipotecario == 1 || $datos.asociado_hipotecario == '1') {
		if (Ti.App.deployType != 'production') console.log('hola10', {});
		if (_.isUndefined($datos.id_entidad_financiera)) {
			if (Ti.App.deployType != 'production') console.log('hola11', {});
			require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
			var preguntarAlerta9_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta9 = Ti.UI.createAlertDialog({
				title: L('x3237162386_traducir', 'Atencion'),
				message: L('x4118601873_traducir', 'Seleccione entidad financiera'),
				buttonNames: preguntarAlerta9_opts
			});
			preguntarAlerta9.addEventListener('click', function(e) {
				var nulo = preguntarAlerta9_opts[e.index];
				nulo = null;
				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta9.show();
		} else if ((_.isObject($datos.id_entidad_financiera) || _.isString($datos.id_entidad_financiera)) && _.isEmpty($datos.id_entidad_financiera)) {
			if (Ti.App.deployType != 'production') console.log('hola12', {});
			require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
			var preguntarAlerta10_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta10 = Ti.UI.createAlertDialog({
				title: L('x3237162386_traducir', 'Atencion'),
				message: L('x4118601873_traducir', 'Seleccione entidad financiera'),
				buttonNames: preguntarAlerta10_opts
			});
			preguntarAlerta10.addEventListener('click', function(e) {
				var nulo = preguntarAlerta10_opts[e.index];
				nulo = null;
				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta10.show();
		} else {
			require('vars')[_var_scopekey]['otro_valor'] = L('x4261170317', 'true');
		}
	} else {}
	if ($datos.inhabitable == 1 || $datos.inhabitable == '1') {
		if (_.isUndefined($datos.estimacion_meses)) {
			if (Ti.App.deployType != 'production') console.log('hola14', {});
			require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
			var preguntarAlerta11_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta11 = Ti.UI.createAlertDialog({
				title: L('x3237162386_traducir', 'Atencion'),
				message: L('x2127433848_traducir', 'Ingrese estimación de meses'),
				buttonNames: preguntarAlerta11_opts
			});
			preguntarAlerta11.addEventListener('click', function(e) {
				var nulo = preguntarAlerta11_opts[e.index];
				nulo = null;
				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta11.show();
		} else if ((_.isObject($datos.estimacion_meses) || _.isString($datos.estimacion_meses)) && _.isEmpty($datos.estimacion_meses)) {
			if (Ti.App.deployType != 'production') console.log('hola15', {});
			require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
			var preguntarAlerta12_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta12 = Ti.UI.createAlertDialog({
				title: L('x3237162386_traducir', 'Atencion'),
				message: L('x2127433848_traducir', 'Ingrese estimación de meses'),
				buttonNames: preguntarAlerta12_opts
			});
			preguntarAlerta12.addEventListener('click', function(e) {
				var nulo = preguntarAlerta12_opts[e.index];
				nulo = null;
				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta12.show();
		} else {
			if (Ti.App.deployType != 'production') console.log('hola16', {});
			require('vars')[_var_scopekey]['otro_valor'] = L('x4261170317', 'true');
		}
	}
	var todobien = ('todobien' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['todobien'] : '';
	if (todobien == true || todobien == 'true') {
		var otro_valor = ('otro_valor' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['otro_valor'] : '';
		if (otro_valor == true || otro_valor == 'true') {
			var sin_niveles = ('sin_niveles' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['sin_niveles'] : '';
			if (sin_niveles == true || sin_niveles == 'true') {
				var preguntarAlerta13_opts = [L('x1518866076_traducir', 'Aceptar')];
				var preguntarAlerta13 = Ti.UI.createAlertDialog({
					title: L('x3237162386_traducir', 'Atencion'),
					message: L('x3117604322_traducir', 'Ingrese al menos un nivel de la vivienda'),
					buttonNames: preguntarAlerta13_opts
				});
				preguntarAlerta13.addEventListener('click', function(e) {
					var nulo = preguntarAlerta13_opts[e.index];
					nulo = null;
					e.source.removeEventListener("click", arguments.callee);
				});
				preguntarAlerta13.show();
			} else {
				/** 
				 * Abrimos pantalla esta de acuerdo 
				 */
				$.widgetPreguntac.enviar({});
			}
		} else {
			var sin_niveles = ('sin_niveles' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['sin_niveles'] : '';
			if (sin_niveles == true || sin_niveles == 'true') {
				var preguntarAlerta14_opts = [L('x1518866076_traducir', 'Aceptar')];
				var preguntarAlerta14 = Ti.UI.createAlertDialog({
					title: L('x3237162386_traducir', 'Atencion'),
					message: L('x3117604322_traducir', 'Ingrese al menos un nivel de la vivienda'),
					buttonNames: preguntarAlerta14_opts
				});
				preguntarAlerta14.addEventListener('click', function(e) {
					var nulo = preguntarAlerta14_opts[e.index];
					nulo = null;
					e.source.removeEventListener("click", arguments.callee);
				});
				preguntarAlerta14.show();
			} else {
				$.widgetPreguntac.enviar({});
			}
		}
	}

}

function si_widgetPreguntac(e) {

	var evento = e;
	/** 
	 * Guardamos lo ingresado en el modelo 
	 */
	Alloy.Collections[$.datos.config.adapter.collection_name].add($.datos);
	$.datos.save();
	Alloy.Collections[$.datos.config.adapter.collection_name].fetch();
	/** 
	 * Limpiamos widget para liberar memoria ram 
	 */
	$.widgetModalmultiple.limpiar({});
	$.widgetModal.limpiar({});
	$.widgetModal3.limpiar({});
	/** 
	 * Enviamos a pantalla siniestro 
	 */
	Alloy.createController("siniestro_index", {}).getView().open();

}

function no_widgetPreguntac(e) {

	var evento = e;

}

$.widgetModalmultiple.init({
	titulo: L('x1313568614_traducir', 'Destino'),
	__id: 'ALL1151684903',
	oncerrar: Cerrar_widgetModalmultiple,
	hint: L('x2017856269_traducir', 'Seleccione destino'),
	color: 'verde',
	subtitulo: L('x1857779775_traducir', 'Indique los destinos'),
	top: 20,
	onafterinit: Afterinit_widgetModalmultiple
});

function Afterinit_widgetModalmultiple(e) {

	var evento = e;
	var ID_1600749054_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {} else {
			var eliminarModelo_i = Alloy.Collections.destino;
			var sql = "DELETE FROM " + eliminarModelo_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo_m = Alloy.Collections.destino;
				var insertarModelo_fila = Alloy.createModel('destino', {
					nombre: String.format(L('x1956100003_traducir', 'Casa%1$s'), item.toString()),
					id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
					id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item.toString()),
					pais: 1
				});
				insertarModelo_m.add(insertarModelo_fila);
				insertarModelo_fila.save();
				_.defer(function() {});
			});
		}
		/** 
		 * Obtenemos datos para selectores 
		 */
		var transformarModelo_i = Alloy.createCollection('destino');
		transformarModelo_i.fetch();
		var transformarModelo_src = require('helper').query2array(transformarModelo_i);
		var datos = [];
		_.each(transformarModelo_src, function(fila, pos) {
			var new_row = {};
			_.each(fila, function(x, llave) {
				var newkey = '';
				if (llave == 'nombre') newkey = 'label';
				if (llave == 'id_segured') newkey = 'valor';
				if (newkey != '') new_row[newkey] = fila[llave];
			});
			new_row['estado'] = 0;
			datos.push(new_row);
		});
		/** 
		 * Enviamos los datos al widget 
		 */
		$.widgetModalmultiple.update({
			data: datos
		});
	};
	var ID_1600749054 = setTimeout(ID_1600749054_func, 1000 * 0.2);

}

function Cerrar_widgetModalmultiple(e) {

	var evento = e;
	/** 
	 * Mostramos en la pantalla el valor seleccionado y actualizamos el modelo con lo seleccionado 
	 */
	$.widgetModalmultiple.update({});
	$.datos.set({
		destinos: evento.valores
	});
	if ('datos' in $) $datos = $.datos.toJSON();

}

function Change_Ao(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	$.datos.set({
		construccion_ano: elemento
	});
	if ('datos' in $) $datos = $.datos.toJSON();
	elemento = null, source = null;

}

function Change_ID_1568773770(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	if (elemento == true || elemento == 'true') {
		$.NO.setText('SI');

		$.datos.set({
			construccion_anexa: 1
		});
		if ('datos' in $) $datos = $.datos.toJSON();
	} else {
		$.NO.setText('NO');

		$.datos.set({
			construccion_anexa: 0
		});
		if ('datos' in $) $datos = $.datos.toJSON();
	}
	elemento = null;

}

function Change_ID_129828234(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	if (elemento == true || elemento == 'true') {
		$.NO4.setText('SI');

		$.datos.set({
			otros_seguros_enlugar: 1
		});
		if ('datos' in $) $datos = $.datos.toJSON();
		$.widgetModal.activar({});
	} else {
		$.NO4.setText('NO');

		$.datos.set({
			otros_seguros_enlugar: 0
		});
		if ('datos' in $) $datos = $.datos.toJSON();
		$.widgetModal.desactivar({});
	}
	elemento = null;

}

$.widgetModal.init({
	titulo: L('x86212863_traducir', 'COMPAÑIA'),
	cargando: L('x1740321226_traducir', 'cargando ..'),
	__id: 'ALL1379917834',
	onrespuesta: Respuesta_widgetModal,
	campo: L('x3363644313_traducir', 'Compañía'),
	onabrir: Abrir_widgetModal,
	color: 'verde',
	top: 10,
	seleccione: L('x3859572977_traducir', 'seleccione compañía'),
	activo: false,
	onafterinit: Afterinit_widgetModal
});

function Afterinit_widgetModal(e) {

	var evento = e;
	var ID_1948046515_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {} else {
			var eliminarModelo2_i = Alloy.Collections.compania;
			var sql = "DELETE FROM " + eliminarModelo2_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo2_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo2_i.trigger('remove');
			var item_index = 0;
			_.each('A,A,B,C,D,E,F,F,G'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo2_m = Alloy.Collections.compania;
				var insertarModelo2_fila = Alloy.createModel('compania', {
					nombre: String.format(L('x1904871496', '%1$s Empresa'), item.toString()),
					id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
					id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item_pos.toString()),
					pais: 1
				});
				insertarModelo2_m.add(insertarModelo2_fila);
				insertarModelo2_fila.save();
				_.defer(function() {});
			});
		}
		var transformarModelo2_i = Alloy.createCollection('compania');
		transformarModelo2_i.fetch();
		var transformarModelo2_src = require('helper').query2array(transformarModelo2_i);
		var datos = [];
		_.each(transformarModelo2_src, function(fila, pos) {
			var new_row = {};
			_.each(fila, function(x, llave) {
				var newkey = '';
				if (llave == 'nombre') newkey = 'valor';
				if (llave == 'id_segured') newkey = 'id_segured';
				if (newkey != '') new_row[newkey] = fila[llave];
			});
			datos.push(new_row);
		});
		$.widgetModal.data({
			data: datos
		});
	};
	var ID_1948046515 = setTimeout(ID_1948046515_func, 1000 * 0.2);

}

function Abrir_widgetModal(e) {

	var evento = e;

}

function Respuesta_widgetModal(e) {

	var evento = e;
	$.widgetModal.labels({
		valor: evento.valor
	});
	$.datos.set({
		id_compania: evento.item.id_segured
	});
	if ('datos' in $) $datos = $.datos.toJSON();

}

function Change_ID_222185281(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	if (elemento == true || elemento == 'true') {
		$.NO5.setText('SI');

		$.datos.set({
			asociado_hipotecario: 1
		});
		if ('datos' in $) $datos = $.datos.toJSON();
		$.widgetModal3.activar({});
	} else {
		$.NO5.setText('NO');

		$.datos.set({
			asociado_hipotecario: 0
		});
		if ('datos' in $) $datos = $.datos.toJSON();
		$.widgetModal3.desactivar({});
	}
	elemento = null;

}

$.widgetModal3.init({
	titulo: L('x2059554513_traducir', 'ENTIDAD'),
	cargando: L('x1740321226_traducir', 'cargando ..'),
	__id: 'ALL511044183',
	onrespuesta: Respuesta_widgetModal3,
	campo: L('x2538351238_traducir', 'Entidad Financiera'),
	onabrir: Abrir_widgetModal3,
	color: 'verde',
	seleccione: L('x3782298892_traducir', 'seleccione entidad'),
	activo: false,
	onafterinit: Afterinit_widgetModal3
});

function Afterinit_widgetModal3(e) {

	var evento = e;
	var ID_779848452_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {} else {
			var eliminarModelo3_i = Alloy.Collections.entidad_financiera;
			var sql = "DELETE FROM " + eliminarModelo3_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo3_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo3_i.trigger('remove');
			var item_index = 0;
			_.each('A,A,B,C,D,E,F,F,G'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo3_m = Alloy.Collections.entidad_financiera;
				var insertarModelo3_fila = Alloy.createModel('entidad_financiera', {
					nombre: String.format(L('x380244590', '%1$s Entidad'), item.toString()),
					id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
					id_segured: String.format(L('x1929201954_traducir', '20%1$s'), item_pos.toString()),
					pais: 1
				});
				insertarModelo3_m.add(insertarModelo3_fila);
				insertarModelo3_fila.save();
				_.defer(function() {});
			});
		}
		var transformarModelo3_i = Alloy.createCollection('entidad_financiera');
		transformarModelo3_i.fetch();
		var transformarModelo3_src = require('helper').query2array(transformarModelo3_i);
		var datos = [];
		_.each(transformarModelo3_src, function(fila, pos) {
			var new_row = {};
			_.each(fila, function(x, llave) {
				var newkey = '';
				if (llave == 'nombre') newkey = 'valor';
				if (llave == 'id_segured') newkey = 'id_segured';
				if (newkey != '') new_row[newkey] = fila[llave];
			});
			datos.push(new_row);
		});
		$.widgetModal3.data({
			data: datos
		});
	};
	var ID_779848452 = setTimeout(ID_779848452_func, 1000 * 0.2);

}

function Abrir_widgetModal3(e) {

	var evento = e;

}

function Respuesta_widgetModal3(e) {

	var evento = e;
	$.widgetModal3.labels({
		valor: evento.valor
	});
	$.datos.set({
		id_entidad_financiera: evento.item.id_segured
	});
	if ('datos' in $) $datos = $.datos.toJSON();

}

function Change_ID_1246703263(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	if (elemento == true || elemento == 'true') {
		$.NO6.setText('SI');

		$.Meses.setEditable(true);

		$.datos.set({
			inhabitable: 1
		});
		if ('datos' in $) $datos = $.datos.toJSON();
	} else {
		$.NO6.setText('NO');

		$.Meses.setEditable('false');

		$.datos.set({
			inhabitable: 0
		});
		if ('datos' in $) $datos = $.datos.toJSON();
	}
	elemento = null;

}

function Change_Meses(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	$.datos.set({
		estimacion_meses: elemento
	});
	if ('datos' in $) $datos = $.datos.toJSON();
	elemento = null, source = null;

}

function Click_vista18(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	Alloy.createController("nuevo_nivel", {}).getView().open();

}

function Itemclick_listado(e) {

	e.cancelBubble = true;
	var objeto = e.section.getItemAt(e.itemIndex);
	var modelo = {},
		_modelo = [];
	var fila = {},
		fila_bak = {},
		info = {
			_template: objeto.template,
			_what: [],
			_seccion_ref: e.section.getHeaderTitle(),
			_model_id: -1
		},
		_tmp = {
			objmap: {}
		};
	if ('itemId' in e) {
		info._model_id = e.itemId;
		modelo._id = info._model_id;
		if (info._seccion_ref != '' && info._seccion_ref in _bind4section) {
			modelo._collection = _bind4section[info._seccion_ref];
			_tmp._coll = modelo._collection;
		}
	}
	var findVariables = require('fvariables');
	_.each(_list_templates[info._template], function(obj_id, id) {
		_.each(obj_id, function(valor, prop) {
			var llaves = findVariables(valor, '{', '}');
			_.each(llaves, function(llave) {
				_tmp.objmap[llave] = {
					id: id,
					prop: prop
				};
				fila[llave] = objeto[id][prop];
				if (id == e.bindId) info._what.push(llave);
			});
		});
	});
	info._what = info._what.join(',');
	fila_bak = JSON.parse(JSON.stringify(fila));
	if (Ti.App.deployType != 'production') console.log('click en fila nivel', {});
	/** 
	 * Enviamos el parametro _dato para indicar cual sera el id del nivel a editar 
	 */
	Alloy.createController("editar_nivel", {
		'_fila': fila,
		'__master_model': (typeof modelo !== 'undefined') ? modelo : {},
		'__modelo': (typeof _modelo !== 'undefined' && _modelo.length > 0) ? _modelo[0] : {}
	}).getView().open();
	_tmp.changed = false;
	_tmp.diff_keys = [];
	_.each(fila, function(value1, prop) {
		var had_samekey = false;
		_.each(fila_bak, function(value2, prop2) {
			if (prop == prop2 && value1 == value2) {
				had_samekey = true;
			} else if (!_.has(fila_bak, prop) || !_.has(fila, prop2)) {
				has_samekey = true;
			}
		});
		if (!had_samekey) _tmp.diff_keys.push(prop);
	});
	if (_tmp.diff_keys.length > 0) _tmp.changed = true;
	if (_tmp.changed == true) {
		_.each(_tmp.diff_keys, function(llave) {
			objeto[_tmp.objmap[llave].id][_tmp.objmap[llave].prop] = fila[llave];
		});
		e.section.updateItemAt(e.itemIndex, objeto);
	}

}

function Postlayout_vista23(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.progreso.show();

}

function Postlayout_CARACTERISTICAS(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	/** 
	 * Cerramos pantalla datos basicos 
	 */
	Alloy.Events.trigger('_cerrar_insp', {
		pantalla: 'basicos'
	});

}

var preguntarOpciones_opts = [L('x3481285698_traducir', 'Asegurado no puede seguir'), L('x3077268071_traducir', ' Se me acabo la bateria'), L('x479989047_traducir', ' Tuve un accidente'), L('x2376009830_traducir', ' Cancelar')];
var preguntarOpciones = Ti.UI.createOptionDialog({
	title: L('x1670176792_traducir', 'RAZON PARA CANCELAR INSPECCION ACTUAL'),
	options: preguntarOpciones_opts
});
preguntarOpciones.addEventListener('click', function(e) {
	var resp = preguntarOpciones_opts[e.index];
	/** 
	 * Se recupera la variable para saber cual es el id_inspector 
	 */
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	/** 
	 * Se recupera la variable para saber cual es el codigo_identificador del inspector 
	 */
	var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
	if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
		var razon = "";
		if (resp == L('x3481285698_traducir', 'Asegurado no puede seguir')) {
			/** 
			 * Si el inspector eligio esta opcion la razon del cancelamiento de la inspeccion sera porque el asegurado no puede seguir 
			 */
			razon = "Asegurado no puede seguir";
		}
		if (resp == L('x1862794075_traducir', 'Se me acabo la bateria')) {
			/** 
			 * Si el inspector eligio esta opcion la razon del cancelamiento de la inspeccion sera porque el inspector se le acabara la bateria del celular 
			 */
			razon = "Se me acabo la bateria";
		}
		if (resp == L('x4012805111_traducir', 'Tuve un accidente')) {
			/** 
			 * Si el inspector eligio esta opcion la razon del cancelamiento de la inspeccion sera porque el inspector tuvo un accidente 
			 */
			razon = "Tuve un accidente";
		}
		if ((_.isObject(razon) || (_.isString(razon)) && !_.isEmpty(razon)) || _.isNumber(razon) || _.isBoolean(razon)) {
			/** 
			 * Se valida que la razon de cancelamiento tenga un motivo 
			 */
			if (Ti.App.deployType != 'production') console.log('llamando servicio cancelarTarea', {});
			/** 
			 * Se guarda una variable para saber en que pantalla se cancelo la inspeccion 
			 */
			require('vars')['insp_cancelada'] = L('x345484177_traducir', 'caracteristicas');
			/** 
			 * Se guarda el modelo con los datos que alcanz&#243; a rellenar el inspector 
			 */
			Alloy.Collections[$.datos.config.adapter.collection_name].add($.datos);
			$.datos.save();
			Alloy.Collections[$.datos.config.adapter.collection_name].fetch();
			/** 
			 * Recuperamos la url del servidor para posteriormente hacer el env&#237;o 
			 */
			var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
			/** 
			 * Despliega una vista para que el usuario vea que el envio de cancelamiento est&#225; en proceso 
			 */
			var vista23_visible = true;

			if (vista23_visible == 'si') {
				vista23_visible = true;
			} else if (vista23_visible == 'no') {
				vista23_visible = false;
			}
			$.vista23.setVisible(vista23_visible);

			/** 
			 * Se llama al servidor para enviar los datos de cancelamiento 
			 */
			var consultarURL = {};
			/** 
			 * en el caso de que el envio sea exitoso se hara lo siguiente 
			 */
			consultarURL.success = function(e) {
				var elemento = e,
					valor = e;
				/** 
				 * Se limpian las variables que usan los widgets y asi tambien liberar memoria 
				 */
				$.widgetModalmultiple.limpiar({});
				$.widgetModal.limpiar({});
				$.widgetModal3.limpiar({});
				/** 
				 * Se oculta la vista donde el usuario ve que el envio de cancelamiento est&#225; en proceso 
				 */
				var vista23_visible = false;

				if (vista23_visible == 'si') {
					vista23_visible = true;
				} else if (vista23_visible == 'no') {
					vista23_visible = false;
				}
				$.vista23.setVisible(vista23_visible);

				/** 
				 * Se envia a la pantalla de firma para el posterior envio de los datos que alcanz&#243; a rellenar el inspector 
				 */
				Alloy.createController("firma_index", {}).getView().open();
				elemento = null, valor = null;
			};
			/** 
			 * En el caso de que el envio hacia el servidor no haya sido exitoso se hara lo siguiente 
			 */
			consultarURL.error = function(e) {
				var elemento = e,
					valor = e;
				/** 
				 * Log para ver en consola cual fue el error 
				 */
				if (Ti.App.deployType != 'production') console.log('hubo error llamando cancelarTarea, la agregamos a cola de salida', {
					"elemento": elemento
				});
				if (Ti.App.deployType != 'production') console.log('agregando servicio cancelarTarea a cola', {});
				/** 
				 * Se crea una estructura con los datos que iban a ser enviados al servidor, notificando que la inspeccion fue cancelada por algun motivo 
				 */
				var datos = {
					id_inspector: seltarea.id_inspector,
					codigo_identificador: inspector.codigo_identificador,
					id_server: seltarea.id_server,
					num_caso: seltarea.num_caso,
					razon: razon
				};
				var insertarModelo4_m = Alloy.Collections.cola;
				var insertarModelo4_fila = Alloy.createModel('cola', {
					data: JSON.stringify(datos),
					id_tarea: seltarea.id_server,
					tipo: 'cancelar'
				});
				insertarModelo4_m.add(insertarModelo4_fila);
				insertarModelo4_fila.save();
				_.defer(function() {});
				/** 
				 * Se oculta la vista donde el usuario ve que el envio de cancelamiento est&#225; en proceso 
				 */
				var vista23_visible = false;

				if (vista23_visible == 'si') {
					vista23_visible = true;
				} else if (vista23_visible == 'no') {
					vista23_visible = false;
				}
				$.vista23.setVisible(vista23_visible);

				/** 
				 * Se envia a la pantalla de firma para el posterior envio de los datos que alcanz&#243; a rellenar el inspector 
				 */
				Alloy.createController("firma_index", {}).getView().open();
				elemento = null, valor = null;
			};
			require('helper').ajaxUnico('consultarURL', '' + String.format(L('x3244141284', '%1$scancelarTarea'), url_server.toString()) + '', 'POST', {
				id_inspector: seltarea.id_inspector,
				codigo_identificador: inspector.codigo_identificador,
				id_tarea: seltarea.id_server,
				num_caso: seltarea.num_caso,
				mensaje: razon,
				opcion: 0,
				tipo: 1
			}, 15000, consultarURL);
		}
	}
	resp = null;
});
var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
	$.datos.set({
		id_inspeccion: seltarea.id_server
	});
	if ('datos' in $) $datos = $.datos.toJSON();
	/** 
	 * restringimos niveles mostrados a la inspeccion actual (por si los temporales aun tienen datos sin enviar) 
	 */
	consultarModelo_filter = function(coll) {
		var filtered = coll.filter(function(m) {
			var _tests = [],
				_all_true = false,
				model = m.toJSON();
			_tests.push((model.id_inspeccion == seltarea.id_server));
			var _all_true_s = _.uniq(_tests);
			_all_true = (_all_true_s.length == 1 && _all_true_s[0] == true) ? true : false;
			return _all_true;
		});
		filtered = _.toArray(filtered);
		return filtered;
	};
	_.defer(function() {
		Alloy.Collections.insp_niveles.fetch();
	});
	/** 
	 * Definimos campos defaults 
	 */
	$.datos.set({
		construccion_anexa: 0,
		otros_seguros_enlugar: 0,
		asociado_hipotecario: 0,
		inhabitable: 0
	});
	if ('datos' in $) $datos = $.datos.toJSON();
}
/** 
 * Cerramos esta pantalla cuando es llamada en la pantalla siguente (Siniestro) 
 */
_my_events['_cerrar_insp,ID_231485652'] = function(evento) {
	if (!_.isUndefined(evento.pantalla)) {
		if (evento.pantalla == L('x345484177_traducir', 'caracteristicas')) {
			if (Ti.App.deployType != 'production') console.log('debug cerrando caracteristicas', {});
			var ID_1436370236_trycatch = {
				error: function(e) {
					if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
				}
			};
			try {
				ID_1436370236_trycatch.error = function(evento) {
					if (Ti.App.deployType != 'production') console.log('error cerrando caracteristicas', {});
				};
				$.widgetModalmultiple.limpiar({});
				$.widgetModal.limpiar({});
				$.widgetModal3.limpiar({});
				$.CARACTERISTICAS.close();
			} catch (e) {
				ID_1436370236_trycatch.error(e);
			}
		}
	} else {
		if (Ti.App.deployType != 'production') console.log('debug cerrando (todas) caracteristicas', {});
		var ID_1962200830_trycatch = {
			error: function(e) {
				if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
			}
		};
		try {
			ID_1962200830_trycatch.error = function(evento) {
				if (Ti.App.deployType != 'production') console.log('error cerrando caracteristicas', {});
			};
			$.widgetModalmultiple.limpiar({});
			$.widgetModal.limpiar({});
			$.widgetModal3.limpiar({});
			$.CARACTERISTICAS.close();
		} catch (e) {
			ID_1962200830_trycatch.error(e);
		}
	}
};
Alloy.Events.on('_cerrar_insp', _my_events['_cerrar_insp,ID_231485652']);

if (OS_IOS || OS_ANDROID) {
	$.CARACTERISTICAS.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}
//$.CARACTERISTICAS.open();
