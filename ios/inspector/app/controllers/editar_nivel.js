var _bind4section = {};
var _list_templates = {};
var $nivel = $.nivel.toJSON();

$.EDITAR_NIVEL_window.setTitleAttributes({
	color: 'WHITE'
});
var _activity;
if (OS_ANDROID) {
	_activity = $.EDITAR_NIVEL.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'EDITAR_NIVEL';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.EDITAR_NIVEL_window.addEventListener('open', function(e) {
		abx.setStatusbarColor("#FFFFFF");
		abx.setBackgroundColor("#8ce5bd");
	});
}

function Click_vista41(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	/** 
	 * Limpiamos widget para liberar memoria ram 
	 */
	$.widgetModalmultiple8.limpiar({});
	$.widgetModalmultiple9.limpiar({});
	$.widgetModalmultiple10.limpiar({});
	$.widgetModalmultiple11.limpiar({});
	$.widgetModalmultiple12.limpiar({});
	$.widgetModalmultiple13.limpiar({});
	$.EDITAR_NIVEL.close();

}

function Click_vista42(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	/** 
	 * Obtenemos el ano actual 
	 */
	var d = new Date();
	var anoactual = d.getFullYear();
	if (_.isUndefined($nivel.nombre)) {
		/** 
		 * Verificamos que los campos ingresados esten correctos 
		 */
		var preguntarAlerta25_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta25 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x1734272038_traducir', 'Ingrese nombre del nivel'),
			buttonNames: preguntarAlerta25_opts
		});
		preguntarAlerta25.addEventListener('click', function(e) {
			var nulo = preguntarAlerta25_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta25.show();
	} else if ((_.isObject($nivel.nombre) || _.isString($nivel.nombre)) && _.isEmpty($nivel.nombre)) {
		var preguntarAlerta26_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta26 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x1734272038_traducir', 'Ingrese nombre del nivel'),
			buttonNames: preguntarAlerta26_opts
		});
		preguntarAlerta26.addEventListener('click', function(e) {
			var nulo = preguntarAlerta26_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta26.show();
	} else if (_.isUndefined($nivel.piso)) {
		var preguntarAlerta27_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta27 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x2669747147_traducir', 'Ingrese Nº de piso'),
			buttonNames: preguntarAlerta27_opts
		});
		preguntarAlerta27.addEventListener('click', function(e) {
			var nulo = preguntarAlerta27_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta27.show();
	} else if (_.isUndefined($nivel.ano)) {
		var preguntarAlerta28_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta28 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x853726930_traducir', 'Ingrese año de construcción del nivel'),
			buttonNames: preguntarAlerta28_opts
		});
		preguntarAlerta28.addEventListener('click', function(e) {
			var nulo = preguntarAlerta28_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta28.show();
	} else if ($nivel.ano < (anoactual - 100) == true || $nivel.ano < (anoactual - 100) == 'true') {
		if (Ti.App.deployType != 'production') console.log('ano mayor', {});
		require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
		var preguntarAlerta29_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta29 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x624168147_traducir', 'Tiene que tener máximo 100 años de antigüedad'),
			buttonNames: preguntarAlerta29_opts
		});
		preguntarAlerta29.addEventListener('click', function(e) {
			var nulo = preguntarAlerta29_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta29.show();
	} else if ($nivel.ano > anoactual == true || $nivel.ano > anoactual == 'true') {
		if (Ti.App.deployType != 'production') console.log('ano mayor', {});
		require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
		var preguntarAlerta30_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta30 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x2118147629_traducir', 'El año tiene que ser menor al año actual'),
			buttonNames: preguntarAlerta30_opts
		});
		preguntarAlerta30.addEventListener('click', function(e) {
			var nulo = preguntarAlerta30_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta30.show();
	} else if (_.isUndefined($nivel.largo)) {
		var preguntarAlerta31_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta31 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x779559340_traducir', 'Ingrese largo del nivel'),
			buttonNames: preguntarAlerta31_opts
		});
		preguntarAlerta31.addEventListener('click', function(e) {
			var nulo = preguntarAlerta31_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta31.show();
	} else if (_.isUndefined($nivel.ancho)) {
		var preguntarAlerta32_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta32 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x2682783608_traducir', 'Ingrese ancho del nivel'),
			buttonNames: preguntarAlerta32_opts
		});
		preguntarAlerta32.addEventListener('click', function(e) {
			var nulo = preguntarAlerta32_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta32.show();
	} else if (_.isUndefined($nivel.alto)) {
		var preguntarAlerta33_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta33 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x4120715490_traducir', 'Ingrese la altura del nivel'),
			buttonNames: preguntarAlerta33_opts
		});
		preguntarAlerta33.addEventListener('click', function(e) {
			var nulo = preguntarAlerta33_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta33.show();
	} else if (_.isUndefined($nivel.ids_estructuras_soportantes)) {
		var preguntarAlerta34_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta34 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x3045996758_traducir', 'Seleccione la estructura soportante del nivel'),
			buttonNames: preguntarAlerta34_opts
		});
		preguntarAlerta34.addEventListener('click', function(e) {
			var nulo = preguntarAlerta34_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta34.show();
	} else {
		if (!_.isUndefined(args._fila)) {
			/** 
			 * Eliminamos el nivel que estamos editando de la tabla y reemplazamos por el que tenemos actualmente con datos editados 
			 */
			/** 
			 * Eliminamos el nivel que estamos editando de la tabla y reemplazamos por el que tenemos actualmente con datos editados 
			 */
			var eliminarModelo10_i = Alloy.Collections.insp_niveles;
			var sql = 'DELETE FROM ' + eliminarModelo10_i.config.adapter.collection_name + ' WHERE id=\'' + args._fila.id + '\'';
			var db = Ti.Database.open(eliminarModelo10_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo10_i.trigger('remove');
		}
		Alloy.Collections[$.nivel.config.adapter.collection_name].add($.nivel);
		$.nivel.save();
		Alloy.Collections[$.nivel.config.adapter.collection_name].fetch();
		var ID_373252409_func = function() {
			/** 
			 * Limpiamos widget para liberar memoria ram 
			 */
			$.widgetModalmultiple8.limpiar({});
			$.widgetModalmultiple9.limpiar({});
			$.widgetModalmultiple10.limpiar({});
			$.widgetModalmultiple11.limpiar({});
			$.widgetModalmultiple12.limpiar({});
			$.widgetModalmultiple13.limpiar({});
			$.EDITAR_NIVEL.close();
		};
		var ID_373252409 = setTimeout(ID_373252409_func, 1000 * 0.1);
	}
}

function Focus_IngreseNombre2(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	elemento = null, source = null;

}

function Change_IngreseNombre2(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	$.nivel.set({
		nombre: elemento
	});
	if ('nivel' in $) $nivel = $.nivel.toJSON();
	elemento = null, source = null;

}

function Focus_campo6(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	elemento = null, source = null;

}

function Change_campo6(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	/** 
	 * Actualizamos el modelo con el piso ingresado 
	 */
	$.nivel.set({
		piso: elemento
	});
	if ('nivel' in $) $nivel = $.nivel.toJSON();
	elemento = null, source = null;

}

function Focus_IngreseAo(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	elemento = null, source = null;

}

function Change_IngreseAo(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	$.nivel.set({
		ano: elemento
	});
	if ('nivel' in $) $nivel = $.nivel.toJSON();
	elemento = null, source = null;

}

function Change_campo7(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	/** 
	 * Obtenemos el valor escrito en el campo de ancho 
	 */
	var ancho;
	ancho = $.campo8.getValue();

	if ((_.isObject(ancho) || (_.isString(ancho)) && !_.isEmpty(ancho)) || _.isNumber(ancho) || _.isBoolean(ancho)) {
		/** 
		 * Calculamos la superficie 
		 */
		if ((_.isObject(elemento) || (_.isString(elemento)) && !_.isEmpty(elemento)) || _.isNumber(elemento) || _.isBoolean(elemento)) {
			/** 
			 * nos aseguramos que ancho y valor de campo sean decimales con puntos (flotante), y no comas (espa&#241;ol) 
			 */
			var nuevo = parseFloat(ancho.split(',').join('.')) * parseFloat(elemento.split(',').join('.'));
			$.nivel.set({
				superficie: nuevo
			});
			if ('nivel' in $) $nivel = $.nivel.toJSON();
		}
	}
	/** 
	 * Actualizamos el valor del largo del recinto 
	 */
	$.nivel.set({
		largo: elemento
	});
	if ('nivel' in $) $nivel = $.nivel.toJSON();
	elemento = null, source = null;

}

function Focus_campo7(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	elemento = null, source = null;

}

function Change_campo8(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	var largo;
	largo = $.campo7.getValue();

	if ((_.isObject(largo) || (_.isString(largo)) && !_.isEmpty(largo)) || _.isNumber(largo) || _.isBoolean(largo)) {
		if ((_.isObject(elemento) || (_.isString(elemento)) && !_.isEmpty(elemento)) || _.isNumber(elemento) || _.isBoolean(elemento)) {
			/** 
			 * nos aseguramos que largo y valor de campo sean decimales con puntos (flotante), y no comas (espa&#241;ol) 
			 */
			var nuevo = parseFloat(largo.split(',').join('.')) * parseFloat(elemento.split(',').join('.'));
			$.nivel.set({
				superficie: nuevo
			});
			if ('nivel' in $) $nivel = $.nivel.toJSON();
		}
	}
	$.nivel.set({
		ancho: elemento
	});
	if ('nivel' in $) $nivel = $.nivel.toJSON();
	elemento = null, source = null;

}

function Focus_campo8(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	elemento = null, source = null;

}

function Change_campo9(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	$.nivel.set({
		alto: elemento
	});
	if ('nivel' in $) $nivel = $.nivel.toJSON();
	elemento = null, source = null;

}

function Focus_campo9(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	elemento = null, source = null;

}

$.widgetModalmultiple8.init({
	titulo: L('x1975271086_traducir', 'Estructura Soportante'),
	cargando: L('x1740321226_traducir', 'cargando ..'),
	__id: 'ALL409036750',
	oncerrar: Cerrar_widgetModalmultiple8,
	hint: L('x2898603391_traducir', 'Seleccione estructura'),
	color: 'verde',
	subtitulo: L('x4011106049_traducir', 'Indique los tipos'),
	top: 5,
	onclick: Click_widgetModalmultiple8,
	onafterinit: Afterinit_widgetModalmultiple8
});

function Click_widgetModalmultiple8(e) {

	var evento = e;

}

function Cerrar_widgetModalmultiple8(e) {

	var evento = e;
	$.widgetModalmultiple8.update({});
	$.nivel.set({
		ids_estructuras_soportantes: evento.valores
	});
	if ('nivel' in $) $nivel = $.nivel.toJSON();

}

function Afterinit_widgetModalmultiple8(e) {

	var evento = e;
	/** 
	 * ejecutamos desfasado para no congelar el hilo. 
	 */
	var ID_764832750_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		var nivel_editado = ('nivel_editado' in require('vars')) ? require('vars')['nivel_editado'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			var consultarModelo12_i = Alloy.createCollection('estructura_soportante');
			var consultarModelo12_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
			consultarModelo12_i.fetch({
				query: 'SELECT * FROM estructura_soportante WHERE pais_texto=\'' + pais[0].nombre + '\''
			});
			var estructura = require('helper').query2array(consultarModelo12_i);
			var padre_index = 0;
			_.each(estructura, function(padre, padre_pos, padre_list) {
				padre_index += 1;
				if (_.isNull(nivel_editado.ids_estructuras_soportantes)) {
					/** 
					 * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
					 */
					padre._estado = 0;
				} else if (nivel_editado.ids_estructuras_soportantes.indexOf(padre.id_segured) != -1) {
					padre._estado = 1;
				} else {
					padre._estado = 0;
				}
			});
			var datos = [];
			_.each(estructura, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'label';
					if (llave == 'id_segured') newkey = 'valor';
					if (llave == '_estado') newkey = 'estado';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
			$.widgetModalmultiple8.update({
				data: datos
			});
		} else {
			/** 
			 * cargamos datos dummy para cuando compilamos de forma individual. 
			 */
			var eliminarModelo11_i = Alloy.Collections.estructura_soportante;
			var sql = "DELETE FROM " + eliminarModelo11_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo11_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo11_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo11_m = Alloy.Collections.estructura_soportante;
				var insertarModelo11_fila = Alloy.createModel('estructura_soportante', {
					nombre: String.format(L('x1088195980_traducir', 'Estructura%1$s'), item.toString()),
					id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
					pais_texto: 'Chile',
					id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item.toString()),
					pais: 1
				});
				insertarModelo11_m.add(insertarModelo11_fila);
				insertarModelo11_fila.save();
			});
			var consultarModelo13_i = Alloy.createCollection('estructura_soportante');
			var consultarModelo13_i_where = '';
			consultarModelo13_i.fetch();
			var estructura = require('helper').query2array(consultarModelo13_i);
			var padre_index = 0;
			_.each(estructura, function(padre, padre_pos, padre_list) {
				padre_index += 1;
				if (_.isNull(nivel_editado.ids_estructuras_soportantes)) {
					/** 
					 * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
					 */
					padre._estado = 0;
				} else if (nivel_editado.ids_estructuras_soportantes.indexOf(padre.id_segured) != -1) {
					padre._estado = 1;
				} else {
					padre._estado = 0;
				}
			});
			var datos = [];
			_.each(estructura, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'label';
					if (llave == 'id_segured') newkey = 'valor';
					if (llave == '_estado') newkey = 'estado';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
			$.widgetModalmultiple8.update({
				data: datos
			});
		}
	};
	var ID_764832750 = setTimeout(ID_764832750_func, 1000 * 0.2);

}

$.widgetModalmultiple9.init({
	titulo: L('x1219835481_traducir', 'Muros / Tabiques'),
	cargando: L('x1740321226_traducir', 'cargando ..'),
	__id: 'ALL1893667930',
	oncerrar: Cerrar_widgetModalmultiple9,
	hint: L('x2879998099_traducir', 'Seleccione muros'),
	color: 'verde',
	subtitulo: L('x4011106049_traducir', 'Indique los tipos'),
	top: 5,
	onclick: Click_widgetModalmultiple9,
	onafterinit: Afterinit_widgetModalmultiple9
});

function Click_widgetModalmultiple9(e) {

	var evento = e;

}

function Cerrar_widgetModalmultiple9(e) {

	var evento = e;
	$.widgetModalmultiple9.update({});
	$.nivel.set({
		ids_muros: evento.valores
	});
	if ('nivel' in $) $nivel = $.nivel.toJSON();

}

function Afterinit_widgetModalmultiple9(e) {

	var evento = e;
	/** 
	 * ejecutamos desfasado para no congelar el hilo. 
	 */
	var ID_34250152_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		var nivel_editado = ('nivel_editado' in require('vars')) ? require('vars')['nivel_editado'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			var consultarModelo14_i = Alloy.createCollection('muros_tabiques');
			var consultarModelo14_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
			consultarModelo14_i.fetch({
				query: 'SELECT * FROM muros_tabiques WHERE pais_texto=\'' + pais[0].nombre + '\''
			});
			var tabiques = require('helper').query2array(consultarModelo14_i);
			var padre_index = 0;
			_.each(tabiques, function(padre, padre_pos, padre_list) {
				padre_index += 1;
				if (_.isNull(nivel_editado.ids_muros)) {
					/** 
					 * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
					 */
					padre._estado = 0;
				} else if (nivel_editado.ids_muros.indexOf(padre.id_segured) != -1) {
					padre._estado = 1;
				} else {
					padre._estado = 0;
				}
			});
			var datos = [];
			_.each(tabiques, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'label';
					if (llave == 'id_segured') newkey = 'valor';
					if (llave == '_estado') newkey = 'estado';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
			$.widgetModalmultiple9.update({
				data: datos
			});
		} else {
			/** 
			 * cargamos datos dummy para cuando compilamos de forma individual. 
			 */
			var eliminarModelo12_i = Alloy.Collections.muros_tabiques;
			var sql = "DELETE FROM " + eliminarModelo12_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo12_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo12_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo12_m = Alloy.Collections.muros_tabiques;
				var insertarModelo12_fila = Alloy.createModel('muros_tabiques', {
					nombre: String.format(L('x3565664878_traducir', 'Muros%1$s'), item.toString()),
					id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
					pais_texto: 'Chile',
					id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item.toString()),
					pais: 1
				});
				insertarModelo12_m.add(insertarModelo12_fila);
				insertarModelo12_fila.save();
			});
			var consultarModelo15_i = Alloy.createCollection('muros_tabiques');
			var consultarModelo15_i_where = '';
			consultarModelo15_i.fetch();
			var muros = require('helper').query2array(consultarModelo15_i);
			var padre_index = 0;
			_.each(muros, function(padre, padre_pos, padre_list) {
				padre_index += 1;
				if (_.isNull(nivel_editado.ids_muros)) {
					/** 
					 * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
					 */
					padre._estado = 0;
				} else if (nivel_editado.ids_muros.indexOf(padre.id_segured) != -1) {
					padre._estado = 1;
				} else {
					padre._estado = 0;
				}
			});
			var datos = [];
			_.each(muros, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'label';
					if (llave == 'id_segured') newkey = 'valor';
					if (llave == '_estado') newkey = 'estado';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
			$.widgetModalmultiple9.update({
				data: datos
			});
		}
	};
	var ID_34250152 = setTimeout(ID_34250152_func, 1000 * 0.2);

}

$.widgetModalmultiple10.init({
	titulo: L('x3327059844_traducir', 'Entrepisos'),
	cargando: L('x1740321226_traducir', 'cargando ..'),
	__id: 'ALL1168052393',
	oncerrar: Cerrar_widgetModalmultiple10,
	hint: L('x2146928948_traducir', 'Seleccione entrepisos'),
	color: 'verde',
	subtitulo: L('x4011106049_traducir', 'Indique los tipos'),
	top: 5,
	onclick: Click_widgetModalmultiple10,
	onafterinit: Afterinit_widgetModalmultiple10
});

function Click_widgetModalmultiple10(e) {

	var evento = e;

}

function Cerrar_widgetModalmultiple10(e) {

	var evento = e;
	$.widgetModalmultiple10.update({});
	$.nivel.set({
		ids_entrepisos: evento.valores
	});
	if ('nivel' in $) $nivel = $.nivel.toJSON();

}

function Afterinit_widgetModalmultiple10(e) {

	var evento = e;
	var ID_1681776986_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		var nivel_editado = ('nivel_editado' in require('vars')) ? require('vars')['nivel_editado'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			var consultarModelo16_i = Alloy.createCollection('entrepisos');
			var consultarModelo16_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
			consultarModelo16_i.fetch({
				query: 'SELECT * FROM entrepisos WHERE pais_texto=\'' + pais[0].nombre + '\''
			});
			var entrepisos = require('helper').query2array(consultarModelo16_i);
			var padre_index = 0;
			_.each(entrepisos, function(padre, padre_pos, padre_list) {
				padre_index += 1;
				if (_.isNull(nivel_editado.ids_entrepisos)) {
					/** 
					 * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
					 */
					padre._estado = 0;
				} else if (nivel_editado.ids_entrepisos.indexOf(padre.id_segured) != -1) {
					padre._estado = 1;
				} else {
					padre._estado = 0;
				}
			});
			var datos = [];
			_.each(entrepisos, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'label';
					if (llave == 'id_segured') newkey = 'valor';
					if (llave == '_estado') newkey = 'estado';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
			$.widgetModalmultiple10.update({
				data: datos
			});
		} else {
			var eliminarModelo13_i = Alloy.Collections.entrepisos;
			var sql = "DELETE FROM " + eliminarModelo13_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo13_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo13_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo13_m = Alloy.Collections.entrepisos;
				var insertarModelo13_fila = Alloy.createModel('entrepisos', {
					nombre: String.format(L('x2266735154_traducir', 'Entrepiso%1$s'), item.toString()),
					id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
					pais_texto: 'Chile',
					id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item.toString()),
					pais: 1
				});
				insertarModelo13_m.add(insertarModelo13_fila);
				insertarModelo13_fila.save();
			});
			var consultarModelo17_i = Alloy.createCollection('entrepisos');
			var consultarModelo17_i_where = '';
			consultarModelo17_i.fetch();
			var entrepisos = require('helper').query2array(consultarModelo17_i);
			var padre_index = 0;
			_.each(entrepisos, function(padre, padre_pos, padre_list) {
				padre_index += 1;
				if (_.isNull(nivel_editado.ids_entrepisos)) {
					/** 
					 * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
					 */
					padre._estado = 0;
				} else if (nivel_editado.ids_entrepisos.indexOf(padre.id_segured) != -1) {
					padre._estado = 1;
				} else {
					padre._estado = 0;
				}
			});
			var datos = [];
			_.each(entrepisos, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'label';
					if (llave == 'id_segured') newkey = 'valor';
					if (llave == '_estado') newkey = 'estado';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
			$.widgetModalmultiple10.update({
				data: datos
			});
		}
	};
	var ID_1681776986 = setTimeout(ID_1681776986_func, 1000 * 0.2);

}

$.widgetModalmultiple11.init({
	titulo: L('x591862035_traducir', 'Pavimentos'),
	cargando: L('x1740321226_traducir', 'cargando ..'),
	__id: 'ALL400889681',
	oncerrar: Cerrar_widgetModalmultiple11,
	hint: L('x2600368035_traducir', 'Seleccione pavimentos'),
	color: 'verde',
	subtitulo: L('x4011106049_traducir', 'Indique los tipos'),
	top: 5,
	onclick: Click_widgetModalmultiple11,
	onafterinit: Afterinit_widgetModalmultiple11
});

function Click_widgetModalmultiple11(e) {

	var evento = e;

}

function Cerrar_widgetModalmultiple11(e) {

	var evento = e;
	$.widgetModalmultiple11.update({});
	$.nivel.set({
		ids_pavimentos: evento.valores
	});
	if ('nivel' in $) $nivel = $.nivel.toJSON();

}

function Afterinit_widgetModalmultiple11(e) {

	var evento = e;
	var ID_1696478889_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		var nivel_editado = ('nivel_editado' in require('vars')) ? require('vars')['nivel_editado'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			var consultarModelo18_i = Alloy.createCollection('pavimento');
			var consultarModelo18_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
			consultarModelo18_i.fetch({
				query: 'SELECT * FROM pavimento WHERE pais_texto=\'' + pais[0].nombre + '\''
			});
			var pavimentos = require('helper').query2array(consultarModelo18_i);
			var padre_index = 0;
			_.each(pavimentos, function(padre, padre_pos, padre_list) {
				padre_index += 1;
				if (_.isNull(nivel_editado.ids_pavimentos)) {
					/** 
					 * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
					 */
					padre._estado = 0;
				} else if (nivel_editado.ids_pavimentos.indexOf(padre.id_segured) != -1) {
					padre._estado = 1;
				} else {
					padre._estado = 0;
				}
			});
			var datos = [];
			_.each(pavimentos, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'label';
					if (llave == 'id_segured') newkey = 'valor';
					if (llave == '_estado') newkey = 'estado';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
			$.widgetModalmultiple11.update({
				data: datos
			});
		} else {
			var eliminarModelo14_i = Alloy.Collections.pavimento;
			var sql = "DELETE FROM " + eliminarModelo14_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo14_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo14_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo14_m = Alloy.Collections.pavimento;
				var insertarModelo14_fila = Alloy.createModel('pavimento', {
					nombre: String.format(L('x427067467_traducir', 'Pavimento%1$s'), item.toString()),
					id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
					pais_texto: 'Chile',
					id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item.toString()),
					pais: 1
				});
				insertarModelo14_m.add(insertarModelo14_fila);
				insertarModelo14_fila.save();
			});
			var consultarModelo19_i = Alloy.createCollection('pavimento');
			var consultarModelo19_i_where = '';
			consultarModelo19_i.fetch();
			var pavimentos = require('helper').query2array(consultarModelo19_i);
			var padre_index = 0;
			_.each(pavimentos, function(padre, padre_pos, padre_list) {
				padre_index += 1;
				if (_.isNull(nivel_editado.ids_pavimentos)) {
					/** 
					 * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
					 */
					padre._estado = 0;
				} else if (nivel_editado.ids_pavimentos.indexOf(padre.id_segured) != -1) {
					padre._estado = 1;
				} else {
					padre._estado = 0;
				}
			});
			var datos = [];
			_.each(pavimentos, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'label';
					if (llave == 'id_segured') newkey = 'valor';
					if (llave == '_estado') newkey = 'estado';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
			$.widgetModalmultiple11.update({
				data: datos
			});
		}
	};
	var ID_1696478889 = setTimeout(ID_1696478889_func, 1000 * 0.2);

}

$.widgetModalmultiple12.init({
	titulo: L('x1866523485_traducir', 'Estruct. cubierta'),
	cargando: L('x1740321226_traducir', 'cargando ..'),
	__id: 'ALL252896651',
	oncerrar: Cerrar_widgetModalmultiple12,
	hint: L('x2460890829_traducir', 'Seleccione e.cubiertas'),
	color: 'verde',
	subtitulo: L('x4011106049_traducir', 'Indique los tipos'),
	top: 5,
	onclick: Click_widgetModalmultiple12,
	onafterinit: Afterinit_widgetModalmultiple12
});

function Click_widgetModalmultiple12(e) {

	var evento = e;

}

function Cerrar_widgetModalmultiple12(e) {

	var evento = e;
	$.widgetModalmultiple12.update({});
	$.nivel.set({
		ids_estructura_cubiera: evento.valores
	});
	if ('nivel' in $) $nivel = $.nivel.toJSON();

}

function Afterinit_widgetModalmultiple12(e) {

	var evento = e;
	var ID_1080255621_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		var nivel_editado = ('nivel_editado' in require('vars')) ? require('vars')['nivel_editado'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			var consultarModelo20_i = Alloy.createCollection('estructura_cubierta');
			var consultarModelo20_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
			consultarModelo20_i.fetch({
				query: 'SELECT * FROM estructura_cubierta WHERE pais_texto=\'' + pais[0].nombre + '\''
			});
			var ecubiertas = require('helper').query2array(consultarModelo20_i);
			var padre_index = 0;
			_.each(ecubiertas, function(padre, padre_pos, padre_list) {
				padre_index += 1;
				if (_.isNull(nivel_editado.ids_estructura_cubiera)) {
					/** 
					 * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
					 */
					padre._estado = 0;
				} else if (nivel_editado.ids_estructura_cubiera.indexOf(padre.id_segured) != -1) {
					padre._estado = 1;
				} else {
					padre._estado = 0;
				}
			});
			var datos = [];
			_.each(ecubiertas, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'label';
					if (llave == 'id_segured') newkey = 'valor';
					if (llave == '_estado') newkey = 'estado';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
			$.widgetModalmultiple12.update({
				data: datos
			});
		} else {
			var eliminarModelo15_i = Alloy.Collections.estructura_cubierta;
			var sql = "DELETE FROM " + eliminarModelo15_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo15_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo15_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo15_m = Alloy.Collections.estructura_cubierta;
				var insertarModelo15_fila = Alloy.createModel('estructura_cubierta', {
					nombre: String.format(L('x1686539481_traducir', 'Estru Cubierta %1$s'), item.toString()),
					id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
					pais_texto: 'Chile',
					id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item.toString()),
					pais: 1
				});
				insertarModelo15_m.add(insertarModelo15_fila);
				insertarModelo15_fila.save();
			});
			var consultarModelo21_i = Alloy.createCollection('estructura_cubierta');
			var consultarModelo21_i_where = '';
			consultarModelo21_i.fetch();
			var ecubiertas = require('helper').query2array(consultarModelo21_i);
			var padre_index = 0;
			_.each(ecubiertas, function(padre, padre_pos, padre_list) {
				padre_index += 1;
				if (_.isNull(nivel_editado.ids_estructura_cubiera)) {
					/** 
					 * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
					 */
					padre._estado = 0;
				} else if (nivel_editado.ids_estructura_cubiera.indexOf(padre.id_segured) != -1) {
					padre._estado = 1;
				} else {
					padre._estado = 0;
				}
			});
			var datos = [];
			_.each(ecubiertas, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'label';
					if (llave == 'id_segured') newkey = 'valor';
					if (llave == '_estado') newkey = 'estado';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
			$.widgetModalmultiple12.update({
				data: datos
			});
		}
	};
	var ID_1080255621 = setTimeout(ID_1080255621_func, 1000 * 0.2);

}

$.widgetModalmultiple13.init({
	titulo: L('x2266302645_traducir', 'Cubierta'),
	cargando: L('x1740321226_traducir', 'cargando ..'),
	__id: 'ALL232106170',
	oncerrar: Cerrar_widgetModalmultiple13,
	hint: L('x2134385782_traducir', 'Seleccione cubiertas'),
	color: 'verde',
	subtitulo: L('x4011106049_traducir', 'Indique los tipos'),
	top: 5,
	onclick: Click_widgetModalmultiple13,
	onafterinit: Afterinit_widgetModalmultiple13
});

function Click_widgetModalmultiple13(e) {

	var evento = e;

}

function Cerrar_widgetModalmultiple13(e) {

	var evento = e;
	$.widgetModalmultiple13.update({});
	$.nivel.set({
		ids_cubierta: evento.valores
	});
	if ('nivel' in $) $nivel = $.nivel.toJSON();

}

function Afterinit_widgetModalmultiple13(e) {

	var evento = e;
	var ID_1214955522_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		var nivel_editado = ('nivel_editado' in require('vars')) ? require('vars')['nivel_editado'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			var consultarModelo22_i = Alloy.createCollection('cubierta');
			var consultarModelo22_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
			consultarModelo22_i.fetch({
				query: 'SELECT * FROM cubierta WHERE pais_texto=\'' + pais[0].nombre + '\''
			});
			var cubiertas = require('helper').query2array(consultarModelo22_i);
			var padre_index = 0;
			_.each(cubiertas, function(padre, padre_pos, padre_list) {
				padre_index += 1;
				if (_.isNull(nivel_editado.ids_cubierta)) {
					/** 
					 * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
					 */
					padre._estado = 0;
				} else if (nivel_editado.ids_cubierta.indexOf(padre.id_segured) != -1) {
					padre._estado = 1;
				} else {
					padre._estado = 0;
				}
			});
			var datos = [];
			_.each(cubiertas, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'label';
					if (llave == 'id_segured') newkey = 'valor';
					if (llave == '_estado') newkey = 'estado';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
			$.widgetModalmultiple13.update({
				data: datos
			});
		} else {
			var eliminarModelo16_i = Alloy.Collections.cubierta;
			var sql = "DELETE FROM " + eliminarModelo16_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo16_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo16_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo16_m = Alloy.Collections.cubierta;
				var insertarModelo16_fila = Alloy.createModel('cubierta', {
					nombre: String.format(L('x2246230604_traducir', 'Cubierta %1$s'), item.toString()),
					id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
					pais_texto: 'Chile',
					id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item.toString()),
					pais: 1
				});
				insertarModelo16_m.add(insertarModelo16_fila);
				insertarModelo16_fila.save();
			});
			var consultarModelo23_i = Alloy.createCollection('cubierta');
			var consultarModelo23_i_where = '';
			consultarModelo23_i.fetch();
			var cubiertas = require('helper').query2array(consultarModelo23_i);
			var padre_index = 0;
			_.each(cubiertas, function(padre, padre_pos, padre_list) {
				padre_index += 1;
				if (_.isNull(nivel_editado.ids_cubierta)) {
					/** 
					 * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
					 */
					padre._estado = 0;
				} else if (nivel_editado.ids_cubierta.indexOf(padre.id_segured) != -1) {
					padre._estado = 1;
				} else {
					padre._estado = 0;
				}
			});
			var datos = [];
			_.each(cubiertas, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'label';
					if (llave == 'id_segured') newkey = 'valor';
					if (llave == '_estado') newkey = 'estado';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
			$.widgetModalmultiple13.update({
				data: datos
			});
		}
	};
	var ID_1214955522 = setTimeout(ID_1214955522_func, 1000 * 0.2);

}

(function() {
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
		$.nivel.set({
			id_inspeccion: seltarea.id_server
		});
		if ('nivel' in $) $nivel = $.nivel.toJSON();
	}
	if (!_.isUndefined(args._fila)) {
		/** 
		 * heredamos datos previos de nivel en modelo nuevo con binding de esta pantalla 
		 */
		var consultarModelo24_i = Alloy.createCollection('insp_niveles');
		var consultarModelo24_i_where = 'id=\'' + args._fila.id + '\'';
		consultarModelo24_i.fetch({
			query: 'SELECT * FROM insp_niveles WHERE id=\'' + args._fila.id + '\''
		});
		var insp_n = require('helper').query2array(consultarModelo24_i);
		/** 
		 * Cargamos los datos en la tabla 
		 */
		$.nivel.set({
			ids_entrepisos: insp_n[0].ids_entrepisos,
			id_inspeccion: insp_n[0].id_inspeccion,
			nombre: insp_n[0].nombre,
			superficie: insp_n[0].superficie,
			largo: insp_n[0].largo,
			ids_pavimentos: insp_n[0].ids_pavimentos,
			alto: insp_n[0].alto,
			ano: insp_n[0].ano,
			ids_muros: insp_n[0].ids_muros,
			ancho: insp_n[0].ancho,
			ids_estructura_cubiera: insp_n[0].ids_estructura_cubiera,
			piso: insp_n[0].piso,
			ids_cubierta: insp_n[0].ids_cubierta,
			ids_estructuras_soportantes: insp_n[0].ids_estructuras_soportantes
		});
		if ('nivel' in $) $nivel = $.nivel.toJSON();
		require('vars')['nivel_editado'] = insp_n[0];
	}
})();

if (OS_IOS || OS_ANDROID) {
	$.EDITAR_NIVEL.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}