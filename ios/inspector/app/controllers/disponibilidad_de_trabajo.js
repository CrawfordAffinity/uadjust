var _bind4section = {};
var _list_templates = {};

var _activity;
if (OS_ANDROID) {
	_activity = $.ENROLAMIENTO4.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'ENROLAMIENTO4';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.ENROLAMIENTO4.addEventListener('open', function(e) {
		abx.setStatusbarColor("#000000");
		abx.setBackgroundColor("white");
	});
}

function Click_vista30(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.ENROLAMIENTO4.close();

}

$.widgetHeader4.init({
	titulo: L('x3752633607_traducir', 'PARTE 4: Disponibilidad de trabajo'),
	__id: 'ALL1222663545',
	avance: L('x25508266_traducir', '4/6')
});



function Change_picker5(e) {

	e.cancelBubble = true;
	var elemento = e;
	var _columna = e.columnIndex;
	var columna = e.columnIndex + 1;
	var _fila = e.rowIndex;
	var fila = e.rowIndex + 1;
	if (Ti.App.deployType != 'production') console.log('seleccionado', {
		"fila": _fila
	});
	require('vars')['seleccionado'] = _fila;

}

function Change_ID_1568773770(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	if (elemento == true || elemento == 'true') {
		/** 
		 * Cambiamos el texto de la opcion y cambiamos el valor de la variable 
		 */
		$.NO.setText('SI');

		require('vars')['fueraciudad'] = L('x2212294583', '1');
	} else {
		$.NO.setText('NO');

		require('vars')['fueraciudad'] = L('x4108050209', '0');
	}
	elemento = null;

}

function Change_ID_952608080(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	if (elemento == true || elemento == 'true') {
		/** 
		 * Cambiamos el texto de la opcion y cambiamos el valor de la variable 
		 */
		$.NO13.setText('SI');

		require('vars')['fuerapais'] = L('x2212294583', '1');
	} else {
		$.NO13.setText('NO');

		require('vars')['fuerapais'] = L('x4108050209', '0');
	}
	elemento = null;

}

$.widgetBotonlargo4.init({
	titulo: L('x1524107289_traducir', 'CONTINUAR'),
	__id: 'ALL218695237',
	onclick: Click_widgetBotonlargo4
});

function Click_widgetBotonlargo4(e) {

	var evento = e;
	/** 
	 * Recuperamos los valores de las variables, guardamos en la variable de registro 
	 */
	var seleccionado = ('seleccionado' in require('vars')) ? require('vars')['seleccionado'] : '';
	var fueraciudad = ('fueraciudad' in require('vars')) ? require('vars')['fueraciudad'] : '';
	var fuerapais = ('fuerapais' in require('vars')) ? require('vars')['fuerapais'] : '';
	var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
	var registro = _.extend(registro, {
		disponibilidad: seleccionado,
		disp_viajar_ciudad: fueraciudad,
		disp_viajar_pais: fuerapais
	});
	require('vars')['registro'] = registro;
	if (seleccionado == 1 || seleccionado == '1') {
		/** 
		 * Dependiendo de la seleccion de disponibilidad de tiempo, se puede accionar distintas funciones 
		 */
		/** 
		 * Si se escoge fulltime, marcamos disponibilidad completa, guardamos en registro y continuamos a pantalla contacto 
		 */
		var registro = _.extend(registro, {
			disp_horas: '',
			d1: 1,
			d2: 1,
			d3: 1,
			d4: 1,
			d5: 1,
			d6: 1,
			d7: 1
		});
		require('vars')['registro'] = registro;
		if ("ENROLAMIENTO" in Alloy.Globals) {
			Alloy.Globals["ENROLAMIENTO"].openWindow(Alloy.createController("contactos", {}).getView());
		} else {
			Alloy.Globals["ENROLAMIENTO"] = $.ENROLAMIENTO;
			Alloy.Globals["ENROLAMIENTO"].openWindow(Alloy.createController("contactos", {}).getView());
		}

	} else {
		if ("ENROLAMIENTO" in Alloy.Globals) {
			Alloy.Globals["ENROLAMIENTO"].openWindow(Alloy.createController("part_time", {}).getView());
		} else {
			Alloy.Globals["ENROLAMIENTO"] = $.ENROLAMIENTO;
			Alloy.Globals["ENROLAMIENTO"].openWindow(Alloy.createController("part_time", {}).getView());
		}

	}
}

(function() {
	/** 
	 * Creamos variables seteadas en 0 
	 */
	require('vars')['fueraciudad'] = L('x4108050209', '0');
	require('vars')['fuerapais'] = L('x4108050209', '0');
	require('vars')['seleccionado'] = L('x4108050209', '0');
	/** 
	 * Creamos evento que al cerrar desde la ultima pantalla, esta tambi&#233;n se cierre. Evitamos el uso excesivo de memoria ram 
	 */

	_my_events['_close_enrolamiento,ID_1656393466'] = function(evento) {
		if (Ti.App.deployType != 'production') console.log('escuchando cerrar enrolamiento disponibilidad trabajo', {});
		$.ENROLAMIENTO4.close();
	};
	Alloy.Events.on('_close_enrolamiento', _my_events['_close_enrolamiento,ID_1656393466']);
})();

if (OS_IOS || OS_ANDROID) {
	$.ENROLAMIENTO4.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}