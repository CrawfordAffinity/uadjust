var _bind4section = {};
var _list_templates = {};

var _activity;
if (OS_ANDROID) {
	_activity = $.EDITAR.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'EDITAR';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.EDITAR_window.addEventListener('open', function(e) {
		abx.setStatusbarColor("#000000");
		abx.setBackgroundColor("white");
	});
}

function Click_vista(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.EDITAR.close();

}

function Click_vista2(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
		/** 
		 * Obtenemos los valores de los campos ingresados en la pantalla 
		 */
		var correo;
		correo = $.IngreseCorreo.getValue();

		var correo_aux;
		correo_aux = $.RepitasuCorreo.getValue();

		var telefono;
		telefono = $.IngresesuTelfono.getValue();

		var telefono_aux;
		telefono_aux = $.RepitasuTelfono.getValue();

		if ((_.isObject(correo) || _.isString(correo)) && _.isEmpty(correo)) {
			/** 
			 * Revisamos que los campos no esten vacios o que no coincidan 
			 */
			var preguntarAlerta_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta = Ti.UI.createAlertDialog({
				title: L('x57652245_traducir', 'Alerta'),
				message: L('x3211270885_traducir', 'Debe ingresar un correo'),
				buttonNames: preguntarAlerta_opts
			});
			preguntarAlerta.addEventListener('click', function(e) {
				var suu = preguntarAlerta_opts[e.index];
				suu = null;

				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta.show();
		} else if (!(_.isString(correo) && /\S+@\S+\.\S+/.test(correo))) {
			var preguntarAlerta2_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta2 = Ti.UI.createAlertDialog({
				title: L('x57652245_traducir', 'Alerta'),
				message: L('x3725915824_traducir', 'Debe ingresar un correo valido'),
				buttonNames: preguntarAlerta2_opts
			});
			preguntarAlerta2.addEventListener('click', function(e) {
				var suu = preguntarAlerta2_opts[e.index];
				suu = null;

				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta2.show();
		} else if (correo != correo_aux) {
			var preguntarAlerta3_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta3 = Ti.UI.createAlertDialog({
				title: L('x57652245_traducir', 'Alerta'),
				message: L('x3338569552_traducir', 'Los correos no coinciden'),
				buttonNames: preguntarAlerta3_opts
			});
			preguntarAlerta3.addEventListener('click', function(e) {
				var suu = preguntarAlerta3_opts[e.index];
				suu = null;

				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta3.show();
		} else if ((_.isObject(telefono) || _.isString(telefono)) && _.isEmpty(telefono)) {
			var preguntarAlerta4_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta4 = Ti.UI.createAlertDialog({
				title: L('x57652245_traducir', 'Alerta'),
				message: L('x674181321_traducir', 'Debe ingresar un teléfono'),
				buttonNames: preguntarAlerta4_opts
			});
			preguntarAlerta4.addEventListener('click', function(e) {
				var suu = preguntarAlerta4_opts[e.index];
				suu = null;

				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta4.show();
		} else if (_.isNumber((telefono.length)) && _.isNumber(3) && (telefono.length) < 3) {
			var preguntarAlerta5_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta5 = Ti.UI.createAlertDialog({
				title: L('x57652245_traducir', 'Alerta'),
				message: L('x770664310_traducir', 'El telefono debe tener más digitos'),
				buttonNames: preguntarAlerta5_opts
			});
			preguntarAlerta5.addEventListener('click', function(e) {
				var suu = preguntarAlerta5_opts[e.index];
				suu = null;

				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta5.show();
		} else if (telefono != telefono_aux) {
			var preguntarAlerta6_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta6 = Ti.UI.createAlertDialog({
				title: L('x57652245_traducir', 'Alerta'),
				message: L('x2467132173_traducir', 'Los teléfonos no coinciden'),
				buttonNames: preguntarAlerta6_opts
			});
			preguntarAlerta6.addEventListener('click', function(e) {
				var suu = preguntarAlerta6_opts[e.index];
				suu = null;

				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta6.show();
		} else {
			var consultarModelo_i = Alloy.createCollection('inspectores');
			var consultarModelo_i_where = '';
			consultarModelo_i.fetch();
			var inspector_list = require('helper').query2array(consultarModelo_i);
			/** 
			 * Guardamos correo y telefono en caso de que la consulta al servidor sea exitosa 
			 */
			require('vars')['correo'] = correo;
			require('vars')['telefono'] = telefono;
			/** 
			 * Recuperamos variable url de uadjust 
			 */
			var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
			var consultarURL = {};

			consultarURL.success = function(e) {
				var elemento = e,
					valor = e;
				if (Ti.App.deployType != 'production') console.log('Servidor responde editarPerfilTipo3', {
					"elemento": elemento
				});
				/** 
				 * no me hace sentido que esto se repita al responder el servidor, ya que ya fue escrito antes de llamar al servidor tambien. 
				 */
				var consultarModelo2_i = Alloy.createCollection('inspectores');
				var consultarModelo2_i_where = '';
				consultarModelo2_i.fetch();
				var inspector_list = require('helper').query2array(consultarModelo2_i);
				var correo = ('correo' in require('vars')) ? require('vars')['correo'] : '';
				var telefono = ('telefono' in require('vars')) ? require('vars')['telefono'] : '';
				if (Ti.App.deployType != 'production') console.log('hare modificacion', {
					"datos": String.format(L('x1843559429_traducir', '%1$s y %2$s'), (correo) ? correo.toString() : '', (telefono) ? telefono.toString() : '')
				});
				var db = Ti.Database.open(consultarModelo2_i.config.adapter.db_name);
				if (consultarModelo2_i_where == '') {
					var sql = 'UPDATE ' + consultarModelo2_i.config.adapter.collection_name + ' SET correo=\'' + correo + '\'';
				} else {
					var sql = 'UPDATE ' + consultarModelo2_i.config.adapter.collection_name + ' SET correo=\'' + correo + '\' WHERE ' + consultarModelo2_i_where;
				}
				db.execute(sql);
				if (consultarModelo2_i_where == '') {
					var sql = 'UPDATE ' + consultarModelo2_i.config.adapter.collection_name + ' SET telefono=\'' + telefono + '\'';
				} else {
					var sql = 'UPDATE ' + consultarModelo2_i.config.adapter.collection_name + ' SET telefono=\'' + telefono + '\' WHERE ' + consultarModelo2_i_where;
				}
				db.execute(sql);
				db.close();
				/** 
				 * Mostramos mensaje de exito de cambio 
				 */
				var preguntarAlerta7_opts = [L('x2859518234_traducir', 'ACEPTAR')];
				var preguntarAlerta7 = Ti.UI.createAlertDialog({
					title: L('x2185084353_traducir', 'Atención'),
					message: L('x134968077_traducir', 'Cambios realizados'),
					buttonNames: preguntarAlerta7_opts
				});
				preguntarAlerta7.addEventListener('click', function(e) {
					var abc = preguntarAlerta7_opts[e.index];
					$.EDITAR.close();
					abc = null;

					e.source.removeEventListener("click", arguments.callee);
				});
				preguntarAlerta7.show();
				elemento = null, valor = null;
			};

			consultarURL.error = function(e) {
				var elemento = e,
					valor = e;
				if (Ti.App.deployType != 'production') console.log('Hubo un error', {
					"elemento": elemento
				});
				elemento = null, valor = null;
			};
			require('helper').ajaxUnico('consultarURL', '' + String.format(L('x2421050525', '%1$seditarPerfilTipo3'), url_server.toString()) + '', 'POST', {
				id_inspector: inspector.id_server,
				correo: correo,
				telefono: telefono
			}, 15000, consultarURL);
		}
	} else {
		var preguntarAlerta8_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta8 = Ti.UI.createAlertDialog({
			title: L('x57652245_traducir', 'Alerta'),
			message: L('x3970757340_traducir', 'No se pueden efectuar los cambios, ya que no hay internet'),
			buttonNames: preguntarAlerta8_opts
		});
		preguntarAlerta8.addEventListener('click', function(e) {
			var suu = preguntarAlerta8_opts[e.index];
			suu = null;

			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta8.show();
	}
}

function Click_scroll(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.IngreseCorreo.blur();
	$.RepitasuCorreo.blur();
	$.IngresesuTelfono.blur();
	$.RepitasuTelfono.blur();

}

(function() {
	var consultarModelo3_i = Alloy.createCollection('inspectores');
	var consultarModelo3_i_where = '';
	consultarModelo3_i.fetch();
	var inspector_list = require('helper').query2array(consultarModelo3_i);
	if (Ti.App.deployType != 'production') console.log('Mi inspector es', {
		"inspector": inspector_list[0]
	});
	if (inspector_list && inspector_list.length) {
		$.IngreseCorreo.setValue(inspector_list[0].correo);

		$.IngresesuTelfono.setValue(inspector_list[0].telefono);

	}
})();

function Postlayout_EDITAR(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var ID_1294585966_func = function() {
		/** 
		 * Una vez ya cargada la tarea, dejamos la variable limpia para poder abrir otras pantallas 
		 */
		require('vars')['var_abriendo'] = '';
	};
	var ID_1294585966 = setTimeout(ID_1294585966_func, 1000 * 0.2);

}
if (OS_IOS || OS_ANDROID) {
	$.EDITAR.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}
//$.EDITAR.open();
