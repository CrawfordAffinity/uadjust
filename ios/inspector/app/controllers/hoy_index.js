var _bind4section = {
	"ref1": "tareas"
};
var _list_templates = {
	"contenido": {
		"Label2": {
			"text": "{id}"
		},
		"vista4": {},
		"Label": {
			"text": "{nombre}"
		}
	},
	"recinto": {
		"Label2": {
			"text": "{id}"
		},
		"vista4": {},
		"Label": {
			"text": "{nombre}"
		}
	},
	"nivel": {
		"Label2": {
			"text": "{id}"
		},
		"vista20": {},
		"Label": {
			"text": "{nombre}"
		}
	},
	"tarea": {
		"vista86": {},
		"Label7": {
			"text": "{direccion}"
		},
		"vista97": {},
		"vista92": {},
		"vista107": {
			"visible": "{seguir}"
		},
		"vista93": {},
		"Adistance4": {
			"text": "a {distance} km"
		},
		"vista106": {},
		"vista102": {},
		"Label3": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"Label11": {
			"text": "{comuna}"
		},
		"Label21": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"Adistance7": {
			"text": "a {distance} km"
		},
		"vista38": {},
		"vista41": {},
		"vista55": {},
		"vista47": {},
		"vista19": {},
		"vista103": {},
		"vista48": {},
		"vista65": {},
		"vista7": {},
		"Label30": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"Label5": {
			"text": "{comuna}"
		},
		"vista64": {},
		"vista61": {},
		"Label14": {
			"text": "{comuna}"
		},
		"vista16": {},
		"vista15": {},
		"vista72": {
			"backgroundColor": "{bgcolor}"
		},
		"vista80": {
			"visible": "{vis_tipo9}"
		},
		"vista83": {},
		"Label23": {
			"text": "{comuna}"
		},
		"Label13": {
			"text": "{direccion}"
		},
		"vista90": {
			"backgroundColor": "{bgcolor}"
		},
		"Label33": {
			"text": "{comuna}"
		},
		"vista57": {},
		"Label8": {
			"text": "{comuna}"
		},
		"vista85": {},
		"vista68": {},
		"vista53": {
			"visible": "{vis_tipo6}"
		},
		"Label28": {
			"text": "{direccion}"
		},
		"vista76": {},
		"vista34": {},
		"vista37": {},
		"Label16": {
			"text": "{direccion}"
		},
		"vista25": {},
		"vista49": {},
		"Adistance6": {
			"text": "a {distance} km"
		},
		"Adistance": {
			"text": "a {distance} km"
		},
		"Adistance11": {
			"text": "a {distance} km"
		},
		"vista13": {},
		"vista27": {
			"backgroundColor": "{bgcolor}"
		},
		"vista79": {},
		"vista73": {},
		"Label34": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"Label35": {
			"text": "{id}"
		},
		"vista60": {},
		"vista82": {},
		"vista35": {
			"visible": "{vis_tipo4}"
		},
		"Label": {
			"text": "{direccion}"
		},
		"vista87": {},
		"vista67": {},
		"vista33": {},
		"vista70": {},
		"Label10": {
			"text": "{direccion}"
		},
		"Label12": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"vista96": {},
		"vista22": {},
		"Label24": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"vista52": {},
		"vista28": {},
		"vista99": {
			"backgroundColor": "{bgcolor}"
		},
		"vista84": {},
		"vista30": {},
		"vista78": {},
		"vista45": {
			"backgroundColor": "{bgcolor}"
		},
		"Label9": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"vista63": {
			"backgroundColor": "{bgcolor}"
		},
		"Label20": {
			"text": "{comuna}"
		},
		"Label29": {
			"text": "{comuna}"
		},
		"vista12": {},
		"Label18": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"vista29": {},
		"vista43": {},
		"vista105": {},
		"Label27": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"vista9": {
			"backgroundColor": "{bgcolor}"
		},
		"vista46": {},
		"Label22": {
			"text": "{direccion}"
		},
		"vista39": {},
		"Label26": {
			"text": "{comuna}"
		},
		"vista14": {},
		"Adistance9": {
			"text": "a {distance} km"
		},
		"vista58": {},
		"vista21": {},
		"vista24": {},
		"vista66": {},
		"vista89": {
			"visible": "{vis_tipo10}"
		},
		"vista23": {},
		"Label2": {
			"text": "{comuna}"
		},
		"vista88": {},
		"vista40": {},
		"vista74": {},
		"vista42": {},
		"vista94": {},
		"vista56": {},
		"vista91": {},
		"vista75": {},
		"vista71": {
			"visible": "{vis_tipo8}"
		},
		"Label15": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"vista95": {},
		"vista81": {
			"backgroundColor": "{bgcolor}"
		},
		"vista36": {
			"backgroundColor": "{bgcolor}"
		},
		"Label31": {
			"text": "{prioridad_tiempo}"
		},
		"vista98": {
			"visible": "{vis_otro}"
		},
		"vista10": {},
		"Label17": {
			"text": "{comuna}"
		},
		"vista100": {},
		"vista51": {},
		"vista44": {
			"visible": "{vis_tipo5}"
		},
		"Adistance8": {
			"text": "a {distance} km"
		},
		"imagen": {},
		"vista59": {},
		"Label4": {
			"text": "{direccion}"
		},
		"vista18": {
			"backgroundColor": "{bgcolor}"
		},
		"vista101": {},
		"vista31": {},
		"vista17": {
			"visible": "{vis_tipo2}"
		},
		"Label6": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"vista8": {
			"visible": "{vis_tipo1}"
		},
		"vista104": {},
		"vista11": {},
		"Adistance2": {
			"text": "a {distance} km"
		},
		"vista32": {},
		"vista69": {},
		"Adistance3": {
			"text": "a {distance} km"
		},
		"vista62": {
			"visible": "{vis_tipo7}"
		},
		"vista50": {},
		"Label25": {
			"text": "{direccion}"
		},
		"vista77": {},
		"vista54": {
			"backgroundColor": "{bgcolor}"
		},
		"Adistance5": {
			"text": "a {distance} km"
		},
		"Adistance10": {
			"text": "a {distance} km"
		},
		"vista20": {},
		"Label19": {
			"text": "{direccion}"
		},
		"Label32": {
			"text": "{direccion}"
		},
		"vista26": {
			"visible": "{vis_tipo3}"
		}
	}
};

var _activity;
if (OS_ANDROID) {
	_activity = $.HOY.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'HOY';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.HOY.addEventListener('open', function(e) {
		abx.setStatusbarColor("#000000");
		abx.setBackgroundColor("white");
	});
}


var consultarModelo_like = function(search) {
	if (typeof search !== 'string' || this === null) {
		return false;
	}
	search = search.replace(new RegExp("([\\.\\\\\\+\\*\\?\\[\\^\\]\\$\\(\\)\\{\\}\\=\\!\\<\\>\\|\\:\\-])", "g"), "\\$1");
	search = search.replace(/%/g, '.*').replace(/_/g, '.');
	return RegExp('^' + search + '$', 'gi').test(this);
};
var consultarModelo_filter = function(coll) {
	var filtered = _.toArray(coll.filter(function(m) {
		return true;
	}));
	return filtered;
};
var consultarModelo_update = function(e) {};
_.defer(function() {
	Alloy.Collections.tareas.fetch();
});
Alloy.Collections.tareas.on('add change delete', function(ee) {
	consultarModelo_update(ee);
});
var consultarModelo_transform = function(model) {
	var fila = model.toJSON();
	/** 
	 * Valores extra default 
	 */
	var fila = _.extend(fila, {
		vis_tipo1: L('x734881840_traducir', 'false'),
		vis_tipo2: L('x734881840_traducir', 'false'),
		vis_tipo3: L('x734881840_traducir', 'false'),
		vis_tipo4: L('x734881840_traducir', 'false'),
		vis_tipo5: L('x734881840_traducir', 'false'),
		vis_tipo6: L('x734881840_traducir', 'false'),
		vis_tipo7: L('x734881840_traducir', 'false'),
		vis_tipo8: L('x734881840_traducir', 'false'),
		vis_tipo9: L('x734881840_traducir', 'false'),
		vis_tipo10: L('x734881840_traducir', 'false'),
		vis_otro: L('x734881840_traducir', 'false'),
		bgcolor: L('x1492402853', '#CECECE'),
		seguir: L('x734881840_traducir', 'false')
	});
	if (fila.prioridad_tiempo == 1 || fila.prioridad_tiempo == '1') {
		/** 
		 * Movemos pinchos de mapa segun posicion de tareas de hoy 
		 */
		$.pinchoMapa2.setLatitude(fila.lat);

		$.pinchoMapa2.setLongitude(fila.lon);

		var fila = _.extend(fila, {
			vis_tipo1: L('x4261170317', 'true'),
			bgcolor: L('x1884157677', '#2D9EDB')
		});
	} else if (fila.prioridad_tiempo == 2) {
		$.pinchoMapa3.setLatitude(fila.lat);

		$.pinchoMapa3.setLongitude(fila.lon);

		var fila = _.extend(fila, {
			vis_tipo2: L('x4261170317', 'true'),
			bgcolor: L('x602248408', '#EE7F7E')
		});
	} else if (fila.prioridad_tiempo == 3) {
		$.pinchoMapa4.setLatitude(fila.lat);

		$.pinchoMapa4.setLongitude(fila.lon);

		var fila = _.extend(fila, {
			vis_tipo3: L('x4261170317', 'true'),
			bgcolor: L('x1927584467', '#8383DB')
		});
	} else if (fila.prioridad_tiempo == 4) {
		$.pinchoMapa5.setLatitude(fila.lat);

		$.pinchoMapa5.setLongitude(fila.lon);

		var fila = _.extend(fila, {
			vis_tipo4: L('x4261170317', 'true'),
			bgcolor: L('x1269456848', '#FCBD83')
		});
	} else if (fila.prioridad_tiempo == 5) {
		$.pinchoMapa6.setLatitude(fila.lat);

		$.pinchoMapa6.setLongitude(fila.lon);

		var fila = _.extend(fila, {
			vis_tipo5: L('x4261170317', 'true'),
			bgcolor: L('x2849471580', '#8CE5BD')
		});
	} else if (fila.prioridad_tiempo == 6) {
		$.pinchoMapa7.setLatitude(fila.lat);

		$.pinchoMapa7.setLongitude(fila.lon);

		var fila = _.extend(fila, {
			vis_tipo6: L('x4261170317', 'true'),
			bgcolor: L('x3452106967', '#F8DA54')
		});
	} else if (fila.prioridad_tiempo == 7) {
		$.pinchoMapa8.setLatitude(fila.lat);

		$.pinchoMapa8.setLongitude(fila.lon);

		var fila = _.extend(fila, {
			vis_tipo7: L('x4261170317', 'true'),
			bgcolor: L('x3332103381', '#B9AAF3')
		});
	} else if (fila.prioridad_tiempo == 8) {
		$.pinchoMapa9.setLatitude(fila.lat);

		$.pinchoMapa9.setLongitude(fila.lon);

		var fila = _.extend(fila, {
			vis_tipo8: L('x4261170317', 'true'),
			bgcolor: L('x3561405284', '#FFACAA')
		});
	} else if (fila.prioridad_tiempo == 9) {
		$.pinchoMapa10.setLatitude(fila.lat);

		$.pinchoMapa10.setLongitude(fila.lon);

		var fila = _.extend(fila, {
			vis_tipo9: L('x4261170317', 'true'),
			bgcolor: L('x2922666116', '#8BC9E8')
		});
	} else if (fila.prioridad_tiempo == 10) {
		$.pinchoMapa11.setLatitude(fila.lat);

		$.pinchoMapa11.setLongitude(fila.lon);

		var fila = _.extend(fila, {
			vis_tipo10: L('x4261170317', 'true'),
			bgcolor: L('x585770471', '#A5876D')
		});
	} else {
		$.pinchoMapa11.setLatitude(fila.lat);

		$.pinchoMapa11.setLongitude(fila.lon);

		var fila = _.extend(fila, {
			vis_otro: L('x4261170317', 'true')
		});
	}
	if ((_.isObject(fila.nivel_2) || _.isString(fila.nivel_2)) && _.isEmpty(fila.nivel_2)) {
		/** 
		 * Mapeamos campos de niveles 
		 */
		fila.comuna = fila.nivel_1;
	} else if ((_.isObject(fila.nivel_3) || _.isString(fila.nivel_3)) && _.isEmpty(fila.nivel_3)) {
		fila.comuna = fila.nivel_2;
	} else if ((_.isObject(fila.nivel_4) || _.isString(fila.nivel_4)) && _.isEmpty(fila.nivel_4)) {
		fila.comuna = fila.nivel_3;
	} else if ((_.isObject(fila.nivel_5) || _.isString(fila.nivel_5)) && _.isEmpty(fila.nivel_5)) {
		fila.comuna = fila.nivel_4;
	} else {
		fila.comuna = fila.nivel_5;
	}
	if (fila.estado_tarea == 4) {
		/** 
		 * Revisamos si el estado de la tarea esta en seguimiento 
		 */
		fila.seguir = true;
	}
	return fila;
};
consultarModelo_filter = function(coll) {
	var filtered = _.toArray(coll.filter(function(filax) {
		var fila = filax.toJSON();
		var test = true;
		var moment = require('alloy/moment');
		var fecha_hoy = moment(new Date()).format('YYYY-MM-DD');
		if (fila.fecha_tarea == fecha_hoy) {
			test = true;
		} else {
			test = false;
		}
		fila = null;
		return test;
	}));
	var ordered = _.sortBy(filtered, 'prioridad_tiempo');
	return ordered;
};
Alloy.Collections.tareas.fetch();


$.pinchoMapa.applyProperties({
	latitude: parseFloat('0.0'),
	longitude: parseFloat('0.0'),
	image: '/images/i31D884D83D4B504A668DECEFFAE7EA43.png'
});


$.pinchoMapa2.applyProperties({
	latitude: parseFloat('0.0'),
	longitude: parseFloat('0.0'),
	image: '/images/i016B8B08EE4DEC0313B1A3082A84D2F2.png'
});


$.pinchoMapa3.applyProperties({
	latitude: parseFloat('0.0'),
	longitude: parseFloat('0.0'),
	image: '/images/iAF9569EFAB97FCE93FE3DAD449050784.png'
});


$.pinchoMapa4.applyProperties({
	latitude: parseFloat('0.0'),
	longitude: parseFloat('0.0'),
	image: '/images/i56ABF76585C9B6A661090A44C21595F2.png'
});


$.pinchoMapa5.applyProperties({
	latitude: parseFloat('0.0'),
	longitude: parseFloat('0.0'),
	image: '/images/i999E1B96A6FB0BFB9034B067828F4A68.png'
});


$.pinchoMapa6.applyProperties({
	latitude: parseFloat('0.0'),
	longitude: parseFloat('0.0'),
	image: '/images/i0B10A526AD8D5B25BC5BAC5C2C06F119.png'
});


$.pinchoMapa7.applyProperties({
	latitude: parseFloat('0.0'),
	longitude: parseFloat('0.0'),
	image: '/images/i15970AF9F27BB88EC2DC1EE088D2551E.png'
});


$.pinchoMapa8.applyProperties({
	latitude: parseFloat('0.0'),
	longitude: parseFloat('0.0'),
	image: '/images/iA6C17CF225E89F54DE02BF67B8FCA6A0.png'
});


$.pinchoMapa9.applyProperties({
	latitude: parseFloat('0.0'),
	longitude: parseFloat('0.0'),
	image: '/images/iCF8339009327869FD10EC12DA520DC5A.png'
});


$.pinchoMapa10.applyProperties({
	latitude: parseFloat('0.0'),
	longitude: parseFloat('0.0'),
	image: '/images/i8893A1F7922FA64C321EAD4A6119E30D.png'
});


$.pinchoMapa11.applyProperties({
	latitude: parseFloat('0.0'),
	longitude: parseFloat('0.0'),
	image: '/images/i71C6F623091942D94C46CC378343ABF6.png'
});


$.mapa.setRegion({
	latitude: -33.392047,
	longitude: -70.542939,
	latitudeDelta: 0.005,
	longitudeDelta: 0.005
});
$.mapa.applyProperties({});
if (OS_IOS) {
	var mapa_camara = require('ti.map').createCamera({
		pitch: 0,
		heading: 0
	});
	$.mapa.setCamera(mapa_camara);
}


$.widgetMono.init({
	titulo: L('x2349703141_traducir', 'NO TIENES TAREAS'),
	__id: 'ALL859578408',
	texto: L('x2279881512_traducir', 'Asegurate de tomar tareas para hoy y revisar tu ruta'),
	alto: '-',
	top: '10%',
	ancho: '*',
	tipo: '_sorry'
});

function Click_tabbar(e) {

	e.cancelBubble = true;
	if ('index' in e) {
		var elemento = e.source.getLabels()[e.index];
		var valor = elemento.title;
		if (Ti.App.deployType != 'production') console.log('cambiado tipo de calculo de ruta', {
			"elemento": elemento
		});
		if (e.index == 0 || e.index == '0') {
			require('vars')['avoid'] = '';
		} else {
			require('vars')['avoid'] = L('x1287501108_traducir', 'highways');
		}
		Alloy.Events.trigger('_calcular_ruta');
		elemento = null, valor = null;
	}

}

function Load_imagen(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var evento = e;
	elemento.start();

}

function Itemclick_listado(e) {

	e.cancelBubble = true;
	var objeto = e.section.getItemAt(e.itemIndex);
	var modelo = {},
		_modelo = [];
	var fila = {},
		fila_bak = {},
		info = {
			_template: objeto.template,
			_what: [],
			_seccion_ref: e.section.getHeaderTitle(),
			_model_id: -1
		},
		_tmp = {
			objmap: {}
		};
	if ('itemId' in e) {
		info._model_id = e.itemId;
		modelo._id = info._model_id;
		if (info._seccion_ref != '' && info._seccion_ref in _bind4section) {
			modelo._collection = _bind4section[info._seccion_ref];
			_tmp._coll = modelo._collection;
		}
	}
	var findVariables = require('fvariables');
	_.each(_list_templates[info._template], function(obj_id, id) {
		_.each(obj_id, function(valor, prop) {
			var llaves = findVariables(valor, '{', '}');
			_.each(llaves, function(llave) {
				_tmp.objmap[llave] = {
					id: id,
					prop: prop
				};
				fila[llave] = objeto[id][prop];
				if (id == e.bindId) info._what.push(llave);
			});
		});
	});
	info._what = info._what.join(',');
	fila_bak = JSON.parse(JSON.stringify(fila));
	/** 
	 * Ocupamos variable para impedir que la proxima pantalla se abra dos veces 
	 */
	var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
	if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
		require('vars')['var_abriendo'] = L('x4261170317', 'true');
		var nulo = Alloy.createController("detalletarea_index", {
			'_id': fila.id,
			'__master_model': (typeof modelo !== 'undefined') ? modelo : {},
			'__modelo': (typeof _modelo !== 'undefined' && _modelo.length > 0) ? _modelo[0] : {}
		}).getView();
		nulo.open({
			modal: true
		});
		nulo = null;
	}
	_tmp.changed = false;
	_tmp.diff_keys = [];
	_.each(fila, function(value1, prop) {
		var had_samekey = false;
		_.each(fila_bak, function(value2, prop2) {
			if (prop == prop2 && value1 == value2) {
				had_samekey = true;
			} else if (!_.has(fila_bak, prop) || !_.has(fila, prop2)) {
				has_samekey = true;
			}
		});
		if (!had_samekey) _tmp.diff_keys.push(prop);
	});
	if (_tmp.diff_keys.length > 0) _tmp.changed = true;
	if (_tmp.changed == true) {
		_.each(_tmp.diff_keys, function(llave) {
			objeto[_tmp.objmap[llave].id][_tmp.objmap[llave].prop] = fila[llave];
		});
		e.section.updateItemAt(e.itemIndex, objeto);
	}

}

function Longpress_vista110(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	/** 
	 * Armamos listado de casos para confirmar ruta 
	 */
	var moment = require('alloy/moment');
	var hoy_date = moment(new Date()).format('YYYY-MM-DD');
	var consultarModelo2_i = Alloy.createCollection('tareas');
	var consultarModelo2_i_where = 'fecha_tarea=\'' + hoy_date + '\'';
	consultarModelo2_i.fetch({
		query: 'SELECT * FROM tareas WHERE fecha_tarea=\'' + hoy_date + '\''
	});
	var tarea_lista = require('helper').query2array(consultarModelo2_i);
	var listacasos = _.pluck(tarea_lista, 'num_caso').join(',');
	var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
	var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
	Ti.App.Properties.setString('confirmarpush', JSON.stringify(false));
	require('vars')['rutaconfirmada'] = L('x734881840_traducir', 'false');
	var jsonfinal = {
		_method: L('x1814004025_traducir', 'POST'),
		_url: String.format(L('x2181838308', '%1$sconfirmarRuta'), url_server.toString()),
		id_inspector: inspector[0].id_server,
		codigo_identificador: inspector[0].codigo_identificador,
		tareas: listacasos
	};
	var jsonfinal = JSON.stringify(jsonfinal);
	var insertarModelo_m = Alloy.Collections.cola;
	var insertarModelo_fila = Alloy.createModel('cola', {
		data: jsonfinal,
		tipo: 'confirmar_ruta'
	});
	insertarModelo_m.add(insertarModelo_fila);
	insertarModelo_fila.save();
	_.defer(function() {});
	/** 
	 * Oculta boton mantener para iniciar 
	 */
	var vista109_visible = false;

	if (vista109_visible == 'si') {
		vista109_visible = true;
	} else if (vista109_visible == 'no') {
		vista109_visible = false;
	}
	$.vista109.setVisible(vista109_visible);

	/** 
	 * Limpieza de memoria 
	 */
	jsonfinal = null, listacasos = null;

}

(function() {
	$.mapa.setTraffic(true);

	/** 
	 * cuando se gatilla este evento, la app se esta cerrando. 30-dic-2017. Revisar 
	 */
	_my_events['_refrescar_tareas_hoy,ID_1168607634'] = function(evento) {
		var inspeccion_encurso = ('inspeccion_encurso' in require('vars')) ? require('vars')['inspeccion_encurso'] : '';
		if (inspeccion_encurso == false || inspeccion_encurso == 'false') {
			$.pinchoMapa2.setLatitude('0.0');

			$.pinchoMapa2.setLongitude('0.0');

			$.pinchoMapa3.setLatitude('0.0');

			$.pinchoMapa3.setLongitude('0.0');

			$.pinchoMapa4.setLatitude('0.0');

			$.pinchoMapa4.setLongitude('0.0');

			$.pinchoMapa5.setLatitude('0.0');

			$.pinchoMapa5.setLongitude('0.0');

			$.pinchoMapa6.setLatitude('0.0');

			$.pinchoMapa6.setLongitude('0.0');

			$.pinchoMapa7.setLatitude('0.0');

			$.pinchoMapa7.setLongitude('0.0');

			$.pinchoMapa8.setLatitude('0.0');

			$.pinchoMapa8.setLongitude('0.0');

			$.pinchoMapa9.setLatitude('0.0');

			$.pinchoMapa9.setLongitude('0.0');

			$.pinchoMapa10.setLatitude('0.0');

			$.pinchoMapa10.setLongitude('0.0');

			$.pinchoMapa11.setLatitude('0.0');

			$.pinchoMapa11.setLongitude('0.0');

			$.pinchoMapa11.setLatitude('0.0');

			$.pinchoMapa11.setLongitude('0.0');

			var ID_568119611_func = function() {
				_.defer(function() {
					Alloy.Collections.tareas.fetch();
				});
			};
			var ID_568119611 = setTimeout(ID_568119611_func, 1000 * 0.2);
			var moment = require('alloy/moment');
			var hoy_date = moment(new Date()).format('YYYY-MM-DD');
			var consultarModelo3_i = Alloy.createCollection('tareas');
			var consultarModelo3_i_where = 'fecha_tarea=\'' + hoy_date + '\' ORDER BY PRIORIDAD_TIEMPO ASC';
			consultarModelo3_i.fetch({
				query: 'SELECT * FROM tareas WHERE fecha_tarea=\'' + hoy_date + '\' ORDER BY PRIORIDAD_TIEMPO ASC'
			});
			var tarea_lista = require('helper').query2array(consultarModelo3_i);
			if (tarea_lista && tarea_lista.length == 0) {
				$.widgetMono.mostrar({});
				var vista3_visible = false;

				if (vista3_visible == 'si') {
					vista3_visible = true;
				} else if (vista3_visible == 'no') {
					vista3_visible = false;
				}
				$.vista3.setVisible(vista3_visible);

			} else {
				$.widgetMono.ocultar({});
				var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
				$.pinchoMapa.setLatitude(inspector.lat_dir);

				$.pinchoMapa.setLongitude(inspector.lon_dir);

				var vista3_visible = true;

				if (vista3_visible == 'si') {
					vista3_visible = true;
				} else if (vista3_visible == 'no') {
					vista3_visible = false;
				}
				$.vista3.setVisible(vista3_visible);

			}
			var confirmarpush = JSON.parse(Ti.App.Properties.getString('confirmarpush'));
			if (confirmarpush == true || confirmarpush == 'true') {
				var vista109_visible = true;

				if (vista109_visible == 'si') {
					vista109_visible = true;
				} else if (vista109_visible == 'no') {
					vista109_visible = false;
				}
				$.vista109.setVisible(vista109_visible);

			} else {
				var vista109_visible = false;

				if (vista109_visible == 'si') {
					vista109_visible = true;
				} else if (vista109_visible == 'no') {
					vista109_visible = false;
				}
				$.vista109.setVisible(vista109_visible);

			}
			tarea_lista = null, hoy_date = null, confirmarpush = null, inspeccion_encurso = null, evento = null;
		}
	};
	Alloy.Events.on('_refrescar_tareas_hoy', _my_events['_refrescar_tareas_hoy,ID_1168607634']);
	/** 
	 * &#160; 
	 */
	_my_events['_calcular_ruta,ID_598459730'] = function(evento) {
		var gps_error = ('gps_error' in require('vars')) ? require('vars')['gps_error'] : '';
		if (gps_error == false || gps_error == 'false') {
			var inspeccion_encurso = ('inspeccion_encurso' in require('vars')) ? require('vars')['inspeccion_encurso'] : '';
			if (inspeccion_encurso == false || inspeccion_encurso == 'false') {
				if (Ti.App.deployType != 'production') console.log('calculando ruta optima para el dia de hoy', {});
				var moment = require('alloy/moment');
				var hoy_date = moment(new Date()).format('YYYY-MM-DD');
				var consultarModelo4_i = Alloy.createCollection('tareas');
				var consultarModelo4_i_where = 'fecha_tarea=\'' + hoy_date + '\'';
				consultarModelo4_i.fetch({
					query: 'SELECT * FROM tareas WHERE fecha_tarea=\'' + hoy_date + '\''
				});
				var tarea_lista = require('helper').query2array(consultarModelo4_i);
				if (tarea_lista && tarea_lista.length == 0) {
					Alloy.Events.trigger('_refrescar_tareas_hoy');
				} else {
					var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
					var gps_latitud = ('gps_latitud' in require('vars')) ? require('vars')['gps_latitud'] : '';
					var gps_longitud = ('gps_longitud' in require('vars')) ? require('vars')['gps_longitud'] : '';
					/** 
					 * Recorremos tareas para armar waypoints, considerando tarea seguida como inicial 
					 */

					//mapea el array de tarea_lista considerando las columnas lat y lon en un array de string tipo 'lat,lon'
					var waypoints = _.map(tarea_lista,
						function(num) {
							var item = num.lat + "," + num.lon;
							return item;
						});
					var waypoints = waypoints.join("|");
					/*
					var destino = _.max(tarea_lista, function(tarea){ return tarea.distancia_2; });*/
					var origen = gps_latitud + ',' + gps_longitud;
					var destino = inspector.lat_dir + ',' + inspector.lon_dir;
					//agregamos destino como ultimo waypoint, porque no lo marca como ruta.
					waypoints = waypoints + '|' + destino
					var avoid = ('avoid' in require('vars')) ? require('vars')['avoid'] : '';
					var googlekeymap = ('googlekeymap' in require('vars')) ? require('vars')['googlekeymap'] : '';
					if (Ti.App.deployType != 'production') console.log('consultando a google rutas para waypoints', {
						"waypoints": waypoints
					});
					/** 
					 * API de Google Directions 
					 */
					var consultarURL = {};
					consultarURL.success = function(e) {
						var elemento = e,
							valor = e;
						if (elemento.status == L('x3610695981_traducir', 'OK')) {
							/** 
							 * Borramos ruta previa si existe 
							 */
							var old_ruta = ('old_ruta' in require('vars')) ? require('vars')['old_ruta'] : '';
							if (old_ruta && old_ruta.length) {
								var item_rutas_index = 0;
								_.each(old_ruta, function(item_rutas, item_rutas_pos, item_rutas_list) {
									item_rutas_index += 1;
									$.mapa.removeRoute(item_rutas);

								});
								/** 
								 * Limpieza de memoria 
								 */
								item_rutas = null;
							}
							/** 
							 * Dibujamos nuevas rutas 
							 */
							var ruta = elemento.routes[0];
							var rutas = [];
							var pincho_index = 0;
							_.each(ruta.legs, function(pincho, pincho_pos, pincho_list) {
								pincho_index += 1;
								var tramo_index = 0;
								_.each(pincho.steps, function(tramo, tramo_pos, tramo_list) {
									tramo_index += 1;
									if (pincho_index == ruta.legs.length) {
										var rutaMapa_poly = require('polyline').decode(tramo.polyline.points);
										var nuevaruta = require('ti.map').createRoute({
											width: 6,
											color: '#f7d952',
											points: rutaMapa_poly
										});
									} else if (pincho_index == 1 || pincho_index == '1') {
										var rutaMapa2_poly = require('polyline').decode(tramo.polyline.points);
										var nuevaruta = require('ti.map').createRoute({
											width: 6,
											color: '#2d9edb',
											points: rutaMapa2_poly
										});
									} else if (pincho_index == 2) {
										var rutaMapa3_poly = require('polyline').decode(tramo.polyline.points);
										var nuevaruta = require('ti.map').createRoute({
											width: 6,
											color: '#ee7f7e',
											points: rutaMapa3_poly
										});
									} else if (pincho_index == 3) {
										var rutaMapa4_poly = require('polyline').decode(tramo.polyline.points);
										var nuevaruta = require('ti.map').createRoute({
											width: 6,
											color: '#b9aaf3',
											points: rutaMapa4_poly
										});
									} else if (pincho_index == 4) {
										var rutaMapa5_poly = require('polyline').decode(tramo.polyline.points);
										var nuevaruta = require('ti.map').createRoute({
											width: 6,
											color: '#efb275',
											points: rutaMapa5_poly
										});
									} else if (pincho_index == 5) {
										var rutaMapa6_poly = require('polyline').decode(tramo.polyline.points);
										var nuevaruta = require('ti.map').createRoute({
											width: 6,
											color: '#8ce5bd',
											points: rutaMapa6_poly
										});
									} else if (pincho_index == 6) {
										var rutaMapa7_poly = require('polyline').decode(tramo.polyline.points);
										var nuevaruta = require('ti.map').createRoute({
											width: 6,
											color: '#f8da54',
											points: rutaMapa7_poly
										});
									} else if (pincho_index == 7) {
										var rutaMapa8_poly = require('polyline').decode(tramo.polyline.points);
										var nuevaruta = require('ti.map').createRoute({
											width: 6,
											color: '#8383db',
											points: rutaMapa8_poly
										});
									} else if (pincho_index == 8) {
										var rutaMapa9_poly = require('polyline').decode(tramo.polyline.points);
										var nuevaruta = require('ti.map').createRoute({
											width: 6,
											color: '#ffacaa',
											points: rutaMapa9_poly
										});
									} else if (pincho_index == 9) {
										var rutaMapa10_poly = require('polyline').decode(tramo.polyline.points);
										var nuevaruta = require('ti.map').createRoute({
											width: 6,
											color: '#8bc9e8',
											points: rutaMapa10_poly
										});
									} else if (pincho_index == 10) {
										var rutaMapa11_poly = require('polyline').decode(tramo.polyline.points);
										var nuevaruta = require('ti.map').createRoute({
											width: 6,
											color: '#a5876d',
											points: rutaMapa11_poly
										});
									} else {
										var rutaMapa12_poly = require('polyline').decode(tramo.polyline.points);
										var nuevaruta = require('ti.map').createRoute({
											width: 6,
											color: '#2d9edb',
											points: rutaMapa12_poly
										});
									}
									/** 
									 * Agregamos la ruta al mapa 
									 */
									$.mapa.addRoute(nuevaruta);

									/** 
									 * guardamos la ruta en una variable 
									 */
									rutas.push(nuevaruta);
									require('vars')['old_ruta'] = rutas;
								});
							});
							/** 
							 * Limpieza de memoria 
							 */
							rutas = null;
							/** 
							 * Ordenamos las tareas 
							 */
							var moment = require('alloy/moment');
							var hoy_date = moment(new Date()).format('YYYY-MM-DD');
							var consultarModelo5_i = Alloy.createCollection('tareas');
							var consultarModelo5_i_where = 'fecha_tarea=\'' + hoy_date + '\'';
							consultarModelo5_i.fetch({
								query: 'SELECT * FROM tareas WHERE fecha_tarea=\'' + hoy_date + '\''
							});
							var tarea_lista = require('helper').query2array(consultarModelo5_i);
							var item_index = 0;
							_.each(ruta.waypoint_order, function(item, item_pos, item_list) {
								item_index += 1;
								var seguir_tarea = ('seguir_tarea' in require('vars')) ? require('vars')['seguir_tarea'] : '';
								if (item_index == ruta.waypoint_order.length) {
									/** 
									 * el ultimo valor lo omitimos porque el camino a casa no es una tarea 
									 */
								} else {
									var tareaid = tarea_lista[item].id;
									var consultarModelo6_i = Alloy.createCollection('tareas');
									var consultarModelo6_i_where = 'id=\'' + tareaid + '\'';
									consultarModelo6_i.fetch({
										query: 'SELECT * FROM tareas WHERE id=\'' + tareaid + '\''
									});
									var tarea_lista_aux = require('helper').query2array(consultarModelo6_i);
									var db = Ti.Database.open(consultarModelo6_i.config.adapter.db_name);
									if (consultarModelo6_i_where == '') {
										var sql = 'UPDATE ' + consultarModelo6_i.config.adapter.collection_name + ' SET prioridad_tiempo=\'' + item_index + '\'';
									} else {
										var sql = 'UPDATE ' + consultarModelo6_i.config.adapter.collection_name + ' SET prioridad_tiempo=\'' + item_index + '\' WHERE ' + consultarModelo6_i_where;
									}
									db.execute(sql);
									db.close();
									/** 
									 * Limpieza de memoria 
									 */
									tarea_lista_aux = null, tareaid = null;
								}
							});
							/** 
							 * Limpieza de memoria 
							 */
							ruta = null, rutas = null, tarea_lista = null, hoy_date = null, old_ruta = null, pincho = null, item = null;
						}
						/** 
						 * Limpieza de memoria 
						 */
						elemento = null;
						if (Ti.App.deployType != 'production') console.log('refrescando con ruta', {});
						Alloy.Events.trigger('_refrescar_tareas_hoy');
						elemento = null, valor = null;
					};
					consultarURL.error = function(e) {
						var elemento = e,
							valor = e;
						if (Ti.App.deployType != 'production') console.log('refrescando con error', {});
						Alloy.Events.trigger('_refrescar_tareas_hoy');
						elemento = null, valor = null;
					};
					require('helper').ajaxUnico('consultarURL', 'https://maps.googleapis.com/maps/api/directions/json?origin=' + origen + '&destination=' + destino + '&waypoints=' + waypoints + '&key=AIzaSyAJoATeoDjsJ3fNSe5q7eIKZ7VjT2AojqY&avoid=' + avoid + '&alternatives=false', 'get', {}, 15000, consultarURL);
					/** 
					 * Evaluar, elemento=null; 
					 */
					waypoints = null, destino = null, origen = null, avoid = null, googlekeymap = null, tarea_lista = null;
				}
			}
		}
	};
	Alloy.Events.on('_calcular_ruta', _my_events['_calcular_ruta,ID_598459730']);
	_my_events['_refrescar_tareas,ID_106592695'] = function(evento) {
		var inspeccion_encurso = ('inspeccion_encurso' in require('vars')) ? require('vars')['inspeccion_encurso'] : '';
		if (inspeccion_encurso == false || inspeccion_encurso == 'false') {
			/** 
			 * solo refrescamos si no hay una inspecci&#243;n en curso 
			 */
			if (Ti.App.deployType != 'production') console.log('llamado refrescar ruta de hoy', {});
			Alloy.Events.trigger('_calcular_ruta');
		}
	};
	Alloy.Events.on('_refrescar_tareas', _my_events['_refrescar_tareas,ID_106592695']);
	/** 
	 * Esto se llama para que parta con informacion si tiene ya tareas para hoy 
	 */
	var ID_599015300_func = function() {
		Alloy.Events.trigger('_calcular_ruta');
	};
	var ID_599015300 = setTimeout(ID_599015300_func, 1000 * 0.1);
})();

if (OS_IOS || OS_ANDROID) {
	$.HOY.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}
//$.HOY.open();
