var _bind4section = {
	"ref1": "insp_contenido"
};
var _list_templates = {
	"contenido": {
		"Label2": {
			"text": "{id}"
		},
		"vista4": {},
		"Label": {
			"text": "{nombre}"
		}
	}
};

$.DAÑOS_EN_CONTENIDO_window.setTitleAttributes({
	color: 'WHITE'
});
var _activity;
if (OS_ANDROID) {
	_activity = $.DAÑOS_EN_CONTENIDO.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'DAÑOS_EN_CONTENIDO';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.DAÑOS_EN_CONTENIDO_window.addEventListener('open', function(e) {
		abx.setStatusbarColor("#FFFFFF");
		abx.setBackgroundColor("#8bc9e8");
	});
}


var consultarModelo_like = function(search) {
	if (typeof search !== 'string' || this === null) {
		return false;
	}
	search = search.replace(new RegExp("([\\.\\\\\\+\\*\\?\\[\\^\\]\\$\\(\\)\\{\\}\\=\\!\\<\\>\\|\\:\\-])", "g"), "\\$1");
	search = search.replace(/%/g, '.*').replace(/_/g, '.');
	return RegExp('^' + search + '$', 'gi').test(this);
};
var consultarModelo_filter = function(coll) {
	var filtered = _.toArray(coll.filter(function(m) {
		return true;
	}));
	return filtered;
};
var consultarModelo_transform = function(model) {
	var fila = model.toJSON();
	return fila;
};
var consultarModelo_update = function(e) {};
_.defer(function() {
	Alloy.Collections.insp_contenido.fetch();
});
Alloy.Collections.insp_contenido.on('add change delete', function(ee) {
	consultarModelo_update(ee);
});
Alloy.Collections.insp_contenido.fetch();

function Click_vista(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	preguntarOpciones.show();

}

function Click_vista2(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	Alloy.createController("nuevo_contenido", {}).getView().open();

}

function Itemclick_listado(e) {

	e.cancelBubble = true;
	var objeto = e.section.getItemAt(e.itemIndex);
	var modelo = {},
		_modelo = [];
	var fila = {},
		fila_bak = {},
		info = {
			_template: objeto.template,
			_what: [],
			_seccion_ref: e.section.getHeaderTitle(),
			_model_id: -1
		},
		_tmp = {
			objmap: {}
		};
	if ('itemId' in e) {
		info._model_id = e.itemId;
		modelo._id = info._model_id;
		if (info._seccion_ref != '' && info._seccion_ref in _bind4section) {
			modelo._collection = _bind4section[info._seccion_ref];
			_tmp._coll = modelo._collection;
		}
	}
	var findVariables = require('fvariables');
	_.each(_list_templates[info._template], function(obj_id, id) {
		_.each(obj_id, function(valor, prop) {
			var llaves = findVariables(valor, '{', '}');
			_.each(llaves, function(llave) {
				_tmp.objmap[llave] = {
					id: id,
					prop: prop
				};
				fila[llave] = objeto[id][prop];
				if (id == e.bindId) info._what.push(llave);
			});
		});
	});
	info._what = info._what.join(',');
	fila_bak = JSON.parse(JSON.stringify(fila));
	if (Ti.App.deployType != 'production') console.log('click para editar contenido', {
		"dato": fila
	});
	/** 
	 * Enviamos a editar contenido, y pasamos parametro _dato para enviarle el id del contenido a editar 
	 */
	Alloy.createController("nuevo_contenido", {
		'_dato': fila,
		'__master_model': (typeof modelo !== 'undefined') ? modelo : {},
		'__modelo': (typeof _modelo !== 'undefined' && _modelo.length > 0) ? _modelo[0] : {}
	}).getView().open();
	_tmp.changed = false;
	_tmp.diff_keys = [];
	_.each(fila, function(value1, prop) {
		var had_samekey = false;
		_.each(fila_bak, function(value2, prop2) {
			if (prop == prop2 && value1 == value2) {
				had_samekey = true;
			} else if (!_.has(fila_bak, prop) || !_.has(fila, prop2)) {
				has_samekey = true;
			}
		});
		if (!had_samekey) _tmp.diff_keys.push(prop);
	});
	if (_tmp.diff_keys.length > 0) _tmp.changed = true;
	if (_tmp.changed == true) {
		_.each(_tmp.diff_keys, function(llave) {
			objeto[_tmp.objmap[llave].id][_tmp.objmap[llave].prop] = fila[llave];
		});
		e.section.updateItemAt(e.itemIndex, objeto);
	}

}

$.widgetBotonlargo.init({
	titulo: L('x8960895_traducir', 'GUARDAR DAÑOS'),
	__id: 'ALL1007469789',
	color: 'verde',
	onclick: Click_widgetBotonlargo
});

function Click_widgetBotonlargo(e) {

	var evento = e;
	$.widgetPreguntaco.ejecutar({});

}

$.widgetPreguntaco.init({
	titulo: L('x404129330_traducir', '¿EL ASEGURADO CONFIRMA ESTOS DATOS?'),
	__id: 'ALL1088806219',
	si: L('x1723413441_traducir', 'SI, Están correctos'),
	texto: L('x2083765231_traducir', 'El asegurado debe confirmar que los datos de esta sección están correctos'),
	pantalla: L('x2851883104_traducir', '¿ESTA DE ACUERDO?'),
	onno: no_widgetPreguntaco,
	onsi: si_widgetPreguntaco,
	no: L('x55492959_traducir', 'NO, Hay que modificar algo'),
	header: L('x252540350_traducir', 'celeste')
});

function si_widgetPreguntaco(e) {

	var evento = e;
	if (Ti.App.deployType != 'production') console.log('enviamos a documentos', {});
	Alloy.createController("documentos_index", {}).getView().open();

}

function no_widgetPreguntaco(e) {

	var evento = e;

}

function Postlayout_vista7(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.progreso.show();

}

function Postlayout_DAÑOS_EN_CONTENIDO(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	/** 
	 * Limpieza memoria ram 
	 */
	Alloy.Events.trigger('_cerrar_insp', {
		pantalla: 'recintos'
	});

}

var preguntarOpciones_opts = [L('x3481285698_traducir', 'Asegurado no puede seguir'), L('x3077268071_traducir', ' Se me acabo la bateria'), L('x479989047_traducir', ' Tuve un accidente'), L('x2376009830_traducir', ' Cancelar')];
var preguntarOpciones = Ti.UI.createOptionDialog({
	title: L('x1670176792_traducir', 'RAZON PARA CANCELAR INSPECCION ACTUAL'),
	options: preguntarOpciones_opts
});
preguntarOpciones.addEventListener('click', function(e) {
	var resp = preguntarOpciones_opts[e.index];
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
	if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
		var razon = "";
		if (resp == L('x3481285698_traducir', 'Asegurado no puede seguir')) {
			/** 
			 * Esto parece redundante, pero es para escapar de i18n el texto 
			 */
			razon = "Asegurado no puede seguir";
		}
		if (resp == L('x1862794075_traducir', 'Se me acabo la bateria')) {
			razon = "Se me acabo la bateria";
		}
		if (resp == L('x4012805111_traducir', 'Tuve un accidente')) {
			razon = "Tuve un accidente";
		}
		if ((_.isObject(razon) || (_.isString(razon)) && !_.isEmpty(razon)) || _.isNumber(razon) || _.isBoolean(razon)) {
			if (Ti.App.deployType != 'production') console.log('llamando servicio cancelarTarea', {});
			require('vars')['insp_cancelada'] = L('x3680765087_traducir', 'contenidos');
			Alloy.Collections[$.contenidos.config.adapter.collection_name].add($.contenidos);
			$.contenidos.save();
			Alloy.Collections[$.contenidos.config.adapter.collection_name].fetch();
			var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
			/** 
			 * Mostramos popup avisando que se esta cancelando la inspeccion 
			 */
			var vista7_visible = true;

			if (vista7_visible == 'si') {
				vista7_visible = true;
			} else if (vista7_visible == 'no') {
				vista7_visible = false;
			}
			$.vista7.setVisible(vista7_visible);

			var consultarURL = {};
			consultarURL.success = function(e) {
				var elemento = e,
					valor = e;
				/** 
				 * Ocultamos popup avisando que se esta cancelando la inspeccion 
				 */
				var vista7_visible = false;

				if (vista7_visible == 'si') {
					vista7_visible = true;
				} else if (vista7_visible == 'no') {
					vista7_visible = false;
				}
				$.vista7.setVisible(vista7_visible);

				/** 
				 * Enviamos a firma 
				 */
				Alloy.createController("firma_index", {}).getView().open();
				elemento = null, valor = null;
			};
			consultarURL.error = function(e) {
				var elemento = e,
					valor = e;
				if (Ti.App.deployType != 'production') console.log('hubo error llamando cancelarTarea, la agregamos a cola de salida', {
					"elemento": elemento
				});
				if (Ti.App.deployType != 'production') console.log('agregando servicio cancelarTarea a cola', {});
				/** 
				 * En el caso que haya habido un problema llamando al servicio cancelarTarea, guardamos los datos en un objeto 
				 */
				var datos = {
					id_inspector: seltarea.id_inspector,
					codigo_identificador: inspector.codigo_identificador,
					id_server: seltarea.id_server,
					num_caso: seltarea.num_caso,
					razon: razon
				};
				var insertarModelo_m = Alloy.Collections.cola;
				var insertarModelo_fila = Alloy.createModel('cola', {
					data: JSON.stringify(datos),
					id_tarea: seltarea.id_server,
					tipo: 'cancelar'
				});
				insertarModelo_m.add(insertarModelo_fila);
				insertarModelo_fila.save();
				_.defer(function() {});
				/** 
				 * Ocultamos popup avisando que se esta cancelando la inspeccion 
				 */
				var vista7_visible = false;

				if (vista7_visible == 'si') {
					vista7_visible = true;
				} else if (vista7_visible == 'no') {
					vista7_visible = false;
				}
				$.vista7.setVisible(vista7_visible);

				/** 
				 * Enviamos a firma 
				 */
				Alloy.createController("firma_index", {}).getView().open();
				elemento = null, valor = null;
			};
			require('helper').ajaxUnico('consultarURL', '' + String.format(L('x3244141284', '%1$scancelarTarea'), url_server.toString()) + '', 'POST', {
				id_inspector: seltarea.id_inspector,
				codigo_identificador: inspector.codigo_identificador,
				id_tarea: seltarea.id_server,
				num_caso: seltarea.num_caso,
				mensaje: razon,
				opcion: 0,
				tipo: 1
			}, 15000, consultarURL);
		}
	}
	resp = null;
});
_my_events['_cerrar_insp,ID_231485652'] = function(evento) {
	if (!_.isUndefined(evento.pantalla)) {
		if (evento.pantalla == L('x3680765087_traducir', 'contenidos')) {
			if (Ti.App.deployType != 'production') console.log('debug cerrando contenidos', {});
			var ID_1781710521_trycatch = {
				error: function(e) {
					if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
				}
			};
			try {
				ID_1781710521_trycatch.error = function(evento) {
					if (Ti.App.deployType != 'production') console.log('error cerrando contenidos', {});
				};
				$.DAÑOS_EN_CONTENIDO.close();
			} catch (e) {
				ID_1781710521_trycatch.error(e);
			}
		}
	} else {
		if (Ti.App.deployType != 'production') console.log('debug cerrando (todas) contenidos', {});
		var ID_1962200830_trycatch = {
			error: function(e) {
				if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
			}
		};
		try {
			ID_1962200830_trycatch.error = function(evento) {
				if (Ti.App.deployType != 'production') console.log('error cerrando contenidos', {});
			};
			$.DAÑOS_EN_CONTENIDO.close();
		} catch (e) {
			ID_1962200830_trycatch.error(e);
		}
	}
};
Alloy.Events.on('_cerrar_insp', _my_events['_cerrar_insp,ID_231485652']);
var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
	/** 
	 * restringimos contenidos mostrados a la inspeccion actual (por si los temporales aun tienen datos) 
	 */
	consultarModelo_filter = function(coll) {
		var filtered = coll.filter(function(m) {
			var _tests = [],
				_all_true = false,
				model = m.toJSON();
			_tests.push((model.id_inspeccion == seltarea.id_server));
			var _all_true_s = _.uniq(_tests);
			_all_true = (_all_true_s.length == 1 && _all_true_s[0] == true) ? true : false;
			return _all_true;
		});
		filtered = _.toArray(filtered);
		return filtered;
	};
	_.defer(function() {
		Alloy.Collections.insp_contenido.fetch();
	});
} else {
	/** 
	 * Borramos dummies 
	 */
	/** 
	 * Borramos dummies 
	 */
	var eliminarModelo_i = Alloy.Collections.insp_contenido;
	var sql = "DELETE FROM " + eliminarModelo_i.config.adapter.collection_name;
	var db = Ti.Database.open(eliminarModelo_i.config.adapter.db_name);
	db.execute(sql);
	db.close();
	eliminarModelo_i.trigger('remove');
	_.defer(function() {
		Alloy.Collections.insp_contenido.fetch();
	});
}
if (OS_IOS || OS_ANDROID) {
	$.DAÑOS_EN_CONTENIDO.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}
//$.DAÑOS_EN_CONTENIDO.open();
