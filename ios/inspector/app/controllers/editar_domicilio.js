var _bind4section = {};
var _list_templates = {};
var $otro_inspector = $.otro_inspector.toJSON();

var _activity;
if (OS_ANDROID) {
	_activity = $.EDITAR2.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'EDITAR2';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.EDITAR2_window.addEventListener('open', function(e) {
		abx.setStatusbarColor("#000000");
		abx.setBackgroundColor("white");
	});
}

function Click_vista3(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.EDITAR2.close();
	Alloy.Events.trigger('_close_editar');

}

function Click_vista4(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
		/** 
		 * Revisamos si el smartphone tiene internet 
		 */
		/** 
		 * Obtenemos los valores ingresados en los campos de texto y guardamos en variables 
		 */
		var direccion;
		direccion = $.EscribirDireccion.getValue();

		var data_nivel_2;
		data_nivel_2 = $.Escribir.getValue();

		var data_nivel_3;
		data_nivel_3 = $.EscribirNivel.getValue();

		var data_nivel_4;
		data_nivel_4 = $.EscribirNivel2.getValue();

		var data_nivel_5;
		data_nivel_5 = $.EscribirNivel3.getValue();

		/** 
		 * Recuperamos el detalle del pais seleccionado 
		 */
		var pais = ('pais' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['pais'] : '';
		/** 
		 * Creamos variable para identificar que no falten datos por ser ingresado en la pantalla 
		 */

		var falta_algo =
			(data_nivel_2 == "" && pais.label_nivel2 != "") ||
			(data_nivel_3 == "" && pais.label_nivel3 != "") ||
			(data_nivel_4 == "" && pais.label_nivel4 != "") ||
			(data_nivel_5 == "" && pais.label_nivel5 != "") ||
			(direccion == "")
		/** 
		 * Recuperamos la variable del nivel_1 seleccionado 
		 */
		var data_nivel_1 = ('data_nivel_1' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['data_nivel_1'] : '';
		if (Ti.App.deployType != 'production') console.log('detalle de niveles', {
			"nivel4": data_nivel_5,
			"nivel2": data_nivel_2,
			"nivel1": data_nivel_1,
			"direccion": direccion,
			"nivel3": data_nivel_3
		});
		if ((_.isObject(data_nivel_1) || _.isString(data_nivel_1)) && _.isEmpty(data_nivel_1)) {
			/** 
			 * Revisamos que no falten datos, y en caso que falten, avisamos al inspector 
			 */
			var preguntarAlerta4_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta4 = Ti.UI.createAlertDialog({
				title: L('x2185084353_traducir', 'Atención'),
				message: L('x3693432715_traducir', 'Seleccione región'),
				buttonNames: preguntarAlerta4_opts
			});
			preguntarAlerta4.addEventListener('click', function(e) {
				var suu = preguntarAlerta4_opts[e.index];
				suu = null;
				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta4.show();
		} else if ((_.isObject(direccion) || _.isString(direccion)) && _.isEmpty(direccion)) {
			var preguntarAlerta5_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta5 = Ti.UI.createAlertDialog({
				title: L('x2185084353_traducir', 'Atención'),
				message: L('x3153806545_traducir', 'Ingrese dirección'),
				buttonNames: preguntarAlerta5_opts
			});
			preguntarAlerta5.addEventListener('click', function(e) {
				var suu = preguntarAlerta5_opts[e.index];
				suu = null;
				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta5.show();
		} else if (falta_algo == true || falta_algo == 'true') {
			var preguntarAlerta6_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta6 = Ti.UI.createAlertDialog({
				title: L('x2185084353_traducir', 'Atención'),
				message: L('x1330841706_traducir', 'Ingrese datos faltantes'),
				buttonNames: preguntarAlerta6_opts
			});
			preguntarAlerta6.addEventListener('click', function(e) {
				var suu = preguntarAlerta6_opts[e.index];
				suu = null;
				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta6.show();
		} else {
			var data_nivel_1 = ('data_nivel_1' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['data_nivel_1'] : '';
			if (_.isString(data_nivel_1)) {
				var url = data_nivel_1.replace(" ", "+");
				url = url + "+" + data_nivel_2.replace(" ", "+");
				url = url + "+" + data_nivel_3.replace(" ", "+");
				url = url + "+" + data_nivel_4.replace(" ", "+");
				url = url + "+" + data_nivel_5.replace(" ", "+");
				url = url + "+" + direccion.replace(" ", "+");
				url = url + "+" + pais.nombre;
				url = encodeURI(url);
				url2 = "https://maps.googleapis.com/maps/api/geocode/json?address=" + url
			} else {
				var url = data_nivel_1.valor.replace(" ", "+");
				url = url + "+" + data_nivel_2.replace(" ", "+");
				url = url + "+" + data_nivel_3.replace(" ", "+");
				url = url + "+" + data_nivel_4.replace(" ", "+");
				url = url + "+" + data_nivel_5.replace(" ", "+");
				url = url + "+" + direccion.replace(" ", "+");
				url = url + "+" + pais.nombre;
				url = encodeURI(url);
				url2 = "https://maps.googleapis.com/maps/api/geocode/json?address=" + url
			}
			if (Ti.App.deployType != 'production') console.log('Consultando a Google si direccion es valida', {
				"url": url2
			});
			var consultarURL2 = {};
			consultarURL2.success = function(e) {
				var elemento = e,
					valor = e;
				if (Ti.App.deployType != 'production') console.log('respuesta de google', {
					"datos": elemento
				});
				if (elemento.status == L('x3610695981_traducir', 'OK')) {
					/** 
					 * Revisamos que la respuesta de google sea ok y asi verificar que existan datos 
					 */
					if (elemento.results[0].formatted_address.toLowerCase().indexOf(direccion.toLowerCase()) != -1) {
						/** 
						 * Si google retorno la misma direccion que ingreso el usuario 
						 */
						/** 
						 * Si la consulta al servidor fue exitosa, limpiamos la tabla del inspector, guardamos los datos del binding y finalmente una consulta a la tabla para poder actualizar la variable del inspector 
						 */
						/** 
						 * Si la consulta al servidor fue exitosa, limpiamos la tabla del inspector, guardamos los datos del binding y finalmente una consulta a la tabla para poder actualizar la variable del inspector 
						 */
						var eliminarModelo4_i = Alloy.Collections.inspectores;
						var sql = "DELETE FROM " + eliminarModelo4_i.config.adapter.collection_name;
						var db = Ti.Database.open(eliminarModelo4_i.config.adapter.db_name);
						db.execute(sql);
						db.close();
						eliminarModelo4_i.trigger('remove');
						/** 
						 * Recuperamos los datos del nivel1 para actualizarlo en la tabla del inspector, cargamos los datos en la tabla, limpiamos los datos del inspector viejo, guardamos los datos nuevos y actualizamos los datos de la variable inspector con los datos nuevo 
						 */
						var data_nivel_1 = ('data_nivel_1' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['data_nivel_1'] : '';
						var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
						var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
						if (Ti.App.deployType != 'production') console.log('contiene y guardo', {
							"nivel4": data_nivel_5,
							"nivel2": data_nivel_2,
							"nivel1": data_nivel_1,
							"direccion": direccion,
							"nivel3": data_nivel_3
						});
						$.otro_inspector.set({
							nivel4: data_nivel_4,
							id_nivel1: data_nivel_1.id_server,
							nivel2: data_nivel_2,
							nivel5: data_nivel_5,
							lat_dir: elemento.results[0].geometry.location.lat,
							lon_dir: elemento.results[0].geometry.location.lat,
							direccion: elemento.results[0].formatted_address.split(',')[0],
							correccion_direccion: 2,
							nivel3: data_nivel_3
						});
						if ('otro_inspector' in $) $otro_inspector = $.otro_inspector.toJSON();
						var consultarURL3 = {};
						consultarURL3.success = function(e) {
							var elemento = e,
								valor = e;
							Alloy.Collections[$.otro_inspector.config.adapter.collection_name].add($.otro_inspector);
							$.otro_inspector.save();
							Alloy.Collections[$.otro_inspector.config.adapter.collection_name].fetch();
							var ID_1310720795_func = function() {
								var consultarModelo3_i = Alloy.createCollection('inspectores');
								var consultarModelo3_i_where = '';
								consultarModelo3_i.fetch();
								var inspector = require('helper').query2array(consultarModelo3_i);
								if (inspector && inspector.length) {}
							};
							var ID_1310720795 = setTimeout(ID_1310720795_func, 1000 * 0.5);
							$.EDITAR2.close();
							Alloy.Events.trigger('_close_editar');
							elemento = null, valor = null;
						};
						consultarURL3.error = function(e) {
							var elemento = e,
								valor = e;
							/** 
							 * Si hubo error, no actualizamos nada y solo mostramos un mensaje de que no se pudo actualizar la informacion 
							 */
							var preguntarAlerta7_opts = [L('x1518866076_traducir', 'Aceptar')];
							var preguntarAlerta7 = Ti.UI.createAlertDialog({
								title: L('x57652245_traducir', 'Alerta'),
								message: L('x1693828390_traducir', 'Problema de conexión por favor intentar después'),
								buttonNames: preguntarAlerta7_opts
							});
							preguntarAlerta7.addEventListener('click', function(e) {
								var suu = preguntarAlerta7_opts[e.index];
								suu = null;
								e.source.removeEventListener("click", arguments.callee);
							});
							preguntarAlerta7.show();
							elemento = null, valor = null;
						};
						require('helper').ajaxUnico('consultarURL3', '' + String.format(L('x2725785119', '%1$seditarPerfilTipo1'), url_server.toString()) + '', 'POST', {
							id_inspector: inspector.id_server,
							pais: inspector.pais,
							nivel_1: data_nivel_1.id_server,
							nivel_2: data_nivel_2,
							nivel_3: data_nivel_3,
							nivel_4: data_nivel_4,
							nivel_5: data_nivel_5,
							direccion: direccion,
							correccion_direccion: 2,
							lat_dir: elemento.results[0].geometry.location.lat,
							lon_dir: elemento.results[0].geometry.location.lng,
							google: JSON.stringify(elemento.results[0].address_components)
						}, 15000, consultarURL3);
					} else {
						require('vars')[_var_scopekey]['resultado_google'] = elemento.results[0];
						/** 
						 * Modificamos la tabla con los datos que encuentra google 
						 */
						$.otro_inspector.set({
							nivel_4: data_nivel_4,
							nivel_2: data_nivel_2,
							lat_dir: elemento.results[0].geometry.location.lat,
							lon_dir: elemento.results[0].geometry.location.lat,
							nivel_3: data_nivel_3,
							nivel_1: data_nivel_1,
							direccion: direccion,
							correccion_direccion: 1,
							nivel_5: data_nivel_5
						});
						if ('otro_inspector' in $) $otro_inspector = $.otro_inspector.toJSON();
						/** 
						 * Abrimos dialogo de confirmar direccion que encontro google 
						 */
						var preguntarAlerta8_opts = [L('x3827418516_traducir', 'Si'), L('x4063104189_traducir', 'No'), L('x2632684629_traducir', 'Continuar')];
						var preguntarAlerta8 = Ti.UI.createAlertDialog({
							title: L('x4067820461_traducir', 'La direccion indicada no parece valida.\n ¿es esta la correcta?'),
							message: '' + elemento.results[0].formatted_address + '',
							buttonNames: preguntarAlerta8_opts
						});
						preguntarAlerta8.addEventListener('click', function(e) {
							var resp_pregunta = preguntarAlerta8_opts[e.index];
							if (Ti.App.deployType != 'production') console.log('PREGUNTAMOS !!', {});
							var data_nivel_1 = ('data_nivel_1' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['data_nivel_1'] : '';
							if (_.isString(data_nivel_1)) {
								$.otro_inspector.set({
									nivel_1: data_nivel_1
								});
								if ('otro_inspector' in $) $otro_inspector = $.otro_inspector.toJSON();
							} else {
								$.otro_inspector.set({
									nivel_1: data_nivel_1.id_server
								});
								if ('otro_inspector' in $) $otro_inspector = $.otro_inspector.toJSON();
							}
							if (resp_pregunta == L('x3827418516_traducir', 'Si')) {
								/** 
								 * Si la respuesta del usuario es un si, recuperamos variables del detalle de informacion de google, la url de uadjust, y los datos del inspector 
								 */
								var resultado_google = ('resultado_google' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['resultado_google'] : '';
								var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
								var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
								if (Ti.App.deployType != 'production') console.log('CONSULTAAAAAAA!!', {});
								if (Ti.App.deployType != 'production') console.log('CONSULTANDO DIRECCION!!!', {
									"nivel_4": data_nivel_4,
									"nivel_2": data_nivel_2,
									"google": JSON.stringify(resultado_google.address_components),
									"lat_dir": resultado_google.geometry.location.lat,
									"lon_dir": resultado_google.geometry.location.lng,
									"id_inspector": inspector.id_server,
									"nivel_3": data_nivel_3,
									"nivel_1": data_nivel_1.id_server,
									"pais": inspector.pais,
									"direccion": direccion,
									"correccion_direccion": 0,
									"_url": String.format(L('x2725785119', '%1$seditarPerfilTipo1'), url_server.toString()),
									"nivel_5": data_nivel_5
								});
								var consultarURL4 = {};
								consultarURL4.success = function(e) {
									var elemento = e,
										valor = e;
									/** 
									 * Si la consulta al servidor fue exitosa, limpiamos la tabla del inspector, guardamos los datos del binding y finalmente una consulta a la tabla para poder actualizar la variable del inspector 
									 */
									/** 
									 * Si la consulta al servidor fue exitosa, limpiamos la tabla del inspector, guardamos los datos del binding y finalmente una consulta a la tabla para poder actualizar la variable del inspector 
									 */
									var eliminarModelo5_i = Alloy.Collections.inspectores;
									var sql = "DELETE FROM " + eliminarModelo5_i.config.adapter.collection_name;
									var db = Ti.Database.open(eliminarModelo5_i.config.adapter.db_name);
									db.execute(sql);
									db.close();
									eliminarModelo5_i.trigger('remove');
									Alloy.Collections[$.otro_inspector.config.adapter.collection_name].add($.otro_inspector);
									$.otro_inspector.save();
									Alloy.Collections[$.otro_inspector.config.adapter.collection_name].fetch();
									var consultarModelo4_i = Alloy.createCollection('inspectores');
									var consultarModelo4_i_where = '';
									consultarModelo4_i.fetch();
									var inspector = require('helper').query2array(consultarModelo4_i);
									if (Ti.App.deployType != 'production') console.log('SIGUIENTE !!!!!!!!', {});
									$.EDITAR2.close();
									Alloy.Events.trigger('_close_editar');
									elemento = null, valor = null;
								};
								consultarURL4.error = function(e) {
									var elemento = e,
										valor = e;
									/** 
									 * Si hubo error, no actualizamos nada y solo mostramos un mensaje de que no se pudo actualizar la informacion 
									 */
									var preguntarAlerta9_opts = [L('x1518866076_traducir', 'Aceptar')];
									var preguntarAlerta9 = Ti.UI.createAlertDialog({
										title: L('x57652245_traducir', 'Alerta'),
										message: L('x1693828390_traducir', 'Problema de conexión por favor intentar después'),
										buttonNames: preguntarAlerta9_opts
									});
									preguntarAlerta9.addEventListener('click', function(e) {
										var suu = preguntarAlerta9_opts[e.index];
										suu = null;
										e.source.removeEventListener("click", arguments.callee);
									});
									preguntarAlerta9.show();
									elemento = null, valor = null;
								};
								require('helper').ajaxUnico('consultarURL4', '' + String.format(L('x2725785119', '%1$seditarPerfilTipo1'), url_server.toString()) + '', 'POST', {
									id_inspector: inspector.id_server,
									pais: inspector.pais,
									nivel_1: data_nivel_1.id_server,
									nivel_2: data_nivel_2,
									nivel_3: data_nivel_3,
									nivel_4: data_nivel_4,
									nivel_5: data_nivel_5,
									direccion: direccion,
									correccion_direccion: 0,
									lat_dir: resultado_google.geometry.location.lat,
									lon_dir: resultado_google.geometry.location.lng,
									google: JSON.stringify(resultado_google.address_components)
								}, 15000, consultarURL4);
							} else if (resp_pregunta == 'No') {
								if (Ti.App.deployType != 'production') console.log('uy... si apreto la opcion no', {});
								/** 
								 * Dejamos los datos vacios para que el usuario vuelva a escribir la informacion 
								 */
								$.otro_inspector.set({
									lat_dir: '',
									lon_dir: '',
									direccion: ''
								});
								if ('otro_inspector' in $) $otro_inspector = $.otro_inspector.toJSON();
								preguntarAlerta8.hide();
							} else {
								if (Ti.App.deployType != 'production') console.log('ENTRA EN OTRA CONDICION', {});
								$.otro_inspector.set({
									google: '',
									lat_dir: '',
									lon_dir: ''
								});
								if ('otro_inspector' in $) $otro_inspector = $.otro_inspector.toJSON();
							}
							resp_pregunta = null;
							e.source.removeEventListener("click", arguments.callee);
						});
						preguntarAlerta8.show();
					}
				} else {
					var preguntarAlerta10_opts = [L('x3610695981_traducir', 'OK')];
					var preguntarAlerta10 = Ti.UI.createAlertDialog({
						title: L('x3237162386_traducir', 'Atencion'),
						message: L('x3185903755_traducir', 'Error al verificar la dirección, reintente'),
						buttonNames: preguntarAlerta10_opts
					});
					preguntarAlerta10.addEventListener('click', function(e) {
						var xdd = preguntarAlerta10_opts[e.index];
						xdd = null;
						e.source.removeEventListener("click", arguments.callee);
					});
					preguntarAlerta10.show();
				}
				elemento = null, valor = null;
			};
			consultarURL2.error = function(e) {
				var elemento = e,
					valor = e;
				var preguntarAlerta11_opts = [L('x3610695981_traducir', 'OK')];
				var preguntarAlerta11 = Ti.UI.createAlertDialog({
					title: L('x3237162386_traducir', 'Atencion'),
					message: L('x3117757034_traducir', 'Error al conectarse a internet, reintente'),
					buttonNames: preguntarAlerta11_opts
				});
				preguntarAlerta11.addEventListener('click', function(e) {
					var xdd = preguntarAlerta11_opts[e.index];
					xdd = null;
					e.source.removeEventListener("click", arguments.callee);
				});
				preguntarAlerta11.show();
				elemento = null, valor = null;
			};
			require('helper').ajaxUnico('consultarURL2', 'https://maps.googleapis.com/maps/api/geocode/json?address=' + url + '&key=AIzaSyDdRVLOtyNvZUAleL0mDO-mAWd2M9AircQ', 'GET', {}, 15000, consultarURL2);
		}
	} else {
		var preguntarAlerta12_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta12 = Ti.UI.createAlertDialog({
			title: L('x57652245_traducir', 'Alerta'),
			message: L('x607021664_traducir', 'No hay internet, no se pueden efectuar los cambios'),
			buttonNames: preguntarAlerta12_opts
		});
		preguntarAlerta12.addEventListener('click', function(e) {
			var suu = preguntarAlerta12_opts[e.index];
			suu = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta12.show();
	}
}

$.widgetHeader2.init({
	titulo: L('x258069688_traducir', 'Editar domicilio'),
	__id: 'ALL1399189593',
	avance: '',
	onclick: Click_widgetHeader2
});

function Click_widgetHeader2(e) {

	var evento = e;
	$.Escribir.blur();
	$.EscribirNivel.blur();
	$.EscribirNivel2.blur();
	$.EscribirNivel3.blur();

}

$.widgetModal.init({
	titulo: L('x3180818344_traducir', 'SELECCIONE REGION'),
	__id: 'ALL1379917834',
	left: 0,
	onrespuesta: Respuesta_widgetModal,
	campo: L('x147780672_traducir', 'Region'),
	onabrir: Abrir_widgetModal,
	right: 0,
	seleccione: L('x677075728_traducir', 'seleccione region'),
	onafterinit: Afterinit_widgetModal
});

function Afterinit_widgetModal(e) {

	var evento = e;
	/** 
	 * Recuperamos los datos de la region del pais y mandamos informacion al widget 
	 */
	var data_region = ('data_region' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['data_region'] : '';
	$.widgetModal.data({
		data: data_region
	});

}

function Abrir_widgetModal(e) {

	var evento = e;
	/** 
	 * Desenfocamos campos de texto 
	 */
	$.Escribir.blur();
	$.EscribirNivel.blur();
	$.EscribirNivel2.blur();
	$.EscribirNivel3.blur();

}

function Respuesta_widgetModal(e) {

	var evento = e;
	/** 
	 * Editamos el texto para mostrar la opcion seleccionada en widget 
	 */
	$.widgetModal.labels({
		valor: evento.valor
	});
	/** 
	 * Guardamos en la variable el id de la region seleccionada 
	 */
	require('vars')[_var_scopekey]['data_nivel_1'] = evento.item;

}



(function() {
	var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
	var consultarURL5 = {};
	consultarURL5.success = function(e) {
		var elemento = e,
			valor = e;
		if (elemento == false || elemento == 'false') {
			/** 
			 * Si en la consulta los datos obtenidos no existen, mostramos mensaje para decir que hubo problemas al obtener los datos. Caso contrario, limpiamos tablas y cargamos los datos obtenidos desde el servidor en las tablas 
			 */
			var preguntarAlerta13_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta13 = Ti.UI.createAlertDialog({
				title: L('x57652245_traducir', 'Alerta'),
				message: L('x1693828390_traducir', 'Problema de conexión por favor intentar después'),
				buttonNames: preguntarAlerta13_opts
			});
			preguntarAlerta13.addEventListener('click', function(e) {
				var suu = preguntarAlerta13_opts[e.index];
				suu = null;
				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta13.show();
		} else {
			var eliminarModelo6_i = Alloy.Collections.pais;
			var sql = "DELETE FROM " + eliminarModelo6_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo6_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo6_i.trigger('remove');
			var eliminarModelo7_i = Alloy.Collections.nivel1;
			var sql = "DELETE FROM " + eliminarModelo7_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo7_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo7_i.trigger('remove');
			var eliminarModelo8_i = Alloy.Collections.experiencia_oficio;
			var sql = "DELETE FROM " + eliminarModelo8_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo8_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo8_i.trigger('remove');
			var elemento_regiones = elemento.regiones;
			var insertarModelo4_m = Alloy.Collections.nivel1;
			var db_insertarModelo4 = Ti.Database.open(insertarModelo4_m.config.adapter.db_name);
			db_insertarModelo4.execute('BEGIN');
			_.each(elemento_regiones, function(insertarModelo4_fila, pos) {
				db_insertarModelo4.execute('INSERT INTO nivel1 (id_label, id_server, dif_horaria, id_pais) VALUES (?,?,?,?)', insertarModelo4_fila.subdivision_name, insertarModelo4_fila.id, 2, insertarModelo4_fila.id_pais);
			});
			db_insertarModelo4.execute('COMMIT');
			db_insertarModelo4.close();
			db_insertarModelo4 = null;
			insertarModelo4_m.trigger('change');
			var elemento_paises = elemento.paises;
			var insertarModelo5_m = Alloy.Collections.pais;
			var db_insertarModelo5 = Ti.Database.open(insertarModelo5_m.config.adapter.db_name);
			db_insertarModelo5.execute('BEGIN');
			_.each(elemento_paises, function(insertarModelo5_fila, pos) {
				db_insertarModelo5.execute('INSERT INTO pais (label_nivel2, moneda, nombre, label_nivel4, label_codigo_identificador, label_nivel3, id_server, label_nivel1, iso, sis_metrico, niveles_pais, id_pais, label_nivel5, lenguaje) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)', insertarModelo5_fila.nivel_2, insertarModelo5_fila.moneda, insertarModelo5_fila.nombre, insertarModelo5_fila.nivel_4, insertarModelo5_fila.label_codigo_identificador, insertarModelo5_fila.nivel_3, insertarModelo5_fila.id, insertarModelo5_fila.nivel_1, insertarModelo5_fila.iso, insertarModelo5_fila.sis_metrico, insertarModelo5_fila.niveles_pais, insertarModelo5_fila.idpais, insertarModelo5_fila.nivel_5, insertarModelo5_fila.lenguaje);
			});
			db_insertarModelo5.execute('COMMIT');
			db_insertarModelo5.close();
			db_insertarModelo5 = null;
			insertarModelo5_m.trigger('change');
			var elemento_experiencia_oficio = elemento.experiencia_oficio;
			var insertarModelo6_m = Alloy.Collections.experiencia_oficio;
			var db_insertarModelo6 = Ti.Database.open(insertarModelo6_m.config.adapter.db_name);
			db_insertarModelo6.execute('BEGIN');
			_.each(elemento_experiencia_oficio, function(insertarModelo6_fila, pos) {
				db_insertarModelo6.execute('INSERT INTO experiencia_oficio (nombre, id_server, id_pais) VALUES (?,?,?)', insertarModelo6_fila.nombre, insertarModelo6_fila.id, insertarModelo6_fila.idpais);
			});
			db_insertarModelo6.execute('COMMIT');
			db_insertarModelo6.close();
			db_insertarModelo6 = null;
			insertarModelo6_m.trigger('change');
		}
		elemento = null, valor = null;
	};
	consultarURL5.error = function(e) {
		var elemento = e,
			valor = e;
		var preguntarAlerta14_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta14 = Ti.UI.createAlertDialog({
			title: L('x57652245_traducir', 'Alerta'),
			message: L('x1693828390_traducir', 'Problema de conexión por favor intentar después'),
			buttonNames: preguntarAlerta14_opts
		});
		preguntarAlerta14.addEventListener('click', function(e) {
			var suu = preguntarAlerta14_opts[e.index];
			suu = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta14.show();
		elemento = null, valor = null;
	};
	require('helper').ajaxUnico('consultarURL5', '' + String.format(L('x2963862531', '%1$sobtenerPais'), url_server.toString()) + '', 'POST', {}, 15000, consultarURL5);
	/** 
	 * Consultamos la tabla del inspector para cargarla en binding, y guardamos en una variable el id del nivel_1 del inspector, y consultamos la tabla de pais para saber cuantos niveles tiene para desplegar la informacion correcta 
	 */
	var consultarModelo5_i = Alloy.createCollection('inspectores');
	var consultarModelo5_i_where = '';
	consultarModelo5_i.fetch();
	var inspector = require('helper').query2array(consultarModelo5_i);
	require('vars')[_var_scopekey]['data_nivel_1'] = inspector[0].id_nivel1;
	if (Ti.App.deployType != 'production') console.log('carga de los datos del inspector', {
		"asd": inspector[0]
	});
	var consultarModelo6_i = Alloy.createCollection('pais');
	var consultarModelo6_i_where = 'id_server=\'' + inspector[0].pais + '\'';
	consultarModelo6_i.fetch({
		query: 'SELECT * FROM pais WHERE id_server=\'' + inspector[0].pais + '\''
	});
	var pais_iter = require('helper').query2array(consultarModelo6_i);
	var pais = pais_iter[0];
	if (Ti.App.deployType != 'production') console.log('detalle del pais', {
		"datos": pais
	});
	var niveles_pais = 1;
	$.widgetModal.labels({
		nivel1: pais.label_nivel1
	});
	if ((_.isObject(pais.label_nivel2) || _.isString(pais.label_nivel2)) && _.isEmpty(pais.label_nivel2)) {
		var scroll_view;
		scroll_view = $.scroll;
		var nivel_2_view;
		nivel_2_view = $.vista7;
		scroll_view.remove(nivel_2_view);
	} else {
		pais.label_nivel2 = pais.label_nivel2[0].toUpperCase() + pais.label_nivel2.substr(1);
		$.Nivel.setText(pais.label_nivel2);

		$.Escribir.setHintText(String.format(L('x27730249_traducir', 'escribir %1$s'), pais.label_nivel2.toString()));

		$.Escribir.setValue(inspector[0].nivel2);

	}
	if ((_.isObject(pais.label_nivel3) || _.isString(pais.label_nivel3)) && _.isEmpty(pais.label_nivel3)) {
		var scroll_view;
		scroll_view = $.scroll;
		var nivel_3_view;
		nivel_3_view = $.vista8;
		scroll_view.remove(nivel_3_view);
	} else {
		pais.label_nivel3 = pais.label_nivel3[0].toUpperCase() + pais.label_nivel3.substr(1);
		$.Nivel2.setText(pais.label_nivel3);

		$.EscribirNivel.setHintText(String.format(L('x27730249_traducir', 'escribir %1$s'), pais.label_nivel3.toString()));

		$.EscribirNivel.setValue(inspector[0].nivel3);

	}
	if ((_.isObject(pais.label_nivel4) || _.isString(pais.label_nivel4)) && _.isEmpty(pais.label_nivel4)) {
		var scroll_view;
		scroll_view = $.scroll;
		var nivel_4_view;
		nivel_4_view = $.vista9;
		scroll_view.remove(nivel_4_view);
	} else {
		pais.label_nivel4 = pais.label_nivel4[0].toUpperCase() + pais.label_nivel4.substr(1);
		$.Nivel4.setText(pais.label_nivel4);

		$.EscribirNivel2.setHintText(String.format(L('x27730249_traducir', 'escribir %1$s'), pais.label_nivel4.toString()));

		$.EscribirNivel2.setValue(inspector[0].nivel4);

	}
	if ((_.isObject(pais.label_nivel5) || _.isString(pais.label_nivel5)) && _.isEmpty(pais.label_nivel5)) {
		var scroll_view;
		scroll_view = $.scroll;
		var nivel_5_view;
		nivel_5_view = $.vista10;
		scroll_view.remove(nivel_5_view);
	} else {
		pais.label_nivel5 = pais.label_nivel5[0].toUpperCase() + pais.label_nivel5.substr(1);
		$.Nivel6.setText(pais.label_nivel5);

		$.EscribirNivel3.setHintText(String.format(L('x27730249_traducir', 'escribir %1$s'), pais.label_nivel5.toString()));

		$.EscribirNivel3.setValue(inspector[0].nivel5);

	}
	$.EscribirDireccion.setValue(inspector[0].direccion);

	var consultarModelo7_i = Alloy.createCollection('nivel1');
	var consultarModelo7_i_where = 'id_pais=\'' + pais.id_pais + '\'';
	consultarModelo7_i.fetch({
		query: 'SELECT * FROM nivel1 WHERE id_pais=\'' + pais.id_pais + '\''
	});
	var region_list = require('helper').query2array(consultarModelo7_i);
	var item_index = 0;
	_.each(region_list, function(item, item_pos, item_list) {
		item_index += 1;
		item.valor = item.id_label;
	});
	require('vars')[_var_scopekey]['data_region'] = region_list;
	var ID_1122488460_func = function() {
		var consultarModelo8_i = Alloy.createCollection('nivel1');
		var consultarModelo8_i_where = 'id_pais=\'' + pais.id_pais + '\' AND id_server=\'' + inspector[0].id_nivel1 + '\'';
		consultarModelo8_i.fetch({
			query: 'SELECT * FROM nivel1 WHERE id_pais=\'' + pais.id_pais + '\' AND id_server=\'' + inspector[0].id_nivel1 + '\''
		});
		var dato_n1 = require('helper').query2array(consultarModelo8_i);
		if (Ti.App.deployType != 'production') console.log('detalle de dato_n1', {
			"datos": dato_n1[0]
		});
		if (Ti.App.deployType != 'production') console.log('detalle de dato_n1', {
			"datos": dato_n1[0].id_label
		});
		$.widgetModal.labels({
			valor: dato_n1[0].id_label
		});
	};
	var ID_1122488460 = setTimeout(ID_1122488460_func, 1000 * 0.2);
	require('vars')[_var_scopekey]['pais'] = pais;
	$.otro_inspector.set({
		apellido_materno: inspector[0].apellido_materno,
		id_nivel1: inspector[0].id_nivel1,
		lat_dir: inspector[0].lat_dir,
		disponibilidad_viajar_pais: inspector[0].disponibilidad_viajar_pais,
		uuid: inspector[0].uidd,
		fecha_nacimiento: inspector[0].fecha_nacimiento,
		d1: inspector[0].d1,
		d2: inspector[0].d2,
		password: inspector[0].password,
		pais: inspector[0].pais,
		direccion: inspector[0].direccion,
		d3: inspector[0].d3,
		nivel3: inspector[0].nivel3,
		d5: inspector[0].d5,
		d4: inspector[0].d4,
		disponibilidad_fechas: inspector[0].disponibilidad_fechas,
		d7: inspector[0].d7,
		nivel4: inspector[0].nivel4,
		nombre: inspector[0].nombre,
		nivel5: inspector[0].nivel5,
		nivel2: inspector[0].nivel2,
		disponibilidad_horas: inspector[0].disponibilidad_horas,
		disponibilidad_viajar_ciudad: inspector[0].disponibilidad_viajar_ciudad,
		lon_dir: inspector[0].lon_dir,
		id_server: inspector[0].id_server,
		d6: inspector[0].d6,
		telefono: inspector[0].telefono,
		experiencia_detalle: inspector[0].experiencia_detalle,
		disponibilidad: inspector[0].disponibilidad,
		codigo_identificador: inspector[0].codigo_identificador,
		experiencia_oficio: inspector[0].experiencia_oficio,
		direccion_correccion: inspector[0].direccion_correccion,
		apellido_paterno: inspector[0].apellido_paterno,
		correo: inspector[0].correo
	});
	if ('otro_inspector' in $) $otro_inspector = $.otro_inspector.toJSON();
	/** 
	 * Guardamos variables con datos en blanco, esto sirve para poder hacer la consulta a google y mandar los datos que el usuario ingreso en la pantalla 
	 */
	require('vars')[_var_scopekey]['data_nivel_1'] = '';
	require('vars')[_var_scopekey]['data_nivel_2'] = '';
	require('vars')[_var_scopekey]['data_nivel_3'] = '';
	require('vars')[_var_scopekey]['data_nivel_4'] = '';
	require('vars')[_var_scopekey]['data_nivel_5'] = '';
})();

function Postlayout_EDITAR2(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var ID_515670629_func = function() {
		/** 
		 * Una vez ya cargada la tarea, dejamos la variable limpia para poder abrir otras pantallas 
		 */
		require('vars')['var_abriendo'] = '';
	};
	var ID_515670629 = setTimeout(ID_515670629_func, 1000 * 0.2);

}
if (OS_IOS || OS_ANDROID) {
	$.EDITAR2.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}