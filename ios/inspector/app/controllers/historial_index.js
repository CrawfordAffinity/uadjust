var _bind4section = {};
var _list_templates = {
	"tarea_historia": {
		"Label3": {
			"text": "{comuna}"
		},
		"vista6": {},
		"vista8": {},
		"vista4": {},
		"Label2": {
			"text": "{hora_termino}"
		},
		"vista10": {},
		"vista15": {
			"visible": "{bt_enviartarea}"
		},
		"vista12": {},
		"Label": {
			"text": "{direccion}"
		},
		"vista5": {
			"idlocal": "{id}",
			"estado": "{estado_tarea}"
		},
		"vista11": {},
		"vista14": {},
		"Label4": {
			"text": "{ciudad}, {pais}"
		},
		"vista16": {
			"visible": "{enviando_tarea}"
		},
		"vista13": {},
		"vista7": {},
		"ENVIAR": {},
		"imagen": {},
		"vista9": {}
	}
};

var _activity;
if (OS_ANDROID) {
	_activity = $.HISTORIAL.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'HISTORIAL';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.HISTORIAL_window.addEventListener('open', function(e) {
		abx.setStatusbarColor("#FFFFFF");
		abx.setBackgroundColor("#ffffff");
	});
}

function Touchstart_vista(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.iconoCerrar.setColor('#ff0033');


}

function Touchend_vista(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	/** 
	 * detenemos intervalo que revisa envios activos 
	 */
	var ID_875505225_detener = true;

	var detenerRepetir = function(mula) {
		ID_875505225_continuar = false;
		clearTimeout(_out_vars['ID_875505225']._run);
	};
	detenerRepetir(ID_875505225_detener);

	$.iconoCerrar.setColor('#2d9edb');

	$.HISTORIAL.close();

}

function Click_vista2(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var vista17_visible = true;

	if (vista17_visible == 'si') {
		vista17_visible = true;
	} else if (vista17_visible == 'no') {
		vista17_visible = false;
	}
	$.vista17.setVisible(vista17_visible);

	require('vars')['enviando_inspecciones'] = L('x4261170317', 'true');
	/** 
	 * Ocultamos enviar todos 
	 */
	var vista2_visible = false;

	if (vista2_visible == 'si') {
		vista2_visible = true;
	} else if (vista2_visible == 'no') {
		vista2_visible = false;
	}
	$.vista2.setVisible(vista2_visible);

	/** 
	 * Ocultamos cerrar historial 
	 */
	var vista_visible = false;

	if (vista_visible == 'si') {
		vista_visible = true;
	} else if (vista_visible == 'no') {
		vista_visible = false;
	}
	$.vista.setVisible(vista_visible);


}

function Load_imagen(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var evento = e;
	/** 
	 * Elemento es el objeto que llamo el evento, como es virtual no hay referencia de nodo para un modificar 
	 */
	elemento.start();

}

function Itemclick_listado(e) {

	e.cancelBubble = true;
	var objeto = e.section.getItemAt(e.itemIndex);
	var modelo = {},
		_modelo = [];
	var fila = {},
		fila_bak = {},
		info = {
			_template: objeto.template,
			_what: [],
			_seccion_ref: e.section.getHeaderTitle(),
			_model_id: -1
		},
		_tmp = {
			objmap: {}
		};
	if ('itemId' in e) {
		info._model_id = e.itemId;
		modelo._id = info._model_id;
		if (info._seccion_ref != '' && info._seccion_ref in _bind4section) {
			modelo._collection = _bind4section[info._seccion_ref];
			_tmp._coll = modelo._collection;
		}
	}
	if (info._seccion_ref != '' && info._seccion_ref in _bind4section) {
		if (Alloy.Collections[_tmp._coll].config.adapter.type == 'sql') {
			_tmp._inst = Alloy.Collections[_tmp._coll];
			_tmp._id = Alloy.Collections[_tmp._coll].config.adapter.idAttribute;
			_tmp._dbsql = 'SELECT * FROM ' + Alloy.Collections[_tmp._coll].config.adapter.collection_name + ' WHERE ' + _tmp._id + ' = ' + e.itemId;
			_tmp._db = Ti.Database.open(Alloy.Collections[_tmp._coll].config.adapter.db_name);
			_modelo = _tmp._db.execute(_tmp._dbsql);
			var values = [],
				fieldNames = [];
			var fieldCount = _.isFunction(_modelo.fieldCount) ? _modelo.fieldCount() : _modelo.fieldCount;
			var getField = _modelo.field;
			var i = 0;
			for (; fieldCount > i; i++) fieldNames.push(_modelo.fieldName(i));
			while (_modelo.isValidRow()) {
				var o = {};
				for (i = 0; fieldCount > i; i++) o[fieldNames[i]] = getField(i);
				values.push(o);
				_modelo.next();
			}
			_modelo = values;
			_tmp._db.close();
		} else {
			_tmp._search = {};
			_tmp._search[_tmp._id] = e.itemId + '';
			_modelo = Alloy.Collections[_tmp._coll].fetch(_tmp._search);
		}
	}
	var findVariables = require('fvariables');
	_.each(_list_templates[info._template], function(obj_id, id) {
		_.each(obj_id, function(valor, prop) {
			var llaves = findVariables(valor, '{', '}');
			_.each(llaves, function(llave) {
				_tmp.objmap[llave] = {
					id: id,
					prop: prop
				};
				fila[llave] = objeto[id][prop];
				if (id == e.bindId) info._what.push(llave);
			});
		});
	});
	info._what = info._what.join(',');
	fila_bak = JSON.parse(JSON.stringify(fila));
	if (fila.bt_enviartarea == true || fila.bt_enviartarea == 'true') {
		/** 
		 * Solo ejecutamos si esta el boton 
		 */
		var fila = _.extend(fila, {
			enviando_tarea: L('x4261170317', 'true'),
			bt_enviartarea: L('x734881840_traducir', 'false')
		});
		require('vars')[_var_scopekey]['enviar_id'] = fila.id;
		if (Ti.App.deployType != 'production') console.log('fila tarea por enviar pinchada', {
			"fila": fila
		});
		/** 
		 * Modificamos registro en tabla por enviando tarea 
		 */
		var consultarModelo_i = Alloy.createCollection('historial_tareas');
		var consultarModelo_i_where = 'id=\'' + fila.id + '\'';
		consultarModelo_i.fetch({
			query: 'SELECT * FROM historial_tareas WHERE id=\'' + fila.id + '\''
		});
		var tareaclick = require('helper').query2array(consultarModelo_i);
		var db = Ti.Database.open(consultarModelo_i.config.adapter.db_name);
		if (consultarModelo_i_where == '') {
			var sql = 'UPDATE ' + consultarModelo_i.config.adapter.collection_name + ' SET estado_envio=0';
		} else {
			var sql = 'UPDATE ' + consultarModelo_i.config.adapter.collection_name + ' SET estado_envio=0 WHERE ' + consultarModelo_i_where;
		}
		db.execute(sql);
		db.close();
		require('vars')['enviando_inspecciones'] = L('x4261170317', 'true');
		/** 
		 * Mostramos popup 
		 */
		var vista17_visible = true;

		if (vista17_visible == 'si') {
			vista17_visible = true;
		} else if (vista17_visible == 'no') {
			vista17_visible = false;
		}
		$.vista17.setVisible(vista17_visible);

		var vista2_visible = false;

		if (vista2_visible == 'si') {
			vista2_visible = true;
		} else if (vista2_visible == 'no') {
			vista2_visible = false;
		}
		$.vista2.setVisible(vista2_visible);

		var vista_visible = false;

		if (vista_visible == 'si') {
			vista_visible = true;
		} else if (vista_visible == 'no') {
			vista_visible = false;
		}
		$.vista.setVisible(vista_visible);

	}
	_tmp.changed = false;
	_tmp.diff_keys = [];
	_.each(fila, function(value1, prop) {
		var had_samekey = false;
		_.each(fila_bak, function(value2, prop2) {
			if (prop == prop2 && value1 == value2) {
				had_samekey = true;
			} else if (!_.has(fila_bak, prop) || !_.has(fila, prop2)) {
				has_samekey = true;
			}
		});
		if (!had_samekey) _tmp.diff_keys.push(prop);
	});
	if (_tmp.diff_keys.length > 0) _tmp.changed = true;
	if (_tmp.changed == true) {
		_.each(_tmp.diff_keys, function(llave) {
			objeto[_tmp.objmap[llave].id][_tmp.objmap[llave].prop] = fila[llave];
		});
		e.section.updateItemAt(e.itemIndex, objeto);
	}

}

function Load_imagen2(e) {
	/** 
	 * Evento que se ejecuta una vez cargada la vista 
	 */

	e.cancelBubble = true;
	var elemento = e.source;
	var evento = e;
	/** 
	 * Hace que la animacion se ejecute 
	 */
	elemento.start();

}

function Longpress_vista17(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var preguntarAlerta_opts = [L('x1518866076_traducir', 'Aceptar'), L('x2376009830_traducir', ' Cancelar')];
	var preguntarAlerta = Ti.UI.createAlertDialog({
		title: L('x3836149088_traducir', 'Abortar envio'),
		message: L('x4264692327_traducir', '¿ Esta seguro que desea cancelar los envios ?'),
		buttonNames: preguntarAlerta_opts
	});
	preguntarAlerta.addEventListener('click', function(e) {
		var seguro = preguntarAlerta_opts[e.index];
		if (seguro == L('x1518866076_traducir', 'Aceptar')) {
			require('vars')['enviando_inspecciones'] = L('x734881840_traducir', 'false');
			var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
			/** 
			 * Desactivamos estado_envio de todas las tareas de este inspector 
			 */
			var consultarModelo2_i = Alloy.createCollection('historial_tareas');
			var consultarModelo2_i_where = 'id_inspector=\'' + inspector.id_server + '\'';
			consultarModelo2_i.fetch({
				query: 'SELECT * FROM historial_tareas WHERE id_inspector=\'' + inspector.id_server + '\''
			});
			var cancelar_envios = require('helper').query2array(consultarModelo2_i);
			var db = Ti.Database.open(consultarModelo2_i.config.adapter.db_name);
			if (consultarModelo2_i_where == '') {
				var sql = 'UPDATE ' + consultarModelo2_i.config.adapter.collection_name + ' SET estado_envio=0';
			} else {
				var sql = 'UPDATE ' + consultarModelo2_i.config.adapter.collection_name + ' SET estado_envio=0 WHERE ' + consultarModelo2_i_where;
			}
			db.execute(sql);
			db.close();
			var ID_1279375049 = null;
			if ('refrescar_tareas' in require('funciones')) {
				ID_1279375049 = require('funciones').refrescar_tareas({});
			} else {
				try {
					ID_1279375049 = f_refrescar_tareas({});
				} catch (ee) {}
			}
			/** 
			 * Ocultamos popup 
			 */
			var vista17_visible = false;

			if (vista17_visible == 'si') {
				vista17_visible = true;
			} else if (vista17_visible == 'no') {
				vista17_visible = false;
			}
			$.vista17.setVisible(vista17_visible);

			var vista2_visible = true;

			if (vista2_visible == 'si') {
				vista2_visible = true;
			} else if (vista2_visible == 'no') {
				vista2_visible = false;
			}
			$.vista2.setVisible(vista2_visible);

			var vista_visible = true;

			if (vista_visible == 'si') {
				vista_visible = true;
			} else if (vista_visible == 'no') {
				vista_visible = false;
			}
			$.vista.setVisible(vista_visible);

		}
		seguro = null;

		e.source.removeEventListener("click", arguments.callee);
	});
	preguntarAlerta.show();

}

var f_refrescar_tareas = function(x_params) {
	var xyz = x_params['xyz'];
	/** 
	 * Limpiamos la tabla de tareas 
	 */
	var listado_borrar = false;

	var limpiarListado = function(animar) {
		var a_nimar = (typeof animar == 'undefined') ? false : animar;
		if (OS_IOS && a_nimar == true) {
			var s_ecciones = $.listado.getSections();
			_.each(s_ecciones, function(obj_id, pos) {
				$.listado.deleteSectionAt(0, {
					animated: true
				});
			});
		} else {
			$.listado.setSections([]);
		}
	};
	limpiarListado(listado_borrar);

	var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
	/** 
	 * no es necesario filtrar por id_inspector, ya que esta tabla se reemplaza con los datos del login 
	 */
	var consultarModelo3_i = Alloy.createCollection('historial_tareas');
	var consultarModelo3_i_where = 'ORDER BY FECHA_TERMINO DESC';
	consultarModelo3_i.fetch({
		query: 'SELECT * FROM historial_tareas ORDER BY FECHA_TERMINO DESC'
	});
	var tareas = require('helper').query2array(consultarModelo3_i);
	/** 
	 * Inicializamos la variable ultima_fecha en vacio, esto servira por si encontramos una tarea en la misma fecha y la podremos guardar en la misma seccion que la tarea anterior (si fueron realizadas el mismo dia) 
	 */
	require('vars')[_var_scopekey]['ultima_fecha'] = '';
	var tarea_index = 0;
	_.each(tareas, function(tarea, tarea_pos, tarea_list) {
		tarea_index += 1;
		/** 
		 * Recuperamos la variable para ver si la tarea anterior tiene la misma fecha que la tarea que estamos recorriendo 
		 */
		var ultima_fecha = ('ultima_fecha' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['ultima_fecha'] : '';
		/** 
		 * Formateamos fecha para mostrar en pantalla 
		 */
		var moment = require('alloy/moment');
		var fecha_hoy = moment(new Date()).format('YYYY-MM-DD');
		/** 
		 * Creamos variable ayer con la fecha del dia de ayer (segun la que tenga el smartphone) 
		 */
		var ayer = new Date();
		ayer.setDate(ayer.getDate() - 1);;
		/** 
		 * Formateamos la fecha de ayer para mostrar en pantalla 
		 */
		var moment = require('alloy/moment');
		var formatearFecha2 = ayer;
		var fecha_ayer = moment(formatearFecha2).format('YYYY-MM-DD');
		if (ultima_fecha != tarea.fecha_termino) {
			/** 
			 * Revisamos la fecha de la tarea, si es la misma que la anterior, no nos cambiamos de seccion, caso contrario vemos de que dia es y dejamos esa tarea en la fecha que corresponde 
			 */
			if (tarea.fecha_termino == fecha_hoy) {
				var seccionListadoHoy = Titanium.UI.createListSection({
					headerTitle: L('x1916403066_traducir', 'hoy')
				});
				var headerListado = Titanium.UI.createView({
					height: Ti.UI.FILL,
					width: Ti.UI.FILL
				});
				var vista20 = Titanium.UI.createView({
					height: Ti.UI.SIZE,
					layout: 'vertical',
					width: Ti.UI.FILL,
					backgroundColor: '#F7F7F7'
				});
				var HOY = Titanium.UI.createLabel({
					text: L('x3835609072_traducir', 'HOY'),
					color: '#999999',
					touchEnabled: false,
					font: {
						fontFamily: 'SFUIText-Medium',
						fontSize: '14dp'
					}

				});
				vista20.add(HOY);

				headerListado.add(vista20);
				seccionListadoHoy.setHeaderView(headerListado);
				$.listado.appendSection(seccionListadoHoy);
				require('vars')[_var_scopekey]['ultima_fecha'] = tarea.fecha_termino;
			} else if (tarea.fecha_termino == fecha_ayer) {
				var seccionListadoAyer = Titanium.UI.createListSection({
					headerTitle: L('x1602196311_traducir', 'ayer')
				});
				var headerListado2 = Titanium.UI.createView({
					height: Ti.UI.FILL,
					width: Ti.UI.FILL
				});
				var vista21 = Titanium.UI.createView({
					height: Ti.UI.SIZE,
					layout: 'vertical',
					width: Ti.UI.FILL,
					backgroundColor: '#F7F7F7'
				});
				var AYER = Titanium.UI.createLabel({
					text: L('x1776975587_traducir', 'AYER'),
					color: '#999999',
					touchEnabled: false,
					font: {
						fontFamily: 'SFUIText-Medium',
						fontSize: '14dp'
					}

				});
				vista21.add(AYER);

				headerListado2.add(vista21);
				seccionListadoAyer.setHeaderView(headerListado2);
				$.listado.appendSection(seccionListadoAyer);
				require('vars')[_var_scopekey]['ultima_fecha'] = tarea.fecha_termino;
			} else {
				var seccionListadoOtra = Titanium.UI.createListSection({
					headerTitle: L('x910537343_traducir', 'otra')
				});
				var headerListado3 = Titanium.UI.createView({
					height: Ti.UI.FILL,
					width: Ti.UI.FILL
				});
				var vista22 = Titanium.UI.createView({
					height: Ti.UI.SIZE,
					layout: 'vertical',
					width: Ti.UI.FILL,
					backgroundColor: '#F7F7F7'
				});
				var Tareafechatermino = Titanium.UI.createLabel({
					text: tarea.fecha_termino,
					color: '#999999',
					touchEnabled: false,
					font: {
						fontFamily: 'SFUIText-Medium',
						fontSize: '14dp'
					}

				});
				vista22.add(Tareafechatermino);

				headerListado3.add(vista22);
				seccionListadoOtra.setHeaderView(headerListado3);
				$.listado.appendSection(seccionListadoOtra);
				require('vars')[_var_scopekey]['ultima_fecha'] = tarea.fecha_termino;
			}
		}
		if ((_.isObject(tarea.nivel_2) || _.isString(tarea.nivel_2)) && _.isEmpty(tarea.nivel_2)) {
			/** 
			 * Define ultimo nivel de la tarea 
			 */
			var tarea = _.extend(tarea, {
				ultimo_nivel: tarea.nivel_1
			});
		} else if ((_.isObject(tarea.nivel_3) || _.isString(tarea.nivel_3)) && _.isEmpty(tarea.nivel_3)) {
			var tarea = _.extend(tarea, {
				ultimo_nivel: tarea.nivel_2
			});
		} else if ((_.isObject(tarea.nivel_4) || _.isString(tarea.nivel_4)) && _.isEmpty(tarea.nivel_4)) {
			var tarea = _.extend(tarea, {
				ultimo_nivel: tarea.nivel_3
			});
		} else if ((_.isObject(tarea.nivel_5) || _.isString(tarea.nivel_5)) && _.isEmpty(tarea.nivel_5)) {
			var tarea = _.extend(tarea, {
				ultimo_nivel: tarea.nivel_4
			});
		} else {
			var tarea = _.extend(tarea, {
				ultimo_nivel: tarea.nivel_5
			});
		}
		/** 
		 * Formateamos hora_termino de hh:mm:ss a HH:MM hrs en la variable de tarea 
		 */
		var hora_t = tarea.hora_termino.split(':');
		/** 
		 * Actualizamos la variable de la tarea con el estado de bt_enviartarea, enviando_tarea y la hora de termino 
		 */
		var tarea = _.extend(tarea, {
			bt_enviartarea: L('x734881840_traducir', 'false'),
			enviando_tarea: L('x734881840_traducir', 'false'),
			hora_termino: String.format(L('x1815173162_traducir', '%1$s:%2$shrs'), (hora_t[0]) ? hora_t[0].toString() : '', (hora_t[1]) ? hora_t[1].toString() : '')
		});
		if (tarea.estado_tarea == 8) {
			/** 
			 * Revisamos el estado de la tarea 
			 */
			/** 
			 * Revisamos que el directorio de la inspeccion exista en el telefono y tenga archivos 
			 */
			var tarea = _.extend(tarea, {
				bt_enviartarea: L('x4261170317', 'true'),
				enviando_tarea: L('x734881840_traducir', 'false')
			});
		} else if (tarea.estado_tarea == 9) {
			/** 
			 * Revisamos que el directorio de la inspeccion exista en el telefono y tenga archivos 
			 */
			var tarea = _.extend(tarea, {
				bt_enviartarea: L('x4261170317', 'true'),
				enviando_tarea: L('x734881840_traducir', 'false')
			});
		}
		if (tarea.bt_enviartarea == true || tarea.bt_enviartarea == 'true') {
			if (tarea.estado_envio == 0 || tarea.estado_envio == '0') {
				/** 
				 * Revisamos que el equipo tenga imagenes guardadas para la tarea actual 
				 */

				var fotos = [];
				var ID_1988302164_f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + tarea.id_server + '/');
				if (ID_1988302164_f.exists() == true) {
					fotos = ID_1988302164_f.getDirectoryListing();
				}
				if (Ti.App.deployType != 'production') console.log(String.format(L('x2358582387_traducir', 'probando si hay fotos para id %1$s'), tarea.id_server.toString()), {
					"fotos": fotos
				});
				if (fotos && fotos.length) {
					/** 
					 * Cambiamos el estado de bt_enviartarea y enviando_tarea 
					 */
					var tarea = _.extend(tarea, {
						bt_enviartarea: L('x4261170317', 'true'),
						enviando_tarea: L('x734881840_traducir', 'false')
					});
				} else {
					var tarea = _.extend(tarea, {
						bt_enviartarea: L('x734881840_traducir', 'false'),
						enviando_tarea: L('x734881840_traducir', 'false')
					});
				}
			} else if (tarea.estado_envio == 1 || tarea.estado_envio == '1') {
				/** 
				 * Cambiamos el estado de bt_enviartarea y enviando_tarea 
				 */
				var tarea = _.extend(tarea, {
					bt_enviartarea: L('x734881840_traducir', 'false'),
					enviando_tarea: L('x4261170317', 'true')
				});
			} else {
				/** 
				 * Caso estado:null 
				 */
				var fotos = [];
				var ID_516777710_f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + tarea.id_server + '/');
				if (ID_516777710_f.exists() == true) {
					fotos = ID_516777710_f.getDirectoryListing();
				}
				if (Ti.App.deployType != 'production') console.log(String.format(L('x2358582387_traducir', 'probando si hay fotos para id %1$s'), tarea.id_server.toString()), {
					"fotos": fotos
				});
				if (fotos && fotos.length) {
					var tarea = _.extend(tarea, {
						bt_enviartarea: L('x4261170317', 'true'),
						enviando_tarea: L('x734881840_traducir', 'false')
					});
				} else {
					var tarea = _.extend(tarea, {
						bt_enviartarea: L('x734881840_traducir', 'false'),
						enviando_tarea: L('x734881840_traducir', 'false')
					});
				}
			}
		}
		if (tarea.fecha_termino == fecha_hoy) {
			/** 
			 * Agregamos items tareas a la seccion de las tareas del dia de hoy, ayer u otra fecha 
			 */
			var itemListado = [{
				Label3: {
					text: tarea.ultimo_nivel
				},
				vista6: {},
				vista8: {},
				vista4: {},
				Label2: {
					text: tarea.hora_termino
				},
				vista10: {},
				vista15: {
					visible: tarea.bt_enviartarea
				},
				vista12: {},
				Label: {
					text: tarea.direccion
				},
				vista5: {
					idlocal: tarea.id_server,
					estado: tarea.estado_tarea
				},
				vista11: {},
				template: 'tarea_historia',
				vista14: {},
				Label4: {
					text: String.format(L('x1487588533_traducir', '%1$s, %2$s'), (tarea.nivel_2) ? tarea.nivel_2.toString() : '', (tarea.pais) ? tarea.pais.toString() : '')
				},
				vista16: {
					visible: tarea.enviando_tarea
				},
				vista13: {},
				vista7: {},
				ENVIAR: {},
				imagen: {},
				vista9: {}

			}];
			var itemListado_secs = {};
			_.map($.listado.getSections(), function(itemListado_valor, itemListado_indice) {
				itemListado_secs[itemListado_valor.getHeaderTitle()] = itemListado_indice;
				return itemListado_valor;
			});
			if ('' + L('x1916403066_traducir', 'hoy') + '' in itemListado_secs) {
				$.listado.sections[itemListado_secs['' + L('x1916403066_traducir', 'hoy') + '']].appendItems(itemListado);
			} else {
				console.log('DEBUG:dynamic listItem points to a invalid section id: check attribute _seccion.');
			}
		} else if (tarea.fecha_termino == fecha_ayer) {
			var itemListado2 = [{
				Label3: {
					text: tarea.ultimo_nivel
				},
				vista6: {},
				vista8: {},
				vista4: {},
				Label2: {
					text: tarea.hora_termino
				},
				vista10: {},
				vista15: {
					visible: tarea.bt_enviartarea
				},
				vista12: {},
				Label: {
					text: tarea.direccion
				},
				vista5: {
					idlocal: tarea.id_server,
					estado: tarea.estado_tarea
				},
				vista11: {},
				template: 'tarea_historia',
				vista14: {},
				Label4: {
					text: String.format(L('x1487588533_traducir', '%1$s, %2$s'), (tarea.nivel_2) ? tarea.nivel_2.toString() : '', (tarea.pais) ? tarea.pais.toString() : '')
				},
				vista16: {
					visible: tarea.enviando_tarea
				},
				vista13: {},
				vista7: {},
				ENVIAR: {},
				imagen: {},
				vista9: {}

			}];
			var itemListado2_secs = {};
			_.map($.listado.getSections(), function(itemListado2_valor, itemListado2_indice) {
				itemListado2_secs[itemListado2_valor.getHeaderTitle()] = itemListado2_indice;
				return itemListado2_valor;
			});
			if ('' + L('x1602196311_traducir', 'ayer') + '' in itemListado2_secs) {
				$.listado.sections[itemListado2_secs['' + L('x1602196311_traducir', 'ayer') + '']].appendItems(itemListado2);
			} else {
				console.log('DEBUG:dynamic listItem points to a invalid section id: check attribute _seccion.');
			}
		} else {
			var itemListado3 = [{
				Label3: {
					text: tarea.ultimo_nivel
				},
				vista6: {},
				vista8: {},
				vista4: {},
				Label2: {
					text: tarea.hora_termino
				},
				vista10: {},
				vista15: {
					visible: tarea.bt_enviartarea
				},
				vista12: {},
				Label: {
					text: tarea.direccion
				},
				vista5: {
					idlocal: tarea.id_server,
					estado: tarea.estado_tarea
				},
				vista11: {},
				template: 'tarea_historia',
				vista14: {},
				Label4: {
					text: String.format(L('x1487588533_traducir', '%1$s, %2$s'), (tarea.nivel_2) ? tarea.nivel_2.toString() : '', (tarea.pais) ? tarea.pais.toString() : '')
				},
				vista16: {
					visible: tarea.enviando_tarea
				},
				vista13: {},
				vista7: {},
				ENVIAR: {},
				imagen: {},
				vista9: {}

			}];
			var itemListado3_secs = {};
			_.map($.listado.getSections(), function(itemListado3_valor, itemListado3_indice) {
				itemListado3_secs[itemListado3_valor.getHeaderTitle()] = itemListado3_indice;
				return itemListado3_valor;
			});
			if ('' + L('x910537343_traducir', 'otra') + '' in itemListado3_secs) {
				$.listado.sections[itemListado3_secs['' + L('x910537343_traducir', 'otra') + '']].appendItems(itemListado3);
			} else {
				console.log('DEBUG:dynamic listItem points to a invalid section id: check attribute _seccion.');
			}
		}
		/** 
		 * Limpiamos memoria 
		 */
		fechahoy = null, ayer = null, fecha_ayer = null, hora_t = null;
	});
	/** 
	 * Limpiar memoria 
	 */
	tarea = null, tareas = null;
	return null;
};
var ciclo = 0;
var ID_875505225_continuar = true;
_out_vars['ID_875505225'] = {
	_remove: ["clearTimeout(_out_vars['ID_875505225']._run)"] 
};
var ID_875505225_func = function() {
	ciclo = ciclo + 1;
	var enviando_inspecciones = ('enviando_inspecciones' in require('vars')) ? require('vars')['enviando_inspecciones'] : '';
	if (enviando_inspecciones == true || enviando_inspecciones == 'true') {
		if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
			/** 
			 * Tratamos de enviar imagenes 
			 */
			var enviar_id = ('enviar_id' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['enviar_id'] : '';
			if ((_.isObject(enviar_id) || (_.isString(enviar_id)) && !_.isEmpty(enviar_id)) || _.isNumber(enviar_id) || _.isBoolean(enviar_id)) {
				/** 
				 * si enviar_id esta definido es porque se solito un envio en particular. 
				 */
				/** 
				 * Revisamos que la inspeccion tenga archivos en el directorio 
				 */

				var fotos = [];
				var ID_1233051231_f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + enviar_id + '/');
				if (ID_1233051231_f.exists() == true) {
					fotos = ID_1233051231_f.getDirectoryListing();
				}
				if (Ti.App.deployType != 'production') console.log(String.format(L('x3071089469_traducir', 'fotos de %1$s'), enviar_id.toString()), {
					"fotos": fotos
				});
				var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
				/** 
				 * Actualizamos el estado de envio de la inspeccion en la tabla 
				 */
				var consultarModelo4_i = Alloy.createCollection('historial_tareas');
				var consultarModelo4_i_where = 'id=\'' + enviar_id + '\'';
				consultarModelo4_i.fetch({
					query: 'SELECT * FROM historial_tareas WHERE id=\'' + enviar_id + '\''
				});
				var tareaclick = require('helper').query2array(consultarModelo4_i);
				var db = Ti.Database.open(consultarModelo4_i.config.adapter.db_name);
				if (consultarModelo4_i_where == '') {
					var sql = 'UPDATE ' + consultarModelo4_i.config.adapter.collection_name + ' SET estado_envio=1';
				} else {
					var sql = 'UPDATE ' + consultarModelo4_i.config.adapter.collection_name + ' SET estado_envio=1 WHERE ' + consultarModelo4_i_where;
				}
				db.execute(sql);
				db.close();
				/** 
				 * Llamamos la funcion refrescar_tareas 
				 */
				var ID_972710997 = null;
				if ('refrescar_tareas' in require('funciones')) {
					ID_972710997 = require('funciones').refrescar_tareas({});
				} else {
					try {
						ID_972710997 = f_refrescar_tareas({});
					} catch (ee) {}
				}
				var qfoto_index = 0;
				_.each(fotos, function(qfoto, qfoto_pos, qfoto_list) {
					qfoto_index += 1;
					if (Ti.App.deployType != 'production') console.log('leyendo binario', {});
					/** 
					 * Leemos la imagen y la almacenamos en variable 
					 */

					var firmabin = '';
					var ID_502219582_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + enviar_id);
					if (ID_502219582_d.exists() == true) {
						var ID_502219582_f = Ti.Filesystem.getFile(ID_502219582_d.resolve(), qfoto);
						if (ID_502219582_f.exists() == true) {
							firmabin = ID_502219582_f.read();
						}
						ID_502219582_f = null;
					}
					ID_502219582_d = null;
					if (Ti.App.deployType != 'production') console.log('enviando a servidor', {});
					/** 
					 * Hacemos consulta al servidor mandando las fotos de la inspeccion 
					 */
					var resp = null;
					console.log('DEBUG WEB: requesting url:' + String.format(L('x757910167', '%1$ssubirImagenes'), url_server.toString()) + ' with data:', {
						_method: 'POST',
						_params: {
							id_tarea: enviar_id,
							archivo: qfoto,
							imagen: firmabin,
							app_id: String.format(L('x1234417110_traducir', '%1$s,%2$s'), (qfoto) ? qfoto.toString() : '', (enviar_id) ? enviar_id.toString() : '')
						},
						_timeout: '20000'
					});
					require('helper').ajaxUnico(String.format(L('x1234417110_traducir', '%1$s,%2$s'), (qfoto) ? qfoto.toString() : '', (enviar_id) ? enviar_id.toString() : ''), '' + String.format(L('x757910167', '%1$ssubirImagenes'), url_server.toString()) + '', 'POST', {
						id_tarea: enviar_id,
						archivo: qfoto,
						imagen: firmabin,
						app_id: String.format(L('x1234417110_traducir', '%1$s,%2$s'), (qfoto) ? qfoto.toString() : '', (enviar_id) ? enviar_id.toString() : '')
					}, 20000, {
						success: function(x9) {
							resp = x9;
							if (resp.error != 0 && resp.error != '0') {
								/** 
								 * Si el servidor responde con error 
								 */
								if (Ti.App.deployType != 'production') console.log('fallo envio de imagen (en servidor)', {
									"qfoto": qfoto,
									"resp": resp
								});
								/** 
								 * Recuperamos variable 
								 */
								var enviar_id = ('enviar_id' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['enviar_id'] : '';
								var pendientes = ('pendientes' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['pendientes'] : '';
								if (resp.mensaje.toLowerCase().indexOf(L('x3185409421_traducir', 'insertar la imagen').toLowerCase()) != -1) {
									/** 
									 * Revisamos la respuesta del servidor 
									 */
									/** 
									 * imagen ya existe en servidor, borramos de equipo y marcamos como enviada 
									 */
									var info = resp.app_id.split(',');
									/** 
									 * Borramos archivo 
									 */

									var ID_1001674794_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + info[1]);
									var ID_1001674794_f = Ti.Filesystem.getFile(ID_1001674794_d.resolve(), info[0]);
									if (ID_1001674794_f.exists() == true) ID_1001674794_f.deleteFile();
									if (Ti.App.deployType != 'production') console.log('foto enviada borrada de equipo', {
										"info": info
									});
									/** 
									 * Modificamos la tarea actualizando el estado de envio 
									 */
									var consultarModelo5_i = Alloy.createCollection('historial_tareas');
									var consultarModelo5_i_where = 'id=\'' + enviar_id + '\'';
									consultarModelo5_i.fetch({
										query: 'SELECT * FROM historial_tareas WHERE id=\'' + enviar_id + '\''
									});
									var tareaclick = require('helper').query2array(consultarModelo5_i);
									var db = Ti.Database.open(consultarModelo5_i.config.adapter.db_name);
									if (consultarModelo5_i_where == '') {
										var sql = 'UPDATE ' + consultarModelo5_i.config.adapter.collection_name + ' SET estado_envio=0';
									} else {
										var sql = 'UPDATE ' + consultarModelo5_i.config.adapter.collection_name + ' SET estado_envio=0 WHERE ' + consultarModelo5_i_where;
									}
									db.execute(sql);
									if (consultarModelo5_i_where == '') {
										var sql = 'UPDATE ' + consultarModelo5_i.config.adapter.collection_name + ' SET estado_tarea=10';
									} else {
										var sql = 'UPDATE ' + consultarModelo5_i.config.adapter.collection_name + ' SET estado_tarea=10 WHERE ' + consultarModelo5_i_where;
									}
									db.execute(sql);
									db.close();
								} else {
									/** 
									 * otro error, modificamos la tarea actualizando el estado de envio 
									 */
									var consultarModelo6_i = Alloy.createCollection('historial_tareas');
									var consultarModelo6_i_where = 'id=\'' + enviar_id + '\'';
									consultarModelo6_i.fetch({
										query: 'SELECT * FROM historial_tareas WHERE id=\'' + enviar_id + '\''
									});
									var tareaclick = require('helper').query2array(consultarModelo6_i);
									var db = Ti.Database.open(consultarModelo6_i.config.adapter.db_name);
									if (consultarModelo6_i_where == '') {
										var sql = 'UPDATE ' + consultarModelo6_i.config.adapter.collection_name + ' SET estado_envio=0';
									} else {
										var sql = 'UPDATE ' + consultarModelo6_i.config.adapter.collection_name + ' SET estado_envio=0 WHERE ' + consultarModelo6_i_where;
									}
									db.execute(sql);
									db.close();
								}
								if ((_.has(pendientes, enviar_id)) == true || (_.has(pendientes, enviar_id)) == 'true') {
									/** 
									 * Revisamos que el objeto pendientes contenga la propiedad enviar_id, si contiene, borramos la propiedad enviar_id del objeto y actualizamos 
									 */
									/** 
									 * Eliminamos la propiedad enviar_id del objeto pendientes y actualizamos la variable 
									 */
									delete pendientes[enviar_id];
									require('vars')[_var_scopekey]['pendientes'] = pendientes;
								}
								var pendientes = ('pendientes' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['pendientes'] : '';
								require('vars')[_var_scopekey]['enviar_id'] = '';
								if ((Object.keys(pendientes).length) == 0 || (Object.keys(pendientes).length) == '0') {
									/** 
									 * Revisamos si el objeto tiene propiedades 
									 */
									/** 
									 * Tratamos de cancelar envio y quitamos popup y refrescamos listado 
									 */
									require('vars')['enviando_inspecciones'] = L('x734881840_traducir', 'false');
									var ID_1867246625 = null;
									if ('refrescar_tareas' in require('funciones')) {
										ID_1867246625 = require('funciones').refrescar_tareas({});
									} else {
										try {
											ID_1867246625 = f_refrescar_tareas({});
										} catch (ee) {}
									}
									/** 
									 * Ocultamos popup 
									 */
									var vista17_visible = false;

									if (vista17_visible == 'si') {
										vista17_visible = true;
									} else if (vista17_visible == 'no') {
										vista17_visible = false;
									}
									$.vista17.setVisible(vista17_visible);

									/** 
									 * Mostramos enviar todos 
									 */
									var vista2_visible = true;

									if (vista2_visible == 'si') {
										vista2_visible = true;
									} else if (vista2_visible == 'no') {
										vista2_visible = false;
									}
									$.vista2.setVisible(vista2_visible);

									/** 
									 * Mostramos cerrar historial 
									 */
									var vista_visible = true;

									if (vista_visible == 'si') {
										vista_visible = true;
									} else if (vista_visible == 'no') {
										vista_visible = false;
									}
									$.vista.setVisible(vista_visible);

									/** 
									 * rervisamos si no queda ninguna imagen por enviar (desactivamos enviar_todos) 
									 */
									var faltan = false;
									/** 
									 * 27-feb esto se podria mejorar, agregando soporte para filtrar por estado_tarea = 8,9 y contar los registros. 
									 */
									var consultarModelo7_i = Alloy.createCollection('historial_tareas');
									var consultarModelo7_i_where = '';
									consultarModelo7_i.fetch();
									var tareastest = require('helper').query2array(consultarModelo7_i);
									var test_index = 0;
									_.each(tareastest, function(test, test_pos, test_list) {
										test_index += 1;
										if (require('helper').arr_contains('8,9'.toLowerCase().split(','), test.estado_tarea.toString().toLowerCase())) {
											/** 
											 * Revisamos que el equipo tenga imagenes guardadas para la tarea actual y actualizamos la variable faltan 
											 */

											var fotos = [];
											var ID_668109677_f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + test.id_server + '/');
											if (ID_668109677_f.exists() == true) {
												fotos = ID_668109677_f.getDirectoryListing();
											}
											if (fotos && fotos.length == 0) {
												faltan = true;
											}
										}
									});
									if (faltan == false || faltan == 'false') {
										if (Ti.App.deployType != 'production') console.log('esconder en la consulta web', {});
										var vista2_visible = false;

										if (vista2_visible == 'si') {
											vista2_visible = true;
										} else if (vista2_visible == 'no') {
											vista2_visible = false;
										}
										$.vista2.setVisible(vista2_visible);

									}
									/** 
									 * esto es para refrescar badge de pantalla perfil 
									 */

									Alloy.Events.trigger('refrescar_historial');
								}
							} else if ((_.isObject(resp.app_id) || (_.isString(resp.app_id)) && !_.isEmpty(resp.app_id)) || _.isNumber(resp.app_id) || _.isBoolean(resp.app_id)) {
								var pendientes = ('pendientes' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['pendientes'] : '';
								var info = resp.app_id.split(',');
								var ID_1203936130_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + info[1]);
								var ID_1203936130_f = Ti.Filesystem.getFile(ID_1203936130_d.resolve(), info[0]);
								if (ID_1203936130_f.exists() == true) ID_1203936130_f.deleteFile();
								if (Ti.App.deployType != 'production') console.log('foto enviada borrada de equipo', {
									"info": info
								});
								var testmas = [];
								var ID_1036625800_f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + info[1] + '/');
								if (ID_1036625800_f.exists() == true) {
									testmas = ID_1036625800_f.getDirectoryListing();
								}
								var consultarModelo8_i = Alloy.createCollection('historial_tareas');
								var consultarModelo8_i_where = 'id=\'' + info[1] + '\'';
								consultarModelo8_i.fetch({
									query: 'SELECT * FROM historial_tareas WHERE id=\'' + info[1] + '\''
								});
								var tareaclick = require('helper').query2array(consultarModelo8_i);
								var db = Ti.Database.open(consultarModelo8_i.config.adapter.db_name);
								if (consultarModelo8_i_where == '') {
									var sql = 'UPDATE ' + consultarModelo8_i.config.adapter.collection_name + ' SET estado_envio=0';
								} else {
									var sql = 'UPDATE ' + consultarModelo8_i.config.adapter.collection_name + ' SET estado_envio=0 WHERE ' + consultarModelo8_i_where;
								}
								db.execute(sql);
								if (consultarModelo8_i_where == '') {
									var sql = 'UPDATE ' + consultarModelo8_i.config.adapter.collection_name + ' SET estado_tarea=10';
								} else {
									var sql = 'UPDATE ' + consultarModelo8_i.config.adapter.collection_name + ' SET estado_tarea=10 WHERE ' + consultarModelo8_i_where;
								}
								db.execute(sql);
								db.close();
								info = null;
								if ((_.has(pendientes, enviar_id)) == true || (_.has(pendientes, enviar_id)) == 'true') {
									delete pendientes[enviar_id];
									require('vars')[_var_scopekey]['pendientes'] = pendientes;
								}
								var pendientes = ('pendientes' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['pendientes'] : '';
								if ((Object.keys(pendientes).length) == 0 || (Object.keys(pendientes).length) == '0') {
									/** 
									 * Revisamos que pendientes no tenga mas registros pendientes 
									 */
									if (testmas && testmas.length == 0) {
										if (Ti.App.deployType != 'production') console.log('no hay mas imagenes para tarea activa, desactivamos popup', {});
										/** 
										 * Recuperamos variables 
										 */
										require('vars')[_var_scopekey]['enviar_id'] = '';
										require('vars')['enviando_inspecciones'] = L('x734881840_traducir', 'false');
										/** 
										 * Refrescamos la lista de tareas 
										 */
										var ID_1985286020 = null;
										if ('refrescar_tareas' in require('funciones')) {
											ID_1985286020 = require('funciones').refrescar_tareas({});
										} else {
											try {
												ID_1985286020 = f_refrescar_tareas({});
											} catch (ee) {}
										}
										/** 
										 * Ocultamos popup 
										 */
										var vista17_visible = false;

										if (vista17_visible == 'si') {
											vista17_visible = true;
										} else if (vista17_visible == 'no') {
											vista17_visible = false;
										}
										$.vista17.setVisible(vista17_visible);

										/** 
										 * Mostramos Enviar Todos 
										 */
										var vista2_visible = true;

										if (vista2_visible == 'si') {
											vista2_visible = true;
										} else if (vista2_visible == 'no') {
											vista2_visible = false;
										}
										$.vista2.setVisible(vista2_visible);

										/** 
										 * Mostramos cerrar historial 
										 */
										var vista_visible = true;

										if (vista_visible == 'si') {
											vista_visible = true;
										} else if (vista_visible == 'no') {
											vista_visible = false;
										}
										$.vista.setVisible(vista_visible);

										/** 
										 * Revisamos si no queda ninguna imagen por enviar (desactivamos enviar_todos) 
										 */
										var faltan = false;
										/** 
										 * 27-feb esto se podria mejorar, agregando soporte para filtrar por estado_tarea = 8,9 y contar los registros. 
										 */
										var consultarModelo9_i = Alloy.createCollection('historial_tareas');
										var consultarModelo9_i_where = '';
										consultarModelo9_i.fetch();
										var tareastest = require('helper').query2array(consultarModelo9_i);
										var test_index = 0;
										_.each(tareastest, function(test, test_pos, test_list) {
											test_index += 1;
											if (require('helper').arr_contains('8,9'.toLowerCase().split(','), test.estado_tarea.toString().toLowerCase())) {
												/** 
												 * Revisamos que el equipo tenga imagenes guardadas para la tarea actual 
												 */

												var fotos = [];
												var ID_1873863856_f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + test.id_server + '/');
												if (ID_1873863856_f.exists() == true) {
													fotos = ID_1873863856_f.getDirectoryListing();
												}
												if (fotos && fotos.length == 0) {}
											}
										});
										if (faltan == false || faltan == 'false') {
											var vista2_visible = false;

											if (vista2_visible == 'si') {
												vista2_visible = true;
											} else if (vista2_visible == 'no') {
												vista2_visible = false;
											}
											$.vista2.setVisible(vista2_visible);

											if (Ti.App.deployType != 'production') console.log('esconder en la otra consulta web', {});
										}
										/** 
										 * esto es para refrescar badge de pantalla perfil 
										 */

										Alloy.Events.trigger('refrescar_historial');
									}
								} else {
									if (testmas && testmas.length == 0) {
										require('vars')[_var_scopekey]['enviar_id'] = '';
										var ID_213243253 = null;
										if ('refrescar_tareas' in require('funciones')) {
											ID_213243253 = require('funciones').refrescar_tareas({});
										} else {
											try {
												ID_213243253 = f_refrescar_tareas({});
											} catch (ee) {}
										}
									}
								}
							}
						}
					});
					if (Ti.App.deployType != 'production') console.log('debug qfoto', {
						"ciclo": ciclo,
						"qfoto": qfoto
					});
					/** 
					 * Limpiamos memoria 
					 */
					firma64 = null;
				});
			} else {
				/** 
				 * si enviar_id no esta definido, se solicito enviar_todas 
				 */
				/** 
				 * Enviamos todas las fotos disponibles 
				 */
				var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
				if (Ti.App.deployType != 'production') console.log('enviamos las fotos de todas las inspecciones con fotos', {});
				/** 
				 * falta filtrar por campo id_inspector (que sea igual al inspector logeado) 
				 */
				var consultarModelo10_i = Alloy.createCollection('historial_tareas');
				var consultarModelo10_i_where = 'ORDER BY FECHA_TERMINO DESC';
				consultarModelo10_i.fetch({
					query: 'SELECT * FROM historial_tareas ORDER BY FECHA_TERMINO DESC'
				});
				var tareas = require('helper').query2array(consultarModelo10_i);
				var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
				var lista = {};
				var tarea_index = 0;
				_.each(tareas, function(tarea, tarea_pos, tarea_list) {
					tarea_index += 1;
					if (require('helper').arr_contains('8,9'.toLowerCase().split(','), tarea.estado_tarea.toString().toLowerCase())) {
						/** 
						 * Revisamos que cada tarea tenga un estado_tarea en 8 o 9 
						 */
						var fotos = [];
						var ID_792171103_f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + tarea.id_server + '/');
						if (ID_792171103_f.exists() == true) {
							fotos = ID_792171103_f.getDirectoryListing();
						}
						if (fotos && fotos.length) {
							/** 
							 * Revisamos que el directorio tenga archivos 
							 */
							/** 
							 * Actualizamos la variable enviar_id con el id de la tarea que estamos enviando 
							 */
							var enviar_id = ('enviar_id' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['enviar_id'] : '';
							if ((_.isObject(enviar_id) || _.isString(enviar_id)) && _.isEmpty(enviar_id)) {
								require('vars')[_var_scopekey]['enviar_id'] = tarea.id_server;
							} else {
								lista[tarea.id_server] = true;
							}
						}
					}
				});
				if (Ti.App.deployType != 'production') console.log('detalle de la lista', {
					"datos": lista
				});
				/** 
				 * Actualizamos variable pendientes con la lista de tareas 
				 */
				require('vars')[_var_scopekey]['pendientes'] = lista;
				if ((Object.keys(lista).length) == 0 || (Object.keys(lista).length) == '0') {
					/** 
					 * Revisamos que la variable de lista este vacia 
					 */
					/** 
					 * Detenemos popup enviando 
					 */
					require('vars')['enviando_inspecciones'] = L('x734881840_traducir', 'false');
					/** 
					 * Limpiamos variable de enviar_id 
					 */
					require('vars')[_var_scopekey]['enviar_id'] = '';
					/** 
					 * Llamamos la funcion de refrescar_tareas 
					 */
					var ID_1150953943 = null;
					if ('refrescar_tareas' in require('funciones')) {
						ID_1150953943 = require('funciones').refrescar_tareas({});
					} else {
						try {
							ID_1150953943 = f_refrescar_tareas({});
						} catch (ee) {}
					}
					/** 
					 * Ocultamos popup 
					 */
					var vista17_visible = false;

					if (vista17_visible == 'si') {
						vista17_visible = true;
					} else if (vista17_visible == 'no') {
						vista17_visible = false;
					}
					$.vista17.setVisible(vista17_visible);

					/** 
					 * Mostramos enviar todos 
					 */
					var vista2_visible = true;

					if (vista2_visible == 'si') {
						vista2_visible = true;
					} else if (vista2_visible == 'no') {
						vista2_visible = false;
					}
					$.vista2.setVisible(vista2_visible);

					/** 
					 * Mostramos cerrar historial 
					 */
					var vista_visible = true;

					if (vista_visible == 'si') {
						vista_visible = true;
					} else if (vista_visible == 'no') {
						vista_visible = false;
					}
					$.vista.setVisible(vista_visible);

					/** 
					 * Revisamos si no queda ninguna imagen por enviar (desactivamos enviar_todos) 
					 */
					var faltan = false;
					/** 
					 * 27-feb esto se podria mejorar, agregando soporte para filtrar por estado_tarea = 8,9 y contar los registros. 
					 */
					var consultarModelo11_i = Alloy.createCollection('historial_tareas');
					var consultarModelo11_i_where = '';
					consultarModelo11_i.fetch();
					var tareastest = require('helper').query2array(consultarModelo11_i);
					var test_index = 0;
					_.each(tareastest, function(test, test_pos, test_list) {
						test_index += 1;
						if (require('helper').arr_contains('8,9'.toLowerCase().split(','), test.estado_tarea.toString().toLowerCase())) {
							/** 
							 * Revisamos que el equipo tenga imagenes guardadas para la tarea actual 
							 */

							var fotos = [];
							var ID_352149052_f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + test.id_server + '/');
							if (ID_352149052_f.exists() == true) {
								fotos = ID_352149052_f.getDirectoryListing();
							}
							if (fotos && fotos.length == 0) {
								/** 
								 * Si no existen fotos en el equipo, actualizamos la variable de que no hay fotos en el equipo 
								 */
								faltan = true;
							}
						}
					});
					if (faltan == false || faltan == 'false') {
						/** 
						 * Ocultamos boton enviar todos 
						 */
						if (Ti.App.deployType != 'production') console.log('esconder en la consulta web', {});
						/** 
						 * Ocultamos enviar todos 
						 */
						var vista2_visible = false;

						if (vista2_visible == 'si') {
							vista2_visible = true;
						} else if (vista2_visible == 'no') {
							vista2_visible = false;
						}
						$.vista2.setVisible(vista2_visible);

					}
				}
			}
		}
	}
	if (ID_875505225_continuar == true) {
		_out_vars['ID_875505225']._run = setTimeout(ID_875505225_func, 1000 * 5);
	}
};
_out_vars['ID_875505225']._run = setTimeout(ID_875505225_func, 1000 * 5);


(function() {
	require('vars')['enviando_inspecciones'] = L('x734881840_traducir', 'false');
	require('vars')['pendientes'] = {};
	/** 
	 * Recuperamos variable inspector 
	 */
	var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
	if ((_.isObject(inspector) || _.isString(inspector)) && _.isEmpty(inspector)) {
		/** 
		 * Revisamos que la variable inspector tenga datos, si no hacemos una consulta a la tabla y actualizamos la variable con los datos 
		 */
		var consultarModelo12_i = Alloy.createCollection('inspectores');
		var consultarModelo12_i_where = '';
		consultarModelo12_i.fetch();
		var inspector_list = require('helper').query2array(consultarModelo12_i);
		require('vars')['inspector'] = inspector_list[0];
		var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
	}
	require('vars')[_var_scopekey]['faltan'] = L('x734881840_traducir', 'false');
	/** 
	 * 27-feb esto se podria mejorar, agregando soporte para filtrar por estado_tarea = 8,9 y contar los registros. 
	 */
	var consultarModelo13_i = Alloy.createCollection('historial_tareas');
	var consultarModelo13_i_where = '';
	consultarModelo13_i.fetch();
	var tareastest = require('helper').query2array(consultarModelo13_i);
	var test_index = 0;
	_.each(tareastest, function(test, test_pos, test_list) {
		test_index += 1;
		if (require('helper').arr_contains('8,9'.toLowerCase().split(','), test.estado_tarea.toString().toLowerCase())) {
			/** 
			 * Revisamos que el equipo tenga imagenes guardadas para la tarea actual 
			 */

			var fotos = [];
			var ID_287152173_f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + test.id_server + '/');
			if (ID_287152173_f.exists() == true) {
				fotos = ID_287152173_f.getDirectoryListing();
			}
			if (fotos && fotos.length == 0) {
				/** 
				 * Si no existen fotos en el equipo, actualizamos la variable de que no hay fotos en el equipo 
				 */
				require('vars')[_var_scopekey]['faltan'] = L('x4261170317', 'true');
			}
		}
	});
	var ID_897259323_func = function() {
		var faltan = ('faltan' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['faltan'] : '';
		if (faltan == true || faltan == 'true') {
			/** 
			 * Revisamos si no queda ninguna imagen por enviar (desactivamos enviar_todos) 
			 */
			if (Ti.App.deployType != 'production') console.log('faltan es true', {});
			/** 
			 * Mostramos enviar todos 
			 */
			var vista2_visible = true;

			if (vista2_visible == 'si') {
				vista2_visible = true;
			} else if (vista2_visible == 'no') {
				vista2_visible = false;
			}
			$.vista2.setVisible(vista2_visible);

			/** 
			 * Mostramos cerrar historial 
			 */
			var vista_visible = true;

			if (vista_visible == 'si') {
				vista_visible = true;
			} else if (vista_visible == 'no') {
				vista_visible = false;
			}
			$.vista.setVisible(vista_visible);

		} else {
			if (Ti.App.deployType != 'production') console.log('faltan es falso', {});
			/** 
			 * Ocultamos enviar todos 
			 */
			var vista2_visible = false;

			if (vista2_visible == 'si') {
				vista2_visible = true;
			} else if (vista2_visible == 'no') {
				vista2_visible = false;
			}
			$.vista2.setVisible(vista2_visible);

		}
		var ID_1815814068 = null;
		if ('refrescar_tareas' in require('funciones')) {
			ID_1815814068 = require('funciones').refrescar_tareas({});
		} else {
			try {
				ID_1815814068 = f_refrescar_tareas({});
			} catch (ee) {}
		}
	};
	var ID_897259323 = setTimeout(ID_897259323_func, 1000 * 0.2);
})();

if (OS_IOS || OS_ANDROID) {
	$.HISTORIAL.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}
//$.HISTORIAL.open();
