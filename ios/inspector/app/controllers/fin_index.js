var _bind4section = {};
var _list_templates = {};

$.FINALIZADO_window.setTitleAttributes({
	color: 'WHITE'
});
var _activity;
if (OS_ANDROID) {
	_activity = $.FINALIZADO.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'FINALIZADO';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.FINALIZADO_window.addEventListener('open', function(e) {
		abx.setStatusbarColor("#FFFFFF");
		abx.setBackgroundColor("#2d9edb");
	});
}


$.widgetMono.init({
	titulo: L('x542998687_traducir', '¡FELICIDADES EN TERMINAR CON EXITO TU INSPECCION!'),
	__id: 'ALL395419113',
	texto: L('x1072203252_traducir', 'Tu inspección será recibida por nosotros, gracias por tu trabajo.'),
	top: 40,
	tipo: '_info'
});


$.widgetBotonlargo.init({
	titulo: L('x1197985134_traducir', 'SALIR'),
	__id: 'ALL1012442242',
	color: 'verde',
	onclick: Click_widgetBotonlargo
});

function Click_widgetBotonlargo(e) {

	var evento = e;
	/** 
	 * Avisamos que ya no estamos en inspeccion 
	 */
	require('vars')['inspeccion_encurso'] = L('x734881840_traducir', 'false');
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	var consultarModelo_i = Alloy.createCollection('tareas');
	var consultarModelo_i_where = 'id_server=\'' + seltarea.id_server + '\'';
	consultarModelo_i.fetch({
		query: 'SELECT * FROM tareas WHERE id_server=\'' + seltarea.id_server + '\''
	});
	var tarea_actual = require('helper').query2array(consultarModelo_i);
	if (tarea_actual && tarea_actual.length) {
		if (Ti.App.deployType != 'production') console.log('copiando tarea a tabla historial_tareas', {});
		var insertarModelo_m = Alloy.Collections.historial_tareas;
		var db_insertarModelo = Ti.Database.open(insertarModelo_m.config.adapter.db_name);
		db_insertarModelo.execute('BEGIN');
		_.each(tarea_actual, function(insertarModelo_fila, pos) {
			db_insertarModelo.execute('INSERT INTO historial_tareas (distance, nivel_4, id_inspeccion, nivel_2, id_asegurado, perfil, estado_tarea, id_inspector, estado_envio, id_server, lat, nivel_3, categoria, lon, nivel_1, pais, direccion, nivel_5) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', 0, insertarModelo_fila.nivel_4, insertarModelo_fila.id_inspeccion, insertarModelo_fila.nivel_2, insertarModelo_fila.id_asegurado, insertarModelo_fila.perfil, 9, insertarModelo_fila.id_inspector, 0, insertarModelo_fila.id_server, insertarModelo_fila.lat, insertarModelo_fila.nivel_3, insertarModelo_fila.categoria, insertarModelo_fila.lon, insertarModelo_fila.nivel_1, insertarModelo_fila.pais, insertarModelo_fila.direccion, insertarModelo_fila.nivel_5);
		});
		db_insertarModelo.execute('COMMIT');
		db_insertarModelo.close();
		db_insertarModelo = null;
		insertarModelo_m.trigger('change');
		var eliminarModelo_i = Alloy.Collections.tareas;
		var sql = 'DELETE FROM ' + eliminarModelo_i.config.adapter.collection_name + ' WHERE id_server=\'' + seltarea.id_server + '\'';
		var db = Ti.Database.open(eliminarModelo_i.config.adapter.db_name);
		db.execute(sql);
		db.close();
		eliminarModelo_i.trigger('remove');
	}
	/** 
	 * Llamo refresco tareas 
	 */

	Alloy.Events.trigger('_refrescar_tareas');
	/** 
	 * Cerramos pantalla fin 
	 */
	$.FINALIZADO.close();

}

function Postlayout_FINALIZADO(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	/** 
	 * Limpieza memoria 
	 */

	Alloy.Events.trigger('_cerrar_insp', {
		pantalla: 'firma'
	});

}

(function() {
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
		/** 
		 * Enviamos datos de inspeccion a servidor 
		 */
		/** 
		 * Enviamos datos 
		 */

		$.widgetBotonlargo.iniciar_progreso({});
		$.StatusEsperando.setText('status: consultando datos ..');

		/** 
		 * Consultamos todas las tablas de la inspeccion 
		 */
		var consultarModelo2_i = Alloy.createCollection('insp_fotosrequeridas');
		var consultarModelo2_i_where = '';
		consultarModelo2_i.fetch();
		var fotos = require('helper').query2array(consultarModelo2_i);
		var consultarModelo3_i = Alloy.createCollection('insp_datosbasicos');
		var consultarModelo3_i_where = '';
		consultarModelo3_i.fetch();
		var basicos = require('helper').query2array(consultarModelo3_i);
		var consultarModelo4_i = Alloy.createCollection('insp_caracteristicas');
		var consultarModelo4_i_where = '';
		consultarModelo4_i.fetch();
		var cara = require('helper').query2array(consultarModelo4_i);
		var consultarModelo5_i = Alloy.createCollection('insp_niveles');
		var consultarModelo5_i_where = '';
		consultarModelo5_i.fetch();
		var niveles = require('helper').query2array(consultarModelo5_i);
		var consultarModelo6_i = Alloy.createCollection('insp_siniestro');
		var consultarModelo6_i_where = '';
		consultarModelo6_i.fetch();
		var siniestro = require('helper').query2array(consultarModelo6_i);
		var consultarModelo7_i = Alloy.createCollection('insp_recintos');
		var consultarModelo7_i_where = '';
		consultarModelo7_i.fetch();
		var recintos = require('helper').query2array(consultarModelo7_i);
		var consultarModelo8_i = Alloy.createCollection('insp_itemdanos');
		var consultarModelo8_i_where = '';
		consultarModelo8_i.fetch();
		var danos = require('helper').query2array(consultarModelo8_i);
		var consultarModelo9_i = Alloy.createCollection('insp_contenido');
		var consultarModelo9_i_where = '';
		consultarModelo9_i.fetch();
		var contenidos = require('helper').query2array(consultarModelo9_i);
		var consultarModelo10_i = Alloy.createCollection('insp_documentos');
		var consultarModelo10_i_where = '';
		consultarModelo10_i.fetch();
		var documentos = require('helper').query2array(consultarModelo10_i);
		var consultarModelo11_i = Alloy.createCollection('insp_firma');
		var consultarModelo11_i_where = '';
		consultarModelo11_i.fetch();
		var firma = require('helper').query2array(consultarModelo11_i);
		/** 
		 * Actualizamos fecha fin de inspeccion actual 
		 */
		var consultarModelo12_i = Alloy.createCollection('inspecciones');
		var consultarModelo12_i_where = 'id_server=\'' + seltarea.id_server + '\'';
		consultarModelo12_i.fetch({
			query: 'SELECT * FROM inspecciones WHERE id_server=\'' + seltarea.id_server + '\''
		});
		var nula = require('helper').query2array(consultarModelo12_i);
		var db = Ti.Database.open(consultarModelo12_i.config.adapter.db_name);
		if (consultarModelo12_i_where == '') {
			var sql = 'UPDATE ' + consultarModelo12_i.config.adapter.collection_name + ' SET fecha_inspeccion_fin=\'' + new Date() + '\'';
		} else {
			var sql = 'UPDATE ' + consultarModelo12_i.config.adapter.collection_name + ' SET fecha_inspeccion_fin=\'' + new Date() + '\' WHERE ' + consultarModelo12_i_where;
		}
		db.execute(sql);
		db.close();
		var consultarModelo13_i = Alloy.createCollection('inspecciones');
		var consultarModelo13_i_where = '';
		consultarModelo13_i.fetch();
		var inspecciones = require('helper').query2array(consultarModelo13_i);
		$.StatusEsperando.setText('status: enviando ..');

		var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
		if ((_.isObject(url_server) || (_.isString(url_server)) && !_.isEmpty(url_server)) || _.isNumber(url_server) || _.isBoolean(url_server)) {
			var consultarURL = {};
			console.log('DEBUG WEB: requesting url:' + String.format(L('x3471857721', '%1$sfinalizarTarea'), url_server.toString()) + ' with JSON data:', {
				_method: 'POSTJSON',
				_params: {
					fotosrequeridas: fotos,
					datosbasicos: basicos,
					caracteristicas: cara,
					niveles: niveles,
					siniestro: siniestro,
					recintos: recintos,
					itemdanos: danos,
					contenido: contenidos,
					documentos: documentos,
					inspecciones: inspecciones,
					firma: firma
				},
				_timeout: '15000'
			});

			consultarURL.success = function(e) {
				var elemento = e,
					valor = e;
				$.StatusEsperando.setText('status: limpiando temporales');

				/** 
				 * Eliminando datos enviados 
				 */
				/** 
				 * Eliminando datos enviados 
				 */

				var eliminarModelo2_i = Alloy.Collections.insp_fotosrequeridas;
				var sql = "DELETE FROM " + eliminarModelo2_i.config.adapter.collection_name;
				var db = Ti.Database.open(eliminarModelo2_i.config.adapter.db_name);
				db.execute(sql);
				db.close();
				eliminarModelo2_i.trigger('remove');
				var eliminarModelo3_i = Alloy.Collections.insp_datosbasicos;
				var sql = "DELETE FROM " + eliminarModelo3_i.config.adapter.collection_name;
				var db = Ti.Database.open(eliminarModelo3_i.config.adapter.db_name);
				db.execute(sql);
				db.close();
				eliminarModelo3_i.trigger('remove');
				var eliminarModelo4_i = Alloy.Collections.insp_caracteristicas;
				var sql = "DELETE FROM " + eliminarModelo4_i.config.adapter.collection_name;
				var db = Ti.Database.open(eliminarModelo4_i.config.adapter.db_name);
				db.execute(sql);
				db.close();
				eliminarModelo4_i.trigger('remove');
				var eliminarModelo5_i = Alloy.Collections.insp_niveles;
				var sql = "DELETE FROM " + eliminarModelo5_i.config.adapter.collection_name;
				var db = Ti.Database.open(eliminarModelo5_i.config.adapter.db_name);
				db.execute(sql);
				db.close();
				eliminarModelo5_i.trigger('remove');
				var eliminarModelo6_i = Alloy.Collections.insp_siniestro;
				var sql = "DELETE FROM " + eliminarModelo6_i.config.adapter.collection_name;
				var db = Ti.Database.open(eliminarModelo6_i.config.adapter.db_name);
				db.execute(sql);
				db.close();
				eliminarModelo6_i.trigger('remove');
				var eliminarModelo7_i = Alloy.Collections.insp_recintos;
				var sql = "DELETE FROM " + eliminarModelo7_i.config.adapter.collection_name;
				var db = Ti.Database.open(eliminarModelo7_i.config.adapter.db_name);
				db.execute(sql);
				db.close();
				eliminarModelo7_i.trigger('remove');
				var eliminarModelo8_i = Alloy.Collections.insp_itemdanos;
				var sql = "DELETE FROM " + eliminarModelo8_i.config.adapter.collection_name;
				var db = Ti.Database.open(eliminarModelo8_i.config.adapter.db_name);
				db.execute(sql);
				db.close();
				eliminarModelo8_i.trigger('remove');
				var eliminarModelo9_i = Alloy.Collections.insp_contenido;
				var sql = "DELETE FROM " + eliminarModelo9_i.config.adapter.collection_name;
				var db = Ti.Database.open(eliminarModelo9_i.config.adapter.db_name);
				db.execute(sql);
				db.close();
				eliminarModelo9_i.trigger('remove');
				var eliminarModelo10_i = Alloy.Collections.insp_documentos;
				var sql = "DELETE FROM " + eliminarModelo10_i.config.adapter.collection_name;
				var db = Ti.Database.open(eliminarModelo10_i.config.adapter.db_name);
				db.execute(sql);
				db.close();
				eliminarModelo10_i.trigger('remove');
				var eliminarModelo11_i = Alloy.Collections.inspecciones;
				var sql = "DELETE FROM " + eliminarModelo11_i.config.adapter.collection_name;
				var db = Ti.Database.open(eliminarModelo11_i.config.adapter.db_name);
				db.execute(sql);
				db.close();
				eliminarModelo11_i.trigger('remove');
				/** 
				 * Llamamos funcion enviarFirmas 
				 */
				var ID_266553545 = null;
				if ('enviarfirmas' in require('funciones')) {
					ID_266553545 = require('funciones').enviarfirmas({});
				} else {
					try {
						ID_266553545 = f_enviarfirmas({});
					} catch (ee) {}
				}
				/** 
				 * Limpieza de memoria 
				 */
				insp_fotosrequeridas = null, insp_datosbasicos = null, insp_caracteristicas = null, insp_niveles = null, insp_siniestro = null, insp_recintos = null, insp_itemdanos = null, insp_contenido = null, insp_documentos = null, insp_firma = null, inspecciones = null;
				elemento = null, valor = null;
			};

			consultarURL.error = function(e) {
				var elemento = e,
					valor = e;
				if (Ti.App.deployType != 'production') console.log('hubo error enviando inspeccion', {
					"elemento": elemento
				});
				$.StatusEsperando.setText('status: error sincronizando');


				$.widgetBotonlargo.detener_progreso({});
				/** 
				 * Limpieza de memoria 
				 */
				insp_fotosrequeridas = null, insp_datosbasicos = null, insp_caracteristicas = null, insp_niveles = null, insp_siniestro = null, insp_recintos = null, insp_itemdanos = null, insp_contenido = null, insp_documentos = null, insp_firma = null, inspecciones = null;
				elemento = null, valor = null;
			};
			require('helper').ajaxUnico('consultarURL', '' + String.format(L('x3471857721', '%1$sfinalizarTarea'), url_server.toString()) + '', 'POSTJSON', {
				fotosrequeridas: fotos,
				datosbasicos: basicos,
				caracteristicas: cara,
				niveles: niveles,
				siniestro: siniestro,
				recintos: recintos,
				itemdanos: danos,
				contenido: contenidos,
				documentos: documentos,
				inspecciones: inspecciones,
				firma: firma
			}, 15000, consultarURL);
		} else {
			$.StatusEsperando.setText('status: simulador ..');


			$.widgetBotonlargo.detener_progreso({});
		}
	} else {
		$.StatusEsperando.setText('status: simulador ..');

	}
})();


var f_enviarfirmas = function(x_params) {
	var xyz = x_params['xyz'];
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	var consultarModelo14_i = Alloy.createCollection('insp_firma');
	var consultarModelo14_i_where = '';
	consultarModelo14_i.fetch();
	var firmas = require('helper').query2array(consultarModelo14_i);
	var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
	if ((_.isObject(url_server) || (_.isString(url_server)) && !_.isEmpty(url_server)) || _.isNumber(url_server) || _.isBoolean(url_server)) {
		/** 
		 * enviamos la imagen de las firmas 
		 */
		if (firmas && firmas.length) {
			/** 
			 * Revisamos si el modelo de las firmas contiene firmas pendientes por enviar 
			 */
			var firma_index = 0;
			_.each(firmas, function(firma, firma_pos, firma_list) {
				firma_index += 1;
				/** 
				 * Editamos el texto en pantalla para indiciar que firma estamos enviando 
				 */
				$.StatusEsperando.setText(String.format(L('x288279950_traducir', 'status: enviando firma %1$s'), firma.id.toString()));

				if (Ti.App.deployType != 'production') console.log(String.format(L('x1276337118_traducir', 'leyendo archivo: {data}/inspeccion%1$s/%2$s'), (firma.id_inspeccion) ? firma.id_inspeccion.toString() : '', (firma.firma64) ? firma.firma64.toString() : ''), {});
				/** 
				 * Leemos archivo de firma 
				 */

				var firmabin = '';
				var ID_1674385895_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + firma.id_inspeccion);
				if (ID_1674385895_d.exists() == true) {
					var ID_1674385895_f = Ti.Filesystem.getFile(ID_1674385895_d.resolve(), firma.firma64);
					if (ID_1674385895_f.exists() == true) {
						firmabin = ID_1674385895_f.read();
					}
					ID_1674385895_f = null;
				}
				ID_1674385895_d = null;
				if (Ti.App.deployType != 'production') console.log('enviando a servidor', {});
				var consultarURL2 = {};
				console.log('DEBUG WEB: requesting url:' + String.format(L('x757910167', '%1$ssubirImagenes'), url_server.toString()) + ' with data:', {
					_method: 'POST',
					_params: {
						id_tarea: firma.id_inspeccion,
						archivo: firma.firma64,
						imagen: firmabin,
						app_id: firma.firma64
					},
					_timeout: '20000'
				});

				consultarURL2.success = function(e) {
					var elemento = e,
						valor = e;
					if (Ti.App.deployType != 'production') console.log('envio firma exitoso', {
						"elemento": elemento
					});
					if (elemento.error != 0 && elemento.error != '0') {

						$.widgetBotonlargo.detener_progreso({});
						$.StatusEsperando.setText('status: fallo envio firma');

						var eliminarModelo12_i = Alloy.Collections.insp_firma;
						var sql = "DELETE FROM " + eliminarModelo12_i.config.adapter.collection_name;
						var db = Ti.Database.open(eliminarModelo12_i.config.adapter.db_name);
						db.execute(sql);
						db.close();
						eliminarModelo12_i.trigger('remove');
					}
					if (!_.isUndefined(elemento.app_id)) {
						$.StatusEsperando.setText(String.format(L('x3916020026_traducir', 'status: %1$s exitosa'), elemento.app_id.toString()));

						var consultarModelo15_i = Alloy.createCollection('insp_firma');
						var consultarModelo15_i_where = 'firma64=\'' + elemento.app_id + '\'';
						consultarModelo15_i.fetch({
							query: 'SELECT * FROM insp_firma WHERE firma64=\'' + elemento.app_id + '\''
						});
						var datotarea = require('helper').query2array(consultarModelo15_i);
						if (datotarea && datotarea.length) {
							var ID_1840379282_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + datotarea[0].id_inspeccion);
							var ID_1840379282_f = Ti.Filesystem.getFile(ID_1840379282_d.resolve(), elemento.app_id);
							if (ID_1840379282_f.exists() == true) ID_1840379282_f.deleteFile();
							var eliminarModelo13_i = Alloy.Collections.insp_firma;
							var sql = 'DELETE FROM ' + eliminarModelo13_i.config.adapter.collection_name + ' WHERE firma64=\'' + elemento.app_id + '\'';
							var db = Ti.Database.open(eliminarModelo13_i.config.adapter.db_name);
							db.execute(sql);
							db.close();
							eliminarModelo13_i.trigger('remove');
							if (Ti.App.deployType != 'production') console.log(String.format(L('x480248166_traducir', 'firma %1$s borrada'), elemento.app_id.toString()), {});
						}
						datotarea = null;
					} else {
						/** 
						 * Borramos imagen local porque debe ser historia previa 
						 */
					}
					/** 
					 * Preguntamos si quedan firmas por enviar 
					 */
					var ID_1905102524_func = function() {
						var consultarModelo16_i = Alloy.createCollection('insp_firma');
						var consultarModelo16_i_where = '';
						consultarModelo16_i.fetch();
						var firmasb = require('helper').query2array(consultarModelo16_i);
						if (firmasb && firmasb.length == 0) {

							$.widgetBotonlargo.detener_progreso({});
							$.StatusEsperando.setText('status: listos');

						}
						/** 
						 * Limpieza de memoria 
						 */
						firmasb = null, firma64 = null, firmabin = null;
					};
					var ID_1905102524 = setTimeout(ID_1905102524_func, 1000 * 0.2);
					elemento = null, valor = null;
				};

				consultarURL2.error = function(e) {
					var elemento = e,
						valor = e;
					if (Ti.App.deployType != 'production') console.log('envio firma fallido', {
						"elemento": elemento
					});
					$.StatusEsperando.setText('status: envio firma fallido');


					$.widgetBotonlargo.detener_progreso({});
					elemento = null, valor = null;
				};
				require('helper').ajaxUnico('consultarURL2', '' + String.format(L('x757910167', '%1$ssubirImagenes'), url_server.toString()) + '', 'POST', {
					id_tarea: firma.id_inspeccion,
					archivo: firma.firma64,
					imagen: firmabin,
					app_id: firma.firma64
				}, 20000, consultarURL2);
			});
		}
	}
	return null;
};

if (OS_IOS || OS_ANDROID) {
	$.FINALIZADO.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}
//$.FINALIZADO.open();
