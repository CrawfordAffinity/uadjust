var _bind4section = {};
var _list_templates = {};

var _activity;
if (OS_ANDROID) {
	_activity = $.EDITAR2.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'EDITAR2';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.EDITAR2_window.addEventListener('open', function(e) {
		abx.setStatusbarColor("#000000");
		abx.setBackgroundColor("white");
	});
}

function Click_vista10(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.EDITAR2.close();

	Alloy.Events.trigger('_close_editar');

}

function Click_vista11(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var desde = ('desde' in require('vars')) ? require('vars')['desde'] : '';
	var hasta = ('hasta' in require('vars')) ? require('vars')['hasta'] : '';
	var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
	var dias = ('dias' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['dias'] : '';

	//validaciones
	var al_menos_uno = registro.d1 || registro.d2 || registro.d3 || registro.d4 || registro.d5 || registro.d6 || registro.d7;
	desde = parseInt(desde);
	hasta = parseInt(hasta)
	if (al_menos_uno == false || al_menos_uno == 'false') {
		var preguntarAlerta2_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta2 = Ti.UI.createAlertDialog({
			title: L('x57652245_traducir', 'Alerta'),
			message: L('x2430147855_traducir', 'Seleccione al menos un día'),
			buttonNames: preguntarAlerta2_opts
		});
		preguntarAlerta2.addEventListener('click', function(e) {
			var suu = preguntarAlerta2_opts[e.index];
			suu = null;

			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta2.show();
	} else if (_.isNumber(desde) && _.isNumber(hasta) && desde > hasta) {
		var preguntarAlerta3_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta3 = Ti.UI.createAlertDialog({
			title: L('x57652245_traducir', 'Alerta'),
			message: L('x4068516320_traducir', 'El rango de horas está mal ingresado'),
			buttonNames: preguntarAlerta3_opts
		});
		preguntarAlerta3.addEventListener('click', function(e) {
			var suu = preguntarAlerta3_opts[e.index];
			suu = null;

			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta3.show();
	} else if (desde == hasta) {
		var preguntarAlerta4_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta4 = Ti.UI.createAlertDialog({
			title: L('x57652245_traducir', 'Alerta'),
			message: L('x178692076_traducir', 'El rango debe ser superior a una hora'),
			buttonNames: preguntarAlerta4_opts
		});
		preguntarAlerta4.addEventListener('click', function(e) {
			var suu = preguntarAlerta4_opts[e.index];
			suu = null;

			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta4.show();
	} else {
		var registro = _.extend(registro, {
			disp_horas: String.format(L('x1576037006_traducir', '%1$s:00 %2$s:00'), (desde) ? desde.toString() : '', (hasta) ? hasta.toString() : '')
		});
		require('vars')['registro'] = registro;
		if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
			var consultarModelo4_i = Alloy.createCollection('inspectores');
			var consultarModelo4_i_where = '';
			consultarModelo4_i.fetch();
			var inspector_list = require('helper').query2array(consultarModelo4_i);
			inspector = inspector_list[0];
			var fueraciudad = ('fueraciudad' in require('vars')) ? require('vars')['fueraciudad'] : '';
			var fuerapais = ('fuerapais' in require('vars')) ? require('vars')['fuerapais'] : '';
			var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
			var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
			var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
			var consultarURL2 = {};

			consultarURL2.success = function(e) {
				var elemento = e,
					valor = e;
				if (Ti.App.deployType != 'production') console.log('Mi resultado es', {
					"elemento": elemento
				});
				if (elemento.error == 0 || elemento.error == '0') {
					var consultarModelo5_i = Alloy.createCollection('inspectores');
					var consultarModelo5_i_where = '';
					consultarModelo5_i.fetch();
					var inspector_list = require('helper').query2array(consultarModelo5_i);
					var fueraciudad = ('fueraciudad' in require('vars')) ? require('vars')['fueraciudad'] : '';
					var fuerapais = ('fuerapais' in require('vars')) ? require('vars')['fuerapais'] : '';
					var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
					if (Ti.App.deployType != 'production') console.log('modificando disponibilidad en BD local', {
						"registro": registro
					});
					var db = Ti.Database.open(consultarModelo5_i.config.adapter.db_name);
					if (consultarModelo5_i_where == '') {
						var sql = 'UPDATE ' + consultarModelo5_i.config.adapter.collection_name + ' SET disponibilidad_viajar_pais=\'' + fuerapais + '\'';
					} else {
						var sql = 'UPDATE ' + consultarModelo5_i.config.adapter.collection_name + ' SET disponibilidad_viajar_pais=\'' + fuerapais + '\' WHERE ' + consultarModelo5_i_where;
					}
					db.execute(sql);
					if (consultarModelo5_i_where == '') {
						var sql = 'UPDATE ' + consultarModelo5_i.config.adapter.collection_name + ' SET disponibilidad_viajar_ciudad=\'' + fueraciudad + '\'';
					} else {
						var sql = 'UPDATE ' + consultarModelo5_i.config.adapter.collection_name + ' SET disponibilidad_viajar_ciudad=\'' + fueraciudad + '\' WHERE ' + consultarModelo5_i_where;
					}
					db.execute(sql);
					if (consultarModelo5_i_where == '') {
						var sql = 'UPDATE ' + consultarModelo5_i.config.adapter.collection_name + ' SET disponibilidad=\'' + 0 + '\'';
					} else {
						var sql = 'UPDATE ' + consultarModelo5_i.config.adapter.collection_name + ' SET disponibilidad=\'' + 0 + '\' WHERE ' + consultarModelo5_i_where;
					}
					db.execute(sql);
					if (consultarModelo5_i_where == '') {
						var sql = 'UPDATE ' + consultarModelo5_i.config.adapter.collection_name + ' SET disponibilidad_horas=\'' + registro.disp_horas + '\'';
					} else {
						var sql = 'UPDATE ' + consultarModelo5_i.config.adapter.collection_name + ' SET disponibilidad_horas=\'' + registro.disp_horas + '\' WHERE ' + consultarModelo5_i_where;
					}
					db.execute(sql);
					if (consultarModelo5_i_where == '') {
						var sql = 'UPDATE ' + consultarModelo5_i.config.adapter.collection_name + ' SET d1=\'' + registro.d1 + '\'';
					} else {
						var sql = 'UPDATE ' + consultarModelo5_i.config.adapter.collection_name + ' SET d1=\'' + registro.d1 + '\' WHERE ' + consultarModelo5_i_where;
					}
					db.execute(sql);
					if (consultarModelo5_i_where == '') {
						var sql = 'UPDATE ' + consultarModelo5_i.config.adapter.collection_name + ' SET d2=\'' + registro.d2 + '\'';
					} else {
						var sql = 'UPDATE ' + consultarModelo5_i.config.adapter.collection_name + ' SET d2=\'' + registro.d2 + '\' WHERE ' + consultarModelo5_i_where;
					}
					db.execute(sql);
					if (consultarModelo5_i_where == '') {
						var sql = 'UPDATE ' + consultarModelo5_i.config.adapter.collection_name + ' SET d3=\'' + registro.d3 + '\'';
					} else {
						var sql = 'UPDATE ' + consultarModelo5_i.config.adapter.collection_name + ' SET d3=\'' + registro.d3 + '\' WHERE ' + consultarModelo5_i_where;
					}
					db.execute(sql);
					if (consultarModelo5_i_where == '') {
						var sql = 'UPDATE ' + consultarModelo5_i.config.adapter.collection_name + ' SET d4=\'' + registro.d4 + '\'';
					} else {
						var sql = 'UPDATE ' + consultarModelo5_i.config.adapter.collection_name + ' SET d4=\'' + registro.d4 + '\' WHERE ' + consultarModelo5_i_where;
					}
					db.execute(sql);
					if (consultarModelo5_i_where == '') {
						var sql = 'UPDATE ' + consultarModelo5_i.config.adapter.collection_name + ' SET d5=\'' + registro.d5 + '\'';
					} else {
						var sql = 'UPDATE ' + consultarModelo5_i.config.adapter.collection_name + ' SET d5=\'' + registro.d5 + '\' WHERE ' + consultarModelo5_i_where;
					}
					db.execute(sql);
					if (consultarModelo5_i_where == '') {
						var sql = 'UPDATE ' + consultarModelo5_i.config.adapter.collection_name + ' SET d6=\'' + registro.d6 + '\'';
					} else {
						var sql = 'UPDATE ' + consultarModelo5_i.config.adapter.collection_name + ' SET d6=\'' + registro.d6 + '\' WHERE ' + consultarModelo5_i_where;
					}
					db.execute(sql);
					if (consultarModelo5_i_where == '') {
						var sql = 'UPDATE ' + consultarModelo5_i.config.adapter.collection_name + ' SET d7=\'' + registro.d7 + '\'';
					} else {
						var sql = 'UPDATE ' + consultarModelo5_i.config.adapter.collection_name + ' SET d7=\'' + registro.d7 + '\' WHERE ' + consultarModelo5_i_where;
					}
					db.execute(sql);
					db.close();
				}
				elemento = null, valor = null;
			};

			consultarURL2.error = function(e) {
				var elemento = e,
					valor = e;
				if (Ti.App.deployType != 'production') console.log('Hubo un error', {
					"elemento": elemento
				});
				elemento = null, valor = null;
			};
			require('helper').ajaxUnico('consultarURL2', '' + String.format(L('x2304081372', '%1$seditarPerfilTipo2'), url_server.toString()) + '', 'POST', {
				id_inspector: inspector.id_server,
				disponibilidad_viajar_pais: fuerapais,
				disponibilidad_viajar_ciudad: fueraciudad,
				disponibilidad_horas: registro.disp_horas,
				disponibilidad: 0,
				d1: registro.d1,
				d2: registro.d2,
				d3: registro.d3,
				d4: registro.d4,
				d5: registro.d5,
				d6: registro.d6,
				d7: registro.d7
			}, 15000, consultarURL2);
			$.EDITAR2.close();

			Alloy.Events.trigger('_close_editar');
		} else {
			var preguntarAlerta5_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta5 = Ti.UI.createAlertDialog({
				title: L('x3071602690_traducir', 'No hay internet'),
				message: L('x2846685350_traducir', 'No se pueden efectuar los cambios sin conexion.'),
				buttonNames: preguntarAlerta5_opts
			});
			preguntarAlerta5.addEventListener('click', function(e) {
				var suu = preguntarAlerta5_opts[e.index];
				suu = null;

				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta5.show();
		}
	}
}

$.widgetHeader2.init({
	titulo: L('x31606107_traducir', 'Editar Disponibilidad de trabajo'),
	__id: 'ALL1614118096',
	avance: L('', '')
});


$.widgetPelota.init({
	__id: 'ALL1132173110',
	letra: L('x2909332022', 'L'),
	onon: on_widgetPelota,
	onoff: Off_widgetPelota
});

function on_widgetPelota(e) {

	var evento = e;
	var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
	var registro = _.extend(registro, {
		d1: 1
	});

}

function Off_widgetPelota(e) {

	var evento = e;
	var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
	var registro = _.extend(registro, {
		d1: 0
	});

}

$.widgetPelota2.init({
	__id: 'ALL1396533677',
	letra: L('x3664761504', 'M'),
	onon: on_widgetPelota2,
	onoff: Off_widgetPelota2
});

function on_widgetPelota2(e) {

	var evento = e;
	var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
	var registro = _.extend(registro, {
		d2: 1
	});

}

function Off_widgetPelota2(e) {

	var evento = e;
	var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
	var registro = _.extend(registro, {
		d2: 0
	});

}

$.widgetPelota3.init({
	__id: 'ALL364580565',
	letra: L('x185522819_traducir', 'MI'),
	onon: on_widgetPelota3,
	onoff: Off_widgetPelota3
});

function on_widgetPelota3(e) {

	var evento = e;
	var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
	var registro = _.extend(registro, {
		d3: 1
	});

}

function Off_widgetPelota3(e) {

	var evento = e;
	var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
	var registro = _.extend(registro, {
		d3: 0
	});

}

$.widgetPelota4.init({
	__id: 'ALL180804538',
	letra: L('x1141589763', 'J'),
	onon: on_widgetPelota4,
	onoff: Off_widgetPelota4
});

function on_widgetPelota4(e) {

	var evento = e;
	var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
	var registro = _.extend(registro, {
		d4: 1
	});

}

function Off_widgetPelota4(e) {

	var evento = e;
	var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
	var registro = _.extend(registro, {
		d4: 0
	});

}

$.widgetPelota5.init({
	__id: 'ALL457319757',
	letra: L('x1342839628', 'V'),
	onon: on_widgetPelota5,
	onoff: Off_widgetPelota5
});

function on_widgetPelota5(e) {

	var evento = e;
	var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
	var registro = _.extend(registro, {
		d5: 1
	});

}

function Off_widgetPelota5(e) {

	var evento = e;
	var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
	var registro = _.extend(registro, {
		d5: 0
	});

}

$.widgetPelota6.init({
	__id: 'ALL993907842',
	letra: L('x543223747', 'S'),
	onon: on_widgetPelota6,
	onoff: Off_widgetPelota6
});

function on_widgetPelota6(e) {

	var evento = e;
	var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
	var registro = _.extend(registro, {
		d6: 1
	});

}

function Off_widgetPelota6(e) {

	var evento = e;
	var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
	var registro = _.extend(registro, {
		d6: 0
	});

}

$.widgetPelota7.init({
	__id: 'ALL947148270',
	letra: L('x2746444292', 'D'),
	onon: on_widgetPelota7,
	onoff: Off_widgetPelota7
});

function on_widgetPelota7(e) {

	var evento = e;
	var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
	var registro = _.extend(registro, {
		d7: 1
	});

}

function Off_widgetPelota7(e) {

	var evento = e;
	var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
	var registro = _.extend(registro, {
		d7: 0
	});

}

$.widgetPickerhoras.init({
	__id: 'ALL1517641115',
	onchange: Change_widgetPickerhoras,
	mins: L('x4261170317', 'true'),
	a: L('x3904355907', 'a')
});

function Change_widgetPickerhoras(e) {

	var evento = e;
	require('vars')['desde'] = evento.desde;
	require('vars')['hasta'] = evento.hasta;

}

(function() {
	var consultarModelo6_i = Alloy.createCollection('inspectores');
	var consultarModelo6_i_where = '';
	consultarModelo6_i.fetch();
	var inspector_list = require('helper').query2array(consultarModelo6_i);
	inspector = inspector_list[0];
	var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
	if (_.isObject(registro) && !_.isArray(registro) && !_.isFunction(registro)) {} else {
		require('vars')['registro'] = inspector;
		var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
	}
	if (inspector_list && inspector_list.length == 0) {
		var registro = _.extend(registro, {
			d1: 0,
			d2: 0,
			d3: 0,
			d4: 0,
			d5: 0,
			d6: 0,
			d7: 0
		});
		require('vars')['desde'] = L('x2212294583', '1');
		require('vars')['hasta'] = L('x2212294583', '1');
	} else {
		var registro = _.extend(registro, {
			d1: inspector.d1,
			d2: inspector.d2,
			d3: inspector.d3,
			d4: inspector.d4,
			d5: inspector.d5,
			d6: inspector.d6,
			d7: inspector.d7
		});
		if (Ti.App.deployType != 'production') console.log('editar-disponibilidad: info inspector dice', {
			"inspector": inspector
		});
		if (!_.isNull(inspector.disponibilidad_horas)) {
			if ((_.isObject(inspector.disponibilidad_horas) || (_.isString(inspector.disponibilidad_horas)) && !_.isEmpty(inspector.disponibilidad_horas)) || _.isNumber(inspector.disponibilidad_horas) || _.isBoolean(inspector.disponibilidad_horas)) {
				if (Ti.App.deployType != 'production') console.log('aqui vamos a evaluar las horas', {});

				horas = inspector.disponibilidad_horas.split(" ");
				desde_horas = horas[0].split(":");
				hasta_horas = horas[1].split(":")
				require('vars')['desde'] = desde_horas[0];
				require('vars')['hasta'] = hasta_horas[0];

				$.widgetPickerhoras.set({
					desde: desde_horas[0]
				});

				$.widgetPickerhoras.set({
					hasta: hasta_horas[0]
				});
			} else {
				require('vars')['desde'] = L('x2212294583', '1');
				require('vars')['hasta'] = L('x2212294583', '1');
			}
		} else {
			require('vars')['desde'] = L('x2212294583', '1');
			require('vars')['hasta'] = L('x2212294583', '1');
		}
	}
	require('vars')['registro'] = registro;
	var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';

	$.widgetPelota.set({
		on: registro.d1
	});

	$.widgetPelota2.set({
		on: registro.d2
	});

	$.widgetPelota3.set({
		on: registro.d3
	});

	$.widgetPelota4.set({
		on: registro.d4
	});

	$.widgetPelota5.set({
		on: registro.d5
	});

	$.widgetPelota6.set({
		on: registro.d6
	});

	$.widgetPelota7.set({
		on: registro.d7
	});
})();

function Postlayout_EDITAR2(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var ID_1482413520_func = function() {
		/** 
		 * Una vez ya cargada la tarea, dejamos la variable limpia para poder abrir otras pantallas 
		 */
		require('vars')['var_abriendo'] = '';
	};
	var ID_1482413520 = setTimeout(ID_1482413520_func, 1000 * 0.2);

}
if (OS_IOS || OS_ANDROID) {
	$.EDITAR2.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}