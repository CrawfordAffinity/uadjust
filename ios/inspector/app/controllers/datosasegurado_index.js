var _bind4section = {};
var _list_templates = {};

var _activity;
if (OS_ANDROID) {
	_activity = $.DATOS_DEL_ASEGURADO.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'DATOS_DEL_ASEGURADO';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.DATOS_DEL_ASEGURADO_window.addEventListener('open', function(e) {
		abx.setStatusbarColor("#000000");
		abx.setBackgroundColor("white");
	});
}

function Click_vista(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.DATOS_DEL_ASEGURADO.close();

}

$.widgetBotonlargo.init({
	titulo: L('x2353066504_traducir', 'LLAMAR ASEGURADO'),
	__id: 'ALL1303818490',
	color: 'verde',
	ancho: '90%',
	icono: 'telefono',
	onclick: Click_widgetBotonlargo
});

function Click_widgetBotonlargo(e) {

	var evento = e;
	var telefono;
	telefono = $.Telefono.getText();

	if (OS_ANDROID) {
		try {
			var intent = Ti.Android.createIntent({
				action: Ti.Android.ACTION_CALL,
				data: 'tel:' + String.format(L('x636969803_traducir', '+%1$s'), telefono.toString()) + ''
			});
			Ti.Android.currentActivity.startActivity(intent);
		} catch (e) {
			Ti.Platform.openURL('tel:' + String.format(L('x636969803_traducir', '+%1$s'), telefono.toString()) + '');
		}
	} else {
		Ti.Platform.openURL('tel:' + String.format(L('x636969803_traducir', '+%1$s'), telefono.toString()) + '');
	}

}

$.widgetMono.init({
	__id: 'ALL108765079',
	texto: L('x1693196576_traducir', 'Tip: No llames a horas imprudentes y mantén siempre una buena actitud'),
	top: 50,
	ancho: '90%',
	tipo: '_tip'
});


(function() {
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	$.NombreAsegurado.setText(seltarea.asegurado_nombre);

	$.Telefono.setText(Math.round(seltarea.asegurado_tel_movil));

	$.Correo.setText(seltarea.asegurado_correo);

	$.Caso.setText(seltarea.num_caso);

})();

if (OS_IOS || OS_ANDROID) {
	$.DATOS_DEL_ASEGURADO.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}
//$.DATOS_DEL_ASEGURADO.open();
