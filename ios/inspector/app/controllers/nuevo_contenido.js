var _bind4section = {};
var _list_templates = {};
var $contenido = $.contenido.toJSON();

$.NUEVO_CONTENIDO_window.setTitleAttributes({
	color: 'WHITE'
});
var _activity;
if (OS_ANDROID) {
	_activity = $.NUEVO_CONTENIDO.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'NUEVO_CONTENIDO';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.NUEVO_CONTENIDO_window.addEventListener('open', function(e) {
		abx.setStatusbarColor("#FFFFFF");
		abx.setBackgroundColor("#8bc9e8");
	});
}

function Click_vista8(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	/** 
	 * Limpiamos widgets multiples (memoria ram) 
	 */
	$.widgetModal.limpiar({});
	$.widgetModal2.limpiar({});
	$.widgetModal3.limpiar({});
	$.widgetModal4.limpiar({});
	$.NUEVO_CONTENIDO.close();

}

function Click_vista14(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	/** 
	 * Obtenemos la fecha actual 
	 */

	var hoy = new Date();
	if ($contenido.fecha_compra > hoy == true || $contenido.fecha_compra > hoy == 'true') {
		/** 
		 * Validamos que los campos ingresados sean correctos 
		 */
		var preguntarAlerta_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x2327084729_traducir', 'La fecha de compra no puede ser superior a hoy'),
			buttonNames: preguntarAlerta_opts
		});
		preguntarAlerta.addEventListener('click', function(e) {
			var nulo = preguntarAlerta_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta.show();
	} else if (_.isUndefined($contenido.nombre)) {
		var preguntarAlerta2_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta2 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x2224804335_traducir', 'Seleccione producto'),
			buttonNames: preguntarAlerta2_opts
		});
		preguntarAlerta2.addEventListener('click', function(e) {
			var nulo = preguntarAlerta2_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta2.show();
	} else if (_.isUndefined($contenido.id_marca)) {
		var preguntarAlerta3_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta3 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x3747506986_traducir', 'Seleccione marca del producto'),
			buttonNames: preguntarAlerta3_opts
		});
		preguntarAlerta3.addEventListener('click', function(e) {
			var nulo = preguntarAlerta3_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta3.show();
	} else if (_.isUndefined($contenido.id_recinto)) {
		var preguntarAlerta4_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta4 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x2405532746_traducir', 'Seleccione el recinto donde estaba el producto'),
			buttonNames: preguntarAlerta4_opts
		});
		preguntarAlerta4.addEventListener('click', function(e) {
			var nulo = preguntarAlerta4_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta4.show();
	} else if (_.isUndefined($contenido.cantidad)) {
		var preguntarAlerta5_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta5 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x4257958740_traducir', 'Ingrese cantidad de productos'),
			buttonNames: preguntarAlerta5_opts
		});
		preguntarAlerta5.addEventListener('click', function(e) {
			var nulo = preguntarAlerta5_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta5.show();
	} else if ((_.isObject($contenido.cantidad) || _.isString($contenido.cantidad)) && _.isEmpty($contenido.cantidad)) {
		var preguntarAlerta6_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta6 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x4257958740_traducir', 'Ingrese cantidad de productos'),
			buttonNames: preguntarAlerta6_opts
		});
		preguntarAlerta6.addEventListener('click', function(e) {
			var nulo = preguntarAlerta6_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta6.show();
	} else if (_.isUndefined($contenido.fecha_compra)) {
		var preguntarAlerta7_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta7 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x1056100694_traducir', 'Ingrese fecha de compra aproximada (año)'),
			buttonNames: preguntarAlerta7_opts
		});
		preguntarAlerta7.addEventListener('click', function(e) {
			var nulo = preguntarAlerta7_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta7.show();
	} else if (_.isUndefined($contenido.id_moneda)) {
		var preguntarAlerta8_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta8 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x2624129841_traducir', 'Seleccione el tipo de moneda del producto'),
			buttonNames: preguntarAlerta8_opts
		});
		preguntarAlerta8.addEventListener('click', function(e) {
			var nulo = preguntarAlerta8_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta8.show();
	} else if (_.isUndefined($contenido.valor)) {
		var preguntarAlerta9_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta9 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x1829790159_traducir', 'Ingrese valor del producto'),
			buttonNames: preguntarAlerta9_opts
		});
		preguntarAlerta9.addEventListener('click', function(e) {
			var nulo = preguntarAlerta9_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta9.show();
	} else if ((_.isObject($contenido.valor) || _.isString($contenido.valor)) && _.isEmpty($contenido.valor)) {
		var preguntarAlerta10_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta10 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x1829790159_traducir', 'Ingrese valor del producto'),
			buttonNames: preguntarAlerta10_opts
		});
		preguntarAlerta10.addEventListener('click', function(e) {
			var nulo = preguntarAlerta10_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta10.show();
	} else if (_.isUndefined($contenido.descripcion)) {
		var preguntarAlerta11_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta11 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x417344283_traducir', 'Describa el producto brevemente, con un mínimo de 30 caracteres'),
			buttonNames: preguntarAlerta11_opts
		});
		preguntarAlerta11.addEventListener('click', function(e) {
			var nulo = preguntarAlerta11_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta11.show();
	} else if (_.isNumber($contenido.descripcion.length) && _.isNumber(29) && $contenido.descripcion.length <= 29) {
		var preguntarAlerta12_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta12 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x417344283_traducir', 'Describa el producto brevemente, con un mínimo de 30 caracteres'),
			buttonNames: preguntarAlerta12_opts
		});
		preguntarAlerta12.addEventListener('click', function(e) {
			var nulo = preguntarAlerta12_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta12.show();
	} else {
		if (!_.isUndefined(args._dato)) {
			/** 
			 * Si estamos editando, debemos borrar modelo previo. 
			 */
			var id_contenido = ('id_contenido' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['id_contenido'] : '';
			var eliminarModelo2_i = Alloy.Collections.insp_contenido;
			var sql = 'DELETE FROM ' + eliminarModelo2_i.config.adapter.collection_name + ' WHERE id=\'' + id_contenido + '\'';
			var db = Ti.Database.open(eliminarModelo2_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo2_i.trigger('remove');
		}
		/** 
		 * Guardamos modelo nuevo 
		 */
		Alloy.Collections[$.contenido.config.adapter.collection_name].add($.contenido);
		$.contenido.save();
		Alloy.Collections[$.contenido.config.adapter.collection_name].fetch();
		test = null;
		/** 
		 * limpiamos widgets multiples (memoria ram) 
		 */
		$.widgetModal.limpiar({});
		$.widgetModal2.limpiar({});
		$.widgetModal3.limpiar({});
		$.widgetModal4.limpiar({});
		$.NUEVO_CONTENIDO.close();
	}
}

$.widgetDpicker.init({
	__id: 'ALL1585064981',
	aceptar: L('x1518866076_traducir', 'Aceptar'),
	cancelar: L('x2353348866_traducir', 'Cancelar'),
	onaceptar: Aceptar_widgetDpicker,
	oncancelar: Cancelar_widgetDpicker
});

function Aceptar_widgetDpicker(e) {

	var evento = e;
	/** 
	 * Formateamos la fecha que responde el widget, mostramos en pantalla y actualizamos la fecha de compra 
	 */
	var moment = require('alloy/moment');
	var formatearFecha = evento.valor;
	var resp = moment(formatearFecha).format('DD-MM-YYYY');
	$.DDMMYYYY.setText(resp);

	var DDMMYYYY_estilo = 'estilo12';

	var _tmp_a4w = require('a4w');
	if ((typeof DDMMYYYY_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (DDMMYYYY_estilo in _tmp_a4w.styles['classes'])) {
		try {
			DDMMYYYY_estilo = _tmp_a4w.styles['classes'][DDMMYYYY_estilo];
		} catch (st_val_err) {}
	}
	_tmp_a4w = null;
	$.DDMMYYYY.applyProperties(DDMMYYYY_estilo);

	$.contenido.set({
		fecha_compra: evento.valor
	});
	if ('contenido' in $) $contenido = $.contenido.toJSON();
	if (Ti.App.deployType != 'production') console.log('FECHA SELECCIONADA', {
		"fecha": evento.valor
	});

}

function Cancelar_widgetDpicker(e) {

	var evento = e;
	if (Ti.App.deployType != 'production') console.log('selector de fecha compra cancelado', {});

}

$.widgetModal.init({
	titulo: L('x850824713_traducir', 'PRODUCTOS'),
	cargando: L('x1740321226_traducir', 'cargando ..'),
	__id: 'ALL1687673767',
	left: 0,
	onrespuesta: Respuesta_widgetModal,
	campo: L('x425774392_traducir', 'Nombre del Item'),
	onabrir: Abrir_widgetModal,
	color: 'celeste',
	right: 0,
	top: 15,
	seleccione: L('x2719983611_traducir', 'seleccione producto'),
	activo: true,
	onafterinit: Afterinit_widgetModal
});

function Afterinit_widgetModal(e) {

	var evento = e;
	var ID_1331103465_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (!_.isUndefined(args._dato)) {
			/** 
			 * Si estamos editando contenido, cargamos dato previo seleccionado en base de datos. 
			 */
			var consultarModelo3_i = Alloy.createCollection('insp_contenido');
			var consultarModelo3_i_where = 'id=\'' + args._dato.id + '\'';
			consultarModelo3_i.fetch({
				query: 'SELECT * FROM insp_contenido WHERE id=\'' + args._dato.id + '\''
			});
			var insp_dato = require('helper').query2array(consultarModelo3_i);
			if (insp_dato && insp_dato.length) {
				/** 
				 * obtenemos valor seleccionado previamente y lo mostramos en selector. 
				 */
				var consultarModelo4_i = Alloy.createCollection('bienes');
				var consultarModelo4_i_where = 'id_segured=\'' + insp_dato[0].id_nombre + '\'';
				consultarModelo4_i.fetch({
					query: 'SELECT * FROM bienes WHERE id_segured=\'' + insp_dato[0].id_nombre + '\''
				});
				var bienes = require('helper').query2array(consultarModelo4_i);
				if (bienes && bienes.length) {
					var ID_87299450_func = function() {
						$.widgetModal.labels({
							valor: bienes[0].nombre
						});
						if (Ti.App.deployType != 'production') console.log('el nombre de la partida es', {
							"datos": bienes[0].nombre
						});
					};
					var ID_87299450 = setTimeout(ID_87299450_func, 1000 * 0.1);
				}
			}
		}
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			/** 
			 * Obtenemos datos para selectores (solo los de este pais) 
			 */
			var consultarModelo5_i = Alloy.createCollection('bienes');
			var consultarModelo5_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
			consultarModelo5_i.fetch({
				query: 'SELECT * FROM bienes WHERE pais_texto=\'' + pais[0].nombre + '\''
			});
			var bienes = require('helper').query2array(consultarModelo5_i);
			var datos = [];
			_.each(bienes, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'valor';
					if (llave == 'id_segured') newkey = 'id_interno';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
		} else {
			var eliminarModelo3_i = Alloy.Collections.bienes;
			var sql = "DELETE FROM " + eliminarModelo3_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo3_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo3_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo2_m = Alloy.Collections.bienes;
				var insertarModelo2_fila = Alloy.createModel('bienes', {
					nombre: String.format(L('x3389261753_traducir', 'Lampara%1$s'), item.toString()),
					id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
					id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item.toString()),
					pais: 1
				});
				insertarModelo2_m.add(insertarModelo2_fila);
				insertarModelo2_fila.save();
			});
			var transformarModelo2_i = Alloy.createCollection('bienes');
			transformarModelo2_i.fetch();
			var transformarModelo2_src = require('helper').query2array(transformarModelo2_i);
			var datos = [];
			_.each(transformarModelo2_src, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'valor';
					if (llave == 'id_segured') newkey = 'id_interno';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
		}
		/** 
		 * Obtenemos datos para selectores 
		 */
		$.widgetModal.data({
			data: datos
		});
	};
	var ID_1331103465 = setTimeout(ID_1331103465_func, 1000 * 0.2);

}

function Abrir_widgetModal(e) {

	var evento = e;

}

function Respuesta_widgetModal(e) {

	var evento = e;
	$.widgetModal.labels({
		valor: evento.valor
	});
	/** 
	 * Asumimos que el valor mostrado seleccionado es el nombre de la fila dano, ya que no existe el campo nombre en esta pantalla. 
	 */
	$.contenido.set({
		nombre: evento.valor,
		id_nombre: evento.item.id_interno
	});
	if ('contenido' in $) $contenido = $.contenido.toJSON();
	if (Ti.App.deployType != 'production') console.log('datos recibidos de modal nombre item', {
		"datos": evento
	});

}

$.widgetModal2.init({
	titulo: L('x1867465554_traducir', 'MARCAS'),
	cargando: L('x1740321226_traducir', 'cargando ..'),
	__id: 'ALL1413999243',
	left: 0,
	onrespuesta: Respuesta_widgetModal2,
	campo: L('x3837495059_traducir', 'Marca del Item'),
	onabrir: Abrir_widgetModal2,
	color: 'celeste',
	right: 0,
	top: 20,
	seleccione: L('x1021431138_traducir', 'seleccione marca'),
	activo: true,
	onafterinit: Afterinit_widgetModal2
});

function Afterinit_widgetModal2(e) {

	var evento = e;
	var ID_1686389527_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (!_.isUndefined(args._dato)) {
			/** 
			 * Si estamos editando contenido, cargamos dato previo seleccionado en base de datos. 
			 */
			var consultarModelo6_i = Alloy.createCollection('insp_contenido');
			var consultarModelo6_i_where = 'id=\'' + args._dato.id + '\'';
			consultarModelo6_i.fetch({
				query: 'SELECT * FROM insp_contenido WHERE id=\'' + args._dato.id + '\''
			});
			var insp_dato = require('helper').query2array(consultarModelo6_i);
			if (insp_dato && insp_dato.length) {
				/** 
				 * obtenemos valor seleccionado previamente y lo mostramos en selector. 
				 */
				var consultarModelo7_i = Alloy.createCollection('marcas');
				var consultarModelo7_i_where = 'id_segured=\'' + insp_dato[0].id_marca + '\'';
				consultarModelo7_i.fetch({
					query: 'SELECT * FROM marcas WHERE id_segured=\'' + insp_dato[0].id_marca + '\''
				});
				var marcas = require('helper').query2array(consultarModelo7_i);
				if (marcas && marcas.length) {
					var ID_81923934_func = function() {
						$.widgetModal2.labels({
							valor: marcas[0].nombre
						});
						if (Ti.App.deployType != 'production') console.log('el nombre de la marca es', {
							"datos": marcas[0].nombre
						});
					};
					var ID_81923934 = setTimeout(ID_81923934_func, 1000 * 0.1);
				}
			}
		}
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			var consultarModelo8_i = Alloy.createCollection('marcas');
			var consultarModelo8_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
			consultarModelo8_i.fetch({
				query: 'SELECT * FROM marcas WHERE pais_texto=\'' + pais[0].nombre + '\''
			});
			var marcas = require('helper').query2array(consultarModelo8_i);
			var datos = [];
			_.each(marcas, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'valor';
					if (llave == 'id_segured') newkey = 'id_interno';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
		} else {
			var eliminarModelo4_i = Alloy.Collections.marcas;
			var sql = "DELETE FROM " + eliminarModelo4_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo4_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo4_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo3_m = Alloy.Collections.marcas;
				var insertarModelo3_fila = Alloy.createModel('marcas', {
					nombre: String.format(L('x391662843_traducir', 'HomeDepot%1$s'), item.toString()),
					id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
					id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item.toString()),
					pais: 1
				});
				insertarModelo3_m.add(insertarModelo3_fila);
				insertarModelo3_fila.save();
			});
			var transformarModelo4_i = Alloy.createCollection('marcas');
			transformarModelo4_i.fetch();
			var transformarModelo4_src = require('helper').query2array(transformarModelo4_i);
			var datos = [];
			_.each(transformarModelo4_src, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'valor';
					if (llave == 'id_segured') newkey = 'id_interno';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
		}
		$.widgetModal2.data({
			data: datos
		});
	};
	var ID_1686389527 = setTimeout(ID_1686389527_func, 1000 * 0.2);

}

function Abrir_widgetModal2(e) {

	var evento = e;

}

function Respuesta_widgetModal2(e) {

	var evento = e;
	$.widgetModal2.labels({
		valor: evento.valor
	});
	$.contenido.set({
		id_marca: evento.item.id_interno
	});
	if ('contenido' in $) $contenido = $.contenido.toJSON();
	if (Ti.App.deployType != 'production') console.log('datos recibidos de modal marca item', {
		"datos": evento
	});

}

$.widgetFotochica.init({
	caja: L('x2889884971_traducir', '45'),
	__id: 'ALL1420842518',
	onlisto: Listo_widgetFotochica,
	onclick: Click_widgetFotochica
});

function Click_widgetFotochica(e) {

	var evento = e;
	/** 
	 * Capturamos foto 
	 */
	var capturarFoto_result = function(e) {
		var foto_full = e.valor;
		var foto = e.valor.data;
		if (foto_full.error == true || foto_full.error == 'true') {
			/** 
			 * Mostramos alerta indicando que ha ocurrido un error al sacar la foto 
			 */
			var preguntarAlerta13_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta13 = Ti.UI.createAlertDialog({
				title: L('x57652245_traducir', 'Alerta'),
				message: L('x1068520423_traducir', 'Existe un problema con la camara'),
				buttonNames: preguntarAlerta13_opts
			});
			preguntarAlerta13.addEventListener('click', function(e) {
				var suu = preguntarAlerta13_opts[e.index];
				suu = null;
				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta13.show();
		} else {
			/** 
			 * Enviamos a procesar la foto al widget fotochica 
			 */
			$.widgetFotochica.procesar({
				imagen: foto,
				nueva: 640,
				calidad: 91
			});
		}
	};

	function camara_capturarFoto() {
		Ti.Media.showCamera({
			success: function(event) {
				if (event.mediaType == Ti.Media.MEDIA_TYPE_PHOTO) {
					capturarFoto_result({
						valor: {
							error: false,
							cancel: false,
							data: event.media,
							reason: ''
						}
					});
				} else {
					capturarFoto_result({
						valor: {
							error: true,
							cancel: false,
							data: '',
							reason: 'not image'
						}
					});
				}
			},
			cancel: function() {
				capturarFoto_result({
					valor: {
						error: false,
						cancel: true,
						data: '',
						reason: 'cancelled'
					}
				});
			},
			error: function(error) {
				capturarFoto_result({
					valor: {
						error: true,
						cancel: false,
						data: error,
						reason: error.error
					}
				});
			},
			saveToPhotoGallery: false,
			allowImageEditing: true,
			mediaTypes: [Ti.Media.MEDIA_TYPE_PHOTO]
		});
	}
	require('vars')['_estadoflash_'] = 'auto';
	require('vars')['_tipocamara_'] = 'trasera';
	require('vars')['_hayflash_'] = true;
	if (Ti.Media.hasCameraPermissions()) {
		camara_capturarFoto();
	} else {
		Ti.Media.requestCameraPermissions(function(ercp) {
			if (ercp.success) {
				camara_capturarFoto();
			} else {
				capturarFoto_result({
					valor: {
						error: true,
						cancel: false,
						data: 'nopermission',
						type: 'permission',
						reason: 'camera doesnt have permissions'
					}
				});
			}
		});
	}

}

function Listo_widgetFotochica(e) {

	var evento = e;
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	var insertarModelo4_m = Alloy.Collections.numero_unico;
	var insertarModelo4_fila = Alloy.createModel('numero_unico', {
		comentario: 'nuevo danocontenido foto1'
	});
	insertarModelo4_m.add(insertarModelo4_fila);
	insertarModelo4_fila.save();
	var nuevoid = require('helper').model2object(insertarModelo4_m.last());
	if (!_.isUndefined(evento.comprimida)) {
		if (Ti.App.deployType != 'production') console.log(String.format(L('x2721240094', 'imagen capturada, comprimida y asignada como cap%1$s.jpg'), nuevoid.id.toString()), {});
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var ID_921903760_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
			if (ID_921903760_d.exists() == false) ID_921903760_d.createDirectory();
			var ID_921903760_f = Ti.Filesystem.getFile(ID_921903760_d.resolve(), 'cap' + nuevoid.id + '.jpg');
			if (ID_921903760_f.exists() == true) ID_921903760_f.deleteFile();
			ID_921903760_f.write(evento.comprimida);
			ID_921903760_d = null;
			ID_921903760_f = null;
			var ID_1150065702_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'mini' + seltarea.id_server);
			if (ID_1150065702_d.exists() == false) ID_1150065702_d.createDirectory();
			var ID_1150065702_f = Ti.Filesystem.getFile(ID_1150065702_d.resolve(), 'cap' + nuevoid.id + '.jpg');
			if (ID_1150065702_f.exists() == true) ID_1150065702_f.deleteFile();
			ID_1150065702_f.write(evento.mini);
			ID_1150065702_d = null;
			ID_1150065702_f = null;
		} else {
			var ID_632543922_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + 0);
			if (ID_632543922_d.exists() == false) ID_632543922_d.createDirectory();
			var ID_632543922_f = Ti.Filesystem.getFile(ID_632543922_d.resolve(), 'cap' + nuevoid.id + '.jpg');
			if (ID_632543922_f.exists() == true) ID_632543922_f.deleteFile();
			ID_632543922_f.write(evento.comprimida);
			ID_632543922_d = null;
			ID_632543922_f = null;
			var ID_697608180_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'mini' + 0);
			if (ID_697608180_d.exists() == false) ID_697608180_d.createDirectory();
			var ID_697608180_f = Ti.Filesystem.getFile(ID_697608180_d.resolve(), 'cap' + nuevoid.id + '.jpg');
			if (ID_697608180_f.exists() == true) ID_697608180_f.deleteFile();
			ID_697608180_f.write(evento.mini);
			ID_697608180_d = null;
			ID_697608180_f = null;
		}
		$.contenido.set({
			foto1: String.format(L('x1070475521', 'cap%1$s.jpg'), nuevoid.id.toString())
		});
		if ('contenido' in $) $contenido = $.contenido.toJSON();
	}

}

$.widgetFotochica2.init({
	caja: L('x2889884971_traducir', '45'),
	__id: 'ALL677343075',
	onlisto: Listo_widgetFotochica2,
	onclick: Click_widgetFotochica2
});

function Click_widgetFotochica2(e) {

	var evento = e;
	var capturarFoto2_result = function(e) {
		var foto_full = e.valor;
		var foto = e.valor.data;
		if (foto_full.error == true || foto_full.error == 'true') {
			var preguntarAlerta14_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta14 = Ti.UI.createAlertDialog({
				title: L('x57652245_traducir', 'Alerta'),
				message: L('x1068520423_traducir', 'Existe un problema con la camara'),
				buttonNames: preguntarAlerta14_opts
			});
			preguntarAlerta14.addEventListener('click', function(e) {
				var suu = preguntarAlerta14_opts[e.index];
				suu = null;
				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta14.show();
		} else {
			$.widgetFotochica2.procesar({
				imagen: foto,
				nueva: 640,
				calidad: 91
			});
		}
	};

	function camara_capturarFoto2() {
		Ti.Media.showCamera({
			success: function(event) {
				if (event.mediaType == Ti.Media.MEDIA_TYPE_PHOTO) {
					capturarFoto2_result({
						valor: {
							error: false,
							cancel: false,
							data: event.media,
							reason: ''
						}
					});
				} else {
					capturarFoto2_result({
						valor: {
							error: true,
							cancel: false,
							data: '',
							reason: 'not image'
						}
					});
				}
			},
			cancel: function() {
				capturarFoto2_result({
					valor: {
						error: false,
						cancel: true,
						data: '',
						reason: 'cancelled'
					}
				});
			},
			error: function(error) {
				capturarFoto2_result({
					valor: {
						error: true,
						cancel: false,
						data: error,
						reason: error.error
					}
				});
			},
			saveToPhotoGallery: false,
			allowImageEditing: true,
			mediaTypes: [Ti.Media.MEDIA_TYPE_PHOTO]
		});
	}
	require('vars')['_estadoflash_'] = 'auto';
	require('vars')['_tipocamara_'] = 'trasera';
	require('vars')['_hayflash_'] = true;
	if (Ti.Media.hasCameraPermissions()) {
		camara_capturarFoto2();
	} else {
		Ti.Media.requestCameraPermissions(function(ercp) {
			if (ercp.success) {
				camara_capturarFoto2();
			} else {
				capturarFoto2_result({
					valor: {
						error: true,
						cancel: false,
						data: 'nopermission',
						type: 'permission',
						reason: 'camera doesnt have permissions'
					}
				});
			}
		});
	}

}

function Listo_widgetFotochica2(e) {

	var evento = e;
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	var insertarModelo5_m = Alloy.Collections.numero_unico;
	var insertarModelo5_fila = Alloy.createModel('numero_unico', {
		comentario: 'nuevo danocontenido foto2'
	});
	insertarModelo5_m.add(insertarModelo5_fila);
	insertarModelo5_fila.save();
	var nuevoid = require('helper').model2object(insertarModelo5_m.last());
	if (!_.isUndefined(evento.comprimida)) {
		if (Ti.App.deployType != 'production') console.log(String.format(L('x2721240094', 'imagen capturada, comprimida y asignada como cap%1$s.jpg'), nuevoid.id.toString()), {});
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var ID_434647999_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
			if (ID_434647999_d.exists() == false) ID_434647999_d.createDirectory();
			var ID_434647999_f = Ti.Filesystem.getFile(ID_434647999_d.resolve(), 'cap' + nuevoid.id + '.jpg');
			if (ID_434647999_f.exists() == true) ID_434647999_f.deleteFile();
			ID_434647999_f.write(evento.comprimida);
			ID_434647999_d = null;
			ID_434647999_f = null;
			var ID_710898845_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'mini' + seltarea.id_server);
			if (ID_710898845_d.exists() == false) ID_710898845_d.createDirectory();
			var ID_710898845_f = Ti.Filesystem.getFile(ID_710898845_d.resolve(), 'cap' + nuevoid.id + '.jpg');
			if (ID_710898845_f.exists() == true) ID_710898845_f.deleteFile();
			ID_710898845_f.write(evento.mini);
			ID_710898845_d = null;
			ID_710898845_f = null;
		} else {
			var ID_1308965600_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + 0);
			if (ID_1308965600_d.exists() == false) ID_1308965600_d.createDirectory();
			var ID_1308965600_f = Ti.Filesystem.getFile(ID_1308965600_d.resolve(), 'cap' + nuevoid.id + '.jpg');
			if (ID_1308965600_f.exists() == true) ID_1308965600_f.deleteFile();
			ID_1308965600_f.write(evento.comprimida);
			ID_1308965600_d = null;
			ID_1308965600_f = null;
			var ID_1473417320_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'mini' + 0);
			if (ID_1473417320_d.exists() == false) ID_1473417320_d.createDirectory();
			var ID_1473417320_f = Ti.Filesystem.getFile(ID_1473417320_d.resolve(), 'cap' + nuevoid.id + '.jpg');
			if (ID_1473417320_f.exists() == true) ID_1473417320_f.deleteFile();
			ID_1473417320_f.write(evento.mini);
			ID_1473417320_d = null;
			ID_1473417320_f = null;
		}
		$.contenido.set({
			foto2: String.format(L('x1070475521', 'cap%1$s.jpg'), nuevoid.id.toString())
		});
		if ('contenido' in $) $contenido = $.contenido.toJSON();
	}

}

$.widgetFotochica3.init({
	caja: L('x2889884971_traducir', '45'),
	__id: 'ALL333724151',
	onlisto: Listo_widgetFotochica3,
	onclick: Click_widgetFotochica3
});

function Click_widgetFotochica3(e) {

	var evento = e;
	var capturarFoto3_result = function(e) {
		var foto_full = e.valor;
		var foto = e.valor.data;
		if (foto_full.error == true || foto_full.error == 'true') {
			var preguntarAlerta15_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta15 = Ti.UI.createAlertDialog({
				title: L('x57652245_traducir', 'Alerta'),
				message: L('x1068520423_traducir', 'Existe un problema con la camara'),
				buttonNames: preguntarAlerta15_opts
			});
			preguntarAlerta15.addEventListener('click', function(e) {
				var suu = preguntarAlerta15_opts[e.index];
				suu = null;
				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta15.show();
		} else {
			$.widgetFotochica3.procesar({
				imagen: foto,
				nueva: 640,
				calidad: 91
			});
		}
	};

	function camara_capturarFoto3() {
		Ti.Media.showCamera({
			success: function(event) {
				if (event.mediaType == Ti.Media.MEDIA_TYPE_PHOTO) {
					capturarFoto3_result({
						valor: {
							error: false,
							cancel: false,
							data: event.media,
							reason: ''
						}
					});
				} else {
					capturarFoto3_result({
						valor: {
							error: true,
							cancel: false,
							data: '',
							reason: 'not image'
						}
					});
				}
			},
			cancel: function() {
				capturarFoto3_result({
					valor: {
						error: false,
						cancel: true,
						data: '',
						reason: 'cancelled'
					}
				});
			},
			error: function(error) {
				capturarFoto3_result({
					valor: {
						error: true,
						cancel: false,
						data: error,
						reason: error.error
					}
				});
			},
			saveToPhotoGallery: false,
			allowImageEditing: true,
			mediaTypes: [Ti.Media.MEDIA_TYPE_PHOTO]
		});
	}
	require('vars')['_estadoflash_'] = 'auto';
	require('vars')['_tipocamara_'] = 'trasera';
	require('vars')['_hayflash_'] = true;
	if (Ti.Media.hasCameraPermissions()) {
		camara_capturarFoto3();
	} else {
		Ti.Media.requestCameraPermissions(function(ercp) {
			if (ercp.success) {
				camara_capturarFoto3();
			} else {
				capturarFoto3_result({
					valor: {
						error: true,
						cancel: false,
						data: 'nopermission',
						type: 'permission',
						reason: 'camera doesnt have permissions'
					}
				});
			}
		});
	}

}

function Listo_widgetFotochica3(e) {

	var evento = e;
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	var insertarModelo6_m = Alloy.Collections.numero_unico;
	var insertarModelo6_fila = Alloy.createModel('numero_unico', {
		comentario: 'nuevo danocontenido foto3'
	});
	insertarModelo6_m.add(insertarModelo6_fila);
	insertarModelo6_fila.save();
	var nuevoid = require('helper').model2object(insertarModelo6_m.last());
	if (!_.isUndefined(evento.comprimida)) {
		if (Ti.App.deployType != 'production') console.log(String.format(L('x2721240094', 'imagen capturada, comprimida y asignada como cap%1$s.jpg'), nuevoid.id.toString()), {});
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var ID_1791446374_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
			if (ID_1791446374_d.exists() == false) ID_1791446374_d.createDirectory();
			var ID_1791446374_f = Ti.Filesystem.getFile(ID_1791446374_d.resolve(), 'cap' + nuevoid.id + '.jpg');
			if (ID_1791446374_f.exists() == true) ID_1791446374_f.deleteFile();
			ID_1791446374_f.write(evento.comprimida);
			ID_1791446374_d = null;
			ID_1791446374_f = null;
			var ID_531307954_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'mini' + seltarea.id_server);
			if (ID_531307954_d.exists() == false) ID_531307954_d.createDirectory();
			var ID_531307954_f = Ti.Filesystem.getFile(ID_531307954_d.resolve(), 'cap' + nuevoid.id + '.jpg');
			if (ID_531307954_f.exists() == true) ID_531307954_f.deleteFile();
			ID_531307954_f.write(evento.mini);
			ID_531307954_d = null;
			ID_531307954_f = null;
		} else {
			var ID_1791226483_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + 0);
			if (ID_1791226483_d.exists() == false) ID_1791226483_d.createDirectory();
			var ID_1791226483_f = Ti.Filesystem.getFile(ID_1791226483_d.resolve(), 'cap' + nuevoid.id + '.jpg');
			if (ID_1791226483_f.exists() == true) ID_1791226483_f.deleteFile();
			ID_1791226483_f.write(evento.comprimida);
			ID_1791226483_d = null;
			ID_1791226483_f = null;
			var ID_41835150_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'mini' + 0);
			if (ID_41835150_d.exists() == false) ID_41835150_d.createDirectory();
			var ID_41835150_f = Ti.Filesystem.getFile(ID_41835150_d.resolve(), 'cap' + nuevoid.id + '.jpg');
			if (ID_41835150_f.exists() == true) ID_41835150_f.deleteFile();
			ID_41835150_f.write(evento.mini);
			ID_41835150_d = null;
			ID_41835150_f = null;
		}
		$.contenido.set({
			foto3: String.format(L('x1070475521', 'cap%1$s.jpg'), nuevoid.id.toString())
		});
		if ('contenido' in $) $contenido = $.contenido.toJSON();
	}

}

$.widgetModal3.init({
	titulo: L('x767609104_traducir', 'RECINTOS'),
	cargando: L('x1740321226_traducir', 'cargando ..'),
	__id: 'ALL1908023149',
	left: 0,
	onrespuesta: Respuesta_widgetModal3,
	campo: L('x382177638_traducir', 'Ubicación'),
	onabrir: Abrir_widgetModal3,
	color: 'celeste',
	right: 5,
	top: 3,
	seleccione: L('x4060681104_traducir', 'recinto'),
	activo: true,
	onafterinit: Afterinit_widgetModal3
});

function Afterinit_widgetModal3(e) {

	var evento = e;
	var ID_940344009_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (!_.isUndefined(args._dato)) {
			/** 
			 * Si estamos editando contenido, cargamos dato previo seleccionado en base de datos. 
			 */
			var consultarModelo9_i = Alloy.createCollection('insp_contenido');
			var consultarModelo9_i_where = 'id=\'' + args._dato.id + '\'';
			consultarModelo9_i.fetch({
				query: 'SELECT * FROM insp_contenido WHERE id=\'' + args._dato.id + '\''
			});
			var insp_dato = require('helper').query2array(consultarModelo9_i);
			if (insp_dato && insp_dato.length) {
				/** 
				 * obtenemos valor seleccionado previamente y lo mostramos en selector. 
				 */
				var consultarModelo10_i = Alloy.createCollection('insp_recintos');
				var consultarModelo10_i_where = 'id_recinto=\'' + insp_dato[0].id_recinto + '\'';
				consultarModelo10_i.fetch({
					query: 'SELECT * FROM insp_recintos WHERE id_recinto=\'' + insp_dato[0].id_recinto + '\''
				});
				var recintos = require('helper').query2array(consultarModelo10_i);
				if (recintos && recintos.length) {
					var ID_1331976723_func = function() {
						$.widgetModal3.labels({
							valor: recintos[0].nombre
						});
						if (Ti.App.deployType != 'production') console.log('el nombre del recinto es', {
							"datos": recintos[0].nombre
						});
					};
					var ID_1331976723 = setTimeout(ID_1331976723_func, 1000 * 0.1);
				}
			}
		}
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			var consultarModelo11_i = Alloy.createCollection('insp_recintos');
			var consultarModelo11_i_where = 'id_inspeccion=\'' + seltarea.id_server + '\'';
			consultarModelo11_i.fetch({
				query: 'SELECT * FROM insp_recintos WHERE id_inspeccion=\'' + seltarea.id_server + '\''
			});
			var recintos = require('helper').query2array(consultarModelo11_i);
			var datos = [];
			_.each(recintos, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'valor';
					if (llave == 'id_recinto') newkey = 'id_interno';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
		} else {
			var eliminarModelo5_i = Alloy.Collections.insp_recintos;
			var sql = "DELETE FROM " + eliminarModelo5_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo5_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo5_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo7_m = Alloy.Collections.insp_recintos;
				var insertarModelo7_fila = Alloy.createModel('insp_recintos', {
					nombre: String.format(L('x1101343128_traducir', 'Pieza%1$s'), item.toString()),
					id_recinto: item
				});
				insertarModelo7_m.add(insertarModelo7_fila);
				insertarModelo7_fila.save();
			});
			var transformarModelo6_i = Alloy.createCollection('insp_recintos');
			transformarModelo6_i.fetch();
			var transformarModelo6_src = require('helper').query2array(transformarModelo6_i);
			var datos = [];
			_.each(transformarModelo6_src, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'valor';
					if (llave == 'id_recinto') newkey = 'id_interno';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
		}
		$.widgetModal3.data({
			data: datos
		});
	};
	var ID_940344009 = setTimeout(ID_940344009_func, 1000 * 0.1);

}

function Abrir_widgetModal3(e) {

	var evento = e;

}

function Respuesta_widgetModal3(e) {

	var evento = e;
	$.widgetModal3.labels({
		valor: evento.valor
	});
	$.contenido.set({
		id_recinto: evento.item.id_interno
	});
	if ('contenido' in $) $contenido = $.contenido.toJSON();
	if (Ti.App.deployType != 'production') console.log('datos recibidos de modal ubicacion', {
		"datos": evento
	});

}

function Change_campo(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	$.contenido.set({
		cantidad: elemento
	});
	if ('contenido' in $) $contenido = $.contenido.toJSON();
	elemento = null, source = null;

}

function Click_vista26(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.vista25.fireEvent('click');

}

function Click_vista27(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.vista25.fireEvent('click');

}

function Click_vista25(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.campo.blur();
	$.campo2.blur();
	$.area.blur();
	$.widgetDpicker.abrir({});

}

$.widgetModal4.init({
	titulo: L('x1629775439_traducir', 'MONEDAS'),
	cargando: L('x1740321226_traducir', 'cargando ..'),
	__id: 'ALL1996770915',
	left: 0,
	onrespuesta: Respuesta_widgetModal4,
	campo: L('x3081186843_traducir', 'Moneda'),
	onabrir: Abrir_widgetModal4,
	color: 'celeste',
	right: 5,
	top: 3,
	seleccione: L('x2547889144', '-'),
	activo: true,
	onafterinit: Afterinit_widgetModal4
});

function Afterinit_widgetModal4(e) {

	var evento = e;
	var ID_961160588_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (!_.isUndefined(args._dato)) {
			/** 
			 * Si estamos editando contenido, cargamos dato previo seleccionado en base de datos. 
			 */
			var consultarModelo12_i = Alloy.createCollection('insp_contenido');
			var consultarModelo12_i_where = 'id=\'' + args._dato.id + '\'';
			consultarModelo12_i.fetch({
				query: 'SELECT * FROM insp_contenido WHERE id=\'' + args._dato.id + '\''
			});
			var insp_dato = require('helper').query2array(consultarModelo12_i);
			if (insp_dato && insp_dato.length) {
				/** 
				 * obtenemos valor seleccionado previamente y lo mostramos en selector. 
				 */
				var consultarModelo13_i = Alloy.createCollection('monedas');
				var consultarModelo13_i_where = 'id_segured=\'' + insp_dato[0].id_moneda + '\'';
				consultarModelo13_i.fetch({
					query: 'SELECT * FROM monedas WHERE id_segured=\'' + insp_dato[0].id_moneda + '\''
				});
				var monedas = require('helper').query2array(consultarModelo13_i);
				if (monedas && monedas.length) {
					var ID_1788581551_func = function() {
						$.widgetModal4.labels({
							valor: monedas[0].nombre
						});
						if (Ti.App.deployType != 'production') console.log('el nombre de la moneda es', {
							"datos": monedas[0].nombre
						});
					};
					var ID_1788581551 = setTimeout(ID_1788581551_func, 1000 * 0.1);
				}
			}
		}
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			var consultarModelo14_i = Alloy.createCollection('monedas');
			var consultarModelo14_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
			consultarModelo14_i.fetch({
				query: 'SELECT * FROM monedas WHERE pais_texto=\'' + pais[0].nombre + '\''
			});
			var monedas = require('helper').query2array(consultarModelo14_i);
			var datos = [];
			_.each(monedas, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'valor';
					if (llave == 'id_segured') newkey = 'id_interno';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
		} else {
			var eliminarModelo6_i = Alloy.Collections.monedas;
			var sql = "DELETE FROM " + eliminarModelo6_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo6_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo6_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo8_m = Alloy.Collections.monedas;
				var insertarModelo8_fila = Alloy.createModel('monedas', {
					nombre: String.format(L('x4148455394_traducir', 'CLP%1$s'), item.toString()),
					id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item.toString())
				});
				insertarModelo8_m.add(insertarModelo8_fila);
				insertarModelo8_fila.save();
			});
			var transformarModelo8_i = Alloy.createCollection('monedas');
			transformarModelo8_i.fetch();
			var transformarModelo8_src = require('helper').query2array(transformarModelo8_i);
			var datos = [];
			_.each(transformarModelo8_src, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'valor';
					if (llave == 'id_segured') newkey = 'id_interno';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
		}
		$.widgetModal4.data({
			data: datos
		});
	};
	var ID_961160588 = setTimeout(ID_961160588_func, 1000 * 0.1);

}

function Abrir_widgetModal4(e) {

	var evento = e;
	$.widgetDpicker.cerrar({});

}

function Respuesta_widgetModal4(e) {

	var evento = e;
	$.widgetModal4.labels({
		valor: evento.valor
	});
	$.contenido.set({
		id_moneda: evento.item.id_interno
	});
	if ('contenido' in $) $contenido = $.contenido.toJSON();
	if (Ti.App.deployType != 'production') console.log('datos recibidos de modal monedas', {
		"datos": evento
	});

}

function Change_campo2(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	$.contenido.set({
		valor: elemento
	});
	if ('contenido' in $) $contenido = $.contenido.toJSON();
	elemento = null, source = null;

}

function Change_ID_1568773770(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	if (elemento == true || elemento == 'true') {
		$.NO.setText('SI');

		$.contenido.set({
			recupero: 1
		});
		if ('contenido' in $) $contenido = $.contenido.toJSON();
	} else {
		$.NO.setText('NO');

		$.contenido.set({
			recupero: 0
		});
		if ('contenido' in $) $contenido = $.contenido.toJSON();
	}
	elemento = null;

}

function Change_area(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	$.contenido.set({
		descripcion: elemento
	});
	if ('contenido' in $) $contenido = $.contenido.toJSON();
	elemento = null, source = null;

}

(function() {
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
		/** 
		 * Actualizamos el id de la inspeccion en la tabla de contenidos 
		 */
		$.contenido.set({
			id_inspeccion: seltarea.id_server
		});
		if ('contenido' in $) $contenido = $.contenido.toJSON();
		$.contenido.set({
			recupero: 0
		});
		if ('contenido' in $) $contenido = $.contenido.toJSON();
	}
	if (!_.isUndefined(args._dato)) {
		/** 
		 * si existe args._dato es porque estamos editando el contenido 
		 */
		require('vars')[_var_scopekey]['id_contenido'] = args._dato.id;
		/** 
		 * Modificamos el titulo del header cuando estamos editando contenido 
		 */
		var NUEVO_CONTENIDO_titulo = 'EDITAR CONTENIDO';

		var setTitle2 = function(valor) {
			if (OS_ANDROID) {
				abx.title = valor;
			} else {
				$.NUEVO_CONTENIDO_window.setTitle(valor);
			}
		};
		var getTitle2 = function() {
			if (OS_ANDROID) {
				return abx.title;
			} else {
				return $.NUEVO_CONTENIDO_window.getTitle();
			}
		};
		setTitle2(NUEVO_CONTENIDO_titulo);

		/** 
		 * Modificamos estilo del selector de fecha 
		 */
		var DDMMYYYY_estilo = 'estilo12';

		var _tmp_a4w = require('a4w');
		if ((typeof DDMMYYYY_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (DDMMYYYY_estilo in _tmp_a4w.styles['classes'])) {
			try {
				DDMMYYYY_estilo = _tmp_a4w.styles['classes'][DDMMYYYY_estilo];
			} catch (st_val_err) {}
		}
		_tmp_a4w = null;
		$.DDMMYYYY.applyProperties(DDMMYYYY_estilo);

		/** 
		 * heredamos datos previos de nivel en modelo nuevo con binding de esta pantalla 
		 */
		var consultarModelo15_i = Alloy.createCollection('insp_contenido');
		var consultarModelo15_i_where = 'id=\'' + args._dato.id + '\'';
		consultarModelo15_i.fetch({
			query: 'SELECT * FROM insp_contenido WHERE id=\'' + args._dato.id + '\''
		});
		var insp_c = require('helper').query2array(consultarModelo15_i);
		if (Ti.App.deployType != 'production') console.log('detalles del contenido', {
			"datos": insp_c[0]
		});
		/** 
		 * Seteamos los valor del modelo contenido con los datos del contenido a editar 
		 */
		$.contenido.set({
			id_moneda: insp_c[0].id_moneda,
			fecha_compra: insp_c[0].fecha_compra,
			id_inspeccion: insp_c[0].id_inspeccion,
			nombre: insp_c[0].nombre,
			cantidad: insp_c[0].cantidad,
			foto1: insp_c[0].foto1,
			id_nombre: insp_c[0].id_nombre,
			foto2: insp_c[0].foto2,
			id_recinto: insp_c[0].id_recinto,
			id_marca: insp_c[0].id_marca,
			foto3: insp_c[0].foto3,
			recupero: insp_c[0].recupero,
			valor: insp_c[0].valor,
			descripcion: insp_c[0].descripcion
		});
		if ('contenido' in $) $contenido = $.contenido.toJSON();
		/** 
		 * Formateamos fecha con formato Unix a formato &quot;DD-MM-YYYY&quot; y la asignamos a la variable fecha_compra 
		 */
		var moment = require('alloy/moment');
		var formatearFecha2 = insp_c[0].fecha_compra;
		if (typeof formatearFecha2 === 'string' || typeof formatearFecha2 === 'number') {
			var fecha_compra = moment(formatearFecha2, '"X"').format('DD-MM-YYYY');
		} else {
			var fecha_compra = moment(formatearFecha2).format('DD-MM-YYYY');
		}
		if (Ti.App.deployType != 'production') console.log('FECHA RECIBIDA', {
			"fecha": fecha_compra
		});
		/** 
		 * Modifica el valor del texto de la fecha 
		 */
		$.DDMMYYYY.setText(fecha_compra);

		/** 
		 * Modificamos valor del texto de cantidad de items 
		 */
		$.campo.setValue(insp_c[0].cantidad);

		/** 
		 * Modificamos valor del texto de valor del objeto 
		 */
		$.campo2.setValue(insp_c[0].valor);

		/** 
		 * Modificamos valor de texto del campo descripcion de contenido 
		 */
		$.area.setValue(insp_c[0].descripcion);

		if (insp_c[0].recupero == 1 || insp_c[0].recupero == '1') {
			/** 
			 * Si switch recupero material esta encendido se modifica el valor y texto del switch recupero material 
			 */
			$.ID_1568773770.setValue(true);

			$.NO.setText('SI');

		} else {
			$.ID_1568773770.setValue('false');

			$.NO.setText('NO');

		}
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			/** 
			 * Verificamos si las fotos estan en la carpeta de inspeccion o es un dummy 
			 */
			if (Ti.App.deployType != 'production') console.log('es parte de la compilacion inspeccion completa', {});
			var ID_1218924586_trycatch = {
				error: function(e) {
					if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
				}
			};
			try {
				ID_1218924586_trycatch.error = function(evento) {};
				var foto1 = '';
				var ID_1743680972_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'mini' + insp_c[0].id_inspeccion);
				if (ID_1743680972_d.exists() == true) {
					var ID_1743680972_f = Ti.Filesystem.getFile(ID_1743680972_d.resolve(), insp_c[0].foto1);
					if (ID_1743680972_f.exists() == true) {
						foto1 = ID_1743680972_f.read();
					}
					ID_1743680972_f = null;
				}
				ID_1743680972_d = null;
			} catch (e) {
				ID_1218924586_trycatch.error(e);
			}
			var ID_1324389329_trycatch = {
				error: function(e) {
					if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
				}
			};
			try {
				ID_1324389329_trycatch.error = function(evento) {};
				var foto2 = '';
				var ID_1116016580_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'mini' + insp_c[0].id_inspeccion);
				if (ID_1116016580_d.exists() == true) {
					var ID_1116016580_f = Ti.Filesystem.getFile(ID_1116016580_d.resolve(), insp_c[0].foto2);
					if (ID_1116016580_f.exists() == true) {
						foto2 = ID_1116016580_f.read();
					}
					ID_1116016580_f = null;
				}
				ID_1116016580_d = null;
			} catch (e) {
				ID_1324389329_trycatch.error(e);
			}
			var ID_55032749_trycatch = {
				error: function(e) {
					if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
				}
			};
			try {
				ID_55032749_trycatch.error = function(evento) {};
				var foto3 = '';
				var ID_1411782814_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'mini' + insp_c[0].id_inspeccion);
				if (ID_1411782814_d.exists() == true) {
					var ID_1411782814_f = Ti.Filesystem.getFile(ID_1411782814_d.resolve(), insp_c[0].foto3);
					if (ID_1411782814_f.exists() == true) {
						foto3 = ID_1411782814_f.read();
					}
					ID_1411782814_f = null;
				}
				ID_1411782814_d = null;
			} catch (e) {
				ID_55032749_trycatch.error(e);
			}
		} else {
			if (Ti.App.deployType != 'production') console.log('esta compilacion esta sola, simulamos carpetas', {});
			var ID_547629173_trycatch = {
				error: function(e) {
					if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
				}
			};
			try {
				ID_547629173_trycatch.error = function(evento) {};
				var foto1 = '';
				var ID_315896947_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'mini' + 0);
				if (ID_315896947_d.exists() == true) {
					var ID_315896947_f = Ti.Filesystem.getFile(ID_315896947_d.resolve(), insp_c[0].foto1);
					if (ID_315896947_f.exists() == true) {
						foto1 = ID_315896947_f.read();
					}
					ID_315896947_f = null;
				}
				ID_315896947_d = null;
			} catch (e) {
				ID_547629173_trycatch.error(e);
			}
			var ID_1616909749_trycatch = {
				error: function(e) {
					if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
				}
			};
			try {
				ID_1616909749_trycatch.error = function(evento) {};
				var foto2 = '';
				var ID_33999265_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'mini' + 0);
				if (ID_33999265_d.exists() == true) {
					var ID_33999265_f = Ti.Filesystem.getFile(ID_33999265_d.resolve(), insp_c[0].foto2);
					if (ID_33999265_f.exists() == true) {
						foto2 = ID_33999265_f.read();
					}
					ID_33999265_f = null;
				}
				ID_33999265_d = null;
			} catch (e) {
				ID_1616909749_trycatch.error(e);
			}
			var ID_1025504743_trycatch = {
				error: function(e) {
					if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
				}
			};
			try {
				ID_1025504743_trycatch.error = function(evento) {};
				var foto3 = '';
				var ID_4931431_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'mini' + 0);
				if (ID_4931431_d.exists() == true) {
					var ID_4931431_f = Ti.Filesystem.getFile(ID_4931431_d.resolve(), insp_c[0].foto3);
					if (ID_4931431_f.exists() == true) {
						foto3 = ID_4931431_f.read();
					}
					ID_4931431_f = null;
				}
				ID_4931431_d = null;
			} catch (e) {
				ID_1025504743_trycatch.error(e);
			}
		}
		if ((_.isObject(foto1) || (_.isString(foto1)) && !_.isEmpty(foto1)) || _.isNumber(foto1) || _.isBoolean(foto1)) {
			/** 
			 * Revisamos si las fotos existen, y modificamos la imagen en la vista para cargar la que esta en la memoria del equipo 
			 */
			if (Ti.App.deployType != 'production') console.log('llamamos evento mini', {});
			$.widgetFotochica.mini({
				mini: foto1
			});
		}
		if ((_.isObject(foto2) || (_.isString(foto2)) && !_.isEmpty(foto2)) || _.isNumber(foto2) || _.isBoolean(foto2)) {
			if (Ti.App.deployType != 'production') console.log('llamamos evento mini', {});
			$.widgetFotochica2.mini({
				mini: foto2
			});
		}
		if ((_.isObject(foto3) || (_.isString(foto3)) && !_.isEmpty(foto3)) || _.isNumber(foto3) || _.isBoolean(foto3)) {
			if (Ti.App.deployType != 'production') console.log('llamamos evento mini', {});
			$.widgetFotochica3.mini({
				mini: foto3
			});
		}
	}
})();