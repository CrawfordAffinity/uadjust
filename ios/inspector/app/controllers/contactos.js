var _bind4section = {};
var _list_templates = {};

var _activity;
if (OS_ANDROID) {
	_activity = $.ENROLAMIENTO6.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'ENROLAMIENTO6';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.ENROLAMIENTO6.addEventListener('open', function(e) {
		abx.setStatusbarColor("#000000");
		abx.setBackgroundColor("white");
	});
}
$.ENROLAMIENTO6.orientationModes = [Titanium.UI.PORTRAIT];

function Click_vista44(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.ENROLAMIENTO6.close();

}

$.widgetHeader6.init({
	titulo: L('x1291961897_traducir', 'PARTE 5: Contactos'),
	__id: 'ALL1741978890',
	avance: L('x4674461_traducir', '5/6'),
	onclick: Click_widgetHeader6
});

function Click_widgetHeader6(e) {

	var evento = e;
	$.IngreseCorreo.blur();
	$.RepitasuCorreo.blur();
	$.IngresesuTelfono.blur();
	$.RepitasuTelfono.blur();

}

$.widgetBotonlargo6.init({
	titulo: L('x1524107289_traducir', 'CONTINUAR'),
	__id: 'ALL23912785',
	onclick: Click_widgetBotonlargo6
});

function Click_widgetBotonlargo6(e) {

	var evento = e;
	/** 
	 * Obtenemos los valores de los campos de texto 
	 */
	var correo;
	correo = $.IngreseCorreo.getValue();

	var correo_aux;
	correo_aux = $.RepitasuCorreo.getValue();

	var telefono;
	telefono = $.IngresesuTelfono.getValue();

	var telefono_aux;
	telefono_aux = $.RepitasuTelfono.getValue();

	if ((_.isObject(correo) || _.isString(correo)) && _.isEmpty(correo)) {
		/** 
		 * Validamos que los datos ingresados existan, esten correctos, y que coincidan 
		 */
		var preguntarAlerta16_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta16 = Ti.UI.createAlertDialog({
			title: L('x2185084353_traducir', 'Atención'),
			message: L('x971706488_traducir', 'Ingrese correo electrónico'),
			buttonNames: preguntarAlerta16_opts
		});
		preguntarAlerta16.addEventListener('click', function(e) {
			var suu = preguntarAlerta16_opts[e.index];
			suu = null;

			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta16.show();
	} else if (!(_.isString(correo) && /\S+@\S+\.\S+/.test(correo))) {
		var preguntarAlerta17_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta17 = Ti.UI.createAlertDialog({
			title: L('x2185084353_traducir', 'Atención'),
			message: L('x971706488_traducir', 'Ingrese correo electrónico'),
			buttonNames: preguntarAlerta17_opts
		});
		preguntarAlerta17.addEventListener('click', function(e) {
			var suu = preguntarAlerta17_opts[e.index];
			suu = null;

			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta17.show();
	} else if (correo != correo_aux) {
		var preguntarAlerta18_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta18 = Ti.UI.createAlertDialog({
			title: L('x2185084353_traducir', 'Atención'),
			message: L('x2875543814_traducir', 'Verifique que los correos electrónicos coincidan'),
			buttonNames: preguntarAlerta18_opts
		});
		preguntarAlerta18.addEventListener('click', function(e) {
			var suu = preguntarAlerta18_opts[e.index];
			suu = null;

			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta18.show();
	} else if ((_.isObject(telefono) || _.isString(telefono)) && _.isEmpty(telefono)) {
		var preguntarAlerta19_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta19 = Ti.UI.createAlertDialog({
			title: L('x2185084353_traducir', 'Atención'),
			message: L('x3389757436_traducir', 'Ingrese número de teléfono'),
			buttonNames: preguntarAlerta19_opts
		});
		preguntarAlerta19.addEventListener('click', function(e) {
			var suu = preguntarAlerta19_opts[e.index];
			suu = null;

			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta19.show();
	} else if (_.isNumber((telefono.length)) && _.isNumber(3) && (telefono.length) <= 3) {
		var preguntarAlerta20_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta20 = Ti.UI.createAlertDialog({
			title: L('x2185084353_traducir', 'Atención'),
			message: L('x269948556_traducir', 'Ingrese número de teléfono de contacto'),
			buttonNames: preguntarAlerta20_opts
		});
		preguntarAlerta20.addEventListener('click', function(e) {
			var suu = preguntarAlerta20_opts[e.index];
			suu = null;

			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta20.show();
	} else if (telefono != telefono_aux) {
		var preguntarAlerta21_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta21 = Ti.UI.createAlertDialog({
			title: L('x2185084353_traducir', 'Atención'),
			message: L('x1479672865_traducir', 'Verifique que los teléfonos coincidan'),
			buttonNames: preguntarAlerta21_opts
		});
		preguntarAlerta21.addEventListener('click', function(e) {
			var suu = preguntarAlerta21_opts[e.index];
			suu = null;

			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta21.show();
	} else {
		/** 
		 * Recuperamos variable, ingresamos los valores de contacto y guardamos en la variable 
		 */
		var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
		var registro = _.extend(registro, {
			correo: correo,
			telefono: telefono
		});
		require('vars')['registro'] = registro;
		/** 
		 * Enviamos a pantalla de oficio 
		 */
		if ("ENROLAMIENTO" in Alloy.Globals) {
			Alloy.Globals["ENROLAMIENTO"].openWindow(Alloy.createController("oficio", {}).getView());
		} else {
			Alloy.Globals["ENROLAMIENTO"] = $.ENROLAMIENTO;
			Alloy.Globals["ENROLAMIENTO"].openWindow(Alloy.createController("oficio", {}).getView());
		}

	}
}

(function() {
	/** 
	 * Creamos evento que al cerrar desde la ultima pantalla, esta tambi&#233;n se cierre. Evitamos el uso excesivo de memoria ram 
	 */

	_my_events['_close_enrolamiento,ID_1570225866'] = function(evento) {
		if (Ti.App.deployType != 'production') console.log('escuchando cerrar enrolamiento contactos', {});
		$.ENROLAMIENTO6.close();
	};
	Alloy.Events.on('_close_enrolamiento', _my_events['_close_enrolamiento,ID_1570225866']);
})();

if (OS_IOS || OS_ANDROID) {
	$.ENROLAMIENTO6.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}