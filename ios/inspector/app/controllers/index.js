var _bind4section = {};
var _list_templates = {
	"contenido": {
		"Label2": {
			"text": "{id}"
		},
		"vista4": {},
		"Label": {
			"text": "{nombre}"
		}
	},
	"recinto": {
		"Label2": {
			"text": "{id}"
		},
		"vista4": {},
		"Label": {
			"text": "{nombre}"
		}
	},
	"nivel": {
		"Label2": {
			"text": "{id}"
		},
		"vista20": {},
		"Label": {
			"text": "{nombre}"
		}
	},
	"tareas_mistareas": {
		"Label2": {
			"text": "{comuna}"
		},
		"vista5": {},
		"vista7": {},
		"vista3": {},
		"Adistancia": {
			"text": "a {distancia} km"
		},
		"vista9": {},
		"imagen": {},
		"vista11": {},
		"vista13": {
			"visible": "{seguirvisible}"
		},
		"Label": {
			"text": "{direccion}"
		},
		"vista4": {
			"idlocal": "{idlocal}"
		},
		"vista10": {},
		"Label3": {
			"text": "{ciudad}, {pais}"
		},
		"vista12": {},
		"vista6": {},
		"vista8": {}
	},
	"tareas": {
		"vista7": {},
		"Label3": {
			"text": "{direcciontarea}"
		},
		"vista8": {},
		"vista5": {},
		"Label5": {
			"text": "{ciudadtarea}, {paistarea} "
		},
		"vista4": {},
		"vista10": {},
		"Label4": {
			"text": "{comunatarea}"
		},
		"Aubicaciontarea": {
			"text": "a {ubicaciontarea} km"
		},
		"vista9": {},
		"vista6": {},
		"vista11": {},
		"vista3": {
			"myid": "{myid}"
		}
	},
	"criticas": {
		"vista15": {},
		"Label8": {
			"text": "{ciudadcritica}, {paiscritica}"
		},
		"vista14": {},
		"vista18": {},
		"Label6": {
			"text": "{direccioncritica}"
		},
		"vista20": {},
		"vista17": {},
		"Label7": {
			"text": "{comunacritica}"
		},
		"vista16": {},
		"vista19": {},
		"Aubicacioncritica": {
			"text": "a {ubicacioncritica} km"
		},
		"vista12": {
			"myid": "{myid}"
		},
		"vista13": {}
	},
	"tarea": {
		"vista86": {},
		"Label7": {
			"text": "{direccion}"
		},
		"vista97": {},
		"vista92": {},
		"vista107": {
			"visible": "{seguir}"
		},
		"vista93": {},
		"Adistance4": {
			"text": "a {distance} km"
		},
		"vista106": {},
		"vista102": {},
		"Label3": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"Label11": {
			"text": "{comuna}"
		},
		"Label21": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"Adistance7": {
			"text": "a {distance} km"
		},
		"vista38": {},
		"vista41": {},
		"vista55": {},
		"vista47": {},
		"vista19": {},
		"vista103": {},
		"vista48": {},
		"vista65": {},
		"vista7": {},
		"Label30": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"Label5": {
			"text": "{comuna}"
		},
		"vista64": {},
		"vista61": {},
		"Label14": {
			"text": "{comuna}"
		},
		"vista16": {},
		"vista15": {},
		"vista72": {
			"backgroundColor": "{bgcolor}"
		},
		"vista80": {
			"visible": "{vis_tipo9}"
		},
		"vista83": {},
		"Label23": {
			"text": "{comuna}"
		},
		"Label13": {
			"text": "{direccion}"
		},
		"vista90": {
			"backgroundColor": "{bgcolor}"
		},
		"Label33": {
			"text": "{comuna}"
		},
		"vista57": {},
		"Label8": {
			"text": "{comuna}"
		},
		"vista85": {},
		"vista68": {},
		"vista53": {
			"visible": "{vis_tipo6}"
		},
		"Label28": {
			"text": "{direccion}"
		},
		"vista76": {},
		"vista34": {},
		"vista37": {},
		"Label16": {
			"text": "{direccion}"
		},
		"vista25": {},
		"vista49": {},
		"Adistance6": {
			"text": "a {distance} km"
		},
		"Adistance": {
			"text": "a {distance} km"
		},
		"Adistance11": {
			"text": "a {distance} km"
		},
		"vista13": {},
		"vista27": {
			"backgroundColor": "{bgcolor}"
		},
		"vista79": {},
		"vista73": {},
		"Label34": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"Label35": {
			"text": "{id}"
		},
		"vista60": {},
		"vista82": {},
		"vista35": {
			"visible": "{vis_tipo4}"
		},
		"Label": {
			"text": "{direccion}"
		},
		"vista87": {},
		"vista67": {},
		"vista33": {},
		"vista70": {},
		"Label10": {
			"text": "{direccion}"
		},
		"Label12": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"vista96": {},
		"vista22": {},
		"Label24": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"vista52": {},
		"vista28": {},
		"vista99": {
			"backgroundColor": "{bgcolor}"
		},
		"vista84": {},
		"vista30": {},
		"vista78": {},
		"vista45": {
			"backgroundColor": "{bgcolor}"
		},
		"Label9": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"vista63": {
			"backgroundColor": "{bgcolor}"
		},
		"Label20": {
			"text": "{comuna}"
		},
		"Label29": {
			"text": "{comuna}"
		},
		"vista12": {},
		"Label18": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"vista29": {},
		"vista43": {},
		"vista105": {},
		"Label27": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"vista9": {
			"backgroundColor": "{bgcolor}"
		},
		"vista46": {},
		"Label22": {
			"text": "{direccion}"
		},
		"vista39": {},
		"Label26": {
			"text": "{comuna}"
		},
		"vista14": {},
		"Adistance9": {
			"text": "a {distance} km"
		},
		"vista58": {},
		"vista21": {},
		"vista24": {},
		"vista66": {},
		"vista89": {
			"visible": "{vis_tipo10}"
		},
		"vista23": {},
		"Label2": {
			"text": "{comuna}"
		},
		"vista88": {},
		"vista40": {},
		"vista74": {},
		"vista42": {},
		"vista94": {},
		"vista56": {},
		"vista91": {},
		"vista75": {},
		"vista71": {
			"visible": "{vis_tipo8}"
		},
		"Label15": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"vista95": {},
		"vista81": {
			"backgroundColor": "{bgcolor}"
		},
		"vista36": {
			"backgroundColor": "{bgcolor}"
		},
		"Label31": {
			"text": "{prioridad_tiempo}"
		},
		"vista98": {
			"visible": "{vis_otro}"
		},
		"vista10": {},
		"Label17": {
			"text": "{comuna}"
		},
		"vista100": {},
		"vista51": {},
		"vista44": {
			"visible": "{vis_tipo5}"
		},
		"Adistance8": {
			"text": "a {distance} km"
		},
		"imagen": {},
		"vista59": {},
		"Label4": {
			"text": "{direccion}"
		},
		"vista18": {
			"backgroundColor": "{bgcolor}"
		},
		"vista101": {},
		"vista31": {},
		"vista17": {
			"visible": "{vis_tipo2}"
		},
		"Label6": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"vista8": {
			"visible": "{vis_tipo1}"
		},
		"vista104": {},
		"vista11": {},
		"Adistance2": {
			"text": "a {distance} km"
		},
		"vista32": {},
		"vista69": {},
		"Adistance3": {
			"text": "a {distance} km"
		},
		"vista62": {
			"visible": "{vis_tipo7}"
		},
		"vista50": {},
		"Label25": {
			"text": "{direccion}"
		},
		"vista77": {},
		"vista54": {
			"backgroundColor": "{bgcolor}"
		},
		"Adistance5": {
			"text": "a {distance} km"
		},
		"Adistance10": {
			"text": "a {distance} km"
		},
		"vista20": {},
		"Label19": {
			"text": "{direccion}"
		},
		"Label32": {
			"text": "{direccion}"
		},
		"vista26": {
			"visible": "{vis_tipo3}"
		}
	},
	"tarea_historia": {
		"Label3": {
			"text": "{comuna}"
		},
		"vista6": {},
		"vista8": {},
		"vista4": {},
		"Label2": {
			"text": "{hora_termino}"
		},
		"vista10": {},
		"vista15": {
			"visible": "{bt_enviartarea}"
		},
		"vista12": {},
		"Label": {
			"text": "{direccion}"
		},
		"vista5": {
			"idlocal": "{id}",
			"estado": "{estado_tarea}"
		},
		"vista11": {},
		"vista14": {},
		"Label4": {
			"text": "{ciudad}, {pais}"
		},
		"vista16": {
			"visible": "{enviando_tarea}"
		},
		"vista13": {},
		"vista7": {},
		"ENVIAR": {},
		"imagen": {},
		"vista9": {}
	}
};

var _activity;
if (OS_ANDROID) {
	_activity = $.PORTADA.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'PORTADA';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.PORTADA.addEventListener('open', function(e) {
		abx.setStatusbarColor("#000000");
	});
}

function Load_imagen(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var evento = e;
	elemento.start();

}

function Click_vista7(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	/** 
	 * Creamos una estructura para usarla en el enrolamiento 
	 */
	var vacio = {};
	/** 
	 * Guardamos la variable registro para que sea de acceso permanente, (que no se pierda despues de una vez cerrado) 
	 */
	Ti.App.Properties.setString('registro', JSON.stringify(vacio));
	var mi_pais = ('mi_pais' in require('vars')) ? require('vars')['mi_pais'] : '';
	var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
	var ID_1674696970 = null;
	if ('ocultar_botones' in require('funciones')) {
		ID_1674696970 = require('funciones').ocultar_botones({});
	} else {
		try {
			ID_1674696970 = f_ocultar_botones({});
		} catch (ee) {}
	}
	$.StatusEsperando.setText('status: obteniendo info');

	var consultarURL = {};

	consultarURL.success = function(e) {
		var elemento = e,
			valor = e;
		if (elemento == false || elemento == 'false') {
			$.StatusEsperando.setText('status: esperando');

			var preguntarAlerta_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta = Ti.UI.createAlertDialog({
				title: L('x57652245_traducir', 'Alerta'),
				message: L('x1693828390_traducir', 'Problema de conexión por favor intentar después'),
				buttonNames: preguntarAlerta_opts
			});
			preguntarAlerta.addEventListener('click', function(e) {
				var suu = preguntarAlerta_opts[e.index];
				suu = null;

				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta.show();
		} else {
			/** 
			 * En el caso de obtener un resultado desde el servidor, limpiamos y cargamos las tablas 
			 */
			$.StatusEsperando.setText('status: cargando datos');

			var eliminarModelo_i = Alloy.Collections.pais;
			var sql = "DELETE FROM " + eliminarModelo_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo_i.trigger('remove');
			var eliminarModelo2_i = Alloy.Collections.nivel1;
			var sql = "DELETE FROM " + eliminarModelo2_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo2_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo2_i.trigger('remove');
			var eliminarModelo3_i = Alloy.Collections.experiencia_oficio;
			var sql = "DELETE FROM " + eliminarModelo3_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo3_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo3_i.trigger('remove');
			var elemento_regiones = elemento.regiones;
			var insertarModelo_m = Alloy.Collections.nivel1;
			var db_insertarModelo = Ti.Database.open(insertarModelo_m.config.adapter.db_name);
			db_insertarModelo.execute('BEGIN');
			_.each(elemento_regiones, function(insertarModelo_fila, pos) {
				db_insertarModelo.execute('INSERT INTO nivel1 (id_label, id_server, dif_horaria, id_pais) VALUES (?,?,?,?)', insertarModelo_fila.subdivision_name, insertarModelo_fila.id, 2, insertarModelo_fila.id_pais);
			});
			db_insertarModelo.execute('COMMIT');
			db_insertarModelo.close();
			db_insertarModelo = null;
			insertarModelo_m.trigger('change');
			var elemento_paises = elemento.paises;
			var insertarModelo2_m = Alloy.Collections.pais;
			var db_insertarModelo2 = Ti.Database.open(insertarModelo2_m.config.adapter.db_name);
			db_insertarModelo2.execute('BEGIN');
			_.each(elemento_paises, function(insertarModelo2_fila, pos) {
				db_insertarModelo2.execute('INSERT INTO pais (label_nivel2, moneda, nombre, label_nivel4, label_codigo_identificador, label_nivel3, id_server, label_nivel1, iso, sis_metrico, niveles_pais, id_pais, label_nivel5, lenguaje) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)', insertarModelo2_fila.nivel_2, insertarModelo2_fila.moneda, insertarModelo2_fila.nombre, insertarModelo2_fila.nivel_4, insertarModelo2_fila.label_codigo_identificador, insertarModelo2_fila.nivel_3, insertarModelo2_fila.id, insertarModelo2_fila.nivel_1, insertarModelo2_fila.iso, insertarModelo2_fila.sis_metrico, insertarModelo2_fila.niveles_pais, insertarModelo2_fila.idpais, insertarModelo2_fila.nivel_5, insertarModelo2_fila.lenguaje);
			});
			db_insertarModelo2.execute('COMMIT');
			db_insertarModelo2.close();
			db_insertarModelo2 = null;
			insertarModelo2_m.trigger('change');
			var elemento_experiencia_oficio = elemento.experiencia_oficio;
			var insertarModelo3_m = Alloy.Collections.experiencia_oficio;
			var db_insertarModelo3 = Ti.Database.open(insertarModelo3_m.config.adapter.db_name);
			db_insertarModelo3.execute('BEGIN');
			_.each(elemento_experiencia_oficio, function(insertarModelo3_fila, pos) {
				db_insertarModelo3.execute('INSERT INTO experiencia_oficio (nombre, id_server, id_pais) VALUES (?,?,?)', insertarModelo3_fila.nombre, insertarModelo3_fila.id, insertarModelo3_fila.idpais);
			});
			db_insertarModelo3.execute('COMMIT');
			db_insertarModelo3.close();
			db_insertarModelo3 = null;
			insertarModelo3_m.trigger('change');
			$.StatusEsperando.setText('status: esperando');

			/** 
			 * Y cargamos la primera pantalla del enrolamiento 
			 */
			Alloy.createController("registro_index", {}).getView().open();
		}
		var ID_1865449898 = null;
		if ('mostrar_botones' in require('funciones')) {
			ID_1865449898 = require('funciones').mostrar_botones({});
		} else {
			try {
				ID_1865449898 = f_mostrar_botones({});
			} catch (ee) {}
		}
		elemento = null, valor = null;
	};

	consultarURL.error = function(e) {
		var elemento = e,
			valor = e;
		var preguntarAlerta2_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta2 = Ti.UI.createAlertDialog({
			title: L('x57652245_traducir', 'Alerta'),
			message: L('x1693828390_traducir', 'Problema de conexión por favor intentar después'),
			buttonNames: preguntarAlerta2_opts
		});
		preguntarAlerta2.addEventListener('click', function(e) {
			var suu = preguntarAlerta2_opts[e.index];
			suu = null;

			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta2.show();
		var ID_758659505 = null;
		if ('mostrar_botones' in require('funciones')) {
			ID_758659505 = require('funciones').mostrar_botones({});
		} else {
			try {
				ID_758659505 = f_mostrar_botones({});
			} catch (ee) {}
		}
		elemento = null, valor = null;
	};
	require('helper').ajaxUnico('consultarURL', '' + String.format(L('x2963862531', '%1$sobtenerPais'), url_server.toString()) + '', 'POST', {}, 15000, consultarURL);

}

$.widgetBotonlargo.init({
	titulo: L('x1321529571_traducir', 'ENTRAR'),
	__id: 'ALL337746182',
	color: 'amarillo',
	onclick: Click_widgetBotonlargo
});

function Click_widgetBotonlargo(e) {

	var evento = e;
	if (Ti.App.deployType != 'production') console.log('LLamando al servidor por login', {});
	var correo;
	correo = $.IngreseUsuario.getValue();

	var password;
	password = $.IngreseContrasea.getValue();

	if ((_.isObject(correo) || _.isString(correo)) && _.isEmpty(correo)) {
		var preguntarAlerta3_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta3 = Ti.UI.createAlertDialog({
			title: L('x57652245_traducir', 'Alerta'),
			message: L('x2435587168_traducir', 'Debe ingresar su nombre de usuario'),
			buttonNames: preguntarAlerta3_opts
		});
		preguntarAlerta3.addEventListener('click', function(e) {
			var x = preguntarAlerta3_opts[e.index];
			$.IngreseUsuario.focus();
			x = null;

			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta3.show();
	} else if ((_.isObject(password) || _.isString(password)) && _.isEmpty(password)) {
		var preguntarAlerta4_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta4 = Ti.UI.createAlertDialog({
			title: L('x57652245_traducir', 'Alerta'),
			message: L('x2082354442_traducir', 'Debe ingresar su clave'),
			buttonNames: preguntarAlerta4_opts
		});
		preguntarAlerta4.addEventListener('click', function(e) {
			var x = preguntarAlerta4_opts[e.index];
			$.IngreseContrasea.focus();
			x = null;

			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta4.show();
	} else {
		/** 
		 * Verificamos que los campos de email y password no esten vacios, en caso que haya datos, proseguimos con la carga de paises 
		 */
		var ID_1791823265 = null;
		if ('ocultar_botones' in require('funciones')) {
			ID_1791823265 = require('funciones').ocultar_botones({});
		} else {
			try {
				ID_1791823265 = f_ocultar_botones({});
			} catch (ee) {}
		}
		require('vars')['correo'] = correo;
		require('vars')['password'] = password;
		$.IngreseUsuario.blur();
		$.IngreseContrasea.blur();
		if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
			var ID_988851127 = null;
			if ('cargar_paises' in require('funciones')) {
				ID_988851127 = require('funciones').cargar_paises({});
			} else {
				try {
					ID_988851127 = f_cargar_paises({});
				} catch (ee) {}
			}
		} else {
			var ID_1947630548 = null;
			if ('mostrar_botones' in require('funciones')) {
				ID_1947630548 = require('funciones').mostrar_botones({});
			} else {
				try {
					ID_1947630548 = f_mostrar_botones({});
				} catch (ee) {}
			}
			var preguntarAlerta5_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta5 = Ti.UI.createAlertDialog({
				title: L('x57652245_traducir', 'Alerta'),
				message: L('x954934616_traducir', 'Problema de conexion por favor intentar despues'),
				buttonNames: preguntarAlerta5_opts
			});
			preguntarAlerta5.addEventListener('click', function(e) {
				var suu = preguntarAlerta5_opts[e.index];
				suu = null;

				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta5.show();
		}
	}
}

/** 
 * Modificamos el ancho de la vista del registro para que se ajuste a su contenido y no tome la totalidad 
 */
var vista7_ancho = '-';

if (vista7_ancho == '*') {
	vista7_ancho = Ti.UI.FILL;
} else if (vista7_ancho == '-') {
	vista7_ancho = Ti.UI.SIZE;
} else if (!isNaN(vista7_ancho)) {
	vista7_ancho = vista7_ancho + 'dp';
}
$.vista7.setWidth(vista7_ancho);

/** 
 * Definimos en una variable el lenguaje que tiene el telefono 
 */
var pais_codigo = Titanium.Locale.currentCountry;
/** 
 * Guardamos en una variable global el codigo de pais que tiene el telefono 
 */
require('vars')['mi_pais'] = pais_codigo;
/** 
 * Recuperamos la url del servidor de uadjust 
 */
var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
/** 
 * Partimos sin inspeccion desde login 
 */
require('vars')['inspeccion_encurso'] = L('x734881840_traducir', 'false');
/** 
 * Obtenemos una primera ubicaci&#243;n para el login 
 */
var _ifunc_res_obtenerGPS = function(e) {
	if (e.error) {
		var geopos = {
			error: true,
			latitude: -1,
			longitude: -1,
			reason: (e.reason) ? e.reason : 'unknown',
			accuracy: 0,
			speed: 0,
			error_compass: false
		};
	} else {
		var geopos = e;
	}
	if (geopos.error == false || geopos.error == 'false') {
		/** 
		 * Revisamos que no haya error al obtener la ubicacion 
		 */
		require('vars')['gps_error'] = L('x734881840_traducir', 'false');
		require('vars')['gps_latitud'] = geopos.latitude;
		require('vars')['gps_longitud'] = geopos.longitude;
	}
};
Ti.Geolocation.setAccuracy(Ti.Geolocation.ACCURACY_HIGH);
if (Ti.Geolocation.hasLocationPermissions(Ti.Geolocation.AUTHORIZATION_WHEN_IN_USE)) {
	Ti.Geolocation.getCurrentPosition(function(ee) {
		if (Ti.Geolocation.getHasCompass() == false) {
			var pgeopos = ('coords' in ee) ? ee.coords : {};
			pgeopos.error = ('coords' in ee) ? false : true;
			pgeopos.reason = '';
			pgeopos.speed = 0;
			pgeopos.compass = -1;
			pgeopos.compass_accuracy = -1;
			pgeopos.error_compass = true;
			_ifunc_res_obtenerGPS(pgeopos);
		} else {
			Ti.Geolocation.getCurrentHeading(function(yy) {
				var pgeopos = ('coords' in ee) ? ee.coords : {};
				pgeopos.error = ('coords' in ee) ? false : true;
				pgeopos.reason = ('error' in ee) ? ee.error : '';
				if (yy.error) {
					pgeopos.error_compass = true;
				} else {
					pgeopos.compass = yy.heading;
					pgeopos.compass_accuracy = yy.heading.accuracy;
				}
				_ifunc_res_obtenerGPS(pgeopos);
			});
		}
	});
} else {
	Ti.Geolocation.requestLocationPermissions(Ti.Geolocation.AUTHORIZATION_WHEN_IN_USE, function(u) {
		if (u.success) {
			Ti.Geolocation.getCurrentPosition(function(ee) {
				if (Ti.Geolocation.getHasCompass() == false) {
					var pgeopos = ('coords' in ee) ? ee.coords : {};
					pgeopos.error = ('coords' in ee) ? false : true;
					pgeopos.reason = '';
					pgeopos.speed = 0;
					pgeopos.compass = -1;
					pgeopos.compass_accuracy = -1;
					pgeopos.error_compass = true;
					_ifunc_res_obtenerGPS(pgeopos);
				} else {
					Ti.Geolocation.getCurrentHeading(function(yy) {
						var pgeopos = ('coords' in ee) ? ee.coords : {};
						pgeopos.error = ('coords' in ee) ? false : true;
						pgeopos.reason = '';
						if (yy.error) {
							pgeopos.error_compass = true;
						} else {
							pgeopos.compass = yy.heading;
							pgeopos.compass_accuracy = yy.heading.accuracy;
						}
						_ifunc_res_obtenerGPS(pgeopos);
					});
				}
			});
		} else {
			_ifunc_res_obtenerGPS({
				error: true,
				latitude: -1,
				longitude: -1,
				reason: 'permission_denied',
				accuracy: 0,
				speed: 0,
				error_compass: false
			});
		}
	});
}
var f_cargar_paises = function(x_params) {
	var item = x_params['item'];
	$.StatusEsperando.setText('status: cargando paises');

	/** 
	 * Hacemos una consulta al servidor para obtener los paises 
	 */
	var consultarURL2 = {};

	consultarURL2.success = function(e) {
		var elemento = e,
			valor = e;
		if (_.isObject(elemento)) {
			if (elemento.error == 0 || elemento.error == '0') {
				/** 
				 * En el caso que el servidor retorne un resultado 0 es porque esta todo bien, caso contrario, se mostrara mensaje de error y vuelve a mostrar los errores 
				 */
				if (Ti.App.deployType != 'production') console.log('registrando paises', {});
				/** 
				 * Si no hubo problema al obtener los paises, limpiamos los modelos y cargamos los datos 
				 */
				/** 
				 * Si no hubo problema al obtener los paises, limpiamos los modelos y cargamos los datos 
				 */

				var eliminarModelo4_i = Alloy.Collections.pais;
				var sql = "DELETE FROM " + eliminarModelo4_i.config.adapter.collection_name;
				var db = Ti.Database.open(eliminarModelo4_i.config.adapter.db_name);
				db.execute(sql);
				db.close();
				eliminarModelo4_i.trigger('remove');
				var eliminarModelo5_i = Alloy.Collections.nivel1;
				var sql = "DELETE FROM " + eliminarModelo5_i.config.adapter.collection_name;
				var db = Ti.Database.open(eliminarModelo5_i.config.adapter.db_name);
				db.execute(sql);
				db.close();
				eliminarModelo5_i.trigger('remove');
				var eliminarModelo6_i = Alloy.Collections.experiencia_oficio;
				var sql = "DELETE FROM " + eliminarModelo6_i.config.adapter.collection_name;
				var db = Ti.Database.open(eliminarModelo6_i.config.adapter.db_name);
				db.execute(sql);
				db.close();
				eliminarModelo6_i.trigger('remove');
				var elemento_regiones = elemento.regiones;
				var insertarModelo4_m = Alloy.Collections.nivel1;
				var db_insertarModelo4 = Ti.Database.open(insertarModelo4_m.config.adapter.db_name);
				db_insertarModelo4.execute('BEGIN');
				_.each(elemento_regiones, function(insertarModelo4_fila, pos) {
					db_insertarModelo4.execute('INSERT INTO nivel1 (id_label, id_server, dif_horaria, id_pais) VALUES (?,?,?,?)', insertarModelo4_fila.subdivision_name, insertarModelo4_fila.id, 2, insertarModelo4_fila.id_pais);
				});
				db_insertarModelo4.execute('COMMIT');
				db_insertarModelo4.close();
				db_insertarModelo4 = null;
				insertarModelo4_m.trigger('change');
				var elemento_paises = elemento.paises;
				var insertarModelo5_m = Alloy.Collections.pais;
				var db_insertarModelo5 = Ti.Database.open(insertarModelo5_m.config.adapter.db_name);
				db_insertarModelo5.execute('BEGIN');
				_.each(elemento_paises, function(insertarModelo5_fila, pos) {
					db_insertarModelo5.execute('INSERT INTO pais (label_nivel2, nombre, label_nivel4, label_codigo_identificador, label_nivel3, id_server, label_nivel1, iso, sis_metrico, id_pais, label_nivel5, lenguaje) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)', insertarModelo5_fila.nivel_2, insertarModelo5_fila.nombre, insertarModelo5_fila.nivel_4, insertarModelo5_fila.label_codigo_identificador, insertarModelo5_fila.nivel_3, insertarModelo5_fila.id, insertarModelo5_fila.nivel_1, insertarModelo5_fila.iso, insertarModelo5_fila.sis_metrico, insertarModelo5_fila.idpais, insertarModelo5_fila.nivel_5, insertarModelo5_fila.lenguaje);
				});
				db_insertarModelo5.execute('COMMIT');
				db_insertarModelo5.close();
				db_insertarModelo5 = null;
				insertarModelo5_m.trigger('change');
				var elemento_experiencia_oficio = elemento.experiencia_oficio;
				var insertarModelo6_m = Alloy.Collections.experiencia_oficio;
				var db_insertarModelo6 = Ti.Database.open(insertarModelo6_m.config.adapter.db_name);
				db_insertarModelo6.execute('BEGIN');
				_.each(elemento_experiencia_oficio, function(insertarModelo6_fila, pos) {
					db_insertarModelo6.execute('INSERT INTO experiencia_oficio (nombre, id_server, id_pais) VALUES (?,?,?)', insertarModelo6_fila.nombre, insertarModelo6_fila.id, insertarModelo6_fila.idpais);
				});
				db_insertarModelo6.execute('COMMIT');
				db_insertarModelo6.close();
				db_insertarModelo6 = null;
				insertarModelo6_m.trigger('change');
				/** 
				 * Al terminar la carga de datos de la base de datos, llamamos una funcion para continuar con el proceso 
				 */
				var ID_203502002 = null;
				if ('llamar_login' in require('funciones')) {
					ID_203502002 = require('funciones').llamar_login({});
				} else {
					try {
						ID_203502002 = f_llamar_login({});
					} catch (ee) {}
				}
			} else {
				var ID_452613796 = null;
				if ('mostrar_botones' in require('funciones')) {
					ID_452613796 = require('funciones').mostrar_botones({});
				} else {
					try {
						ID_452613796 = f_mostrar_botones({});
					} catch (ee) {}
				}
				var preguntarAlerta6_opts = [L('x1518866076_traducir', 'Aceptar')];
				var preguntarAlerta6 = Ti.UI.createAlertDialog({
					title: L('x57652245_traducir', 'Alerta'),
					message: '' + String.format(L('x2630571488_traducir', 'Hubo un problema con el servidor (%1$s)'), elemento.error.toString()) + '',
					buttonNames: preguntarAlerta6_opts
				});
				preguntarAlerta6.addEventListener('click', function(e) {
					var suu = preguntarAlerta6_opts[e.index];
					suu = null;

					e.source.removeEventListener("click", arguments.callee);
				});
				preguntarAlerta6.show();
			}
		} else {
			var ID_790527593 = null;
			if ('mostrar_botones' in require('funciones')) {
				ID_790527593 = require('funciones').mostrar_botones({});
			} else {
				try {
					ID_790527593 = f_mostrar_botones({});
				} catch (ee) {}
			}
			var preguntarAlerta7_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta7 = Ti.UI.createAlertDialog({
				title: L('x57652245_traducir', 'Alerta'),
				message: L('x1364461137_traducir', 'Hubo un problema con el servidor'),
				buttonNames: preguntarAlerta7_opts
			});
			preguntarAlerta7.addEventListener('click', function(e) {
				var suu = preguntarAlerta7_opts[e.index];
				suu = null;

				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta7.show();
		}
		elemento = null, valor = null;
	};

	consultarURL2.error = function(e) {
		var elemento = e,
			valor = e;
		var ID_930600987 = null;
		if ('mostrar_botones' in require('funciones')) {
			ID_930600987 = require('funciones').mostrar_botones({});
		} else {
			try {
				ID_930600987 = f_mostrar_botones({});
			} catch (ee) {}
		}
		var preguntarAlerta8_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta8 = Ti.UI.createAlertDialog({
			title: L('x57652245_traducir', 'Alerta'),
			message: L('x954934616_traducir', 'Problema de conexion por favor intentar despues'),
			buttonNames: preguntarAlerta8_opts
		});
		preguntarAlerta8.addEventListener('click', function(e) {
			var suu = preguntarAlerta8_opts[e.index];
			suu = null;

			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta8.show();
		elemento = null, valor = null;
	};
	require('helper').ajaxUnico('consultarURL2', '' + String.format(L('x2963862531', '%1$sobtenerPais'), url_server.toString()) + '', 'POST', {}, 15000, consultarURL2);
	return null;
};
var f_llamar_login = function(x_params) {
	var item = x_params['item'];
	if (Ti.App.deployType != 'production') console.log('capturando ubicacion', {});
	$.StatusEsperando.setText('status: verificando credenciales');

	/** 
	 * Recuperamos la variable gps_error 
	 */
	var gps_error = ('gps_error' in require('vars')) ? require('vars')['gps_error'] : '';
	if (gps_error == true || gps_error == 'true') {
		/** 
		 * En el caso que gps_error sea verdadero, mostramos mensaje de problema. Caso contrario, seguimos con la validacion de usuario 
		 */
		var preguntarAlerta9_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta9 = Ti.UI.createAlertDialog({
			title: L('x57652245_traducir', 'Alerta'),
			message: L('x3873304135_traducir', 'No se pudo obtener la ubicación'),
			buttonNames: preguntarAlerta9_opts
		});
		preguntarAlerta9.addEventListener('click', function(e) {
			var errori = preguntarAlerta9_opts[e.index];
			errori = null;

		});
		preguntarAlerta9.show();
		var ID_1353705667 = null;
		if ('mostrar_botones' in require('funciones')) {
			ID_1353705667 = require('funciones').mostrar_botones({});
		} else {
			try {
				ID_1353705667 = f_mostrar_botones({});
			} catch (ee) {}
		}
	} else {
		var correo = ('correo' in require('vars')) ? require('vars')['correo'] : '';
		var password = ('password' in require('vars')) ? require('vars')['password'] : '';
		var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
		/** 
		 * La variable devicetoken es el token obtenido cuando se solicita obtener el push 
		 */
		var devicetoken = ('devicetoken' in require('vars')) ? require('vars')['devicetoken'] : '';
		if ((_.isObject(devicetoken) || _.isString(devicetoken)) && _.isEmpty(devicetoken)) {
			/** 
			 * Se aplica cuando se compila en iOS, ya que los push no funcionan en simuladores 
			 */
			require('vars')['devicetoken'] = L('x1803495209_traducir', 'emulador-iphone6-creador');
			Ti.App.Properties.setString('devicetoken', JSON.stringify('emulador-iphone6-creador'));
			var devicetoken = ('devicetoken' in require('vars')) ? require('vars')['devicetoken'] : '';
		}
		if (Ti.App.deployType != 'production') console.log('llamando servicio login', {});
		/** 
		 * Obtenemos el id unico de cada equipo 
		 */
		var uidd = Ti.Platform.id;
		var gps_latitud = ('gps_latitud' in require('vars')) ? require('vars')['gps_latitud'] : '';
		var gps_longitud = ('gps_longitud' in require('vars')) ? require('vars')['gps_longitud'] : '';
		/** 
		 * Obtenemos la zona horaria 
		 */
		var reloj_celular = null;
		if ('formatear_fecha' in require('funciones')) {
			reloj_celular = require('funciones').formatear_fecha({
				'fecha': new Date(),
				'formato': L('x2318606283_traducir', 'ZZ')
			});
		} else {
			try {
				reloj_celular = f_formatear_fecha({
					'fecha': new Date(),
					'formato': L('x2318606283_traducir', 'ZZ')
				});
			} catch (ee) {}
		}
		/** 
		 * Obtenemos la zona horaria del equipo 
		 */
		var dif = +reloj_celular * 0.01;
		if (Ti.App.deployType != 'production') console.log('psb dato para sincronizacion horaria (solo gmt)', {
			"fecha": dif
		});
		var consultarURL3 = {};
		console.log('DEBUG WEB: requesting url:' + String.format(L('x796226125', '%1$slogin'), url_server.toString()) + ' with data:', {
			_method: 'POST',
			_params: {
				correo: correo,
				password: password,
				device: Ti.Platform.name,
				lat: gps_latitud,
				lon: gps_longitud,
				device_token: devicetoken,
				uidd: uidd,
				fecha: dif
			},
			_timeout: '15000'
		});

		consultarURL3.success = function(e) {
			var elemento = e,
				valor = e;
			if (Ti.App.deployType != 'production') console.log('respuesta de servidor: login', {
				"elemento": elemento
			});
			if (elemento.error == 401) {
				/** 
				 * Manejamos los distintos tipos de mensaje de error que puede decir el servidor. 
				 */
				if (Ti.App.deployType != 'production') console.log('login error 401', {});
				var ID_1463949108 = null;
				if ('mostrar_botones' in require('funciones')) {
					ID_1463949108 = require('funciones').mostrar_botones({});
				} else {
					try {
						ID_1463949108 = f_mostrar_botones({});
					} catch (ee) {}
				}
				var preguntarAlerta10_opts = [L('x1518866076_traducir', 'Aceptar')];
				var preguntarAlerta10 = Ti.UI.createAlertDialog({
					title: L('x57652245_traducir', 'Alerta'),
					message: L('x2082395189_traducir', 'Clave o usuario incorrectos.'),
					buttonNames: preguntarAlerta10_opts
				});
				preguntarAlerta10.addEventListener('click', function(e) {
					var res = preguntarAlerta10_opts[e.index];
					res = null;

					e.source.removeEventListener("click", arguments.callee);
				});
				preguntarAlerta10.show();
			} else if (elemento.error == 402) {
				if (Ti.App.deployType != 'production') console.log('login error 402', {});
				var ID_1281427820 = null;
				if ('mostrar_botones' in require('funciones')) {
					ID_1281427820 = require('funciones').mostrar_botones({});
				} else {
					try {
						ID_1281427820 = f_mostrar_botones({});
					} catch (ee) {}
				}
				var preguntarAlerta11_opts = [L('x1518866076_traducir', 'Aceptar')];
				var preguntarAlerta11 = Ti.UI.createAlertDialog({
					title: L('x57652245_traducir', 'Alerta'),
					message: L('x2797729566_traducir', 'Usuario bloqueado'),
					buttonNames: preguntarAlerta11_opts
				});
				preguntarAlerta11.addEventListener('click', function(e) {
					var res = preguntarAlerta11_opts[e.index];
					res = null;

					e.source.removeEventListener("click", arguments.callee);
				});
				preguntarAlerta11.show();
			} else if (elemento.error == 0 || elemento.error == '0') {
				/** 
				 * En el caso que no exista error (O sea, error 0), procedemos con la limpieza y carga de datos en la base de datos 
				 */
				if (Ti.App.deployType != 'production') console.log('login error 0: todo bien, accediendo', {});
				if (Ti.App.deployType != 'production') console.log('limpiando tablas locales asociadas a login previo', {});
				var eliminarModelo7_i = Alloy.Collections.inspectores;
				var sql = "DELETE FROM " + eliminarModelo7_i.config.adapter.collection_name;
				var db = Ti.Database.open(eliminarModelo7_i.config.adapter.db_name);
				db.execute(sql);
				db.close();
				eliminarModelo7_i.trigger('remove');
				var eliminarModelo8_i = Alloy.Collections.tareas_entrantes;
				var sql = "DELETE FROM " + eliminarModelo8_i.config.adapter.collection_name;
				var db = Ti.Database.open(eliminarModelo8_i.config.adapter.db_name);
				db.execute(sql);
				db.close();
				eliminarModelo8_i.trigger('remove');
				var eliminarModelo9_i = Alloy.Collections.tareas;
				var sql = "DELETE FROM " + eliminarModelo9_i.config.adapter.collection_name;
				var db = Ti.Database.open(eliminarModelo9_i.config.adapter.db_name);
				db.execute(sql);
				db.close();
				eliminarModelo9_i.trigger('remove');
				var eliminarModelo10_i = Alloy.Collections.emergencia;
				var sql = "DELETE FROM " + eliminarModelo10_i.config.adapter.collection_name;
				var db = Ti.Database.open(eliminarModelo10_i.config.adapter.db_name);
				db.execute(sql);
				db.close();
				eliminarModelo10_i.trigger('remove');
				var eliminarModelo11_i = Alloy.Collections.historial_tareas;
				var sql = "DELETE FROM " + eliminarModelo11_i.config.adapter.collection_name;
				var db = Ti.Database.open(eliminarModelo11_i.config.adapter.db_name);
				db.execute(sql);
				db.close();
				eliminarModelo11_i.trigger('remove');
				/** 
				 * alias para simplificar campos de inspector. 
				 */
				inspector = elemento.inspector;
				var insertarModelo7_m = Alloy.Collections.inspectores;
				var insertarModelo7_fila = Alloy.createModel('inspectores', {
					apellido_materno: inspector.apellido_materno,
					id_nivel1: inspector.nivel_1,
					lat_dir: inspector.lat_dir,
					disponibilidad_viajar_pais: inspector.disponibilidad_viajar_pais,
					uuid: inspector.uidd,
					fecha_nacimiento: inspector.fecha_nacimiento,
					d1: inspector.d1,
					d2: inspector.d2,
					password: inspector.password,
					estado: inspector.estado,
					pais: inspector.pais,
					direccion: inspector.direccion,
					d3: inspector.d3,
					nivel3: inspector.nivel_3,
					d5: inspector.d5,
					d4: inspector.d4,
					disponibilidad_fechas: inspector.disponibilidad_fechas,
					d7: inspector.d7,
					nivel4: inspector.nivel_4,
					nombre: inspector.nombre,
					nivel5: inspector.nivel_5,
					disponibilidad_horas: inspector.disponibilidad_horas,
					nivel2: inspector.nivel_2,
					lon_dir: inspector.lon_dir,
					disponibilidad_viajar_ciudad: inspector.disponibilidad_viajar_ciudad,
					id_server: inspector.id,
					d6: inspector.d6,
					telefono: inspector.telefono,
					experiencia_detalle: inspector.experiencia_detalle,
					disponibilidad: inspector.disponibilidad,
					experiencia_oficio: inspector.id_experencia_oficio,
					codigo_identificador: inspector.codigo_identificador,
					apellido_paterno: inspector.apellido_paterno,
					correo: inspector.correo
				});
				insertarModelo7_m.add(insertarModelo7_fila);
				insertarModelo7_fila.save();
				var elemento_tareas_mistareas = elemento.tareas.mistareas;
				var insertarModelo8_m = Alloy.Collections.tareas;
				var db_insertarModelo8 = Ti.Database.open(insertarModelo8_m.config.adapter.db_name);
				db_insertarModelo8.execute('BEGIN');
				_.each(elemento_tareas_mistareas, function(insertarModelo8_fila, pos) {
					db_insertarModelo8.execute('INSERT INTO tareas (fecha_tarea, id_inspeccion, id_asegurado, nivel_2, comentario_can_o_rech, asegurado_tel_fijo, estado_tarea, bono, evento, id_inspector, asegurado_codigo_identificador, lat, nivel_1, asegurado_nombre, pais, direccion, asegurador, fecha_ingreso, fecha_siniestro, nivel_1_texto, distance, nivel_4, perfil, asegurado_id, pais_texto, id_server, categoria, nivel_3, asegurado_correo, num_caso, lon, asegurado_tel_movil, nivel_5, tipo_tarea) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', insertarModelo8_fila.fecha_tarea, insertarModelo8_fila.id_inspeccion, insertarModelo8_fila.id_asegurado, insertarModelo8_fila.nivel_2, insertarModelo8_fila.comentario_can_o_rech, insertarModelo8_fila.asegurado_tel_fijo, insertarModelo8_fila.estado_tarea, insertarModelo8_fila.bono, insertarModelo8_fila.evento, insertarModelo8_fila.id_inspector, insertarModelo8_fila.asegurado_codigo_identificador, insertarModelo8_fila.lat, insertarModelo8_fila.nivel_1, insertarModelo8_fila.asegurado_nombre, insertarModelo8_fila.pais, insertarModelo8_fila.direccion, insertarModelo8_fila.asegurador, insertarModelo8_fila.fecha_ingreso, insertarModelo8_fila.fecha_siniestro, insertarModelo8_fila.nivel_1_, insertarModelo8_fila.distancia, insertarModelo8_fila.nivel_4, 'ubicacion', insertarModelo8_fila.asegurado_id, insertarModelo8_fila.pais, insertarModelo8_fila.id, insertarModelo8_fila.categoria, insertarModelo8_fila.nivel_3, insertarModelo8_fila.asegurado_correo, insertarModelo8_fila.num_caso, insertarModelo8_fila.lon, insertarModelo8_fila.asegurado_tel_movil, insertarModelo8_fila.nivel_5, insertarModelo8_fila.tipo_tarea);
				});
				db_insertarModelo8.execute('COMMIT');
				db_insertarModelo8.close();
				db_insertarModelo8 = null;
				insertarModelo8_m.trigger('change');
				var elemento_tareas_historial_tareas = elemento.tareas.historial_tareas;
				var insertarModelo9_m = Alloy.Collections.historial_tareas;
				var db_insertarModelo9 = Ti.Database.open(insertarModelo9_m.config.adapter.db_name);
				db_insertarModelo9.execute('BEGIN');
				_.each(elemento_tareas_historial_tareas, function(insertarModelo9_fila, pos) {
					db_insertarModelo9.execute('INSERT INTO historial_tareas (fecha_tarea, id_inspeccion, id_asegurado, nivel_2, comentario_can_o_rech, asegurado_tel_fijo, estado_tarea, bono, evento, id_inspector, asegurado_codigo_identificador, lat, nivel_1, asegurado_nombre, pais, direccion, asegurador, fecha_ingreso, fecha_siniestro, nivel_1_texto, distance, fecha_termino, nivel_4, perfil, asegurado_id, id_server, categoria, nivel_3, asegurado_correo, num_caso, lon, asegurado_tel_movil, nivel_5, tipo_tarea, hora_termino) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', insertarModelo9_fila.fecha_tarea, insertarModelo9_fila.id_inspeccion, insertarModelo9_fila.id_asegurado, insertarModelo9_fila.nivel_2, insertarModelo9_fila.comentario_can_o_rech, insertarModelo9_fila.asegurado_tel_fijo, insertarModelo9_fila.estado_tarea, insertarModelo9_fila.bono, insertarModelo9_fila.evento, insertarModelo9_fila.id_inspector, insertarModelo9_fila.asegurado_codigo_identificador, insertarModelo9_fila.lat, insertarModelo9_fila.nivel_1, insertarModelo9_fila.asegurado_nombre, insertarModelo9_fila.pais, insertarModelo9_fila.direccion, insertarModelo9_fila.asegurador, insertarModelo9_fila.fecha_ingreso, insertarModelo9_fila.fecha_siniestro, insertarModelo9_fila.nivel_1_, insertarModelo9_fila.distancia, insertarModelo9_fila.fecha_finalizacion, insertarModelo9_fila.nivel_4, 'ubicacion', insertarModelo9_fila.asegurado_id, insertarModelo9_fila.id, insertarModelo9_fila.categoria, insertarModelo9_fila.nivel_3, insertarModelo9_fila.asegurado_correo, insertarModelo9_fila.num_caso, insertarModelo9_fila.lon, insertarModelo9_fila.asegurado_tel_movil, insertarModelo9_fila.nivel_5, insertarModelo9_fila.tipo_tarea, insertarModelo9_fila.hora_finalizacion);
				});
				db_insertarModelo9.execute('COMMIT');
				db_insertarModelo9.close();
				db_insertarModelo9 = null;
				insertarModelo9_m.trigger('change');
				var elemento_tareas_emergencias_perfil = elemento.tareas.emergencias.perfil;
				var insertarModelo10_m = Alloy.Collections.emergencia;
				var db_insertarModelo10 = Ti.Database.open(insertarModelo10_m.config.adapter.db_name);
				db_insertarModelo10.execute('BEGIN');
				_.each(elemento_tareas_emergencias_perfil, function(insertarModelo10_fila, pos) {
					db_insertarModelo10.execute('INSERT INTO emergencia (fecha_tarea, id_inspeccion, id_asegurado, nivel_2, comentario_can_o_rech, asegurado_tel_fijo, estado_tarea, bono, evento, id_inspector, asegurado_codigo_identificador, lat, nivel_1, asegurado_nombre, pais, direccion, asegurador, fecha_ingreso, fecha_siniestro, nivel_1_texto, distance, nivel_4, perfil, asegurado_id, pais_texto, id_server, categoria, nivel_3, asegurado_correo, num_caso, lon, asegurado_tel_movil, distancia_2, nivel_5, tipo_tarea) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', insertarModelo10_fila.fecha_tarea, insertarModelo10_fila.id_inspeccion, insertarModelo10_fila.id_asegurado, insertarModelo10_fila.nivel_2, insertarModelo10_fila.comentario_can_o_rech, insertarModelo10_fila.asegurado_tel_fijo, insertarModelo10_fila.estado_tarea, insertarModelo10_fila.bono, insertarModelo10_fila.evento, insertarModelo10_fila.id_inspector, insertarModelo10_fila.asegurado_codigo_identificador, insertarModelo10_fila.lat, insertarModelo10_fila.nivel_1, insertarModelo10_fila.asegurado_nombre, insertarModelo10_fila.pais, insertarModelo10_fila.direccion, insertarModelo10_fila.asegurador, insertarModelo10_fila.fecha_ingreso, insertarModelo10_fila.fecha_siniestro, insertarModelo10_fila.nivel_1_, insertarModelo10_fila.distancia, insertarModelo10_fila.nivel_4, 'casa', insertarModelo10_fila.asegurado_id, insertarModelo10_fila.pais, insertarModelo10_fila.id, insertarModelo10_fila.categoria, insertarModelo10_fila.nivel_3, insertarModelo10_fila.asegurado_correo, insertarModelo10_fila.num_caso, insertarModelo10_fila.lon, insertarModelo10_fila.asegurado_tel_movil, insertarModelo10_fila.distancia_2, insertarModelo10_fila.nivel_5, insertarModelo10_fila.tipo_tarea);
				});
				db_insertarModelo10.execute('COMMIT');
				db_insertarModelo10.close();
				db_insertarModelo10 = null;
				insertarModelo10_m.trigger('change');
				var elemento_tareas_emergencias_ubicacion = elemento.tareas.emergencias.ubicacion;
				var insertarModelo11_m = Alloy.Collections.emergencia;
				var db_insertarModelo11 = Ti.Database.open(insertarModelo11_m.config.adapter.db_name);
				db_insertarModelo11.execute('BEGIN');
				_.each(elemento_tareas_emergencias_ubicacion, function(insertarModelo11_fila, pos) {
					db_insertarModelo11.execute('INSERT INTO emergencia (fecha_tarea, id_inspeccion, id_asegurado, nivel_2, comentario_can_o_rech, asegurado_tel_fijo, estado_tarea, bono, evento, id_inspector, asegurado_codigo_identificador, lat, nivel_1, asegurado_nombre, pais, direccion, asegurador, fecha_ingreso, fecha_siniestro, nivel_1_texto, distance, nivel_4, perfil, asegurado_id, pais_texto, id_server, categoria, nivel_3, asegurado_correo, num_caso, lon, asegurado_tel_movil, distancia_2, nivel_5, tipo_tarea) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', insertarModelo11_fila.fecha_tarea, insertarModelo11_fila.id_inspeccion, insertarModelo11_fila.id_asegurado, insertarModelo11_fila.nivel_2, insertarModelo11_fila.comentario_can_o_rech, insertarModelo11_fila.asegurado_tel_fijo, insertarModelo11_fila.estado_tarea, insertarModelo11_fila.bono, insertarModelo11_fila.evento, insertarModelo11_fila.id_inspector, insertarModelo11_fila.asegurado_codigo_identificador, insertarModelo11_fila.lat, insertarModelo11_fila.nivel_1, insertarModelo11_fila.asegurado_nombre, insertarModelo11_fila.pais, insertarModelo11_fila.direccion, insertarModelo11_fila.asegurador, insertarModelo11_fila.fecha_ingreso, insertarModelo11_fila.fecha_siniestro, insertarModelo11_fila.nivel_1_, insertarModelo11_fila.distancia, insertarModelo11_fila.nivel_4, 'ubicacion', insertarModelo11_fila.asegurado_id, insertarModelo11_fila.pais, insertarModelo11_fila.id, insertarModelo11_fila.categoria, insertarModelo11_fila.nivel_3, insertarModelo11_fila.asegurado_correo, insertarModelo11_fila.num_caso, insertarModelo11_fila.lon, insertarModelo11_fila.asegurado_tel_movil, insertarModelo11_fila.distancia_2, insertarModelo11_fila.nivel_5, insertarModelo11_fila.tipo_tarea);
				});
				db_insertarModelo11.execute('COMMIT');
				db_insertarModelo11.close();
				db_insertarModelo11 = null;
				insertarModelo11_m.trigger('change');
				var elemento_tareas_entrantes_critica = elemento.tareas.entrantes.critica;
				var insertarModelo12_m = Alloy.Collections.tareas_entrantes;
				var db_insertarModelo12 = Ti.Database.open(insertarModelo12_m.config.adapter.db_name);
				db_insertarModelo12.execute('BEGIN');
				_.each(elemento_tareas_entrantes_critica, function(insertarModelo12_fila, pos) {
					db_insertarModelo12.execute('INSERT INTO tareas_entrantes (fecha_tarea, id_inspeccion, id_asegurado, nivel_2, comentario_can_o_rech, asegurado_tel_fijo, estado_tarea, bono, evento, id_inspector, asegurado_codigo_identificador, lat, nivel_1, asegurado_nombre, pais, direccion, asegurador, fecha_ingreso, fecha_siniestro, nivel_1_texto, distance, nivel_4, perfil, asegurado_id, pais_texto, id_server, categoria, nivel_3, asegurado_correo, num_caso, lon, asegurado_tel_movil, tipo_tarea, nivel_5) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', insertarModelo12_fila.fecha_tarea, insertarModelo12_fila.id_inspeccion, insertarModelo12_fila.id_asegurado, insertarModelo12_fila.nivel_2, insertarModelo12_fila.comentario_can_o_rech, insertarModelo12_fila.asegurado_tel_fijo, insertarModelo12_fila.estado_tarea, insertarModelo12_fila.bono, insertarModelo12_fila.evento, insertarModelo12_fila.id_inspector, insertarModelo12_fila.asegurado_codigo_identificador, insertarModelo12_fila.lat, insertarModelo12_fila.nivel_1, insertarModelo12_fila.asegurado_nombre, insertarModelo12_fila.pais, insertarModelo12_fila.direccion, insertarModelo12_fila.asegurador, insertarModelo12_fila.fecha_ingreso, insertarModelo12_fila.fecha_siniestro, insertarModelo12_fila.nivel_1_, insertarModelo12_fila.distancia, insertarModelo12_fila.nivel_4, 'casa', insertarModelo12_fila.asegurado_id, insertarModelo12_fila.pais, insertarModelo12_fila.id, insertarModelo12_fila.categoria, insertarModelo12_fila.nivel_3, insertarModelo12_fila.asegurado_correo, insertarModelo12_fila.num_caso, insertarModelo12_fila.lon, insertarModelo12_fila.asegurado_tel_movil, insertarModelo12_fila.tipo_tarea, insertarModelo12_fila.nivel_5);
				});
				db_insertarModelo12.execute('COMMIT');
				db_insertarModelo12.close();
				db_insertarModelo12 = null;
				insertarModelo12_m.trigger('change');
				var elemento_tareas_entrantes_normal = elemento.tareas.entrantes.normal;
				var insertarModelo13_m = Alloy.Collections.tareas_entrantes;
				var db_insertarModelo13 = Ti.Database.open(insertarModelo13_m.config.adapter.db_name);
				db_insertarModelo13.execute('BEGIN');
				_.each(elemento_tareas_entrantes_normal, function(insertarModelo13_fila, pos) {
					db_insertarModelo13.execute('INSERT INTO tareas_entrantes (fecha_tarea, id_inspeccion, id_asegurado, nivel_2, comentario_can_o_rech, asegurado_tel_fijo, estado_tarea, bono, evento, id_inspector, asegurado_codigo_identificador, lat, nivel_1, asegurado_nombre, pais, direccion, asegurador, fecha_ingreso, fecha_siniestro, nivel_1_texto, distance, nivel_4, perfil, asegurado_id, pais_texto, id_server, categoria, nivel_3, asegurado_correo, num_caso, lon, asegurado_tel_movil, tipo_tarea, nivel_5) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', insertarModelo13_fila.fecha_tarea, insertarModelo13_fila.id_inspeccion, insertarModelo13_fila.id_asegurado, insertarModelo13_fila.nivel_2, insertarModelo13_fila.comentario_can_o_rech, insertarModelo13_fila.asegurado_tel_fijo, insertarModelo13_fila.estado_tarea, insertarModelo13_fila.bono, insertarModelo13_fila.evento, insertarModelo13_fila.id_inspector, insertarModelo13_fila.asegurado_codigo_identificador, insertarModelo13_fila.lat, insertarModelo13_fila.nivel_1, insertarModelo13_fila.asegurado_nombre, insertarModelo13_fila.pais, insertarModelo13_fila.direccion, insertarModelo13_fila.asegurador, insertarModelo13_fila.fecha_ingreso, insertarModelo13_fila.fecha_siniestro, insertarModelo13_fila.nivel_1_, insertarModelo13_fila.distancia, insertarModelo13_fila.nivel_4, 'casa', insertarModelo13_fila.asegurado_id, insertarModelo13_fila.pais, insertarModelo13_fila.id, insertarModelo13_fila.categoria, insertarModelo13_fila.nivel_3, insertarModelo13_fila.asegurado_correo, insertarModelo13_fila.num_caso, insertarModelo13_fila.lon, insertarModelo13_fila.asegurado_tel_movil, insertarModelo13_fila.tipo_tarea, insertarModelo13_fila.nivel_5);
				});
				db_insertarModelo13.execute('COMMIT');
				db_insertarModelo13.close();
				db_insertarModelo13 = null;
				insertarModelo13_m.trigger('change');
				/** 
				 * Al terminar la carga de datos de la base de datos, llamamos una funcion para continuar con el proceso 
				 */
				var ID_1038535132 = null;
				if ('cargar_selectores' in require('funciones')) {
					ID_1038535132 = require('funciones').cargar_selectores({});
				} else {
					try {
						ID_1038535132 = f_cargar_selectores({});
					} catch (ee) {}
				}
			} else {
				/** 
				 * &#160;Y si es un error que no tenemos registrado, mostramos el mensaje directamente en una alerta 
				 */
				var ID_1626215927 = null;
				if ('mostrar_botones' in require('funciones')) {
					ID_1626215927 = require('funciones').mostrar_botones({});
				} else {
					try {
						ID_1626215927 = f_mostrar_botones({});
					} catch (ee) {}
				}
				var preguntarAlerta12_opts = [L('x1518866076_traducir', 'Aceptar')];
				var preguntarAlerta12 = Ti.UI.createAlertDialog({
					title: L('x57652245_traducir', 'Alerta'),
					message: '' + String.format(L('x921749426_traducir', 'Error %1$s'), elemento.mensaje.toString()) + '',
					buttonNames: preguntarAlerta12_opts
				});
				preguntarAlerta12.addEventListener('click', function(e) {
					var res = preguntarAlerta12_opts[e.index];
					res = null;

					e.source.removeEventListener("click", arguments.callee);
				});
				preguntarAlerta12.show();
			}
			elemento = null, valor = null;
		};

		consultarURL3.error = function(e) {
			var elemento = e,
				valor = e;
			var ID_697710795 = null;
			if ('mostrar_botones' in require('funciones')) {
				ID_697710795 = require('funciones').mostrar_botones({});
			} else {
				try {
					ID_697710795 = f_mostrar_botones({});
				} catch (ee) {}
			}
			var preguntarAlerta13_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta13 = Ti.UI.createAlertDialog({
				title: L('x57652245_traducir', 'Alerta'),
				message: L('x3632753785_traducir', 'Problema de conexion, favor intentar más tarde'),
				buttonNames: preguntarAlerta13_opts
			});
			preguntarAlerta13.addEventListener('click', function(e) {
				var suu = preguntarAlerta13_opts[e.index];
				suu = null;

				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta13.show();
			elemento = null, valor = null;
		};
		require('helper').ajaxUnico('consultarURL3', '' + String.format(L('x796226125', '%1$slogin'), url_server.toString()) + '', 'POST', {
			correo: correo,
			password: password,
			device: Ti.Platform.name,
			lat: gps_latitud,
			lon: gps_longitud,
			device_token: devicetoken,
			uidd: uidd,
			fecha: dif
		}, 15000, consultarURL3);
	}
	return null;
};
var f_cargar_selectores = function(x_params) {
	var item = x_params['item'];
	$.StatusEsperando.setText('status: cargando selectores');

	/** 
	 * Consultamos la tabla de inspectores y su resultado lo guardamos en la variable inspector 
	 */
	var consultarModelo_i = Alloy.createCollection('inspectores');
	var consultarModelo_i_where = '';
	consultarModelo_i.fetch();
	var inspector = require('helper').query2array(consultarModelo_i);
	var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
	var selectores_fechas = JSON.parse(Ti.App.Properties.getString('selectores_fechas'));
	if (inspector && inspector.length) {
		/** 
		 * guardamos la variable: inspector con los datos del inspector activo para no tener que consultarla cada vez dentro del menu. 
		 */
		require('vars')['inspector'] = inspector[0];
		if (Ti.App.deployType != 'production') console.log('psb inspector cargado', {
			"inspector": inspector[0]
		});
		/** 
		 * Consultamos la tabla de pais, filtrando por el pais del inspector, guardamos la consulta en una variable pais 
		 */
		var consultarModelo2_i = Alloy.createCollection('pais');
		var consultarModelo2_i_where = 'id_server=\'' + inspector[0].pais + '\'';
		consultarModelo2_i.fetch({
			query: 'SELECT * FROM pais WHERE id_server=\'' + inspector[0].pais + '\''
		});
		var pais = require('helper').query2array(consultarModelo2_i);
		if (pais && pais.length) {
			require('vars')['pais'] = pais;
			var defecto = {
				fecha: ''
			};
			/** 
			 * Generamos una estructura con los datos de los selectores y guardando la fecha (de consulta) en vacio 
			 */
			var selectores_fechas = {
				destino: defecto,
				compania: defecto,
				entidad_financiera: defecto,
				tipo_siniestro: defecto,
				estructura_soportante: defecto,
				muros_tabiques: defecto,
				entrepisos: defecto,
				pavimento: defecto,
				estructura_cubierta: defecto,
				cubierta: defecto,
				partida: defecto,
				bienes: defecto,
				tipo_dano: defecto,
				marcas: defecto,
				monedas: defecto
			};
			if (Ti.App.deployType != 'production') console.log('consultando selectores', {});
			var consultarURL4 = {};

			consultarURL4.success = function(e) {
				var elemento = e,
					valor = e;
				if (elemento.error == 0 || elemento.error == '0') {
					var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
					if (Ti.App.deployType != 'production') console.log('Mi lenguage a ocupar es', {
						"leng": pais[0].lenguaje
					});
					var selectores_fechas = JSON.parse(Ti.App.Properties.getString('selectores_fechas'));
					/** 
					 * Creamos una variable selectores_list y agregamos un campo nuevo, que sera la fecha. Despues los ordenamos segun el nombre del selector 
					 */

					selectores_list = _.map(elemento.selectores,
						function(num) {
							var object = {
								selector: num.selector,
								fecha: num.fecha
							};
							return object;
						});
					selectores_fechas = _.indexBy(selectores_list, 'selector');
					/** 
					 * Guardamos la variable selectores_fecha para que sea de acceso permanente, (que no se pierda despues de una vez cerrado) 
					 */
					Ti.App.Properties.setString('selectores_fechas', JSON.stringify(selectores_fechas));
					/** 
					 * Limpiamos las tablas, insertamos los datos desde el servidor y vamos modificando el texto que muestra en pantalla el status de los datos que estan siendo almacenados 
					 */
					if (Ti.App.deployType != 'production') console.log('Ingresando datos de pais actual', {});
					if (Ti.App.deployType != 'production') console.log(String.format(L('x869747243_traducir', 'eliminando e ingresando: destino (%1$s)'), elemento.destino.length.toString()), {});
					$.StatusEsperando.setText(String.format(L('x420234614_traducir', 'status: insertando %1$s destinos'), elemento.destino.length.toString()));

					var eliminarModelo12_i = Alloy.Collections.destino;
					var sql = 'DELETE FROM ' + eliminarModelo12_i.config.adapter.collection_name + ' WHERE pais_texto=\'' + pais[0].nombre + '\'';
					var db = Ti.Database.open(eliminarModelo12_i.config.adapter.db_name);
					db.execute(sql);
					db.close();
					eliminarModelo12_i.trigger('remove');
					var elemento_destino = elemento.destino;
					var insertarModelo14_m = Alloy.Collections.destino;
					var db_insertarModelo14 = Ti.Database.open(insertarModelo14_m.config.adapter.db_name);
					db_insertarModelo14.execute('BEGIN');
					_.each(elemento_destino, function(insertarModelo14_fila, pos) {
						db_insertarModelo14.execute('INSERT INTO destino (nombre, fecha, pais_texto, id_server, id_segured, pais) VALUES (?,?,?,?,?,?)', insertarModelo14_fila.valor, insertarModelo14_fila.fecha, insertarModelo14_fila.pais_texto, insertarModelo14_fila.id, insertarModelo14_fila.id_segured, insertarModelo14_fila.pais);
					});
					db_insertarModelo14.execute('COMMIT');
					db_insertarModelo14.close();
					db_insertarModelo14 = null;
					insertarModelo14_m.trigger('change');
					if (Ti.App.deployType != 'production') console.log(String.format(L('x3307041009_traducir', 'eliminando e ingresando: compania (%1$s)'), elemento.compania.length.toString()), {});
					$.StatusEsperando.setText(String.format(L('x750134968_traducir', 'status: insertando %1$s companias'), elemento.compania.length.toString()));

					var eliminarModelo13_i = Alloy.Collections.compania;
					var sql = 'DELETE FROM ' + eliminarModelo13_i.config.adapter.collection_name + ' WHERE pais_texto=\'' + pais[0].nombre + '\'';
					var db = Ti.Database.open(eliminarModelo13_i.config.adapter.db_name);
					db.execute(sql);
					db.close();
					eliminarModelo13_i.trigger('remove');
					var elemento_compania = elemento.compania;
					var insertarModelo15_m = Alloy.Collections.compania;
					var db_insertarModelo15 = Ti.Database.open(insertarModelo15_m.config.adapter.db_name);
					db_insertarModelo15.execute('BEGIN');
					_.each(elemento_compania, function(insertarModelo15_fila, pos) {
						db_insertarModelo15.execute('INSERT INTO compania (nombre, fecha, id_server, pais_texto, id_segured, pais) VALUES (?,?,?,?,?,?)', insertarModelo15_fila.valor, insertarModelo15_fila.fecha, insertarModelo15_fila.id, insertarModelo15_fila.pais_texto, insertarModelo15_fila.id_segured, insertarModelo15_fila.pais);
					});
					db_insertarModelo15.execute('COMMIT');
					db_insertarModelo15.close();
					db_insertarModelo15 = null;
					insertarModelo15_m.trigger('change');
					if (Ti.App.deployType != 'production') console.log(String.format(L('x1259104841_traducir', 'eliminando e ingresando: entidad_financiera (%1$s)'), elemento.entidad_financiera.length.toString()), {});
					$.StatusEsperando.setText(String.format(L('x2157955010_traducir', 'status: insertando %1$s entidades'), elemento.entidad_financiera.length.toString()));

					var eliminarModelo14_i = Alloy.Collections.entidad_financiera;
					var sql = 'DELETE FROM ' + eliminarModelo14_i.config.adapter.collection_name + ' WHERE pais_texto=\'' + pais[0].nombre + '\'';
					var db = Ti.Database.open(eliminarModelo14_i.config.adapter.db_name);
					db.execute(sql);
					db.close();
					eliminarModelo14_i.trigger('remove');
					var elemento_entidad_financiera = elemento.entidad_financiera;
					var insertarModelo16_m = Alloy.Collections.entidad_financiera;
					var db_insertarModelo16 = Ti.Database.open(insertarModelo16_m.config.adapter.db_name);
					db_insertarModelo16.execute('BEGIN');
					_.each(elemento_entidad_financiera, function(insertarModelo16_fila, pos) {
						db_insertarModelo16.execute('INSERT INTO entidad_financiera (nombre, fecha, id_server, pais_texto, id_segured, pais) VALUES (?,?,?,?,?,?)', insertarModelo16_fila.valor, insertarModelo16_fila.fecha, insertarModelo16_fila.id, insertarModelo16_fila.pais_texto, insertarModelo16_fila.id_segured, insertarModelo16_fila.pais);
					});
					db_insertarModelo16.execute('COMMIT');
					db_insertarModelo16.close();
					db_insertarModelo16 = null;
					insertarModelo16_m.trigger('change');
					if (Ti.App.deployType != 'production') console.log(String.format(L('x2170187839_traducir', 'eliminando e ingresando: tipo_siniestro (%1$s)'), elemento.tipo_siniestro.length.toString()), {});
					$.StatusEsperando.setText(String.format(L('x4131184409_traducir', 'status: insertando %1$s tipo siniestros'), elemento.tipo_siniestro.length.toString()));

					var eliminarModelo15_i = Alloy.Collections.tipo_siniestro;
					var sql = 'DELETE FROM ' + eliminarModelo15_i.config.adapter.collection_name + ' WHERE pais_texto=\'' + pais[0].nombre + '\'';
					var db = Ti.Database.open(eliminarModelo15_i.config.adapter.db_name);
					db.execute(sql);
					db.close();
					eliminarModelo15_i.trigger('remove');
					var elemento_tipo_siniestro = elemento.tipo_siniestro;
					var insertarModelo17_m = Alloy.Collections.tipo_siniestro;
					var db_insertarModelo17 = Ti.Database.open(insertarModelo17_m.config.adapter.db_name);
					db_insertarModelo17.execute('BEGIN');
					_.each(elemento_tipo_siniestro, function(insertarModelo17_fila, pos) {
						db_insertarModelo17.execute('INSERT INTO tipo_siniestro (nombre, fecha, id_server, pais_texto, id_segured, pais) VALUES (?,?,?,?,?,?)', insertarModelo17_fila.valor, insertarModelo17_fila.fecha, insertarModelo17_fila.id, insertarModelo17_fila.pais_texto, insertarModelo17_fila.id_segured, insertarModelo17_fila.pais);
					});
					db_insertarModelo17.execute('COMMIT');
					db_insertarModelo17.close();
					db_insertarModelo17 = null;
					insertarModelo17_m.trigger('change');
					if (Ti.App.deployType != 'production') console.log(String.format(L('x1439237550_traducir', 'eliminando e ingresando: estructura_soportante (%1$s)'), elemento.estructura_soportante.length.toString()), {});
					$.StatusEsperando.setText(String.format(L('x586790134_traducir', 'status: insertando %1$s estructuras soportantes'), elemento.estructura_soportante.length.toString()));

					var eliminarModelo16_i = Alloy.Collections.estructura_soportante;
					var sql = 'DELETE FROM ' + eliminarModelo16_i.config.adapter.collection_name + ' WHERE pais_texto=\'' + pais[0].nombre + '\'';
					var db = Ti.Database.open(eliminarModelo16_i.config.adapter.db_name);
					db.execute(sql);
					db.close();
					eliminarModelo16_i.trigger('remove');
					var elemento_estructura_soportante = elemento.estructura_soportante;
					var insertarModelo18_m = Alloy.Collections.estructura_soportante;
					var db_insertarModelo18 = Ti.Database.open(insertarModelo18_m.config.adapter.db_name);
					db_insertarModelo18.execute('BEGIN');
					_.each(elemento_estructura_soportante, function(insertarModelo18_fila, pos) {
						db_insertarModelo18.execute('INSERT INTO estructura_soportante (nombre, fecha, id_server, pais_texto, id_segured, pais) VALUES (?,?,?,?,?,?)', insertarModelo18_fila.valor, insertarModelo18_fila.fecha, insertarModelo18_fila.id, insertarModelo18_fila.pais_texto, insertarModelo18_fila.id_segured, insertarModelo18_fila.pais);
					});
					db_insertarModelo18.execute('COMMIT');
					db_insertarModelo18.close();
					db_insertarModelo18 = null;
					insertarModelo18_m.trigger('change');
					if (Ti.App.deployType != 'production') console.log(String.format(L('x112713061_traducir', 'eliminando e ingresando: muros_tabiques (%1$s)'), elemento.muros_tabiques.length.toString()), {});
					$.StatusEsperando.setText(String.format(L('x2158727374_traducir', 'status: insertando %1$s muros_tabiques'), elemento.muros_tabiques.length.toString()));

					var eliminarModelo17_i = Alloy.Collections.muros_tabiques;
					var sql = 'DELETE FROM ' + eliminarModelo17_i.config.adapter.collection_name + ' WHERE pais_texto=\'' + pais[0].nombre + '\'';
					var db = Ti.Database.open(eliminarModelo17_i.config.adapter.db_name);
					db.execute(sql);
					db.close();
					eliminarModelo17_i.trigger('remove');
					var elemento_muros_tabiques = elemento.muros_tabiques;
					var insertarModelo19_m = Alloy.Collections.muros_tabiques;
					var db_insertarModelo19 = Ti.Database.open(insertarModelo19_m.config.adapter.db_name);
					db_insertarModelo19.execute('BEGIN');
					_.each(elemento_muros_tabiques, function(insertarModelo19_fila, pos) {
						db_insertarModelo19.execute('INSERT INTO muros_tabiques (nombre, fecha, id_server, pais_texto, id_segured, pais) VALUES (?,?,?,?,?,?)', insertarModelo19_fila.valor, insertarModelo19_fila.fecha, insertarModelo19_fila.id, insertarModelo19_fila.pais_texto, insertarModelo19_fila.id_segured, insertarModelo19_fila.pais);
					});
					db_insertarModelo19.execute('COMMIT');
					db_insertarModelo19.close();
					db_insertarModelo19 = null;
					insertarModelo19_m.trigger('change');
					if (Ti.App.deployType != 'production') console.log(String.format(L('x322969612_traducir', 'eliminando e ingresando: entrepisos (%1$s)'), elemento.entrepisos.length.toString()), {});
					$.StatusEsperando.setText(String.format(L('x197631996_traducir', 'status: insertando %1$s entrepisos'), elemento.entrepisos.length.toString()));

					var eliminarModelo18_i = Alloy.Collections.entrepisos;
					var sql = 'DELETE FROM ' + eliminarModelo18_i.config.adapter.collection_name + ' WHERE pais_texto=\'' + pais[0].nombre + '\'';
					var db = Ti.Database.open(eliminarModelo18_i.config.adapter.db_name);
					db.execute(sql);
					db.close();
					eliminarModelo18_i.trigger('remove');
					var elemento_entrepisos = elemento.entrepisos;
					var insertarModelo20_m = Alloy.Collections.entrepisos;
					var db_insertarModelo20 = Ti.Database.open(insertarModelo20_m.config.adapter.db_name);
					db_insertarModelo20.execute('BEGIN');
					_.each(elemento_entrepisos, function(insertarModelo20_fila, pos) {
						db_insertarModelo20.execute('INSERT INTO entrepisos (nombre, fecha, id_server, pais_texto, id_segured, pais) VALUES (?,?,?,?,?,?)', insertarModelo20_fila.valor, insertarModelo20_fila.fecha, insertarModelo20_fila.id, insertarModelo20_fila.pais_texto, insertarModelo20_fila.id_segured, insertarModelo20_fila.pais);
					});
					db_insertarModelo20.execute('COMMIT');
					db_insertarModelo20.close();
					db_insertarModelo20 = null;
					insertarModelo20_m.trigger('change');
					if (Ti.App.deployType != 'production') console.log(String.format(L('x2733921830_traducir', 'eliminando e ingresando: pavimento (%1$s)'), elemento.pavimento.length.toString()), {});
					$.StatusEsperando.setText(String.format(L('x1699320037_traducir', 'status: insertando %1$s pavimentos'), elemento.pavimento.length.toString()));

					var eliminarModelo19_i = Alloy.Collections.pavimento;
					var sql = 'DELETE FROM ' + eliminarModelo19_i.config.adapter.collection_name + ' WHERE pais_texto=\'' + pais[0].nombre + '\'';
					var db = Ti.Database.open(eliminarModelo19_i.config.adapter.db_name);
					db.execute(sql);
					db.close();
					eliminarModelo19_i.trigger('remove');
					var elemento_pavimento = elemento.pavimento;
					var insertarModelo21_m = Alloy.Collections.pavimento;
					var db_insertarModelo21 = Ti.Database.open(insertarModelo21_m.config.adapter.db_name);
					db_insertarModelo21.execute('BEGIN');
					_.each(elemento_pavimento, function(insertarModelo21_fila, pos) {
						db_insertarModelo21.execute('INSERT INTO pavimento (nombre, fecha, id_server, pais_texto, id_segured, pais) VALUES (?,?,?,?,?,?)', insertarModelo21_fila.valor, insertarModelo21_fila.fecha, insertarModelo21_fila.id, insertarModelo21_fila.pais_texto, insertarModelo21_fila.id_segured, insertarModelo21_fila.pais);
					});
					db_insertarModelo21.execute('COMMIT');
					db_insertarModelo21.close();
					db_insertarModelo21 = null;
					insertarModelo21_m.trigger('change');
					if (Ti.App.deployType != 'production') console.log(String.format(L('x4260007882_traducir', 'eliminando e ingresando: estructura_cubierta (%1$s)'), elemento.estructura_cubierta.length.toString()), {});
					$.StatusEsperando.setText(String.format(L('x2054507840_traducir', 'status: insertando %1$s estructuras cubiertas'), elemento.estructura_cubierta.length.toString()));

					var eliminarModelo20_i = Alloy.Collections.estructura_cubierta;
					var sql = 'DELETE FROM ' + eliminarModelo20_i.config.adapter.collection_name + ' WHERE pais_texto=\'' + pais[0].nombre + '\'';
					var db = Ti.Database.open(eliminarModelo20_i.config.adapter.db_name);
					db.execute(sql);
					db.close();
					eliminarModelo20_i.trigger('remove');
					var elemento_estructura_cubierta = elemento.estructura_cubierta;
					var insertarModelo22_m = Alloy.Collections.estructura_cubierta;
					var db_insertarModelo22 = Ti.Database.open(insertarModelo22_m.config.adapter.db_name);
					db_insertarModelo22.execute('BEGIN');
					_.each(elemento_estructura_cubierta, function(insertarModelo22_fila, pos) {
						db_insertarModelo22.execute('INSERT INTO estructura_cubierta (nombre, fecha, id_server, pais_texto, id_segured, pais) VALUES (?,?,?,?,?,?)', insertarModelo22_fila.valor, insertarModelo22_fila.fecha, insertarModelo22_fila.id, insertarModelo22_fila.pais_texto, insertarModelo22_fila.id_segured, insertarModelo22_fila.pais);
					});
					db_insertarModelo22.execute('COMMIT');
					db_insertarModelo22.close();
					db_insertarModelo22 = null;
					insertarModelo22_m.trigger('change');
					if (Ti.App.deployType != 'production') console.log(String.format(L('x4140625223_traducir', 'eliminando e ingresando: cubierta (%1$s)'), elemento.cubierta.length.toString()), {});
					$.StatusEsperando.setText(String.format(L('x2731315761_traducir', 'status: insertando %1$s cubiertas'), elemento.cubierta.length.toString()));

					var eliminarModelo21_i = Alloy.Collections.cubierta;
					var sql = 'DELETE FROM ' + eliminarModelo21_i.config.adapter.collection_name + ' WHERE pais_texto=\'' + pais[0].nombre + '\'';
					var db = Ti.Database.open(eliminarModelo21_i.config.adapter.db_name);
					db.execute(sql);
					db.close();
					eliminarModelo21_i.trigger('remove');
					var elemento_cubierta = elemento.cubierta;
					var insertarModelo23_m = Alloy.Collections.cubierta;
					var db_insertarModelo23 = Ti.Database.open(insertarModelo23_m.config.adapter.db_name);
					db_insertarModelo23.execute('BEGIN');
					_.each(elemento_cubierta, function(insertarModelo23_fila, pos) {
						db_insertarModelo23.execute('INSERT INTO cubierta (nombre, fecha, id_server, pais_texto, id_segured, pais) VALUES (?,?,?,?,?,?)', insertarModelo23_fila.valor, insertarModelo23_fila.fecha, insertarModelo23_fila.id, insertarModelo23_fila.pais_texto, insertarModelo23_fila.id_segured, insertarModelo23_fila.pais);
					});
					db_insertarModelo23.execute('COMMIT');
					db_insertarModelo23.close();
					db_insertarModelo23 = null;
					insertarModelo23_m.trigger('change');
					if (Ti.App.deployType != 'production') console.log(String.format(L('x56040633_traducir', 'eliminando e ingresando: tipo_partida (%1$s)'), elemento.partida.length.toString()), {});
					$.StatusEsperando.setText(String.format(L('x2130744222_traducir', 'status: insertando %1$s tipo partidas'), elemento.partida.length.toString()));

					var eliminarModelo22_i = Alloy.Collections.tipo_partida;
					var sql = 'DELETE FROM ' + eliminarModelo22_i.config.adapter.collection_name + ' WHERE pais_texto=\'' + pais[0].nombre + '\'';
					var db = Ti.Database.open(eliminarModelo22_i.config.adapter.db_name);
					db.execute(sql);
					db.close();
					eliminarModelo22_i.trigger('remove');
					var elemento_partida = elemento.partida;
					var insertarModelo24_m = Alloy.Collections.tipo_partida;
					var db_insertarModelo24 = Ti.Database.open(insertarModelo24_m.config.adapter.db_name);
					db_insertarModelo24.execute('BEGIN');
					_.each(elemento_partida, function(insertarModelo24_fila, pos) {
						db_insertarModelo24.execute('INSERT INTO tipo_partida (nombre, fecha, id_server, pais_texto, id_segured, pais) VALUES (?,?,?,?,?,?)', insertarModelo24_fila.valor, insertarModelo24_fila.fecha, insertarModelo24_fila.id, insertarModelo24_fila.pais_texto, insertarModelo24_fila.id_segured, insertarModelo24_fila.pais);
					});
					db_insertarModelo24.execute('COMMIT');
					db_insertarModelo24.close();
					db_insertarModelo24 = null;
					insertarModelo24_m.trigger('change');
					if (Ti.App.deployType != 'production') console.log(String.format(L('x318662177_traducir', 'eliminando e ingresando: bienes (%1$s)'), elemento.bienes.length.toString()), {});
					$.StatusEsperando.setText(String.format(L('x3960306369_traducir', 'status: insertando %1$s bienes'), elemento.bienes.length.toString()));

					var eliminarModelo23_i = Alloy.Collections.bienes;
					var sql = 'DELETE FROM ' + eliminarModelo23_i.config.adapter.collection_name + ' WHERE pais_texto=\'' + pais[0].nombre + '\'';
					var db = Ti.Database.open(eliminarModelo23_i.config.adapter.db_name);
					db.execute(sql);
					db.close();
					eliminarModelo23_i.trigger('remove');
					var elemento_bienes = elemento.bienes;
					var insertarModelo25_m = Alloy.Collections.bienes;
					var db_insertarModelo25 = Ti.Database.open(insertarModelo25_m.config.adapter.db_name);
					db_insertarModelo25.execute('BEGIN');
					_.each(elemento_bienes, function(insertarModelo25_fila, pos) {
						db_insertarModelo25.execute('INSERT INTO bienes (nombre, fecha, id_server, pais_texto, id_segured, pais) VALUES (?,?,?,?,?,?)', insertarModelo25_fila.valor, insertarModelo25_fila.fecha, insertarModelo25_fila.id, insertarModelo25_fila.pais_texto, insertarModelo25_fila.id_segured, insertarModelo25_fila.pais);
					});
					db_insertarModelo25.execute('COMMIT');
					db_insertarModelo25.close();
					db_insertarModelo25 = null;
					insertarModelo25_m.trigger('change');
					if (Ti.App.deployType != 'production') console.log(String.format(L('x2761353396_traducir', 'eliminando e ingresando: marcas (%1$s)'), elemento.marcas.length.toString()), {});
					$.StatusEsperando.setText(String.format(L('x3518346198_traducir', 'status: insertando %1$s marcas'), elemento.marcas.length.toString()));

					var eliminarModelo24_i = Alloy.Collections.marcas;
					var sql = 'DELETE FROM ' + eliminarModelo24_i.config.adapter.collection_name + ' WHERE pais_texto=\'' + pais[0].nombre + '\'';
					var db = Ti.Database.open(eliminarModelo24_i.config.adapter.db_name);
					db.execute(sql);
					db.close();
					eliminarModelo24_i.trigger('remove');
					var elemento_marcas = elemento.marcas;
					var insertarModelo26_m = Alloy.Collections.marcas;
					var db_insertarModelo26 = Ti.Database.open(insertarModelo26_m.config.adapter.db_name);
					db_insertarModelo26.execute('BEGIN');
					_.each(elemento_marcas, function(insertarModelo26_fila, pos) {
						db_insertarModelo26.execute('INSERT INTO marcas (nombre, fecha, id_server, pais_texto, id_segured, pais) VALUES (?,?,?,?,?,?)', insertarModelo26_fila.valor, insertarModelo26_fila.fecha, insertarModelo26_fila.id, insertarModelo26_fila.pais_texto, insertarModelo26_fila.id_segured, insertarModelo26_fila.pais);
					});
					db_insertarModelo26.execute('COMMIT');
					db_insertarModelo26.close();
					db_insertarModelo26 = null;
					insertarModelo26_m.trigger('change');
					if (Ti.App.deployType != 'production') console.log(String.format(L('x2205107975_traducir', 'eliminando e ingresando: monedas (%1$s)'), elemento.monedas.length.toString()), {});
					$.StatusEsperando.setText(String.format(L('x1168755669_traducir', 'status: insertando %1$s monedas'), elemento.monedas.length.toString()));

					var eliminarModelo25_i = Alloy.Collections.monedas;
					var sql = 'DELETE FROM ' + eliminarModelo25_i.config.adapter.collection_name + ' WHERE pais_texto=\'' + pais[0].nombre + '\'';
					var db = Ti.Database.open(eliminarModelo25_i.config.adapter.db_name);
					db.execute(sql);
					db.close();
					eliminarModelo25_i.trigger('remove');
					var elemento_monedas = elemento.monedas;
					var insertarModelo27_m = Alloy.Collections.monedas;
					var db_insertarModelo27 = Ti.Database.open(insertarModelo27_m.config.adapter.db_name);
					db_insertarModelo27.execute('BEGIN');
					_.each(elemento_monedas, function(insertarModelo27_fila, pos) {
						db_insertarModelo27.execute('INSERT INTO monedas (nombre, fecha, id_server, pais_texto, id_segured, pais) VALUES (?,?,?,?,?,?)', insertarModelo27_fila.valor, insertarModelo27_fila.fecha, insertarModelo27_fila.id, insertarModelo27_fila.pais_texto, insertarModelo27_fila.id_segured, insertarModelo27_fila.pais);
					});
					db_insertarModelo27.execute('COMMIT');
					db_insertarModelo27.close();
					db_insertarModelo27 = null;
					insertarModelo27_m.trigger('change');
					if (Ti.App.deployType != 'production') console.log(String.format(L('x656637966_traducir', 'eliminando e ingresando: tipo_dano (%1$s)'), elemento.tipo_dano.length.toString()), {});
					$.StatusEsperando.setText(String.format(L('x541471912_traducir', 'status: insertando %1$s tipo danos'), elemento.tipo_dano.length.toString()));

					var eliminarModelo26_i = Alloy.Collections.tipo_dano;
					var sql = 'DELETE FROM ' + eliminarModelo26_i.config.adapter.collection_name + ' WHERE pais_texto=\'' + pais[0].nombre + '\'';
					var db = Ti.Database.open(eliminarModelo26_i.config.adapter.db_name);
					db.execute(sql);
					db.close();
					eliminarModelo26_i.trigger('remove');
					var elemento_tipo_dano = elemento.tipo_dano;
					var insertarModelo28_m = Alloy.Collections.tipo_dano;
					var db_insertarModelo28 = Ti.Database.open(insertarModelo28_m.config.adapter.db_name);
					db_insertarModelo28.execute('BEGIN');
					_.each(elemento_tipo_dano, function(insertarModelo28_fila, pos) {
						db_insertarModelo28.execute('INSERT INTO tipo_dano (nombre, fecha, id_partida, id_server, pais_texto, id_segured, pais) VALUES (?,?,?,?,?,?,?)', insertarModelo28_fila.valor, insertarModelo28_fila.fecha, insertarModelo28_fila.id_partida, insertarModelo28_fila.id, insertarModelo28_fila.pais_texto, insertarModelo28_fila.id_segured, insertarModelo28_fila.pais);
					});
					db_insertarModelo28.execute('COMMIT');
					db_insertarModelo28.close();
					db_insertarModelo28 = null;
					insertarModelo28_m.trigger('change');
					if (Ti.App.deployType != 'production') console.log(String.format(L('x1231345309_traducir', 'eliminando e ingresando: tipo_accion (%1$s)'), elemento.accion.length.toString()), {});
					$.StatusEsperando.setText(String.format(L('x3484808259_traducir', 'status: insertando %1$s acciones'), elemento.accion.length.toString()));

					var eliminarModelo27_i = Alloy.Collections.tipo_accion;
					var sql = 'DELETE FROM ' + eliminarModelo27_i.config.adapter.collection_name + ' WHERE pais_texto=\'' + pais[0].nombre + '\'';
					var db = Ti.Database.open(eliminarModelo27_i.config.adapter.db_name);
					db.execute(sql);
					db.close();
					eliminarModelo27_i.trigger('remove');
					var elemento_accion = elemento.accion;
					var insertarModelo29_m = Alloy.Collections.tipo_accion;
					var db_insertarModelo29 = Ti.Database.open(insertarModelo29_m.config.adapter.db_name);
					db_insertarModelo29.execute('BEGIN');
					_.each(elemento_accion, function(insertarModelo29_fila, pos) {
						db_insertarModelo29.execute('INSERT INTO tipo_accion (nombre, id_tipo_dano, fecha, id_server, pais_texto, id_segured, pais) VALUES (?,?,?,?,?,?,?)', insertarModelo29_fila.valor, insertarModelo29_fila.id_tipo_dano, insertarModelo29_fila.fecha, insertarModelo29_fila.id, insertarModelo29_fila.pais_texto, insertarModelo29_fila.id_segured, insertarModelo29_fila.pais);
					});
					db_insertarModelo29.execute('COMMIT');
					db_insertarModelo29.close();
					db_insertarModelo29 = null;
					insertarModelo29_m.trigger('change');
					if (Ti.App.deployType != 'production') console.log(String.format(L('x3902904863_traducir', 'eliminando e ingresando: unidad_medida (%1$s)'), elemento.unidad_medida.length.toString()), {});
					$.StatusEsperando.setText(String.format(L('x3837695618_traducir', 'status: insertando %1$s unidades de medida'), elemento.unidad_medida.length.toString()));

					var eliminarModelo28_i = Alloy.Collections.unidad_medida;
					var sql = 'DELETE FROM ' + eliminarModelo28_i.config.adapter.collection_name + ' WHERE pais_texto=\'' + pais[0].nombre + '\'';
					var db = Ti.Database.open(eliminarModelo28_i.config.adapter.db_name);
					db.execute(sql);
					db.close();
					eliminarModelo28_i.trigger('remove');
					var elemento_unidad_medida = elemento.unidad_medida;
					var insertarModelo30_m = Alloy.Collections.unidad_medida;
					var db_insertarModelo30 = Ti.Database.open(insertarModelo30_m.config.adapter.db_name);
					db_insertarModelo30.execute('BEGIN');
					_.each(elemento_unidad_medida, function(insertarModelo30_fila, pos) {
						db_insertarModelo30.execute('INSERT INTO unidad_medida (id_accion, nombre, fecha, id_server, pais_texto, id_segured, pais, abrev) VALUES (?,?,?,?,?,?,?,?)', insertarModelo30_fila.id_accion, insertarModelo30_fila.valor, insertarModelo30_fila.fecha, insertarModelo30_fila.id, insertarModelo30_fila.pais_texto, insertarModelo30_fila.id_segured, insertarModelo30_fila.pais, insertarModelo30_fila.valor_s);
					});
					db_insertarModelo30.execute('COMMIT');
					db_insertarModelo30.close();
					db_insertarModelo30 = null;
					insertarModelo30_m.trigger('change');
					/** 
					 * Volvemos a mostrar los botones presentes en el login 
					 */
					var ID_1916611841 = null;
					if ('mostrar_botones' in require('funciones')) {
						ID_1916611841 = require('funciones').mostrar_botones({});
					} else {
						try {
							ID_1916611841 = f_mostrar_botones({});
						} catch (ee) {}
					}
					/** 
					 * Enviamos a la pantalla de menu principal 
					 */
					Alloy.createController("menu_index", {}).getView().open();
				} else {
					var ID_204306486 = null;
					if ('mostrar_botones' in require('funciones')) {
						ID_204306486 = require('funciones').mostrar_botones({});
					} else {
						try {
							ID_204306486 = f_mostrar_botones({});
						} catch (ee) {}
					}
					var preguntarAlerta14_opts = [L('x1518866076_traducir', 'Aceptar')];
					var preguntarAlerta14 = Ti.UI.createAlertDialog({
						title: L('x57652245_traducir', 'Alerta'),
						message: '' + String.format(L('x921749426_traducir', 'Error %1$s'), elemento.mensaje.toString()) + '',
						buttonNames: preguntarAlerta14_opts
					});
					preguntarAlerta14.addEventListener('click', function(e) {
						var res = preguntarAlerta14_opts[e.index];
						res = null;

						e.source.removeEventListener("click", arguments.callee);
					});
					preguntarAlerta14.show();
				}
				elemento = null, valor = null;
			};

			consultarURL4.error = function(e) {
				var elemento = e,
					valor = e;
				var ID_1229908354 = null;
				if ('mostrar_botones' in require('funciones')) {
					ID_1229908354 = require('funciones').mostrar_botones({});
				} else {
					try {
						ID_1229908354 = f_mostrar_botones({});
					} catch (ee) {}
				}
				var preguntarAlerta15_opts = [L('x1518866076_traducir', 'Aceptar')];
				var preguntarAlerta15 = Ti.UI.createAlertDialog({
					title: L('x57652245_traducir', 'Alerta'),
					message: L('x954934616_traducir', 'Problema de conexion por favor intentar despues'),
					buttonNames: preguntarAlerta15_opts
				});
				preguntarAlerta15.addEventListener('click', function(e) {
					var suu = preguntarAlerta15_opts[e.index];
					suu = null;

					e.source.removeEventListener("click", arguments.callee);
				});
				preguntarAlerta15.show();
				elemento = null, valor = null;
			};
			require('helper').ajaxUnico('consultarURL4', '' + String.format(L('x3323641845', '%1$sselectores'), url_server.toString()) + '', 'POST', {
				pais: pais[0].nombre,
				destino: selectores_fechas.destino.fecha,
				compania: selectores_fechas.compania.fecha,
				entidad_financiera: selectores_fechas.entidad_financiera.fecha,
				tipo_siniestro: selectores_fechas.tipo_siniestro.fecha,
				estructura_soportante: selectores_fechas.estructura_soportante.fecha,
				muros_tabiques: selectores_fechas.muros_tabiques.fecha,
				entrepisos: selectores_fechas.entrepisos.fecha,
				pavimento: selectores_fechas.pavimento.fecha,
				estructura_cubierta: selectores_fechas.estructura_cubierta.fecha,
				cubierta: selectores_fechas.cubierta.fecha,
				partida: selectores_fechas.partida.fecha,
				bienes: selectores_fechas.bienes.fecha,
				monedas: selectores_fechas.monedas.fecha,
				marcas: selectores_fechas.marcas.fecha
			}, 15000, consultarURL4);
			if (Ti.App.deployType != 'production') console.log('detalle pais', {
				"asd": pais
			});
		} else {
			var consultarModelo3_i = Alloy.createCollection('pais');
			var consultarModelo3_i_where = '';
			consultarModelo3_i.fetch();
			var paises = require('helper').query2array(consultarModelo3_i);
			if (Ti.App.deployType != 'production') console.log('hubo un error, no se encontro pais, dump dice', {
				"paises": paises
			});
		}
	}
	return null;
};
var f_mostrar_botones = function(x_params) {
	/** 
	 * Mostramos el boton registrar 
	 */
	var vista7_visible = true;

	if (vista7_visible == 'si') {
		vista7_visible = true;
	} else if (vista7_visible == 'no') {
		vista7_visible = false;
	}
	$.vista7.setVisible(vista7_visible);

	/** 
	 * Detenemos animacion progreso de registro 
	 */
	$.progreso.hide();
	/** 
	 * Mostramos boton entrar 
	 */

	$.widgetBotonlargo.detener_progreso({});
	return null;
};
var f_ocultar_botones = function(x_params) {
	/** 
	 * Ocultamos boton registrar 
	 */
	var vista7_visible = false;

	if (vista7_visible == 'si') {
		vista7_visible = true;
	} else if (vista7_visible == 'no') {
		vista7_visible = false;
	}
	$.vista7.setVisible(vista7_visible);

	/** 
	 * Iniciamos animacion progreso de registro 
	 */
	$.progreso.show();
	/** 
	 * Ocultamos boton entrar 
	 */

	$.widgetBotonlargo.iniciar_progreso({});
	return null;
};

if (OS_IOS || OS_ANDROID) {
	$.PORTADA.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}
$.PORTADA.open();