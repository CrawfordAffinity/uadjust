var _bind4section = {};
var _list_templates = {};

var _activity;
if (OS_ANDROID) {
	_activity = $.EDITAR.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'EDITAR';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.EDITAR_window.addEventListener('open', function(e) {
		abx.setStatusbarColor("#000000");
		abx.setBackgroundColor("white");
	});
}

function Click_vista(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.EDITAR.close();

}

function Click_vista2(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var seleccionado = ('seleccionado' in require('vars')) ? require('vars')['seleccionado'] : '';
	var fueraciudad = ('fueraciudad' in require('vars')) ? require('vars')['fueraciudad'] : '';
	var fuerapais = ('fuerapais' in require('vars')) ? require('vars')['fuerapais'] : '';
	var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
	var registro = _.extend(registro, {
		disponibilidad: seleccionado,
		disp_viajar_ciudad: fueraciudad,
		disp_viajar_pais: fuerapais
	});
	require('vars')['registro'] = registro;
	if (seleccionado == 1 || seleccionado == '1') {
		if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
			var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
			var registro = _.extend(registro, {
				disp_horas: '',
				d1: 1,
				d2: 1,
				d3: 1,
				d4: 1,
				d5: 1,
				d6: 1,
				d7: 1
			});
			require('vars')['registro'] = registro;
			$.EDITAR.close();
			var consultarModelo_i = Alloy.createCollection('inspectores');
			var consultarModelo_i_where = '';
			consultarModelo_i.fetch();
			var inspector_list = require('helper').query2array(consultarModelo_i);
			inspector = inspector_list[0];
			var fueraciudad = ('fueraciudad' in require('vars')) ? require('vars')['fueraciudad'] : '';
			var fuerapais = ('fuerapais' in require('vars')) ? require('vars')['fuerapais'] : '';
			var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
			var consultarURL = {};

			consultarURL.success = function(e) {
				var elemento = e,
					valor = e;
				if (Ti.App.deployType != 'production') console.log('Mi resultado es', {
					"elemento": elemento
				});
				if (elemento.error == 0 || elemento.error == '0') {
					var consultarModelo2_i = Alloy.createCollection('inspectores');
					var consultarModelo2_i_where = '';
					consultarModelo2_i.fetch();
					var inspector_list = require('helper').query2array(consultarModelo2_i);
					var fueraciudad = ('fueraciudad' in require('vars')) ? require('vars')['fueraciudad'] : '';
					var seleccionado = ('seleccionado' in require('vars')) ? require('vars')['seleccionado'] : '';
					var fuerapais = ('fuerapais' in require('vars')) ? require('vars')['fuerapais'] : '';
					var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
					var db = Ti.Database.open(consultarModelo2_i.config.adapter.db_name);
					if (consultarModelo2_i_where == '') {
						var sql = 'UPDATE ' + consultarModelo2_i.config.adapter.collection_name + ' SET disponibilidad_viajar_pais=\'' + fuerapais + '\'';
					} else {
						var sql = 'UPDATE ' + consultarModelo2_i.config.adapter.collection_name + ' SET disponibilidad_viajar_pais=\'' + fuerapais + '\' WHERE ' + consultarModelo2_i_where;
					}
					db.execute(sql);
					if (consultarModelo2_i_where == '') {
						var sql = 'UPDATE ' + consultarModelo2_i.config.adapter.collection_name + ' SET disponibilidad_viajar_ciudad=\'' + fueraciudad + '\'';
					} else {
						var sql = 'UPDATE ' + consultarModelo2_i.config.adapter.collection_name + ' SET disponibilidad_viajar_ciudad=\'' + fueraciudad + '\' WHERE ' + consultarModelo2_i_where;
					}
					db.execute(sql);
					if (consultarModelo2_i_where == '') {
						var sql = 'UPDATE ' + consultarModelo2_i.config.adapter.collection_name + ' SET disponibilidad=\'' + 1 + '\'';
					} else {
						var sql = 'UPDATE ' + consultarModelo2_i.config.adapter.collection_name + ' SET disponibilidad=\'' + 1 + '\' WHERE ' + consultarModelo2_i_where;
					}
					db.execute(sql);
					if (consultarModelo2_i_where == '') {
						var sql = 'UPDATE ' + consultarModelo2_i.config.adapter.collection_name + ' SET d1=\'' + 1 + '\'';
					} else {
						var sql = 'UPDATE ' + consultarModelo2_i.config.adapter.collection_name + ' SET d1=\'' + 1 + '\' WHERE ' + consultarModelo2_i_where;
					}
					db.execute(sql);
					if (consultarModelo2_i_where == '') {
						var sql = 'UPDATE ' + consultarModelo2_i.config.adapter.collection_name + ' SET d2=\'' + 1 + '\'';
					} else {
						var sql = 'UPDATE ' + consultarModelo2_i.config.adapter.collection_name + ' SET d2=\'' + 1 + '\' WHERE ' + consultarModelo2_i_where;
					}
					db.execute(sql);
					if (consultarModelo2_i_where == '') {
						var sql = 'UPDATE ' + consultarModelo2_i.config.adapter.collection_name + ' SET d3=\'' + 1 + '\'';
					} else {
						var sql = 'UPDATE ' + consultarModelo2_i.config.adapter.collection_name + ' SET d3=\'' + 1 + '\' WHERE ' + consultarModelo2_i_where;
					}
					db.execute(sql);
					if (consultarModelo2_i_where == '') {
						var sql = 'UPDATE ' + consultarModelo2_i.config.adapter.collection_name + ' SET d4=\'' + 1 + '\'';
					} else {
						var sql = 'UPDATE ' + consultarModelo2_i.config.adapter.collection_name + ' SET d4=\'' + 1 + '\' WHERE ' + consultarModelo2_i_where;
					}
					db.execute(sql);
					if (consultarModelo2_i_where == '') {
						var sql = 'UPDATE ' + consultarModelo2_i.config.adapter.collection_name + ' SET d5=\'' + 1 + '\'';
					} else {
						var sql = 'UPDATE ' + consultarModelo2_i.config.adapter.collection_name + ' SET d5=\'' + 1 + '\' WHERE ' + consultarModelo2_i_where;
					}
					db.execute(sql);
					if (consultarModelo2_i_where == '') {
						var sql = 'UPDATE ' + consultarModelo2_i.config.adapter.collection_name + ' SET d6=\'' + 1 + '\'';
					} else {
						var sql = 'UPDATE ' + consultarModelo2_i.config.adapter.collection_name + ' SET d6=\'' + 1 + '\' WHERE ' + consultarModelo2_i_where;
					}
					db.execute(sql);
					if (consultarModelo2_i_where == '') {
						var sql = 'UPDATE ' + consultarModelo2_i.config.adapter.collection_name + ' SET d7=\'' + 1 + '\'';
					} else {
						var sql = 'UPDATE ' + consultarModelo2_i.config.adapter.collection_name + ' SET d7=\'' + 1 + '\' WHERE ' + consultarModelo2_i_where;
					}
					db.execute(sql);
					if (consultarModelo2_i_where == '') {
						var sql = 'UPDATE ' + consultarModelo2_i.config.adapter.collection_name + ' SET disponibilidad_horas=\'' + registro.disp_horas + '\'';
					} else {
						var sql = 'UPDATE ' + consultarModelo2_i.config.adapter.collection_name + ' SET disponibilidad_horas=\'' + registro.disp_horas + '\' WHERE ' + consultarModelo2_i_where;
					}
					db.execute(sql);
					db.close();
				}
				elemento = null, valor = null;
			};

			consultarURL.error = function(e) {
				var elemento = e,
					valor = e;
				if (Ti.App.deployType != 'production') console.log('Hubo un error', {
					"elemento": elemento
				});
				elemento = null, valor = null;
			};
			require('helper').ajaxUnico('consultarURL', '' + String.format(L('x2304081372', '%1$seditarPerfilTipo2'), url_server.toString()) + '', 'POST', {
				tipo_cambio: 2,
				id_inspector: inspector.id_server,
				disponibilidad_viajar_pais: fuerapais,
				disponibilidad_viajar_ciudad: fueraciudad,
				d1: 1,
				d2: 1,
				d3: 1,
				d4: 1,
				d5: 1,
				d6: 1,
				d7: 1,
				disponibilidad: 1,
				disponibilidad_horas: registro.disp_horas
			}, 15000, consultarURL);
		} else {
			var preguntarAlerta_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta = Ti.UI.createAlertDialog({
				title: L('x3071602690_traducir', 'No hay internet'),
				message: L('x2846685350_traducir', 'No se pueden efectuar los cambios sin conexion.'),
				buttonNames: preguntarAlerta_opts
			});
			preguntarAlerta.addEventListener('click', function(e) {
				var suu = preguntarAlerta_opts[e.index];
				suu = null;

				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta.show();
		}
	} else {
		if ("EDITAR" in Alloy.Globals) {
			Alloy.Globals["EDITAR"].openWindow(Alloy.createController("editar_part-time", {}).getView());
		} else {
			Alloy.Globals["EDITAR"] = $.EDITAR;
			Alloy.Globals["EDITAR"].openWindow(Alloy.createController("editar_part-time", {}).getView());
		}

		nulo = null;
	}
}

$.widgetHeader.init({
	titulo: L('x1574293037_traducir', 'Editar Disponibilidad'),
	__id: 'ALL1222663545',
	avance: L('', '')
});



function Change_picker(e) {

	e.cancelBubble = true;
	var elemento = e;
	var _columna = e.columnIndex;
	var columna = e.columnIndex + 1;
	var _fila = e.rowIndex;
	var fila = e.rowIndex + 1;
	if (Ti.App.deployType != 'production') console.log('seleccionado', {
		"fila": _fila
	});
	require('vars')['seleccionado'] = _fila;
	if (_fila == 0 || _fila == '0') {
		$.Siguiente.setTitle('Siguiente');

		var Siguiente_ancho = '-';

		if (Siguiente_ancho == '*') {
			Siguiente_ancho = Ti.UI.FILL;
		} else if (Siguiente_ancho == '-') {
			Siguiente_ancho = Ti.UI.SIZE;
		} else if (!isNaN(Siguiente_ancho)) {
			Siguiente_ancho = Siguiente_ancho + 'dp';
		}
		$.Siguiente.setWidth(Siguiente_ancho);

	} else {
		$.Siguiente.setTitle('Guardar');

	}
}

function Change_ID_1568773770(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	if (elemento == true || elemento == 'true') {
		$.NO.setText('SI');

		require('vars')['fueraciudad'] = L('x2212294583', '1');
	} else {
		$.NO.setText('NO');

		require('vars')['fueraciudad'] = L('x4108050209', '0');
	}
	elemento = null;

}

function Change_ID_952608080(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	if (elemento == true || elemento == 'true') {
		$.NO3.setText('SI');

		require('vars')['fuerapais'] = L('x2212294583', '1');
	} else {
		$.NO3.setText('NO');

		require('vars')['fuerapais'] = L('x4108050209', '0');
	}
	elemento = null;

}

(function() {
	require('vars')['fueraciudad'] = L('x4108050209', '0');
	require('vars')['fuerapais'] = L('x4108050209', '0');
	require('vars')['seleccionado'] = L('x4108050209', '0');

	_my_events['_close_editar,ID_518578093'] = function(evento) {
		if (Ti.App.deployType != 'production') console.log('estamos en el _close_editar', {});
		$.EDITAR.close();
	};
	Alloy.Events.on('_close_editar', _my_events['_close_editar,ID_518578093']);
	var consultarModelo3_i = Alloy.createCollection('inspectores');
	var consultarModelo3_i_where = '';
	consultarModelo3_i.fetch();
	var inspector_list = require('helper').query2array(consultarModelo3_i);
	if (inspector_list.length == 0 || inspector_list.length == '0') {} else {
		inspector = inspector_list[0];
		if (Ti.App.deployType != 'production') console.log('completando datos previos para editar disponibilidad de inspector', {
			"inspector": inspector
		});
		var picker;
		picker = $.picker;
		picker.setSelectedRow(0, inspector.disponibilidad);
		require('vars')['fueraciudad'] = inspector.disponibilidad_viajar_ciudad;
		if (inspector.disponibilidad_viajar_ciudad == 1 || inspector.disponibilidad_viajar_ciudad == '1') {
			$.ID_1568773770.setValue(true);

		} else {
			$.ID_1568773770.setValue('false');

		}
		require('vars')['fuerapais'] = inspector.disponibilidad_viajar_pais;
		if (inspector.disponibilidad_viajar_pais == 1 || inspector.disponibilidad_viajar_pais == '1') {
			$.ID_952608080.setValue(true);

		} else {
			$.ID_952608080.setValue('false');

		}
		if (Ti.App.deployType != 'production') console.log('valores configurados en editar disponibilidad', {
			"disponibilidad_pais": inspector.disponibilidad_viajar_pais,
			"disponibilidad_ciudad": inspector.disponibilidad_viajar_ciudad
		});
	}
})();

if (OS_IOS || OS_ANDROID) {
	$.EDITAR.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}
//$.EDITAR.open();
