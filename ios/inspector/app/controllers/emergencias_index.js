var _bind4section = {
	"ref1": "emergencia"
};
var _list_templates = {
	"tarea": {
		"vista9": {},
		"vista10": {},
		"vista11": {},
		"vista3": {
			"id_server": "{id_server}"
		},
		"Label2": {
			"text": "{direccion}"
		},
		"Adistance": {
			"text": "a {distance} km"
		},
		"vista7": {},
		"vista5": {},
		"vista8": {},
		"Label": {
			"text": "{id}"
		},
		"Label4": {
			"text": "{nivel_2}, {pais}"
		},
		"vista4": {},
		"vista6": {},
		"Label3": {
			"text": "{comuna}"
		}
	}
};

var _activity;
if (OS_ANDROID) {
	_activity = $.inicio.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'inicio';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.inicio.addEventListener('open', function(e) {});
}


var consultarModelo_like = function(search) {
	if (typeof search !== 'string' || this === null) {
		return false;
	}
	search = search.replace(new RegExp("([\\.\\\\\\+\\*\\?\\[\\^\\]\\$\\(\\)\\{\\}\\=\\!\\<\\>\\|\\:\\-])", "g"), "\\$1");
	search = search.replace(/%/g, '.*').replace(/_/g, '.');
	return RegExp('^' + search + '$', 'gi').test(this);
};
var consultarModelo_filter = function(coll) {
	var filtered = coll.filter(function(m) {
		var _tests = [],
			_all_true = false,
			model = m.toJSON();
		_tests.push((model.perfil == 'casa'));
		var _all_true_s = _.uniq(_tests);
		_all_true = (_all_true_s.length == 1 && _all_true_s[0] == true) ? true : false;
		return _all_true;
	});
	filtered = _.toArray(filtered);
	var ordered = _.sortBy(filtered, 'distancia_2');
	return ordered;
};
var consultarModelo_update = function(e) {};
_.defer(function() {
	Alloy.Collections.emergencia.fetch();
});
Alloy.Collections.emergencia.on('add change delete', function(ee) {
	consultarModelo_update(ee);
});
var consultarModelo_transform = function(model) {
	var fila = model.toJSON();
	if ((_.isObject(fila.nivel_2) || _.isString(fila.nivel_2)) && _.isEmpty(fila.nivel_2)) {
		/** 
		 * El nivel_2 (region) no deber&#237;a estar nunca en blanco, por lo que marcamos los campos comuna y ciudad con guiones para marcar falla en datos. 
		 */
		var fila = _.extend(fila, {
			comuna: fila.nivel_1
		});
	} else if ((_.isObject(fila.nivel_3) || _.isString(fila.nivel_3)) && _.isEmpty(fila.nivel_3)) {
		/** 
		 * El nivel_3 (ciudad) tampoco deberia estar nunca en blanco. 
		 */
		var fila = _.extend(fila, {
			comuna: fila.nivel_2
		});
	} else if ((_.isObject(fila.nivel_4) || _.isString(fila.nivel_4)) && _.isEmpty(fila.nivel_4)) {
		var fila = _.extend(fila, {
			comuna: fila.nivel_3
		});
	} else if ((_.isObject(fila.nivel_5) || _.isString(fila.nivel_5)) && _.isEmpty(fila.nivel_5)) {
		var fila = _.extend(fila, {
			comuna: fila.nivel_4
		});
	} else {
		var fila = _.extend(fila, {
			comuna: fila.nivel_5
		});
	}
	return fila;
};
Alloy.Collections.emergencia.fetch();

function Click_tabbar(e) {

	e.cancelBubble = true;
	if ('index' in e) {
		var elemento = e.source.getLabels()[e.index];
		var valor = elemento.title;
		if (Ti.App.deployType != 'production') console.log('Cambiado', {
			"evento": e,
			"elemento": elemento
		});
		if (e.index == 0 || e.index == '0') {
			consultarModelo_filter = function(coll) {
				var filtered = coll.filter(function(m) {
					var _tests = [],
						_all_true = false,
						model = m.toJSON();
					_tests.push((model.perfil == 'casa'));
					var _all_true_s = _.uniq(_tests);
					_all_true = (_all_true_s.length == 1 && _all_true_s[0] == true) ? true : false;
					return _all_true;
				});
				filtered = _.toArray(filtered);
				var ordered = _.sortBy(filtered, 'distancia_2');
				return ordered;
			};
			_.defer(function() {
				Alloy.Collections.emergencia.fetch();
			});
		} else {
			consultarModelo_filter = function(coll) {
				var filtered = coll.filter(function(m) {
					var _tests = [],
						_all_true = false,
						model = m.toJSON();
					_tests.push((model.perfil == 'ubicacion'));
					var _all_true_s = _.uniq(_tests);
					_all_true = (_all_true_s.length == 1 && _all_true_s[0] == true) ? true : false;
					return _all_true;
				});
				filtered = _.toArray(filtered);
				var ordered = _.sortBy(filtered, 'distancia_2');
				return ordered;
			};
			_.defer(function() {
				Alloy.Collections.emergencia.fetch();
			});
		}
		elemento = null, valor = null;
	}

}

function Click_vista(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.iconoRefresh.setColor('#ff0033');

	var gps_error = ('gps_error' in require('vars')) ? require('vars')['gps_error'] : '';
	if (gps_error == true || gps_error == 'true') {
		$.iconoRefresh.setColor('#2d9edb');

		var preguntarAlerta_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta = Ti.UI.createAlertDialog({
			title: L('x57652245_traducir', 'Alerta'),
			message: L('x3873304135_traducir', 'No se pudo obtener la ubicación'),
			buttonNames: preguntarAlerta_opts
		});
		preguntarAlerta.addEventListener('click', function(e) {
			var errori = preguntarAlerta_opts[e.index];
			errori = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta.show();
	}
	var obteneremergencias = ('obteneremergencias' in require('vars')) ? require('vars')['obteneremergencias'] : '';
	if (obteneremergencias == true || obteneremergencias == 'true') {
		if (Ti.App.deployType != 'production') console.log('esta actualizando', {});
	} else {
		var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
		require('vars')['obteneremergencias'] = L('x4261170317', 'true');
		if (Ti.App.deployType != 'production') console.log('llamando a servidor obtenerEmergencias', {});
		var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
		var gps_latitud = ('gps_latitud' in require('vars')) ? require('vars')['gps_latitud'] : '';
		var gps_longitud = ('gps_longitud' in require('vars')) ? require('vars')['gps_longitud'] : '';
		var consultarURL = {};
		consultarURL.success = function(e) {
			var elemento = e,
				valor = e;
			var eliminarModelo_i = Alloy.Collections.emergencia;
			var sql = "DELETE FROM " + eliminarModelo_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo_i.trigger('remove');
			$.iconoRefresh.setColor('#2d9edb');

			if (elemento.error != 0 && elemento.error != '0') {
				var preguntarAlerta2_opts = [L('x1518866076_traducir', 'Aceptar')];
				var preguntarAlerta2 = Ti.UI.createAlertDialog({
					title: L('x57652245_traducir', 'Alerta'),
					message: L('x344160560_traducir', 'Error con el servidor'),
					buttonNames: preguntarAlerta2_opts
				});
				preguntarAlerta2.addEventListener('click', function(e) {
					var errori = preguntarAlerta2_opts[e.index];
					errori = null;
					e.source.removeEventListener("click", arguments.callee);
				});
				preguntarAlerta2.show();
			} else {
				if (Ti.App.deployType != 'production') console.log('Insertando emergencias', {
					"elemento": elemento
				});
				var elemento_perfil = elemento.perfil;
				var insertarModelo_m = Alloy.Collections.emergencia;
				var db_insertarModelo = Ti.Database.open(insertarModelo_m.config.adapter.db_name);
				db_insertarModelo.execute('BEGIN');
				_.each(elemento_perfil, function(insertarModelo_fila, pos) {
					db_insertarModelo.execute('INSERT INTO emergencia (fecha_tarea, id_inspeccion, nivel_2, id_asegurado, comentario_can_o_rech, asegurado_tel_fijo, estado_tarea, bono, id_inspector, asegurado_codigo_identificador, lat, nivel_1, asegurado_nombre, pais, direccion, asegurador, fecha_ingreso, fecha_siniestro, nivel_1_texto, distance, nivel_4, perfil, asegurado_id, pais_texto, id_server, categoria, nivel_3, asegurado_correo, num_caso, lon, asegurado_tel_movil, distancia_2, nivel_5, tipo_tarea) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', insertarModelo_fila.fecha_tarea, insertarModelo_fila.id_inspeccion, insertarModelo_fila.nivel_2, insertarModelo_fila.id_asegurado, insertarModelo_fila.comentario_can_o_rech, insertarModelo_fila.asegurado_tel_fijo, insertarModelo_fila.estado_tarea, insertarModelo_fila.bono, insertarModelo_fila.id_inspector, insertarModelo_fila.asegurado_codigo_identificador, insertarModelo_fila.lat, insertarModelo_fila.nivel_1, insertarModelo_fila.asegurado_nombre, insertarModelo_fila.pais, insertarModelo_fila.direccion, insertarModelo_fila.asegurador, insertarModelo_fila.fecha_ingreso, insertarModelo_fila.fecha_siniestro, insertarModelo_fila.nivel_1_google, insertarModelo_fila.distancia, insertarModelo_fila.nivel_4, 'casa', insertarModelo_fila.asegurado_id, insertarModelo_fila.pais_, insertarModelo_fila.id, insertarModelo_fila.categoria, insertarModelo_fila.nivel_3, insertarModelo_fila.asegurado_correo, insertarModelo_fila.num_caso, insertarModelo_fila.lon, insertarModelo_fila.asegurado_tel_movil, insertarModelo_fila.distancia_2, insertarModelo_fila.nivel_5, insertarModelo_fila.tipo_tarea);
				});
				db_insertarModelo.execute('COMMIT');
				db_insertarModelo.close();
				db_insertarModelo = null;
				insertarModelo_m.trigger('change');
				var elemento_ubicacion = elemento.ubicacion;
				var insertarModelo2_m = Alloy.Collections.emergencia;
				var db_insertarModelo2 = Ti.Database.open(insertarModelo2_m.config.adapter.db_name);
				db_insertarModelo2.execute('BEGIN');
				_.each(elemento_ubicacion, function(insertarModelo2_fila, pos) {
					db_insertarModelo2.execute('INSERT INTO emergencia (fecha_tarea, id_inspeccion, id_asegurado, nivel_2, comentario_can_o_rech, asegurado_tel_fijo, estado_tarea, bono, id_inspector, asegurado_codigo_identificador, lat, nivel_1, asegurado_nombre, pais, direccion, asegurador, fecha_ingreso, fecha_siniestro, nivel_1_texto, distance, nivel_4, perfil, asegurado_id, pais_texto, id_server, categoria, nivel_3, asegurado_correo, num_caso, lon, asegurado_tel_movil, distancia_2, tipo_tarea, nivel_5) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', insertarModelo2_fila.fecha_tarea, insertarModelo2_fila.id_inspeccion, insertarModelo2_fila.id_asegurado, insertarModelo2_fila.nivel_2, insertarModelo2_fila.comentario_can_o_rech, insertarModelo2_fila.asegurado_tel_fijo, insertarModelo2_fila.estado_tarea, insertarModelo2_fila.bono, insertarModelo2_fila.id_inspector, insertarModelo2_fila.asegurado_codigo_identificador, insertarModelo2_fila.lat, insertarModelo2_fila.nivel_1, insertarModelo2_fila.asegurado_nombre, insertarModelo2_fila.pais, insertarModelo2_fila.direccion, insertarModelo2_fila.asegurador, insertarModelo2_fila.fecha_ingreso, insertarModelo2_fila.fecha_siniestro, insertarModelo2_fila.nivel_1_google, insertarModelo2_fila.distancia, insertarModelo2_fila.nivel_4, 'ubicacion', insertarModelo2_fila.asegurado_id, insertarModelo2_fila.pais_, insertarModelo2_fila.id, insertarModelo2_fila.categoria, insertarModelo2_fila.nivel_3, insertarModelo2_fila.asegurado_correo, insertarModelo2_fila.num_caso, insertarModelo2_fila.lon, insertarModelo2_fila.asegurado_tel_movil, insertarModelo2_fila.distancia_2, insertarModelo2_fila.tipo_tarea, insertarModelo2_fila.nivel_5);
				});
				db_insertarModelo2.execute('COMMIT');
				db_insertarModelo2.close();
				db_insertarModelo2 = null;
				insertarModelo2_m.trigger('change');
				require('vars')['obteneremergencias'] = L('x734881840_traducir', 'false');
				_.defer(function() {
					Alloy.Collections.emergencia.fetch();
				});
			}
			elemento = null, valor = null;
		};
		consultarURL.error = function(e) {
			var elemento = e,
				valor = e;
			if (Ti.App.deployType != 'production') console.log('Hubo un error', {});
			$.iconoRefresh.setColor('#2d9edb');

			var preguntarAlerta3_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta3 = Ti.UI.createAlertDialog({
				title: L('x57652245_traducir', 'Alerta'),
				message: L('x3389756565_traducir', 'No se pudo conectar con el servidor'),
				buttonNames: preguntarAlerta3_opts
			});
			preguntarAlerta3.addEventListener('click', function(e) {
				var errori = preguntarAlerta3_opts[e.index];
				errori = null;
				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta3.show();
			elemento = null, valor = null;
		};
		require('helper').ajaxUnico('consultarURL', '' + String.format(L('x1578679327', '%1$sobtenerEmergencias'), url_server.toString()) + '', 'POST', {
			id_inspector: inspector.id_server,
			lat: gps_latitud,
			lon: gps_longitud
		}, 15000, consultarURL);
	}
}

$.widgetSininternet.init({
	titulo: L('x2828751865_traducir', '¡ESTAS SIN CONEXION!'),
	__id: 'ALL1512316616',
	mensaje: L('x1855928898_traducir', 'No puedes ver las tareas de emergencias de hoy'),
	onon: on_widgetSininternet,
	onoff: Off_widgetSininternet
});

function on_widgetSininternet(e) {

	var evento = e;
	var vista2_visible = true;

	if (vista2_visible == 'si') {
		vista2_visible = true;
	} else if (vista2_visible == 'no') {
		vista2_visible = false;
	}
	$.vista2.setVisible(vista2_visible);


}

function Off_widgetSininternet(e) {

	var evento = e;
	var vista2_visible = false;

	if (vista2_visible == 'si') {
		vista2_visible = true;
	} else if (vista2_visible == 'no') {
		vista2_visible = false;
	}
	$.vista2.setVisible(vista2_visible);


}

function Itemclick_listado(e) {

	e.cancelBubble = true;
	var objeto = e.section.getItemAt(e.itemIndex);
	var modelo = {},
		_modelo = [];
	var fila = {},
		fila_bak = {},
		info = {
			_template: objeto.template,
			_what: [],
			_seccion_ref: e.section.getHeaderTitle(),
			_model_id: -1
		},
		_tmp = {
			objmap: {}
		};
	if ('itemId' in e) {
		info._model_id = e.itemId;
		modelo._id = info._model_id;
		if (info._seccion_ref != '' && info._seccion_ref in _bind4section) {
			modelo._collection = _bind4section[info._seccion_ref];
			_tmp._coll = modelo._collection;
		}
	}
	var findVariables = require('fvariables');
	_.each(_list_templates[info._template], function(obj_id, id) {
		_.each(obj_id, function(valor, prop) {
			var llaves = findVariables(valor, '{', '}');
			_.each(llaves, function(llave) {
				_tmp.objmap[llave] = {
					id: id,
					prop: prop
				};
				fila[llave] = objeto[id][prop];
				if (id == e.bindId) info._what.push(llave);
			});
		});
	});
	info._what = info._what.join(',');
	fila_bak = JSON.parse(JSON.stringify(fila));
	if (Ti.App.deployType != 'production') console.log('Mi objeto emergencias es', {
		"info": info,
		"objeto": fila
	});
	/** 
	 * Ocupamos esta variable para impedir que la pantalla siguente se cargue dos veces 
	 */
	var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
	if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
		require('vars')['var_abriendo'] = L('x4261170317', 'true');
		var nulo = Alloy.createController("tomartarea_index", {
			'_objeto': fila,
			'_id': fila.id,
			'_tipo': 'emergencias',
			'__master_model': (typeof modelo !== 'undefined') ? modelo : {},
			'__modelo': (typeof _modelo !== 'undefined' && _modelo.length > 0) ? _modelo[0] : {}
		}).getView();
		nulo.open({
			modal: true
		});
		nulo = null;
	}
	_tmp.changed = false;
	_tmp.diff_keys = [];
	_.each(fila, function(value1, prop) {
		var had_samekey = false;
		_.each(fila_bak, function(value2, prop2) {
			if (prop == prop2 && value1 == value2) {
				had_samekey = true;
			} else if (!_.has(fila_bak, prop) || !_.has(fila, prop2)) {
				has_samekey = true;
			}
		});
		if (!had_samekey) _tmp.diff_keys.push(prop);
	});
	if (_tmp.diff_keys.length > 0) _tmp.changed = true;
	if (_tmp.changed == true) {
		_.each(_tmp.diff_keys, function(llave) {
			objeto[_tmp.objmap[llave].id][_tmp.objmap[llave].prop] = fila[llave];
		});
		e.section.updateItemAt(e.itemIndex, objeto);
	}

}

(function() {
	_my_events['_refrescar_tareas,ID_1146039108'] = function(evento) {
		/** 
		 * Revisamos si hay inspecciones en curso, si no hay ninguna, se refresca la lista 
		 */
		var inspeccion_encurso = ('inspeccion_encurso' in require('vars')) ? require('vars')['inspeccion_encurso'] : '';
		if (inspeccion_encurso == false || inspeccion_encurso == 'false') {
			_.defer(function() {
				Alloy.Collections.emergencia.fetch();
			});
		}
	};
	Alloy.Events.on('_refrescar_tareas', _my_events['_refrescar_tareas,ID_1146039108']);
})();

if (OS_IOS || OS_ANDROID) {
	$.inicio.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}
//$.inicio.open();
