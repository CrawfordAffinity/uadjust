var _bind4section = {};
var _list_templates = {};

var _activity;
if (OS_ANDROID) {
	_activity = $.ENROLAMIENTO3.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'ENROLAMIENTO3';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.ENROLAMIENTO3.addEventListener('open', function(e) {
		abx.setStatusbarColor("#000000");
		abx.setBackgroundColor("white");
	});
}
$.ENROLAMIENTO3.orientationModes = [Titanium.UI.PORTRAIT];

function Click_vista18(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.ENROLAMIENTO3.close();

}

$.widgetHeader3.init({
	titulo: L('x233750493_traducir', 'PARTE 3: Domicilio'),
	__id: 'ALL1399189593',
	avance: L('x80359215_traducir', '3/6'),
	onclick: Click_widgetHeader3
});

function Click_widgetHeader3(e) {

	var evento = e;
	$.Escribir.blur();
	$.EscribirNivel.blur();
	$.EscribirNivel2.blur();
	$.EscribirNivel3.blur();

}

$.widgetModal.init({
	titulo: L('x4288296688_traducir', 'REGION'),
	__id: 'ALL1379917834',
	left: 0,
	onrespuesta: Respuesta_widgetModal,
	campo: L('x147780672_traducir', 'Region'),
	onabrir: Abrir_widgetModal,
	right: 0,
	seleccione: L('x116943338_traducir', 'Seleccione region'),
	onafterinit: Afterinit_widgetModal
});

function Afterinit_widgetModal(e) {

	var evento = e;
	/** 
	 * Recuperamos la variable con los datos de las regiones y las cargamos en el widget 
	 */
	var data_region = ('data_region' in require('vars')) ? require('vars')['data_region'] : '';
	if (Ti.App.deployType != 'production') console.log('enviando data a widget', {
		"data": data_region
	});

	$.widgetModal.data({
		data: data_region
	});

}

function Abrir_widgetModal(e) {

	var evento = e;
	$.Escribir.blur();
	$.EscribirNivel.blur();
	$.EscribirNivel2.blur();
	$.EscribirNivel3.blur();

}

function Respuesta_widgetModal(e) {

	var evento = e;
	if (Ti.App.deployType != 'production') console.log('datos recibidos de modal', {
		"datos": evento
	});
	/** 
	 * Ponemos el texto de la region escogida 
	 */

	$.widgetModal.labels({
		valor: evento.valor
	});
	/** 
	 * Guardamos el id del valor seleccionado 
	 */
	require('vars')['data_nivel_1'] = evento.item;

}

$.widgetBotonlargo3.init({
	titulo: L('x1524107289_traducir', 'CONTINUAR'),
	__id: 'ALL337746182',
	onclick: Click_widgetBotonlargo3
});

function Click_widgetBotonlargo3(e) {

	var evento = e;
	/** 
	 * Obtenemos los valores de los campos 
	 */
	var direccion;
	direccion = $.EscribirDireccion.getValue();

	var data_nivel_2;
	data_nivel_2 = $.Escribir.getValue();

	var data_nivel_3;
	data_nivel_3 = $.EscribirNivel.getValue();

	var data_nivel_4;
	data_nivel_4 = $.EscribirNivel2.getValue();

	var data_nivel_5;
	data_nivel_5 = $.EscribirNivel3.getValue();

	var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
	/** 
	 * Creamos una variable para ver si falta algun dato por ser ingresado 
	 */

	var falta_algo =
		(data_nivel_2 == "" && pais.label_nivel2 != "") ||
		(data_nivel_3 == "" && pais.label_nivel3 != "") ||
		(data_nivel_4 == "" && pais.label_nivel4 != "") ||
		(data_nivel_5 == "" && pais.label_nivel5 != "") ||
		(direccion == "")
	var data_nivel_1 = ('data_nivel_1' in require('vars')) ? require('vars')['data_nivel_1'] : '';
	if ((_.isObject(data_nivel_1) || _.isString(data_nivel_1)) && _.isEmpty(data_nivel_1)) {
		var preguntarAlerta8_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta8 = Ti.UI.createAlertDialog({
			title: L('x2185084353_traducir', 'Atención'),
			message: L('x3693432715_traducir', 'Seleccione región'),
			buttonNames: preguntarAlerta8_opts
		});
		preguntarAlerta8.addEventListener('click', function(e) {
			var suu = preguntarAlerta8_opts[e.index];
			suu = null;

			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta8.show();
	} else if ((_.isObject(direccion) || _.isString(direccion)) && _.isEmpty(direccion)) {
		var preguntarAlerta9_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta9 = Ti.UI.createAlertDialog({
			title: L('x2185084353_traducir', 'Atención'),
			message: L('x3153806545_traducir', 'Ingrese dirección'),
			buttonNames: preguntarAlerta9_opts
		});
		preguntarAlerta9.addEventListener('click', function(e) {
			var suu = preguntarAlerta9_opts[e.index];
			suu = null;

			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta9.show();
	} else if (falta_algo == true || falta_algo == 'true') {
		var preguntarAlerta10_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta10 = Ti.UI.createAlertDialog({
			title: L('x2185084353_traducir', 'Atención'),
			message: L('x1330841706_traducir', 'Ingrese datos faltantes'),
			buttonNames: preguntarAlerta10_opts
		});
		preguntarAlerta10.addEventListener('click', function(e) {
			var suu = preguntarAlerta10_opts[e.index];
			suu = null;

			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta10.show();
	} else {
		var data_nivel_1 = ('data_nivel_1' in require('vars')) ? require('vars')['data_nivel_1'] : '';
		if (_.isString(data_nivel_1)) {
			/** 
			 * generamos la url para hacer la consulta a google 
			 */

			var url = data_nivel_1.replace(" ", "+");
			url = url + "+" + data_nivel_2.replace(" ", "+");
			url = url + "+" + data_nivel_3.replace(" ", "+");
			url = url + "+" + data_nivel_4.replace(" ", "+");
			url = url + "+" + data_nivel_5.replace(" ", "+");
			url = url + "+" + direccion.replace(" ", "+");
			url = url + "+" + pais.nombre;
			url2 = "https://maps.googleapis.com/maps/api/geocode/json?address=" + url
		} else {

			var url = data_nivel_1.valor.replace(" ", "+");
			url = url + "+" + data_nivel_2.replace(" ", "+");
			url = url + "+" + data_nivel_3.replace(" ", "+");
			url = url + "+" + data_nivel_4.replace(" ", "+");
			url = url + "+" + data_nivel_5.replace(" ", "+");
			url = url + "+" + direccion.replace(" ", "+");
			url = url + "+" + pais.nombre;
			url = encodeURI(url);
			url2 = "https://maps.googleapis.com/maps/api/geocode/json?address=" + url
		}
		if (Ti.App.deployType != 'production') console.log('Consultando a Google si direccion es valida', {
			"url": url2
		});

		$.widgetBotonlargo3.iniciar_progreso({});
		var consultarURL = {};

		consultarURL.success = function(e) {
			var elemento = e,
				valor = e;
			if (Ti.App.deployType != 'production') console.log('respuesta de google', {
				"datos": elemento
			});
			if (elemento.status == L('x3610695981_traducir', 'OK')) {
				if (elemento.results[0].formatted_address.toLowerCase().indexOf(direccion.toLowerCase()) != -1) {
					var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
					var data_nivel_1 = ('data_nivel_1' in require('vars')) ? require('vars')['data_nivel_1'] : '';
					/** 
					 * Agregamos los campos de los niveles a la variable del registro, diferenciamos si nivel1 es texto o id 
					 */
					var registro = _.extend(registro, {
						nivel_2: data_nivel_2,
						nivel_3: data_nivel_3,
						nivel_4: data_nivel_4,
						nivel_5: data_nivel_5
					});
					if (_.isString(data_nivel_1)) {
						/** 
						 * si la region fue ingresada a mano, asociamos el texto en vez del id_server 
						 */
						var registro = _.extend(registro, {
							nivel_1: data_nivel_1
						});
					} else {
						var registro = _.extend(registro, {
							nivel_1: data_nivel_1.id_server
						});
					}
					var registro = _.extend(registro, {
						latitude: elemento.results[0].geometry.location.lat,
						longitude: elemento.results[0].geometry.location.lng,
						correccion_direccion: 0,
						direccion: elemento.results[0].formatted_address.split(',')[0]
					});

					registro.google = JSON.stringify(elemento.results[0].address_components)
					/** 
					 * Guardamos los nuevos datos de registro 
					 */
					require('vars')['registro'] = registro;

					$.widgetBotonlargo3.detener_progreso({});
					/** 
					 * Guardamos y enviamos a la proxima pantalla, disponibilidad de trabajo 
					 */
					if ("ENROLAMIENTO" in Alloy.Globals) {
						Alloy.Globals["ENROLAMIENTO"].openWindow(Alloy.createController("disponibilidad_de_trabajo", {}).getView());
					} else {
						Alloy.Globals["ENROLAMIENTO"] = $.ENROLAMIENTO;
						Alloy.Globals["ENROLAMIENTO"].openWindow(Alloy.createController("disponibilidad_de_trabajo", {}).getView());
					}

				} else {
					var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
					/** 
					 * Agregamos los campos nuevos, y mostramos al usuario la direccion que encontro google 
					 */
					var registro = _.extend(registro, {
						nivel_2: data_nivel_2,
						nivel_3: data_nivel_3,
						nivel_4: data_nivel_4,
						nivel_5: data_nivel_5
					});

					registro.google = JSON.stringify(elemento.results[0].address_components)
					var registro = _.extend(registro, {
						latitude: elemento.results[0].geometry.location.lat,
						longitude: elemento.results[0].geometry.location.lng,
						correccion_direccion: 0,
						direccion: elemento.results[0].formatted_address.split(',')[0]
					});
					require('vars')['registro'] = registro;
					require('vars')['resultado'] = elemento.results[0];

					$.widgetBotonlargo3.detener_progreso({});
					preguntarAlerta12.setMessage(String.format(L('x1377491309_traducir', 'Es esta la correcta? %1$s'), elemento.results[0].formatted_address.toString()));

					preguntarAlerta12.show();
				}
			} else {
				/** 
				 * En el caso de la respuesta de google no sea ok, mostramos este mensaje 
				 */
				var preguntarAlerta11_opts = [L('x3610695981_traducir', 'OK')];
				var preguntarAlerta11 = Ti.UI.createAlertDialog({
					title: L('x3237162386_traducir', 'Atencion'),
					message: L('x3185903755_traducir', 'Error al verificar la dirección, reintente'),
					buttonNames: preguntarAlerta11_opts
				});
				preguntarAlerta11.addEventListener('click', function(e) {
					var xdd = preguntarAlerta11_opts[e.index];
					xdd = null;

					e.source.removeEventListener("click", arguments.callee);
				});
				preguntarAlerta11.show();

				$.widgetBotonlargo3.detener_progreso({});
			}
			elemento = null, valor = null;
		};

		consultarURL.error = function(e) {
			var elemento = e,
				valor = e;
			if (Ti.App.deployType != 'production') console.log('hubo error consultando google, asumimos direccion es correcta', {});

			$.widgetBotonlargo3.detener_progreso({});
			var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
			var data_nivel_1 = ('data_nivel_1' in require('vars')) ? require('vars')['data_nivel_1'] : '';
			var registro = _.extend(registro, {
				nivel_2: data_nivel_2,
				nivel_3: data_nivel_3,
				nivel_4: data_nivel_4,
				nivel_5: data_nivel_5,
				direccion: direccion,
				correccion_direccion: 1,
				google: '',
				longitude: '',
				latitude: ''
			});
			if (_.isString(data_nivel_1)) {
				/** 
				 * si la region fue ingresada a mano, asociamos el texto en vez del id_server 
				 */
				var registro = _.extend(registro, {
					nivel_1: data_nivel_1
				});
			} else {
				var registro = _.extend(registro, {
					nivel_1: data_nivel_1.id_server
				});
			}
			require('vars')['registro'] = registro;
			if ("ENROLAMIENTO" in Alloy.Globals) {
				Alloy.Globals["ENROLAMIENTO"].openWindow(Alloy.createController("disponibilidad_de_trabajo", {}).getView());
			} else {
				Alloy.Globals["ENROLAMIENTO"] = $.ENROLAMIENTO;
				Alloy.Globals["ENROLAMIENTO"].openWindow(Alloy.createController("disponibilidad_de_trabajo", {}).getView());
			}

			elemento = null, valor = null;
		};
		require('helper').ajaxUnico('consultarURL', 'https://maps.googleapis.com/maps/api/geocode/json?address=' + url + '&key=AIzaSyDdRVLOtyNvZUAleL0mDO-mAWd2M9AircQ', 'GET', {}, 15000, consultarURL);
	}
}

var preguntarAlerta12_opts = [L('x3827418516_traducir', 'Si'), L('x4063104189_traducir', 'No'), L('x2632684629_traducir', 'Continuar')];
var preguntarAlerta12 = Ti.UI.createAlertDialog({
	title: L('x4067820461_traducir', 'La direccion indicada no parece valida.\n ¿es esta la correcta?'),
	message: L('x2547889144', '-'),
	buttonNames: preguntarAlerta12_opts
});
preguntarAlerta12.addEventListener('click', function(e) {
	var resp_pregunta = preguntarAlerta12_opts[e.index];
	var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
	var data_nivel_1 = ('data_nivel_1' in require('vars')) ? require('vars')['data_nivel_1'] : '';
	var resultado = ('resultado' in require('vars')) ? require('vars')['resultado'] : '';
	if (_.isString(data_nivel_1)) {
		/** 
		 * si la region fue ingresada a mano, asociamos el texto en vez del id_server. Hasta ahora, eso no deberia suceder ya que solo almacenamos el id_server 
		 */
		var registro = _.extend(registro, {
			nivel_1: data_nivel_1
		});
	} else {
		var registro = _.extend(registro, {
			nivel_1: data_nivel_1.id_server
		});
	}
	if (resp_pregunta == L('x3827418516_traducir', 'Si')) {
		var temp = resultado[0].formatted_address.split(',')[0];
		$.EscribirDireccion.setValue(temp);

		preguntarAlerta12.setOpened('false');

		/** 
		 * Guardamos los cambios y continuamos a la proxima pantalla, disponibilidad de trabajo 
		 */
		require('vars')['registro'] = registro;
		if ("ENROLAMIENTO" in Alloy.Globals) {
			Alloy.Globals["ENROLAMIENTO"].openWindow(Alloy.createController("disponibilidad_de_trabajo", {}).getView());
		} else {
			Alloy.Globals["ENROLAMIENTO"] = $.ENROLAMIENTO;
			Alloy.Globals["ENROLAMIENTO"].openWindow(Alloy.createController("disponibilidad_de_trabajo", {}).getView());
		}

	} else if (resp_pregunta == 'No') {
		var registro = _.extend(registro, {
			latitude: '',
			longitude: '',
			direccion: ''
		});
		preguntarAlerta12.hide();
		require('vars')['registro'] = registro;
	} else {
		/** 
		 * En el caso que el usuario haya escrito su direccion y google no la encuentra, dejamos en blanco la longitud, latitud, respuesta de google y dejamos el flag de coreccion_direccion en 1 
		 */
		var registro = _.extend(registro, {
			longitude: '',
			latitude: '',
			google: '',
			correccion_direccion: 1
		});
		preguntarAlerta12.setOpened('false');

		/** 
		 * Omitimos los campos anteriores y continuamos a la proxima pantalla, disponibilidad de trabajo 
		 */
		require('vars')['registro'] = registro;
		if ("ENROLAMIENTO" in Alloy.Globals) {
			Alloy.Globals["ENROLAMIENTO"].openWindow(Alloy.createController("disponibilidad_de_trabajo", {}).getView());
		} else {
			Alloy.Globals["ENROLAMIENTO"] = $.ENROLAMIENTO;
			Alloy.Globals["ENROLAMIENTO"].openWindow(Alloy.createController("disponibilidad_de_trabajo", {}).getView());
		}

	}
	resp_pregunta = null;

});


(function() {
	var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
	var consultarModelo4_i = Alloy.createCollection('pais');
	var consultarModelo4_i_where = 'id_server=\'' + registro.pais + '\'';
	consultarModelo4_i.fetch({
		query: 'SELECT * FROM pais WHERE id_server=\'' + registro.pais + '\''
	});
	var pais_iter = require('helper').query2array(consultarModelo4_i);
	/** 
	 * Creamos alias para el pais 
	 */
	var pais = pais_iter[0];
	var niveles_pais = 1;
	/** 
	 * Cambiamos titulo al widget segun los datos del primer nivel del pais 
	 */

	$.widgetModal.labels({
		nivel1: pais.label_nivel1
	});
	if ((_.isObject(pais.label_nivel2) || _.isString(pais.label_nivel2)) && _.isEmpty(pais.label_nivel2)) {
		/** 
		 * Dejamos los campos de textos que existan segun el pais, si no existen, se eliminan 
		 */
		var scroll_view;
		scroll_view = $.vista19;
		var nivel_2_view;
		nivel_2_view = $.vista20;
		scroll_view.remove(nivel_2_view);
	} else {
		pais.label_nivel2 = pais.label_nivel2[0].toUpperCase() + pais.label_nivel2.substr(1);
		$.Nivel.setText(pais.label_nivel2);

		$.Escribir.setHintText(String.format(L('x3435354513_traducir', 'Escribir %1$s'), pais.label_nivel2.toString()));

	}
	if ((_.isObject(pais.label_nivel3) || _.isString(pais.label_nivel3)) && _.isEmpty(pais.label_nivel3)) {
		var scroll_view;
		scroll_view = $.vista19;
		var nivel_3_view;
		nivel_3_view = $.vista22;
		scroll_view.remove(nivel_3_view);
	} else {
		pais.label_nivel3 = pais.label_nivel3[0].toUpperCase() + pais.label_nivel3.substr(1);
		$.Nivel2.setText(pais.label_nivel3);

		$.EscribirNivel.setHintText(String.format(L('x3435354513_traducir', 'Escribir %1$s'), pais.label_nivel3.toString()));

	}
	if ((_.isObject(pais.label_nivel4) || _.isString(pais.label_nivel4)) && _.isEmpty(pais.label_nivel4)) {
		var scroll_view;
		scroll_view = $.vista19;
		var nivel_4_view;
		nivel_4_view = $.vista24;
		scroll_view.remove(nivel_4_view);
	} else {
		pais.label_nivel4 = pais.label_nivel4[0].toUpperCase() + pais.label_nivel4.substr(1);
		$.Nivel4.setText(pais.label_nivel4);

		$.EscribirNivel2.setHintText(String.format(L('x3435354513_traducir', 'Escribir %1$s'), pais.label_nivel4.toString()));

	}
	if ((_.isObject(pais.label_nivel5) || _.isString(pais.label_nivel5)) && _.isEmpty(pais.label_nivel5)) {
		var scroll_view;
		scroll_view = $.vista19;
		var nivel_5_view;
		nivel_5_view = $.vista26;
		scroll_view.remove(nivel_5_view);
	} else {
		pais.label_nivel5 = pais.label_nivel5[0].toUpperCase() + pais.label_nivel5.substr(1);
		$.Nivel6.setText(pais.label_nivel5);

		$.EscribirNivel3.setHintText(String.format(L('x3435354513_traducir', 'Escribir %1$s'), pais.label_nivel5.toString()));

	}
	var consultarModelo5_i = Alloy.createCollection('nivel1');
	var consultarModelo5_i_where = 'id_pais=\'' + pais.id_pais + '\'';
	consultarModelo5_i.fetch({
		query: 'SELECT * FROM nivel1 WHERE id_pais=\'' + pais.id_pais + '\''
	});
	var region_list = require('helper').query2array(consultarModelo5_i);
	var item_index = 0;
	_.each(region_list, function(item, item_pos, item_list) {
		item_index += 1;
		item.valor = item.id_label;
	});
	if (Ti.App.deployType != 'production') console.log('armado de regiones dice', {
		"data": region_list
	});
	require('vars')['data_region'] = region_list;
	require('vars')['pais'] = pais;
	/** 
	 * guardamos las variables de data_nivel en vacio 
	 */
	require('vars')['data_nivel_1'] = '';
	require('vars')['data_nivel_2'] = '';
	require('vars')['data_nivel_3'] = '';
	require('vars')['data_nivel_4'] = '';
	require('vars')['data_nivel_5'] = '';
	/** 
	 * Creamos evento que al cerrar desde la ultima pantalla, esta tambi&#233;n se cierre. Evitamos el uso excesivo de memoria ram 
	 */

	_my_events['_close_enrolamiento,ID_447057723'] = function(evento) {
		if (Ti.App.deployType != 'production') console.log('escuchando cerrar enrolamiento domicilio', {});
		$.ENROLAMIENTO3.close();
	};
	Alloy.Events.on('_close_enrolamiento', _my_events['_close_enrolamiento,ID_447057723']);
})();

if (OS_IOS || OS_ANDROID) {
	$.ENROLAMIENTO3.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}