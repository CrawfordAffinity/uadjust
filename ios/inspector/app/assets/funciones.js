var funciones = {}
funciones.formatear_fecha = function(x_params) {
	var formato = x_params['formato'];
	var fecha = x_params['fecha'];
	var formato_entrada = x_params['formato_entrada'];
	var moment = require("alloy/moment");
	var fecha_era_texto = (typeof fecha === 'string' || typeof fecha === 'number') ? true : false;
	var nuevo = '';
	if (fecha_era_texto == true || fecha_era_texto == 'true') {
		nuevo = moment(fecha, formato_entrada).format(formato);
	} else {
		nuevo = moment(fecha).format(formato);
	}
	return nuevo;
};
funciones.consumir_cola = function(x_params) {
	var consultarModelo4_i = Alloy.createCollection('cola');
	var consultarModelo4_i_where = '';
	consultarModelo4_i.fetch();
	var lista_enviar = require('helper').query2array(consultarModelo4_i);
	if (lista_enviar.length == 0 || lista_enviar.length == '0') {} else {
		item = lista_enviar[0];
		if (item.tipo == L('x1966285652_traducir', 'cancelar')) {
			if (Ti.App.deployType != 'production') console.log('Voy a enviar un cancelar tarea', {
				"item": item
			});
			data = JSON.parse(item.data);
			var consultarURL5 = {};

			consultarURL5.success = function(e) {
				var elemento = e,
					valor = e;
				if (Ti.App.deployType != 'production') console.log('Respuesta de servidor de cancelar tarea', {
					"elemento": elemento
				});
				if (elemento.error == 0 || elemento.error == '0') {
					id_cola = elemento.id_app;
					var eliminarModelo29_i = Alloy.Collections.cola;
					var sql = 'DELETE FROM ' + eliminarModelo29_i.config.adapter.collection_name + ' WHERE id=\'' + id_cola + '\'';
					var db = Ti.Database.open(eliminarModelo29_i.config.adapter.db_name);
					db.execute(sql);
					db.close();
					eliminarModelo29_i.trigger('remove');
				}
				elemento = null, valor = null;
			};

			consultarURL5.error = function(e) {
				var elemento = e,
					valor = e;
				if (Ti.App.deployType != 'production') console.log('cancelarTarea fallo', {
					"asd": elemento
				});
				elemento = null, valor = null;
			};
			require('helper').ajaxUnico('consultarURL5', '' + String.format(L('x3244141284', '%1$scancelarTarea'), url_server.toString()) + '', 'POST', {
				id_inspector: data.id_inspector,
				codigo_identificador: data.codigo_identificador,
				id_tarea: data.id_server,
				num_caso: data.num_caso,
				mensaje: data.razon,
				opcion: 0,
				tipo: 1,
				id_app: item.id
			}, 15000, consultarURL5);
		} else if (item.tipo == L('x71742335_traducir', 'enviar')) {
			if (Ti.App.deployType != 'production') console.log('Voy a enviar un finalizar tarea', {
				"item": item
			});
			var consultarURL6 = {};

			consultarURL6.success = function(e) {
				var elemento = e,
					valor = e;
				if (Ti.App.deployType != 'production') console.log('Respuesta de servidor de finalizar tarea', {
					"elemento": elemento
				});
				if (elemento.error == 0 || elemento.error == '0') {
					id_cola = elemento.id_app;
					var eliminarModelo30_i = Alloy.Collections.cola;
					var sql = 'DELETE FROM ' + eliminarModelo30_i.config.adapter.collection_name + ' WHERE id=\'' + id_cola + '\'';
					var db = Ti.Database.open(eliminarModelo30_i.config.adapter.db_name);
					db.execute(sql);
					db.close();
					eliminarModelo30_i.trigger('remove');
				}
				elemento = null, valor = null;
			};

			consultarURL6.error = function(e) {
				var elemento = e,
					valor = e;
				if (Ti.App.deployType != 'production') console.log('finalizar Tarea fallo', {
					"asd": elemento
				});
				elemento = null, valor = null;
			};
			require('helper').ajaxUnico('consultarURL6', '' + String.format(L('x3471857721', '%1$sfinalizarTarea'), url_server.toString()) + '', 'POST', {
				inspeccion: data,
				id_app: item.id
			}, 15000, consultarURL6);
		} else if (item.tipo == L('x3414427079_traducir', 'fotos')) {
			if (Ti.App.deployType != 'production') console.log('Voy a enviar finalizar imagen', {
				"item": item
			});
			data = JSON.parse(item.data);
			var consultarURL7 = {};

			consultarURL7.success = function(e) {
				var elemento = e,
					valor = e;
				if (Ti.App.deployType != 'production') console.log('Respuesta de servidor de finalizar imagenes', {
					"elemento": elemento
				});
				if (elemento.error == 0 || elemento.error == '0') {
					id_cola = elemento.id_app;
					var eliminarModelo31_i = Alloy.Collections.cola;
					var sql = 'DELETE FROM ' + eliminarModelo31_i.config.adapter.collection_name + ' WHERE id=\'' + id_cola + '\'';
					var db = Ti.Database.open(eliminarModelo31_i.config.adapter.db_name);
					db.execute(sql);
					db.close();
					eliminarModelo31_i.trigger('remove');
				}
				elemento = null, valor = null;
			};

			consultarURL7.error = function(e) {
				var elemento = e,
					valor = e;
				if (Ti.App.deployType != 'production') console.log('subirImagenes fallo', {
					"asd": elemento
				});
				elemento = null, valor = null;
			};
			require('helper').ajaxUnico('consultarURL7', '' + String.format(L('x757910167', '%1$ssubirImagenes'), url_server.toString()) + '', 'POST', {
				id_inspector: data.id_inspector,
				num_caso: data.num_caso,
				id_tarea: data.id_tarea,
				imagen_final: 'true',
				id_app: item.id
			}, 15000, consultarURL7);
		} else if (item.tipo == L('x4199404193', 'confirmar_ruta')) {
			if (Ti.App.deployType != 'production') console.log('Voy a confirmar ruta', {
				"item": item
			});
			data = JSON.parse(item.data);
			var consultarURL8 = {};

			consultarURL8.success = function(e) {
				var elemento = e,
					valor = e;
				if (elemento.error == 0 || elemento.error == '0') {
					id_cola = elemento.id_app;
					var eliminarModelo32_i = Alloy.Collections.cola;
					var sql = 'DELETE FROM ' + eliminarModelo32_i.config.adapter.collection_name + ' WHERE id=\'' + id_cola + '\'';
					var db = Ti.Database.open(eliminarModelo32_i.config.adapter.db_name);
					db.execute(sql);
					db.close();
					eliminarModelo32_i.trigger('remove');
				}
				elemento = null, valor = null;
			};

			consultarURL8.error = function(e) {
				var elemento = e,
					valor = e;
				if (Ti.App.deployType != 'production') console.log('confirmar ruta fallo', {
					"asd": elemento
				});
				elemento = null, valor = null;
			};
			require('helper').ajaxUnico('consultarURL8', '' + String.format(L('x2181838308', '%1$sconfirmarRuta'), url_server.toString()) + '', 'POST', {
				id_inspector: data.id_inspector,
				codigo_identificador: data.codigo_identificador,
				tareas: data.tareas
			}, 15000, consultarURL8);
		}
	}
	return null;
};

module.exports = funciones;