exports.events = _.clone(Backbone.Events);
exports.ajaxkeys = {};
exports.variables = {};
exports.styles = {
	"images": {
		"id_164841395492": "/images/i7F73828276786D80F13715BB5BFB4CCC.png",
		"id_38920584913": "/images/i09E757E6A96CDFD1A3071F75A86B9703.png",
		"id_1245138369": "/images/iBD1E17BE427423F6FA053D07C27B494E.png",
		"id_164841395410": "/images/iE9355204576B1FD1BB3F1F0E435BFF6D.png",
		"id_164841395471": "/images/iDFEBE4EC019BCE94F6EC5853A3BFABF5.png",
		"id_164841395442": "/images/iE50DF32C64581FCE5856C9454B2D58E2.png",
		"id_12451383625": "/images/iC37FBC50BE131D22685223C641DA5C9B.png",
		"tareas_on": "/images/iF50519A47DC67364BC4E523D4F9D7B6C.png",
		"id_12451383656": "/images/iC9AFD1E7FA9C0486C5424E816865BE8C.png",
		"id_17250186488": "/images/i451D26CB7C617839FAAABBEE243EE91B.png",
		"id_12451383621": "/images/i52CB6FB175FB303E19F6C3CEA9C8D640.png",
		"id_38920584930": "/images/iCEF1D427EF5C147BC79A0D6848913C44.png",
		"id_164841395419": "/images/i988F7B1016AF1A4927CD7ED9AE8AB990.png",
		"id_164841395430": "/images/i9AC6E8C97F63FEB0A46CCD655AC44431.png",
		"id_17304885114": "/images/i0E5D79CD99D2BBA09F0BEAA30A79A3D9.png",
		"id_17304885111": "/images/iE67E9F2E19A329505444E7465B5FD7D0.png",
		"id_17304885144": "/images/i87DE3A07ABFD59B0F96861DEC12DDF8F.png",
		"id_17304885149": "/images/i47F6C47BCA8C1146589A6899C5016EA7.png",
		"id_164841395454": "/images/i20769EDB2A387AD7E63BFEE1ADB6E01A.png",
		"id_12451383648": "/images/iF70C2EE509A92FCB43AF685DDD2B2FA2.png",
		"id_12451383619": "/images/i7F617027CD4EC4189C4E8CED725823AC.png",
		"id_17304885150": "/images/iF37AFC4AE87333385D84EF93517E2F25.png",
		"id_38920584924": "/images/i6F4B0EAC06F1D5F21CF0ADB7E3036782.png",
		"hoy_on": "/images/iAEC3B644E80683643754D43C54627375.png",
		"pincho10": "/images/i71C6F623091942D94C46CC378343ABF6.png",
		"id_12451383657": "/images/i2A3F902C97F8E816FBF43D6072C8EA38.png",
		"id_38920584923": "/images/i53F37CB31AA110AB8BF1BCA095F905FE.png",
		"id_12451383654": "/images/i54B57D779036FF5C5902278D3A9A329A.png",
		"id_1648413954110": "/images/iDD2C93752E1169F8D71892A477C85A11.png",
		"id_17304885140": "/images/iE1BAB1BE32595937A03D896936E17846.png",
		"id_17304885122": "/images/iF6C82CEBB73C3F50FC5AC08E6FA50DF2.png",
		"id_1648413954118": "/images/iE02AFE3817F8B969AEAACF7456E25AB5.png",
		"id_1648413954120": "/images/i870DC5E31CA825DAEE1AD00C6FB1D68A.png",
		"id_17304885138": "/images/iF746C788F42D49A6AD715DC319BFF2B7.png",
		"id_12451383617": "/images/i29429A8CA3B30681CA2BB487FE2DDF26.png",
		"id_12451383644": "/images/i10280424F1EE424A6A514F0F16858345.png",
		"id_172501864818": "/images/i57F0B2CBFC207F6540A08C8209B6461C.png",
		"id_1648413954115": "/images/i6AF96FB60F20F611E16F44E4A4C366B4.png",
		"id_1648413954100": "/images/iF086F6FE7A1868188FD09EE6FA03485B.png",
		"id_164841395411": "/images/i0E4650BFDDB114F81A51C600521AC2D7.png",
		"id_1730488511": "/images/iBC783890F4B9774991803EB6ECA81549.png",
		"id_12451383635": "/images/iE89BCB17377B1DC1F77A2DE2512A28EC.png",
		"id_164841395433": "/images/i388C5A5B6FEC1AAA9E0FED2BE6842A35.png",
		"id_172501864828": "/images/iC97BF367BA6481E3A5CEAEBFBD492506.png",
		"id_164841395441": "/images/i10BF85AEDE9F2E6087E8EECF917665A8.png",
		"id_12451383639": "/images/i9CFC507846CDD4330DF7FDF1C0B0C31E.png",
		"id_3892058491": "/images/i4ECB85A2AE41ACB15653139F35BFA8C7.png",
		"id_1648413954102": "/images/i47EFAC4DEC72BFED1EAD8D5621ECF913.png",
		"sin_imagen": "/images/i7C39FFD7014473CB54CF601D7555AA02.png",
		"id_164841395489": "/images/iECBF5EDE5417D57D71BD067290909C22.png",
		"id_17304885161": "/images/iD07A2DFA41B1AF2BAD9EC1879066DBE8.png",
		"id_12451383659": "/images/iC217A8FE8F59CC255970BBB562470CC2.png",
		"id_1648413954113": "/images/i7DF846739C1FD71098DE3902990DA8A9.png",
		"id_12451383651": "/images/i3D15430021DB8CCF5FC81650384162EA.png",
		"id_38920584926": "/images/iF8FD2CB3805AB4250C4E53D99093CA3C.png",
		"id_1648413954104": "/images/i6394513CA228CCB57ED602297C9422BC.png",
		"id_164841395472": "/images/iAC27C6E0D5FAF36E89B0E75BD07AEB87.png",
		"id_17304885152": "/images/i49BA5221BED2176E506AA26FABE14E75.png",
		"id_17304885141": "/images/i09B77564ECA69742FF6926701ADCC788.png",
		"id_17304885169": "/images/i7C907FDBDA8C040F234553C55D821A45.png",
		"id_12451383615": "/images/iDD8E3DB6271506344EFC962A6F6C907E.png",
		"id_16484139547": "/images/i77B96376A21E9E530BF99DEF0E191F24.png",
		"id_3892058499": "/images/iBF02E0A9F57D34BA382B6B4851AEB549.png",
		"id_164841395414": "/images/i19F89DC20905F566DA7CCDBE900B1828.png",
		"id_38920584928": "/images/iC97BF367BA6481E3A5CEAEBFBD492506.png",
		"id_1245138361": "/images/iB1F901AEFA79EE1F3A05C41650B368A4.png",
		"id_17304885159": "/images/i771C97B5058ACE91E7A66AD6D47C1934.png",
		"id_17304885116": "/images/iD464C126D5638562F447AE472C2BD133.png",
		"id_17304885127": "/images/i574DB1641BB1F722625EB07223F436E0.png",
		"id_12451383626": "/images/i10257EAFE36B26C91213946BD6B916BE.png",
		"id_16484139542": "/images/iEB558FE382408A66B62F79BCD08E87DE.png",
		"id_12451383624": "/images/iFC47B0B749489B593F6282CEBE075C92.png",
		"id_17304885162": "/images/iB32E3B863CF1AC37F2816A7BCD60A554.png",
		"id_12451383640": "/images/iE9825E0C72E7F6D3C9F3E8C359F44947.png",
		"id_17304885148": "/images/i77F111A1F32961B1F386B1DC7377767D.png",
		"id_17304885156": "/images/i98CA10AE81168639ED3D051D83F9FF4A.png",
		"id_164841395438": "/images/i54701C93E11CA7BAF12DF89AABF2AE04.png",
		"id_164841395497": "/images/iA7B0C6D62FD940727B9D5DBBD4165324.png",
		"id_17304885143": "/images/iC35735FF5486867B522A8ADFAFC3B6C7.png",
		"id_17304885155": "/images/i94E3A2CA38A4F73C31EF4B2C5D5BE94B.png",
		"id_172501864811": "/images/iF173895CDE559CF4A789ECA310F324C1.png",
		"id_164841395445": "/images/i0BEBD7212FCEADD8E81C2812FD68C36D.png",
		"id_12451383650": "/images/i9C794F568EA9A9383A579EBC118ECE1C.png",
		"pincho2": "/images/iAF9569EFAB97FCE93FE3DAD449050784.png",
		"id_17304885129": "/images/i97EEC684C8F34E49FB30501C8417541F.png",
		"seguir": "/images/i1434B8CFE569C191FCC8DE95E5A1BC96.png",
		"id_164841395481": "/images/i15B71869941D9D056D2771715265227F.png",
		"id_164841395434": "/images/iBB18FBADFC2D527AD2709E8652AAFD5A.png",
		"id_172501864830": "/images/iCEF1D427EF5C147BC79A0D6848913C44.png",
		"id_172501864823": "/images/i53F37CB31AA110AB8BF1BCA095F905FE.png",
		"id_17304885139": "/images/i570A2B785DF574495DD66D6D91299947.png",
		"id_164841395476": "/images/i691DBFC540A724998A2353C7D6DC199F.png",
		"id_1245138364": "/images/i058F2A04E46A9135BF2007D00D84E610.png",
		"emergencia_on": "/images/i7E92861F463166F66224942F55FA69B0.png",
		"id_3892058497": "/images/iC18AD8B25C7E89544B26A56D72763ACF.png",
		"id_164841395478": "/images/i35A05BBCDD161E2A6C0C478F703901C0.png",
		"id_164841395436": "/images/i359C4AC08A6FB6D5912E5ECA8ABBB00A.png",
		"id_1730488517": "/images/i5C878AB139179D2864AE95A2540738B3.png",
		"id_172501864815": "/images/i3BD062E17CB084738EC3C8544C4F562C.png",
		"id_38920584911": "/images/iF173895CDE559CF4A789ECA310F324C1.png",
		"id_17304885158": "/images/iAADD9B59546B4EABA48E369D34ED0C21.png",
		"id_164841395465": "/images/i396F93C0F37490C405609068F53808DE.png",
		"pincho5": "/images/i0B10A526AD8D5B25BC5BAC5C2C06F119.png",
		"id_1648413954117": "/images/i2F569FBCA8D42EB735DED2831286DC66.png",
		"id_164841395444": "/images/i89096AB8DBE818E384384C292C89811F.png",
		"id_17304885151": "/images/i093B3780EEFCFADC8E24C0D54540C0B5.png",
		"id_17304885125": "/images/iCB3B5FFBFA8DF33CB29C50BC26542162.png",
		"pincho_normal": "/images/i1EC4F2338BB0785F7A6208CC19D16FA3.png",
		"id_17304885166": "/images/i0DE5EA646AA925B3530150BF5F230AA5.png",
		"id_1245138363": "/images/iE18490E3CB42EE21FA2CA39791E6EE07.png",
		"id_1648413954103": "/images/i96D645D648D27F12B2E4F54E9503E8DF.png",
		"id_12451383630": "/images/i77B67FB15DAF2AA6C7DFAB3AEF96CC3D.png",
		"id_164841395467": "/images/iF3F5D57B22CF688DF19FE14CCD909402.png",
		"hoy_off": "/images/iEE7B77BF68990BD6E87F22F782B6F317.png",
		"id_164841395470": "/images/i6EA1D3660FE93F229A4DD0E034A728EB.png",
		"id_164841395468": "/images/i399EE8FDF222FDA603DF0C82E0BCCFAC.png",
		"id_38920584925": "/images/iD8AE92E691560F489DE642319547F8F9.png",
		"id_164841395423": "/images/i42D15D48B2ED837F7903FE534A06494B.png",
		"id_38920584916": "/images/i0B25A07F8BA6A3D1155E5635855725E3.png",
		"entrada_off": "/images/i3DA1303A718E3AD93FF5A8CA452A055D.png",
		"id_17304885120": "/images/i907B21FBECE83B7799349F9FAA0D472A.png",
		"id_164841395459": "/images/iA152F49A26EC92286520C265AABD8CFC.png",
		"mono_6": "/images/i1EE8695ED4E44BD032E4C96194C31486.png",
		"tip_extra": "/images/i07526F3913A2AE7CCEDF61A779050CF9.png",
		"id_38920584919": "/images/i6F4FBDF546B1311877A208695B53B1F5.png",
		"id_164841395429": "/images/i9BDA9EE2558DFE3F2125992BECA4278D.png",
		"id_12451383636": "/images/iE8162669F36B070D6B7D99F53757E770.png",
		"id_38920584922": "/images/i7A0053CD88CD6D72A30F33A67115B84F.png",
		"id_164841395440": "/images/i585BBC1586441528C908AD41EB8EBDBD.png",
		"id_172501864825": "/images/iD8AE92E691560F489DE642319547F8F9.png",
		"id_164841395498": "/images/i51313033EE92F99494C0CEF93F77F8D9.png",
		"id_16484139549": "/images/i88E908455A225B9390E3678E9C19FBA2.png",
		"id_164841395415": "/images/i6755383116D1A5AD862D47B6D5554641.png",
		"id_16484139546": "/images/iD7DACAF52A6FE787F42E337F896ED7E5.png",
		"perfil_off": "/images/i56ADB5CB2BA30CF0527FC3963E4B5673.png",
		"id_12451383616": "/images/i4BE78669A95AFE52AA53FBDD4361D3E4.png",
		"id_164841395488": "/images/iDAA2A0A70B3960D02C55DDF038216353.png",
		"id_164841395435": "/images/i10063715BD30D221415EF53335926168.png",
		"id_17250186483": "/images/iD4FE3BB66A3A734672CFBF9A4FE820B3.png",
		"id_17250186486": "/images/i1AAB9DCC791F6C8D5CBD86A69F2BD28A.png",
		"id_17304885112": "/images/i3915CB7F49F97487FF413D7B39ED0B37.png",
		"id_17304885147": "/images/i7F291EBC02606240803647C45234D01B.png",
		"id_17304885126": "/images/i220830766E53497CDAC422E9AAE22296.png",
		"id_38920584918": "/images/i57F0B2CBFC207F6540A08C8209B6461C.png",
		"id_17304885165": "/images/i8C849BCE5D5B003A5C94BC535D7C65EC.png",
		"id_164841395426": "/images/i077D23D9EA5866B0EF5E25614919665F.png",
		"id_17304885153": "/images/iF065FCB3FEA5C1EBC4123E4F1156C63D.png",
		"id_1245138368": "/images/i553359F66995CFAE34B27D5D0D4C95E0.png",
		"id_164841395494": "/images/i18ACDDC89C81835BFB74D3A0E71D8C10.png",
		"id_3892058498": "/images/i451D26CB7C617839FAAABBEE243EE91B.png",
		"id_17304885118": "/images/i37DC047D2355AA99AF3408D44781C90B.png",
		"id_1648413954111": "/images/i661E5F5296ABB7AB4B0E5D236E6C7630.png",
		"id_12451383645": "/images/i00F9DCB834689DD92A6F5679DF478CEC.png",
		"id_164841395421": "/images/i46769D88018418464C9F4B24D384CB8B.png",
		"id_172501864826": "/images/iF8FD2CB3805AB4250C4E53D99093CA3C.png",
		"id_1648413954119": "/images/iAE28EA1830BF41F55AD84813E8F1D056.png",
		"id_1648413954116": "/images/iBBA0A82C21EA239F79E171AC51EA7B95.png",
		"id_12451383629": "/images/iE4678809AAB8130B87C7E9341736D608.png",
		"id_38920584917": "/images/i51CD06F5D53967A813F0F7C4F8679F81.png",
		"id_164841395460": "/images/i154CF2D0DF49584435B369E991104CD0.png",
		"id_164841395449": "/images/iF714D356682197276705B90C40BA8A03.png",
		"id_164841395418": "/images/i38E6B9D378262B4EF35387DC47BBF31D.png",
		"id_17304885121": "/images/i901A29B5913762D207E0BEBD505A9A5F.png",
		"id_164841395446": "/images/i1B954A4FDCFE0860F6D3EBC4D01D2F19.png",
		"id_3892058493": "/images/iD4FE3BB66A3A734672CFBF9A4FE820B3.png",
		"id_172501864819": "/images/i6F4FBDF546B1311877A208695B53B1F5.png",
		"id_164841395499": "/images/i26C8BDFC1DEA0C67358C293A353C327B.png",
		"id_1648413954101": "/images/i9C4EAA87C9F69FE64BE240DAE158CF0E.png",
		"pincho9": "/images/i8893A1F7922FA64C321EAD4A6119E30D.png",
		"id_38920584910": "/images/iB810656D7547F7A2C967AC3DB0C7CAD4.png",
		"id_1648413954105": "/images/i6CD686DC5A11FB94CBB2A09FC90DB6EF.png",
		"id_164841395495": "/images/i5A8E6E8D34ABB47533165D506B7E1462.png",
		"pincho1": "/images/i016B8B08EE4DEC0313B1A3082A84D2F2.png",
		"id_1648413954112": "/images/iCCB062F45E0D9D917C617FDB2EA1E357.png",
		"id_172501864821": "/images/i997DFD2C59EEEC93E94CA8831CCFE464.png",
		"id_172501864829": "/images/iC9B3A576F9A65C4C9C208CFB562D3A8D.png",
		"id_12451383611": "/images/i889E7951CC79C107C7467F773D851F53.png",
		"id_17304885157": "/images/i89E93536F928FB549C7EEE44EF370DCE.png",
		"pincho3": "/images/i56ABF76585C9B6A661090A44C21595F2.png",
		"id_164841395448": "/images/iBEB7E7D434C1667889663859EAE75D92.png",
		"id_164841395424": "/images/iBC7C1AA388608962A1F7B8C61BD6DA82.png",
		"id_17304885168": "/images/i8B825AD136827F17DB6D478184C85301.png",
		"mono_3": "/images/i66166742DB624694FEA59FCCAF583FB7.png",
		"id_1648413954114": "/images/iAD45960BC2841EC6E2D86283A2F33E96.png",
		"id_164841395458": "/images/i380DF6EF85EB2185DE1A501FEE66371A.png",
		"id_17250186482": "/images/i4DB2E8AD733DFC95AF5E6A5115E117EE.png",
		"id_12451383637": "/images/i642B66133CEB6E403BB536FBADA0E681.png",
		"id_164841395457": "/images/i0DE060270629150E8A87173EDB8B79F7.png",
		"tareas_off": "/images/iA1C4E28A42E2B254D31FB3A96A742031.png",
		"id_164841395417": "/images/i40A27D206CA23815C331AB71422629F2.png",
		"id_12451383610": "/images/iFE945B7620AFD4BCA1698FF05C97EEDF.png",
		"id_3892058496": "/images/i1AAB9DCC791F6C8D5CBD86A69F2BD28A.png",
		"id_164841395484": "/images/i7FDF4568F230AA5A4D3B701708D1E8D6.png",
		"id_1648413954108": "/images/i5907CCF7C26ECC19EAD231C90A4FAB9D.png",
		"id_1245138367": "/images/i0337CBA53F3589380EFAC3F505EDAFF1.png",
		"id_17250186484": "/images/iD95E1371C4A9D4060E4C293FEBB2A371.png",
		"id_17304885131": "/images/i829FF9A81D8C1537788109FCE55BA372.png",
		"id_164841395493": "/images/i68421A3D08C3B75227181E0EC9EBCE42.png",
		"id_12451383646": "/images/i3310017A13BD47BA4E4549FD12A7788A.png",
		"id_38920584929": "/images/iC9B3A576F9A65C4C9C208CFB562D3A8D.png",
		"id_12451383655": "/images/iFAD70BC931105FCA07FD8E0DAEAF4137.png",
		"id_17304885163": "/images/iF77E81CC3C9BE873EB33BA0AAD43D591.png",
		"id_17304885164": "/images/i79D6CE5169F51E74EF1FEA39DB07CEB9.png",
		"id_164841395477": "/images/iD182D4C677A7FEF7B7F9D2021F901A9C.png",
		"id_164841395491": "/images/iF8B6DC3340FEC020058F7C743ADA0098.png",
		"id_164841395456": "/images/i9B1B0BD5A88BDAB459D66F68B80309F5.png",
		"id_164841395483": "/images/i88CB92AA112DA9B4D3913374D31E5489.png",
		"id_12451383647": "/images/iE77D1DCF0D78AED4D64B7BE4DEC18855.png",
		"id_12451383614": "/images/i5CCD03CB61FDCAE6B11B30DC864BAE99.png",
		"id_164841395466": "/images/i28D1EA7CF68179295028D9F1DC787292.png",
		"id_12451383653": "/images/i129B9502415F1FFEF9E8936317CEFF50.png",
		"id_17250186481": "/images/i4ECB85A2AE41ACB15653139F35BFA8C7.png",
		"id_1730488514": "/images/iCD6AA874B07BD726ABF7364ED6851236.png",
		"id_12451383652": "/images/iA9CA15D575EC6434CD632DD4B0458E02.png",
		"id_164841395453": "/images/i3CD6AA1281B7B4ED5F37ED76FCC2A808.png",
		"id_17250186485": "/images/i1846D1705B39A0DFD7CA331E2A624A7B.png",
		"id_164841395475": "/images/i7E06C884017083155A6857A40DCF4DE3.png",
		"id_17304885110": "/images/i5CF917F99E733C468810E2F5D08CC88D.png",
		"id_164841395425": "/images/iF09D8144E898AB6F35D2FF363EE98F9E.png",
		"id_164841395428": "/images/i21932839113ADBF836C5D8C8B4B11AEB.png",
		"id_17304885167": "/images/i3137CC503DA91A0688C5F38A0095A244.png",
		"pincho8": "/images/iCF8339009327869FD10EC12DA520DC5A.png",
		"id_1648413954106": "/images/iEBD4EE843BEEB6E225C25387B7DCF30E.png",
		"id_164841395431": "/images/iE28ABABF9CABE7255711922BBF55B983.png",
		"id_17304885130": "/images/iEA1F5243153E7D22D525312FDFAE5A90.png",
		"id_12451383638": "/images/i634ADF5364D3A8C7F50C71FE6DD90A80.png",
		"id_17304885123": "/images/i5D746AFA0C7F3FCE385618C403086A21.png",
		"id_17304885154": "/images/iA06FB0DE96CD4315F67874E0FBDB4EE1.png",
		"id_17304885160": "/images/i749E117F5781F156EF8FE76959C4144F.png",
		"id_12451383618": "/images/i69D7DC35EF1C27805FA3D2751325A855.png",
		"id_172501864810": "/images/iB810656D7547F7A2C967AC3DB0C7CAD4.png",
		"emergencia_off": "/images/iCF94090E508927BED4CDC1EEA6432A46.png",
		"id_12451383620": "/images/i0C1216BE3263895DD5C0DCCE921413F2.png",
		"id_12451383658": "/images/i487CD84F81C016C6569519B588DF619F.png",
		"id_172501864824": "/images/i6F4B0EAC06F1D5F21CF0ADB7E3036782.png",
		"id_17304885145": "/images/iEDCBD1534AE3D23AD5893D34EEA1BB5C.png",
		"id_17250186487": "/images/iC18AD8B25C7E89544B26A56D72763ACF.png",
		"id_16484139548": "/images/i1A8982F638B1931C2A37ED5E826C17C0.png",
		"id_17304885137": "/images/i77C1E7F3FBBE442E1B12C53126B13B1B.png",
		"id_12451383649": "/images/iD68DDDF47F8B5A8C859DEE487442816E.png",
		"id_164841395452": "/images/iBAAC1AF775CEE1B716B575455DB68FDC.png",
		"mono_7": "/images/i3ECE8B0251AA8C358DD86D279774A6FE.png",
		"perfil_on": "/images/iAD127855BF1E2CC6C2F28479754B79C0.png",
		"id_17304885115": "/images/i1B011A35F1C8D4708DA9E83D549EA011.png",
		"id_12451383612": "/images/i1DC1B37D94F0D03FB92F1569E2D04139.png",
		"id_164841395439": "/images/i9AE08E1C51AF160F89D84F868261FCFD.png",
		"id_172501864827": "/images/iCA260B7119401A0D456373D95DAD7824.png",
		"id_17304885142": "/images/iF0674BDE61F66D48C2CA74C15C3AC823.png",
		"id_164841395487": "/images/i57C1B3043D0424531659720F9C32B1C2.png",
		"id_172501864817": "/images/i51CD06F5D53967A813F0F7C4F8679F81.png",
		"id_172501864814": "/images/iB1B0726192E9AFA5B4393DD2789A8E73.png",
		"id_164841395473": "/images/i273522DAAED644BC07219E8653954EE3.png",
		"id_17304885124": "/images/i21006680354E2478937ED256C47F0D6F.png",
		"id_164841395482": "/images/i8A0B707451CCBE122A5B28F640716AF1.png",
		"id_164841395437": "/images/i0E59AE058F689EF3F4FAD9FDDF470974.png",
		"id_12451383622": "/images/iD51F4417351E1B84F7F9B808956CD544.png",
		"linea": "/images/iA56941C357BC902627770A6229AC94A1.png",
		"pinchocasa": "/images/i31D884D83D4B504A668DECEFFAE7EA43.png",
		"id_1730488513": "/images/i8454D88E5895192B0770F08DB2F4C7D6.png",
		"id_1245138362": "/images/iBD0F891E767949293A5E319C70468E61.png",
		"id_17304885136": "/images/iCA6997D4EA75B7A7EA52E0BBCF58089A.png",
		"id_164841395474": "/images/i86B7E95443CEBED206372766EAF4F59C.png",
		"id_3892058494": "/images/iD95E1371C4A9D4060E4C293FEBB2A371.png",
		"id_12451383613": "/images/i69B0B3F2A0625ADD474DEFFDE5FB783D.png",
		"id_12451383633": "/images/iF78AD53B709F20115D8977BF9F612522.png",
		"id_172501864822": "/images/i7A0053CD88CD6D72A30F33A67115B84F.png",
		"id_12451383631": "/images/i1352BFB08B32246214800F652EA481A9.png",
		"id_17304885171": "/images/i9136DCA319DE76A1A2437B6415790BB9.png",
		"id_164841395447": "/images/i304ED678B67AD49378DB6228C609BE45.png",
		"id_164841395413": "/images/i2175C7E985066E9DBB5FD1E204BDED5D.png",
		"id_1730488515": "/images/iAB265FAD2F6EA6964A2299BAF0375F29.png",
		"id_17250186489": "/images/iBF02E0A9F57D34BA382B6B4851AEB549.png",
		"id_1245138366": "/images/iC9EDBE23CD4A8C3ACACF6159FAE7896F.png",
		"id_1730488518": "/images/iFDDDAFE794E6B3D1640B628D4327FCD4.png",
		"id_38920584914": "/images/iB1B0726192E9AFA5B4393DD2789A8E73.png",
		"id_17304885119": "/images/i400DF90467135851598103A438FA2CBE.png",
		"id_164841395463": "/images/iF0D35F95D8CF565D84F4B2EABC07C0C3.png",
		"id_38920584921": "/images/i997DFD2C59EEEC93E94CA8831CCFE464.png",
		"pincho6": "/images/i15970AF9F27BB88EC2DC1EE088D2551E.png",
		"id_164841395412": "/images/i867483CAEDC8D77255A7F25A15625CA5.png",
		"id_164841395464": "/images/iAC8C1B1E912288D6EF52DC4C71B79944.png",
		"id_12451383628": "/images/i28054D3A888BD526A6B71FA0D72B495E.png",
		"id_38920584915": "/images/i3BD062E17CB084738EC3C8544C4F562C.png",
		"id_164841395416": "/images/i1391131C784CF99686D19D65AC20986E.png",
		"id_12451383627": "/images/i64703C2596335309216F0F02CBE47A0A.png",
		"id_12451383634": "/images/iB9E2226298DF133710B12402166BE94F.png",
		"id_12451383643": "/images/i8F4CE4FFC4978421CD759A09A64FD5DF.png",
		"id_164841395485": "/images/i69B8A04A79F91820581E4D3708B7D003.png",
		"id_164841395461": "/images/iB6A8B660E26087802754C713C196B739.png",
		"id_164841395443": "/images/iBFDCFF2749FF3ACE5A75BF05C2C1E25C.png",
		"id_3892058492": "/images/i4DB2E8AD733DFC95AF5E6A5115E117EE.png",
		"id_164841395432": "/images/i0A41ED8B1E443429769FFB49137003C2.png",
		"id_164841395422": "/images/i3F6BAF33AC1E4011205917185EA98CAD.png",
		"id_1730488519": "/images/i21A429706C7934A4774996F025B3B1AF.png",
		"id_164841395420": "/images/i405EC5CB58F536363712327BE6376BB1.png",
		"id_1730488512": "/images/iC91EEFE37F9302D5B31812F55B1658CC.png",
		"id_17304885135": "/images/iE289D15EF298A75107CE5D8C53D93DFA.png",
		"pincho7": "/images/iA6C17CF225E89F54DE02BF67B8FCA6A0.png",
		"logo": "/images/i31ABAC96411E678886E8E42DD5A4F520.png",
		"id_172501864813": "/images/i09E757E6A96CDFD1A3071F75A86B9703.png",
		"id_164841395469": "/images/i09C63C089B2E569672618ED5FC69504C.png",
		"id_16484139545": "/images/iC844F1FD37D49F099C8E19CDD3168A41.png",
		"id_16484139541": "/images/i21578740E736331F7BDC9D452BD712C9.png",
		"id_38920584912": "/images/iB13BBB34910D6E346D89A425DF9D9AA7.png",
		"id_164841395496": "/images/i9AD38C8643DBCC036D0D226508F2B971.png",
		"id_17304885117": "/images/i8E921D2D9D8781FB98CDAC70A1508CD5.png",
		"id_172501864820": "/images/i20546752F9D3EB6A38FD547BB45E2DA7.png",
		"id_17304885113": "/images/iAED1FDE3F57D021F59389F23987A43CC.png",
		"id_1648413954107": "/images/iB0DCEF19023ADDE9B38C97AF31540D4D.png",
		"id_164841395455": "/images/iDAED2AF86C310E481C7409143758ECB6.png",
		"id_164841395427": "/images/iA83DD447D387E7698AB46331B8A8D019.png",
		"id_17304885128": "/images/i46787C084B30E2C588FF57C442127DBE.png",
		"id_12451383642": "/images/iC926655337EBEE90D344237131E954A2.png",
		"id_12451383632": "/images/iDE34B298EE1EF399563C371BB1352EE0.png",
		"id_172501864816": "/images/i0B25A07F8BA6A3D1155E5635855725E3.png",
		"entrada_on": "/images/i93E2F664C26053BA732526F3E38C6FD3.png",
		"id_17304885146": "/images/i4D19A43D79882DDD81ADC3FEE88FC5E0.png",
		"id_3892058495": "/images/i1846D1705B39A0DFD7CA331E2A624A7B.png",
		"id_38920584920": "/images/i20546752F9D3EB6A38FD547BB45E2DA7.png",
		"id_16484139543": "/images/iE4617B7080958F45BF2C192CC042382D.png",
		"id_1648413954109": "/images/i8143DE4F0A77BC1F4111FF6A02EED95E.png",
		"id_164841395451": "/images/i00760138B01D3550976723B1B225C943.png",
		"id_17304885170": "/images/i7926A4414B70F4E087DBB9DC20D34D84.png",
		"id_38920584927": "/images/iCA260B7119401A0D456373D95DAD7824.png",
		"id_17304885132": "/images/i72E9850BB987DCF26DD0CC778C372383.png",
		"id_164841395486": "/images/i55288AB1A85C7038BCA8AD843ADF586F.png",
		"id_12451383660": "/images/iD730909ABD133F91A1EFBC1B1E5C70DA.png",
		"id_17304885172": "/images/i19277EEC1CB9EB9A319964D895656ECE.png",
		"id_17304885133": "/images/i14318836CA86ECEF8C2D9F7ADD5EABAE.png",
		"id_172501864812": "/images/iB13BBB34910D6E346D89A425DF9D9AA7.png",
		"id_1245138365": "/images/iBC6A9F0C0BC8FF1BFAAE65D31F1594AB.png",
		"pincho4": "/images/i999E1B96A6FB0BFB9034B067828F4A68.png",
		"id_12451383623": "/images/i87AA4E2C8283824C531D6EA19313D656.png",
		"id_164841395462": "/images/iD0A8DD7F8257D87CADD4557D2DDE27A0.png",
		"id_164841395490": "/images/i10CCB6F0BFED76841F1607FA34C062B1.png",
		"id_16484139544": "/images/i9290A0A895DCA701657AA3A2701BC5B1.png",
		"id_164841395480": "/images/iE34B2EECFDC74FC20A5DEA25E204DB7A.png",
		"id_164841395450": "/images/iA1B6778946A3A0358E419143E8081D2B.png",
		"id_12451383641": "/images/i7E452776BF9CF2E5102B37DB75E66F36.png",
		"id_1730488516": "/images/i9ABB2FA1BACD1BC4DAFC6D59EDE7E61F.png",
		"id_164841395479": "/images/iCF6F841564C2B00839FA0338556EE25E.png",
		"id_17304885134": "/images/i52476D8DE05175CC624D3B1F13DB9411.png"
	},
	"classes": {
		"#Largomt2": {
			"text": "L('x2558236558_traducir','Largo (mt)')"
		},
		"#iconoEntrar4": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "14dp"
			}
		},
		"#iconoUbicacion10": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#widgetHeader4": {
			"titulo": "L('x3752633607_traducir','PARTE 4: Disponibilidad de trabajo')",
			"avance": "L('x25508266_traducir','4/6')"
		},
		"#iconoVolver4": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "18dp"
			}
		},
		"#DescribaelDao": {
			"hintText": "L('x397976520_traducir','Describa el daño')"
		},
		"#FechaSiniestro": {
			"text": "L('x3210826773_traducir','Fecha Siniestro')"
		},
		"#Definirde": {
			"text": "L('x1350052795_traducir','Definir % de daños')"
		},
		"#Label10": {
			"title": "L('x1249164186_traducir','Hoy')"
		},
		"#ID_355170618": {
			"valor": "L('x3172123379','recintos[0].nombre')"
		},
		"#ID_163921178": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#widgetBotonlargo3": {
			"titulo": "L('x1524107289_traducir','CONTINUAR')"
		},
		"fondolila": {
			"backgroundColor": "#8383db",
			"font": {
				"fontSize": "12dp"
			}
		},
		"#widgetPreguntab": {
			"titulo": "L('x404129330_traducir','¿EL ASEGURADO CONFIRMA ESTOS DATOS?')",
			"si": "L('x1723413441_traducir','SI, Están correctos')",
			"texto": "L('x2083765231_traducir','El asegurado debe confirmar que los datos de esta sección están correctos')",
			"pantalla": "L('x2851883104_traducir','¿ESTA DE ACUERDO?')",
			"no": "L('x55492959_traducir','NO, Hay que modificar algo')"
		},
		"#Escriturade": {
			"text": "L('x1878155044_traducir','Escritura de Compraventa')"
		},
		"bg_progreso_celeste": {
			"backgroundColor": "#60bde2",
			"font": {
				"fontSize": "12dp"
			}
		},
		"#ID_915854835": {
			"on": "L('x1532004165','registro.d2')"
		},
		"btheader": {
			"color": "#2d9edb",
			"font": {
				"fontWeight": "bold",
				"fontSize": "18dp"
			}
		},
		"#iconoNuevo_dano2": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "18dp"
			}
		},
		"estilo8": {
			"color": "#ee7f7e",
			"font": {
				"fontSize": "12dp"
			}
		},
		"#PartTime": {
			"text": "L('x3964605301_traducir','Part time')"
		},
		"#ID_1775759583": {
			"mini": "L('x3176678487_traducir','foto3')"
		},
		"#Piso": {
			"text": "L('x1951430381_traducir','Piso')"
		},
		"#seccionListadoFechaentrantes": {
			"headerTitle": "L('x2345059420','fecha_entrantes')"
		},
		"#FechadeNacimiento": {
			"text": "L('x2443017383_traducir','Fecha de nacimiento')"
		},
		"#imagen": {
			"images": ["/images/iBC783890F4B9774991803EB6ECA81549.png", "/images/iC91EEFE37F9302D5B31812F55B1658CC.png", "/images/i8454D88E5895192B0770F08DB2F4C7D6.png", "/images/iCD6AA874B07BD726ABF7364ED6851236.png", "/images/iAB265FAD2F6EA6964A2299BAF0375F29.png", "/images/i9ABB2FA1BACD1BC4DAFC6D59EDE7E61F.png", "/images/i5C878AB139179D2864AE95A2540738B3.png", "/images/iFDDDAFE794E6B3D1640B628D4327FCD4.png", "/images/i21A429706C7934A4774996F025B3B1AF.png", "/images/i5CF917F99E733C468810E2F5D08CC88D.png", "/images/iE67E9F2E19A329505444E7465B5FD7D0.png", "/images/i3915CB7F49F97487FF413D7B39ED0B37.png", "/images/iAED1FDE3F57D021F59389F23987A43CC.png", "/images/i0E5D79CD99D2BBA09F0BEAA30A79A3D9.png", "/images/i1B011A35F1C8D4708DA9E83D549EA011.png", "/images/iD464C126D5638562F447AE472C2BD133.png", "/images/i8E921D2D9D8781FB98CDAC70A1508CD5.png", "/images/i37DC047D2355AA99AF3408D44781C90B.png", "/images/i400DF90467135851598103A438FA2CBE.png", "/images/i907B21FBECE83B7799349F9FAA0D472A.png", "/images/i901A29B5913762D207E0BEBD505A9A5F.png", "/images/iF6C82CEBB73C3F50FC5AC08E6FA50DF2.png", "/images/i5D746AFA0C7F3FCE385618C403086A21.png", "/images/i21006680354E2478937ED256C47F0D6F.png", "/images/iCB3B5FFBFA8DF33CB29C50BC26542162.png", "/images/i220830766E53497CDAC422E9AAE22296.png", "/images/i574DB1641BB1F722625EB07223F436E0.png", "/images/i46787C084B30E2C588FF57C442127DBE.png", "/images/i97EEC684C8F34E49FB30501C8417541F.png", "/images/iEA1F5243153E7D22D525312FDFAE5A90.png", "/images/i829FF9A81D8C1537788109FCE55BA372.png", "/images/i72E9850BB987DCF26DD0CC778C372383.png", "/images/i14318836CA86ECEF8C2D9F7ADD5EABAE.png", "/images/i52476D8DE05175CC624D3B1F13DB9411.png", "/images/iE289D15EF298A75107CE5D8C53D93DFA.png", "/images/iCA6997D4EA75B7A7EA52E0BBCF58089A.png", "/images/i77C1E7F3FBBE442E1B12C53126B13B1B.png", "/images/iF746C788F42D49A6AD715DC319BFF2B7.png", "/images/i570A2B785DF574495DD66D6D91299947.png", "/images/iE1BAB1BE32595937A03D896936E17846.png", "/images/i09B77564ECA69742FF6926701ADCC788.png", "/images/iF0674BDE61F66D48C2CA74C15C3AC823.png", "/images/iC35735FF5486867B522A8ADFAFC3B6C7.png", "/images/i87DE3A07ABFD59B0F96861DEC12DDF8F.png", "/images/iEDCBD1534AE3D23AD5893D34EEA1BB5C.png", "/images/i4D19A43D79882DDD81ADC3FEE88FC5E0.png", "/images/i7F291EBC02606240803647C45234D01B.png", "/images/i77F111A1F32961B1F386B1DC7377767D.png", "/images/i47F6C47BCA8C1146589A6899C5016EA7.png", "/images/iF37AFC4AE87333385D84EF93517E2F25.png", "/images/i093B3780EEFCFADC8E24C0D54540C0B5.png", "/images/i49BA5221BED2176E506AA26FABE14E75.png", "/images/iF065FCB3FEA5C1EBC4123E4F1156C63D.png", "/images/iA06FB0DE96CD4315F67874E0FBDB4EE1.png", "/images/i94E3A2CA38A4F73C31EF4B2C5D5BE94B.png", "/images/i98CA10AE81168639ED3D051D83F9FF4A.png", "/images/i89E93536F928FB549C7EEE44EF370DCE.png", "/images/iAADD9B59546B4EABA48E369D34ED0C21.png", "/images/i771C97B5058ACE91E7A66AD6D47C1934.png", "/images/i749E117F5781F156EF8FE76959C4144F.png", "/images/iD07A2DFA41B1AF2BAD9EC1879066DBE8.png", "/images/iB32E3B863CF1AC37F2816A7BCD60A554.png", "/images/iF77E81CC3C9BE873EB33BA0AAD43D591.png", "/images/i79D6CE5169F51E74EF1FEA39DB07CEB9.png", "/images/i8C849BCE5D5B003A5C94BC535D7C65EC.png", "/images/i0DE5EA646AA925B3530150BF5F230AA5.png", "/images/i3137CC503DA91A0688C5F38A0095A244.png", "/images/i8B825AD136827F17DB6D478184C85301.png", "/images/i7C907FDBDA8C040F234553C55D821A45.png", "/images/i7926A4414B70F4E087DBB9DC20D34D84.png", "/images/i9136DCA319DE76A1A2437B6415790BB9.png", "/images/i19277EEC1CB9EB9A319964D895656ECE.png"]
		},
		"#SeIniciarel": {
			"text": "L('x3731894882_traducir','Se iniciará el proceso de inspección')"
		},
		"fondorosado": {
			"backgroundColor": "#ffacaa",
			"font": {
				"fontSize": "12dp"
			}
		},
		"#widgetMono": {
			"titulo": "L('x2349703141_traducir','NO TIENES TAREAS')",
			"texto": "L('x2279881512_traducir','Asegurate de tomar tareas para hoy y revisar tu ruta')"
		},
		"#Definirde3": {
			"text": "L('x1350052795_traducir','Definir % de daños')"
		},
		"#Contrasea": {
			"text": "L('x3861721694_traducir','Contraseña')"
		},
		"#ID_611316667": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#ENROLAMIENTO3": {
			"title": "L('x2146494644_traducir','ENROLAMIENTO')"
		},
		"#widgetModal3": {
			"titulo": "L('x2059554513_traducir','ENTIDAD')",
			"cargando": "L('x1740321226_traducir','cargando ..')",
			"campo": "L('x2538351238_traducir','Entidad Financiera')",
			"seleccione": "L('x3782298892_traducir','seleccione entidad')"
		},
		"#AseguradorCorredor": {
			"text": "L('x2324757213_traducir','Asegurador/Corredor')"
		},
		"#ID_863076866": {
			"hasta": "L('x2081216563','hasta_horas[0]')"
		},
		"#NoCierrela": {
			"text": "L('x1187778027_traducir','No cierre la aplicacion y asegurese de estar conectado a internet')"
		},
		"#icono7": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "15dp"
			}
		},
		"#label2": {
			"title": "L('x3567030427_traducir','TAREAS ENTRANTES')"
		},
		"#iconoComuna3": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#iconoContinuar": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "20dp"
			}
		},
		"#PedroHerrera": {
			"text": "L('x4000049319_traducir','Pedro Herrera Silva')"
		},
		"#ID_60862390": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#iconoCaso": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "18dp"
			}
		},
		"#NO3": {
			"text": "L('x3376426101_traducir','NO')"
		},
		"#widgetPreguntas": {
			"titulo": "L('x404129330_traducir','¿EL ASEGURADO CONFIRMA ESTOS DATOS?')",
			"si": "L('x1723413441_traducir','SI, Están correctos')",
			"texto": "L('x2083765231_traducir','El asegurado debe confirmar que los datos de esta sección están correctos')",
			"pantalla": "L('x2851883104_traducir','¿ESTA DE ACUERDO?')",
			"no": "L('x55492959_traducir','NO, Hay que modificar algo')",
			"header": "L('x2241007166_traducir','naranjo')"
		},
		"#widgetPelota7": {
			"letra": "L('x2746444292','D')"
		},
		"#Superficiem": {
			"text": "L('x4117795806_traducir','Superficie (m2)')"
		},
		"#ID_321453933": {
			"vertexto": "L('x2310355463','true')",
			"verprogreso": "L('x2001657581_traducir','false')"
		},
		"#ID_1327135108": {
			"data": "L('x1872543855','data_region')"
		},
		"#MximoCaracteres": {
			"hintText": "L('x3505400086_traducir','Máximo 140 caracteres')"
		},
		"#iconoVolver6": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "18dp"
			}
		},
		"#Altomt2": {
			"text": "L('x2760387473_traducir','Alto (mt)')"
		},
		"#RepitasuCorreo": {
			"hintText": "L('x2070103557_traducir','repita su correo')"
		},
		"#AoDelNivel": {
			"text": "L('x2973460117_traducir','Año del Nivel')"
		},
		"#iconoVolver": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "20dp"
			}
		},
		"#ENROLAMIENTO5": {
			"title": "L('x2146494644_traducir','ENROLAMIENTO')"
		},
		"fondomorado": {
			"backgroundColor": "#b9aaf3",
			"font": {
				"fontSize": "12dp"
			}
		},
		"#NroCasoSistema": {
			"text": "L('x2151727540_traducir','Nro caso sistema')"
		},
		"#ID_1755646569": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#ID_238375036": {
			"valor": "L('x2491491901','u_medida[0].nombre')"
		},
		"#Desde": {
			"text": "L('x2611428876_traducir','desde:')"
		},
		"#Presupuesto": {
			"text": "L('x2010049695_traducir','Presupuesto de Reparaciones del edificio en formato Excel, detallado por recintos a través de precios unitarios, según la siguiente descripcion: Partida, Unidad, Cantidad, Valor Unitario y Valor Total, incluyendo adicionalmente los Gastos Generales, Utilidad e IVA. (Se entrega formato completo).')"
		},
		"#widgetPelota6": {
			"letra": "L('x543223747','S')"
		},
		"#iconoUbicacion4": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#ID_201283538": {
			"valor": "L('x4264937424','evento.valor')"
		},
		"estilo9": {
			"font": {
				"fontSize": "16dp"
			}
		},
		"#Contenidos": {
			"text": "L('x2486765391_traducir','Contenidos')"
		},
		"#FechaInspeccion": {
			"text": "L('x2793345159_traducir','Fecha inspeccion')"
		},
		"#ID_138241524": {
			"imagen2": "L('x1347115794_traducir','elemento')",
			"nueva": "L('x1112834923','640')"
		},
		"#ID_48701328": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#Disponibilidad": {
			"text": "L('x2249118711_traducir','Disponibilidad')"
		},
		"#ID_244368868": {
			"bono": "L('x1979562742','tareas[0].bono')"
		},
		"#ID_21955821": {
			"texto": "L('x1414281015_traducir','Tip: Al iniciar seguimiento, le avisaremos al cliente que vas en camino.')"
		},
		"#widgetModalmultiple13": {
			"titulo": "L('x2266302645_traducir','Cubierta')",
			"cargando": "L('x1740321226_traducir','cargando ..')",
			"hint": "L('x2134385782_traducir','Seleccione cubiertas')",
			"subtitulo": "L('x4011106049_traducir','Indique los tipos')"
		},
		"#TomarFotosDel": {
			"text": "L('x1885200639_traducir','Tomar fotos del daño')"
		},
		"#Direccin": {
			"text": "L('x1396136993_traducir','Dirección')"
		},
		"#widgetModalmultiple3": {
			"titulo": "L('x1219835481_traducir','Muros / Tabiques')",
			"cargando": "L('x1740321226_traducir','cargando ..')",
			"hint": "L('x2879998099_traducir','Seleccione muros')",
			"subtitulo": "L('x4011106049_traducir','Indique los tipos')"
		},
		"#iconoUbicacion": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#seccionListadoOtra": {
			"headerTitle": "L('x910537343_traducir','otra')"
		},
		"#widgetModal8": {
			"titulo": "L('x2525275670_traducir','PARTIDA')",
			"cargando": "L('x1740321226_traducir','cargando ..')",
			"campo": "L('x1719427472_traducir','Partida')",
			"seleccione": "L('x221750731_traducir','seleccione partida')"
		},
		"#ID_488751613": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#iconoCiudad7": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#SeRecomienda": {
			"text": "L('x2439138629_traducir','¿Se recomienda analisis estructural por especialista?')"
		},
		"prioridad_otro": {
			"color": "#a5876d",
			"font": {
				"fontSize": "15dp"
			}
		},
		"#StatusEsperando": {
			"text": "L('x2451619465_traducir','status: esperando')"
		},
		"fondoverde": {
			"backgroundColor": "#8ce5bd",
			"font": {
				"fontSize": "12dp"
			}
		},
		"#ID_1098579444": {
			"on": "L('x743946195','registro.d3')"
		},
		"#ColoqueRango": {
			"text": "L('x3172022095_traducir','Coloque rango de horas')"
		},
		"#EntraParaEditar": {
			"text": "L('x2434817046_traducir','Entra para editar tus datos')"
		},
		"#ConfirmaTus": {
			"text": "L('x22218330_traducir','Confirma tus tareas de hoy')"
		},
		"#ID_241989500": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#inicio": {
			"title": "L('x3719714762_traducir','inicio')"
		},
		"#IngreseNombre": {
			"hintText": "L('x314662607_traducir','Ingrese nombre')"
		},
		"#Evaluacionde": {
			"text": "L('x2467432966_traducir','Evaluacion de daños generales')"
		},
		"#Piso2": {
			"text": "L('x1951430381_traducir','Piso')"
		},
		"#AodeConstruccion": {
			"text": "L('x2722748660_traducir','Año de construccion')"
		},
		"#icono4": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "15dp"
			}
		},
		"#Recinto": {
			"text": "L('x1035217420_traducir','Recinto')"
		},
		"estilo8_2": {
			"color": "#ee7f7e",
			"font": {
				"fontFamily": "SFUIText-Medium",
				"fontSize": "10dp"
			}
		},
		"#widgetModal9": {
			"titulo": "L('x1384515306_traducir','TIPO DAÑO')",
			"cargando": "L('x1740321226_traducir','cargando ..')",
			"campo": "L('x953397154_traducir','Tipo de Daño')",
			"seleccione": "L('x4123108455_traducir','seleccione tipo')"
		},
		"#campo5": {
			"hintText": "L('x4108050209','0')"
		},
		"#iconoEntrar5": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "14dp"
			}
		},
		"#iconoUbicacion2": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#ID_1816801192": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#ID_1014684330": {
			"valor": "L('x1367393739','dato_n1[0].id_label')"
		},
		"#EstaListo": {
			"text": "L('x3976272881_traducir','¿ Esta listo ?')"
		},
		"#EnPresencia": {
			"text": "L('x2016219634_traducir','En presencia de')"
		},
		"#NombreAsegurado": {
			"text": "L('x1960877763_traducir','NombreAsegurado')"
		},
		"#Correo": {
			"text": "L('x1890062079_traducir','Correo')"
		},
		"#iconoCiudad2": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#RUTDelPresente": {
			"text": "L('x3652084882_traducir','RUT del presente')"
		},
		"#widgetHeader3": {
			"titulo": "L('x233750493_traducir','PARTE 3: Domicilio')",
			"avance": "L('x80359215_traducir','3/6')"
		},
		"#ListadodeContenidos": {
			"text": "L('x2188845716_traducir','Listado de Contenidos Afectados Valorizados, detallado por: Tipo de bien, Marca, Modelo, Edad y Valor Estimado (se entrega formato ejemplo).')"
		},
		"#ID_960197216": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#RUTDelAsegurado": {
			"text": "L('x2270754326_traducir','RUT del asegurado')"
		},
		"#ID_1762464778": {
			"mini": "L('x3176678487_traducir','foto3')"
		},
		"#ID_1963341563": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#ID_117470963": {
			"valor": "L('x4168777795','bienes[0].nombre')"
		},
		"#Telfono": {
			"text": "L('x2354316101_traducir','Teléfono')"
		},
		"#MIS_TAREAS": {
			"title": "L('x271733139_traducir','MIS TAREAS')"
		},
		"#Nombre3": {
			"text": "L('x1027380240_traducir','Nombre')"
		},
		"#Escribir": {
			"hintText": "L('x714216034_traducir','escribir')"
		},
		"#widgetBoton": {
			"texto": "L('x990554165_traducir','PRESIONE PARA VERIFICAR')",
			"vertexto": "L('x4261170317','true')",
			"verprogreso": "L('x734881840_traducir','false')"
		},
		"#ID_1676699588": {
			"mini": "L('x3395253441_traducir','foto2')"
		},
		"#AoDelNivel2": {
			"text": "L('x2973460117_traducir','Año del Nivel')"
		},
		"#ID_156411241": {
			"vertexto": "L('x2001657581_traducir','false')",
			"verprogreso": "L('x2310355463','true')"
		},
		"#iconoEntrar6": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "14dp"
			}
		},
		"#iconoSalir_insp": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "20dp"
			}
		},
		"#ID_1852315924": {
			"texto": "L('x1488972008_traducir','Tip: No puedes iniciar inspecciones para tareas que no son para el dia')"
		},
		"#ENVIANDOINSPECCIONES": {
			"text": "L('x2721945877_traducir','ENVIANDO INSPECCIONES')"
		},
		"#iconoUbicacion5": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"porcentaje": {
			"color": "#333333",
			"font": {
				"fontWeight": "bold",
				"fontSize": "18dp"
			}
		},
		"#HAY_ALGUIEN": {
			"title": "L('x1489379407_traducir','¿HAY ALGUIEN?')"
		},
		"#ApellidoMaterno": {
			"text": "L('x3313466056_traducir','Apellido Materno')"
		},
		"#ID_233047728": {
			"imagen": "L('x3940301797_traducir','foto')",
			"calidad": "L('x511942527_traducir','91')",
			"nueva": "L('x1112834923','640')"
		},
		"#widgetModalmultiple4": {
			"titulo": "L('x3327059844_traducir','Entrepisos')",
			"cargando": "L('x1740321226_traducir','cargando ..')",
			"hint": "L('x2146928948_traducir','Seleccione entrepisos')",
			"subtitulo": "L('x4011106049_traducir','Indique los tipos')"
		},
		"#TomarFotosde": {
			"text": "L('x2601646010_traducir','Tomar fotos de Recinto')"
		},
		"#TuCasa": {
			"title": "L('x2398692269_traducir','Tu casa')"
		},
		"#ID_1501498971": {
			"pantalla": "L('x3681655494_traducir','caracteristicas')"
		},
		"#REGISTRARSE": {
			"title": "L('x2745628043_traducir','REGISTRARSE')"
		},
		"#ID_897626244": {
			"vertexto": "L('x2310355463','true')",
			"verprogreso": "L('x2001657581_traducir','false')"
		},
		"#widgetBotonlargo5": {
			"titulo": "L('x1524107289_traducir','CONTINUAR')"
		},
		"#ID_747197978": {
			"monito": "L('x2310355463','true')"
		},
		"estilo14": {
			"color": "#fcbd83",
			"font": {
				"fontWeight": "bold",
				"fontSize": "15dp"
			}
		},
		"#widget4fotos": {
			"label1": "L('x724488137_traducir','Fachada')",
			"label4": "L('x676841594_traducir','Nº Depto')",
			"label3": "L('x4076266664_traducir','Numero')",
			"label2": "L('x2825989175_traducir','Barrio')"
		},
		"#ID_266965602": {
			"imagen": "L('x3940301797_traducir','foto')",
			"calidad": "L('x511942527_traducir','91')",
			"nueva": "L('x1112834923','640')"
		},
		"#ID_1576008367": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#icono5": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "15dp"
			}
		},
		"#iconoRegistrarse": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "17dp"
			}
		},
		"#iconoEntrar": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "14dp"
			}
		},
		"#iconoEntrar3": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "16dp"
			}
		},
		"#ID_628891891": {
			"pantalla": "L('x2627320454','insp_cancelada')"
		},
		"#SI": {
			"text": "L('x3746555228_traducir','SI')"
		},
		"#iconoUbicacion7": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#ID_1156891271": {
			"nueva": "L('x1112834923','640')",
			"imagen4": "L('x1347115794_traducir','elemento')"
		},
		"#EditaCadaItem": {
			"text": "L('x2087500331_traducir','Edita cada item independientemente')"
		},
		"#widgetBotonlargo": {
			"titulo": "L('x1678967761_traducir','GUARDAR RECINTOS')"
		},
		"estilo7": {
			"color": "#999999",
			"font": {
				"fontFamily": "SFUIText-Medium",
				"fontSize": "14dp"
			}
		},
		"#ID_420132078": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#ID_848195789": {
			"on": "L('x725251018','registro.d7')"
		},
		"#icono3": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "15dp"
			}
		},
		"#ID_1050761344": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"fondorojo": {
			"backgroundColor": "#ee7f7e",
			"font": {
				"fontSize": "12dp"
			}
		},
		"#widgetFotochica6": {
			"caja": "L('x2889884971_traducir','45')"
		},
		"#ID_1428144668": {
			"valor": "L('x3452300925_traducir','estructurasElegidos')"
		},
		"#RepetirCorreo": {
			"text": "L('x3920081714_traducir','Repetir correo')"
		},
		"#SinPeaje": {
			"title": "L('x1633918740_traducir','Sin Peaje')"
		},
		"#DocumentosSolicitados": {
			"text": "L('x3381833431_traducir','Documentos solicitados al asegurado')"
		},
		"#ID_848421275": {
			"vertexto": "L('x2310355463','true')",
			"verprogreso": "L('x2001657581_traducir','false')"
		},
		"#IngreseUsuario": {
			"hintText": "L('x1935574480_traducir','ingrese usuario')"
		},
		"#iconoRefresh": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "18dp"
			}
		},
		"#ID_705519987": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#widgetPelota3": {
			"letra": "L('x185522819_traducir','MI')"
		},
		"#widgetYpicker": {
			"aceptar": "L('x1518866076_traducir','Aceptar')",
			"cancelar": "L('x2353348866_traducir','Cancelar')"
		},
		"estilo11": {
			"color": "#2d9edb",
			"font": {
				"fontWeight": "bold",
				"fontSize": "15dp"
			}
		},
		"#PRESIONAPARA": {
			"text": "L('x3327316334_traducir','PRESIONA PARA CONFIRMAR')"
		},
		"#Niveles": {
			"text": "L('x1283592374_traducir','Niveles')"
		},
		"#ENVIAR": {
			"text": "L('x4106122489_traducir','ENVIAR')"
		},
		"#Meses2": {
			"text": "L('x742992573_traducir','meses')"
		},
		"#ENROLAMIENTO7": {
			"title": "L('x2146494644_traducir','ENROLAMIENTO')"
		},
		"#Especificaciones": {
			"text": "L('x2458576127_traducir','Especificaciones Técnicas de la Construcción')"
		},
		"#widgetPelota4": {
			"letra": "L('x1141589763','J')"
		},
		"#iconoCiudad3": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#ASEGURATEDE": {
			"text": "L('x1820953783_traducir','ASEGURATE DE ENVIAR LA INSPECCION TERMINADA EN TU PERFIL')"
		},
		"#ID_1748478756": {
			"valor": "L('x4264937424','evento.valor')"
		},
		"#ID_1327360331": {
			"pantalla": "L('x3772634211_traducir','siniestro')"
		},
		"#NO5": {
			"text": "L('x3376426101_traducir','NO')"
		},
		"#ID_1907635340": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#ID_1787217865": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"estilo5_1": {
			"color": "#c0c0c7",
			"font": {
				"fontSize": "17dp"
			}
		},
		"#ID_1647538814": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#iconoComuna10": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#Asegurado": {
			"text": "L('x2764946924_traducir','Asegurado')"
		},
		"#ConPeaje": {
			"title": "L('x197973218_traducir','Con Peaje')"
		},
		"fondoceleste": {
			"backgroundColor": "#8bc9e8",
			"font": {
				"fontSize": "12dp"
			}
		},
		"#EstsDispuesto2": {
			"text": "L('x529917922_traducir','¿Estás dispuesto a viajar fuera de tu país?')"
		},
		"estilo13": {
			"color": "#ffffff",
			"font": {
				"fontSize": "12dp"
			}
		},
		"#icono8": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "15dp"
			}
		},
		"#Respaldosde": {
			"text": "L('x3534566262_traducir','Respaldos de Adquisición de Bienes Afectados (Facturas, Boletas, Cartolas de Casas Comerciales, Catálogos, Fotos)')"
		},
		"#widgetModalmultiple9": {
			"titulo": "L('x1219835481_traducir','Muros / Tabiques')",
			"cargando": "L('x1740321226_traducir','cargando ..')",
			"hint": "L('x2879998099_traducir','Seleccione muros')",
			"subtitulo": "L('x4011106049_traducir','Indique los tipos')"
		},
		"#IngreseAo": {
			"hintText": "L('x3573583009_traducir','Ingrese año')"
		},
		"#iconoCiudad10": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#Edificio": {
			"text": "L('x664979134_traducir','Edificio')"
		},
		"#ENROLAMIENTO": {
			"title": "L('x2146494644_traducir','ENROLAMIENTO')"
		},
		"#MEJORRUTA": {
			"text": "L('x1110261063_traducir','MEJOR RUTA')"
		},
		"#iconoEntrar7": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "14dp"
			}
		},
		"#iconoEnviar_insp2": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "15dp"
			}
		},
		"#NO13": {
			"text": "L('x3376426101_traducir','NO')"
		},
		"#ValorDelObjeto": {
			"text": "L('x1523866503_traducir','Valor del Objeto')"
		},
		"#ID_1905744782": {
			"valor": "L('x3432014939','t_dano[0].nombre')"
		},
		"#ID_1697960401": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#Inhabitable": {
			"text": "L('x2452838785_traducir','¿Inhabitable?')"
		},
		"#Superficie": {
			"text": "L('x2877523867_traducir','Superficie')"
		},
		"#ID_824066568": {
			"seleccione": "L('x3621251639_traducir','unidad')"
		},
		"#ID_497149076": {
			"data": "L('x2347414356','data_oficios')"
		},
		"#Guardar": {
			"title": "L('x2943883035_traducir','Guardar')"
		},
		"#iconoCiudad6": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#Asegratede": {
			"text": "L('x1626050066_traducir','Asegúrate de haber ingresado todos los recintos')"
		},
		"#NUEVO_NIVEL": {
			"title": "L('x2578278336_traducir','NUEVO NIVEL')"
		},
		"#ASEGURADOSE": {
			"text": "L('x2536760995_traducir','ASEGURADO SE COMPROMETE EN ENVIAR LOS ANTECEDENTES SOLICITADOS EN:')"
		},
		"#OtrosEspecificar": {
			"text": "L('x939625677_traducir','Otros (Especificar)')"
		},
		"bg_progreso_rosado": {
			"backgroundColor": "#ff9292",
			"font": {
				"fontSize": "12dp"
			}
		},
		"#Descripcion": {
			"text": "L('x435194361_traducir','Descripcion del siniestro')"
		},
		"#Meses": {
			"hintText": "L('x742992573_traducir','meses')"
		},
		"#widgetPelota2": {
			"letra": "L('x3664761504','M')"
		},
		"#iconoComuna7": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#FINALIZADO": {
			"title": "L('x1452485313_traducir','FINALIZADO')"
		},
		"#widgetPelota": {
			"letra": "L('x2909332022','L')"
		},
		"#CuntosDas": {
			"text": "L('x3307875748_traducir','¿Cuántos días?')"
		},
		"#IngresesuTelfono": {
			"hintText": "L('x93906838_traducir','ingrese su teléfono')"
		},
		"#PERFIL": {
			"title": "L('x1721610689_traducir','PERFIL')"
		},
		"#icono10": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "15dp"
			}
		},
		"#ID_362822250": {
			"on": "L('x2989695600','registro.d4')"
		},
		"#Superficiem2": {
			"text": "L('x4117795806_traducir','Superficie (m2)')"
		},
		"#ID_494103805": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#widgetModalmultiple10": {
			"titulo": "L('x3327059844_traducir','Entrepisos')",
			"cargando": "L('x1740321226_traducir','cargando ..')",
			"hint": "L('x2146928948_traducir','Seleccione entrepisos')",
			"subtitulo": "L('x4011106049_traducir','Indique los tipos')"
		},
		"#seccionListadoFecha": {
			"headerTitle": "L('x27834329_traducir','fecha')"
		},
		"#AgregueRecintos": {
			"text": "L('x1886674359_traducir','Agregue recintos para indicar daños')"
		},
		"#DetallarExperiencia": {
			"text": "L('x2089188151_traducir','Detallar experiencia (Quedan 140 caracteres)')"
		},
		"#EditarContactos": {
			"text": "L('x574786063_traducir','Editar Contactos')"
		},
		"#ID_252175981": {
			"texto": "L('x886429039_traducir','Tip: No puedes iniciar inspecciones sin confirmar tus tareas del dia')"
		},
		"#EDITAR_NIVEL": {
			"title": "L('x902724175_traducir','EDITAR NIVEL')"
		},
		"#icono6": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "15dp"
			}
		},
		"#RepitasuTelfono": {
			"hintText": "L('x1125245_traducir','repita su teléfono')"
		},
		"#iconoTelefono": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "16dp"
			}
		},
		"#ID_1164248528": {
			"monito": "L('x2310355463','true')"
		},
		"#ID_1972845563": {
			"imagen": "L('x3940301797_traducir','foto')",
			"calidad": "L('x511942527_traducir','91')",
			"nueva": "L('x1112834923','640')"
		},
		"titulo_niveles": {
			"color": "#8ce5bd",
			"font": {
				"fontWeight": "bold",
				"fontFamily": "SFUIText-Medium",
				"fontSize": "16dp"
			}
		},
		"fondooscuro": {
			"color": "#878787",
			"backgroundColor": "#878787",
			"font": {
				"fontSize": "12dp"
			}
		},
		"#ID_277265460": {
			"desde": "L('x828196665','desde_horas[0]')"
		},
		"#RECINTOS": {
			"title": "L('x767609104_traducir','RECINTOS')"
		},
		"#itemListado": {
			"_section": "L('x2819214932_traducir','holi1')"
		},
		"#iconoCamara": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "36dp"
			}
		},
		"#iconoCerrar": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "18dp"
			}
		},
		"#ID_754196014": {
			"imagen1": "L('x1347115794_traducir','elemento')",
			"nueva": "L('x1112834923','640')"
		},
		"#CARACTERISTICAS": {
			"title": "L('x2004545330_traducir','CARACTERISTICAS')"
		},
		"#icono1": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "15dp"
			}
		},
		"#iconoComuna8": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"fondoamarillo": {
			"backgroundColor": "#f8da54",
			"font": {
				"fontSize": "12dp"
			}
		},
		"#ID_598691919": {
			"on": "L('x3308524262','registro.d5')"
		},
		"#Label11": {
			"title": "L('x2445898609_traducir','Perfil')"
		},
		"#ID_278574846": {
			"valor": "L('x4264937424','evento.valor')"
		},
		"#Nivel2": {
			"text": "L('x2028852218_traducir','nivel 3')"
		},
		"#FullTime": {
			"text": "L('x318795834_traducir','Full time')"
		},
		"#widgetFotochica4": {
			"caja": "L('x2889884971_traducir','45')"
		},
		"#Label": {
			"text": "L('x4108050209','0')"
		},
		"#iconoEntrar10": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "14dp"
			}
		},
		"#imagen": {
			"images": ["/images/iB1F901AEFA79EE1F3A05C41650B368A4.png", "/images/iBD0F891E767949293A5E319C70468E61.png", "/images/iE18490E3CB42EE21FA2CA39791E6EE07.png", "/images/i058F2A04E46A9135BF2007D00D84E610.png", "/images/iBC6A9F0C0BC8FF1BFAAE65D31F1594AB.png", "/images/iC9EDBE23CD4A8C3ACACF6159FAE7896F.png", "/images/i0337CBA53F3589380EFAC3F505EDAFF1.png", "/images/i553359F66995CFAE34B27D5D0D4C95E0.png", "/images/iBD1E17BE427423F6FA053D07C27B494E.png", "/images/iFE945B7620AFD4BCA1698FF05C97EEDF.png", "/images/i889E7951CC79C107C7467F773D851F53.png", "/images/i1DC1B37D94F0D03FB92F1569E2D04139.png", "/images/i69B0B3F2A0625ADD474DEFFDE5FB783D.png", "/images/i5CCD03CB61FDCAE6B11B30DC864BAE99.png", "/images/iDD8E3DB6271506344EFC962A6F6C907E.png", "/images/i4BE78669A95AFE52AA53FBDD4361D3E4.png", "/images/i29429A8CA3B30681CA2BB487FE2DDF26.png", "/images/i69D7DC35EF1C27805FA3D2751325A855.png", "/images/i7F617027CD4EC4189C4E8CED725823AC.png", "/images/i0C1216BE3263895DD5C0DCCE921413F2.png", "/images/i52CB6FB175FB303E19F6C3CEA9C8D640.png", "/images/iD51F4417351E1B84F7F9B808956CD544.png", "/images/i87AA4E2C8283824C531D6EA19313D656.png", "/images/iFC47B0B749489B593F6282CEBE075C92.png", "/images/iC37FBC50BE131D22685223C641DA5C9B.png", "/images/i10257EAFE36B26C91213946BD6B916BE.png", "/images/i64703C2596335309216F0F02CBE47A0A.png", "/images/i28054D3A888BD526A6B71FA0D72B495E.png", "/images/iE4678809AAB8130B87C7E9341736D608.png", "/images/i77B67FB15DAF2AA6C7DFAB3AEF96CC3D.png", "/images/i1352BFB08B32246214800F652EA481A9.png", "/images/iDE34B298EE1EF399563C371BB1352EE0.png", "/images/iF78AD53B709F20115D8977BF9F612522.png", "/images/iB9E2226298DF133710B12402166BE94F.png", "/images/iE89BCB17377B1DC1F77A2DE2512A28EC.png", "/images/iE8162669F36B070D6B7D99F53757E770.png", "/images/i642B66133CEB6E403BB536FBADA0E681.png", "/images/i634ADF5364D3A8C7F50C71FE6DD90A80.png", "/images/i9CFC507846CDD4330DF7FDF1C0B0C31E.png", "/images/iE9825E0C72E7F6D3C9F3E8C359F44947.png", "/images/i7E452776BF9CF2E5102B37DB75E66F36.png", "/images/iC926655337EBEE90D344237131E954A2.png", "/images/i8F4CE4FFC4978421CD759A09A64FD5DF.png", "/images/i10280424F1EE424A6A514F0F16858345.png", "/images/i00F9DCB834689DD92A6F5679DF478CEC.png", "/images/i3310017A13BD47BA4E4549FD12A7788A.png", "/images/iE77D1DCF0D78AED4D64B7BE4DEC18855.png", "/images/iF70C2EE509A92FCB43AF685DDD2B2FA2.png", "/images/iD68DDDF47F8B5A8C859DEE487442816E.png", "/images/i9C794F568EA9A9383A579EBC118ECE1C.png", "/images/i3D15430021DB8CCF5FC81650384162EA.png", "/images/iA9CA15D575EC6434CD632DD4B0458E02.png", "/images/i129B9502415F1FFEF9E8936317CEFF50.png", "/images/i54B57D779036FF5C5902278D3A9A329A.png", "/images/iFAD70BC931105FCA07FD8E0DAEAF4137.png", "/images/iC9AFD1E7FA9C0486C5424E816865BE8C.png", "/images/i2A3F902C97F8E816FBF43D6072C8EA38.png", "/images/i487CD84F81C016C6569519B588DF619F.png", "/images/iC217A8FE8F59CC255970BBB562470CC2.png", "/images/iD730909ABD133F91A1EFBC1B1E5C70DA.png"]
		},
		"estilo1": {
			"color": "#4d4d4d",
			"font": {
				"fontSize": "15dp"
			}
		},
		"#Caso": {
			"text": "L('x2564648986_traducir','caso')"
		},
		"#iconoCiudad8": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#ID_1976003288": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#iconoUbicacion9": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#FOTOS_REQUERIDAS": {
			"title": "L('x810026296_traducir','FOTOS REQUERIDAS')"
		},
		"#FechadeNacimiento2": {
			"text": "L('x2443017383_traducir','Fecha de nacimiento')"
		},
		"#EMail": {
			"text": "L('x123080099_traducir','E-Mail')"
		},
		"#iconoComuna2": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#ApellidoMaterno2": {
			"hintText": "L('x180503380_traducir','Apellido materno')"
		},
		"#IngreseNombre2": {
			"hintText": "L('x314662607_traducir','Ingrese nombre')"
		},
		"#PlanosyMemoria": {
			"text": "L('x1323451372_traducir','Planos y Memoria de Cálculo Estructural')"
		},
		"#EscribaNombre": {
			"hintText": "L('x3329282304_traducir','Escriba nombre del recinto')"
		},
		"#DireccinRiesgo": {
			"text": "L('x4067701965_traducir','Dirección Riesgo')"
		},
		"#Certificado": {
			"text": "L('x1431676631_traducir','Certificado de Dominio Vigente Actualizado')"
		},
		"#StatusEsperando": {
			"text": "L('x2451619465_traducir','status: esperando')"
		},
		"#ID_402705466": {
			"valor": "L('x4264937424','evento.valor')"
		},
		"#ID_359431236": {
			"valor": "L('x775199465_traducir','entrepisoElegidos')"
		},
		"#EscribirNombre": {
			"hintText": "L('x1731734927_traducir','Escribir nombre')"
		},
		"estilo4": {
			"color": "#4d4d4d",
			"font": {
				"fontSize": "16dp"
			}
		},
		"#Altomt": {
			"text": "L('x2760387473_traducir','Alto (mt)')"
		},
		"#Descripcin": {
			"text": "L('x1756503588_traducir','Descripción del daño')"
		},
		"#widgetModal4": {
			"titulo": "L('x1629775439_traducir','MONEDAS')",
			"cargando": "L('x1740321226_traducir','cargando ..')",
			"campo": "L('x3081186843_traducir','Moneda')",
			"seleccione": "L('x2547889144','-')"
		},
		"#PlanosdeArquitectura": {
			"text": "L('x1344045403_traducir','Planos de Arquitectura (Plantas, Elevaciones y Cortes)')"
		},
		"desc": {
			"color": "#838383",
			"font": {
				"fontFamily": "SFUIText-Medium",
				"fontSize": "11dp"
			}
		},
		"#Anchomt2": {
			"text": "L('x4100297395_traducir','Ancho (mt)')"
		},
		"#seccionListadoRef2": {
			"headerTitle": "L('x3424088795_traducir','ref1')"
		},
		"#seccionListadoHoli": {
			"headerTitle": "L('x2819214932_traducir','holi1')"
		},
		"#ENROLAMIENTO6": {
			"title": "L('x2146494644_traducir','ENROLAMIENTO')"
		},
		"#seccionListadoHoy": {
			"headerTitle": "L('x1916403066_traducir','hoy')"
		},
		"#FIRMA_CLIENTE": {
			"title": "L('x2464850483_traducir','FIRMA CLIENTE')"
		},
		"estilo8_1": {
			"color": "#ee7f7e",
			"font": {
				"fontFamily": "SFUIText-Medium",
				"fontSize": "15dp"
			}
		},
		"#ENROLAMIENTO2": {
			"title": "L('x2146494644_traducir','ENROLAMIENTO')"
		},
		"#Siguiente": {
			"title": "L('x3683967382_traducir','Siguiente')"
		},
		"#PrimeroDebemos": {
			"text": "L('x1692519842_traducir','Primero debemos verificar tu disponibilidad')"
		},
		"#ID_1252541880": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#Certificado2": {
			"text": "L('x962007463_traducir','Certificado de Hipotecas y Gravámenes Actualizado')"
		},
		"fondoplomo": {
			"backgroundColor": "#f7f7f7",
			"font": {
				"fontSize": "12dp"
			}
		},
		"bt_guardar": {
			"color": "#ffffff",
			"font": {
				"fontSize": "18dp"
			}
		},
		"#iconoEntrar2": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "16dp"
			}
		},
		"#iconoUbicacion8": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#iconoNuevo_dano": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "20dp"
			}
		},
		"#ApellidoPaterno2": {
			"hintText": "L('x214679130_traducir','Apellido paterno')"
		},
		"#iconoEntrar11": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "14dp"
			}
		},
		"#Nivel": {
			"text": "L('x267051884_traducir','nivel 2')"
		},
		"#widgetPreguntac": {
			"titulo": "L('x404129330_traducir','¿EL ASEGURADO CONFIRMA ESTOS DATOS?')",
			"si": "L('x1723413441_traducir','SI, Están correctos')",
			"texto": "L('x2083765231_traducir','El asegurado debe confirmar que los datos de esta sección están correctos')",
			"pantalla": "L('x2851883104_traducir','¿ESTA DE ACUERDO?')",
			"no": "L('x55492959_traducir','NO, Hay que modificar algo')",
			"header": "L('x3614728713_traducir','verde')"
		},
		"#iconoEnviar_insp": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "15dp"
			}
		},
		"#Contactos": {
			"text": "L('x3065475174_traducir','Contactos')"
		},
		"#widgetFotochica5": {
			"caja": "L('x2889884971_traducir','45')"
		},
		"#Label9": {
			"title": "L('x4216663563_traducir','Emergencias')"
		},
		"#iconoComuna5": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#widgetPreguntare": {
			"titulo": "L('x404129330_traducir','¿EL ASEGURADO CONFIRMA ESTOS DATOS?')",
			"si": "L('x1723413441_traducir','SI, Están correctos')",
			"texto": "L('x2083765231_traducir','El asegurado debe confirmar que los datos de esta sección están correctos')",
			"pantalla": "L('x2851883104_traducir','¿ESTA DE ACUERDO?')",
			"no": "L('x55492959_traducir','NO, Hay que modificar algo')",
			"header": "L('x1249199825_traducir','morado')"
		},
		"bg_progreso_verde": {
			"backgroundColor": "#33cc7f",
			"font": {
				"fontSize": "12dp"
			}
		},
		"#widgetModalmultiple12": {
			"titulo": "L('x1866523485_traducir','Estruct. cubierta')",
			"cargando": "L('x1740321226_traducir','cargando ..')",
			"hint": "L('x2460890829_traducir','Seleccione e.cubiertas')",
			"subtitulo": "L('x4011106049_traducir','Indique los tipos')"
		},
		"#Ciudad": {
			"text": "L('x2301231272_traducir','Ciudad')"
		},
		"#FechadeCompra": {
			"text": "L('x173701603_traducir','Fecha de Compra')"
		},
		"estilo6": {
			"color": "#4d4d4d",
			"font": {
				"fontFamily": "SFUIText-Medium",
				"fontSize": "16dp"
			}
		},
		"#DETALLES_DE_TAREA": {
			"title": "L('x3218612972_traducir','DETALLES DE TAREA')"
		},
		"fondoazul": {
			"backgroundColor": "#2d9edb",
			"font": {
				"fontSize": "12dp"
			}
		},
		"#iconoUbicacion3": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#widgetModal10": {
			"titulo": "L('x52303785_traducir','UNIDAD')",
			"cargando": "L('x1740321226_traducir','cargando ..')",
			"campo": "L('x2054670809_traducir','Unidad de medida')",
			"seleccione": "L('x4091990063_traducir','unidad')"
		},
		"#Das": {
			"text": "L('x2854409003_traducir','días')"
		},
		"#Anchomt": {
			"text": "L('x4100297395_traducir','Ancho (mt)')"
		},
		"#EscribirNivel": {
			"hintText": "L('x3229948791_traducir','escribir nivel 3')"
		},
		"#ID_1447737581": {
			"valor": "L('x4264937424','evento.valor')"
		},
		"#Nivel4": {
			"text": "L('x3867756121_traducir','nivel 4')"
		},
		"#ID_211976969": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#Domicilio": {
			"text": "L('x426093682_traducir','Domicilio')"
		},
		"estilo12": {
			"color": "#000000",
			"font": {
				"fontSize": "18dp"
			}
		},
		"#ID_1256506288": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#iconoCiudad5": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#widgetModal2": {
			"titulo": "L('x1867465554_traducir','MARCAS')",
			"cargando": "L('x1740321226_traducir','cargando ..')",
			"campo": "L('x3837495059_traducir','Marca del Item')",
			"seleccione": "L('x1021431138_traducir','seleccione marca')"
		},
		"#HoradeInspeccion": {
			"text": "L('x1045762689_traducir','Hora de inspeccion')"
		},
		"#ID_916677064": {
			"imagen": "L('x3940301797_traducir','foto')",
			"calidad": "L('x511942527_traducir','91')",
			"nueva": "L('x1112834923','640')"
		},
		"#widgetModalmultiple2": {
			"titulo": "L('x1975271086_traducir','Estructura Soportante')",
			"cargando": "L('x1740321226_traducir','cargando ..')",
			"hint": "L('x2898603391_traducir','Seleccione estructura')",
			"subtitulo": "L('x4011106049_traducir','Indique los tipos')"
		},
		"#widgetPreguntaco": {
			"titulo": "L('x404129330_traducir','¿EL ASEGURADO CONFIRMA ESTOS DATOS?')",
			"si": "L('x1723413441_traducir','SI, Están correctos')",
			"texto": "L('x2083765231_traducir','El asegurado debe confirmar que los datos de esta sección están correctos')",
			"pantalla": "L('x2851883104_traducir','¿ESTA DE ACUERDO?')",
			"no": "L('x55492959_traducir','NO, Hay que modificar algo')",
			"header": "L('x252540350_traducir','celeste')"
		},
		"#IngreseCorreo": {
			"hintText": "L('x2452733176_traducir','ingrese correo')"
		},
		"#ID_1833888642": {
			"valor": "L('x392683181','monedas[0].nombre')"
		},
		"#widgetMapa": {
			"externo": "L('x4261170317','true')",
			"label_a": "L('x3904355907','a')",
			"_bono": "L('x2764662954_traducir','Bono adicional:')",
			"bt_cancelar": "L('x1030930307_traducir','Cerrar')"
		},
		"#iconoUbicacion11": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#widgetModal": {
			"titulo": "L('x4288296688_traducir','REGION')",
			"campo": "L('x147780672_traducir','Region')",
			"seleccione": "L('x116943338_traducir','Seleccione region')",
			"cargando": "L('x1740321226_traducir','cargando ..')"
		},
		"#FonoMovil": {
			"text": "L('x3999754113_traducir','Fono movil')"
		},
		"#iconoVolver3": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "18dp"
			}
		},
		"#iconoCerrar2": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "18dp"
			}
		},
		"#iconoComuna9": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"rojo": {
			"color": "#ee7f7e",
			"font": {
				"fontWeight": "bold",
				"fontFamily": "SFUIText-Medium",
				"fontSize": "16dp"
			}
		},
		"#ID_1552521694": {
			"imagen3": "L('x1347115794_traducir','elemento')",
			"nueva": "L('x1112834923','640')"
		},
		"#Estimacinde": {
			"text": "L('x2770200964_traducir','Estimación de Meses')"
		},
		"#widgetDpicker": {
			"aceptar": "L('x1518866076_traducir','Aceptar')",
			"cancelar": "L('x2353348866_traducir','Cancelar')"
		},
		"#SINIESTRO": {
			"title": "L('x3952572309_traducir','SINIESTRO')"
		},
		"#EscribirNivel3": {
			"hintText": "L('x702987842_traducir','escribir nivel 5')"
		},
		"#ID_1282712073": {
			"pantalla": "L('x887781275_traducir','detalle')"
		},
		"#ID_745187833": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#ENROLAMIENTO4": {
			"title": "L('x2146494644_traducir','ENROLAMIENTO')"
		},
		"#ID_675248659": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#widgetModalmultiple11": {
			"titulo": "L('x591862035_traducir','Pavimentos')",
			"cargando": "L('x1740321226_traducir','cargando ..')",
			"hint": "L('x2600368035_traducir','Seleccione pavimentos')",
			"subtitulo": "L('x4011106049_traducir','Indique los tipos')"
		},
		"#widgetFotochica2": {
			"caja": "L('x2889884971_traducir','45')"
		},
		"#itemListado3": {
			"_section": "L('x3690817388','manana_entrantes')"
		},
		"#ID_42440756": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#ID_1657448720": {
			"bono": "L('x1979562742','tareas[0].bono')"
		},
		"#ID_1997004832": {
			"valor": "L('x520993688','nivel[0].nombre')"
		},
		"#iconoCiudad9": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#HISTORIAL": {
			"title": "L('x1649875133_traducir','HISTORIAL')"
		},
		"#Guardar2": {
			"title": "L('x2943883035_traducir','Guardar')"
		},
		"#seccionListadoHoyentrantes": {
			"headerTitle": "L('x3166667470','hoy_entrantes')"
		},
		"#RepetirTelfono": {
			"text": "L('x3573009323_traducir','Repetir teléfono')"
		},
		"#iconoComuna11": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#seccionListadoManana": {
			"headerTitle": "L('x4074624282_traducir','manana')"
		},
		"#iconoHistorial": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "20dp"
			}
		},
		"#iconoComuna4": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#AgregueCada": {
			"text": "L('x246910460_traducir','Agregue cada contenido movible dañado')"
		},
		"#TOMAR_TAREA": {
			"title": "L('x3096182311_traducir','TOMAR TAREA')"
		},
		"#iconoCiudad": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#itemListado4": {
			"_section": "L('x2345059420','fecha_entrantes')"
		},
		"#widgetHeader": {
			"titulo": "L('x1574293037_traducir','Editar Disponibilidad')"
		},
		"#ItemsdeDao": {
			"text": "L('x649484684_traducir','Items de daño')"
		},
		"#widgetModalmultiple": {
			"titulo": "L('x1313568614_traducir','Destino')",
			"hint": "L('x2017856269_traducir','Seleccione destino')",
			"subtitulo": "L('x1857779775_traducir','Indique los destinos')",
			"cargando": "L('x1740321226_traducir','cargando ..')"
		},
		"#DATOS_BASICOS": {
			"title": "L('x3046757478_traducir','DATOS BASICOS')"
		},
		"#iconoVolver5": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "18dp"
			}
		},
		"estilo10": {
			"color": "#ffffff",
			"font": {
				"fontWeight": "bold",
				"fontSize": "16dp"
			}
		},
		"#seccionListadoAyer": {
			"headerTitle": "L('x1602196311_traducir','ayer')"
		},
		"#ID_783513222": {
			"texto": "L('x2695983055_traducir','PRESIONE PARA ACEPTAR')",
			"estilo": "L('x2379090022_traducir','fondoverde')",
			"estado": "L('x2212294583','1')",
			"vertexto": "L('x2310355463','true')",
			"verprogreso": "L('x2001657581_traducir','false')"
		},
		"#ID_1953351488": {
			"vertexto": "L('x2310355463','true')",
			"verprogreso": "L('x2001657581_traducir','false')"
		},
		"mini": {
			"color": "#333333",
			"font": {
				"fontFamily": "SFUIText-Medium",
				"fontSize": "10dp"
			}
		},
		"#ID_1213709362": {
			"monito": "L('x2001657581_traducir','false')"
		},
		"#widgetPelota5": {
			"letra": "L('x1342839628','V')"
		},
		"fondoblanco": {
			"backgroundColor": "#ffffff",
			"font": {
				"fontSize": "12dp"
			}
		},
		"estilo8_3": {
			"color": "#ee7f7e",
			"font": {
				"fontFamily": "SFUIText-Medium",
				"fontSize": "9dp"
			}
		},
		"#Escriba": {
			"hintText": "L('x859125505_traducir','Escriba')"
		},
		"#widgetSininternet": {
			"titulo": "L('x2828751865_traducir','¡ESTAS SIN CONEXION!')",
			"mensaje": "L('x1855928898_traducir','No puedes ver las tareas de emergencias de hoy')"
		},
		"#ID_593731157": {
			"imagen": "L('x3940301797_traducir','foto')",
			"calidad": "L('x511942527_traducir','91')",
			"nueva": "L('x1112834923','640')"
		},
		"#EscribirDireccion": {
			"hintText": "L('x3041923657_traducir','Escribir direccion')"
		},
		"#campo": {
			"hintText": "L('x4108050209','0')"
		},
		"#EDITAR": {
			"title": "L('x2726960050_traducir','EDITAR')"
		},
		"#ID_582453330": {
			"pantalla": "L('x3181120844_traducir','hayalguien')"
		},
		"#ID_1114620463": {
			"mini": "L('x1398154619_traducir','foto1')"
		},
		"#ID_883609983": {
			"valor": "L('x4264937424','evento.valor')"
		},
		"#NO6": {
			"text": "L('x3376426101_traducir','NO')"
		},
		"#MANTENERPARA": {
			"text": "L('x2905032041_traducir','MANTENER PARA INICIAR')"
		},
		"#widgetModalmultiple8": {
			"titulo": "L('x1975271086_traducir','Estructura Soportante')",
			"cargando": "L('x1740321226_traducir','cargando ..')",
			"hint": "L('x2898603391_traducir','Seleccione estructura')",
			"subtitulo": "L('x4011106049_traducir','Indique los tipos')"
		},
		"#iconoCamara2": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "36dp"
			}
		},
		"estilo14n": {
			"color": "#666666",
			"font": {
				"fontWeight": "bold",
				"fontSize": "15dp"
			}
		},
		"titulo_danos": {
			"color": "#b9aaf3",
			"font": {
				"fontWeight": "bold",
				"fontFamily": "SFUIText-Medium",
				"fontSize": "16dp"
			}
		},
		"#ID_1908193253": {
			"valor": "L('x3346168137_traducir','pavimentoElegidos')"
		},
		"#RecuperoMaterial": {
			"text": "L('x3354371722_traducir','Recupero Material')"
		},
		"#SeleccioneDas": {
			"text": "L('x2509905523_traducir','Seleccione días disponibles')"
		},
		"#ESTRUCTURASOPORTANTE": {
			"text": "L('x2268209570_traducir','ESTRUCTURA SOPORTANTE')"
		},
		"#icono2": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "15dp"
			}
		},
		"#iconoEntrar9": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "14dp"
			}
		},
		"#label3": {
			"text": "L('x1046761583_traducir','12-03-1989')"
		},
		"#ID_1337115680": {
			"texto": "L('x2695983055_traducir','PRESIONE PARA ACEPTAR')",
			"estilo": "L('x2379090022_traducir','fondoverde')",
			"estado": "L('x2212294583','1')",
			"vertexto": "L('x2310355463','true')",
			"verprogreso": "L('x2001657581_traducir','false')"
		},
		"#NO": {
			"text": "L('x3376426101_traducir','NO')"
		},
		"#widgetModalmultiple5": {
			"titulo": "L('x591862035_traducir','Pavimentos')",
			"cargando": "L('x1740321226_traducir','cargando ..')",
			"hint": "L('x2600368035_traducir','Seleccione pavimentos')",
			"subtitulo": "L('x4011106049_traducir','Indique los tipos')"
		},
		"#DDMMYYYY": {
			"text": "L('x2520471236_traducir','DD-MM-YYYY')"
		},
		"#ID_373907674": {
			"valor": "L('x1533041928_traducir','esCubiertaElegidos')"
		},
		"#ID_406211360": {
			"valor": "L('x4091584045_traducir','cubiertaElegidos')"
		},
		"#EDITAR2": {
			"title": "L('x2726960050_traducir','EDITAR')"
		},
		"#PORTADA": {
			"title": "L('x2728236766_traducir','PORTADA')"
		},
		"#ID_688541926": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#Datos": {
			"text": "L('x2421821072_traducir','Datos')"
		},
		"#widgetBotonlargo2": {
			"titulo": "L('x1524107289_traducir','CONTINUAR')"
		},
		"#Definirde2": {
			"text": "L('x1350052795_traducir','Definir % de daños')"
		},
		"#ID_61744736": {
			"mini": "L('x3395253441_traducir','foto2')"
		},
		"#ID_1728499625": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"bg_progreso_naranjo": {
			"backgroundColor": "#f2a566",
			"font": {
				"fontSize": "12dp"
			}
		},
		"#Ao": {
			"hintText": "L('x300188755_traducir','año')"
		},
		"fondonaranjo": {
			"backgroundColor": "#fcbd83",
			"font": {
				"fontSize": "12dp"
			}
		},
		"#ID_118298449": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#CantidadItems": {
			"text": "L('x503985650_traducir','Cantidad Items')"
		},
		"#NUEVO_RECINTO": {
			"title": "L('x1471097171_traducir','NUEVO RECINTO')"
		},
		"#ID_1681305290": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#Usuario": {
			"text": "L('x3990391233_traducir','Usuario')"
		},
		"#icono9": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "15dp"
			}
		},
		"#ApellidoPaterno": {
			"text": "L('x3279325126_traducir','Apellido Paterno')"
		},
		"#AgregaCadaNivel": {
			"text": "L('x1411564115_traducir','Agrega cada nivel')"
		},
		"#iconoVolver7": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "18dp"
			}
		},
		"#imagen2": {
			"images": ["/images/i21578740E736331F7BDC9D452BD712C9.png", "/images/iEB558FE382408A66B62F79BCD08E87DE.png", "/images/iE4617B7080958F45BF2C192CC042382D.png", "/images/i9290A0A895DCA701657AA3A2701BC5B1.png", "/images/iC844F1FD37D49F099C8E19CDD3168A41.png", "/images/iD7DACAF52A6FE787F42E337F896ED7E5.png", "/images/i77B96376A21E9E530BF99DEF0E191F24.png", "/images/i1A8982F638B1931C2A37ED5E826C17C0.png", "/images/i88E908455A225B9390E3678E9C19FBA2.png", "/images/iE9355204576B1FD1BB3F1F0E435BFF6D.png", "/images/i0E4650BFDDB114F81A51C600521AC2D7.png", "/images/i867483CAEDC8D77255A7F25A15625CA5.png", "/images/i2175C7E985066E9DBB5FD1E204BDED5D.png", "/images/i19F89DC20905F566DA7CCDBE900B1828.png", "/images/i6755383116D1A5AD862D47B6D5554641.png", "/images/i1391131C784CF99686D19D65AC20986E.png", "/images/i40A27D206CA23815C331AB71422629F2.png", "/images/i38E6B9D378262B4EF35387DC47BBF31D.png", "/images/i988F7B1016AF1A4927CD7ED9AE8AB990.png", "/images/i405EC5CB58F536363712327BE6376BB1.png", "/images/i46769D88018418464C9F4B24D384CB8B.png", "/images/i3F6BAF33AC1E4011205917185EA98CAD.png", "/images/i42D15D48B2ED837F7903FE534A06494B.png", "/images/iBC7C1AA388608962A1F7B8C61BD6DA82.png", "/images/iF09D8144E898AB6F35D2FF363EE98F9E.png", "/images/i077D23D9EA5866B0EF5E25614919665F.png", "/images/iA83DD447D387E7698AB46331B8A8D019.png", "/images/i21932839113ADBF836C5D8C8B4B11AEB.png", "/images/i9BDA9EE2558DFE3F2125992BECA4278D.png", "/images/i9AC6E8C97F63FEB0A46CCD655AC44431.png", "/images/iE28ABABF9CABE7255711922BBF55B983.png", "/images/i0A41ED8B1E443429769FFB49137003C2.png", "/images/i388C5A5B6FEC1AAA9E0FED2BE6842A35.png", "/images/iBB18FBADFC2D527AD2709E8652AAFD5A.png", "/images/i10063715BD30D221415EF53335926168.png", "/images/i359C4AC08A6FB6D5912E5ECA8ABBB00A.png", "/images/i0E59AE058F689EF3F4FAD9FDDF470974.png", "/images/i54701C93E11CA7BAF12DF89AABF2AE04.png", "/images/i9AE08E1C51AF160F89D84F868261FCFD.png", "/images/i585BBC1586441528C908AD41EB8EBDBD.png", "/images/i10BF85AEDE9F2E6087E8EECF917665A8.png", "/images/iE50DF32C64581FCE5856C9454B2D58E2.png", "/images/iBFDCFF2749FF3ACE5A75BF05C2C1E25C.png", "/images/i89096AB8DBE818E384384C292C89811F.png", "/images/i0BEBD7212FCEADD8E81C2812FD68C36D.png", "/images/i1B954A4FDCFE0860F6D3EBC4D01D2F19.png", "/images/i304ED678B67AD49378DB6228C609BE45.png", "/images/iBEB7E7D434C1667889663859EAE75D92.png", "/images/iF714D356682197276705B90C40BA8A03.png", "/images/iA1B6778946A3A0358E419143E8081D2B.png", "/images/i00760138B01D3550976723B1B225C943.png", "/images/iBAAC1AF775CEE1B716B575455DB68FDC.png", "/images/i3CD6AA1281B7B4ED5F37ED76FCC2A808.png", "/images/i20769EDB2A387AD7E63BFEE1ADB6E01A.png", "/images/iDAED2AF86C310E481C7409143758ECB6.png", "/images/i9B1B0BD5A88BDAB459D66F68B80309F5.png", "/images/i0DE060270629150E8A87173EDB8B79F7.png", "/images/i380DF6EF85EB2185DE1A501FEE66371A.png", "/images/iA152F49A26EC92286520C265AABD8CFC.png", "/images/i154CF2D0DF49584435B369E991104CD0.png", "/images/iB6A8B660E26087802754C713C196B739.png", "/images/iD0A8DD7F8257D87CADD4557D2DDE27A0.png", "/images/iF0D35F95D8CF565D84F4B2EABC07C0C3.png", "/images/iAC8C1B1E912288D6EF52DC4C71B79944.png", "/images/i396F93C0F37490C405609068F53808DE.png", "/images/i28D1EA7CF68179295028D9F1DC787292.png", "/images/iF3F5D57B22CF688DF19FE14CCD909402.png", "/images/i399EE8FDF222FDA603DF0C82E0BCCFAC.png", "/images/i09C63C089B2E569672618ED5FC69504C.png", "/images/i6EA1D3660FE93F229A4DD0E034A728EB.png", "/images/iDFEBE4EC019BCE94F6EC5853A3BFABF5.png", "/images/iAC27C6E0D5FAF36E89B0E75BD07AEB87.png", "/images/i273522DAAED644BC07219E8653954EE3.png", "/images/i86B7E95443CEBED206372766EAF4F59C.png", "/images/i7E06C884017083155A6857A40DCF4DE3.png", "/images/i691DBFC540A724998A2353C7D6DC199F.png", "/images/iD182D4C677A7FEF7B7F9D2021F901A9C.png", "/images/i35A05BBCDD161E2A6C0C478F703901C0.png", "/images/iCF6F841564C2B00839FA0338556EE25E.png", "/images/iE34B2EECFDC74FC20A5DEA25E204DB7A.png", "/images/i15B71869941D9D056D2771715265227F.png", "/images/i8A0B707451CCBE122A5B28F640716AF1.png", "/images/i88CB92AA112DA9B4D3913374D31E5489.png", "/images/i7FDF4568F230AA5A4D3B701708D1E8D6.png", "/images/i69B8A04A79F91820581E4D3708B7D003.png", "/images/i55288AB1A85C7038BCA8AD843ADF586F.png", "/images/i57C1B3043D0424531659720F9C32B1C2.png", "/images/iDAA2A0A70B3960D02C55DDF038216353.png", "/images/iECBF5EDE5417D57D71BD067290909C22.png", "/images/i10CCB6F0BFED76841F1607FA34C062B1.png", "/images/iF8B6DC3340FEC020058F7C743ADA0098.png", "/images/i7F73828276786D80F13715BB5BFB4CCC.png", "/images/i68421A3D08C3B75227181E0EC9EBCE42.png", "/images/i18ACDDC89C81835BFB74D3A0E71D8C10.png", "/images/i5A8E6E8D34ABB47533165D506B7E1462.png", "/images/i9AD38C8643DBCC036D0D226508F2B971.png", "/images/iA7B0C6D62FD940727B9D5DBBD4165324.png", "/images/i51313033EE92F99494C0CEF93F77F8D9.png", "/images/i26C8BDFC1DEA0C67358C293A353C327B.png", "/images/iF086F6FE7A1868188FD09EE6FA03485B.png", "/images/i9C4EAA87C9F69FE64BE240DAE158CF0E.png", "/images/i47EFAC4DEC72BFED1EAD8D5621ECF913.png", "/images/i96D645D648D27F12B2E4F54E9503E8DF.png", "/images/i6394513CA228CCB57ED602297C9422BC.png", "/images/i6CD686DC5A11FB94CBB2A09FC90DB6EF.png", "/images/iEBD4EE843BEEB6E225C25387B7DCF30E.png", "/images/iB0DCEF19023ADDE9B38C97AF31540D4D.png", "/images/i5907CCF7C26ECC19EAD231C90A4FAB9D.png", "/images/i8143DE4F0A77BC1F4111FF6A02EED95E.png", "/images/iDD2C93752E1169F8D71892A477C85A11.png", "/images/i661E5F5296ABB7AB4B0E5D236E6C7630.png", "/images/iCCB062F45E0D9D917C617FDB2EA1E357.png", "/images/i7DF846739C1FD71098DE3902990DA8A9.png", "/images/iAD45960BC2841EC6E2D86283A2F33E96.png", "/images/i6AF96FB60F20F611E16F44E4A4C366B4.png", "/images/iBBA0A82C21EA239F79E171AC51EA7B95.png", "/images/i2F569FBCA8D42EB735DED2831286DC66.png", "/images/iE02AFE3817F8B969AEAACF7456E25AB5.png", "/images/iAE28EA1830BF41F55AD84813E8F1D056.png", "/images/i870DC5E31CA825DAEE1AD00C6FB1D68A.png"]
		},
		"#widgetModalmultiple6": {
			"titulo": "L('x1866523485_traducir','Estruct. cubierta')",
			"cargando": "L('x1740321226_traducir','cargando ..')",
			"hint": "L('x2460890829_traducir','Seleccione e.cubiertas')",
			"subtitulo": "L('x4011106049_traducir','Indique los tipos')"
		},
		"#AceptaDesistir": {
			"text": "L('x4144438243_traducir','Acepta desistir la realización de esta inspección')"
		},
		"#HOY": {
			"title": "L('x3835609072_traducir','HOY')"
		},
		"#IngreseContrasea": {
			"hintText": "L('x2113698382_traducir','ingrese contraseña')"
		},
		"#ID_1072199175": {
			"valor": "L('x4264937424','evento.valor')"
		},
		"#Cercanos": {
			"title": "L('x1769798096_traducir','Cercanos')"
		},
		"#ID_445622784": {
			"vertexto": "L('x2310355463','true')",
			"verprogreso": "L('x2001657581_traducir','false')"
		},
		"#widgetPreguntadoc": {
			"titulo": "L('x404129330_traducir','¿EL ASEGURADO CONFIRMA ESTOS DATOS?')",
			"si": "L('x1723413441_traducir','SI, Están correctos')",
			"texto": "L('x2083765231_traducir','El asegurado debe confirmar que los datos de esta sección están correctos')",
			"pantalla": "L('x2851883104_traducir','¿ESTA DE ACUERDO?')",
			"no": "L('x55492959_traducir','NO, Hay que modificar algo')"
		},
		"#iconoCiudad11": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#INSTALACIONES": {
			"text": "L('x4060777286_traducir','INSTALACIONES')"
		},
		"#iconoInfo": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "18dp"
			}
		},
		"#ID_627481044": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"progreso": {
			"color": "#ffffff",
			"font": {
				"fontWeight": "bold",
				"fontFamily": "SFUIText-Medium",
				"fontSize": "14dp"
			}
		},
		"#iconoUbicacion6": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"bg_progreso_morado": {
			"backgroundColor": "#9e8ff2",
			"font": {
				"fontSize": "12dp"
			}
		},
		"fondoplomo2": {
			"backgroundColor": "#e6e6e6",
			"font": {
				"fontSize": "12dp"
			}
		},
		"#EscribirNivel2": {
			"hintText": "L('x1591840468_traducir','escribir nivel 4')"
		},
		"#seccionListadoMananaentrantes": {
			"headerTitle": "L('x3690817388','manana_entrantes')"
		},
		"#widgetPickerhoras": {
			"mins": "L('x4261170317','true')",
			"a": "L('x3904355907','a')"
		},
		"#iconoVolver2": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "20dp"
			}
		},
		"#EstsDispuesto": {
			"text": "L('x1110802308_traducir','¿Estás dispuesto a viajar fuera de tu ciudad?')"
		},
		"estilo2": {
			"color": "#a0a1a3",
			"font": {
				"fontFamily": "SFUIText-Light",
				"fontSize": "12dp"
			}
		},
		"#ID_717638193": {
			"on": "L('x3260667647','registro.d1')"
		},
		"#DOCUMENTOS": {
			"title": "L('x354890761_traducir','DOCUMENTOS')"
		},
		"#NUEVO_DAÑO": {
			"title": "L('x2435611700_traducir','NUEVO DAÑO')"
		},
		"#ID_327747054": {
			"texto": "L('x438354520_traducir','Tip: Nunca dejes una tarea sin inspeccionar. El no hacerlo te afecta directamente')"
		},
		"#widgetHeader6": {
			"titulo": "L('x1291961897_traducir','PARTE 5: Contactos')",
			"avance": "L('x4674461_traducir','5/6')"
		},
		"#Construccin": {
			"text": "L('x1380747035_traducir','Construcción Anexa')"
		},
		"estilo2_1": {
			"color": "#a0a1a3",
			"font": {
				"fontFamily": "SFUIText-Light",
				"fontSize": "15dp"
			}
		},
		"desistirtexto": {
			"color": "#ffffff",
			"font": {
				"fontWeight": "bold",
				"fontSize": "14dp"
			}
		},
		"#Direccion": {
			"text": "L('x1903508534_traducir','Direccion')"
		},
		"#LaDireccion": {
			"text": "L('x1242435453_traducir','La direccion es correcta?')"
		},
		"#NUEVO_CONTENIDO": {
			"title": "L('x1486275980_traducir','NUEVO CONTENIDO')"
		},
		"#iconoPerfil": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "16dp"
			}
		},
		"fondocafe": {
			"backgroundColor": "#a5876d",
			"font": {
				"fontSize": "12dp"
			}
		},
		"#widgetHeader5": {
			"titulo": "L('x3752633607_traducir','PARTE 4: Disponibilidad de trabajo')",
			"avance": "L('x25508266_traducir','4/6')"
		},
		"#TERMINACIONES": {
			"text": "L('x2474326379_traducir','TERMINACIONES')"
		},
		"#Largomt": {
			"text": "L('x2558236558_traducir','Largo (mt)')"
		},
		"#ID_896481991": {
			"valor": "L('x1340218847_traducir','muroElegidos')"
		},
		"#ID_611022613": {
			"pantalla": "L('x1828862638_traducir','basicos')"
		},
		"#ID_861542679": {
			"nivel1": "L('x3264699902','pais.label_nivel1')"
		},
		"#ID_493770240": {
			"texto": "L('x1422333761_traducir','Tienes una capacidad maxima de tareas por dia pero aun puedes tomar en otros dias')"
		},
		"#ID_1252135805": {
			"ruta": "L('x2310355463','true')",
			"externo": "L('x2310355463','true')",
			"ciudad": "L('x1526661684','info.ciudad_pais')",
			"longitud": "L('x2465019777','tareas[0].lon')",
			"comuna": "L('x4190055040','info.comuna')",
			"latitud": "L('x4044153717','tareas[0].lat')",
			"direccion": "L('x1199566094','info.direccion')",
			"distancia": "L('x1784316920','info.distancia')",
			"tipo": "L('x2512664346_traducir','ubicacion')"
		},
		"#widgetBotonlargo7": {
			"titulo": "L('x4058536315_traducir','ENVIAR FORMULARIO')"
		},
		"#widgetHeader7": {
			"titulo": "L('x4235921061_traducir','PARTE 6: Experiencia de trabajo')",
			"avance": "L('x33680836_traducir','6/6')"
		},
		"#iconoCiudad4": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#ID_593271354": {
			"mini": "L('x1398154619_traducir','foto1')"
		},
		"#ID_1642642723": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#widgetFotochica3": {
			"caja": "L('x2889884971_traducir','45')"
		},
		"estilo6_1": {
			"color": "#848484",
			"font": {
				"fontFamily": "SFUIText-Bold",
				"fontSize": "14dp"
			}
		},
		"#CancelandoInspeccin": {
			"text": "L('x2067448595_traducir','Cancelando Inspección, espere ..')"
		},
		"#ID_1450585256": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#widgetPregunta": {
			"titulo": "L('x1951816285_traducir','¿PUEDE CONTINUAR CON LA INSPECCION?')",
			"si": "L('x369557195_traducir','SI puedo continuar')",
			"texto": "L('x2522333127_traducir','Si está el asegurado en el domicilio presione SI para continuar')",
			"no": "L('x3892244486_traducir','NO se pudo realizar la inspección')"
		},
		"#iconoComuna": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#ENVIARTODOS": {
			"text": "L('x993337110_traducir','ENVIAR TODOS')"
		},
		"#ElSeguroEst": {
			"text": "L('x4137765611_traducir','¿El seguro está asociado a un Crédito Hipotecario?')"
		},
		"#DATOS_DEL_ASEGURADO": {
			"title": "L('x582013978_traducir','DATOS DEL ASEGURADO')"
		},
		"estilo5": {
			"color": "#808080",
			"font": {
				"fontSize": "16dp"
			}
		},
		"#widgetHeader2": {
			"titulo": "L('x118417065_traducir','PARTE 2: Datos personales')",
			"avance": "L('x84428056_traducir','2/6')"
		},
		"#iconoCorreo": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "12dp"
			}
		},
		"#iconoEntrar8": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "14dp"
			}
		},
		"#FonoFijo": {
			"text": "L('x3136609939_traducir','Fono fijo')"
		},
		"desistir": {
			"backgroundColor": "#adadad",
			"font": {
				"fontSize": "12dp"
			}
		},
		"#ID_1395628503": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#ID_1877908171": {
			"ruta": "L('x2310355463','true')",
			"ciudad": "L('x1526661684','info.ciudad_pais')",
			"destino_longitud": "L('x2465019777','tareas[0].lon')",
			"origen_latitud": "L('x2063034443','inspector.lat_dir')",
			"destino_latitud": "L('x4044153717','tareas[0].lat')",
			"origen_longitud": "L('x1789756952','inspector.lon_dir')",
			"comuna": "L('x4190055040','info.comuna')",
			"direccion": "L('x1199566094','info.direccion')",
			"distancia": "L('x1784316920','info.distancia')",
			"tipo": "L('x2403206383_traducir','origen')"
		},
		"#SeleccioneAo": {
			"text": "L('x483197933_traducir','Seleccione año')"
		},
		"estilo3": {
			"color": "#838383",
			"font": {
				"fontFamily": "SFUIText-Medium",
				"fontSize": "12dp"
			}
		},
		"#NO4": {
			"text": "L('x3376426101_traducir','NO')"
		},
		"#ID_1215658811": {
			"imagen": "L('x3940301797_traducir','foto')",
			"calidad": "L('x511942527_traducir','91')",
			"nueva": "L('x1112834923','640')"
		},
		"#Telefono": {
			"text": "L('x3253144191_traducir','telefono')"
		},
		"#ID_830036912": {
			"valor": "L('x19238826','t_partida[0].nombre')"
		},
		"#itemListado2": {
			"_section": "L('x3166667470','hoy_entrantes')"
		},
		"#widgetModalmultiple7": {
			"titulo": "L('x2266302645_traducir','Cubierta')",
			"cargando": "L('x1740321226_traducir','cargando ..')",
			"hint": "L('x2134385782_traducir','Seleccione cubiertas')",
			"subtitulo": "L('x4011106049_traducir','Indique los tipos')"
		},
		"#ID_970705546": {
			"texto": "L('x3287250645_traducir','CERRAR')",
			"estilo": "L('x977382917_traducir','fondorojo')",
			"estado": "L('x450215437','2')",
			"vertexto": "L('x2310355463','true')",
			"verprogreso": "L('x2001657581_traducir','false')"
		},
		"#Comuna": {
			"text": "L('x2043784063_traducir','Comuna')"
		},
		"#ID_324967777": {
			"seleccione": "L('x3621251639_traducir','unidad')"
		},
		"#widgetBotonlargo6": {
			"titulo": "L('x1524107289_traducir','CONTINUAR')"
		},
		"#widgetBotonlargo": {
			"titulo": "L('x1321529571_traducir','ENTRAR')"
		},
		"#label5": {
			"text": "L('x2547889144','-')"
		},
		"#seccionListadoRef": {
			"headerTitle": "L('x3424088795_traducir','ref1')"
		},
		"#ID_1059959544": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#ExistenOtros": {
			"text": "L('x607758518_traducir','¿Existen otros seguros por la misma materia siniestrada?')"
		},
		"#DAÑOS_EN_CONTENIDO": {
			"title": "L('x3976038131_traducir','DAÑOS EN CONTENIDO')"
		},
		"#ID_541645047": {
			"valor": "L('x4266452563','marcas[0].nombre')"
		},
		"#campo2": {
			"hintText": "L('x4108050209','0')"
		},
		"#widgetFotochica": {
			"caja": "L('x2889884971_traducir','45')"
		},
		"#RUT": {
			"text": "L('x1001594672_traducir','RUT')",
			"hintText": "L('x1001594672_traducir','RUT')"
		},
		"#iconoComuna6": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#ID_1623440774": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#widgetBotonlargo4": {
			"titulo": "L('x1524107289_traducir','CONTINUAR')"
		},
		"#ID_613908889": {
			"seleccione": "L('x2404293600_traducir','seleccione tipo')"
		},
		"#iconoCritico": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "14dp"
			}
		},
		"#Nivel6": {
			"text": "L('x2442032847_traducir','nivel 5')"
		},
		"#Nombre": {
			"text": "L('x982552870_traducir','nombre')"
		},
		"#ID_564931174": {
			"on": "L('x1547518812','registro.d6')"
		},
		"#iconoGirar_camara": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "24dp"
			}
		}
	}
};
exports.fontello = {
	"adjust": {
		"CODES": {
			"auto": "\uE80b",
			"entrar": "\uE81b",
			"emergencias": "\uE819",
			"registrarse": "\uE827",
			"continuar": "\uE812",
			"info": "\uE821",
			"cerrar": "\uE80e",
			"salir_insp": "\uE828",
			"hoy": "\uE820",
			"check": "\uE80f",
			"agregar_item": "\uE80a",
			"9": "\uE808",
			"girar_camara": "\uE81d",
			"llamar": "\uE822",
			"volver": "\uE82a",
			"cruz": "\uE814",
			"1": "\uE800",
			"hora": "\uE81f",
			"ciudad": "\uE810",
			"3": "\uE802",
			"flecha": "\uE82b",
			"7": "\uE806",
			"4": "\uE803",
			"perfil": "\uE817",
			"cancelar_tarea": "\uE80d",
			"6": "\uE805",
			"mistareas": "\uE823",
			"historial": "\uE81e",
			"8": "\uE807",
			"entradas": "\uE81a",
			"caso": "\uE815",
			"ubicacion": "\uE829",
			"2": "\uE801",
			"refresh": "\uE826",
			"enviar_insp": "\uE81c",
			"telefono": "\uE818",
			"nuevo_dano": "\uE824",
			"comuna": "\uE811",
			"critico": "\uE813",
			"5": "\uE804",
			"camara": "\uE80c",
			"10": "\uE809",
			"correo": "\uE816"
		},
		"POSTSCRIPT": "beta1",
		"TTF": "/Applications/CreadorOPEN/CreadorOPEN.app/Contents/MacOS/webapps/ROOT/WEB-INF/lucee/temp/_font/fontello-85cc3601/font/beta1.ttf"
	}
};