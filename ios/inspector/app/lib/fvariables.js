"use strict";
/* 
usage:
var test = pattern.init('Hello my friends of the world').pattern('{salute} my {type} of the {where}');
test = [
	{
		salute: 'Hello',
		type: 'friends',
		where: 'world'
	}
];
*/
var findVariables = function(text,symbol,symbol_closing) {
	var vars = '', symbol_t = '', symbol_l = '';
	var symbol_tb = '', symbol_lb = '', partirEn = 0, tmp_name = '';
	var allowed_symbols = 'a-zA-Z0-9\\_\\-\\+\\/\\ \\*\\{\\}\\.\\,\\[\\]\\(\\)\\\'\%\?\"\<\>\\\\';
	for (var qss in symbol) {
		symbol_l  = symbol[qss];
		symbol_t += '[\\' + symbol_l + ']';
		allowed_symbols = allowed_symbols.split('\\'+symbol_l).join('');
	}
	if (symbol_closing!='') {
		for (var qss in symbol_closing) {
			symbol_lb  = symbol_closing[qss];
			symbol_tb += '[\\' + symbol_lb + ']';
			allowed_symbols = allowed_symbols.split('\\'+symbol_lb).join('');
		}
	}
	if (symbol_tb=='') symbol_tb=symbol_t;
	var nadamas = false;
	while(!nadamas) {
		var to_m = symbol_t+'['+allowed_symbols+']*'+symbol_tb;
		var test = new RegExp(to_m,'gim');
		var utiles = text.match(test);
		var resp = [];
		for (var i in utiles) {
			resp.push(utiles[i].split(symbol).join('').split(symbol_closing).join(''));
		}
		nadamas = true;
	}
	return resp;
};

module.exports = findVariables;