Alloy.Events = _.clone(Backbone.Events);
Alloy.Collections.marcas = Alloy.createCollection('marcas');
Alloy.Collections.entidad_financiera = Alloy.createCollection('entidad_financiera');
Alloy.Collections.emergencia_ubicacion = Alloy.createCollection('emergencia_ubicacion');
Alloy.Collections.estructura_cubierta = Alloy.createCollection('estructura_cubierta');
Alloy.Collections.pavimento = Alloy.createCollection('pavimento');
Alloy.Collections.compania = Alloy.createCollection('compania');
Alloy.Collections.insp_itemdanos = Alloy.createCollection('insp_itemdanos');
Alloy.Collections.numero_unico = Alloy.createCollection('numero_unico');
Alloy.Collections.insp_firma = Alloy.createCollection('insp_firma');
Alloy.Collections.muros_tabiques = Alloy.createCollection('muros_tabiques');
Alloy.Collections.insp_niveles = Alloy.createCollection('insp_niveles');
Alloy.Collections.insp_contenido = Alloy.createCollection('insp_contenido');
Alloy.Collections.inspectores = Alloy.createCollection('inspectores');
Alloy.Collections.emergencia_perfil = Alloy.createCollection('emergencia_perfil');
Alloy.Collections.pais = Alloy.createCollection('pais');
Alloy.Collections.tipo_accion = Alloy.createCollection('tipo_accion');
Alloy.Collections.estructura_soportante = Alloy.createCollection('estructura_soportante');
Alloy.Collections.entrepisos = Alloy.createCollection('entrepisos');
Alloy.Collections.insp_recintos = Alloy.createCollection('insp_recintos');
Alloy.Collections.insp_datosbasicos = Alloy.createCollection('insp_datosbasicos');
Alloy.Collections.tipo_partida = Alloy.createCollection('tipo_partida');
Alloy.Collections.unidad_medida = Alloy.createCollection('unidad_medida');
Alloy.Collections.tipo_dano = Alloy.createCollection('tipo_dano');
Alloy.Collections.emergencia = Alloy.createCollection('emergencia');
Alloy.Collections.asegurado = Alloy.createCollection('asegurado');
Alloy.Collections.inspecciones = Alloy.createCollection('inspecciones');
Alloy.Collections.monedas = Alloy.createCollection('monedas');
Alloy.Collections.insp_documentos = Alloy.createCollection('insp_documentos');
Alloy.Collections.insp_siniestro = Alloy.createCollection('insp_siniestro');
Alloy.Collections.cubierta = Alloy.createCollection('cubierta');
Alloy.Collections.historial_tareas = Alloy.createCollection('historial_tareas');
Alloy.Collections.tipo_siniestro = Alloy.createCollection('tipo_siniestro');
Alloy.Collections.nivel1 = Alloy.createCollection('nivel1');
Alloy.Collections.insp_fotosrequeridas = Alloy.createCollection('insp_fotosrequeridas');
Alloy.Collections.tareas = Alloy.createCollection('tareas');
Alloy.Collections.experiencia_oficio = Alloy.createCollection('experiencia_oficio');
Alloy.Collections.insp_caracteristicas = Alloy.createCollection('insp_caracteristicas');
Alloy.Collections.destino = Alloy.createCollection('destino');
Alloy.Collections.bienes = Alloy.createCollection('bienes');
Alloy.Collections.tareas_entrantes = Alloy.createCollection('tareas_entrantes');
Alloy.Collections.cola = Alloy.createCollection('cola');
//contenido de nodo global..
(function() {
	var _my_events = {},
		_out_vars = {},
		_var_scopekey = 'ID_1771290876';
	require('vars')[_var_scopekey] = {};
	require('vars')['url_server'] = L('x2575237648', 'http://api.uadjust.com:9999/api/');
	require('vars')['gps_error'] = L('x4261170317', 'true');

	var ID_1258954520 = function(evento) {
		if (evento.error == false || evento.error == 'false') {
			require('vars')['gps_error'] = L('x734881840_traducir', 'false');
			require('vars')['gps_latitud'] = evento.latitude;
			require('vars')['gps_longitud'] = evento.longitude;
			require('vars')['gps_coords'] = evento.coords;
			var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
			var inspeccion_encurso = ('inspeccion_encurso' in require('vars')) ? require('vars')['inspeccion_encurso'] : '';
			if (inspeccion_encurso == false || inspeccion_encurso == 'false') {
				if (_.isObject(inspector)) {
					var seguir_tarea = ('seguir_tarea' in require('vars')) ? require('vars')['seguir_tarea'] : '';
					var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
					if (_.isNumber(seguir_tarea)) {
						var consultarURL9 = {};

						consultarURL9.success = function(e) {
							var elemento = e,
								valor = e;
							if (Ti.App.deployType != 'production') console.log('respuesta exitosa de guardarUbicacion', {
								"datos": elemento
							});
							elemento = null, valor = null;
						};

						consultarURL9.error = function(e) {
							var elemento = e,
								valor = e;
							if (Ti.App.deployType != 'production') console.log('respuesta fallida de guardarUbicacion', {
								"datos": elemento
							});
							elemento = null, valor = null;
						};
						require('helper').ajaxUnico('consultarURL9', '' + String.format(L('x4011689324', '%1$sguardarUbicacion'), url_server.toString()) + '', 'POST', {
							id_inspector: inspector.id_server,
							id_tarea: seguir_tarea,
							lat: evento.latitude,
							lon: evento.longitude
						}, 15000, consultarURL9);
					}
					var consultarURL10 = {};

					consultarURL10.success = function(e) {
						var elemento = e,
							valor = e;
						if (elemento.error == 0 || elemento.error == '0') {
							var eliminarModelo33_i = Alloy.Collections.tareas;
							var sql = "DELETE FROM " + eliminarModelo33_i.config.adapter.collection_name;
							var db = Ti.Database.open(eliminarModelo33_i.config.adapter.db_name);
							db.execute(sql);
							db.close();
							eliminarModelo33_i.trigger('remove');
							var elemento_mistareas = elemento.mistareas;
							var insertarModelo31_m = Alloy.Collections.tareas;
							var db_insertarModelo31 = Ti.Database.open(insertarModelo31_m.config.adapter.db_name);
							db_insertarModelo31.execute('BEGIN');
							_.each(elemento_mistareas, function(insertarModelo31_fila, pos) {
								db_insertarModelo31.execute('INSERT INTO tareas (fecha_tarea, id_inspeccion, id_asegurado, nivel_2, comentario_can_o_rech, asegurado_tel_fijo, estado_tarea, bono, evento, id_inspector, asegurado_codigo_identificador, lat, nivel_1, asegurado_nombre, pais, direccion, asegurador, fecha_ingreso, fecha_siniestro, nivel_1_texto, distance, nivel_4, perfil, asegurado_id, pais_texto, id_server, categoria, nivel_3, asegurado_correo, num_caso, lon, asegurado_tel_movil, nivel_5, tipo_tarea) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', insertarModelo31_fila.fecha_tarea, insertarModelo31_fila.id_inspeccion, insertarModelo31_fila.id_asegurado, insertarModelo31_fila.nivel_2, insertarModelo31_fila.comentario_can_o_rech, insertarModelo31_fila.asegurado_tel_fijo, insertarModelo31_fila.estado_tarea, insertarModelo31_fila.bono, insertarModelo31_fila.evento, insertarModelo31_fila.id_inspector, insertarModelo31_fila.asegurado_codigo_identificador, insertarModelo31_fila.lat, insertarModelo31_fila.nivel_1, insertarModelo31_fila.asegurado_nombre, insertarModelo31_fila.pais, insertarModelo31_fila.direccion, insertarModelo31_fila.asegurador, insertarModelo31_fila.fecha_ingreso, insertarModelo31_fila.fecha_siniestro, insertarModelo31_fila.nivel_1_, insertarModelo31_fila.distancia, insertarModelo31_fila.nivel_4, 'ubicacion', insertarModelo31_fila.asegurado_id, insertarModelo31_fila.pais, insertarModelo31_fila.id, insertarModelo31_fila.categoria, insertarModelo31_fila.nivel_3, insertarModelo31_fila.asegurado_correo, insertarModelo31_fila.num_caso, insertarModelo31_fila.lon, insertarModelo31_fila.asegurado_tel_movil, insertarModelo31_fila.nivel_5, insertarModelo31_fila.tipo_tarea);
							});
							db_insertarModelo31.execute('COMMIT');
							db_insertarModelo31.close();
							db_insertarModelo31 = null;
							insertarModelo31_m.trigger('change');
							if (Ti.App.deployType != 'production') console.log('psb: refrescando mistareas', {});
							var ID_1089888790_func = function() {

								Alloy.Events.trigger('_refrescar_tareas_mistareas');

								Alloy.Events.trigger('_calcular_ruta');
							};
							var ID_1089888790 = setTimeout(ID_1089888790_func, 1000 * 0.1);
						}
						elemento = null, valor = null;
					};

					consultarURL10.error = function(e) {
						var elemento = e,
							valor = e;
						if (Ti.App.deployType != 'production') console.log('respuesta fallida de obtenerMisTareas', {
							"datos": elemento
						});
						elemento = null, valor = null;
					};
					require('helper').ajaxUnico('consultarURL10', '' + String.format(L('x2334609226', '%1$sobtenerMisTareas'), url_server.toString()) + '', 'POST', {
						id_inspector: inspector.id_server,
						lat: evento.latitude,
						lon: evento.longitude
					}, 15000, consultarURL10);
				}
			}
		} else {
			if (Ti.App.deployType != 'production') console.log('psb ha ocurrido error localizando por equipo a usuario', {
				"evento": evento
			});
			require('vars')['gps_error'] = L('x4261170317', 'true');
		}
	};
	var ID_1258954520_locupd = function() {
		Titanium.Geolocation.purpose = 'Requerido para aplicacion.';
		Titanium.Geolocation.accuracy = Ti.Geolocation.ACCURACY_HUNDRED_METERS;
		Titanium.Geolocation.distanceFilter = 100;
		Titanium.Geolocation.preferredProvider = Ti.Geolocation.PROVIDER_GPS;
	};
	ID_1258954520_moment = require('alloy/moment');
	if (OS_ANDROID) {
		var providerGps = Ti.Geolocation.Android.createLocationProvider({
			name: Ti.Geolocation.PROVIDER_GPS,
			minUpdateDistance: 100.0,
			minUpdateTime: 0
		});
		Ti.Geolocation.Android.addLocationProvider(providerGps);
		Ti.Geolocation.Android.manualMode = true;
		Titanium.Geolocation.addEventListener('location', function(ee) {
			if (ee && ee.coords) {
				ID_1258954520({
					error: false,
					error_data: {},
					latitude: ('latitude' in ee.coords) ? ee.coords.latitude : -1,
					longitude: ('longitude' in ee.coords) ? ee.coords.longitude : -1,
					coords: ee.coords,
					date: ID_1258954520_moment(ee.coords.timestamp).format()
				});
			}
		});
	} else if (OS_IOS) {
		if (Ti.Geolocation.hasLocationPermissions(Ti.Geolocation.AUTHORIZATION_WHEN_IN_USE)) {
			if (Ti.Geolocation.locationServicesEnabled) {
				ID_1258954520_locupd();
				Titanium.Geolocation.addEventListener('location', function(ee) {
					if (ee && ee.coords) {
						ID_1258954520({
							error: false,
							error_data: {},
							latitude: ('latitude' in ee.coords) ? ee.coords.latitude : -1,
							longitude: ('longitude' in ee.coords) ? ee.coords.longitude : -1,
							coords: ee.coords,
							date: ID_1258954520_moment(ee.coords.timestamp).format()
						});
					}
				});
			}
		} else {
			Ti.Geolocation.requestLocationPermissions(Ti.Geolocation.AUTHORIZATION_WHEN_IN_USE, function(u) {
				if (u.success) {
					ID_1258954520_locupd();
					Titanium.Geolocation.addEventListener('location', function(ee) {
						if (ee && ee.coords) {
							ID_1258954520({
								error: false,
								error_data: {},
								latitude: ('latitude' in ee.coords) ? ee.coords.latitude : -1,
								longitude: ('longitude' in ee.coords) ? ee.coords.longitude : -1,
								coords: ee.coords,
								date: ID_1258954520_moment(ee.coords.timestamp).format()
							});
						}
					});
				} else {
					ID_1258954520({
						error: true,
						error_data: u,
						latitude: -1,
						longitude: -1,
						coords: {},
						date: ID_1258954520_moment().format()
					});
				}
			});
		}
	}
	var mensajesPush = Ti.UI.createView({});
	var mensajesPush_cloud = require('ti.cloud');
	var mensajesPush_register = function(_meta) {
		var elemento = _meta.value;
		if (Ti.App.deployType != 'production') console.log('registrado', {
			"datos": elemento
		});
		require('vars')['devicetoken'] = elemento;
		Ti.App.Properties.setString('devicetoken', JSON.stringify(elemento));
		elemento = null;
	};
	var mensajesPush_message = function(_meta) {
		var elemento = _meta.value;
		if (Ti.App.deployType != 'production') console.log('llego elemento', {
			"datos": elemento
		});
		/** 
		 * Se usa para mantener similitud con el código de android 
		 */
		mensaje = elemento.data;
		if (Ti.App.deployType != 'production') console.log('llego mensaje', {
			"datos": mensaje
		});
		var preguntarAlerta16_opts = [L('x1518866076_traducir', 'Aceptar'), L('x2376009830_traducir', ' Cancelar')];
		var preguntarAlerta16 = Ti.UI.createAlertDialog({
			title: L('x1789641236_traducir', 'Notificacion'),
			message: '' + mensaje.alert + '',
			buttonNames: preguntarAlerta16_opts
		});
		preguntarAlerta16.addEventListener('click', function(e) {
			var cosa = preguntarAlerta16_opts[e.index];
			cosa = null;

			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta16.show();
		if (mensaje.confirmacion == true || mensaje.confirmacion == 'true') {
			Ti.App.Properties.setString('confirmarpush', JSON.stringify(true));
			var confirmarpush = JSON.parse(Ti.App.Properties.getString('confirmarpush'));
			if (Ti.App.deployType != 'production') console.log('mirame we', {
				"asd": confirmarpush
			});
			var fechaactual = new Date();
			var fecha_hoy = null;
			if ('formatear_fecha' in require('funciones')) {
				fecha_hoy = require('funciones').formatear_fecha({
					'fecha': fechaactual,
					'formato': L('x591439515_traducir', 'YYYY-MM-DD')
				});
			} else {
				try {
					fecha_hoy = f_formatear_fecha({
						'fecha': fechaactual,
						'formato': L('x591439515_traducir', 'YYYY-MM-DD')
					});
				} catch (ee) {}
			}
			if (Ti.App.deployType != 'production') console.log('pasando por confirmacion true', {
				"asd": fecha_hoy
			});
			var eliminarModelo34_i = Alloy.Collections.consultarpush;
			var sql = "DELETE FROM " + eliminarModelo34_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo34_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo34_i.trigger('remove');
			if (Ti.App.deployType != 'production') console.log('eliminando modelo', {});
			var insertarModelo32_m = Alloy.Collections.consultarpush;
			var insertarModelo32_fila = Alloy.createModel('consultarpush', {
				fecha: fecha_hoy
			});
			insertarModelo32_m.add(insertarModelo32_fila);
			insertarModelo32_fila.save();
			var consultarModelo5_i = Alloy.createCollection('consultarpush');
			var consultarModelo5_i_where = '';
			consultarModelo5_i.fetch();
			var consulta = require('helper').query2array(consultarModelo5_i);
			if (Ti.App.deployType != 'production') console.log('consultando modelo', {
				"confecha": consulta
			});

			Alloy.Events.trigger('_refrescar_tareas_hoy');
		}
		elemento = null;
	};
	var mensajesPush_error = function(_meta) {
		var elemento = _meta.value;
		if (Ti.App.deployType != 'production') console.log('error en aceptarPush', {
			"datos": elemento
		});
		elemento = null;
	};
	if (OS_ANDROID) {
		var mensajesPush_cloudpush = require('ti.cloudpush');
		mensajesPush_cloudpush.retrieveDeviceToken({
			success: function(e) {
				mensajesPush_cloud.PushNotifications.subscribeToken({
					device_token: e.deviceToken,
					channel: 'generico',
					type: 'android'
				}, function(ee) {
					if (ee.success) {
						if (typeof mensajesPush_register != 'undefined') mensajesPush_register({
							value: e.deviceToken
						});
					} else {
						if (typeof mensajesPush_error != 'undefined') mensajesPush_error({
							value: ee.message,
							code: ee.error
						});
					}
				});
			},
			error: function(e) {
				if (typeof mensajesPush_error != 'undefined') mensajesPush_error({
					value: e.error,
					code: 'token'
				});
			},
		});
		mensajesPush_cloudpush.addEventListener('callback', function(evt) {
			if (typeof mensajesPush_message != 'undefined') mensajesPush_message({
				value: evt.payload
			});
		});
		mensajesPush_cloudpush.addEventListener('trayClick', function(evt) {
			if (typeof mensajesPush_message != 'undefined') mensajesPush_message({
				value: evt.payload
			});
		});
	} else if (OS_IOS) {
		if (Ti.Platform.name == 'iPhone OS' && parseInt(Ti.Platform.version.split('.')[0]) >= 8) {
			Ti.App.iOS.addEventListener('usernotificationsettings', function registerForPush() {
				Ti.App.iOS.removeEventListener('usernotificationsettings', registerForPush);
				Ti.Network.registerForPushNotifications({
					success: function(e) {
						mensajesPush_cloud.PushNotifications.subscribeToken({
							device_token: e.deviceToken,
							channel: 'generico',
							type: 'ios'
						}, function(ee) {
							if (ee.success) {
								if (typeof mensajesPush_register != 'undefined') mensajesPush_register({
									value: e.deviceToken
								});
							} else {
								if (typeof mensajesPush_error != 'undefined') mensajesPush_error({
									value: ee.message,
									code: ee.error
								});
							}
						});
					},
					error: function(e) {
						if (typeof mensajesPush_error != 'undefined') mensajesPush_error({
							value: e.error
						});
					},
					callback: function(e) {
						if (typeof mensajesPush_message != 'undefined') mensajesPush_message({
							value: e
						});
					}
				});
			});
			Ti.App.iOS.registerUserNotificationSettings({
				types: [Ti.App.iOS.USER_NOTIFICATION_TYPE_ALERT, Ti.App.iOS.USER_NOTIFICATION_TYPE_SOUND, Ti.App.iOS.USER_NOTIFICATION_TYPE_BADGE]
			});
		} else {
			Ti.Network.registerForPushNotifications({
				types: [Ti.App.iOS.USER_NOTIFICATION_TYPE_ALERT, Ti.App.iOS.USER_NOTIFICATION_TYPE_SOUND, Ti.App.iOS.USER_NOTIFICATION_TYPE_BADGE],
				success: function(e) {
					mensajesPush_cloud.PushNotifications.subscribeToken({
						device_token: e.deviceToken,
						channel: 'generico',
						type: 'ios'
					}, function(ee) {
						if (ee.success) {
							if (typeof mensajesPush_register != 'undefined') mensajesPush_register({
								value: e.deviceToken
							});
						} else {
							if (typeof mensajesPush_error != 'undefined') mensajesPush_error({
								value: ee.message,
								code: ee.error
							});
						}
					});
				},
				error: function(e) {
					if (typeof mensajesPush_error != 'undefined') mensajesPush_error({
						value: e.error
					});
				},
				callback: function(e) {
					if (typeof mensajesPush_message != 'undefined') mensajesPush_message({
						value: e
					});
				}
			});
		}
	}
})();