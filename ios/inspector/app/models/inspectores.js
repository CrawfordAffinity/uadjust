exports.definition = {
	config: {
		columns: {
			"apellido_materno": "TEXT",
			"id_nivel1": "INTEGER",
			"lat_dir": "TEXT",
			"disponibilidad_viajar_pais": "INTEGER",
			"uuid": "TEXT",
			"fecha_nacimiento": "INTEGER",
			"d2": "INTEGER",
			"d1": "INTEGER",
			"password": "TEXT",
			"pais": "INTEGER",
			"direccion": "TEXT",
			"d3": "INTEGER",
			"nivel3": "TEXT",
			"d5": "INTEGER",
			"d4": "INTEGER",
			"disponibilidad_fechas": "TEXT",
			"d7": "INTEGER",
			"nivel4": "TEXT",
			"nombre": "TEXT",
			"disponibilidad_horas": "TEXT",
			"nivel5": "TEXT",
			"nivel2": "TEXT",
			"disponibilidad_viajar_ciudad": "INTEGER",
			"lon_dir": "TEXT",
			"id_server": "INTEGER",
			"d6": "INTEGER",
			"experiencia_detalle": "TEXT",
			"telefono": "TEXT",
			"disponibilidad": "INTEGER",
			"id": "INTEGER PRIMARY KEY AUTOINCREMENT",
			"direccion_correccion": "INTEGER",
			"experiencia_oficio": "INTEGER",
			"codigo_identificador": "TEXT",
			"apellido_paterno": "TEXT",
			"correo": "TEXT",
		},
		adapter: {
			"type": "sql",
			"collection_name": "inspectores",
			"idAttribute": "id",
			"remoteBackup": "false",
		}
	},
	extendModel: function(Model) {
		_.extend(Model.prototype, {
			transform: function transform() {
				var transformed = this.toJSON();
				return transformed;
			}
		});
		return Model;
	},
	extendCollection: function(Collection) {
		// helper functions
		function S4() {
			return (0 | 65536 * (1 + Math.random())).toString(16).substring(1);
		}
		function guid() {
			return S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4();
		}
		// start extend
		_.extend(Collection.prototype, {
			deleteAll : function() {
				var collection = this;
				var sql = "DELETE FROM " + collection.config.adapter.collection_name;
				db = Ti.Database.open(collection.config.adapter.db_name);
				db.execute(sql);
				db.close();
				collection.trigger('sync');
			}
		});
		// end extend
		return Collection;
	}
};
