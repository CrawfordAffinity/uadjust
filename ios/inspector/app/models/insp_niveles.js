exports.definition = {
	config: {
		columns: {
			"ids_entrepisos": "TEXT",
			"id_inspeccion": "INTEGER",
			"superficie": "TEXT",
			"nombre": "TEXT",
			"largo": "TEXT",
			"ids_pavimentos": "TEXT",
			"alto": "TEXT",
			"ano": "INTEGER",
			"ids_muros": "TEXT",
			"ancho": "TEXT",
			"id": "INTEGER PRIMARY KEY AUTOINCREMENT",
			"ids_estructura_cubiera": "TEXT",
			"ids_cubierta": "TEXT",
			"piso": "TEXT",
			"ids_estructuras_soportantes": "TEXT",
		},
		adapter: {
			"type": "sql",
			"collection_name": "insp_niveles",
			"idAttribute": "id",
			"remoteBackup": "false",
		}
	},
	extendModel: function(Model) {
		_.extend(Model.prototype, {
			transform: function transform() {
				var transformed = this.toJSON();
				return transformed;
			}
		});
		return Model;
	},
	extendCollection: function(Collection) {
		// helper functions
		function S4() {
			return (0 | 65536 * (1 + Math.random())).toString(16).substring(1);
		}
		function guid() {
			return S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4();
		}
		// start extend
		_.extend(Collection.prototype, {
			deleteAll : function() {
				var collection = this;
				var sql = "DELETE FROM " + collection.config.adapter.collection_name;
				db = Ti.Database.open(collection.config.adapter.db_name);
				db.execute(sql);
				db.close();
				collection.trigger('sync');
			}
		});
		// end extend
		return Collection;
	}
};
