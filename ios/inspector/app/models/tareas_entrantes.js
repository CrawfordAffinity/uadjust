exports.definition = {
	config: {
		columns: {
			"fecha_tarea": "TEXT",
			"id_inspeccion": "INTEGER",
			"id_asegurado": "INTEGER",
			"nivel_2": "TEXT",
			"comentario_can_o_rech": "TEXT",
			"asegurado_tel_fijo": "TEXT",
			"estado_tarea": "INTEGER",
			"bono": "TEXT",
			"evento": "TEXT",
			"id_inspector": "INTEGER",
			"asegurado_codigo_identificador": "TEXT",
			"lat": "TEXT",
			"nivel_1": "TEXT",
			"asegurado_nombre": "TEXT",
			"pais": "INTEGER",
			"direccion": "TEXT",
			"asegurador": "TEXT",
			"fecha_ingreso": "TEXT",
			"fecha_siniestro": "TEXT",
			"nivel_1_texto": "TEXT",
			"distance": "INTEGER",
			"nivel_4": "TEXT",
			"perfil": "TEXT",
			"asegurado_id": "TEXT",
			"pais_texto": "TEXT",
			"id_server": "INTEGER",
			"categoria": "INTEGER",
			"nivel_3": "TEXT",
			"asegurado_correo": "TEXT",
			"num_caso": "INTEGER",
			"lon": "TEXT",
			"id": "INTEGER PRIMARY KEY AUTOINCREMENT",
			"asegurado_tel_movil": "TEXT",
			"tipo_tarea": "INTEGER",
			"nivel_5": "TEXT",
		},
		adapter: {
			"type": "sql",
			"collection_name": "tareas_entrantes",
			"idAttribute": "id",
			"remoteBackup": "false",
		}
	},
	extendModel: function(Model) {
		_.extend(Model.prototype, {
			transform: function transform() {
				var transformed = this.toJSON();
				return transformed;
			}
		});
		return Model;
	},
	extendCollection: function(Collection) {
		// helper functions
		function S4() {
			return (0 | 65536 * (1 + Math.random())).toString(16).substring(1);
		}
		function guid() {
			return S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4();
		}
		// start extend
		_.extend(Collection.prototype, {
			deleteAll : function() {
				var collection = this;
				var sql = "DELETE FROM " + collection.config.adapter.collection_name;
				db = Ti.Database.open(collection.config.adapter.db_name);
				db.execute(sql);
				db.close();
				collection.trigger('sync');
			}
		});
		// end extend
		return Collection;
	}
};
