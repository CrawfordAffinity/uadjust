Widget Boton Largo
Representa un boton con progreso opcional y diferentes estilos

@param titulo Define el texto del boton
@param color Define el color del boton: amarillo, rojo, verde, lila, naranjo, morado, rosado, celeste, cafe
@param icono Define el alias del icono a mostrar junto al texto
