/** 
 * Widget foto_chica
 * Control para centralizar la compresion y escalamiento de las imagenes capturadas, en una caja
 * 
 * @param caja Indica el ancho-alto de este widget
 * @param label Indica el texto que se muestra sobre la imagen de este widget; solo se muestra si esta definido
 * @param opcional Indica el subtitulo del texto label para mostrar que imagen es opcional; solo se muestra si esta definido
 * @param id Identifica esta instancia para su uso en conjunto con otras en una misma pantalla 
 */
var _bind4section = {};

var args = arguments[0] || {};

function Load_imagen2(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var evento = e;
	elemento.start();

}

function Change_imagen3(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var evento = e;
	var conteo_end = ('conteo_end' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['conteo_end'] : '';
	if (evento.index == conteo_end) {
		require(WPATH('vars'))[args.__id]['conteo_end'] = '';
		elemento.pause();
	}

}

function Click_vista2(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var estado = ('estado' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['estado'] : '';
	if (Ti.App.deployType != 'production') console.log('estado actual en click', {
		"estado": estado
	});
	if (estado == 0 || estado == '0') {
		var vista3_visible = false;

		if (vista3_visible == 'si') {
			vista3_visible = true;
		} else if (vista3_visible == 'no') {
			vista3_visible = false;
		}
		$.vista3.setVisible(vista3_visible);

		var vista4_visible = true;

		if (vista4_visible == 'si') {
			vista4_visible = true;
		} else if (vista4_visible == 'no') {
			vista4_visible = false;
		}
		$.vista4.setVisible(vista4_visible);

		$.imagen2.start();
		var Label2_estilo = 'destacado';

		if (typeof WPATH != 'undefined') {
			var _tmp_a4w = require(WPATH('a4w'));
		} else {
			var _tmp_a4w = require('a4w');
		}
		if ((typeof Label2_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (Label2_estilo in _tmp_a4w.styles['classes'])) {
			try {
				Label2_estilo = _tmp_a4w.styles['classes'][Label2_estilo];
			} catch (st_val_err) {}
		}
		$.Label2.applyProperties(Label2_estilo);

		require(WPATH('vars'))[args.__id]['estado'] = '1';

		if ('__args' in args) {
			args['__args'].onclick({});
		} else {
			args.onclick({});
		}
	} else if (estado == 2) {
		var Label2_estilo = 'destacado';

		if (typeof WPATH != 'undefined') {
			var _tmp_a4w = require(WPATH('a4w'));
		} else {
			var _tmp_a4w = require('a4w');
		}
		if ((typeof Label2_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (Label2_estilo in _tmp_a4w.styles['classes'])) {
			try {
				Label2_estilo = _tmp_a4w.styles['classes'][Label2_estilo];
			} catch (st_val_err) {}
		}
		$.Label2.applyProperties(Label2_estilo);


		if ('__args' in args) {
			args['__args'].onclick({});
		} else {
			args.onclick({});
		}
	}
}
/** 
 * Funcion que inicializa el widget 
 */

$.init = function(params) {
	for (var tobe in params) args[tobe] = params[tobe];
	if ('__id' in params) require(WPATH('vars'))[params.__id] = {};
	/** 
	 * Declaramos e instanciamos la variable &quot;estado&quot; 
	 */
	require(WPATH('vars'))[args.__id]['estado'] = '0';
	/** 
	 * Declaramos e instanciamos la variable &quot;caja&quot; 
	 */
	require(WPATH('vars'))[args.__id]['caja'] = 55;
	if (!_.isUndefined(params.caja)) {
		/** 
		 * Verifica si params.caja existe 
		 */
		/** 
		 * modifica el ancho y alto de la vista que contiene la foto con el valor de params.caja 
		 */
		var vista2_ancho = params.caja;

		if (vista2_ancho == '*') {
			vista2_ancho = Ti.UI.FILL;
		} else if (vista2_ancho == '-') {
			vista2_ancho = Ti.UI.SIZE;
		} else if (!isNaN(vista2_ancho)) {
			vista2_ancho = vista2_ancho + 'dp';
		}
		$.vista2.setWidth(vista2_ancho);

		var vista2_alto = params.caja;

		if (vista2_alto == '*') {
			vista2_alto = Ti.UI.FILL;
		} else if (vista2_alto == '-') {
			vista2_alto = Ti.UI.SIZE;
		} else if (!isNaN(vista2_alto)) {
			vista2_alto = vista2_alto + 'dp';
		}
		$.vista2.setHeight(vista2_alto);

		require(WPATH('vars'))[args.__id]['caja'] = params.caja;
	}
	if (!_.isUndefined(params.label)) {
		/** 
		 * Verifica si params.label existe 
		 */
		/** 
		 * Modifica el texto del label que actua como titulo y ademas lo muestra si esque estuviera oculto. 
		 */
		$.Label2.setText(params.label);

		var Label2_visible = true;

		if (Label2_visible == 'si') {
			Label2_visible = true;
		} else if (Label2_visible == 'no') {
			Label2_visible = false;
		}
		$.Label2.setVisible(Label2_visible);

	}
	if (!_.isUndefined(params.opcional)) {
		/** 
		 * Verifica si params.opcional existe 
		 */
		/** 
		 * Modifica el texto del label que actua como titulo opcional y ademas lo muestra si esque estuviera oculto.. 
		 */
		$.Label.setText(params.opcional);

		var Label_visible = true;

		if (Label_visible == 'si') {
			Label_visible = true;
		} else if (Label_visible == 'no') {
			Label_visible = false;
		}
		$.Label.setVisible(Label_visible);

	}
	if (!_.isUndefined(params.id)) {
		/** 
		 * Verifica si params.id existe 
		 */
		require(WPATH('vars'))[args.__id]['id'] = params.id;
	}
};

/** 
 * Funcion que recibe una imagen, la escala y comprime segun sus parametros y retorna una respuesta al padre de la instancia
 * 
 * @param imagen Imagen BLOB a ser procesada
 * @param nueva Pixeles para definir nuevo ancho-alto de imagen escalada
 * @param calidad Si definida, comprime imagen entregada al porcentaje indicado (solo enteros). 
 */

$.procesar = function(params) {
	if (!_.isUndefined(params.imagen)) {
		/** 
		 * Verifica si params.imagen existe 
		 */
		/** 
		 * Ocultamos wiggle 
		 */
		var vista4_visible = false;

		if (vista4_visible == 'si') {
			vista4_visible = true;
		} else if (vista4_visible == 'no') {
			vista4_visible = false;
		}
		$.vista4.setVisible(vista4_visible);

		/** 
		 * Mostramos anim1 
		 */
		var vista5_visible = true;

		if (vista5_visible == 'si') {
			vista5_visible = true;
		} else if (vista5_visible == 'no') {
			vista5_visible = false;
		}
		$.vista5.setVisible(vista5_visible);

		require(WPATH('vars'))[args.__id]['conteo_end'] = 30;
		/** 
		 * Comenzamos la animacion 
		 */
		$.imagen3.start();
		var caja = ('caja' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['caja'] : '';
		/** 
		 * Escalamos imagen al tama&#241;o requerido 
		 */
		if (OS_ANDROID) {
			var escalarImagen_imagefactory = require('ti.imagefactory');
			var mini = escalarImagen_imagefactory.imageAsResized(params.imagen, {
				width: caja,
				height: caja,
				quality: 0.7
			});
		} else {
			var mini = params.imagen.imageAsResized(caja, caja);
		}
		if (!_.isUndefined(params.nueva)) {
			/** 
			 * Verifica si existe params.nueva 
			 */
			/** 
			 * Escala imagen al tama&#241;o requerido 
			 */
			if (OS_ANDROID) {
				var escalarImagen2_imagefactory = require('ti.imagefactory');
				var fotofinal = escalarImagen2_imagefactory.imageAsResized(params.imagen, {
					width: params.nueva,
					height: params.nueva,
					quality: 0.7
				});
			} else {
				var fotofinal = params.imagen.imageAsResized(params.nueva, params.nueva);
			}
			require(WPATH('vars'))[args.__id]['conteo_end'] = '';
			/** 
			 * Finalizamos la animacion 
			 */
			$.imagen3.resume();
			if (!_.isUndefined(params.calidad)) {
				/** 
				 * Verifica si existe params.calidad 
				 */
				/** 
				 * Comprime imagen escalada en la calidad indicada 
				 */
				if (OS_ANDROID) {
					var comprimirImagen_imagefactory = require('ti.imagefactory');
					var comprimida = comprimirImagen_imagefactory.compress(fotofinal, params.calidad / 100);
				} else if (OS_IOS) {
					var comprimirImagen_imagefactory = require('ti.imagefactory');
					var comprimida = comprimirImagen_imagefactory.compress(fotofinal, params.calidad / 100);
				}
				require(WPATH('vars'))[args.__id]['estado'] = 2;
				var vista5_visible = false;

				if (vista5_visible == 'si') {
					vista5_visible = true;
				} else if (vista5_visible == 'no') {
					vista5_visible = false;
				}
				$.vista5.setVisible(vista5_visible);

				/** 
				 * Ocultamos wiggle 
				 */
				var vista4_visible = false;

				if (vista4_visible == 'si') {
					vista4_visible = true;
				} else if (vista4_visible == 'no') {
					vista4_visible = false;
				}
				$.vista4.setVisible(vista4_visible);

				/** 
				 * mostramos thumbnail 
				 */
				var vista6_visible = true;

				if (vista6_visible == 'si') {
					vista6_visible = true;
				} else if (vista6_visible == 'no') {
					vista6_visible = false;
				}
				$.vista6.setVisible(vista6_visible);

				/** 
				 * cambiamos la imagen de la caja que contiene lafoto por la imagen escalada a mini 
				 */
				var imagen4_imagen = mini;

				if (typeof imagen4_imagen == 'string' && 'styles' in require(WPATH('a4w')) && imagen4_imagen in require(WPATH('a4w')).styles['images']) {
					imagen4_imagen = WPATH(require(WPATH('a4w')).styles['images'][imagen4_imagen]);
				}
				$.imagen4.setImage(imagen4_imagen);


				if ('__args' in args) {
					args['__args'].onlisto({
						escalada: fotofinal,
						mini: mini,
						comprimida: comprimida
					});
				} else {
					args.onlisto({
						escalada: fotofinal,
						mini: mini,
						comprimida: comprimida
					});
				}
			} else {
				require(WPATH('vars'))[args.__id]['estado'] = 2;
				var vista5_visible = false;

				if (vista5_visible == 'si') {
					vista5_visible = true;
				} else if (vista5_visible == 'no') {
					vista5_visible = false;
				}
				$.vista5.setVisible(vista5_visible);

				var vista4_visible = false;

				if (vista4_visible == 'si') {
					vista4_visible = true;
				} else if (vista4_visible == 'no') {
					vista4_visible = false;
				}
				$.vista4.setVisible(vista4_visible);

				var vista6_visible = true;

				if (vista6_visible == 'si') {
					vista6_visible = true;
				} else if (vista6_visible == 'no') {
					vista6_visible = false;
				}
				$.vista6.setVisible(vista6_visible);

				var imagen4_imagen = mini;

				if (typeof imagen4_imagen == 'string' && 'styles' in require(WPATH('a4w')) && imagen4_imagen in require(WPATH('a4w')).styles['images']) {
					imagen4_imagen = WPATH(require(WPATH('a4w')).styles['images'][imagen4_imagen]);
				}
				$.imagen4.setImage(imagen4_imagen);


				if ('__args' in args) {
					args['__args'].onlisto({
						escalada: fotofinal,
						mini: mini
					});
				} else {
					args.onlisto({
						escalada: fotofinal,
						mini: mini
					});
				}
			}
		} else {
			require(WPATH('vars'))[args.__id]['estado'] = 2;
			var vista5_visible = false;

			if (vista5_visible == 'si') {
				vista5_visible = true;
			} else if (vista5_visible == 'no') {
				vista5_visible = false;
			}
			$.vista5.setVisible(vista5_visible);

			var vista4_visible = false;

			if (vista4_visible == 'si') {
				vista4_visible = true;
			} else if (vista4_visible == 'no') {
				vista4_visible = false;
			}
			$.vista4.setVisible(vista4_visible);

			var vista6_visible = true;

			if (vista6_visible == 'si') {
				vista6_visible = true;
			} else if (vista6_visible == 'no') {
				vista6_visible = false;
			}
			$.vista6.setVisible(vista6_visible);

			var imagen4_imagen = mini;

			if (typeof imagen4_imagen == 'string' && 'styles' in require(WPATH('a4w')) && imagen4_imagen in require(WPATH('a4w')).styles['images']) {
				imagen4_imagen = WPATH(require(WPATH('a4w')).styles['images'][imagen4_imagen]);
			}
			$.imagen4.setImage(imagen4_imagen);


			if ('__args' in args) {
				args['__args'].onlisto({
					mini: mini
				});
			} else {
				args.onlisto({
					mini: mini
				});
			}
		}
	}
};

/** 
 * Detiene todas las animaciones y muestra la imagen estatica miniatura 
 */

$.detener = function(params) {
	var estado = ('estado' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['estado'] : '';
	if (estado == 1 || estado == '1') {
		/** 
		 * Verifica si la variable &quot;estado&quot; es igual a 1 
		 */
		/** 
		 * Ocultamos wiggle 
		 */
		var vista4_visible = false;

		if (vista4_visible == 'si') {
			vista4_visible = true;
		} else if (vista4_visible == 'no') {
			vista4_visible = false;
		}
		$.vista4.setVisible(vista4_visible);

		/** 
		 * Ocultamos conteo 
		 */
		var vista5_visible = false;

		if (vista5_visible == 'si') {
			vista5_visible = true;
		} else if (vista5_visible == 'no') {
			vista5_visible = false;
		}
		$.vista5.setVisible(vista5_visible);

		/** 
		 * Ocultamos minifoto en caso de se estuviera mostrando 
		 */
		var vista6_visible = false;

		if (vista6_visible == 'si') {
			vista6_visible = true;
		} else if (vista6_visible == 'no') {
			vista6_visible = false;
		}
		$.vista6.setVisible(vista6_visible);

		/** 
		 * Mostramos imagen estatica 
		 */
		var vista3_visible = true;

		if (vista3_visible == 'si') {
			vista3_visible = true;
		} else if (vista3_visible == 'no') {
			vista3_visible = false;
		}
		$.vista3.setVisible(vista3_visible);

		/** 
		 * Modifica el estilo del label que actua como titulo del widget 
		 */
		var Label2_estilo = 'normal';

		if (typeof WPATH != 'undefined') {
			var _tmp_a4w = require(WPATH('a4w'));
		} else {
			var _tmp_a4w = require('a4w');
		}
		if ((typeof Label2_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (Label2_estilo in _tmp_a4w.styles['classes'])) {
			try {
				Label2_estilo = _tmp_a4w.styles['classes'][Label2_estilo];
			} catch (st_val_err) {}
		}
		$.Label2.applyProperties(Label2_estilo);

		/** 
		 * Guarda variable &quot;estado&quot; con valor 0 
		 */
		require(WPATH('vars'))[args.__id]['estado'] = '0';
	} else if (estado == 2) {
		/** 
		 * Verifica si la variable &quot;estado&quot; es igual a 2 
		 */
		/** 
		 * Modifica estilo del label que actua como titulo del widget 
		 */
		var Label2_estilo = 'normal';

		if (typeof WPATH != 'undefined') {
			var _tmp_a4w = require(WPATH('a4w'));
		} else {
			var _tmp_a4w = require('a4w');
		}
		if ((typeof Label2_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (Label2_estilo in _tmp_a4w.styles['classes'])) {
			try {
				Label2_estilo = _tmp_a4w.styles['classes'][Label2_estilo];
			} catch (st_val_err) {}
		}
		$.Label2.applyProperties(Label2_estilo);

	}
};

/** 
 * Funcion que asigna imagen mini en vista de control
 * 
 * @param mini Imagen miniatura a asignar 
 */

$.mini = function(params) {
	if (!_.isUndefined(params.mini)) {
		/** 
		 * Verifica si existe params.mini 
		 */
		/** 
		 * Ocultamos wiggle 
		 */
		var vista4_visible = false;

		if (vista4_visible == 'si') {
			vista4_visible = true;
		} else if (vista4_visible == 'no') {
			vista4_visible = false;
		}
		$.vista4.setVisible(vista4_visible);

		/** 
		 * Mostramos thumbnail 
		 */
		var vista6_visible = true;

		if (vista6_visible == 'si') {
			vista6_visible = true;
		} else if (vista6_visible == 'no') {
			vista6_visible = false;
		}
		$.vista6.setVisible(vista6_visible);

		/** 
		 * Modificamos imagen de la caja por la que viene en params.mini 
		 */
		var imagen4_imagen = params.mini;

		if (typeof imagen4_imagen == 'string' && 'styles' in require(WPATH('a4w')) && imagen4_imagen in require(WPATH('a4w')).styles['images']) {
			imagen4_imagen = WPATH(require(WPATH('a4w')).styles['images'][imagen4_imagen]);
		}
		$.imagen4.setImage(imagen4_imagen);

	}
};