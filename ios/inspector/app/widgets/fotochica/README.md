Widget foto_chica
Control para centralizar la compresion y escalamiento de las imagenes capturadas, en una caja

@param caja Indica el ancho-alto de este widget
@param label Indica el texto que se muestra sobre la imagen de este widget; solo se muestra si esta definido
@param opcional Indica el subtitulo del texto label para mostrar que imagen es opcional; solo se muestra si esta definido
@param id Identifica esta instancia para su uso en conjunto con otras en una misma pantalla
