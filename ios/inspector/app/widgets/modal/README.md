Widget Modal
Representa un combobox que abre una pantalla en forma de modal con items para seleccionar

@param data Variable con datos para modal
@param campo Define el texto del campo
@param titulo Define el titulo del modal
@param guardar Define si mostrar o no el boton guardar
@param color Define el color del modal
@param seleccione Modifica el hint del combobox
@param left Modifica el margen izquierdo del campo del selector
@param right Modifica el margen derecho del campo del selector
@param top Modifica el margen superior del campo del selector
@param activo Si true, permite seleccionar un valor. En false, esta desactivado
@param cargando Texto a mostrar mientras se carga en memoria datos para selector
