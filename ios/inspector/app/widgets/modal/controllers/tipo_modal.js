var _bind4section = {};
var _list_templates = {
	"elemento": {
		"Label2": {
			"text": "{valor}",
			"_data": "{data}"
		},
		"vista6": {}
	}
};

var _activity;
if (OS_ANDROID) {
	_activity = $.MODAL.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
if (_.isUndefined(require(WPATH('vars'))[args.__id])) require(WPATH('vars'))[args.__id] = {};
if (OS_ANDROID) {
	$.MODAL_window.addEventListener('open', function(e) {
		abx.setStatusbarColor("#000000");
		abx.setBackgroundColor("white");
	});
}

function Click_vista4(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.MODAL.close();

}

function Click_vista5(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var valor;
	$.buscadorListado.valor('valor');

	if ('__args' in args) {
		args['__args'].onrespuesta({
			valor: valor,
			item: valor
		});
	} else {
		args.onrespuesta({
			valor: valor,
			item: valor
		});
	}
	$.MODAL.close();

}

function Itemclick_listado(e) {

	e.cancelBubble = true;
	var objeto = e.section.getItemAt(e.itemIndex);
	var modelo = {},
		_modelo = [];
	var fila = {},
		fila_bak = {},
		info = {
			_template: objeto.template,
			_what: [],
			_seccion_ref: e.section.getHeaderTitle(),
			_model_id: -1
		},
		_tmp = {
			objmap: {}
		};
	if ('itemId' in e) {
		info._model_id = e.itemId;
		modelo._id = info._model_id;
		if (info._seccion_ref != '' && info._seccion_ref in _bind4section) {
			modelo._collection = _bind4section[info._seccion_ref];
			_tmp._coll = modelo._collection;
		}
	}
	if (info._seccion_ref != '' && info._seccion_ref in _bind4section) {
		if (Widget.Collections[_tmp._coll].config.adapter.type == 'sql') {
			_tmp._inst = Widget.Collections[_tmp._coll];
			_tmp._id = Widget.Collections[_tmp._coll].config.adapter.idAttribute;
			_tmp._dbsql = 'SELECT * FROM ' + Widget.Collections[_tmp._coll].config.adapter.collection_name + ' WHERE ' + _tmp._id + ' = ' + e.itemId;
			_tmp._db = Ti.Database.open(Widget.Collections[_tmp._coll].config.adapter.db_name);
			_modelo = _tmp._db.execute(_tmp._dbsql);
			var values = [],
				fieldNames = [];
			var fieldCount = _.isFunction(_modelo.fieldCount) ? _modelo.fieldCount() : _modelo.fieldCount;
			var getField = _modelo.field;
			var i = 0;
			for (; fieldCount > i; i++) fieldNames.push(_modelo.fieldName(i));
			while (_modelo.isValidRow()) {
				var o = {};
				for (i = 0; fieldCount > i; i++) o[fieldNames[i]] = getField(i);
				values.push(o);
				_modelo.next();
			}
			_modelo = values;
			_tmp._db.close();
		} else {
			_tmp._search = {};
			_tmp._search[_tmp._id] = e.itemId + '';
			_modelo = Widget.Collections[_tmp._coll].fetch(_tmp._search);
		}
	}
	var findVariables = require(WPATH('fvariables'));
	_.each(_list_templates[info._template], function(obj_id, id) {
		_.each(obj_id, function(valor, prop) {
			var llaves = findVariables(valor, '{', '}');
			_.each(llaves, function(llave) {
				_tmp.objmap[llave] = {
					id: id,
					prop: prop
				};
				fila[llave] = objeto[id][prop];
				if (id == e.bindId) info._what.push(llave);
			});
		});
	});
	info._what = info._what.join(',');
	fila_bak = JSON.parse(JSON.stringify(fila));
	if (Ti.App.deployType != 'production') console.log('click en item de modal', {
		"fila": fila
	});

	if ('__args' in args) {
		args['__args'].onrespuesta({
			valor: fila.data.valor,
			item: fila.data
		});
	} else {
		args.onrespuesta({
			valor: fila.data.valor,
			item: fila.data
		});
	}
	$.MODAL.close();
	_tmp.changed = false;
	_tmp.diff_keys = [];
	_.each(fila, function(value1, prop) {
		var had_samekey = false;
		_.each(fila_bak, function(value2, prop2) {
			if (prop == prop2 && value1 == value2) {
				had_samekey = true;
			} else if (!_.has(fila_bak, prop) || !_.has(fila, prop2)) {
				has_samekey = true;
			}
		});
		if (!had_samekey) _tmp.diff_keys.push(prop);
	});
	if (_tmp.diff_keys.length > 0) _tmp.changed = true;
	if (_tmp.changed == true) {
		_.each(_tmp.diff_keys, function(llave) {
			objeto[_tmp.objmap[llave].id][_tmp.objmap[llave].prop] = fila[llave];
		});
		e.section.updateItemAt(e.itemIndex, objeto);
	}

}

(function() {
	var titulo = ('titulo' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['titulo'] : '';
	var guardar = ('guardar' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['guardar'] : '';
	var color = ('color' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['color'] : '';
	var data = ('data' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['data'] : '';
	var params = ('params' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['params'] : '';
	var MODAL_titulo = titulo;

	var setTitle2 = function(valor) {
		if (OS_ANDROID) {
			abx.title = valor;
		} else {
			$.MODAL_window.setTitle(valor);
		}
	};
	var getTitle2 = function() {
		if (OS_ANDROID) {
			return abx.title;
		} else {
			return $.MODAL_window.getTitle();
		}
	};
	setTitle2(MODAL_titulo);

	if (guardar == false || guardar == 'false') {
		var vista5_visible = false;

		if (vista5_visible == 'si') {
			vista5_visible = true;
		} else if (vista5_visible == 'no') {
			vista5_visible = false;
		}
		$.vista5.setVisible(vista5_visible);

	}
	if (!_.isUndefined(params.color)) {
		if (params.color == 'blanco') {
			var iconoCerrar_estilo = 'color_blanco';

			if (typeof WPATH != 'undefined') {
				var _tmp_a4w = require(WPATH('a4w'));
			} else {
				var _tmp_a4w = require('a4w');
			}
			if ((typeof iconoCerrar_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (iconoCerrar_estilo in _tmp_a4w.styles['classes'])) {
				try {
					iconoCerrar_estilo = _tmp_a4w.styles['classes'][iconoCerrar_estilo]['color'];
				} catch (st_val_err) {}
			}
			$.iconoCerrar.setColor(iconoCerrar_estilo);

			var buscadorListado_barcolor = 'color_blanco';

			var cambiarColor = function(i__valor) {
				var _tmp_a4w = require(WPATH('a4w'));
				var i_set_val = i__valor;
				if ((typeof i_set_val == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (i_set_val in _tmp_a4w.styles['classes'])) {
					try {
						i_set_val = _tmp_a4w.styles['classes'][i_set_val]['color'];
					} catch (st_val_err) {}
				}
				_tmp_a4w = null;
				if (OS_IOS) {
					$.buscadorListado.setBarColor(i_set_val);
				} else {
					$.buscadorListado.setBackgroundColor(i_set_val);
				}
			};
			cambiarColor(buscadorListado_barcolor);

		} else if (params.color == 'negro') {
			var iconoCerrar_estilo = 'color_negro';

			if (typeof WPATH != 'undefined') {
				var _tmp_a4w = require(WPATH('a4w'));
			} else {
				var _tmp_a4w = require('a4w');
			}
			if ((typeof iconoCerrar_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (iconoCerrar_estilo in _tmp_a4w.styles['classes'])) {
				try {
					iconoCerrar_estilo = _tmp_a4w.styles['classes'][iconoCerrar_estilo]['color'];
				} catch (st_val_err) {}
			}
			$.iconoCerrar.setColor(iconoCerrar_estilo);

			var buscadorListado_barcolor = 'color_negro';

			var cambiarColor = function(i__valor) {
				var _tmp_a4w = require(WPATH('a4w'));
				var i_set_val = i__valor;
				if ((typeof i_set_val == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (i_set_val in _tmp_a4w.styles['classes'])) {
					try {
						i_set_val = _tmp_a4w.styles['classes'][i_set_val]['color'];
					} catch (st_val_err) {}
				}
				_tmp_a4w = null;
				if (OS_IOS) {
					$.buscadorListado.setBarColor(i_set_val);
				} else {
					$.buscadorListado.setBackgroundColor(i_set_val);
				}
			};
			cambiarColor(buscadorListado_barcolor);

		} else if (params.color == 'azul') {
			var iconoCerrar_estilo = 'color_azul';

			if (typeof WPATH != 'undefined') {
				var _tmp_a4w = require(WPATH('a4w'));
			} else {
				var _tmp_a4w = require('a4w');
			}
			if ((typeof iconoCerrar_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (iconoCerrar_estilo in _tmp_a4w.styles['classes'])) {
				try {
					iconoCerrar_estilo = _tmp_a4w.styles['classes'][iconoCerrar_estilo]['color'];
				} catch (st_val_err) {}
			}
			$.iconoCerrar.setColor(iconoCerrar_estilo);

			var buscadorListado_barcolor = 'fondoazul';

			var cambiarColor = function(i__valor) {
				var _tmp_a4w = require(WPATH('a4w'));
				var i_set_val = i__valor;
				if ((typeof i_set_val == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (i_set_val in _tmp_a4w.styles['classes'])) {
					try {
						i_set_val = _tmp_a4w.styles['classes'][i_set_val]['color'];
					} catch (st_val_err) {}
				}
				_tmp_a4w = null;
				if (OS_IOS) {
					$.buscadorListado.setBarColor(i_set_val);
				} else {
					$.buscadorListado.setBackgroundColor(i_set_val);
				}
			};
			cambiarColor(buscadorListado_barcolor);

		} else if (params.color == 'plomo') {
			var iconoCerrar_estilo = 'color_plomo';

			if (typeof WPATH != 'undefined') {
				var _tmp_a4w = require(WPATH('a4w'));
			} else {
				var _tmp_a4w = require('a4w');
			}
			if ((typeof iconoCerrar_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (iconoCerrar_estilo in _tmp_a4w.styles['classes'])) {
				try {
					iconoCerrar_estilo = _tmp_a4w.styles['classes'][iconoCerrar_estilo]['color'];
				} catch (st_val_err) {}
			}
			$.iconoCerrar.setColor(iconoCerrar_estilo);

			var buscadorListado_barcolor = 'color_plomo';

			var cambiarColor = function(i__valor) {
				var _tmp_a4w = require(WPATH('a4w'));
				var i_set_val = i__valor;
				if ((typeof i_set_val == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (i_set_val in _tmp_a4w.styles['classes'])) {
					try {
						i_set_val = _tmp_a4w.styles['classes'][i_set_val]['color'];
					} catch (st_val_err) {}
				}
				_tmp_a4w = null;
				if (OS_IOS) {
					$.buscadorListado.setBarColor(i_set_val);
				} else {
					$.buscadorListado.setBackgroundColor(i_set_val);
				}
			};
			cambiarColor(buscadorListado_barcolor);

		} else if (params.color == 'rojo') {
			var iconoCerrar_estilo = 'color_rojo';

			if (typeof WPATH != 'undefined') {
				var _tmp_a4w = require(WPATH('a4w'));
			} else {
				var _tmp_a4w = require('a4w');
			}
			if ((typeof iconoCerrar_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (iconoCerrar_estilo in _tmp_a4w.styles['classes'])) {
				try {
					iconoCerrar_estilo = _tmp_a4w.styles['classes'][iconoCerrar_estilo]['color'];
				} catch (st_val_err) {}
			}
			$.iconoCerrar.setColor(iconoCerrar_estilo);

			var buscadorListado_barcolor = 'color_rojo';

			var cambiarColor = function(i__valor) {
				var _tmp_a4w = require(WPATH('a4w'));
				var i_set_val = i__valor;
				if ((typeof i_set_val == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (i_set_val in _tmp_a4w.styles['classes'])) {
					try {
						i_set_val = _tmp_a4w.styles['classes'][i_set_val]['color'];
					} catch (st_val_err) {}
				}
				_tmp_a4w = null;
				if (OS_IOS) {
					$.buscadorListado.setBarColor(i_set_val);
				} else {
					$.buscadorListado.setBackgroundColor(i_set_val);
				}
			};
			cambiarColor(buscadorListado_barcolor);

		} else if (params.color == 'lila') {
			var iconoCerrar_estilo = 'color_lila';

			if (typeof WPATH != 'undefined') {
				var _tmp_a4w = require(WPATH('a4w'));
			} else {
				var _tmp_a4w = require('a4w');
			}
			if ((typeof iconoCerrar_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (iconoCerrar_estilo in _tmp_a4w.styles['classes'])) {
				try {
					iconoCerrar_estilo = _tmp_a4w.styles['classes'][iconoCerrar_estilo]['color'];
				} catch (st_val_err) {}
			}
			$.iconoCerrar.setColor(iconoCerrar_estilo);

			var buscadorListado_barcolor = 'color_lila';

			var cambiarColor = function(i__valor) {
				var _tmp_a4w = require(WPATH('a4w'));
				var i_set_val = i__valor;
				if ((typeof i_set_val == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (i_set_val in _tmp_a4w.styles['classes'])) {
					try {
						i_set_val = _tmp_a4w.styles['classes'][i_set_val]['color'];
					} catch (st_val_err) {}
				}
				_tmp_a4w = null;
				if (OS_IOS) {
					$.buscadorListado.setBarColor(i_set_val);
				} else {
					$.buscadorListado.setBackgroundColor(i_set_val);
				}
			};
			cambiarColor(buscadorListado_barcolor);

		} else if (params.color == 'naranjo') {
			var iconoCerrar_estilo = 'color_naranjo';

			if (typeof WPATH != 'undefined') {
				var _tmp_a4w = require(WPATH('a4w'));
			} else {
				var _tmp_a4w = require('a4w');
			}
			if ((typeof iconoCerrar_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (iconoCerrar_estilo in _tmp_a4w.styles['classes'])) {
				try {
					iconoCerrar_estilo = _tmp_a4w.styles['classes'][iconoCerrar_estilo]['color'];
				} catch (st_val_err) {}
			}
			$.iconoCerrar.setColor(iconoCerrar_estilo);

			var buscadorListado_barcolor = 'fondonaranjo';

			var cambiarColor = function(i__valor) {
				var _tmp_a4w = require(WPATH('a4w'));
				var i_set_val = i__valor;
				if ((typeof i_set_val == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (i_set_val in _tmp_a4w.styles['classes'])) {
					try {
						i_set_val = _tmp_a4w.styles['classes'][i_set_val]['color'];
					} catch (st_val_err) {}
				}
				_tmp_a4w = null;
				if (OS_IOS) {
					$.buscadorListado.setBarColor(i_set_val);
				} else {
					$.buscadorListado.setBackgroundColor(i_set_val);
				}
			};
			cambiarColor(buscadorListado_barcolor);

		} else if (params.color == 'amarillo') {
			var iconoCerrar_estilo = 'color_amarillo';

			if (typeof WPATH != 'undefined') {
				var _tmp_a4w = require(WPATH('a4w'));
			} else {
				var _tmp_a4w = require('a4w');
			}
			if ((typeof iconoCerrar_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (iconoCerrar_estilo in _tmp_a4w.styles['classes'])) {
				try {
					iconoCerrar_estilo = _tmp_a4w.styles['classes'][iconoCerrar_estilo]['color'];
				} catch (st_val_err) {}
			}
			$.iconoCerrar.setColor(iconoCerrar_estilo);

			var buscadorListado_barcolor = 'color_amarillo';

			var cambiarColor = function(i__valor) {
				var _tmp_a4w = require(WPATH('a4w'));
				var i_set_val = i__valor;
				if ((typeof i_set_val == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (i_set_val in _tmp_a4w.styles['classes'])) {
					try {
						i_set_val = _tmp_a4w.styles['classes'][i_set_val]['color'];
					} catch (st_val_err) {}
				}
				_tmp_a4w = null;
				if (OS_IOS) {
					$.buscadorListado.setBarColor(i_set_val);
				} else {
					$.buscadorListado.setBackgroundColor(i_set_val);
				}
			};
			cambiarColor(buscadorListado_barcolor);

		} else if (params.color == 'morado') {
			var iconoCerrar_estilo = 'color_morado';

			if (typeof WPATH != 'undefined') {
				var _tmp_a4w = require(WPATH('a4w'));
			} else {
				var _tmp_a4w = require('a4w');
			}
			if ((typeof iconoCerrar_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (iconoCerrar_estilo in _tmp_a4w.styles['classes'])) {
				try {
					iconoCerrar_estilo = _tmp_a4w.styles['classes'][iconoCerrar_estilo]['color'];
				} catch (st_val_err) {}
			}
			$.iconoCerrar.setColor(iconoCerrar_estilo);

			var buscadorListado_barcolor = 'fondomorado';

			var cambiarColor = function(i__valor) {
				var _tmp_a4w = require(WPATH('a4w'));
				var i_set_val = i__valor;
				if ((typeof i_set_val == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (i_set_val in _tmp_a4w.styles['classes'])) {
					try {
						i_set_val = _tmp_a4w.styles['classes'][i_set_val]['color'];
					} catch (st_val_err) {}
				}
				_tmp_a4w = null;
				if (OS_IOS) {
					$.buscadorListado.setBarColor(i_set_val);
				} else {
					$.buscadorListado.setBackgroundColor(i_set_val);
				}
			};
			cambiarColor(buscadorListado_barcolor);

		} else if (params.color == 'rosado') {
			var iconoCerrar_estilo = 'color_rosado';

			if (typeof WPATH != 'undefined') {
				var _tmp_a4w = require(WPATH('a4w'));
			} else {
				var _tmp_a4w = require('a4w');
			}
			if ((typeof iconoCerrar_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (iconoCerrar_estilo in _tmp_a4w.styles['classes'])) {
				try {
					iconoCerrar_estilo = _tmp_a4w.styles['classes'][iconoCerrar_estilo]['color'];
				} catch (st_val_err) {}
			}
			$.iconoCerrar.setColor(iconoCerrar_estilo);

			var buscadorListado_barcolor = 'fondorosado';

			var cambiarColor = function(i__valor) {
				var _tmp_a4w = require(WPATH('a4w'));
				var i_set_val = i__valor;
				if ((typeof i_set_val == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (i_set_val in _tmp_a4w.styles['classes'])) {
					try {
						i_set_val = _tmp_a4w.styles['classes'][i_set_val]['color'];
					} catch (st_val_err) {}
				}
				_tmp_a4w = null;
				if (OS_IOS) {
					$.buscadorListado.setBarColor(i_set_val);
				} else {
					$.buscadorListado.setBackgroundColor(i_set_val);
				}
			};
			cambiarColor(buscadorListado_barcolor);

		} else if (params.color == 'celeste') {
			var iconoCerrar_estilo = 'color_celeste';

			if (typeof WPATH != 'undefined') {
				var _tmp_a4w = require(WPATH('a4w'));
			} else {
				var _tmp_a4w = require('a4w');
			}
			if ((typeof iconoCerrar_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (iconoCerrar_estilo in _tmp_a4w.styles['classes'])) {
				try {
					iconoCerrar_estilo = _tmp_a4w.styles['classes'][iconoCerrar_estilo]['color'];
				} catch (st_val_err) {}
			}
			$.iconoCerrar.setColor(iconoCerrar_estilo);

			var buscadorListado_barcolor = 'fondoceleste';

			var cambiarColor = function(i__valor) {
				var _tmp_a4w = require(WPATH('a4w'));
				var i_set_val = i__valor;
				if ((typeof i_set_val == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (i_set_val in _tmp_a4w.styles['classes'])) {
					try {
						i_set_val = _tmp_a4w.styles['classes'][i_set_val]['color'];
					} catch (st_val_err) {}
				}
				_tmp_a4w = null;
				if (OS_IOS) {
					$.buscadorListado.setBarColor(i_set_val);
				} else {
					$.buscadorListado.setBackgroundColor(i_set_val);
				}
			};
			cambiarColor(buscadorListado_barcolor);

		} else if (params.color == 'cafe') {
			var iconoCerrar_estilo = 'color_cafe';

			if (typeof WPATH != 'undefined') {
				var _tmp_a4w = require(WPATH('a4w'));
			} else {
				var _tmp_a4w = require('a4w');
			}
			if ((typeof iconoCerrar_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (iconoCerrar_estilo in _tmp_a4w.styles['classes'])) {
				try {
					iconoCerrar_estilo = _tmp_a4w.styles['classes'][iconoCerrar_estilo]['color'];
				} catch (st_val_err) {}
			}
			$.iconoCerrar.setColor(iconoCerrar_estilo);

			var buscadorListado_barcolor = 'color_cafe';

			var cambiarColor = function(i__valor) {
				var _tmp_a4w = require(WPATH('a4w'));
				var i_set_val = i__valor;
				if ((typeof i_set_val == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (i_set_val in _tmp_a4w.styles['classes'])) {
					try {
						i_set_val = _tmp_a4w.styles['classes'][i_set_val]['color'];
					} catch (st_val_err) {}
				}
				_tmp_a4w = null;
				if (OS_IOS) {
					$.buscadorListado.setBarColor(i_set_val);
				} else {
					$.buscadorListado.setBackgroundColor(i_set_val);
				}
			};
			cambiarColor(buscadorListado_barcolor);

		} else if (params.color == 'verde') {
			var iconoCerrar_estilo = 'color_verde';

			if (typeof WPATH != 'undefined') {
				var _tmp_a4w = require(WPATH('a4w'));
			} else {
				var _tmp_a4w = require('a4w');
			}
			if ((typeof iconoCerrar_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (iconoCerrar_estilo in _tmp_a4w.styles['classes'])) {
				try {
					iconoCerrar_estilo = _tmp_a4w.styles['classes'][iconoCerrar_estilo]['color'];
				} catch (st_val_err) {}
			}
			$.iconoCerrar.setColor(iconoCerrar_estilo);

			var buscadorListado_barcolor = 'fondoverde';

			var cambiarColor = function(i__valor) {
				var _tmp_a4w = require(WPATH('a4w'));
				var i_set_val = i__valor;
				if ((typeof i_set_val == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (i_set_val in _tmp_a4w.styles['classes'])) {
					try {
						i_set_val = _tmp_a4w.styles['classes'][i_set_val]['color'];
					} catch (st_val_err) {}
				}
				_tmp_a4w = null;
				if (OS_IOS) {
					$.buscadorListado.setBarColor(i_set_val);
				} else {
					$.buscadorListado.setBackgroundColor(i_set_val);
				}
			};
			cambiarColor(buscadorListado_barcolor);

		}
	}
	var elcolor;
	elcolor = $.iconoCerrar.getColor();

	var ID_1476068402_func = function() {
		var data = ('data' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['data'] : '';
		if (Ti.App.deployType != 'production') console.log('ordenando data de modal', {});

		data = _.sortBy(data, function(item) {
			return item.valor;
		})
		if (Ti.App.deployType != 'production') console.log('agregando a listado', {});
		var arrayItems = [];
		_.each(data, function(__currItem, __currPos, __currList) {
			arrayItems.push({
				Label2: {
					text: __currItem.valor,
					_data: __currItem
				},
				template: 'elemento',
				properties: {
					searchableText: __currItem.valor
				},
				vista6: {}

			});
		});
		$.seccionListadoRef.setItems(arrayItems);
		arrayItems = null;
		if (Ti.App.deployType != 'production') console.log('fin agregando items a modal', {});
	};
	var ID_1476068402 = setTimeout(ID_1476068402_func, 1000 * 0.05);
})();

if (OS_IOS || OS_ANDROID) {
	$.MODAL.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this widget controller
		try {
			//require(WPATH('vars'))[args.__id]=null;
			args = null;
			if (OS_ANDROID) {
				abx = null;
			}
			if ($item) $item = null;
			if (_my_events) {
				for (_ev_tmp in _my_events) {
					try {
						if (_ev_tmp.indexOf('_web') != -1) {
							Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
						} else {
							Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
						}
					} catch (err10) {}
				}
				_my_events = null;
				//delete _my_events;
			}
		} catch (err10) {}
		if (_out_vars) {
			var _ev_tmp;
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			_out_vars = null;
			//delete _out_vars;
		}
	});
}