/** 
 * Widget Mono-Personaje
 * Muestra diferentes tipos de personajes con un mensaje de texto personalizable
 * 
 * @param visible Indica visibilidad de widget
 * @param tipo Indica tipo de personaje: tip, info, sorry 
 */
var _bind4section = {};

var args = arguments[0] || {};

/** 
 * Funcion que inicializa widget 
 */

$.init = function(params) {
	for (var tobe in params) args[tobe] = params[tobe];
	if ('__id' in params) require(WPATH('vars'))[params.__id] = {};
	require(WPATH('vars'))[args.__id]['visible'] = 'true';
	if (!_.isUndefined(params.visible)) {
		require(WPATH('vars'))[args.__id]['visible'] = params.visible;
	}
	var visible = ('visible' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['visible'] : '';
	if (!_.isUndefined(params.tipo)) {
		if (params.tipo == '_tip') {
			require(WPATH('vars'))[args.__id]['alto'] = 120;
			require(WPATH('vars'))[args.__id]['tipo'] = '_tip';
			var vista_visible = visible;

			if (vista_visible == 'si') {
				vista_visible = true;
			} else if (vista_visible == 'no') {
				vista_visible = false;
			}
			$.vista.setVisible(vista_visible);

			var vista_alto = 120;

			if (vista_alto == '*') {
				vista_alto = Ti.UI.FILL;
			} else if (vista_alto == '-') {
				vista_alto = Ti.UI.SIZE;
			} else if (!isNaN(vista_alto)) {
				vista_alto = vista_alto + 'dp';
			}
			$.vista.setHeight(vista_alto);

			var vista3_visible = false;

			if (vista3_visible == 'si') {
				vista3_visible = true;
			} else if (vista3_visible == 'no') {
				vista3_visible = false;
			}
			$.vista3.setVisible(vista3_visible);

			var vista3_alto = 0;

			if (vista3_alto == '*') {
				vista3_alto = Ti.UI.FILL;
			} else if (vista3_alto == '-') {
				vista3_alto = Ti.UI.SIZE;
			} else if (!isNaN(vista3_alto)) {
				vista3_alto = vista3_alto + 'dp';
			}
			$.vista3.setHeight(vista3_alto);

			var vista5_visible = false;

			if (vista5_visible == 'si') {
				vista5_visible = true;
			} else if (vista5_visible == 'no') {
				vista5_visible = false;
			}
			$.vista5.setVisible(vista5_visible);

			var vista5_alto = 0;

			if (vista5_alto == '*') {
				vista5_alto = Ti.UI.FILL;
			} else if (vista5_alto == '-') {
				vista5_alto = Ti.UI.SIZE;
			} else if (!isNaN(vista5_alto)) {
				vista5_alto = vista5_alto + 'dp';
			}
			$.vista5.setHeight(vista5_alto);

			if (!_.isUndefined(params.texto)) {
				$.TipNoAceptes.setText(params.texto);

			}
			if (!_.isUndefined(params.top)) {
				$.vista.setTop(params.top);

			}
			if (!_.isUndefined(params.bottom)) {
				$.vista.setBottom(params.bottom);

			}
			if (!_.isUndefined(params.ancho)) {
				var vista_ancho = params.ancho;

				if (vista_ancho == '*') {
					vista_ancho = Ti.UI.FILL;
				} else if (vista_ancho == '-') {
					vista_ancho = Ti.UI.SIZE;
				} else if (!isNaN(vista_ancho)) {
					vista_ancho = vista_ancho + 'dp';
				}
				$.vista.setWidth(vista_ancho);

			}
			if (!_.isUndefined(params.alto)) {
				var vista_alto = params.alto;

				if (vista_alto == '*') {
					vista_alto = Ti.UI.FILL;
				} else if (vista_alto == '-') {
					vista_alto = Ti.UI.SIZE;
				} else if (!isNaN(vista_alto)) {
					vista_alto = vista_alto + 'dp';
				}
				$.vista.setHeight(vista_alto);

				require(WPATH('vars'))[args.__id]['alto'] = params.alto;
			}
			if (!_.isUndefined(params.titulo)) {
				$.ESPERATULLAMADA.setText(params.titulo);

				var ESPERATULLAMADA_visible = true;

				if (ESPERATULLAMADA_visible == 'si') {
					ESPERATULLAMADA_visible = true;
				} else if (ESPERATULLAMADA_visible == 'no') {
					ESPERATULLAMADA_visible = false;
				}
				$.ESPERATULLAMADA.setVisible(ESPERATULLAMADA_visible);

			}
		} else if (params.tipo == '_sorry') {
			require(WPATH('vars'))[args.__id]['tipo'] = '_sorry';
			require(WPATH('vars'))[args.__id]['alto'] = '-';
			var vista3_visible = visible;

			if (vista3_visible == 'si') {
				vista3_visible = true;
			} else if (vista3_visible == 'no') {
				vista3_visible = false;
			}
			$.vista3.setVisible(vista3_visible);

			var vista3_alto = '-';

			if (vista3_alto == '*') {
				vista3_alto = Ti.UI.FILL;
			} else if (vista3_alto == '-') {
				vista3_alto = Ti.UI.SIZE;
			} else if (!isNaN(vista3_alto)) {
				vista3_alto = vista3_alto + 'dp';
			}
			$.vista3.setHeight(vista3_alto);

			var vista_visible = false;

			if (vista_visible == 'si') {
				vista_visible = true;
			} else if (vista_visible == 'no') {
				vista_visible = false;
			}
			$.vista.setVisible(vista_visible);

			var vista_alto = 0;

			if (vista_alto == '*') {
				vista_alto = Ti.UI.FILL;
			} else if (vista_alto == '-') {
				vista_alto = Ti.UI.SIZE;
			} else if (!isNaN(vista_alto)) {
				vista_alto = vista_alto + 'dp';
			}
			$.vista.setHeight(vista_alto);

			var vista5_visible = false;

			if (vista5_visible == 'si') {
				vista5_visible = true;
			} else if (vista5_visible == 'no') {
				vista5_visible = false;
			}
			$.vista5.setVisible(vista5_visible);

			var vista5_alto = 0;

			if (vista5_alto == '*') {
				vista5_alto = Ti.UI.FILL;
			} else if (vista5_alto == '-') {
				vista5_alto = Ti.UI.SIZE;
			} else if (!isNaN(vista5_alto)) {
				vista5_alto = vista5_alto + 'dp';
			}
			$.vista5.setHeight(vista5_alto);

			if (!_.isUndefined(params.titulo)) {
				$.NOTIENESTAREAS.setText(params.titulo);

			}
			if (!_.isUndefined(params.texto)) {
				$.Aseguratede.setText(params.texto);

			}
			if (!_.isUndefined(params.top)) {
				$.vista3.setTop(params.top);

			}
			if (!_.isUndefined(params.bottom)) {
				$.vista3.setBottom(params.bottom);

			}
			if (!_.isUndefined(params.ancho)) {
				var vista3_ancho = params.ancho;

				if (vista3_ancho == '*') {
					vista3_ancho = Ti.UI.FILL;
				} else if (vista3_ancho == '-') {
					vista3_ancho = Ti.UI.SIZE;
				} else if (!isNaN(vista3_ancho)) {
					vista3_ancho = vista3_ancho + 'dp';
				}
				$.vista3.setWidth(vista3_ancho);

			}
			if (!_.isUndefined(params.alto)) {
				var vista3_alto = params.alto;

				if (vista3_alto == '*') {
					vista3_alto = Ti.UI.FILL;
				} else if (vista3_alto == '-') {
					vista3_alto = Ti.UI.SIZE;
				} else if (!isNaN(vista3_alto)) {
					vista3_alto = vista3_alto + 'dp';
				}
				$.vista3.setHeight(vista3_alto);

				require(WPATH('vars'))[args.__id]['alto'] = params.alto;
			}
		} else if (params.tipo == '_info') {
			require(WPATH('vars'))[args.__id]['tipo'] = '_info';
			require(WPATH('vars'))[args.__id]['alto'] = '-';
			var vista5_visible = visible;

			if (vista5_visible == 'si') {
				vista5_visible = true;
			} else if (vista5_visible == 'no') {
				vista5_visible = false;
			}
			$.vista5.setVisible(vista5_visible);

			var vista5_alto = '-';

			if (vista5_alto == '*') {
				vista5_alto = Ti.UI.FILL;
			} else if (vista5_alto == '-') {
				vista5_alto = Ti.UI.SIZE;
			} else if (!isNaN(vista5_alto)) {
				vista5_alto = vista5_alto + 'dp';
			}
			$.vista5.setHeight(vista5_alto);

			var vista_visible = false;

			if (vista_visible == 'si') {
				vista_visible = true;
			} else if (vista_visible == 'no') {
				vista_visible = false;
			}
			$.vista.setVisible(vista_visible);

			var vista_alto = 0;

			if (vista_alto == '*') {
				vista_alto = Ti.UI.FILL;
			} else if (vista_alto == '-') {
				vista_alto = Ti.UI.SIZE;
			} else if (!isNaN(vista_alto)) {
				vista_alto = vista_alto + 'dp';
			}
			$.vista.setHeight(vista_alto);

			var vista3_visible = false;

			if (vista3_visible == 'si') {
				vista3_visible = true;
			} else if (vista3_visible == 'no') {
				vista3_visible = false;
			}
			$.vista3.setVisible(vista3_visible);

			var vista3_alto = 0;

			if (vista3_alto == '*') {
				vista3_alto = Ti.UI.FILL;
			} else if (vista3_alto == '-') {
				vista3_alto = Ti.UI.SIZE;
			} else if (!isNaN(vista3_alto)) {
				vista3_alto = vista3_alto + 'dp';
			}
			$.vista3.setHeight(vista3_alto);

			if (!_.isUndefined(params.titulo)) {
				$.PUEDECONTINUAR.setText(params.titulo);

			}
			if (!_.isUndefined(params.texto)) {
				$.SiEstel.setText(params.texto);

			}
			if (!_.isUndefined(params.top)) {
				$.vista5.setTop(params.top);

			}
			if (!_.isUndefined(params.bottom)) {
				$.vista5.setBottom(params.bottom);

			}
			if (!_.isUndefined(params.ancho)) {
				var vista5_ancho = params.ancho;

				if (vista5_ancho == '*') {
					vista5_ancho = Ti.UI.FILL;
				} else if (vista5_ancho == '-') {
					vista5_ancho = Ti.UI.SIZE;
				} else if (!isNaN(vista5_ancho)) {
					vista5_ancho = vista5_ancho + 'dp';
				}
				$.vista5.setWidth(vista5_ancho);

			}
			if (!_.isUndefined(params.alto)) {
				var vista5_alto = params.alto;

				if (vista5_alto == '*') {
					vista5_alto = Ti.UI.FILL;
				} else if (vista5_alto == '-') {
					vista5_alto = Ti.UI.SIZE;
				} else if (!isNaN(vista5_alto)) {
					vista5_alto = vista5_alto + 'dp';
				}
				$.vista5.setHeight(vista5_alto);

				require(WPATH('vars'))[args.__id]['alto'] = params.alto;
			}
		}
	}
};

/** 
 * Funcion que actualiza texto y/o personaje
 * 
 * @param visible Indica si mostrar o no este widget
 * @param tipo Tipo de personaje: tip, info, sorry 
 */

$.update = function(params) {
	if (!_.isUndefined(params.tipo)) {
		if (params.tipo == '_tip') {
			require(WPATH('vars'))[args.__id]['tipo'] = '_tip';
		} else if (params.tipo == '_sorry') {
			require(WPATH('vars'))[args.__id]['tipo'] = '_sorry';
		} else if (params.tipo == '_info') {
			require(WPATH('vars'))[args.__id]['tipo'] = '_info';
		}
	}
	if (!_.isUndefined(params.visible)) {
		require(WPATH('vars'))[args.__id]['visible'] = params.visible;
	}
	var tipo = ('tipo' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['tipo'] : '';
	var visible = ('visible' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['visible'] : '';
	var alto = ('alto' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['alto'] : '';
	if (tipo == '_tip') {
		var vista_visible = visible;

		if (vista_visible == 'si') {
			vista_visible = true;
		} else if (vista_visible == 'no') {
			vista_visible = false;
		}
		$.vista.setVisible(vista_visible);

		var vista_alto = alto;

		if (vista_alto == '*') {
			vista_alto = Ti.UI.FILL;
		} else if (vista_alto == '-') {
			vista_alto = Ti.UI.SIZE;
		} else if (!isNaN(vista_alto)) {
			vista_alto = vista_alto + 'dp';
		}
		$.vista.setHeight(vista_alto);

		var vista3_visible = false;

		if (vista3_visible == 'si') {
			vista3_visible = true;
		} else if (vista3_visible == 'no') {
			vista3_visible = false;
		}
		$.vista3.setVisible(vista3_visible);

		var vista3_alto = 0;

		if (vista3_alto == '*') {
			vista3_alto = Ti.UI.FILL;
		} else if (vista3_alto == '-') {
			vista3_alto = Ti.UI.SIZE;
		} else if (!isNaN(vista3_alto)) {
			vista3_alto = vista3_alto + 'dp';
		}
		$.vista3.setHeight(vista3_alto);

		var vista5_visible = false;

		if (vista5_visible == 'si') {
			vista5_visible = true;
		} else if (vista5_visible == 'no') {
			vista5_visible = false;
		}
		$.vista5.setVisible(vista5_visible);

		var vista5_alto = 0;

		if (vista5_alto == '*') {
			vista5_alto = Ti.UI.FILL;
		} else if (vista5_alto == '-') {
			vista5_alto = Ti.UI.SIZE;
		} else if (!isNaN(vista5_alto)) {
			vista5_alto = vista5_alto + 'dp';
		}
		$.vista5.setHeight(vista5_alto);

		if (!_.isUndefined(params.texto)) {
			$.TipNoAceptes.setText(params.texto);

		}
		if (!_.isUndefined(params.top)) {
			$.vista.setTop(params.top);

		}
		if (!_.isUndefined(params.bottom)) {
			$.vista.setBottom(params.bottom);

		}
		if (!_.isUndefined(params.ancho)) {
			var vista_ancho = params.ancho;

			if (vista_ancho == '*') {
				vista_ancho = Ti.UI.FILL;
			} else if (vista_ancho == '-') {
				vista_ancho = Ti.UI.SIZE;
			} else if (!isNaN(vista_ancho)) {
				vista_ancho = vista_ancho + 'dp';
			}
			$.vista.setWidth(vista_ancho);

		}
		if (!_.isUndefined(params.alto)) {
			var vista_alto = params.alto;

			if (vista_alto == '*') {
				vista_alto = Ti.UI.FILL;
			} else if (vista_alto == '-') {
				vista_alto = Ti.UI.SIZE;
			} else if (!isNaN(vista_alto)) {
				vista_alto = vista_alto + 'dp';
			}
			$.vista.setHeight(vista_alto);

			require(WPATH('vars'))[args.__id]['alto'] = params.alto;
		}
		if (!_.isUndefined(params.titulo)) {
			if ((_.isObject(params.titulo) || (_.isString(params.titulo)) && !_.isEmpty(params.titulo)) || _.isNumber(params.titulo) || _.isBoolean(params.titulo)) {
				$.ESPERATULLAMADA.setText(params.titulo);

				var ESPERATULLAMADA_visible = true;

				if (ESPERATULLAMADA_visible == 'si') {
					ESPERATULLAMADA_visible = true;
				} else if (ESPERATULLAMADA_visible == 'no') {
					ESPERATULLAMADA_visible = false;
				}
				$.ESPERATULLAMADA.setVisible(ESPERATULLAMADA_visible);

			} else {
				$.ESPERATULLAMADA.setText(params.titulo);

				var ESPERATULLAMADA_visible = false;

				if (ESPERATULLAMADA_visible == 'si') {
					ESPERATULLAMADA_visible = true;
				} else if (ESPERATULLAMADA_visible == 'no') {
					ESPERATULLAMADA_visible = false;
				}
				$.ESPERATULLAMADA.setVisible(ESPERATULLAMADA_visible);

			}
		}
	} else if (tipo == '_sorry') {
		var vista3_visible = visible;

		if (vista3_visible == 'si') {
			vista3_visible = true;
		} else if (vista3_visible == 'no') {
			vista3_visible = false;
		}
		$.vista3.setVisible(vista3_visible);

		var vista3_alto = alto;

		if (vista3_alto == '*') {
			vista3_alto = Ti.UI.FILL;
		} else if (vista3_alto == '-') {
			vista3_alto = Ti.UI.SIZE;
		} else if (!isNaN(vista3_alto)) {
			vista3_alto = vista3_alto + 'dp';
		}
		$.vista3.setHeight(vista3_alto);

		var vista_visible = false;

		if (vista_visible == 'si') {
			vista_visible = true;
		} else if (vista_visible == 'no') {
			vista_visible = false;
		}
		$.vista.setVisible(vista_visible);

		var vista_alto = 0;

		if (vista_alto == '*') {
			vista_alto = Ti.UI.FILL;
		} else if (vista_alto == '-') {
			vista_alto = Ti.UI.SIZE;
		} else if (!isNaN(vista_alto)) {
			vista_alto = vista_alto + 'dp';
		}
		$.vista.setHeight(vista_alto);

		var vista5_visible = false;

		if (vista5_visible == 'si') {
			vista5_visible = true;
		} else if (vista5_visible == 'no') {
			vista5_visible = false;
		}
		$.vista5.setVisible(vista5_visible);

		var vista5_alto = 0;

		if (vista5_alto == '*') {
			vista5_alto = Ti.UI.FILL;
		} else if (vista5_alto == '-') {
			vista5_alto = Ti.UI.SIZE;
		} else if (!isNaN(vista5_alto)) {
			vista5_alto = vista5_alto + 'dp';
		}
		$.vista5.setHeight(vista5_alto);

		if (!_.isUndefined(params.titulo)) {
			$.NOTIENESTAREAS.setText(params.titulo);

		}
		if (!_.isUndefined(params.texto)) {
			$.Aseguratede.setText(params.texto);

		}
		if (!_.isUndefined(params.top)) {
			$.vista3.setTop(params.top);

		}
		if (!_.isUndefined(params.bottom)) {
			$.vista3.setBottom(params.bottom);

		}
		if (!_.isUndefined(params.ancho)) {
			var vista3_ancho = params.ancho;

			if (vista3_ancho == '*') {
				vista3_ancho = Ti.UI.FILL;
			} else if (vista3_ancho == '-') {
				vista3_ancho = Ti.UI.SIZE;
			} else if (!isNaN(vista3_ancho)) {
				vista3_ancho = vista3_ancho + 'dp';
			}
			$.vista3.setWidth(vista3_ancho);

		}
		if (!_.isUndefined(params.alto)) {
			var vista3_alto = params.alto;

			if (vista3_alto == '*') {
				vista3_alto = Ti.UI.FILL;
			} else if (vista3_alto == '-') {
				vista3_alto = Ti.UI.SIZE;
			} else if (!isNaN(vista3_alto)) {
				vista3_alto = vista3_alto + 'dp';
			}
			$.vista3.setHeight(vista3_alto);

			require(WPATH('vars'))[args.__id]['alto'] = params.alto;
		}
	} else if (tipo == '_info') {
		var vista5_visible = visible;

		if (vista5_visible == 'si') {
			vista5_visible = true;
		} else if (vista5_visible == 'no') {
			vista5_visible = false;
		}
		$.vista5.setVisible(vista5_visible);

		var vista5_alto = alto;

		if (vista5_alto == '*') {
			vista5_alto = Ti.UI.FILL;
		} else if (vista5_alto == '-') {
			vista5_alto = Ti.UI.SIZE;
		} else if (!isNaN(vista5_alto)) {
			vista5_alto = vista5_alto + 'dp';
		}
		$.vista5.setHeight(vista5_alto);

		var vista_visible = false;

		if (vista_visible == 'si') {
			vista_visible = true;
		} else if (vista_visible == 'no') {
			vista_visible = false;
		}
		$.vista.setVisible(vista_visible);

		var vista_alto = 0;

		if (vista_alto == '*') {
			vista_alto = Ti.UI.FILL;
		} else if (vista_alto == '-') {
			vista_alto = Ti.UI.SIZE;
		} else if (!isNaN(vista_alto)) {
			vista_alto = vista_alto + 'dp';
		}
		$.vista.setHeight(vista_alto);

		var vista3_visible = false;

		if (vista3_visible == 'si') {
			vista3_visible = true;
		} else if (vista3_visible == 'no') {
			vista3_visible = false;
		}
		$.vista3.setVisible(vista3_visible);

		var vista3_alto = 0;

		if (vista3_alto == '*') {
			vista3_alto = Ti.UI.FILL;
		} else if (vista3_alto == '-') {
			vista3_alto = Ti.UI.SIZE;
		} else if (!isNaN(vista3_alto)) {
			vista3_alto = vista3_alto + 'dp';
		}
		$.vista3.setHeight(vista3_alto);

		if (!_.isUndefined(params.titulo)) {
			$.PUEDECONTINUAR.setText(params.titulo);

		}
		if (!_.isUndefined(params.texto)) {
			$.SiEstel.setText(params.texto);

		}
		if (!_.isUndefined(params.top)) {
			$.vista5.setTop(params.top);

		}
		if (!_.isUndefined(params.bottom)) {
			$.vista5.setBottom(params.bottom);

		}
		if (!_.isUndefined(params.ancho)) {
			var vista5_ancho = params.ancho;

			if (vista5_ancho == '*') {
				vista5_ancho = Ti.UI.FILL;
			} else if (vista5_ancho == '-') {
				vista5_ancho = Ti.UI.SIZE;
			} else if (!isNaN(vista5_ancho)) {
				vista5_ancho = vista5_ancho + 'dp';
			}
			$.vista5.setWidth(vista5_ancho);

		}
		if (!_.isUndefined(params.alto)) {
			var vista5_alto = params.alto;

			if (vista5_alto == '*') {
				vista5_alto = Ti.UI.FILL;
			} else if (vista5_alto == '-') {
				vista5_alto = Ti.UI.SIZE;
			} else if (!isNaN(vista5_alto)) {
				vista5_alto = vista5_alto + 'dp';
			}
			$.vista5.setHeight(vista5_alto);

			require(WPATH('vars'))[args.__id]['alto'] = params.alto;
		}
	}
};

/** 
 * Funcion que oculta widget 
 */

$.ocultar = function(params) {
	var vista_visible = false;

	if (vista_visible == 'si') {
		vista_visible = true;
	} else if (vista_visible == 'no') {
		vista_visible = false;
	}
	$.vista.setVisible(vista_visible);

	var vista3_visible = false;

	if (vista3_visible == 'si') {
		vista3_visible = true;
	} else if (vista3_visible == 'no') {
		vista3_visible = false;
	}
	$.vista3.setVisible(vista3_visible);

	var vista5_visible = false;

	if (vista5_visible == 'si') {
		vista5_visible = true;
	} else if (vista5_visible == 'no') {
		vista5_visible = false;
	}
	$.vista5.setVisible(vista5_visible);

};

/** 
 * Funcion que muestra widget 
 */

$.mostrar = function(params) {
	var tipo = ('tipo' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['tipo'] : '';
	var visible = ('visible' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['visible'] : '';
	if (tipo == '_tip') {
		var vista_visible = visible;

		if (vista_visible == 'si') {
			vista_visible = true;
		} else if (vista_visible == 'no') {
			vista_visible = false;
		}
		$.vista.setVisible(vista_visible);

		var vista3_visible = false;

		if (vista3_visible == 'si') {
			vista3_visible = true;
		} else if (vista3_visible == 'no') {
			vista3_visible = false;
		}
		$.vista3.setVisible(vista3_visible);

	} else if (tipo == '_sorry') {
		var vista_visible = false;

		if (vista_visible == 'si') {
			vista_visible = true;
		} else if (vista_visible == 'no') {
			vista_visible = false;
		}
		$.vista.setVisible(vista_visible);

		var vista3_visible = visible;

		if (vista3_visible == 'si') {
			vista3_visible = true;
		} else if (vista3_visible == 'no') {
			vista3_visible = false;
		}
		$.vista3.setVisible(vista3_visible);

	} else if (tipo == '_info') {
		var vista_visible = false;

		if (vista_visible == 'si') {
			vista_visible = true;
		} else if (vista_visible == 'no') {
			vista_visible = false;
		}
		$.vista.setVisible(vista_visible);

		var vista3_visible = false;

		if (vista3_visible == 'si') {
			vista3_visible = true;
		} else if (vista3_visible == 'no') {
			vista3_visible = false;
		}
		$.vista3.setVisible(vista3_visible);

		var vista5_visible = visible;

		if (vista5_visible == 'si') {
			vista5_visible = true;
		} else if (vista5_visible == 'no') {
			vista5_visible = false;
		}
		$.vista5.setVisible(vista5_visible);

	}
};