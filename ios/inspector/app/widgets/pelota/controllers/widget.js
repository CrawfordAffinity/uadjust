/** 
 * Widget Pelota Dias
 * Este widget tiene como objetivo mostrar un boton en forma de circulo que se activa y desactiva al ser pulsado para representar un dia de la semana.
 * 
 * @param letra Representa la letra a mostrar dentro del circulo
 * @param radio Indica el ancho del radio en pixeles
 * @param borde Indica el grosor del borde en pixeles
 * @param estado True indica que estado inicial es prendido, false en caso contrario
 * @param on Alias de estado 
 */
var _bind4section = {};

var args = arguments[0] || {};

function Touchend_vista(e) {
	/** 
	 * este evento se ejecuta cuando dejamos de presionar el boton 
	 */

	e.cancelBubble = true;
	var elemento = e.source;
	var on = ('on' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['on'] : '';
	if (on == true || on == 'true') {
		/** 
		 * verifiuca que el valor de la variable &quot;on&quot; sea &quot;true&quot; 
		 */
		var vista_estilo = 'fondoplomo';

		var setEstilo = function(clase) {
			if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
				try {
					$.vista.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
				} catch (sete_err) {}
			}
		};
		setEstilo(vista_estilo);

		var label_estilo = 'estilo11';

		if (typeof WPATH != 'undefined') {
			var _tmp_a4w = require(WPATH('a4w'));
		} else {
			var _tmp_a4w = require('a4w');
		}
		if ((typeof label_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (label_estilo in _tmp_a4w.styles['classes'])) {
			try {
				label_estilo = _tmp_a4w.styles['classes'][label_estilo];
			} catch (st_val_err) {}
		}
		$.label.applyProperties(label_estilo);

		require(WPATH('vars'))[args.__id]['on'] = 'false';

		if ('__args' in args) {
			args['__args'].onoff({});
		} else {
			args.onoff({});
		}
	} else {
		var vista_estilo = 'fondoazul';

		var setEstilo = function(clase) {
			if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
				try {
					$.vista.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
				} catch (sete_err) {}
			}
		};
		setEstilo(vista_estilo);

		var label_estilo = 'estilo10';

		if (typeof WPATH != 'undefined') {
			var _tmp_a4w = require(WPATH('a4w'));
		} else {
			var _tmp_a4w = require('a4w');
		}
		if ((typeof label_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (label_estilo in _tmp_a4w.styles['classes'])) {
			try {
				label_estilo = _tmp_a4w.styles['classes'][label_estilo];
			} catch (st_val_err) {}
		}
		$.label.applyProperties(label_estilo);

		require(WPATH('vars'))[args.__id]['on'] = 'true';

		if ('__args' in args) {
			args['__args'].onon({});
		} else {
			args.onon({});
		}
	}
}
/** 
 * Funcion que inicializa widget 
 */

$.init = function(params) {
	for (var tobe in params) args[tobe] = params[tobe];
	if ('__id' in params) require(WPATH('vars'))[params.__id] = {};
	/** 
	 * Definimos los valores por default 
	 */
	require(WPATH('vars'))[args.__id]['borde'] = 2;
	/** 
	 * Definimos los valores por default 
	 */
	require(WPATH('vars'))[args.__id]['on'] = 'false';
	/** 
	 * Definimos los valores por default 
	 */
	require(WPATH('vars'))[args.__id]['radio'] = 12;
	if (!_.isUndefined(params.letra)) {
		/** 
		 * verifica si el argumento params.letra existe 
		 */
		$.label.setText(params.letra);

	}
	if (!_.isUndefined(params.radio)) {
		/** 
		 * verifica si el argumento params.radio existe 
		 */
		require(WPATH('vars'))[args.__id]['radio'] = params.radio;
	}
	if (!_.isUndefined(params.borde)) {
		/** 
		 * verifica si el argumento params.borde existe 
		 */
		require(WPATH('vars'))[args.__id]['borde'] = params.borde;
	}
	if (!_.isUndefined(params.estado)) {
		/** 
		 * verifica si el argumento params.estado existe 
		 */
		require(WPATH('vars'))[args.__id]['on'] = params.estado;
	}
	if (!_.isUndefined(params.on)) {
		/** 
		 * verifica si el argumento params.on existe 
		 */
		require(WPATH('vars'))[args.__id]['on'] = params.on;
	}
	var borde = ('borde' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['borde'] : '';
	$.vista.setLeft(borde);

	$.vista.setRight(borde);

	var on = ('on' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['on'] : '';
	if (on == true || on == 'true') {
		/** 
		 * Verifica si el valor de la variable &quot;on&quot; sea &quot;true&quot; 
		 */
		var vista_estilo = 'fondoazul';

		var setEstilo = function(clase) {
			if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
				try {
					$.vista.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
				} catch (sete_err) {}
			}
		};
		setEstilo(vista_estilo);

		var label_estilo = 'estilo10';

		if (typeof WPATH != 'undefined') {
			var _tmp_a4w = require(WPATH('a4w'));
		} else {
			var _tmp_a4w = require('a4w');
		}
		if ((typeof label_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (label_estilo in _tmp_a4w.styles['classes'])) {
			try {
				label_estilo = _tmp_a4w.styles['classes'][label_estilo];
			} catch (st_val_err) {}
		}
		$.label.applyProperties(label_estilo);

		require(WPATH('vars'))[args.__id]['on'] = 'false';
	}
	var radio = ('radio' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['radio'] : '';
	/** 
	 * creamos y calculamos valor de variables ancho_final y radio_final en base al valor de params.radio y params.borde. 
	 */

	var ancho_final = Ti.Platform.displayCaps.platformWidth * (parseInt(radio) / 100) - parseInt(borde);
	var radio_final = ancho_final / 2
	var vista_ancho = ancho_final;

	if (vista_ancho == '*') {
		vista_ancho = Ti.UI.FILL;
	} else if (vista_ancho == '-') {
		vista_ancho = Ti.UI.SIZE;
	} else if (!isNaN(vista_ancho)) {
		vista_ancho = vista_ancho + 'dp';
	}
	$.vista.setWidth(vista_ancho);

	var vista_alto = ancho_final;

	if (vista_alto == '*') {
		vista_alto = Ti.UI.FILL;
	} else if (vista_alto == '-') {
		vista_alto = Ti.UI.SIZE;
	} else if (!isNaN(vista_alto)) {
		vista_alto = vista_alto + 'dp';
	}
	$.vista.setHeight(vista_alto);

	$.vista.setBorderRadius(radio_final);

};

/** 
 * Funcion que prende o apaga el estado del boton
 * 
 * @param on Si true, boton se muestra encendido, false en caso contrario 
 */

$.set = function(params) {
	if (!_.isUndefined(params.on)) {
		/** 
		 * verifica si params.on existe 
		 */
		if (params.on == true || params.on == 'true') {
			/** 
			 * verifica si el valor de params.on es &quot;true&quot; 
			 */
			var vista_estilo = 'fondoazul';

			var setEstilo = function(clase) {
				if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
					try {
						$.vista.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
					} catch (sete_err) {}
				}
			};
			setEstilo(vista_estilo);

			var label_estilo = 'estilo10';

			if (typeof WPATH != 'undefined') {
				var _tmp_a4w = require(WPATH('a4w'));
			} else {
				var _tmp_a4w = require('a4w');
			}
			if ((typeof label_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (label_estilo in _tmp_a4w.styles['classes'])) {
				try {
					label_estilo = _tmp_a4w.styles['classes'][label_estilo];
				} catch (st_val_err) {}
			}
			$.label.applyProperties(label_estilo);

			require(WPATH('vars'))[args.__id]['on'] = 'true';
		} else {
			var vista_estilo = 'fondoplomo';

			var setEstilo = function(clase) {
				if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
					try {
						$.vista.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
					} catch (sete_err) {}
				}
			};
			setEstilo(vista_estilo);

			var label_estilo = 'estilo11';

			if (typeof WPATH != 'undefined') {
				var _tmp_a4w = require(WPATH('a4w'));
			} else {
				var _tmp_a4w = require('a4w');
			}
			if ((typeof label_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (label_estilo in _tmp_a4w.styles['classes'])) {
				try {
					label_estilo = _tmp_a4w.styles['classes'][label_estilo];
				} catch (st_val_err) {}
			}
			$.label.applyProperties(label_estilo);

			require(WPATH('vars'))[args.__id]['on'] = 'false';
		}
	}
};