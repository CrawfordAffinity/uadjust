exports.events = _.clone(Backbone.Events);
exports.ajaxkeys = {};
exports.variables = {};
exports.styles = {
	"images": {},
	"classes": {
		"estilo11": {
			"color": "#2d9edb",
			"font": {
				"fontWeight": "bold",
				"fontSize": "15dp"
			}
		},
		"fondoazul": {
			"backgroundColor": "#2d9edb",
			"font": {
				"fontSize": "12dp"
			}
		},
		"fondoplomo": {
			"backgroundColor": "#f7f7f7",
			"font": {
				"fontSize": "12dp"
			}
		},
		"estilo10": {
			"color": "#ffffff",
			"font": {
				"fontWeight": "bold",
				"fontSize": "16dp"
			}
		}
	}
};
exports.fontello = {};