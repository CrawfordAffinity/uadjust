Widget Pelota Dias
Este widget tiene como objetivo mostrar un boton en forma de circulo que se activa y desactiva al ser pulsado para representar un dia de la semana.

@param letra Representa la letra a mostrar dentro del circulo
@param radio Indica el ancho del radio en pixeles
@param borde Indica el grosor del borde en pixeles
@param estado True indica que estado inicial es prendido, false en caso contrario
@param on Alias de estado
