/** 
 * Widget Sin Internet
 * Vista oculta que monitorea constantemente si el equipo esta con internet, y si no lo esta, muestra una pantalla avisando tal cosa con un personaje
 * 
 * @param titulo Indica titulo de texto a mostrar
 * @param mensaje Indica texto detalle a mostrar 
 */
var _bind4section = {};

var args = arguments[0] || {};

/** 
 * Funcion que inicializa el widget 
 */

$.init = function(params) {
	for (var tobe in params) args[tobe] = params[tobe];
	if ('__id' in params) require(WPATH('vars'))[params.__id] = {};
	if (!_.isUndefined(params.titulo)) {
		/** 
		 * Revisamos si fue enviado por parametros un titulo y mensaje, ademas que revisa si existe internet para mostrar u ocultar la vista 
		 */
		$.ESTASSINCONEXION.setText(params.titulo);

	}
	if (!_.isUndefined(params.mensaje)) {
		$.NoPuedesVer.setText(params.mensaje);

	} else if (Ti.Network.networkType == Ti.Network.NETWORK_NONE) {
		var vista_visible = true;

		if (vista_visible == 'si') {
			vista_visible = true;
		} else if (vista_visible == 'no') {
			vista_visible = false;
		}
		$.vista.setVisible(vista_visible);


		if ('__args' in args) {
			args['__args'].onoff({});
		} else {
			args.onoff({});
		}
	} else {
		var vista_visible = false;

		if (vista_visible == 'si') {
			vista_visible = true;
		} else if (vista_visible == 'no') {
			vista_visible = false;
		}
		$.vista.setVisible(vista_visible);


		if ('__args' in args) {
			args['__args'].onon({});
		} else {
			args.onon({});
		}
	}
};


(function() {
	/** 
	 * Monitoreando internet, y segun el caso... mostraremos u ocultamos una vista 
	 */

	Ti.Network.addEventListener("change", function(data4) {
		var evento = {
			tipo: data4.networkTypeName,
			online: data4.online
		};
		if (evento.online == true || evento.online == 'true') {
			var vista_visible = false;

			if (vista_visible == 'si') {
				vista_visible = true;
			} else if (vista_visible == 'no') {
				vista_visible = false;
			}
			$.vista.setVisible(vista_visible);


			if ('__args' in args) {
				args['__args'].onon({});
			} else {
				args.onon({});
			}
		} else {
			var vista_visible = true;

			if (vista_visible == 'si') {
				vista_visible = true;
			} else if (vista_visible == 'no') {
				vista_visible = false;
			}
			$.vista.setVisible(vista_visible);


			if ('__args' in args) {
				args['__args'].onoff({});
			} else {
				args.onoff({});
			}
		}
	});
})();