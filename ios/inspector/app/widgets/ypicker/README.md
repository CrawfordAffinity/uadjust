Widget Selector de Anio
Representa un control para elegir anios

@param aceptar Indica el texto del boton aceptar
@param cancelar Indica el texto del boton cancelar
@param desde Indica desde que anio se puede elegir
@param hasta Indica hasta que anio se puede elegir
