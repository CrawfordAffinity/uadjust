exports.definition = {
	config: {
		columns: {
			"valor": "TEXT",
			"idinstancia": "TEXT",
			"estado": "INTEGER",
			"id": "INTEGER",
			"amostrar": "TEXT",
		},
		adapter: {
			"type": "modalmultiple/memory_adapter",
			"collection_name": "temp_multiple",
			"idAttribute": "id",
			"remoteBackup": "false",
		}
	},
	extendModel: function(Model) {
		_.extend(Model.prototype, {
			transform: function transform() {
				var transformed = this.toJSON();
				return transformed;
			}
		});
		return Model;
	},
	extendCollection: function(Collection) {
		// helper functions
		function S4() {
			return (0 | 65536 * (1 + Math.random())).toString(16).substring(1);
		}
		function guid() {
			return S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4();
		}
		// start extend
		_.extend(Collection.prototype, {
		});
		// end extend
		return Collection;
	}
};
