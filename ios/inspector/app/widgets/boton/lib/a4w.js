exports.events = _.clone(Backbone.Events);
exports.ajaxkeys = {};
exports.variables = {};
exports.styles = {
	"images": {},
	"classes": {
		"fondoverde": {
			"backgroundColor": "#8ce5bd",
			"font": {
				"fontSize": "12dp"
			}
		},
		"fondorojo": {
			"backgroundColor": "#ee7f7e",
			"font": {
				"fontSize": "12dp"
			}
		},
		"estilo10": {
			"color": "#ffffff",
			"font": {
				"fontWeight": "bold",
				"fontSize": "16dp"
			}
		},
		"fondoamarillo": {
			"backgroundColor": "#f8da54",
			"font": {
				"fontSize": "12dp"
			}
		}
	}
};
exports.fontello = {};