/** 
 * Widget Boton
 * Boton amarillo utilizado para tomar una tarea
 * 
 * @param texto Texto a definir en boton
 * @param verprogreso Si true, muestra animacion de progreso
 * @param vertexto Si true, muestra texto en boton 
 */
var _bind4section = {};

var args = arguments[0] || {};

function Click_vista(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var estado = ('estado' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['estado'] : '';

	if ('__args' in args) {
		args['__args'].onpresiono({
			estado: estado
		});
	} else {
		args.onpresiono({
			estado: estado
		});
	}

}
/** 
 * Funcion que inicializa widget 
 */

$.init = function(params) {
	for (var tobe in params) args[tobe] = params[tobe];
	if ('__id' in params) require(WPATH('vars'))[params.__id] = {};
	if (!_.isUndefined(params.texto)) {
		$.PRESIONEPARA.setText(params.texto);

	}
	if (!_.isUndefined(params.verprogreso)) {
		if (params.verprogreso == true || params.verprogreso == 'true') {
			$.progreso.show();
		} else if (params.verprogreso == false || params.verprogreso == 'false') {
			$.progreso.hide();
		}
	}
	if (!_.isUndefined(params.vertexto)) {
		var PRESIONEPARA_visible = params.vertexto;

		if (PRESIONEPARA_visible == 'si') {
			PRESIONEPARA_visible = true;
		} else if (PRESIONEPARA_visible == 'no') {
			PRESIONEPARA_visible = false;
		}
		$.PRESIONEPARA.setVisible(PRESIONEPARA_visible);

	}
	if (!_.isUndefined(params.estilo)) {
		var vista_estilo = params.estilo;

		var setEstilo = function(clase) {
			if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
				try {
					$.vista.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
				} catch (sete_err) {}
			}
		};
		setEstilo(vista_estilo);

	}
	require(WPATH('vars'))[args.__id]['estado'] = '0';
};

/** 
 * Funcion que actualiza boton
 * 
 * @param texto Modifica el texto del boton
 * @param verprogreso Si true, muestra animacion de progreso
 * @param vertexto Si true, muestra texto en boton 
 */

$.datos = function(params) {
	if (!_.isUndefined(params.texto)) {
		$.PRESIONEPARA.setText(params.texto);

	}
	if (!_.isUndefined(params.verprogreso)) {
		if (params.verprogreso == true || params.verprogreso == 'true') {
			$.progreso.show();
		} else if (params.verprogreso == false || params.verprogreso == 'false') {
			$.progreso.hide();
		}
	}
	if (!_.isUndefined(params.vertexto)) {
		var PRESIONEPARA_visible = params.vertexto;

		if (PRESIONEPARA_visible == 'si') {
			PRESIONEPARA_visible = true;
		} else if (PRESIONEPARA_visible == 'no') {
			PRESIONEPARA_visible = false;
		}
		$.PRESIONEPARA.setVisible(PRESIONEPARA_visible);

	}
	if (!_.isUndefined(params.estilo)) {
		var vista_estilo = params.estilo;

		var setEstilo = function(clase) {
			if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
				try {
					$.vista.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
				} catch (sete_err) {}
			}
		};
		setEstilo(vista_estilo);

	}
	require(WPATH('vars'))[args.__id]['estado'] = params.estado;
};