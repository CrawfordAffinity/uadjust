exports.events = _.clone(Backbone.Events);
exports.ajaxkeys = {};
exports.variables = {};
exports.styles = {
	"images": {
		"linea": "/images/iA56941C357BC902627770A6229AC94A1.png"
	},
	"classes": {
		"estilo11": {
			"color": "#2d9edb",
			"font": {
				"fontWeight": "bold",
				"fontSize": "15dp"
			}
		},
		"#imagen": {
			"image": "WPATH('images/iA56941C357BC902627770A6229AC94A1.png')"
		}
	}
};
exports.fontello = {};