/** 
 * Widget Registro Header
 * Control que muestra encabezado con titulo y progreso de registro
 * 
 * @param titulo Indica texto a mostrar en encabezado
 * @param avance Indica estado de avance de registro 
 */
var _bind4section = {};

var args = arguments[0] || {};

function Click_vista(e) {

	e.cancelBubble = true;
	var elemento = e.source;

	if ('__args' in args) {
		args['__args'].onclick({});
	} else {
		args.onclick({});
	}

}
/** 
 * Funcion que inicializa widget 
 */

$.init = function(params) {
	for (var tobe in params) args[tobe] = params[tobe];
	if ('__id' in params) require(WPATH('vars'))[params.__id] = {};
	if (!_.isUndefined(params.titulo)) {
		/** 
		 * Modificamos el titulo 
		 */
		$.PARTEPas.setText(params.titulo);

	}
	if (!_.isUndefined(params.avance)) {
		/** 
		 * Modificamos el avance 
		 */
		$.label.setText(params.avance);

	}
};