Widget Registro Header
Control que muestra encabezado con titulo y progreso de registro

@param titulo Indica texto a mostrar en encabezado
@param avance Indica estado de avance de registro
