exports.events = _.clone(Backbone.Events);
exports.ajaxkeys = {};
exports.variables = {};
exports.styles = {
	"images": {},
	"classes": {
		"fondomorado": {
			"backgroundColor": "#b9aaf3",
			"font": {
				"fontSize": "12dp"
			}
		},
		"fondoverde": {
			"backgroundColor": "#8ce5bd",
			"font": {
				"fontSize": "12dp"
			}
		},
		"fondoazul": {
			"backgroundColor": "#2d9edb",
			"font": {
				"fontSize": "12dp"
			}
		},
		"fondoceleste": {
			"backgroundColor": "#8bc9e8",
			"font": {
				"fontSize": "12dp"
			}
		},
		"fondoplomo": {
			"backgroundColor": "#f7f7f7",
			"font": {
				"fontSize": "12dp"
			}
		},
		"fondocafe": {
			"backgroundColor": "#a5876d",
			"font": {
				"fontSize": "12dp"
			}
		},
		"fondonaranjo": {
			"backgroundColor": "#fcbd83",
			"font": {
				"fontSize": "12dp"
			}
		},
		"fondorojo": {
			"backgroundColor": "#ee7f7e",
			"font": {
				"fontSize": "12dp"
			}
		},
		"fondorosado": {
			"backgroundColor": "#ffacaa",
			"font": {
				"fontSize": "12dp"
			}
		},
		"fondolila": {
			"backgroundColor": "#8383db",
			"font": {
				"fontSize": "12dp"
			}
		},
		"#iconoContinuar": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "20dp"
			}
		},
		"fondoamarillo": {
			"backgroundColor": "#f8da54",
			"font": {
				"fontSize": "12dp"
			}
		}
	}
};
exports.fontello = {
	"adjust": {
		"CODES": {
			"auto": "\uE80b",
			"entrar": "\uE81b",
			"emergencias": "\uE819",
			"registrarse": "\uE827",
			"continuar": "\uE812",
			"info": "\uE821",
			"cerrar": "\uE80e",
			"salir_insp": "\uE828",
			"hoy": "\uE820",
			"check": "\uE80f",
			"agregar_item": "\uE80a",
			"9": "\uE808",
			"girar_camara": "\uE81d",
			"llamar": "\uE822",
			"volver": "\uE82a",
			"cruz": "\uE814",
			"1": "\uE800",
			"hora": "\uE81f",
			"ciudad": "\uE810",
			"3": "\uE802",
			"flecha": "\uE82b",
			"7": "\uE806",
			"4": "\uE803",
			"perfil": "\uE817",
			"cancelar_tarea": "\uE80d",
			"6": "\uE805",
			"mistareas": "\uE823",
			"historial": "\uE81e",
			"8": "\uE807",
			"entradas": "\uE81a",
			"caso": "\uE815",
			"ubicacion": "\uE829",
			"2": "\uE801",
			"refresh": "\uE826",
			"enviar_insp": "\uE81c",
			"telefono": "\uE818",
			"nuevo_dano": "\uE824",
			"comuna": "\uE811",
			"critico": "\uE813",
			"5": "\uE804",
			"camara": "\uE80c",
			"10": "\uE809",
			"correo": "\uE816"
		},
		"POSTSCRIPT": "beta1",
		"TTF": "/Applications/CreadorOPEN/CreadorOPEN.app/Contents/MacOS/webapps/ROOT/WEB-INF/lucee/temp/_font/fontello-85cc3601/font/beta1.ttf"
	}
};