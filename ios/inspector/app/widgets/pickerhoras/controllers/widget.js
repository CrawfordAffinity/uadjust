/** 
 * Widget Picker Horas
 * Control para elegir un horario en disponibilidad
 * 
 * @param mins Si true, agrega los minutos al selector, false los omite 
 */
var _bind4section = {};

var args = arguments[0] || {};



function Change_picker(e) {

	e.cancelBubble = true;
	var elemento = e;
	var _columna = e.columnIndex;
	var columna = e.columnIndex + 1;
	var _fila = e.rowIndex;
	var fila = e.rowIndex + 1;
	/** 
	 * Obtenemos el texto del picker y guardamos en variable 
	 */
	desde_sel = elemento.selectedValue[0].value;
	/** 
	 * Obtenemos el texto del picker y guardamos en variable 
	 */
	hasta_sel = elemento.selectedValue[2].value;

	if ('__args' in args) {
		args['__args'].onchange({
			desde: desde_sel,
			hasta: hasta_sel
		});
	} else {
		args.onchange({
			desde: desde_sel,
			hasta: hasta_sel
		});
	}

}
/** 
 * Funcion para inicializar widget 
 */

$.init = function(params) {
	for (var tobe in params) args[tobe] = params[tobe];
	if ('__id' in params) require(WPATH('vars'))[params.__id] = {};
	/** 
	 * Seteamos variable en vacio 
	 */
	require(WPATH('vars'))[args.__id]['mostrar_mins'] = '';
	if (!_.isUndefined(params.mins)) {
		require(WPATH('vars'))[args.__id]['mostrar_mins'] = ':00';
	}
	if (!_.isUndefined(params.a)) {
		$.a.setText(params.a);

	}
	/** 
	 * Seteamos valores default en 00 
	 */
	var mostrar_mins = ('mostrar_mins' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['mostrar_mins'] : '';
	var desde_index = 0;
	_.each('00,01,02,03,04,05,06,07,08,09,10,11,12,13,14,15,16,17,18,19,20,21,22,23'.split(','), function(desde, desde_pos, desde_list) {
		desde_index += 1;
		var filaPicker2 = Titanium.UI.createPickerRow({
			value: desde
		});
		var Desdemostrarmins = Titanium.UI.createLabel({
			text: desde + mostrar_mins,
			color: '#000000',
			touchEnabled: false
		});
		filaPicker2.add(Desdemostrarmins);

		$.columnaPicker.addRow(filaPicker2);
	});
	var hasta_index = 0;
	_.each('00,01,02,03,04,05,06,07,08,09,10,11,12,13,14,15,16,17,18,19,20,21,22,23'.split(','), function(hasta, hasta_pos, hasta_list) {
		hasta_index += 1;
		var filaPicker3 = Titanium.UI.createPickerRow({
			value: hasta
		});
		var Hastamostrarmins = Titanium.UI.createLabel({
			text: hasta + mostrar_mins,
			color: '#000000',
			touchEnabled: false
		});
		filaPicker3.add(Hastamostrarmins);

		$.columnaPicker3.addRow(filaPicker3);
	});
};

/** 
 * Funcion para ajustar parametros de widget
 * 
 * @param desde Indica desde que hora mostrar el selector
 * @param hasta Indica hasta que hora mostrar el selector 
 */

$.set = function(params) {
	if (!_.isUndefined(params.desde)) {
		/** 
		 * Seteamos valor seleccionado de columna desde 
		 */
		var picker;
		picker = $.picker;
		var filas = picker.getColumns()[0].getRows();
		var des_index = 0;
		_.each(filas, function(des, des_pos, des_list) {
			des_index += 1;
			if (params.desde == parseInt(des.value)) {
				picker.setSelectedRow(0, des_pos);
			}
		});
	}
	if (!_.isUndefined(params.hasta)) {
		/** 
		 * Seteamos valor seleccionado de columna hasta 
		 */
		/** 
		 * Obtenemos el id del picker y fijamos el valor obtenido desde el parametro 
		 */
		var picker;
		picker = $.picker;
		var filas = picker.getColumns()[2].getRows();
		var cual_index = 0;
		_.each(filas, function(cual, cual_pos, cual_list) {
			cual_index += 1;
			if (params.hasta == parseInt(cual.value)) {
				picker.setSelectedRow(2, cual_pos);
			}
		});
	}
};