/** 
 * Widget Pregunta
 * Este control representa una vista que permite escoger entre dos opciones: si y no.
 * 
 * @param top Indica el margen superior de este widget
 * @param titulo Indica el titulo mostrado sobre el personaje
 * @param texto Indica el texto de la pregunta
 * @param si Indica el texto para el boton SI
 * @param no Indica el texto para el boton NO 
 */
var _bind4section = {};

var args = arguments[0] || {};


$.widgetMono.init({
	titulo: '¿PUEDE CONTINUAR CON LA INSPECCION?',
	__id: 'ALL1300949542',
	texto: 'Si está el asegurado en el domicilio presione SI para continuar',
	alto: '-',
	top: 0,
	ancho: '90%',
	tipo: '_info'
});

function Touchstart_vista2(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.vista2.setBackgroundColor('#f6fdfa');

}

function Touchend_vista2(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.vista2.setBackgroundColor('#ffffff');

	if ('__args' in args) {
		args['__args'].onsi({});
	} else {
		args.onsi({});
	}

}

function Touchstart_vista3(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.vista3.setBackgroundColor('#fdf5f5');

}

function Touchend_vista3(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.vista3.setBackgroundColor('#ffffff');

	if ('__args' in args) {
		args['__args'].onno({});
	} else {
		args.onno({});
	}

}
/** 
 * Funcion que inicializa widget 
 */

$.init = function(params) {
	for (var tobe in params) args[tobe] = params[tobe];
	if ('__id' in params) require(WPATH('vars'))[params.__id] = {};
	if (!_.isUndefined(params.top)) {
		/** 
		 * Verifica que existe params.top 
		 */
		$.vista.setTop(params.top);

	}
	if (!_.isUndefined(params.titulo)) {
		/** 
		 * Verifica que exista params.titulo 
		 */

		$.widgetMono.update({
			titulo: params.titulo
		});
	}
	if (!_.isUndefined(params.texto)) {
		/** 
		 * Verifica que exista params.texto 
		 */

		$.widgetMono.update({
			texto: params.texto
		});
	}
	if (!_.isUndefined(params.si)) {
		/** 
		 * Verifica que exista params.si 
		 */
		$.SIPuedoContinuar.setText(params.si);

	}
	if (!_.isUndefined(params.no)) {
		/** 
		 * Verifica que exista params.no 
		 */
		$.NOsePudo.setText(params.no);

		if (_.isNumber((params.no.length)) && _.isNumber(28) && (params.no.length) < 28) {
			$.NOsePudo.setTop(25);

		} else {
			$.NOsePudo.setTop(18);

		}
	}
};

/** 
 * Funcion que actualiza los datos mostrados en la vista de este widget
 * 
 * @param titulo Modifica el titulo sobre el personaje
 * @param texto Modifica el texto de detalle de la pregunta
 * @param si Modifica el texto del boton SI
 * @param no Modifica el texto del boton NO 
 */

$.update = function(params) {
	if (!_.isUndefined(params.titulo)) {
		/** 
		 * Verifica que exista params.titulo 
		 */

		$.widgetMono.update({
			titulo: params.titulo
		});
	}
	if (!_.isUndefined(params.texto)) {
		/** 
		 * Verifica que exista params.texto 
		 */

		$.widgetMono.update({
			texto: params.texto
		});
	}
	if (!_.isUndefined(params.si)) {
		/** 
		 * Verifica que exista params.si 
		 */
		$.SIPuedoContinuar.setText(params.si);

	}
	if (!_.isUndefined(params.no)) {
		/** 
		 * Verifica que exista params.no 
		 */
		$.NOsePudo.setText(params.no);

		if (_.isNumber((params.no.length)) && _.isNumber(28) && (params.no.length) < 28) {
			/** 
			 * Consulta si la cantidad de caracteres del valor de params.no es menor a 28 
			 */
			$.NOsePudo.setTop(25);

		} else {
			$.NOsePudo.setTop(18);

		}
	}
};