Widget 4 Fotos Chicas
Este control representa una vista con 3-4 cajas que contienen version mini de una foto y las procesa.

@param caja Indica el ancho y alto de cada mini caja
@param label1 Indica el texto a mostrar bajo la primera mini caja
@param label2 Indica el texto a mostrar bajo la segunda mini caja
@param label3 Indica el texto a mostrar bajo la tercera mini caja
@param label4 Indica el texto a mostrar bajo la cuerta mini caja
@param ancho Indica el ancho de este widget, como contenedor de las 4 mini cajas
@param alto Indica el alto de este widget, como contenedor de las 4 mini cajas
