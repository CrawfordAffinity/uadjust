exports.events = _.clone(Backbone.Events);
exports.ajaxkeys = {};
exports.variables = {};
exports.styles = {
	"images": {},
	"classes": {
		"fondoplomo": {
			"backgroundColor": "#f7f7f7",
			"font": {
				"fontSize": "12dp"
			}
		}
	}
};
exports.fontello = {};