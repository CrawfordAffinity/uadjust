Widget Selector Fechas
Permite seleccionar una fecha universalmente

@param desde Define el valor del picker con el valor inciial de las fechas
@param hasta Define el valor del picker con el valor final de las fechas
@param aceptar Define el texto del boton aceptar
@param cancelar Define el texto del boton cancelar
