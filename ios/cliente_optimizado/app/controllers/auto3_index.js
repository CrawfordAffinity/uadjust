var _bind4section = {};
var _list_templates = {};

$.ID_375356980_window.setTitleAttributes({
    color: '#FFFFFF'
});
var _activity;
if (OS_ANDROID) {
    _activity = $.ID_375356980.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_375356980';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_375356980_window.addEventListener('open', function(e) {
        abx.setStatusbarColor("#FFFFFF");
        abx.setBackgroundColor("#2d9edb");
    });
}

function Click_ID_1549723165(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    $.ID_375356980.close();

}

$.ID_507654472.init({
    __id: 'ALL507654472',
    paso: L('x1842515611', '3'),
    titulo1: L('x807033745_traducir', 'Received'),
    titulo3: L('x951154001_traducir', 'End'),
    titulo2: L('x1018769216_traducir', 'Evaluating')
});


$.ID_1926115404.init({
    titulo: L('x3950563313_traducir', 'Description'),
    __id: 'ALL1926115404'
});

function Load_ID_322282592(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var evento = e;
    elemento.start();

}

(function() {
    $.ID_220723656.setShowVerticalScrollIndicator(false);
    if ((_.isObject(args) || (_.isString(args)) && !_.isEmpty(args)) || _.isNumber(args) || _.isBoolean(args)) {
        var siniestro = args._data;
        if (Ti.App.deployType != 'production') console.log('nbrsiniestro', {
            "asd": siniestro
        });
        var ID_1258519750_i = Alloy.createCollection('casos');
        var ID_1258519750_i_where = 'ID=\'' + args.__master_model._id + '\'';
        ID_1258519750_i.fetch({
            query: 'SELECT * FROM casos WHERE num=\'' + siniestro.num_caso + '\''
        });
        var datos = require('helper').query2array(ID_1258519750_i);
        if (Ti.App.deployType != 'production') console.log('precarga', {
            "asd": datos
        });
        datos = datos[0];
        if (Ti.App.deployType != 'production') console.log('carga', {
            "asd": datos
        });
        var docu = JSON.parse(datos.documentos);
        var hola_index = 0;
        _.each(docu, function(hola, hola_pos, hola_list) {
            hola_index += 1;
            var ID_1309026480 = Titanium.UI.createView({
                height: '50dp',
                bottom: '10dp',
                borderColor: '#e6e6e6',
                layout: 'composite',
                width: '90%',
                top: '10dp',
                borderWidth: 1,
                borderRadius: 5,
                backgroundColor: '#FFFFFF'
            });
            var ID_1265644961 = Titanium.UI.createLabel({
                bottom: 15,
                text: hola.nombre,
                color: '#2d9edb',
                touchEnabled: false,
                top: 15,
                font: {
                    fontFamily: 'SFUIDisplay-Regular',
                    fontSize: '14dp'
                }

            });
            ID_1309026480.add(ID_1265644961);


            ID_1309026480.addEventListener('click', function(e) {
                e.cancelBubble = true;
                var elemento = e.source;
                Ti.Platform.openURL(hola.url);
            });
            $.ID_220723656.add(ID_1309026480);
        });
        var days = L('x3957652582_traducir', 'days');
        var legal = L('x2139489547_traducir', 'legal:');
        var uadjust = L('x1807599243_traducir', 'uadjust:');
        $.ID_1949013077.setText(String.format(L('x3000044032', '%1$s %2$s %3$s %4$s %5$s %6$s'), (legal) ? legal.toString() : '', (datos.legal) ? datos.legal.toString() : '', (days) ? days.toString() : '', (uadjust) ? uadjust.toString() : '', (datos.estimado) ? datos.estimado.toString() : '', (days) ? days.toString() : ''));

        $.ID_1104555048.setText(datos.compania);

    }
    var car3 = L('x2037834496_traducir', 'desccar3');

    $.ID_1926115404.texto({
        valor: L('x3810221188_traducir', 'car3')
    });
})();

if (OS_IOS || OS_ANDROID) {
    $.ID_375356980.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}
//$.ID_375356980.open();