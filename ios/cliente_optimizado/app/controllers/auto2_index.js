var _bind4section = {};
var _list_templates = {};

$.ID_375356980_window.setTitleAttributes({
    color: '#FFFFFF'
});
var _activity;
if (OS_ANDROID) {
    _activity = $.ID_375356980.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_375356980';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_375356980_window.addEventListener('open', function(e) {
        abx.setStatusbarColor("#FFFFFF");
        abx.setBackgroundColor("#2d9edb");
    });
}

function Click_ID_1549723165(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    $.ID_375356980.close();

}

$.ID_507654472.init({
    __id: 'ALL507654472',
    paso: L('x450215437', '2'),
    titulo1: L('x807033745_traducir', 'Received'),
    titulo3: L('x951154001_traducir', 'End'),
    titulo2: L('x1018769216_traducir', 'Evaluating')
});


(function() {
    $.ID_220723656.setShowVerticalScrollIndicator(false);
    if ((_.isObject(args) || (_.isString(args)) && !_.isEmpty(args)) || _.isNumber(args) || _.isBoolean(args)) {
        var siniestro = args._data;
        var ID_1258519750_i = Alloy.createCollection('casos');
        var ID_1258519750_i_where = 'num_caso=\'' + siniestro.num_caso + '\'';
        ID_1258519750_i.fetch({
            query: 'SELECT * FROM casos WHERE num=\'' + siniestro.num_caso + '\''
        });
        var datos = require('helper').query2array(ID_1258519750_i);
        datos = datos[0];
        if (Ti.App.deployType != 'production') console.log('carga', {
            "asd": datos
        });
        var docu = JSON.parse(datos.documentos);
        var hola_index = 0;
        _.each(docu, function(hola, hola_pos, hola_list) {
            hola_index += 1;
            var ID_601368690 = Titanium.UI.createView({
                height: '1dp',
                layout: 'vertical',
                width: Ti.UI.FILL,
                backgroundColor: '#E6E6E6',
                font: {
                    fontSize: '12dp'
                }

            });
            $.ID_1945909135.add(ID_601368690);
            if (Ti.App.deployType != 'production') console.log('asdasd', {
                "asd": hola
            });
            if (hola.estado == 1 || hola.estado == '1') {
                var ID_419998710 = Titanium.UI.createView({
                    height: Ti.UI.SIZE,
                    bottom: '20dp',
                    layout: 'horizontal',
                    top: '20dp'
                });
                if (OS_IOS) {
                    var ID_1770965280 = Ti.UI.createImageView({
                        left: 17,
                        image: '/images/i2C7AF58BC23164AB82E5D62503EC1D98.png',
                        id: ID_1770965280
                    });
                } else {
                    var ID_1770965280 = Ti.UI.createImageView({
                        left: 17,
                        image: '/images/i2C7AF58BC23164AB82E5D62503EC1D98.png',
                        id: ID_1770965280
                    });
                }
                require('vars')['_ID_1770965280_original_'] = ID_1770965280.getImage();
                require('vars')['_ID_1770965280_filtro_'] = 'original';
                ID_419998710.add(ID_1770965280);
                var ID_1691342959 = Titanium.UI.createLabel({
                    left: 10,
                    text: hola.nombre,
                    color: '#a0a1a3',
                    touchEnabled: false,
                    width: Ti.UI.FILL,
                    font: {
                        fontFamily: 'SFUIDisplay-Light',
                        fontSize: '13dp'
                    }

                });
                ID_419998710.add(ID_1691342959);

                $.ID_1945909135.add(ID_419998710);
            } else {
                var ID_978681892 = Titanium.UI.createView({
                    height: Ti.UI.SIZE,
                    bottom: '20dp',
                    layout: 'composite',
                    top: '20dp'
                });
                var ID_1431811554 = Titanium.UI.createLabel({
                    left: 40,
                    text: hola.nombre,
                    color: '#a0a1a3',
                    touchEnabled: false,
                    width: Ti.UI.FILL,
                    font: {
                        fontFamily: 'SFUIDisplay-Light',
                        fontSize: '13dp'
                    }

                });
                ID_978681892.add(ID_1431811554);

                $.ID_1945909135.add(ID_978681892);
            }
        });
        $.ID_1104555048.setText(datos.compania);

        $.ID_2268284.setText(datos.descripcion);

        $.ID_1454886879.setText(datos.correo);

        var days = L('x3957652582_traducir', 'days');
        var legal = L('x2139489547_traducir', 'legal:');
        var uadjust = L('x1807599243_traducir', 'uadjust:');
        $.ID_1464872141.setText(String.format(L('x3000044032', '%1$s %2$s %3$s %4$s %5$s %6$s'), (legal) ? legal.toString() : '', (datos.legal) ? datos.legal.toString() : '', (days) ? days.toString() : '', (uadjust) ? uadjust.toString() : '', (datos.estimado) ? datos.estimado.toString() : '', (days) ? days.toString() : ''));

    }
})();

if (OS_IOS || OS_ANDROID) {
    $.ID_375356980.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}
//$.ID_375356980.open();