var _bind4section = {};
var _list_templates = {};

$.ID_379415434_window.setTitleAttributes({
    color: '#FFFFFF'
});
var _activity;
if (OS_ANDROID) {
    _activity = $.ID_379415434.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_379415434';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_379415434_window.addEventListener('open', function(e) {
        abx.setStatusbarColor("#FFFFFF");
        abx.setBackgroundColor("#2d9edb");
    });
}

function Click_ID_335637390(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    $.ID_379415434.close();

}

$.ID_1646094736.init({
    __id: 'ALL1646094736',
    paso: L('x2212294583', '1'),
    titulo1: L('x807033745_traducir', 'Received'),
    titulo3: L('x951154001_traducir', 'End'),
    titulo2: L('x1018769216_traducir', 'Evaluating')
});


$.ID_789232151.init({
    titulo: L('x3950563313_traducir', 'Description'),
    __id: 'ALL789232151'
});

function Load_ID_487912265(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var evento = e;
    elemento.start();

}

(function() {
    $.ID_1699648863.setShowVerticalScrollIndicator(false);
    if ((_.isObject(args) || (_.isString(args)) && !_.isEmpty(args)) || _.isNumber(args) || _.isBoolean(args)) {
        var siniestro = args._data;
        var ID_1258519750_i = Alloy.createCollection('casos');
        var ID_1258519750_i_where = 'num_caso=\'' + siniestro.num_caso + '\'';
        ID_1258519750_i.fetch({
            query: 'SELECT * FROM casos WHERE num=\'' + siniestro.num_caso + '\''
        });
        var datos = require('helper').query2array(ID_1258519750_i);
        datos = datos[0];
        $.ID_1264810838.setText(datos.compania);

        var days = L('x3957652582_traducir', 'days');
        var legal = L('x2139489547_traducir', 'legal:');
        var uadjust = L('x1807599243_traducir', 'uadjust:');
        $.ID_701822901.setText(String.format(L('x3000044032', '%1$s %2$s %3$s %4$s %5$s %6$s'), (legal) ? legal.toString() : '', (datos.legal) ? datos.legal.toString() : '', (days) ? days.toString() : '', (uadjust) ? uadjust.toString() : '', (datos.estimado) ? datos.estimado.toString() : '', (days) ? days.toString() : ''));

    }
    var car1 = L('x2541257260_traducir', 'desccar1');

    $.ID_789232151.texto({
        valor: L('x219495848_traducir', 'car1')
    });
})();

if (OS_IOS || OS_ANDROID) {
    $.ID_379415434.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}
//$.ID_379415434.open();