var _bind4section = {};
var _list_templates = {};

$.ID_375356980_window.setTitleAttributes({
    color: '#FFFFFF'
});
var _activity;
if (OS_ANDROID) {
    _activity = $.ID_375356980.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_375356980';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_375356980_window.addEventListener('open', function(e) {
        abx.setStatusbarColor("#FFFFFF");
        abx.setBackgroundColor("#2d9edb");
    });
}

function Click_ID_1549723165(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    $.ID_375356980.close();
    require('vars')[_var_scopekey]['num_caso'] = L('x634125391_traducir', 'null');
    require('vars')[_var_scopekey]['datos'] = L('x634125391_traducir', 'null');

}

$.ID_1655304175.init({
    __id: 'ALL1655304175',
    paso: L('x1842515611', '3'),
    titulo5: L('x951154001_traducir', 'End'),
    titulo1: L('x807033745_traducir', 'Received'),
    titulo4: L('x1018769216_traducir', 'Evaluating'),
    titulo3: L('x4262956787_traducir', 'Inspected'),
    titulo2: L('x807001066_traducir', 'Scheduled')
});


$.ID_1926115404.init({
    titulo: L('x3950563313_traducir', 'Description'),
    __id: 'ALL1926115404'
});

function Click_ID_1390435365(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var evento = e;
    require('vars')['calificacion'] = L('x2212294583', '1');
    if (Ti.App.deployType != 'production') console.log('caja1', {});
    $.ID_1669888124.start();
    var ID_832185643_visible = false;

    if (ID_832185643_visible == 'si') {
        ID_832185643_visible = true;
    } else if (ID_832185643_visible == 'no') {
        ID_832185643_visible = false;
    }
    $.ID_832185643.setVisible(ID_832185643_visible);

    var ID_1174740711_visible = false;

    if (ID_1174740711_visible == 'si') {
        ID_1174740711_visible = true;
    } else if (ID_1174740711_visible == 'no') {
        ID_1174740711_visible = false;
    }
    $.ID_1174740711.setVisible(ID_1174740711_visible);

    var ID_1562627163_visible = false;

    if (ID_1562627163_visible == 'si') {
        ID_1562627163_visible = true;
    } else if (ID_1562627163_visible == 'no') {
        ID_1562627163_visible = false;
    }
    $.ID_1562627163.setVisible(ID_1562627163_visible);

    var ID_449093496_visible = false;

    if (ID_449093496_visible == 'si') {
        ID_449093496_visible = true;
    } else if (ID_449093496_visible == 'no') {
        ID_449093496_visible = false;
    }
    $.ID_449093496.setVisible(ID_449093496_visible);


}

function Click_ID_310428030(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var evento = e;
    var ID_832185643_visible = true;

    if (ID_832185643_visible == 'si') {
        ID_832185643_visible = true;
    } else if (ID_832185643_visible == 'no') {
        ID_832185643_visible = false;
    }
    $.ID_832185643.setVisible(ID_832185643_visible);

    var ID_1669888124_visible = true;

    if (ID_1669888124_visible == 'si') {
        ID_1669888124_visible = true;
    } else if (ID_1669888124_visible == 'no') {
        ID_1669888124_visible = false;
    }
    $.ID_1669888124.setVisible(ID_1669888124_visible);

    $.ID_1669888124.start();
    $.ID_832185643.start();

}

function Click_ID_1744609614(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var evento = e;
    require('vars')['calificacion'] = 2;
    if (Ti.App.deployType != 'production') console.log('caja2', {});
    var ID_832185643_visible = true;

    if (ID_832185643_visible == 'si') {
        ID_832185643_visible = true;
    } else if (ID_832185643_visible == 'no') {
        ID_832185643_visible = false;
    }
    $.ID_832185643.setVisible(ID_832185643_visible);

    $.ID_1669888124.start();
    $.ID_832185643.start();
    var ID_1174740711_visible = false;

    if (ID_1174740711_visible == 'si') {
        ID_1174740711_visible = true;
    } else if (ID_1174740711_visible == 'no') {
        ID_1174740711_visible = false;
    }
    $.ID_1174740711.setVisible(ID_1174740711_visible);

    var ID_1562627163_visible = false;

    if (ID_1562627163_visible == 'si') {
        ID_1562627163_visible = true;
    } else if (ID_1562627163_visible == 'no') {
        ID_1562627163_visible = false;
    }
    $.ID_1562627163.setVisible(ID_1562627163_visible);

    var ID_449093496_visible = false;

    if (ID_449093496_visible == 'si') {
        ID_449093496_visible = true;
    } else if (ID_449093496_visible == 'no') {
        ID_449093496_visible = false;
    }
    $.ID_449093496.setVisible(ID_449093496_visible);


}

function Click_ID_1182093161(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var evento = e;
    var ID_1174740711_visible = true;

    if (ID_1174740711_visible == 'si') {
        ID_1174740711_visible = true;
    } else if (ID_1174740711_visible == 'no') {
        ID_1174740711_visible = false;
    }
    $.ID_1174740711.setVisible(ID_1174740711_visible);

    var ID_1669888124_visible = true;

    if (ID_1669888124_visible == 'si') {
        ID_1669888124_visible = true;
    } else if (ID_1669888124_visible == 'no') {
        ID_1669888124_visible = false;
    }
    $.ID_1669888124.setVisible(ID_1669888124_visible);

    var ID_832185643_visible = true;

    if (ID_832185643_visible == 'si') {
        ID_832185643_visible = true;
    } else if (ID_832185643_visible == 'no') {
        ID_832185643_visible = false;
    }
    $.ID_832185643.setVisible(ID_832185643_visible);

    $.ID_1669888124.start();
    $.ID_832185643.start();
    $.ID_1174740711.start();

}

function Click_ID_1733141608(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var evento = e;
    require('vars')['calificacion'] = 3;
    $.ID_1669888124.start();
    $.ID_832185643.start();
    $.ID_1174740711.start();
    if (Ti.App.deployType != 'production') console.log('caja3', {});
    var ID_1562627163_visible = false;

    if (ID_1562627163_visible == 'si') {
        ID_1562627163_visible = true;
    } else if (ID_1562627163_visible == 'no') {
        ID_1562627163_visible = false;
    }
    $.ID_1562627163.setVisible(ID_1562627163_visible);

    var ID_449093496_visible = false;

    if (ID_449093496_visible == 'si') {
        ID_449093496_visible = true;
    } else if (ID_449093496_visible == 'no') {
        ID_449093496_visible = false;
    }
    $.ID_449093496.setVisible(ID_449093496_visible);


}

function Click_ID_51597909(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var evento = e;
    var ID_1562627163_visible = true;

    if (ID_1562627163_visible == 'si') {
        ID_1562627163_visible = true;
    } else if (ID_1562627163_visible == 'no') {
        ID_1562627163_visible = false;
    }
    $.ID_1562627163.setVisible(ID_1562627163_visible);

    var ID_1669888124_visible = true;

    if (ID_1669888124_visible == 'si') {
        ID_1669888124_visible = true;
    } else if (ID_1669888124_visible == 'no') {
        ID_1669888124_visible = false;
    }
    $.ID_1669888124.setVisible(ID_1669888124_visible);

    var ID_832185643_visible = true;

    if (ID_832185643_visible == 'si') {
        ID_832185643_visible = true;
    } else if (ID_832185643_visible == 'no') {
        ID_832185643_visible = false;
    }
    $.ID_832185643.setVisible(ID_832185643_visible);

    var ID_1174740711_visible = true;

    if (ID_1174740711_visible == 'si') {
        ID_1174740711_visible = true;
    } else if (ID_1174740711_visible == 'no') {
        ID_1174740711_visible = false;
    }
    $.ID_1174740711.setVisible(ID_1174740711_visible);

    $.ID_1669888124.start();
    $.ID_832185643.start();
    $.ID_1174740711.start();
    $.ID_1562627163.start();

}

function Click_ID_572209958(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var evento = e;
    if (Ti.App.deployType != 'production') console.log('caja4', {});
    require('vars')['calificacion'] = 4;
    $.ID_1669888124.start();
    $.ID_832185643.start();
    $.ID_1174740711.start();
    $.ID_1562627163.start();
    var ID_449093496_visible = false;

    if (ID_449093496_visible == 'si') {
        ID_449093496_visible = true;
    } else if (ID_449093496_visible == 'no') {
        ID_449093496_visible = false;
    }
    $.ID_449093496.setVisible(ID_449093496_visible);


}

function Click_ID_1270974172(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var evento = e;
    var ID_449093496_visible = true;

    if (ID_449093496_visible == 'si') {
        ID_449093496_visible = true;
    } else if (ID_449093496_visible == 'no') {
        ID_449093496_visible = false;
    }
    $.ID_449093496.setVisible(ID_449093496_visible);

    var ID_1669888124_visible = true;

    if (ID_1669888124_visible == 'si') {
        ID_1669888124_visible = true;
    } else if (ID_1669888124_visible == 'no') {
        ID_1669888124_visible = false;
    }
    $.ID_1669888124.setVisible(ID_1669888124_visible);

    var ID_832185643_visible = true;

    if (ID_832185643_visible == 'si') {
        ID_832185643_visible = true;
    } else if (ID_832185643_visible == 'no') {
        ID_832185643_visible = false;
    }
    $.ID_832185643.setVisible(ID_832185643_visible);

    var ID_1174740711_visible = true;

    if (ID_1174740711_visible == 'si') {
        ID_1174740711_visible = true;
    } else if (ID_1174740711_visible == 'no') {
        ID_1174740711_visible = false;
    }
    $.ID_1174740711.setVisible(ID_1174740711_visible);

    var ID_1562627163_visible = true;

    if (ID_1562627163_visible == 'si') {
        ID_1562627163_visible = true;
    } else if (ID_1562627163_visible == 'no') {
        ID_1562627163_visible = false;
    }
    $.ID_1562627163.setVisible(ID_1562627163_visible);

    $.ID_1669888124.start();
    $.ID_832185643.start();
    $.ID_1174740711.start();
    $.ID_1562627163.start();
    $.ID_449093496.start();

}

function Click_ID_1337360451(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var evento = e;
    if (Ti.App.deployType != 'production') console.log('caja5', {});
    require('vars')['calificacion'] = 5;
    $.ID_1669888124.start();
    $.ID_832185643.start();
    $.ID_1174740711.start();
    $.ID_1562627163.start();
    $.ID_449093496.start();

}

function Click_ID_640948675(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var correo_login = ('correo_login' in require('vars')) ? require('vars')['correo_login'] : '';
    var datos = ('datos' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['datos'] : '';
    var calificacion = ('calificacion' in require('vars')) ? require('vars')['calificacion'] : '';
    var urlcrawford = ('urlcrawford' in require('vars')) ? require('vars')['urlcrawford'] : '';
    var num_caso = ('num_caso' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['num_caso'] : '';
    if (_.isNumber(calificacion) && _.isNumber(0) && calificacion > 0) {
        var ID_623417874 = {};
        console.log('DEBUG WEB: requesting url:' + String.format(L('x554964598', '%1$scalificarInspector/'), urlcrawford.toString()) + ' with data:', {
            _method: 'POST',
            _params: {
                correo: correo_login,
                siniestro: num_caso,
                rut_inspector: datos.rut_inspector,
                calificacion: calificacion
            },
            _timeout: '15000'
        });

        ID_623417874.success = function(e) {
            var elemento = e,
                valor = e;
            if (Ti.App.deployType != 'production') console.log('respuesta de calificar inspector', {
                "datos": elemento
            });
            var num_caso = ('num_caso' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['num_caso'] : '';
            var calificacion = ('calificacion' in require('vars')) ? require('vars')['calificacion'] : '';
            var ID_349616952_i = Alloy.createCollection('casos');
            var ID_349616952_i_where = 'num_caso=\'' + num_caso + '\'';
            ID_349616952_i.fetch({
                query: 'SELECT * FROM casos WHERE num_caso=\'' + num_caso + '\''
            });
            var caso_insp = require('helper').query2array(ID_349616952_i);
            var db = Ti.Database.open(ID_349616952_i.config.adapter.db_name);
            if (ID_349616952_i_where == '') {
                var sql = 'UPDATE ' + ID_349616952_i.config.adapter.collection_name + ' SET calificacion=\'' + calificacion + '\'';
            } else {
                var sql = 'UPDATE ' + ID_349616952_i.config.adapter.collection_name + ' SET calificacion=\'' + calificacion + '\' WHERE ' + ID_349616952_i_where;
            }
            db.execute(sql);
            sql = null;
            db.close();
            db = null;
            if (Ti.App.deployType != 'production') console.log('detalle del modelo filtrado', {
                "datos": caso_insp[0]
            });
            var ID_131489538_visible = false;

            if (ID_131489538_visible == 'si') {
                ID_131489538_visible = true;
            } else if (ID_131489538_visible == 'no') {
                ID_131489538_visible = false;
            }
            $.ID_131489538.setVisible(ID_131489538_visible);

            var ID_1351469457_visible = true;

            if (ID_1351469457_visible == 'si') {
                ID_1351469457_visible = true;
            } else if (ID_1351469457_visible == 'no') {
                ID_1351469457_visible = false;
            }
            $.ID_1351469457.setVisible(ID_1351469457_visible);

            if (calificacion == 1 || calificacion == '1') {
                $.ID_780972318.start();
                var ID_269232931_visible = false;

                if (ID_269232931_visible == 'si') {
                    ID_269232931_visible = true;
                } else if (ID_269232931_visible == 'no') {
                    ID_269232931_visible = false;
                }
                $.ID_269232931.setVisible(ID_269232931_visible);

            } else if (calificacion == 2) {
                $.ID_780972318.start();
                $.ID_1193585301.start();
                var ID_269232931_visible = false;

                if (ID_269232931_visible == 'si') {
                    ID_269232931_visible = true;
                } else if (ID_269232931_visible == 'no') {
                    ID_269232931_visible = false;
                }
                $.ID_269232931.setVisible(ID_269232931_visible);

            } else if (calificacion == 3) {
                $.ID_780972318.start();
                $.ID_1193585301.start();
                $.ID_1272879231.start();
                var ID_269232931_visible = false;

                if (ID_269232931_visible == 'si') {
                    ID_269232931_visible = true;
                } else if (ID_269232931_visible == 'no') {
                    ID_269232931_visible = false;
                }
                $.ID_269232931.setVisible(ID_269232931_visible);

            } else if (calificacion == 4) {
                $.ID_780972318.start();
                $.ID_1193585301.start();
                $.ID_1272879231.start();
                $.ID_1747431818.start();
                var ID_269232931_visible = false;

                if (ID_269232931_visible == 'si') {
                    ID_269232931_visible = true;
                } else if (ID_269232931_visible == 'no') {
                    ID_269232931_visible = false;
                }
                $.ID_269232931.setVisible(ID_269232931_visible);

            } else if (calificacion == 5) {
                $.ID_780972318.start();
                $.ID_1193585301.start();
                $.ID_1272879231.start();
                $.ID_1747431818.start();
                $.ID_1530145237.start();
                var ID_269232931_visible = false;

                if (ID_269232931_visible == 'si') {
                    ID_269232931_visible = true;
                } else if (ID_269232931_visible == 'no') {
                    ID_269232931_visible = false;
                }
                $.ID_269232931.setVisible(ID_269232931_visible);

            }
            var ID_1472936917_func = function() {
                var num_caso = ('num_caso' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['num_caso'] : '';
                var ID_1498942543_i = Alloy.createCollection('casos');
                var ID_1498942543_i_where = 'num_caso=\'' + num_caso + '\'';
                ID_1498942543_i.fetch({
                    query: 'SELECT * FROM casos WHERE num_caso=\'' + num_caso + '\''
                });
                var prueba = require('helper').query2array(ID_1498942543_i);
                if (Ti.App.deployType != 'production') console.log('detalle', {
                    "datos": prueba[0]
                });

                Alloy.Events.trigger('refrescar_casos');
            };
            var ID_1472936917 = setTimeout(ID_1472936917_func, 1000 * 0.5);
            /** 
             * La respuesta debe ser true, en caso exitoso, pero estamos cerrando igual con cualquier respuesta para que el usuario tenga una buena experiencia 
             */
            var ID_269232931_visible = false;

            if (ID_269232931_visible == 'si') {
                ID_269232931_visible = true;
            } else if (ID_269232931_visible == 'no') {
                ID_269232931_visible = false;
            }
            $.ID_269232931.setVisible(ID_269232931_visible);

            elemento = null, valor = null;
        };

        ID_623417874.error = function(e) {
            var elemento = e,
                valor = e;
            var ID_269232931_visible = false;

            if (ID_269232931_visible == 'si') {
                ID_269232931_visible = true;
            } else if (ID_269232931_visible == 'no') {
                ID_269232931_visible = false;
            }
            $.ID_269232931.setVisible(ID_269232931_visible);

            elemento = null, valor = null;
        };
        require('helper').ajaxUnico('ID_623417874', '' + String.format(L('x554964598', '%1$scalificarInspector/'), urlcrawford.toString()) + '', 'POST', {
            correo: correo_login,
            siniestro: num_caso,
            rut_inspector: datos.rut_inspector,
            calificacion: calificacion
        }, 15000, ID_623417874);
        var ID_1903142033_visible = false;

        if (ID_1903142033_visible == 'si') {
            ID_1903142033_visible = true;
        } else if (ID_1903142033_visible == 'no') {
            ID_1903142033_visible = false;
        }
        $.ID_1903142033.setVisible(ID_1903142033_visible);

        $.ID_558165208.show();
    } else {}
}

(function() {
    var urlcrawford = ('urlcrawford' in require('vars')) ? require('vars')['urlcrawford'] : '';
    var urluadjust = ('urluadjust' in require('vars')) ? require('vars')['urluadjust'] : '';
    if (Ti.App.deployType != 'production') console.log('hogar3', {
        "datos": args
    });
    if ((_.isObject(args) || (_.isString(args)) && !_.isEmpty(args)) || _.isNumber(args) || _.isBoolean(args)) {
        var siniestro = args._data;
        require('vars')[_var_scopekey]['num_caso'] = siniestro.num_caso;
        var ID_1258519750_i = Alloy.createCollection('casos');
        var ID_1258519750_i_where = 'num_caso=\'' + siniestro.num_caso + '\'';
        ID_1258519750_i.fetch({
            query: 'SELECT * FROM casos WHERE num_caso=\'' + siniestro.num_caso + '\''
        });
        var datos = require('helper').query2array(ID_1258519750_i);
        datos = datos[0];
        require('vars')[_var_scopekey]['datos'] = datos;
        if (Ti.App.deployType != 'production') console.log('carga', {
            "asd": datos
        });
        $.ID_1104555048.setText(datos.compania);

        if ((_.isObject(datos.imagen_inspector) || (_.isString(datos.imagen_inspector)) && !_.isEmpty(datos.imagen_inspector)) || _.isNumber(datos.imagen_inspector) || _.isBoolean(datos.imagen_inspector)) {
            var imageBlob = Ti.Utils.base64decode(datos.imagen_inspector);
            var ID_1095538453_imagen = imageBlob;

            if (typeof ID_1095538453_imagen == 'string' && 'styles' in require('a4w') && ID_1095538453_imagen in require('a4w').styles['images']) {
                ID_1095538453_imagen = require('a4w').styles['images'][ID_1095538453_imagen];
            }
            $.ID_1095538453.setImage(ID_1095538453_imagen);

        }
        $.ID_787932640.setText(datos.inspector);

        $.ID_156639973.setText(datos.rut_inspector);

        if ((_.isObject(datos.fecha_agendamiento) || (_.isString(datos.fecha_agendamiento)) && !_.isEmpty(datos.fecha_agendamiento)) || _.isNumber(datos.fecha_agendamiento) || _.isBoolean(datos.fecha_agendamiento)) {
            $.ID_543546700.setText(datos.fecha_agendamiento);

            var ID_1510189946_visible = true;

            if (ID_1510189946_visible == 'si') {
                ID_1510189946_visible = true;
            } else if (ID_1510189946_visible == 'no') {
                ID_1510189946_visible = false;
            }
            $.ID_1510189946.setVisible(ID_1510189946_visible);

        }
        if ((_.isObject(datos.calificacion) || _.isString(datos.calificacion)) && _.isEmpty(datos.calificacion)) {
            var ID_1351469457_visible = false;

            if (ID_1351469457_visible == 'si') {
                ID_1351469457_visible = true;
            } else if (ID_1351469457_visible == 'no') {
                ID_1351469457_visible = false;
            }
            $.ID_1351469457.setVisible(ID_1351469457_visible);

        } else if (datos.calificacion == -1) {
            var ID_1351469457_visible = false;

            if (ID_1351469457_visible == 'si') {
                ID_1351469457_visible = true;
            } else if (ID_1351469457_visible == 'no') {
                ID_1351469457_visible = false;
            }
            $.ID_1351469457.setVisible(ID_1351469457_visible);

        } else if (datos.calificacion == 0 || datos.calificacion == '0') {
            var ID_1351469457_visible = false;

            if (ID_1351469457_visible == 'si') {
                ID_1351469457_visible = true;
            } else if (ID_1351469457_visible == 'no') {
                ID_1351469457_visible = false;
            }
            $.ID_1351469457.setVisible(ID_1351469457_visible);

        } else if (datos.calificacion == 1 || datos.calificacion == '1') {
            $.ID_780972318.start();
            var ID_269232931_visible = false;

            if (ID_269232931_visible == 'si') {
                ID_269232931_visible = true;
            } else if (ID_269232931_visible == 'no') {
                ID_269232931_visible = false;
            }
            $.ID_269232931.setVisible(ID_269232931_visible);

        } else if (datos.calificacion == 2) {
            $.ID_780972318.start();
            $.ID_1193585301.start();
            var ID_269232931_visible = false;

            if (ID_269232931_visible == 'si') {
                ID_269232931_visible = true;
            } else if (ID_269232931_visible == 'no') {
                ID_269232931_visible = false;
            }
            $.ID_269232931.setVisible(ID_269232931_visible);

        } else if (datos.calificacion == 3) {
            $.ID_780972318.start();
            $.ID_1193585301.start();
            $.ID_1272879231.start();
            var ID_269232931_visible = false;

            if (ID_269232931_visible == 'si') {
                ID_269232931_visible = true;
            } else if (ID_269232931_visible == 'no') {
                ID_269232931_visible = false;
            }
            $.ID_269232931.setVisible(ID_269232931_visible);

        } else if (datos.calificacion == 4) {
            $.ID_780972318.start();
            $.ID_1193585301.start();
            $.ID_1272879231.start();
            $.ID_1747431818.start();
            var ID_269232931_visible = false;

            if (ID_269232931_visible == 'si') {
                ID_269232931_visible = true;
            } else if (ID_269232931_visible == 'no') {
                ID_269232931_visible = false;
            }
            $.ID_269232931.setVisible(ID_269232931_visible);

        } else if (datos.calificacion == 5) {
            $.ID_780972318.start();
            $.ID_1193585301.start();
            $.ID_1272879231.start();
            $.ID_1747431818.start();
            $.ID_1530145237.start();
            var ID_269232931_visible = false;

            if (ID_269232931_visible == 'si') {
                ID_269232931_visible = true;
            } else if (ID_269232931_visible == 'no') {
                ID_269232931_visible = false;
            }
            $.ID_269232931.setVisible(ID_269232931_visible);

        }
        var days = L('x3957652582_traducir', 'days');
        var legal = L('x2139489547_traducir', 'legal:');
        var uadjust = L('x1807599243_traducir', 'uadjust:');
        $.ID_1932185343.setText(String.format(L('x3000044032', '%1$s %2$s %3$s %4$s %5$s %6$s'), (legal) ? legal.toString() : '', (datos.legal) ? datos.legal.toString() : '', (days) ? days.toString() : '', (uadjust) ? uadjust.toString() : '', (datos.estimado) ? datos.estimado.toString() : '', (days) ? days.toString() : ''));

    }
    $.ID_220723656.setShowVerticalScrollIndicator(false);
    var hog3 = L('x118206968_traducir', 'deschogar3');

    $.ID_1926115404.texto({
        valor: L('x154210459_traducir', 'hog3')
    });
})();

if (OS_IOS || OS_ANDROID) {
    $.ID_375356980.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}
//$.ID_375356980.open();