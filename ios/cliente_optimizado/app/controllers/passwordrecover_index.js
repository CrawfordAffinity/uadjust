var _bind4section = {};
var _list_templates = {};

$.ID_1575514239_window.setTitleAttributes({
    color: 'WHITE'
});
var _activity;
if (OS_ANDROID) {
    _activity = $.ID_1575514239.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_1575514239';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_1575514239_window.addEventListener('open', function(e) {
        abx.setStatusbarColor("#FFFFFF");
        abx.setBackgroundColor("#2d9edb");
    });
}

function Click_ID_1549723165(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    $.ID_1575514239.close();

}

function Change_ID_794397656(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    var regemail = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
    var validaremail = regemail.test(elemento);
    if (validaremail == true || validaremail == 'true') {
        $.ID_1915908811.setBackgroundColor('#8ce5bd');
    } else {
        $.ID_1915908811.setBackgroundColor('#e6e6e6');
    }
    elemento = null, source = null;

}

function Click_ID_1877749090(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
        /** 
         * Revisamos si el celular tiene internet 
         */
        /** 
         * Recuperamos el texto ingresado del correo 
         */
        var email;
        email = $.ID_1477419901.getValue();

        if ((_.isObject(email) || _.isString(email)) && _.isEmpty(email)) {
            /** 
             * Revisamos si el campo esta vacio, si es asi mostramos mensaje de que faltan datos 
             */
            var ID_477749531_opts = [L('x3610695981_traducir', 'OK')];
            var ID_477749531 = Ti.UI.createAlertDialog({
                title: L('x2861137601_traducir', 'ERROR'),
                message: L('x1676137616_traducir', 'EMPTY EMAIL'),
                buttonNames: ID_477749531_opts
            });
            ID_477749531.addEventListener('click', function(e) {
                var xd = ID_477749531_opts[e.index];
                xd = null;

                e.source.removeEventListener("click", arguments.callee);
            });
            ID_477749531.show();
        } else {
            /** 
             * Revisamos si el correo cumple el formato de email con RegEx 
             */
            var regemail = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
            var validaremail = regemail.test(email);
            if (validaremail != true && validaremail != 'true') {
                /** 
                 * Si el email cumple el formato, enviamos a crawford la solicitud de recuperar contrasena 
                 */
                var ID_684370049_opts = [L('x3610695981_traducir', 'OK')];
                var ID_684370049 = Ti.UI.createAlertDialog({
                    title: L('x2861137601_traducir', 'ERROR'),
                    message: L('x1647703686_traducir', 'INVALID EMAIL'),
                    buttonNames: ID_684370049_opts
                });
                ID_684370049.addEventListener('click', function(e) {
                    var xdd = ID_684370049_opts[e.index];
                    xdd = null;

                    e.source.removeEventListener("click", arguments.callee);
                });
                ID_684370049.show();
            } else {
                var urlcrawford = ('urlcrawford' in require('vars')) ? require('vars')['urlcrawford'] : '';
                var ID_623417874 = {};

                ID_623417874.success = function(e) {
                    var elemento = e,
                        valor = e;
                    if (elemento == true || elemento == 'true') {
                        /** 
                         * Si el servidor responde con un true, cerramos la pantalla 
                         */
                        $.ID_1575514239.close();
                    } else {
                        var ID_627395636_opts = [L('x3610695981_traducir', 'OK')];
                        var ID_627395636 = Ti.UI.createAlertDialog({
                            title: L('x3769205873_traducir', 'ALERT'),
                            message: L('x2486062857_traducir', 'MAIL SENT'),
                            buttonNames: ID_627395636_opts
                        });
                        ID_627395636.addEventListener('click', function(e) {
                            var xdx = ID_627395636_opts[e.index];
                            xdx = null;

                            e.source.removeEventListener("click", arguments.callee);
                        });
                        ID_627395636.show();
                    }
                    if (Ti.App.deployType != 'production') console.log('respuesta recuperarpassword', {
                        "asd": elemento
                    });
                    /** 
                     * Ocultamos imagen de progreso 
                     */
                    $.ID_1419859455.hide();
                    var ID_1716581815_visible = true;

                    if (ID_1716581815_visible == 'si') {
                        ID_1716581815_visible = true;
                    } else if (ID_1716581815_visible == 'no') {
                        ID_1716581815_visible = false;
                    }
                    $.ID_1716581815.setVisible(ID_1716581815_visible);

                    elemento = null, valor = null;
                };

                ID_623417874.error = function(e) {
                    var elemento = e,
                        valor = e;
                    var ID_153250154_opts = [L('x3610695981_traducir', 'OK')];
                    var ID_153250154 = Ti.UI.createAlertDialog({
                        title: L('x3769205873_traducir', 'ALERT'),
                        message: L('x1755017309_traducir', 'ERROR CONNECTING TO THE SERVER'),
                        buttonNames: ID_153250154_opts
                    });
                    ID_153250154.addEventListener('click', function(e) {
                        var zcx = ID_153250154_opts[e.index];
                        zcx = null;

                        e.source.removeEventListener("click", arguments.callee);
                    });
                    ID_153250154.show();
                    elemento = null, valor = null;
                };
                require('helper').ajaxUnico('ID_623417874', '' + String.format(L('x1410305732', '%1$srecuperarPassword/'), urlcrawford.toString()) + '', 'POST', {
                    correo: email
                }, 15000, ID_623417874);
                var ID_1716581815_visible = false;

                if (ID_1716581815_visible == 'si') {
                    ID_1716581815_visible = true;
                } else if (ID_1716581815_visible == 'no') {
                    ID_1716581815_visible = false;
                }
                $.ID_1716581815.setVisible(ID_1716581815_visible);

                /** 
                 * Mostramos imagen de progreso 
                 */
                $.ID_1419859455.show();
            }
        }
    } else {
        var ID_732533234_opts = [L('x3610695981_traducir', 'OK')];
        var ID_732533234 = Ti.UI.createAlertDialog({
            title: L('x3769205873_traducir', 'ALERT'),
            message: L('x1867626468_traducir', 'YOU DONT HAVE INTERNET CONNECTION'),
            buttonNames: ID_732533234_opts
        });
        ID_732533234.addEventListener('click', function(e) {
            var asd = ID_732533234_opts[e.index];
            asd = null;

            e.source.removeEventListener("click", arguments.callee);
        });
        ID_732533234.show();
    }
}

function Click_ID_1151293507(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    $.ID_1477419901.blur();

}

(function() {
    $.ID_1351033224.setShowVerticalScrollIndicator(false);
})();

if (OS_IOS || OS_ANDROID) {
    $.ID_1575514239.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}
//$.ID_1575514239.open();