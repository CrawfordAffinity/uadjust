Alloy.Events = _.clone(Backbone.Events);
Alloy.Collections.usuario = Alloy.createCollection('usuario');
Alloy.Collections.casos = Alloy.createCollection('casos');
//contenido de nodo global..
(function() {
    var _my_events = {},
        _out_vars = {},
        _var_scopekey = 'ID_1511373359';
    require('vars')[_var_scopekey] = {};
    require('vars')['urlcrawford'] = L('x1025326216', 'http://qa-serviciosproflow.crawfordaffinity.com/ServiceApp.svc/');
    require('vars')['urluadjust'] = L('x2575237648', 'http://api.uadjust.com:9999/api/');
    var ID_781143015 = Ti.UI.createView({});
    var ID_781143015_cloud = require('ti.cloud');
    var ID_781143015_register = function(_meta) {
        var elemento = _meta.value;
        if (Ti.App.deployType != 'production') console.log('registrado', {
            "datos": elemento
        });
        require('vars')['device_token'] = elemento;
        elemento = null;
    };
    var ID_781143015_message = function(_meta) {
        var elemento = _meta.value;
        if (Ti.App.deployType != 'production') console.log('llego mensaje', {
            "datos": elemento
        });
        var ID_1574481652_opts = [L('x1518866076_traducir', 'Aceptar'), L('x2376009830_traducir', ' Cancelar')];
        var ID_1574481652 = Ti.UI.createAlertDialog({
            title: L('x1789641236_traducir', 'Notificacion'),
            message: '' + elemento.data.aps.alert + '',
            buttonNames: ID_1574481652_opts
        });
        ID_1574481652.addEventListener('click', function(e) {
            var cosa = ID_1574481652_opts[e.index];
            cosa = null;

            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1574481652.show();
        elemento = null;
    };
    var ID_781143015_error = function(_meta) {
        var elemento = _meta.value;
        if (Ti.App.deployType != 'production') console.log('error', {
            "datos": elemento
        });
        elemento = null;
    };
    if (OS_ANDROID) {
        var ID_781143015_cloudpush = require('ti.cloudpush');
        ID_781143015_cloudpush.retrieveDeviceToken({
            success: function(e) {
                ID_781143015_cloud.PushNotifications.subscribeToken({
                    device_token: e.deviceToken,
                    channel: 'generico',
                    type: 'android'
                }, function(ee) {
                    if (ee.success) {
                        if (typeof ID_781143015_register != 'undefined') ID_781143015_register({
                            value: e.deviceToken
                        });
                    } else {
                        if (typeof ID_781143015_error != 'undefined') ID_781143015_error({
                            value: ee.message,
                            code: ee.error
                        });
                    }
                });
            },
            error: function(e) {
                if (typeof ID_781143015_error != 'undefined') ID_781143015_error({
                    value: e.error,
                    code: 'token'
                });
            },
        });
        ID_781143015_cloudpush.addEventListener('callback', function(evt) {
            if (typeof ID_781143015_message != 'undefined') ID_781143015_message({
                value: evt.payload
            });
        });
        ID_781143015_cloudpush.addEventListener('trayClick', function(evt) {
            if (typeof ID_781143015_message != 'undefined') ID_781143015_message({
                value: evt.payload
            });
        });
    } else if (OS_IOS) {
        if (Ti.Platform.name == 'iPhone OS' && parseInt(Ti.Platform.version.split('.')[0]) >= 8) {
            Ti.App.iOS.addEventListener('usernotificationsettings', function registerForPush() {
                Ti.App.iOS.removeEventListener('usernotificationsettings', registerForPush);
                Ti.Network.registerForPushNotifications({
                    success: function(e) {
                        ID_781143015_cloud.PushNotifications.subscribeToken({
                            device_token: e.deviceToken,
                            channel: 'generico',
                            type: 'ios'
                        }, function(ee) {
                            if (ee.success) {
                                if (typeof ID_781143015_register != 'undefined') ID_781143015_register({
                                    value: e.deviceToken
                                });
                            } else {
                                if (typeof ID_781143015_error != 'undefined') ID_781143015_error({
                                    value: ee.message,
                                    code: ee.error
                                });
                            }
                        });
                    },
                    error: function(e) {
                        if (typeof ID_781143015_error != 'undefined') ID_781143015_error({
                            value: e.error
                        });
                    },
                    callback: function(e) {
                        if (typeof ID_781143015_message != 'undefined') ID_781143015_message({
                            value: e
                        });
                    }
                });
            });
            Ti.App.iOS.registerUserNotificationSettings({
                types: [Ti.App.iOS.USER_NOTIFICATION_TYPE_ALERT, Ti.App.iOS.USER_NOTIFICATION_TYPE_SOUND, Ti.App.iOS.USER_NOTIFICATION_TYPE_BADGE]
            });
        } else {
            Ti.Network.registerForPushNotifications({
                types: [Ti.App.iOS.USER_NOTIFICATION_TYPE_ALERT, Ti.App.iOS.USER_NOTIFICATION_TYPE_SOUND, Ti.App.iOS.USER_NOTIFICATION_TYPE_BADGE],
                success: function(e) {
                    ID_781143015_cloud.PushNotifications.subscribeToken({
                        device_token: e.deviceToken,
                        channel: 'generico',
                        type: 'ios'
                    }, function(ee) {
                        if (ee.success) {
                            if (typeof ID_781143015_register != 'undefined') ID_781143015_register({
                                value: e.deviceToken
                            });
                        } else {
                            if (typeof ID_781143015_error != 'undefined') ID_781143015_error({
                                value: ee.message,
                                code: ee.error
                            });
                        }
                    });
                },
                error: function(e) {
                    if (typeof ID_781143015_error != 'undefined') ID_781143015_error({
                        value: e.error
                    });
                },
                callback: function(e) {
                    if (typeof ID_781143015_message != 'undefined') ID_781143015_message({
                        value: e
                    });
                }
            });
        }
    }
    /*
    console.log('eliminando casos');
    var ID_1186679924_i = Alloy.Collections.casos;
    var sql = "DELETE FROM " + ID_1186679924_i.config.adapter.collection_name;
    var db = Ti.Database.open(ID_1186679924_i.config.adapter.db_name);
    db.execute(sql);
    db.close();
    ID_1186679924_i.trigger('remove');
    console.log('cargando casos casos');
    var ID_310703634_m = Alloy.Collections.casos;
    var ID_310703634_fila = Alloy.createModel('casos', {
        siniestro: 'Hogar 1',
        tipotexto: 1,
        rut_inspector: '',
        documentos: '',
        calificacion: '',
        compania: 'Nombre compañia',
        imagen_inspector: '',
        legal: 45,
        estimado: 23,
        inspector: '',
        fecha_agendamiento: '',
        realizado_fecha: '',
        num_caso: '',
        estado: 1,
        realizado_dias: '',
        estadotexto: 1,
        tipo: 1,
        correo: '',
        descripcion: ''
    });
    ID_310703634_m.add(ID_310703634_fila);
    ID_310703634_fila.save();
    var ID_1250967109_m = Alloy.Collections.casos;
    var ID_1250967109_fila = Alloy.createModel('casos', {
        siniestro: 'Hogar 2',
        tipotexto: 1,
        rut_inspector: '18.373.278-1',
        documentos: '',
        calificacion: '',
        compania: 'Nombre compañia',
        imagen_inspector: '/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAoHBwkHBgoJCAkLCwoMDxkQDw4ODx4WFxIZJCAmJSMgIyIoLTkwKCo2KyIjMkQyNjs9QEBAJjBGS0U+Sjk/QD3/2wBDAQsLCw8NDx0QEB09KSMpPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT3/wAARCAEsATwDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwCo5zGSD26V6Pof/Iv2H/XL+tYH/CCShWzdL+ddLY232PTre23bjCm0n1rijE3xFSMo+6T/AFpRTVGSaccVqcjD3oOAc9zSjpikxnrSDUM470fePNAA6UY9adg1Bc5JoAxk04DAIoH0o6jsN5xRzTiPl4pADimFgGaU5/A0DpS44xS2EIVzxRg5wKeqkr05qSG3fzNxHFNalojjtZH6LwfWm3NrMmwqm8scfStVeAOKdz2rVIZjT20kKgtyCO1V9yj8a2rtSY+BWVPYsfmUkGs5RFexXY/pTEHz596cUZEIbrSRcsCKhuwbl0/dFIT0pxGFGTSFeM1VrhYawOaack47U8ZNBGDSuMYQBTcfNT2xSY5pXEIKZnJp3rxSYx1p3FYaTmmnOfan4xTGPFMYjjd0NNYDGKXoOlNHJ6UMdxp9O1MzyBUj8GmheRnrQiRrcjPpTduTmnMOopBkHFMdriHAFAHFI1OHTpUjtY1ecc0gFOUZHNLtHrSMxnTpQRinFQAKMjHNAmriDig9KceKOoxQMZ3FPxkUEYxSgflTuCIx8rU9eaDjPFGcUXGKRxSAZ6U7tSIOTTAAp/CpY4TLzjFLDCZD14FX0QIABVKNxpDEhCgDFS4pOlLWiikUFFFFMBCARTGQEYxUlFJoDPuLVSCcVmzxGA5A4roGGQap3MIZTxWUogU42EqAg9Kc3bHQ1Tz9lbn7p9KtkgqCOhqbgNNFB4x70Y5o3AbxkjFIT8vvSnhqQYzxQIQUhOcil6n2pMjBxQIaRgdcio881IegAppAFMaGP0pp9BTmOCKaxG+gXUbgh+eaOp96cx5pp4bIoKGHGeaG9R1pzfeHpTGOKCbMaec8UobA60ZIBqPb6mgs3scHNJjkelPPFN+lSZtWGg9RR0pSOc0ucjgc0AhCMc0Up6UZA7UBYQY/Ggg07Az0ob2oCwzPPTFOxkUh9TS571SAAO2aUKW4HXpSZ5zVm0jyxY9KaQ0izDH5aAd6koFLWqKEopGOKQtTAdRTd3rRuPpQA6iql9qUGn28k9w4CRAFsckfhWZaeMdKvZESKfaXOF38AmkUotm9UUqZU04PnpzSt0oZOxj3kQZDxVa1kPMLdR0rTul4NYsuYrgOKyegGgSR1pO9G7cuR6cU0n9KAA9elH3T0oz8vvSE881LTExKac5GKcSMU3pkfrSQhh6nBpCOAc0/AH+NNIGKoY3bkgmmsvOQKduzxSZ7ZpDGHOaac08nHGKYQecimA3mgjNLim565pgIaKTPGcZpR0oA2x0oxijpQTxUEiEGgAinA4oByaQhpHANLjI9KUAGg4zigYgoFBAzSYFMYGgACnACm9TTQgwenrWhbKBEPWqBA6+lX7cERj3q46jRNRmiitRlDWpXi0yVonKSEYVh2JrltP1vWm1ePTGkttkUXmSSlSdw9PrXaTwJcRNHIMq3UVTtdEsrOXzYocSFdpYkniqi0tweqsZmp3l0sXmZOYlLfJ0PHFZUWs63Iti3n26rffMGZThWzjbXYmzhZQrICBn9ar/2LYi3SDyR5aPvUZPB9qfMiKcXHdnPeIcyRzwMFe6eNA4A4bntWNc+UPsltHobwS71+cgYT3rv5NPt5H3ugL4Az7VRn0CCa7+0b2Geq5rLl1OyFZJWF0N2YTI0hkCtjf61rdRzVOwsVsUZE+6zZq7VMwk7u5Uul+U1g34/CuldN6kHiuevo2wSR0NZyVibElnJugX261IQcmqmnMMumec5q4fSoAac/lQRmlcdOelJnFMBvUUnTrSn2pC2TSBDSDimsTT+pppoAb0FNwc5p+DimsM0DGEAnrSEHmnkZOaYc5oFcaOhpvXOaXFN74pgA9TTaGyTjvS7lHWgDdpKX8KMVBNxM0vam4pyjFNoSEPWl9KDg0hB4pFID1oAxR/nmj2ppAIfXtS/yo9qbI4QZPA6Z96dgJFjZwQg61fjBVFBHIFcPrurz2byxXE32eJFLsy+leVaz411K6uStlczRQL91ixJb39quKGfSFFfM8XjnxJCuI9SYexGau23xA8XNsZdRGwsVBMYwSOo/UVo3YFdn0ZSE14Zb+OvFQGW1CLP/XIVbXx/4lXg3sJ/7Yis/aI1VGTPaM0EivHR4/8AEfe6iP8A2xFO/wCFheIV6zRn/tmKXtEP2Ej17d9aTcD715IvxL1tB8yo2P8AZXmn/wDC1tTXrZqfyo57i9jNHrW6kMgxXkp+LuooP+Qare+aib413kLfvdFDr7SYqr3IcJI9fDBhWZfKpU45BrzFvju+0hdDw3/Xb/61U/8Ahc9xcSBX09bdGON+7dj8KbjdEJu56JaYF849qvMwzmuRsdckuIluFCjeMqwOcit7Sr83/mK2N685qOWw2aBPemknIpw5A9uKQrSAD1wDTRgUuRTccfWgAzt470hPNKQMc0hwaVgGsDkYNMOc9KdnpgdaCe9Axp6daa3ag8c+tNY5NMQ3O4mm4w2SaUoBk+tJ0pFIa3I460mR360u3qc05enSgTRurQRmj04HFA70tiBOlGSRSYOcZ4ozjijcaF6UpIAz3pMEnFGDkg0DYEZHPWk+vWl5oPUGmAZxUVxEJoWjPUnIPuORUh45PNA45PQ0COE1/Tpr6z1GO43G6kyUAHHTpXlVzZsEMcimOaLhgeK+irmzhugFm4PZl4Neb/ETwyNO0uS/ZkZTwjIMEfX1q4sq9zyxWAkBZdyjsDitnSovtUpldgojGFRQeB+fTv15PXqTWEoJwMdTXaabALOziZR8xwxpVJWRtRj1JRACSD1pwttw9KgnujhUgQl2zt5znHJ+mBzVVdcNvhJBk1z8jZ081jV+yMDxkin/AGQt1BqvZ69FMQNpya145FbnsaHGxadzLks2bIA4qrLasg+Y81tXM6wQs7YrnLzXYw20KSRTirik7CmMjgjFVJ4so2OtQNqckz/KNtWY5RMArDBHWqs0Z3ujnnB3tntQq+YcfnVjUITDct/dbkVe8NaS2s3oh2sy55VOprpi9DklG0je8GtNFpN0yktEsuEB+lem6AqyO9zFGywNGqkH+93qlpXg5baCJbnEMKYxEv3j9TXTqiRRqkShEUYAFQ3qJhnrnp2pDmlPIpce9SSR4xQORSkc9aQUAN5zSGlpCaAEOAKZgD8acab2pANfpSGlbjikNBQxuaaeRSk8mm0BYRvummgkCjnPWkyPSgL2OjB4NNFLwoxRxSIsBWjGKQnHNKDkUFCGjGe9L6UdDigQMcAY70nWjFHekAv5UmefrTqaR05qgEA3MEA6mvNPiVqz6xePoVug8i3UMzg9T6fmK9PiIEy/WvJtUtntfFl7HKpEjktz3B6Um7GtGKkzzVImFwkRHzBsYrvI4C9vGucEKARXMiAyeKigA+9muzjGMjHSpqS0OmlDWxlzm004b3b5jxj19qzpr+wkYj7L+8Zj977xPUk+g+vv71szaVFdSh5AeDkZ9arXWhRPctOVJJ+8p6H/AD/SphNW1LlTZUs5YHPyxbG7A1vWYEkRIHIrMECG4aQ8HgdOSO30rQtGMYYngHpUSepcEZeoT75jGTgDrWWbqxibMsW73x1q7dxiW6kVsgN3FQzaYl7sVkKmMbV28A1pC3Uiab2Gw32nz4CwBR0BqWe1QFXiA57im/2IuxVUYK91q9Bb+VAUfk9qJPXQlRsc9r6bRCcY9au+C/EF14W1NL2KNZbeZhHKje/FReIYibRGH8LVUst32KNcZDyqB9c1rB+6YSjeWp9FZWQLLGSySLuUn0pNuRUNojw6TZQyZEixgMPSpCzcYHFDMXoxaDTWJyKTJ9KZIpIzTCeaXvTM5NAh2OKQYFBPy03JpDHEdc0wrxTsnFM3GkNIQnn2pjDrTj0prHigdhnH50h4pScEUxyGp2GBx1qLd6CnFsDpTcn0osJnQBxjGc0oNOVBzgUBagkTcBQrA9DTtmTmgKAM1QWGlulGctTivTmk2/NmhgGcHHak/ipT9KPxoAXNICSDxS4PrRjAoAaCFOe9cj470whoNZgHzr8sx9sYFdeMVneI4hceGr6NhkEDgfWk1cum2pHjEVoy+KGuACUdSckcV08Kb+veqU4KW0BznBxn8au27/KKxld7npU7Jk5hUH6VXmUscVc61HIoBqDqcU0ZzoI8HA4OTSp86Z7jkn1oulZmCqMkmpobLgEnk9aDFx10MO4AW5PvVyFFZQRS6hbKGJOML2A5Pt/9f/8AVUdixZCMYGapKxm1cvIi7eagnjABK1YUfLVedsAimEtEYGvDdpzkjoa0Ph9pCazrllBOuYY8uR79R/Kq95CLkC3PRjkiuu+HEaQ+JbpUXAaNVGP4cCtYPQ5Zq12ehzMZGJ9e1NAIGDTyP1ptaHINI60mKc3TJpCeaYDcU3aM07PNNPWkwExmmNgcGnkZpHA49aQXGE4FN5IpzAE0j9BSZa2G49aYRkn2p7ZIph64pWBsafemEU/qeaaasWpHgijFPIB7im4FA7HRAUbR2pc0valYz1DGKTApc8UDnmgAxxSHk80vP4GkPp6UAhCAKQjil7c0nNOwwAJBo60djSZ5pAIQelNxlSrKCjcMD3p4OTQVyegosM4fxX4UjstNe8spD5auMxnnH41zthKJI1+lena1B9p8P3sf92JmA9wK8osCVjXJ5Awazmjqozd9TYBPWkkYEUwTBgMVXmlJOBmsbHoKaSIrqdoQzxjcxBH0qrYPd29qVmmE287hgY21KQWb5zgCkBQrtRhx1pohybM+4uLiPUxNK+LYjGzGSatWeSDIRgMeBTLkRvICrcjtRDI6e4z0p3M2zQ3fJVS4IwTTxJlaq3MvBANCJm7j9H0y71zU2t7MZljXeT6CvTPDnhyLw9bSZk828mx5knb6Cub+FltmW/vz0wYfxzmu868kVtFI46kr6AT0pCMUo6nNBqzFDCeOmaQ49aU8YpDikVEafbrTWAp7Yxx1pnFAMSg4x1oPHFN9qCRuaTg/WlIyaaRg0rDTEc44qM8c04kGmE81VhMbkGk6mnMMYx0pgbmk0XHQTJ54qIls9aeGw3Wm5GTxRYHI6kDrQOUNHWloIEx8tHakGeaB0pCDJxiil96TnrTCwhpKUmkoKQd6MY4pe9I2e1K40wIpD0pAD3pTxwKGygCrKGicZWQFG+hrx+6jFjrV3bZ+VZW2/TNewxf61frXkHinEXiu6x/ETn86U9i6e49ZACD/AA1WvJnO5YiA7cA+lRxzFlwT9KM/NnpWNzqTK40y4bAe6Jz6E1FNp3kPhbhtx681pKdy9ee1RTor4OMt3oTOhSVjOksJokDLLuz3p1mtxA7NM4ZDxirxwqYz07VUllVQVXvTREpJlosBHkHrVC5mCxuxPQcU0yME254qrcEuY0H8bBfzqoo55aHrngCzFl4RiZsA3T+d+Yroepzmq2lxfZtB06DoEgX86s59a1Rxy1YuOaDxSUp6VctBJDWOaQ4FKe9JgVICNxTCOKGBFNIzzQAjUjkjBFONMOSKQhpbK030zSseKRh0xTGNZcHNMf3p5PHNN4waChpPFMz1pfWmPmmD0Gg4PSmlhSk5pML3NFhbnVcA8UhIzwKUdaU4FSjO43JzS/Sko7ZpjQpHFIR05pQCaQc5HpQAlNOeKd9KTpmgpAaaDwT3p2aZkfgfSpeg0AOBmgEsccbjRgsdox9ewrh/Gvj+HS9PuLTRpC1/91ptvyx/Q9D3pxVwOm1LxNo+hvi+voo5V6x968x1e+t9e12a9tVItnGAT3NcBJJPfztJNI0sznknvXY6PF5VlHG3XFZ1VZHTRV2QB3tHKSDKHoanjm3/AHj9DV2a1EqYYZzWXNaSQZK9BWSlc3cWXt2B1oZuOKyxdsv38gVMmoLgA1ViW2iaUlY2OeaoM4XO45Jpbi93ZC1T8x3PTJqkg1ZYknVF65JqLDr5c5GRG4cj1ANSW9oWG6SpZ8CJlA7YoUtQcdD1fw14x0zxNBFDat5NxGAvkSHk/St/kNtYcCvmSKaW0ud8TtGwb7ynmvbPAniq41lRp+pMr3CoDE4/jGO/4V0OK3RyNHX9B/KjmmE9uhFOGT16UmQmHOaCKGOOnSkyKQriGomJ6VITzUZzmgLiDPSkOelPBxTDyeKQEbDmkYkdKc+M0jHOMdKYDM7hzTDmn470zPagLjO9NJ9aewwDUZApgMwM0hIzSkjA4pOf7v60COqxSjk0DmjBGaCLAxx0oTpRgY60dBSKQdqQ8Up9aaTn60DEJxTc0vVeaYDuPApMaQ7OKQcknpgZJPTFV7+/s9Ltmn1C5SCMDPJyT+HWvPPEnxFj1pH0jQo3VZT810WxwOox71UYt7lKLk7I3db8WWmoW11p2lSuzqCs06fdA7gH1rzbxZCYo7S0jUrGfmLH+Mkda7LwRY21xBcKqjaj7mGOWPvWF8SWV/EFvBGABGoOAMAZFUtHobVYxgrdTjtLVRNKccrxzXT6awMQy3zVzdv+6vHBxhzW/aYj5zxXNVNaPwm0BuXNRyR0BiACp4qQMHHNc503uZs1pG4+Zcn1rPntI1yVOK33jU96rPbI2eM1akyXEwFtS7cdKuW9oB26Vca3EYFKjqFIxTchMiKbRjHFUZwdpAGSTWjndz2FV5gO3A9aFoScnNGwvSjDBzW5oc93aN9rsnZJ7c8EHt6GqDstzfs4X5U4B9TW54SxJe3lnjLTx5UHvgGu6Pw6nFL4rHo3hnx1aeIJUtLxBa37D5eySH2966eQNE+1hg9xXzykEqzNAxdJ4ThCDgg/Wup0j4m6xpUS297GL2KLgoflb86HG4ShY9bDYOMcUZ965rQ/iDoOvFIfNNnePx5L8jP+90rpWjKjnp2IOQfxqXGxnZjSaTjHvQcg4xzTcjn261IhM0e9HXntSe1FikJkN9aaRihuCMUOSO1ADCKYad360wn5uOlArCE8GomPFPJySMUzknFMGhtJupec4qI5zQSdf34px5FJuHbrSA7Rz3oYC44pDS57U3Bznj8TikNICRTMZ6ZrF13xpo2gExz3CzXQ6QIeT+Nchd/FK/vg/wDZdmLWNeCZQHOfrTUSowcnoehXl5aadHvv7qK3UjPztgmuO1P4heerxaFbEnBBmkGGHutcLE9xqOqebfyS3OW3Orkso/Ct0n/Rm8iEKOxC4qrI6Xh+VamJdx3WqebNfPLc3KDIkk7U7wrp1utrdXtwu914Un+HtxTtRmv7a3/0eHKvw7H+EetS28tvDpKWltMrBTmRvrVImCtPU7DwZClvpN5McLmTAY9ge9efeItVj1nxBK8P3IPlPoSOK3NX8W2OkeGm0rTpftV3LFsdkBAXPesDR9Ab7KFnO1pRu3/Wktwrb3MyaNncGNfnHK47+1a2lzxS4VgRIOGRutQ22mXNrqhFwpEcf/LTsav2XhbVNWhuL2yAyshAA5Yj1rOcLo1glGne5eDDAA6CpQRxjpVG4t9W01M3mnyEKOWXgH8KsWkouLZZACpYZ2nqK5ZQaNITuTNxSUE0mag0IrgZQDuarAAE5+lXWAI7Goni8wADC/hTSvuQ1qVSpyPTPQVk6teNM5sbFXkuDy4QZwvetm4t5hC3ksqt/fboo9a1PDmhrHbGHS1BW5UrcalKM5z2XuvpW9GnfUiWrscLLbiwijCksjjO73qWxv5NGv4tSiAcxdR9a09T0eW0NxpsyP5tuxaNyfvR9jWTGm+3MT49Ca699DOvS9m01sdTeFNRtLXXLeHYsw/eADo1ZetWokiF1HGAHG2QjtjvTdC1iXRtOuLC9hafTZmyrr/yzbtV+z1GwvQ9ruxGwx81PYnmU4nHRW6zOqdy2AemK7jRPG+t+Hwlpcqt7apwA5JKr7Vyc1mqzOqPhlbAPY+9bjadMERxMpOwcY609AowjJ2PUdM8TaVq8ayQT+QxH3JztPvWivzDcMFT/EvINeZaPpc02mmRFV3Q4yB83PvWjBeavorFo9209Vk+YVlZdBTo8rO8Jx1/Co2bOK5q28fWAuYrXWIXsZZOkzHKn8BXR/I8SzwyLLC33WU9aVjBpocO9N39qTceM4pAdwyBzRYSAjios4/GnPk84ppGQBRYbELAE03dzmjGDg0ZAFJg2MJOaYw5p5FMyPU0IR1/Gc9qazZ6daM9Se/QVFd3UGn2c15dNsgt13Ox9KdtSU9CHVNRh0q2DSMDPJnyYh1c/SuPv/7T1RC2r3BijByYE6L+IribjxPeal4wg8R3JIt4ZPLt4s8YPy5A/Wu51m62xXpU7o2j3IwquU1WiuecTQxfarhlXIDlVLHNbdoi2ulQxjGWGWOOprmHlbymYnkvk/nXSO6rbwqHXJXIHeqtoa0G3M3PCiKtxdXPy/d2jIzyDW1fTIkIVAoBHpWB4clRdKmcyKP3p5qzfXluIgfOU+tZW1NcRN3smZniCYR6W+Ty424HeuPtbWOS6ijkDFGzx0rb8VapB/Z8Sod7F+o9K5qHVZPtsTQIMgYAYVrE5b6m3NpcMCukahVzyT/jXUGBRaQcqCEGK4S8v7qdnEjhccFRW4rytaQu0rk7B3pNam9RWijT1COGawmV3G4KcY61w+neINU0kypp+oSW6E8qP4q6WBt0oySdwxXFX6BNRuExt2uePxqrHM5aWOv07xxqVtDHLdkX8ZzuWTjb+FbN/qOj6xpwvtPc298q4YAdvTHauCsWU2DhuACKltbOS9uGgtX8t2Xkj+IUpwUjeF+W6Orsrtri3V5dgcHB2tkVYPHfKmuf0SxubOSeF1ZgmCfQVuJIJI1YHIPSuOpT5WdFOd0SHkYPSkGMkscADrSE4NVdRt5rqNYY5PKycsP4iPb2qIx5nYcpKOps2WkLeqsuof8AHuDmOEHhvfNbhvyqrDChJT+FVwgH1rgpfEcmlxrZ2srXVwPlVCc7T71g6p4k1q4cxTTm2x95Icrn613xhZWOSc3JnbeNtRsbXV7K7S8je5kQRz28RDKqe59a5jVrJrRxcW3z28nzAj3rnIo1aJpHyzsfvHqa6PQ9SQ27WN8SFIO0ntSkuV3R24eSrQdOQuj3ccc4glO6C4+6T2atmbSrVo33RhePvLXKGIR3DwRtvQPlSO1biX91BaPja67cfPzirauefKDi7GRCgVHbqc9T3rpUk3WUJPXAriBfTKhGRg+tdDba1H9gjWVGLAY4pIuj8Wh13hWdVtr1CD95cVpXk4aMY69wa5Lw3rkCC7SRXAJXac8CtifVbVlz5q5PvSZdaV2UPFUENxpqSyKGZDw3pzVPw9ql9ol3K2nvvgC5a3dsqw+tS+IbuE6OQJFySMA9+aw7K+FnMhkyUccn0FJIx1kexabfwazpqX1p8qMdrIeCrDqPpU2SDxXGeE9RFlqhtQ2bO9GA3ZD1zXZEkMR1I9KVjN7i7u1MI75pAM/4UGkXYbnOab1pemaaTQxNA3SmbTTic8U05FSFjrevIrzr4r6s0psvD1pLiSdg8mD1B7GvRHkEEUs7g7YULn6AZrwqS9/tfxJqOryHMRZmgJ/hbsKpbl0qfPKxWmsXuL6KxhUrDEAcDnBHWuj1TVcaRPFbrufyCvPb3qXw5bta2c1zMmZbo9+qiqerRr5rJAnDqQQtWnc6MRScNDz3zZCuCx962JnYwwkscFfWs6WzZJHRjjDdO9b62sP9nwn5nOznPQGr6GWHUucm0QB7CQMzAhz/ABVdz82MMf1qbwzBD/Z8xaIMwc8nvV6QJH91VXHpWZrVoScrnIeI7eVY4sIACazbGyaS/iRm2g9T6V0Pixi7Ww65GcCsjTYZX1OIIjEntjrVLcz9jZ6lm609RJL+8zjrxXV22l2qWMPJIKA5NYFzY3O+QGMqAeR6129roGoT2Fu6CMDYOCfaolKz0PQnRhyK7MZLSCO5UlenQ+lcd4kjCa5IQg2kfnXoEmmXhnEZVQQevauR8S6U6ao3myKHCcbTwapNs450oLYztKWIafISm45H41s+GvLTXdyoP9XyOuKz9DsLmW0uNoQop79a1vDlhcnxBFCAFaQbTu9KctVodeHhT9ndm1EuTcMABuz2rHtI9pePOcNwPau3tPCc8txIs9wixZx+7PNcrc6Z9n8UTWFszSFm2ox61jURi3TTtEktIkM++UAQxjLEniua1rXJ9cuHtLDENorYL45f8e1aniGVri7j0PT5AqA/6RMD94+n51p6N4WtLSAvMGlYjhSOBV0oJak1Gtmc5pWmwWW6ThnUck8muauZDJdTyN8xZu9enXGl2n2eRVUxjByQK87SyjZXkdm4Yj61pfUwcoWLdiwGmKGRT8/PFaegRxtqEm6IOu3uOlOt9Htxp0LeY2Wx1ra8LaDHeT3gWeRBEF59amV+h6FFQVO5FLZWpQ/uAPTFUX06FreXDkAKc11Fx4bmRSVnVh/tGsi90u4t7CaRzHgLzg9aE3axyzhBu55uYAYwyt1PQ1qmznW3iyoAIBBzVIQyGLd5TYJ9OtbLMFtYkfcG2jg01sFCjeWjF0dCsU+UOSRzjINXpAGHCEEDpir2hnZpQBwQT0/GrMxUsAVGD3FJMmtQlGVzktfkC2cUfRic1X0yP7ZayJIcPGMj3FS+JGW41RY1+URj86pWE/2a7Qv/AKtvlb6etUrGMI2kdbossdzpJhjBFxA25ue2eK9A0m/Oo6XFKxzNH8s317V5ppLSW+sIcbIZOCx6Edq7jSlOlalJbFyIrj5iPftUM2xEIx1RvFs81HnOaXdjikZsHFSjlvrqJ2phbPFLycj0prdKY9BMkNTd4FOb7vNMz9aVhGp441N9H8HXlwn3mxF+DcV5bYabGmh2lvz/AKSRJx1zXZfFm5YWem2OcJcSfMvY4IxWPZQhdYsLXA2xuF46UbHRhlrzG9qGi3FjZRyRESoIxwO3Fcys6RTJ5gw5OMEda9H1U4s8KeFODivPfG7xpcaa3CyiVckf1pxNZ1v5jidZtvs2qTAEkMd2PTNadhbT3GkoyqQoOC2a2PFunRQWkFzGoJPzSv7GoNFfdozxk5CsPwq3oKjVSmjb8I+H2vLKV2ulQJIQVx1rev8AQLO3KMiuTjnLVneEdWs7C0u1umKgfNtHU89q2dRv2kjVlhlVGGQzL19Kiw8TUalY5DxvCkYtGRFXgLwO1c/po261BzyQcYroPGMjzWUMpwFVguO9c3ZM/wDaEBHU559KtHFKcnI3L0MyTZ79DmvR9PIXSbTLY/dLkfhXmN6GBlPIGOf8a7+zWRtHtWVmA2D5fXioa1O6q/3abIL0KbtlQnn0NcR4xjxq8QPynaOtdRqcdxHJuikkifcPmA5rivEiXEOrr9ouWuC6DDv1q7HFKV0P0QlJLmIk4PPX2rV0FzF4l05mP35QvXoKxNIb/T2Q9Cp5q2JPsmo28uR+7kDcGm9zqwsvccWeswzxw3jgnH1rz7xLc/2VrdzqORvlJjgQddx6NXQ3tx5JS4kcxqyBj9MV5tqOq/274m3pgwW4xGM9TnilJJmKi+eyNXwhYrdarPc3ALNH+8PPc9a7OVo44GPc9qwPB9t/otzcMpBkJX24Na95Ink5zgDjig0rdjK1W/tbLTpmuXK7xhAFJya8/jmBiIx1JxXa63fwWmnSOZF3kfKO9cPG0BgT5vnLU0tTml2OqV0WwgQj+EHFdH4LeNLW7kbAMmBzXOSrtt4VV+Ag4NdJ4Sto5NDkaRQwkbAGfQ9qHa56CfLRNe+mUgbCMZxnNc74ikCaHOVbH41sarbRxhBEm0jqM9q5jxUpj0YseS3Sk0jhctTmM7Yoox/GwCj3rfvVT7KqtGuQgXOOc1z8UW7ULXeeAwYAetaV1d3QllSfDhn+Q+gptaGuEbc7nX6Jo1mvh23aZH8xs5w3vVW80kRyBrd8r/cPNbMDCCzhgbgKoPHuKzriT94xU5ABNZmlatLmsebXKNL4k8tkJ3OFxVjXdDOmao8cg/dSjch9ParGlobvxMgYBmLsQK6zxSLaXS7gXEYaZQShPVTiqRg6l9Dmre5F7o7RNxcw4z2yo6V1UUpl0qxuy+5+GLfQ1wMEu1o33Hdjafyrr9Fk8/QBFzmD5cfWiSGpOUHc7YSeZEkvHzKGppOfmqtpk32nTY3HSP8Ad59xVg9fbvUmb1ANgcdTTWP50cc47UmcinoITORTc/WlORRuH+TSEzH+JM6z+MtMtSQfLjZgv61U0H974jjbOQpDms74kTmTxyZAx/drhfbgVmeHL+5s9VknRt2EKlT3FDVzpoSSg2enaldPuC+rHj1rgvHLCS5gXOTs/WtuTxCLggzQLFjgbTmuX8YXcU9zbiJxuEZyB2pxRhN3NueVdR8Es2cyBQhX2Fc34cnf7Y0JJ8tkJ59a2dNuNktuGb93OgTbjjIHWuet5P7M8RXUA+8oO3+dWxU73R1/htbY6xdLehfJEI5P17V0eo6zYi2WLzmEY6bjmvONMup7m/k8xypK88fpWjKmcMASR79alK5viJaknjLVoDo8aQESEyfeHpXL2Wrr9tt9sZ44bmrHiRD9hjYqFPmetYFswFzGc9DVJanNfqdRqOtvI0saxlV6ZrqbTxVcjRrdFTawUAE+grgbmQvNISpBro7JGk06JgGIIx0oaszrrP8Ado0r/wASXk23lQMgkEVy3im/nluIZ2ABxgcVtSQtjAjY/hWL4mhkW0hBTAznnrTsciMy01W5hvkkBUH7uCOOa0bzVblg0LrHg9wvNYttbSteRJjkntWjexzJM4ZQNvek1odWHd2bl94hur/ww3nOCI12DHX0rkbB3S3aVWIYnsa3b2zFv4KBfCSytnHqM1kW1o39mIwcfMeR6UrFRT5zp9LM8Ngm24lw3zEBuOanlnm27fOfB6jNWLXSJIrKENL8pUEGkm0/a43SHmmc9Tm5jnfETD7JHuJLA8GsBMl0HPUV0PiW0EbQgSkhj0IrLs7EyahEgYc8807Eu7ZcuJnDKFlfAXAya29HmnXTU2zSAEno3Ssi+sZhJKw2nb1INb9lpU0elwMqquckfNSauzrqt+zSJHvbkJt85mHqTzWR4i1C4bTEic5G7rWlLbTphXU/XFYHiMFFjTa2PcUWONPuZ1pfSi+gLHhWFbiaktzqUaTAKgk61zloP9MQYrRgAk1ONc4Bfkin0N6Dtc9TlmB53AjYMYPtWLqdy1rps869cHFZLyPDGTHIwA6d6patrDjSDBMPnfjd7VFjKTux3hHB8VQSNzhWb6nFbd8jXUd0shy8mcZrJ8ALHNqFyZB+88vMWfYc1FJqM89zdKrbULHA9KpoVtbmMZlhYRtjcGwfwrc0bUZE+1ww/wCrdw3uOK4/J+1EEnO41uaRMF1NkY8uMD0zR0Ljs0egeD7p5I7q0YlhFmX8Sa3d3zD171yvht5LLxO1sQFN3EsY5711TDD+4qWZ6jS2D0ppf2p3NL9aQDHPANJ5ntR1OKOO9JgtTifHkRufGrGFwDJ3boAKz9LtpY57p1UuqsVyKPFs8j6/DNyjFhkY7VtaYc29xJGP3TynHvxVXSOqFFyi0io/MfPWud8RQBPLnjYBiMMO5rt3WMIMqMGuV8XJGghjVcFuc1SdzCdKUNyrYas0ejq0mWeF8pjqc1XupWbxRJPODtfBXHfiq9uCbGRO6nINPnnE9tFIyktEwyfSjqUlpc19GS4m1xUKFWlOAGrp5LBgMM449K56yvP9PsbyMjYSFJ9D3rrbkAoWJ461LkkzreF9olK5y/iexSPSFbczHzO9c7aRwrdwHZkZ5zXVeKMnR0IU7fM61zdhDJJeRhYyc1UXdmDoxi9TWv1i8yRkQcLjJ7112nqq6XbgIBlQSPwrmb7T7sI+9FVR05rvtG8MXd1pNq4nEYKDjGcUpNt6HpyhRhTTkYt2zhRjAx3Fcl4sdmngGSQBkg+tejX/AIWlim2vdgLj+7XD+OdLFreWyLJvyoycYpK5x1HR3ijm9Pd/7UjZDyAauXsm8S7yPmbrTtL05W1ZELkLtJJ9as6rpQiV/Lk+8fyq+heGdO7DxRt/sOxAIwVrJhx9gQY4rtPiDbRR+H9KiUKNsQPA5JwK5+C3jOkQkryP1qTSlOLqvQ6WAothbrv34jHPpxUFw6s4JPSu0tNNsvsFqGhyDEp4PtVW6ihS5BjhUEdiKhJmdWvC9rHl3iV1NxGM9s1R08/6cjZ6Cup8YwpJqUb7FB9hj9Ko6RZwy6vtKAqFJzWqRzKrHm2K07gq6AkZauygj22UIbkhRzWBfadbuxMakZbnmvTbfwhp7WMDAOGMSnO4ntUyu9UelVqU4RXMtzirh3L4J4Fcz4ulHmWqbT3Oa7y/8O+VfGKO4yp9ulcJ4uspY9USIuH2Dr0oVzz5um72Rk6Ukcl+29RgIea0NOs4pdXQdAORj1qno8Ep+0SImQp2n6mtbQR5d1cSSDBCY5HSqLpUouDZZvbd4RtHP07Vx+q3Zubnyz9xOo966vV78WkMshblhtWuLfGzzDyxOSfWkckoOJo6FdyQaxE6ylAqsBz6iteytJJZZWYYUnP1rG0G3dr1ZiPlGciuss8Rk5PJ60Gns21c429gFvqrLkHBBNbFykaNHPAAMMGUis3UY5f7RmEy/Of5VesoHuNOg8z5Y/NEYb1zRfQulFRlqdGt6tjr+h3kr9ZMufwrvJspKQwxn5ue+ea4LxFaR2miIVXL2+0r7cgV3kku5YGVsqYk6jp8oqHqc9WXvaDCeeOlITxxQTjkjrSChEjCfTrQMd6MDNIRSEjjvFSrd+NfIdSUiIwG7/Wt3SdKW/sL7Y4jminI2A8dO9ZOsWcdx4knuZC3mllOQfSt3Rrh7U3hixl3JbIzmkzqoTcYsq3GmXIRRgMR2FcX4yVlv7eN42Vlj544616G9zJvJzz1rA16zhvryKS4UswX196qJNSvdHC6YC07oPukc1PbiOHV/JuMC1n4Oeme1dDY6NaQR3LIjZI7n3qM6LaPborKx/fK2c81TNISTp7GbbWBgF7ASwjgfzEUdduccV6tp9nYXWlwz+WJNy4+bucVxU1hCutrIN2WQIRngiun0S6ktdLW3jI8tWOARmoY1VfLYr+MbaIeGigiVNrHgdhiuHsiUuIOeBjBrv8AVZGvdPuIp8FSnpXKR6TbAR4VhtIx81VFnJOTbLupsX84MvGO1el+GJFPhm2bOAoxk159PZxSBg27p610mmXs1ro0UERAjDHgil1OzESbpK5qa1Knyncu4g8mvO/Hfz31rIP7qj9K6q9neZdrnIxXN65YQ3YhabexTp81W2cCZz+mEf2vGQcnYat6kwZiozndViy023ivRIikMFI61M9hDK+9gxO71oudGGla5J8RHU2unIANywr/ACrBszjToVI+8wz710niW0iv5YRcAkJGAMHHaqEGmW8dvGihsA8ZNTsaU6nvs72Bwun2ykEfu16fSs67mJuMDH4U9JXaOJSeEQAflVZzulJPUVSZhVepx/iNg+qgZ5AqLQyFvpS3UKefwrS1GwgubwySKS3qDRp2m28LzsinJ4600zKL95EfEk0Uec73r1hJ1jt4VDAbY1H6V5tZ2EKXlq4Bzv7munmvJclcjHTpUpnbip3iiS5kEmoMU5wfwrzDxPdLN4jmKsWRV7etd3k+f949fWuUv9GtJb2V2VtznnDe9O+pxtlbS1MOlIMj97+8/KtXR4EbR7md1JEkjLzSnToFghiUMECFevatM20cHh5YYwQgOevPSk2d1OVqR5x4hl82/McZJih/WsqWE+UhIPznj6V10fh2xaQbhIcnJy9Wv7AsTdAlG+VcAbuKZlB8zsyp4f0uWXTRNEo2scZPWukttIityrTNvfHP92l0i2jtbcwxZCKeAT61oSQq8ioc7R71Nx1qjjojifE8aPqcbRxeWuMbh0amaCUe7W0mJ8veJFB/vDpXRa9ptvcyxeYrfIeMHFZ9volpBPFLGrhw4Od1CZinqaevoJNKuV2ZZl7/AFrf02QXGiWM+7cGUg+oxxVCe3SUNvyQQSal0RRb6MlrHkRQMdmevJ9aVzOT1NE8rSZwKjVick0u40rki9RwabkDvSbzRuPoKExJn//Z',
        legal: 45,
        estimado: 23,
        inspector: 'Elias Baeza',
        fecha_agendamiento: '12-09-2017',
        realizado_fecha: '',
        num_caso: 5556,
        estado: 2,
        realizado_dias: '',
        estadotexto: 2,
        tipo: 1,
        correo: '',
        descripcion: ''
    });
    ID_1250967109_m.add(ID_1250967109_fila);
    ID_1250967109_fila.save();
    var ID_1905377468_m = Alloy.Collections.casos;
    var ID_1905377468_fila = Alloy.createModel('casos', {
        siniestro: 'Hogar 3 (Sin evaluar)',
        tipotexto: 1,
        rut_inspector: '18.373.278-1',
        documentos: '',
        calificacion: '-1',
        compania: 'Nombre compañia',
        imagen_inspector: '',
        legal: 45,
        estimado: 23,
        inspector: 'Elias Baeza',
        fecha_agendamiento: '12-09-2017',
        realizado_fecha: '',
        num_caso: 5556,
        estado: 3,
        realizado_dias: '',
        estadotexto: 3,
        tipo: 1,
        correo: '',
        descripcion: ''
    });
    ID_1905377468_m.add(ID_1905377468_fila);
    ID_1905377468_fila.save();
    var ID_519016784_m = Alloy.Collections.casos;
    var ID_519016784_fila = Alloy.createModel('casos', {
        siniestro: 'Hogar 3 (evaluado)',
        tipotexto: 1,
        rut_inspector: '18.373.278-1',
        documentos: '',
        calificacion: 4,
        compania: 'Nombre compañia',
        imagen_inspector: '',
        legal: 45,
        estimado: 23,
        inspector: 'Elias Baeza',
        fecha_agendamiento: '12-09-2017',
        realizado_fecha: '',
        num_caso: 5556,
        estado: 3,
        realizado_dias: '',
        estadotexto: 3,
        tipo: 1,
        correo: '',
        descripcion: ''
    });
    ID_519016784_m.add(ID_519016784_fila);
    ID_519016784_fila.save();
    var ID_1029896859_m = Alloy.Collections.casos;
    var ID_1029896859_fila = Alloy.createModel('casos', {
        siniestro: 'Hogar 4',
        tipotexto: 1,
        rut_inspector: '',
        documentos: '[{"nombre":"Documento legal 1","estado":1},{"nombre":"Documento legal 2","estado":0}]',
        calificacion: '',
        compania: 'Nombre compañia',
        imagen_inspector: '',
        legal: 45,
        estimado: 23,
        inspector: '',
        fecha_agendamiento: '',
        realizado_fecha: '',
        num_caso: 5556,
        estado: 4,
        realizado_dias: '',
        estadotexto: 4,
        tipo: 1,
        correo: 'correo@creador.cl',
        descripcion: 'Envie su correo a correo@creador.cl'
    });
    ID_1029896859_m.add(ID_1029896859_fila);
    ID_1029896859_fila.save();
    var ID_1712465194_m = Alloy.Collections.casos;
    var ID_1712465194_fila = Alloy.createModel('casos', {
        siniestro: 'Hogar 5',
        tipotexto: 1,
        rut_inspector: '',
        documentos: '[{"nombre":"Descarga informe final","url":"http://www.document.cl/documento_final_1.pdf"},{"nombre":"Descarga documento adjunto 2","url":"http://www.document.cl/doc_adjunto_2.pdf"}]',
        calificacion: '',
        compania: 'Nombre compañia',
        imagen_inspector: '',
        legal: 45,
        estimado: 23,
        inspector: '',
        fecha_agendamiento: '',
        realizado_fecha: '',
        num_caso: 5556,
        estado: 5,
        realizado_dias: '',
        estadotexto: 5,
        tipo: 1,
        correo: '',
        descripcion: ''
    });
    ID_1712465194_m.add(ID_1712465194_fila);
    ID_1712465194_fila.save();
    var ID_873380466_m = Alloy.Collections.casos;
    var ID_873380466_fila = Alloy.createModel('casos', {
        siniestro: 'Fraude 1',
        tipotexto: 3,
        rut_inspector: '',
        documentos: '',
        calificacion: '',
        compania: 'Nombre compañia',
        imagen_inspector: '',
        legal: 45,
        estimado: 23,
        inspector: '',
        fecha_agendamiento: '',
        realizado_fecha: '',
        num_caso: '',
        estado: 1,
        realizado_dias: '',
        estadotexto: 1,
        tipo: 2,
        correo: '',
        descripcion: ''
    });
    ID_873380466_m.add(ID_873380466_fila);
    ID_873380466_fila.save();
    var ID_1713196526_m = Alloy.Collections.casos;
    var ID_1713196526_fila = Alloy.createModel('casos', {
        siniestro: 'Fraude 2',
        tipotexto: 3,
        rut_inspector: '',
        documentos: '[{"nombre":"Documento legal 3","estado":1},{"nombre":"Documento legal 4","estado":0}]',
        calificacion: '',
        compania: 'Nombre compañia',
        imagen_inspector: '',
        legal: 45,
        estimado: 23,
        inspector: '',
        fecha_agendamiento: '',
        realizado_fecha: '',
        num_caso: 5556,
        estado: 2,
        realizado_dias: '',
        estadotexto: 2,
        tipo: 2,
        correo: 'correo@creador.cl',
        descripcion: 'Envie su correo a correo@creador.cl'
    });
    ID_1713196526_m.add(ID_1713196526_fila);
    ID_1713196526_fila.save();
    var ID_629799338_m = Alloy.Collections.casos;
    var ID_629799338_fila = Alloy.createModel('casos', {
        siniestro: 'Fraude 3',
        tipotexto: 3,
        rut_inspector: '',
        documentos: '[{"nombre":"Descarga informe final","url":"http://www.document.cl/documento_final_1.pdf"},{"nombre":"Descarga documento adjunto 2","url":"http://www.document.cl/doc_adjunto_2.pdf"}]',
        calificacion: '',
        compania: 'Nombre compañia',
        imagen_inspector: '',
        legal: 45,
        estimado: 23,
        inspector: '',
        fecha_agendamiento: '',
        realizado_fecha: '',
        num_caso: 5556,
        estado: 3,
        realizado_dias: '',
        estadotexto: 3,
        tipo: 2,
        correo: '',
        descripcion: ''
    });
    ID_629799338_m.add(ID_629799338_fila);
    ID_629799338_fila.save();
    var ID_1535472290_m = Alloy.Collections.casos;
    var ID_1535472290_fila = Alloy.createModel('casos', {
        siniestro: 'Auto 1',
        tipotexto: 2,
        rut_inspector: '',
        documentos: '',
        calificacion: '',
        compania: 'Nombre compañia',
        imagen_inspector: '',
        legal: 45,
        estimado: 23,
        inspector: '',
        fecha_agendamiento: '',
        realizado_fecha: '',
        num_caso: '',
        estado: 1,
        realizado_dias: '',
        estadotexto: 1,
        tipo: 3,
        correo: '',
        descripcion: ''
    });
    ID_1535472290_m.add(ID_1535472290_fila);
    ID_1535472290_fila.save();
    var ID_577965363_m = Alloy.Collections.casos;
    var ID_577965363_fila = Alloy.createModel('casos', {
        siniestro: 'Auto 2',
        tipotexto: 2,
        rut_inspector: '',
        documentos: '[{"nombre":"Documento legal 3","estado":1},{"nombre":"Documento legal 4","estado":0}]',
        calificacion: '',
        compania: 'Nombre compañia',
        imagen_inspector: '',
        legal: 45,
        estimado: 23,
        inspector: '',
        fecha_agendamiento: '',
        realizado_fecha: '',
        num_caso: 5556,
        estado: 2,
        realizado_dias: '',
        estadotexto: 2,
        tipo: 3,
        correo: 'correo@creador.cl',
        descripcion: 'Envie su correo a correo@creador.cl'
    });
    ID_577965363_m.add(ID_577965363_fila);
    ID_577965363_fila.save();
    var ID_146678199_m = Alloy.Collections.casos;
    var ID_146678199_fila = Alloy.createModel('casos', {
        siniestro: 'Auto 3',
        tipotexto: 2,
        rut_inspector: '',
        documentos: '[{"nombre":"Descarga informe final","url":"http://www.document.cl/documento_final_1.pdf"},{"nombre":"Descarga documento adjunto 2","url":"http://www.document.cl/doc_adjunto_2.pdf"}]',
        calificacion: '',
        compania: 'Nombre compañia',
        imagen_inspector: '',
        legal: 45,
        estimado: 23,
        inspector: '',
        fecha_agendamiento: '',
        realizado_fecha: '',
        num_caso: 5556,
        estado: 3,
        realizado_dias: '',
        estadotexto: 3,
        tipo: 3,
        correo: '',
        descripcion: ''
    });
    ID_146678199_m.add(ID_146678199_fila);
    ID_146678199_fila.save();
    console.log('casos cargados');
    */
})();