exports.events = _.clone(Backbone.Events);
exports.ajaxkeys = {};
exports.variables = {};
exports.styles = {
	"images": {
		"androidbadge54": "/images/badge54.png",
		"androidbadge52": "/images/badge52.png",
		"androidbadge100": "/images/badge100.png",
		"androidbadge19": "/images/badge19.png",
		"androidbadge81": "/images/badge81.png",
		"pincho1": "/images/i5966E5EF4E79A0D34400962627AA0DD4.png",
		"androidbadge89": "/images/badge89.png",
		"androidbadge2": "/images/badge2.png",
		"androidbadge95": "/images/badge95.png",
		"id_17304885114": "/images/i136A2F3FAE3171B1D0663DAC94D17116.png",
		"id_17304885157": "/images/iCC9F207BEB9CE5E888C1D7BBBFAB6BF7.png",
		"androidbadge61": "/images/badge61.png",
		"id_17304885111": "/images/i412BD54D4C1F58C726A5474AB430897E.png",
		"androidbadge8": "/images/badge8.png",
		"id_17304885144": "/images/i628093B7B49694434747C34ADE3F5507.png",
		"androidbadge1": "/images/badge1.png",
		"id_17304885149": "/images/iBE8EEC7E7193BB226FA1C9CEE9D5893C.png",
		"androidbadge45": "/images/badge45.png",
		"androidbadge62": "/images/badge62.png",
		"id_17304885168": "/images/i783C9BB1181A7F71ADFA56527055AD91.png",
		"androidbadge71": "/images/badge71.png",
		"androidbadge92": "/images/badge92.png",
		"androidbadge37": "/images/badge37.png",
		"androidbadge69": "/images/badge69.png",
		"id_17304885150": "/images/i7DA1F286DB7815A661642602DE1EAB24.png",
		"androidbadge38": "/images/badge38.png",
		"androidbadge86": "/images/badge86.png",
		"androidbadge36": "/images/badge36.png",
		"androidbadge14": "/images/badge14.png",
		"id_17304885131": "/images/i42E95FF51C86FA43CECB305483E011C5.png",
		"androidbadge13": "/images/badge13.png",
		"androidbadge53": "/images/badge53.png",
		"testcheck": "/images/i99209DB57B68F536E7F4B4D1FF07BD2D.png",
		"id_17304885163": "/images/iD36767E107126E889B6581A0FE3025DE.png",
		"id_17304885164": "/images/i1427BEB8000B25CEB0A57DB528A986C3.png",
		"androidbadge87": "/images/badge87.png",
		"androidbadge58": "/images/badge58.png",
		"androidbadge17": "/images/badge17.png",
		"id_17304885140": "/images/iF6E2658A5AC3394D23F32BEACE29AD31.png",
		"androidbadge63": "/images/badge63.png",
		"id_17304885122": "/images/iF05CCD47129C757F30BD2FAF9AADBF62.png",
		"androidbadge68": "/images/badge68.png",
		"id_17304885138": "/images/i2E0689EFC8DE0330B64EE6A3D704FAB3.png",
		"androidbadge46": "/images/badge46.png",
		"id_1730488514": "/images/i1B20B98113AFCDA632D16051C6A64A98.png",
		"androidbadge91": "/images/badge91.png",
		"id_17304885110": "/images/i27966B0E1612B95C0ED9194AFE80E04F.png",
		"id_1730488511": "/images/i0C2B7820FCC1199737EED8B2AA09C3EF.png",
		"id_17304885167": "/images/i1DF709EEBBF149FD314750E973E857CF.png",
		"androidbadge47": "/images/badge47.png",
		"id_17304885130": "/images/i6B3F36EBE4877FD179EDB51408EEF261.png",
		"androidbadge34": "/images/badge34.png",
		"id_17304885123": "/images/iD9E1D9978C5953680AD1EC68F5BE7157.png",
		"id_17304885154": "/images/iD32F5B479234D1D43BC10F794AFB3B3A.png",
		"id_17304885160": "/images/i7BD3EBC35955B8C79154C2697E44979E.png",
		"androidbadge26": "/images/badge26.png",
		"androidbadge12": "/images/badge12.png",
		"androidbadge85": "/images/badge85.png",
		"androidbadge76": "/images/badge76.png",
		"id_17304885161": "/images/i72C0C96100A2E326AC19A6A8132E4676.png",
		"androidbadge44": "/images/badge44.png",
		"id_17304885145": "/images/iC9882C24D721E4991C74DF65F07567F8.png",
		"androidbadge83": "/images/badge83.png",
		"id_17304885137": "/images/i812D913B0C7277A1BF6E62841493E702.png",
		"pincho_casa": "/images/iFC793A64AD38CFB17552E0CF4AE60F0A.png",
		"androidbadge88": "/images/badge88.png",
		"androidbadge84": "/images/badge84.png",
		"id_17304885115": "/images/iDB18D1C7F9E9CCE22988E4475CC0BF0B.png",
		"androidbadge65": "/images/badge65.png",
		"androidbadge98": "/images/badge98.png",
		"androidbadge22": "/images/badge22.png",
		"id_17304885142": "/images/i3FAFCFD4D5FC18D32AB92FB432328B00.png",
		"id_17304885152": "/images/i5F8A3633B6007A6D7D569ACFFD4C3B20.png",
		"androidbadge23": "/images/badge23.png",
		"androidbadge15": "/images/badge15.png",
		"id_17304885141": "/images/iDB8813219AD0551384CF6FC06D49E7DE.png",
		"id_17304885169": "/images/i7E57E509944C48F63C08B9E3171E3D04.png",
		"androidbadge24": "/images/badge24.png",
		"id_17304885124": "/images/iF4345EA5EF7DD4F061618340B0447EB3.png",
		"androidbadge3": "/images/badge3.png",
		"androidbadge75": "/images/badge75.png",
		"androidbadge99": "/images/badge99.png",
		"id_17304885159": "/images/i9307083360A85A604E47C2C428B3D585.png",
		"id_17304885116": "/images/iEE4918503FDEB086A6D00E1D5D919B81.png",
		"id_17304885127": "/images/i1AC45348AD216F5FBE4D190C98429756.png",
		"linea": "/images/iA880A9D4A9876891BD483C4DFDA3803A.png",
		"id_1730488513": "/images/iC05A2344FAAF963A0E04860A2CEE8944.png",
		"id_17304885136": "/images/i7F4D674587BDE17C2B5F9D2FC0485F23.png",
		"androidbadge97": "/images/badge97.png",
		"id_17304885162": "/images/iDC4E12FDF7EBA08B33772538F82B1B5C.png",
		"androidbadge10": "/images/badge10.png",
		"id_17304885156": "/images/i532BD4B2142939EE158BC4F656DF2FF4.png",
		"id_17304885148": "/images/i4A50FFFE6B28616E9F8AFFFB27D42710.png",
		"id_17304885171": "/images/iA99262010F403D9251E58C6FA3992DDF.png",
		"androidbadge49": "/images/badge49.png",
		"id_1730488515": "/images/i90FF546E0155BCA08A28D134969678C6.png",
		"id_17304885143": "/images/i788B1FE6E1CD08AB3FBA0BA1D64C68DE.png",
		"id_17304885155": "/images/i8979543C97944EFEC4ABB6A7DA23E468.png",
		"androidbadge48": "/images/badge48.png",
		"androidbadge29": "/images/badge29.png",
		"androidbadge4": "/images/badge4.png",
		"cerrar": "/images/i0DC188A5563AA098110726CB1DEFB9B5.png",
		"id_1730488518": "/images/i3E3C563B05D8F1C7342BBC772F3AFE8A.png",
		"id_17304885129": "/images/iD3B32CD4A486DE94286175705DC5E2F6.png",
		"id_17304885119": "/images/iCF48201A56EB40729FDB6476DA0FF7B5.png",
		"androidbadge5": "/images/badge5.png",
		"androidbadge73": "/images/badge73.png",
		"androidbadge40": "/images/badge40.png",
		"id_17304885139": "/images/i6374F3A978AAB3F0C3F3F56BA5880646.png",
		"androidbadge59": "/images/badge59.png",
		"androidbadge25": "/images/badge25.png",
		"androidbadge67": "/images/badge67.png",
		"androidbadge43": "/images/badge43.png",
		"androidbadge7": "/images/badge7.png",
		"androidbadge11": "/images/badge11.png",
		"androidbadge31": "/images/badge31.png",
		"id_1730488517": "/images/i2A382C462E807E5A786AB331AB866BAD.png",
		"id_17304885158": "/images/i5F4FB5ABA23FFD8AF2F50AA3A8CE9488.png",
		"androidbadge90": "/images/badge90.png",
		"id_1730488519": "/images/i6279EE5DE8B1444086DC4869B1A789AF.png",
		"id_1730488512": "/images/iABC8E8EBB1EB10A3F51BA46263C83113.png",
		"id_17304885135": "/images/i9B5BF672DB6B15F1CE1F4689772255FA.png",
		"androidbadge33": "/images/badge33.png",
		"androidbadge56": "/images/badge56.png",
		"androidbadge50": "/images/badge50.png",
		"logo": "/images/i14EFE990940EC7C984099DFC0E2812DB.png",
		"androidbadge74": "/images/badge74.png",
		"id_17304885151": "/images/i6281471DD781D02DCD82A6D9D812BEDC.png",
		"id_17304885125": "/images/i7560CF2AFAF67BA2A0505874345861EE.png",
		"androidbadge6": "/images/badge6.png",
		"id_17304885166": "/images/iD738B5BE6FADC27C9A3BD2E727DFE06F.png",
		"androidbadge77": "/images/badge77.png",
		"id_17304885117": "/images/iACF462EBF90E5753FB66F5B2013AFCDE.png",
		"id_17304885113": "/images/i78F0F76C360F0A03A95597D25CE6FEEB.png",
		"androidbadge27": "/images/badge27.png",
		"androidbadge16": "/images/badge16.png",
		"androidbadge41": "/images/badge41.png",
		"id_17304885128": "/images/i7CCA9D8543E0361AA98EE3F4FFCC1E80.png",
		"androidbadge18": "/images/badge18.png",
		"id_17304885120": "/images/iA32F4BE4FC271ABB54F34697C342B1B0.png",
		"androidbadge60": "/images/badge60.png",
		"androidbadge35": "/images/badge35.png",
		"androidbadge79": "/images/badge79.png",
		"androidbadge30": "/images/badge30.png",
		"id_796862108": "/images/i99209DB57B68F536E7F4B4D1FF07BD2D.png",
		"androidbadge96": "/images/badge96.png",
		"id_17304885146": "/images/i0759428431BFBBCE274ACC5E95405EBE.png",
		"androidbadge9": "/images/badge9.png",
		"androidbadge66": "/images/badge66.png",
		"androidbadge21": "/images/badge21.png",
		"androidbadge64": "/images/badge64.png",
		"id_17304885170": "/images/iDDF9DF0972A849CFE0F63C27D1B29A88.png",
		"androidbadge80": "/images/badge80.png",
		"androidbadge70": "/images/badge70.png",
		"id_17304885132": "/images/i7540598193ED0110AB96D64579CA99DB.png",
		"androidbadge57": "/images/badge57.png",
		"androidbadge72": "/images/badge72.png",
		"id_17304885172": "/images/i1565780F3B0554590E6173EA6090F560.png",
		"id_17304885112": "/images/i0DBA6E0B7CA67FD9E918C8AAFCB844C0.png",
		"id_17304885147": "/images/iC47741F97D23A70CEA2C98DD007F5C84.png",
		"androidbadge94": "/images/badge94.png",
		"id_17304885133": "/images/i30E099319615261CDB332D437485307B.png",
		"androidbadge42": "/images/badge42.png",
		"androidbadge78": "/images/badge78.png",
		"androidbadge51": "/images/badge51.png",
		"sin_asignar": "/images/i4A22DEA00A2F2DEEA9163E8432A3DD40.png",
		"id_17304885126": "/images/i027A685EE54B3C45F5A78D93A9761D93.png",
		"id_17304885165": "/images/i6F0BB0AC881EF8504F6554106E2B644F.png",
		"id_17304885153": "/images/i4FFB1988556522E7E3C3AF210557E317.png",
		"androidbadge55": "/images/badge55.png",
		"id_1730488516": "/images/i0AB0AC5EC3987396034DBEDA61CBEBB6.png",
		"id_17304885118": "/images/i21B6879F90FF416F20EF18DC6F7C523B.png",
		"androidbadge82": "/images/badge82.png",
		"androidbadge39": "/images/badge39.png",
		"fondopantalla": "/images/i4DDC620865F38C5A3439087EED99348F.png",
		"androidbadge28": "/images/badge28.png",
		"androidbadge32": "/images/badge32.png",
		"androidbadge20": "/images/badge20.png",
		"id_17304885134": "/images/i3F9D3AC4B04FA569047B34579060FFD2.png",
		"androidbadge93": "/images/badge93.png",
		"id_17304885121": "/images/i0993DAB3930C4D3ED8CB725CA4B5F989.png"
	},
	"classes": {
		"#ID_1203049884": {
			"title": "L('x696679517_traducir','Mexico')"
		},
		"#ID_429943992": {
			"text": "L('x3957652582_traducir','days')"
		},
		"#ID_1403271616": {
			"hintText": "L('x267280490_traducir','repeat your password')"
		},
		"#ID_1865141254": {
			"hintText": "L('x1049143614_traducir','write your email')"
		},
		"#ID_1737225565": {
			"title": "L('x3947872571_traducir','CASES')"
		},
		"ID_1077472615": {
			"height": "120dp",
			"scaleMode": "fitXY",
			"autoStart": "true",
			"width": "120dp",
			"top": "5dp",
			"file": "feliz120.json",
			"disableHardwareAcceleration": "true"
		},
		"#ID_1807524742": {
			"font": {
				"fontFamily": "iconuadjusts",
				"fontSize": "12dp"
			}
		},
		"#ID_205372915": {
			"text": "L('x807033745_traducir','Received')"
		},
		"est13": {
			"color": "#4d4d4d",
			"font": {
				"fontFamily": "Roboto-Regular",
				"fontSize": "18dp"
			}
		},
		"#ID_375356980": {
			"title": "L('x3719714762_traducir','inicio')"
		},
		"#ID_1823458956": {
			"text": "L('x1818803644_traducir','QUALIFIED')"
		},
		"#ID_897186323": {
			"text": "L('x642995056_traducir','Email')"
		},
		"#ID_70014429": {
			"text": "L('x3880041433_traducir','Unassigned Inspector')"
		},
		"#ID_1477419901": {
			"hintText": "L('x3545716984_traducir','your email')"
		},
		"#ID_1304125439": {
			"text": "L('x2733925026_traducir','SIGN UP')"
		},
		"#ID_1394908684": {
			"text": "L('x211534962','0000')"
		},
		"#ID_138747237": {
			"titulo": "L('x3785008151_traducir','CAR')",
			"nrofinal": "L('x1842515611','3')",
			"nroinicial": "L('x1842515611','3')",
			"fondo": "L('x1602558431_traducir','fondoazul')",
			"top": "L('x4108050209','0')",
			"modal": "L('x2212294583','1')"
		},
		"#ID_1555358288": {
			"text": "L('x3434357891_traducir','Password')"
		},
		"#ID_1448397919": {
			"hintText": "L('x1239582266_traducir','write your password')"
		},
		"#ID_212927648": {
			"text": "L('x2752414257_traducir','Middle name')"
		},
		"#ID_1753341304": {
			"text": "L('x1561639535_traducir','Inspection date:')"
		},
		"#ID_739966299": {
			"hintText": "L('x2460805359_traducir','write your name')"
		},
		"#ID_617532473": {
			"text": "L('x1221698487_traducir','Repeat email')"
		},
		"#ID_727666593": {
			"text": "L('x3980808842_traducir','Time remaining:')"
		},
		"ID_9946986": {
			"height": "53dp",
			"scaleMode": "fitXY",
			"left": "10dp",
			"width": "32dp",
			"file": "estrella_gris.json",
			"disableHardwareAcceleration": "true"
		},
		"#ID_1454886879": {
			"text": "L('x1996753865_traducir','correo')"
		},
		"#ID_173048851": {
			"images": ["/images/i0C2B7820FCC1199737EED8B2AA09C3EF.png", "/images/iABC8E8EBB1EB10A3F51BA46263C83113.png", "/images/iC05A2344FAAF963A0E04860A2CEE8944.png", "/images/i1B20B98113AFCDA632D16051C6A64A98.png", "/images/i90FF546E0155BCA08A28D134969678C6.png", "/images/i0AB0AC5EC3987396034DBEDA61CBEBB6.png", "/images/i2A382C462E807E5A786AB331AB866BAD.png", "/images/i3E3C563B05D8F1C7342BBC772F3AFE8A.png", "/images/i6279EE5DE8B1444086DC4869B1A789AF.png", "/images/i27966B0E1612B95C0ED9194AFE80E04F.png", "/images/i412BD54D4C1F58C726A5474AB430897E.png", "/images/i0DBA6E0B7CA67FD9E918C8AAFCB844C0.png", "/images/i78F0F76C360F0A03A95597D25CE6FEEB.png", "/images/i136A2F3FAE3171B1D0663DAC94D17116.png", "/images/iDB18D1C7F9E9CCE22988E4475CC0BF0B.png", "/images/iEE4918503FDEB086A6D00E1D5D919B81.png", "/images/iACF462EBF90E5753FB66F5B2013AFCDE.png", "/images/i21B6879F90FF416F20EF18DC6F7C523B.png", "/images/iCF48201A56EB40729FDB6476DA0FF7B5.png", "/images/iA32F4BE4FC271ABB54F34697C342B1B0.png", "/images/i0993DAB3930C4D3ED8CB725CA4B5F989.png", "/images/iF05CCD47129C757F30BD2FAF9AADBF62.png", "/images/iD9E1D9978C5953680AD1EC68F5BE7157.png", "/images/iF4345EA5EF7DD4F061618340B0447EB3.png", "/images/i7560CF2AFAF67BA2A0505874345861EE.png", "/images/i027A685EE54B3C45F5A78D93A9761D93.png", "/images/i1AC45348AD216F5FBE4D190C98429756.png", "/images/i7CCA9D8543E0361AA98EE3F4FFCC1E80.png", "/images/iD3B32CD4A486DE94286175705DC5E2F6.png", "/images/i6B3F36EBE4877FD179EDB51408EEF261.png", "/images/i42E95FF51C86FA43CECB305483E011C5.png", "/images/i7540598193ED0110AB96D64579CA99DB.png", "/images/i30E099319615261CDB332D437485307B.png", "/images/i3F9D3AC4B04FA569047B34579060FFD2.png", "/images/i9B5BF672DB6B15F1CE1F4689772255FA.png", "/images/i7F4D674587BDE17C2B5F9D2FC0485F23.png", "/images/i812D913B0C7277A1BF6E62841493E702.png", "/images/i2E0689EFC8DE0330B64EE6A3D704FAB3.png", "/images/i6374F3A978AAB3F0C3F3F56BA5880646.png", "/images/iF6E2658A5AC3394D23F32BEACE29AD31.png", "/images/iDB8813219AD0551384CF6FC06D49E7DE.png", "/images/i3FAFCFD4D5FC18D32AB92FB432328B00.png", "/images/i788B1FE6E1CD08AB3FBA0BA1D64C68DE.png", "/images/i628093B7B49694434747C34ADE3F5507.png", "/images/iC9882C24D721E4991C74DF65F07567F8.png", "/images/i0759428431BFBBCE274ACC5E95405EBE.png", "/images/iC47741F97D23A70CEA2C98DD007F5C84.png", "/images/i4A50FFFE6B28616E9F8AFFFB27D42710.png", "/images/iBE8EEC7E7193BB226FA1C9CEE9D5893C.png", "/images/i7DA1F286DB7815A661642602DE1EAB24.png", "/images/i6281471DD781D02DCD82A6D9D812BEDC.png", "/images/i5F8A3633B6007A6D7D569ACFFD4C3B20.png", "/images/i4FFB1988556522E7E3C3AF210557E317.png", "/images/iD32F5B479234D1D43BC10F794AFB3B3A.png", "/images/i8979543C97944EFEC4ABB6A7DA23E468.png", "/images/i532BD4B2142939EE158BC4F656DF2FF4.png", "/images/iCC9F207BEB9CE5E888C1D7BBBFAB6BF7.png", "/images/i5F4FB5ABA23FFD8AF2F50AA3A8CE9488.png", "/images/i9307083360A85A604E47C2C428B3D585.png", "/images/i7BD3EBC35955B8C79154C2697E44979E.png", "/images/i72C0C96100A2E326AC19A6A8132E4676.png", "/images/iDC4E12FDF7EBA08B33772538F82B1B5C.png", "/images/iD36767E107126E889B6581A0FE3025DE.png", "/images/i1427BEB8000B25CEB0A57DB528A986C3.png", "/images/i6F0BB0AC881EF8504F6554106E2B644F.png", "/images/iD738B5BE6FADC27C9A3BD2E727DFE06F.png", "/images/i1DF709EEBBF149FD314750E973E857CF.png", "/images/i783C9BB1181A7F71ADFA56527055AD91.png", "/images/i7E57E509944C48F63C08B9E3171E3D04.png", "/images/iDDF9DF0972A849CFE0F63C27D1B29A88.png", "/images/iA99262010F403D9251E58C6FA3992DDF.png", "/images/i1565780F3B0554590E6173EA6090F560.png"]
		},
		"est10": {
			"color": "#ee7f7f",
			"font": {
				"fontFamily": "Roboto-Medium",
				"fontSize": "16dp"
			}
		},
		"#ID_726731608": {
			"text": "L('x1106383585_traducir','AGREE')"
		},
		"ID_216564626": {
			"height": "53dp",
			"scaleMode": "fitXY",
			"left": "10dp",
			"width": "32dp",
			"file": "estrella_gris.json",
			"disableHardwareAcceleration": "true"
		},
		"est11": {
			"color": "#4d4d4d",
			"font": {
				"fontFamily": "Roboto-Medium",
				"fontSize": "16dp"
			}
		},
		"#ID_561134970": {
			"text": "L('x2087673718_traducir','Assignment in process')"
		},
		"fondoplomo": {
			"backgroundColor": "#e6e6e6",
			"font": {
				"fontSize": "12dp"
			}
		},
		"#inicio": {
			"title": "L('x3719714762_traducir','inicio')"
		},
		"#ID_97386118": {
			"text": "L('x1291815763_traducir','MM/DD/YYYY')"
		},
		"#ID_1324400464": {
			"hintText": "L('x2654060152_traducir','xxxx-xxxx')"
		},
		"ID_892385145": {
			"height": "53dp",
			"scaleMode": "fitXY",
			"width": "32dp",
			"file": "estrella_azul.json",
			"disableHardwareAcceleration": "true"
		},
		"#ID_821037248": {
			"headerTitle": "L('x393297498_traducir','titulo')"
		},
		"#ID_1007522093": {
			"title": "L('x2490715067_traducir','Chile')"
		},
		"#ID_1596320302": {
			"text": "L('x3913325165_traducir','1/4')"
		},
		"#ID_1542218451": {
			"text": "L('x3353174397_traducir','forgot password?')"
		},
		"#ID_474926121": {
			"text": "L('x211534962','0000')"
		},
		"#ID_724433026": {
			"text": "L('x1425976992_traducir','Emails change authorization text')"
		},
		"#ID_423384131": {
			"hintText": "L('x3339416476_traducir','your password')"
		},
		"#ID_827969565": {
			"text": "L('x3412399334_traducir','Inspector will arrive at:')"
		},
		"#ID_200092270": {
			"text": "L('x830164967_traducir','Last name')"
		},
		"ID_1534022184": {
			"height": "53dp",
			"scaleMode": "fitXY",
			"left": "10dp",
			"width": "32dp",
			"file": "estrella_azul.json",
			"disableHardwareAcceleration": "true"
		},
		"#ID_180599748": {
			"text": "L('x512840298_traducir','Proin feuguat aliquam tortor id hendreirit urna semper pulvina.')"
		},
		"#ID_1375338207": {
			"text": "L('x3980808842_traducir','Time remaining:')"
		},
		"#ID_975590590": {
			"text": "L('x269868672_traducir','Birthdate')"
		},
		"#ID_665853240": {
			"text": "L('x3980808842_traducir','Time remaining:')"
		},
		"#ID_1045139224": {
			"paso": "L('x450215437','2')",
			"titulo5": "L('x951154001_traducir','End')",
			"titulo1": "L('x807033745_traducir','Received')",
			"titulo4": "L('x1018769216_traducir','Evaluating')",
			"titulo3": "L('x4262956787_traducir','Inspected')",
			"titulo2": "L('x807001066_traducir','Scheduled')"
		},
		"#ID_925787205": {
			"paso": "L('x2212294583','1')",
			"titulo5": "L('x951154001_traducir','End')",
			"titulo1": "L('x807033745_traducir','Received')",
			"titulo4": "L('x1018769216_traducir','Evaluating')",
			"titulo3": "L('x4262956787_traducir','Inspected')",
			"titulo2": "L('x807001066_traducir','Scheduled')"
		},
		"#ID_1926115404": {
			"titulo": "L('x3950563313_traducir','Description')"
		},
		"#ID_58480005": {
			"font": {
				"fontFamily": "iconuadjusts",
				"fontSize": "12dp"
			}
		},
		"est99": {
			"color": "#000000",
			"font": {
				"fontSize": "16dp"
			}
		},
		"#ID_1764020484": {
			"text": "L('x1083036559_traducir','See inspector at the map')"
		},
		"#ID_1807547329": {
			"title": "L('x3719714762_traducir','inicio')"
		},
		"#ID_173279620": {
			"text": "L('x3950563313_traducir','Description')"
		},
		"#ID_1200956051": {
			"text": "L('x3021917836_traducir','WE HAVE RECEIVED YOUR CASE!')"
		},
		"#ID_787932640": {
			"text": "L('x4166562134_traducir','Inspectors name')"
		},
		"#ID_1560069116": {
			"hintText": "L('x1350088082_traducir','write middle name')"
		},
		"#ID_24942151": {
			"text": "L('x2614953286_traducir','Edit contact')"
		},
		"est7": {
			"color": "#2d9edb",
			"font": {
				"fontFamily": "Roboto-Bold",
				"fontSize": "16dp"
			}
		},
		"#ID_1229323263": {
			"text": "L('x807001066_traducir','Scheduled')"
		},
		"#ID_573493621": {
			"valor": "L('x3810221188_traducir','car3')"
		},
		"ID_1922145361": {
			"height": "53dp",
			"scaleMode": "fitXY",
			"left": "10dp",
			"width": "32dp",
			"file": "estrella_azul.json",
			"disableHardwareAcceleration": "true"
		},
		"est2": {
			"color": "#000000",
			"font": {
				"fontFamily": "Roboto-Medium",
				"fontSize": "16dp"
			}
		},
		"#ID_1074340138": {
			"paso": "L('x1842515611','3')",
			"titulo1": "L('x807033745_traducir','Received')",
			"titulo3": "L('x951154001_traducir','End')",
			"titulo2": "L('x1018769216_traducir','Evaluating')"
		},
		"est4": {
			"color": "#848484",
			"font": {
				"fontFamily": "Roboto-Bold",
				"fontSize": "14dp"
			}
		},
		"#ID_1456694609": {
			"text": "L('x1709644991_traducir','Repeat phone number')"
		},
		"#ID_1655304175": {
			"paso": "L('x450215437','2')",
			"titulo1": "L('x807033745_traducir','Received')",
			"titulo3": "L('x951154001_traducir','End')",
			"titulo2": "L('x1018769216_traducir','Evaluating')"
		},
		"#ID_1915447013": {
			"text": "L('x807001066_traducir','Scheduled')"
		},
		"#iconoContent-paste": {
			"font": {
				"fontFamily": "iconuadjusts",
				"fontSize": "14dp"
			}
		},
		"#ID_1721022077": {
			"titulo": "L('x1195630948_traducir','HOME')",
			"nrofinal": "L('x2226203566','5')",
			"nroinicial": "L('x1842515611','3')",
			"fondo": "L('x1602558431_traducir','fondoazul')",
			"top": "L('x4108050209','0')",
			"modal": "L('x2212294583','1')"
		},
		"#ID_393903472": {
			"title": "L('x293014778_traducir','TOS')"
		},
		"#ID_166052581": {
			"text": "L('x555777092_traducir','Date Scheduling:')"
		},
		"ID_1302126390": {
			"height": "53dp",
			"scaleMode": "fitXY",
			"left": "10dp",
			"width": "32dp",
			"file": "estrella_gris.json",
			"disableHardwareAcceleration": "true"
		},
		"#ID_507654472": {
			"paso": "L('x2212294583','1')",
			"titulo1": "L('x807033745_traducir','Received')",
			"titulo3": "L('x951154001_traducir','End')",
			"titulo2": "L('x1018769216_traducir','Evaluating')"
		},
		"ID_272445538": {
			"height": "53dp",
			"scaleMode": "fitXY",
			"width": "32dp",
			"file": "estrella_gris.json",
			"disableHardwareAcceleration": "true"
		},
		"#ID_1753204810": {
			"paso": "L('x1842515611','3')",
			"titulo1": "L('x807033745_traducir','Received')",
			"titulo3": "L('x951154001_traducir','End')",
			"titulo2": "L('x1018769216_traducir','Evaluating')"
		},
		"est1": {
			"color": "#838383",
			"font": {
				"fontFamily": "Roboto-Medium",
				"fontSize": "13dp"
			}
		},
		"est14": {
			"color": "#8ce5bd",
			"font": {
				"fontFamily": "Roboto-Medium",
				"fontSize": "12dp"
			}
		},
		"est16": {
			"color": "#a0a1a3",
			"font": {
				"fontFamily": "Roboto-Light",
				"fontSize": "16dp"
			}
		},
		"#ID_689069073": {
			"paso": "L('x4088798008','4')",
			"titulo5": "L('x951154001_traducir','End')",
			"titulo1": "L('x807033745_traducir','Received')",
			"titulo4": "L('x1018769216_traducir','Evaluating')",
			"titulo3": "L('x4262956787_traducir','Inspected')",
			"titulo2": "L('x807001066_traducir','Scheduled')"
		},
		"#ID_1962171680": {
			"text": "L('x807001066_traducir','Scheduled')"
		},
		"fondoverde": {
			"backgroundColor": "#8ce5bd",
			"font": {
				"fontSize": "12dp"
			}
		},
		"#ID_1710161767": {
			"text": "L('x448113098_traducir','00:00 min')"
		},
		"ID_259517590": {
			"height": "53dp",
			"scaleMode": "fitXY",
			"left": "10dp",
			"width": "32dp",
			"file": "estrella_gris.json",
			"disableHardwareAcceleration": "true"
		},
		"#ID_810208344": {
			"hintText": "L('x1757887940_traducir','empty')"
		},
		"#ID_1469387549": {
			"font": {
				"fontFamily": "iconuadjusts",
				"fontSize": "14dp"
			}
		},
		"#ID_115378316": {
			"paso": "L('x2226203566','5')",
			"titulo5": "L('x951154001_traducir','End')",
			"titulo1": "L('x807033745_traducir','Received')",
			"titulo4": "L('x1018769216_traducir','Evaluating')",
			"titulo3": "L('x4262956787_traducir','Inspected')",
			"titulo2": "L('x807001066_traducir','Scheduled')"
		},
		"#ID_1631013432": {
			"text": "L('x3980808842_traducir','Time remaining:')"
		},
		"est15": {
			"color": "#7c7c7c",
			"font": {
				"fontSize": "17dp"
			}
		},
		"#ID_717863402": {
			"titulo": "L('x1195630948_traducir','HOME')",
			"nrofinal": "L('x2226203566','5')",
			"nroinicial": "L('x4088798008','4')",
			"fondo": "L('x1602558431_traducir','fondoazul')",
			"top": "L('x4108050209','0')",
			"modal": "L('x2212294583','1')"
		},
		"est5": {
			"color": "#a0a1a3",
			"font": {
				"fontFamily": "Roboto-Light",
				"fontSize": "13dp"
			}
		},
		"#ID_853575261": {
			"text": "L('x3957652582_traducir','days')"
		},
		"#ID_1341026539": {
			"title": "L('x3719714762_traducir','inicio')"
		},
		"#ID_555212209": {
			"text": "L('x3377813129_traducir','ETA:')"
		},
		"#ID_114426975": {
			"text": "L('x146107193_traducir','WE ARE DONE')"
		},
		"#ID_744885958": {
			"text": "L('x2903793082_traducir','rut')"
		},
		"#ID_1893708853": {
			"text": "L('x2912171436_traducir','descauto2')"
		},
		"#ID_1756046703": {
			"text": "L('x4262956787_traducir','Inspected')"
		},
		"est12": {
			"color": "#2d9edb",
			"font": {
				"fontFamily": "Roboto-Regular",
				"fontSize": "14dp"
			}
		},
		"est9": {
			"font": {
				"fontFamily": "Roboto-Regular",
				"fontSize": "12dp"
			}
		},
		"#ID_1267020280": {
			"text": "L('x2435937108_traducir','PART 1: Country')"
		},
		"#ID_156639973": {
			"text": "L('x2968366014','10.745.986-8')"
		},
		"est8": {
			"color": "#4d4d4d",
			"font": {
				"fontFamily": "Roboto-Regular",
				"fontSize": "16dp"
			}
		},
		"#ID_1774393411": {
			"title": "L('x3188872213_traducir','PROFILE')"
		},
		"#ID_224613881": {
			"title": "L('x1273338369_traducir','Estados Unidos')"
		},
		"#ID_48861286": {
			"text": "L('x2630807802_traducir','Country')"
		},
		"#ID_1091400848": {
			"text": "L('x3131041319_traducir','Code identifier')"
		},
		"#ID_1558362882": {
			"text": "L('x2089206772_traducir','SAVE CHANGES')"
		},
		"est6": {
			"color": "#ffffff",
			"font": {
				"fontSize": "16dp"
			}
		},
		"#ID_1690731552": {
			"hintText": "L('x1356888143_traducir','repeat email')"
		},
		"fondoamarillo": {
			"backgroundColor": "#f8da54",
			"font": {
				"fontSize": "12dp"
			}
		},
		"ID_1518069056": {
			"height": "53dp",
			"scaleMode": "fitXY",
			"left": "10dp",
			"width": "32dp",
			"file": "estrella_azul.json",
			"disableHardwareAcceleration": "true"
		},
		"#ID_1370796037": {
			"title": "L('x293014778_traducir','TOS')"
		},
		"#ID_1232718239": {
			"text": "L('x2464439331_traducir','birthdate')"
		},
		"#ID_838190276": {
			"text": "L('x3015113473_traducir','Clients name')"
		},
		"#ID_208439963": {
			"text": "L('x1003998638_traducir','I agree')"
		},
		"est3": {
			"color": "#c8c7cc",
			"font": {
				"fontFamily": "Roboto-Medium",
				"fontSize": "16dp"
			}
		},
		"ID_1714144029": {
			"height": "53dp",
			"scaleMode": "fitXY",
			"left": "10dp",
			"width": "32dp",
			"file": "estrella_azul.json",
			"disableHardwareAcceleration": "true"
		},
		"#ID_1058664237": {
			"text": "L('x3505507568_traducir','Legal ETA:')"
		},
		"#ID_904513478": {
			"hintText": "L('x1768613553_traducir','write email')"
		},
		"#ID_318754451": {
			"text": "L('x1046092454_traducir','we have received your case, be alert for the next steps, some of them needs your information')"
		},
		"#ID_454637771": {
			"title": "L('x3188872213_traducir','PROFILE')"
		},
		"#ID_1083616020": {
			"hintText": "L('x2654060152_traducir','xxxx-xxxx')"
		},
		"#ID_925916927": {
			"hintText": "L('x3573033924_traducir','write last name')"
		},
		"#ID_745493547": {
			"hintText": "L('x1983234681_traducir','repeat your email')"
		},
		"#ID_847416265": {
			"title": "L('x3947872571_traducir','CASES')"
		}
	}
};
exports.fontello = {
	"adjust": {
		"CODES": {
			"info-outline": "\uE804",
			"verified-user": "\uE808",
			"flecha": "\uE803",
			"list": "\uE805",
			"account-circle": "\uE800",
			"check-circle": "\uE801",
			"perm-identity": "\uE806",
			"content-paste": "\uE802",
			"timelapse": "\uE807"
		},
		"POSTSCRIPT": "iconuadjusts",
		"TTF": "/Applications/CreadorOPEN/CreadorOPEN.app/Contents/MacOS/webapps/ROOT/WEB-INF/lucee/temp/_font/fontello-d94d26c7/font/iconuadjusts.ttf"
	},
	"uadjusts": {
		"CODES": {
			"info-outline": "\uE804",
			"verified-user": "\uE808",
			"flecha": "\uE803",
			"list": "\uE805",
			"account-circle": "\uE800",
			"check-circle": "\uE801",
			"perm-identity": "\uE806",
			"content-paste": "\uE802",
			"timelapse": "\uE807"
		},
		"POSTSCRIPT": "iconuadjusts",
		"TTF": "/Applications/CreadorOPEN/CreadorOPEN.app/Contents/MacOS/webapps/ROOT/WEB-INF/lucee/temp/_font/fontello-d94d26c7/font/iconuadjusts.ttf"
	}
};