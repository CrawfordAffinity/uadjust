var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'hogar2_index';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.ID_375356980 = Ti.UI.createWindow(
  { title: L('x3719714762_traducir', 'inicio'), titleid: "x3719714762_traducir", backgroundImage: "/images/i4DDC620865F38C5A3439087EED99348F.png", layout: "composite", windowSoftInputMode: Titanium.UI.Android.SOFT_KEYBOARD_HIDE_ON_FOCUS, id: "ID_375356980", backgroundColor: "#ffffff", theme: "sinbarra" });

  $.__views.ID_375356980 && $.addTopLevelView($.__views.ID_375356980);
  Androidback_ID_157499917 ? $.addListener($.__views.ID_375356980, 'androidback', Androidback_ID_157499917) : __defers['$.__views.ID_375356980!androidback!Androidback_ID_157499917'] = true;$.__views.ID_256650807 = Ti.UI.createView(
  { height: "120dp", layout: "vertical", top: "55dp", id: "ID_256650807", backgroundColor: "#ffffff" });

  $.__views.ID_375356980.add($.__views.ID_256650807);
  $.__views.ID_1919315447 = Ti.UI.createView(
  { height: "25dp", layout: "horizontal", id: "ID_1919315447" });

  $.__views.ID_256650807.add($.__views.ID_1919315447);
  $.__views.ID_1915447013 = Ti.UI.createLabel(
  { color: "#4d4d4d", font: { fontFamily: "Roboto-Medium", fontSize: "16dp" }, text: L('x807001066_traducir', 'Scheduled'), touchEnabled: false, left: 15, bottom: 5, width: Ti.UI.SIZE, id: "ID_1915447013", textid: "x807001066_traducir" });

  $.__views.ID_1919315447.add($.__views.ID_1915447013);
  $.__views.ID_999526548 = Ti.UI.createLabel(
  { color: "#a0a1a3", font: { fontFamily: "Roboto-Light", fontSize: "13dp" }, touchEnabled: false, left: 10, bottom: 5, width: Ti.UI.SIZE, id: "ID_999526548" });

  $.__views.ID_1919315447.add($.__views.ID_999526548);
  $.__views.ID_1709783160 = Ti.UI.createView(
  { height: "25dp", layout: "horizontal", id: "ID_1709783160" });

  $.__views.ID_256650807.add($.__views.ID_1709783160);
  $.__views.ID_665853240 = Ti.UI.createLabel(
  { color: "#838383", font: { fontFamily: "Roboto-Medium", fontSize: "13dp" }, text: L('x3980808842_traducir', 'Time remaining:'), touchEnabled: false, left: 15, bottom: 5, width: Ti.UI.SIZE, id: "ID_665853240", textid: "x3980808842_traducir" });

  $.__views.ID_1709783160.add($.__views.ID_665853240);
  $.__views.ID_1932185343 = Ti.UI.createLabel(
  { color: "#a0a1a3", font: { fontFamily: "Roboto-Light", fontSize: "13dp" }, touchEnabled: false, left: 10, bottom: 5, width: Ti.UI.SIZE, id: "ID_1932185343" });

  $.__views.ID_1709783160.add($.__views.ID_1932185343);
  $.__views.ID_952191814 = Ti.UI.createView(
  { backgroundColor: "#e6e6e6", font: { fontSize: "12dp" }, height: "1dp", layout: "vertical", width: Ti.UI.FILL, id: "ID_952191814" });

  $.__views.ID_256650807.add($.__views.ID_952191814);
  $.__views.ID_1045139224 = Alloy.createWidget('barracasos', 'widget', { paso: L('x450215437', '2'), titulo5: L('x951154001_traducir', 'End'), titulo1: L('x807033745_traducir', 'Received'), titulo4: L('x1018769216_traducir', 'Evaluating'), titulo3: L('x4262956787_traducir', 'Inspected'), titulo2: L('x807001066_traducir', 'Scheduled'), __id: "ALL1045139224", id: "ID_1045139224", __parentSymbol: $.__views.ID_256650807 });
  $.__views.ID_1045139224.setParent($.__views.ID_256650807);
  $.__views.ID_1036013130 = Ti.UI.createView(
  { backgroundColor: "#e6e6e6", font: { fontSize: "12dp" }, height: "1dp", layout: "vertical", width: Ti.UI.FILL, id: "ID_1036013130" });

  $.__views.ID_256650807.add($.__views.ID_1036013130);
  $.__views.ID_220723656 = Ti.UI.createScrollView(
  { showVerticalScrollIndicator: true, layout: "vertical", top: "175dp", id: "ID_220723656", showHorizontalScrollIndicator: true });

  $.__views.ID_375356980.add($.__views.ID_220723656);
  $.__views.ID_1926115404 = Alloy.createWidget('cajadescripcion', 'widget', { titulo: L('x3950563313_traducir', 'Description'), __id: "ALL1926115404", id: "ID_1926115404", __parentSymbol: $.__views.ID_220723656 });
  $.__views.ID_1926115404.setParent($.__views.ID_220723656);
  $.__views.ID_1945909135 = Ti.UI.createView(
  { height: Ti.UI.SIZE, bottom: "10dp", borderColor: "#e6e6e6", layout: "horizontal", width: "90%", top: "10dp", id: "ID_1945909135", borderRadius: 5, borderWidth: 1, backgroundColor: "#ffffff" });

  $.__views.ID_220723656.add($.__views.ID_1945909135);
  $.__views.ID_1388948574 = Ti.UI.createView(
  { height: "60dp", bottom: "25dp", left: "10dp", layout: "vertical", width: "60dp", top: "25dp", id: "ID_1388948574", borderRadius: 30 });

  $.__views.ID_1945909135.add($.__views.ID_1388948574);
  $.__views.ID_780699779 = Ti.UI.createImageView(
  { width: "100%", image: "/images/i4A22DEA00A2F2DEEA9163E8432A3DD40.png", id: "ID_780699779" });

  $.__views.ID_1388948574.add($.__views.ID_780699779);
  $.__views.ID_667053664 = Ti.UI.createView(
  { height: Ti.UI.SIZE, left: "10dp", layout: "vertical", width: Ti.UI.FILL, id: "ID_667053664" });

  $.__views.ID_1945909135.add($.__views.ID_667053664);
  $.__views.ID_70014429 = Ti.UI.createLabel(
  { color: "#838383", font: { fontFamily: "Roboto-Medium", fontSize: "13dp" }, text: L('x3880041433_traducir', 'Unassigned Inspector'), touchEnabled: false, left: 0, id: "ID_70014429", textid: "x3880041433_traducir" });

  $.__views.ID_667053664.add($.__views.ID_70014429);
  $.__views.ID_561134970 = Ti.UI.createLabel(
  { color: "#a0a1a3", font: { fontFamily: "Roboto-Light", fontSize: "13dp" }, text: L('x2087673718_traducir', 'Assignment in process'), touchEnabled: false, left: 0, id: "ID_561134970", textid: "x2087673718_traducir" });

  $.__views.ID_667053664.add($.__views.ID_561134970);
  $.__views.ID_1628898902 = Ti.UI.createView(
  { height: Ti.UI.SIZE, left: "0dp", visible: false, layout: "horizontal", width: Ti.UI.FILL, id: "ID_1628898902" });

  $.__views.ID_667053664.add($.__views.ID_1628898902);
  $.__views.ID_166052581 = Ti.UI.createLabel(
  { color: "#a0a1a3", font: { fontFamily: "Roboto-Light", fontSize: "13dp" }, text: L('x555777092_traducir', 'Date Scheduling:'), touchEnabled: false, width: Ti.UI.SIZE, id: "ID_166052581", textid: "x555777092_traducir" });

  $.__views.ID_1628898902.add($.__views.ID_166052581);
  $.__views.ID_1394908684 = Ti.UI.createLabel(
  { color: "#a0a1a3", font: { fontFamily: "Roboto-Light", fontSize: "13dp" }, text: L('x211534962', '0000'), touchEnabled: false, width: "50%", id: "ID_1394908684", textid: "x211534962" });

  $.__views.ID_1628898902.add($.__views.ID_1394908684);
  $.__views.ID_798819823 = Ti.UI.createLabel(
  { color: "#a0a1a3", font: { fontFamily: "Roboto-Light", fontSize: "13dp" }, touchEnabled: false, left: 0, id: "ID_798819823" });

  $.__views.ID_667053664.add($.__views.ID_798819823);
  $.__views.ID_967739855 = Ti.UI.createView(
  { height: Ti.UI.SIZE, bottom: "10dp", borderColor: "#e6e6e6", visible: false, layout: "vertical", width: "90%", top: "10dp", id: "ID_967739855", borderRadius: 5, borderWidth: 1, backgroundColor: "#ffffff" });

  $.__views.ID_220723656.add($.__views.ID_967739855);
  $.__views.ID_827969565 = Ti.UI.createLabel(
  { color: "#838383", font: { fontFamily: "Roboto-Medium", fontSize: "13dp" }, text: L('x3412399334_traducir', 'Inspector will arrive at:'), touchEnabled: false, top: 10, id: "ID_827969565", textid: "x3412399334_traducir" });

  $.__views.ID_967739855.add($.__views.ID_827969565);
  $.__views.ID_1710161767 = Ti.UI.createLabel(
  { color: "#a0a1a3", font: { fontFamily: "Roboto-Light", fontSize: "16dp" }, text: L('x448113098_traducir', '00:00 min'), touchEnabled: false, id: "ID_1710161767", textid: "x448113098_traducir" });

  $.__views.ID_967739855.add($.__views.ID_1710161767);
  $.__views.ID_1446416921 = Ti.UI.createView(
  { height: Ti.UI.SIZE, bottom: "10dp", layout: "vertical", id: "ID_1446416921" });

  $.__views.ID_967739855.add($.__views.ID_1446416921);
  Click_ID_1932852360 ? $.addListener($.__views.ID_1446416921, 'click', Click_ID_1932852360) : __defers['$.__views.ID_1446416921!click!Click_ID_1932852360'] = true;$.__views.ID_1764020484 = Ti.UI.createLabel(
  { color: "#2d9edb", font: { fontFamily: "Roboto-Regular", fontSize: "14dp" }, text: L('x1083036559_traducir', 'See inspector at the map'), touchEnabled: false, id: "ID_1764020484", textid: "x1083036559_traducir" });

  $.__views.ID_1446416921.add($.__views.ID_1764020484);
  $.__views.ID_845652527 = Ti.UI.createView(
  { height: "72%", bottom: "-72%", layout: "composite", width: "100%", id: "ID_845652527" });

  $.__views.ID_375356980.add($.__views.ID_845652527);
  var __alloyId60 = [];
  $.__views.ID_1529484317 = require("ti.map").createAnnotation(
  { latitude: 0, longitude: 0, image: "/images/iFC793A64AD38CFB17552E0CF4AE60F0A.png", id: "ID_1529484317" });

  __alloyId60.push($.__views.ID_1529484317);
  $.__views.ID_1345416283 = require("ti.map").createAnnotation(
  { latitude: 0, longitude: 0, image: "/images/i5966E5EF4E79A0D34400962627AA0DD4.png", id: "ID_1345416283" });

  __alloyId60.push($.__views.ID_1345416283);
  $.__views.ID_1083238633 = (require("ti.map").createView || Ti.UI.createView)(
  { annotations: __alloyId60, height: Ti.UI.FILL, userLocation: false, delta: 0.005, width: Ti.UI.FILL, id: "ID_1083238633" });

  $.__views.ID_845652527.add($.__views.ID_1083238633);
  $.__views.ID_832819328 = Ti.UI.createImageView(
  { height: "30dp", left: 5, image: "/images/i0DC188A5563AA098110726CB1DEFB9B5.png", top: 5, id: "ID_832819328" });

  $.__views.ID_845652527.add($.__views.ID_832819328);
  Click_ID_125229435 ? $.addListener($.__views.ID_832819328, 'click', Click_ID_125229435) : __defers['$.__views.ID_832819328!click!Click_ID_125229435'] = true;$.__views.ID_1721022077 = Alloy.createWidget('barra', 'widget', { titulo: L('x1195630948_traducir', 'HOME'), nrofinal: 5, nroinicial: 2, fondo: "fondoazul", top: 0, modal: 1, __id: "ALL1721022077", id: "ID_1721022077", __parentSymbol: $.__views.ID_375356980 });
  $.__views.ID_1721022077.setParent($.__views.ID_375356980);
  exports.destroy = function () {};




  _.extend($, $.__views);


  var _bind4section = {};
  var _list_templates = {};

  var _activity;
  if (true) {
    _activity = $.ID_375356980.activity;var abx = require('com.alcoapps.actionbarextras');
  }
  var _my_events = {},
  _out_vars = {},
  $item = {},
  args = arguments[0] || {};if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);$item = $.item.toJSON();
  }
  var _var_scopekey = 'ID_375356980';
  require('vars')[_var_scopekey] = {};
  if (true) {
    $.ID_375356980.addEventListener('open', function (e) {});
  }

  $.ID_1045139224.init({
    __id: 'ALL1045139224',
    paso: L('x450215437', '2'),
    titulo5: L('x951154001_traducir', 'End'),
    titulo1: L('x807033745_traducir', 'Received'),
    titulo4: L('x1018769216_traducir', 'Evaluating'),
    titulo3: L('x4262956787_traducir', 'Inspected'),
    titulo2: L('x807001066_traducir', 'Scheduled') });


  $.ID_1926115404.init({
    titulo: L('x3950563313_traducir', 'Description'),
    __id: 'ALL1926115404' });


  function Click_ID_1932852360(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    $.ID_845652527.animate({
      bottom: 0 });

  }

  $.ID_1529484317.applyProperties({ latitude: parseFloat('0.0'), longitude: parseFloat('0.0'), image: '/images/iFC793A64AD38CFB17552E0CF4AE60F0A.png' });

  $.ID_1345416283.applyProperties({ latitude: parseFloat('0.0'), longitude: parseFloat('0.0'), image: '/images/i5966E5EF4E79A0D34400962627AA0DD4.png' });

  $.ID_1083238633.setRegion({ latitude: 0, longitude: 0, latitudeDelta: 0.005, longitudeDelta: 0.005 });
  $.ID_1083238633.applyProperties({});
  if (false) {
    var ID_1083238633_camara = require('ti.map').createCamera({
      pitch: 0,
      heading: 0 });

    $.ID_1083238633.setCamera(ID_1083238633_camara);
  }

  function Click_ID_125229435(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var evento = e;
    $.ID_845652527.animate({
      bottom: '-72%' });

  }

  $.ID_1721022077.init({
    titulo: L('x1195630948_traducir', 'HOME'),
    __id: 'ALL1721022077',
    nrofinal: 5,
    nroinicial: 2,
    fondo: 'fondoazul',
    top: 0,
    modal: 1 });


  (function () {
    var urlcrawford = 'urlcrawford' in require('vars') ? require('vars')['urlcrawford'] : '';
    var urluadjust = 'urluadjust' in require('vars') ? require('vars')['urluadjust'] : '';
    if (Ti.App.deployType != 'production') console.log('hogar2', { "datos": args });
    if (_.isObject(args) || _.isString(args) && !_.isEmpty(args) || _.isNumber(args) || _.isBoolean(args)) {






      var datos = args._data;
      if (Ti.App.deployType != 'production') console.log('carga', { "asd": datos });
      $.ID_999526548.setText(datos.compania);

      if (_.isObject(datos.inspector) || _.isString(datos.inspector) && !_.isEmpty(datos.inspector) || _.isNumber(datos.inspector) || _.isBoolean(datos.inspector)) {



        var assigned = L('x1366852108_traducir', 'Assigned inspector');
        if (Ti.App.deployType != 'production') console.log('asigne a alguien', { "asd": datos.inspector });



        $.ID_70014429.setText(assigned);

        $.ID_561134970.setText(datos.inspector);

        if (_.isObject(datos.fecha_agendamiento) || _.isString(datos.fecha_agendamiento) && !_.isEmpty(datos.fecha_agendamiento) || _.isNumber(datos.fecha_agendamiento) || _.isBoolean(datos.fecha_agendamiento)) {
          $.ID_1394908684.setText(datos.fecha_agendamiento);

          var ID_1628898902_visible = true;

          if (ID_1628898902_visible == 'si') {
            ID_1628898902_visible = true;
          } else if (ID_1628898902_visible == 'no') {
            ID_1628898902_visible = false;
          }
          $.ID_1628898902.setVisible(ID_1628898902_visible);
        }
        $.ID_798819823.setText(datos.rut_inspector);




        var ID_1885055036 = {};

        ID_1885055036.success = function (e) {
          var elemento = e,
          valor = e;
          if (elemento.error == 0 || elemento.error == '0') {



            if (_.isObject(elemento.inspector) || _.isString(elemento.inspector) && !_.isEmpty(elemento.inspector) || _.isNumber(elemento.inspector) || _.isBoolean(elemento.inspector)) {



              var ID_967739855_visible = true;

              if (ID_967739855_visible == 'si') {
                ID_967739855_visible = true;
              } else if (ID_967739855_visible == 'no') {
                ID_967739855_visible = false;
              }
              $.ID_967739855.setVisible(ID_967739855_visible);




              inspector = elemento.inspector.split(",");
              tarea = elemento.tarea.split(",");
              $.ID_1345416283.setLatitude(inspector[0]);

              $.ID_1345416283.setLongitude(inspector[1]);

              $.ID_1529484317.setLatitude(tarea[0]);

              $.ID_1529484317.setLongitude(tarea[1]);

              var ID_1083238633_latitud = tarea[0];

              var ID_1083238633_tmp = $.ID_1083238633.getRegion();
              ID_1083238633_tmp.latitude = ID_1083238633_latitud;
              ID_1083238633_latitud = ID_1083238633_tmp;
              $.ID_1083238633.setRegion(ID_1083238633_latitud);

              var ID_1083238633_longitud = tarea[1];

              var ID_1083238633_tmp = $.ID_1083238633.getRegion();
              ID_1083238633_tmp.longitude = ID_1083238633_longitud;
              ID_1083238633_longitud = ID_1083238633_tmp;
              $.ID_1083238633.setRegion(ID_1083238633_longitud);




              $.ID_1710161767.setText(elemento.duracion);

              require('vars')['duracion_seg'] = elemento.duracion_seg;




              var ID_106359922_poly = require('polyline').decode(elemento.polyline);
              var varruta = require('ti.map').createRoute({ width: 6, color: '#2d9edb', points: ID_106359922_poly });
              if (Ti.App.deployType != 'production') console.log('instanciada la ruta 1vez', {});



              $.ID_1083238633.addRoute(varruta);

              if (Ti.App.deployType != 'production') console.log('agregada la ruta 1vez', {});
              require('vars')['laruta'] = varruta;
            } else {
              if (Ti.App.deployType != 'production') console.log('los datos estan en blanco', {});
            }
          } else if (elemento.error == 400) {
            if (Ti.App.deployType != 'production') console.log('error obteniendo datos del mapa...datosfaltan', {});
          }if (Ti.App.deployType != 'production') console.log('resp_ubicacion', { "asd": elemento });
          elemento = null, valor = null;
        };

        ID_1885055036.error = function (e) {
          var elemento = e,
          valor = e;
          var ID_1389141229_opts = [L('x3610695981_traducir', 'OK')];
          var ID_1389141229 = Ti.UI.createAlertDialog({
            title: L('x3769205873_traducir', 'ALERT'),
            message: L('x4290892430_traducir', 'ERROR OBTAINING THE MAP DATA'),
            buttonNames: ID_1389141229_opts });

          ID_1389141229.addEventListener('click', function (e) {
            var zxc = ID_1389141229_opts[e.index];
            zxc = null;

            e.source.removeEventListener("click", arguments.callee);
          });
          ID_1389141229.show();
          elemento = null, valor = null;
        };
        require('helper').ajaxUnico('ID_1885055036', '' + String.format(L('x2319149298', '%1$sobtenerUbicacion/'), urluadjust.toString()) + '', 'POST', { ci_inspector: datos.rut_inspector, num_caso: datos.num_caso }, 15000, ID_1885055036);



        var hola = 0;
        var ID_172435652_continuar = true;
        _out_vars['ID_172435652'] = { _remove: ["clearTimeout(_out_vars['ID_172435652']._run)"] };
        var ID_172435652_func = function () {
          hola = hola + 1;
          var duracion_seg = 'duracion_seg' in require('vars') ? require('vars')['duracion_seg'] : '';
          if (_.isNumber(duracion_seg) && _.isNumber(60) && duracion_seg > 60) {
            if (Ti.App.deployType != 'production') console.log('sin resta minuto', { "asd": duracion_seg });

            var moment = require('alloy/moment');
            duracion_seg = duracion_seg - 60;
            var now = moment.utc(duracion_seg * 1000).format('HH[h]:mm[m]');
            if (Ti.App.deployType != 'production') console.log('menos 1 min', { "asd": duracion_seg });
            require('vars')['duracion_seg'] = duracion_seg;
            $.ID_1710161767.setText(now);

            var ID_1278490204 = {};

            ID_1278490204.success = function (e) {
              var elemento = e,
              valor = e;
              if (_.isObject(elemento.inspector) || _.isString(elemento.inspector) && !_.isEmpty(elemento.inspector) || _.isNumber(elemento.inspector) || _.isBoolean(elemento.inspector)) {
                if (Ti.App.deployType != 'production') console.log('repitientdo ubucacion', {});



                var ID_967739855_visible = true;

                if (ID_967739855_visible == 'si') {
                  ID_967739855_visible = true;
                } else if (ID_967739855_visible == 'no') {
                  ID_967739855_visible = false;
                }
                $.ID_967739855.setVisible(ID_967739855_visible);

                inspector = elemento.inspector.split(",");
                tarea = elemento.tarea.split(",");
                $.ID_1345416283.setLatitude(inspector[0]);

                $.ID_1345416283.setLongitude(inspector[1]);

                $.ID_1529484317.setLatitude(tarea[0]);

                $.ID_1529484317.setLongitude(tarea[1]);

                $.ID_1710161767.setText(elemento.duracion);

                var laruta = 'laruta' in require('vars') ? require('vars')['laruta'] : '';
                if (_.isObject(laruta)) {



                  if (Ti.App.deployType != 'production') console.log('es objeto', {});
                  $.ID_1083238633.removeRoute(laruta);

                  if (Ti.App.deployType != 'production') console.log('borrando ruta', {});
                } else {
                  if (Ti.App.deployType != 'production') console.log('no es objeto', {});
                }
                var ID_1965389598_poly = require('polyline').decode(elemento.polyline);
                var varruta = require('ti.map').createRoute({ width: 6, color: '#2d9edb', points: ID_1965389598_poly });
                if (Ti.App.deployType != 'production') console.log('instanciada nueva ruta', {});
                require('vars')['laruta'] = varruta;
                $.ID_1083238633.addRoute(varruta);

                if (Ti.App.deployType != 'production') console.log('agregada la ruta nueva', {});
              }
              elemento = null, valor = null;
            };

            ID_1278490204.error = function (e) {
              var elemento = e,
              valor = e;
              var ID_1579001190_opts = [L('x3610695981_traducir', 'OK')];
              var ID_1579001190 = Ti.UI.createAlertDialog({
                title: L('x3769205873_traducir', 'ALERT'),
                message: L('x4290892430_traducir', 'ERROR OBTAINING THE MAP DATA'),
                buttonNames: ID_1579001190_opts });

              ID_1579001190.addEventListener('click', function (e) {
                var zxc = ID_1579001190_opts[e.index];
                zxc = null;

                e.source.removeEventListener("click", arguments.callee);
              });
              ID_1579001190.show();
              elemento = null, valor = null;
            };
            require('helper').ajaxUnico('ID_1278490204', '' + String.format(L('x2319149298', '%1$sobtenerUbicacion/'), urluadjust.toString()) + '', 'POST', { ci_inspector: datos.rut_inspector, num_caso: datos.num_caso }, 15000, ID_1278490204);
          } else {}if (ID_172435652_continuar == true) {
            _out_vars['ID_172435652']._run = setTimeout(ID_172435652_func, 1000 * 20);
          }
        };
        _out_vars['ID_172435652']._run = setTimeout(ID_172435652_func, 1000 * 20);
        if (_.isObject(datos.imagen_inspector) || _.isString(datos.imagen_inspector) && !_.isEmpty(datos.imagen_inspector) || _.isNumber(datos.imagen_inspector) || _.isBoolean(datos.imagen_inspector)) {



          var imageBlob = Ti.Utils.base64decode(datos.imagen_inspector);
          var ID_780699779_imagen = imageBlob;

          if (typeof ID_780699779_imagen == 'string' && 'styles' in require('a4w') && ID_780699779_imagen in require('a4w').styles['images']) {
            ID_780699779_imagen = require('a4w').styles['images'][ID_780699779_imagen];
          }
          $.ID_780699779.setImage(ID_780699779_imagen);
        }
      }
      if (Ti.App.deployType != 'production') console.log('contiene datos', {});



      var days = L('x3957652582_traducir', 'days');
      var legal = L('x2139489547_traducir', 'legal:');
      var uadjust = L('x1807599243_traducir', 'uadjust:');
      $.ID_1932185343.setText(String.format(L('x3000044032', '%1$s %2$s %3$s %4$s %5$s %6$s'), legal ? legal.toString() : '', datos.legal.split(" ")[0] ? datos.legal.split(" ")[0].toString() : '', days ? days.toString() : '', uadjust ? uadjust.toString() : '', datos.estimado.split(" ")[0] ? datos.estimado.split(" ")[0].toString() : '', days ? days.toString() : ''));
    }
    $.ID_220723656.setShowVerticalScrollIndicator(false);



    var hog2 = L('x1879867758_traducir', 'deschogar2');




    $.ID_1926115404.texto({ valor: L('x2117476365_traducir', 'hog2') });
  })();

  function Androidback_ID_157499917(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    if (_.isObject(args) || _.isString(args) && !_.isEmpty(args) || _.isNumber(args) || _.isBoolean(args)) {
      if (_.isObject(args._data.inspector) || _.isString(args._data.inspector) && !_.isEmpty(args._data.inspector) || _.isNumber(args._data.inspector) || _.isBoolean(args._data.inspector)) {



        var ID_172435652_detener = true;

        var detenerRepetir = function (mula) {
          ID_172435652_continuar = false;
          clearTimeout(_out_vars['ID_172435652']._run);
        };detenerRepetir(ID_172435652_detener);
      }
    }
    $.ID_375356980.close();
  }
  if (false || true) {
    $.ID_375356980.addEventListener('close', function () {
      $.destroy();
      $.off();
      var _ev_tmp = null,
      _ev_rem = null;
      if (_my_events) {
        for (_ev_tmp in _my_events) {
          try {
            if (_ev_tmp.indexOf('_web') != -1) {
              Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
            } else {
              Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
            }
          } catch (err10) {}
        }
        _my_events = null;

      }
      if (_out_vars) {
        for (_ev_tmp in _out_vars) {
          for (_ev_rem in _out_vars[_ev_tmp]._remove) {
            try {
              eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
            } catch (_errt) {}
          }
          _out_vars[_ev_tmp] = null;
        }
        _ev_tmp = null;

      }
    });
  }






  __defers['$.__views.ID_375356980!androidback!Androidback_ID_157499917'] && $.addListener($.__views.ID_375356980, 'androidback', Androidback_ID_157499917);__defers['$.__views.ID_1446416921!click!Click_ID_1932852360'] && $.addListener($.__views.ID_1446416921, 'click', Click_ID_1932852360);__defers['$.__views.ID_832819328!click!Click_ID_125229435'] && $.addListener($.__views.ID_832819328, 'click', Click_ID_125229435);



  _.extend($, exports);
}

module.exports = Controller;