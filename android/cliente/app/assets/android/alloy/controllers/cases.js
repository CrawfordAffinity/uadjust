var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'cases';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};




  Alloy.Collections.instance('casos');


  $.__views.ID_1737225565 = Ti.UI.createWindow(
  { title: L('x3947872571_traducir', 'CASES'), titleid: "x3947872571_traducir", backgroundImage: "/images/i4DDC620865F38C5A3439087EED99348F.png", layout: "composite", windowSoftInputMode: Titanium.UI.Android.SOFT_KEYBOARD_HIDE_ON_FOCUS, id: "ID_1737225565", backgroundColor: "#fafafc" });

  $.__views.ID_1737225565 && $.addTopLevelView($.__views.ID_1737225565);
  var __alloyId2 = [];
  $.__views.ID_1353765508 = Ti.UI.createScrollableView(
  { views: __alloyId2, showPagingControl: true, height: "130dp", pagingControlTimeout: 0, layout: "horizontal", width: "100%", top: "0dp", id: "ID_1353765508" });

  $.__views.ID_1737225565.add($.__views.ID_1353765508);
  var __alloyId3 = {};var __alloyId6 = [];var __alloyId7 = { type: 'Ti.UI.View', childTemplates: function () {var __alloyId8 = [];var __alloyId9 = { type: 'Ti.UI.View', childTemplates: function () {var __alloyId10 = [];var __alloyId11 = { type: 'Ti.UI.View', childTemplates: function () {var __alloyId12 = [];var __alloyId13 = { type: 'Ti.UI.Label', bindId: 'ID_1542432033', properties: { color: "#838383", font: { fontFamily: "Roboto-Medium", fontSize: "13dp" }, touchEnabled: false, bindId: "ID_1542432033", right: 0 } };__alloyId12.push(__alloyId13);var __alloyId14 = { type: 'Ti.UI.Label', bindId: 'ID_882211798', properties: { color: "#4d4d4d", font: { fontFamily: "Roboto-Regular", fontSize: "16dp" }, touchEnabled: false, left: 0, bindId: "ID_882211798" } };__alloyId12.push(__alloyId14);var __alloyId15 = { type: 'Ti.UI.Label', bindId: 'ID_877203508', properties: { touchEnabled: false, ellipsize: true, color: "#000000", visible: false, wordWrap: false, width: "1dp", bindId: "ID_877203508" } };__alloyId12.push(__alloyId15);var __alloyId16 = { type: 'Ti.UI.Label', bindId: 'ID_923484212', properties: { touchEnabled: false, ellipsize: true, color: "#000000", visible: false, wordWrap: false, width: "1dp", bindId: "ID_923484212" } };__alloyId12.push(__alloyId16);var __alloyId17 = { type: 'Ti.UI.Label', bindId: 'ID_544163570', properties: { touchEnabled: false, ellipsize: true, color: "#000000", visible: false, wordWrap: false, width: "1dp", bindId: "ID_544163570" } };__alloyId12.push(__alloyId17);var __alloyId18 = { type: 'Ti.UI.Label', bindId: 'ID_622359318', properties: { touchEnabled: false, ellipsize: true, color: "#000000", visible: false, wordWrap: false, width: "1dp", bindId: "ID_622359318" } };__alloyId12.push(__alloyId18);var __alloyId19 = { type: 'Ti.UI.Label', bindId: 'ID_48648805', properties: { touchEnabled: false, ellipsize: true, color: "#000000", visible: false, wordWrap: false, width: "1dp", bindId: "ID_48648805" } };__alloyId12.push(__alloyId19);var __alloyId20 = { type: 'Ti.UI.Label', bindId: 'ID_555733476', properties: { touchEnabled: false, ellipsize: true, color: "#000000", visible: false, wordWrap: false, width: "1dp", bindId: "ID_555733476" } };__alloyId12.push(__alloyId20);var __alloyId21 = { type: 'Ti.UI.Label', bindId: 'ID_149148931', properties: { touchEnabled: false, ellipsize: true, color: "#000000", visible: false, wordWrap: false, width: "1dp", bindId: "ID_149148931" } };__alloyId12.push(__alloyId21);var __alloyId22 = { type: 'Ti.UI.Label', bindId: 'ID_1375133572', properties: { touchEnabled: false, ellipsize: true, color: "#000000", visible: false, wordWrap: false, width: "1dp", bindId: "ID_1375133572" } };__alloyId12.push(__alloyId22);var __alloyId23 = { type: 'Ti.UI.Label', bindId: 'ID_1767631682', properties: { touchEnabled: false, ellipsize: true, color: "#000000", visible: false, wordWrap: false, width: "1dp", bindId: "ID_1767631682" } };__alloyId12.push(__alloyId23);var __alloyId24 = { type: 'Ti.UI.Label', bindId: 'ID_1539314311', properties: { touchEnabled: false, ellipsize: true, color: "#000000", visible: false, wordWrap: false, width: "1dp", bindId: "ID_1539314311" } };__alloyId12.push(__alloyId24);var __alloyId25 = { type: 'Ti.UI.Label', bindId: 'ID_764024818', properties: { touchEnabled: false, ellipsize: true, color: "#000000", visible: false, wordWrap: false, width: "1dp", bindId: "ID_764024818" } };__alloyId12.push(__alloyId25);var __alloyId26 = { type: 'Ti.UI.Label', bindId: 'ID_322609251', properties: { touchEnabled: false, ellipsize: true, color: "#000000", visible: false, wordWrap: false, width: "1dp", bindId: "ID_322609251" } };__alloyId12.push(__alloyId26);var __alloyId27 = { type: 'Ti.UI.Label', bindId: 'ID_382088194', properties: { touchEnabled: false, ellipsize: true, color: "#000000", visible: false, wordWrap: false, width: "1dp", bindId: "ID_382088194" } };__alloyId12.push(__alloyId27);return __alloyId12;}(), properties: { height: Ti.UI.SIZE, layout: "composite", top: "10dp" } };__alloyId10.push(__alloyId11);var __alloyId28 = { type: 'Ti.UI.View', childTemplates: function () {var __alloyId29 = [];var __alloyId30 = { type: 'Ti.UI.Label', properties: { font: { fontFamily: "iconuadjusts", fontSize: "12dp" }, touchEnabled: false, height: Ti.UI.SIZE, text: "\uE807", color: "#8ce5bd", width: Ti.UI.SIZE, ancho: "-" } };__alloyId29.push(__alloyId30);var __alloyId31 = { type: 'Ti.UI.Label', bindId: 'ID_514163814', properties: { color: "#a0a1a3", font: { fontFamily: "Roboto-Light", fontSize: "13dp" }, touchEnabled: false, left: 3, width: Ti.UI.SIZE, bindId: "ID_514163814" } };__alloyId29.push(__alloyId31);return __alloyId29;}(), properties: { height: Ti.UI.SIZE, layout: "horizontal", top: "5dp" } };__alloyId10.push(__alloyId28);var __alloyId32 = { type: 'Ti.UI.View', childTemplates: function () {var __alloyId33 = [];var __alloyId34 = { type: 'Ti.UI.Label', properties: { font: { fontFamily: "iconuadjusts", fontSize: "12dp" }, touchEnabled: false, height: Ti.UI.SIZE, text: "\uE808", color: "#2d9edb", width: Ti.UI.SIZE, ancho: "-" } };__alloyId33.push(__alloyId34);var __alloyId35 = { type: 'Ti.UI.Label', bindId: 'ID_201785329', properties: { color: "#a0a1a3", font: { fontFamily: "Roboto-Light", fontSize: "13dp" }, touchEnabled: false, left: 3, width: "90%", bindId: "ID_201785329" } };__alloyId33.push(__alloyId35);return __alloyId33;}(), properties: { height: Ti.UI.SIZE, layout: "horizontal", top: "5dp" } };__alloyId10.push(__alloyId32);var __alloyId36 = { type: 'Ti.UI.View', childTemplates: function () {var __alloyId37 = [];var __alloyId38 = { type: 'Ti.UI.Label', properties: { color: "#838383", font: { fontFamily: "Roboto-Medium", fontSize: "13dp" }, text: L('x3505507568_traducir', 'Legal ETA:'), touchEnabled: false, width: Ti.UI.SIZE, textid: "x3505507568_traducir" } };__alloyId37.push(__alloyId38);var __alloyId39 = { type: 'Ti.UI.View', childTemplates: function () {var __alloyId40 = [];var __alloyId41 = { type: 'Ti.UI.Label', bindId: 'ID_1144123809', properties: { color: "#a0a1a3", font: { fontFamily: "Roboto-Light", fontSize: "13dp" }, touchEnabled: false, left: 3, width: Ti.UI.SIZE, bindId: "ID_1144123809" } };__alloyId40.push(__alloyId41);var __alloyId42 = { type: 'Ti.UI.Label', properties: { color: "#a0a1a3", font: { fontFamily: "Roboto-Light", fontSize: "13dp" }, text: L('x3957652582_traducir', 'days'), touchEnabled: false, left: 0, width: "50%", textid: "x3957652582_traducir" } };__alloyId40.push(__alloyId42);return __alloyId40;}(), properties: { height: Ti.UI.SIZE, layout: "horizontal", width: "50%" } };__alloyId37.push(__alloyId39);return __alloyId37;}(), properties: { height: Ti.UI.SIZE, layout: "horizontal", top: "5dp" } };__alloyId10.push(__alloyId36);var __alloyId43 = { type: 'Ti.UI.View', childTemplates: function () {var __alloyId44 = [];var __alloyId45 = { type: 'Ti.UI.Label', properties: { color: "#838383", font: { fontFamily: "Roboto-Medium", fontSize: "13dp" }, text: L('x3377813129_traducir', 'ETA:'), touchEnabled: false, width: Ti.UI.SIZE, textid: "x3377813129_traducir" } };__alloyId44.push(__alloyId45);var __alloyId46 = { type: 'Ti.UI.View', childTemplates: function () {var __alloyId47 = [];var __alloyId48 = { type: 'Ti.UI.Label', bindId: 'ID_1844210260', properties: { color: "#a0a1a3", font: { fontFamily: "Roboto-Light", fontSize: "13dp" }, touchEnabled: false, left: 3, width: Ti.UI.SIZE, bindId: "ID_1844210260" } };__alloyId47.push(__alloyId48);var __alloyId49 = { type: 'Ti.UI.Label', properties: { color: "#a0a1a3", font: { fontFamily: "Roboto-Light", fontSize: "13dp" }, text: L('x3957652582_traducir', 'days'), touchEnabled: false, left: 0, width: "50%", textid: "x3957652582_traducir" } };__alloyId47.push(__alloyId49);return __alloyId47;}(), properties: { height: Ti.UI.SIZE, layout: "horizontal", width: "50%" } };__alloyId44.push(__alloyId46);return __alloyId44;}(), properties: { height: Ti.UI.SIZE, bottom: "5dp", layout: "horizontal", width: Ti.UI.FILL, top: "5dp" } };__alloyId10.push(__alloyId43);return __alloyId10;}(), properties: { height: Ti.UI.SIZE, left: "10dp", layout: "vertical", width: "90%" } };__alloyId8.push(__alloyId9);var __alloyId50 = { type: 'Ti.UI.View', childTemplates: function () {var __alloyId51 = [];var __alloyId52 = { type: 'Ti.UI.Label', properties: { font: { fontFamily: "iconuadjusts", fontSize: "14dp" }, touchEnabled: false, height: Ti.UI.SIZE, text: "\uE803", color: "#a0a1a3", width: Ti.UI.SIZE } };__alloyId51.push(__alloyId52);return __alloyId51;}(), properties: { height: Ti.UI.SIZE, layout: "composite", width: Ti.UI.FILL } };__alloyId8.push(__alloyId50);return __alloyId8;}(), properties: { height: "160dp", bottom: "5dp", borderColor: "#e6e6e6", layout: "horizontal", width: "90%", top: "5dp", borderRadius: 5, borderWidth: 1, backgroundColor: "#ffffff" } };__alloyId6.push(__alloyId7);var __alloyId5 = { properties: { name: "casos1" }, childTemplates: __alloyId6 };__alloyId3["casos1"] = __alloyId5;$.__views.ID_1477395059 = Ti.UI.createView(
  { height: Ti.UI.SIZE, layout: "vertical", id: "ID_1477395059" });

  $.__views.ID_821037248 = Ti.UI.createListSection(
  { headerTitle: L('x393297498_traducir', 'titulo'), headerView: $.__views.ID_1477395059, id: "ID_821037248" });

  var __alloyId57 = Alloy.Collections['casos'] || casos;function __alloyId58(e) {if (e && e.fromAdapter) {return;}var opts = __alloyId58.opts || {};var models = ID_1163770763_filter(__alloyId57);var len = models.length;var __alloyId53 = [];for (var i = 0; i < len; i++) {var __alloyId54 = models[i];__alloyId54.__transform = ID_1163770763_transform(__alloyId54);var __alloyId56 = { ID_201785329: { text: __alloyId54.__transform.compania }, ID_149148931: { text: __alloyId54.__transform.calificacion }, ID_1542432033: { text: __alloyId54.__transform.tipotexto }, ID_622359318: { text: __alloyId54.__transform.inspector }, ID_544163570: { text: __alloyId54.__transform.imagen_inspector }, ID_1539314311: { text: __alloyId54.__transform.documentos }, ID_382088194: { text: __alloyId54.__transform.num_caso }, ID_555733476: { text: __alloyId54.__transform.rut_inspector }, ID_48648805: { text: __alloyId54.__transform.fecha_agendamiento }, ID_764024818: { text: __alloyId54.__transform.realizado_fecha }, ID_322609251: { text: __alloyId54.__transform.realizado_dias }, ID_882211798: { text: __alloyId54.__transform.siniestro }, ID_877203508: { text: __alloyId54.__transform.estado }, properties: { searchableText: __alloyId54.__transform.tipo + __alloyId54.__transform.estado + __alloyId54.__transform.rut_inspector + __alloyId54.__transform.siniestro + __alloyId54.__transform.estadotexto + __alloyId54.__transform.num_caso + __alloyId54.__transform.imagen_inspector + __alloyId54.__transform.realizado_dias + __alloyId54.__transform.compania + __alloyId54.__transform.realizado_fecha + __alloyId54.__transform.fecha_agendamiento + __alloyId54.__transform.descripcion + __alloyId54.__transform.inspector + __alloyId54.__transform.legal + ' ' + __alloyId54.__transform.documentos + __alloyId54.__transform.tipotexto + __alloyId54.__transform.calificacion + __alloyId54.__transform.estimado + ' ' + __alloyId54.__transform.correo, itemId: __alloyId54.__transform.id }, ID_1375133572: { text: __alloyId54.__transform.descripcion }, template: "casos1", ID_1767631682: { text: __alloyId54.__transform.correo }, ID_514163814: { text: __alloyId54.__transform.estadotexto }, ID_1144123809: { text: __alloyId54.__transform.legal + ' ' }, ID_1844210260: { text: __alloyId54.__transform.estimado + ' ' }, ID_923484212: { text: __alloyId54.__transform.tipo } };__alloyId53.push(__alloyId56);}opts.animation ? $.__views.ID_821037248.setItems(__alloyId53, opts.animation) : $.__views.ID_821037248.setItems(__alloyId53);};__alloyId57.on('fetch destroy change add remove reset', __alloyId58);var __alloyId59 = [];__alloyId59.push($.__views.ID_821037248);$.__views.ID_513906812 = Ti.UI.createListView(
  { sections: __alloyId59, templates: __alloyId3, allowsSelection: true, top: 130, id: "ID_513906812" });

  $.__views.ID_1737225565.add($.__views.ID_513906812);
  Itemclick_ID_1481600886 ? $.addListener($.__views.ID_513906812, 'itemclick', Itemclick_ID_1481600886) : __defers['$.__views.ID_513906812!itemclick!Itemclick_ID_1481600886'] = true;exports.destroy = function () {__alloyId57 && __alloyId57.off('fetch destroy change add remove reset', __alloyId58);};




  _.extend($, $.__views);


  var _bind4section = { "titulo": "casos" };
  var _list_templates = { "casos1": { "ID_923484212": { "text": "{tipo}" }, "ID_853575261": {}, "ID_877203508": { "text": "{estado}" }, "ID_247226025": {}, "ID_555733476": { "text": "{rut_inspector}" }, "ID_1048154264": {}, "ID_378618338": {}, "ID_1058664237": {}, "ID_882211798": { "text": "{siniestro}" }, "ID_514163814": { "text": "{estadotexto}" }, "ID_382088194": { "text": "{num_caso}" }, "ID_544163570": { "text": "{imagen_inspector}" }, "ID_322609251": { "text": "{realizado_dias}" }, "ID_555212209": {}, "ID_201785329": { "text": "{compania}" }, "ID_1012523009": {}, "ID_1642474614": {}, "ID_494690396": {}, "ID_764024818": { "text": "{realizado_fecha}" }, "ID_429943992": {}, "ID_48648805": { "text": "{fecha_agendamiento}" }, "ID_1375133572": { "text": "{descripcion}" }, "ID_622359318": { "text": "{inspector}" }, "ID_1144123809": { "text": "{legal} " }, "ID_1539314311": { "text": "{documentos}" }, "ID_1542432033": { "text": "{tipotexto}" }, "ID_149148931": { "text": "{calificacion}" }, "ID_870355169": {}, "ID_1162235703": {}, "ID_1844210260": { "text": "{estimado} " }, "ID_194254598": {}, "ID_1767631682": { "text": "{correo}" }, "ID_1945909135": {} } };

  var _activity;
  if (true) {
    _activity = $.ID_1737225565.activity;var abx = require('com.alcoapps.actionbarextras');
  }
  var _my_events = {},
  _out_vars = {},
  $item = {},
  args = arguments[0] || {};if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);$item = $.item.toJSON();
  }
  var _var_scopekey = 'ID_1737225565';
  require('vars')[_var_scopekey] = {};
  if (true) {
    $.ID_1737225565.addEventListener('open', function (e) {});
  }

  var ID_1163770763_like = function (search) {
    if (typeof search !== 'string' || this === null) {
      return false;
    }
    search = search.replace(new RegExp("([\\.\\\\\\+\\*\\?\\[\\^\\]\\$\\(\\)\\{\\}\\=\\!\\<\\>\\|\\:\\-])", "g"), "\\$1");
    search = search.replace(/%/g, '.*').replace(/_/g, '.');
    return RegExp('^' + search + '$', 'gi').test(this);
  };
  var ID_1163770763_filter = function (coll) {
    var filtered = _.toArray(coll.filter(function (m) {
      return true;
    }));
    return filtered;
  };
  var ID_1163770763_update = function (e) {};
  _.defer(function () {
    Alloy.Collections.casos.fetch();
  });
  Alloy.Collections.casos.on('add change delete', function (ee) {
    ID_1163770763_update(ee);
  });
  var ID_1163770763_transform = function (model) {
    var fila = model.toJSON();
    if (fila.tipotexto == 1 || fila.tipotexto == '1') {



      var home = L('x3521422318_traducir', 'Home');
      fila.tipotexto = home;
    } else if (fila.tipotexto == 2) {
      var car = L('x1332781181_traducir', 'Car');
      fila.tipotexto = car;
    } else if (fila.tipotexto == 3) {
      var fraud = L('x2166733574_traducir', 'Fraud');
      fila.tipotexto = fraud;
    }if (fila.estadotexto == 1 || fila.estadotexto == '1') {
      var phase = L('x1887238607_traducir', 'Phase');
      fila.estadotexto = phase;
    } else if (fila.estadotexto == 2) {
      var assignment = L('x2140686186_traducir', 'Assignment');
      fila.estadotexto = assignment;
    } else if (fila.estadotexto == 3) {
      var inspection = L('x3064739669_traducir', 'Inspection');
      fila.estadotexto = inspection;
    } else if (fila.estadotexto == 4) {
      var evaluating = L('x1018769216_traducir', 'Evaluating');
      fila.estadotexto = evaluating;
    } else if (fila.estadotexto == 5) {
      var end = L('x951154001_traducir', 'End');
      fila.estadotexto = end;
    }return fila;
  };
  Alloy.Collections.casos.fetch();

  function Itemclick_ID_1481600886(e) {

    e.cancelBubble = true;
    var objeto = e.section.getItemAt(e.itemIndex);var modelo = {},
    _modelo = [];
    var fila = {},
    fila_bak = {},
    info = { _template: objeto.template, _what: [], _seccion_ref: e.section.getHeaderTitle(), _model_id: -1 },
    _tmp = { objmap: {} };
    if ('itemId' in e) {
      info._model_id = e.itemId;
      modelo._id = info._model_id;
      if (info._seccion_ref != '' && info._seccion_ref in _bind4section) {
        modelo._collection = _bind4section[info._seccion_ref];
        _tmp._coll = modelo._collection;
      }
    }
    var findVariables = require('fvariables');
    _.each(_list_templates[info._template], function (obj_id, id) {
      _.each(obj_id, function (valor, prop) {
        var llaves = findVariables(valor, '{', '}');
        _.each(llaves, function (llave) {
          _tmp.objmap[llave] = { id: id, prop: prop };
          fila[llave] = objeto[id][prop];
          if (id == e.bindId) info._what.push(llave);
        });
      });
    });
    info._what = info._what.join(',');
    fila_bak = JSON.parse(JSON.stringify(fila));
    if (fila.tipo == 1 || fila.tipo == '1') {






      if (fila.estado == 1 || fila.estado == '1') {








        Alloy.createController("hogar1_index", { '_data': fila, '__master_model': typeof modelo !== 'undefined' ? modelo : {}, '__modelo': typeof _modelo !== 'undefined' && _modelo.length > 0 ? _modelo[0] : {} }).getView().open();
      } else if (fila.estado == 2) {
        Alloy.createController("hogar2_index", { '_data': fila, '__master_model': typeof modelo !== 'undefined' ? modelo : {}, '__modelo': typeof _modelo !== 'undefined' && _modelo.length > 0 ? _modelo[0] : {} }).getView().open();
      } else if (fila.estado == 3) {
        Alloy.createController("hogar3_index", { '_data': fila, '__master_model': typeof modelo !== 'undefined' ? modelo : {}, '__modelo': typeof _modelo !== 'undefined' && _modelo.length > 0 ? _modelo[0] : {} }).getView().open();
      } else if (fila.estado == 4) {
        Alloy.createController("hogar4_index", { '_data': fila, '__master_model': typeof modelo !== 'undefined' ? modelo : {}, '__modelo': typeof _modelo !== 'undefined' && _modelo.length > 0 ? _modelo[0] : {} }).getView().open();
      } else if (fila.estado == 5) {
        Alloy.createController("hogar5_index", { '_data': fila, '__master_model': typeof modelo !== 'undefined' ? modelo : {}, '__modelo': typeof _modelo !== 'undefined' && _modelo.length > 0 ? _modelo[0] : {} }).getView().open();
      }
    } else if (fila.tipo == 2) {
      if (fila.estado == 1 || fila.estado == '1') {
        Alloy.createController("fraude1_index", { '_data': fila, '__master_model': typeof modelo !== 'undefined' ? modelo : {}, '__modelo': typeof _modelo !== 'undefined' && _modelo.length > 0 ? _modelo[0] : {} }).getView().open();
      } else if (fila.estado == 2) {
        Alloy.createController("fraude2_index", { '_data': fila, '__master_model': typeof modelo !== 'undefined' ? modelo : {}, '__modelo': typeof _modelo !== 'undefined' && _modelo.length > 0 ? _modelo[0] : {} }).getView().open();
      } else if (fila.estado == 3) {
        Alloy.createController("fraude3_index", { '_data': fila, '__master_model': typeof modelo !== 'undefined' ? modelo : {}, '__modelo': typeof _modelo !== 'undefined' && _modelo.length > 0 ? _modelo[0] : {} }).getView().open();
      }
    } else if (fila.tipo == 3) {
      if (fila.estado == 1 || fila.estado == '1') {
        Alloy.createController("auto1_index", { '_data': fila, '__master_model': typeof modelo !== 'undefined' ? modelo : {}, '__modelo': typeof _modelo !== 'undefined' && _modelo.length > 0 ? _modelo[0] : {} }).getView().open();
      } else if (fila.estado == 2) {
        Alloy.createController("auto2_index", { '_data': fila, '__master_model': typeof modelo !== 'undefined' ? modelo : {}, '__modelo': typeof _modelo !== 'undefined' && _modelo.length > 0 ? _modelo[0] : {} }).getView().open();
      } else if (fila.estado == 3) {
        Alloy.createController("auto3_index", { '_data': fila, '__master_model': typeof modelo !== 'undefined' ? modelo : {}, '__modelo': typeof _modelo !== 'undefined' && _modelo.length > 0 ? _modelo[0] : {} }).getView().open();
      }
    }_tmp.changed = false;_tmp.diff_keys = [];
    _.each(fila, function (value1, prop) {
      var had_samekey = false;
      _.each(fila_bak, function (value2, prop2) {
        if (prop == prop2 && value1 == value2) {
          had_samekey = true;
        } else if (!_.has(fila_bak, prop) || !_.has(fila, prop2)) {
          has_samekey = true;
        }
      });
      if (!had_samekey) _tmp.diff_keys.push(prop);
    });
    if (_tmp.diff_keys.length > 0) _tmp.changed = true;
    if (_tmp.changed == true) {
      _.each(_tmp.diff_keys, function (llave) {
        objeto[_tmp.objmap[llave].id][_tmp.objmap[llave].prop] = fila[llave];
      });
      e.section.updateItemAt(e.itemIndex, objeto);
    }
  }

  (function () {



    var urlcrawford = 'urlcrawford' in require('vars') ? require('vars')['urlcrawford'] : '';
    var cantbanners = 'cantbanners' in require('vars') ? require('vars')['cantbanners'] : '';
    var ID_623417874 = {};
    ID_623417874.success = function (e) {
      var elemento = e,
      valor = e;
      var hola_index = 0;
      _.each(elemento.imagenes, function (hola, hola_pos, hola_list) {
        hola_index += 1;
        if (false) {
          var ID_1607356917 = Ti.UI.createImageView({
            height: Ti.UI.FILL,
            width: Ti.UI.FILL,
            image: hola.url_imagen,
            id: ID_1607356917 });

        } else {
          var ID_1607356917 = Ti.UI.createImageView({
            height: Ti.UI.FILL,
            width: Ti.UI.FILL,
            image: hola.url_imagen,
            id: ID_1607356917 });

        }
        require('vars')['_ID_1607356917_original_'] = ID_1607356917.getImage();
        require('vars')['_ID_1607356917_filtro_'] = 'original';
        ID_1607356917.addEventListener('click', function (e) {
          e.cancelBubble = true;
          var elemento = e.source;
          var evento = e;
          Ti.Platform.openURL(hola.link);
        });
        $.ID_1353765508.addView(ID_1607356917);require('vars')['cantbanners'] = hola_pos + 1;
      });
      elemento = null, valor = null;
    };
    ID_623417874.error = function (e) {
      var elemento = e,
      valor = e;
      var ID_790041558_opts = [L('x3610695981_traducir', 'OK')];
      var ID_790041558 = Ti.UI.createAlertDialog({
        title: L('x3769205873_traducir', 'ALERT'),
        message: L('x3455773058_traducir', 'ERROR OBTAINING THE BANNERS'),
        buttonNames: ID_790041558_opts });

      ID_790041558.addEventListener('click', function (e) {
        var xd = ID_790041558_opts[e.index];
        xd = null;
        e.source.removeEventListener("click", arguments.callee);
      });
      ID_790041558.show();
      elemento = null, valor = null;
    };
    require('helper').ajaxUnico('ID_623417874', '' + String.format(L('x2232578092', '%1$sbanners/'), urlcrawford.toString()) + '', 'POST', { pais: 'Chile' }, 15000, ID_623417874);



    var carrusel = 0;
    var ID_1856920329_continuar = true;
    _out_vars['ID_1856920329'] = { _remove: ["clearTimeout(_out_vars['ID_1856920329']._run)"] };
    var ID_1856920329_func = function () {
      carrusel = carrusel + 1;
      var cantbanners = 'cantbanners' in require('vars') ? require('vars')['cantbanners'] : '';
      if (carrusel % cantbanners != 0 && carrusel % cantbanners != '0') {
        $.ID_1353765508.moveNext();
      } else {
        $.ID_1353765508.scrollToView(0);
      }if (ID_1856920329_continuar == true) {
        _out_vars['ID_1856920329']._run = setTimeout(ID_1856920329_func, 1000 * 5);
      }
    };
    _out_vars['ID_1856920329']._run = setTimeout(ID_1856920329_func, 1000 * 5);
    var days = L('x1272337240_traducir', 'Days');



    var tareas = 0;
    var ID_1726701770_continuar = true;
    _out_vars['ID_1726701770'] = { _remove: ["clearTimeout(_out_vars['ID_1726701770']._run)"] };
    var ID_1726701770_func = function () {
      tareas = tareas + 1;






      var ID_499911396_i = Alloy.Collections.casos;
      var sql = "DELETE FROM " + ID_499911396_i.config.adapter.collection_name;
      var db = Ti.Database.open(ID_499911396_i.config.adapter.db_name);
      db.execute(sql);
      db.close();
      ID_499911396_i.trigger('remove');



      var ID_842823704_i = Alloy.createCollection('usuario');
      var ID_842823704_i_where = '';
      ID_842823704_i.fetch();
      var user = require('helper').query2array(ID_842823704_i);
      if (Ti.App.deployType != 'production') console.log('usuario', { "asd": user });
      var ID_1202677947 = {};
      ID_1202677947.success = function (e) {
        var elemento = e,
        valor = e;
        var insp_caso_index = 0;
        _.each(elemento.inspecciones, function (insp_caso, insp_caso_pos, insp_caso_list) {
          insp_caso_index += 1;
          var ID_1199177641_m = Alloy.Collections.casos;
          var ID_1199177641_fila = Alloy.createModel('casos', {
            siniestro: insp_caso.siniestro,
            tipotexto: insp_caso.tipotexto,
            rut_inspector: insp_caso.datos_estado.rut_inspector,
            documentos: JSON.stringify(insp_caso.datos_estado.documentos),
            calificacion: insp_caso.datos_estado.calificacion,
            compania: insp_caso.compania,
            imagen_inspector: insp_caso.datos_estado.imagen_inspector,
            legal: insp_caso.legal,
            estimado: insp_caso.estimado,
            inspector: insp_caso.datos_estado.inspector,
            fecha_agendamiento: insp_caso.datos_estado.fecha_agendamiento,
            realizado_fecha: insp_caso.realizado_fecha,
            num_caso: insp_caso.datos_estado.num_caso,
            estado: insp_caso.estado,
            realizado_dias: insp_caso.realizado_dias,
            estadotexto: insp_caso.estadotexto,
            tipo: insp_caso.tipo,
            correo: insp_caso.datos_estado.correo,
            descripcion: insp_caso.datos_estado.descripcion });

          ID_1199177641_m.add(ID_1199177641_fila);
          ID_1199177641_fila.save();
          _.defer(function () {
            Alloy.Collections.casos.fetch();
          });
        });
        elemento = null, valor = null;
      };
      ID_1202677947.error = function (e) {
        var elemento = e,
        valor = e;
        var ID_1141582910_opts = [L('x3610695981_traducir', 'OK')];
        var ID_1141582910 = Ti.UI.createAlertDialog({
          title: L('x3769205873_traducir', 'ALERT'),
          message: L('x3064151243_traducir', 'ERROR OBTAINING THE CASES'),
          buttonNames: ID_1141582910_opts });

        ID_1141582910.addEventListener('click', function (e) {
          var xd = ID_1141582910_opts[e.index];
          xd = null;
          e.source.removeEventListener("click", arguments.callee);
        });
        ID_1141582910.show();
        elemento = null, valor = null;
      };
      require('helper').ajaxUnico('ID_1202677947', '' + String.format(L('x4094662162', '%1$ssiniestrosAsegurado/'), urlcrawford.toString()) + '', 'POST', { correo: user[0].email }, 15000, ID_1202677947);
      if (ID_1726701770_continuar == true) {
        _out_vars['ID_1726701770']._run = setTimeout(ID_1726701770_func, 1000 * 3600);
      }
    };
    _out_vars['ID_1726701770']._run = setTimeout(ID_1726701770_func, 1000 * 3600);
  })();

  if (false || true) {
    $.ID_1737225565.addEventListener('close', function () {
      $.destroy();
      $.off();
      var _ev_tmp = null,
      _ev_rem = null;
      if (_my_events) {
        for (_ev_tmp in _my_events) {
          try {
            if (_ev_tmp.indexOf('_web') != -1) {
              Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
            } else {
              Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
            }
          } catch (err10) {}
        }
        _my_events = null;

      }
      if (_out_vars) {
        for (_ev_tmp in _out_vars) {
          for (_ev_rem in _out_vars[_ev_tmp]._remove) {
            try {
              eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
            } catch (_errt) {}
          }
          _out_vars[_ev_tmp] = null;
        }
        _ev_tmp = null;

      }
    });
  }





  __defers['$.__views.ID_513906812!itemclick!Itemclick_ID_1481600886'] && $.addListener($.__views.ID_513906812, 'itemclick', Itemclick_ID_1481600886);



  _.extend($, exports);
}

module.exports = Controller;