var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'hogar3_index';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.ID_375356980 = Ti.UI.createWindow(
  { title: L('x3719714762_traducir', 'inicio'), titleid: "x3719714762_traducir", backgroundImage: "/images/i4DDC620865F38C5A3439087EED99348F.png", layout: "composite", windowSoftInputMode: Titanium.UI.Android.SOFT_KEYBOARD_HIDE_ON_FOCUS, id: "ID_375356980", backgroundColor: "#fafafc", theme: "sinbarra" });

  $.__views.ID_375356980 && $.addTopLevelView($.__views.ID_375356980);
  Androidback_ID_68727843 ? $.addListener($.__views.ID_375356980, 'androidback', Androidback_ID_68727843) : __defers['$.__views.ID_375356980!androidback!Androidback_ID_68727843'] = true;$.__views.ID_1073367722 = Ti.UI.createView(
  { height: "120dp", layout: "vertical", top: "55dp", id: "ID_1073367722", backgroundColor: "#ffffff" });

  $.__views.ID_375356980.add($.__views.ID_1073367722);
  $.__views.ID_121536055 = Ti.UI.createView(
  { height: "25dp", layout: "horizontal", id: "ID_121536055" });

  $.__views.ID_1073367722.add($.__views.ID_121536055);
  $.__views.ID_1756046703 = Ti.UI.createLabel(
  { color: "#4d4d4d", font: { fontFamily: "Roboto-Medium", fontSize: "16dp" }, text: L('x4262956787_traducir', 'Inspected'), touchEnabled: false, left: 15, bottom: 5, width: Ti.UI.SIZE, id: "ID_1756046703", textid: "x4262956787_traducir" });

  $.__views.ID_121536055.add($.__views.ID_1756046703);
  $.__views.ID_826724046 = Ti.UI.createLabel(
  { color: "#a0a1a3", font: { fontFamily: "Roboto-Light", fontSize: "13dp" }, touchEnabled: false, left: 10, bottom: 5, width: Ti.UI.SIZE, id: "ID_826724046" });

  $.__views.ID_121536055.add($.__views.ID_826724046);
  $.__views.ID_1709783160 = Ti.UI.createView(
  { height: "25dp", layout: "horizontal", id: "ID_1709783160" });

  $.__views.ID_1073367722.add($.__views.ID_1709783160);
  $.__views.ID_665853240 = Ti.UI.createLabel(
  { color: "#838383", font: { fontFamily: "Roboto-Medium", fontSize: "13dp" }, text: L('x3980808842_traducir', 'Time remaining:'), touchEnabled: false, left: 15, bottom: 5, width: Ti.UI.SIZE, id: "ID_665853240", textid: "x3980808842_traducir" });

  $.__views.ID_1709783160.add($.__views.ID_665853240);
  $.__views.ID_1932185343 = Ti.UI.createLabel(
  { color: "#a0a1a3", font: { fontFamily: "Roboto-Light", fontSize: "13dp" }, touchEnabled: false, left: 10, bottom: 5, width: Ti.UI.SIZE, id: "ID_1932185343" });

  $.__views.ID_1709783160.add($.__views.ID_1932185343);
  $.__views.ID_574656646 = Ti.UI.createView(
  { backgroundColor: "#e6e6e6", font: { fontSize: "12dp" }, height: "1dp", layout: "vertical", width: Ti.UI.FILL, id: "ID_574656646" });

  $.__views.ID_1073367722.add($.__views.ID_574656646);
  $.__views.ID_1655304175 = Alloy.createWidget('barracasos', 'widget', { paso: L('x1842515611', '3'), titulo1: L('x807033745_traducir', 'Received'), titulo3: L('x4262956787_traducir', 'Inspected'), titulo2: L('x807001066_traducir', 'Scheduled'), __id: "ALL1655304175", titulo5: L('x951154001_traducir', 'End'), titulo4: L('x1018769216_traducir', 'Evaluating'), id: "ID_1655304175", __parentSymbol: $.__views.ID_1073367722 });
  $.__views.ID_1655304175.setParent($.__views.ID_1073367722);
  $.__views.ID_1104347678 = Ti.UI.createView(
  { backgroundColor: "#e6e6e6", font: { fontSize: "12dp" }, height: "1dp", layout: "vertical", width: Ti.UI.FILL, id: "ID_1104347678" });

  $.__views.ID_1073367722.add($.__views.ID_1104347678);
  $.__views.ID_220723656 = Ti.UI.createScrollView(
  { showVerticalScrollIndicator: true, layout: "vertical", top: "180dp", id: "ID_220723656", showHorizontalScrollIndicator: true });

  $.__views.ID_375356980.add($.__views.ID_220723656);
  $.__views.ID_1926115404 = Alloy.createWidget('cajadescripcion', 'widget', { titulo: L('x3950563313_traducir', 'Description'), __id: "ALL1926115404", id: "ID_1926115404", __parentSymbol: $.__views.ID_220723656 });
  $.__views.ID_1926115404.setParent($.__views.ID_220723656);
  $.__views.ID_1945909135 = Ti.UI.createView(
  { height: Ti.UI.SIZE, borderColor: "#e6e6e6", layout: "vertical", width: "90%", top: "10dp", id: "ID_1945909135", borderRadius: 5, borderWidth: 1, backgroundColor: "#ffffff" });

  $.__views.ID_220723656.add($.__views.ID_1945909135);
  $.__views.ID_24942151 = Ti.UI.createLabel(
  { color: "#838383", font: { fontFamily: "Roboto-Medium", fontSize: "13dp" }, text: L('x2614953286_traducir', 'Edit contact'), touchEnabled: false, top: 10, id: "ID_24942151", textid: "x3965918122_traducir" });

  $.__views.ID_1945909135.add($.__views.ID_24942151);
  $.__views.ID_1388948574 = Ti.UI.createView(
  { height: "65dp", layout: "vertical", width: "65dp", top: "10dp", id: "ID_1388948574", borderRadius: 35 });

  $.__views.ID_1945909135.add($.__views.ID_1388948574);
  $.__views.ID_1095538453 = Ti.UI.createImageView(
  { width: "100%", image: "/images/i4A22DEA00A2F2DEEA9163E8432A3DD40.png", id: "ID_1095538453" });

  $.__views.ID_1388948574.add($.__views.ID_1095538453);
  $.__views.ID_787932640 = Ti.UI.createLabel(
  { color: "#a0a1a3", font: { fontFamily: "Roboto-Light", fontSize: "13dp" }, text: L('x4166562134_traducir', 'Inspectors name'), touchEnabled: false, top: 2, id: "ID_787932640", textid: "x4166562134_traducir" });

  $.__views.ID_1945909135.add($.__views.ID_787932640);
  $.__views.ID_156639973 = Ti.UI.createLabel(
  { color: "#a0a1a3", font: { fontFamily: "Roboto-Light", fontSize: "13dp" }, text: L('x2968366014', '10.745.986-8'), touchEnabled: false, top: 2, id: "ID_156639973", textid: "x2968366014" });

  $.__views.ID_1945909135.add($.__views.ID_156639973);
  $.__views.ID_1510189946 = Ti.UI.createView(
  { height: Ti.UI.SIZE, visible: false, layout: "horizontal", top: "2dp", id: "ID_1510189946" });

  $.__views.ID_1945909135.add($.__views.ID_1510189946);
  $.__views.ID_1753341304 = Ti.UI.createLabel(
  { color: "#a0a1a3", font: { fontFamily: "Roboto-Light", fontSize: "13dp" }, text: L('x1561639535_traducir', 'Inspection date:'), touchEnabled: false, width: Ti.UI.SIZE, id: "ID_1753341304", textid: "x1561639535_traducir" });

  $.__views.ID_1510189946.add($.__views.ID_1753341304);
  $.__views.ID_474926121 = Ti.UI.createLabel(
  { color: "#a0a1a3", font: { fontFamily: "Roboto-Light", fontSize: "13dp" }, text: L('x211534962', '0000'), touchEnabled: false, width: Ti.UI.SIZE, id: "ID_474926121", textid: "x211534962" });

  $.__views.ID_1510189946.add($.__views.ID_474926121);
  $.__views.ID_514502397 = Ti.UI.createView(
  { height: Ti.UI.SIZE, bottom: "10dp", layout: "composite", id: "ID_514502397" });

  $.__views.ID_1945909135.add($.__views.ID_514502397);
  $.__views.ID_1040567561 = Ti.UI.createView(
  { height: "53dp", visible: true, layout: "composite", width: "200dp", id: "ID_1040567561", backgroundColor: "#ffffff" });

  $.__views.ID_514502397.add($.__views.ID_1040567561);
  $.__views.ID_131489538 = Ti.UI.createView(
  { height: Ti.UI.SIZE, layout: "horizontal", id: "ID_131489538" });

  $.__views.ID_1040567561.add($.__views.ID_131489538);
  $.__views.ID_892385145 = (require("ti.animation").createLottieView || Ti.UI.createLottieView)(
  { height: "53dp", scaleMode: "fitXY", width: "32dp", file: "estrella_azul.json", disableHardwareAcceleration: true, id: "ID_892385145" });

  $.__views.ID_131489538.add($.__views.ID_892385145);
  $.__views.ID_1518069056 = (require("ti.animation").createLottieView || Ti.UI.createLottieView)(
  { height: "53dp", scaleMode: "fitXY", left: "10dp", width: "32dp", file: "estrella_azul.json", disableHardwareAcceleration: true, id: "ID_1518069056" });

  $.__views.ID_131489538.add($.__views.ID_1518069056);
  $.__views.ID_1534022184 = (require("ti.animation").createLottieView || Ti.UI.createLottieView)(
  { height: "53dp", scaleMode: "fitXY", left: "10dp", width: "32dp", file: "estrella_azul.json", disableHardwareAcceleration: true, id: "ID_1534022184" });

  $.__views.ID_131489538.add($.__views.ID_1534022184);
  $.__views.ID_1714144029 = (require("ti.animation").createLottieView || Ti.UI.createLottieView)(
  { height: "53dp", scaleMode: "fitXY", left: "10dp", width: "32dp", file: "estrella_azul.json", disableHardwareAcceleration: true, id: "ID_1714144029" });

  $.__views.ID_131489538.add($.__views.ID_1714144029);
  $.__views.ID_1922145361 = (require("ti.animation").createLottieView || Ti.UI.createLottieView)(
  { height: "53dp", scaleMode: "fitXY", left: "10dp", width: "32dp", file: "estrella_azul.json", disableHardwareAcceleration: true, id: "ID_1922145361" });

  $.__views.ID_131489538.add($.__views.ID_1922145361);
  $.__views.ID_1699365811 = Ti.UI.createView(
  { height: Ti.UI.SIZE, layout: "horizontal", id: "ID_1699365811" });

  $.__views.ID_1040567561.add($.__views.ID_1699365811);
  $.__views.ID_530780394 = Ti.UI.createView(
  { height: Ti.UI.FILL, layout: "vertical", width: "20%", id: "ID_530780394" });

  $.__views.ID_1699365811.add($.__views.ID_530780394);
  Click_ID_1398059792 ? $.addListener($.__views.ID_530780394, 'click', Click_ID_1398059792) : __defers['$.__views.ID_530780394!click!Click_ID_1398059792'] = true;$.__views.ID_1476524595 = Ti.UI.createView(
  { height: Ti.UI.FILL, layout: "vertical", width: "20%", id: "ID_1476524595" });

  $.__views.ID_1699365811.add($.__views.ID_1476524595);
  Click_ID_840242074 ? $.addListener($.__views.ID_1476524595, 'click', Click_ID_840242074) : __defers['$.__views.ID_1476524595!click!Click_ID_840242074'] = true;$.__views.ID_538604860 = Ti.UI.createView(
  { height: Ti.UI.FILL, layout: "vertical", width: "20%", id: "ID_538604860" });

  $.__views.ID_1699365811.add($.__views.ID_538604860);
  Click_ID_937322876 ? $.addListener($.__views.ID_538604860, 'click', Click_ID_937322876) : __defers['$.__views.ID_538604860!click!Click_ID_937322876'] = true;$.__views.ID_526757686 = Ti.UI.createView(
  { height: Ti.UI.FILL, layout: "vertical", width: "20%", id: "ID_526757686" });

  $.__views.ID_1699365811.add($.__views.ID_526757686);
  Click_ID_421623373 ? $.addListener($.__views.ID_526757686, 'click', Click_ID_421623373) : __defers['$.__views.ID_526757686!click!Click_ID_421623373'] = true;$.__views.ID_118194161 = Ti.UI.createView(
  { height: Ti.UI.FILL, layout: "vertical", width: "20%", id: "ID_118194161" });

  $.__views.ID_1699365811.add($.__views.ID_118194161);
  Click_ID_1724483260 ? $.addListener($.__views.ID_118194161, 'click', Click_ID_1724483260) : __defers['$.__views.ID_118194161!click!Click_ID_1724483260'] = true;$.__views.ID_1351469457 = Ti.UI.createView(
  { height: "53dp", visible: true, layout: "horizontal", width: "200dp", id: "ID_1351469457", backgroundColor: "#ffffff" });

  $.__views.ID_514502397.add($.__views.ID_1351469457);
  $.__views.ID_272445538 = (require("ti.animation").createLottieView || Ti.UI.createLottieView)(
  { height: "53dp", scaleMode: "fitXY", width: "32dp", file: "estrella_gris.json", disableHardwareAcceleration: true, id: "ID_272445538" });

  $.__views.ID_1351469457.add($.__views.ID_272445538);
  $.__views.ID_216564626 = (require("ti.animation").createLottieView || Ti.UI.createLottieView)(
  { height: "53dp", scaleMode: "fitXY", left: "10dp", width: "32dp", file: "estrella_gris.json", disableHardwareAcceleration: true, id: "ID_216564626" });

  $.__views.ID_1351469457.add($.__views.ID_216564626);
  $.__views.ID_9946986 = (require("ti.animation").createLottieView || Ti.UI.createLottieView)(
  { height: "53dp", scaleMode: "fitXY", left: "10dp", width: "32dp", file: "estrella_gris.json", disableHardwareAcceleration: true, id: "ID_9946986" });

  $.__views.ID_1351469457.add($.__views.ID_9946986);
  $.__views.ID_259517590 = (require("ti.animation").createLottieView || Ti.UI.createLottieView)(
  { height: "53dp", scaleMode: "fitXY", left: "10dp", width: "32dp", file: "estrella_gris.json", disableHardwareAcceleration: true, id: "ID_259517590" });

  $.__views.ID_1351469457.add($.__views.ID_259517590);
  $.__views.ID_1302126390 = (require("ti.animation").createLottieView || Ti.UI.createLottieView)(
  { height: "53dp", scaleMode: "fitXY", left: "10dp", width: "32dp", file: "estrella_gris.json", disableHardwareAcceleration: true, id: "ID_1302126390" });

  $.__views.ID_1351469457.add($.__views.ID_1302126390);
  $.__views.ID_1944290557 = Ti.UI.createView(
  { height: "45dp", bottom: "30dp", layout: "composite", width: Ti.UI.FILL, top: "20dp", id: "ID_1944290557" });

  $.__views.ID_220723656.add($.__views.ID_1944290557);
  $.__views.ID_126865667 = Ti.UI.createView(
  { backgroundColor: "#e6e6e6", font: { fontSize: "12dp" }, height: "45dp", visible: true, layout: "composite", width: "80%", id: "ID_126865667", borderRadius: 25 });

  $.__views.ID_1944290557.add($.__views.ID_126865667);
  $.__views.ID_1823458956 = Ti.UI.createLabel(
  { color: "#ffffff", font: { fontSize: "16dp" }, text: L('x1818803644_traducir', 'QUALIFIED'), touchEnabled: false, id: "ID_1823458956", textid: "x1818803644_traducir" });

  $.__views.ID_126865667.add($.__views.ID_1823458956);
  $.__views.ID_269232931 = Ti.UI.createView(
  { backgroundColor: "#8ce5bd", font: { fontSize: "12dp" }, height: "45dp", visible: true, layout: "composite", width: "80%", id: "ID_269232931", borderRadius: 25 });

  $.__views.ID_1944290557.add($.__views.ID_269232931);
  Click_ID_640948675 ? $.addListener($.__views.ID_269232931, 'click', Click_ID_640948675) : __defers['$.__views.ID_269232931!click!Click_ID_640948675'] = true;$.__views.ID_558165208 = Ti.UI.createActivityIndicator(
  { message: "", id: "ID_558165208" });

  $.__views.ID_269232931.add($.__views.ID_558165208);
  $.__views.ID_1558362882 = Ti.UI.createLabel(
  { color: "#ffffff", font: { fontSize: "16dp" }, text: L('x2089206772_traducir', 'SAVE CHANGES'), touchEnabled: false, visible: true, id: "ID_1558362882", textid: "x990086617_traducir" });

  $.__views.ID_269232931.add($.__views.ID_1558362882);
  $.__views.ID_1721022077 = Alloy.createWidget('barra', 'widget', { titulo: L('x1195630948_traducir', 'HOME'), nrofinal: L('x2226203566', '5'), nroinicial: L('x1842515611', '3'), fondo: L('x1602558431_traducir', 'fondoazul'), top: L('x4108050209', '0'), modal: L('x2212294583', '1'), __id: "ALL1721022077", id: "ID_1721022077", __parentSymbol: $.__views.ID_375356980 });
  $.__views.ID_1721022077.setParent($.__views.ID_375356980);
  exports.destroy = function () {};




  _.extend($, $.__views);


  var _bind4section = {};
  var _list_templates = {};

  var _activity;
  if (true) {
    _activity = $.ID_375356980.activity;var abx = require('com.alcoapps.actionbarextras');
  }
  var _my_events = {},
  _out_vars = {},
  $item = {},
  args = arguments[0] || {};if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);$item = $.item.toJSON();
  }
  var _var_scopekey = 'ID_375356980';
  require('vars')[_var_scopekey] = {};
  if (true) {
    $.ID_375356980.addEventListener('open', function (e) {});
  }

  $.ID_1655304175.init({
    __id: 'ALL1655304175',
    paso: L('x1842515611', '3'),
    titulo5: L('x951154001_traducir', 'End'),
    titulo1: L('x807033745_traducir', 'Received'),
    titulo4: L('x1018769216_traducir', 'Evaluating'),
    titulo3: L('x4262956787_traducir', 'Inspected'),
    titulo2: L('x807001066_traducir', 'Scheduled') });


  $.ID_1926115404.init({
    titulo: L('x3950563313_traducir', 'Description'),
    __id: 'ALL1926115404' });


  if (false) {
    $.ID_892385145.setTransform(Ti.UI.create2DMatrix().scale(1, 1));
    $.ID_892385145.setAnchorPoint({ x: 0, y: 0 });
  }

  if (false) {
    $.ID_1518069056.setTransform(Ti.UI.create2DMatrix().scale(1, 1));
    $.ID_1518069056.setAnchorPoint({ x: 0, y: 0 });
  }

  if (false) {
    $.ID_1534022184.setTransform(Ti.UI.create2DMatrix().scale(1, 1));
    $.ID_1534022184.setAnchorPoint({ x: 0, y: 0 });
  }

  if (false) {
    $.ID_1714144029.setTransform(Ti.UI.create2DMatrix().scale(1, 1));
    $.ID_1714144029.setAnchorPoint({ x: 0, y: 0 });
  }

  if (false) {
    $.ID_1922145361.setTransform(Ti.UI.create2DMatrix().scale(1, 1));
    $.ID_1922145361.setAnchorPoint({ x: 0, y: 0 });
  }

  function Click_ID_1398059792(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    $.ID_892385145.stop();
    $.ID_1518069056.stop();
    $.ID_1534022184.stop();
    $.ID_1714144029.stop();
    $.ID_1922145361.stop();
    require('vars')['calificacion'] = L('x2212294583', '1');
    $.ID_892385145.start();
    if (Ti.App.deployType != 'production') console.log('caja1', {});
    var ID_1518069056_progreso = 0;

    var i_setProgress = function (valor) {
      if (typeof ID_1518069056_progreso == 'string') {
        var t = parseInt(ID_1518069056_progreso.split('%').join('')) / 100;
      } else {
        var t = ID_1518069056_progreso / 100;
      }
      $.ID_1518069056.setProgress(t);
    };
    i_setProgress(ID_1518069056_progreso);

    var ID_1534022184_progreso = 0;

    var i_setProgress = function (valor) {
      if (typeof ID_1534022184_progreso == 'string') {
        var t = parseInt(ID_1534022184_progreso.split('%').join('')) / 100;
      } else {
        var t = ID_1534022184_progreso / 100;
      }
      $.ID_1534022184.setProgress(t);
    };
    i_setProgress(ID_1534022184_progreso);

    var ID_1714144029_progreso = 0;

    var i_setProgress = function (valor) {
      if (typeof ID_1714144029_progreso == 'string') {
        var t = parseInt(ID_1714144029_progreso.split('%').join('')) / 100;
      } else {
        var t = ID_1714144029_progreso / 100;
      }
      $.ID_1714144029.setProgress(t);
    };
    i_setProgress(ID_1714144029_progreso);

    var ID_1922145361_progreso = 0;

    var i_setProgress = function (valor) {
      if (typeof ID_1922145361_progreso == 'string') {
        var t = parseInt(ID_1922145361_progreso.split('%').join('')) / 100;
      } else {
        var t = ID_1922145361_progreso / 100;
      }
      $.ID_1922145361.setProgress(t);
    };
    i_setProgress(ID_1922145361_progreso);
  }
  function Click_ID_840242074(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    $.ID_892385145.stop();
    $.ID_1518069056.stop();
    $.ID_1534022184.stop();
    $.ID_1714144029.stop();
    $.ID_1922145361.stop();
    if (Ti.App.deployType != 'production') console.log('caja2', {});
    $.ID_892385145.start();
    $.ID_1518069056.start();
    require('vars')['calificacion'] = 2;
    var ID_1534022184_progreso = 0;

    var i_setProgress = function (valor) {
      if (typeof ID_1534022184_progreso == 'string') {
        var t = parseInt(ID_1534022184_progreso.split('%').join('')) / 100;
      } else {
        var t = ID_1534022184_progreso / 100;
      }
      $.ID_1534022184.setProgress(t);
    };
    i_setProgress(ID_1534022184_progreso);

    var ID_1714144029_progreso = 0;

    var i_setProgress = function (valor) {
      if (typeof ID_1714144029_progreso == 'string') {
        var t = parseInt(ID_1714144029_progreso.split('%').join('')) / 100;
      } else {
        var t = ID_1714144029_progreso / 100;
      }
      $.ID_1714144029.setProgress(t);
    };
    i_setProgress(ID_1714144029_progreso);

    var ID_1922145361_progreso = 0;

    var i_setProgress = function (valor) {
      if (typeof ID_1922145361_progreso == 'string') {
        var t = parseInt(ID_1922145361_progreso.split('%').join('')) / 100;
      } else {
        var t = ID_1922145361_progreso / 100;
      }
      $.ID_1922145361.setProgress(t);
    };
    i_setProgress(ID_1922145361_progreso);
  }
  function Click_ID_937322876(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    $.ID_892385145.stop();
    $.ID_1518069056.stop();
    $.ID_1534022184.stop();
    $.ID_1714144029.stop();
    $.ID_1922145361.stop();
    if (Ti.App.deployType != 'production') console.log('caja3', {});
    $.ID_892385145.start();
    $.ID_1518069056.start();
    $.ID_1534022184.start();
    require('vars')['calificacion'] = 3;
    var ID_1714144029_progreso = 0;

    var i_setProgress = function (valor) {
      if (typeof ID_1714144029_progreso == 'string') {
        var t = parseInt(ID_1714144029_progreso.split('%').join('')) / 100;
      } else {
        var t = ID_1714144029_progreso / 100;
      }
      $.ID_1714144029.setProgress(t);
    };
    i_setProgress(ID_1714144029_progreso);

    var ID_1922145361_progreso = 0;

    var i_setProgress = function (valor) {
      if (typeof ID_1922145361_progreso == 'string') {
        var t = parseInt(ID_1922145361_progreso.split('%').join('')) / 100;
      } else {
        var t = ID_1922145361_progreso / 100;
      }
      $.ID_1922145361.setProgress(t);
    };
    i_setProgress(ID_1922145361_progreso);
  }
  function Click_ID_421623373(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    $.ID_892385145.stop();
    $.ID_1518069056.stop();
    $.ID_1534022184.stop();
    $.ID_1714144029.stop();
    $.ID_1922145361.stop();
    if (Ti.App.deployType != 'production') console.log('caja4', {});
    $.ID_892385145.start();
    $.ID_1518069056.start();
    $.ID_1534022184.start();
    $.ID_1714144029.start();
    require('vars')['calificacion'] = 4;
    var ID_1922145361_progreso = 0;

    var i_setProgress = function (valor) {
      if (typeof ID_1922145361_progreso == 'string') {
        var t = parseInt(ID_1922145361_progreso.split('%').join('')) / 100;
      } else {
        var t = ID_1922145361_progreso / 100;
      }
      $.ID_1922145361.setProgress(t);
    };
    i_setProgress(ID_1922145361_progreso);
  }
  function Click_ID_1724483260(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    if (Ti.App.deployType != 'production') console.log('caja5', {});
    require('vars')['calificacion'] = 5;
    $.ID_892385145.stop();
    $.ID_1518069056.stop();
    $.ID_1534022184.stop();
    $.ID_1714144029.stop();
    $.ID_1922145361.stop();
    $.ID_892385145.start();
    $.ID_1518069056.start();
    $.ID_1534022184.start();
    $.ID_1714144029.start();
    $.ID_1922145361.start();
  }

  if (false) {
    $.ID_272445538.setTransform(Ti.UI.create2DMatrix().scale(1, 1));
    $.ID_272445538.setAnchorPoint({ x: 0, y: 0 });
  }

  if (false) {
    $.ID_216564626.setTransform(Ti.UI.create2DMatrix().scale(1, 1));
    $.ID_216564626.setAnchorPoint({ x: 0, y: 0 });
  }

  if (false) {
    $.ID_9946986.setTransform(Ti.UI.create2DMatrix().scale(1, 1));
    $.ID_9946986.setAnchorPoint({ x: 0, y: 0 });
  }

  if (false) {
    $.ID_259517590.setTransform(Ti.UI.create2DMatrix().scale(1, 1));
    $.ID_259517590.setAnchorPoint({ x: 0, y: 0 });
  }

  if (false) {
    $.ID_1302126390.setTransform(Ti.UI.create2DMatrix().scale(1, 1));
    $.ID_1302126390.setAnchorPoint({ x: 0, y: 0 });
  }

  function Click_ID_640948675(e) {

    e.cancelBubble = true;
    var elemento = e.source;



    var email = 'email' in require('vars') ? require('vars')['email'] : '';
    var datos = 'datos' in require('vars') ? require('vars')['datos'] : '';
    var calificacion = 'calificacion' in require('vars') ? require('vars')['calificacion'] : '';
    var urlcrawford = 'urlcrawford' in require('vars') ? require('vars')['urlcrawford'] : '';
    var ID_623417874 = {};

    ID_623417874.success = function (e) {
      var elemento = e,
      valor = e;
      if (elemento == true || elemento == 'true') {



        var ID_269232931_visible = false;

        if (ID_269232931_visible == 'si') {
          ID_269232931_visible = true;
        } else if (ID_269232931_visible == 'no') {
          ID_269232931_visible = false;
        }
        $.ID_269232931.setVisible(ID_269232931_visible);
      } else {
        var ID_1217795807_opts = [L('x3610695981_traducir', 'OK')];
        var ID_1217795807 = Ti.UI.createAlertDialog({
          title: L('x3769205873_traducir', 'ALERT'),
          message: L('x733696324_traducir', 'ERROR WITH THE SERVER'),
          buttonNames: ID_1217795807_opts });

        ID_1217795807.addEventListener('click', function (e) {
          var xd = ID_1217795807_opts[e.index];
          xd = null;

          e.source.removeEventListener("click", arguments.callee);
        });
        ID_1217795807.show();
      }elemento = null, valor = null;
    };
    require('helper').ajaxUnico('ID_623417874', '' + String.format(L('x554964598', '%1$scalificarInspector/'), urlcrawford.toString()) + '', 'POST', { correo: email, siniestro: datos.siniestro, rut_inspector: datos.rut_inspector }, 15000, ID_623417874);
    var ID_1558362882_visible = false;

    if (ID_1558362882_visible == 'si') {
      ID_1558362882_visible = true;
    } else if (ID_1558362882_visible == 'no') {
      ID_1558362882_visible = false;
    }
    $.ID_1558362882.setVisible(ID_1558362882_visible);




    $.ID_558165208.show();
  }

  $.ID_1721022077.init({
    titulo: L('x1195630948_traducir', 'HOME'),
    __id: 'ALL1721022077',
    nrofinal: L('x2226203566', '5'),
    nroinicial: L('x1842515611', '3'),
    fondo: L('x1602558431_traducir', 'fondoazul'),
    top: L('x4108050209', '0'),
    modal: L('x2212294583', '1') });


  (function () {
    var urlcrawford = 'urlcrawford' in require('vars') ? require('vars')['urlcrawford'] : '';
    var urluadjust = 'urluadjust' in require('vars') ? require('vars')['urluadjust'] : '';
    if (Ti.App.deployType != 'production') console.log('hogar3', { "datos": args });
    if (_.isObject(args) || _.isString(args) && !_.isEmpty(args) || _.isNumber(args) || _.isBoolean(args)) {






      var datos = args._data;
      if (Ti.App.deployType != 'production') console.log('carga', { "asd": datos });
      if (_.isObject(datos.imagen_inspector) || _.isString(datos.imagen_inspector) && !_.isEmpty(datos.imagen_inspector) || _.isNumber(datos.imagen_inspector) || _.isBoolean(datos.imagen_inspector)) {



        var imageBlob = Ti.Utils.base64decode(datos.imagen_inspector);
        var ID_1095538453_imagen = imageBlob;

        if (typeof ID_1095538453_imagen == 'string' && 'styles' in require('a4w') && ID_1095538453_imagen in require('a4w').styles['images']) {
          ID_1095538453_imagen = require('a4w').styles['images'][ID_1095538453_imagen];
        }
        $.ID_1095538453.setImage(ID_1095538453_imagen);
      }



      $.ID_826724046.setText(datos.compania);

      $.ID_787932640.setText(datos.inspector);

      $.ID_156639973.setText(datos.rut_inspector);

      if (_.isObject(datos.fecha_agendamiento) || _.isString(datos.fecha_agendamiento) && !_.isEmpty(datos.fecha_agendamiento) || _.isNumber(datos.fecha_agendamiento) || _.isBoolean(datos.fecha_agendamiento)) {
        $.ID_474926121.setText(datos.fecha_agendamiento);
      }



      datos.calificacion = parseInt(datos.calificacion);
      if (datos.calificacion == 0 || datos.calificacion == '0') {



        var ID_1351469457_visible = false;

        if (ID_1351469457_visible == 'si') {
          ID_1351469457_visible = true;
        } else if (ID_1351469457_visible == 'no') {
          ID_1351469457_visible = false;
        }
        $.ID_1351469457.setVisible(ID_1351469457_visible);

        if (Ti.App.deployType != 'production') console.log('sincalificacion', {});
      } else if (datos.calificacion == -1) {
        var ID_1351469457_visible = false;

        if (ID_1351469457_visible == 'si') {
          ID_1351469457_visible = true;
        } else if (ID_1351469457_visible == 'no') {
          ID_1351469457_visible = false;
        }
        $.ID_1351469457.setVisible(ID_1351469457_visible);

        if (Ti.App.deployType != 'production') console.log('sincalificacion', {});
      } else if (datos.calificacion == 1 || datos.calificacion == '1') {
        if (Ti.App.deployType != 'production') console.log('caja1', {});
        $.ID_272445538.stop();
        $.ID_216564626.stop();
        $.ID_9946986.stop();
        $.ID_259517590.stop();
        $.ID_1302126390.stop();
        $.ID_272445538.start();
        var ID_1302126390_progreso = 100;

        var i_setProgress = function (valor) {
          if (typeof ID_1302126390_progreso == 'string') {
            var t = parseInt(ID_1302126390_progreso.split('%').join('')) / 100;
          } else {
            var t = ID_1302126390_progreso / 100;
          }
          $.ID_1302126390.setProgress(t);
        };
        i_setProgress(ID_1302126390_progreso);

        var ID_259517590_progreso = 100;

        var i_setProgress = function (valor) {
          if (typeof ID_259517590_progreso == 'string') {
            var t = parseInt(ID_259517590_progreso.split('%').join('')) / 100;
          } else {
            var t = ID_259517590_progreso / 100;
          }
          $.ID_259517590.setProgress(t);
        };
        i_setProgress(ID_259517590_progreso);

        var ID_9946986_progreso = 100;

        var i_setProgress = function (valor) {
          if (typeof ID_9946986_progreso == 'string') {
            var t = parseInt(ID_9946986_progreso.split('%').join('')) / 100;
          } else {
            var t = ID_9946986_progreso / 100;
          }
          $.ID_9946986.setProgress(t);
        };
        i_setProgress(ID_9946986_progreso);

        var ID_216564626_progreso = 100;

        var i_setProgress = function (valor) {
          if (typeof ID_216564626_progreso == 'string') {
            var t = parseInt(ID_216564626_progreso.split('%').join('')) / 100;
          } else {
            var t = ID_216564626_progreso / 100;
          }
          $.ID_216564626.setProgress(t);
        };
        i_setProgress(ID_216564626_progreso);

        if (Ti.App.deployType != 'production') console.log('cali1', {});
        var ID_269232931_visible = false;

        if (ID_269232931_visible == 'si') {
          ID_269232931_visible = true;
        } else if (ID_269232931_visible == 'no') {
          ID_269232931_visible = false;
        }
        $.ID_269232931.setVisible(ID_269232931_visible);
      } else if (datos.calificacion == 2) {
        if (Ti.App.deployType != 'production') console.log('caja2', {});
        $.ID_272445538.stop();
        $.ID_216564626.stop();
        $.ID_9946986.stop();
        $.ID_259517590.stop();
        $.ID_1302126390.stop();
        $.ID_272445538.start();
        $.ID_216564626.start();
        var ID_1302126390_progreso = 100;

        var i_setProgress = function (valor) {
          if (typeof ID_1302126390_progreso == 'string') {
            var t = parseInt(ID_1302126390_progreso.split('%').join('')) / 100;
          } else {
            var t = ID_1302126390_progreso / 100;
          }
          $.ID_1302126390.setProgress(t);
        };
        i_setProgress(ID_1302126390_progreso);

        var ID_259517590_progreso = 100;

        var i_setProgress = function (valor) {
          if (typeof ID_259517590_progreso == 'string') {
            var t = parseInt(ID_259517590_progreso.split('%').join('')) / 100;
          } else {
            var t = ID_259517590_progreso / 100;
          }
          $.ID_259517590.setProgress(t);
        };
        i_setProgress(ID_259517590_progreso);

        var ID_9946986_progreso = 100;

        var i_setProgress = function (valor) {
          if (typeof ID_9946986_progreso == 'string') {
            var t = parseInt(ID_9946986_progreso.split('%').join('')) / 100;
          } else {
            var t = ID_9946986_progreso / 100;
          }
          $.ID_9946986.setProgress(t);
        };
        i_setProgress(ID_9946986_progreso);

        if (Ti.App.deployType != 'production') console.log('cali2', {});
        var ID_269232931_visible = false;

        if (ID_269232931_visible == 'si') {
          ID_269232931_visible = true;
        } else if (ID_269232931_visible == 'no') {
          ID_269232931_visible = false;
        }
        $.ID_269232931.setVisible(ID_269232931_visible);
      } else if (datos.calificacion == 3) {
        if (Ti.App.deployType != 'production') console.log('caja3', {});
        $.ID_272445538.stop();
        $.ID_216564626.stop();
        $.ID_9946986.stop();
        $.ID_259517590.stop();
        $.ID_1302126390.stop();
        $.ID_272445538.start();
        $.ID_216564626.start();
        $.ID_9946986.start();
        var ID_259517590_progreso = 0;

        var i_setProgress = function (valor) {
          if (typeof ID_259517590_progreso == 'string') {
            var t = parseInt(ID_259517590_progreso.split('%').join('')) / 100;
          } else {
            var t = ID_259517590_progreso / 100;
          }
          $.ID_259517590.setProgress(t);
        };
        i_setProgress(ID_259517590_progreso);

        var ID_1302126390_progreso = 0;

        var i_setProgress = function (valor) {
          if (typeof ID_1302126390_progreso == 'string') {
            var t = parseInt(ID_1302126390_progreso.split('%').join('')) / 100;
          } else {
            var t = ID_1302126390_progreso / 100;
          }
          $.ID_1302126390.setProgress(t);
        };
        i_setProgress(ID_1302126390_progreso);

        if (Ti.App.deployType != 'production') console.log('cali3', {});
        var ID_269232931_visible = false;

        if (ID_269232931_visible == 'si') {
          ID_269232931_visible = true;
        } else if (ID_269232931_visible == 'no') {
          ID_269232931_visible = false;
        }
        $.ID_269232931.setVisible(ID_269232931_visible);
      } else if (datos.calificacion == 4) {
        if (Ti.App.deployType != 'production') console.log('caja4', {});
        $.ID_272445538.stop();
        $.ID_216564626.stop();
        $.ID_9946986.stop();
        $.ID_259517590.stop();
        $.ID_1302126390.stop();
        $.ID_272445538.start();
        $.ID_216564626.start();
        $.ID_9946986.start();
        $.ID_259517590.start();
        var ID_1302126390_progreso = 0;

        var i_setProgress = function (valor) {
          if (typeof ID_1302126390_progreso == 'string') {
            var t = parseInt(ID_1302126390_progreso.split('%').join('')) / 100;
          } else {
            var t = ID_1302126390_progreso / 100;
          }
          $.ID_1302126390.setProgress(t);
        };
        i_setProgress(ID_1302126390_progreso);

        if (Ti.App.deployType != 'production') console.log('cali4', {});
        var ID_269232931_visible = false;

        if (ID_269232931_visible == 'si') {
          ID_269232931_visible = true;
        } else if (ID_269232931_visible == 'no') {
          ID_269232931_visible = false;
        }
        $.ID_269232931.setVisible(ID_269232931_visible);
      } else if (datos.calificacion == 5) {
        if (Ti.App.deployType != 'production') console.log('caja5', {});
        $.ID_272445538.stop();
        $.ID_216564626.stop();
        $.ID_9946986.stop();
        $.ID_259517590.stop();
        $.ID_1302126390.stop();
        $.ID_272445538.start();
        $.ID_216564626.start();
        $.ID_9946986.start();
        $.ID_259517590.start();
        $.ID_1302126390.start();
        if (Ti.App.deployType != 'production') console.log('cali5', {});
        var ID_269232931_visible = false;

        if (ID_269232931_visible == 'si') {
          ID_269232931_visible = true;
        } else if (ID_269232931_visible == 'no') {
          ID_269232931_visible = false;
        }
        $.ID_269232931.setVisible(ID_269232931_visible);
      }


      var days = L('x3957652582_traducir', 'days');
      var legal = L('x2139489547_traducir', 'legal:');
      var uadjust = L('x1807599243_traducir', 'uadjust:');



      $.ID_1932185343.setText(String.format(L('x3000044032', '%1$s %2$s %3$s %4$s %5$s %6$s'), legal ? legal.toString() : '', datos.legal.split(" ")[0] ? datos.legal.split(" ")[0].toString() : '', days ? days.toString() : '', uadjust ? uadjust.toString() : '', datos.estimado.split(" ")[0] ? datos.estimado.split(" ")[0].toString() : '', days ? days.toString() : ''));
    }
    var hog3 = L('x118206968_traducir', 'deschogar3');

    $.ID_1926115404.texto({ valor: L('x154210459_traducir', 'hog3') });
    $.ID_220723656.setShowVerticalScrollIndicator(false);
  })();

  function Androidback_ID_68727843(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    $.ID_375356980.close();
  }
  if (false || true) {
    $.ID_375356980.addEventListener('close', function () {
      $.destroy();
      $.off();
      var _ev_tmp = null,
      _ev_rem = null;
      if (_my_events) {
        for (_ev_tmp in _my_events) {
          try {
            if (_ev_tmp.indexOf('_web') != -1) {
              Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
            } else {
              Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
            }
          } catch (err10) {}
        }
        _my_events = null;

      }
      if (_out_vars) {
        for (_ev_tmp in _out_vars) {
          for (_ev_rem in _out_vars[_ev_tmp]._remove) {
            try {
              eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
            } catch (_errt) {}
          }
          _out_vars[_ev_tmp] = null;
        }
        _ev_tmp = null;

      }
    });
  }






  __defers['$.__views.ID_375356980!androidback!Androidback_ID_68727843'] && $.addListener($.__views.ID_375356980, 'androidback', Androidback_ID_68727843);__defers['$.__views.ID_530780394!click!Click_ID_1398059792'] && $.addListener($.__views.ID_530780394, 'click', Click_ID_1398059792);__defers['$.__views.ID_1476524595!click!Click_ID_840242074'] && $.addListener($.__views.ID_1476524595, 'click', Click_ID_840242074);__defers['$.__views.ID_538604860!click!Click_ID_937322876'] && $.addListener($.__views.ID_538604860, 'click', Click_ID_937322876);__defers['$.__views.ID_526757686!click!Click_ID_421623373'] && $.addListener($.__views.ID_526757686, 'click', Click_ID_421623373);__defers['$.__views.ID_118194161!click!Click_ID_1724483260'] && $.addListener($.__views.ID_118194161, 'click', Click_ID_1724483260);__defers['$.__views.ID_269232931!click!Click_ID_640948675'] && $.addListener($.__views.ID_269232931, 'click', Click_ID_640948675);



  _.extend($, exports);
}

module.exports = Controller;