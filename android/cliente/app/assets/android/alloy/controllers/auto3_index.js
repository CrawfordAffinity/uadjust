var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'auto3_index';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.ID_375356980 = Ti.UI.createWindow(
  { title: L('x3719714762_traducir', 'inicio'), titleid: "x3719714762_traducir", backgroundImage: "/images/i4DDC620865F38C5A3439087EED99348F.png", layout: "composite", windowSoftInputMode: Titanium.UI.Android.SOFT_KEYBOARD_HIDE_ON_FOCUS, id: "ID_375356980", backgroundColor: "#ffffff", theme: "sinbarra" });

  $.__views.ID_375356980 && $.addTopLevelView($.__views.ID_375356980);
  Androidback_ID_1522686406 ? $.addListener($.__views.ID_375356980, 'androidback', Androidback_ID_1522686406) : __defers['$.__views.ID_375356980!androidback!Androidback_ID_1522686406'] = true;$.__views.ID_33649477 = Ti.UI.createView(
  { height: "120dp", layout: "vertical", top: "55dp", id: "ID_33649477", backgroundColor: "#ffffff" });

  $.__views.ID_375356980.add($.__views.ID_33649477);
  $.__views.ID_1384767453 = Ti.UI.createView(
  { height: "25dp", layout: "horizontal", id: "ID_1384767453" });

  $.__views.ID_33649477.add($.__views.ID_1384767453);
  $.__views.ID_1962171680 = Ti.UI.createLabel(
  { color: "#4d4d4d", font: { fontFamily: "Roboto-Medium", fontSize: "16dp" }, text: L('x807001066_traducir', 'Scheduled'), touchEnabled: false, left: 15, bottom: 5, width: Ti.UI.SIZE, id: "ID_1962171680", textid: "x807001066_traducir" });

  $.__views.ID_1384767453.add($.__views.ID_1962171680);
  $.__views.ID_1589947158 = Ti.UI.createLabel(
  { color: "#a0a1a3", font: { fontFamily: "Roboto-Light", fontSize: "13dp" }, touchEnabled: false, left: 10, bottom: 5, width: Ti.UI.SIZE, id: "ID_1589947158" });

  $.__views.ID_1384767453.add($.__views.ID_1589947158);
  $.__views.ID_1759759164 = Ti.UI.createView(
  { height: "25dp", layout: "horizontal", id: "ID_1759759164" });

  $.__views.ID_33649477.add($.__views.ID_1759759164);
  $.__views.ID_1631013432 = Ti.UI.createLabel(
  { color: "#838383", font: { fontFamily: "Roboto-Medium", fontSize: "13dp" }, text: L('x3980808842_traducir', 'Time remaining:'), touchEnabled: false, left: 15, bottom: 5, width: Ti.UI.SIZE, id: "ID_1631013432", textid: "x3980808842_traducir" });

  $.__views.ID_1759759164.add($.__views.ID_1631013432);
  $.__views.ID_1949013077 = Ti.UI.createLabel(
  { color: "#a0a1a3", font: { fontFamily: "Roboto-Light", fontSize: "13dp" }, touchEnabled: false, left: 10, bottom: 5, width: Ti.UI.SIZE, id: "ID_1949013077" });

  $.__views.ID_1759759164.add($.__views.ID_1949013077);
  $.__views.ID_1204465151 = Ti.UI.createView(
  { backgroundColor: "#e6e6e6", font: { fontSize: "12dp" }, height: "1dp", layout: "vertical", width: Ti.UI.FILL, id: "ID_1204465151" });

  $.__views.ID_33649477.add($.__views.ID_1204465151);
  $.__views.ID_1074340138 = Alloy.createWidget('barrafraude', 'widget', { paso: L('x1842515611', '3'), titulo1: L('x807033745_traducir', 'Received'), titulo3: L('x951154001_traducir', 'End'), titulo2: L('x1018769216_traducir', 'Evaluating'), __id: "ALL1074340138", id: "ID_1074340138", __parentSymbol: $.__views.ID_33649477 });
  $.__views.ID_1074340138.setParent($.__views.ID_33649477);
  $.__views.ID_1036013130 = Ti.UI.createView(
  { backgroundColor: "#e6e6e6", font: { fontSize: "12dp" }, height: "1dp", layout: "vertical", width: Ti.UI.FILL, id: "ID_1036013130" });

  $.__views.ID_33649477.add($.__views.ID_1036013130);
  $.__views.ID_220723656 = Ti.UI.createScrollView(
  { showVerticalScrollIndicator: true, layout: "vertical", top: "180dp", id: "ID_220723656", showHorizontalScrollIndicator: true });

  $.__views.ID_375356980.add($.__views.ID_220723656);
  $.__views.ID_1926115404 = Alloy.createWidget('cajadescripcion', 'widget', { titulo: L('x3950563313_traducir', 'Description'), __id: "ALL1926115404", id: "ID_1926115404", __parentSymbol: $.__views.ID_220723656 });
  $.__views.ID_1926115404.setParent($.__views.ID_220723656);
  $.__views.ID_1805152349 = Ti.UI.createView(
  { height: Ti.UI.SIZE, bottom: "10dp", borderColor: "#e6e6e6", layout: "vertical", width: "90%", top: "10dp", id: "ID_1805152349", borderRadius: 5, borderWidth: 1, backgroundColor: "#ffffff" });

  $.__views.ID_220723656.add($.__views.ID_1805152349);
  $.__views.ID_1077472615 = (require("ti.animation").createLottieView || Ti.UI.createLottieView)(
  { height: "120dp", scaleMode: "fitXY", autoStart: true, width: "120dp", top: "5dp", file: "feliz120.json", disableHardwareAcceleration: true, id: "ID_1077472615" });

  $.__views.ID_1805152349.add($.__views.ID_1077472615);
  $.__views.ID_1200956051 = Ti.UI.createLabel(
  { color: "#ee7f7f", font: { fontFamily: "Roboto-Medium", fontSize: "16dp" }, text: L('x3021917836_traducir', 'WE HAVE RECEIVED YOUR CASE!'), touchEnabled: false, id: "ID_1200956051", textid: "x3021917836_traducir" });

  $.__views.ID_1805152349.add($.__views.ID_1200956051);
  $.__views.ID_318754451 = Ti.UI.createLabel(
  { color: "#a0a1a3", font: { fontFamily: "Roboto-Light", fontSize: "13dp" }, text: L('x1046092454_traducir', 'we have received your case, be alert for the next steps, some of them needs your information'), touchEnabled: false, bottom: 15, width: "90%", id: "ID_318754451", textid: "x1046092454_traducir", textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER });

  $.__views.ID_1805152349.add($.__views.ID_318754451);
  $.__views.ID_138747237 = Alloy.createWidget('barra', 'widget', { titulo: L('x3785008151_traducir', 'CAR'), nrofinal: L('x1842515611', '3'), nroinicial: L('x1842515611', '3'), fondo: L('x1602558431_traducir', 'fondoazul'), top: L('x4108050209', '0'), modal: L('x2212294583', '1'), __id: "ALL138747237", id: "ID_138747237", __parentSymbol: $.__views.ID_375356980 });
  $.__views.ID_138747237.setParent($.__views.ID_375356980);
  exports.destroy = function () {};




  _.extend($, $.__views);


  var _bind4section = {};
  var _list_templates = {};

  var _activity;
  if (true) {
    _activity = $.ID_375356980.activity;var abx = require('com.alcoapps.actionbarextras');
  }
  var _my_events = {},
  _out_vars = {},
  $item = {},
  args = arguments[0] || {};if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);$item = $.item.toJSON();
  }
  var _var_scopekey = 'ID_375356980';
  require('vars')[_var_scopekey] = {};
  if (true) {
    $.ID_375356980.addEventListener('open', function (e) {});
  }

  $.ID_1074340138.init({
    __id: 'ALL1074340138',
    paso: L('x1842515611', '3'),
    titulo1: L('x807033745_traducir', 'Received'),
    titulo3: L('x951154001_traducir', 'End'),
    titulo2: L('x1018769216_traducir', 'Evaluating') });


  $.ID_1926115404.init({
    titulo: L('x3950563313_traducir', 'Description'),
    __id: 'ALL1926115404' });


  if (false) {
    $.ID_1077472615.setTransform(Ti.UI.create2DMatrix().scale(1, 1));
    $.ID_1077472615.setAnchorPoint({ x: 0, y: 0 });
  }

  $.ID_138747237.init({
    titulo: L('x3785008151_traducir', 'CAR'),
    __id: 'ALL138747237',
    nrofinal: L('x1842515611', '3'),
    nroinicial: L('x1842515611', '3'),
    fondo: L('x1602558431_traducir', 'fondoazul'),
    top: L('x4108050209', '0'),
    modal: L('x2212294583', '1') });


  function Androidback_ID_1522686406(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    $.ID_375356980.close();
  }

  (function () {
    $.ID_220723656.setShowVerticalScrollIndicator(false);
    if (_.isObject(args) || _.isString(args) && !_.isEmpty(args) || _.isNumber(args) || _.isBoolean(args)) {



      datos = args._data;
      if (Ti.App.deployType != 'production') console.log('carga', { "asd": datos });



      var docu = JSON.parse(datos.documentos);
      var hola_index = 0;
      _.each(docu, function (hola, hola_pos, hola_list) {
        hola_index += 1;
        var ID_1309026480 = Titanium.UI.createView({
          height: '50dp',
          bottom: '10dp',
          borderColor: '#e6e6e6',
          layout: 'composite',
          width: '90%',
          top: '10dp',
          borderWidth: 1,
          borderRadius: 5,
          backgroundColor: '#FFFFFF' });

        var ID_1265644961 = Titanium.UI.createLabel({
          bottom: 15,
          text: hola.nombre,
          color: '#2d9edb',
          touchEnabled: false,
          top: 15,
          font: {
            fontFamily: 'Roboto-Regular',
            fontSize: '14dp' } });



        ID_1309026480.add(ID_1265644961);

        ID_1309026480.addEventListener('click', function (e) {
          e.cancelBubble = true;
          var elemento = e.source;
          Ti.Platform.openURL(hola.url);
        });
        $.ID_220723656.add(ID_1309026480);
      });
      var days = L('x3957652582_traducir', 'days');
      var legal = L('x2139489547_traducir', 'legal:');
      var uadjust = L('x1807599243_traducir', 'uadjust:');
      $.ID_1949013077.setText(String.format(L('x3000044032', '%1$s %2$s %3$s %4$s %5$s %6$s'), legal ? legal.toString() : '', datos.legal.split(" ")[0] ? datos.legal.split(" ")[0].toString() : '', days ? days.toString() : '', uadjust ? uadjust.toString() : '', datos.estimado.split(" ")[0] ? datos.estimado.split(" ")[0].toString() : '', days ? days.toString() : ''));

      $.ID_1589947158.setText(datos.compania);
    }
    var car3 = L('x3667068218_traducir', 'descauto3');

    $.ID_1926115404.texto({ valor: L('x3810221188_traducir', 'car3') });
  })();

  if (false || true) {
    $.ID_375356980.addEventListener('close', function () {
      $.destroy();
      $.off();
      var _ev_tmp = null,
      _ev_rem = null;
      if (_my_events) {
        for (_ev_tmp in _my_events) {
          try {
            if (_ev_tmp.indexOf('_web') != -1) {
              Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
            } else {
              Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
            }
          } catch (err10) {}
        }
        _my_events = null;

      }
      if (_out_vars) {
        for (_ev_tmp in _out_vars) {
          for (_ev_rem in _out_vars[_ev_tmp]._remove) {
            try {
              eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
            } catch (_errt) {}
          }
          _out_vars[_ev_tmp] = null;
        }
        _ev_tmp = null;

      }
    });
  }






  __defers['$.__views.ID_375356980!androidback!Androidback_ID_1522686406'] && $.addListener($.__views.ID_375356980, 'androidback', Androidback_ID_1522686406);



  _.extend($, exports);
}

module.exports = Controller;