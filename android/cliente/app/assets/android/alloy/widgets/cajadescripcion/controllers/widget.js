var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;


function WPATH(s) {
	var index = s.lastIndexOf('/');
	var path = index === -1 ?
	'cajadescripcion/' + s :
	s.substring(0, index) + '/cajadescripcion/' + s.substring(index + 1);

	return path.indexOf('/') !== 0 ? '/' + path : path;
}

function __processArg(obj, key) {
	var arg = null;
	if (obj) {
		arg = obj[key] || null;
		delete obj[key];
	}
	return arg;
}

function Controller() {
	var Widget = new (require('/alloy/widget'))('cajadescripcion');this.__widgetId = 'cajadescripcion';
	require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
	this.__controllerPath = 'widget';
	this.args = arguments[0] || {};

	if (arguments[0]) {
		var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
		var $model = __processArg(arguments[0], '$model');
		var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
	}
	var $ = this;
	var exports = {};
	var __defers = {};







	$.__views.ID_904182391 = Ti.UI.createView(
	{ height: Ti.UI.SIZE, bottom: "10dp", borderColor: "#e6e6e6", layout: "vertical", width: "90%", top: "10dp", id: "ID_904182391", borderRadius: 5, borderWidth: 1, backgroundColor: "#ffffff" });

	$.__views.ID_904182391 && $.addTopLevelView($.__views.ID_904182391);
	$.__views.ID_173279620 = Ti.UI.createLabel(
	{ color: "#838383", font: { fontFamily: "Roboto-Medium", fontSize: "13dp" }, text: 'Description', touchEnabled: false, left: 17, width: "90%", top: 15, id: "ID_173279620" });

	$.__views.ID_904182391.add($.__views.ID_173279620);
	$.__views.ID_1893708853 = Ti.UI.createLabel(
	{ color: "#a0a1a3", font: { fontFamily: "Roboto-Light", fontSize: "13dp" }, text: 'Proin feuguat aliquam tortor, id hendreirit urna semper pulvina.', touchEnabled: false, bottom: 15, width: "90%", id: "ID_1893708853" });

	$.__views.ID_904182391.add($.__views.ID_1893708853);
	exports.destroy = function () {};




	_.extend($, $.__views);










	var _bind4section = {};

	var args = arguments[0] || {};

	$.init = function (params) {
		for (var tobe in params) args[tobe] = params[tobe];
		if ('__id' in params) require(WPATH('vars'))[params.__id] = {};
		if (!_.isUndefined(params.titulo)) {
			$.ID_173279620.setText(params.titulo);
		} else if (!_.isUndefined(params.descripcion)) {
			$.ID_1893708853.setText(params.descripcion);
		}
	};

	$.texto = function (params) {
		if (!_.isUndefined(params.valor)) {
			$.ID_1893708853.setText(params.valor);
		}
	};









	_.extend($, exports);
}

module.exports = Controller;