var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;


function WPATH(s) {
		var index = s.lastIndexOf('/');
		var path = index === -1 ?
		'barra/' + s :
		s.substring(0, index) + '/barra/' + s.substring(index + 1);

		return path.indexOf('/') !== 0 ? '/' + path : path;
}

function __processArg(obj, key) {
		var arg = null;
		if (obj) {
				arg = obj[key] || null;
				delete obj[key];
		}
		return arg;
}

function Controller() {
		var Widget = new (require('/alloy/widget'))('barra');this.__widgetId = 'barra';
		require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
		this.__controllerPath = 'widget';
		this.args = arguments[0] || {};

		if (arguments[0]) {
				var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
				var $model = __processArg(arguments[0], '$model');
				var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
		}
		var $ = this;
		var exports = {};
		var __defers = {};







		$.__views.ID_748382869 = Ti.UI.createView(
		{ backgroundColor: "#2d9edb", font: { fontSize: "12dp" }, height: "55dp", layout: "composite", width: "100%", top: "0dp", id: "ID_748382869", elevation: 5 });

		$.__views.ID_748382869 && $.addTopLevelView($.__views.ID_748382869);
		$.__views.ID_56241598 = Ti.UI.createLabel(
		{ color: "#ffffff", font: { fontFamily: "Roboto-Medium", fontSize: "18dp" }, text: 'TITULO', touchEnabled: false, id: "ID_56241598" });

		$.__views.ID_748382869.add($.__views.ID_56241598);
		$.__views.ID_569043732 = Ti.UI.createView(
		{ height: Ti.UI.SIZE, visible: false, layout: "horizontal", width: "50dp", right: "10dp", id: "ID_569043732" });

		$.__views.ID_748382869.add($.__views.ID_569043732);
		$.__views.ID_1631729973 = Ti.UI.createLabel(
		{ color: "#ffffff", font: { fontFamily: "Roboto-Medium", fontSize: "18dp" }, text: '1', touchEnabled: false, width: "33%", id: "ID_1631729973" });

		$.__views.ID_569043732.add($.__views.ID_1631729973);
		$.__views.ID_962221305 = Ti.UI.createLabel(
		{ color: "#ffffff", font: { fontFamily: "Roboto-Medium", fontSize: "18dp" }, text: '/', touchEnabled: false, width: "33%", id: "ID_962221305" });

		$.__views.ID_569043732.add($.__views.ID_962221305);
		$.__views.ID_1484477318 = Ti.UI.createLabel(
		{ color: "#ffffff", font: { fontFamily: "Roboto-Medium", fontSize: "18dp" }, text: '2', touchEnabled: false, width: "33%", id: "ID_1484477318" });

		$.__views.ID_569043732.add($.__views.ID_1484477318);
		exports.destroy = function () {};




		_.extend($, $.__views);












		var _bind4section = {};

		var args = arguments[0] || {};

		$.init = function (params) {
				for (var tobe in params) args[tobe] = params[tobe];
				if ('__id' in params) require(WPATH('vars'))[params.__id] = {};
				if (!_.isUndefined(params.titulo)) {
						$.ID_56241598.setText(params.titulo);
				}
				if (!_.isUndefined(params.fondo)) {
						var ID_748382869_estilo = params.fondo;

						var setEstilo = function (clase) {
								if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
										try {
												$.ID_748382869.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
										} catch (sete_err) {}
								}
						};setEstilo(ID_748382869_estilo);
				}
				if (!_.isUndefined(params.modal)) {
						$.ID_56241598.setLeft(20);
				}
				if (!_.isUndefined(params.nroinicial)) {
						$.ID_1631729973.setText(params.nroinicial);

						var ID_569043732_visible = true;

						if (ID_569043732_visible == 'si') {
								ID_569043732_visible = true;
						} else if (ID_569043732_visible == 'no') {
								ID_569043732_visible = false;
						}
						$.ID_569043732.setVisible(ID_569043732_visible);
				}
				if (!_.isUndefined(params.nrofinal)) {
						$.ID_1484477318.setText(params.nrofinal);

						var ID_569043732_visible = true;

						if (ID_569043732_visible == 'si') {
								ID_569043732_visible = true;
						} else if (ID_569043732_visible == 'no') {
								ID_569043732_visible = false;
						}
						$.ID_569043732.setVisible(ID_569043732_visible);
				}
		};









		_.extend($, exports);
}

module.exports = Controller;