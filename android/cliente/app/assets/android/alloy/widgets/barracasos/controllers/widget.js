var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;


function WPATH(s) {
  var index = s.lastIndexOf('/');
  var path = index === -1 ?
  'barracasos/' + s :
  s.substring(0, index) + '/barracasos/' + s.substring(index + 1);

  return path.indexOf('/') !== 0 ? '/' + path : path;
}

function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
    delete obj[key];
  }
  return arg;
}

function Controller() {
  var Widget = new (require('/alloy/widget'))('barracasos');this.__widgetId = 'barracasos';
  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'widget';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.ID_856701346 = Ti.UI.createView(
  { height: "65dp", layout: "horizontal", id: "ID_856701346", backgroundColor: "#ffffff" });

  $.__views.ID_856701346 && $.addTopLevelView($.__views.ID_856701346);
  $.__views.ID_867794261 = Ti.UI.createView(
  { height: Ti.UI.FILL, layout: "composite", width: "18%", id: "ID_867794261" });

  $.__views.ID_856701346.add($.__views.ID_867794261);
  $.__views.ID_1412811338 = Ti.UI.createView(
  { backgroundColor: "#e6e6e6", font: { fontSize: "12dp" }, height: "3dp", layout: "vertical", width: "50%", right: "0dp", id: "ID_1412811338" });

  $.__views.ID_867794261.add($.__views.ID_1412811338);
  $.__views.ID_1631216700 = Ti.UI.createImageView(
  { image: WPATH('images/i02DA78CF21C46D9FAF1D7028DAD7B263.png'), height: "15dp", id: "ID_1631216700" });

  $.__views.ID_867794261.add($.__views.ID_1631216700);
  $.__views.ID_320920405 = Ti.UI.createLabel(
  { color: "#a0a1a3", font: { fontFamily: "Roboto-Light", fontSize: "12dp" }, text: '1', touchEnabled: false, top: 5, id: "ID_320920405" });

  $.__views.ID_867794261.add($.__views.ID_320920405);
  $.__views.ID_834025233 = Ti.UI.createView(
  { height: Ti.UI.FILL, layout: "composite", width: "20%", id: "ID_834025233" });

  $.__views.ID_856701346.add($.__views.ID_834025233);
  $.__views.ID_1063795638 = Ti.UI.createView(
  { backgroundColor: "#e6e6e6", font: { fontSize: "12dp" }, height: "3dp", left: "0dp", layout: "vertical", width: "50%", id: "ID_1063795638" });

  $.__views.ID_834025233.add($.__views.ID_1063795638);
  $.__views.ID_6714774 = Ti.UI.createView(
  { backgroundColor: "#e6e6e6", font: { fontSize: "12dp" }, height: "3dp", layout: "vertical", width: "50%", right: "0dp", id: "ID_6714774" });

  $.__views.ID_834025233.add($.__views.ID_6714774);
  $.__views.ID_568818512 = Ti.UI.createLabel(
  { color: "#a0a1a3", font: { fontFamily: "Roboto-Light", fontSize: "12dp" }, text: '2', touchEnabled: false, bottom: 5, id: "ID_568818512" });

  $.__views.ID_834025233.add($.__views.ID_568818512);
  $.__views.ID_381422891 = Ti.UI.createImageView(
  { image: WPATH('images/i2AB525E172B40F7964CDADA256C27D7E.png'), height: "15dp", id: "ID_381422891" });

  $.__views.ID_834025233.add($.__views.ID_381422891);
  $.__views.ID_614362517 = Ti.UI.createView(
  { height: Ti.UI.FILL, layout: "composite", width: "24%", id: "ID_614362517" });

  $.__views.ID_856701346.add($.__views.ID_614362517);
  $.__views.ID_1075196614 = Ti.UI.createView(
  { backgroundColor: "#e6e6e6", font: { fontSize: "12dp" }, height: "3dp", left: "0dp", layout: "vertical", width: "50%", id: "ID_1075196614" });

  $.__views.ID_614362517.add($.__views.ID_1075196614);
  $.__views.ID_1962540837 = Ti.UI.createView(
  { backgroundColor: "#e6e6e6", font: { fontSize: "12dp" }, height: "3dp", layout: "vertical", width: "50%", right: "0dp", id: "ID_1962540837" });

  $.__views.ID_614362517.add($.__views.ID_1962540837);
  $.__views.ID_1942830887 = Ti.UI.createLabel(
  { color: "#a0a1a3", font: { fontFamily: "Roboto-Light", fontSize: "12dp" }, text: '3', touchEnabled: false, top: 5, id: "ID_1942830887" });

  $.__views.ID_614362517.add($.__views.ID_1942830887);
  $.__views.ID_1091144769 = Ti.UI.createImageView(
  { image: WPATH('images/i700EF80CBB75C24AA82D473B0152E89C.png'), height: "15dp", id: "ID_1091144769" });

  $.__views.ID_614362517.add($.__views.ID_1091144769);
  $.__views.ID_1476821417 = Ti.UI.createView(
  { height: Ti.UI.FILL, layout: "composite", width: "19%", id: "ID_1476821417" });

  $.__views.ID_856701346.add($.__views.ID_1476821417);
  $.__views.ID_1548105713 = Ti.UI.createView(
  { backgroundColor: "#e6e6e6", font: { fontSize: "12dp" }, height: "3dp", left: "0dp", layout: "vertical", width: "50%", id: "ID_1548105713" });

  $.__views.ID_1476821417.add($.__views.ID_1548105713);
  $.__views.ID_1855025197 = Ti.UI.createView(
  { backgroundColor: "#e6e6e6", font: { fontSize: "12dp" }, height: "3dp", layout: "vertical", width: "50%", right: "0dp", id: "ID_1855025197" });

  $.__views.ID_1476821417.add($.__views.ID_1855025197);
  $.__views.ID_1779340269 = Ti.UI.createLabel(
  { color: "#a0a1a3", font: { fontFamily: "Roboto-Light", fontSize: "12dp" }, text: '4', touchEnabled: false, bottom: 5, id: "ID_1779340269" });

  $.__views.ID_1476821417.add($.__views.ID_1779340269);
  $.__views.ID_1778314790 = Ti.UI.createImageView(
  { image: WPATH('images/i7D03144E54AE7E1DF58A7E3E9B594B77.png'), height: "15dp", id: "ID_1778314790" });

  $.__views.ID_1476821417.add($.__views.ID_1778314790);
  $.__views.ID_1842348998 = Ti.UI.createView(
  { height: Ti.UI.FILL, layout: "composite", width: "18%", id: "ID_1842348998" });

  $.__views.ID_856701346.add($.__views.ID_1842348998);
  $.__views.ID_68533462 = Ti.UI.createView(
  { backgroundColor: "#e6e6e6", font: { fontSize: "12dp" }, height: "3dp", left: "0dp", layout: "vertical", width: "50%", id: "ID_68533462" });

  $.__views.ID_1842348998.add($.__views.ID_68533462);
  $.__views.ID_1055421960 = Ti.UI.createLabel(
  { color: "#a0a1a3", font: { fontFamily: "Roboto-Light", fontSize: "12dp" }, text: '5', touchEnabled: false, top: 5, id: "ID_1055421960" });

  $.__views.ID_1842348998.add($.__views.ID_1055421960);
  $.__views.ID_800179407 = Ti.UI.createImageView(
  { image: WPATH('images/iF336EEE68C31DE77682B4B4B656D2DED.png'), height: "15dp", id: "ID_800179407" });

  $.__views.ID_1842348998.add($.__views.ID_800179407);
  exports.destroy = function () {};




  _.extend($, $.__views);













  var _bind4section = {};

  var args = arguments[0] || {};

  $.init = function (params) {
    for (var tobe in params) args[tobe] = params[tobe];
    if ('__id' in params) require(WPATH('vars'))[params.__id] = {};
    if (params.paso == 1 || params.paso == '1') {
      var ID_1631216700_imagen = '1on';

      if (typeof ID_1631216700_imagen == 'string' && 'styles' in require(WPATH('a4w')) && ID_1631216700_imagen in require(WPATH('a4w')).styles['images']) {
        ID_1631216700_imagen = WPATH(require(WPATH('a4w')).styles['images'][ID_1631216700_imagen]);
      }
      $.ID_1631216700.setImage(ID_1631216700_imagen);

      $.ID_320920405.setColor('#000000');
    }
    if (params.paso == 2) {
      var ID_1631216700_imagen = '1off';

      if (typeof ID_1631216700_imagen == 'string' && 'styles' in require(WPATH('a4w')) && ID_1631216700_imagen in require(WPATH('a4w')).styles['images']) {
        ID_1631216700_imagen = WPATH(require(WPATH('a4w')).styles['images'][ID_1631216700_imagen]);
      }
      $.ID_1631216700.setImage(ID_1631216700_imagen);

      var ID_1412811338_estilo = 'fondoazul';

      var setEstilo = function (clase) {
        if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
          try {
            $.ID_1412811338.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
          } catch (sete_err) {}
        }
      };setEstilo(ID_1412811338_estilo);

      var ID_1063795638_estilo = 'fondoazul';

      var setEstilo = function (clase) {
        if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
          try {
            $.ID_1063795638.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
          } catch (sete_err) {}
        }
      };setEstilo(ID_1063795638_estilo);

      var ID_381422891_imagen = '2on';

      if (typeof ID_381422891_imagen == 'string' && 'styles' in require(WPATH('a4w')) && ID_381422891_imagen in require(WPATH('a4w')).styles['images']) {
        ID_381422891_imagen = WPATH(require(WPATH('a4w')).styles['images'][ID_381422891_imagen]);
      }
      $.ID_381422891.setImage(ID_381422891_imagen);

      $.ID_568818512.setColor('#000000');
    }
    if (params.paso == 3) {
      var ID_1631216700_imagen = '1off';

      if (typeof ID_1631216700_imagen == 'string' && 'styles' in require(WPATH('a4w')) && ID_1631216700_imagen in require(WPATH('a4w')).styles['images']) {
        ID_1631216700_imagen = WPATH(require(WPATH('a4w')).styles['images'][ID_1631216700_imagen]);
      }
      $.ID_1631216700.setImage(ID_1631216700_imagen);

      var ID_381422891_imagen = '2off';

      if (typeof ID_381422891_imagen == 'string' && 'styles' in require(WPATH('a4w')) && ID_381422891_imagen in require(WPATH('a4w')).styles['images']) {
        ID_381422891_imagen = WPATH(require(WPATH('a4w')).styles['images'][ID_381422891_imagen]);
      }
      $.ID_381422891.setImage(ID_381422891_imagen);

      var ID_1091144769_imagen = '3on';

      if (typeof ID_1091144769_imagen == 'string' && 'styles' in require(WPATH('a4w')) && ID_1091144769_imagen in require(WPATH('a4w')).styles['images']) {
        ID_1091144769_imagen = WPATH(require(WPATH('a4w')).styles['images'][ID_1091144769_imagen]);
      }
      $.ID_1091144769.setImage(ID_1091144769_imagen);

      var ID_1412811338_estilo = 'fondoazul';

      var setEstilo = function (clase) {
        if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
          try {
            $.ID_1412811338.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
          } catch (sete_err) {}
        }
      };setEstilo(ID_1412811338_estilo);

      var ID_1063795638_estilo = 'fondoazul';

      var setEstilo = function (clase) {
        if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
          try {
            $.ID_1063795638.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
          } catch (sete_err) {}
        }
      };setEstilo(ID_1063795638_estilo);

      var ID_6714774_estilo = 'fondoazul';

      var setEstilo = function (clase) {
        if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
          try {
            $.ID_6714774.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
          } catch (sete_err) {}
        }
      };setEstilo(ID_6714774_estilo);

      var ID_1075196614_estilo = 'fondoazul';

      var setEstilo = function (clase) {
        if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
          try {
            $.ID_1075196614.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
          } catch (sete_err) {}
        }
      };setEstilo(ID_1075196614_estilo);

      $.ID_1942830887.setColor('#000000');
    }
    if (params.paso == 4) {
      var ID_1631216700_imagen = '1off';

      if (typeof ID_1631216700_imagen == 'string' && 'styles' in require(WPATH('a4w')) && ID_1631216700_imagen in require(WPATH('a4w')).styles['images']) {
        ID_1631216700_imagen = WPATH(require(WPATH('a4w')).styles['images'][ID_1631216700_imagen]);
      }
      $.ID_1631216700.setImage(ID_1631216700_imagen);

      var ID_381422891_imagen = '2off';

      if (typeof ID_381422891_imagen == 'string' && 'styles' in require(WPATH('a4w')) && ID_381422891_imagen in require(WPATH('a4w')).styles['images']) {
        ID_381422891_imagen = WPATH(require(WPATH('a4w')).styles['images'][ID_381422891_imagen]);
      }
      $.ID_381422891.setImage(ID_381422891_imagen);

      var ID_1091144769_imagen = '3off';

      if (typeof ID_1091144769_imagen == 'string' && 'styles' in require(WPATH('a4w')) && ID_1091144769_imagen in require(WPATH('a4w')).styles['images']) {
        ID_1091144769_imagen = WPATH(require(WPATH('a4w')).styles['images'][ID_1091144769_imagen]);
      }
      $.ID_1091144769.setImage(ID_1091144769_imagen);

      var ID_1778314790_imagen = '4on';

      if (typeof ID_1778314790_imagen == 'string' && 'styles' in require(WPATH('a4w')) && ID_1778314790_imagen in require(WPATH('a4w')).styles['images']) {
        ID_1778314790_imagen = WPATH(require(WPATH('a4w')).styles['images'][ID_1778314790_imagen]);
      }
      $.ID_1778314790.setImage(ID_1778314790_imagen);

      var ID_1412811338_estilo = 'fondoazul';

      var setEstilo = function (clase) {
        if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
          try {
            $.ID_1412811338.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
          } catch (sete_err) {}
        }
      };setEstilo(ID_1412811338_estilo);

      var ID_1063795638_estilo = 'fondoazul';

      var setEstilo = function (clase) {
        if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
          try {
            $.ID_1063795638.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
          } catch (sete_err) {}
        }
      };setEstilo(ID_1063795638_estilo);

      var ID_1075196614_estilo = 'fondoazul';

      var setEstilo = function (clase) {
        if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
          try {
            $.ID_1075196614.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
          } catch (sete_err) {}
        }
      };setEstilo(ID_1075196614_estilo);

      var ID_6714774_estilo = 'fondoazul';

      var setEstilo = function (clase) {
        if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
          try {
            $.ID_6714774.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
          } catch (sete_err) {}
        }
      };setEstilo(ID_6714774_estilo);

      var ID_1962540837_estilo = 'fondoazul';

      var setEstilo = function (clase) {
        if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
          try {
            $.ID_1962540837.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
          } catch (sete_err) {}
        }
      };setEstilo(ID_1962540837_estilo);

      var ID_1548105713_estilo = 'fondoazul';

      var setEstilo = function (clase) {
        if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
          try {
            $.ID_1548105713.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
          } catch (sete_err) {}
        }
      };setEstilo(ID_1548105713_estilo);

      $.ID_1779340269.setColor('#000000');
    }
    if (params.paso == 5) {
      var ID_1631216700_imagen = '1off';

      if (typeof ID_1631216700_imagen == 'string' && 'styles' in require(WPATH('a4w')) && ID_1631216700_imagen in require(WPATH('a4w')).styles['images']) {
        ID_1631216700_imagen = WPATH(require(WPATH('a4w')).styles['images'][ID_1631216700_imagen]);
      }
      $.ID_1631216700.setImage(ID_1631216700_imagen);

      var ID_381422891_imagen = '2off';

      if (typeof ID_381422891_imagen == 'string' && 'styles' in require(WPATH('a4w')) && ID_381422891_imagen in require(WPATH('a4w')).styles['images']) {
        ID_381422891_imagen = WPATH(require(WPATH('a4w')).styles['images'][ID_381422891_imagen]);
      }
      $.ID_381422891.setImage(ID_381422891_imagen);

      var ID_1091144769_imagen = '3off';

      if (typeof ID_1091144769_imagen == 'string' && 'styles' in require(WPATH('a4w')) && ID_1091144769_imagen in require(WPATH('a4w')).styles['images']) {
        ID_1091144769_imagen = WPATH(require(WPATH('a4w')).styles['images'][ID_1091144769_imagen]);
      }
      $.ID_1091144769.setImage(ID_1091144769_imagen);

      var ID_1778314790_imagen = '4off';

      if (typeof ID_1778314790_imagen == 'string' && 'styles' in require(WPATH('a4w')) && ID_1778314790_imagen in require(WPATH('a4w')).styles['images']) {
        ID_1778314790_imagen = WPATH(require(WPATH('a4w')).styles['images'][ID_1778314790_imagen]);
      }
      $.ID_1778314790.setImage(ID_1778314790_imagen);

      var ID_800179407_imagen = '5on';

      if (typeof ID_800179407_imagen == 'string' && 'styles' in require(WPATH('a4w')) && ID_800179407_imagen in require(WPATH('a4w')).styles['images']) {
        ID_800179407_imagen = WPATH(require(WPATH('a4w')).styles['images'][ID_800179407_imagen]);
      }
      $.ID_800179407.setImage(ID_800179407_imagen);

      var ID_1412811338_estilo = 'fondoazul';

      var setEstilo = function (clase) {
        if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
          try {
            $.ID_1412811338.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
          } catch (sete_err) {}
        }
      };setEstilo(ID_1412811338_estilo);

      var ID_1063795638_estilo = 'fondoazul';

      var setEstilo = function (clase) {
        if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
          try {
            $.ID_1063795638.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
          } catch (sete_err) {}
        }
      };setEstilo(ID_1063795638_estilo);

      var ID_1075196614_estilo = 'fondoazul';

      var setEstilo = function (clase) {
        if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
          try {
            $.ID_1075196614.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
          } catch (sete_err) {}
        }
      };setEstilo(ID_1075196614_estilo);

      var ID_1962540837_estilo = 'fondoazul';

      var setEstilo = function (clase) {
        if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
          try {
            $.ID_1962540837.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
          } catch (sete_err) {}
        }
      };setEstilo(ID_1962540837_estilo);

      var ID_1548105713_estilo = 'fondoazul';

      var setEstilo = function (clase) {
        if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
          try {
            $.ID_1548105713.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
          } catch (sete_err) {}
        }
      };setEstilo(ID_1548105713_estilo);

      var ID_1855025197_estilo = 'fondoazul';

      var setEstilo = function (clase) {
        if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
          try {
            $.ID_1855025197.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
          } catch (sete_err) {}
        }
      };setEstilo(ID_1855025197_estilo);

      var ID_68533462_estilo = 'fondoazul';

      var setEstilo = function (clase) {
        if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
          try {
            $.ID_68533462.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
          } catch (sete_err) {}
        }
      };setEstilo(ID_68533462_estilo);

      var ID_6714774_estilo = 'fondoazul';

      var setEstilo = function (clase) {
        if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
          try {
            $.ID_6714774.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
          } catch (sete_err) {}
        }
      };setEstilo(ID_6714774_estilo);

      $.ID_1055421960.setColor('#000000');
    }
    if (!_.isUndefined(params.titulo1)) {
      $.ID_320920405.setText(params.titulo1);
    }
    if (!_.isUndefined(params.titulo2)) {
      $.ID_568818512.setText(params.titulo2);
    }
    if (!_.isUndefined(params.titulo3)) {
      $.ID_1942830887.setText(params.titulo3);
    }
    if (!_.isUndefined(params.titulo4)) {
      $.ID_1779340269.setText(params.titulo4);
    }
    if (!_.isUndefined(params.titulo5)) {
      $.ID_1055421960.setText(params.titulo5);
    }
  };









  _.extend($, exports);
}

module.exports = Controller;