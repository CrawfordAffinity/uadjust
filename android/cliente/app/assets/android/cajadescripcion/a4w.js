exports.events = _.clone(Backbone.Events);
exports.ajaxkeys = {};
exports.variables = {};
exports.styles = {"images":{},"classes":{"est1":{"color":"#838383","font":{"fontFamily":"Roboto-Medium","fontSize":"13dp"}},"est5":{"color":"#a0a1a3","font":{"fontFamily":"Roboto-Light","fontSize":"13dp"}}}};
exports.fontello = {};
