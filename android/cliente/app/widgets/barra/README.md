Widget Barra
La funcion de este widget es crear una vista que parezca una barra. Posee textos que son utilizados a lo largo del proyecto

@params titulo Texto que se mostrara en la barra
@params fondo Define el color de fondo que tendra la barra
@params modal Mueve el titulo hacia la izquierda
@params nroinicial Indica la posicion en la que esta el usuario en cierta seccion
@params nrofinal Indica la totalidad de pantallas que tiene cierta seccion
