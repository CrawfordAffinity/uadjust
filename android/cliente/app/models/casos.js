exports.definition = {
	config: {
		columns: {
			"tipotexto": "TEXT",
			"siniestro": "TEXT",
			"rut_inspector": "TEXT",
			"documentos": "TEXT",
			"calificacion": "TEXT",
			"compania": "TEXT",
			"legal": "TEXT",
			"imagen_inspector": "TEXT",
			"estimado": "TEXT",
			"inspector": "TEXT",
			"realizado_fecha": "TEXT",
			"fecha_agendamiento": "TEXT",
			"num_caso": "TEXT",
			"realizado_dias": "TEXT",
			"estado": "TEXT",
			"id": "INTEGER PRIMARY KEY AUTOINCREMENT",
			"estadotexto": "TEXT",
			"tipo": "TEXT",
			"correo": "TEXT",
			"descripcion": "TEXT",
		},
		adapter: {
			"type": "sql",
			"collection_name": "casos",
			"idAttribute": "id",
			"remoteBackup": "false",
		}
	},
	extendModel: function(Model) {
		_.extend(Model.prototype, {
			transform: function transform() {
				var transformed = this.toJSON();
				return transformed;
			}
		});
		return Model;
	},
	extendCollection: function(Collection) {
		// helper functions
		function S4() {
			return (0 | 65536 * (1 + Math.random())).toString(16).substring(1);
		}
		function guid() {
			return S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4();
		}
		// start extend
		_.extend(Collection.prototype, {
			deleteAll : function() {
				var collection = this;
				var sql = "DELETE FROM " + collection.config.adapter.collection_name;
				db = Ti.Database.open(collection.config.adapter.db_name);
				db.execute(sql);
				db.close();
				collection.trigger('sync');
			}
		});
		// end extend
		return Collection;
	}
};
