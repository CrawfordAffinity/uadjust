exports.definition = {
	config: {
		columns: {
			"fecha_tarea": "TEXT",
			"fecha_sinestro": "TEXT",
			"id_inspeccion": "INTEGER",
			"id_asegurado": "INTEGER",
			"nivel_2": "TEXT",
			"comentario_can_o_rech": "TEXT",
			"estado_tarea": "INTEGER",
			"bono": "TEXT",
			"id_inspector": "INTEGER",
			"evento": "TEXT",
			"lat": "TEXT",
			"nivel_1": "INTEGER",
			"pais": "INTEGER",
			"direccion": "TEXT",
			"asegurador": "TEXT",
			"fecha_ingreso": "TEXT",
			"distance": "INTEGER",
			"data": "TEXT",
			"nivel_4": "TEXT",
			"id_server": "INTEGER",
			"categoria": "INTEGER",
			"nivel_3": "TEXT",
			"num_caso": "INTEGER",
			"lon": "TEXT",
			"id": "INTEGER PRIMARY KEY AUTOINCREMENT",
			"exige_medidas": "INTEGER",
			"nivel_5": "TEXT",
			"tipo_tarea": "INTEGER",
		},
		adapter: {
			"type": "sql",
			"collection_name": "emergencia_ubicacion",
			"idAttribute": "id",
			"remoteBackup": "false",
		}
	},
	extendModel: function(Model) {
		_.extend(Model.prototype, {
			transform: function transform() {
				var transformed = this.toJSON();
				return transformed;
			}
		});
		return Model;
	},
	extendCollection: function(Collection) {
		// helper functions
		function S4() {
			return (0 | 65536 * (1 + Math.random())).toString(16).substring(1);
		}
		function guid() {
			return S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4();
		}
		// start extend
		_.extend(Collection.prototype, {
			deleteAll : function() {
				var collection = this;
				var sql = "DELETE FROM " + collection.config.adapter.collection_name;
				db = Ti.Database.open(collection.config.adapter.db_name);
				db.execute(sql);
				db.close();
				collection.trigger('sync');
			}
		});
		// end extend
		return Collection;
	}
};
