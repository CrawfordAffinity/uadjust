exports.definition = {
	config: {
		columns: {
			"codigo_verificador": "TEXT",
			"tel_movi": "TEXT",
			"nombre": "TEXT",
			"tel_fijo": "TEXT",
			"id_tarea": "INTEGER",
			"id": "INTEGER PRIMARY KEY AUTOINCREMENT",
			"correo": "TEXT",
		},
		adapter: {
			"type": "sql",
			"collection_name": "asegurado",
			"idAttribute": "id",
			"remoteBackup": "false",
		}
	},
	extendModel: function(Model) {
		_.extend(Model.prototype, {
			transform: function transform() {
				var transformed = this.toJSON();
				return transformed;
			}
		});
		return Model;
	},
	extendCollection: function(Collection) {
		// helper functions
		function S4() {
			return (0 | 65536 * (1 + Math.random())).toString(16).substring(1);
		}
		function guid() {
			return S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4();
		}
		// start extend
		_.extend(Collection.prototype, {
			deleteAll : function() {
				var collection = this;
				var sql = "DELETE FROM " + collection.config.adapter.collection_name;
				db = Ti.Database.open(collection.config.adapter.db_name);
				db.execute(sql);
				db.close();
				collection.trigger('sync');
			}
		});
		// end extend
		return Collection;
	}
};
