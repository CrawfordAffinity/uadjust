Alloy.Events = _.clone(Backbone.Events);
Alloy.Globals.mod_ti_map = require('ti.map');
Alloy.Collections.marcas = Alloy.createCollection('marcas');
Alloy.Collections.entidad_financiera = Alloy.createCollection('entidad_financiera');
Alloy.Collections.emergencia_ubicacion = Alloy.createCollection('emergencia_ubicacion');
Alloy.Collections.estructura_cubierta = Alloy.createCollection('estructura_cubierta');
Alloy.Collections.pavimento = Alloy.createCollection('pavimento');
Alloy.Collections.compania = Alloy.createCollection('compania');
Alloy.Collections.insp_itemdanos = Alloy.createCollection('insp_itemdanos');
Alloy.Collections.numero_unico = Alloy.createCollection('numero_unico');
Alloy.Collections.insp_firma = Alloy.createCollection('insp_firma');
Alloy.Collections.muros_tabiques = Alloy.createCollection('muros_tabiques');
Alloy.Collections.insp_niveles = Alloy.createCollection('insp_niveles');
Alloy.Collections.insp_contenido = Alloy.createCollection('insp_contenido');
Alloy.Collections.inspectores = Alloy.createCollection('inspectores');
Alloy.Collections.emergencia_perfil = Alloy.createCollection('emergencia_perfil');
Alloy.Collections.pais = Alloy.createCollection('pais');
Alloy.Collections.medidas_seguridad = Alloy.createCollection('medidas_seguridad');
Alloy.Collections.tipo_accion = Alloy.createCollection('tipo_accion');
Alloy.Collections.estructura_soportante = Alloy.createCollection('estructura_soportante');
Alloy.Collections.entrepisos = Alloy.createCollection('entrepisos');
Alloy.Collections.insp_recintos = Alloy.createCollection('insp_recintos');
Alloy.Collections.insp_datosbasicos = Alloy.createCollection('insp_datosbasicos');
Alloy.Collections.tipo_partida = Alloy.createCollection('tipo_partida');
Alloy.Collections.unidad_medida = Alloy.createCollection('unidad_medida');
Alloy.Collections.tipo_dano = Alloy.createCollection('tipo_dano');
Alloy.Collections.emergencia = Alloy.createCollection('emergencia');
Alloy.Collections.inspeccion = Alloy.createCollection('inspeccion');
Alloy.Collections.asegurado = Alloy.createCollection('asegurado');
Alloy.Collections.inspecciones = Alloy.createCollection('inspecciones');
Alloy.Collections.monedas = Alloy.createCollection('monedas');
Alloy.Collections.insp_documentos = Alloy.createCollection('insp_documentos');
Alloy.Collections.insp_siniestro = Alloy.createCollection('insp_siniestro');
Alloy.Collections.cubierta = Alloy.createCollection('cubierta');
Alloy.Collections.historial_tareas = Alloy.createCollection('historial_tareas');
Alloy.Collections.tipo_siniestro = Alloy.createCollection('tipo_siniestro');
Alloy.Collections.nivel1 = Alloy.createCollection('nivel1');
Alloy.Collections.insp_fotosrequeridas = Alloy.createCollection('insp_fotosrequeridas');
Alloy.Collections.tareas = Alloy.createCollection('tareas');
Alloy.Collections.experiencia_oficio = Alloy.createCollection('experiencia_oficio');
Alloy.Collections.insp_caracteristicas = Alloy.createCollection('insp_caracteristicas');
Alloy.Collections.destino = Alloy.createCollection('destino');
Alloy.Collections.bienes = Alloy.createCollection('bienes');
Alloy.Collections.tareas_entrantes = Alloy.createCollection('tareas_entrantes');
Alloy.Collections.cola = Alloy.createCollection('cola');
//contenido de nodo global..
(function() {
var _my_events = {}, _out_vars = {}, _var_scopekey='ID_1502919501'; require('vars')[_var_scopekey]={};
require('vars')['url_server']='http://api-qa.uadjust.com:9999/api/';
require('vars')['gps_error']='true';
/** 
* Lo siguiente actualiza solicita al sistema operativo que indique su posicion mas precisa con mejor consumo de bateria 
*/

var ID_1368607013 = function(evento) {
if (evento.error==false||evento.error=='false') {
/** 
* En el caso que no haya error capturando ubicacion gps de usuario 
*/
/** 
* Guardamos variables para decir que no hubo error al obtener la posicion, tambien guardar la posicion y coordenadas 
*/
require('vars')['gps_error']='false';
require('vars')['gps_latitud']=evento.latitude;
require('vars')['gps_longitud']=evento.longitude;
require('vars')['gps_coords']=evento.coords;
if (Ti.App.deployType != 'production') console.log('psb localizacion de usuario actualizada con exito',{"evento":evento});
/** 
* Recuperamos variables del inspector y el estado de la ultima inspeccion para obtener las tareas del inspector 
*/
var inspector = ('inspector' in require('vars'))?require('vars')['inspector']:'';
var inspeccion_encurso = ('inspeccion_encurso' in require('vars'))?require('vars')['inspeccion_encurso']:'';
if (inspeccion_encurso==false||inspeccion_encurso=='false') {
if (_.isObject(inspector)) {
var seguir_tarea = ('seguir_tarea' in require('vars'))?require('vars')['seguir_tarea']:'';
var url_server = ('url_server' in require('vars'))?require('vars')['url_server']:'';
if (_.isNumber(seguir_tarea)) {
/** 
* Mandamos ubicacion del inspector al servidor 
*/
/** 
* Mandamos ubicacion del inspector al servidor 
*/
var ID_585034087={};

ID_585034087.success = function(e) {
var elemento=e, valor=e;
if (Ti.App.deployType != 'production') console.log('respuesta exitosa de guardarUbicacion',{"datos":elemento});
elemento=null, valor=null;
};

ID_585034087.error = function(e) {
var elemento=e, valor=e;
if (Ti.App.deployType != 'production') console.log('respuesta fallida de guardarUbicacion',{"datos":elemento});
elemento=null, valor=null;
};
require('helper').ajaxUnico('ID_585034087', ''+url_server+'guardarUbicacion'+'', 'POST', { id_inspector:inspector.id_server,id_tarea:seguir_tarea,lat:evento.latitude,lon:evento.longitude }, 15000, ID_585034087);
}
/** 
* Consultamos al servidor por las tareas del inspector, en caso de que hayan tareas, limpiamos y cargamos tablas. Despues de que hayan sido cargadas, refrescamos la lista de tareas en pantalla y ruta de las tareas 
*/
var ID_1844268925={};

ID_1844268925.success = function(e) {
var elemento=e, valor=e;
if (elemento.error==0||elemento.error=='0') {
var ID_1372828275_i=Alloy.Collections.tareas;
var sql = "DELETE FROM " + ID_1372828275_i.config.adapter.collection_name;
var db = Ti.Database.open(ID_1372828275_i.config.adapter.db_name);
db.execute(sql);
db.close();
sql=null;
db=null;
ID_1372828275_i.trigger('delete');
var elemento_mistareas = elemento.mistareas;
var ID_1183371961_m=Alloy.Collections.tareas;
var db_ID_1183371961 = Ti.Database.open(ID_1183371961_m.config.adapter.db_name);
db_ID_1183371961.execute('BEGIN');
_.each(elemento_mistareas, function(ID_1183371961_fila,pos) {
   db_ID_1183371961.execute('INSERT INTO tareas (fecha_tarea, id_inspeccion, id_asegurado, nivel_2, comentario_can_o_rech, asegurado_tel_fijo, estado_tarea, bono, evento, id_inspector, asegurado_codigo_identificador, lat, nivel_1, asegurado_nombre, pais, direccion, asegurador, fecha_ingreso, fecha_siniestro, nivel_1_texto, distance, nivel_4, perfil, asegurado_id, pais_texto, id_server, categoria, nivel_3, asegurado_correo, num_caso, lon, asegurado_tel_movil, nivel_5, tipo_tarea) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',ID_1183371961_fila.fecha_tarea, ID_1183371961_fila.id_inspeccion, ID_1183371961_fila.id_asegurado, ID_1183371961_fila.nivel_2, ID_1183371961_fila.comentario_can_o_rech, ID_1183371961_fila.asegurado_tel_fijo, ID_1183371961_fila.estado_tarea, ID_1183371961_fila.bono, ID_1183371961_fila.evento, ID_1183371961_fila.id_inspector, ID_1183371961_fila.asegurado_codigo_identificador, ID_1183371961_fila.lat, ID_1183371961_fila.nivel_1, ID_1183371961_fila.asegurado_nombre, ID_1183371961_fila.pais, ID_1183371961_fila.direccion, ID_1183371961_fila.asegurador, ID_1183371961_fila.fecha_ingreso, ID_1183371961_fila.fecha_siniestro, ID_1183371961_fila.nivel_1_, ID_1183371961_fila.distancia, ID_1183371961_fila.nivel_4, 'ubicacion', ID_1183371961_fila.asegurado_id, ID_1183371961_fila.pais, ID_1183371961_fila.id, ID_1183371961_fila.categoria, ID_1183371961_fila.nivel_3, ID_1183371961_fila.asegurado_correo, ID_1183371961_fila.num_caso, ID_1183371961_fila.lon, ID_1183371961_fila.asegurado_tel_movil, ID_1183371961_fila.nivel_5, ID_1183371961_fila.tipo_tarea);
});
db_ID_1183371961.execute('COMMIT');
db_ID_1183371961.close();
db_ID_1183371961 = null;
ID_1183371961_m.trigger('change');
if (Ti.App.deployType != 'production') console.log('psb: refrescando mistareas',{});
var ID_93848163_func = function() {

Alloy.Events.trigger('_refrescar_tareas_mistareas');

Alloy.Events.trigger('_calcular_ruta');
};
var ID_93848163 = setTimeout(ID_93848163_func, 1000*0.1);
}
elemento=null, valor=null;
};

ID_1844268925.error = function(e) {
var elemento=e, valor=e;
if (Ti.App.deployType != 'production') console.log('respuesta fallida de obtenerMisTareas',{"datos":elemento});
elemento=null, valor=null;
};
require('helper').ajaxUnico('ID_1844268925', ''+url_server+'obtenerMisTareas'+'', 'POST', { id_inspector:inspector.id_server,lat:evento.latitude,lon:evento.longitude }, 15000, ID_1844268925);
}
}
}
 else {
if (Ti.App.deployType != 'production') console.log('psb ha ocurrido error localizando por equipo a usuario',{"evento":evento});
require('vars')['gps_error']='true';
}
};
var ID_1368607013_locupd = function() {
         Titanium.Geolocation.purpose = 'Requerido para aplicacion.';
         Titanium.Geolocation.accuracy = Ti.Geolocation.ACCURACY_HUNDRED_METERS;
         Titanium.Geolocation.distanceFilter = 100;
         Titanium.Geolocation.preferredProvider = Ti.Geolocation.PROVIDER_GPS;
};
ID_1368607013_moment = require('alloy/moment');
if (OS_ANDROID) {
   var providerGps = Ti.Geolocation.Android.createLocationProvider({
   		name: Ti.Geolocation.PROVIDER_GPS,
   		minUpdateDistance: 100.0,
   		minUpdateTime: 0
   });
   Ti.Geolocation.Android.addLocationProvider(providerGps);
   Ti.Geolocation.Android.manualMode = true;
   Titanium.Geolocation.addEventListener('location',function(ee) { if (ee && ee.coords) { ID_1368607013({ error:false, error_data:{}, latitude:('latitude' in ee.coords)?ee.coords.latitude:-1, longitude:('longitude' in ee.coords)?ee.coords.longitude:-1, coords:ee.coords, date:ID_1368607013_moment(ee.coords.timestamp).format() }); } });
} else if (OS_IOS) {
   if (Ti.Geolocation.hasLocationPermissions(Ti.Geolocation.AUTHORIZATION_WHEN_IN_USE)) {
      if (Ti.Geolocation.locationServicesEnabled) {
         ID_1368607013_locupd();
         Titanium.Geolocation.addEventListener('location', function(ee) { if (ee && ee.coords) { ID_1368607013({ error:false, error_data:{}, latitude:('latitude' in ee.coords)?ee.coords.latitude:-1, longitude:('longitude' in ee.coords)?ee.coords.longitude:-1, coords:ee.coords, date:ID_1368607013_moment(ee.coords.timestamp).format() }); } });
      }
   } else {
      Ti.Geolocation.requestLocationPermissions(Ti.Geolocation.AUTHORIZATION_WHEN_IN_USE, function(u) {
         if (u.success) {
            ID_1368607013_locupd();
            Titanium.Geolocation.addEventListener('location', function(ee) { if (ee && ee.coords) { ID_1368607013({ error:false, error_data:{}, latitude:('latitude' in ee.coords)?ee.coords.latitude:-1, longitude:('longitude' in ee.coords)?ee.coords.longitude:-1, coords:ee.coords, date:ID_1368607013_moment(ee.coords.timestamp).format()  }); } });
         } else {
            ID_1368607013({ error:true, error_data:u, latitude:-1, longitude:-1, coords:{}, date:ID_1368607013_moment().format() });
         }
      });
   }
}
var ID_697357331 = Ti.UI.createView({});
var ID_697357331_cloud = require('ti.cloud');
var ID_697357331_register = function(_meta) {
var elemento = _meta.value;
if (Ti.App.deployType != 'production') console.log('registrado',{"datos":elemento});
require('vars')['devicetoken']=elemento;
Ti.App.Properties.setString('devicetoken',JSON.stringify(elemento));
elemento=null;
};
var ID_697357331_message = function(_meta) {
var elemento = _meta.value;
 mensaje = JSON.parse(elemento);
if (Ti.App.deployType != 'production') console.log('llego mensaje',{"datos":mensaje});
var ID_930544837_opts=['Aceptar','Cancelar'];
var ID_930544837 = Ti.UI.createAlertDialog({
   title: 'Notificacion',
   message: ''+mensaje.android.alert+'',
   buttonNames: ID_930544837_opts
});
ID_930544837.addEventListener('click', function(e) {
   var cosa=ID_930544837_opts[e.index];
cosa = null;

   e.source.removeEventListener("click", arguments.callee);
});
ID_930544837.show();
if (mensaje.confirmacion==true||mensaje.confirmacion=='true') {
Ti.App.Properties.setString('confirmarpush',JSON.stringify(true));
var confirmarpush=JSON.parse(Ti.App.Properties.getString('confirmarpush'));
var moment = require('alloy/moment');
var fecha_hoy = moment(new Date()).format('YYYY-MM-DD');
var ID_821971401_i=Alloy.Collections.consultarpush;
var sql = "DELETE FROM " + ID_821971401_i.config.adapter.collection_name;
var db = Ti.Database.open(ID_821971401_i.config.adapter.db_name);
db.execute(sql);
db.close();
sql=null;
db=null;
ID_821971401_i.trigger('delete');
var ID_951910938_m=Alloy.Collections.consultarpush;
var ID_951910938_fila = Alloy.createModel('consultarpush', {
fecha : fecha_hoy
}
 );
ID_951910938_m.add(ID_951910938_fila);
ID_951910938_fila.save();
}
elemento=null;
};
var ID_697357331_error = function(_meta) {
var elemento = _meta.value;
if (Ti.App.deployType != 'production') console.log('error en aceptarPush',{"datos":elemento});
elemento=null;
};
if (OS_ANDROID) {
  var ID_697357331_cloudpush = require('ti.cloudpush');
	ID_697357331_cloudpush.retrieveDeviceToken({
		success: function(e) { 
			ID_697357331_cloud.PushNotifications.subscribeToken({
				device_token: e.deviceToken,
				channel: 'generico',
				type: 'android'
			}, function(ee) {
				if (ee.success) {
 					if (typeof ID_697357331_register != 'undefined') ID_697357331_register({ value:e.deviceToken });
				} else {
 					if (typeof ID_697357331_error != 'undefined') ID_697357331_error({ value:ee.message, code:ee.error });
				}
			});
		},
		error: function(e) {
 			if (typeof ID_697357331_error != 'undefined') ID_697357331_error({ value:e.error, code:'token' });
 		},
	});
	ID_697357331_cloudpush.addEventListener('callback', function(evt) { if (typeof ID_697357331_message != 'undefined') ID_697357331_message({ value:evt.payload }); });
	ID_697357331_cloudpush.addEventListener('trayClick', function(evt) { if (typeof ID_697357331_message != 'undefined') ID_697357331_message({ value:evt.payload }); });
} else if (OS_IOS) {
	if (Ti.Platform.name == 'iPhone OS' && parseInt(Ti.Platform.version.split('.')[0]) >= 8) {
		Ti.App.iOS.addEventListener('usernotificationsettings', function registerForPush() {
			Ti.App.iOS.removeEventListener('usernotificationsettings', registerForPush);
			Ti.Network.registerForPushNotifications({
				success: function(e) {
					ID_697357331_cloud.PushNotifications.subscribeToken({
						device_token: e.deviceToken,
						channel: 'generico',
						type: 'ios'
					}, function(ee) {
					 	if (ee.success) {
 						    if (typeof ID_697357331_register != 'undefined') ID_697357331_register({ value:e.deviceToken });
						} else {
 						   if (typeof ID_697357331_error != 'undefined') ID_697357331_error({ value:ee.message, code:ee.error });
						}
					});
				},
				error: function(e) { if (typeof ID_697357331_error != 'undefined') ID_697357331_error({ value:e.error }); },
				callback: function(e) { if (typeof ID_697357331_message != 'undefined') ID_697357331_message({ value:e }); }
			});
		});
		Ti.App.iOS.registerUserNotificationSettings({
			types:	[Ti.App.iOS.USER_NOTIFICATION_TYPE_ALERT,Ti.App.iOS.USER_NOTIFICATION_TYPE_SOUND,Ti.App.iOS.USER_NOTIFICATION_TYPE_BADGE]
		});
	} else {
		Ti.Network.registerForPushNotifications({
			types: [Ti.App.iOS.USER_NOTIFICATION_TYPE_ALERT,Ti.App.iOS.USER_NOTIFICATION_TYPE_SOUND,Ti.App.iOS.USER_NOTIFICATION_TYPE_BADGE],
			success: function(e) { 
				ID_697357331_cloud.PushNotifications.subscribeToken({
					device_token: e.deviceToken,
					channel: 'generico',
					type: 'ios'
				}, function(ee) {
					if (ee.success) {
 					    if (typeof ID_697357331_register != 'undefined') ID_697357331_register({ value:e.deviceToken });
					} else {
 					   if (typeof ID_697357331_error != 'undefined') ID_697357331_error({ value:ee.message, code:ee.error });
					}
				});
 			},
			error: function(e) { if (typeof ID_697357331_error != 'undefined') ID_697357331_error({ value:e.error }); },
			callback: function(e) { if (typeof ID_697357331_message != 'undefined') ID_697357331_message({ value:e }); }
		});
	}
}
/** 
* Ciclo de notificaciones 
*/
var hola = 0;
var ID_1235979713_continuar = true;
_out_vars['ID_1235979713'] = { _remove:["clearTimeout(_out_vars['ID_1235979713']._run)"] };
var ID_1235979713_func = function() {
hola = hola+1;
if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
var ID_223777049 = null;
if ('consumir_cola' in require('funciones')) {
  ID_223777049 = require('funciones').consumir_cola({  });
} else {
  try {
     ID_223777049 = f_consumir_cola({});
  } catch(ee) {
  }
}
}
if (ID_1235979713_continuar==true) {
   _out_vars['ID_1235979713']._run = setTimeout(ID_1235979713_func, 1000*30);
}
};
_out_vars['ID_1235979713']._run = setTimeout(ID_1235979713_func, 1000*30);
})();
