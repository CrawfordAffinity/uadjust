var _bind4section={};

var args = arguments[0] || {};

function Click_ID_926713238(e) {

e.cancelBubble=true;
var elemento=e.source;
/* Envia a la pantalla del modal */Widget.createController("modal_multiple",{'__args' : (typeof args!=='undefined')?args:__args}).getView().open();

}

$.init = function(params) {
   for (var tobe in params) args[tobe] = params[tobe];
   if ('__id' in params) require(WPATH('vars'))[params.__id]={};
require(WPATH('vars'))[args.__id]['params']=params;
if (!_.isUndefined(params.titulo)) {
/* Revisamos los parametros al iniciar el widget y modificamos lo que sea pertinente */
/* Cambia el texto del titulo con el valor que se envia desde la pantalla que utiliza el widget */
$.ID_372511936.setText(params.titulo);

}
if (!_.isUndefined(params.hint)) {
/* Cambia el texto de hint con el valor que se envia desde la pantalla que utiliza el widget */
$.ID_1041790471.setText(params.hint);

/* Mostramos hint */
var ID_1041790471_visible = true;

								  if (ID_1041790471_visible=='si') {
									  ID_1041790471_visible=true;
								  } else if (ID_1041790471_visible=='no') {
									  ID_1041790471_visible=false;
								  }
								  $.ID_1041790471.setVisible(ID_1041790471_visible);

/* Ocultamos valor */
var ID_952750652_visible = false;

								  if (ID_952750652_visible=='si') {
									  ID_952750652_visible=true;
								  } else if (ID_952750652_visible=='no') {
									  ID_952750652_visible=false;
								  }
								  $.ID_952750652.setVisible(ID_952750652_visible);

}
if (!_.isUndefined(params.top)) {
/* Define el top que tendra la vista */
$.ID_967942946.setTop(params.top);

}
if (!_.isUndefined(params.left)) {
/* Define el left que tendra la vista */
$.ID_967942946.setLeft(params.left);

}
if (!_.isUndefined(params.subleft)) {
/* Define el left que tendra que tendra el titulo */
$.ID_372511936.setLeft(params.subleft);

}
if (!_.isUndefined(params.right)) {
/* Define el right que tendra la vista */
$.ID_967942946.setRight(params.right);

}
if (!_.isUndefined(params.bottom)) {
/* Define el bottom que tendra la vista */
$.ID_967942946.setBottom(params.bottom);

}
if (!_.isUndefined(params.ancho)) {
/* Define el ancho que tendra la vista */
var ID_967942946_ancho = params.ancho;

								  if (ID_967942946_ancho=='*') {
									  ID_967942946_ancho=Ti.UI.FILL;
								  } else if (ID_967942946_ancho=='-') {
									  ID_967942946_ancho=Ti.UI.SIZE;
								  } else if (!isNaN(ID_967942946_ancho)) {
									  ID_967942946_ancho=ID_967942946_ancho+'dp';
								  }
								  $.ID_967942946.setWidth(ID_967942946_ancho);

}
if (!_.isUndefined(params.alto)) {
/* Define el alto que tendra la vista */
var ID_967942946_alto = params.alto;

								  if (ID_967942946_alto=='*') {
									  ID_967942946_alto=Ti.UI.FILL;
								  } else if (ID_967942946_alto=='-') {
									  ID_967942946_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_967942946_alto)) {
									  ID_967942946_alto=ID_967942946_alto+'dp';
								  }
								  $.ID_967942946.setHeight(ID_967942946_alto);

}
 else if (!_.isUndefined(params.__id)) {
/* Si tenemos id de instancia de widget, guardamos id de instancia en var.&#160; Establecemos scope para datos de modelo local */
/* Guarda la variable id_instacia con el id del widget */
require(WPATH('vars'))[args.__id]['id_instancia']=params.__id;
}/* Llama el evento afterinit por si se requiere ejecutar algo despu&#233;s de que haya cargado el widget */
if ('__args' in args) {
	args['__args'].onafterinit({});
} else {
	args.onafterinit({});
}
};


$.update = function(params) {
if (!_.isUndefined(params.data)) {
/* Recibimos datos en caso de que se hayan mandado datos desde la pantalla que ocupa el widget */
/* Se guardan los datos que trae lo que venga de la pantalla que utiliza el widget */
require(WPATH('vars'))[args.__id]['data']=params.data;
if (_.isArray(params.data)) {
/* Si los datos que se traen de la pantalla que utiliza el widget es un array */
/* Recupera la variable id_instacia con el id del widget */
var id_instancia = ('id_instancia' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['id_instancia']:'';
/* Se eliminan los datos que esten cargados en el modelo temp_multiple *//* Se eliminan los datos que esten cargados en el modelo temp_multiple */
Widget.Collections.temp_multiple.fetch();
var ID_1380315445_i=Widget.Collections.temp_multiple;
//filtramos a borrar
var to_delete_ID_1380315445=[];
for (var reg_ID_1380315445=0;reg_ID_1380315445<ID_1380315445_i.models.length;reg_ID_1380315445++) {
  if ('idinstancia' in ID_1380315445_i.models[reg_ID_1380315445].attributes && ID_1380315445_i.models[reg_ID_1380315445].attributes['idinstancia'] === id_instancia) {
    to_delete_ID_1380315445.push(Widget.Collections.temp_multiple.at(reg_ID_1380315445));
  }
}
//borramos marcados
for (var ii_ID_1380315445 in to_delete_ID_1380315445) {
  to_delete_ID_1380315445[ii_ID_1380315445].destroy();
  Widget.Collections.temp_multiple.remove(to_delete_ID_1380315445[ii_ID_1380315445], { silent:true });
  Widget.Collections.temp_multiple.remove(to_delete_ID_1380315445[ii_ID_1380315445]);
}
ID_1380315445_i.trigger('remove');
var params_data = params.data;
var ID_1293594803_m=Widget.Collections.instance("temp_multiple");
_.each(params_data, function(ID_1293594803_fila,pos) {
   var ID_1293594803_modelo = Widget.createModel('temp_multiple', { valor : ID_1293594803_fila.valor, 
idinstancia : id_instancia, 
estado : ID_1293594803_fila.estado, 
amostrar : ID_1293594803_fila.label });
   ID_1293594803_m.add(ID_1293594803_modelo, { silent: true });
   ID_1293594803_modelo.save();
});
_.defer(function() { 
   Widget.Collections.instance("temp_multiple").fetch();
});
}
}
if (!_.isUndefined(params.valor)) {
/* Revisamos si el parametro valor existe */
if ((_.isObject(params.valor) ||_.isString(params.valor)) &&  _.isEmpty(params.valor)) {
/* Revisamos si el string de valor esta vacio */
/* Mostramos hint */
var ID_1041790471_visible = true;

								  if (ID_1041790471_visible=='si') {
									  ID_1041790471_visible=true;
								  } else if (ID_1041790471_visible=='no') {
									  ID_1041790471_visible=false;
								  }
								  $.ID_1041790471.setVisible(ID_1041790471_visible);

/* Ocultamos valor */
var ID_952750652_visible = false;

								  if (ID_952750652_visible=='si') {
									  ID_952750652_visible=true;
								  } else if (ID_952750652_visible=='no') {
									  ID_952750652_visible=false;
								  }
								  $.ID_952750652.setVisible(ID_952750652_visible);

}
 else {
/* Mostramos valor indicado */
$.ID_952750652.setText(params.valor);

var ID_952750652_visible = true;

								  if (ID_952750652_visible=='si') {
									  ID_952750652_visible=true;
								  } else if (ID_952750652_visible=='no') {
									  ID_952750652_visible=false;
								  }
								  $.ID_952750652.setVisible(ID_952750652_visible);

/* Ocultamos hint */
var ID_1041790471_visible = false;

								  if (ID_1041790471_visible=='si') {
									  ID_1041790471_visible=true;
								  } else if (ID_1041790471_visible=='no') {
									  ID_1041790471_visible=false;
								  }
								  $.ID_1041790471.setVisible(ID_1041790471_visible);

}}
 else {
/* Recuperamos los datos que fueron mandados desde la pantalla que utiliza el widget */
var data = ('data' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['data']:'';
/* Creamos una variable local que guardara los valores escogidos */
require(WPATH('vars'))[args.__id]['elegidos']='';
/* Recupera la variable id_instacia con el id del widget */
var id_instancia = ('id_instancia' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['id_instancia']:'';
/* Consultamos el modelo temp_multiple, filtrando por la id_instancia del widget y ordenados por el valor de amostrar de manera ascendente. Y el resultadoes almacenado en una variable llamada datos */
if (Widget.Collections.temp_multiple.models.length==0) Widget.Collections.temp_multiple.fetch();
var ID_389555799_i=Widget.Collections.temp_multiple;
//filtramos modelos segun consulta (where futuro linkeado) y armamos respuesta
var ID_389555799_i_where=[], datos=[], struct={}
for (var reg_ID_389555799=0;reg_ID_389555799<ID_389555799_i.models.length;reg_ID_389555799++) {
  if ('idinstancia' in ID_389555799_i.models[reg_ID_389555799].attributes && ID_389555799_i.models[reg_ID_389555799].attributes['idinstancia'] == id_instancia) {
    ID_389555799_i_where.push(Widget.Collections.temp_multiple.at(reg_ID_389555799));
    struct={};
    for (var key in ID_389555799_i.models[reg_ID_389555799].attributes) {
      struct[key]=ID_389555799_i.models[reg_ID_389555799].attributes[key];
    }
    datos.push(struct);
  }
}
/* Recuperamos la variable elegidos recien creada */
var elegidos = ('elegidos' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['elegidos']:'';
var item_index = 0;
_.each(datos, function(item, item_pos, item_list) {
item_index += 1;
if ((_.isObject(elegidos) ||_.isString(elegidos)) &&  _.isEmpty(elegidos)) {
/* Revisamos si la variable elegidos esta vacia */
if (item.estado==1||item.estado=='1') {
/* Revisamos cada item que este en la variable datos */
/* Almacena el primer nombre del item */
 elegidos = item.amostrar;
}
}
 else {
if (item.estado==1||item.estado=='1') {
/* En caso de que exista mas de un valor en elegidos, se va concatenando los nombres */
 elegidos += ' + ' + item.amostrar;
}
}});
/* Se actualiza la variable con el/los nombres que estaban activos en los datos */
require(WPATH('vars'))[args.__id]['elegidos']=elegidos;
/* Mostramos valor procesado. Cambiando el texto del valor seleccionado con la variable de elegidos (que traia todos los items activos) y dejando el texto visible */
$.ID_952750652.setText(elegidos);

var ID_952750652_visible = true;

								  if (ID_952750652_visible=='si') {
									  ID_952750652_visible=true;
								  } else if (ID_952750652_visible=='no') {
									  ID_952750652_visible=false;
								  }
								  $.ID_952750652.setVisible(ID_952750652_visible);

if ((_.isObject(elegidos) ||_.isString(elegidos)) &&  _.isEmpty(elegidos)) {
/* En el caso de que no hayan items elegidos se muestra el hint y se oculta el valor */
/* Mostramos hint */
var ID_1041790471_visible = true;

								  if (ID_1041790471_visible=='si') {
									  ID_1041790471_visible=true;
								  } else if (ID_1041790471_visible=='no') {
									  ID_1041790471_visible=false;
								  }
								  $.ID_1041790471.setVisible(ID_1041790471_visible);

/* Ocultamos valor */
var ID_952750652_visible = false;

								  if (ID_952750652_visible=='si') {
									  ID_952750652_visible=true;
								  } else if (ID_952750652_visible=='no') {
									  ID_952750652_visible=false;
								  }
								  $.ID_952750652.setVisible(ID_952750652_visible);

}
 else {
/* Ocultamos hint */
var ID_1041790471_visible = false;

								  if (ID_1041790471_visible=='si') {
									  ID_1041790471_visible=true;
								  } else if (ID_1041790471_visible=='no') {
									  ID_1041790471_visible=false;
								  }
								  $.ID_1041790471.setVisible(ID_1041790471_visible);

/* Mostramos valor */
var ID_952750652_visible = true;

								  if (ID_952750652_visible=='si') {
									  ID_952750652_visible=true;
								  } else if (ID_952750652_visible=='no') {
									  ID_952750652_visible=false;
								  }
								  $.ID_952750652.setVisible(ID_952750652_visible);

}}};


$.limpiar = function(params) {
/* Limpia variables y contenidos de memoria (solo al cerrar pantalla) */
var params = ('params' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['params']:'';
Widget.Collections.temp_multiple.fetch();
var ID_1314983014_i=Widget.Collections.temp_multiple;
//filtramos a borrar
var to_delete_ID_1314983014=[];
for (var reg_ID_1314983014=0;reg_ID_1314983014<ID_1314983014_i.models.length;reg_ID_1314983014++) {
  if ('idinstancia' in ID_1314983014_i.models[reg_ID_1314983014].attributes && ID_1314983014_i.models[reg_ID_1314983014].attributes['idinstancia'] === params.__id) {
    to_delete_ID_1314983014.push(Widget.Collections.temp_multiple.at(reg_ID_1314983014));
  }
}
//borramos marcados
for (var ii_ID_1314983014 in to_delete_ID_1314983014) {
  to_delete_ID_1314983014[ii_ID_1314983014].destroy();
  Widget.Collections.temp_multiple.remove(to_delete_ID_1314983014[ii_ID_1314983014], { silent:true });
  Widget.Collections.temp_multiple.remove(to_delete_ID_1314983014[ii_ID_1314983014]);
}
ID_1314983014_i.trigger('remove');
};