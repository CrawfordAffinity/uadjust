var _bind4section={};

var args = arguments[0] || {};

function Click_ID_1440636373(e) {

e.cancelBubble=true;
var elemento=e.source;
require(WPATH('vars'))[args.__id]['flag']='cerrado';
if (OS_IOS) {
$.ID_580156269.animate({
bottom: -265,
duration: 300
});
}

if ('__args' in args) {
	args['__args'].oncancelar({});
} else {
	args.oncancelar({});
}

}
function Click_ID_230649132(e) {

e.cancelBubble=true;
var elemento=e.source;
require(WPATH('vars'))[args.__id]['flag']='cerrado';
if (OS_IOS) {
$.ID_580156269.animate({
bottom: -265,
duration: 300
});
}
var fechanueva;
fechanueva = $.ID_648278184.getValue();


if ('__args' in args) {
	args['__args'].onaceptar({valor : fechanueva});
} else {
	args.onaceptar({valor : fechanueva});
}

}



$.init = function(params) {
   for (var tobe in params) args[tobe] = params[tobe];
   if ('__id' in params) require(WPATH('vars'))[params.__id]={};
/* Iniciamos variable */
require(WPATH('vars'))[args.__id]['flag']='cerrado';
if (OS_IOS) {
/* Definimos que tipo de picker es el que tiene que cargarse */
var ID_648278184_tipo = 'fecha';

										if (_.contains('countdown,timer,count down,count_down,tiempo'.split(','),ID_648278184_tipo)) {
											ID_648278184_tipo=Titanium.UI.PICKER_TYPE_COUNT_DOWN_TIMER;
										} else if (_.contains('date,fecha'.split(','),ID_648278184_tipo)) {
											ID_648278184_tipo=Titanium.UI.PICKER_TYPE_DATE;
										} else if (_.contains('date_and_time,date and time,fecha y hora,fechahora,fecha hora,datetime'.split(','),ID_648278184_tipo)) {
											ID_648278184_tipo=Titanium.UI.PICKER_TYPE_DATE_AND_TIME;
										} else if (_.contains('tiempo,hora,time'.split(','),ID_648278184_tipo)) {
											ID_648278184_tipo=Titanium.UI.PICKER_TYPE_TIME;
										} else {
											ID_648278184_tipo=Titanium.UI.PICKER_TYPE_PLAIN;
										}
										$.ID_648278184.setType(ID_648278184_tipo);

}
if (!_.isUndefined(params.desde)) {
var ID_648278184_desde = params.desde;

										var setMinDate2 = function(_fecha) {
											var _moment = require('alloy/moment');
											if (_.isString(_fecha) && _fecha.indexOf('/')!=-1) {
												$.ID_648278184.setMinDate(_moment(_fecha,'DD/MM/YYYY').toDate());
											} else if (_.isDate(_fecha)) {
												$.ID_648278184.setMinDate(_fecha);
											}
										};setMinDate2(ID_648278184_desde);

}
if (!_.isUndefined(params.hasta)) {
var ID_648278184_hasta = params.hasta;

										var setMaxDate2 = function(_fecha) {
											var _moment = require('alloy/moment');
											if (_.isString(_fecha) && _fecha.indexOf('/')!=-1) {
												$.ID_648278184.setMaxDate(_moment(_fecha,'DD/MM/YYYY').toDate());
											} else if (_.isDate(_fecha)) {
												$.ID_648278184.setMaxDate(_fecha);
											}
										};setMaxDate2(ID_648278184_hasta);

}
if (!_.isUndefined(params.aceptar)) {
$.ID_71286252.setText(params.aceptar);

}
if (!_.isUndefined(params.cancelar)) {
$.ID_78538994.setText(params.cancelar);

}
};


$.abrir = function(params) {
var flag = ('flag' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['flag']:'';
if (OS_IOS) {
/* Revisamos si el equipo es android o iOS, en el caso de que sea un iphone se sube una vista desde la parte baja de la pantalla para seleccionar la fecha, si es android, levanta la vista nativa del celular para seleccionar la fecha */
if (flag=='cerrado') {
require(WPATH('vars'))[args.__id]['flag']='abierto';
$.ID_580156269.animate({
bottom: 0,
duration: 300
});
}
}
 else {
var ID_365061588_moment = require('alloy/moment');
var ID_365061588_datepicker = $.ID_648278184
ID_365061588_datepicker.showDatePickerDialog({ value:new Date(), callback: function(e) {
   if (e.cancel) {
      var resp = { cancel:true, format:'', formato:'' };
   } else {
      var resp = { valor:e.value, cancel:false, format:'', formato:'' };
   }
if (resp.cancel==true||resp.cancel=='true') {
/* Escondemos la seleccion de fecha */

if ('__args' in args) {
	args['__args'].oncancelar({});
} else {
	args.oncancelar({});
}
}
 else {
/* Enviamos por evento la fecha seleccionada */

if ('__args' in args) {
	args['__args'].onaceptar({valor : resp.valor});
} else {
	args.onaceptar({valor : resp.valor});
}
}} });
}};


$.cerrar = function(params) {
var flag = ('flag' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['flag']:'';
if (OS_IOS) {
/* Al haber seleccionado la fecha, revisamos si es iphone, si lo es, escondemos la vista */
if (flag=='abierto') {
require(WPATH('vars'))[args.__id]['flag']='cerrado';
$.ID_580156269.animate({
bottom: -265,
duration: 300
});
}
}
};