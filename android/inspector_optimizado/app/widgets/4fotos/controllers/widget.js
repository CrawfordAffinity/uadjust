var _bind4section={};

var args = arguments[0] || {};


$.ID_187601693.init({
caja : 55,
__id : 'ALL187601693',
onlisto : Listo_ID_1021188106,
onclick : Click_ID_1781808913
}
);

function Click_ID_1781808913(e) {
/* esta llamada es usada cuando se clickea el widget */
var evento=e;
/* Detiene la animacion de los widget y permite que se abra una sola camara a la vez */
$.ID_1901394969.detener({});
$.ID_1364996196.detener({});
$.ID_1437730300.detener({});
/* manda un numero con la posicion del widget que se esta utilizando y asi procesarlo en la pantalla que la esta usando */
if ('__args' in args) {
	args['__args'].onclick({pos : 1});
} else {
	args.onclick({pos : 1});
}
var caja1 = ('caja1' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['caja1']:'';
if (caja1==0||caja1=='0') {
require(WPATH('vars'))[args.__id]['caja1']='1';
require(WPATH('vars'))[args.__id]['caja2']='0';
require(WPATH('vars'))[args.__id]['caja3']='0';
require(WPATH('vars'))[args.__id]['caja4']='0';
$.ID_245413744.setColor('#EE7F7E');

var ID_24282456_estilo = 'normal';

						if (typeof WPATH != 'undefined') {
							var _tmp_a4w = require(WPATH('a4w'));
						} else {
							var _tmp_a4w = require('a4w');
						}
						if ((typeof ID_24282456_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (ID_24282456_estilo in _tmp_a4w.styles['classes'])) {
							try {
								ID_24282456_estilo = _tmp_a4w.styles['classes'][ID_24282456_estilo];
							} catch (st_val_err) {
							}
						}
						$.ID_24282456.applyProperties(ID_24282456_estilo);

var ID_479135773_estilo = 'normal';

						if (typeof WPATH != 'undefined') {
							var _tmp_a4w = require(WPATH('a4w'));
						} else {
							var _tmp_a4w = require('a4w');
						}
						if ((typeof ID_479135773_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (ID_479135773_estilo in _tmp_a4w.styles['classes'])) {
							try {
								ID_479135773_estilo = _tmp_a4w.styles['classes'][ID_479135773_estilo];
							} catch (st_val_err) {
							}
						}
						$.ID_479135773.applyProperties(ID_479135773_estilo);

var ID_1611904845_estilo = 'normal';

						if (typeof WPATH != 'undefined') {
							var _tmp_a4w = require(WPATH('a4w'));
						} else {
							var _tmp_a4w = require('a4w');
						}
						if ((typeof ID_1611904845_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (ID_1611904845_estilo in _tmp_a4w.styles['classes'])) {
							try {
								ID_1611904845_estilo = _tmp_a4w.styles['classes'][ID_1611904845_estilo];
							} catch (st_val_err) {
							}
						}
						$.ID_1611904845.applyProperties(ID_1611904845_estilo);

}
 else {
require(WPATH('vars'))[args.__id]['caja1']='0';
}
}
function Listo_ID_1021188106(e) {
/* esta llamada se utiliza cuando se ha procesado una imagen (escalar y comprimir) */
var evento=e;
if (!_.isUndefined(evento.comprimida)) {
/* Si la imagen comprimida desde el widget fue comprimida se hace lo siguiente */
/* Mando la posicion, imagen en miniatura y otra comprimida a la pantalla que utiliza el widget (4fotos) */
if ('__args' in args) {
	args['__args'].onlisto({pos : 1,mini : evento.mini,nueva : evento.comprimida});
} else {
	args.onlisto({pos : 1,mini : evento.mini,nueva : evento.comprimida});
}
}
 else if (!_.isUndefined(evento.escalada)) {
/* Si la imagen recibida fue escalada se hace lo siguiente */
/* Mando la posicion, imagen en miniatura y otra escalada a la pantalla que utiliza el widget (4fotos) */
if ('__args' in args) {
	args['__args'].onlisto({pos : 1,mini : evento.mini,nueva : evento.escalada});
} else {
	args.onlisto({pos : 1,mini : evento.mini,nueva : evento.escalada});
}
} else {
/* Si no se recibe la imagen comprimida o escalada, se hace lo siguiente */
/* Mando la posicion, imagen en miniatura a la pantalla que utiliza el widget (4fotos) */
if ('__args' in args) {
	args['__args'].onlisto({pos : 1,mini : evento.mini});
} else {
	args.onlisto({pos : 1,mini : evento.mini});
}
}
}

$.ID_1901394969.init({
caja : 55,
__id : 'ALL1901394969',
onlisto : Listo_ID_490762142,
onclick : Click_ID_416941178
}
);

function Click_ID_416941178(e) {
/* esta llamada es usada cuando se clickea el widget */
var evento=e;
/* Detiene la animacion de los widget y permite que se abra una sola camara a la vez */
$.ID_187601693.detener({});
$.ID_1364996196.detener({});
$.ID_1437730300.detener({});
/* manda un numero con la posicion del widget que se esta utilizando y asi procesarlo en la pantalla que la esta usando */
if ('__args' in args) {
	args['__args'].onclick({pos : 2});
} else {
	args.onclick({pos : 2});
}
var caja2 = ('caja2' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['caja2']:'';
if (caja2==0||caja2=='0') {
require(WPATH('vars'))[args.__id]['caja1']='0';
require(WPATH('vars'))[args.__id]['caja2']='1';
require(WPATH('vars'))[args.__id]['caja3']='0';
require(WPATH('vars'))[args.__id]['caja4']='0';
$.ID_24282456.setColor('#EE7F7E');

var ID_245413744_estilo = 'normal';

						if (typeof WPATH != 'undefined') {
							var _tmp_a4w = require(WPATH('a4w'));
						} else {
							var _tmp_a4w = require('a4w');
						}
						if ((typeof ID_245413744_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (ID_245413744_estilo in _tmp_a4w.styles['classes'])) {
							try {
								ID_245413744_estilo = _tmp_a4w.styles['classes'][ID_245413744_estilo];
							} catch (st_val_err) {
							}
						}
						$.ID_245413744.applyProperties(ID_245413744_estilo);

var ID_479135773_estilo = 'normal';

						if (typeof WPATH != 'undefined') {
							var _tmp_a4w = require(WPATH('a4w'));
						} else {
							var _tmp_a4w = require('a4w');
						}
						if ((typeof ID_479135773_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (ID_479135773_estilo in _tmp_a4w.styles['classes'])) {
							try {
								ID_479135773_estilo = _tmp_a4w.styles['classes'][ID_479135773_estilo];
							} catch (st_val_err) {
							}
						}
						$.ID_479135773.applyProperties(ID_479135773_estilo);

var ID_1611904845_estilo = 'normal';

						if (typeof WPATH != 'undefined') {
							var _tmp_a4w = require(WPATH('a4w'));
						} else {
							var _tmp_a4w = require('a4w');
						}
						if ((typeof ID_1611904845_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (ID_1611904845_estilo in _tmp_a4w.styles['classes'])) {
							try {
								ID_1611904845_estilo = _tmp_a4w.styles['classes'][ID_1611904845_estilo];
							} catch (st_val_err) {
							}
						}
						$.ID_1611904845.applyProperties(ID_1611904845_estilo);

}
 else {
require(WPATH('vars'))[args.__id]['caja2']='0';
}
}
function Listo_ID_490762142(e) {
/* esta llamada se utiliza cuando se ha procesado una imagen (escalar y comprimir) */
var evento=e;
if (!_.isUndefined(evento.comprimida)) {
/* Si la imagen comprimida desde el widget fue comprimida se hace lo siguiente */
/* Mando la posicion, imagen en miniatura y otra comprimida a la pantalla que utiliza el widget (4fotos) */
if ('__args' in args) {
	args['__args'].onlisto({pos : 2,mini : evento.mini,nueva : evento.comprimida});
} else {
	args.onlisto({pos : 2,mini : evento.mini,nueva : evento.comprimida});
}
}
 else if (!_.isUndefined(evento.escalada)) {
/* Si la imagen recibida fue escalada se hace lo siguiente */
/* Mando la posicion, imagen en miniatura y otra escalada a la pantalla que utiliza el widget (4fotos) */
if ('__args' in args) {
	args['__args'].onlisto({pos : 2,mini : evento.mini,nueva : evento.escalada});
} else {
	args.onlisto({pos : 2,mini : evento.mini,nueva : evento.escalada});
}
} else {
/* Si no se recibe la imagen comprimida o escalada, se hace lo siguiente */
/* Mando la posicion, imagen en miniatura a la pantalla que utiliza el widget (4fotos) */
if ('__args' in args) {
	args['__args'].onlisto({pos : 2,mini : evento.mini});
} else {
	args.onlisto({pos : 2,mini : evento.mini});
}
}
}

$.ID_1364996196.init({
caja : 55,
__id : 'ALL1364996196',
onlisto : Listo_ID_1893986936,
onclick : Click_ID_1918189988
}
);

function Click_ID_1918189988(e) {
/* esta llamada es usada cuando se clickea el widget */
var evento=e;
/* Detiene la animacion de los widget y permite que se abra una sola camara a la vez */
$.ID_187601693.detener({});
$.ID_1901394969.detener({});
$.ID_1437730300.detener({});
/* manda un numero con la posicion del widget que se esta utilizando y asi procesarlo en la pantalla que la esta usando */
if ('__args' in args) {
	args['__args'].onclick({pos : 3});
} else {
	args.onclick({pos : 3});
}
var caja3 = ('caja3' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['caja3']:'';
if (caja3==0||caja3=='0') {
require(WPATH('vars'))[args.__id]['caja1']='0';
require(WPATH('vars'))[args.__id]['caja2']='0';
require(WPATH('vars'))[args.__id]['caja3']='1';
require(WPATH('vars'))[args.__id]['caja4']='0';
$.ID_479135773.setColor('#EE7F7E');

var ID_245413744_estilo = 'normal';

						if (typeof WPATH != 'undefined') {
							var _tmp_a4w = require(WPATH('a4w'));
						} else {
							var _tmp_a4w = require('a4w');
						}
						if ((typeof ID_245413744_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (ID_245413744_estilo in _tmp_a4w.styles['classes'])) {
							try {
								ID_245413744_estilo = _tmp_a4w.styles['classes'][ID_245413744_estilo];
							} catch (st_val_err) {
							}
						}
						$.ID_245413744.applyProperties(ID_245413744_estilo);

var ID_24282456_estilo = 'normal';

						if (typeof WPATH != 'undefined') {
							var _tmp_a4w = require(WPATH('a4w'));
						} else {
							var _tmp_a4w = require('a4w');
						}
						if ((typeof ID_24282456_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (ID_24282456_estilo in _tmp_a4w.styles['classes'])) {
							try {
								ID_24282456_estilo = _tmp_a4w.styles['classes'][ID_24282456_estilo];
							} catch (st_val_err) {
							}
						}
						$.ID_24282456.applyProperties(ID_24282456_estilo);

var ID_1611904845_estilo = 'normal';

						if (typeof WPATH != 'undefined') {
							var _tmp_a4w = require(WPATH('a4w'));
						} else {
							var _tmp_a4w = require('a4w');
						}
						if ((typeof ID_1611904845_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (ID_1611904845_estilo in _tmp_a4w.styles['classes'])) {
							try {
								ID_1611904845_estilo = _tmp_a4w.styles['classes'][ID_1611904845_estilo];
							} catch (st_val_err) {
							}
						}
						$.ID_1611904845.applyProperties(ID_1611904845_estilo);

}
 else {
require(WPATH('vars'))[args.__id]['caja3']='0';
}
}
function Listo_ID_1893986936(e) {
/* esta llamada se utiliza cuando se ha procesado una imagen (escalar y comprimir) */
var evento=e;
if (!_.isUndefined(evento.comprimida)) {
/* Si la imagen comprimida desde el widget fue comprimida se hace lo siguiente */
/* Mando la posicion, imagen en miniatura y otra comprimida a la pantalla que utiliza el widget (4fotos) */
if ('__args' in args) {
	args['__args'].onlisto({pos : 3,mini : evento.mini,nueva : evento.comprimida});
} else {
	args.onlisto({pos : 3,mini : evento.mini,nueva : evento.comprimida});
}
}
 else if (!_.isUndefined(evento.escalada)) {
/* Si la imagen recibida fue escalada se hace lo siguiente */
/* Mando la posicion, imagen en miniatura y otra escalada a la pantalla que utiliza el widget (4fotos) */
if ('__args' in args) {
	args['__args'].onlisto({pos : 3,mini : evento.mini,nueva : evento.escalada});
} else {
	args.onlisto({pos : 3,mini : evento.mini,nueva : evento.escalada});
}
} else {
/* Si no se recibe la imagen comprimida o escalada, se hace lo siguiente */
/* Mando la posicion, imagen en miniatura a la pantalla que utiliza el widget (4fotos) */
if ('__args' in args) {
	args['__args'].onlisto({pos : 3,mini : evento.mini});
} else {
	args.onlisto({pos : 3,mini : evento.mini});
}
}
}

$.ID_1437730300.init({
caja : 55,
__id : 'ALL1437730300',
onlisto : Listo_ID_1977539798,
onclick : Click_ID_357755274
}
);

function Click_ID_357755274(e) {
/* esta llamada es usada cuando se clickea el widget */
var evento=e;
/* Detiene la animacion de los widget y permite que se abra una sola camara a la vez */
$.ID_187601693.detener({});
$.ID_1901394969.detener({});
$.ID_1364996196.detener({});
/* manda un numero con la posicion del widget que se esta utilizando y asi procesarlo en la pantalla que la esta usando */
if ('__args' in args) {
	args['__args'].onclick({pos : 4});
} else {
	args.onclick({pos : 4});
}
var caja4 = ('caja4' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['caja4']:'';
if (caja4==0||caja4=='0') {
require(WPATH('vars'))[args.__id]['caja1']='0';
require(WPATH('vars'))[args.__id]['caja2']='0';
require(WPATH('vars'))[args.__id]['caja3']='0';
require(WPATH('vars'))[args.__id]['caja4']='1';
$.ID_1611904845.setColor('#EE7F7E');

var ID_479135773_estilo = 'normal';

						if (typeof WPATH != 'undefined') {
							var _tmp_a4w = require(WPATH('a4w'));
						} else {
							var _tmp_a4w = require('a4w');
						}
						if ((typeof ID_479135773_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (ID_479135773_estilo in _tmp_a4w.styles['classes'])) {
							try {
								ID_479135773_estilo = _tmp_a4w.styles['classes'][ID_479135773_estilo];
							} catch (st_val_err) {
							}
						}
						$.ID_479135773.applyProperties(ID_479135773_estilo);

var ID_24282456_estilo = 'normal';

						if (typeof WPATH != 'undefined') {
							var _tmp_a4w = require(WPATH('a4w'));
						} else {
							var _tmp_a4w = require('a4w');
						}
						if ((typeof ID_24282456_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (ID_24282456_estilo in _tmp_a4w.styles['classes'])) {
							try {
								ID_24282456_estilo = _tmp_a4w.styles['classes'][ID_24282456_estilo];
							} catch (st_val_err) {
							}
						}
						$.ID_24282456.applyProperties(ID_24282456_estilo);

var ID_245413744_estilo = 'normal';

						if (typeof WPATH != 'undefined') {
							var _tmp_a4w = require(WPATH('a4w'));
						} else {
							var _tmp_a4w = require('a4w');
						}
						if ((typeof ID_245413744_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (ID_245413744_estilo in _tmp_a4w.styles['classes'])) {
							try {
								ID_245413744_estilo = _tmp_a4w.styles['classes'][ID_245413744_estilo];
							} catch (st_val_err) {
							}
						}
						$.ID_245413744.applyProperties(ID_245413744_estilo);

}
 else {
require(WPATH('vars'))[args.__id]['caja4']='0';
}
}
function Listo_ID_1977539798(e) {
/* esta llamada se utiliza cuando se ha procesado una imagen (escalar y comprimir) */
var evento=e;
if (!_.isUndefined(evento.comprimida)) {
/* Si la imagen comprimida desde el widget fue comprimida se hace lo siguiente */
/* llamar evento &quot;listo&quot; */
if ('__args' in args) {
	args['__args'].onlisto({pos : 4,mini : evento.mini,nueva : evento.comprimida});
} else {
	args.onlisto({pos : 4,mini : evento.mini,nueva : evento.comprimida});
}
}
 else if (!_.isUndefined(evento.escalada)) {
/* Si la imagen recibida fue escalada se hace lo siguiente */
/* Mando la posicion, imagen en miniatura y otra escalada a la pantalla que utiliza el widget (4fotos) */
if ('__args' in args) {
	args['__args'].onlisto({pos : 4,mini : evento.mini,nueva : evento.escalada});
} else {
	args.onlisto({pos : 4,mini : evento.mini,nueva : evento.escalada});
}
} else {
/* Si no se recibe la imagen comprimida o escalada, se hace lo siguiente */
/* Mando la posicion, imagen en miniatura a la pantalla que utiliza el widget (4fotos) */
if ('__args' in args) {
	args['__args'].onlisto({pos : 4,mini : evento.mini});
} else {
	args.onlisto({pos : 4,mini : evento.mini});
}
}
}
/* Acciones que se ejecutan al iniciar el widget */
$.init = function(params) {
   for (var tobe in params) args[tobe] = params[tobe];
   if ('__id' in params) require(WPATH('vars'))[params.__id]={};
if (!_.isUndefined(params.caja)) {
/* Si en la pantalla utlizada se definio la altura de la caja, esta condicion recibe el parametro para enviarlo al widget que lo ocupa */
/* Recibe el valor del parametro caja y lo envia al widget */
$.ID_187601693.init({caja : params.caja});
$.ID_1901394969.init({caja : params.caja});
$.ID_1364996196.init({caja : params.caja});
$.ID_1437730300.init({caja : params.caja});
}
if (!_.isUndefined(params.ancho)) {
/* Si se define cual es el ancho de la vista que contiene los 4widget, lo redimensiona al iniciarse el widget */
/* Define el ancho de la vista que contiene los 4 widget */
var ID_265479629_ancho = params.ancho;

								  if (ID_265479629_ancho=='*') {
									  ID_265479629_ancho=Ti.UI.FILL;
								  } else if (ID_265479629_ancho=='-') {
									  ID_265479629_ancho=Ti.UI.SIZE;
								  } else if (!isNaN(ID_265479629_ancho)) {
									  ID_265479629_ancho=ID_265479629_ancho+'dp';
								  }
								  $.ID_265479629.setWidth(ID_265479629_ancho);

}
if (!_.isUndefined(params.alto)) {
/* Si se define cual es el alto de la vista que contiene los 4widget, lo redimensiona al iniciarse el widget */
/* Define el alto de la vista que contiene los 4 widget */
var ID_265479629_alto = params.alto;

								  if (ID_265479629_alto=='*') {
									  ID_265479629_alto=Ti.UI.FILL;
								  } else if (ID_265479629_alto=='-') {
									  ID_265479629_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_265479629_alto)) {
									  ID_265479629_alto=ID_265479629_alto+'dp';
								  }
								  $.ID_265479629.setHeight(ID_265479629_alto);

}
require(WPATH('vars'))[args.__id]['caja1']='0';
require(WPATH('vars'))[args.__id]['caja2']='0';
require(WPATH('vars'))[args.__id]['caja3']='0';
};

/* Recibe la imagen que ser&#225; procesada seg&#250;n la posicion definida en la llamada click del widget &quot;fotochica&quot; */
$.procesar = function(params) {
if (!_.isUndefined(params.imagen1)) {
/* En el caso de que la imagen1 exista, se ejecutar&#225; lo siguiente */
if (!_.isUndefined(params.camara)) {
if (!_.isUndefined(params.nueva)) {
/* si existe el parametro &quot;nueva&quot;, definira la resolucion de la imagen a escalar y/o comprimir */
/* Se envia la imagen, la resolucion y calidad de la imagen a procesar al widget fotochica */
$.ID_187601693.procesar({imagen : params.imagen1,nueva : params.nueva,calidad : 91,camara : params.camara});
}
 else {
/* Si no existe el parametro &quot;nueva&quot; si existe el parametro &quot;nueva&quot;, solo definira la calidad de la imagen a obtener de regreso */
/* Se envia la imagen, y calidad de la imagen a procesar al widget fotochica */
$.ID_187601693.procesar({imagen : params.imagen1,calidad : 91,camara : params.camara});
}}
 else {
if (!_.isUndefined(params.nueva)) {
/* si existe el parametro &quot;nueva&quot;, definira la resolucion de la imagen a escalar y/o comprimir */
/* Se envia la imagen, la resolucion y calidad de la imagen a procesar al widget fotochica */
$.ID_187601693.procesar({imagen : params.imagen1,nueva : params.nueva,calidad : 91});
}
 else {
/* Si no existe el parametro &quot;nueva&quot; si existe el parametro &quot;nueva&quot;, solo definira la calidad de la imagen a obtener de regreso */
/* Se envia la imagen, y calidad de la imagen a procesar al widget fotochica */
$.ID_187601693.procesar({imagen : params.imagen1,calidad : 91});
}}/* Se limpia la memoria y evitar fugas de memoria */
 params.imagen1=null;
}
if (!_.isUndefined(params.imagen2)) {
/* En el caso de que la imagen2 exista, se ejecutar&#225; lo siguiente */
if (!_.isUndefined(params.nueva)) {
/* si existe el parametro &quot;nueva&quot;, definira la resolucion de la imagen a escalar y/o comprimir */
/* Se envia la imagen, la resolucion y calidad de la imagen a procesar al widget fotochica */
$.ID_1901394969.procesar({imagen : params.imagen2,nueva : params.nueva,calidad : 91,camara : params.camara});
}
 else {
/* Si no existe el parametro &quot;nueva&quot; si existe el parametro &quot;nueva&quot;, solo definira la calidad de la imagen a obtener de regreso */
/* Se envia la imagen, y calidad de la imagen a procesar al widget fotochica */
$.ID_1901394969.procesar({imagen : params.imagen2,calidad : 91,camara : params.camara});
}/* Se limpia la memoria y evitar fugas de memoria */
 params.imagen2=null;
}
if (!_.isUndefined(params.imagen3)) {
/* En el caso de que la imagen3 exista, se ejecutar&#225; lo siguiente */
if (!_.isUndefined(params.nueva)) {
/* si existe el parametro &quot;nueva&quot;, definira la resolucion de la imagen a escalar y/o comprimir */
/* Se envia la imagen, la resolucion y calidad de la imagen a procesar al widget fotochica */
$.ID_1364996196.procesar({imagen : params.imagen3,nueva : params.nueva,calidad : 91,camara : params.camara});
}
 else {
/* Si no existe el parametro &quot;nueva&quot; si existe el parametro &quot;nueva&quot;, solo definira la calidad de la imagen a obtener de regreso */
/* Se envia la imagen, y calidad de la imagen a procesar al widget fotochica */
$.ID_1364996196.procesar({imagen : params.imagen3,calidad : 91,camara : params.camara});
}/* Se limpia la memoria y evitar fugas de memoria */
 params.imagen3=null;
}
if (!_.isUndefined(params.imagen4)) {
/* En el caso de que la imagen4 exista, se ejecutar&#225; lo siguiente */
if (!_.isUndefined(params.nueva)) {
/* si existe el parametro &quot;nueva&quot;, definira la resolucion de la imagen a escalar y/o comprimir */
/* Se envia la imagen, la resolucion y calidad de la imagen a procesar al widget fotochica */
$.ID_1437730300.procesar({imagen : params.imagen4,nueva : params.nueva,calidad : 91,camara : params.camara});
}
 else {
/* Si no existe el parametro &quot;nueva&quot; si existe el parametro &quot;nueva&quot;, solo definira la calidad de la imagen a obtener de regreso */
/* Se envia la imagen, y calidad de la imagen a procesar al widget fotochica */
$.ID_1437730300.procesar({imagen : params.imagen4,calidad : 91,camara : params.camara});
}/* Se limpia la memoria y evitar fugas de memoria */
 params.imagen4=null;
}
};