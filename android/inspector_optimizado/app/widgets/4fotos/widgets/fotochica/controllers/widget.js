var _bind4section={};

var args = arguments[0] || {};

function Load_ID_891945745(e) {
/* Evento que se ejecuta una vez cargada la vista */
e.cancelBubble=true;
var elemento=e.source;
var evento=e;
/* Hace que la animacion se ejecute */
 elemento.start();

}
function Change_ID_573947256(e) {

e.cancelBubble=true;
var elemento=e.source;
var evento=e;
/* Recupera la cantidad de frames de la imagen */
var conteo_end = ('conteo_end' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['conteo_end']:'';
if (evento.index==conteo_end) {
/* Hara un conteo que cuando llegue a la cantidad definida en conteo_end se pausara la animacion */
/* Deja el contador en vacio */
require(WPATH('vars'))[args.__id]['conteo_end']='';
/* Detiene la animacion */
 elemento.pause();
}

}
function Click_ID_1011662708(e) {

e.cancelBubble=true;
var elemento=e.source;
var estado = ('estado' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['estado']:'';
if (estado==0||estado=='0') {
/* Cuando el estado es 0, es cuando el widget es recien ocupado */
/* Se oculta vista que contiene imagen estatica */
var ID_1352627270_visible = false;

										  if (ID_1352627270_visible=='si') {
											  ID_1352627270_visible=true;
										  } else if (ID_1352627270_visible=='no') {
											  ID_1352627270_visible=false;
										  }
										  $.ID_1352627270.setVisible(ID_1352627270_visible);

/* Se pone visible la vista que tiene la animacion */
var ID_1822037338_visible = true;

										  if (ID_1822037338_visible=='si') {
											  ID_1822037338_visible=true;
										  } else if (ID_1822037338_visible=='no') {
											  ID_1822037338_visible=false;
										  }
										  $.ID_1822037338.setVisible(ID_1822037338_visible);

/* Se inicia la animacion */
$.ID_1860721359.start();
/* Se destaca el texto para indicar que es la vista activa */
var ID_1109012109_estilo = 'destacado';

						if (typeof WPATH != 'undefined') {
							var _tmp_a4w = require(WPATH('a4w'));
						} else {
							var _tmp_a4w = require('a4w');
						}
						if ((typeof ID_1109012109_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (ID_1109012109_estilo in _tmp_a4w.styles['classes'])) {
							try {
								ID_1109012109_estilo = _tmp_a4w.styles['classes'][ID_1109012109_estilo];
							} catch (st_val_err) {
							}
						}
						$.ID_1109012109.applyProperties(ID_1109012109_estilo);

/* Se guarda el estado en un valor que no se utiliza, y evitar errores */
require(WPATH('vars'))[args.__id]['estado']='1';

if ('__args' in args) {
	args['__args'].onclick({});
} else {
	args.onclick({});
}
}
 else if (estado==2) {
/* Se activa cuando ya existe una miniatura procesada y cargada */
/* Se destaca el texto para indicar que es la vista activa */
var ID_1109012109_estilo = 'destacado';

						if (typeof WPATH != 'undefined') {
							var _tmp_a4w = require(WPATH('a4w'));
						} else {
							var _tmp_a4w = require('a4w');
						}
						if ((typeof ID_1109012109_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (ID_1109012109_estilo in _tmp_a4w.styles['classes'])) {
							try {
								ID_1109012109_estilo = _tmp_a4w.styles['classes'][ID_1109012109_estilo];
							} catch (st_val_err) {
							}
						}
						$.ID_1109012109.applyProperties(ID_1109012109_estilo);


if ('__args' in args) {
	args['__args'].onclick({});
} else {
	args.onclick({});
}
}
}

$.init = function(params) {
   for (var tobe in params) args[tobe] = params[tobe];
   if ('__id' in params) require(WPATH('vars'))[params.__id]={};
/* Se inicializa el widget con estado 0, ya que no hay imagenes procesadas al comienzo */
require(WPATH('vars'))[args.__id]['estado']='0';
/* Sirve para definir en el caso de que la caja no se le haya definido un alto/ancho, pondra 55 como por defecto */
require(WPATH('vars'))[args.__id]['caja']=55;
if (!_.isUndefined(params.caja)) {
/* Modifica el alto y ancho de la vista con los valores de caja */
var ID_1498362955_ancho = params.caja;

								  if (ID_1498362955_ancho=='*') {
									  ID_1498362955_ancho=Ti.UI.FILL;
								  } else if (ID_1498362955_ancho=='-') {
									  ID_1498362955_ancho=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1498362955_ancho)) {
									  ID_1498362955_ancho=ID_1498362955_ancho+'dp';
								  }
								  $.ID_1498362955.setWidth(ID_1498362955_ancho);

var ID_1498362955_alto = params.caja;

								  if (ID_1498362955_alto=='*') {
									  ID_1498362955_alto=Ti.UI.FILL;
								  } else if (ID_1498362955_alto=='-') {
									  ID_1498362955_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1498362955_alto)) {
									  ID_1498362955_alto=ID_1498362955_alto+'dp';
								  }
								  $.ID_1498362955.setHeight(ID_1498362955_alto);

require(WPATH('vars'))[args.__id]['caja']=params.caja;
}
if (!_.isUndefined(params.label)) {
/* En el caso de que se haya definido un texto de titulo para el widget, esto lo modifica */
/* Cambia el texto del titulo y lo deja visible */
$.ID_1109012109.setText(params.label);

var ID_1109012109_visible = true;

								  if (ID_1109012109_visible=='si') {
									  ID_1109012109_visible=true;
								  } else if (ID_1109012109_visible=='no') {
									  ID_1109012109_visible=false;
								  }
								  $.ID_1109012109.setVisible(ID_1109012109_visible);

}
if (!_.isUndefined(params.opcional)) {
/* En el caso de que exista un titulo opcional, lo carga y deja visible */
/* Cambia el texto del titulo opcional y lo deja visible */
$.ID_965641207.setText(params.opcional);

var ID_965641207_visible = true;

								  if (ID_965641207_visible=='si') {
									  ID_965641207_visible=true;
								  } else if (ID_965641207_visible=='no') {
									  ID_965641207_visible=false;
								  }
								  $.ID_965641207.setVisible(ID_965641207_visible);

}
if (!_.isUndefined(params.id)) {
require(WPATH('vars'))[args.__id]['id']=params.id;
}
};

/* recibe una imagen cuadrada, y la escala (a tama&#241;o 'nueva' y 'mini' (tama&#241;o caja)) y comprime, y envia la respuesta */
$.procesar = function(params) {
if (!_.isUndefined(params.imagen)) {
/* Recibe una imagen cuadrada, y la escala (a tama&#241;o 'nueva' y 'mini' (tama&#241;o caja)) y comprime, y envia a respuesta */
/* Ocultamos wiggle */
var ID_1822037338_visible = false;

										  if (ID_1822037338_visible=='si') {
											  ID_1822037338_visible=true;
										  } else if (ID_1822037338_visible=='no') {
											  ID_1822037338_visible=false;
										  }
										  $.ID_1822037338.setVisible(ID_1822037338_visible);

/* Mostramos animacion */
var ID_602677766_visible = true;

										  if (ID_602677766_visible=='si') {
											  ID_602677766_visible=true;
										  } else if (ID_602677766_visible=='no') {
											  ID_602677766_visible=false;
										  }
										  $.ID_602677766.setVisible(ID_602677766_visible);

/* Guarda la variable con 30 para contar hasta 30imagenes de la animacion */
require(WPATH('vars'))[args.__id]['conteo_end']=30;
/* Se inicia la animacion */
$.ID_965642494.start();
/* Hacemos thumbnail. Recupera el valor de la caja que se paso anteriormente */
var caja = ('caja' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['caja']:'';
/* Recibe la imagen, los valores de la caja para definir alto/ancho. Retorna la imagen en una variable llamada mini */
if (OS_ANDROID) {
   var ID_1808522135_imagefactory = require('ti.imagefactory');
   var mini = ID_1808522135_imagefactory.imageAsResized(params.imagen, { width: caja, height: caja, quality: 0.7 });
} else {
   var mini = params.imagen.imageAsResized(caja, caja);
}
if (!_.isUndefined(params.nueva)) {
/* Deja vacio el contador de la animacion */
require(WPATH('vars'))[args.__id]['conteo_end']='';
/* Continuamos reproduccion de conteo hasta donde acabe */
$.ID_965642494.resume();
if (!_.isUndefined(params.calidad)) {
/* Si el evento llamado tiene parametro de calidad se hace lo siguiente */
/* Recibe la imagen y la calidad. Retorna la imagen comprimida en una variable llamada comprimida */
if (OS_ANDROID) {
  var ID_744309819_imagefactory = require('ti.imagefactory');
  var comprimida = ID_744309819_imagefactory.compress(params.imagen, params.calidad/100);
} else if (OS_IOS) {
  var ID_744309819_imagefactory = require('ti.imagefactory');
  var comprimida = ID_744309819_imagefactory.compress(params.imagen, params.calidad/100);
}
/* Escala la imagen, recibe la imagen y alto/ancho. Retorna la imagen en una variable llamada fotofinal */
if (OS_ANDROID) {
   var ID_431277259_imagefactory = require('ti.imagefactory');
   var fotofinal = ID_431277259_imagefactory.imageAsResized(comprimida, { width: params.nueva, height: params.nueva, quality: 0.7 });
} else {
   var fotofinal = comprimida.imageAsResized(params.nueva, params.nueva);
}
/* Actualiza el estado del widget para que haga otra accion dependiendo del click */
require(WPATH('vars'))[args.__id]['estado']=2;
/* Oculta vista de animacion conteo */
var ID_602677766_visible = false;

										  if (ID_602677766_visible=='si') {
											  ID_602677766_visible=true;
										  } else if (ID_602677766_visible=='no') {
											  ID_602677766_visible=false;
										  }
										  $.ID_602677766.setVisible(ID_602677766_visible);

/* Oculta vista de animacion wiggle */
var ID_1822037338_visible = false;

										  if (ID_1822037338_visible=='si') {
											  ID_1822037338_visible=true;
										  } else if (ID_1822037338_visible=='no') {
											  ID_1822037338_visible=false;
										  }
										  $.ID_1822037338.setVisible(ID_1822037338_visible);

/* Mostramos thumbnail */
var ID_1206598763_visible = true;

										  if (ID_1206598763_visible=='si') {
											  ID_1206598763_visible=true;
										  } else if (ID_1206598763_visible=='no') {
											  ID_1206598763_visible=false;
										  }
										  $.ID_1206598763.setVisible(ID_1206598763_visible);

/* Modifica la imagen para mostrarla en pantalla */
var ID_687034887_imagen = mini;

													if (typeof ID_687034887_imagen == 'string' && 'styles' in require(WPATH('a4w')) && ID_687034887_imagen in require(WPATH('a4w')).styles['images']) {
														ID_687034887_imagen = WPATH(require(WPATH('a4w')).styles['images'][ID_687034887_imagen]);
													}
													$.ID_687034887.setImage(ID_687034887_imagen);

if ((Ti.Platform.manufacturer)=='samsung') {
/* Revisamos si el equipo es samsung, ya que es la unica marca que saca las miniaturas mal rotadas */
if (params.camara=='frontal') {
var ID_1206598763_rotar = 270;

										var setRotacion = function(angulo) {
											$.ID_1206598763.setTransform(Ti.UI.create2DMatrix().rotate(angulo));
										};setRotacion(ID_1206598763_rotar);

}
 else if (params.camara=='trasera') {
var ID_1206598763_rotar = 90;

										var setRotacion = function(angulo) {
											$.ID_1206598763.setTransform(Ti.UI.create2DMatrix().rotate(angulo));
										};setRotacion(ID_1206598763_rotar);

} else {
}}
/* Llama al evento listo (donde se ocupa el widget) mandando los valores de la foto escalada, miniatura y la foto comprimida */
if ('__args' in args) {
	args['__args'].onlisto({escalada : fotofinal,mini : mini,comprimida : comprimida});
} else {
	args.onlisto({escalada : fotofinal,mini : mini,comprimida : comprimida});
}
}
 else {
/* Actualiza el estado del widget para que haga otra accion dependiendo del click */
require(WPATH('vars'))[args.__id]['estado']=2;
/* Escala la imagen, recibe la imagen y alto/ancho. Retorna la imagen en una variable llamada fotofinal */
if (OS_ANDROID) {
   var ID_168489391_imagefactory = require('ti.imagefactory');
   var fotofinal = ID_168489391_imagefactory.imageAsResized(params.imagen, { width: params.nueva, height: params.nueva, quality: 0.7 });
} else {
   var fotofinal = params.imagen.imageAsResized(params.nueva, params.nueva);
}
/* Oculta vista de animacion conteo */
var ID_602677766_visible = false;

										  if (ID_602677766_visible=='si') {
											  ID_602677766_visible=true;
										  } else if (ID_602677766_visible=='no') {
											  ID_602677766_visible=false;
										  }
										  $.ID_602677766.setVisible(ID_602677766_visible);

/* Oculta vista de animacion wiggle */
var ID_1822037338_visible = false;

										  if (ID_1822037338_visible=='si') {
											  ID_1822037338_visible=true;
										  } else if (ID_1822037338_visible=='no') {
											  ID_1822037338_visible=false;
										  }
										  $.ID_1822037338.setVisible(ID_1822037338_visible);

/* Mostramos thumbnail */
var ID_1206598763_visible = true;

										  if (ID_1206598763_visible=='si') {
											  ID_1206598763_visible=true;
										  } else if (ID_1206598763_visible=='no') {
											  ID_1206598763_visible=false;
										  }
										  $.ID_1206598763.setVisible(ID_1206598763_visible);

/* Modifica la imagen para mostrarla en pantalla */
var ID_687034887_imagen = mini;

													if (typeof ID_687034887_imagen == 'string' && 'styles' in require(WPATH('a4w')) && ID_687034887_imagen in require(WPATH('a4w')).styles['images']) {
														ID_687034887_imagen = WPATH(require(WPATH('a4w')).styles['images'][ID_687034887_imagen]);
													}
													$.ID_687034887.setImage(ID_687034887_imagen);

if ((Ti.Platform.manufacturer)=='samsung') {
/* Revisamos si el equipo es samsung, ya que es la unica marca que saca las miniaturas mal rotadas */
var ID_1206598763_rotar = 90;

										var setRotacion = function(angulo) {
											$.ID_1206598763.setTransform(Ti.UI.create2DMatrix().rotate(angulo));
										};setRotacion(ID_1206598763_rotar);

if (Ti.App.deployType != 'production') console.log('entro a la condicion',{});
}
/* Llama al evento listo (donde se ocupa el widget) mandando los valores de la foto escalada, miniatura */
if ('__args' in args) {
	args['__args'].onlisto({escalada : fotofinal,mini : mini});
} else {
	args.onlisto({escalada : fotofinal,mini : mini});
}
}}
 else {
/* Actualiza el estado del widget para que haga otra accion dependiendo del click */
require(WPATH('vars'))[args.__id]['estado']=2;
/* Oculta vista de animacion conteo */
var ID_602677766_visible = false;

										  if (ID_602677766_visible=='si') {
											  ID_602677766_visible=true;
										  } else if (ID_602677766_visible=='no') {
											  ID_602677766_visible=false;
										  }
										  $.ID_602677766.setVisible(ID_602677766_visible);

/* Oculta vista de animacion wiggle */
var ID_1822037338_visible = false;

										  if (ID_1822037338_visible=='si') {
											  ID_1822037338_visible=true;
										  } else if (ID_1822037338_visible=='no') {
											  ID_1822037338_visible=false;
										  }
										  $.ID_1822037338.setVisible(ID_1822037338_visible);

/* Mostramos thumbnail */
var ID_1206598763_visible = true;

										  if (ID_1206598763_visible=='si') {
											  ID_1206598763_visible=true;
										  } else if (ID_1206598763_visible=='no') {
											  ID_1206598763_visible=false;
										  }
										  $.ID_1206598763.setVisible(ID_1206598763_visible);

/* Modifica la imagen para mostrarla en pantalla */
var ID_687034887_imagen = mini;

													if (typeof ID_687034887_imagen == 'string' && 'styles' in require(WPATH('a4w')) && ID_687034887_imagen in require(WPATH('a4w')).styles['images']) {
														ID_687034887_imagen = WPATH(require(WPATH('a4w')).styles['images'][ID_687034887_imagen]);
													}
													$.ID_687034887.setImage(ID_687034887_imagen);

if ((Ti.Platform.manufacturer)=='samsung') {
/* Revisamos si el equipo es samsung, ya que es la unica marca que saca las miniaturas mal rotadas */
if (params.camara=='frontal') {
var ID_1206598763_rotar = 270;

										var setRotacion = function(angulo) {
											$.ID_1206598763.setTransform(Ti.UI.create2DMatrix().rotate(angulo));
										};setRotacion(ID_1206598763_rotar);

}
 else if (params.camara=='trasera') {
var ID_1206598763_rotar = 90;

										var setRotacion = function(angulo) {
											$.ID_1206598763.setTransform(Ti.UI.create2DMatrix().rotate(angulo));
										};setRotacion(ID_1206598763_rotar);

} else {
}}
/* Llama al evento listo (donde se ocupa el widget) mandando los valores de la foto escalada, miniatura */
if ('__args' in args) {
	args['__args'].onlisto({mini : mini});
} else {
	args.onlisto({mini : mini});
}
}/* Se limpian las variables para evitar fugas de memoria */
 
mini = null;
fotofinal = null;
comprimida = null;
params.imagen = null; ;
}
};

/* Detiene todas las animaciones */
$.detener = function(params) {
/* Detiene wiggle y muestra foto estatica, solo si no tiene imagen */
var estado = ('estado' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['estado']:'';
if (estado==1||estado=='1') {
/* Ocultamos wiggle */
var ID_1822037338_visible = false;

										  if (ID_1822037338_visible=='si') {
											  ID_1822037338_visible=true;
										  } else if (ID_1822037338_visible=='no') {
											  ID_1822037338_visible=false;
										  }
										  $.ID_1822037338.setVisible(ID_1822037338_visible);

/* Ocultamos conteo */
var ID_602677766_visible = false;

										  if (ID_602677766_visible=='si') {
											  ID_602677766_visible=true;
										  } else if (ID_602677766_visible=='no') {
											  ID_602677766_visible=false;
										  }
										  $.ID_602677766.setVisible(ID_602677766_visible);

/* Ocultamos minifoto (por si acaso) */
var ID_1206598763_visible = false;

										  if (ID_1206598763_visible=='si') {
											  ID_1206598763_visible=true;
										  } else if (ID_1206598763_visible=='no') {
											  ID_1206598763_visible=false;
										  }
										  $.ID_1206598763.setVisible(ID_1206598763_visible);

/* Mostramos imagen estatica */
var ID_1352627270_visible = true;

										  if (ID_1352627270_visible=='si') {
											  ID_1352627270_visible=true;
										  } else if (ID_1352627270_visible=='no') {
											  ID_1352627270_visible=false;
										  }
										  $.ID_1352627270.setVisible(ID_1352627270_visible);

/* Dejamos estilo label normal */
var ID_1109012109_estilo = 'normal';

						if (typeof WPATH != 'undefined') {
							var _tmp_a4w = require(WPATH('a4w'));
						} else {
							var _tmp_a4w = require('a4w');
						}
						if ((typeof ID_1109012109_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (ID_1109012109_estilo in _tmp_a4w.styles['classes'])) {
							try {
								ID_1109012109_estilo = _tmp_a4w.styles['classes'][ID_1109012109_estilo];
							} catch (st_val_err) {
							}
						}
						$.ID_1109012109.applyProperties(ID_1109012109_estilo);

/* Deja al widget con estado 0, ya que no hay imagenes */
require(WPATH('vars'))[args.__id]['estado']='0';
}
 else if (estado==2) {
/* Dejamos estilo label normal */
var ID_1109012109_estilo = 'normal';

						if (typeof WPATH != 'undefined') {
							var _tmp_a4w = require(WPATH('a4w'));
						} else {
							var _tmp_a4w = require('a4w');
						}
						if ((typeof ID_1109012109_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (ID_1109012109_estilo in _tmp_a4w.styles['classes'])) {
							try {
								ID_1109012109_estilo = _tmp_a4w.styles['classes'][ID_1109012109_estilo];
							} catch (st_val_err) {
							}
						}
						$.ID_1109012109.applyProperties(ID_1109012109_estilo);

}};