var _bind4section={};

var args = arguments[0] || {};

function Click_ID_984251325(e) {

e.cancelBubble=true;
var elemento=e.source;
/* Recuperamos variable data para despues ver si contiene datos */
var data = ('data' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['data']:'';
/* Recuperamos variable para saber si se cargara el modal */
var activo = ('activo' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['activo']:'';
if (activo==true||activo=='true') {
/* comprobamos si el widget esta activo para editar los datos */

if ('__args' in args) {
	args['__args'].onabrir({});
} else {
	args.onabrir({});
}
if (_.isArray(data)) {
/* Verificamos que la variable data sea un array y saber que tiene datos para desplegar en el modal */
Widget.createController("tipo_modal",{'__args' : (typeof args!=='undefined')?args:__args}).getView().open();
}
 else {
/* Caso contrario, lanzamos un mensaje de error */
var ID_1530241800_opts=['Aceptar'];
var ID_1530241800 = Ti.UI.createAlertDialog({
   title: 'Error',
   message: 'Faltan los datos para modal',
   buttonNames: ID_1530241800_opts
});
ID_1530241800.addEventListener('click', function(e) {
   var nulo=ID_1530241800_opts[e.index];
nulo = null;

   e.source.removeEventListener("click", arguments.callee);
});
ID_1530241800.show();
}}

}

$.init = function(params) {
   for (var tobe in params) args[tobe] = params[tobe];
   if ('__id' in params) require(WPATH('vars'))[params.__id]={};
/* Creamos variables de inicializacion por default */
require(WPATH('vars'))[args.__id]['data']='';
require(WPATH('vars'))[args.__id]['campo']='no definido';
require(WPATH('vars'))[args.__id]['titulo']='no definido';
require(WPATH('vars'))[args.__id]['guardar']='false';
require(WPATH('vars'))[args.__id]['color']='';
require(WPATH('vars'))[args.__id]['activo']='true';
require(WPATH('vars'))[args.__id]['cargando']='cargando ..';
require(WPATH('vars'))[args.__id]['params']=params;
if (!_.isUndefined(params.data)) {
/* Revisamos los parametros al iniciar el widget, y cargamos datos o modificamos vistas segun sea */
require(WPATH('vars'))[args.__id]['data']=params.data;
}
if (!_.isUndefined(params.campo)) {
require(WPATH('vars'))[args.__id]['campo']=params.campo;
$.ID_813529883.setText(params.campo);

}
if (!_.isUndefined(params.titulo)) {
require(WPATH('vars'))[args.__id]['titulo']=params.titulo;
}
if (!_.isUndefined(params.seleccione)) {
$.ID_460439112.setText(params.seleccione);

require(WPATH('vars'))[args.__id]['seleccione']=params.seleccione;
}
if (!_.isUndefined(params.left)) {
$.ID_813529883.setLeft(params.left);

$.ID_809844812.setLeft(params.left);

}
if (!_.isUndefined(params.right)) {
$.ID_809844812.setRight(params.right);

}
if (!_.isUndefined(params.top)) {
$.ID_1309803634.setTop(params.top);

}
if (!_.isUndefined(params.activo)) {
require(WPATH('vars'))[args.__id]['activo']=params.activo;
}
if (!_.isUndefined(params.cargando)) {
/* Verificamos si el parametro cargando existe, siendo asi, guardamos en una variable el texto, y modifica el subtitulo para que el usuario vea que se estan cargando los datos */
$.ID_460439112.setText(params.cargando);

require(WPATH('vars'))[args.__id]['cargando']=params.cargando;
}
/* Se genera una espera de 0.2 segundos para que despues que se haya cargado la informacion en el widget, llama el evento afterinit */
var ID_346372225_func = function() {

if ('__args' in args) {
	args['__args'].onafterinit({});
} else {
	args.onafterinit({});
}
};
var ID_346372225 = setTimeout(ID_346372225_func, 1000*0.2);
};


$.data = function(params) {
var cargando = ('cargando' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['cargando']:'';
var seleccione = ('seleccione' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['seleccione']:'';
if ((_.isObject(cargando) || (_.isString(cargando)) &&  !_.isEmpty(cargando)) || _.isNumber(cargando) || _.isBoolean(cargando)) {
/* Verificamos que la variable cargando contenga datos */
if ((_.isObject(seleccione) || (_.isString(seleccione)) &&  !_.isEmpty(seleccione)) || _.isNumber(seleccione) || _.isBoolean(seleccione)) {
/* Verificamos que la variable seleccione contenga datos, si contiene datos, se modifica el subtitulo de la vista del widget que se ve en la pantalla que lo utiliza */
$.ID_460439112.setText(seleccione);

}
}
if (!_.isUndefined(params.data)) {
/* Verificamos que el parametro data exista, siendo asi, guardamos en una variable data para poder ocuparla en la pantalla modal. Si no, llamamos el evento afterinit */
require(WPATH('vars'))[args.__id]['data']=params.data;
}
 else {

if ('__args' in args) {
	args['__args'].onafterinit({});
} else {
	args.onafterinit({});
}
}};


$.labels = function(params) {
if (!_.isUndefined(params.campo)) {
/* Revisamos que parametros son recibidos desde el evento labels y editamos los textos */
$.ID_813529883.setText(params.titulo);

}
if (!_.isUndefined(params.titulo)) {
require(WPATH('vars'))[args.__id]['titulo']=params.titulo;
}
if (!_.isUndefined(params.seleccione)) {
$.ID_460439112.setText(params.seleccione);

var ID_460439112_estilo = 'estilo5_1';

						if (typeof WPATH != 'undefined') {
							var _tmp_a4w = require(WPATH('a4w'));
						} else {
							var _tmp_a4w = require('a4w');
						}
						if ((typeof ID_460439112_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (ID_460439112_estilo in _tmp_a4w.styles['classes'])) {
							try {
								ID_460439112_estilo = _tmp_a4w.styles['classes'][ID_460439112_estilo];
							} catch (st_val_err) {
							}
						}
						$.ID_460439112.applyProperties(ID_460439112_estilo);

}
if (!_.isUndefined(params.valor)) {
$.ID_460439112.setText(params.valor);

var ID_460439112_estilo = 'estilo12';

						if (typeof WPATH != 'undefined') {
							var _tmp_a4w = require(WPATH('a4w'));
						} else {
							var _tmp_a4w = require('a4w');
						}
						if ((typeof ID_460439112_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (ID_460439112_estilo in _tmp_a4w.styles['classes'])) {
							try {
								ID_460439112_estilo = _tmp_a4w.styles['classes'][ID_460439112_estilo];
							} catch (st_val_err) {
							}
						}
						$.ID_460439112.applyProperties(ID_460439112_estilo);

}
};


$.desactivar = function(params) {
/* Si el widget recibe el evento desactivar, guarda una variable activo para que la pantalla modal no sea abierta */
require(WPATH('vars'))[args.__id]['activo']='false';
/* Cambia el color para que pareciera que esta desactivado */
var ID_460439112_estilo = 'estilo5_1';

						if (typeof WPATH != 'undefined') {
							var _tmp_a4w = require(WPATH('a4w'));
						} else {
							var _tmp_a4w = require('a4w');
						}
						if ((typeof ID_460439112_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (ID_460439112_estilo in _tmp_a4w.styles['classes'])) {
							try {
								ID_460439112_estilo = _tmp_a4w.styles['classes'][ID_460439112_estilo];
							} catch (st_val_err) {
							}
						}
						$.ID_460439112.applyProperties(ID_460439112_estilo);

};


$.activar = function(params) {
/* Si el widget recibe el evento activar, guarda una variable activo para que la pantalla modal sea abierta */
require(WPATH('vars'))[args.__id]['activo']='true';
/* Recuperamos la variable seleccione */
var seleccione = ('seleccione' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['seleccione']:'';
var textosel;
textosel = $.ID_460439112.getText();

if (seleccione!=textosel) {
/* Solo activamos estilo on si ya se ha seleccionado un valor previamente, sino dejamos opaco para hint */
var ID_460439112_estilo = 'estilo12';

						if (typeof WPATH != 'undefined') {
							var _tmp_a4w = require(WPATH('a4w'));
						} else {
							var _tmp_a4w = require('a4w');
						}
						if ((typeof ID_460439112_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (ID_460439112_estilo in _tmp_a4w.styles['classes'])) {
							try {
								ID_460439112_estilo = _tmp_a4w.styles['classes'][ID_460439112_estilo];
							} catch (st_val_err) {
							}
						}
						$.ID_460439112.applyProperties(ID_460439112_estilo);

}
};


$.limpiar = function(params) {
/* Limpiamos memoria para evitar fugas de memoria */
require(WPATH('vars'))[args.__id]['data']='';
};