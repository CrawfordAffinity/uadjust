var _bind4section={};

var _activity; 
if (OS_ANDROID) { _activity = $.ID_1238129225.activity; var abx = require('com.alcoapps.actionbarextras'); }
var _my_events = {}, _out_vars = {}, $item = {}, args = arguments[0] || {}; if ('__args' in args && '__id' in args['__args']) args.__id=args['__args'].__id; if ('__modelo' in args && _.keys(args.__modelo).length>0 && 'item' in $) { $.item.set(args.__modelo); $item = $.item.toJSON(); }
if (_.isUndefined(require(WPATH('vars'))[args.__id])) require(WPATH('vars'))[args.__id]={};
if (OS_ANDROID) {
   $.ID_1238129225.addEventListener('open', function(e) {
   });
}

/* Se inicia el modulo de camara */

var ID_1855143904 = Ti.UI.createView({
height : Ti.UI.FILL,
left : 0,
layout : 'composite',
width : Ti.UI.FILL,
top : 0,
tipo : 'imagen'
}
);
/* main window must open here */
function camaraID_1855143904() {
   require('com.skypanther.picatsize').showCamera({
      success: function(event) {
         if (event.mediaType==require('com.skypanther.picatsize').MEDIA_TYPE_PHOTO) {
            ID_1855143904.fireEvent("success", { valor:{ error:false, cancel:false, data:event.media, type:'photo' } }); 
         } else if (event.mediaType==require('com.skypanther.picatsize').MEDIA_TYPE_VIDEO) {
            ID_1855143904.fireEvent("success", { valor:{ error:false, cancel:false, data:event.media, type:'video' } }); 
         }
      },
      cancel: function() {
         ID_1855143904.fireEvent("cancel", { valor:{ error:false, cancel:true, data:'', reason:'cancelled' } }); 
      },
      error: function(error) {
         ID_1855143904.fireEvent("error", { valor:{ error:true, cancel:false, data:error, reason:error.error, type:'camera' } }); 
      },
      autohide: false,
      showControls: false,
      targetWidth: 480,
      targetHeight: 640,
      saveToPhotoGallery: false,
      overlay:ID_1855143904,
   });
}
require(WPATH('vars'))['_estadoflash_']='auto';
require(WPATH('vars'))['_tipocamara_']='trasera';
require(WPATH('vars'))['_hayflash_']=true;
if (Ti.Media.hasCameraPermissions()) {
  camaraID_1855143904();
} else {
  Ti.Media.requestCameraPermissions(function(ercp) {
     if (ercp.success) {
        camaraID_1855143904();
     } else {
        ID_1855143904.fireEvent("error", { valor:{ error:true, cancel:false, data:'nopermission', type:'permission', reason:'camera doesnt have permissions' } });
     }
  });
}
var ID_1364367324 = Titanium.UI.createView({
height : '50dp',
bottom : '10dp',
layout : 'composite',
width : '50dp',
borderRadius : 25,
backgroundColor : '#EE7F7E'
}
);
var ID_656257826 = Titanium.UI.createView({
height : Ti.UI.SIZE,
layout : 'vertical'
}
);
if (OS_IOS) {
var ID_1938836709 = Ti.UI.createImageView({
width : Ti.UI.FILL,
image : WPATH('images/iF6DA61D9235A335B8B38B9ADD7061672.png'),
id : ID_1938836709
}
);
} else {
var ID_1938836709 = Ti.UI.createImageView({
width : Ti.UI.FILL,
image : WPATH('images/iF6DA61D9235A335B8B38B9ADD7061672.png'),
id : ID_1938836709
}
);
}
require('vars')['_ID_1938836709_original_']=ID_1938836709.getImage();
require('vars')['_ID_1938836709_filtro_']='original';
ID_656257826.add(ID_1938836709);
ID_1364367324.add(ID_656257826);

ID_1364367324.addEventListener('touchstart',function(e) {
e.cancelBubble=true;
var elemento=e.source;
var ID_1938836709_imagen = 'camara_on';

													if (typeof ID_1938836709_imagen == 'string' && 'styles' in require(WPATH('a4w')) && ID_1938836709_imagen in require(WPATH('a4w')).styles['images']) {
														ID_1938836709_imagen = WPATH(require(WPATH('a4w')).styles['images'][ID_1938836709_imagen]);
													}
													ID_1938836709.setImage(ID_1938836709_imagen);

});

ID_1364367324.addEventListener('touchend',function(e) {
e.cancelBubble=true;
var elemento=e.source;
/* Comando que acciona el obtener la imagen que esta siendo vista en pantalla */
require('com.skypanther.picatsize').takePicture();
var ID_1938836709_imagen = 'camara_off';

													if (typeof ID_1938836709_imagen == 'string' && 'styles' in require(WPATH('a4w')) && ID_1938836709_imagen in require(WPATH('a4w')).styles['images']) {
														ID_1938836709_imagen = WPATH(require(WPATH('a4w')).styles['images'][ID_1938836709_imagen]);
													}
													ID_1938836709.setImage(ID_1938836709_imagen);

});
ID_1855143904.add(ID_1364367324);
var ID_894219185 = Titanium.UI.createView({
height : '50dp',
bottom : '10dp',
left : '30dp',
layout : 'composite',
width : '50dp'
}
);
var ID_551116671 = Titanium.UI.createView({
height : Ti.UI.SIZE,
layout : 'vertical'
}
);
if (OS_IOS) {
var ID_1736952448 = Ti.UI.createImageView({
image : WPATH('images/iFE1E6C4FA5DC4857A6CBF751E283DA6F.png'),
id : ID_1736952448
}
);
} else {
var ID_1736952448 = Ti.UI.createImageView({
image : WPATH('images/iFE1E6C4FA5DC4857A6CBF751E283DA6F.png'),
id : ID_1736952448
}
);
}
require('vars')['_ID_1736952448_original_']=ID_1736952448.getImage();
require('vars')['_ID_1736952448_filtro_']='original';
ID_551116671.add(ID_1736952448);
ID_894219185.add(ID_551116671);

ID_894219185.addEventListener('click',function(e) {
e.cancelBubble=true;
var elemento=e.source;
/* Recupera el ultimo valor del zoom que se aplico */
var zoom = ('zoom' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['zoom']:'';
if (_.isNumber(zoom) && _.isNumber(0) && zoom > 0) {
/* Se le resta un 10% al valor que ten&#237;a para aplicar el efecto de zoom */
 zoom = zoom-10;
/* Le quita un 10% de aumento visual a lo que se esta capturando a traves de la camara */
var ID_1855143904_zoom = zoom+'%';
	
				var modificarZoom = function(valor) {
					if (!isNaN(valor)) {
						require('com.skypanther.picatsize').setZoom(valor);
					} else if (valor.indexOf('%')!=-1) {
						require('com.skypanther.picatsize').setZoomPercentage(parseInt(valor));
					}
				};
				modificarZoom(ID_1855143904_zoom);
modificarZoom = null;

/* Guarda el valor del zoom que se aplico recien */
require(WPATH('vars'))[args.__id]['zoom']=zoom;
}
});
ID_1855143904.add(ID_894219185);
var ID_1958379218 = Titanium.UI.createView({
height : '50dp',
bottom : '10dp',
layout : 'composite',
width : '50dp',
right : '30dp'
}
);
var ID_710651880 = Titanium.UI.createView({
height : Ti.UI.SIZE,
layout : 'vertical'
}
);
if (OS_IOS) {
var ID_1757942441 = Ti.UI.createImageView({
image : WPATH('images/iC7C261216F75FB0B2EEDFEB678D6B75F.png'),
id : ID_1757942441
}
);
} else {
var ID_1757942441 = Ti.UI.createImageView({
image : WPATH('images/iC7C261216F75FB0B2EEDFEB678D6B75F.png'),
id : ID_1757942441
}
);
}
require('vars')['_ID_1757942441_original_']=ID_1757942441.getImage();
require('vars')['_ID_1757942441_filtro_']='original';
ID_710651880.add(ID_1757942441);
ID_1958379218.add(ID_710651880);

ID_1958379218.addEventListener('click',function(e) {
e.cancelBubble=true;
var elemento=e.source;
/* Recupera el ultimo valor del zoom que se aplico */
var zoom = ('zoom' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['zoom']:'';
if (_.isNumber(zoom) && _.isNumber(100) && zoom < 100) {
/* Se le resta un 10% al valor que ten&#237;a para aplicar el efecto de zoom */
 zoom = zoom+10;
/* Le quita un 10% de aumento visual a lo que se esta capturando a traves de la camara */
var ID_1855143904_zoom = zoom+'%';
	
				var modificarZoom = function(valor) {
					if (!isNaN(valor)) {
						require('com.skypanther.picatsize').setZoom(valor);
					} else if (valor.indexOf('%')!=-1) {
						require('com.skypanther.picatsize').setZoomPercentage(parseInt(valor));
					}
				};
				modificarZoom(ID_1855143904_zoom);
modificarZoom = null;

/* Guarda el valor del zoom que se aplico recien */
require(WPATH('vars'))[args.__id]['zoom']=zoom;
}
});
ID_1855143904.add(ID_1958379218);

ID_1855143904.addEventListener('success',function(e) {
e.cancelBubble=true;
var elemento=e.valor;
/* Envia la imagen a la pantalla que se esta utilizando el widget */
if ('__args' in args) {
	args['__args'].onfotolista({foto : elemento.data});
} else {
	args.onfotolista({foto : elemento.data});
}
/* Esconde la camara y la vista ya que se capturo la imagen */
var ID_1855143904_esconder = 'camara';

			var esconderVistaCamara = function(tipo) {
				if (tipo=='camara'||tipo=='cam'||tipo=='Camara') {
					require('com.skypanther.picatsize').hideCamera();
				} else if (tipo=='vista'||tipo=='view') {
					ID_1855143904.hide();
				}
			};
			esconderVistaCamara(ID_1855143904_esconder);

var ID_1855143904_esconder = 'vista';

			var esconderVistaCamara = function(tipo) {
				if (tipo=='camara'||tipo=='cam'||tipo=='Camara') {
					require('com.skypanther.picatsize').hideCamera();
				} else if (tipo=='vista'||tipo=='view') {
					ID_1855143904.hide();
				}
			};
			esconderVistaCamara(ID_1855143904_esconder);

/* Se deja nulo la variable para no tener fugas de memoria */
elemento=null;
});
var ID_1149317021 = Titanium.UI.createView({
height : '10dp',
layout : 'vertical',
top : '0dp',
elevation : 40
}
);
ID_1855143904.add(ID_1149317021);

function Androidback_ID_1917509173(e) {

e.cancelBubble=true;
var elemento=e.source;

}

(function() {
/* Se inicializa el numero del zoom en 0 */
 var numero = 0;
/* Se guarda la variable con el valor de numero para poder utlizarla en el zoom */
require(WPATH('vars'))[args.__id]['zoom']=numero;
})();

if (OS_IOS || OS_ANDROID) {
$.ID_1238129225.addEventListener('close',function(){
   $.destroy(); // cleanup bindings
   $.off(); //remove backbone events of this widget controller
   try {
      //require(WPATH('vars'))[args.__id]=null;
      args = null;
      if (OS_ANDROID) {
         abx = null;
      }
      if ($item) $item = null;
      if (_my_events) {
         for(_ev_tmp in _my_events) { 
            try {
               if (_ev_tmp.indexOf('_web')!=-1) {
                  Ti.App.removeEventListener(_ev_tmp.split(',')[0],_my_events[_ev_tmp]);
               } else {
                  Alloy.Events.off(_ev_tmp.split(',')[0],_my_events[_ev_tmp]);
               }
            } catch(err10) {
            }
         }
         _my_events = null;
         //delete _my_events;
      }
   } catch(err10) {
   }
   if (_out_vars) {
      var _ev_tmp;
      for(_ev_tmp in _out_vars) { 
         for (_ev_rem in _out_vars[_ev_tmp]._remove) {
            try {
 eval(_out_vars[_ev_tmp]._remove[_ev_rem]); 
 } catch(_errt) {}
         }
         _out_vars[_ev_tmp] = null;
      }
      _ev_tmp = null;
      _out_vars = null;
      //delete _out_vars;
   }
});
}