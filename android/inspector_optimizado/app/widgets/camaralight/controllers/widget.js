var _bind4section={};

var args = arguments[0] || {};


$.init = function(params) {
   for (var tobe in params) args[tobe] = params[tobe];
   if ('__id' in params) require(WPATH('vars'))[params.__id]={};
};

/* Cuando el widget es solicitado usar en otra pantalla se ocupa este evento */
$.disparar = function(params) {
/* Redireccion a la pantalla que ocupa una vista para capturar la foto */Widget.createController("capturador",{'_open' : 'false','__args' : (typeof args!=='undefined')?args:__args}).getView();
};