var _bind4section={};

var args = arguments[0] || {};


$.ID_1300949542.init({
titulo : '¿PUEDE CONTINUAR CON LA INSPECCION?',
__id : 'ALL1300949542',
texto : 'Si está el asegurado en el domicilio presione SI para continuar',
alto : '-',
top : 0,
ancho : '90%',
tipo : '_info'
}
);

function Touchstart_ID_479156976(e) {

e.cancelBubble=true;
var elemento=e.source;
/* Cambia el color de fondo mientras la vista sea presionada */
$.ID_1746056874.setBackgroundColor('#f6fdfa');

}
function Touchend_ID_1911965702(e) {

e.cancelBubble=true;
var elemento=e.source;
/* Cambia el color de fondo cuando la vista deja de ser presionada */
$.ID_1746056874.setBackgroundColor('#ffffff');
/* Llama al evento si en la pantalla que se ocupa el widget */
if ('__args' in args) {
	args['__args'].onsi({});
} else {
	args.onsi({});
}

}
function Touchstart_ID_979464485(e) {

e.cancelBubble=true;
var elemento=e.source;
/* Cambia el color de fondo mientras la vista sea presionada */
$.ID_1824685387.setBackgroundColor('#fdf5f5');

}
function Touchend_ID_988414676(e) {

e.cancelBubble=true;
var elemento=e.source;
/* Cambia el color de fondo cuando la vista deja de ser presionada */
$.ID_1824685387.setBackgroundColor('#ffffff');
/* Llama al evento no en la pantalla que se ocupa el widget */
if ('__args' in args) {
	args['__args'].onno({});
} else {
	args.onno({});
}

}

$.init = function(params) {
   for (var tobe in params) args[tobe] = params[tobe];
   if ('__id' in params) require(WPATH('vars'))[params.__id]={};
if (!_.isUndefined(params.top)) {
/* Define el top que tendra la vista */

$.ID_1300949542.update({top : params.top});
}
if (!_.isUndefined(params.titulo)) {
/* Define el titulo que tendra el mensaje */

$.ID_1300949542.update({titulo : params.titulo});
}
if (!_.isUndefined(params.texto)) {
/* Define el mensaje del mono en pantalla */

$.ID_1300949542.update({texto : params.texto});
}
if (!_.isUndefined(params.si)) {
/* Define el texto que ira en el boton si */
$.ID_853778123.setText(params.si);

}
if (!_.isUndefined(params.no)) {
/* Define el texto que ira en el boton no */
$.ID_1376957032.setText(params.no);

if (_.isNumber((params.no.length)) && _.isNumber(28) && (params.no.length) < 28) {
/* Si el texto que se mostrara en el boton no es menor a 28 caracteres cambiara el top que tendra el texto respecto a la vista que lo contiene */
$.ID_1376957032.setTop(25);

}
 else {
$.ID_1376957032.setTop(18);

}}
if (!_.isUndefined(params.ajustado)) {
/* Define el top que tendra el texto de no */
$.ID_776836918.setTop(params.ajustado);

}
};

/* En el caso que la vista ya haya sido cargada y se quiera modificar algo, se usa el evento update */
$.update = function(params) {
if (!_.isUndefined(params.titulo)) {
/* Define el top que tendra la vista */

$.ID_1300949542.update({titulo : params.titulo});
}
if (!_.isUndefined(params.texto)) {
/* Define el titulo que tendra el mensaje */

$.ID_1300949542.update({texto : params.texto});
}
if (!_.isUndefined(params.si)) {
/* Define el texto que ira en el boton si */
$.ID_853778123.setText(params.si);

}
if (!_.isUndefined(params.no)) {
/* Define el texto que ira en el boton no */
$.ID_1376957032.setText(params.no);

if (_.isNumber((params.no.length)) && _.isNumber(28) && (params.no.length) < 28) {
/* Si el texto que se mostrara en el boton no es menor a 28 caracteres cambiara el top que tendra el texto respecto a la vista que lo contiene */
$.ID_1376957032.setTop(25);

}
 else {
$.ID_1376957032.setTop(18);

}}
};