/** 
* Widget Intermedio Esta de Acuerdo
* Representa un control oculto que al ser ejecutado su funcion abrir, abre una pantalla que contiene el widget pregunta para decidir si continua con a la siguiente pantalla o se mantiene donde esta el control, para continuar editando los datos.
* No requiere parametros. 
*/
var _bind4section={};

var args = arguments[0] || {};

/** 
* Funcion que fuerza apertura de pantalla
* 
* @param color Indica el color del header para la pantalla 
*/

$.abrir = function(params) {
/** 
* Recibe el valor del color para poder pintar el statusbar y navbar desde la pantalla que la esta utilizando 
*/
require(WPATH('vars'))[args.__id]['color']=params.color;
Widget.createController("pregunta_esta_de_acuerdo",{'__args' : (typeof args!=='undefined')?args:__args}).getView().open();
};

/** 
* Funcion que inicializa el widget 
*/

$.init = function(params) {
   for (var tobe in params) args[tobe] = params[tobe];
   if ('__id' in params) require(WPATH('vars'))[params.__id]={};
require(WPATH('vars'))[args.__id]['params']=params;
};