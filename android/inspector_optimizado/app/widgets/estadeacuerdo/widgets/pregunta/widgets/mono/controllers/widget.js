/** 
* Widget Mono-Personaje
* Muestra diferentes tipos de personajes con un mensaje de texto personalizable
* 
* @param visible Indica visibilidad de widget
* @param tipo Indica tipo de personaje: tip, info, sorry 
*/
var _bind4section={};

var args = arguments[0] || {};

/** 
* Funcion que inicializa widget 
*/

$.init = function(params) {
   for (var tobe in params) args[tobe] = params[tobe];
   if ('__id' in params) require(WPATH('vars'))[params.__id]={};
/** 
* Seteamos la variable de la visibilidad del mono en true, a menos que se indique lo contrario al inicializar el widget 
*/
require(WPATH('vars'))[args.__id]['visible']='true';
if (!_.isUndefined(params.visible)) {
require(WPATH('vars'))[args.__id]['visible']=params.visible;
}
var visible = ('visible' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['visible']:'';
if (!_.isUndefined(params.tipo)) {
if (params.tipo=='_tip') {
/** 
* Dependiendo del tipo de mono a mostrar, ocultamos todas excepto la que necesitamos mostrar 
*/
/** 
* Definimos el alto de la vista y actualizamos el tipo de mono 
*/
require(WPATH('vars'))[args.__id]['alto']=120;
require(WPATH('vars'))[args.__id]['tipo']='_tip';
/** 
* mono happy horizontal 
*/
var ID_519072476_visible = visible;

										  if (ID_519072476_visible=='si') {
											  ID_519072476_visible=true;
										  } else if (ID_519072476_visible=='no') {
											  ID_519072476_visible=false;
										  }
										  $.ID_519072476.setVisible(ID_519072476_visible);

var ID_519072476_alto = 120;

								  if (ID_519072476_alto=='*') {
									  ID_519072476_alto=Ti.UI.FILL;
								  } else if (ID_519072476_alto=='-') {
									  ID_519072476_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_519072476_alto)) {
									  ID_519072476_alto=ID_519072476_alto+'dp';
								  }
								  $.ID_519072476.setHeight(ID_519072476_alto);

var ID_1845899524_visible = false;

										  if (ID_1845899524_visible=='si') {
											  ID_1845899524_visible=true;
										  } else if (ID_1845899524_visible=='no') {
											  ID_1845899524_visible=false;
										  }
										  $.ID_1845899524.setVisible(ID_1845899524_visible);

var ID_1845899524_alto = 0;

								  if (ID_1845899524_alto=='*') {
									  ID_1845899524_alto=Ti.UI.FILL;
								  } else if (ID_1845899524_alto=='-') {
									  ID_1845899524_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1845899524_alto)) {
									  ID_1845899524_alto=ID_1845899524_alto+'dp';
								  }
								  $.ID_1845899524.setHeight(ID_1845899524_alto);

var ID_1015068704_visible = false;

										  if (ID_1015068704_visible=='si') {
											  ID_1015068704_visible=true;
										  } else if (ID_1015068704_visible=='no') {
											  ID_1015068704_visible=false;
										  }
										  $.ID_1015068704.setVisible(ID_1015068704_visible);

var ID_1015068704_alto = 0;

								  if (ID_1015068704_alto=='*') {
									  ID_1015068704_alto=Ti.UI.FILL;
								  } else if (ID_1015068704_alto=='-') {
									  ID_1015068704_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1015068704_alto)) {
									  ID_1015068704_alto=ID_1015068704_alto+'dp';
								  }
								  $.ID_1015068704.setHeight(ID_1015068704_alto);

if (!_.isUndefined(params.texto)) {
$.ID_444332723.setText(params.texto);

}
if (!_.isUndefined(params.top)) {
$.ID_519072476.setTop(params.top);

}
if (!_.isUndefined(params.bottom)) {
$.ID_519072476.setBottom(params.bottom);

}
if (!_.isUndefined(params.ancho)) {
var ID_519072476_ancho = params.ancho;

								  if (ID_519072476_ancho=='*') {
									  ID_519072476_ancho=Ti.UI.FILL;
								  } else if (ID_519072476_ancho=='-') {
									  ID_519072476_ancho=Ti.UI.SIZE;
								  } else if (!isNaN(ID_519072476_ancho)) {
									  ID_519072476_ancho=ID_519072476_ancho+'dp';
								  }
								  $.ID_519072476.setWidth(ID_519072476_ancho);

}
if (!_.isUndefined(params.alto)) {
var ID_519072476_alto = params.alto;

								  if (ID_519072476_alto=='*') {
									  ID_519072476_alto=Ti.UI.FILL;
								  } else if (ID_519072476_alto=='-') {
									  ID_519072476_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_519072476_alto)) {
									  ID_519072476_alto=ID_519072476_alto+'dp';
								  }
								  $.ID_519072476.setHeight(ID_519072476_alto);

require(WPATH('vars'))[args.__id]['alto']=params.alto;
}
if (!_.isUndefined(params.titulo)) {
$.ID_908514794.setText(params.titulo);

var ID_908514794_visible = true;

								  if (ID_908514794_visible=='si') {
									  ID_908514794_visible=true;
								  } else if (ID_908514794_visible=='no') {
									  ID_908514794_visible=false;
								  }
								  $.ID_908514794.setVisible(ID_908514794_visible);

}
}
 else if (params.tipo=='_sorry') {
/** 
* Definimos el alto de la vista y actualizamos el tipo de mono 
*/
require(WPATH('vars'))[args.__id]['tipo']='_sorry';
require(WPATH('vars'))[args.__id]['alto']='-';
var ID_1845899524_visible = visible;

										  if (ID_1845899524_visible=='si') {
											  ID_1845899524_visible=true;
										  } else if (ID_1845899524_visible=='no') {
											  ID_1845899524_visible=false;
										  }
										  $.ID_1845899524.setVisible(ID_1845899524_visible);

var ID_1845899524_alto = '-';

								  if (ID_1845899524_alto=='*') {
									  ID_1845899524_alto=Ti.UI.FILL;
								  } else if (ID_1845899524_alto=='-') {
									  ID_1845899524_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1845899524_alto)) {
									  ID_1845899524_alto=ID_1845899524_alto+'dp';
								  }
								  $.ID_1845899524.setHeight(ID_1845899524_alto);

var ID_519072476_visible = false;

										  if (ID_519072476_visible=='si') {
											  ID_519072476_visible=true;
										  } else if (ID_519072476_visible=='no') {
											  ID_519072476_visible=false;
										  }
										  $.ID_519072476.setVisible(ID_519072476_visible);

var ID_519072476_alto = 0;

								  if (ID_519072476_alto=='*') {
									  ID_519072476_alto=Ti.UI.FILL;
								  } else if (ID_519072476_alto=='-') {
									  ID_519072476_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_519072476_alto)) {
									  ID_519072476_alto=ID_519072476_alto+'dp';
								  }
								  $.ID_519072476.setHeight(ID_519072476_alto);

var ID_1015068704_visible = false;

										  if (ID_1015068704_visible=='si') {
											  ID_1015068704_visible=true;
										  } else if (ID_1015068704_visible=='no') {
											  ID_1015068704_visible=false;
										  }
										  $.ID_1015068704.setVisible(ID_1015068704_visible);

var ID_1015068704_alto = 0;

								  if (ID_1015068704_alto=='*') {
									  ID_1015068704_alto=Ti.UI.FILL;
								  } else if (ID_1015068704_alto=='-') {
									  ID_1015068704_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1015068704_alto)) {
									  ID_1015068704_alto=ID_1015068704_alto+'dp';
								  }
								  $.ID_1015068704.setHeight(ID_1015068704_alto);

if (!_.isUndefined(params.titulo)) {
$.ID_344364396.setText(params.titulo);

}
if (!_.isUndefined(params.texto)) {
$.ID_760973822.setText(params.texto);

}
if (!_.isUndefined(params.top)) {
$.ID_1845899524.setTop(params.top);

}
if (!_.isUndefined(params.bottom)) {
$.ID_1845899524.setBottom(params.bottom);

}
if (!_.isUndefined(params.ancho)) {
var ID_1845899524_ancho = params.ancho;

								  if (ID_1845899524_ancho=='*') {
									  ID_1845899524_ancho=Ti.UI.FILL;
								  } else if (ID_1845899524_ancho=='-') {
									  ID_1845899524_ancho=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1845899524_ancho)) {
									  ID_1845899524_ancho=ID_1845899524_ancho+'dp';
								  }
								  $.ID_1845899524.setWidth(ID_1845899524_ancho);

}
if (!_.isUndefined(params.alto)) {
var ID_1845899524_alto = params.alto;

								  if (ID_1845899524_alto=='*') {
									  ID_1845899524_alto=Ti.UI.FILL;
								  } else if (ID_1845899524_alto=='-') {
									  ID_1845899524_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1845899524_alto)) {
									  ID_1845899524_alto=ID_1845899524_alto+'dp';
								  }
								  $.ID_1845899524.setHeight(ID_1845899524_alto);

require(WPATH('vars'))[args.__id]['alto']=params.alto;
}
} else if (params.tipo=='_info') {
/** 
* Definimos el alto de la vista y actualizamos el tipo de mono 
*/
require(WPATH('vars'))[args.__id]['tipo']='_info';
require(WPATH('vars'))[args.__id]['alto']='-';
var ID_1015068704_visible = visible;

										  if (ID_1015068704_visible=='si') {
											  ID_1015068704_visible=true;
										  } else if (ID_1015068704_visible=='no') {
											  ID_1015068704_visible=false;
										  }
										  $.ID_1015068704.setVisible(ID_1015068704_visible);

var ID_1015068704_alto = '-';

								  if (ID_1015068704_alto=='*') {
									  ID_1015068704_alto=Ti.UI.FILL;
								  } else if (ID_1015068704_alto=='-') {
									  ID_1015068704_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1015068704_alto)) {
									  ID_1015068704_alto=ID_1015068704_alto+'dp';
								  }
								  $.ID_1015068704.setHeight(ID_1015068704_alto);

var ID_519072476_visible = false;

										  if (ID_519072476_visible=='si') {
											  ID_519072476_visible=true;
										  } else if (ID_519072476_visible=='no') {
											  ID_519072476_visible=false;
										  }
										  $.ID_519072476.setVisible(ID_519072476_visible);

var ID_519072476_alto = 0;

								  if (ID_519072476_alto=='*') {
									  ID_519072476_alto=Ti.UI.FILL;
								  } else if (ID_519072476_alto=='-') {
									  ID_519072476_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_519072476_alto)) {
									  ID_519072476_alto=ID_519072476_alto+'dp';
								  }
								  $.ID_519072476.setHeight(ID_519072476_alto);

var ID_1845899524_visible = false;

										  if (ID_1845899524_visible=='si') {
											  ID_1845899524_visible=true;
										  } else if (ID_1845899524_visible=='no') {
											  ID_1845899524_visible=false;
										  }
										  $.ID_1845899524.setVisible(ID_1845899524_visible);

var ID_1845899524_alto = 0;

								  if (ID_1845899524_alto=='*') {
									  ID_1845899524_alto=Ti.UI.FILL;
								  } else if (ID_1845899524_alto=='-') {
									  ID_1845899524_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1845899524_alto)) {
									  ID_1845899524_alto=ID_1845899524_alto+'dp';
								  }
								  $.ID_1845899524.setHeight(ID_1845899524_alto);

if (!_.isUndefined(params.titulo)) {
$.ID_184880256.setText(params.titulo);

}
if (!_.isUndefined(params.texto)) {
$.ID_1906529925.setText(params.texto);

}
if (!_.isUndefined(params.top)) {
$.ID_1015068704.setTop(params.top);

}
if (!_.isUndefined(params.bottom)) {
$.ID_1015068704.setBottom(params.bottom);

}
if (!_.isUndefined(params.ancho)) {
var ID_1015068704_ancho = params.ancho;

								  if (ID_1015068704_ancho=='*') {
									  ID_1015068704_ancho=Ti.UI.FILL;
								  } else if (ID_1015068704_ancho=='-') {
									  ID_1015068704_ancho=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1015068704_ancho)) {
									  ID_1015068704_ancho=ID_1015068704_ancho+'dp';
								  }
								  $.ID_1015068704.setWidth(ID_1015068704_ancho);

}
if (!_.isUndefined(params.alto)) {
var ID_1015068704_alto = params.alto;

								  if (ID_1015068704_alto=='*') {
									  ID_1015068704_alto=Ti.UI.FILL;
								  } else if (ID_1015068704_alto=='-') {
									  ID_1015068704_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1015068704_alto)) {
									  ID_1015068704_alto=ID_1015068704_alto+'dp';
								  }
								  $.ID_1015068704.setHeight(ID_1015068704_alto);

require(WPATH('vars'))[args.__id]['alto']=params.alto;
}
}}
};

/** 
* Funcion que actualiza texto y/o personaje
* 
* @param visible Indica si mostrar o no este widget
* @param tipo Tipo de personaje: tip, info, sorry 
*/

$.update = function(params) {
if (!_.isUndefined(params.tipo)) {
/** 
* Revisamos si se tiene que actualizar el tipo de mono que se va a mostrar, en caso de que exista, actualizamos la variable del tipo de mono a mostrar 
*/
if (params.tipo=='_tip') {
require(WPATH('vars'))[args.__id]['tipo']='_tip';
}
 else if (params.tipo=='_sorry') {
require(WPATH('vars'))[args.__id]['tipo']='_sorry';
} else if (params.tipo=='_info') {
require(WPATH('vars'))[args.__id]['tipo']='_info';
}}
if (!_.isUndefined(params.visible)) {
/** 
* Revisamos si el widget sera visible o no 
*/
require(WPATH('vars'))[args.__id]['visible']=params.visible;
}
var tipo = ('tipo' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['tipo']:'';
var visible = ('visible' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['visible']:'';
var alto = ('alto' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['alto']:'';
if (params.tipo=='_tip') {
/** 
* Definimos el alto de la vista y guardamos el tipo de vista en una variable 
*/
require(WPATH('vars'))[args.__id]['alto']=120;
require(WPATH('vars'))[args.__id]['tipo']='_tip';
/** 
* mono happy horizontal 
*/
var ID_519072476_visible = visible;

										  if (ID_519072476_visible=='si') {
											  ID_519072476_visible=true;
										  } else if (ID_519072476_visible=='no') {
											  ID_519072476_visible=false;
										  }
										  $.ID_519072476.setVisible(ID_519072476_visible);

var ID_519072476_alto = 120;

								  if (ID_519072476_alto=='*') {
									  ID_519072476_alto=Ti.UI.FILL;
								  } else if (ID_519072476_alto=='-') {
									  ID_519072476_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_519072476_alto)) {
									  ID_519072476_alto=ID_519072476_alto+'dp';
								  }
								  $.ID_519072476.setHeight(ID_519072476_alto);

/** 
* ocultamos otros tipos... sorry 
*/
var ID_1845899524_visible = false;

										  if (ID_1845899524_visible=='si') {
											  ID_1845899524_visible=true;
										  } else if (ID_1845899524_visible=='no') {
											  ID_1845899524_visible=false;
										  }
										  $.ID_1845899524.setVisible(ID_1845899524_visible);

var ID_1845899524_alto = 0;

								  if (ID_1845899524_alto=='*') {
									  ID_1845899524_alto=Ti.UI.FILL;
								  } else if (ID_1845899524_alto=='-') {
									  ID_1845899524_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1845899524_alto)) {
									  ID_1845899524_alto=ID_1845899524_alto+'dp';
								  }
								  $.ID_1845899524.setHeight(ID_1845899524_alto);

/** 
* Ocultamos info 
*/
var ID_1015068704_visible = false;

										  if (ID_1015068704_visible=='si') {
											  ID_1015068704_visible=true;
										  } else if (ID_1015068704_visible=='no') {
											  ID_1015068704_visible=false;
										  }
										  $.ID_1015068704.setVisible(ID_1015068704_visible);

var ID_1015068704_alto = 0;

								  if (ID_1015068704_alto=='*') {
									  ID_1015068704_alto=Ti.UI.FILL;
								  } else if (ID_1015068704_alto=='-') {
									  ID_1015068704_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1015068704_alto)) {
									  ID_1015068704_alto=ID_1015068704_alto+'dp';
								  }
								  $.ID_1015068704.setHeight(ID_1015068704_alto);

if (!_.isUndefined(params.texto)) {
/** 
* Modificamos vista o texto en caso de que sea recibido algun parametro por pantalla que consume el widget 
*/
$.ID_444332723.setText(params.texto);

}
if (!_.isUndefined(params.top)) {
$.ID_519072476.setTop(params.top);

}
if (!_.isUndefined(params.bottom)) {
$.ID_519072476.setBottom(params.bottom);

}
if (!_.isUndefined(params.ancho)) {
var ID_519072476_ancho = params.ancho;

								  if (ID_519072476_ancho=='*') {
									  ID_519072476_ancho=Ti.UI.FILL;
								  } else if (ID_519072476_ancho=='-') {
									  ID_519072476_ancho=Ti.UI.SIZE;
								  } else if (!isNaN(ID_519072476_ancho)) {
									  ID_519072476_ancho=ID_519072476_ancho+'dp';
								  }
								  $.ID_519072476.setWidth(ID_519072476_ancho);

}
if (!_.isUndefined(params.alto)) {
var ID_519072476_alto = params.alto;

								  if (ID_519072476_alto=='*') {
									  ID_519072476_alto=Ti.UI.FILL;
								  } else if (ID_519072476_alto=='-') {
									  ID_519072476_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_519072476_alto)) {
									  ID_519072476_alto=ID_519072476_alto+'dp';
								  }
								  $.ID_519072476.setHeight(ID_519072476_alto);

require(WPATH('vars'))[args.__id]['alto']=params.alto;
}
if (!_.isUndefined(params.titulo)) {
if ((_.isObject(params.titulo) || (_.isString(params.titulo)) &&  !_.isEmpty(params.titulo)) || _.isNumber(params.titulo) || _.isBoolean(params.titulo)) {
$.ID_908514794.setText(params.titulo);

var ID_908514794_visible = true;

								  if (ID_908514794_visible=='si') {
									  ID_908514794_visible=true;
								  } else if (ID_908514794_visible=='no') {
									  ID_908514794_visible=false;
								  }
								  $.ID_908514794.setVisible(ID_908514794_visible);

}
 else {
$.ID_908514794.setText(params.titulo);

var ID_908514794_visible = false;

								  if (ID_908514794_visible=='si') {
									  ID_908514794_visible=true;
								  } else if (ID_908514794_visible=='no') {
									  ID_908514794_visible=false;
								  }
								  $.ID_908514794.setVisible(ID_908514794_visible);

}}
}
 else if (tipo=='_sorry') {
/** 
* Mono triste vertical 
*/
var ID_1845899524_visible = visible;

										  if (ID_1845899524_visible=='si') {
											  ID_1845899524_visible=true;
										  } else if (ID_1845899524_visible=='no') {
											  ID_1845899524_visible=false;
										  }
										  $.ID_1845899524.setVisible(ID_1845899524_visible);

var ID_1845899524_alto = alto;

								  if (ID_1845899524_alto=='*') {
									  ID_1845899524_alto=Ti.UI.FILL;
								  } else if (ID_1845899524_alto=='-') {
									  ID_1845899524_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1845899524_alto)) {
									  ID_1845899524_alto=ID_1845899524_alto+'dp';
								  }
								  $.ID_1845899524.setHeight(ID_1845899524_alto);

/** 
* ocultamos otros tipos 
*/
var ID_519072476_visible = false;

										  if (ID_519072476_visible=='si') {
											  ID_519072476_visible=true;
										  } else if (ID_519072476_visible=='no') {
											  ID_519072476_visible=false;
										  }
										  $.ID_519072476.setVisible(ID_519072476_visible);

var ID_519072476_alto = 0;

								  if (ID_519072476_alto=='*') {
									  ID_519072476_alto=Ti.UI.FILL;
								  } else if (ID_519072476_alto=='-') {
									  ID_519072476_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_519072476_alto)) {
									  ID_519072476_alto=ID_519072476_alto+'dp';
								  }
								  $.ID_519072476.setHeight(ID_519072476_alto);

var ID_1015068704_visible = false;

										  if (ID_1015068704_visible=='si') {
											  ID_1015068704_visible=true;
										  } else if (ID_1015068704_visible=='no') {
											  ID_1015068704_visible=false;
										  }
										  $.ID_1015068704.setVisible(ID_1015068704_visible);

var ID_1015068704_alto = 0;

								  if (ID_1015068704_alto=='*') {
									  ID_1015068704_alto=Ti.UI.FILL;
								  } else if (ID_1015068704_alto=='-') {
									  ID_1015068704_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1015068704_alto)) {
									  ID_1015068704_alto=ID_1015068704_alto+'dp';
								  }
								  $.ID_1015068704.setHeight(ID_1015068704_alto);

if (!_.isUndefined(params.titulo)) {
/** 
* Modificamos vista o texto en caso de que sea recibido algun parametro por pantalla que consume el widget 
*/
$.ID_344364396.setText(params.titulo);

}
if (!_.isUndefined(params.texto)) {
$.ID_760973822.setText(params.texto);

}
if (!_.isUndefined(params.top)) {
$.ID_1845899524.setTop(params.top);

}
if (!_.isUndefined(params.bottom)) {
$.ID_1845899524.setBottom(params.bottom);

}
if (!_.isUndefined(params.ancho)) {
var ID_1845899524_ancho = params.ancho;

								  if (ID_1845899524_ancho=='*') {
									  ID_1845899524_ancho=Ti.UI.FILL;
								  } else if (ID_1845899524_ancho=='-') {
									  ID_1845899524_ancho=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1845899524_ancho)) {
									  ID_1845899524_ancho=ID_1845899524_ancho+'dp';
								  }
								  $.ID_1845899524.setWidth(ID_1845899524_ancho);

}
if (!_.isUndefined(params.alto)) {
var ID_1845899524_alto = params.alto;

								  if (ID_1845899524_alto=='*') {
									  ID_1845899524_alto=Ti.UI.FILL;
								  } else if (ID_1845899524_alto=='-') {
									  ID_1845899524_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1845899524_alto)) {
									  ID_1845899524_alto=ID_1845899524_alto+'dp';
								  }
								  $.ID_1845899524.setHeight(ID_1845899524_alto);

require(WPATH('vars'))[args.__id]['alto']=params.alto;
}
} else if (tipo=='_info') {
/** 
* mono happy vertical 
*/
var ID_1015068704_visible = visible;

										  if (ID_1015068704_visible=='si') {
											  ID_1015068704_visible=true;
										  } else if (ID_1015068704_visible=='no') {
											  ID_1015068704_visible=false;
										  }
										  $.ID_1015068704.setVisible(ID_1015068704_visible);

var ID_1015068704_alto = alto;

								  if (ID_1015068704_alto=='*') {
									  ID_1015068704_alto=Ti.UI.FILL;
								  } else if (ID_1015068704_alto=='-') {
									  ID_1015068704_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1015068704_alto)) {
									  ID_1015068704_alto=ID_1015068704_alto+'dp';
								  }
								  $.ID_1015068704.setHeight(ID_1015068704_alto);

/** 
* ocultamos otros tipos 
*/
var ID_519072476_visible = false;

										  if (ID_519072476_visible=='si') {
											  ID_519072476_visible=true;
										  } else if (ID_519072476_visible=='no') {
											  ID_519072476_visible=false;
										  }
										  $.ID_519072476.setVisible(ID_519072476_visible);

var ID_519072476_alto = 0;

								  if (ID_519072476_alto=='*') {
									  ID_519072476_alto=Ti.UI.FILL;
								  } else if (ID_519072476_alto=='-') {
									  ID_519072476_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_519072476_alto)) {
									  ID_519072476_alto=ID_519072476_alto+'dp';
								  }
								  $.ID_519072476.setHeight(ID_519072476_alto);

var ID_1845899524_visible = false;

										  if (ID_1845899524_visible=='si') {
											  ID_1845899524_visible=true;
										  } else if (ID_1845899524_visible=='no') {
											  ID_1845899524_visible=false;
										  }
										  $.ID_1845899524.setVisible(ID_1845899524_visible);

var ID_1845899524_alto = 0;

								  if (ID_1845899524_alto=='*') {
									  ID_1845899524_alto=Ti.UI.FILL;
								  } else if (ID_1845899524_alto=='-') {
									  ID_1845899524_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1845899524_alto)) {
									  ID_1845899524_alto=ID_1845899524_alto+'dp';
								  }
								  $.ID_1845899524.setHeight(ID_1845899524_alto);

if (!_.isUndefined(params.titulo)) {
/** 
* Modificamos vista o texto en caso de que sea recibido algun parametro por pantalla que consume el widget 
*/
$.ID_184880256.setText(params.titulo);

}
if (!_.isUndefined(params.texto)) {
$.ID_1906529925.setText(params.texto);

}
if (!_.isUndefined(params.top)) {
$.ID_1015068704.setTop(params.top);

}
if (!_.isUndefined(params.bottom)) {
$.ID_1015068704.setBottom(params.bottom);

}
if (!_.isUndefined(params.ancho)) {
var ID_1015068704_ancho = params.ancho;

								  if (ID_1015068704_ancho=='*') {
									  ID_1015068704_ancho=Ti.UI.FILL;
								  } else if (ID_1015068704_ancho=='-') {
									  ID_1015068704_ancho=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1015068704_ancho)) {
									  ID_1015068704_ancho=ID_1015068704_ancho+'dp';
								  }
								  $.ID_1015068704.setWidth(ID_1015068704_ancho);

}
if (!_.isUndefined(params.alto)) {
var ID_1015068704_alto = params.alto;

								  if (ID_1015068704_alto=='*') {
									  ID_1015068704_alto=Ti.UI.FILL;
								  } else if (ID_1015068704_alto=='-') {
									  ID_1015068704_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1015068704_alto)) {
									  ID_1015068704_alto=ID_1015068704_alto+'dp';
								  }
								  $.ID_1015068704.setHeight(ID_1015068704_alto);

require(WPATH('vars'))[args.__id]['alto']=params.alto;
}
}};

/** 
* Funcion que oculta widget 
*/

$.ocultar = function(params) {
/** 
* Ocultamos todas...
* _tip 
*/
var ID_519072476_visible = false;

										  if (ID_519072476_visible=='si') {
											  ID_519072476_visible=true;
										  } else if (ID_519072476_visible=='no') {
											  ID_519072476_visible=false;
										  }
										  $.ID_519072476.setVisible(ID_519072476_visible);

var ID_519072476_alto = 0;

								  if (ID_519072476_alto=='*') {
									  ID_519072476_alto=Ti.UI.FILL;
								  } else if (ID_519072476_alto=='-') {
									  ID_519072476_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_519072476_alto)) {
									  ID_519072476_alto=ID_519072476_alto+'dp';
								  }
								  $.ID_519072476.setHeight(ID_519072476_alto);

/** 
* Ocultamos _sorry 
*/
var ID_1845899524_visible = false;

										  if (ID_1845899524_visible=='si') {
											  ID_1845899524_visible=true;
										  } else if (ID_1845899524_visible=='no') {
											  ID_1845899524_visible=false;
										  }
										  $.ID_1845899524.setVisible(ID_1845899524_visible);

var ID_1845899524_alto = 0;

								  if (ID_1845899524_alto=='*') {
									  ID_1845899524_alto=Ti.UI.FILL;
								  } else if (ID_1845899524_alto=='-') {
									  ID_1845899524_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1845899524_alto)) {
									  ID_1845899524_alto=ID_1845899524_alto+'dp';
								  }
								  $.ID_1845899524.setHeight(ID_1845899524_alto);

/** 
* Ocultamos _info 
*/
var ID_1015068704_visible = false;

										  if (ID_1015068704_visible=='si') {
											  ID_1015068704_visible=true;
										  } else if (ID_1015068704_visible=='no') {
											  ID_1015068704_visible=false;
										  }
										  $.ID_1015068704.setVisible(ID_1015068704_visible);

var ID_1015068704_alto = 0;

								  if (ID_1015068704_alto=='*') {
									  ID_1015068704_alto=Ti.UI.FILL;
								  } else if (ID_1015068704_alto=='-') {
									  ID_1015068704_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1015068704_alto)) {
									  ID_1015068704_alto=ID_1015068704_alto+'dp';
								  }
								  $.ID_1015068704.setHeight(ID_1015068704_alto);

};

/** 
* Funcion que muestra widget 
*/

$.mostrar = function(params) {
/** 
* Mostramos ultimo tipo 
*/
var tipo = ('tipo' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['tipo']:'';
var visible = ('visible' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['visible']:'';
if (!_.isUndefined(params.tipo)) {
if (tipo=='_tip') {
/** 
* Dejamos _tip como estaba 
*/
var ID_519072476_visible = visible;

										  if (ID_519072476_visible=='si') {
											  ID_519072476_visible=true;
										  } else if (ID_519072476_visible=='no') {
											  ID_519072476_visible=false;
										  }
										  $.ID_519072476.setVisible(ID_519072476_visible);

var ID_519072476_alto = 120;

								  if (ID_519072476_alto=='*') {
									  ID_519072476_alto=Ti.UI.FILL;
								  } else if (ID_519072476_alto=='-') {
									  ID_519072476_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_519072476_alto)) {
									  ID_519072476_alto=ID_519072476_alto+'dp';
								  }
								  $.ID_519072476.setHeight(ID_519072476_alto);

/** 
* Ocultamos _sorry 
*/
var ID_1845899524_visible = false;

										  if (ID_1845899524_visible=='si') {
											  ID_1845899524_visible=true;
										  } else if (ID_1845899524_visible=='no') {
											  ID_1845899524_visible=false;
										  }
										  $.ID_1845899524.setVisible(ID_1845899524_visible);

var ID_1845899524_alto = 0;

								  if (ID_1845899524_alto=='*') {
									  ID_1845899524_alto=Ti.UI.FILL;
								  } else if (ID_1845899524_alto=='-') {
									  ID_1845899524_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1845899524_alto)) {
									  ID_1845899524_alto=ID_1845899524_alto+'dp';
								  }
								  $.ID_1845899524.setHeight(ID_1845899524_alto);

/** 
* Ocultamos _info 
*/
var ID_1015068704_visible = false;

										  if (ID_1015068704_visible=='si') {
											  ID_1015068704_visible=true;
										  } else if (ID_1015068704_visible=='no') {
											  ID_1015068704_visible=false;
										  }
										  $.ID_1015068704.setVisible(ID_1015068704_visible);

var ID_1015068704_alto = 0;

								  if (ID_1015068704_alto=='*') {
									  ID_1015068704_alto=Ti.UI.FILL;
								  } else if (ID_1015068704_alto=='-') {
									  ID_1015068704_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1015068704_alto)) {
									  ID_1015068704_alto=ID_1015068704_alto+'dp';
								  }
								  $.ID_1015068704.setHeight(ID_1015068704_alto);

}
 else if (tipo=='_sorry') {
/** 
* Ocultamos _tip 
*/
var ID_519072476_visible = false;

										  if (ID_519072476_visible=='si') {
											  ID_519072476_visible=true;
										  } else if (ID_519072476_visible=='no') {
											  ID_519072476_visible=false;
										  }
										  $.ID_519072476.setVisible(ID_519072476_visible);

var ID_519072476_alto = 0;

								  if (ID_519072476_alto=='*') {
									  ID_519072476_alto=Ti.UI.FILL;
								  } else if (ID_519072476_alto=='-') {
									  ID_519072476_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_519072476_alto)) {
									  ID_519072476_alto=ID_519072476_alto+'dp';
								  }
								  $.ID_519072476.setHeight(ID_519072476_alto);

/** 
* Ocultamos _info 
*/
var ID_1015068704_visible = false;

										  if (ID_1015068704_visible=='si') {
											  ID_1015068704_visible=true;
										  } else if (ID_1015068704_visible=='no') {
											  ID_1015068704_visible=false;
										  }
										  $.ID_1015068704.setVisible(ID_1015068704_visible);

var ID_1015068704_alto = 0;

								  if (ID_1015068704_alto=='*') {
									  ID_1015068704_alto=Ti.UI.FILL;
								  } else if (ID_1015068704_alto=='-') {
									  ID_1015068704_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1015068704_alto)) {
									  ID_1015068704_alto=ID_1015068704_alto+'dp';
								  }
								  $.ID_1015068704.setHeight(ID_1015068704_alto);

/** 
* Dejamos _sorry como estaba 
*/
var ID_1845899524_visible = visible;

										  if (ID_1845899524_visible=='si') {
											  ID_1845899524_visible=true;
										  } else if (ID_1845899524_visible=='no') {
											  ID_1845899524_visible=false;
										  }
										  $.ID_1845899524.setVisible(ID_1845899524_visible);

var ID_1845899524_alto = '-';

								  if (ID_1845899524_alto=='*') {
									  ID_1845899524_alto=Ti.UI.FILL;
								  } else if (ID_1845899524_alto=='-') {
									  ID_1845899524_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1845899524_alto)) {
									  ID_1845899524_alto=ID_1845899524_alto+'dp';
								  }
								  $.ID_1845899524.setHeight(ID_1845899524_alto);

} else if (tipo=='_info') {
/** 
* Ocultamos _tip 
*/
var ID_519072476_visible = false;

										  if (ID_519072476_visible=='si') {
											  ID_519072476_visible=true;
										  } else if (ID_519072476_visible=='no') {
											  ID_519072476_visible=false;
										  }
										  $.ID_519072476.setVisible(ID_519072476_visible);

var ID_519072476_alto = 0;

								  if (ID_519072476_alto=='*') {
									  ID_519072476_alto=Ti.UI.FILL;
								  } else if (ID_519072476_alto=='-') {
									  ID_519072476_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_519072476_alto)) {
									  ID_519072476_alto=ID_519072476_alto+'dp';
								  }
								  $.ID_519072476.setHeight(ID_519072476_alto);

/** 
* Ocultamos _sorry 
*/
var ID_1845899524_visible = false;

										  if (ID_1845899524_visible=='si') {
											  ID_1845899524_visible=true;
										  } else if (ID_1845899524_visible=='no') {
											  ID_1845899524_visible=false;
										  }
										  $.ID_1845899524.setVisible(ID_1845899524_visible);

var ID_1845899524_alto = 0;

								  if (ID_1845899524_alto=='*') {
									  ID_1845899524_alto=Ti.UI.FILL;
								  } else if (ID_1845899524_alto=='-') {
									  ID_1845899524_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1845899524_alto)) {
									  ID_1845899524_alto=ID_1845899524_alto+'dp';
								  }
								  $.ID_1845899524.setHeight(ID_1845899524_alto);

/** 
* Dejamos _info como estaba 
*/
var ID_1015068704_visible = visible;

										  if (ID_1015068704_visible=='si') {
											  ID_1015068704_visible=true;
										  } else if (ID_1015068704_visible=='no') {
											  ID_1015068704_visible=false;
										  }
										  $.ID_1015068704.setVisible(ID_1015068704_visible);

var ID_1015068704_alto = '-';

								  if (ID_1015068704_alto=='*') {
									  ID_1015068704_alto=Ti.UI.FILL;
								  } else if (ID_1015068704_alto=='-') {
									  ID_1015068704_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1015068704_alto)) {
									  ID_1015068704_alto=ID_1015068704_alto+'dp';
								  }
								  $.ID_1015068704.setHeight(ID_1015068704_alto);

}}
 else {
if (tipo=='_tip') {
/** 
* Dependiendo del tipo de mono a mostrar, ocultamos todas excepto la que necesitamos mostrar 
*/
var ID_519072476_visible = visible;

										  if (ID_519072476_visible=='si') {
											  ID_519072476_visible=true;
										  } else if (ID_519072476_visible=='no') {
											  ID_519072476_visible=false;
										  }
										  $.ID_519072476.setVisible(ID_519072476_visible);

var ID_519072476_alto = 120;

								  if (ID_519072476_alto=='*') {
									  ID_519072476_alto=Ti.UI.FILL;
								  } else if (ID_519072476_alto=='-') {
									  ID_519072476_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_519072476_alto)) {
									  ID_519072476_alto=ID_519072476_alto+'dp';
								  }
								  $.ID_519072476.setHeight(ID_519072476_alto);

var ID_1845899524_visible = false;

										  if (ID_1845899524_visible=='si') {
											  ID_1845899524_visible=true;
										  } else if (ID_1845899524_visible=='no') {
											  ID_1845899524_visible=false;
										  }
										  $.ID_1845899524.setVisible(ID_1845899524_visible);

var ID_1845899524_alto = 0;

								  if (ID_1845899524_alto=='*') {
									  ID_1845899524_alto=Ti.UI.FILL;
								  } else if (ID_1845899524_alto=='-') {
									  ID_1845899524_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1845899524_alto)) {
									  ID_1845899524_alto=ID_1845899524_alto+'dp';
								  }
								  $.ID_1845899524.setHeight(ID_1845899524_alto);

var ID_1015068704_visible = false;

										  if (ID_1015068704_visible=='si') {
											  ID_1015068704_visible=true;
										  } else if (ID_1015068704_visible=='no') {
											  ID_1015068704_visible=false;
										  }
										  $.ID_1015068704.setVisible(ID_1015068704_visible);

var ID_1015068704_alto = 0;

								  if (ID_1015068704_alto=='*') {
									  ID_1015068704_alto=Ti.UI.FILL;
								  } else if (ID_1015068704_alto=='-') {
									  ID_1015068704_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1015068704_alto)) {
									  ID_1015068704_alto=ID_1015068704_alto+'dp';
								  }
								  $.ID_1015068704.setHeight(ID_1015068704_alto);

}
 else if (tipo=='_sorry') {
/** 
* Ocultamos _tip 
*/
var ID_519072476_visible = false;

										  if (ID_519072476_visible=='si') {
											  ID_519072476_visible=true;
										  } else if (ID_519072476_visible=='no') {
											  ID_519072476_visible=false;
										  }
										  $.ID_519072476.setVisible(ID_519072476_visible);

var ID_519072476_alto = 0;

								  if (ID_519072476_alto=='*') {
									  ID_519072476_alto=Ti.UI.FILL;
								  } else if (ID_519072476_alto=='-') {
									  ID_519072476_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_519072476_alto)) {
									  ID_519072476_alto=ID_519072476_alto+'dp';
								  }
								  $.ID_519072476.setHeight(ID_519072476_alto);

/** 
* Ocultamos _info 
*/
var ID_1015068704_visible = false;

										  if (ID_1015068704_visible=='si') {
											  ID_1015068704_visible=true;
										  } else if (ID_1015068704_visible=='no') {
											  ID_1015068704_visible=false;
										  }
										  $.ID_1015068704.setVisible(ID_1015068704_visible);

var ID_1015068704_alto = 0;

								  if (ID_1015068704_alto=='*') {
									  ID_1015068704_alto=Ti.UI.FILL;
								  } else if (ID_1015068704_alto=='-') {
									  ID_1015068704_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1015068704_alto)) {
									  ID_1015068704_alto=ID_1015068704_alto+'dp';
								  }
								  $.ID_1015068704.setHeight(ID_1015068704_alto);

/** 
* Dejamos _sorry como estaba 
*/
var ID_1845899524_visible = visible;

										  if (ID_1845899524_visible=='si') {
											  ID_1845899524_visible=true;
										  } else if (ID_1845899524_visible=='no') {
											  ID_1845899524_visible=false;
										  }
										  $.ID_1845899524.setVisible(ID_1845899524_visible);

var ID_1845899524_alto = '-';

								  if (ID_1845899524_alto=='*') {
									  ID_1845899524_alto=Ti.UI.FILL;
								  } else if (ID_1845899524_alto=='-') {
									  ID_1845899524_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1845899524_alto)) {
									  ID_1845899524_alto=ID_1845899524_alto+'dp';
								  }
								  $.ID_1845899524.setHeight(ID_1845899524_alto);

} else if (tipo=='_info') {
/** 
* Ocultamos _tip 
*/
var ID_519072476_visible = false;

										  if (ID_519072476_visible=='si') {
											  ID_519072476_visible=true;
										  } else if (ID_519072476_visible=='no') {
											  ID_519072476_visible=false;
										  }
										  $.ID_519072476.setVisible(ID_519072476_visible);

var ID_519072476_alto = 0;

								  if (ID_519072476_alto=='*') {
									  ID_519072476_alto=Ti.UI.FILL;
								  } else if (ID_519072476_alto=='-') {
									  ID_519072476_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_519072476_alto)) {
									  ID_519072476_alto=ID_519072476_alto+'dp';
								  }
								  $.ID_519072476.setHeight(ID_519072476_alto);

/** 
* Ocultamos _sorry 
*/
var ID_1845899524_visible = false;

										  if (ID_1845899524_visible=='si') {
											  ID_1845899524_visible=true;
										  } else if (ID_1845899524_visible=='no') {
											  ID_1845899524_visible=false;
										  }
										  $.ID_1845899524.setVisible(ID_1845899524_visible);

var ID_1845899524_alto = 0;

								  if (ID_1845899524_alto=='*') {
									  ID_1845899524_alto=Ti.UI.FILL;
								  } else if (ID_1845899524_alto=='-') {
									  ID_1845899524_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1845899524_alto)) {
									  ID_1845899524_alto=ID_1845899524_alto+'dp';
								  }
								  $.ID_1845899524.setHeight(ID_1845899524_alto);

/** 
* Dejamos _info como estaba 
*/
var ID_1015068704_visible = visible;

										  if (ID_1015068704_visible=='si') {
											  ID_1015068704_visible=true;
										  } else if (ID_1015068704_visible=='no') {
											  ID_1015068704_visible=false;
										  }
										  $.ID_1015068704.setVisible(ID_1015068704_visible);

var ID_1015068704_alto = '-';

								  if (ID_1015068704_alto=='*') {
									  ID_1015068704_alto=Ti.UI.FILL;
								  } else if (ID_1015068704_alto=='-') {
									  ID_1015068704_alto=Ti.UI.SIZE;
								  } else if (!isNaN(ID_1015068704_alto)) {
									  ID_1015068704_alto=ID_1015068704_alto+'dp';
								  }
								  $.ID_1015068704.setHeight(ID_1015068704_alto);

}}};