/** 
* Widget Barra Inspeccion
* La funcion de este widget es simular un barra tipo header.
* Posee imagenes/textos que son utilizados en el proyecto principal.
* 
* @param info Muestra una vista a la derecha con icono de informacion
* @param titulo Define el titulo de la pantalla
* @param fondo Define el color de fondo de la barra (Definido segun los estilos de este widget)
* @param modal Define la posicion del titulo en la barra
* @param nroinicial En el caso de que sea una secuencia de pantallas, permite definir el numero en la que esta actualmente
* @param nrofinal En el caso de que sea una secuencia de pantallas, permite definir el numero de pantallas que tiene esa secuencia
* @param colortitulo Define el color del texto del titulo (Por defecto es blanco), la otra opcion es negro, al utilizar esta opcion se tiene que escribir literalmente la palabra negro
* @param textoderecha Define el texto que se mostrara en la parte derecha de la barra
* @param colortextoderecha Se define el color del texto que se mostrara en la parte derecha de la barra, las opciones disponibles son blanco, negro, azul
* @param salir_insp Muestra una vista a la izquierda con icono de salir
* @param continuar Muestra una vista a la derecha con icono color plomo de continuar
* @param continuarazul Muestra una vista a la derecha con icono color azul de continuar
* @param agregar_dano Muestra una vista a la derecha con icono de agregar dano
* @param enviar_insp Muestra una vista a la derecha con texto de enviar todas (pantalla historial)
* @param cerrar Muestra una vista a la izquierda con icono de cerrar 
*/
var _bind4section={};

var args = arguments[0] || {};

function Touchstart_ID_975751920(e) {

e.cancelBubble=true;
var elemento=e.source;
$.ID_196420590.setColor('#cccccc');


}
function Touchend_ID_1002516663(e) {

e.cancelBubble=true;
var elemento=e.source;

if ('__args' in args) {
	args['__args'].onsalirinsp({});
} else {
	args.onsalirinsp({});
}
if (Ti.App.deployType != 'production') console.log('salir insp widget',{});
$.ID_196420590.setColor('#ffffff');


}
function Touchstart_ID_1985011874(e) {

e.cancelBubble=true;
var elemento=e.source;
$.ID_1226075105.setColor('#cccccc');


}
function Touchend_ID_249861399(e) {

e.cancelBubble=true;
var elemento=e.source;

if ('__args' in args) {
	args['__args'].oncerrar({});
} else {
	args.oncerrar({});
}
if (Ti.App.deployType != 'production') console.log('salir insp widget',{});
$.ID_1226075105.setColor('#ffffff');


}
function Touchstart_ID_1872634646(e) {

e.cancelBubble=true;
var elemento=e.source;

}
function Touchend_ID_1731286178(e) {

e.cancelBubble=true;
var elemento=e.source;

if ('__args' in args) {
	args['__args'].onpresiono({});
} else {
	args.onpresiono({});
}

}
function Touchstart_ID_560202555(e) {

e.cancelBubble=true;
var elemento=e.source;
$.ID_729204441.setColor('#cccccc');


}
function Touchend_ID_1882380135(e) {

e.cancelBubble=true;
var elemento=e.source;

if ('__args' in args) {
	args['__args'].onpresiono({});
} else {
	args.onpresiono({});
}
$.ID_729204441.setColor('#ffffff');


}
function Touchstart_ID_452773199(e) {

e.cancelBubble=true;
var elemento=e.source;
$.ID_1313168682.setColor('#cccccc');


}
function Touchend_ID_240696293(e) {

e.cancelBubble=true;
var elemento=e.source;

if ('__args' in args) {
	args['__args'].onpresiono({});
} else {
	args.onpresiono({});
}
$.ID_1313168682.setColor('#2d9edb');


}
function Touchstart_ID_1556768012(e) {

e.cancelBubble=true;
var elemento=e.source;
$.ID_572758682.setColor('#cccccc');


}
function Touchend_ID_423933733(e) {

e.cancelBubble=true;
var elemento=e.source;

if ('__args' in args) {
	args['__args'].onagregar_dano({});
} else {
	args.onagregar_dano({});
}
$.ID_572758682.setColor('#ffffff');


}
function Touchstart_ID_511692959(e) {

e.cancelBubble=true;
var elemento=e.source;
$.ID_196420590.setColor('#2dc8ff');


}
function Touchend_ID_296717895(e) {

e.cancelBubble=true;
var elemento=e.source;

if ('__args' in args) {
	args['__args'].onpresiono({});
} else {
	args.onpresiono({});
}
$.ID_1283451065.setColor('#2d9edb');


}
function Click_ID_1789796281(e) {

e.cancelBubble=true;
var elemento=e.source;

if ('__args' in args) {
	args['__args'].onpresiono({});
} else {
	args.onpresiono({});
}

}
/** 
* Funcion que inicializa el widget segun sus parametros 
*/

$.init = function(params) {
   for (var tobe in params) args[tobe] = params[tobe];
   if ('__id' in params) require(WPATH('vars'))[params.__id]={};
if (!_.isUndefined(params.info)) {
var ID_1637555995_visible = true;

										  if (ID_1637555995_visible=='si') {
											  ID_1637555995_visible=true;
										  } else if (ID_1637555995_visible=='no') {
											  ID_1637555995_visible=false;
										  }
										  $.ID_1637555995.setVisible(ID_1637555995_visible);

var ID_1009538314_visible = false;

										  if (ID_1009538314_visible=='si') {
											  ID_1009538314_visible=true;
										  } else if (ID_1009538314_visible=='no') {
											  ID_1009538314_visible=false;
										  }
										  $.ID_1009538314.setVisible(ID_1009538314_visible);

}
if (!_.isUndefined(params.titulo)) {
$.ID_1822725602.setText(params.titulo);

}
if (!_.isUndefined(params.fondo)) {
var ID_1345248954_estilo = params.fondo;

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_1345248954.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_1345248954_estilo);

}
if (!_.isUndefined(params.modal)) {
$.ID_1822725602.setLeft(20);

}
if (!_.isUndefined(params.nroinicial)) {
$.ID_1631729973.setText(params.nroinicial);

var ID_569043732_visible = true;

										  if (ID_569043732_visible=='si') {
											  ID_569043732_visible=true;
										  } else if (ID_569043732_visible=='no') {
											  ID_569043732_visible=false;
										  }
										  $.ID_569043732.setVisible(ID_569043732_visible);

}
if (!_.isUndefined(params.nrofinal)) {
$.ID_1484477318.setText(params.nrofinal);

var ID_569043732_visible = true;

										  if (ID_569043732_visible=='si') {
											  ID_569043732_visible=true;
										  } else if (ID_569043732_visible=='no') {
											  ID_569043732_visible=false;
										  }
										  $.ID_569043732.setVisible(ID_569043732_visible);

}
if (!_.isUndefined(params.colortitulo)) {
if (params.colortitulo=='negro') {
$.ID_1822725602.setColor('#000000');

}
}
if (!_.isUndefined(params.textoderecha)) {
var ID_1009538314_visible = true;

										  if (ID_1009538314_visible=='si') {
											  ID_1009538314_visible=true;
										  } else if (ID_1009538314_visible=='no') {
											  ID_1009538314_visible=false;
										  }
										  $.ID_1009538314.setVisible(ID_1009538314_visible);

$.ID_412646308.setText(params.textoderecha);

var ID_1637555995_visible = false;

										  if (ID_1637555995_visible=='si') {
											  ID_1637555995_visible=true;
										  } else if (ID_1637555995_visible=='no') {
											  ID_1637555995_visible=false;
										  }
										  $.ID_1637555995.setVisible(ID_1637555995_visible);

}
if (!_.isUndefined(params.colortextoderecha)) {
if (params.colortextoderecha=='blanco') {
$.ID_412646308.setColor('#ffffff');

}
 else if (params.colortextoderecha=='negro') {
$.ID_412646308.setColor('#000000');

} else if (params.colortextoderecha=='azul') {
$.ID_412646308.setColor('#2d9edb');

}}
if (!_.isUndefined(params.salir_insp)) {
var ID_877457277_visible = true;

										  if (ID_877457277_visible=='si') {
											  ID_877457277_visible=true;
										  } else if (ID_877457277_visible=='no') {
											  ID_877457277_visible=false;
										  }
										  $.ID_877457277.setVisible(ID_877457277_visible);

}
if (!_.isUndefined(params.continuar)) {
var ID_803181788_visible = true;

										  if (ID_803181788_visible=='si') {
											  ID_803181788_visible=true;
										  } else if (ID_803181788_visible=='no') {
											  ID_803181788_visible=false;
										  }
										  $.ID_803181788.setVisible(ID_803181788_visible);

}
if (!_.isUndefined(params.continuarazul)) {
var ID_1786805562_visible = true;

										  if (ID_1786805562_visible=='si') {
											  ID_1786805562_visible=true;
										  } else if (ID_1786805562_visible=='no') {
											  ID_1786805562_visible=false;
										  }
										  $.ID_1786805562.setVisible(ID_1786805562_visible);

}
if (!_.isUndefined(params.agregar_dano)) {
var ID_685785058_visible = true;

										  if (ID_685785058_visible=='si') {
											  ID_685785058_visible=true;
										  } else if (ID_685785058_visible=='no') {
											  ID_685785058_visible=false;
										  }
										  $.ID_685785058.setVisible(ID_685785058_visible);

}
if (!_.isUndefined(params.enviar_insp)) {
var ID_1193889870_visible = true;

										  if (ID_1193889870_visible=='si') {
											  ID_1193889870_visible=true;
										  } else if (ID_1193889870_visible=='no') {
											  ID_1193889870_visible=false;
										  }
										  $.ID_1193889870.setVisible(ID_1193889870_visible);

}
if (!_.isUndefined(params.cerrar)) {
var ID_1474250356_visible = true;

										  if (ID_1474250356_visible=='si') {
											  ID_1474250356_visible=true;
										  } else if (ID_1474250356_visible=='no') {
											  ID_1474250356_visible=false;
										  }
										  $.ID_1474250356.setVisible(ID_1474250356_visible);

}
};

/** 
* Funcion update
* 
* @param titulo Define el nuevo titulo de la pantalla
* @param fondo Define el nuevo color de fondo de la barra (definido segun los estilos de este widget)
* @param texto Define el texto en la parte derecha de la barra 
*/

$.update = function(params) {
if (!_.isUndefined(params.titulo)) {
/** 
* En el caso que se tenga que modificar el titulo de la pantalla una vez ya definido, esto permite reeditar ese cambio 
*/
$.ID_1822725602.setText(params.titulo);

}
if (!_.isUndefined(params.fondo)) {
/** 
* En el caso que se tenga que modificar el color de la barra de la pantalla una vez ya definido, esto permite reeditar ese cambio 
*/
var ID_1345248954_estilo = params.fondo;

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_1345248954.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_1345248954_estilo);

}
if (!_.isUndefined(params.texto)) {
/** 
* En el caso que se tenga que modificar el texto del boton de la barra de la pantalla una vez ya definido, esto permite reeditar ese cambio 
*/
$.ID_412646308.setText(params.texto);

}
};

/** 
* Funcion que muestra una vista a la derecha con texto de 'enviar todas' (pantalla historial) 
*/

$.mostrar_envio = function(params) {
/** 
* Al recibir evento solo mostramos la vista de enviar todos los pendientes en la pantalla historial 
*/
var ID_1193889870_visible = true;

										  if (ID_1193889870_visible=='si') {
											  ID_1193889870_visible=true;
										  } else if (ID_1193889870_visible=='no') {
											  ID_1193889870_visible=false;
										  }
										  $.ID_1193889870.setVisible(ID_1193889870_visible);

};

/** 
* Funcion que oculta la vista a la derecha con texto de 'enviar todas' (pantalla historial) 
*/

$.esconder_envio = function(params) {
/** 
* Al recibir evento solo escondemos la vista de enviar todos los pendientes en la pantalla historial 
*/
var ID_1193889870_visible = false;

										  if (ID_1193889870_visible=='si') {
											  ID_1193889870_visible=true;
										  } else if (ID_1193889870_visible=='no') {
											  ID_1193889870_visible=false;
										  }
										  $.ID_1193889870.setVisible(ID_1193889870_visible);

};