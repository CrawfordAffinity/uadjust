var _bind4section={};

var args = arguments[0] || {};

function Touchstart_ID_1950876699(e) {

e.cancelBubble=true;
var elemento=e.source;
var color = ('color' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['color']:'';
if (color=='amarillo') {
var ID_281086116_estilo = 'fd_amarillo_on';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_281086116.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_281086116_estilo);

$.ID_281086116.setBorderColor('#FCBD83');

}
 else if (color=='rojo') {
var ID_281086116_estilo = 'fd_rojo_on';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_281086116.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_281086116_estilo);

$.ID_281086116.setBorderColor('#FCBD83');

} else if (color=='verde') {
var ID_281086116_estilo = 'fd_verde_on';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_281086116.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_281086116_estilo);

$.ID_281086116.setBorderColor('#8BC9E8');

} else if (color=='lila') {
var ID_281086116_estilo = 'fd_lila_on';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_281086116.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_281086116_estilo);

$.ID_281086116.setBorderColor('#B9AAF3');

} else if (color=='naranjo') {
var ID_281086116_estilo = 'fd_naranjo_on';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_281086116.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_281086116_estilo);

$.ID_281086116.setBorderColor('#F8DA54');

} else if (color=='morado') {
var ID_281086116_estilo = 'fd_morado_on';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_281086116.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_281086116_estilo);

$.ID_281086116.setBorderColor('#8383DB');

} else if (color=='rosado') {
var ID_281086116_estilo = 'fd_rosado_on';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_281086116.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_281086116_estilo);

$.ID_281086116.setBorderColor('#FCBD83');

} else if (color=='celeste') {
var ID_281086116_estilo = 'fd_celeste_on';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_281086116.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_281086116_estilo);

$.ID_281086116.setBorderColor('#8CE5BD');

} else if (color=='cafe') {
var ID_281086116_estilo = 'fd_cafe_on';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_281086116.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_281086116_estilo);

$.ID_281086116.setBorderColor('#FFACAA');

}
}
function Touchend_ID_1022085204(e) {

e.cancelBubble=true;
var elemento=e.source;
/* Segun el color que se haya definido al iniciar el widget creamos la sensacion de clickeo para cambio de color al ser presionado y soltado */
var color = ('color' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['color']:'';
if (color=='amarillo') {
var ID_281086116_estilo = 'fd_amarillo';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_281086116.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_281086116_estilo);

$.ID_281086116.setBorderColor('#F8DA54');

}
 else if (color=='rojo') {
var ID_281086116_estilo = 'fd_rojo';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_281086116.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_281086116_estilo);

$.ID_281086116.setBorderColor('#EE7F7E');

} else if (color=='verde') {
var ID_281086116_estilo = 'fd_verde';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_281086116.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_281086116_estilo);

$.ID_281086116.setBorderColor('#8CE5BD');

} else if (color=='lila') {
var ID_281086116_estilo = 'fd_lila';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_281086116.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_281086116_estilo);

$.ID_281086116.setBorderColor('#8383DB');

} else if (color=='naranjo') {
var ID_281086116_estilo = 'fd_naranjo';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_281086116.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_281086116_estilo);

$.ID_281086116.setBorderColor('#FCBD83');

} else if (color=='morado') {
var ID_281086116_estilo = 'fd_morado';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_281086116.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_281086116_estilo);

$.ID_281086116.setBorderColor('#B9AAF3');

} else if (color=='rosado') {
var ID_281086116_estilo = 'fd_rosado';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_281086116.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_281086116_estilo);

$.ID_281086116.setBorderColor('#FFACAA');

} else if (color=='celeste') {
var ID_281086116_estilo = 'fd_celeste';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_281086116.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_281086116_estilo);

$.ID_281086116.setBorderColor('#8BC9E8');

} else if (color=='cafe') {
var ID_281086116_estilo = 'fd_cafe';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_281086116.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_281086116_estilo);

$.ID_281086116.setBorderColor('#A5876D');

}
if ('__args' in args) {
	args['__args'].onclick({});
} else {
	args.onclick({});
}

}

$.init = function(params) {
   for (var tobe in params) args[tobe] = params[tobe];
   if ('__id' in params) require(WPATH('vars'))[params.__id]={};
/* Seteamos por defecto el color amarillo */
require(WPATH('vars'))[args.__id]['color']='amarillo';
if (!_.isUndefined(params.titulo)) {
/* Revisamos si reciben parametros para modificar valores de texto, color */
$.ID_675293245.setText(params.titulo);

}
if (!_.isUndefined(params.color)) {
require(WPATH('vars'))[args.__id]['color']=params.color;
}
if (!_.isUndefined(params.icono)) {
require(WPATH('vars'))[args.__id]['icono']=params.icono;
var ID_1525222491_icono = params.icono;

				  if ('fontello' in require(WPATH('a4w')) && 'adjust' in require(WPATH('a4w')).fontello && ID_1525222491_icono in require(WPATH('a4w')).fontello['adjust'].CODES) {
				  		ID_1525222491_icono = require(WPATH('a4w')).fontello['adjust'].CODES[ID_1525222491_icono];
				  } else {
				  		console.log('live/modificar -> error setting new svg icon alias');
				  }
				  $.ID_1525222491.setText(ID_1525222491_icono);

var ID_1525222491_visible = true;

										  if (ID_1525222491_visible=='si') {
											  ID_1525222491_visible=true;
										  } else if (ID_1525222491_visible=='no') {
											  ID_1525222491_visible=false;
										  }
										  $.ID_1525222491.setVisible(ID_1525222491_visible);

}
/* Recuperamos variable de color en caso de que por parametro haya cambiado y modificamos el fondo de la vista */
var color = ('color' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['color']:'';
if (color=='amarillo') {
var ID_281086116_estilo = 'fd_amarillo';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_281086116.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_281086116_estilo);

}
 else if (color=='rojo') {
var ID_281086116_estilo = 'fd_rojo';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_281086116.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_281086116_estilo);

} else if (color=='verde') {
var ID_281086116_estilo = 'fd_verde';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_281086116.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_281086116_estilo);

} else if (color=='lila') {
var ID_281086116_estilo = 'fd_lila';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_281086116.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_281086116_estilo);

} else if (color=='naranjo') {
var ID_281086116_estilo = 'fd_naranjo';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_281086116.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_281086116_estilo);

} else if (color=='morado') {
var ID_281086116_estilo = 'fd_morado';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_281086116.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_281086116_estilo);

} else if (color=='rosado') {
var ID_281086116_estilo = 'fd_rosado';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_281086116.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_281086116_estilo);

} else if (color=='celeste') {
var ID_281086116_estilo = 'fd_celeste';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_281086116.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_281086116_estilo);

} else if (color=='cafe') {
var ID_281086116_estilo = 'fd_cafe';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_281086116.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_281086116_estilo);

}};


$.iniciar_progreso = function(params) {
/* oculta el bot&#243;n continuar y muestra la animaci&#243;n de progreso. */
var ID_235307810_visible = false;

										  if (ID_235307810_visible=='si') {
											  ID_235307810_visible=true;
										  } else if (ID_235307810_visible=='no') {
											  ID_235307810_visible=false;
										  }
										  $.ID_235307810.setVisible(ID_235307810_visible);

$.ID_1972029426.show();
};


$.detener_progreso = function(params) {
var ID_235307810_visible = true;

										  if (ID_235307810_visible=='si') {
											  ID_235307810_visible=true;
										  } else if (ID_235307810_visible=='no') {
											  ID_235307810_visible=false;
										  }
										  $.ID_235307810.setVisible(ID_235307810_visible);

$.ID_1972029426.hide();
};