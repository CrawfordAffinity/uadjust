var _bind4section={};

var args = arguments[0] || {};

function Click_ID_556842754(e) {

e.cancelBubble=true;
var elemento=e.source;
var estado = ('estado' in require(WPATH('vars'))[args.__id])?require(WPATH('vars'))[args.__id]['estado']:'';

if ('__args' in args) {
	args['__args'].onpresiono({estado : estado});
} else {
	args.onpresiono({estado : estado});
}

}

$.init = function(params) {
   for (var tobe in params) args[tobe] = params[tobe];
   if ('__id' in params) require(WPATH('vars'))[params.__id]={};
if (!_.isUndefined(params.texto)) {
/* Inicializamos el widget y ponemos los datos recibidos desde la pantalla que lo utiliza */
$.ID_690999687.setText(params.texto);

}
if (!_.isUndefined(params.verprogreso)) {
if (params.verprogreso==true||params.verprogreso=='true') {
}
 else if (params.verprogreso==false||params.verprogreso=='false') {
}}
if (!_.isUndefined(params.vertexto)) {
var ID_690999687_visible = params.vertexto;

								  if (ID_690999687_visible=='si') {
									  ID_690999687_visible=true;
								  } else if (ID_690999687_visible=='no') {
									  ID_690999687_visible=false;
								  }
								  $.ID_690999687.setVisible(ID_690999687_visible);

}
if (!_.isUndefined(params.estilo)) {
var ID_1955206092_estilo = params.estilo;

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_1955206092.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_1955206092_estilo);

}
require(WPATH('vars'))[args.__id]['estado']='0';
};


$.datos = function(params) {
if (!_.isUndefined(params.texto)) {
/* Revisamos los parametros recibidos y modificamos texto, vista */
$.ID_690999687.setText(params.texto);

}
if (!_.isUndefined(params.verprogreso)) {
if (params.verprogreso==true||params.verprogreso=='true') {
}
 else if (params.verprogreso==false||params.verprogreso=='false') {
}}
if (!_.isUndefined(params.vertexto)) {
var ID_690999687_visible = params.vertexto;

								  if (ID_690999687_visible=='si') {
									  ID_690999687_visible=true;
								  } else if (ID_690999687_visible=='no') {
									  ID_690999687_visible=false;
								  }
								  $.ID_690999687.setVisible(ID_690999687_visible);

}
if (!_.isUndefined(params.estilo)) {
var ID_1955206092_estilo = params.estilo;

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_1955206092.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_1955206092_estilo);

if (Ti.App.deployType != 'production') console.log('color de estilo en widget boton',{"datos":params.estilo});
if (params.estilo=='fondoverde') {
if (Ti.App.deployType != 'production') console.log('cambiado a fondover',{});
$.ID_1955206092.setBorderColor('#8CE5BD');

}
 else if (params.estilo=='fondorojo') {
if (Ti.App.deployType != 'production') console.log('cambiado a fondoroj',{});
$.ID_1955206092.setBorderColor('#EE7F7E');

} else if (params.estilo=='fondoamarillo') {
if (Ti.App.deployType != 'production') console.log('cambiado a fondoamari',{});
$.ID_1955206092.setBorderColor('#F8DA54');

}}
require(WPATH('vars'))[args.__id]['estado']=params.estado;
};


$.iniciar_progreso = function(params) {
/* oculta el bot&#243;n continuar y muestra la animaci&#243;n de progreso. */
var ID_690999687_visible = false;

								  if (ID_690999687_visible=='si') {
									  ID_690999687_visible=true;
								  } else if (ID_690999687_visible=='no') {
									  ID_690999687_visible=false;
								  }
								  $.ID_690999687.setVisible(ID_690999687_visible);

$.ID_639460215.show();
};


$.detener_progreso = function(params) {
/* Muestra el bot&#243;n continuar y oculta la animaci&#243;n de progreso. */
var ID_690999687_visible = true;

								  if (ID_690999687_visible=='si') {
									  ID_690999687_visible=true;
								  } else if (ID_690999687_visible=='no') {
									  ID_690999687_visible=false;
								  }
								  $.ID_690999687.setVisible(ID_690999687_visible);

$.ID_639460215.hide();
};