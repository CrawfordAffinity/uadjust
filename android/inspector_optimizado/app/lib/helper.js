var llaves={}, time_mess = {};
var helper = {};

helper.console_time = function(thename) {
    time_mess[thename] = new Date().getTime();
};
helper.console_timeEnd = function(thename) {
    if (thename in time_mess) {
        var inicio = time_mess[thename];
        var pasado = new Date().getTime() - inicio;
        console.log('MethodTimer[' + thename + '] Elaspsed time: ' + pasado + 'ms.');
        return pasado;
    }
    return -1;
};
helper.arr_diff = function(a1, a2) {
    var a = [],
        diff = [];
    for (var i = 0; i < a1.length; i++)
        a[a1[i]] = true;
    for (var i = 0; i < a2.length; i++)
        if (a[a2[i]]) delete a[a2[i]];
        else a[a2[i]] = true;
    for (var k in a)
        diff.push(k);
    return diff;
};
helper.list_diff = function(n1, n2) {
    var a = [],
        diff = [];
    var a1 = n1.split(',');
    var a2 = n2.split(',');
    for (var i = 0; i < a1.length; i++)
        a[a1[i]] = true;
    for (var i = 0; i < a2.length; i++)
        if (a[a2[i]]) delete a[a2[i]];
        else a[a2[i]] = true;
    for (var k in a)
        diff.push(k);
    return diff.join(',');
};
helper.arr_contains = function(a, obj) {
    var i = a.length;
    while (i--) {
        if (a[i] === obj) {
            return true;
        }
    }
    return false;
};
helper.ajaxReset = function() {
    llaves={};
};
helper.ajaxUnico = function(id, url, method, data, timeout, events) {
    if (!id in llaves) {
        llaves[id] = null;
    } else {
        try {
            if (llaves[id] && llaves[id].readyState != 4) {
                if ('abort' in events) {
                    if (events.abort(id)==true) {
                        llaves[id].abort();
                    }
                } else {
                    llaves[id].abort();
                }
            }
        } catch (e) {}
    }
    llaves[id] = Ti.Network.createHTTPClient();
    if ('success' in events) {
        llaves[id].onload = function(e) {
            var proc = this.responseText;
            try {
                proc = JSON.parse(this.responseText);
            } catch (ee) {}
            delete llaves[id];
            events.success(proc);
        };
    }
    if ('error' in events) {
        llaves[id].onerror = function(e) {
            //console.log('DEBUG:ajaxUnico:ERROR:',e);
            delete llaves[id];
            events.error(e.error);
        }
    }
    if ('progress' in events) {
        llaves[id].ondatastream = function(e) {
            events.progress(e);
        }
    }
    //
    llaves[id].timeout = timeout;
    if (method.toLowerCase()=='postjson') {
        llaves[id].open('POST', url);
        llaves[id].setRequestHeader('Content-Type','application/json');
        llaves[id].send(JSON.stringify(data));
    } else if (method.toLowerCase()=='posturl') {
        llaves[id].open('POST', url);
        llaves[id].setRequestHeader('Content-Type','application/x-www-form-urlencoded');
        llaves[id].send(JSON.stringify(data));
    } else {
        llaves[id].open(method.toUpperCase(), url);
        llaves[id].send(data);
    } 
    //llaves[id].setRequestHeader('X-Requested-By': 'Creador OPEN App');
};

helper.query2array=function(query) {
    var x=[];
    var tmp;
    var struct={};
    for (var reg=0;reg<query.models.length;reg++) {
        struct={};
        for (var key in query.models[reg].attributes) {
            struct[key]=query.models[reg].attributes[key];
        }
        x.push(struct);
    }
    return x;
};
helper.model2object=function(model) {
    var x=[];
    var tmp;
    var struct={};
    for (var key in model.attributes) {
        struct[key]=model.attributes[key];
    }
    return struct;
};
module.exports = helper;