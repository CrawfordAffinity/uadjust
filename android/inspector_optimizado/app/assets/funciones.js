var funciones = {}
funciones.formatear_fecha = function(x_params) {
    var formato = x_params['formato'];
    var fecha = x_params['fecha'];
    var formato_entrada = x_params['formato_entrada'];
    var moment = require("alloy/moment");
    var fecha_era_texto = (typeof fecha === 'string' || typeof fecha === 'number') ? true : false;
    var nuevo = '';
    if (fecha_era_texto == true || fecha_era_texto == 'true') {
        nuevo = moment(fecha, formato_entrada).format(formato);
    } else {
        nuevo = moment(fecha).format(formato);
    }
    return nuevo;
};
funciones.consumir_cola = function(x_params) {
    var ID_895075836_i = Alloy.createCollection('cola');
    var ID_895075836_i_where = '';
    ID_895075836_i.fetch();
    var lista_enviar = require('helper').query2array(ID_895075836_i);
    if (lista_enviar && lista_enviar.length) {
        if (Ti.App.deployType != 'production') console.log('procesando cola de salida interna', {});
        var item_index = 0;
        _.each(lista_enviar, function(item, item_pos, item_list) {
            item_index += 1;
            var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
            if (item.tipo == 'cancelar') {
                if (Ti.App.deployType != 'production') console.log('Voy a enviar un cancelar tarea', {
                    "item": item
                });
                data = JSON.parse(item.data);
                var ID_470722920 = {};

                ID_470722920.success = function(e) {
                    var elemento = e,
                        valor = e;
                    if (Ti.App.deployType != 'production') console.log('Respuesta de servidor de cancelar tarea', {
                        "elemento": elemento
                    });
                    if (elemento.error == 0 || elemento.error == '0') {
                        var ID_1681296060_i = Alloy.Collections.cola;
                        var sql = 'DELETE FROM ' + ID_1681296060_i.config.adapter.collection_name + ' WHERE id=\'' + elemento.id_app + '\'';
                        var db = Ti.Database.open(ID_1681296060_i.config.adapter.db_name);
                        db.execute(sql);
                        db.close();
                        sql = null;
                        db = null;
                        ID_1681296060_i.trigger('delete');
                    }
                    elemento = null, valor = null;
                };

                ID_470722920.error = function(e) {
                    var elemento = e,
                        valor = e;
                    if (Ti.App.deployType != 'production') console.log('cancelarTarea fallo', {
                        "asd": elemento
                    });
                    elemento = null, valor = null;
                };
                require('helper').ajaxUnico('ID_470722920', '' + url_server + 'cancelarTarea' + '', 'POST', {
                    id_inspector: data.id_inspector,
                    codigo_identificador: data.codigo_identificador,
                    id_tarea: data.id_server,
                    num_caso: data.num_caso,
                    mensaje: data.razon,
                    opcion: 0,
                    tipo: 1,
                    id_app: item.id
                }, 15000, ID_470722920);
            } else if (item.tipo == 'enviar') {
                if (Ti.App.deployType != 'production') console.log('Voy a enviar un finalizar tarea', {
                    "item": item
                });
                data = JSON.parse(item.data);
                var ID_82591610 = {};
                console.log('DEBUG WEB: requesting url:' + url_server + 'finalizarTarea' + ' with JSON data:', {
                    _method: 'POSTJSON',
                    _params: {
                        fotosrequeridas: data.fotosrequeridas,
                        datosbasicos: data.datosbasicos,
                        caracteristicas: data.caracteristicas,
                        niveles: data.niveles,
                        siniestro: data.siniestro,
                        recintos: data.recintos,
                        itemdanos: data.itemdanos,
                        contenido: data.contenido,
                        documentos: data.documentos,
                        inspecciones: data.inspecciones,
                        firma: data.firma,
                        id_app: item.id
                    },
                    _timeout: '15000'
                });

                ID_82591610.success = function(e) {
                    var elemento = e,
                        valor = e;
                    if (Ti.App.deployType != 'production') console.log('Respuesta de servidor de finalizar tarea (cola)', {
                        "elemento": elemento
                    });
                    if (elemento.error == 0 || elemento.error == '0') {
                        var ID_1296191031 = null;
                        if ('enviarfirmas' in require('funciones')) {
                            ID_1296191031 = require('funciones').enviarfirmas({});
                        } else {
                            try {
                                ID_1296191031 = f_enviarfirmas({});
                            } catch (ee) {}
                        }
                        var ID_586512613_i = Alloy.Collections.cola;
                        var sql = 'DELETE FROM ' + ID_586512613_i.config.adapter.collection_name + ' WHERE id=\'' + elemento.id_app + '\'';
                        var db = Ti.Database.open(ID_586512613_i.config.adapter.db_name);
                        db.execute(sql);
                        db.close();
                        sql = null;
                        db = null;
                        ID_586512613_i.trigger('delete');
                        var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
                        var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
                        if ((_.isObject(inspector) || (_.isString(inspector)) && !_.isEmpty(inspector)) || _.isNumber(inspector) || _.isBoolean(inspector)) {
                            var ID_1249703200 = {};
                            console.log('DEBUG WEB: requesting url:' + url_server + 'obtenerHistorialTareas' + ' with data:', {
                                _method: 'POST',
                                _params: {
                                    id_inspector: inspector.id_server
                                },
                                _timeout: '15000'
                            });

                            ID_1249703200.success = function(e) {
                                var elemento = e,
                                    valor = e;
                                var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
                                var ID_1179568065_i = Alloy.Collections.tareas;
                                var sql = 'DELETE FROM ' + ID_1179568065_i.config.adapter.collection_name + ' WHERE id_server=\'' + seltarea.id_server + '\'';
                                var db = Ti.Database.open(ID_1179568065_i.config.adapter.db_name);
                                db.execute(sql);
                                db.close();
                                sql = null;
                                db = null;
                                ID_1179568065_i.trigger('delete');
                                var ID_1020424568_i = Alloy.Collections.historial_tareas;
                                var sql = "DELETE FROM " + ID_1020424568_i.config.adapter.collection_name;
                                var db = Ti.Database.open(ID_1020424568_i.config.adapter.db_name);
                                db.execute(sql);
                                db.close();
                                sql = null;
                                db = null;
                                ID_1020424568_i.trigger('delete');
                                var elemento_historial_tareas = elemento.historial_tareas;
                                var ID_1768984344_m = Alloy.Collections.historial_tareas;
                                var db_ID_1768984344 = Ti.Database.open(ID_1768984344_m.config.adapter.db_name);
                                db_ID_1768984344.execute('BEGIN');
                                _.each(elemento_historial_tareas, function(ID_1768984344_fila, pos) {
                                    db_ID_1768984344.execute('INSERT INTO historial_tareas (fecha_tarea, id_inspeccion, id_asegurado, nivel_2, comentario_can_o_rech, asegurado_tel_fijo, estado_tarea, bono, evento, id_inspector, asegurado_codigo_identificador, lat, nivel_1, asegurado_nombre, pais, direccion, asegurador, fecha_ingreso, fecha_siniestro, nivel_1_texto, distance, fecha_termino, nivel_4, perfil, asegurado_id, id_server, categoria, nivel_3, asegurado_correo, num_caso, lon, asegurado_tel_movil, nivel_5, tipo_tarea, hora_termino) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', ID_1768984344_fila.fecha_tarea, ID_1768984344_fila.id_inspeccion, ID_1768984344_fila.id_asegurado, ID_1768984344_fila.nivel_2, ID_1768984344_fila.comentario_can_o_rech, ID_1768984344_fila.asegurado_tel_fijo, ID_1768984344_fila.estado_tarea, ID_1768984344_fila.bono, ID_1768984344_fila.evento, ID_1768984344_fila.id_inspector, ID_1768984344_fila.asegurado_codigo_identificador, ID_1768984344_fila.lat, ID_1768984344_fila.nivel_1, ID_1768984344_fila.asegurado_nombre, ID_1768984344_fila.pais, ID_1768984344_fila.direccion, ID_1768984344_fila.asegurador, ID_1768984344_fila.fecha_ingreso, ID_1768984344_fila.fecha_siniestro, ID_1768984344_fila.nivel_1_, ID_1768984344_fila.distancia, ID_1768984344_fila.fecha_finalizacion, ID_1768984344_fila.nivel_4, 'ubicacion', ID_1768984344_fila.asegurado_id, ID_1768984344_fila.id, ID_1768984344_fila.categoria, ID_1768984344_fila.nivel_3, ID_1768984344_fila.asegurado_correo, ID_1768984344_fila.num_caso, ID_1768984344_fila.lon, ID_1768984344_fila.asegurado_tel_movil, ID_1768984344_fila.nivel_5, ID_1768984344_fila.tipo_tarea, ID_1768984344_fila.hora_finalizacion);
                                });
                                db_ID_1768984344.execute('COMMIT');
                                db_ID_1768984344.close();
                                db_ID_1768984344 = null;
                                ID_1768984344_m.trigger('change');
                                /** 
                                 * Llamo refresco tareas 
                                 */

                                Alloy.Events.trigger('refrescar_historial');
                                elemento = null, valor = null;
                            };

                            ID_1249703200.error = function(e) {
                                var elemento = e,
                                    valor = e;
                                elemento = null, valor = null;
                            };
                            require('helper').ajaxUnico('ID_1249703200', '' + url_server + 'obtenerHistorialTareas' + '', 'POST', {
                                id_inspector: inspector.id_server
                            }, 15000, ID_1249703200);
                        }
                    }
                    elemento = null, valor = null;
                };

                ID_82591610.error = function(e) {
                    var elemento = e,
                        valor = e;
                    elemento = null, valor = null;
                };
                require('helper').ajaxUnico('ID_82591610', '' + url_server + 'finalizarTarea' + '', 'POSTJSON', {
                    fotosrequeridas: data.fotosrequeridas,
                    datosbasicos: data.datosbasicos,
                    caracteristicas: data.caracteristicas,
                    niveles: data.niveles,
                    siniestro: data.siniestro,
                    recintos: data.recintos,
                    itemdanos: data.itemdanos,
                    contenido: data.contenido,
                    documentos: data.documentos,
                    inspecciones: data.inspecciones,
                    firma: data.firma,
                    id_app: item.id
                }, 15000, ID_82591610);
            } else if (item.tipo == 'confirmar_ruta') {
                if (Ti.App.deployType != 'production') console.log('Voy a confirmar ruta', {
                    "item": item
                });
                data = JSON.parse(item.data);
                var ID_463418903 = {};

                ID_463418903.success = function(e) {
                    var elemento = e,
                        valor = e;
                    if (elemento.error == 0 || elemento.error == '0') {
                        var ID_1050033970_i = Alloy.Collections.cola;
                        var sql = 'DELETE FROM ' + ID_1050033970_i.config.adapter.collection_name + ' WHERE id=\'' + elemento.id_app + '\'';
                        var db = Ti.Database.open(ID_1050033970_i.config.adapter.db_name);
                        db.execute(sql);
                        db.close();
                        sql = null;
                        db = null;
                        ID_1050033970_i.trigger('delete');
                    }
                    elemento = null, valor = null;
                };

                ID_463418903.error = function(e) {
                    var elemento = e,
                        valor = e;
                    if (Ti.App.deployType != 'production') console.log('confirmar ruta fallo', {
                        "asd": elemento
                    });
                    elemento = null, valor = null;
                };
                require('helper').ajaxUnico('ID_463418903', '' + url_server + 'confirmarRuta' + '', 'POST', {
                    id_inspector: data.id_inspector,
                    codigo_identificador: data.codigo_identificador,
                    tareas: data.tareas,
                    id_app: item.id
                }, 15000, ID_463418903);
            } else if (item.tipo == 'iniciar') {
                if (Ti.App.deployType != 'production') console.log('Voy a enviar un iniciar tarea', {
                    "item": item
                });
                data = JSON.parse(item.data);
                var ID_283596564 = {};

                ID_283596564.success = function(e) {
                    var elemento = e,
                        valor = e;
                    if (Ti.App.deployType != 'production') console.log('Respuesta de servidor de iniciar tarea', {
                        "elemento": elemento
                    });
                    if (elemento.error == 0 || elemento.error == '0') {
                        var ID_1768471640_i = Alloy.Collections.cola;
                        var sql = 'DELETE FROM ' + ID_1768471640_i.config.adapter.collection_name + ' WHERE id=\'' + elemento.id_app + '\'';
                        var db = Ti.Database.open(ID_1768471640_i.config.adapter.db_name);
                        db.execute(sql);
                        db.close();
                        sql = null;
                        db = null;
                        ID_1768471640_i.trigger('delete');
                    }
                    elemento = null, valor = null;
                };

                ID_283596564.error = function(e) {
                    var elemento = e,
                        valor = e;
                    if (Ti.App.deployType != 'production') console.log('iniciarTarea fallo', {
                        "asd": elemento
                    });
                    elemento = null, valor = null;
                };
                require('helper').ajaxUnico('ID_283596564', '' + url_server + 'iniciarTarea' + '', 'POST', {
                    id_inspector: data.id_inspector,
                    codigo_identificador: data.codigo_identificador,
                    id_tarea: data.id_server,
                    num_caso: data.num_caso,
                    id_app: item.id
                }, 15000, ID_283596564);
            }
        });
    }
    return null;
};
funciones.enviarfirmas = function(x_params) {
    if (Ti.App.deployType != 'production') console.log('entre a enviarFirmas', {});
    var ID_683829921_i = Alloy.createCollection('insp_firma');
    var ID_683829921_i_where = '';
    ID_683829921_i.fetch();
    var firmas = require('helper').query2array(ID_683829921_i);
    var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
    if ((_.isObject(url_server) || (_.isString(url_server)) && !_.isEmpty(url_server)) || _.isNumber(url_server) || _.isBoolean(url_server)) {
        /** 
         * enviamos la imagen de las firmas 
         */
        if (firmas && firmas.length) {
            /** 
             * Revisamos si el modelo de las firmas contiene firmas pendientes por enviar 
             */
            var firma_index = 0;
            _.each(firmas, function(firma, firma_pos, firma_list) {
                firma_index += 1;
                if (Ti.App.deployType != 'production') console.log('enviando la firma', {
                    "datos": firma.firma64
                });
                if (Ti.App.deployType != 'production') console.log('leyendo archivo: {data}/inspeccion' + firma.id_inspeccion + '/' + firma.firma64, {});
                /** 
                 * Leemos archivo de firma 
                 */

                var firmabin = '';
                var ID_1272411058_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + firma.id_inspeccion);
                if (ID_1272411058_d.exists() == true) {
                    var ID_1272411058_f = Ti.Filesystem.getFile(ID_1272411058_d.resolve(), firma.firma64);
                    if (ID_1272411058_f.exists() == true) {
                        firmabin = ID_1272411058_f.read();
                    }
                    ID_1272411058_f = null;
                }
                ID_1272411058_d = null;
                var ID_362017495 = {};
                console.log('DEBUG WEB: requesting url:' + url_server + 'subirImagenes' + ' with data:', {
                    _method: 'POST',
                    _params: {
                        id_tarea: firma.id_inspeccion,
                        archivo: firma.firma64,
                        imagen: firmabin,
                        app_id: firma.firma64
                    },
                    _timeout: '20000'
                });

                ID_362017495.success = function(e) {
                    var elemento = e,
                        valor = e;
                    if (elemento.error != 0 && elemento.error != '0') {} else if (!_.isUndefined(elemento.app_id)) {
                        if (Ti.App.deployType != 'production') console.log('envio firma exitoso', {
                            "elemento": elemento
                        });
                        var ID_1194757798_i = Alloy.createCollection('insp_firma');
                        var ID_1194757798_i_where = 'firma64=\'' + elemento.app_id + '\'';
                        ID_1194757798_i.fetch({
                            query: 'SELECT * FROM insp_firma WHERE firma64=\'' + elemento.app_id + '\''
                        });
                        var datotarea = require('helper').query2array(ID_1194757798_i);
                        if (datotarea && datotarea.length) {
                            /** 
                             * borramos archivo que dijo fue exitoso 
                             */
                            var ID_576056460_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + datotarea[0].id_inspeccion);
                            var ID_576056460_f = Ti.Filesystem.getFile(ID_576056460_d.resolve(), elemento.app_id);
                            if (ID_576056460_f.exists() == true) ID_576056460_f.deleteFile();
                            /** 
                             * Borramos registro de tabla firma 
                             */
                            /** 
                             * Borramos registro de tabla firma 
                             */

                            var ID_314260061_i = Alloy.Collections.insp_firma;
                            var sql = 'DELETE FROM ' + ID_314260061_i.config.adapter.collection_name + ' WHERE firma64=\'' + elemento.app_id + '\'';
                            var db = Ti.Database.open(ID_314260061_i.config.adapter.db_name);
                            db.execute(sql);
                            db.close();
                            sql = null;
                            db = null;
                            ID_314260061_i.trigger('delete');
                            if (Ti.App.deployType != 'production') console.log('firma ' + elemento.app_id + ' borrada', {});
                        }
                        /** 
                         * Limpiar memoria 
                         */
                        datotarea = null;
                    } else {
                        /** 
                         * Borramos imagen local porque debe ser historia previa 
                         */
                    }
                    elemento = null, valor = null;
                };

                ID_362017495.error = function(e) {
                    var elemento = e,
                        valor = e;
                    if (Ti.App.deployType != 'production') console.log('envio firma fallido', {
                        "elemento": elemento
                    });
                    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
                    elemento = null, valor = null;
                };
                require('helper').ajaxUnico('ID_362017495', '' + url_server + 'subirImagenes' + '', 'POST', {
                    id_tarea: firma.id_inspeccion,
                    archivo: firma.firma64,
                    imagen: firmabin,
                    app_id: firma.firma64
                }, 20000, ID_362017495);
            });
        }
    }
    return null;
};
funciones.degreestoradians = function(x_params) {
    var degrees = x_params['degrees'];
    resultado = degrees * Math.PI / 180;
    return resultado;
};
funciones.distanceinmtbetweenearthcoordinates = function(x_params) {
    var lat2 = x_params['lat2'];
    var lon2 = x_params['lon2'];
    var lat1 = x_params['lat1'];
    var lon1 = x_params['lon1'];
    var earthradiuskm = 6371;
    var dlat = null;
    if ('degreestoradians' in require('funciones')) {
        dlat = require('funciones').degreestoradians({
            'degrees': lat2 - lat1
        });
    } else {
        try {
            dlat = f_degreestoradians({
                'degrees': lat2 - lat1
            });
        } catch (ee) {}
    }
    var dlon = null;
    if ('degreestoradians' in require('funciones')) {
        dlon = require('funciones').degreestoradians({
            'degrees': lon2 - lon1
        });
    } else {
        try {
            dlon = f_degreestoradians({
                'degrees': lon2 - lon1
            });
        } catch (ee) {}
    }
    var lat1 = null;
    if ('degreestoradians' in require('funciones')) {
        lat1 = require('funciones').degreestoradians({
            'degrees': lat1
        });
    } else {
        try {
            lat1 = f_degreestoradians({
                'degrees': lat1
            });
        } catch (ee) {}
    }
    var lat2 = null;
    if ('degreestoradians' in require('funciones')) {
        lat2 = require('funciones').degreestoradians({
            'degrees': lat2
        });
    } else {
        try {
            lat2 = f_degreestoradians({
                'degrees': lat2
            });
        } catch (ee) {}
    }

    var a = Math.sin(dlat / 2) * Math.sin(dlat / 2) + Math.sin(dlon / 2) * Math.sin(dlon / 2) * Math.cos(lat1) * Math.cos(lat2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    distancia = (earthradiuskm * c) * 1000
    return distancia;
};

module.exports = funciones;