var plantillas = {
}
plantillas.fila_tarea = function(x_params,origen_nav) {
var data = x_params['data'];
var ciudad = x_params['ciudad'];
var seguir = x_params['seguir'];
var tipotarea = x_params['tipotarea'];
var comuna = x_params['comuna'];
var direccion = x_params['direccion'];
var distancia = x_params['distancia'];
var ID_53042977 = Titanium.UI.createTableViewSection({
}
);
var ID_384944981 = Titanium.UI.createTableViewRow({
className : 'hackID_384944981'
}
);
var ID_821554929 = Titanium.UI.createView({
height : '55dp',
layout : 'composite',
width : '100%'
}
);
var ID_1002253527 = Titanium.UI.createView({
height : '55dp',
layout : 'horizontal'
}
);
var ID_1696532710 = Titanium.UI.createView({
height : '100%',
layout : 'vertical',
width : '1%'
}
);
ID_1002253527.add(ID_1696532710);
var ID_1739221934 = Titanium.UI.createView({
height : Ti.UI.SIZE,
left : '5dp',
layout : 'vertical',
width : '87%'
}
);
var ID_1615120356 = Titanium.UI.createView({
height : Ti.UI.SIZE,
layout : 'horizontal',
width : Ti.UI.FILL
}
);
var ID_648162266 = Titanium.UI.createLabel({
height : Ti.UI.SIZE,
text : '\uE800',
color : '#2D9EDB',
width : Ti.UI.SIZE,
ancho : '-',
font : {
fontFamily : 'beta1',
fontSize : '15dp'
}

}
);
ID_1615120356.add(ID_648162266);
var ID_148699692 = Titanium.UI.createLabel({
ellipsize : Titanium.UI.TEXT_ELLIPSIZE_TRUNCATE_END,
left : 4,
text : direccion,
color : '#4d4d4d',
wordWrap : false,
touchEnabled : false,
width : Ti.UI.FILL,
textAlign : Ti.UI.TEXT_ALIGNMENT_LEFT,
font : {
fontSize : '16dp'
}

}
);
ID_1615120356.add(ID_148699692);

ID_1739221934.add(ID_1615120356);
var ID_1224500518 = Titanium.UI.createView({
height : Ti.UI.SIZE,
left : '0dp',
layout : 'horizontal',
width : '100%'
}
);
var ID_1670613628 = Titanium.UI.createView({
height : '20dp',
layout : 'composite',
width : '26%',
top : '2dp'
}
);
var ID_620197319 = Titanium.UI.createLabel({
height : Ti.UI.SIZE,
text : '\uE829',
left : '0dp',
color : '#2D9EDB',
width : Ti.UI.SIZE,
font : {
fontFamily : 'beta1',
fontSize : '9dp'
}

}
);
ID_1670613628.add(ID_620197319);
var ID_940047385 = Titanium.UI.createLabel({
ellipsize : Titanium.UI.TEXT_ELLIPSIZE_TRUNCATE_END,
height : '100%',
left : 14,
text : 'a '+distancia+' km',
color : '#a0a1a3',
wordWrap : false,
touchEnabled : false,
right : 0,
font : {
fontFamily : 'Roboto-Light',
fontSize : '12dp'
}

}
);
ID_1670613628.add(ID_940047385);

ID_1224500518.add(ID_1670613628);
var ID_780703879 = Titanium.UI.createView({
height : '20dp',
layout : 'composite',
width : '35%',
top : '2dp'
}
);
var ID_1352818624 = Titanium.UI.createLabel({
height : Ti.UI.SIZE,
text : '\uE811',
left : '0dp',
color : '#F8DA54',
width : Ti.UI.SIZE,
font : {
fontFamily : 'beta1',
fontSize : '9dp'
}

}
);
ID_780703879.add(ID_1352818624);
var ID_1862215651 = Titanium.UI.createLabel({
ellipsize : Titanium.UI.TEXT_ELLIPSIZE_TRUNCATE_END,
height : '100%',
left : 14,
text : comuna,
color : '#a0a1a3',
wordWrap : false,
touchEnabled : false,
right : 0,
font : {
fontFamily : 'Roboto-Light',
fontSize : '12dp'
}

}
);
ID_780703879.add(ID_1862215651);

ID_1224500518.add(ID_780703879);
var ID_282912275 = Titanium.UI.createView({
height : '20dp',
layout : 'composite',
width : '38%',
top : '2dp'
}
);
var ID_1259303976 = Titanium.UI.createLabel({
height : Ti.UI.SIZE,
text : '\uE810',
left : '0dp',
color : '#8CE5BD',
width : Ti.UI.SIZE,
font : {
fontFamily : 'beta1',
fontSize : '9dp'
}

}
);
ID_282912275.add(ID_1259303976);
var ID_1301734233 = Titanium.UI.createLabel({
ellipsize : Titanium.UI.TEXT_ELLIPSIZE_TRUNCATE_END,
height : '100%',
left : 14,
text : ciudad,
color : '#a0a1a3',
wordWrap : false,
touchEnabled : false,
right : 0,
font : {
fontFamily : 'Roboto-Light',
fontSize : '12dp'
}

}
);
ID_282912275.add(ID_1301734233);

ID_1224500518.add(ID_282912275);
ID_1739221934.add(ID_1224500518);
ID_1002253527.add(ID_1739221934);
var ID_1642626696 = Titanium.UI.createView({
height : '100%',
layout : 'vertical',
width : '10%'
}
);
var ID_1283878755 = Titanium.UI.createLabel({
height : Ti.UI.SIZE,
text : '\uE81b',
color : '#CCCCCC',
width : Ti.UI.SIZE,
top : '35%',
right : '5dp',
font : {
fontFamily : 'beta1',
fontSize : '14dp'
}

}
);
ID_1642626696.add(ID_1283878755);
ID_1002253527.add(ID_1642626696);
var ID_648162266_icono = tipotarea;

										  if ('fontello' in require('a4w') && 'adjust' in require('a4w').fontello && ID_648162266_icono in require('a4w').fontello['adjust'].CODES) {
									  			ID_648162266_icono = require('a4w').fontello['adjust'].CODES[ID_648162266_icono];
										  } else {
										  		console.log('live/modificar -> error setting new svg icon alias');
										  }
										  ID_648162266.setText(ID_648162266_icono);

if (tipotarea==1||tipotarea=='1') {
ID_648162266.setColor('#2d9edb');

ID_1696532710.setBackgroundColor('#2d9edb');
}
 else if (tipotarea==2) {
ID_648162266.setColor('#ee7f7e');

ID_1696532710.setBackgroundColor('#ee7f7e');
} else if (tipotarea==3) {
ID_648162266.setColor('#b9aaf3');

ID_1696532710.setBackgroundColor('#8383db');
} else if (tipotarea==4) {
ID_648162266.setColor('#fcbd83');

ID_1696532710.setBackgroundColor('#fcbd83');
} else if (tipotarea==5) {
ID_648162266.setColor('#8ce5bd');

ID_1696532710.setBackgroundColor('#8ce5bd');
} else if (tipotarea==6) {
ID_648162266.setColor('#f8da54');

ID_1696532710.setBackgroundColor('#f8da54');
} else if (tipotarea==7) {
ID_648162266.setColor('#8383db');

ID_1696532710.setBackgroundColor('#b9aaf3');
} else if (tipotarea==8) {
ID_648162266.setColor('#ffacaa');

ID_1696532710.setBackgroundColor('#ffacaa');
} else if (tipotarea==9) {
ID_648162266.setColor('#8bc9e8');

ID_1696532710.setBackgroundColor('#8bc9e8');
} else if (tipotarea==10) {
ID_648162266.setColor('#a5876d');

ID_1696532710.setBackgroundColor('#a5876d');
} else if (tipotarea=='critico') {
ID_648162266.setColor('#ee7f7e');

ID_1696532710.setBackgroundColor('#ee7f7e');
} else {
ID_648162266.setColor('#ee7f7e');

ID_148699692.setLeft('0');

}var fila;
fila = ID_384944981;
 fila._data=data;
ID_821554929.add(ID_1002253527);
var ID_1945672269 = Titanium.UI.createView({
height : '35dp',
visible : seguir,
layout : 'vertical',
width : '28dp',
top : '9dp',
right : '24dp'
}
);
if (OS_IOS) {
var ID_1725018648 = Ti.UI.createImageView({
height : Ti.UI.FILL,
duration : 50,
images : ['/images/i4ECB85A2AE41ACB15653139F35BFA8C7.png','/images/i4DB2E8AD733DFC95AF5E6A5115E117EE.png','/images/iD4FE3BB66A3A734672CFBF9A4FE820B3.png','/images/iD95E1371C4A9D4060E4C293FEBB2A371.png','/images/i1846D1705B39A0DFD7CA331E2A624A7B.png','/images/i1AAB9DCC791F6C8D5CBD86A69F2BD28A.png','/images/iC18AD8B25C7E89544B26A56D72763ACF.png','/images/i451D26CB7C617839FAAABBEE243EE91B.png','/images/iBF02E0A9F57D34BA382B6B4851AEB549.png','/images/iB810656D7547F7A2C967AC3DB0C7CAD4.png','/images/iF173895CDE559CF4A789ECA310F324C1.png','/images/iB13BBB34910D6E346D89A425DF9D9AA7.png','/images/i09E757E6A96CDFD1A3071F75A86B9703.png','/images/iB1B0726192E9AFA5B4393DD2789A8E73.png','/images/i3BD062E17CB084738EC3C8544C4F562C.png','/images/i0B25A07F8BA6A3D1155E5635855725E3.png','/images/i51CD06F5D53967A813F0F7C4F8679F81.png','/images/i57F0B2CBFC207F6540A08C8209B6461C.png','/images/i6F4FBDF546B1311877A208695B53B1F5.png','/images/i20546752F9D3EB6A38FD547BB45E2DA7.png','/images/i997DFD2C59EEEC93E94CA8831CCFE464.png','/images/i7A0053CD88CD6D72A30F33A67115B84F.png','/images/i53F37CB31AA110AB8BF1BCA095F905FE.png','/images/i6F4B0EAC06F1D5F21CF0ADB7E3036782.png','/images/iD8AE92E691560F489DE642319547F8F9.png','/images/iF8FD2CB3805AB4250C4E53D99093CA3C.png','/images/iCA260B7119401A0D456373D95DAD7824.png','/images/iC97BF367BA6481E3A5CEAEBFBD492506.png','/images/iC9B3A576F9A65C4C9C208CFB562D3A8D.png','/images/iCEF1D427EF5C147BC79A0D6848913C44.png'],
id : ID_1725018648,
repeatCount : 0
}
);
} else {
var ID_1725018648 = Ti.UI.createImageView({
height : Ti.UI.FILL,
duration : 50,
images : ['/images/i4ECB85A2AE41ACB15653139F35BFA8C7.png','/images/i4DB2E8AD733DFC95AF5E6A5115E117EE.png','/images/iD4FE3BB66A3A734672CFBF9A4FE820B3.png','/images/iD95E1371C4A9D4060E4C293FEBB2A371.png','/images/i1846D1705B39A0DFD7CA331E2A624A7B.png','/images/i1AAB9DCC791F6C8D5CBD86A69F2BD28A.png','/images/iC18AD8B25C7E89544B26A56D72763ACF.png','/images/i451D26CB7C617839FAAABBEE243EE91B.png','/images/iBF02E0A9F57D34BA382B6B4851AEB549.png','/images/iB810656D7547F7A2C967AC3DB0C7CAD4.png','/images/iF173895CDE559CF4A789ECA310F324C1.png','/images/iB13BBB34910D6E346D89A425DF9D9AA7.png','/images/i09E757E6A96CDFD1A3071F75A86B9703.png','/images/iB1B0726192E9AFA5B4393DD2789A8E73.png','/images/i3BD062E17CB084738EC3C8544C4F562C.png','/images/i0B25A07F8BA6A3D1155E5635855725E3.png','/images/i51CD06F5D53967A813F0F7C4F8679F81.png','/images/i57F0B2CBFC207F6540A08C8209B6461C.png','/images/i6F4FBDF546B1311877A208695B53B1F5.png','/images/i20546752F9D3EB6A38FD547BB45E2DA7.png','/images/i997DFD2C59EEEC93E94CA8831CCFE464.png','/images/i7A0053CD88CD6D72A30F33A67115B84F.png','/images/i53F37CB31AA110AB8BF1BCA095F905FE.png','/images/i6F4B0EAC06F1D5F21CF0ADB7E3036782.png','/images/iD8AE92E691560F489DE642319547F8F9.png','/images/iF8FD2CB3805AB4250C4E53D99093CA3C.png','/images/iCA260B7119401A0D456373D95DAD7824.png','/images/iC97BF367BA6481E3A5CEAEBFBD492506.png','/images/iC9B3A576F9A65C4C9C208CFB562D3A8D.png','/images/iCEF1D427EF5C147BC79A0D6848913C44.png'],
id : ID_1725018648,
repeatCount : 0
}
);
}
require('vars')['_ID_1725018648_original_']=ID_1725018648.getImage();
require('vars')['_ID_1725018648_filtro_']='original';

ID_1725018648.addEventListener('load',function(e) {
e.cancelBubble=true;
var elemento=e.source;
var evento=e;
 elemento.start();
});
ID_1945672269.add(ID_1725018648);
ID_821554929.add(ID_1945672269);
ID_384944981.add(ID_821554929);
ID_53042977.add(ID_384944981);
return ID_53042977;
};

module.exports = plantillas;