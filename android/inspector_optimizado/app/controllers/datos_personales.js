var _bind4section = {};
var _list_templates = {};

var _activity;
if (OS_ANDROID) {
    _activity = $.ID_334050739.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_334050739';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_334050739.addEventListener('open', function(e) {});
}
$.ID_334050739.orientationModes = [Titanium.UI.PORTRAIT];


$.ID_129849548.init({
    titulo: 'ENROLAMIENTO',
    __id: 'ALL129849548',
    fondo: 'fondoblanco',
    colortitulo: 'negro',
    modal: ''
});


$.ID_2016792055.init({
    __id: 'ALL2016792055',
    aceptar: 'Aceptar',
    cancelar: 'Cancelar',
    onaceptar: Aceptar_ID_1841760870,
    oncancelar: Cancelar_ID_1964061679
});

function Aceptar_ID_1841760870(e) {

    var evento = e;
    var moment = require('alloy/moment');
    var ID_1573430927 = evento.valor;
    var resp = moment(ID_1573430927).format('DD-MM-YYYY');
    $.ID_1227845525.setText(resp);

    var ID_1227845525_estilo = 'estilo12';

    var _tmp_a4w = require('a4w');
    if ((typeof ID_1227845525_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (ID_1227845525_estilo in _tmp_a4w.styles['classes'])) {
        try {
            ID_1227845525_estilo = _tmp_a4w.styles['classes'][ID_1227845525_estilo];
        } catch (st_val_err) {}
    }
    _tmp_a4w = null;
    $.ID_1227845525.applyProperties(ID_1227845525_estilo);

    require('vars')['fecha_nacimiento'] = evento.valor;

}

function Cancelar_ID_1964061679(e) {

    var evento = e;
    if (Ti.App.deployType != 'production') console.log('selector de fecha cancelado', {});

}

$.ID_1146042904.init({
    titulo: 'PARTE 2: Datos personales',
    __id: 'ALL1146042904',
    avance: '2/6',
    onclick: Click_ID_289151780
});

function Click_ID_289151780(e) {

    var evento = e;
    $.ID_154930557.blur();
    $.ID_1663369926.blur();
    $.ID_1731330558.blur();
    $.ID_1719306939.blur();

}

function Focus_ID_38810559(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;

    $.ID_2016792055.cerrar({});
    elemento = null, source = null;

}

function Focus_ID_1159714816(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;

    $.ID_2016792055.cerrar({});
    elemento = null, source = null;

}

function Focus_ID_505244999(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;

    $.ID_2016792055.cerrar({});
    elemento = null, source = null;

}

function Focus_ID_1827264810(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;

    $.ID_2016792055.cerrar({});
    elemento = null, source = null;

}

function Click_ID_1813410245(e) {

    e.cancelBubble = true;
    var elemento = e.source;

    $.ID_2016792055.abrir({});
    $.ID_154930557.blur();
    $.ID_1663369926.blur();
    $.ID_1731330558.blur();
    $.ID_1719306939.blur();

}

$.ID_1860370347.init({
    titulo: 'CONTINUAR',
    __id: 'ALL1860370347',
    onclick: Click_ID_275036354
});

function Click_ID_275036354(e) {

    var evento = e;
    /** 
     * Obtenemos el ano actual y guardamos en variable 
     */
    var moment = require('alloy/moment');
    var hoy_ano = moment(new Date()).format('YYYY');
    /** 
     * Recuperamos variable fecha_nacimiento para obtener el ano 
     */
    var fecha_nacimiento = ('fecha_nacimiento' in require('vars')) ? require('vars')['fecha_nacimiento'] : '';
    var moment = require('alloy/moment');
    var ID_450324445 = fecha_nacimiento;
    var sel_ano = moment(ID_450324445).format('YYYY');
    if (Ti.App.deployType != 'production') console.log('fechas de nacimiento', {
        "fecha_nacimiento": sel_ano + ' y el largo es ' + fecha_nacimiento.lengh,
        "hoy": hoy_ano,
        "tipo de datos": typeof hoy_ano + ' ' + typeof sel_ano
    });
    /** 
     * Obtenemos los datos ingresados en los campos 
     */
    var nombre;
    nombre = $.ID_154930557.getValue();

    var apellido_paterno;
    apellido_paterno = $.ID_1663369926.getValue();

    var apellido_materno;
    apellido_materno = $.ID_1731330558.getValue();

    var codigo_verificador;
    codigo_verificador = $.ID_1719306939.getValue();

    if ((_.isObject(nombre) || _.isString(nombre)) && _.isEmpty(nombre)) {
        /** 
         * Levantamos mensaje de error en caso de que hayan campos vacios o el inspector tenga menos de 18 anos 
         */
        var ID_7089201_opts = ['Aceptar'];
        var ID_7089201 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese nombres',
            buttonNames: ID_7089201_opts
        });
        ID_7089201.addEventListener('click', function(e) {
            var suu = ID_7089201_opts[e.index];
            suu = null;

            e.source.removeEventListener("click", arguments.callee);
        });
        ID_7089201.show();
    } else if ((_.isObject(apellido_paterno) || _.isString(apellido_paterno)) && _.isEmpty(apellido_paterno)) {
        var ID_1438123274_opts = ['Aceptar'];
        var ID_1438123274 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese apellido paterno',
            buttonNames: ID_1438123274_opts
        });
        ID_1438123274.addEventListener('click', function(e) {
            var suu = ID_1438123274_opts[e.index];
            suu = null;

            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1438123274.show();
    } else if ((_.isObject(codigo_verificador) || _.isString(codigo_verificador)) && _.isEmpty(codigo_verificador)) {
        var ID_1884737053_opts = ['Aceptar'];
        var ID_1884737053 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese rut',
            buttonNames: ID_1884737053_opts
        });
        ID_1884737053.addEventListener('click', function(e) {
            var suu = ID_1884737053_opts[e.index];
            suu = null;

            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1884737053.show();
    } else if (_.isNull(fecha_nacimiento)) {
        var ID_955387930_opts = ['Aceptar'];
        var ID_955387930 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese fecha de nacimiento',
            buttonNames: ID_955387930_opts
        });
        ID_955387930.addEventListener('click', function(e) {
            var suu = ID_955387930_opts[e.index];
            suu = null;

            e.source.removeEventListener("click", arguments.callee);
        });
        ID_955387930.show();
    } else if ((fecha_nacimiento.length) == 0 || (fecha_nacimiento.length) == '0') {
        var ID_1140392854_opts = ['Aceptar'];
        var ID_1140392854 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese fecha de nacimiento',
            buttonNames: ID_1140392854_opts
        });
        ID_1140392854.addEventListener('click', function(e) {
            var suu = ID_1140392854_opts[e.index];
            suu = null;

            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1140392854.show();
    } else if (_.isNumber(hoy_ano - sel_ano) && _.isNumber(18) && hoy_ano - sel_ano < 18) {
        var ID_1109899382_opts = ['Aceptar'];
        var ID_1109899382 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Debe ser mayor de edad',
            buttonNames: ID_1109899382_opts
        });
        ID_1109899382.addEventListener('click', function(e) {
            var suu = ID_1109899382_opts[e.index];
            suu = null;

            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1109899382.show();
    } else {
        /** 
         * Mostramos animacion de progreso en el boton continuar 
         */

        $.ID_1860370347.iniciar_progreso({});
        /** 
         * Recuperamos la variable registro y agregamos los campos de la pantalla 
         */
        var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
        var registro = _.extend(registro, {
            nombre: nombre,
            apellido_paterno: apellido_paterno,
            apellido_materno: apellido_materno,
            codigo_identificador: codigo_verificador,
            fecha_nacimiento: fecha_nacimiento
        });
        require('vars')['registro'] = registro;
        var ID_877726171_func = function() {
            /** 
             * Detenemos animacion de progreso en el boton continuar 
             */

            $.ID_1860370347.detener_progreso({});
            /** 
             * Enviamos a la proxima pantalla, domicilio 
             */
            Alloy.createController("domicilio", {}).getView().open();
        };
        var ID_877726171 = setTimeout(ID_877726171_func, 1000 * 0.3);
    }
}

(function() {
    var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
    var ID_1588688991_i = Alloy.createCollection('pais');
    var ID_1588688991_i_where = 'id_server=\'' + registro.pais + '\'';
    ID_1588688991_i.fetch({
        query: 'SELECT * FROM pais WHERE id_server=\'' + registro.pais + '\''
    });
    var pais_iter = require('helper').query2array(ID_1588688991_i);
    /** 
     * Modificamos el label para poner el codigo identificador de una persona 
     */
    $.ID_1454083959.setText(pais_iter[0].label_codigo_identificador);

    var hint;
    hint = $.ID_1719306939.getHintText();

    /** 
     * Modificamos el hint para poner el codigo identificador de una persona 
     */
    $.ID_1719306939.setHintText(hint + ' ' + pais_iter[0].label_codigo_identificador);

    if (Ti.App.deployType != 'production') console.log('DATOS', {
        "del pais": pais_iter[0]
    });
    /** 
     * Creamos evento que al cerrar desde la ultima pantalla, esta tambi&#233;n se cierre. Evitamos el uso excesivo de memoria ram 
     */

    _my_events['_close_enrolamiento,ID_131518285'] = function(evento) {
        $.ID_334050739.close();
        if (Ti.App.deployType != 'production') console.log('escuchando cerrar enrolamiento datospersonales', {});
    };
    Alloy.Events.on('_close_enrolamiento', _my_events['_close_enrolamiento,ID_131518285']);
})();

function Androidback_ID_1390961599(e) {
    /** 
     * Dejamos esta accion vacia para que no pueda volver a la pantalla anterior 
     */

    e.cancelBubble = true;
    var elemento = e.source;

}
if (OS_IOS || OS_ANDROID) {
    $.ID_334050739.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}