var _bind4section = {};
var _list_templates = {};
var $contenido = $.contenido.toJSON();

var _activity;
if (OS_ANDROID) {
    _activity = $.ID_1499400166.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_1499400166';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_1499400166.addEventListener('open', function(e) {});
}
$.ID_1499400166.orientationModes = [Titanium.UI.PORTRAIT];


$.ID_1035873505.init({
    titulo: 'EDITAR CONTENIDO',
    __id: 'ALL1035873505',
    textoderecha: 'Guardar',
    oncerrar: Cerrar_ID_743563266,
    fondo: 'fondoceleste',
    top: 0,
    modal: '',
    onpresiono: Presiono_ID_1253119354,
    colortextoderecha: 'blanco'
});

function Cerrar_ID_743563266(e) {

    var evento = e;
    /** 
     * Limpiamos memoria de widgets 
     */
    $.ID_1509904444.limpiar({});
    $.ID_1620626702.limpiar({});
    $.ID_830800377.limpiar({});
    $.ID_1499400166.close();

}

function Presiono_ID_1253119354(e) {

    var evento = e;
    /** 
     * Obtenemos la fecha actual 
     */

    var hoy = new Date();
    if ($contenido.fecha_compra > hoy == true || $contenido.fecha_compra > hoy == 'true') {
        /** 
         * Validamos que los campos ingresados sean correctos 
         */
        var ID_748772290_opts = ['Aceptar'];
        var ID_748772290 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'La fecha de compra no puede ser superior a hoy',
            buttonNames: ID_748772290_opts
        });
        ID_748772290.addEventListener('click', function(e) {
            var nulo = ID_748772290_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_748772290.show();
    } else if (_.isUndefined($contenido.nombre)) {
        var ID_863352474_opts = ['Aceptar'];
        var ID_863352474 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Seleccione producto',
            buttonNames: ID_863352474_opts
        });
        ID_863352474.addEventListener('click', function(e) {
            var nulo = ID_863352474_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_863352474.show();
    } else if (_.isUndefined($contenido.id_marca)) {
        var ID_1592717384_opts = ['Aceptar'];
        var ID_1592717384 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Seleccione marca del producto',
            buttonNames: ID_1592717384_opts
        });
        ID_1592717384.addEventListener('click', function(e) {
            var nulo = ID_1592717384_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1592717384.show();
    } else if (_.isUndefined($contenido.id_recinto)) {
        var ID_1548198078_opts = ['Aceptar'];
        var ID_1548198078 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Seleccione el recinto donde estaba el producto',
            buttonNames: ID_1548198078_opts
        });
        ID_1548198078.addEventListener('click', function(e) {
            var nulo = ID_1548198078_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1548198078.show();
    } else if (_.isUndefined($contenido.cantidad)) {
        var ID_1207943731_opts = ['Aceptar'];
        var ID_1207943731 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese cantidad de productos',
            buttonNames: ID_1207943731_opts
        });
        ID_1207943731.addEventListener('click', function(e) {
            var nulo = ID_1207943731_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1207943731.show();
    } else if ((_.isObject($contenido.cantidad) || _.isString($contenido.cantidad)) && _.isEmpty($contenido.cantidad)) {
        var ID_589012032_opts = ['Aceptar'];
        var ID_589012032 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese cantidad de productos',
            buttonNames: ID_589012032_opts
        });
        ID_589012032.addEventListener('click', function(e) {
            var nulo = ID_589012032_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_589012032.show();
    } else if (_.isUndefined($contenido.fecha_compra)) {
        var ID_45624129_opts = ['Aceptar'];
        var ID_45624129 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese fecha de compra aproximada (año)',
            buttonNames: ID_45624129_opts
        });
        ID_45624129.addEventListener('click', function(e) {
            var nulo = ID_45624129_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_45624129.show();
    } else if (_.isUndefined($contenido.id_moneda)) {
        var ID_590938979_opts = ['Aceptar'];
        var ID_590938979 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Seleccione el tipo de moneda del producto',
            buttonNames: ID_590938979_opts
        });
        ID_590938979.addEventListener('click', function(e) {
            var nulo = ID_590938979_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_590938979.show();
    } else if (_.isUndefined($contenido.valor)) {
        var ID_1095375569_opts = ['Aceptar'];
        var ID_1095375569 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese valor del producto',
            buttonNames: ID_1095375569_opts
        });
        ID_1095375569.addEventListener('click', function(e) {
            var nulo = ID_1095375569_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1095375569.show();
    } else if ((_.isObject($contenido.valor) || _.isString($contenido.valor)) && _.isEmpty($contenido.valor)) {
        var ID_491654900_opts = ['Aceptar'];
        var ID_491654900 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese valor del producto',
            buttonNames: ID_491654900_opts
        });
        ID_491654900.addEventListener('click', function(e) {
            var nulo = ID_491654900_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_491654900.show();
    } else if (_.isUndefined($contenido.descripcion)) {
        var ID_1952260059_opts = ['Aceptar'];
        var ID_1952260059 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Describa el producto brevemente, con un mínimo de 30 caracteres',
            buttonNames: ID_1952260059_opts
        });
        ID_1952260059.addEventListener('click', function(e) {
            var nulo = ID_1952260059_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1952260059.show();
    } else if (_.isNumber($contenido.descripcion.length) && _.isNumber(29) && $contenido.descripcion.length <= 29) {
        var ID_1195624866_opts = ['Aceptar'];
        var ID_1195624866 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Describa el producto brevemente, con un mínimo de 30 caracteres',
            buttonNames: ID_1195624866_opts
        });
        ID_1195624866.addEventListener('click', function(e) {
            var nulo = ID_1195624866_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1195624866.show();
    } else {
        /** 
         * Eliminamos modelo previo 
         */
        /** 
         * Eliminamos modelo previo 
         */
        var ID_1280961897_i = Alloy.Collections.insp_contenido;
        var sql = 'DELETE FROM ' + ID_1280961897_i.config.adapter.collection_name + ' WHERE id=\'' + args._dato.id + '\'';
        var db = Ti.Database.open(ID_1280961897_i.config.adapter.db_name);
        db.execute(sql);
        db.close();
        sql = null;
        db = null;
        ID_1280961897_i.trigger('delete');
        /** 
         * Guardamos modelo nuevo 
         */
        Alloy.Collections[$.contenido.config.adapter.collection_name].add($.contenido);
        $.contenido.save();
        Alloy.Collections[$.contenido.config.adapter.collection_name].fetch();
        /** 
         * limpiamos widgets multiples (memoria ram) 
         */
        $.ID_1509904444.limpiar({});
        $.ID_1620626702.limpiar({});
        $.ID_830800377.limpiar({});
        $.ID_1499400166.close();
    }
}

function Click_ID_42705456(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    /** 
     * Ocupamos esta variable para impedir que la pantalla siguente se cargue dos veces 
     */
    var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
    if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
        require('vars')['var_abriendo'] = 'true';
        Alloy.createController("listadocontenidos", {}).getView().open();
    }

}

$.ID_830800377.init({
    titulo: 'MARCAS',
    cargando: 'cargando ..',
    __id: 'ALL830800377',
    left: 0,
    onrespuesta: Respuesta_ID_444018628,
    campo: 'Marca del Item',
    onabrir: Abrir_ID_1071645563,
    color: 'celeste',
    right: 0,
    seleccione: 'seleccione marca',
    activo: true,
    onafterinit: Afterinit_ID_145296888
});

function Abrir_ID_1071645563(e) {

    var evento = e;

}

function Respuesta_ID_444018628(e) {

    var evento = e;
    $.ID_830800377.labels({
        valor: evento.valor
    });
    $.contenido.set({
        id_marca: evento.item.id_interno
    });
    if ('contenido' in $) $contenido = $.contenido.toJSON();
    if (Ti.App.deployType != 'production') console.log('datos recibidos de modal marca item', {
        "datos": evento
    });

}

function Afterinit_ID_145296888(e) {

    var evento = e;
    var ID_616239583_func = function() {
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        var ID_558389166_i = Alloy.createCollection('insp_contenido');
        var ID_558389166_i_where = 'id=\'' + args._dato.id + '\'';
        ID_558389166_i.fetch({
            query: 'SELECT * FROM insp_contenido WHERE id=\'' + args._dato.id + '\''
        });
        var insp_dato = require('helper').query2array(ID_558389166_i);
        if (insp_dato && insp_dato.length) {
            /** 
             * obtenemos valor seleccionado previamente y lo mostramos en selector. 
             */
            var ID_1884314487_i = Alloy.createCollection('marcas');
            var ID_1884314487_i_where = 'id_segured=\'' + insp_dato[0].id_marca + '\'';
            ID_1884314487_i.fetch({
                query: 'SELECT * FROM marcas WHERE id_segured=\'' + insp_dato[0].id_marca + '\''
            });
            var marca = require('helper').query2array(ID_1884314487_i);
            if (marca && marca.length) {
                var ID_1058169154_func = function() {
                    $.ID_830800377.labels({
                        valor: marca[0].nombre
                    });
                    if (Ti.App.deployType != 'production') console.log('el nombre de la partida es', {
                        "datos": marca[0].nombre
                    });
                };
                var ID_1058169154 = setTimeout(ID_1058169154_func, 1000 * 0.21);
            }
        }
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
            var ID_1816422964_i = Alloy.createCollection('marcas');
            var ID_1816422964_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
            ID_1816422964_i.fetch({
                query: 'SELECT * FROM marcas WHERE pais_texto=\'' + pais[0].nombre + '\''
            });
            var marcas = require('helper').query2array(ID_1816422964_i);
            var datos = [];
            _.each(marcas, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'valor';
                    if (llave == 'id_segured') newkey = 'id_interno';
                    if (llave == 'id_server') newkey = 'id_gerardo';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                datos.push(new_row);
            });
        } else {
            var ID_1821446232_i = Alloy.Collections.marcas;
            var sql = "DELETE FROM " + ID_1821446232_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_1821446232_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_1821446232_i.trigger('delete');
            var item_index = 0;
            _.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
                item_index += 1;
                var ID_742339479_m = Alloy.Collections.marcas;
                var ID_742339479_fila = Alloy.createModel('marcas', {
                    nombre: 'Muro' + item,
                    id_server: item,
                    id_segured: '10' + item,
                    pais: 1
                });
                ID_742339479_m.add(ID_742339479_fila);
                ID_742339479_fila.save();
            });
            var ID_1232244780_i = Alloy.createCollection('marcas');
            ID_1232244780_i.fetch();
            var ID_1232244780_src = require('helper').query2array(ID_1232244780_i);
            var datos = [];
            _.each(ID_1232244780_src, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'valor';
                    if (llave == 'id_segured') newkey = 'id_interno';
                    if (llave == 'id_server') newkey = 'id_gerardo';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                datos.push(new_row);
            });
        }
        $.ID_830800377.data({
            data: datos
        });
    };
    var ID_616239583 = setTimeout(ID_616239583_func, 1000 * 0.2);

}

$.ID_165543940.init({
    caja: 55,
    __id: 'ALL165543940',
    onlisto: Listo_ID_1076382620,
    left: 0,
    top: 0,
    onclick: Click_ID_906423246
});

function Click_ID_906423246(e) {

    var evento = e;
    /** 
     * Definimos que estamos capturando foto en el primer item 
     */
    require('vars')['cual_foto'] = '1';
    /** 
     * Abrimos camara 
     */
    $.ID_1510471997.disparar({});
    /** 
     * Detenemos las animaciones de los widget 
     */
    $.ID_165543940.detener({});
    $.ID_1692328525.detener({});
    $.ID_480946597.detener({});

}

function Listo_ID_1076382620(e) {

    var evento = e;
    /** 
     * Recuperamos variables para poder definir la estructura de las carpetas donde se guardara la miniatura 
     */
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    var nuevoid_f1 = ('nuevoid_f1' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['nuevoid_f1'] : '';
    if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
        var ID_1607360754_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + seltarea.id_server);
        if (ID_1607360754_d.exists() == false) ID_1607360754_d.createDirectory();
        var ID_1607360754_f = Ti.Filesystem.getFile(ID_1607360754_d.resolve(), 'mini' + nuevoid_f1.id + '.jpg');
        if (ID_1607360754_f.exists() == true) ID_1607360754_f.deleteFile();
        ID_1607360754_f.write(evento.mini);
        ID_1607360754_d = null;
        ID_1607360754_f = null;
    } else {
        var ID_839982148_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + 0);
        if (ID_839982148_d.exists() == false) ID_839982148_d.createDirectory();
        var ID_839982148_f = Ti.Filesystem.getFile(ID_839982148_d.resolve(), 'mini' + nuevoid_f1.id + '.jpg');
        if (ID_839982148_f.exists() == true) ID_839982148_f.deleteFile();
        ID_839982148_f.write(evento.mini);
        ID_839982148_d = null;
        ID_839982148_f = null;
    }
}

function Click_ID_1786963185(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var evento = e;
    /** 
     * Definimos que estamos capturando foto en el primer item 
     */
    require('vars')['cual_foto'] = '1';
    /** 
     * Abrimos camara 
     */
    $.ID_1510471997.disparar({});
    /** 
     * Detenemos las animaciones de los widget 
     */
    $.ID_165543940.detener({});
    $.ID_1692328525.detener({});
    $.ID_480946597.detener({});
    /** 
     * Ocultamos la imagen obtenida desde la memoria del telefono para actualizar con la que acabamos de obtener 
     */
    var ID_1121726543_visible = false;

    if (ID_1121726543_visible == 'si') {
        ID_1121726543_visible = true;
    } else if (ID_1121726543_visible == 'no') {
        ID_1121726543_visible = false;
    }
    $.ID_1121726543.setVisible(ID_1121726543_visible);


}

$.ID_1692328525.init({
    caja: 55,
    __id: 'ALL1692328525',
    onlisto: Listo_ID_1852812303,
    left: 5,
    top: 0,
    onclick: Click_ID_542646327
});

function Click_ID_542646327(e) {

    var evento = e;
    $.ID_1692328525.detener({});
    $.ID_165543940.detener({});
    $.ID_480946597.detener({});
    require('vars')['cual_foto'] = 2;
    $.ID_1510471997.disparar({});

}

function Listo_ID_1852812303(e) {

    var evento = e;
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    var nuevoid_f2 = ('nuevoid_f2' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['nuevoid_f2'] : '';
    if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
        var ID_1720722806_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + seltarea.id_server);
        if (ID_1720722806_d.exists() == false) ID_1720722806_d.createDirectory();
        var ID_1720722806_f = Ti.Filesystem.getFile(ID_1720722806_d.resolve(), 'mini' + nuevoid_f2.id + '.jpg');
        if (ID_1720722806_f.exists() == true) ID_1720722806_f.deleteFile();
        ID_1720722806_f.write(evento.mini);
        ID_1720722806_d = null;
        ID_1720722806_f = null;
    } else {
        var ID_1840602263_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + 0);
        if (ID_1840602263_d.exists() == false) ID_1840602263_d.createDirectory();
        var ID_1840602263_f = Ti.Filesystem.getFile(ID_1840602263_d.resolve(), 'mini' + nuevoid_f2.id + '.jpg');
        if (ID_1840602263_f.exists() == true) ID_1840602263_f.deleteFile();
        ID_1840602263_f.write(evento.mini);
        ID_1840602263_d = null;
        ID_1840602263_f = null;
    }
}

function Click_ID_1622960878(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var evento = e;
    $.ID_165543940.detener({});
    $.ID_1692328525.detener({});
    $.ID_480946597.detener({});
    require('vars')['cual_foto'] = 2;
    $.ID_1510471997.disparar({});
    var ID_88401902_visible = false;

    if (ID_88401902_visible == 'si') {
        ID_88401902_visible = true;
    } else if (ID_88401902_visible == 'no') {
        ID_88401902_visible = false;
    }
    $.ID_88401902.setVisible(ID_88401902_visible);


}

$.ID_480946597.init({
    caja: 55,
    __id: 'ALL480946597',
    onlisto: Listo_ID_1616905469,
    left: 5,
    top: 0,
    onclick: Click_ID_1334522217
});

function Click_ID_1334522217(e) {

    var evento = e;
    $.ID_480946597.detener({});
    $.ID_165543940.detener({});
    $.ID_1692328525.detener({});
    require('vars')['cual_foto'] = 3;
    $.ID_1510471997.disparar({});

}

function Listo_ID_1616905469(e) {

    var evento = e;
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    var nuevoid_f3 = ('nuevoid_f3' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['nuevoid_f3'] : '';
    if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
        var ID_1303191679_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + seltarea.id_server);
        if (ID_1303191679_d.exists() == false) ID_1303191679_d.createDirectory();
        var ID_1303191679_f = Ti.Filesystem.getFile(ID_1303191679_d.resolve(), 'mini' + nuevoid_f3.id + '.jpg');
        if (ID_1303191679_f.exists() == true) ID_1303191679_f.deleteFile();
        ID_1303191679_f.write(evento.mini);
        ID_1303191679_d = null;
        ID_1303191679_f = null;
    } else {
        var ID_1045133508_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + 0);
        if (ID_1045133508_d.exists() == false) ID_1045133508_d.createDirectory();
        var ID_1045133508_f = Ti.Filesystem.getFile(ID_1045133508_d.resolve(), 'mini' + nuevoid_f3.id + '.jpg');
        if (ID_1045133508_f.exists() == true) ID_1045133508_f.deleteFile();
        ID_1045133508_f.write(evento.mini);
        ID_1045133508_d = null;
        ID_1045133508_f = null;
    }
}

function Click_ID_1577775088(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var evento = e;
    $.ID_165543940.detener({});
    $.ID_1692328525.detener({});
    $.ID_480946597.detener({});
    require('vars')['cual_foto'] = 3;
    $.ID_1510471997.disparar({});
    var ID_1912433313_visible = false;

    if (ID_1912433313_visible == 'si') {
        ID_1912433313_visible = true;
    } else if (ID_1912433313_visible == 'no') {
        ID_1912433313_visible = false;
    }
    $.ID_1912433313.setVisible(ID_1912433313_visible);


}

$.ID_1509904444.init({
    titulo: 'RECINTOS',
    cargando: 'cargando...',
    __id: 'ALL1509904444',
    onrespuesta: Respuesta_ID_1983070902,
    campo: 'Ubicación',
    onabrir: Abrir_ID_925413009,
    color: 'celeste',
    seleccione: 'recinto',
    activo: true,
    onafterinit: Afterinit_ID_938642338
});

function Abrir_ID_925413009(e) {

    var evento = e;

}

function Respuesta_ID_1983070902(e) {

    var evento = e;
    $.ID_1509904444.labels({
        valor: evento.valor
    });
    $.contenido.set({
        id_recinto: evento.item.id_interno
    });
    if ('contenido' in $) $contenido = $.contenido.toJSON();
    if (Ti.App.deployType != 'production') console.log('datos recibidos de modal ubicacion', {
        "datos": evento
    });

}

function Afterinit_ID_938642338(e) {

    var evento = e;
    var ID_279917941_func = function() {
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        var ID_1330667841_i = Alloy.createCollection('insp_contenido');
        var ID_1330667841_i_where = 'id=\'' + args._dato.id + '\'';
        ID_1330667841_i.fetch({
            query: 'SELECT * FROM insp_contenido WHERE id=\'' + args._dato.id + '\''
        });
        var insp_dato = require('helper').query2array(ID_1330667841_i);
        if (insp_dato && insp_dato.length) {
            /** 
             * obtenemos valor seleccionado previamente y lo mostramos en selector. 
             */
            var ID_1090335190_func = function() {
                var ID_696445742_i = Alloy.createCollection('insp_recintos');
                var ID_696445742_i_where = 'id_recinto=\'' + insp_dato[0].id_recinto + '\'';
                ID_696445742_i.fetch({
                    query: 'SELECT * FROM insp_recintos WHERE id_recinto=\'' + insp_dato[0].id_recinto + '\''
                });
                var recintosb = require('helper').query2array(ID_696445742_i);
                if (Ti.App.deployType != 'production') console.log('detalle de recintos', {
                    "datos": recintosb
                });
                if (recintosb && recintosb.length) {
                    $.ID_1509904444.labels({
                        valor: recintosb[0].nombre
                    });
                    if (Ti.App.deployType != 'production') console.log('el nombre de la partida es', {
                        "datos": recintosb[0].nombre
                    });
                }
            };
            var ID_1090335190 = setTimeout(ID_1090335190_func, 1000 * 0.21);
        }
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
            var ID_110640539_i = Alloy.createCollection('insp_recintos');
            var ID_110640539_i_where = 'id_inspeccion=\'' + seltarea.id_server + '\'';
            ID_110640539_i.fetch({
                query: 'SELECT * FROM insp_recintos WHERE id_inspeccion=\'' + seltarea.id_server + '\''
            });
            var recintos = require('helper').query2array(ID_110640539_i);
            var datos = [];
            _.each(recintos, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'valor';
                    if (llave == 'id_recinto') newkey = 'id_interno';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                datos.push(new_row);
            });
        } else {
            var ID_648470310_i = Alloy.Collections.insp_recintos;
            var sql = "DELETE FROM " + ID_648470310_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_648470310_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_648470310_i.trigger('delete');
            var item_index = 0;
            _.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
                item_index += 1;
                var ID_572872424_m = Alloy.Collections.insp_recintos;
                var ID_572872424_fila = Alloy.createModel('insp_recintos', {
                    nombre: 'Pieza' + item,
                    id_recinto: item
                });
                ID_572872424_m.add(ID_572872424_fila);
                ID_572872424_fila.save();
            });
            var ID_617211234_i = Alloy.createCollection('insp_recintos');
            ID_617211234_i.fetch();
            var ID_617211234_src = require('helper').query2array(ID_617211234_i);
            var datos = [];
            _.each(ID_617211234_src, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'valor';
                    if (llave == 'id_recinto') newkey = 'id_interno';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                datos.push(new_row);
            });
        }
        $.ID_1509904444.data({
            data: datos
        });
    };
    var ID_279917941 = setTimeout(ID_279917941_func, 1000 * 0.2);

}

function Change_ID_1607477993(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    $.contenido.set({
        cantidad: elemento
    });
    if ('contenido' in $) $contenido = $.contenido.toJSON();
    elemento = null, source = null;

}

function Click_ID_489897532(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    $.ID_1679993746.blur();
    $.ID_232482976.blur();
    $.ID_1816558286.blur();
    $.ID_713928716.abrir({});

}

$.ID_1620626702.init({
    titulo: 'MONEDAS',
    cargando: 'cargando...',
    __id: 'ALL1620626702',
    onrespuesta: Respuesta_ID_965340179,
    campo: 'Moneda',
    onabrir: Abrir_ID_1343075546,
    color: 'celeste',
    seleccione: '-',
    activo: true,
    onafterinit: Afterinit_ID_112420961
});

function Afterinit_ID_112420961(e) {

    var evento = e;
    var ID_1876482487_func = function() {
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        var ID_1944241185_i = Alloy.createCollection('insp_contenido');
        var ID_1944241185_i_where = 'id=\'' + args._dato.id + '\'';
        ID_1944241185_i.fetch({
            query: 'SELECT * FROM insp_contenido WHERE id=\'' + args._dato.id + '\''
        });
        var insp_dato = require('helper').query2array(ID_1944241185_i);
        if (insp_dato && insp_dato.length) {
            var ID_706185240_func = function() {
                var ID_71348223_i = Alloy.createCollection('monedas');
                var ID_71348223_i_where = 'id_segured=\'' + insp_dato[0].id_moneda + '\'';
                ID_71348223_i.fetch({
                    query: 'SELECT * FROM monedas WHERE id_segured=\'' + insp_dato[0].id_moneda + '\''
                });
                var monedas = require('helper').query2array(ID_71348223_i);
                if (monedas && monedas.length) {
                    $.ID_1620626702.labels({
                        valor: monedas[0].nombre
                    });
                }
            };
            var ID_706185240 = setTimeout(ID_706185240_func, 1000 * 0.21);
        }
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
            /** 
             * Obtenemos datos para selectores (solo los de este pais) 
             */
            var ID_1248238750_i = Alloy.createCollection('monedas');
            var ID_1248238750_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
            ID_1248238750_i.fetch({
                query: 'SELECT * FROM monedas WHERE pais_texto=\'' + pais[0].nombre + '\''
            });
            var monedas = require('helper').query2array(ID_1248238750_i);
            var datos = [];
            _.each(monedas, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'valor';
                    if (llave == 'id_segured') newkey = 'id_interno';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                datos.push(new_row);
            });
        } else {
            /** 
             * Insertamos dummies 
             */
            /** 
             * Insertamos dummies 
             */
            var ID_574789074_i = Alloy.Collections.monedas;
            var sql = "DELETE FROM " + ID_574789074_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_574789074_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_574789074_i.trigger('delete');
            var item_index = 0;
            _.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
                item_index += 1;
                var ID_1171924818_m = Alloy.Collections.monedas;
                var ID_1171924818_fila = Alloy.createModel('monedas', {
                    nombre: 'CLP' + item,
                    id_segured: '10' + item
                });
                ID_1171924818_m.add(ID_1171924818_fila);
                ID_1171924818_fila.save();
            });
            var ID_1536424882_i = Alloy.createCollection('monedas');
            ID_1536424882_i.fetch();
            var ID_1536424882_src = require('helper').query2array(ID_1536424882_i);
            var datos = [];
            _.each(ID_1536424882_src, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'valor';
                    if (llave == 'id_segured') newkey = 'id_interno';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                datos.push(new_row);
            });
        }
        $.ID_1620626702.data({
            data: datos
        });
    };
    var ID_1876482487 = setTimeout(ID_1876482487_func, 1000 * 0.2);

}

function Abrir_ID_1343075546(e) {

    var evento = e;
    $.ID_713928716.cerrar({});

}

function Respuesta_ID_965340179(e) {

    var evento = e;
    /** 
     * Mostramos el valor seleccionado desde el widget 
     */
    $.ID_1620626702.labels({
        valor: evento.valor
    });
    /** 
     * Actualizamos la tabla ingresando el id de moneda 
     */
    $.contenido.set({
        id_moneda: evento.item.id_interno
    });
    if ('contenido' in $) $contenido = $.contenido.toJSON();

}

function Change_ID_726214756(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    $.contenido.set({
        valor: elemento
    });
    if ('contenido' in $) $contenido = $.contenido.toJSON();
    elemento = null, source = null;

}

function Return_ID_1846920641(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    $.ID_232482976.blur();
    elemento = null, source = null;

}

function Change_ID_1637073479(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    if (elemento == true || elemento == 'true') {
        /** 
         * Dependiendo del estado del switch es el texto a mostrar y actualizacion a la tabla de contenido 
         */
        var si = 'SI';
        $.ID_624233572.setText(si);

        $.contenido.set({
            recupero: 1
        });
        if ('contenido' in $) $contenido = $.contenido.toJSON();
    } else {
        var no = 'NO';
        $.ID_624233572.setText(no);

        $.contenido.set({
            recupero: 0
        });
        if ('contenido' in $) $contenido = $.contenido.toJSON();
    }
    elemento = null;

}

function Change_ID_1205315875(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    $.contenido.set({
        descripcion: elemento
    });
    if ('contenido' in $) $contenido = $.contenido.toJSON();
    elemento = null, source = null;

}

$.ID_1510471997.init({
    onfotolista: Fotolista_ID_1262027731,
    __id: 'ALL1510471997'
});

function Fotolista_ID_1262027731(e) {

    var evento = e;
    /** 
     * Recuperamos cual fue la foto seleccionada 
     */
    var cual_foto = ('cual_foto' in require('vars')) ? require('vars')['cual_foto'] : '';
    if (cual_foto == 1 || cual_foto == '1') {
        var ID_1647441258_m = Alloy.Collections.numero_unico;
        var ID_1647441258_fila = Alloy.createModel('numero_unico', {
            comentario: 'nuevo danocontenido foto1'
        });
        ID_1647441258_m.add(ID_1647441258_fila);
        ID_1647441258_fila.save();
        var nuevoid_f1 = require('helper').model2object(ID_1647441258_m.last());
        /** 
         * Guardamos en variable el detalle del modelo numero_unico para asignar informacion de foto1 del dano 
         */
        require('vars')[_var_scopekey]['nuevoid_f1'] = nuevoid_f1;
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        var gps_coords = ('gps_coords' in require('vars')) ? require('vars')['gps_coords'] : '';
        var gps_latitud = ('gps_latitud' in require('vars')) ? require('vars')['gps_latitud'] : '';
        var gps_longitud = ('gps_longitud' in require('vars')) ? require('vars')['gps_longitud'] : '';
        var resultado = null;
        if ('distanceinmtbetweenearthcoordinates' in require('funciones')) {
            resultado = require('funciones').distanceinmtbetweenearthcoordinates({
                'lat1': gps_latitud,
                'lon1': gps_longitud,
                'lat2': seltarea.lat,
                'lon2': seltarea.lon
            });
        } else {
            try {
                resultado = f_distanceinmtbetweenearthcoordinates({
                    'lat1': gps_latitud,
                    'lon1': gps_longitud,
                    'lat2': seltarea.lat,
                    'lon2': seltarea.lon
                });
            } catch (ee) {}
        }
        if (_.isNumber(gps_coords.accuracy) && _.isNumber(resultado) && gps_coords.accuracy > resultado) {
            /** 
             * Pablo me comento que el flag fuera_rango se determina comparando precision vs la distancia de ubicacion con la definida en tarea 
             */
            var json_datosfoto = {
                lat: gps_latitud,
                lng: gps_longitud,
                fuera_rango: 0,
                distancia: Math.round(resultado),
                precision: Math.round(gps_coords.accuracy)
            };
        } else {
            var json_datosfoto = {
                lat: gps_latitud,
                lng: gps_longitud,
                fuera_rango: 1,
                distancia: Math.round(resultado),
                precision: Math.round(gps_coords.accuracy)
            };
        }
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var ID_1396057589_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
            if (ID_1396057589_d.exists() == false) ID_1396057589_d.createDirectory();
            var ID_1396057589_f = Ti.Filesystem.getFile(ID_1396057589_d.resolve(), 'cap' + nuevoid_f1.id + '.jpg');
            if (ID_1396057589_f.exists() == true) ID_1396057589_f.deleteFile();
            ID_1396057589_f.write(evento.foto);
            ID_1396057589_d = null;
            ID_1396057589_f = null;
            var ID_258336696_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
            if (ID_258336696_d.exists() == false) ID_258336696_d.createDirectory();
            var ID_258336696_f = Ti.Filesystem.getFile(ID_258336696_d.resolve(), 'cap' + nuevoid_f1.id + '.json');
            if (ID_258336696_f.exists() == true) ID_258336696_f.deleteFile();
            console.log('contenido f1 econtenido', JSON.stringify(json_datosfoto));
            ID_258336696_f.write(JSON.stringify(json_datosfoto));
            ID_258336696_d = null;
            ID_258336696_f = null;
        } else {
            var ID_531334712_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + 0);
            if (ID_531334712_d.exists() == false) ID_531334712_d.createDirectory();
            var ID_531334712_f = Ti.Filesystem.getFile(ID_531334712_d.resolve(), 'cap' + nuevoid_f1.id + '.jpg');
            if (ID_531334712_f.exists() == true) ID_531334712_f.deleteFile();
            ID_531334712_f.write(evento.foto);
            ID_531334712_d = null;
            ID_531334712_f = null;
        }
        /** 
         * Actualizamos el modelo indicando cual es el nombre de la imagen 
         */
        $.contenido.set({
            foto1: 'cap' + nuevoid_f1.id + '.jpg'
        });
        if ('contenido' in $) $contenido = $.contenido.toJSON();
        /** 
         * Enviamos la foto para que genere la miniatura (y tambien la guarde) y muestre en pantalla 
         */
        $.ID_165543940.procesar({
            imagen: evento.foto,
            nueva: 640,
            calidad: 91,
            camara: 'trasera'
        });
        /** 
         * Limpiamos memoria ram 
         */
        evento = null;
    } else if (cual_foto == 2) {
        var ID_1696372021_m = Alloy.Collections.numero_unico;
        var ID_1696372021_fila = Alloy.createModel('numero_unico', {
            comentario: 'nuevo danocontenido foto2'
        });
        ID_1696372021_m.add(ID_1696372021_fila);
        ID_1696372021_fila.save();
        var nuevoid_f2 = require('helper').model2object(ID_1696372021_m.last());
        /** 
         * Guardamos en variable el detalle del modelo numero_unico para asignar informacion de foto1 del dano 
         */
        require('vars')[_var_scopekey]['nuevoid_f2'] = nuevoid_f2;
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        var gps_coords = ('gps_coords' in require('vars')) ? require('vars')['gps_coords'] : '';
        var gps_latitud = ('gps_latitud' in require('vars')) ? require('vars')['gps_latitud'] : '';
        var gps_longitud = ('gps_longitud' in require('vars')) ? require('vars')['gps_longitud'] : '';
        var resultado = null;
        if ('distanceinmtbetweenearthcoordinates' in require('funciones')) {
            resultado = require('funciones').distanceinmtbetweenearthcoordinates({
                'lat1': gps_latitud,
                'lon1': gps_longitud,
                'lat2': seltarea.lat,
                'lon2': seltarea.lon
            });
        } else {
            try {
                resultado = f_distanceinmtbetweenearthcoordinates({
                    'lat1': gps_latitud,
                    'lon1': gps_longitud,
                    'lat2': seltarea.lat,
                    'lon2': seltarea.lon
                });
            } catch (ee) {}
        }
        if (_.isNumber(gps_coords.accuracy) && _.isNumber(resultado) && gps_coords.accuracy > resultado) {
            /** 
             * Pablo me comento que el flag fuera_rango se determina comparando precision vs la distancia de ubicacion con la definida en tarea 
             */
            var json_datosfoto = {
                lat: gps_latitud,
                lng: gps_longitud,
                fuera_rango: 0,
                distancia: Math.round(resultado),
                precision: Math.round(gps_coords.accuracy)
            };
        } else {
            var json_datosfoto = {
                lat: gps_latitud,
                lng: gps_longitud,
                fuera_rango: 1,
                distancia: Math.round(resultado),
                precision: Math.round(gps_coords.accuracy)
            };
        }
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var ID_1842918566_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
            if (ID_1842918566_d.exists() == false) ID_1842918566_d.createDirectory();
            var ID_1842918566_f = Ti.Filesystem.getFile(ID_1842918566_d.resolve(), 'cap' + nuevoid_f2.id + '.jpg');
            if (ID_1842918566_f.exists() == true) ID_1842918566_f.deleteFile();
            ID_1842918566_f.write(evento.foto);
            ID_1842918566_d = null;
            ID_1842918566_f = null;
            var ID_305058593_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
            if (ID_305058593_d.exists() == false) ID_305058593_d.createDirectory();
            var ID_305058593_f = Ti.Filesystem.getFile(ID_305058593_d.resolve(), 'cap' + nuevoid_f2.id + '.json');
            if (ID_305058593_f.exists() == true) ID_305058593_f.deleteFile();
            console.log('contenido f2 econtenido', JSON.stringify(json_datosfoto));
            ID_305058593_f.write(JSON.stringify(json_datosfoto));
            ID_305058593_d = null;
            ID_305058593_f = null;
        } else {
            var ID_79663987_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + 0);
            if (ID_79663987_d.exists() == false) ID_79663987_d.createDirectory();
            var ID_79663987_f = Ti.Filesystem.getFile(ID_79663987_d.resolve(), 'cap' + nuevoid_f2.id + '.jpg');
            if (ID_79663987_f.exists() == true) ID_79663987_f.deleteFile();
            ID_79663987_f.write(evento.foto);
            ID_79663987_d = null;
            ID_79663987_f = null;
        }
        $.contenido.set({
            foto2: 'cap' + nuevoid_f2.id + '.jpg'
        });
        if ('contenido' in $) $contenido = $.contenido.toJSON();
        $.ID_1692328525.procesar({
            imagen: evento.foto,
            nueva: 640,
            calidad: 91,
            camara: 'trasera'
        });
        evento = null;
    } else if (cual_foto == 3) {
        var ID_1835020129_m = Alloy.Collections.numero_unico;
        var ID_1835020129_fila = Alloy.createModel('numero_unico', {
            comentario: 'nuevo danocontenido foto3'
        });
        ID_1835020129_m.add(ID_1835020129_fila);
        ID_1835020129_fila.save();
        var nuevoid_f3 = require('helper').model2object(ID_1835020129_m.last());
        /** 
         * Guardamos en variable el detalle del modelo numero_unico para asignar informacion de foto1 del dano 
         */
        require('vars')[_var_scopekey]['nuevoid_f3'] = nuevoid_f3;
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        var gps_coords = ('gps_coords' in require('vars')) ? require('vars')['gps_coords'] : '';
        var gps_latitud = ('gps_latitud' in require('vars')) ? require('vars')['gps_latitud'] : '';
        var gps_longitud = ('gps_longitud' in require('vars')) ? require('vars')['gps_longitud'] : '';
        var resultado = null;
        if ('distanceinmtbetweenearthcoordinates' in require('funciones')) {
            resultado = require('funciones').distanceinmtbetweenearthcoordinates({
                'lat1': gps_latitud,
                'lon1': gps_longitud,
                'lat2': seltarea.lat,
                'lon2': seltarea.lon
            });
        } else {
            try {
                resultado = f_distanceinmtbetweenearthcoordinates({
                    'lat1': gps_latitud,
                    'lon1': gps_longitud,
                    'lat2': seltarea.lat,
                    'lon2': seltarea.lon
                });
            } catch (ee) {}
        }
        if (_.isNumber(gps_coords.accuracy) && _.isNumber(resultado) && gps_coords.accuracy > resultado) {
            /** 
             * Pablo me comento que el flag fuera_rango se determina comparando precision vs la distancia de ubicacion con la definida en tarea 
             */
            var json_datosfoto = {
                lat: gps_latitud,
                lng: gps_longitud,
                fuera_rango: 0,
                distancia: Math.round(resultado),
                precision: Math.round(gps_coords.accuracy)
            };
        } else {
            var json_datosfoto = {
                lat: gps_latitud,
                lng: gps_longitud,
                fuera_rango: 1,
                distancia: Math.round(resultado),
                precision: Math.round(gps_coords.accuracy)
            };
        }
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var ID_290803720_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
            if (ID_290803720_d.exists() == false) ID_290803720_d.createDirectory();
            var ID_290803720_f = Ti.Filesystem.getFile(ID_290803720_d.resolve(), 'cap' + nuevoid_f3.id + '.jpg');
            if (ID_290803720_f.exists() == true) ID_290803720_f.deleteFile();
            ID_290803720_f.write(evento.foto);
            ID_290803720_d = null;
            ID_290803720_f = null;
            var ID_1501646279_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
            if (ID_1501646279_d.exists() == false) ID_1501646279_d.createDirectory();
            var ID_1501646279_f = Ti.Filesystem.getFile(ID_1501646279_d.resolve(), 'cap' + nuevoid_f3.id + '.json');
            if (ID_1501646279_f.exists() == true) ID_1501646279_f.deleteFile();
            console.log('contenido f3 econtenido', JSON.stringify(json_datosfoto));
            ID_1501646279_f.write(JSON.stringify(json_datosfoto));
            ID_1501646279_d = null;
            ID_1501646279_f = null;
        } else {
            var ID_641260393_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + 0);
            if (ID_641260393_d.exists() == false) ID_641260393_d.createDirectory();
            var ID_641260393_f = Ti.Filesystem.getFile(ID_641260393_d.resolve(), 'cap' + nuevoid_f2.id + '.jpg');
            if (ID_641260393_f.exists() == true) ID_641260393_f.deleteFile();
            ID_641260393_f.write(evento.foto);
            ID_641260393_d = null;
            ID_641260393_f = null;
        }
        $.contenido.set({
            foto3: 'cap' + nuevoid_f3.id + '.jpg'
        });
        if ('contenido' in $) $contenido = $.contenido.toJSON();
        $.ID_480946597.procesar({
            imagen: evento.foto,
            nueva: 640,
            calidad: 91,
            camara: 'trasera'
        });
        evento = null;
    }
}

$.ID_713928716.init({
    __id: 'ALL713928716',
    aceptar: 'Aceptar',
    cancelar: 'Cancelar',
    onaceptar: Aceptar_ID_1635793771,
    oncancelar: Cancelar_ID_1317484105
});

function Aceptar_ID_1635793771(e) {

    var evento = e;
    /** 
     * Formateamos la fecha que responde el widget, mostramos en pantalla y actualizamos la fecha de compra 
     */
    var moment = require('alloy/moment');
    var ID_1034628459 = evento.valor;
    var resp = moment(ID_1034628459).format('DD-MM-YYYY');
    $.ID_886249993.setText(resp);

    var ID_886249993_estilo = 'estilo12';

    var _tmp_a4w = require('a4w');
    if ((typeof ID_886249993_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (ID_886249993_estilo in _tmp_a4w.styles['classes'])) {
        try {
            ID_886249993_estilo = _tmp_a4w.styles['classes'][ID_886249993_estilo];
        } catch (st_val_err) {}
    }
    _tmp_a4w = null;
    $.ID_886249993.applyProperties(ID_886249993_estilo);

    $.contenido.set({
        fecha_compra: evento.valor
    });
    if ('contenido' in $) $contenido = $.contenido.toJSON();

}

function Cancelar_ID_1317484105(e) {

    var evento = e;
    if (Ti.App.deployType != 'production') console.log('selector de fecha cancelado', {});

}

(function() {
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
        /** 
         * Actualizamos el id de la inspeccion en la tabla de contenidos 
         */
        $.contenido.set({
            id_inspeccion: seltarea.id_server
        });
        if ('contenido' in $) $contenido = $.contenido.toJSON();
    }
    /** 
     * heredamos datos previos de nivel en modelo nuevo con binding de esta pantalla 
     */
    var ID_455760152_i = Alloy.createCollection('insp_contenido');
    var ID_455760152_i_where = 'id=\'' + args._dato.id + '\'';
    ID_455760152_i.fetch({
        query: 'SELECT * FROM insp_contenido WHERE id=\'' + args._dato.id + '\''
    });
    var insp_c = require('helper').query2array(ID_455760152_i);
    /** 
     * Cargamos los datos en la tabla 
     */
    $.contenido.set({
        id_moneda: insp_c[0].id_moneda,
        fecha_compra: insp_c[0].fecha_compra,
        id_inspeccion: insp_c[0].id_inspeccion,
        nombre: insp_c[0].nombre,
        cantidad: insp_c[0].cantidad,
        foto1: insp_c[0].foto1,
        id_nombre: insp_c[0].id_nombre,
        foto2: insp_c[0].foto2,
        id_recinto: insp_c[0].id_recinto,
        id_marca: insp_c[0].id_marca,
        foto3: insp_c[0].foto3,
        recupero: insp_c[0].recupero,
        valor: insp_c[0].valor,
        descripcion: insp_c[0].descripcion
    });
    if ('contenido' in $) $contenido = $.contenido.toJSON();
    var moment = require('alloy/moment');
    var ID_515395357 = insp_c[0].fecha_compra;
    var fecha_compra = moment(ID_515395357).format('DD-MM-YYYY');
    /** 
     * Cargamos textos en la pantalla 
     */
    var ID_886249993_estilo = 'estilo12';

    var _tmp_a4w = require('a4w');
    if ((typeof ID_886249993_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (ID_886249993_estilo in _tmp_a4w.styles['classes'])) {
        try {
            ID_886249993_estilo = _tmp_a4w.styles['classes'][ID_886249993_estilo];
        } catch (st_val_err) {}
    }
    _tmp_a4w = null;
    $.ID_886249993.applyProperties(ID_886249993_estilo);

    $.ID_886249993.setText(fecha_compra);

    $.ID_1699546110.setText(insp_c[0].nombre);

    $.ID_1699546110.setColor('#000000');

    $.ID_1816558286.setValue(insp_c[0].cantidad);

    $.ID_232482976.setValue(insp_c[0].valor);

    $.ID_1679993746.setValue(insp_c[0].descripcion);

    if (insp_c[0].recupero == 1 || insp_c[0].recupero == '1') {
        /** 
         * Modificamos el estado del switch 
         */
        $.ID_1002331086.setValue(true);

        $.ID_624233572.setText('SI');

    } else {
        $.ID_1002331086.setValue('false');

        $.ID_624233572.setText('NO');

    }
    if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
        /** 
         * Verificamos si las fotos estan en la carpeta de inspeccion o es un dummy 
         */
        var ID_150567229_trycatch = {
            error: function(e) {
                if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
            }
        };
        try {
            ID_150567229_trycatch.error = function(evento) {};
            insp_c[0].foto1 = "mini" + insp_c[0].foto1.substring(3);
            var foto_1 = '';
            var ID_1235579640_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + seltarea.id_server);
            if (ID_1235579640_d.exists() == true) {
                var ID_1235579640_f = Ti.Filesystem.getFile(ID_1235579640_d.resolve(), insp_c[0].foto1);
                if (ID_1235579640_f.exists() == true) {
                    foto_1 = ID_1235579640_f.read();
                }
                ID_1235579640_f = null;
            }
            ID_1235579640_d = null;
        } catch (e) {
            ID_150567229_trycatch.error(e);
        }
        var ID_587453623_trycatch = {
            error: function(e) {
                if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
            }
        };
        try {
            ID_587453623_trycatch.error = function(evento) {};
            insp_c[0].foto2 = "mini" + insp_c[0].foto2.substring(3);
            var foto_2 = '';
            var ID_1749644865_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + seltarea.id_server);
            if (ID_1749644865_d.exists() == true) {
                var ID_1749644865_f = Ti.Filesystem.getFile(ID_1749644865_d.resolve(), insp_c[0].foto2);
                if (ID_1749644865_f.exists() == true) {
                    foto_2 = ID_1749644865_f.read();
                }
                ID_1749644865_f = null;
            }
            ID_1749644865_d = null;
        } catch (e) {
            ID_587453623_trycatch.error(e);
        }
        var ID_157007749_trycatch = {
            error: function(e) {
                if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
            }
        };
        try {
            ID_157007749_trycatch.error = function(evento) {};
            insp_c[0].foto3 = "mini" + insp_c[0].foto3.substring(3);
            var foto_3 = '';
            var ID_61377515_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + seltarea.id_server);
            if (ID_61377515_d.exists() == true) {
                var ID_61377515_f = Ti.Filesystem.getFile(ID_61377515_d.resolve(), insp_c[0].foto3);
                if (ID_61377515_f.exists() == true) {
                    foto_3 = ID_61377515_f.read();
                }
                ID_61377515_f = null;
            }
            ID_61377515_d = null;
        } catch (e) {
            ID_157007749_trycatch.error(e);
        }
    } else {
        var ID_1956266938_trycatch = {
            error: function(e) {
                if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
            }
        };
        try {
            ID_1956266938_trycatch.error = function(evento) {};
            insp_c[0].foto1 = "mini" + insp_c[0].foto1.substring(3);
            var foto_1 = '';
            var ID_216147007_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + 0);
            if (ID_216147007_d.exists() == true) {
                var ID_216147007_f = Ti.Filesystem.getFile(ID_216147007_d.resolve(), insp_c[0].foto1);
                if (ID_216147007_f.exists() == true) {
                    foto_1 = ID_216147007_f.read();
                }
                ID_216147007_f = null;
            }
            ID_216147007_d = null;
        } catch (e) {
            ID_1956266938_trycatch.error(e);
        }
        var ID_742064338_trycatch = {
            error: function(e) {
                if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
            }
        };
        try {
            ID_742064338_trycatch.error = function(evento) {};
            insp_c[0].foto2 = "mini" + insp_c[0].foto2.substring(3);
            var foto_2 = '';
            var ID_269412720_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + 0);
            if (ID_269412720_d.exists() == true) {
                var ID_269412720_f = Ti.Filesystem.getFile(ID_269412720_d.resolve(), insp_c[0].foto2);
                if (ID_269412720_f.exists() == true) {
                    foto_2 = ID_269412720_f.read();
                }
                ID_269412720_f = null;
            }
            ID_269412720_d = null;
        } catch (e) {
            ID_742064338_trycatch.error(e);
        }
        var ID_1669864641_trycatch = {
            error: function(e) {
                if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
            }
        };
        try {
            ID_1669864641_trycatch.error = function(evento) {};
            insp_c[0].foto3 = "mini" + insp_c[0].foto3.substring(3);
            var foto_3 = '';
            var ID_1551657778_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + 0);
            if (ID_1551657778_d.exists() == true) {
                var ID_1551657778_f = Ti.Filesystem.getFile(ID_1551657778_d.resolve(), insp_c[0].foto3);
                if (ID_1551657778_f.exists() == true) {
                    foto_3 = ID_1551657778_f.read();
                }
                ID_1551657778_f = null;
            }
            ID_1551657778_d = null;
        } catch (e) {
            ID_1669864641_trycatch.error(e);
        }
    }
    if ((_.isObject(foto_1) || (_.isString(foto_1)) && !_.isEmpty(foto_1)) || _.isNumber(foto_1) || _.isBoolean(foto_1)) {
        /** 
         * Revisamos si las fotos existen, y modificamos la imagen en la vista para cargar la que esta en la memoria del equipo 
         */
        var ID_1121726543_visible = true;

        if (ID_1121726543_visible == 'si') {
            ID_1121726543_visible = true;
        } else if (ID_1121726543_visible == 'no') {
            ID_1121726543_visible = false;
        }
        $.ID_1121726543.setVisible(ID_1121726543_visible);

        var ID_1282626517_imagen = foto_1;

        if (typeof ID_1282626517_imagen == 'string' && 'styles' in require('a4w') && ID_1282626517_imagen in require('a4w').styles['images']) {
            ID_1282626517_imagen = require('a4w').styles['images'][ID_1282626517_imagen];
        }
        $.ID_1282626517.setImage(ID_1282626517_imagen);

        if ((Ti.Platform.manufacturer) == 'samsung') {
            var ID_1121726543_rotar = 90;

            var setRotacion = function(angulo) {
                $.ID_1121726543.setTransform(Ti.UI.create2DMatrix().rotate(angulo));
            };
            setRotacion(ID_1121726543_rotar);

        }
    }
    if ((_.isObject(foto_2) || (_.isString(foto_2)) && !_.isEmpty(foto_2)) || _.isNumber(foto_2) || _.isBoolean(foto_2)) {
        var ID_88401902_visible = true;

        if (ID_88401902_visible == 'si') {
            ID_88401902_visible = true;
        } else if (ID_88401902_visible == 'no') {
            ID_88401902_visible = false;
        }
        $.ID_88401902.setVisible(ID_88401902_visible);

        var ID_1754166454_imagen = foto_2;

        if (typeof ID_1754166454_imagen == 'string' && 'styles' in require('a4w') && ID_1754166454_imagen in require('a4w').styles['images']) {
            ID_1754166454_imagen = require('a4w').styles['images'][ID_1754166454_imagen];
        }
        $.ID_1754166454.setImage(ID_1754166454_imagen);

        if ((Ti.Platform.manufacturer) == 'samsung') {
            var ID_88401902_rotar = 90;

            var setRotacion = function(angulo) {
                $.ID_88401902.setTransform(Ti.UI.create2DMatrix().rotate(angulo));
            };
            setRotacion(ID_88401902_rotar);

        }
    }
    if ((_.isObject(foto_3) || (_.isString(foto_3)) && !_.isEmpty(foto_3)) || _.isNumber(foto_3) || _.isBoolean(foto_3)) {
        var ID_1912433313_visible = true;

        if (ID_1912433313_visible == 'si') {
            ID_1912433313_visible = true;
        } else if (ID_1912433313_visible == 'no') {
            ID_1912433313_visible = false;
        }
        $.ID_1912433313.setVisible(ID_1912433313_visible);

        var ID_1400926341_imagen = foto_3;

        if (typeof ID_1400926341_imagen == 'string' && 'styles' in require('a4w') && ID_1400926341_imagen in require('a4w').styles['images']) {
            ID_1400926341_imagen = require('a4w').styles['images'][ID_1400926341_imagen];
        }
        $.ID_1400926341.setImage(ID_1400926341_imagen);

        if ((Ti.Platform.manufacturer) == 'samsung') {
            var ID_1912433313_rotar = 90;

            var setRotacion = function(angulo) {
                $.ID_1912433313.setTransform(Ti.UI.create2DMatrix().rotate(angulo));
            };
            setRotacion(ID_1912433313_rotar);

        }
    }
    /** 
     * Limpiamos variables 
     */
    foto_1 = null;
    foto_2 = null;
    foto_3 = null;
    /** 
     * Dejamos el scroll en el top y desenfocamos los campos de texto 
     */
    var ID_896487059_func = function() {
        $.ID_1094009628.scrollToTop();
        $.ID_1679993746.blur();
        $.ID_232482976.blur();
        $.ID_1816558286.blur();
    };
    var ID_896487059 = setTimeout(ID_896487059_func, 1000 * 0.2);
    /** 
     * Modificamos el statusbar 
     */
    var ID_460312060_func = function() {
        var ID_1499400166_statusbar = '#239EC4';

        var setearStatusColor = function(ID_1499400166_statusbar) {
            if (OS_IOS) {
                if (ID_1499400166_statusbar == 'light' || ID_1499400166_statusbar == 'claro') {
                    $.ID_1499400166_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.LIGHT_CONTENT);
                } else if (ID_1499400166_statusbar == 'grey' || ID_1499400166_statusbar == 'gris' || ID_1499400166_statusbar == 'gray') {
                    $.ID_1499400166_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.GREY);
                } else if (ID_1499400166_statusbar == 'oscuro' || ID_1499400166_statusbar == 'dark') {
                    $.ID_1499400166_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.DEFAULT);
                }
            } else if (OS_ANDROID) {
                abx.setStatusbarColor(ID_1499400166_statusbar);
            }
        };
        setearStatusColor(ID_1499400166_statusbar);

    };
    var ID_460312060 = setTimeout(ID_460312060_func, 1000 * 0.1);
    _my_events['resp_dato1,ID_149158092'] = function(evento) {
        if (Ti.App.deployType != 'production') console.log('detalle de la respuesta', {
            "datos": evento
        });
        $.ID_1699546110.setText(evento.nombre);

        $.ID_1699546110.setColor('#000000');

        /** 
         * Asumimos que el valor mostrado seleccionado es el nombre de la fila dano, ya que no existe el campo nombre en esta pantalla. 
         */
        $.contenido.set({
            nombre: evento.nombre,
            id_nombre: evento.id_segured
        });
        if ('contenido' in $) $contenido = $.contenido.toJSON();
    };
    Alloy.Events.on('resp_dato1', _my_events['resp_dato1,ID_149158092']);
})();

function Postlayout_ID_709893948(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var ID_575874735_func = function() {
        /** 
         * Una vez ya cargada la tarea, dejamos la variable limpia para poder abrir otras pantallas 
         */
        require('vars')['var_abriendo'] = '';
    };
    var ID_575874735 = setTimeout(ID_575874735_func, 1000 * 0.2);

}