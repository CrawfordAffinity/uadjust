var _bind4section = {
    "ref1": "insp_niveles"
};
var _list_templates = {
    "elemento": {
        "ID_432412973": {},
        "ID_291370846": {
            "text": "{nombre}"
        },
        "ID_1925487537": {
            "text": "{id_server}"
        },
        "ID_975786586": {
            "text": "{id_segured}"
        },
        "ID_1546190443": {}
    },
    "dano": {
        "ID_1569764493": {
            "text": "{id}"
        },
        "ID_304218376": {},
        "ID_1912165224": {
            "text": "{nombre}"
        }
    },
    "pborrar": {
        "ID_1236907100": {
            "text": "{id}"
        },
        "ID_1506538078": {},
        "ID_2024360383": {},
        "ID_1765564351": {},
        "ID_1699280427": {
            "text": "{nombre}"
        },
        "ID_1445903745": {},
        "ID_1795050796": {},
        "ID_1752901998": {},
        "ID_1914367022": {
            "text": "{nombre}"
        },
        "ID_1249074865": {
            "text": "{id}"
        },
        "ID_1131557960": {},
        "ID_1361874287": {},
        "ID_1750057271": {},
        "ID_1962135207": {},
        "ID_1947166298": {
            "text": "{nombre}"
        },
        "ID_2008605730": {
            "text": "{id}"
        },
        "ID_1788775425": {},
        "ID_1246915111": {}
    },
    "contenido": {
        "ID_630800887": {},
        "ID_862352109": {
            "text": "{id}"
        },
        "ID_560452274": {
            "text": "{nombre}"
        }
    },
    "recinto": {
        "ID_1960582596": {},
        "ID_871581291": {
            "text": "{nombre}"
        },
        "ID_310297848": {
            "text": "{id}"
        }
    },
    "nivel": {
        "ID_1021786928": {
            "text": "{id}"
        },
        "ID_1241655463": {
            "text": "{nombre}"
        },
        "ID_599581692": {}
    }
};
var $datos = $.datos.toJSON();

var _activity;
if (OS_ANDROID) {
    _activity = $.ID_1944128176.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_1944128176';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_1944128176.addEventListener('open', function(e) {});
}
$.ID_1944128176.orientationModes = [Titanium.UI.PORTRAIT];

/** 
 * Consulta los datos ingresados en la pantalla de niveles y permite desplegarlo en un listado de esta pantalla 
 */
var ID_1108412687_like = function(search) {
    if (typeof search !== 'string' || this === null) {
        return false;
    }
    search = search.replace(new RegExp("([\\.\\\\\\+\\*\\?\\[\\^\\]\\$\\(\\)\\{\\}\\=\\!\\<\\>\\|\\:\\-])", "g"), "\\$1");
    search = search.replace(/%/g, '.*').replace(/_/g, '.');
    return RegExp('^' + search + '$', 'gi').test(this);
};
var ID_1108412687_filter = function(coll) {
    var filtered = _.toArray(coll.filter(function(m) {
        return true;
    }));
    return filtered;
};
var ID_1108412687_transform = function(model) {
    var modelo = model.toJSON();
    return modelo;
};
_.defer(function() {
    Alloy.Collections.insp_niveles.fetch();
});
Alloy.Collections.insp_niveles.on('add change delete', function(ee) {
    ID_1108412687_update(ee);
});
var ID_1108412687_update = function(evento) {
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    var ID_1270536060_i = Alloy.createCollection('insp_niveles');
    var ID_1270536060_i_where = 'id_inspeccion=\'' + seltarea.id_server + '\'';
    ID_1270536060_i.fetch({
        query: 'SELECT * FROM insp_niveles WHERE id_inspeccion=\'' + seltarea.id_server + '\''
    });
    var insp_n = require('helper').query2array(ID_1270536060_i);
    var alto = 55 * insp_n.length;
    var ID_1140450893_alto = alto;

    if (ID_1140450893_alto == '*') {
        ID_1140450893_alto = Ti.UI.FILL;
    } else if (!isNaN(ID_1140450893_alto)) {
        ID_1140450893_alto = ID_1140450893_alto + 'dp';
    }
    $.ID_1140450893.setHeight(ID_1140450893_alto);

};
Alloy.Collections.insp_niveles.fetch();


$.ID_2135024522.init({
    titulo: 'CARACTERISTICAS',
    onsalirinsp: Salirinsp_ID_317287325,
    __id: 'ALL2135024522',
    continuar: '',
    salir_insp: '',
    fondo: 'fondoverde',
    top: 0,
    onpresiono: Presiono_ID_310183181
});

function Salirinsp_ID_317287325(e) {
    /** 
     * Si el usuario desea cancelar la inspeccion se abrira una pantalla de alerta 
     */
    var evento = e;
    var ID_1382661739_opts = ['Asegurado no puede seguir', 'Se me acabo la bateria', 'Tuve un accidente', 'Cancelar'];
    var ID_1382661739 = Ti.UI.createOptionDialog({
        title: 'RAZON PARA CANCELAR INSPECCION ACTUAL',
        options: ID_1382661739_opts
    });
    ID_1382661739.addEventListener('click', function(e) {
        var resp = ID_1382661739_opts[e.index];
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var razon = "";
            if (resp == 'Asegurado no puede seguir') {
                razon = "Asegurado no puede seguir";
            }
            if (resp == 'Se me acabo la bateria') {
                razon = "Se me acabo la bateria";
            }
            if (resp == 'Tuve un accidente') {
                razon = "Tuve un accidente";
            }
            if (Ti.App.deployType != 'production') console.log('mi razon es', {
                "datos": razon
            });
            if ((_.isObject(razon) || (_.isString(razon)) && !_.isEmpty(razon)) || _.isNumber(razon) || _.isBoolean(razon)) {
                if (Ti.App.deployType != 'production') console.log('llamando servicio cancelarTarea', {});
                require('vars')['insp_cancelada'] = 'siniestro';
                Alloy.Collections[$.datos.config.adapter.collection_name].add($.datos);
                $.datos.save();
                Alloy.Collections[$.datos.config.adapter.collection_name].fetch();
                var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
                var ID_2045955133_visible = true;

                if (ID_2045955133_visible == 'si') {
                    ID_2045955133_visible = true;
                } else if (ID_2045955133_visible == 'no') {
                    ID_2045955133_visible = false;
                }
                $.ID_2045955133.setVisible(ID_2045955133_visible);

                var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
                var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
                if (Ti.App.deployType != 'production') console.log('detalle de seltarea', {
                    "data": seltarea
                });
                var datos = {
                    id_inspector: inspector.id_server,
                    codigo_identificador: inspector.codigo_identificador,
                    id_server: seltarea.id_server,
                    num_caso: seltarea.num_caso,
                    razon: razon
                };
                require('vars')[_var_scopekey]['datos'] = datos;
                var ID_1080212392 = {};
                ID_1080212392.success = function(e) {
                    var elemento = e,
                        valor = e;
                    $.ID_1824826361.limpiar({});
                    var ID_2045955133_visible = false;

                    if (ID_2045955133_visible == 'si') {
                        ID_2045955133_visible = true;
                    } else if (ID_2045955133_visible == 'no') {
                        ID_2045955133_visible = false;
                    }
                    $.ID_2045955133.setVisible(ID_2045955133_visible);

                    Alloy.createController("firma_index", {}).getView().open();
                    var ID_1296070880_func = function() {
                        Alloy.Events.trigger('_cerrar_insp', {
                            pantalla: 'caracteristicas'
                        });
                    };
                    var ID_1296070880 = setTimeout(ID_1296070880_func, 1000 * 0.5);
                    elemento = null, valor = null;
                };
                ID_1080212392.error = function(e) {
                    var elemento = e,
                        valor = e;
                    if (Ti.App.deployType != 'production') console.log('hubo error llamando cancelarTarea, la agregamos a cola de salida', {
                        "elemento": elemento
                    });
                    $.ID_1824826361.limpiar({});
                    if (Ti.App.deployType != 'production') console.log('agregando servicio cancelarTarea a cola', {});
                    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
                    var datos = ('datos' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['datos'] : '';
                    var ID_1850155095_m = Alloy.Collections.cola;
                    var ID_1850155095_fila = Alloy.createModel('cola', {
                        data: JSON.stringify(datos),
                        id_tarea: seltarea.id_server,
                        tipo: 'cancelar'
                    });
                    ID_1850155095_m.add(ID_1850155095_fila);
                    ID_1850155095_fila.save();
                    _.defer(function() {});
                    var ID_2045955133_visible = false;

                    if (ID_2045955133_visible == 'si') {
                        ID_2045955133_visible = true;
                    } else if (ID_2045955133_visible == 'no') {
                        ID_2045955133_visible = false;
                    }
                    $.ID_2045955133.setVisible(ID_2045955133_visible);

                    Alloy.createController("firma_index", {}).getView().open();
                    var ID_1441498527_func = function() {
                        Alloy.Events.trigger('_cerrar_insp', {
                            pantalla: 'caracteristicas'
                        });
                    };
                    var ID_1441498527 = setTimeout(ID_1441498527_func, 1000 * 0.5);
                    elemento = null, valor = null;
                };
                require('helper').ajaxUnico('ID_1080212392', '' + url_server + 'cancelarTarea' + '', 'POST', {
                    id_inspector: seltarea.id_inspector,
                    codigo_identificador: inspector.codigo_identificador,
                    id_tarea: seltarea.id_server,
                    num_caso: seltarea.num_caso,
                    mensaje: razon,
                    opcion: 0,
                    tipo: 1
                }, 15000, ID_1080212392);
            }
        }
        resp = null;
        e.source.removeEventListener("click", arguments.callee);
    });
    ID_1382661739.show();

}

function Presiono_ID_310183181(e) {
    /** 
     * Si el usuario seguira con la inspeccion, se revisa que todos los datos obligatorios hayan sido ingresados 
     */
    var evento = e;
    /** 
     * Se obtiene el a&#241;o actual para comparar que el a&#241;o ingresado no sea superior 
     */
    var d = new Date();
    var anoactual = d.getFullYear();
    /** 
     * Flags para verificar que todos los datos ingresados esten correctos 
     */
    require('vars')[_var_scopekey]['otro_valor'] = 'false';
    require('vars')[_var_scopekey]['sin_niveles'] = 'false';
    require('vars')[_var_scopekey]['todobien'] = 'true';
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
        /** 
         * Comprobamos si no hay niveles definidos, para mostrar posteriormente dialogo luego de finalizar resto de validaciones. 
         */
        var ID_1534100381_i = Alloy.createCollection('insp_niveles');
        var ID_1534100381_i_where = 'id_inspeccion=\'' + seltarea.id_server + '\'';
        ID_1534100381_i.fetch({
            query: 'SELECT * FROM insp_niveles WHERE id_inspeccion=\'' + seltarea.id_server + '\''
        });
        var niveles = require('helper').query2array(ID_1534100381_i);
        if (niveles && niveles.length == 0) {
            require('vars')[_var_scopekey]['sin_niveles'] = 'true';
        }
    } else {
        var ID_1874669027_i = Alloy.createCollection('insp_niveles');
        var ID_1874669027_i_where = '';
        ID_1874669027_i.fetch();
        var niveles = require('helper').query2array(ID_1874669027_i);
        if (niveles && niveles.length == 0) {
            require('vars')[_var_scopekey]['sin_niveles'] = 'true';
        }
    }
    if (Ti.App.deployType != 'production') console.log('info datos', {
        "daot": $.datos.toJSON()
    });
    if (_.isUndefined($datos.destinos)) {
        require('vars')[_var_scopekey]['todobien'] = 'false';
        var ID_2002662431_opts = ['Aceptar'];
        var ID_2002662431 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Seleccione destino de la vivienda',
            buttonNames: ID_2002662431_opts
        });
        ID_2002662431.addEventListener('click', function(e) {
            var nulo = ID_2002662431_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_2002662431.show();
        if (Ti.App.deployType != 'production') console.log('hola1', {});
    } else if ((_.isObject($datos.destinos) || _.isString($datos.destinos)) && _.isEmpty($datos.destinos)) {
        require('vars')[_var_scopekey]['todobien'] = 'false';
        var ID_503144617_opts = ['Aceptar'];
        var ID_503144617 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Seleccione destino de la vivienda',
            buttonNames: ID_503144617_opts
        });
        ID_503144617.addEventListener('click', function(e) {
            var nulo = ID_503144617_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_503144617.show();
        if (Ti.App.deployType != 'production') console.log('hola2', {});
    } else if (_.isUndefined($datos.construccion_ano)) {
        require('vars')[_var_scopekey]['todobien'] = 'false';
        var ID_242352338_opts = ['Aceptar'];
        var ID_242352338 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese año de construcción de la vivienda',
            buttonNames: ID_242352338_opts
        });
        ID_242352338.addEventListener('click', function(e) {
            var nulo = ID_242352338_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_242352338.show();
        if (Ti.App.deployType != 'production') console.log('hola3', {});
    } else if ((_.isObject($datos.construccion_ano) || _.isString($datos.construccion_ano)) && _.isEmpty($datos.construccion_ano)) {
        require('vars')[_var_scopekey]['todobien'] = 'false';
        var ID_522693828_opts = ['Aceptar'];
        var ID_522693828 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese año de construcción de la vivienda',
            buttonNames: ID_522693828_opts
        });
        ID_522693828.addEventListener('click', function(e) {
            var nulo = ID_522693828_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_522693828.show();
        if (Ti.App.deployType != 'production') console.log('hola4', {});
    } else if ($datos.construccion_ano > anoactual == true || $datos.construccion_ano > anoactual == 'true') {
        require('vars')[_var_scopekey]['todobien'] = 'false';
        var ID_194242927_opts = ['Aceptar'];
        var ID_194242927 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'El año tiene que ser menor al año actual',
            buttonNames: ID_194242927_opts
        });
        ID_194242927.addEventListener('click', function(e) {
            var nulo = ID_194242927_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_194242927.show();
        if (Ti.App.deployType != 'production') console.log('hola5', {});
    } else if ($datos.construccion_ano < (anoactual - 100) == true || $datos.construccion_ano < (anoactual - 100) == 'true') {
        require('vars')[_var_scopekey]['todobien'] = 'false';
        var ID_804350675_opts = ['Aceptar'];
        var ID_804350675 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Tiene que tener máximo 100 años de antigüedad',
            buttonNames: ID_804350675_opts
        });
        ID_804350675.addEventListener('click', function(e) {
            var nulo = ID_804350675_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_804350675.show();
        if (Ti.App.deployType != 'production') console.log('hola6', {});
    } else if ($datos.otros_seguros_enlugar == 1 || $datos.otros_seguros_enlugar == '1') {
        if (Ti.App.deployType != 'production') console.log('hola7', {});
        if (_.isUndefined($datos.id_compania)) {
            require('vars')[_var_scopekey]['todobien'] = 'false';
            var ID_24469666_opts = ['Aceptar'];
            var ID_24469666 = Ti.UI.createAlertDialog({
                title: 'Atención',
                message: 'Seleccione compañía de seguros',
                buttonNames: ID_24469666_opts
            });
            ID_24469666.addEventListener('click', function(e) {
                var nulo = ID_24469666_opts[e.index];
                nulo = null;
                e.source.removeEventListener("click", arguments.callee);
            });
            ID_24469666.show();
            if (Ti.App.deployType != 'production') console.log('hola8', {});
        } else if ((_.isObject($datos.id_compania) || _.isString($datos.id_compania)) && _.isEmpty($datos.id_compania)) {
            require('vars')[_var_scopekey]['todobien'] = 'false';
            var ID_1387306774_opts = ['Aceptar'];
            var ID_1387306774 = Ti.UI.createAlertDialog({
                title: 'Atención',
                message: 'Seleccione compañía de seguros',
                buttonNames: ID_1387306774_opts
            });
            ID_1387306774.addEventListener('click', function(e) {
                var nulo = ID_1387306774_opts[e.index];
                nulo = null;
                e.source.removeEventListener("click", arguments.callee);
            });
            ID_1387306774.show();
        } else {
            require('vars')[_var_scopekey]['otro_valor'] = 'true';
            if (Ti.App.deployType != 'production') console.log('hola9', {});
        }
    } else {}
    if ($datos.asociado_hipotecario == 1 || $datos.asociado_hipotecario == '1') {
        if (Ti.App.deployType != 'production') console.log('hola10', {});
        if (_.isUndefined($datos.id_entidad_financiera)) {
            if (Ti.App.deployType != 'production') console.log('hola11', {});
            require('vars')[_var_scopekey]['todobien'] = 'false';
            var ID_725720385_opts = ['Aceptar'];
            var ID_725720385 = Ti.UI.createAlertDialog({
                title: 'Atención',
                message: 'Seleccione entidad financiera',
                buttonNames: ID_725720385_opts
            });
            ID_725720385.addEventListener('click', function(e) {
                var nulo = ID_725720385_opts[e.index];
                nulo = null;
                e.source.removeEventListener("click", arguments.callee);
            });
            ID_725720385.show();
        } else if ((_.isObject($datos.id_entidad_financiera) || _.isString($datos.id_entidad_financiera)) && _.isEmpty($datos.id_entidad_financiera)) {
            if (Ti.App.deployType != 'production') console.log('hola12', {});
            require('vars')[_var_scopekey]['todobien'] = 'false';
            var ID_1365952444_opts = ['Aceptar'];
            var ID_1365952444 = Ti.UI.createAlertDialog({
                title: 'Atención',
                message: 'Seleccione entidad financiera',
                buttonNames: ID_1365952444_opts
            });
            ID_1365952444.addEventListener('click', function(e) {
                var nulo = ID_1365952444_opts[e.index];
                nulo = null;
                e.source.removeEventListener("click", arguments.callee);
            });
            ID_1365952444.show();
        } else {
            require('vars')[_var_scopekey]['otro_valor'] = 'true';
        }
    } else {}
    if ($datos.inhabitable == 1 || $datos.inhabitable == '1') {
        if (_.isUndefined($datos.estimacion_meses)) {
            if (Ti.App.deployType != 'production') console.log('hola14', {});
            require('vars')[_var_scopekey]['todobien'] = 'false';
            var ID_1083180607_opts = ['Aceptar'];
            var ID_1083180607 = Ti.UI.createAlertDialog({
                title: 'Atención',
                message: 'Ingrese estimación de meses',
                buttonNames: ID_1083180607_opts
            });
            ID_1083180607.addEventListener('click', function(e) {
                var nulo = ID_1083180607_opts[e.index];
                nulo = null;
                e.source.removeEventListener("click", arguments.callee);
            });
            ID_1083180607.show();
        } else if ((_.isObject($datos.estimacion_meses) || _.isString($datos.estimacion_meses)) && _.isEmpty($datos.estimacion_meses)) {
            if (Ti.App.deployType != 'production') console.log('hola15', {});
            require('vars')[_var_scopekey]['todobien'] = 'false';
            var ID_271339535_opts = ['Aceptar'];
            var ID_271339535 = Ti.UI.createAlertDialog({
                title: 'Atención',
                message: 'Ingrese estimación de meses',
                buttonNames: ID_271339535_opts
            });
            ID_271339535.addEventListener('click', function(e) {
                var nulo = ID_271339535_opts[e.index];
                nulo = null;
                e.source.removeEventListener("click", arguments.callee);
            });
            ID_271339535.show();
        } else {
            if (Ti.App.deployType != 'production') console.log('hola16', {});
            require('vars')[_var_scopekey]['otro_valor'] = 'true';
        }
    }
    var todobien = ('todobien' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['todobien'] : '';
    if (todobien == true || todobien == 'true') {
        var otro_valor = ('otro_valor' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['otro_valor'] : '';
        if (otro_valor == true || otro_valor == 'true') {
            var sin_niveles = ('sin_niveles' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['sin_niveles'] : '';
            if (sin_niveles == true || sin_niveles == 'true') {
                var ID_1570013819_opts = ['Aceptar'];
                var ID_1570013819 = Ti.UI.createAlertDialog({
                    title: 'Atención',
                    message: 'Ingrese al menos una zona de la vivienda',
                    buttonNames: ID_1570013819_opts
                });
                ID_1570013819.addEventListener('click', function(e) {
                    var nulo = ID_1570013819_opts[e.index];
                    nulo = null;
                    e.source.removeEventListener("click", arguments.callee);
                });
                ID_1570013819.show();
            } else {
                /** 
                 * Abrimos pantalla esta de acuerdo 
                 */
                $.ID_1244092239.abrir({
                    color: 'verde'
                });
            }
        } else {
            var sin_niveles = ('sin_niveles' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['sin_niveles'] : '';
            if (sin_niveles == true || sin_niveles == 'true') {
                var ID_837114221_opts = ['Aceptar'];
                var ID_837114221 = Ti.UI.createAlertDialog({
                    title: 'Atención',
                    message: 'Ingrese al menos una zona de la vivienda',
                    buttonNames: ID_837114221_opts
                });
                ID_837114221.addEventListener('click', function(e) {
                    var nulo = ID_837114221_opts[e.index];
                    nulo = null;
                    e.source.removeEventListener("click", arguments.callee);
                });
                ID_837114221.show();
            } else {
                /** 
                 * Abrimos pantalla esta de acuerdo 
                 */
                $.ID_1244092239.abrir({
                    color: 'verde'
                });
            }
        }
    }

}

$.ID_1151684903.init({
    titulo: 'Destino',
    __id: 'ALL1151684903',
    oncerrar: Cerrar_ID_1061002829,
    left: 0,
    hint: 'Seleccione destino',
    color: 'verde',
    subtitulo: 'Indique los destinos',
    right: 0,
    onafterinit: Afterinit_ID_1514337892
});

function Afterinit_ID_1514337892(e) {

    var evento = e;
    var ID_1600749054_func = function() {
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {} else {
            /** 
             * Insertamos dummies 
             */
            /** 
             * Insertamos dummies 
             */
            var ID_1376679078_i = Alloy.Collections.destino;
            var sql = "DELETE FROM " + ID_1376679078_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_1376679078_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_1376679078_i.trigger('delete');
            var item_index = 0;
            _.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
                item_index += 1;
                var ID_1565849607_m = Alloy.Collections.destino;
                var ID_1565849607_fila = Alloy.createModel('destino', {
                    nombre: 'Casa' + item,
                    id_server: '0' + item,
                    id_segured: '10' + item,
                    pais: 1
                });
                ID_1565849607_m.add(ID_1565849607_fila);
                ID_1565849607_fila.save();
                _.defer(function() {});
            });
        }
        /** 
         * Obtenemos datos para selectores 
         */
        var ID_1603996103_i = Alloy.createCollection('destino');
        ID_1603996103_i.fetch();
        var ID_1603996103_src = require('helper').query2array(ID_1603996103_i);
        var datos = [];
        _.each(ID_1603996103_src, function(fila, pos) {
            var new_row = {};
            _.each(fila, function(x, llave) {
                var newkey = '';
                if (llave == 'nombre') newkey = 'label';
                if (llave == 'id_segured') newkey = 'valor';
                if (newkey != '') new_row[newkey] = fila[llave];
            });
            new_row['estado'] = 0;
            datos.push(new_row);
        });
        /** 
         * Enviamos los datos al widget 
         */
        $.ID_1151684903.update({
            data: datos
        });
    };
    var ID_1600749054 = setTimeout(ID_1600749054_func, 1000 * 0.2);

}

function Cerrar_ID_1061002829(e) {

    var evento = e;
    /** 
     * Mostramos en la pantalla el valor seleccionado y actualizamos el modelo con lo seleccioando 
     */
    $.ID_1151684903.update({});
    $.datos.set({
        destinos: evento.valores
    });
    if ('datos' in $) $datos = $.datos.toJSON();

}

function Change_ID_1171433432(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    $.datos.set({
        construccion_ano: elemento
    });
    if ('datos' in $) $datos = $.datos.toJSON();
    elemento = null, source = null;

}

function Return_ID_904623938(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    $.ID_572364594.blur();
    elemento = null, source = null;

}

function Change_ID_1205644443(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    if (elemento == true || elemento == 'true') {
        var si = 'SI';
        $.ID_1711674068.setText(si);

        $.datos.set({
            construccion_anexa: 1
        });
        if ('datos' in $) $datos = $.datos.toJSON();
    } else {
        var no = 'NO';
        $.ID_1711674068.setText(no);

        $.datos.set({
            construccion_anexa: 0
        });
        if ('datos' in $) $datos = $.datos.toJSON();
    }
    elemento = null;

}

function Change_ID_1449385557(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    if (elemento == true || elemento == 'true') {
        var si = 'SI';
        $.ID_1098001258.setText(si);

        $.datos.set({
            otros_seguros_enlugar: 1
        });
        if ('datos' in $) $datos = $.datos.toJSON();
        $.ID_1824826361.activar({});
    } else {
        var no = 'NO';
        $.ID_1098001258.setText(no);

        $.datos.set({
            otros_seguros_enlugar: 0,
            id_compania: ''
        });
        if ('datos' in $) $datos = $.datos.toJSON();
        $.ID_1824826361.labels({
            valor: 'Seleccione compañía'
        });
        $.ID_1824826361.desactivar({});
    }
    elemento = null;

}

$.ID_1824826361.init({
    titulo: 'COMPAÑIA',
    cargando: 'cargando...',
    __id: 'ALL1824826361',
    left: 0,
    onrespuesta: Respuesta_ID_1182316680,
    campo: 'Compañía',
    onabrir: Abrir_ID_1684916739,
    color: 'verde',
    right: 0,
    seleccione: 'Seleccione compañía',
    activo: false,
    onafterinit: Afterinit_ID_1735923035
});

function Afterinit_ID_1735923035(e) {

    var evento = e;
    var ID_1948046515_func = function() {
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {} else {
            var ID_1439646260_i = Alloy.Collections.compania;
            var sql = "DELETE FROM " + ID_1439646260_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_1439646260_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_1439646260_i.trigger('delete');
            var item_index = 0;
            _.each('A,A,B,C,D,E,F,F,G'.split(','), function(item, item_pos, item_list) {
                item_index += 1;
                var ID_1679360791_m = Alloy.Collections.compania;
                var ID_1679360791_fila = Alloy.createModel('compania', {
                    nombre: item + ' Empresa',
                    id_server: '0' + item,
                    id_segured: '10' + item_pos,
                    pais: 1
                });
                ID_1679360791_m.add(ID_1679360791_fila);
                ID_1679360791_fila.save();
                _.defer(function() {});
            });
        }
        var ID_1191274390_i = Alloy.createCollection('compania');
        ID_1191274390_i.fetch();
        var ID_1191274390_src = require('helper').query2array(ID_1191274390_i);
        var datos = [];
        _.each(ID_1191274390_src, function(fila, pos) {
            var new_row = {};
            _.each(fila, function(x, llave) {
                var newkey = '';
                if (llave == 'nombre') newkey = 'valor';
                if (llave == 'id_segured') newkey = 'id_segured';
                if (newkey != '') new_row[newkey] = fila[llave];
            });
            datos.push(new_row);
        });
        $.ID_1824826361.data({
            data: datos
        });
    };
    var ID_1948046515 = setTimeout(ID_1948046515_func, 1000 * 0.2);

}

function Abrir_ID_1684916739(e) {

    var evento = e;

}

function Respuesta_ID_1182316680(e) {

    var evento = e;
    $.ID_1824826361.labels({
        valor: evento.valor
    });
    $.datos.set({
        id_compania: evento.item.id_segured
    });
    if ('datos' in $) $datos = $.datos.toJSON();
    if (Ti.App.deployType != 'production') console.log('datos recibidos de modal compania', {
        "datos": evento
    });

}

function Change_ID_266856704(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    if (elemento == true || elemento == 'true') {
        var si = 'SI';
        $.ID_584016677.setText(si);

        $.datos.set({
            asociado_hipotecario: 1
        });
        if ('datos' in $) $datos = $.datos.toJSON();
        $.ID_511044183.activar({});
    } else {
        var no = 'NO';
        $.ID_584016677.setText(no);

        $.datos.set({
            asociado_hipotecario: 0,
            id_entidad_financiera: ''
        });
        if ('datos' in $) $datos = $.datos.toJSON();
        $.ID_511044183.labels({
            valor: 'Seleccione entidad'
        });
        $.ID_511044183.desactivar({});
    }
    elemento = null;

}

$.ID_511044183.init({
    titulo: 'ENTIDAD',
    cargando: 'cargando...',
    __id: 'ALL511044183',
    left: 0,
    onrespuesta: Respuesta_ID_864977697,
    campo: 'Entidad Financiera',
    onabrir: Abrir_ID_129099414,
    color: 'verde',
    right: 0,
    seleccione: 'Seleccione entidad',
    activo: false,
    onafterinit: Afterinit_ID_1747714774
});

function Afterinit_ID_1747714774(e) {

    var evento = e;
    var ID_779848452_func = function() {
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {} else {
            var ID_116885447_i = Alloy.Collections.entidad_financiera;
            var sql = "DELETE FROM " + ID_116885447_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_116885447_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_116885447_i.trigger('delete');
            var item_index = 0;
            _.each('A,A,B,C,D,E,F,F,G'.split(','), function(item, item_pos, item_list) {
                item_index += 1;
                var ID_1604887491_m = Alloy.Collections.entidad_financiera;
                var ID_1604887491_fila = Alloy.createModel('entidad_financiera', {
                    nombre: item + ' Entidad',
                    id_server: '0' + item,
                    id_segured: '20' + item_pos,
                    pais: 1
                });
                ID_1604887491_m.add(ID_1604887491_fila);
                ID_1604887491_fila.save();
                _.defer(function() {});
            });
        }
        var ID_822123413_i = Alloy.createCollection('entidad_financiera');
        ID_822123413_i.fetch();
        var ID_822123413_src = require('helper').query2array(ID_822123413_i);
        var datos = [];
        _.each(ID_822123413_src, function(fila, pos) {
            var new_row = {};
            _.each(fila, function(x, llave) {
                var newkey = '';
                if (llave == 'nombre') newkey = 'valor';
                if (llave == 'id_segured') newkey = 'id_segured';
                if (newkey != '') new_row[newkey] = fila[llave];
            });
            datos.push(new_row);
        });
        $.ID_511044183.data({
            data: datos
        });
    };
    var ID_779848452 = setTimeout(ID_779848452_func, 1000 * 0.2);

}

function Abrir_ID_129099414(e) {

    var evento = e;

}

function Respuesta_ID_864977697(e) {

    var evento = e;
    $.ID_511044183.labels({
        valor: evento.valor
    });
    $.datos.set({
        id_entidad_financiera: evento.item.id_segured
    });
    if ('datos' in $) $datos = $.datos.toJSON();
    if (Ti.App.deployType != 'production') console.log('datos recibidos de modal entidad', {
        "datos": evento
    });

}

function Change_ID_872181670(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    if (elemento == true || elemento == 'true') {
        var si = 'SI';
        $.ID_1466872410.setText(si);

        $.ID_2077733016.setEditable(true);

        $.datos.set({
            inhabitable: 1
        });
        if ('datos' in $) $datos = $.datos.toJSON();
    } else {
        var no = 'NO';
        $.ID_1466872410.setText(no);

        $.ID_2077733016.setEditable('false');

        $.datos.set({
            inhabitable: 0
        });
        if ('datos' in $) $datos = $.datos.toJSON();
        var var1;
        var1 = $.ID_2077733016;
        var1.setValue("");
    }
    elemento = null;

}

function Change_ID_1769372801(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    $.datos.set({
        estimacion_meses: elemento
    });
    if ('datos' in $) $datos = $.datos.toJSON();
    elemento = null, source = null;

}

function Return_ID_938052382(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    $.ID_2077733016.blur();
    elemento = null, source = null;

}

function Touchstart_ID_1985011874(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    $.ID_1357952722.setColor('#cccccc');


}

function Touchend_ID_249861399(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    $.ID_1357952722.setColor('#8ce5bd');

    /** 
     * Ocupamos esta variable para impedir que la pantalla siguente se cargue dos veces 
     */
    var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
    if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
        require('vars')['var_abriendo'] = 'true';
        Alloy.createController("niveles", {}).getView().open();
    }

}

function Swipe_ID_1329611708(e) {

    e.cancelBubble = true;
    var elemento = e.section.getItemAt(e.itemIndex);
    var findVariables = require('fvariables');
    var fila = {};
    _.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
        _.each(obj_id, function(valor, prop) {
            fila[prop] = elemento[id_field][prop];
            var llaves = findVariables(valor, '{', '}');
            _.each(llaves, function(llave) {
                fila[llave] = elemento[id_field][prop];
            });
        });
    });
    if (Ti.App.deployType != 'production') console.log('en normal', {
        "e": e
    });
    if (e.direction == 'left') {
        var findVariables = require('fvariables');
        elemento.template = 'pborrar';
        _.each(_list_templates['pborrar'], function(obj_id, id_field) {
            _.each(obj_id, function(valor, prop) {
                var llaves = findVariables(valor, '{', '}');
                _.each(llaves, function(llave) {
                    elemento[id_field] = {};
                    elemento[id_field][prop] = fila[llave];
                });
            });
        });
        if (OS_IOS) {
            e.section.updateItemAt(e.itemIndex, elemento, {
                animated: true
            });
        } else if (OS_ANDROID) {
            e.section.updateItemAt(e.itemIndex, elemento);
        }
    }

}

function Click_ID_1710059938(e) {

    e.cancelBubble = true;
    var elemento = e.section.getItemAt(e.itemIndex);
    var findVariables = require('fvariables');
    var fila = {};
    _.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
        _.each(obj_id, function(valor, prop) {
            fila[prop] = elemento[id_field][prop];
            var llaves = findVariables(valor, '{', '}');
            _.each(llaves, function(llave) {
                fila[llave] = elemento[id_field][prop];
            });
        });
    });
    /** 
     * Ocupamos esta variable para impedir que la pantalla siguente se cargue dos veces 
     */
    var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
    if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
        require('vars')['var_abriendo'] = 'true';
        /** 
         * Enviamos el parametro _dato para indicar cual sera el id del nivel a editar 
         */
        Alloy.createController("editarnivel", {
            '_dato': fila
        }).getView().open();
    }

}

function Click_ID_1561531678(e) {

    e.cancelBubble = true;
    var elemento = e.section.getItemAt(e.itemIndex);
    var findVariables = require('fvariables');
    var fila = {};
    _.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
        _.each(obj_id, function(valor, prop) {
            fila[prop] = elemento[id_field][prop];
            var llaves = findVariables(valor, '{', '}');
            _.each(llaves, function(llave) {
                fila[llave] = elemento[id_field][prop];
            });
        });
    });
    if (Ti.App.deployType != 'production') console.log('click en borrar', {
        "e": e
    });
    var ID_2132233649_opts = ['Si', 'No'];
    var ID_2132233649 = Ti.UI.createAlertDialog({
        title: 'ALERTA',
        message: '' + '¿ Seguro desea eliminar: ' + fila.nombre + ' ?' + '',
        buttonNames: ID_2132233649_opts
    });
    ID_2132233649.addEventListener('click', function(e) {
        var xd = ID_2132233649_opts[e.index];
        if (xd == 'Si') {
            if (Ti.App.deployType != 'production') console.log('borrando xd', {});
            var ID_1296906874_i = Alloy.Collections.insp_niveles;
            var sql = 'DELETE FROM ' + ID_1296906874_i.config.adapter.collection_name + ' WHERE id=\'' + fila.id + '\'';
            var db = Ti.Database.open(ID_1296906874_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_1296906874_i.trigger('delete');
            _.defer(function() {
                Alloy.Collections.insp_niveles.fetch();
            });
        }
        xd = null;
        e.source.removeEventListener("click", arguments.callee);
    });
    ID_2132233649.show();

}

function Swipe_ID_1745582830(e) {

    e.cancelBubble = true;
    var elemento = e.section.getItemAt(e.itemIndex);
    var findVariables = require('fvariables');
    var fila = {};
    _.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
        _.each(obj_id, function(valor, prop) {
            fila[prop] = elemento[id_field][prop];
            var llaves = findVariables(valor, '{', '}');
            _.each(llaves, function(llave) {
                fila[llave] = elemento[id_field][prop];
            });
        });
    });
    if (Ti.App.deployType != 'production') console.log('en borrar', {
        "e": e
    });
    if (e.direction == 'right') {
        if (Ti.App.deployType != 'production') console.log('swipe hacia derecha (desde borrar)', {});
        var findVariables = require('fvariables');
        elemento.template = 'nivel';
        _.each(_list_templates['nivel'], function(obj_id, id_field) {
            _.each(obj_id, function(valor, prop) {
                var llaves = findVariables(valor, '{', '}');
                _.each(llaves, function(llave) {
                    elemento[id_field] = {};
                    elemento[id_field][prop] = fila[llave];
                });
            });
        });
        if (OS_IOS) {
            e.section.updateItemAt(e.itemIndex, elemento, {
                animated: true
            });
        } else if (OS_ANDROID) {
            e.section.updateItemAt(e.itemIndex, elemento);
        }
    }

}

function Click_ID_1960417363(e) {

    e.cancelBubble = true;
    var elemento = e.section.getItemAt(e.itemIndex);
    var findVariables = require('fvariables');
    var fila = {};
    _.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
        _.each(obj_id, function(valor, prop) {
            fila[prop] = elemento[id_field][prop];
            var llaves = findVariables(valor, '{', '}');
            _.each(llaves, function(llave) {
                fila[llave] = elemento[id_field][prop];
            });
        });
    });
    if (Ti.App.deployType != 'production') console.log('en borrar', {
        "e": e
    });
    var findVariables = require('fvariables');
    elemento.template = 'nivel';
    _.each(_list_templates['nivel'], function(obj_id, id_field) {
        _.each(obj_id, function(valor, prop) {
            var llaves = findVariables(valor, '{', '}');
            _.each(llaves, function(llave) {
                elemento[id_field] = {};
                elemento[id_field][prop] = fila[llave];
            });
        });
    });
    if (OS_IOS) {
        e.section.updateItemAt(e.itemIndex, elemento, {
            animated: true
        });
    } else if (OS_ANDROID) {
        e.section.updateItemAt(e.itemIndex, elemento);
    }

}

$.ID_1244092239.init({
    titulo: '¿EL ASEGURADO CONFIRMA ESTOS DATOS?',
    __id: 'ALL1244092239',
    si: 'SI, Están correctos',
    texto: 'El asegurado debe confirmar que los datos de esta sección están correctos',
    pantalla: '¿ESTA DE ACUERDO?',
    onno: no_ID_1509897338,
    color: 'verde',
    onsi: si_ID_1010650229,
    no: 'NO, Hay que modificar algo'
});

function no_ID_1509897338(e) {

    var evento = e;

}

function si_ID_1010650229(e) {

    var evento = e;
    /** 
     * Ocupamos esta variable para impedir que la pantalla siguente se cargue dos veces 
     */
    var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
    if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
        require('vars')['var_abriendo'] = 'true';
        /** 
         * Guardamos lo ingresado en el modelo 
         */
        Alloy.Collections[$.datos.config.adapter.collection_name].add($.datos);
        $.datos.save();
        Alloy.Collections[$.datos.config.adapter.collection_name].fetch();
        /** 
         * Limpiamos widget para liberar memoria ram 
         */
        $.ID_1151684903.limpiar({});
        $.ID_1824826361.limpiar({});
        $.ID_511044183.limpiar({});
        /** 
         * Enviamos a pantalla siniestro 
         */
        Alloy.createController("siniestro_index", {}).getView().open();
    }

}

function Postlayout_ID_1616611532(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    $.ID_2098285034.show();

}

function Postlayout_ID_1942514203(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    /** 
     * Cerramos pantalla datos basicos 
     */
    Alloy.Events.trigger('_cerrar_insp', {
        pantalla: 'basicos'
    });
    var ID_1742226677_func = function() {
        /** 
         * Una vez ya cargada la tarea, dejamos la variable limpia para poder abrir otras pantallas 
         */
        require('vars')['var_abriendo'] = '';
    };
    var ID_1742226677 = setTimeout(ID_1742226677_func, 1000 * 0.2);

}

var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
    $.datos.set({
        id_inspeccion: seltarea.id_server
    });
    if ('datos' in $) $datos = $.datos.toJSON();
    /** 
     * restringimos niveles mostrados a la inspeccion actual (por si los temporales aun tienen datos sin enviar) 
     */
    ID_1108412687_filter = function(coll) {
        var filtered = coll.filter(function(m) {
            var _tests = [],
                _all_true = false,
                model = m.toJSON();
            _tests.push((model.id_inspeccion == seltarea.id_server));
            var _all_true_s = _.uniq(_tests);
            _all_true = (_all_true_s.length == 1 && _all_true_s[0] == true) ? true : false;
            return _all_true;
        });
        filtered = _.toArray(filtered);
        return filtered;
    };
    _.defer(function() {
        Alloy.Collections.insp_niveles.fetch();
    });
    /** 
     * Definimos campos defaults 
     */
    $.datos.set({
        construccion_anexa: 0,
        otros_seguros_enlugar: 0,
        asociado_hipotecario: 0,
        inhabitable: 0
    });
    if ('datos' in $) $datos = $.datos.toJSON();
}
/** 
 * Cerramos esta pantalla cuando es llamada en la pantalla siguente (Siniestro) 
 */
_my_events['_cerrar_insp,ID_1362138204'] = function(evento) {
    if (!_.isUndefined(evento.pantalla)) {
        if (evento.pantalla == 'caracteristicas') {
            if (Ti.App.deployType != 'production') console.log('debug cerrando caracteristicas', {});
            var ID_1701237886_trycatch = {
                error: function(e) {
                    if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
                }
            };
            try {
                ID_1701237886_trycatch.error = function(evento) {
                    if (Ti.App.deployType != 'production') console.log('error cerrando caracteristicas', {});
                };
                $.ID_1151684903.limpiar({});
                $.ID_1824826361.limpiar({});
                $.ID_511044183.limpiar({});
                $.ID_1944128176.close();
            } catch (e) {
                ID_1701237886_trycatch.error(e);
            }
        }
    } else {
        if (Ti.App.deployType != 'production') console.log('debug cerrando (todas) caracteristicas', {});
        var ID_959866127_trycatch = {
            error: function(e) {
                if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
            }
        };
        try {
            ID_959866127_trycatch.error = function(evento) {
                if (Ti.App.deployType != 'production') console.log('error cerrando caracteristicas', {});
            };
            $.ID_1151684903.limpiar({});
            $.ID_1824826361.limpiar({});
            $.ID_511044183.limpiar({});
            $.ID_1944128176.close();
        } catch (e) {
            ID_959866127_trycatch.error(e);
        }
    }
};
Alloy.Events.on('_cerrar_insp', _my_events['_cerrar_insp,ID_1362138204']);

function Androidback_ID_1383235272(e) {

    e.cancelBubble = true;
    var elemento = e.source;

}

(function() {
    /** 
     * Dejamos el scroll en el top y desenfocamos los campos de texto 
     */
    var ID_1575921980_func = function() {
        $.ID_1229550402.scrollToTop();
        $.ID_572364594.blur();
        $.ID_2077733016.blur();
    };
    var ID_1575921980 = setTimeout(ID_1575921980_func, 1000 * 0.2);
    /** 
     * Modificamos el color del statusbar 
     */
    var ID_323040650_func = function() {
        var ID_1944128176_statusbar = '#57BC8B';

        var setearStatusColor = function(ID_1944128176_statusbar) {
            if (OS_IOS) {
                if (ID_1944128176_statusbar == 'light' || ID_1944128176_statusbar == 'claro') {
                    $.ID_1944128176_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.LIGHT_CONTENT);
                } else if (ID_1944128176_statusbar == 'grey' || ID_1944128176_statusbar == 'gris' || ID_1944128176_statusbar == 'gray') {
                    $.ID_1944128176_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.GREY);
                } else if (ID_1944128176_statusbar == 'oscuro' || ID_1944128176_statusbar == 'dark') {
                    $.ID_1944128176_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.DEFAULT);
                }
            } else if (OS_ANDROID) {
                abx.setStatusbarColor(ID_1944128176_statusbar);
            }
        };
        setearStatusColor(ID_1944128176_statusbar);

    };
    var ID_323040650 = setTimeout(ID_323040650_func, 1000 * 0.1);
})();

if (OS_IOS || OS_ANDROID) {
    $.ID_1944128176.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}
//$.ID_1944128176.open();