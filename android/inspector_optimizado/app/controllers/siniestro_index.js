var _bind4section = {};
var _list_templates = {
    "elemento": {
        "ID_432412973": {},
        "ID_291370846": {
            "text": "{nombre}"
        },
        "ID_1925487537": {
            "text": "{id_server}"
        },
        "ID_975786586": {
            "text": "{id_segured}"
        },
        "ID_1546190443": {}
    },
    "dano": {
        "ID_1569764493": {
            "text": "{id}"
        },
        "ID_304218376": {},
        "ID_1912165224": {
            "text": "{nombre}"
        }
    },
    "pborrar": {
        "ID_1236907100": {
            "text": "{id}"
        },
        "ID_1506538078": {},
        "ID_2024360383": {},
        "ID_1765564351": {},
        "ID_1699280427": {
            "text": "{nombre}"
        },
        "ID_1445903745": {},
        "ID_1795050796": {},
        "ID_1752901998": {},
        "ID_1914367022": {
            "text": "{nombre}"
        },
        "ID_1249074865": {
            "text": "{id}"
        },
        "ID_1131557960": {},
        "ID_1361874287": {}
    },
    "contenido": {
        "ID_630800887": {},
        "ID_862352109": {
            "text": "{id}"
        },
        "ID_560452274": {
            "text": "{nombre}"
        }
    },
    "recinto": {
        "ID_1960582596": {},
        "ID_871581291": {
            "text": "{nombre}"
        },
        "ID_310297848": {
            "text": "{id}"
        }
    }
};
var $datos = $.datos.toJSON();

var _activity;
if (OS_ANDROID) {
    _activity = $.ID_887883297.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_887883297';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_887883297.addEventListener('open', function(e) {});
}
$.ID_887883297.orientationModes = [Titanium.UI.PORTRAIT];


$.ID_1145872295.init({
    titulo: 'SINIESTRO',
    onsalirinsp: Salirinsp_ID_1843371735,
    __id: 'ALL1145872295',
    continuar: '',
    salir_insp: '',
    fondo: 'fondonaranjo',
    top: 0,
    onpresiono: Presiono_ID_1238820637
});

function Salirinsp_ID_1843371735(e) {

    var evento = e;
    var ID_1516729205_opts = ['Asegurado no puede seguir', 'Se me acabo la bateria', 'Tuve un accidente', 'Cancelar'];
    var ID_1516729205 = Ti.UI.createOptionDialog({
        title: 'RAZON PARA CANCELAR INSPECCION ACTUAL',
        options: ID_1516729205_opts
    });
    ID_1516729205.addEventListener('click', function(e) {
        var resp = ID_1516729205_opts[e.index];
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var razon = "";
            if (resp == 'Asegurado no puede seguir') {
                razon = "Asegurado no puede seguir";
            }
            if (resp == 'Se me acabo la bateria') {
                razon = "Se me acabo la bateria";
            }
            if (resp == 'Tuve un accidente') {
                razon = "Tuve un accidente";
            }
            if (Ti.App.deployType != 'production') console.log('mi razon es', {
                "datos": razon
            });
            if ((_.isObject(razon) || (_.isString(razon)) && !_.isEmpty(razon)) || _.isNumber(razon) || _.isBoolean(razon)) {
                if (Ti.App.deployType != 'production') console.log('llamando servicio cancelarTarea', {});
                require('vars')['insp_cancelada'] = 'siniestro';
                Alloy.Collections[$.datos.config.adapter.collection_name].add($.datos);
                $.datos.save();
                Alloy.Collections[$.datos.config.adapter.collection_name].fetch();
                var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
                var ID_1027197276_visible = true;

                if (ID_1027197276_visible == 'si') {
                    ID_1027197276_visible = true;
                } else if (ID_1027197276_visible == 'no') {
                    ID_1027197276_visible = false;
                }
                $.ID_1027197276.setVisible(ID_1027197276_visible);

                var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
                var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
                if (Ti.App.deployType != 'production') console.log('detalle de seltarea', {
                    "data": seltarea
                });
                if (Ti.App.deployType != 'production') console.log('detalle de inspector', {
                    "data": inspector
                });
                var datos = {
                    id_inspector: inspector.id_server,
                    codigo_identificador: inspector.codigo_identificador,
                    id_server: seltarea.id_server,
                    num_caso: seltarea.num_caso,
                    razon: razon
                };
                require('vars')[_var_scopekey]['datos'] = datos;
                var ID_1183964924 = {};
                ID_1183964924.success = function(e) {
                    var elemento = e,
                        valor = e;
                    $.ID_1949382907.limpiar({});
                    var ID_1027197276_visible = false;

                    if (ID_1027197276_visible == 'si') {
                        ID_1027197276_visible = true;
                    } else if (ID_1027197276_visible == 'no') {
                        ID_1027197276_visible = false;
                    }
                    $.ID_1027197276.setVisible(ID_1027197276_visible);

                    Alloy.createController("firma_index", {}).getView().open();
                    var ID_1118752695_func = function() {
                        Alloy.Events.trigger('_cerrar_insp', {
                            pantalla: 'siniestro'
                        });
                    };
                    var ID_1118752695 = setTimeout(ID_1118752695_func, 1000 * 0.5);
                    elemento = null, valor = null;
                };
                ID_1183964924.error = function(e) {
                    var elemento = e,
                        valor = e;
                    if (Ti.App.deployType != 'production') console.log('hubo error llamando cancelarTarea, la agregamos a cola de salida', {
                        "elemento": elemento
                    });
                    $.ID_1949382907.limpiar({});
                    if (Ti.App.deployType != 'production') console.log('agregando servicio cancelarTarea a cola', {});
                    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
                    var datos = ('datos' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['datos'] : '';
                    var ID_1787955362_m = Alloy.Collections.cola;
                    var ID_1787955362_fila = Alloy.createModel('cola', {
                        data: JSON.stringify(datos),
                        id_tarea: seltarea.id_server,
                        tipo: 'cancelar'
                    });
                    ID_1787955362_m.add(ID_1787955362_fila);
                    ID_1787955362_fila.save();
                    var ID_1027197276_visible = false;

                    if (ID_1027197276_visible == 'si') {
                        ID_1027197276_visible = true;
                    } else if (ID_1027197276_visible == 'no') {
                        ID_1027197276_visible = false;
                    }
                    $.ID_1027197276.setVisible(ID_1027197276_visible);

                    Alloy.createController("firma_index", {}).getView().open();
                    var ID_1423998708_func = function() {
                        Alloy.Events.trigger('_cerrar_insp', {
                            pantalla: 'siniestro'
                        });
                    };
                    var ID_1423998708 = setTimeout(ID_1423998708_func, 1000 * 0.5);
                    elemento = null, valor = null;
                };
                require('helper').ajaxUnico('ID_1183964924', '' + url_server + 'cancelarTarea' + '', 'POST', {
                    id_inspector: inspector.id_server,
                    codigo_identificador: inspector.codigo_identificador,
                    id_tarea: seltarea.id_server,
                    num_caso: seltarea.num_caso,
                    mensaje: razon,
                    opcion: 0,
                    tipo: 1
                }, 15000, ID_1183964924);
            }
        }
        resp = null;
        e.source.removeEventListener("click", arguments.callee);
    });
    ID_1516729205.show();

}

function Presiono_ID_1238820637(e) {

    var evento = e;
    /** 
     * Desenfocamos el campo de texto de la descripcion del siniestro 
     */
    $.ID_1240553527.blur();
    var ID_1431519872_func = function() {
        if (_.isUndefined($datos.id_tipo_siniestro)) {
            /** 
             * Validamos que los campos ingresados sean correctos 
             */
            var ID_1696151105_opts = ['Aceptar'];
            var ID_1696151105 = Ti.UI.createAlertDialog({
                title: 'Atención',
                message: 'Seleccione tipo de siniestro',
                buttonNames: ID_1696151105_opts
            });
            ID_1696151105.addEventListener('click', function(e) {
                var nulo = ID_1696151105_opts[e.index];
                nulo = null;
                e.source.removeEventListener("click", arguments.callee);
            });
            ID_1696151105.show();
        } else if (_.isUndefined($datos.descripcion)) {
            var ID_26478533_opts = ['Aceptar'];
            var ID_26478533 = Ti.UI.createAlertDialog({
                title: 'Atención',
                message: 'Describa el siniestro brevemente, mínimo 30 caracteres',
                buttonNames: ID_26478533_opts
            });
            ID_26478533.addEventListener('click', function(e) {
                var nulo = ID_26478533_opts[e.index];
                nulo = null;
                e.source.removeEventListener("click", arguments.callee);
            });
            ID_26478533.show();
        } else if (_.isNumber($datos.descripcion.length) && _.isNumber(29) && $datos.descripcion.length <= 29) {
            var ID_855578299_opts = ['Aceptar'];
            var ID_855578299 = Ti.UI.createAlertDialog({
                title: 'Atención',
                message: 'Describa el siniestro brevemente, mínimo 30 caracteres',
                buttonNames: ID_855578299_opts
            });
            ID_855578299.addEventListener('click', function(e) {
                var nulo = ID_855578299_opts[e.index];
                nulo = null;
                e.source.removeEventListener("click", arguments.callee);
            });
            ID_855578299.show();
        } else if (_.isUndefined($datos.porcentaje_danos_estructura)) {
            var ID_1123886876_opts = ['Aceptar'];
            var ID_1123886876 = Ti.UI.createAlertDialog({
                title: 'Atención',
                message: 'Indique porcentaje de daños en estructura',
                buttonNames: ID_1123886876_opts
            });
            ID_1123886876.addEventListener('click', function(e) {
                var nulo = ID_1123886876_opts[e.index];
                nulo = null;
                e.source.removeEventListener("click", arguments.callee);
            });
            ID_1123886876.show();
        } else if (_.isUndefined($datos.porcentaje_danos_terminaciones)) {
            var ID_1698458597_opts = ['Aceptar'];
            var ID_1698458597 = Ti.UI.createAlertDialog({
                title: 'Atención',
                message: 'Indique porcentaje de daños en terminaciones',
                buttonNames: ID_1698458597_opts
            });
            ID_1698458597.addEventListener('click', function(e) {
                var nulo = ID_1698458597_opts[e.index];
                nulo = null;
                e.source.removeEventListener("click", arguments.callee);
            });
            ID_1698458597.show();
        } else if (_.isUndefined($datos.porcentaje_danos_instalaciones)) {
            var ID_1822123630_opts = ['Aceptar'];
            var ID_1822123630 = Ti.UI.createAlertDialog({
                title: 'Atención',
                message: 'Indique porcentaje de daños en instalaciones',
                buttonNames: ID_1822123630_opts
            });
            ID_1822123630.addEventListener('click', function(e) {
                var nulo = ID_1822123630_opts[e.index];
                nulo = null;
                e.source.removeEventListener("click", arguments.callee);
            });
            ID_1822123630.show();
        } else {
            /** 
             * Abrimos pantalla esta de acuerdo 
             */
            $.ID_1510301861.abrir({
                color: 'naranjo'
            });
        }
    };
    var ID_1431519872 = setTimeout(ID_1431519872_func, 1000 * 0.2);

}

$.ID_1510301861.init({
    titulo: '¿EL ASEGURADO CONFIRMA ESTOS DATOS?',
    __id: 'ALL1510301861',
    si: 'SI, Están correctos',
    texto: 'El asegurado debe confirmar que los datos de esta sección están correctos',
    pantalla: '¿ESTA DE ACUERDO?',
    onno: no_ID_1110621737,
    color: 'naranjo',
    onsi: si_ID_1427475994,
    no: 'NO, Hay que modificar algo'
});

function si_ID_1427475994(e) {

    var evento = e;
    /** 
     * Ocupamos esta variable para impedir que la pantalla siguente se cargue dos veces 
     */
    var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
    if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
        require('vars')['var_abriendo'] = 'true';
        /** 
         * Guardamos los datos agregados/modificados en el modelo de insp_siniestro 
         */
        Alloy.Collections[$.datos.config.adapter.collection_name].add($.datos);
        $.datos.save();
        Alloy.Collections[$.datos.config.adapter.collection_name].fetch();
        /** 
         * Limpiamos widgets 
         */
        $.ID_1949382907.limpiar({});
        Alloy.createController("recinto_index", {}).getView().open();
    }

}

function no_ID_1110621737(e) {

    var evento = e;

}

$.ID_1949382907.init({
    titulo: 'SINIESTRO',
    cargando: 'cargando...',
    __id: 'ALL1949382907',
    left: 0,
    onrespuesta: Respuesta_ID_1050683358,
    campo: 'Tipo de siniestro',
    onabrir: Abrir_ID_1045626235,
    color: 'naranjo',
    right: 0,
    seleccione: 'Seleccione tipo',
    activo: true,
    onafterinit: Afterinit_ID_1383281866
});

function Afterinit_ID_1383281866(e) {

    var evento = e;
    var ID_1088992865_func = function() {
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {} else {
            /** 
             * Insertamos dummies 
             */
            /** 
             * Insertamos dummies 
             */
            var ID_1519202621_i = Alloy.Collections.tipo_siniestro;
            var sql = "DELETE FROM " + ID_1519202621_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_1519202621_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_1519202621_i.trigger('delete');
            var item_index = 0;
            _.each('1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,245,246,247,248,249,250,251,252,253,254,255,256,257,258,259,260,261,262,263,264,265,266,267,268,269,270,271,272,273,274,275,276,277,278,279,280,281,282,283,284,285,286,287,288,289,290,291,292,293,294,295,296,297,298,299,300,301,302,303,304,305,306,307,308,309,310,311,312,313,314,315,316,317,318,319,320,321,322,323,324,325,326,327,328,329,330,331,332,333,334,335,336,337,338,339,340,341,342,343,344,345,346,347,348,349,350,351,352,353,354,355,356,357,358,359,360,361,362,363,364,365,366,367,368,369,370,371,372,373,374,375,376,377,378,379,380,381,382,383,384,385,386,387,388,389,390,391,392,393,394,395,396,397,398,399,400,401,402,403,404,405,406,407,408,409,410,411,412,413,414,415,416,417,418,419,420,421,422,423,424,425,426,427,428,429,430,431,432,433,434,435,436,437,438,439,440,441,442,443,444,445,446,447,448,449,450,451,452,453,454,455,456,457,458,459,460,461,462,463,464,465,466,467,468,469,470,471,472,473,474,475,476,477,478,479,480,481,482,483,484,485,486,487,488,489,490,491,492,493,494,495,496,497,498,499,500,501,502,503,504,505,506,507,508,509,510,511,512,513,514,515,516,517,518,519,520,521,522,523,524,525,526,527,528,529,530,531,532,533,534,535,536,537,538,539,540,541,542,543,544,545,546,547,548,549,550,551,552,553,554,555,556,557,558,559,560,561,562,563,564,565,566,567,568,569,570,571,572,573,574,575,576,577,578,579,580,581,582,583,584,585,586,587,588,589,590,591,592,593,594,595,596,597,598,599,600,601,602,603,604,605,606,607,608,609,610,611,612,613,614,615,616,617,618,619,620,621,622,623,624,625,626,627,628,629,630,631,632,633,634,635,636,637,638,639,640,641,642,643,644,645,646,647,648,649,650,651,652,653,654,655,656,657,658,659,660,661,662,663,664,665,666,667,668,669,670,671,672,673,674,675,676,677,678,679,680,681,682,683,684,685,686,687,688,689,690,691,692,693,694,695,696,697,698,699,700,701,702,703,704,705,706,707,708,709,710,711,712,713,714,715,716,717,718,719,720,721,722,723,724,725,726,727,728,729,730,731,732,733,734,735,736,737,738,739,740,741,742,743,744,745,746,747,748,749,750,751,752,753,754,755,756,757,758,759,760,761,762,763,764,765,766,767,768,769,770,771,772,773,774,775,776,777,778,779,780,781,782,783,784,785,786,787,788,789,790,791,792,793,794,795,796,797,798,799,800,801,802,803,804,805,806,807,808,809,810,811,812,813,814,815,816,817,818,819,820,821,822,823,824,825,826,827,828,829,830,831,832,833,834,835,836,837,838,839,840,841,842,843,844,845,846,847,848,849,850,851,852,853,854,855,856,857,858,859,860,861,862,863,864,865,866,867,868,869,870,871,872,873,874,875,876,877,878,879,880,881,882,883,884,885,886,887,888,889,890,891,892,893,894,895,896,897,898,899,900,901,902,903,904,905,906,907,908,909,910,911,912,913,914,915,916,917,918,919,920,921,922,923,924,925,926,927,928,929,930,931,932,933,934,935,936,937,938,939,940,941,942,943,944,945,946,947,948,949,950,951,952,953,954,955,956,957,958,959,960,961,962,963,964,965,966,967,968,969,970,971,972,973,974,975,976,977,978,979,980,981,982,983,984,985,986,987,988,989,990,991,992,993,994,995,996,997,998,999,1000'.split(','), function(item, item_pos, item_list) {
                item_index += 1;
                var ID_925670234_m = Alloy.Collections.tipo_siniestro;
                var ID_925670234_fila = Alloy.createModel('tipo_siniestro', {
                    nombre: item + ' Siniestro',
                    id_server: '0' + item,
                    pais_texto: 'Chile',
                    id_segured: '11' + item_pos,
                    pais: 1
                });
                ID_925670234_m.add(ID_925670234_fila);
                ID_925670234_fila.save();
            });
        }
        if (Ti.App.deployType != 'production') console.log('transformando datos de modelo siniestro para modal', {});
        /** 
         * obtenemos datos para selectores 
         */
        var ID_1666945718_i = Alloy.createCollection('tipo_siniestro');
        ID_1666945718_i.fetch();
        var ID_1666945718_src = require('helper').query2array(ID_1666945718_i);
        var datos = [];
        _.each(ID_1666945718_src, function(fila, pos) {
            var new_row = {};
            _.each(fila, function(x, llave) {
                var newkey = '';
                if (llave == 'nombre') newkey = 'valor';
                if (llave == 'id_segured') newkey = 'id_segured';
                if (newkey != '') new_row[newkey] = fila[llave];
            });
            datos.push(new_row);
        });
        /** 
         * Cargamos el widget con los datos de tipo de siniestro 
         */
        $.ID_1949382907.data({
            data: datos
        });
    };
    var ID_1088992865 = setTimeout(ID_1088992865_func, 1000 * 0.2);

}

function Abrir_ID_1045626235(e) {

    var evento = e;

}

function Respuesta_ID_1050683358(e) {

    var evento = e;
    /** 
     * Mostramos en pantalla el tipo de siniestro seleccionado, y guardamos en la tabla el id del tipo de siniestro 
     */
    $.ID_1949382907.labels({
        valor: evento.valor
    });
    $.datos.set({
        id_tipo_siniestro: evento.item.id_segured
    });
    if ('datos' in $) $datos = $.datos.toJSON();
    if (Ti.App.deployType != 'production') console.log('datos recibidos de modal tipo siniestro', {
        "datos": evento
    });

}

function Blur_ID_1909800009(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    /** 
     * Cuando el campo de texto es desenfocado, actualizamos el modelo con la descripcion del siniestro 
     */
    $.datos.set({
        descripcion: elemento
    });
    if ('datos' in $) $datos = $.datos.toJSON();
    elemento = null, source = null;

}

function Change_ID_275512549(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var valor = e.value;
    /** 
     * Dejamos el valor del slider entre 0 y 100, y actualizamos el valor del porcentaje de danos en la estructura en pantalla y tabla 
     */
    var danoestructura = Math.round(elemento * 100);
    $.ID_846992339.setText(danoestructura + '%');

    $.datos.set({
        porcentaje_danos_estructura: danoestructura
    });
    if ('datos' in $) $datos = $.datos.toJSON();
    elemento = null, valor = null;

}

function Change_ID_2051169171(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    if (elemento == true || elemento == 'true') {
        var si = 'SI';
        $.ID_1826953004.setText(si);

        $.datos.set({
            analisis_especialista: elemento
        });
        if ('datos' in $) $datos = $.datos.toJSON();
    } else {
        var no = 'NO';
        $.ID_1826953004.setText(no);

        $.datos.set({
            analisis_especialista: elemento
        });
        if ('datos' in $) $datos = $.datos.toJSON();
    }
    elemento = null;

}

function Change_ID_450459547(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var valor = e.value;
    /** 
     * Dejamos el valor del slider entre 0 y 100, y actualizamos el valor del porcentaje de danos de terminaciones en pantalla y tabla 
     */
    var danoterminaciones = Math.round(elemento * 100);
    $.ID_92913882.setText(danoterminaciones + '%');

    $.datos.set({
        porcentaje_danos_terminaciones: danoterminaciones
    });
    if ('datos' in $) $datos = $.datos.toJSON();
    elemento = null, valor = null;

}

function Change_ID_398386580(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var valor = e.value;
    /** 
     * Dejamos el valor del slider entre 0 y 100, y actualizamos el valor del porcentaje de danos de instalaciones en pantalla y tabla 
     */
    var danoinstalaciones = Math.round(elemento * 100);
    $.ID_1090121881.setText(danoinstalaciones + '%');

    $.datos.set({
        porcentaje_danos_instalaciones: danoinstalaciones
    });
    if ('datos' in $) $datos = $.datos.toJSON();
    elemento = null, valor = null;

}

function Postlayout_ID_1386653099(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    $.ID_1050697219.show();

}

function Postlayout_ID_1260303045(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    /** 
     * Avisamos a la aplicacion que cierre la pantalla que estaba antes que esta... Pantalla caracteristicas 
     */
    Alloy.Events.trigger('_cerrar_insp', {
        pantalla: 'caracteristicas'
    });
    var ID_1343855696_func = function() {
        /** 
         * Una vez ya cargada la tarea, dejamos la variable limpia para poder abrir otras pantallas 
         */
        require('vars')['var_abriendo'] = '';
    };
    var ID_1343855696 = setTimeout(ID_1343855696_func, 1000 * 0.2);

}

var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
    /** 
     * Agregamos el id de inspeccion en la tabla de siniestro 
     */
    $.datos.set({
        id_inspeccion: seltarea.id_server
    });
    if ('datos' in $) $datos = $.datos.toJSON();
}
/** 
 * Escuchamos el evento para poder cerrar pantallas a medida que ya no la estamos utilizando 
 */
_my_events['_cerrar_insp,ID_1760617607'] = function(evento) {
    if (!_.isUndefined(evento.pantalla)) {
        if (evento.pantalla == 'siniestro') {
            if (Ti.App.deployType != 'production') console.log('debug cerrando siniestro', {});
            var ID_1692612534_trycatch = {
                error: function(e) {
                    if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
                }
            };
            try {
                ID_1692612534_trycatch.error = function(evento) {
                    if (Ti.App.deployType != 'production') console.log('error cerrando siniestro', {});
                };
                $.ID_1949382907.limpiar({});
                $.ID_887883297.close();
            } catch (e) {
                ID_1692612534_trycatch.error(e);
            }
        }
    } else {
        if (Ti.App.deployType != 'production') console.log('debug cerrando (todas) siniestro', {});
        var ID_1837380636_trycatch = {
            error: function(e) {
                if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
            }
        };
        try {
            ID_1837380636_trycatch.error = function(evento) {
                if (Ti.App.deployType != 'production') console.log('error cerrando siniestro', {});
            };
            $.ID_1949382907.limpiar({});
            $.ID_887883297.close();
        } catch (e) {
            ID_1837380636_trycatch.error(e);
        }
    }
};
Alloy.Events.on('_cerrar_insp', _my_events['_cerrar_insp,ID_1760617607']);

function Androidback_ID_2028314277(e) {

    e.cancelBubble = true;
    var elemento = e.source;

}

(function() {
    var ID_814182620_func = function() {
        /** 
         * Desenfocamos el campo de texto 
         */
        $.ID_1240553527.blur();
        /** 
         * Modificamos el color del statusbar 
         */
        var ID_887883297_statusbar = '#EA9F60';

        var setearStatusColor = function(ID_887883297_statusbar) {
            if (OS_IOS) {
                if (ID_887883297_statusbar == 'light' || ID_887883297_statusbar == 'claro') {
                    $.ID_887883297_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.LIGHT_CONTENT);
                } else if (ID_887883297_statusbar == 'grey' || ID_887883297_statusbar == 'gris' || ID_887883297_statusbar == 'gray') {
                    $.ID_887883297_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.GREY);
                } else if (ID_887883297_statusbar == 'oscuro' || ID_887883297_statusbar == 'dark') {
                    $.ID_887883297_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.DEFAULT);
                }
            } else if (OS_ANDROID) {
                abx.setStatusbarColor(ID_887883297_statusbar);
            }
        };
        setearStatusColor(ID_887883297_statusbar);

    };
    var ID_814182620 = setTimeout(ID_814182620_func, 1000 * 0.2);
})();

if (OS_IOS || OS_ANDROID) {
    $.ID_887883297.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}
//$.ID_887883297.open();