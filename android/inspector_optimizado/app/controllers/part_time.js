var _bind4section = {};
var _list_templates = {};

var _activity;
if (OS_ANDROID) {
    _activity = $.ID_1059906457.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_1059906457';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_1059906457.addEventListener('open', function(e) {
        abx.setBackgroundColor("white");
    });
}
$.ID_1059906457.orientationModes = [Titanium.UI.PORTRAIT];


$.ID_803579040.init({
    titulo: 'ENROLAMIENTO',
    __id: 'ALL803579040',
    fondo: 'fondoblanco',
    colortitulo: 'negro',
    modal: ''
});


$.ID_1614118096.init({
    titulo: 'PARTE 4: Disponibilidad de trabajo',
    __id: 'ALL1614118096',
    avance: '4/6',
    onclick: Click_ID_1377656868
});

function Click_ID_1377656868(e) {

    var evento = e;

}

$.ID_1132173110.init({
    __id: 'ALL1132173110',
    letra: 'L',
    onon: on_ID_1237590678,
    onoff: Off_ID_1150929050
});

function on_ID_1237590678(e) {
    /** 
     * Dependiendo de la seleccion (encendido o apagado) se actualiza el registro para definir los dias disponibles para trabajar 
     */

    var evento = e;
    var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
    var registro = _.extend(registro, {
        d1: 1
    });

}

function Off_ID_1150929050(e) {

    var evento = e;
    var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
    var registro = _.extend(registro, {
        d1: 0
    });

}

$.ID_1396533677.init({
    __id: 'ALL1396533677',
    letra: 'M',
    onon: on_ID_727785297,
    onoff: Off_ID_610801192
});

function on_ID_727785297(e) {

    var evento = e;
    var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
    var registro = _.extend(registro, {
        d2: 1
    });

}

function Off_ID_610801192(e) {

    var evento = e;
    var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
    var registro = _.extend(registro, {
        d2: 0
    });

}

$.ID_364580565.init({
    __id: 'ALL364580565',
    letra: 'MI',
    onon: on_ID_1272243659,
    onoff: Off_ID_1236282682
});

function on_ID_1272243659(e) {

    var evento = e;
    var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
    var registro = _.extend(registro, {
        d3: 1
    });

}

function Off_ID_1236282682(e) {

    var evento = e;
    var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
    var registro = _.extend(registro, {
        d3: 0
    });

}

$.ID_180804538.init({
    __id: 'ALL180804538',
    letra: 'J',
    onon: on_ID_151645229,
    onoff: Off_ID_768817667
});

function on_ID_151645229(e) {

    var evento = e;
    var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
    var registro = _.extend(registro, {
        d4: 1
    });

}

function Off_ID_768817667(e) {

    var evento = e;
    var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
    var registro = _.extend(registro, {
        d4: 0
    });

}

$.ID_457319757.init({
    __id: 'ALL457319757',
    letra: 'V',
    onon: on_ID_1948647970,
    onoff: Off_ID_119088936
});

function on_ID_1948647970(e) {

    var evento = e;
    var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
    var registro = _.extend(registro, {
        d5: 1
    });

}

function Off_ID_119088936(e) {

    var evento = e;
    var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
    var registro = _.extend(registro, {
        d5: 0
    });

}

$.ID_993907842.init({
    __id: 'ALL993907842',
    letra: 'S',
    onon: on_ID_1501368637,
    onoff: Off_ID_1685362431
});

function on_ID_1501368637(e) {

    var evento = e;
    var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
    var registro = _.extend(registro, {
        d6: 1
    });

}

function Off_ID_1685362431(e) {

    var evento = e;
    var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
    var registro = _.extend(registro, {
        d6: 0
    });

}

$.ID_947148270.init({
    __id: 'ALL947148270',
    letra: 'D',
    onon: on_ID_6265224,
    onoff: Off_ID_689881562
});

function on_ID_6265224(e) {

    var evento = e;
    var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
    var registro = _.extend(registro, {
        d7: 1
    });

}

function Off_ID_689881562(e) {

    var evento = e;
    var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
    var registro = _.extend(registro, {
        d7: 0
    });

}

$.ID_1517641115.init({
    __id: 'ALL1517641115',
    onchange: Change_ID_267735168,
    mins: true
});

function Change_ID_267735168(e) {

    var evento = e;
    /** 
     * Obtenemos los valores desde el widget y los guardamos en variables 
     */
    require('vars')['desde'] = evento.desde;
    require('vars')['hasta'] = evento.hasta;

}

$.ID_1929710970.init({
    titulo: 'CONTINUAR',
    __id: 'ALL1929710970',
    onclick: Click_ID_145378811
});

function Click_ID_145378811(e) {

    var evento = e;
    /** 
     * Recuperamos variables, creamos una variable para identificar que alguna opcion este siendo elegida, y que la hora de fin sea mayor a la de inicio 
     */
    var desde = ('desde' in require('vars')) ? require('vars')['desde'] : '';
    var hasta = ('hasta' in require('vars')) ? require('vars')['hasta'] : '';
    var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';

    //validaciones
    var al_menos_uno = registro.d1 || registro.d2 || registro.d3 || registro.d4 || registro.d5 || registro.d6 || registro.d7;
    desde = parseInt(desde);
    hasta = parseInt(hasta)
    if (al_menos_uno == false || al_menos_uno == 'false') {
        var ID_1232101512_opts = ['Aceptar'];
        var ID_1232101512 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Seleccione a lo menos un día',
            buttonNames: ID_1232101512_opts
        });
        ID_1232101512.addEventListener('click', function(e) {
            var suu = ID_1232101512_opts[e.index];
            suu = null;

            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1232101512.show();
    } else if (_.isNumber(desde) && _.isNumber(hasta) && desde > hasta) {
        var ID_594317031_opts = ['Aceptar'];
        var ID_594317031 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese su horario de trabajo',
            buttonNames: ID_594317031_opts
        });
        ID_594317031.addEventListener('click', function(e) {
            var suu = ID_594317031_opts[e.index];
            suu = null;

            e.source.removeEventListener("click", arguments.callee);
        });
        ID_594317031.show();
    } else if (desde == hasta) {
        var ID_981623759_opts = ['Aceptar'];
        var ID_981623759 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'El rango debe ser superior a una hora',
            buttonNames: ID_981623759_opts
        });
        ID_981623759.addEventListener('click', function(e) {
            var suu = ID_981623759_opts[e.index];
            suu = null;

            e.source.removeEventListener("click", arguments.callee);
        });
        ID_981623759.show();
    } else {
        /** 
         * Iniciamos animacion, despues de 0.3 segundos detenemos animacion y carga pantalla contacto 
         */

        $.ID_1929710970.iniciar_progreso({});
        /** 
         * Actualizamos la variable del registro 
         */
        var registro = _.extend(registro, {
            disp_horas: desde + ':00 ' + hasta + ':00'
        });
        require('vars')['registro'] = registro;
        var ID_285467879_func = function() {

            $.ID_1929710970.detener_progreso({});
            Alloy.createController("contactos", {}).getView().open();
        };
        var ID_285467879 = setTimeout(ID_285467879_func, 1000 * 0.3);
    }
}

(function() {
    var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
    /** 
     * Cuando se recupera la variable de registro, cargamos la disponibilidad en 0 para que el usuario deba escoger un dia al menos de disponibilidad 
     */
    var registro = _.extend(registro, {
        d1: 0,
        d2: 0,
        d3: 0,
        d4: 0,
        d5: 0,
        d6: 0,
        d7: 0
    });
    require('vars')['registro'] = registro;
    /** 
     * Guardamos las horas disponibles en 1 para que el usuario deba escoger un horario 
     */
    require('vars')['desde'] = '1';
    require('vars')['hasta'] = '1';
    /** 
     * Creamos evento que al cerrar desde la ultima pantalla, esta tambi&#233;n se cierre. Evitamos el uso excesivo de memoria ram 
     */

    _my_events['_close_enrolamiento,ID_495036846'] = function(evento) {
        if (Ti.App.deployType != 'production') console.log('escuchando cerrar enrolamiento parttime', {});
        $.ID_1059906457.close();
    };
    Alloy.Events.on('_close_enrolamiento', _my_events['_close_enrolamiento,ID_495036846']);
})();

function Androidback_ID_1341461228(e) {
    /** 
     * Dejamos esta accion vacia para que no pueda volver a la pantalla anterior 
     */

    e.cancelBubble = true;
    var elemento = e.source;

}
if (OS_IOS || OS_ANDROID) {
    $.ID_1059906457.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}