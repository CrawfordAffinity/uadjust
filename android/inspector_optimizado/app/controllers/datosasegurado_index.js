var _bind4section = {};
var _list_templates = {};

var _activity;
if (OS_ANDROID) {
    _activity = $.ID_121244764.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_121244764';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_121244764.addEventListener('open', function(e) {});
}
$.ID_121244764.orientationModes = [Titanium.UI.PORTRAIT];


$.ID_1950928663.init({
    titulo: 'DATOS DEL ASEGURADO',
    __id: 'ALL1950928663',
    fondo: 'fondoblanco',
    colortitulo: 'negro',
    modal: '',
    onpresiono: Presiono_ID_1587391517
});

function Presiono_ID_1587391517(e) {

    var evento = e;

}

$.ID_1303818490.init({
    titulo: 'LLAMAR ASEGURADO',
    __id: 'ALL1303818490',
    color: 'verde',
    ancho: '90%',
    icono: 'telefono',
    onclick: Click_ID_659160764
});

function Click_ID_659160764(e) {

    var evento = e;
    var telefono;
    telefono = $.ID_1557692345.getText();

    if (OS_ANDROID) {
        try {
            var intent = Ti.Android.createIntent({
                action: Ti.Android.ACTION_CALL,
                data: 'tel:' + '+' + telefono + ''
            });
            Ti.Android.currentActivity.startActivity(intent);
        } catch (e) {
            Ti.Platform.openURL('tel:' + '+' + telefono + '');
        }
    } else {
        Ti.Platform.openURL('tel:' + '+' + telefono + '');
    }

}

$.ID_1457490135.init({
    __id: 'ALL1457490135',
    texto: 'Tip: No llames a horas imprudentes y mantén siempre una buena actitud',
    tipo: '_tip'
});


(function() {
    /** 
     * Recuperamos la variable con el detalle de la tarea seleccionada y mostramos los datos en pantalla 
     */
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    $.ID_1804952842.setText(seltarea.asegurado_nombre);

    $.ID_1557692345.setText(Math.round(seltarea.asegurado_tel_movil));

    $.ID_1252140689.setText(seltarea.asegurado_correo);

    $.ID_1935931444.setText(seltarea.num_caso);

})();

if (OS_IOS || OS_ANDROID) {
    $.ID_121244764.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}
//$.ID_121244764.open();