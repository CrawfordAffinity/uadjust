var _bind4section = {};
var _list_templates = {};

var _activity;
if (OS_ANDROID) {
    _activity = $.ID_651351303.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_651351303';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_651351303.addEventListener('open', function(e) {});
}
$.ID_651351303.orientationModes = [Titanium.UI.PORTRAIT];


$.ID_1478346774.init({
    titulo: 'ENROLAMIENTO',
    __id: 'ALL1478346774',
    fondo: 'fondoblanco',
    colortitulo: 'negro',
    modal: ''
});


$.ID_1399189593.init({
    titulo: 'PARTE 3: Domicilio',
    __id: 'ALL1399189593',
    avance: '3/6',
    onclick: Click_ID_1881500399
});

function Click_ID_1881500399(e) {

    var evento = e;
    $.ID_1357980546.blur();
    $.ID_1642423363.blur();
    $.ID_1023458367.blur();
    $.ID_458987331.blur();

}

$.ID_1392146322.init({
    titulo: 'SELECCIONE REGION',
    __id: 'ALL1392146322',
    left: 0,
    onrespuesta: Respuesta_ID_1334377333,
    campo: 'Region',
    onabrir: Abrir_ID_1091489738,
    right: 0,
    seleccione: 'seleccione region',
    onafterinit: Afterinit_ID_1073324780
});

function Afterinit_ID_1073324780(e) {

    var evento = e;
    /** 
     * Recuperamos la variable con los datos de las regiones y las cargamos en el widget 
     */
    var data_region = ('data_region' in require('vars')) ? require('vars')['data_region'] : '';
    if (Ti.App.deployType != 'production') console.log('enviando data a widget', {
        "data": data_region
    });

    $.ID_1392146322.data({
        data: data_region
    });

}

function Abrir_ID_1091489738(e) {

    var evento = e;
    $.ID_1357980546.blur();
    $.ID_1642423363.blur();
    $.ID_1023458367.blur();
    $.ID_458987331.blur();

}

function Respuesta_ID_1334377333(e) {

    var evento = e;
    if (Ti.App.deployType != 'production') console.log('datos recibidos de modal', {
        "datos": evento
    });
    /** 
     * Ponemos el texto de la region escogida 
     */

    $.ID_1392146322.labels({
        valor: evento.valor
    });
    /** 
     * Guardamos el id del valor seleccionado 
     */
    require('vars')['data_nivel_1'] = evento.item;

}

$.ID_337746182.init({
    titulo: 'CONTINUAR',
    __id: 'ALL337746182',
    onclick: Click_ID_1788736104
});

function Click_ID_1788736104(e) {

    var evento = e;
    /** 
     * Obtenemos los valores de los campos 
     */
    var direccion;
    direccion = $.ID_1015210318.getValue();

    var data_nivel_2;
    data_nivel_2 = $.ID_1357980546.getValue();

    var data_nivel_3;
    data_nivel_3 = $.ID_1642423363.getValue();

    var data_nivel_4;
    data_nivel_4 = $.ID_1023458367.getValue();

    var data_nivel_5;
    data_nivel_5 = $.ID_458987331.getValue();

    var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
    var data_nivel_1 = ('data_nivel_1' in require('vars')) ? require('vars')['data_nivel_1'] : '';
    /** 
     * Creamos una variable para ver si falta algun dato por ser ingresado 
     */

    var falta_algo =
        (data_nivel_2 == "" && pais.label_nivel2 != "") ||
        (data_nivel_3 == "" && pais.label_nivel3 != "") ||
        (data_nivel_4 == "" && pais.label_nivel4 != "") ||
        (data_nivel_5 == "" && pais.label_nivel5 != "") ||
        (direccion == "")
    if ((_.isObject(data_nivel_1) || _.isString(data_nivel_1)) && _.isEmpty(data_nivel_1)) {
        var ID_382160665_opts = ['Aceptar'];
        var ID_382160665 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Seleccione región',
            buttonNames: ID_382160665_opts
        });
        ID_382160665.addEventListener('click', function(e) {
            var suu = ID_382160665_opts[e.index];
            suu = null;

            e.source.removeEventListener("click", arguments.callee);
        });
        ID_382160665.show();
    } else if ((_.isObject(direccion) || _.isString(direccion)) && _.isEmpty(direccion)) {
        var ID_1688664109_opts = ['Aceptar'];
        var ID_1688664109 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese dirección',
            buttonNames: ID_1688664109_opts
        });
        ID_1688664109.addEventListener('click', function(e) {
            var suu = ID_1688664109_opts[e.index];
            suu = null;

            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1688664109.show();
    } else if (falta_algo == true || falta_algo == 'true') {
        var ID_156144414_opts = ['Aceptar'];
        var ID_156144414 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese datos faltantes',
            buttonNames: ID_156144414_opts
        });
        ID_156144414.addEventListener('click', function(e) {
            var suu = ID_156144414_opts[e.index];
            suu = null;

            e.source.removeEventListener("click", arguments.callee);
        });
        ID_156144414.show();
    } else {
        var data_nivel_1 = ('data_nivel_1' in require('vars')) ? require('vars')['data_nivel_1'] : '';
        if (_.isString(data_nivel_1)) {
            /** 
             * generamos la url para hacer la consulta a google 
             */

            var url = data_nivel_1.replace(" ", "+");
            url = url + "+" + data_nivel_2.replace(" ", "+");
            url = url + "+" + data_nivel_3.replace(" ", "+");
            url = url + "+" + data_nivel_4.replace(" ", "+");
            url = url + "+" + data_nivel_5.replace(" ", "+");
            url = url + "+" + direccion.replace(" ", "+");
            url = url + "+" + pais.nombre;
            url2 = "https://maps.googleapis.com/maps/api/geocode/json?address=" + url
        } else {

            var url = data_nivel_1.valor.replace(" ", "+");
            url = url + "+" + data_nivel_2.replace(" ", "+");
            url = url + "+" + data_nivel_3.replace(" ", "+");
            url = url + "+" + data_nivel_4.replace(" ", "+");
            url = url + "+" + data_nivel_5.replace(" ", "+");
            url = url + "+" + direccion.replace(" ", "+");
            url = url + "+" + pais.nombre;
            url2 = "https://maps.googleapis.com/maps/api/geocode/json?address=" + url
        }
        if (Ti.App.deployType != 'production') console.log('Consultando a Google si direccion es valida', {
            "url": url2
        });

        $.ID_337746182.iniciar_progreso({});
        var ID_1238770118 = {};

        ID_1238770118.success = function(e) {
            var elemento = e,
                valor = e;
            if (Ti.App.deployType != 'production') console.log('respuesta de google', {
                "datos": elemento
            });
            if (elemento.status == 'OK') {
                if (elemento.results[0].formatted_address.toLowerCase().indexOf(direccion.toLowerCase()) != -1) {
                    var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
                    var data_nivel_1 = ('data_nivel_1' in require('vars')) ? require('vars')['data_nivel_1'] : '';
                    /** 
                     * Agregamos los campos de los niveles a la variable del registro, diferenciamos si nivel1 es texto o id 
                     */
                    var registro = _.extend(registro, {
                        nivel_2: data_nivel_2,
                        nivel_3: data_nivel_3,
                        nivel_4: data_nivel_4,
                        nivel_5: data_nivel_5
                    });
                    if (_.isString(data_nivel_1)) {
                        var registro = _.extend(registro, {
                            nivel_1: data_nivel_1
                        });
                    } else {
                        var registro = _.extend(registro, {
                            nivel_1: data_nivel_1.id_server
                        });
                    }
                    var registro = _.extend(registro, {
                        latitude: elemento.results[0].geometry.location.lat,
                        longitude: elemento.results[0].geometry.location.lng,
                        correccion_direccion: 0,
                        direccion: elemento.results[0].formatted_address.split(',')[0]
                    });

                    registro.google = JSON.stringify(elemento.results[0].address_components)
                        /** 
                         * Guardamos los nuevos datos de registro 
                         */
                    require('vars')['registro'] = registro;

                    $.ID_337746182.detener_progreso({});
                    /** 
                     * Guardamos y enviamos a la proxima pantalla, disponibilidad de trabajo 
                     */
                    Alloy.createController("disponibilidad_de_trabajo", {}).getView().open();
                } else {
                    var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
                    /** 
                     * Agregamos los campos nuevos, y mostramos al usuario la direccion que encontro google 
                     */
                    var registro = _.extend(registro, {
                        nivel_2: data_nivel_2,
                        nivel_3: data_nivel_3,
                        nivel_4: data_nivel_4,
                        nivel_5: data_nivel_5
                    });

                    registro.google = JSON.stringify(elemento.results[0].address_components)
                    var registro = _.extend(registro, {
                        latitude: elemento.results[0].geometry.location.lat,
                        longitude: elemento.results[0].geometry.location.lng,
                        correccion_direccion: 0,
                        direccion: elemento.results[0].formatted_address.split(',')[0]
                    });
                    require('vars')['registro'] = registro;
                    require('vars')['resultado'] = elemento.results[0];

                    $.ID_337746182.detener_progreso({});
                    var esta_es_correcta = 'Es esta la correcta? ';
                    ID_1290014146.setMessage(esta_es_correcta + ' ' + elemento.results[0].formatted_address);

                    ID_1290014146.show();
                }
            } else {
                /** 
                 * En el caso de la respuesta de google no sea ok, mostramos este mensaje 
                 */
                var ID_555754627_opts = ['OK'];
                var ID_555754627 = Ti.UI.createAlertDialog({
                    title: 'Atencion',
                    message: 'Error al verificar la dirección, reintente',
                    buttonNames: ID_555754627_opts
                });
                ID_555754627.addEventListener('click', function(e) {
                    var xdd = ID_555754627_opts[e.index];
                    xdd = null;

                    e.source.removeEventListener("click", arguments.callee);
                });
                ID_555754627.show();

                $.ID_337746182.detener_progreso({});
            }
            elemento = null, valor = null;
        };

        ID_1238770118.error = function(e) {
            var elemento = e,
                valor = e;
            if (Ti.App.deployType != 'production') console.log('hubo error consultando google, asumimos direccion es correcta', {});

            $.ID_337746182.detener_progreso({});
            var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
            var data_nivel_1 = ('data_nivel_1' in require('vars')) ? require('vars')['data_nivel_1'] : '';
            var registro = _.extend(registro, {
                nivel_2: data_nivel_2,
                nivel_3: data_nivel_3,
                nivel_4: data_nivel_4,
                nivel_5: data_nivel_5,
                direccion: direccion,
                correccion_direccion: 1,
                google: '',
                longitude: '',
                latitude: ''
            });
            if (_.isString(data_nivel_1)) {
                var registro = _.extend(registro, {
                    nivel_1: data_nivel_1
                });
            } else {
                var registro = _.extend(registro, {
                    nivel_1: data_nivel_1.id_server
                });
            }
            require('vars')['registro'] = registro;
            Alloy.createController("disponibilidad_de_trabajo", {}).getView().open();
            elemento = null, valor = null;
        };
        require('helper').ajaxUnico('ID_1238770118', 'https://maps.googleapis.com/maps/api/geocode/json?address=' + url + '&key=AIzaSyDdRVLOtyNvZUAleL0mDO-mAWd2M9AircQ', 'GET', {}, 15000, ID_1238770118);
    }
}

(function() {
    var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
    var ID_1237889420_i = Alloy.createCollection('pais');
    var ID_1237889420_i_where = 'id_server=\'' + registro.pais + '\'';
    ID_1237889420_i.fetch({
        query: 'SELECT * FROM pais WHERE id_server=\'' + registro.pais + '\''
    });
    var pais_iter = require('helper').query2array(ID_1237889420_i);
    /** 
     * Creamos alias para el pais 
     */
    var pais = pais_iter[0];
    var niveles_pais = 1;
    /** 
     * Cambiamos titulo al widget segun los datos del primer nivel del pais 
     */

    $.ID_1392146322.labels({
        nivel1: pais.label_nivel1
    });
    if ((_.isObject(pais.label_nivel2) || _.isString(pais.label_nivel2)) && _.isEmpty(pais.label_nivel2)) {
        /** 
         * Dejamos los campos de textos que existan segun el pais, si no existen, se eliminan 
         */
        var scroll_view;
        scroll_view = $.ID_1446076465;
        var nivel_2_view;
        nivel_2_view = $.ID_1654524317;
        scroll_view.remove(nivel_2_view);
    } else {
        $.ID_1950087430.setText(pais.label_nivel2);

        $.ID_1357980546.setHintText('escribir ' + pais.label_nivel2);

    }
    if ((_.isObject(pais.label_nivel3) || _.isString(pais.label_nivel3)) && _.isEmpty(pais.label_nivel3)) {
        var scroll_view;
        scroll_view = $.ID_1446076465;
        var nivel_3_view;
        nivel_3_view = $.ID_1000235555;
        scroll_view.remove(nivel_3_view);
    } else {
        $.ID_1712815186.setText(pais.label_nivel3);

        $.ID_1642423363.setHintText('escribir ' + pais.label_nivel3);

    }
    if ((_.isObject(pais.label_nivel4) || _.isString(pais.label_nivel4)) && _.isEmpty(pais.label_nivel4)) {
        var scroll_view;
        scroll_view = $.ID_1446076465;
        var nivel_4_view;
        nivel_4_view = $.ID_1017726378;
        scroll_view.remove(nivel_4_view);
    } else {
        $.ID_1795647174.setText(pais.label_nivel4);

        $.ID_1023458367.setHintText('escribir ' + pais.label_nivel4);

    }
    if ((_.isObject(pais.label_nivel5) || _.isString(pais.label_nivel5)) && _.isEmpty(pais.label_nivel5)) {
        var scroll_view;
        scroll_view = $.ID_1446076465;
        var nivel_5_view;
        nivel_5_view = $.ID_211790495;
        scroll_view.remove(nivel_5_view);
    } else {
        label_texto = pais.label_nivel5;
        $.ID_705299174.setText(pais.label_nivel5);

        $.ID_458987331.setHintText('escribir ' + pais.label_nivel5);

    }
    var ID_1809947659_i = Alloy.createCollection('nivel1');
    var ID_1809947659_i_where = 'id_pais=\'' + pais.id_pais + '\'';
    ID_1809947659_i.fetch({
        query: 'SELECT * FROM nivel1 WHERE id_pais=\'' + pais.id_pais + '\''
    });
    var region_list = require('helper').query2array(ID_1809947659_i);
    var item_index = 0;
    _.each(region_list, function(item, item_pos, item_list) {
        item_index += 1;
        item.valor = item.id_label;
    });
    if (Ti.App.deployType != 'production') console.log('armado de regiones dice', {
        "data": region_list
    });
    require('vars')['data_region'] = region_list;
    require('vars')['pais'] = pais;
    /** 
     * guardamos las variables de data_nivel en vacio 
     */
    require('vars')['data_nivel_1'] = '';
    require('vars')['data_nivel_2'] = '';
    require('vars')['data_nivel_3'] = '';
    require('vars')['data_nivel_4'] = '';
    require('vars')['data_nivel_5'] = '';
    /** 
     * Creamos evento que al cerrar desde la ultima pantalla, esta tambi&#233;n se cierre. Evitamos el uso excesivo de memoria ram 
     */

    _my_events['_close_enrolamiento,ID_1897903097'] = function(evento) {
        if (Ti.App.deployType != 'production') console.log('escuchando cerrar enrolamiento domicilio', {});
        $.ID_651351303.close();
    };
    Alloy.Events.on('_close_enrolamiento', _my_events['_close_enrolamiento,ID_1897903097']);
    var ID_974303232_func = function() {
        $.ID_1357980546.blur();
        $.ID_1642423363.blur();
        $.ID_1023458367.blur();
        $.ID_458987331.blur();
        $.ID_1015210318.blur();
    };
    var ID_974303232 = setTimeout(ID_974303232_func, 1000 * 0.2);
})();


var ID_1290014146_opts = ['Si', 'No', 'Continuar'];
var ID_1290014146 = Ti.UI.createAlertDialog({
    title: 'La direccion indicada no parece válida',
    message: '-',
    buttonNames: ID_1290014146_opts
});
ID_1290014146.addEventListener('click', function(e) {
    var resp_pregunta = ID_1290014146_opts[e.index];
    var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
    var data_nivel_1 = ('data_nivel_1' in require('vars')) ? require('vars')['data_nivel_1'] : '';
    var resultado = ('resultado' in require('vars')) ? require('vars')['resultado'] : '';
    if (_.isString(data_nivel_1)) {
        /** 
         * si la region fue ingresada a mano, asociamos el texto en vez del id_server. Hasta ahora, eso no deberia suceder ya que solo almacenamos el id_server 
         */
        var registro = _.extend(registro, {
            nivel_1: data_nivel_1
        });
    } else {
        var registro = _.extend(registro, {
            nivel_1: data_nivel_1.id_server
        });
    }
    if (resp_pregunta == 'Si') {
        /** 
         * Guardamos los cambios y continuamos a la proxima pantalla, disponibilidad de trabajo 
         */
        require('vars')['registro'] = registro;
        Alloy.createController("disponibilidad_de_trabajo", {}).getView().open();
    } else if (resp_pregunta == 'No') {
        /** 
         * Si la direccion indicada no es la correcta, dejamos los datos de latitud, longitud y direccion en vacio. Y dejamos que el usuario reintente su direccion 
         */
        var registro = _.extend(registro, {
            latitude: '',
            longitude: '',
            direccion: ''
        });
        ID_1290014146.hide();
        require('vars')['registro'] = registro;
    } else {
        /** 
         * En el caso que el usuario haya escrito su direccion y google no la encuentra, dejamos en blanco la longitud, latitud, respuesta de google y dejamos el flag de coreccion_direccion en 1 
         */
        var registro = _.extend(registro, {
            longitude: '',
            latitude: '',
            google: '',
            correccion_direccion: 1
        });
        /** 
         * Omitimos los campos anteriores y continuamos a la proxima pantalla, disponibilidad de trabajo 
         */
        require('vars')['registro'] = registro;
        Alloy.createController("disponibilidad_de_trabajo", {}).getView().open();
    }
    resp_pregunta = null;

});

function Androidback_ID_409690481(e) {
    /** 
     * Dejamos esta accion vacia para que no pueda volver a la pantalla anterior 
     */

    e.cancelBubble = true;
    var elemento = e.source;

}
if (OS_IOS || OS_ANDROID) {
    $.ID_651351303.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}