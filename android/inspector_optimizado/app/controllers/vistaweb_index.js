var _bind4section = {};
var _list_templates = {};

var _activity;
if (OS_ANDROID) {
    _activity = $.ID_1068546356.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_1068546356';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_1068546356.addEventListener('open', function(e) {});
}
$.ID_1068546356.orientationModes = [Titanium.UI.PORTRAIT];


$.ID_1709091774.init({
    titulo: 'Pagina web',
    onsalirinsp: Salirinsp_ID_2069184044,
    __id: 'ALL1709091774',
    continuar: '',
    salir_insp: '',
    fondo: 'fondorosado',
    top: 0,
    onpresiono: Presiono_ID_80270477
});

function Salirinsp_ID_2069184044(e) {

    var evento = e;
    var ID_2045532719_opts = ['Asegurado no puede seguir', 'Se me acabo la bateria', 'Tuve un accidente', 'Cancelar'];
    var ID_2045532719 = Ti.UI.createOptionDialog({
        title: 'RAZON PARA CANCELAR INSPECCION ACTUAL',
        options: ID_2045532719_opts
    });
    ID_2045532719.addEventListener('click', function(e) {
        var resp = ID_2045532719_opts[e.index];
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var razon = "";
            if (resp == 'Asegurado no puede seguir') {
                razon = "Asegurado no puede seguir";
            }
            if (resp == 'Se me acabo la bateria') {
                razon = "Se me acabo la bateria";
            }
            if (resp == 'Tuve un accidente') {
                razon = "Tuve un accidente";
            }
            if (Ti.App.deployType != 'production') console.log('mi razon es', {
                "datos": razon
            });
            if ((_.isObject(razon) || (_.isString(razon)) && !_.isEmpty(razon)) || _.isNumber(razon) || _.isBoolean(razon)) {
                if (Ti.App.deployType != 'production') console.log('llamando servicio cancelarTarea', {});
                require('vars')['insp_cancelada'] = 'siniestro';
                var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
                var ID_1809090752_visible = true;

                if (ID_1809090752_visible == 'si') {
                    ID_1809090752_visible = true;
                } else if (ID_1809090752_visible == 'no') {
                    ID_1809090752_visible = false;
                }
                $.ID_1809090752.setVisible(ID_1809090752_visible);

                var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
                var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
                if (Ti.App.deployType != 'production') console.log('detalle de seltarea', {
                    "data": seltarea
                });
                var datos = {
                    id_inspector: inspector.id_server,
                    codigo_identificador: inspector.codigo_identificador,
                    id_server: seltarea.id_server,
                    num_caso: seltarea.num_caso,
                    razon: razon
                };
                require('vars')[_var_scopekey]['datos'] = datos;
                var ID_1178779290 = {};

                ID_1178779290.success = function(e) {
                    var elemento = e,
                        valor = e;
                    var ID_1809090752_visible = false;

                    if (ID_1809090752_visible == 'si') {
                        ID_1809090752_visible = true;
                    } else if (ID_1809090752_visible == 'no') {
                        ID_1809090752_visible = false;
                    }
                    $.ID_1809090752.setVisible(ID_1809090752_visible);

                    $.ID_516080514.hide();
                    Alloy.createController("firma_index", {}).getView().open();
                    var ID_1530763560_func = function() {

                        Alloy.Events.trigger('_cerrar_insp', {
                            pantalla: 'vistaweb'
                        });
                    };
                    var ID_1530763560 = setTimeout(ID_1530763560_func, 1000 * 0.5);
                    elemento = null, valor = null;
                };

                ID_1178779290.error = function(e) {
                    var elemento = e,
                        valor = e;
                    if (Ti.App.deployType != 'production') console.log('hubo error llamando cancelarTarea, la agregamos a cola de salida', {
                        "elemento": elemento
                    });
                    if (Ti.App.deployType != 'production') console.log('agregando servicio cancelarTarea a cola', {});
                    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
                    var datos = ('datos' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['datos'] : '';
                    var ID_1913824379_m = Alloy.Collections.cola;
                    var ID_1913824379_fila = Alloy.createModel('cola', {
                        data: JSON.stringify(datos),
                        id_tarea: seltarea.id_server,
                        tipo: 'cancelar'
                    });
                    ID_1913824379_m.add(ID_1913824379_fila);
                    ID_1913824379_fila.save();
                    var ID_1809090752_visible = false;

                    if (ID_1809090752_visible == 'si') {
                        ID_1809090752_visible = true;
                    } else if (ID_1809090752_visible == 'no') {
                        ID_1809090752_visible = false;
                    }
                    $.ID_1809090752.setVisible(ID_1809090752_visible);

                    $.ID_516080514.hide();
                    Alloy.createController("firma_index", {}).getView().open();
                    var ID_851467821_func = function() {

                        Alloy.Events.trigger('_cerrar_insp', {
                            pantalla: 'vistaweb'
                        });
                    };
                    var ID_851467821 = setTimeout(ID_851467821_func, 1000 * 0.5);
                    elemento = null, valor = null;
                };
                require('helper').ajaxUnico('ID_1178779290', '' + url_server + 'cancelarTarea' + '', 'POST', {
                    id_inspector: seltarea.id_inspector,
                    codigo_identificador: inspector.codigo_identificador,
                    id_tarea: seltarea.id_server,
                    num_caso: seltarea.num_caso,
                    mensaje: razon,
                    opcion: 0,
                    tipo: 1
                }, 15000, ID_1178779290);
            }
        }
        resp = null;

        e.source.removeEventListener("click", arguments.callee);
    });
    ID_2045532719.show();

}

function Presiono_ID_80270477(e) {

    var evento = e;
    $.ID_516080514.hide();
    Alloy.createController("firma_index", {}).getView().open();

}

function Postlayout_ID_1349461170(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    $.ID_1216361550.show();

}

function Postlayout_ID_1636040712(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var ID_1707636476_func = function() {
        /** 
         * Una vez ya cargada la tarea, dejamos la variable limpia para poder abrir otras pantallas 
         */
        require('vars')['var_abriendo'] = '';
    };
    var ID_1707636476 = setTimeout(ID_1707636476_func, 1000 * 0.2);
    /** 
     * Cerramos pantalla contenidos 
     */

    Alloy.Events.trigger('_cerrar_insp', {
        pantalla: 'contenidos'
    });

}

function Androidback_ID_2139645792(e) {

    e.cancelBubble = true;
    var elemento = e.source;

}


_my_events['_cerrar_insp,ID_1616976104'] = function(evento) {
    if (!_.isUndefined(evento.pantalla)) {
        if (evento.pantalla == 'vistaweb') {
            if (Ti.App.deployType != 'production') console.log('debug cerrando vistaweb', {});

            var ID_2003061186_trycatch = {
                error: function(e) {
                    if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
                }
            };
            try {
                ID_2003061186_trycatch.error = function(evento) {
                    if (Ti.App.deployType != 'production') console.log('error cerrando vistaweb', {});
                };
                $.ID_1068546356.close();
            } catch (e) {
                ID_2003061186_trycatch.error(e);
            }
        }
    } else {
        if (Ti.App.deployType != 'production') console.log('debug cerrando (todas) vistaweb', {});

        var ID_1367362437_trycatch = {
            error: function(e) {
                if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
            }
        };
        try {
            ID_1367362437_trycatch.error = function(evento) {
                if (Ti.App.deployType != 'production') console.log('error cerrando vistaweb', {});
            };
            $.ID_1068546356.close();
        } catch (e) {
            ID_1367362437_trycatch.error(e);
        }
    }
};
Alloy.Events.on('_cerrar_insp', _my_events['_cerrar_insp,ID_1616976104']);


(function() {
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    $.ID_516080514.setUrl('http://lineahogar.crawfordaffinity.com/integracion/triage.aspx?idTarea=' + seltarea.num_caso);
    /** 
     * Cambiamos el color del statusbar 
     */
    var ID_1714814641_func = function() {
        var ID_1068546356_statusbar = '#E87C7C';

        var setearStatusColor = function(ID_1068546356_statusbar) {
            if (OS_IOS) {
                if (ID_1068546356_statusbar == 'light' || ID_1068546356_statusbar == 'claro') {
                    $.ID_1068546356_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.LIGHT_CONTENT);
                } else if (ID_1068546356_statusbar == 'grey' || ID_1068546356_statusbar == 'gris' || ID_1068546356_statusbar == 'gray') {
                    $.ID_1068546356_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.GREY);
                } else if (ID_1068546356_statusbar == 'oscuro' || ID_1068546356_statusbar == 'dark') {
                    $.ID_1068546356_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.DEFAULT);
                }
            } else if (OS_ANDROID) {
                abx.setStatusbarColor(ID_1068546356_statusbar);
            }
        };
        setearStatusColor(ID_1068546356_statusbar);

    };
    var ID_1714814641 = setTimeout(ID_1714814641_func, 1000 * 0.1);

    var ID_1850747760 = function(evento) {
        evento.cancelBubble = true;
        if (Ti.App.deployType != 'production') console.log('evento', {
            "evento": evento
        });
        if (evento.url.toLowerCase().indexOf('/continuar_inspeccion/'.toLowerCase()) != -1) {
            if (Ti.App.deployType != 'production') console.log('la url dice que tenemos que seguir la inspeccion', {});
            $.ID_516080514.hide();
            Alloy.createController("firma_index", {}).getView().open();
        } else {
            if (Ti.App.deployType != 'production') console.log('la url es otra cosa', {});
        }
    };
    $.ID_516080514.addEventListener('beforeload', ID_1850747760);

    var ID_721833315 = function(evento) {
        evento.cancelBubble = true;
        var eltitulo;
        eltitulo = $.ID_516080514.evalJS('document.title');

        if (Ti.App.deployType != 'production') console.log('la pagina web dice ' + eltitulo, {});
        if ((_.isObject(eltitulo) || (_.isString(eltitulo)) && !_.isEmpty(eltitulo)) || _.isNumber(eltitulo) || _.isBoolean(eltitulo)) {

            $.ID_1709091774.update({
                titulo: eltitulo
            });
        }
    };
    $.ID_516080514.addEventListener('load', ID_721833315);
})();

if (OS_IOS || OS_ANDROID) {
    $.ID_1068546356.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}
//$.ID_1068546356.open();