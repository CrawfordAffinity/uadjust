var _bind4section = {};
var _list_templates = {};

var _activity;
if (OS_ANDROID) {
    _activity = $.ID_1643788099.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_1643788099';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_1643788099.addEventListener('open', function(e) {
        abx.setBackgroundColor("white");
    });
}
$.ID_1643788099.orientationModes = [Titanium.UI.PORTRAIT];


$.ID_1751136142.init({
    titulo: 'ENROLAMIENTO',
    __id: 'ALL1751136142',
    fondo: 'fondoblanco',
    colortitulo: 'negro',
    modal: ''
});


$.ID_1222663545.init({
    titulo: 'PARTE 4: Disponibilidad de trabajo',
    __id: 'ALL1222663545',
    avance: '4/6',
    onclick: Click_ID_22310568
});

function Click_ID_22310568(e) {

    var evento = e;

}


function Change_ID_1746789656(e) {

    e.cancelBubble = true;
    var elemento = e;
    var _columna = e.columnIndex;
    var columna = e.columnIndex + 1;
    var _fila = e.rowIndex;
    var fila = e.rowIndex + 1;
    if (Ti.App.deployType != 'production') console.log('seleccionado', {
        "fila": _fila
    });
    require('vars')['seleccionado'] = _fila;

}

function Change_ID_31855985(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    if (elemento == true || elemento == 'true') {
        /** 
         * Cambiamos el texto de la opcion y cambiamos el valor de la variable 
         */
        $.ID_194255918.setText('SI');

        require('vars')['fueraciudad'] = '1';
    } else {
        $.ID_194255918.setText('NO');

        require('vars')['fueraciudad'] = '0';
    }
    elemento = null;

}

function Change_ID_725652639(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    if (elemento == true || elemento == 'true') {
        /** 
         * Cambiamos el texto de la opcion y cambiamos el valor de la variable 
         */
        $.ID_1019357907.setText('SI');

        require('vars')['fuerapais'] = '1';
    } else {
        $.ID_1019357907.setText('NO');

        require('vars')['fuerapais'] = '0';
    }
    elemento = null;

}

$.ID_218695237.init({
    titulo: 'CONTINUAR',
    __id: 'ALL218695237',
    onclick: Click_ID_341249873
});

function Click_ID_341249873(e) {

    var evento = e;
    /** 
     * Recuperamos los valores de las variables, guardamos en la variable de registro 
     */
    var seleccionado = ('seleccionado' in require('vars')) ? require('vars')['seleccionado'] : '';
    var fueraciudad = ('fueraciudad' in require('vars')) ? require('vars')['fueraciudad'] : '';
    var fuerapais = ('fuerapais' in require('vars')) ? require('vars')['fuerapais'] : '';
    var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
    var registro = _.extend(registro, {
        disponibilidad: seleccionado,
        disp_viajar_ciudad: fueraciudad,
        disp_viajar_pais: fuerapais
    });
    require('vars')['registro'] = registro;
    if (seleccionado == 1 || seleccionado == '1') {
        /** 
         * Dependiendo de la seleccion de disponibilidad de tiempo, se puede accionar distintas funciones 
         */
        /** 
         * Si se escoge fulltime, marcamos disponibilidad completa, guardamos en registro y continuamos a pantalla contacto 
         */
        var registro = _.extend(registro, {
            disp_horas: '',
            d1: 1,
            d2: 1,
            d3: 1,
            d4: 1,
            d5: 1,
            d6: 1,
            d7: 1
        });
        require('vars')['registro'] = registro;
        Alloy.createController("contactos", {}).getView().open();
    } else {
        /** 
         * Iniciamos animacion, despues de 0.3 segundos detenemos animacion y carga pantalla parttime 
         */

        $.ID_218695237.iniciar_progreso({});
        var ID_738322479_func = function() {

            $.ID_218695237.detener_progreso({});
            Alloy.createController("part_time", {}).getView().open();
        };
        var ID_738322479 = setTimeout(ID_738322479_func, 1000 * 0.3);
    }
}

(function() {
    /** 
     * Creamos variables seteadas en 0 
     */
    require('vars')['fueraciudad'] = '0';
    require('vars')['fuerapais'] = '0';
    require('vars')['seleccionado'] = '0';
    /** 
     * Creamos evento que al cerrar desde la ultima pantalla, esta tambi&#233;n se cierre. Evitamos el uso excesivo de memoria ram 
     */

    _my_events['_close_enrolamiento,ID_1332238778'] = function(evento) {
        if (Ti.App.deployType != 'production') console.log('escuchando cerrar enrolamiento disp trabajao', {});
        $.ID_1643788099.close();
    };
    Alloy.Events.on('_close_enrolamiento', _my_events['_close_enrolamiento,ID_1332238778']);
})();

function Androidback_ID_382412859(e) {
    /** 
     * Dejamos esta accion vacia para que no pueda volver a la pantalla anterior 
     */

    e.cancelBubble = true;
    var elemento = e.source;

}
if (OS_IOS || OS_ANDROID) {
    $.ID_1643788099.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}