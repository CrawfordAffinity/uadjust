var _bind4section = {};
var _list_templates = {
    "elemento": {
        "ID_432412973": {},
        "ID_291370846": {
            "text": "{nombre}"
        },
        "ID_1925487537": {
            "text": "{id_server}"
        },
        "ID_975786586": {
            "text": "{id_segured}"
        },
        "ID_1546190443": {}
    },
    "dano": {
        "ID_1569764493": {
            "text": "{id}"
        },
        "ID_304218376": {},
        "ID_1912165224": {
            "text": "{nombre}"
        }
    },
    "pborrar": {
        "ID_1236907100": {
            "text": "{id}"
        },
        "ID_1506538078": {},
        "ID_2024360383": {},
        "ID_1765564351": {},
        "ID_1699280427": {
            "text": "{nombre}"
        },
        "ID_1445903745": {},
        "ID_1795050796": {},
        "ID_1752901998": {},
        "ID_1914367022": {
            "text": "{nombre}"
        },
        "ID_1249074865": {
            "text": "{id}"
        },
        "ID_1131557960": {},
        "ID_1361874287": {},
        "ID_1750057271": {},
        "ID_1962135207": {},
        "ID_1947166298": {
            "text": "{nombre}"
        },
        "ID_2008605730": {
            "text": "{id}"
        },
        "ID_1788775425": {},
        "ID_1246915111": {}
    },
    "contenido": {
        "ID_630800887": {},
        "ID_862352109": {
            "text": "{id}"
        },
        "ID_560452274": {
            "text": "{nombre}"
        }
    },
    "recinto": {
        "ID_1960582596": {},
        "ID_871581291": {
            "text": "{nombre}"
        },
        "ID_310297848": {
            "text": "{id}"
        }
    },
    "nivel": {
        "ID_1021786928": {
            "text": "{id}"
        },
        "ID_1241655463": {
            "text": "{nombre}"
        },
        "ID_599581692": {}
    },
    "tarea": {
        "ID_1668871679": {},
        "ID_700219020": {},
        "ID_135101015": {
            "text": "{direccion}"
        },
        "ID_1113843363": {},
        "ID_1291192599": {
            "text": "{id}"
        },
        "ID_184328649": {},
        "ID_1320539277": {},
        "ID_260293613": {
            "text": "a {distance} km"
        },
        "ID_1225246470": {},
        "ID_1933295287": {},
        "ID_1584607898": {},
        "ID_634115674": {
            "text": "{comuna}"
        },
        "ID_1129660084": {
            "text": "{nivel_2}, {pais_texto}"
        },
        "ID_986890171": {
            "text": "a {distance} km"
        },
        "ID_75175": {},
        "ID_1124829140": {},
        "ID_22021502": {},
        "ID_1565487815": {},
        "ID_1188474916": {},
        "ID_1043900892": {},
        "ID_380606864": {},
        "ID_375542781": {},
        "ID_1653124778": {},
        "ID_1021365371": {
            "text": "{nivel_2}, {pais_texto}"
        },
        "ID_839820231": {
            "text": "{comuna}"
        },
        "ID_1650788926": {},
        "ID_904588626": {},
        "ID_1247657077": {},
        "ID_1634484552": {
            "text": "{comuna}"
        },
        "ID_148052687": {
            "backgroundColor": "{bgcolor}"
        },
        "ID_1469752290": {
            "visible": "{vis_tipo9}"
        },
        "ID_1082463670": {},
        "ID_1903120505": {
            "text": "{comuna}"
        },
        "ID_23206496": {
            "text": "{direccion}"
        },
        "ID_1436234452": {
            "backgroundColor": "{bgcolor}"
        },
        "ID_1262877465": {
            "text": "{comuna}"
        },
        "ID_1680788099": {},
        "ID_1848781491": {
            "text": "{comuna}"
        },
        "ID_532471930": {},
        "ID_1336684321": {},
        "ID_466372042": {
            "visible": "{vis_tipo6}"
        },
        "ID_598257926": {
            "text": "{direccion}"
        },
        "ID_1401335837": {},
        "ID_915725625": {},
        "ID_1012297385": {
            "text": "{nivel_2}, {pais_texto}"
        },
        "ID_976141257": {},
        "ID_639687765": {
            "text": "{direccion}"
        },
        "ID_73578511": {},
        "ID_295209697": {},
        "ID_1109750739": {
            "text": "a {distance} km"
        },
        "ID_586914833": {
            "text": "a {distance} km"
        },
        "ID_1008671603": {
            "backgroundColor": "{bgcolor}"
        },
        "ID_640168083": {},
        "ID_134526248": {},
        "ID_1453555994": {
            "text": "{nivel_2}, {pais_texto}"
        },
        "ID_1654995362": {
            "text": "{direccion}"
        },
        "ID_675490908": {},
        "ID_483618405": {},
        "ID_902659266": {
            "visible": "{vis_tipo4}"
        },
        "ID_177113531": {},
        "ID_348289297": {},
        "ID_269021738": {
            "text": "a {distance} km"
        },
        "ID_1685866714": {},
        "ID_466319634": {},
        "ID_905199894": {},
        "ID_995067518": {
            "text": "{direccion}"
        },
        "ID_1654146588": {
            "text": "{nivel_2}, {pais_texto}"
        },
        "ID_732636877": {},
        "ID_653858230": {},
        "ID_1639540465": {
            "text": "{nivel_2}, {pais_texto}"
        },
        "ID_34151484": {
            "backgroundColor": "{bgcolor}"
        },
        "ID_1157133699": {},
        "ID_579741095": {},
        "ID_1749522495": {
            "backgroundColor": "{bgcolor}"
        },
        "ID_1834569554": {},
        "ID_1983604525": {},
        "ID_1148296760": {},
        "ID_1779783039": {
            "backgroundColor": "{bgcolor}"
        },
        "ID_1935971476": {
            "text": "{nivel_2}, {pais_texto}"
        },
        "ID_1225247169": {
            "backgroundColor": "{bgcolor}"
        },
        "ID_708673244": {
            "text": "{comuna}"
        },
        "ID_1121279285": {
            "text": "{comuna}"
        },
        "ID_1187902687": {
            "text": "{nivel_2}, {pais_texto}"
        },
        "ID_1363673994": {},
        "ID_1980913443": {
            "visible": "{seguir}"
        },
        "ID_1829606280": {},
        "ID_644610354": {},
        "ID_135376617": {
            "text": "{nivel_2}, {pais_texto}"
        },
        "ID_26705836": {},
        "ID_1275917191": {
            "text": "{direccion}"
        },
        "ID_261159715": {},
        "ID_149472566": {
            "text": "{comuna}"
        },
        "ID_609601012": {
            "text": "a {distance} km"
        },
        "ID_767590028": {},
        "ID_826069369": {},
        "ID_1918649305": {},
        "ID_574730052": {},
        "ID_364330445": {},
        "ID_1434863225": {
            "text": "{comuna}"
        },
        "ID_1379816612": {
            "visible": "{vis_tipo10}"
        },
        "ID_480945686": {},
        "ID_309328842": {},
        "ID_720678066": {},
        "ID_511925926": {},
        "ID_1588457992": {},
        "ID_710734301": {},
        "ID_647346064": {},
        "ID_1281228744": {},
        "ID_1991358719": {},
        "ID_1366590855": {},
        "ID_511362493": {
            "visible": "{vis_tipo8}"
        },
        "ID_1297800494": {
            "text": "{nivel_2}, {pais_texto}"
        },
        "ID_828476175": {},
        "ID_1244150292": {
            "backgroundColor": "{bgcolor}"
        },
        "ID_1292045533": {
            "backgroundColor": "{bgcolor}"
        },
        "ID_1433100065": {
            "text": "{prioridad_tiempo}"
        },
        "ID_1683440013": {
            "visible": "{vis_otro}"
        },
        "ID_1398519606": {},
        "ID_1854942384": {
            "text": "{comuna}"
        },
        "ID_1267642540": {},
        "ID_870734514": {},
        "ID_1343703596": {},
        "ID_1502072898": {
            "visible": "{vis_tipo5}"
        },
        "ID_1688585922": {
            "text": "a {distance} km"
        },
        "ID_987598921": {},
        "ID_1498624444": {
            "backgroundColor": "{bgcolor}"
        },
        "ID_1939249430": {
            "text": "{direccion}"
        },
        "ID_1094583180": {},
        "ID_101815200": {},
        "ID_1417402594": {
            "visible": "{vis_tipo2}"
        },
        "ID_13800216": {
            "text": "{nivel_2}, {pais_texto}"
        },
        "ID_1556692359": {},
        "ID_682930065": {
            "text": "a {distance} km"
        },
        "ID_1586191918": {},
        "ID_733552058": {},
        "ID_231988172": {
            "text": "a {distance} km"
        },
        "ID_1565528410": {
            "visible": "{vis_tipo7}"
        },
        "ID_412416512": {},
        "ID_1787687658": {
            "text": "{direccion}"
        },
        "ID_1526803322": {},
        "ID_1655100522": {
            "backgroundColor": "{bgcolor}"
        },
        "ID_1376011318": {
            "text": "a {distance} km"
        },
        "ID_878804940": {
            "visible": "{vis_tipo1}"
        },
        "ID_789021647": {
            "text": "a {distance} km"
        },
        "ID_1412225440": {},
        "ID_794184744": {
            "text": "{direccion}"
        },
        "ID_1149338911": {
            "text": "{direccion}"
        },
        "ID_1642833684": {
            "visible": "{vis_tipo3}"
        }
    },
    "tareas_mistareas": {
        "ID_1575074457": {},
        "ID_1503294346": {
            "text": "{comuna}"
        },
        "ID_1152838109": {
            "idlocal": "{idlocal}"
        },
        "ID_1205965249": {
            "text": "a {distancia} km"
        },
        "ID_1729551944": {},
        "ID_1201332772": {},
        "ID_2063494569": {},
        "ID_1272820672": {
            "text": "{ciudad}, {pais}"
        },
        "ID_1138222879": {},
        "ID_1048592298": {},
        "ID_1695073981": {},
        "ID_2134521582": {},
        "ID_1071163621": {},
        "ID_1299156409": {
            "text": "{direccion}"
        },
        "ID_1676413215": {
            "visible": "{seguirvisible}"
        },
        "ID_1629587369": {}
    },
    "tareas": {
        "ID_1271969677": {},
        "ID_628062793": {
            "text": "{direcciontarea}"
        },
        "ID_568454310": {},
        "ID_926529622": {},
        "ID_1388054861": {
            "text": "{ciudadtarea}, {paistarea} "
        },
        "ID_369983217": {},
        "ID_75895559": {},
        "ID_360694343": {
            "text": "{comunatarea}"
        },
        "ID_1217465633": {
            "text": "a {ubicaciontarea} km"
        },
        "ID_477443044": {},
        "ID_1228388310": {},
        "ID_871033850": {},
        "ID_422941248": {
            "myid": "{myid}"
        }
    },
    "criticas": {
        "ID_202820149": {},
        "ID_1737707475": {
            "text": "{ciudadcritica}, {paiscritica}"
        },
        "ID_422128048": {},
        "ID_403273726": {},
        "ID_620140124": {
            "text": "{direccioncritica}"
        },
        "ID_1438432281": {},
        "ID_1119172715": {},
        "ID_1977608142": {
            "text": "{comunacritica}"
        },
        "ID_1622346619": {},
        "ID_1445023664": {},
        "ID_521530515": {
            "text": "a {ubicacioncritica} km"
        },
        "ID_704055711": {
            "myid": "{myid}"
        },
        "ID_715095938": {}
    },
    "tarea_historia": {
        "ID_1680574104": {
            "text": "{comuna}"
        },
        "ID_1304747472": {},
        "ID_676600495": {},
        "ID_1147033032": {},
        "ID_1810186930": {
            "text": "{hora_termino}"
        },
        "ID_911340427": {},
        "ID_1680180472": {
            "visible": "{bt_enviartarea}"
        },
        "ID_452289613": {},
        "ID_930474704": {
            "text": "{direccion}"
        },
        "ID_428063592": {
            "idlocal": "{id}",
            "estado": "{estado_tarea}"
        },
        "ID_1404040740": {},
        "ID_1184366498": {},
        "ID_459591478": {
            "text": "{ciudad}, {pais}"
        },
        "ID_362564056": {
            "visible": "{enviando_tarea}"
        },
        "ID_165401298": {},
        "ID_1987088054": {},
        "ID_500305974": {},
        "ID_124513836": {},
        "ID_404048727": {}
    }
};

var _activity;
if (OS_ANDROID) {
    _activity = $.ID_365517799.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_365517799';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_365517799.addEventListener('open', function(e) {});
}
$.ID_365517799.orientationModes = [Titanium.UI.PORTRAIT];

function Load_ID_1608714602(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var evento = e;
    elemento.start();

}

function Click_ID_1284362860(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    /** 
     * Creamos una estructura para usarla en el enrolamiento 
     */
    var vacio = {};
    /** 
     * Guardamos la variable registro para que sea de acceso permanente, (que no se pierda despues de una vez cerrado) 
     */
    Ti.App.Properties.setString('registro', JSON.stringify(vacio));
    var mi_pais = ('mi_pais' in require('vars')) ? require('vars')['mi_pais'] : '';
    var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
    var ID_1674696970 = null;
    if ('ocultar_botones' in require('funciones')) {
        ID_1674696970 = require('funciones').ocultar_botones({});
    } else {
        try {
            ID_1674696970 = f_ocultar_botones({});
        } catch (ee) {}
    }
    var obteniendo_info = 'status: obteniendo info';
    $.ID_908004102.setText(obteniendo_info);

    var ID_362822929 = {};

    ID_362822929.success = function(e) {
        var elemento = e,
            valor = e;
        if (elemento == false || elemento == 'false') {
            var esperando = 'status: esperando';
            $.ID_908004102.setText(esperando);

            var ID_244136950_opts = ['Aceptar'];
            var ID_244136950 = Ti.UI.createAlertDialog({
                title: 'Alerta',
                message: 'Problema de conexión por favor intentar después',
                buttonNames: ID_244136950_opts
            });
            ID_244136950.addEventListener('click', function(e) {
                var suu = ID_244136950_opts[e.index];
                suu = null;

                e.source.removeEventListener("click", arguments.callee);
            });
            ID_244136950.show();
        } else {
            /** 
             * En el caso de obtener un resultado desde el servidor, limpiamos y cargamos las tablas 
             */
            var carg_datos = 'status: cargando datos';
            $.ID_908004102.setText(carg_datos);

            var ID_821317784_i = Alloy.Collections.pais;
            var sql = "DELETE FROM " + ID_821317784_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_821317784_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_821317784_i.trigger('delete');
            var ID_1557444604_i = Alloy.Collections.nivel1;
            var sql = "DELETE FROM " + ID_1557444604_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_1557444604_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_1557444604_i.trigger('delete');
            var ID_325623147_i = Alloy.Collections.experiencia_oficio;
            var sql = "DELETE FROM " + ID_325623147_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_325623147_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_325623147_i.trigger('delete');
            var elemento_regiones = elemento.regiones;
            var ID_1376954920_m = Alloy.Collections.nivel1;
            var db_ID_1376954920 = Ti.Database.open(ID_1376954920_m.config.adapter.db_name);
            db_ID_1376954920.execute('BEGIN');
            _.each(elemento_regiones, function(ID_1376954920_fila, pos) {
                db_ID_1376954920.execute('INSERT INTO nivel1 (id_label, id_server, dif_horaria, id_pais) VALUES (?,?,?,?)', ID_1376954920_fila.subdivision_name, ID_1376954920_fila.id, 2, ID_1376954920_fila.id_pais);
            });
            db_ID_1376954920.execute('COMMIT');
            db_ID_1376954920.close();
            db_ID_1376954920 = null;
            ID_1376954920_m.trigger('change');
            var elemento_paises = elemento.paises;
            var ID_518549359_m = Alloy.Collections.pais;
            var db_ID_518549359 = Ti.Database.open(ID_518549359_m.config.adapter.db_name);
            db_ID_518549359.execute('BEGIN');
            _.each(elemento_paises, function(ID_518549359_fila, pos) {
                db_ID_518549359.execute('INSERT INTO pais (label_nivel2, moneda, nombre, label_nivel4, label_codigo_identificador, label_nivel3, id_server, label_nivel1, iso, sis_metrico, niveles_pais, id_pais, label_nivel5, lenguaje) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)', ID_518549359_fila.nivel_2, ID_518549359_fila.moneda, ID_518549359_fila.nombre, ID_518549359_fila.nivel_4, ID_518549359_fila.label_codigo_identificador, ID_518549359_fila.nivel_3, ID_518549359_fila.id, ID_518549359_fila.nivel_1, ID_518549359_fila.iso, ID_518549359_fila.sis_metrico, ID_518549359_fila.niveles_pais, ID_518549359_fila.idpais, ID_518549359_fila.nivel_5, ID_518549359_fila.lenguaje);
            });
            db_ID_518549359.execute('COMMIT');
            db_ID_518549359.close();
            db_ID_518549359 = null;
            ID_518549359_m.trigger('change');
            var elemento_experiencia_oficio = elemento.experiencia_oficio;
            var ID_1594927226_m = Alloy.Collections.experiencia_oficio;
            var db_ID_1594927226 = Ti.Database.open(ID_1594927226_m.config.adapter.db_name);
            db_ID_1594927226.execute('BEGIN');
            _.each(elemento_experiencia_oficio, function(ID_1594927226_fila, pos) {
                db_ID_1594927226.execute('INSERT INTO experiencia_oficio (nombre, id_server, id_pais) VALUES (?,?,?)', ID_1594927226_fila.nombre, ID_1594927226_fila.id, ID_1594927226_fila.idpais);
            });
            db_ID_1594927226.execute('COMMIT');
            db_ID_1594927226.close();
            db_ID_1594927226 = null;
            ID_1594927226_m.trigger('change');
            var esperando = 'status: esperando';
            $.ID_908004102.setText(esperando);

            /** 
             * Y cargamos la primera pantalla del enrolamiento 
             */
            Alloy.createController("registro_index", {}).getView().open();
        }
        var ID_1865449898 = null;
        if ('mostrar_botones' in require('funciones')) {
            ID_1865449898 = require('funciones').mostrar_botones({});
        } else {
            try {
                ID_1865449898 = f_mostrar_botones({});
            } catch (ee) {}
        }
        elemento = null, valor = null;
    };

    ID_362822929.error = function(e) {
        var elemento = e,
            valor = e;
        var ID_6513015_opts = ['Aceptar'];
        var ID_6513015 = Ti.UI.createAlertDialog({
            title: 'Alerta',
            message: 'Problema de conexión por favor intentar después',
            buttonNames: ID_6513015_opts
        });
        ID_6513015.addEventListener('click', function(e) {
            var suu = ID_6513015_opts[e.index];
            suu = null;

            e.source.removeEventListener("click", arguments.callee);
        });
        ID_6513015.show();
        var ID_758659505 = null;
        if ('mostrar_botones' in require('funciones')) {
            ID_758659505 = require('funciones').mostrar_botones({});
        } else {
            try {
                ID_758659505 = f_mostrar_botones({});
            } catch (ee) {}
        }
        elemento = null, valor = null;
    };
    require('helper').ajaxUnico('ID_362822929', '' + url_server + 'obtenerPais' + '', 'POST', {}, 15000, ID_362822929);

}

$.ID_1755668974.init({
    titulo: 'ENTRAR',
    __id: 'ALL1755668974',
    color: 'amarillo',
    onclick: Click_ID_709664060
});

function Click_ID_709664060(e) {

    var evento = e;

    $.ID_1755668974.iniciar_progreso({});
    if (Ti.App.deployType != 'production') console.log('LLamando al servidor por login', {});
    var _ifunc_res_ID_1125012314 = function(e) {
        if (e.error) {
            var gpsportada = {
                error: true,
                latitude: -1,
                longitude: -1,
                reason: (e.reason) ? e.reason : 'unknown',
                accuracy: 0,
                speed: 0,
                error_compass: false
            };
        } else {
            var gpsportada = e;
        }
        require('vars')['gps_latitud'] = gpsportada.latitude;
        require('vars')['gps_longitude'] = gpsportada.longitude;
        /** 
         * Obtenemos la ubicaci&#243;n para el inicio de sesion y guardamos variables con los datos 
         */
        var _ifunc_res_ID_204477733 = function(e) {
            if (e.error) {
                var geopos = {
                    error: true,
                    latitude: -1,
                    longitude: -1,
                    reason: (e.reason) ? e.reason : 'unknown',
                    accuracy: 0,
                    speed: 0,
                    error_compass: false
                };
            } else {
                var geopos = e;
            }
            if (geopos.error == false || geopos.error == 'false') {
                require('vars')['gps_error'] = 'false';
                require('vars')['gps_latitud'] = geopos.latitude;
                require('vars')['gps_longitud'] = geopos.longitude;
            }
        };
        Ti.Geolocation.setAccuracy(Ti.Geolocation.ACCURACY_HIGH);
        if (Ti.Geolocation.hasLocationPermissions(Ti.Geolocation.AUTHORIZATION_WHEN_IN_USE)) {
            Ti.Geolocation.getCurrentPosition(function(ee) {
                if (Ti.Geolocation.getHasCompass() == false) {
                    var pgeopos = ('coords' in ee) ? ee.coords : {};
                    pgeopos.error = ('coords' in ee) ? false : true;
                    pgeopos.reason = '';
                    pgeopos.speed = 0;
                    pgeopos.compass = -1;
                    pgeopos.compass_accuracy = -1;
                    pgeopos.error_compass = true;
                    _ifunc_res_ID_204477733(pgeopos);
                } else {
                    Ti.Geolocation.getCurrentHeading(function(yy) {
                        var pgeopos = ('coords' in ee) ? ee.coords : {};
                        pgeopos.error = ('coords' in ee) ? false : true;
                        pgeopos.reason = ('error' in ee) ? ee.error : '';
                        if (yy.error) {
                            pgeopos.error_compass = true;
                        } else {
                            pgeopos.compass = yy.heading;
                            pgeopos.compass_accuracy = yy.heading.accuracy;
                        }
                        _ifunc_res_ID_204477733(pgeopos);
                    });
                }
            });
        } else {
            Ti.Geolocation.requestLocationPermissions(Ti.Geolocation.AUTHORIZATION_WHEN_IN_USE, function(u) {
                if (u.success) {
                    Ti.Geolocation.getCurrentPosition(function(ee) {
                        if (Ti.Geolocation.getHasCompass() == false) {
                            var pgeopos = ('coords' in ee) ? ee.coords : {};
                            pgeopos.error = ('coords' in ee) ? false : true;
                            pgeopos.reason = '';
                            pgeopos.speed = 0;
                            pgeopos.compass = -1;
                            pgeopos.compass_accuracy = -1;
                            pgeopos.error_compass = true;
                            _ifunc_res_ID_204477733(pgeopos);
                        } else {
                            Ti.Geolocation.getCurrentHeading(function(yy) {
                                var pgeopos = ('coords' in ee) ? ee.coords : {};
                                pgeopos.error = ('coords' in ee) ? false : true;
                                pgeopos.reason = '';
                                if (yy.error) {
                                    pgeopos.error_compass = true;
                                } else {
                                    pgeopos.compass = yy.heading;
                                    pgeopos.compass_accuracy = yy.heading.accuracy;
                                }
                                _ifunc_res_ID_204477733(pgeopos);
                            });
                        }
                    });
                } else {
                    _ifunc_res_ID_204477733({
                        error: true,
                        latitude: -1,
                        longitude: -1,
                        reason: 'permission_denied',
                        accuracy: 0,
                        speed: 0,
                        error_compass: false
                    });
                }
            });
        }
    };
    Ti.Geolocation.setAccuracy(Ti.Geolocation.ACCURACY_HIGH);
    if (Ti.Geolocation.hasLocationPermissions(Ti.Geolocation.AUTHORIZATION_WHEN_IN_USE)) {
        Ti.Geolocation.getCurrentPosition(function(ee) {
            if (Ti.Geolocation.getHasCompass() == false) {
                var pgpsportada = ('coords' in ee) ? ee.coords : {};
                pgpsportada.error = ('coords' in ee) ? false : true;
                pgpsportada.reason = '';
                pgpsportada.speed = 0;
                pgpsportada.compass = -1;
                pgpsportada.compass_accuracy = -1;
                pgpsportada.error_compass = true;
                _ifunc_res_ID_1125012314(pgpsportada);
            } else {
                Ti.Geolocation.getCurrentHeading(function(yy) {
                    var pgpsportada = ('coords' in ee) ? ee.coords : {};
                    pgpsportada.error = ('coords' in ee) ? false : true;
                    pgpsportada.reason = ('error' in ee) ? ee.error : '';
                    if (yy.error) {
                        pgpsportada.error_compass = true;
                    } else {
                        pgpsportada.compass = yy.heading;
                        pgpsportada.compass_accuracy = yy.heading.accuracy;
                    }
                    _ifunc_res_ID_1125012314(pgpsportada);
                });
            }
        });
    } else {
        Ti.Geolocation.requestLocationPermissions(Ti.Geolocation.AUTHORIZATION_WHEN_IN_USE, function(u) {
            if (u.success) {
                Ti.Geolocation.getCurrentPosition(function(ee) {
                    if (Ti.Geolocation.getHasCompass() == false) {
                        var pgpsportada = ('coords' in ee) ? ee.coords : {};
                        pgpsportada.error = ('coords' in ee) ? false : true;
                        pgpsportada.reason = '';
                        pgpsportada.speed = 0;
                        pgpsportada.compass = -1;
                        pgpsportada.compass_accuracy = -1;
                        pgpsportada.error_compass = true;
                        _ifunc_res_ID_1125012314(pgpsportada);
                    } else {
                        Ti.Geolocation.getCurrentHeading(function(yy) {
                            var pgpsportada = ('coords' in ee) ? ee.coords : {};
                            pgpsportada.error = ('coords' in ee) ? false : true;
                            pgpsportada.reason = '';
                            if (yy.error) {
                                pgpsportada.error_compass = true;
                            } else {
                                pgpsportada.compass = yy.heading;
                                pgpsportada.compass_accuracy = yy.heading.accuracy;
                            }
                            _ifunc_res_ID_1125012314(pgpsportada);
                        });
                    }
                });
            } else {
                _ifunc_res_ID_1125012314({
                    error: true,
                    latitude: -1,
                    longitude: -1,
                    reason: 'permission_denied',
                    accuracy: 0,
                    speed: 0,
                    error_compass: false
                });
            }
        });
    }
    var correo;
    correo = $.ID_309715597.getValue();

    var password;
    password = $.ID_730883847.getValue();

    if ((_.isObject(correo) || _.isString(correo)) && _.isEmpty(correo)) {
        var ID_921225015_opts = ['Aceptar'];
        var ID_921225015 = Ti.UI.createAlertDialog({
            title: 'Alerta',
            message: 'Debe ingresar su nombre de usuario',
            buttonNames: ID_921225015_opts
        });
        ID_921225015.addEventListener('click', function(e) {
            var x = ID_921225015_opts[e.index];
            x = null;

            e.source.removeEventListener("click", arguments.callee);
        });
        ID_921225015.show();

        $.ID_1755668974.detener_progreso({});
    } else if ((_.isObject(password) || _.isString(password)) && _.isEmpty(password)) {
        var ID_135104406_opts = ['Aceptar'];
        var ID_135104406 = Ti.UI.createAlertDialog({
            title: 'Alerta',
            message: 'Debe ingresar su clave',
            buttonNames: ID_135104406_opts
        });
        ID_135104406.addEventListener('click', function(e) {
            var x = ID_135104406_opts[e.index];
            x = null;

            e.source.removeEventListener("click", arguments.callee);
        });
        ID_135104406.show();

        $.ID_1755668974.detener_progreso({});
    } else {
        /** 
         * Verificamos que los campos de email y password no esten vacios, en caso que haya datos, proseguimos con la carga de paises 
         */
        var ID_1389656406 = null;
        if ('ocultar_botones' in require('funciones')) {
            ID_1389656406 = require('funciones').ocultar_botones({});
        } else {
            try {
                ID_1389656406 = f_ocultar_botones({});
            } catch (ee) {}
        }
        require('vars')['correo'] = correo;
        require('vars')['password'] = password;
        $.ID_309715597.blur();
        $.ID_730883847.blur();
        var ID_1351900061 = null;
        if ('cargar_paises' in require('funciones')) {
            ID_1351900061 = require('funciones').cargar_paises({});
        } else {
            try {
                ID_1351900061 = f_cargar_paises({});
            } catch (ee) {}
        }
    }
}

/** 
 * Modificamos el ancho de la vista del registro para que se ajuste a su contenido y no tome la totalidad 
 */
var ID_1632570648_ancho = '-';

if (ID_1632570648_ancho == '*') {
    ID_1632570648_ancho = Ti.UI.FILL;
} else if (ID_1632570648_ancho == '-') {
    ID_1632570648_ancho = Ti.UI.SIZE;
} else if (!isNaN(ID_1632570648_ancho)) {
    ID_1632570648_ancho = ID_1632570648_ancho + 'dp';
}
$.ID_1632570648.setWidth(ID_1632570648_ancho);

/** 
 * Definimos en una variable el lenguaje que tiene el telefono 
 */
pais_codigo = Titanium.Locale.currentCountry;
/** 
 * Guardamos en una variable global el codigo de pais que tiene el telefono 
 */
require('vars')['mi_pais'] = pais_codigo;
/** 
 * Recuperamos la url del servidor de uadjust 
 */
var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
/** 
 * Partimos sin inspeccion desde login 
 */
require('vars')['inspeccion_encurso'] = 'false';
var ID_70540566_func = function() {
    /** 
     * Obtenemos una primera ubicaci&#243;n para el login 
     */
    var _ifunc_res_ID_713480223 = function(e) {
        if (e.error) {
            var geopos = {
                error: true,
                latitude: -1,
                longitude: -1,
                reason: (e.reason) ? e.reason : 'unknown',
                accuracy: 0,
                speed: 0,
                error_compass: false
            };
        } else {
            var geopos = e;
        }
        if (geopos.error == false || geopos.error == 'false') {
            /** 
             * Revisamos que no haya error al obtener la ubicacion 
             */
            /** 
             * Guardamos el estado de obtener el gps. Si no hubo error, guardamos la variable con false 
             */
            require('vars')['gps_error'] = 'false';
            /** 
             * Almacenamos la posicion y longitud en variables distintas 
             */
            require('vars')['gps_latitud'] = geopos.latitude;
            require('vars')['gps_longitud'] = geopos.longitude;
            if (Ti.App.deployType != 'production') console.log('psb localizacion de usuario actualizada con exito (script inicial)', {
                "evento": geopos
            });
        }
        /** 
         * Obtenemos el permiso para usar la camara 
         */

        if (Ti.Media.hasCameraPermissions()) {} else {
            Ti.Media.requestCameraPermissions(function(ercp) {
                if (!ercp.success) {


                    /** 
                     * Mostramos mensaje si el usuario no acepto el permiso de camara 
                     */
                    var ID_1519305886_opts = ['Aceptar'];
                    var ID_1519305886 = Ti.UI.createAlertDialog({
                        title: 'Atención',
                        message: 'No se podran realizar inspecciones sin acceso a la camara.',
                        buttonNames: ID_1519305886_opts
                    });
                    ID_1519305886.addEventListener('click', function(e) {
                        var nulo = ID_1519305886_opts[e.index];
                        nulo = null;

                    });
                    ID_1519305886.show();

                }
            });
        }
    };
    Ti.Geolocation.setAccuracy(Ti.Geolocation.ACCURACY_HIGH);
    if (Ti.Geolocation.hasLocationPermissions(Ti.Geolocation.AUTHORIZATION_WHEN_IN_USE)) {
        Ti.Geolocation.getCurrentPosition(function(ee) {
            if (Ti.Geolocation.getHasCompass() == false) {
                var pgeopos = ('coords' in ee) ? ee.coords : {};
                pgeopos.error = ('coords' in ee) ? false : true;
                pgeopos.reason = '';
                pgeopos.speed = 0;
                pgeopos.compass = -1;
                pgeopos.compass_accuracy = -1;
                pgeopos.error_compass = true;
                _ifunc_res_ID_713480223(pgeopos);
            } else {
                Ti.Geolocation.getCurrentHeading(function(yy) {
                    var pgeopos = ('coords' in ee) ? ee.coords : {};
                    pgeopos.error = ('coords' in ee) ? false : true;
                    pgeopos.reason = ('error' in ee) ? ee.error : '';
                    if (yy.error) {
                        pgeopos.error_compass = true;
                    } else {
                        pgeopos.compass = yy.heading;
                        pgeopos.compass_accuracy = yy.heading.accuracy;
                    }
                    _ifunc_res_ID_713480223(pgeopos);
                });
            }
        });
    } else {
        Ti.Geolocation.requestLocationPermissions(Ti.Geolocation.AUTHORIZATION_WHEN_IN_USE, function(u) {
            if (u.success) {
                Ti.Geolocation.getCurrentPosition(function(ee) {
                    if (Ti.Geolocation.getHasCompass() == false) {
                        var pgeopos = ('coords' in ee) ? ee.coords : {};
                        pgeopos.error = ('coords' in ee) ? false : true;
                        pgeopos.reason = '';
                        pgeopos.speed = 0;
                        pgeopos.compass = -1;
                        pgeopos.compass_accuracy = -1;
                        pgeopos.error_compass = true;
                        _ifunc_res_ID_713480223(pgeopos);
                    } else {
                        Ti.Geolocation.getCurrentHeading(function(yy) {
                            var pgeopos = ('coords' in ee) ? ee.coords : {};
                            pgeopos.error = ('coords' in ee) ? false : true;
                            pgeopos.reason = '';
                            if (yy.error) {
                                pgeopos.error_compass = true;
                            } else {
                                pgeopos.compass = yy.heading;
                                pgeopos.compass_accuracy = yy.heading.accuracy;
                            }
                            _ifunc_res_ID_713480223(pgeopos);
                        });
                    }
                });
            } else {
                _ifunc_res_ID_713480223({
                    error: true,
                    latitude: -1,
                    longitude: -1,
                    reason: 'permission_denied',
                    accuracy: 0,
                    speed: 0,
                    error_compass: false
                });
            }
        });
    }
};
var ID_70540566 = setTimeout(ID_70540566_func, 1000 * 0.1);
var f_cargar_paises = function(x_params) {
    var item = x_params['item'];
    var carg_paises = 'status: cargando paises';
    $.ID_908004102.setText(carg_paises);

    /** 
     * Hacemos una consulta al servidor para obtener los paises 
     */
    var ID_1506536448 = {};

    ID_1506536448.success = function(e) {
        var elemento = e,
            valor = e;
        if (_.isObject(elemento)) {
            if (elemento.error == 0 || elemento.error == '0') {
                /** 
                 * En el caso que el servidor retorne un resultado 0 es porque esta todo bien, caso contrario, se mostrara mensaje de error y vuelve a mostrar los errores 
                 */
                if (Ti.App.deployType != 'production') console.log('registrando paises', {});
                /** 
                 * Si no hubo problema al obtener los paises, limpiamos los modelos y cargamos los datos 
                 */
                /** 
                 * Si no hubo problema al obtener los paises, limpiamos los modelos y cargamos los datos 
                 */

                var ID_1512376271_i = Alloy.Collections.pais;
                var sql = "DELETE FROM " + ID_1512376271_i.config.adapter.collection_name;
                var db = Ti.Database.open(ID_1512376271_i.config.adapter.db_name);
                db.execute(sql);
                db.close();
                sql = null;
                db = null;
                ID_1512376271_i.trigger('delete');
                var ID_1612987487_i = Alloy.Collections.nivel1;
                var sql = "DELETE FROM " + ID_1612987487_i.config.adapter.collection_name;
                var db = Ti.Database.open(ID_1612987487_i.config.adapter.db_name);
                db.execute(sql);
                db.close();
                sql = null;
                db = null;
                ID_1612987487_i.trigger('delete');
                var ID_794595785_i = Alloy.Collections.experiencia_oficio;
                var sql = "DELETE FROM " + ID_794595785_i.config.adapter.collection_name;
                var db = Ti.Database.open(ID_794595785_i.config.adapter.db_name);
                db.execute(sql);
                db.close();
                sql = null;
                db = null;
                ID_794595785_i.trigger('delete');
                var elemento_regiones = elemento.regiones;
                var ID_1643530140_m = Alloy.Collections.nivel1;
                var db_ID_1643530140 = Ti.Database.open(ID_1643530140_m.config.adapter.db_name);
                db_ID_1643530140.execute('BEGIN');
                _.each(elemento_regiones, function(ID_1643530140_fila, pos) {
                    db_ID_1643530140.execute('INSERT INTO nivel1 (id_label, id_server, dif_horaria, id_pais) VALUES (?,?,?,?)', ID_1643530140_fila.subdivision_name, ID_1643530140_fila.id, 2, ID_1643530140_fila.id_pais);
                });
                db_ID_1643530140.execute('COMMIT');
                db_ID_1643530140.close();
                db_ID_1643530140 = null;
                ID_1643530140_m.trigger('change');
                var elemento_paises = elemento.paises;
                var ID_1034882584_m = Alloy.Collections.pais;
                var db_ID_1034882584 = Ti.Database.open(ID_1034882584_m.config.adapter.db_name);
                db_ID_1034882584.execute('BEGIN');
                _.each(elemento_paises, function(ID_1034882584_fila, pos) {
                    db_ID_1034882584.execute('INSERT INTO pais (label_nivel2, nombre, label_nivel4, label_codigo_identificador, label_nivel3, id_server, label_nivel1, iso, sis_metrico, id_pais, label_nivel5, lenguaje) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)', ID_1034882584_fila.nivel_2, ID_1034882584_fila.nombre, ID_1034882584_fila.nivel_4, ID_1034882584_fila.label_codigo_identificador, ID_1034882584_fila.nivel_3, ID_1034882584_fila.id, ID_1034882584_fila.nivel_1, ID_1034882584_fila.iso, ID_1034882584_fila.sis_metrico, ID_1034882584_fila.idpais, ID_1034882584_fila.nivel_5, ID_1034882584_fila.lenguaje);
                });
                db_ID_1034882584.execute('COMMIT');
                db_ID_1034882584.close();
                db_ID_1034882584 = null;
                ID_1034882584_m.trigger('change');
                var elemento_experiencia_oficio = elemento.experiencia_oficio;
                var ID_636641228_m = Alloy.Collections.experiencia_oficio;
                var db_ID_636641228 = Ti.Database.open(ID_636641228_m.config.adapter.db_name);
                db_ID_636641228.execute('BEGIN');
                _.each(elemento_experiencia_oficio, function(ID_636641228_fila, pos) {
                    db_ID_636641228.execute('INSERT INTO experiencia_oficio (nombre, id_server, id_pais) VALUES (?,?,?)', ID_636641228_fila.nombre, ID_636641228_fila.id, ID_636641228_fila.idpais);
                });
                db_ID_636641228.execute('COMMIT');
                db_ID_636641228.close();
                db_ID_636641228 = null;
                ID_636641228_m.trigger('change');
                /** 
                 * Al terminar la carga de datos de la base de datos, llamamos una funcion para continuar con el proceso 
                 */
                var ID_203502002 = null;
                if ('llamar_login' in require('funciones')) {
                    ID_203502002 = require('funciones').llamar_login({});
                } else {
                    try {
                        ID_203502002 = f_llamar_login({});
                    } catch (ee) {}
                }
            } else {
                var ID_452613796 = null;
                if ('mostrar_botones' in require('funciones')) {
                    ID_452613796 = require('funciones').mostrar_botones({});
                } else {
                    try {
                        ID_452613796 = f_mostrar_botones({});
                    } catch (ee) {}
                }
                var ID_606943387_opts = ['Aceptar'];
                var ID_606943387 = Ti.UI.createAlertDialog({
                    title: 'Atención',
                    message: '' + 'Hubo un problema con el servidor (' + elemento.error + ')' + '',
                    buttonNames: ID_606943387_opts
                });
                ID_606943387.addEventListener('click', function(e) {
                    var suu = ID_606943387_opts[e.index];
                    suu = null;

                    e.source.removeEventListener("click", arguments.callee);
                });
                ID_606943387.show();
                var esperando = 'status: esperando';
                $.ID_908004102.setText(esperando);

            }
        } else {
            var ID_790527593 = null;
            if ('mostrar_botones' in require('funciones')) {
                ID_790527593 = require('funciones').mostrar_botones({});
            } else {
                try {
                    ID_790527593 = f_mostrar_botones({});
                } catch (ee) {}
            }
            var ID_475986631_opts = ['Aceptar'];
            var ID_475986631 = Ti.UI.createAlertDialog({
                title: 'Atención',
                message: 'Hubo un problema con el servidor',
                buttonNames: ID_475986631_opts
            });
            ID_475986631.addEventListener('click', function(e) {
                var suu = ID_475986631_opts[e.index];
                suu = null;

                e.source.removeEventListener("click", arguments.callee);
            });
            ID_475986631.show();
            var esperando = 'status: esperando';
            $.ID_908004102.setText(esperando);

        }
        elemento = null, valor = null;
    };

    ID_1506536448.error = function(e) {
        var elemento = e,
            valor = e;
        var ID_930600987 = null;
        if ('mostrar_botones' in require('funciones')) {
            ID_930600987 = require('funciones').mostrar_botones({});
        } else {
            try {
                ID_930600987 = f_mostrar_botones({});
            } catch (ee) {}
        }
        var ID_641150474_opts = ['Aceptar'];
        var ID_641150474 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Problema de conexion por favor intentar despues',
            buttonNames: ID_641150474_opts
        });
        ID_641150474.addEventListener('click', function(e) {
            var suu = ID_641150474_opts[e.index];
            suu = null;

            e.source.removeEventListener("click", arguments.callee);
        });
        ID_641150474.show();
        var esperando = 'status: esperando';
        $.ID_908004102.setText(esperando);

        elemento = null, valor = null;
    };
    require('helper').ajaxUnico('ID_1506536448', '' + url_server + 'obtenerPais' + '', 'POST', {}, 15000, ID_1506536448);
    return null;
};
var f_llamar_login = function(x_params) {
    var item = x_params['item'];
    if (Ti.App.deployType != 'production') console.log('capturando ubicacion', {});
    var verificando_credenciales = 'status: verificando credenciales';
    $.ID_908004102.setText(verificando_credenciales);

    /** 
     * Recuperamos la variable gps_error 
     */
    var gps_error = ('gps_error' in require('vars')) ? require('vars')['gps_error'] : '';
    if (gps_error == true || gps_error == 'true') {
        /** 
         * En el caso que gps_error sea verdadero, mostramos mensaje de problema. Caso contrario, seguimos con la validacion de usuario 
         */
        var ID_1222397500_opts = ['Aceptar'];
        var ID_1222397500 = Ti.UI.createAlertDialog({
            title: 'Alerta',
            message: 'No se pudo obtener la ubicación',
            buttonNames: ID_1222397500_opts
        });
        ID_1222397500.addEventListener('click', function(e) {
            var errori = ID_1222397500_opts[e.index];
            errori = null;

        });
        ID_1222397500.show();
        var ID_1353705667 = null;
        if ('mostrar_botones' in require('funciones')) {
            ID_1353705667 = require('funciones').mostrar_botones({});
        } else {
            try {
                ID_1353705667 = f_mostrar_botones({});
            } catch (ee) {}
        }
    } else {
        var correo = ('correo' in require('vars')) ? require('vars')['correo'] : '';
        var password = ('password' in require('vars')) ? require('vars')['password'] : '';
        var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
        /** 
         * La variable devicetoken es el token obtenido cuando se solicita obtener el push 
         */
        var devicetoken = ('devicetoken' in require('vars')) ? require('vars')['devicetoken'] : '';
        if ((_.isObject(devicetoken) || _.isString(devicetoken)) && _.isEmpty(devicetoken)) {
            /** 
             * Se aplica cuando se compila en iOS, ya que los push no funcionan en simuladores 
             */
            require('vars')['devicetoken'] = 'emulador-iphone6-creador';
            Ti.App.Properties.setString('devicetoken', JSON.stringify('emulador-iphone6-creador'));
            var devicetoken = ('devicetoken' in require('vars')) ? require('vars')['devicetoken'] : '';
        }
        if (Ti.App.deployType != 'production') console.log('llamando servicio login', {});
        /** 
         * Obtenemos el id unico de cada equipo 
         */
        var uidd = Ti.Platform.id;
        var gps_latitud = ('gps_latitud' in require('vars')) ? require('vars')['gps_latitud'] : '';
        var gps_longitud = ('gps_longitud' in require('vars')) ? require('vars')['gps_longitud'] : '';
        /** 
         * Obtenemos la zona horaria 
         */
        var moment = require('alloy/moment');
        var reloj_celular = moment(new Date()).format('ZZ');
        /** 
         * Obtenemos la zona horaria del equipo 
         */
        var dif = +reloj_celular * 0.01;
        if (Ti.App.deployType != 'production') console.log('psb dato para sincronizacion horaria (solo gmt)', {
            "fecha": dif
        });
        var ID_822595707 = {};
        console.log('DEBUG WEB: requesting url:' + url_server + 'login' + ' with data:', {
            _method: 'POST',
            _params: {
                correo: correo,
                password: password,
                device: Ti.Platform.name,
                lat: gps_latitud,
                lon: gps_longitud,
                device_token: devicetoken,
                uidd: uidd,
                fecha: dif
            },
            _timeout: '15000'
        });

        ID_822595707.success = function(e) {
            var elemento = e,
                valor = e;
            if (Ti.App.deployType != 'production') console.log('respuesta de servidor: login', {
                "elemento": elemento
            });
            if (elemento.error == 401) {
                /** 
                 * Manejamos los distintos tipos de mensaje de error que puede decir el servidor. 
                 */
                if (Ti.App.deployType != 'production') console.log('login error 401', {});
                var ID_1463949108 = null;
                if ('mostrar_botones' in require('funciones')) {
                    ID_1463949108 = require('funciones').mostrar_botones({});
                } else {
                    try {
                        ID_1463949108 = f_mostrar_botones({});
                    } catch (ee) {}
                }

                $.ID_1755668974.detener_progreso({});
                var ID_1837050684_opts = ['Aceptar'];
                var ID_1837050684 = Ti.UI.createAlertDialog({
                    title: 'Alerta',
                    message: 'Clave o usuario incorrectos.',
                    buttonNames: ID_1837050684_opts
                });
                ID_1837050684.addEventListener('click', function(e) {
                    var res = ID_1837050684_opts[e.index];
                    res = null;

                    e.source.removeEventListener("click", arguments.callee);
                });
                ID_1837050684.show();
            } else if (elemento.error == 402) {
                if (Ti.App.deployType != 'production') console.log('login error 402', {});
                var ID_1281427820 = null;
                if ('mostrar_botones' in require('funciones')) {
                    ID_1281427820 = require('funciones').mostrar_botones({});
                } else {
                    try {
                        ID_1281427820 = f_mostrar_botones({});
                    } catch (ee) {}
                }

                $.ID_1755668974.detener_progreso({});
                var ID_218946890_opts = ['Aceptar'];
                var ID_218946890 = Ti.UI.createAlertDialog({
                    title: 'Alerta',
                    message: 'Usuario bloqueado',
                    buttonNames: ID_218946890_opts
                });
                ID_218946890.addEventListener('click', function(e) {
                    var res = ID_218946890_opts[e.index];
                    res = null;

                    e.source.removeEventListener("click", arguments.callee);
                });
                ID_218946890.show();
            } else if (elemento.error == 0 || elemento.error == '0') {
                /** 
                 * En el caso que no exista error (O sea, error 0), procedemos con la limpieza y carga de datos en la base de datos 
                 */
                if (Ti.App.deployType != 'production') console.log('login error 0: todo bien, accediendo', {});
                if (Ti.App.deployType != 'production') console.log('limpiando tablas locales asociadas a login previo', {});
                var ID_1229479795_i = Alloy.Collections.inspectores;
                var sql = "DELETE FROM " + ID_1229479795_i.config.adapter.collection_name;
                var db = Ti.Database.open(ID_1229479795_i.config.adapter.db_name);
                db.execute(sql);
                db.close();
                sql = null;
                db = null;
                ID_1229479795_i.trigger('delete');
                var ID_644557694_i = Alloy.Collections.tareas_entrantes;
                var sql = "DELETE FROM " + ID_644557694_i.config.adapter.collection_name;
                var db = Ti.Database.open(ID_644557694_i.config.adapter.db_name);
                db.execute(sql);
                db.close();
                sql = null;
                db = null;
                ID_644557694_i.trigger('delete');
                var ID_217861599_i = Alloy.Collections.tareas;
                var sql = "DELETE FROM " + ID_217861599_i.config.adapter.collection_name;
                var db = Ti.Database.open(ID_217861599_i.config.adapter.db_name);
                db.execute(sql);
                db.close();
                sql = null;
                db = null;
                ID_217861599_i.trigger('delete');
                var ID_95859824_i = Alloy.Collections.emergencia;
                var sql = "DELETE FROM " + ID_95859824_i.config.adapter.collection_name;
                var db = Ti.Database.open(ID_95859824_i.config.adapter.db_name);
                db.execute(sql);
                db.close();
                sql = null;
                db = null;
                ID_95859824_i.trigger('delete');
                var ID_548050463_i = Alloy.Collections.historial_tareas;
                var sql = "DELETE FROM " + ID_548050463_i.config.adapter.collection_name;
                var db = Ti.Database.open(ID_548050463_i.config.adapter.db_name);
                db.execute(sql);
                db.close();
                sql = null;
                db = null;
                ID_548050463_i.trigger('delete');
                /** 
                 * alias para simplificar campos de inspector. 
                 */
                inspector = elemento.inspector;
                var ID_911234606_m = Alloy.Collections.inspectores;
                var ID_911234606_fila = Alloy.createModel('inspectores', {
                    apellido_materno: inspector.apellido_materno,
                    id_nivel1: inspector.nivel_1,
                    lat_dir: inspector.lat_dir,
                    disponibilidad_viajar_pais: inspector.disponibilidad_viajar_pais,
                    uuid: inspector.uidd,
                    fecha_nacimiento: inspector.fecha_nacimiento,
                    d1: inspector.d1,
                    d2: inspector.d2,
                    password: inspector.password,
                    estado: inspector.estado,
                    pais: inspector.pais,
                    direccion: inspector.direccion,
                    d3: inspector.d3,
                    nivel3: inspector.nivel_3,
                    d5: inspector.d5,
                    d4: inspector.d4,
                    disponibilidad_fechas: inspector.disponibilidad_fechas,
                    d7: inspector.d7,
                    nivel4: inspector.nivel_4,
                    nombre: inspector.nombre,
                    nivel5: inspector.nivel_5,
                    disponibilidad_horas: inspector.disponibilidad_horas,
                    nivel2: inspector.nivel_2,
                    lon_dir: inspector.lon_dir,
                    disponibilidad_viajar_ciudad: inspector.disponibilidad_viajar_ciudad,
                    id_server: inspector.id,
                    d6: inspector.d6,
                    telefono: inspector.telefono,
                    experiencia_detalle: inspector.experiencia_detalle,
                    disponibilidad: inspector.disponibilidad,
                    experiencia_oficio: inspector.id_experencia_oficio,
                    codigo_identificador: inspector.codigo_identificador,
                    apellido_paterno: inspector.apellido_paterno,
                    correo: inspector.correo
                });
                ID_911234606_m.add(ID_911234606_fila);
                ID_911234606_fila.save();
                var elemento_tareas_mistareas = elemento.tareas.mistareas;
                var ID_1617645398_m = Alloy.Collections.tareas;
                var db_ID_1617645398 = Ti.Database.open(ID_1617645398_m.config.adapter.db_name);
                db_ID_1617645398.execute('BEGIN');
                _.each(elemento_tareas_mistareas, function(ID_1617645398_fila, pos) {
                    db_ID_1617645398.execute('INSERT INTO tareas (fecha_tarea, id_inspeccion, id_asegurado, nivel_2, comentario_can_o_rech, asegurado_tel_fijo, estado_tarea, bono, evento, id_inspector, asegurado_codigo_identificador, lat, nivel_1, asegurado_nombre, pais, direccion, asegurador, fecha_ingreso, fecha_siniestro, nivel_1_texto, distance, nivel_4, perfil, asegurado_id, pais_texto, id_server, categoria, nivel_3, asegurado_correo, num_caso, lon, asegurado_tel_movil, exige_medidas, nivel_5, tipo_tarea) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', ID_1617645398_fila.fecha_tarea, ID_1617645398_fila.id_inspeccion, ID_1617645398_fila.id_asegurado, ID_1617645398_fila.nivel_2, ID_1617645398_fila.comentario_can_o_rech, ID_1617645398_fila.asegurado_tel_fijo, ID_1617645398_fila.estado_tarea, ID_1617645398_fila.bono, ID_1617645398_fila.evento, ID_1617645398_fila.id_inspector, ID_1617645398_fila.asegurado_codigo_identificador, ID_1617645398_fila.lat, ID_1617645398_fila.nivel_1, ID_1617645398_fila.asegurado_nombre, ID_1617645398_fila.pais, ID_1617645398_fila.direccion, ID_1617645398_fila.asegurador, ID_1617645398_fila.fecha_ingreso, ID_1617645398_fila.fecha_siniestro, ID_1617645398_fila.nivel_1_, ID_1617645398_fila.distancia, ID_1617645398_fila.nivel_4, 'ubicacion', ID_1617645398_fila.asegurado_id, ID_1617645398_fila.pais, ID_1617645398_fila.id, ID_1617645398_fila.categoria, ID_1617645398_fila.nivel_3, ID_1617645398_fila.asegurado_correo, ID_1617645398_fila.num_caso, ID_1617645398_fila.lon, ID_1617645398_fila.asegurado_tel_movil, ID_1617645398_fila.exige_medidas, ID_1617645398_fila.nivel_5, ID_1617645398_fila.tipo_tarea);
                });
                db_ID_1617645398.execute('COMMIT');
                db_ID_1617645398.close();
                db_ID_1617645398 = null;
                ID_1617645398_m.trigger('change');
                var elemento_tareas_historial_tareas = elemento.tareas.historial_tareas;
                var ID_352267062_m = Alloy.Collections.historial_tareas;
                var db_ID_352267062 = Ti.Database.open(ID_352267062_m.config.adapter.db_name);
                db_ID_352267062.execute('BEGIN');
                _.each(elemento_tareas_historial_tareas, function(ID_352267062_fila, pos) {
                    db_ID_352267062.execute('INSERT INTO historial_tareas (fecha_tarea, id_inspeccion, id_asegurado, nivel_2, comentario_can_o_rech, asegurado_tel_fijo, estado_tarea, bono, evento, id_inspector, asegurado_codigo_identificador, lat, nivel_1, asegurado_nombre, pais, direccion, asegurador, fecha_ingreso, fecha_siniestro, nivel_1_texto, distance, fecha_termino, nivel_4, perfil, asegurado_id, id_server, categoria, nivel_3, asegurado_correo, num_caso, lon, asegurado_tel_movil, nivel_5, tipo_tarea, hora_termino) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', ID_352267062_fila.fecha_tarea, ID_352267062_fila.id_inspeccion, ID_352267062_fila.id_asegurado, ID_352267062_fila.nivel_2, ID_352267062_fila.comentario_can_o_rech, ID_352267062_fila.asegurado_tel_fijo, ID_352267062_fila.estado_tarea, ID_352267062_fila.bono, ID_352267062_fila.evento, ID_352267062_fila.id_inspector, ID_352267062_fila.asegurado_codigo_identificador, ID_352267062_fila.lat, ID_352267062_fila.nivel_1, ID_352267062_fila.asegurado_nombre, ID_352267062_fila.pais, ID_352267062_fila.direccion, ID_352267062_fila.asegurador, ID_352267062_fila.fecha_ingreso, ID_352267062_fila.fecha_siniestro, ID_352267062_fila.nivel_1_, ID_352267062_fila.distancia, ID_352267062_fila.fecha_finalizacion, ID_352267062_fila.nivel_4, 'ubicacion', ID_352267062_fila.asegurado_id, ID_352267062_fila.id, ID_352267062_fila.categoria, ID_352267062_fila.nivel_3, ID_352267062_fila.asegurado_correo, ID_352267062_fila.num_caso, ID_352267062_fila.lon, ID_352267062_fila.asegurado_tel_movil, ID_352267062_fila.nivel_5, ID_352267062_fila.tipo_tarea, ID_352267062_fila.hora_finalizacion);
                });
                db_ID_352267062.execute('COMMIT');
                db_ID_352267062.close();
                db_ID_352267062 = null;
                ID_352267062_m.trigger('change');
                var elemento_tareas_emergencias_perfil = elemento.tareas.emergencias.perfil;
                var ID_975483869_m = Alloy.Collections.emergencia;
                var db_ID_975483869 = Ti.Database.open(ID_975483869_m.config.adapter.db_name);
                db_ID_975483869.execute('BEGIN');
                _.each(elemento_tareas_emergencias_perfil, function(ID_975483869_fila, pos) {
                    db_ID_975483869.execute('INSERT INTO emergencia (fecha_tarea, id_inspeccion, id_asegurado, nivel_2, comentario_can_o_rech, asegurado_tel_fijo, estado_tarea, bono, evento, id_inspector, asegurado_codigo_identificador, lat, nivel_1, asegurado_nombre, pais, direccion, asegurador, fecha_ingreso, fecha_siniestro, nivel_1_texto, distance, nivel_4, perfil, asegurado_id, pais_texto, id_server, categoria, nivel_3, asegurado_correo, num_caso, lon, asegurado_tel_movil, exige_medidas, distancia_2, nivel_5, tipo_tarea) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', ID_975483869_fila.fecha_tarea, ID_975483869_fila.id_inspeccion, ID_975483869_fila.id_asegurado, ID_975483869_fila.nivel_2, ID_975483869_fila.comentario_can_o_rech, ID_975483869_fila.asegurado_tel_fijo, ID_975483869_fila.estado_tarea, ID_975483869_fila.bono, ID_975483869_fila.evento, ID_975483869_fila.id_inspector, ID_975483869_fila.asegurado_codigo_identificador, ID_975483869_fila.lat, ID_975483869_fila.nivel_1, ID_975483869_fila.asegurado_nombre, ID_975483869_fila.pais, ID_975483869_fila.direccion, ID_975483869_fila.asegurador, ID_975483869_fila.fecha_ingreso, ID_975483869_fila.fecha_siniestro, ID_975483869_fila.nivel_1_, ID_975483869_fila.distancia, ID_975483869_fila.nivel_4, 'casa', ID_975483869_fila.asegurado_id, ID_975483869_fila.pais, ID_975483869_fila.id, ID_975483869_fila.categoria, ID_975483869_fila.nivel_3, ID_975483869_fila.asegurado_correo, ID_975483869_fila.num_caso, ID_975483869_fila.lon, ID_975483869_fila.asegurado_tel_movil, ID_975483869_fila.exige_medidas, ID_975483869_fila.distancia_2, ID_975483869_fila.nivel_5, ID_975483869_fila.tipo_tarea);
                });
                db_ID_975483869.execute('COMMIT');
                db_ID_975483869.close();
                db_ID_975483869 = null;
                ID_975483869_m.trigger('change');
                var elemento_tareas_emergencias_ubicacion = elemento.tareas.emergencias.ubicacion;
                var ID_203866951_m = Alloy.Collections.emergencia;
                var db_ID_203866951 = Ti.Database.open(ID_203866951_m.config.adapter.db_name);
                db_ID_203866951.execute('BEGIN');
                _.each(elemento_tareas_emergencias_ubicacion, function(ID_203866951_fila, pos) {
                    db_ID_203866951.execute('INSERT INTO emergencia (fecha_tarea, id_inspeccion, id_asegurado, nivel_2, comentario_can_o_rech, asegurado_tel_fijo, estado_tarea, bono, evento, id_inspector, asegurado_codigo_identificador, lat, nivel_1, asegurado_nombre, pais, direccion, asegurador, fecha_ingreso, fecha_siniestro, nivel_1_texto, distance, nivel_4, perfil, asegurado_id, pais_texto, id_server, categoria, nivel_3, asegurado_correo, num_caso, lon, asegurado_tel_movil, exige_medidas, distancia_2, nivel_5, tipo_tarea) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', ID_203866951_fila.fecha_tarea, ID_203866951_fila.id_inspeccion, ID_203866951_fila.id_asegurado, ID_203866951_fila.nivel_2, ID_203866951_fila.comentario_can_o_rech, ID_203866951_fila.asegurado_tel_fijo, ID_203866951_fila.estado_tarea, ID_203866951_fila.bono, ID_203866951_fila.evento, ID_203866951_fila.id_inspector, ID_203866951_fila.asegurado_codigo_identificador, ID_203866951_fila.lat, ID_203866951_fila.nivel_1, ID_203866951_fila.asegurado_nombre, ID_203866951_fila.pais, ID_203866951_fila.direccion, ID_203866951_fila.asegurador, ID_203866951_fila.fecha_ingreso, ID_203866951_fila.fecha_siniestro, ID_203866951_fila.nivel_1_, ID_203866951_fila.distancia, ID_203866951_fila.nivel_4, 'ubicacion', ID_203866951_fila.asegurado_id, ID_203866951_fila.pais, ID_203866951_fila.id, ID_203866951_fila.categoria, ID_203866951_fila.nivel_3, ID_203866951_fila.asegurado_correo, ID_203866951_fila.num_caso, ID_203866951_fila.lon, ID_203866951_fila.asegurado_tel_movil, ID_203866951_fila.exige_medidas, ID_203866951_fila.distancia_2, ID_203866951_fila.nivel_5, ID_203866951_fila.tipo_tarea);
                });
                db_ID_203866951.execute('COMMIT');
                db_ID_203866951.close();
                db_ID_203866951 = null;
                ID_203866951_m.trigger('change');
                var elemento_tareas_entrantes_critica = elemento.tareas.entrantes.critica;
                var ID_1943080367_m = Alloy.Collections.tareas_entrantes;
                var db_ID_1943080367 = Ti.Database.open(ID_1943080367_m.config.adapter.db_name);
                db_ID_1943080367.execute('BEGIN');
                _.each(elemento_tareas_entrantes_critica, function(ID_1943080367_fila, pos) {
                    db_ID_1943080367.execute('INSERT INTO tareas_entrantes (fecha_tarea, id_inspeccion, id_asegurado, nivel_2, comentario_can_o_rech, asegurado_tel_fijo, estado_tarea, bono, evento, id_inspector, asegurado_codigo_identificador, lat, nivel_1, asegurado_nombre, pais, direccion, asegurador, fecha_ingreso, fecha_siniestro, nivel_1_texto, distance, nivel_4, perfil, asegurado_id, pais_texto, id_server, categoria, nivel_3, asegurado_correo, num_caso, lon, asegurado_tel_movil, exige_medidas, tipo_tarea, nivel_5) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', ID_1943080367_fila.fecha_tarea, ID_1943080367_fila.id_inspeccion, ID_1943080367_fila.id_asegurado, ID_1943080367_fila.nivel_2, ID_1943080367_fila.comentario_can_o_rech, ID_1943080367_fila.asegurado_tel_fijo, ID_1943080367_fila.estado_tarea, ID_1943080367_fila.bono, ID_1943080367_fila.evento, ID_1943080367_fila.id_inspector, ID_1943080367_fila.asegurado_codigo_identificador, ID_1943080367_fila.lat, ID_1943080367_fila.nivel_1, ID_1943080367_fila.asegurado_nombre, ID_1943080367_fila.pais, ID_1943080367_fila.direccion, ID_1943080367_fila.asegurador, ID_1943080367_fila.fecha_ingreso, ID_1943080367_fila.fecha_siniestro, ID_1943080367_fila.nivel_1_, ID_1943080367_fila.distancia, ID_1943080367_fila.nivel_4, 'casa', ID_1943080367_fila.asegurado_id, ID_1943080367_fila.pais, ID_1943080367_fila.id, ID_1943080367_fila.categoria, ID_1943080367_fila.nivel_3, ID_1943080367_fila.asegurado_correo, ID_1943080367_fila.num_caso, ID_1943080367_fila.lon, ID_1943080367_fila.asegurado_tel_movil, ID_1943080367_fila.exige_medidas, ID_1943080367_fila.tipo_tarea, ID_1943080367_fila.nivel_5);
                });
                db_ID_1943080367.execute('COMMIT');
                db_ID_1943080367.close();
                db_ID_1943080367 = null;
                ID_1943080367_m.trigger('change');
                var elemento_tareas_entrantes_normal = elemento.tareas.entrantes.normal;
                var ID_603720446_m = Alloy.Collections.tareas_entrantes;
                var db_ID_603720446 = Ti.Database.open(ID_603720446_m.config.adapter.db_name);
                db_ID_603720446.execute('BEGIN');
                _.each(elemento_tareas_entrantes_normal, function(ID_603720446_fila, pos) {
                    db_ID_603720446.execute('INSERT INTO tareas_entrantes (fecha_tarea, id_inspeccion, id_asegurado, nivel_2, comentario_can_o_rech, asegurado_tel_fijo, estado_tarea, bono, evento, id_inspector, asegurado_codigo_identificador, lat, nivel_1, asegurado_nombre, pais, direccion, asegurador, fecha_ingreso, fecha_siniestro, nivel_1_texto, distance, nivel_4, perfil, asegurado_id, pais_texto, id_server, categoria, nivel_3, asegurado_correo, num_caso, lon, asegurado_tel_movil, exige_medidas, tipo_tarea, nivel_5) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', ID_603720446_fila.fecha_tarea, ID_603720446_fila.id_inspeccion, ID_603720446_fila.id_asegurado, ID_603720446_fila.nivel_2, ID_603720446_fila.comentario_can_o_rech, ID_603720446_fila.asegurado_tel_fijo, ID_603720446_fila.estado_tarea, ID_603720446_fila.bono, ID_603720446_fila.evento, ID_603720446_fila.id_inspector, ID_603720446_fila.asegurado_codigo_identificador, ID_603720446_fila.lat, ID_603720446_fila.nivel_1, ID_603720446_fila.asegurado_nombre, ID_603720446_fila.pais, ID_603720446_fila.direccion, ID_603720446_fila.asegurador, ID_603720446_fila.fecha_ingreso, ID_603720446_fila.fecha_siniestro, ID_603720446_fila.nivel_1_, ID_603720446_fila.distancia, ID_603720446_fila.nivel_4, 'casa', ID_603720446_fila.asegurado_id, ID_603720446_fila.pais, ID_603720446_fila.id, ID_603720446_fila.categoria, ID_603720446_fila.nivel_3, ID_603720446_fila.asegurado_correo, ID_603720446_fila.num_caso, ID_603720446_fila.lon, ID_603720446_fila.asegurado_tel_movil, ID_603720446_fila.exige_medidas, ID_603720446_fila.tipo_tarea, ID_603720446_fila.nivel_5);
                });
                db_ID_603720446.execute('COMMIT');
                db_ID_603720446.close();
                db_ID_603720446 = null;
                ID_603720446_m.trigger('change');
                /** 
                 * Al terminar la carga de datos de la base de datos, llamamos una funcion para continuar con el proceso 
                 */
                var ID_1038535132 = null;
                if ('cargar_selectores' in require('funciones')) {
                    ID_1038535132 = require('funciones').cargar_selectores({});
                } else {
                    try {
                        ID_1038535132 = f_cargar_selectores({});
                    } catch (ee) {}
                }
            } else {
                /** 
                 * Y si es un error que no tenemos registrado, mostramos el mensaje directamente en una alerta 
                 */
                var ID_1626215927 = null;
                if ('mostrar_botones' in require('funciones')) {
                    ID_1626215927 = require('funciones').mostrar_botones({});
                } else {
                    try {
                        ID_1626215927 = f_mostrar_botones({});
                    } catch (ee) {}
                }
                var ID_927055317_opts = ['Aceptar'];
                var ID_927055317 = Ti.UI.createAlertDialog({
                    title: 'Alerta',
                    message: '' + 'Error ' + elemento.mensaje + '',
                    buttonNames: ID_927055317_opts
                });
                ID_927055317.addEventListener('click', function(e) {
                    var res = ID_927055317_opts[e.index];
                    res = null;

                    e.source.removeEventListener("click", arguments.callee);
                });
                ID_927055317.show();
                var esperando = 'status: esperando';
                $.ID_908004102.setText(esperando);

            }
            elemento = null, valor = null;
        };

        ID_822595707.error = function(e) {
            var elemento = e,
                valor = e;
            var ID_697710795 = null;
            if ('mostrar_botones' in require('funciones')) {
                ID_697710795 = require('funciones').mostrar_botones({});
            } else {
                try {
                    ID_697710795 = f_mostrar_botones({});
                } catch (ee) {}
            }
            var ID_552165355_opts = ['Aceptar'];
            var ID_552165355 = Ti.UI.createAlertDialog({
                title: 'Alerta',
                message: 'Problema de conexion, favor intentar más tarde',
                buttonNames: ID_552165355_opts
            });
            ID_552165355.addEventListener('click', function(e) {
                var suu = ID_552165355_opts[e.index];
                suu = null;

                e.source.removeEventListener("click", arguments.callee);
            });
            ID_552165355.show();
            var esperando = 'status: esperando';
            $.ID_908004102.setText(esperando);

            elemento = null, valor = null;
        };
        require('helper').ajaxUnico('ID_822595707', '' + url_server + 'login' + '', 'POST', {
            correo: correo,
            password: password,
            device: Ti.Platform.name,
            lat: gps_latitud,
            lon: gps_longitud,
            device_token: devicetoken,
            uidd: uidd,
            fecha: dif
        }, 15000, ID_822595707);
    }
    return null;
};
var f_cargar_selectores = function(x_params) {
    var item = x_params['item'];
    $.ID_908004102.setText('status: cargando selectores');

    /** 
     * Consultamos la tabla de inspectores y su resultado lo guardamos en la variable inspector 
     */
    var ID_568274345_i = Alloy.createCollection('inspectores');
    var ID_568274345_i_where = '';
    ID_568274345_i.fetch();
    var inspector = require('helper').query2array(ID_568274345_i);
    var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
    if (inspector && inspector.length) {
        /** 
         * guardamos la variable: inspector con los datos del inspector activo para no tener que consultarla cada vez dentro del menu. 
         */
        require('vars')['inspector'] = inspector[0];
        /** 
         * Consultamos la tabla de pais, filtrando por el pais del inspector, guardamos la consulta en una variable pais 
         */
        var ID_499816964_i = Alloy.createCollection('pais');
        var ID_499816964_i_where = 'id_server=\'' + inspector[0].pais + '\'';
        ID_499816964_i.fetch({
            query: 'SELECT * FROM pais WHERE id_server=\'' + inspector[0].pais + '\''
        });
        var pais = require('helper').query2array(ID_499816964_i);
        if (pais && pais.length) {
            console.log('test0');
            require('vars')['pais'] = pais;
            var selectores_fechas = JSON.parse(Ti.App.Properties.getString('selectores_fechas'));
            if (selectores_fechas == null) {
                var defecto = {
                    fecha: ''
                };

                var selectores_fechas = {
                    destino: defecto,
                    entidad_financiera: defecto,
                    tipo_siniestro: defecto,
                    estructura_soportante: defecto,
                    muros_tabiques: defecto,
                    entrepisos: defecto,
                    pavimento: defecto,
                    estructura_cubierta: defecto,
                    cubierta: defecto,
                    bienes: defecto,
                    compania: defecto,
                    partida: defecto,
                    marcas: defecto,
                    monedas: defecto,
                    medidas_seguridad: defecto
                };
                defecto = null;
            }

            var ID_973615828 = {};

            console.log('DEBUG WEB: requesting url:' + url_server + 'selectores' + ' with data:', {
                _method: 'POST',
                _params: {
                    pais: pais[0].nombre,
                    destino: selectores_fechas.destino.fecha,
                    entidad_financiera: selectores_fechas.entidad_financiera.fecha,
                    compania: selectores_fechas.compania.fecha,
                    tipo_siniestro: selectores_fechas.tipo_siniestro.fecha,
                    estructura_soportante: selectores_fechas.estructura_soportante.fecha,
                    muros_tabiques: selectores_fechas.muros_tabiques.fecha,
                    entrepisos: selectores_fechas.entrepisos.fecha,
                    pavimento: selectores_fechas.pavimento.fecha,
                    estructura_cubierta: selectores_fechas.estructura_cubierta.fecha,
                    cubierta: selectores_fechas.cubierta.fecha,
                    partida: selectores_fechas.partida.fecha,
                    bienes: selectores_fechas.bienes.fecha,
                    monedas: selectores_fechas.monedas.fecha,
                    marcas: selectores_fechas.marcas.fecha,
                    medidas_seguridad: selectores_fechas.medidas_seguridad.fecha,
                    recintos: '',
                    accion: '',
                    unidad_medida: ''
                },
                _timeout: '15000'
            });

            ID_973615828.success = function(e) {
                var elemento = e,
                    valor = e;
                if (elemento.error == 0 || elemento.error == '0') {
                    var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
                    var selectores_fechas = JSON.parse(Ti.App.Properties.getString('selectores_fechas'));
                    /** 
                     * Creamos una variable selectores_list y agregamos un campo nuevo, que sera la fecha. Despues los ordenamos segun el nombre del selector 
                     */

                    selectores_list = _.map(elemento.selectores,
                        function(num) {
                            var object = {
                                selector: num.selector,
                                fecha: num.fecha
                            };
                            return object;
                        });

                    selectores_fechas = _.indexBy(selectores_list, 'selector');
                    Ti.App.Properties.setString('selectores_fechas', JSON.stringify(selectores_fechas));


                    /** 
                     * Limpiamos las tablas, insertamos los datos desde el servidor y vamos modificando el texto que muestra en pantalla el status de los datos que estan siendo almacenados 
                     */
                    if (Ti.App.deployType != 'production') console.log('Ingresando datos de pais actual', {});
                    if (Ti.App.deployType != 'production') console.log('ingresando: destino (' + elemento.destino.length + ')', {});
                    if (elemento.destino && elemento.destino.length) {
                        if (Ti.App.deployType != 'production') console.log('agregando destinos que no deberia agregar cuando se ejecuta por 2da vez...');
                        var elemento_destino = elemento.destino;
                        var ID_409902215_m = Alloy.Collections.destino;
                        var db_ID_409902215 = Ti.Database.open(ID_409902215_m.config.adapter.db_name);
                        db_ID_409902215.execute('BEGIN');
                        _.each(elemento_destino, function(ID_409902215_fila, pos) {
                            db_ID_409902215.execute('INSERT INTO destino (nombre, fecha, pais_texto, id_server, id_segured, pais) VALUES (?,?,?,?,?,?)', ID_409902215_fila.valor, ID_409902215_fila.fecha, ID_409902215_fila.pais_texto, ID_409902215_fila.id, ID_409902215_fila.id_segured, ID_409902215_fila.pais);
                        });
                        db_ID_409902215.execute('COMMIT');
                        db_ID_409902215.close();
                        db_ID_409902215 = null;
                        ID_409902215_m.trigger('change');
                        $.ID_908004102.setText('status: insertando destinos');
                    }
                    if (Ti.App.deployType != 'production') console.log('ingresando: compania (' + elemento.compania.length + ')', {});
                    if (elemento.compania && elemento.compania.length) {
                        var elemento_compania = elemento.compania;
                        var ID_1215758250_m = Alloy.Collections.compania;
                        var db_ID_1215758250 = Ti.Database.open(ID_1215758250_m.config.adapter.db_name);
                        db_ID_1215758250.execute('BEGIN');
                        _.each(elemento_compania, function(ID_1215758250_fila, pos) {
                            db_ID_1215758250.execute('INSERT INTO compania (nombre, fecha, id_server, pais_texto, id_segured, pais) VALUES (?,?,?,?,?,?)', ID_1215758250_fila.valor, ID_1215758250_fila.fecha, ID_1215758250_fila.id, ID_1215758250_fila.pais_texto, ID_1215758250_fila.id_segured, ID_1215758250_fila.pais);
                        });
                        db_ID_1215758250.execute('COMMIT');
                        db_ID_1215758250.close();
                        db_ID_1215758250 = null;
                        ID_1215758250_m.trigger('change');
                        $.ID_908004102.setText('status: insertando companias');
                    }
                    if (Ti.App.deployType != 'production') console.log('ingresando: entidad_financiera (' + elemento.entidad_financiera.length + ')', {});
                    if (elemento.entidad_financiera && elemento.entidad_financiera.length) {
                        var elemento_entidad_financiera = elemento.entidad_financiera;
                        var ID_1955151325_m = Alloy.Collections.entidad_financiera;
                        var db_ID_1955151325 = Ti.Database.open(ID_1955151325_m.config.adapter.db_name);
                        db_ID_1955151325.execute('BEGIN');
                        _.each(elemento_entidad_financiera, function(ID_1955151325_fila, pos) {
                            db_ID_1955151325.execute('INSERT INTO entidad_financiera (nombre, fecha, id_server, pais_texto, id_segured, pais) VALUES (?,?,?,?,?,?)', ID_1955151325_fila.valor, ID_1955151325_fila.fecha, ID_1955151325_fila.id, ID_1955151325_fila.pais_texto, ID_1955151325_fila.id_segured, ID_1955151325_fila.pais);
                        });
                        db_ID_1955151325.execute('COMMIT');
                        db_ID_1955151325.close();
                        db_ID_1955151325 = null;
                        ID_1955151325_m.trigger('change');
                        $.ID_908004102.setText('status: insertando entidades');
                    }
                    if (Ti.App.deployType != 'production') console.log('ingresando: tipo_siniestro (' + elemento.tipo_siniestro.length + ')', {});
                    if (elemento.tipo_siniestro && elemento.tipo_siniestro.length) {
                        var elemento_tipo_siniestro = elemento.tipo_siniestro;
                        var ID_478898033_m = Alloy.Collections.tipo_siniestro;
                        var db_ID_478898033 = Ti.Database.open(ID_478898033_m.config.adapter.db_name);
                        db_ID_478898033.execute('BEGIN');
                        _.each(elemento_tipo_siniestro, function(ID_478898033_fila, pos) {
                            db_ID_478898033.execute('INSERT INTO tipo_siniestro (nombre, fecha, id_server, pais_texto, id_segured, pais) VALUES (?,?,?,?,?,?)', ID_478898033_fila.valor, ID_478898033_fila.fecha, ID_478898033_fila.id, ID_478898033_fila.pais_texto, ID_478898033_fila.id_segured, ID_478898033_fila.pais);
                        });
                        db_ID_478898033.execute('COMMIT');
                        db_ID_478898033.close();
                        db_ID_478898033 = null;
                        ID_478898033_m.trigger('change');
                        $.ID_908004102.setText('status: insertando tipo siniestro');
                    }
                    if (Ti.App.deployType != 'production') console.log('ingresando: estructura_soportante (' + elemento.estructura_soportante.length + ')', {});
                    if (elemento.estructura_soportante && elemento.estructura_soportante.length) {
                        var elemento_estructura_soportante = elemento.estructura_soportante;
                        var ID_155872468_m = Alloy.Collections.estructura_soportante;
                        var db_ID_155872468 = Ti.Database.open(ID_155872468_m.config.adapter.db_name);
                        db_ID_155872468.execute('BEGIN');
                        _.each(elemento_estructura_soportante, function(ID_155872468_fila, pos) {
                            db_ID_155872468.execute('INSERT INTO estructura_soportante (nombre, fecha, id_server, pais_texto, id_segured, pais) VALUES (?,?,?,?,?,?)', ID_155872468_fila.valor, ID_155872468_fila.fecha, ID_155872468_fila.id, ID_155872468_fila.pais_texto, ID_155872468_fila.id_segured, ID_155872468_fila.pais);
                        });
                        db_ID_155872468.execute('COMMIT');
                        db_ID_155872468.close();
                        db_ID_155872468 = null;
                        ID_155872468_m.trigger('change');
                        $.ID_908004102.setText('status: insertando tipo estructuras soportantes');
                    }
                    if (Ti.App.deployType != 'production') console.log('ingresando: muros_tabiques (' + elemento.muros_tabiques.length + ')', {});
                    if (elemento.muros_tabiques && elemento.muros_tabiques.length) {
                        var elemento_muros_tabiques = elemento.muros_tabiques;
                        var ID_1234382929_m = Alloy.Collections.muros_tabiques;
                        var db_ID_1234382929 = Ti.Database.open(ID_1234382929_m.config.adapter.db_name);
                        db_ID_1234382929.execute('BEGIN');
                        _.each(elemento_muros_tabiques, function(ID_1234382929_fila, pos) {
                            db_ID_1234382929.execute('INSERT INTO muros_tabiques (nombre, fecha, id_server, pais_texto, id_segured, pais) VALUES (?,?,?,?,?,?)', ID_1234382929_fila.valor, ID_1234382929_fila.fecha, ID_1234382929_fila.id, ID_1234382929_fila.pais_texto, ID_1234382929_fila.id_segured, ID_1234382929_fila.pais);
                        });
                        db_ID_1234382929.execute('COMMIT');
                        db_ID_1234382929.close();
                        db_ID_1234382929 = null;
                        ID_1234382929_m.trigger('change');
                        $.ID_908004102.setText('status: insertando tipo muro tabiques');
                    }
                    if (Ti.App.deployType != 'production') console.log('ingresando: entrepisos (' + elemento.entrepisos.length + ')', {});
                    if (elemento.entrepisos && elemento.entrepisos.length) {
                        var elemento_entrepisos = elemento.entrepisos;
                        var ID_1619617595_m = Alloy.Collections.entrepisos;
                        var db_ID_1619617595 = Ti.Database.open(ID_1619617595_m.config.adapter.db_name);
                        db_ID_1619617595.execute('BEGIN');
                        _.each(elemento_entrepisos, function(ID_1619617595_fila, pos) {
                            db_ID_1619617595.execute('INSERT INTO entrepisos (nombre, fecha, id_server, pais_texto, id_segured, pais) VALUES (?,?,?,?,?,?)', ID_1619617595_fila.valor, ID_1619617595_fila.fecha, ID_1619617595_fila.id, ID_1619617595_fila.pais_texto, ID_1619617595_fila.id_segured, ID_1619617595_fila.pais);
                        });
                        db_ID_1619617595.execute('COMMIT');
                        db_ID_1619617595.close();
                        db_ID_1619617595 = null;
                        ID_1619617595_m.trigger('change');
                        $.ID_908004102.setText('status: insertando entrepisos');
                    }
                    if (Ti.App.deployType != 'production') console.log('ingresando: pavimento (' + elemento.pavimento.length + ')', {});
                    if (elemento.pavimento && elemento.pavimento.length) {
                        var elemento_pavimento = elemento.pavimento;
                        var ID_580876494_m = Alloy.Collections.pavimento;
                        var db_ID_580876494 = Ti.Database.open(ID_580876494_m.config.adapter.db_name);
                        db_ID_580876494.execute('BEGIN');
                        _.each(elemento_pavimento, function(ID_580876494_fila, pos) {
                            db_ID_580876494.execute('INSERT INTO pavimento (nombre, fecha, id_server, pais_texto, id_segured, pais) VALUES (?,?,?,?,?,?)', ID_580876494_fila.valor, ID_580876494_fila.fecha, ID_580876494_fila.id, ID_580876494_fila.pais_texto, ID_580876494_fila.id_segured, ID_580876494_fila.pais);
                        });
                        db_ID_580876494.execute('COMMIT');
                        db_ID_580876494.close();
                        db_ID_580876494 = null;
                        ID_580876494_m.trigger('change');
                        $.ID_908004102.setText('status: insertando tipo pavimento');
                    }
                    if (Ti.App.deployType != 'production') console.log('ingresando: estructura_cubierta (' + elemento.estructura_cubierta.length + ')', {});
                    if (elemento.estructura_cubierta && elemento.estructura_cubierta.length) {
                        var elemento_estructura_cubierta = elemento.estructura_cubierta;
                        var ID_1068733844_m = Alloy.Collections.estructura_cubierta;
                        var db_ID_1068733844 = Ti.Database.open(ID_1068733844_m.config.adapter.db_name);
                        db_ID_1068733844.execute('BEGIN');
                        _.each(elemento_estructura_cubierta, function(ID_1068733844_fila, pos) {
                            db_ID_1068733844.execute('INSERT INTO estructura_cubierta (nombre, fecha, id_server, pais_texto, id_segured, pais) VALUES (?,?,?,?,?,?)', ID_1068733844_fila.valor, ID_1068733844_fila.fecha, ID_1068733844_fila.id, ID_1068733844_fila.pais_texto, ID_1068733844_fila.id_segured, ID_1068733844_fila.pais);
                        });
                        db_ID_1068733844.execute('COMMIT');
                        db_ID_1068733844.close();
                        db_ID_1068733844 = null;
                        ID_1068733844_m.trigger('change');
                        $.ID_908004102.setText('status: insertando tipo estructuras cubierta');
                    }
                    if (Ti.App.deployType != 'production') console.log('ingresando: cubierta (' + elemento.cubierta.length + ')', {});
                    if (elemento.cubierta && elemento.cubierta.length) {
                        var elemento_cubierta = elemento.cubierta;
                        var ID_545291164_m = Alloy.Collections.cubierta;
                        var db_ID_545291164 = Ti.Database.open(ID_545291164_m.config.adapter.db_name);
                        db_ID_545291164.execute('BEGIN');
                        _.each(elemento_cubierta, function(ID_545291164_fila, pos) {
                            db_ID_545291164.execute('INSERT INTO cubierta (nombre, fecha, id_server, pais_texto, id_segured, pais) VALUES (?,?,?,?,?,?)', ID_545291164_fila.valor, ID_545291164_fila.fecha, ID_545291164_fila.id, ID_545291164_fila.pais_texto, ID_545291164_fila.id_segured, ID_545291164_fila.pais);
                        });
                        db_ID_545291164.execute('COMMIT');
                        db_ID_545291164.close();
                        db_ID_545291164 = null;
                        ID_545291164_m.trigger('change');
                        $.ID_908004102.setText('status: insertando cubiertas');
                    }
                    if (Ti.App.deployType != 'production') console.log('ingresando: tipo_partida (' + elemento.partida.length + ')', {});
                    if (elemento.partida && elemento.partida.length) {
                        var elemento_partida = elemento.partida;
                        var ID_182695522_m = Alloy.Collections.tipo_partida;
                        var db_ID_182695522 = Ti.Database.open(ID_182695522_m.config.adapter.db_name);
                        db_ID_182695522.execute('BEGIN');
                        _.each(elemento_partida, function(ID_182695522_fila, pos) {
                            db_ID_182695522.execute('INSERT INTO tipo_partida (nombre, fecha, id_server, pais_texto, id_segured, pais) VALUES (?,?,?,?,?,?)', ID_182695522_fila.valor, ID_182695522_fila.fecha, ID_182695522_fila.id, ID_182695522_fila.pais_texto, ID_182695522_fila.id_segured, ID_182695522_fila.pais);
                        });
                        db_ID_182695522.execute('COMMIT');
                        db_ID_182695522.close();
                        db_ID_182695522 = null;
                        ID_182695522_m.trigger('change');
                        $.ID_908004102.setText('status: insertando tipo partida');

                    }
                    if (Ti.App.deployType != 'production') console.log('ingresando: bienes (' + elemento.bienes.length + ')', {});
                    if (elemento.bienes && elemento.bienes.length) {
                        var elemento_bienes = elemento.bienes;
                        var ID_1949561285_m = Alloy.Collections.bienes;
                        var db_ID_1949561285 = Ti.Database.open(ID_1949561285_m.config.adapter.db_name);
                        db_ID_1949561285.execute('BEGIN');
                        _.each(elemento_bienes, function(ID_1949561285_fila, pos) {
                            db_ID_1949561285.execute('INSERT INTO bienes (nombre, fecha, id_server, pais_texto, id_segured, pais) VALUES (?,?,?,?,?,?)', ID_1949561285_fila.valor, ID_1949561285_fila.fecha, ID_1949561285_fila.id, ID_1949561285_fila.pais_texto, ID_1949561285_fila.id_segured, ID_1949561285_fila.pais);
                        });
                        db_ID_1949561285.execute('COMMIT');
                        db_ID_1949561285.close();
                        db_ID_1949561285 = null;
                        ID_1949561285_m.trigger('change');
                        $.ID_908004102.setText('status: insertando bienes');

                    }
                    if (Ti.App.deployType != 'production') console.log('ingresando: marcas (' + elemento.marcas.length + ')', {});
                    if (elemento.marcas && elemento.marcas.length) {
                        var elemento_marcas = elemento.marcas;
                        var ID_1357280148_m = Alloy.Collections.marcas;
                        var db_ID_1357280148 = Ti.Database.open(ID_1357280148_m.config.adapter.db_name);
                        db_ID_1357280148.execute('BEGIN');
                        _.each(elemento_marcas, function(ID_1357280148_fila, pos) {
                            db_ID_1357280148.execute('INSERT INTO marcas (nombre, fecha, id_server, pais_texto, id_segured, pais) VALUES (?,?,?,?,?,?)', ID_1357280148_fila.valor, ID_1357280148_fila.fecha, ID_1357280148_fila.id, ID_1357280148_fila.pais_texto, ID_1357280148_fila.id_segured, ID_1357280148_fila.pais);
                        });
                        db_ID_1357280148.execute('COMMIT');
                        db_ID_1357280148.close();
                        db_ID_1357280148 = null;
                        ID_1357280148_m.trigger('change');
                        $.ID_908004102.setText('status: insertando marcas');

                    }
                    if (Ti.App.deployType != 'production') console.log('ingresando: monedas (' + elemento.monedas.length + ')', {});
                    if (elemento.monedas && elemento.monedas.length) {
                        var elemento_monedas = elemento.monedas;
                        var ID_897650751_m = Alloy.Collections.monedas;
                        var db_ID_897650751 = Ti.Database.open(ID_897650751_m.config.adapter.db_name);
                        db_ID_897650751.execute('BEGIN');
                        _.each(elemento_monedas, function(ID_897650751_fila, pos) {
                            db_ID_897650751.execute('INSERT INTO monedas (nombre, fecha, id_server, pais_texto, id_segured, pais) VALUES (?,?,?,?,?,?)', ID_897650751_fila.valor, ID_897650751_fila.fecha, ID_897650751_fila.id, ID_897650751_fila.pais_texto, ID_897650751_fila.id_segured, ID_897650751_fila.pais);
                        });
                        db_ID_897650751.execute('COMMIT');
                        db_ID_897650751.close();
                        db_ID_897650751 = null;
                        ID_897650751_m.trigger('change');
                        $.ID_908004102.setText('status: insertando monedas');

                    }
                    if (Ti.App.deployType != 'production') console.log('ingresando: tipo_dano (' + elemento.tipo_dano.length + ')', {});
                    if (elemento.tipo_dano && elemento.tipo_dano.length) {
                        var elemento_tipo_dano = elemento.tipo_dano;
                        var ID_1466337281_m = Alloy.Collections.tipo_dano;
                        var db_ID_1466337281 = Ti.Database.open(ID_1466337281_m.config.adapter.db_name);
                        db_ID_1466337281.execute('BEGIN');
                        _.each(elemento_tipo_dano, function(ID_1466337281_fila, pos) {
                            db_ID_1466337281.execute('INSERT INTO tipo_dano (nombre, fecha, id_partida, id_server, pais_texto, id_segured, pais) VALUES (?,?,?,?,?,?,?)', ID_1466337281_fila.valor, ID_1466337281_fila.fecha, ID_1466337281_fila.id_partida, ID_1466337281_fila.id, ID_1466337281_fila.pais_texto, ID_1466337281_fila.id_segured, ID_1466337281_fila.pais);
                        });
                        db_ID_1466337281.execute('COMMIT');
                        db_ID_1466337281.close();
                        db_ID_1466337281 = null;
                        ID_1466337281_m.trigger('change');
                        $.ID_908004102.setText('status: insertando tipo danos');

                    }
                    if (Ti.App.deployType != 'production') console.log('ingresando: tipo_accion (' + elemento.accion.length + ')', {});
                    if (elemento.accion && elemento.accion.length) {
                        var elemento_accion = elemento.accion;
                        var ID_767151531_m = Alloy.Collections.tipo_accion;
                        var db_ID_767151531 = Ti.Database.open(ID_767151531_m.config.adapter.db_name);
                        db_ID_767151531.execute('BEGIN');
                        _.each(elemento_accion, function(ID_767151531_fila, pos) {
                            db_ID_767151531.execute('INSERT INTO tipo_accion (nombre, id_tipo_dano, fecha, id_server, pais_texto, id_segured, pais) VALUES (?,?,?,?,?,?,?)', ID_767151531_fila.valor, ID_767151531_fila.id_tipo_dano, ID_767151531_fila.fecha, ID_767151531_fila.id, ID_767151531_fila.pais_texto, ID_767151531_fila.id_segured, ID_767151531_fila.pais);
                        });
                        db_ID_767151531.execute('COMMIT');
                        db_ID_767151531.close();
                        db_ID_767151531 = null;
                        ID_767151531_m.trigger('change');
                        $.ID_908004102.setText('status: insertando tipo accion');

                    }
                    if (Ti.App.deployType != 'production') console.log('ingresando: unidad_medida (' + elemento.unidad_medida.length + ')', {});
                    if (elemento.unidad_medida && elemento.unidad_medida.length) {
                        var elemento_unidad_medida = elemento.unidad_medida;
                        var ID_49749114_m = Alloy.Collections.unidad_medida;
                        var db_ID_49749114 = Ti.Database.open(ID_49749114_m.config.adapter.db_name);
                        db_ID_49749114.execute('BEGIN');
                        _.each(elemento_unidad_medida, function(ID_49749114_fila, pos) {
                            db_ID_49749114.execute('INSERT INTO unidad_medida (id_accion, nombre, fecha, id_server, pais_texto, id_segured, pais, abrev) VALUES (?,?,?,?,?,?,?,?)', ID_49749114_fila.id_accion, ID_49749114_fila.valor, ID_49749114_fila.fecha, ID_49749114_fila.id, ID_49749114_fila.pais_texto, ID_49749114_fila.id_segured, ID_49749114_fila.pais, ID_49749114_fila.valor_s);
                        });
                        db_ID_49749114.execute('COMMIT');
                        db_ID_49749114.close();
                        db_ID_49749114 = null;
                        ID_49749114_m.trigger('change');
                        $.ID_908004102.setText('status: insertando unidades de medida');

                    }
                    if (Ti.App.deployType != 'production') console.log('ingresando: medidas_seguridad (' + elemento.medidas_seguridad.length + ')', {});
                    if (elemento.medidas_seguridad && elemento.medidas_seguridad.length) {
                        var elemento_medidas_seguridad = elemento.medidas_seguridad;
                        var ID_88889560_m = Alloy.Collections.medidas_seguridad;
                        var db_ID_88889560 = Ti.Database.open(ID_88889560_m.config.adapter.db_name);
                        db_ID_88889560.execute('BEGIN');
                        _.each(elemento_medidas_seguridad, function(ID_88889560_fila, pos) {
                            db_ID_88889560.execute('INSERT INTO medidas_seguridad (nombre, fecha, pais_texto, id_server, id_segured, pais) VALUES (?,?,?,?,?,?)', ID_88889560_fila.valor, ID_88889560_fila.fecha, ID_88889560_fila.pais_texto, ID_88889560_fila.id, ID_88889560_fila.id_segured, ID_88889560_fila.pais);
                        });
                        db_ID_88889560.execute('COMMIT');
                        db_ID_88889560.close();
                        db_ID_88889560 = null;
                        ID_88889560_m.trigger('change');
                        $.ID_908004102.setText('status: insertando medidas de seguridad');

                    }
                    /** 
                     * Volvemos a mostrar los botones presentes en el login 
                     */
                    var ID_1916611841 = null;
                    if ('mostrar_botones' in require('funciones')) {
                        ID_1916611841 = require('funciones').mostrar_botones({});
                    } else {
                        try {
                            ID_1916611841 = f_mostrar_botones({});
                        } catch (ee) {}
                    }
                    /** 
                     * Detenemos la animacion de carga en el boton de login 
                     */

                    $.ID_1755668974.detener_progreso({});
                    /** 
                     * Enviamos a la pantalla de menu principal 
                     */
                    Alloy.createController("menu_index", {}).getView().open();
                } else {
                    var ID_204306486 = null;
                    if ('mostrar_botones' in require('funciones')) {
                        ID_204306486 = require('funciones').mostrar_botones({});
                    } else {
                        try {
                            ID_204306486 = f_mostrar_botones({});
                        } catch (ee) {}
                    }
                    var ID_1578755556_opts = ['Aceptar'];
                    var ID_1578755556 = Ti.UI.createAlertDialog({
                        title: 'Alerta',
                        message: '' + 'Error ' + elemento.mensaje + '',
                        buttonNames: ID_1578755556_opts
                    });
                    ID_1578755556.addEventListener('click', function(e) {
                        var res = ID_1578755556_opts[e.index];
                        res = null;

                        e.source.removeEventListener("click", arguments.callee);
                    });
                    ID_1578755556.show();
                    $.ID_908004102.setText('status: esperando');

                }
                elemento = null, valor = null;
            };

            ID_973615828.error = function(e) {
                var elemento = e,
                    valor = e;
                var ID_1229908354 = null;
                if ('mostrar_botones' in require('funciones')) {
                    ID_1229908354 = require('funciones').mostrar_botones({});
                } else {
                    try {
                        ID_1229908354 = f_mostrar_botones({});
                    } catch (ee) {}
                }
                var ID_1769670359_opts = ['Aceptar'];
                var ID_1769670359 = Ti.UI.createAlertDialog({
                    title: 'Alerta',
                    message: 'Problema de conexion por favor intentar despues',
                    buttonNames: ID_1769670359_opts
                });
                ID_1769670359.addEventListener('click', function(e) {
                    var suu = ID_1769670359_opts[e.index];
                    suu = null;

                    e.source.removeEventListener("click", arguments.callee);
                });
                ID_1769670359.show();
                if (Ti.App.deployType != 'production') console.log('error selectores', {
                    "asd": elemento
                });
                $.ID_908004102.setText('status: esperando');

                elemento = null, valor = null;
            };
            require('helper').ajaxUnico('ID_973615828', '' + url_server + 'selectores' + '', 'POST', {
                pais: pais[0].nombre,
                destino: selectores_fechas.destino.fecha,
                entidad_financiera: selectores_fechas.entidad_financiera.fecha,
                compania: selectores_fechas.compania.fecha,
                tipo_siniestro: selectores_fechas.tipo_siniestro.fecha,
                estructura_soportante: selectores_fechas.estructura_soportante.fecha,
                muros_tabiques: selectores_fechas.muros_tabiques.fecha,
                entrepisos: selectores_fechas.entrepisos.fecha,
                pavimento: selectores_fechas.pavimento.fecha,
                estructura_cubierta: selectores_fechas.estructura_cubierta.fecha,
                cubierta: selectores_fechas.cubierta.fecha,
                partida: selectores_fechas.partida.fecha,
                bienes: selectores_fechas.bienes.fecha,
                monedas: selectores_fechas.monedas.fecha,
                marcas: selectores_fechas.marcas.fecha,
                medidas_seguridad: selectores_fechas.medidas_seguridad.fecha,
                recintos: '',
                accion: '',
                unidad_medida: ''
            }, 15000, ID_973615828);
            selectores_fechas = null;
        } else {
            var ID_43157519_i = Alloy.createCollection('pais');
            var ID_43157519_i_where = '';
            ID_43157519_i.fetch();
            var paises = require('helper').query2array(ID_43157519_i);
            paises = null;
        }
        pais = null;
    }
    inspector = null;
    return null;
};
var f_mostrar_botones = function(x_params) {
    /** 
     * Mostramos el boton registrar 
     */
    var ID_1632570648_visible = true;

    if (ID_1632570648_visible == 'si') {
        ID_1632570648_visible = true;
    } else if (ID_1632570648_visible == 'no') {
        ID_1632570648_visible = false;
    }
    $.ID_1632570648.setVisible(ID_1632570648_visible);

    /** 
     * Detenemos animacion progreso de registro 
     */
    $.ID_1843435775.hide();
    /** 
     * Mostramos boton entrar 
     */

    $.ID_1755668974.detener_progreso({});
    return null;
};
var f_ocultar_botones = function(x_params) {
    /** 
     * Ocultamos boton registrar 
     */
    var ID_1632570648_visible = false;

    if (ID_1632570648_visible == 'si') {
        ID_1632570648_visible = true;
    } else if (ID_1632570648_visible == 'no') {
        ID_1632570648_visible = false;
    }
    $.ID_1632570648.setVisible(ID_1632570648_visible);

    /** 
     * Iniciamos animacion progreso de registro 
     */
    $.ID_1843435775.show();
    /** 
     * Ocultamos boton entrar 
     */

    $.ID_1755668974.iniciar_progreso({});
    return null;
};

if (OS_IOS || OS_ANDROID) {
    $.ID_365517799.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}
$.ID_365517799.open();