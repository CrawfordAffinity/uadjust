var _bind4section = {};
var _list_templates = {};

var _activity;
if (OS_ANDROID) {
    _activity = $.ID_1423732331.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_1423732331';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_1423732331.addEventListener('open', function(e) {});
}
$.ID_1423732331.orientationModes = [Titanium.UI.PORTRAIT];


$.ID_1996257419.init({
    titulo: 'DISPONIBILIDAD',
    __id: 'ALL1996257419',
    textoderecha: 'Siguiente',
    fondo: 'fondoblanco',
    colortitulo: 'negro',
    modal: '',
    onpresiono: Presiono_ID_964043030,
    colortextoderecha: 'azul'
});

function Presiono_ID_964043030(e) {

    var evento = e;
    /* Revisamos cual fue la disponibilidad del inspector, dependiendo de eso es si pasamos a la proxima pantalla o enviamos informacion al servidor */
    var seleccionado = ('seleccionado' in require('vars')) ? require('vars')['seleccionado'] : '';
    if (seleccionado == 1 || seleccionado == '1') {
        if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
            /* Revisamos que haya conexion a internet, recuperamos las variables de la disponibilidad de salir de la ciudad y pais, la url de uadjust y el detalle del inspector. */
            var fueraciudad = ('fueraciudad' in require('vars')) ? require('vars')['fueraciudad'] : '';
            var fuerapais = ('fuerapais' in require('vars')) ? require('vars')['fuerapais'] : '';
            var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
            var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
            /* Llenamos los datos requeridos por el servidor y avisamos que tiene disponibilidad full time y marcamos todos los dias como disponibles */
            var ID_552443566 = {};
            console.log('DEBUG WEB: requesting url:' + url_server + 'editarPerfilTipo2' + ' with data:', {
                _method: 'POST',
                _params: {
                    tipo_cambio: 2,
                    id_inspector: inspector.id_server,
                    disponibilidad_viajar_pais: fuerapais,
                    disponibilidad_viajar_ciudad: fueraciudad,
                    d1: 1,
                    d2: 1,
                    d3: 1,
                    d4: 1,
                    d5: 1,
                    d6: 1,
                    d7: 1,
                    disponibilidad: 1,
                    disponibilidad_horas: inspector.disponibilidad_horas
                },
                _timeout: '15000'
            });

            ID_552443566.success = function(e) {
                var elemento = e,
                    valor = e;
                if (Ti.App.deployType != 'production') console.log('Resultado consulta perfiltipo2', {
                    "elemento": elemento
                });
                if (elemento.error == 0 || elemento.error == '0') {
                    /* Modificamos la tabla con la disponibilidad fulltime */
                    var ID_1268933721_i = Alloy.createCollection('inspectores');
                    var ID_1268933721_i_where = '';
                    ID_1268933721_i.fetch();
                    var inspector_list = require('helper').query2array(ID_1268933721_i);
                    var fueraciudad = ('fueraciudad' in require('vars')) ? require('vars')['fueraciudad'] : '';
                    var fuerapais = ('fuerapais' in require('vars')) ? require('vars')['fuerapais'] : '';
                    var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
                    var db = Ti.Database.open(ID_1268933721_i.config.adapter.db_name);
                    if (ID_1268933721_i_where == '') {
                        var sql = 'UPDATE ' + ID_1268933721_i.config.adapter.collection_name + ' SET disponibilidad_viajar_pais=\'' + fuerapais + '\'';
                    } else {
                        var sql = 'UPDATE ' + ID_1268933721_i.config.adapter.collection_name + ' SET disponibilidad_viajar_pais=\'' + fuerapais + '\' WHERE ' + ID_1268933721_i_where;
                    }
                    db.execute(sql);
                    if (ID_1268933721_i_where == '') {
                        var sql = 'UPDATE ' + ID_1268933721_i.config.adapter.collection_name + ' SET disponibilidad_viajar_ciudad=\'' + fueraciudad + '\'';
                    } else {
                        var sql = 'UPDATE ' + ID_1268933721_i.config.adapter.collection_name + ' SET disponibilidad_viajar_ciudad=\'' + fueraciudad + '\' WHERE ' + ID_1268933721_i_where;
                    }
                    db.execute(sql);
                    if (ID_1268933721_i_where == '') {
                        var sql = 'UPDATE ' + ID_1268933721_i.config.adapter.collection_name + ' SET disponibilidad=\'' + 1 + '\'';
                    } else {
                        var sql = 'UPDATE ' + ID_1268933721_i.config.adapter.collection_name + ' SET disponibilidad=\'' + 1 + '\' WHERE ' + ID_1268933721_i_where;
                    }
                    db.execute(sql);
                    if (ID_1268933721_i_where == '') {
                        var sql = 'UPDATE ' + ID_1268933721_i.config.adapter.collection_name + ' SET d1=\'' + 1 + '\'';
                    } else {
                        var sql = 'UPDATE ' + ID_1268933721_i.config.adapter.collection_name + ' SET d1=\'' + 1 + '\' WHERE ' + ID_1268933721_i_where;
                    }
                    db.execute(sql);
                    if (ID_1268933721_i_where == '') {
                        var sql = 'UPDATE ' + ID_1268933721_i.config.adapter.collection_name + ' SET d2=\'' + 1 + '\'';
                    } else {
                        var sql = 'UPDATE ' + ID_1268933721_i.config.adapter.collection_name + ' SET d2=\'' + 1 + '\' WHERE ' + ID_1268933721_i_where;
                    }
                    db.execute(sql);
                    if (ID_1268933721_i_where == '') {
                        var sql = 'UPDATE ' + ID_1268933721_i.config.adapter.collection_name + ' SET d3=\'' + 1 + '\'';
                    } else {
                        var sql = 'UPDATE ' + ID_1268933721_i.config.adapter.collection_name + ' SET d3=\'' + 1 + '\' WHERE ' + ID_1268933721_i_where;
                    }
                    db.execute(sql);
                    if (ID_1268933721_i_where == '') {
                        var sql = 'UPDATE ' + ID_1268933721_i.config.adapter.collection_name + ' SET d4=\'' + 1 + '\'';
                    } else {
                        var sql = 'UPDATE ' + ID_1268933721_i.config.adapter.collection_name + ' SET d4=\'' + 1 + '\' WHERE ' + ID_1268933721_i_where;
                    }
                    db.execute(sql);
                    if (ID_1268933721_i_where == '') {
                        var sql = 'UPDATE ' + ID_1268933721_i.config.adapter.collection_name + ' SET d5=\'' + 1 + '\'';
                    } else {
                        var sql = 'UPDATE ' + ID_1268933721_i.config.adapter.collection_name + ' SET d5=\'' + 1 + '\' WHERE ' + ID_1268933721_i_where;
                    }
                    db.execute(sql);
                    if (ID_1268933721_i_where == '') {
                        var sql = 'UPDATE ' + ID_1268933721_i.config.adapter.collection_name + ' SET d6=\'' + 1 + '\'';
                    } else {
                        var sql = 'UPDATE ' + ID_1268933721_i.config.adapter.collection_name + ' SET d6=\'' + 1 + '\' WHERE ' + ID_1268933721_i_where;
                    }
                    db.execute(sql);
                    if (ID_1268933721_i_where == '') {
                        var sql = 'UPDATE ' + ID_1268933721_i.config.adapter.collection_name + ' SET d7=\'' + 1 + '\'';
                    } else {
                        var sql = 'UPDATE ' + ID_1268933721_i.config.adapter.collection_name + ' SET d7=\'' + 1 + '\' WHERE ' + ID_1268933721_i_where;
                    }
                    db.execute(sql);
                    db.close();
                    /* Actualizamos la variable inspector con los datos recien ingresados */
                    var ID_152353124_i = Alloy.createCollection('inspectores');
                    var ID_152353124_i_where = '';
                    ID_152353124_i.fetch();
                    var inspector_list = require('helper').query2array(ID_152353124_i);
                    require('vars')['inspector'] = inspector_list[0];
                    /* Limpiamos memoria */
                    inspector_list = null;
                    /* Cerramos la pantalla */
                    $.ID_1423732331.close();
                } else {
                    var ID_363307995_opts = ['Aceptar'];
                    var ID_363307995 = Ti.UI.createAlertDialog({
                        title: 'Atencion',
                        message: 'Error **elemento.mensaje**',
                        buttonNames: ID_363307995_opts
                    });
                    ID_363307995.addEventListener('click', function(e) {
                        var abcx = ID_363307995_opts[e.index];
                        abcx = null;

                        e.source.removeEventListener("click", arguments.callee);
                    });
                    ID_363307995.show();
                }
                elemento = null, valor = null;
            };

            ID_552443566.error = function(e) {
                var elemento = e,
                    valor = e;
                if (Ti.App.deployType != 'production') console.log('Hubo un error', {
                    "elemento": elemento
                });
                elemento = null, valor = null;
            };
            require('helper').ajaxUnico('ID_552443566', '' + url_server + 'editarPerfilTipo2' + '', 'POST', {
                tipo_cambio: 2,
                id_inspector: inspector.id_server,
                disponibilidad_viajar_pais: fuerapais,
                disponibilidad_viajar_ciudad: fueraciudad,
                d1: 1,
                d2: 1,
                d3: 1,
                d4: 1,
                d5: 1,
                d6: 1,
                d7: 1,
                disponibilidad: 1,
                disponibilidad_horas: inspector.disponibilidad_horas
            }, 15000, ID_552443566);
        } else {
            var ID_252683259_opts = ['Aceptar'];
            var ID_252683259 = Ti.UI.createAlertDialog({
                title: 'No hay internet',
                message: 'No se pueden efectuar los cambios sin conexion.',
                buttonNames: ID_252683259_opts
            });
            ID_252683259.addEventListener('click', function(e) {
                var suu = ID_252683259_opts[e.index];
                suu = null;

                e.source.removeEventListener("click", arguments.callee);
            });
            ID_252683259.show();
        }
    } else {
        /* Creamos un flag para evitar que la pantalla se abra dos veces */
        var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
        if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
            require('vars')['var_abriendo'] = 'true';
            Alloy.createController("editar_parttime", {}).getView().open();
            $.ID_1423732331.close();
        }
    }
}

$.ID_1222663545.init({
    titulo: 'Editar Disponibilidad',
    __id: 'ALL1222663545',
    avance: '',
    onclick: Click_ID_2857115
});

function Click_ID_2857115(e) {

    var evento = e;

}


function Change_ID_1746789656(e) {

    e.cancelBubble = true;
    var elemento = e;
    var _columna = e.columnIndex;
    var columna = e.columnIndex + 1;
    var _fila = e.rowIndex;
    var fila = e.rowIndex + 1;
    if (Ti.App.deployType != 'production') console.log('seleccionado', {
        "fila": _fila
    });
    require('vars')['seleccionado'] = _fila;
    if (_fila == 0 || _fila == '0') {
        /* Dependiendo de la disponibilidad del inspector, cambiamos el texto de la barra (para saber si mandamos informacion en esta misma pantalla al servidor, o tiene que escoger los dias que tiene disponibles) */

        $.ID_1996257419.update({
            texto: 'Siguiente'
        });
    } else {

        $.ID_1996257419.update({
            texto: 'Guardar'
        });
    }
}

function Change_ID_31855985(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    if (elemento == true || elemento == 'true') {
        /* Cambiamos el detalle de la variable de fuera ciudad */
        $.ID_194255918.setText('SI');

        require('vars')['fueraciudad'] = '1';
    } else {
        $.ID_194255918.setText('NO');

        require('vars')['fueraciudad'] = '0';
    }
    elemento = null;

}

function Change_ID_725652639(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    if (elemento == true || elemento == 'true') {
        /* Cambiamos el detalle de la variable de fuera pais */
        $.ID_1019357907.setText('SI');

        require('vars')['fuerapais'] = '1';
    } else {
        $.ID_1019357907.setText('NO');

        require('vars')['fuerapais'] = '0';
    }
    elemento = null;

}

(function() {
    /* Recuperamos variable del inspector para saber detalle de la seleccion de disponibilidad (part o full time), si tiene disponibilidad de viajar fuera de la ciudad, pais y modificamos los datos en la pantalla */
    var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
    require('vars')['seleccionado'] = inspector.disponibilidad;
    require('vars')['fueraciudad'] = inspector.disponibilidad_viajar_ciudad;
    if (inspector.disponibilidad_viajar_ciudad == 1 || inspector.disponibilidad_viajar_ciudad == '1') {
        $.ID_1568773770.setValue(true);

        $.ID_194255918.setText('SI');

    } else {
        $.ID_1568773770.setValue('false');

        $.ID_194255918.setText('NO');

    }
    require('vars')['fuerapais'] = inspector.disponibilidad_viajar_pais;
    if (inspector.disponibilidad_viajar_pais == 1 || inspector.disponibilidad_viajar_pais == '1') {
        $.ID_952608080.setValue(true);

        $.ID_1019357907.setText('SI');

    } else {
        $.ID_952608080.setValue('false');

        $.ID_1019357907.setText('NO');

    }
    if (inspector.disponibilidad == 0 || inspector.disponibilidad == '0') {
        /* Dependiendo de la disponibilidad del inspector, cambiamos el texto de la barra (para saber si mandamos informacion en esta misma pantalla al servidor, o tiene que escoger los dias que tiene disponibles) */

        $.ID_1996257419.update({
            texto: 'Siguiente'
        });
        if (Ti.App.deployType != 'production') console.log('disponibilidad es cero', {});
    } else {

        $.ID_1996257419.update({
            texto: 'Guardar'
        });
        if (Ti.App.deployType != 'production') console.log('disponibilidad es 1', {});
    } /* Obtenemos el picker para poder modificar la disponibilidad del inspector */
    var picker;
    picker = $.ID_873112354;
    picker.setSelectedRow(0, inspector.disponibilidad);
})();

function Postlayout_ID_1156648359(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var ID_1294585966_func = function() {
        /* Una vez ya cargada la tarea, dejamos la variable limpia para poder abrir otras pantallas */
        require('vars')['var_abriendo'] = '';
    };
    var ID_1294585966 = setTimeout(ID_1294585966_func, 1000 * 0.2);

}
if (OS_IOS || OS_ANDROID) {
    $.ID_1423732331.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}
//$.ID_1423732331.open();