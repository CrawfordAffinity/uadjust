var _bind4section = {};
var _list_templates = {
    "elemento": {
        "ID_432412973": {},
        "ID_291370846": {
            "text": "{nombre}"
        },
        "ID_1925487537": {
            "text": "{id_server}"
        },
        "ID_975786586": {
            "text": "{id_segured}"
        },
        "ID_1546190443": {}
    },
    "dano": {
        "ID_1569764493": {
            "text": "{id}"
        },
        "ID_304218376": {},
        "ID_1912165224": {
            "text": "{nombre}"
        }
    },
    "pborrar": {
        "ID_1236907100": {
            "text": "{id}"
        },
        "ID_1506538078": {},
        "ID_2024360383": {},
        "ID_1765564351": {},
        "ID_1699280427": {
            "text": "{nombre}"
        },
        "ID_1445903745": {},
        "ID_1795050796": {},
        "ID_1752901998": {},
        "ID_1914367022": {
            "text": "{nombre}"
        },
        "ID_1249074865": {
            "text": "{id}"
        },
        "ID_1131557960": {},
        "ID_1361874287": {},
        "ID_1750057271": {},
        "ID_1962135207": {},
        "ID_1947166298": {
            "text": "{nombre}"
        },
        "ID_2008605730": {
            "text": "{id}"
        },
        "ID_1788775425": {},
        "ID_1246915111": {}
    },
    "contenido": {
        "ID_630800887": {},
        "ID_862352109": {
            "text": "{id}"
        },
        "ID_560452274": {
            "text": "{nombre}"
        }
    },
    "recinto": {
        "ID_1960582596": {},
        "ID_871581291": {
            "text": "{nombre}"
        },
        "ID_310297848": {
            "text": "{id}"
        }
    },
    "nivel": {
        "ID_1021786928": {
            "text": "{id}"
        },
        "ID_1241655463": {
            "text": "{nombre}"
        },
        "ID_599581692": {}
    },
    "tareas_mistareas": {
        "ID_1575074457": {},
        "ID_1503294346": {
            "text": "{comuna}"
        },
        "ID_1152838109": {
            "idlocal": "{idlocal}"
        },
        "ID_1205965249": {
            "text": "a {distancia} km"
        },
        "ID_1729551944": {},
        "ID_1201332772": {},
        "ID_2063494569": {},
        "ID_1272820672": {
            "text": "{ciudad}, {pais}"
        },
        "ID_1138222879": {},
        "ID_1048592298": {},
        "ID_1695073981": {},
        "ID_2134521582": {},
        "ID_1071163621": {},
        "ID_1299156409": {
            "text": "{direccion}"
        },
        "ID_1676413215": {
            "visible": "{seguirvisible}"
        },
        "ID_1629587369": {}
    }
};

var _activity;
if (OS_ANDROID) {
    _activity = $.ID_304530978.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_304530978';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_304530978.addEventListener('open', function(e) {});
}
$.ID_304530978.orientationModes = [Titanium.UI.PORTRAIT];

function Load_ID_1879664608(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var evento = e;
    /** 
     * Elemento es el objeto que llamo el evento, como es virtual no hay referencia de nodo para un modificar 
     */
    elemento.start();

}

function Itemclick_ID_1224423571(e) {

    e.cancelBubble = true;
    var objeto = e.section.getItemAt(e.itemIndex);
    var modelo = {},
        _modelo = [];
    var fila = {},
        fila_bak = {},
        info = {
            _template: objeto.template,
            _what: [],
            _seccion_ref: e.section.getHeaderTitle(),
            _model_id: -1
        },
        _tmp = {
            objmap: {}
        };
    if ('itemId' in e) {
        info._model_id = e.itemId;
        modelo._id = info._model_id;
        if (info._seccion_ref != '' && info._seccion_ref in _bind4section) {
            modelo._collection = _bind4section[info._seccion_ref];
            _tmp._coll = modelo._collection;
        }
    }
    if (info._seccion_ref != '' && info._seccion_ref in _bind4section) {
        if (Alloy.Collections[_tmp._coll].config.adapter.type == 'sql') {
            _tmp._inst = Alloy.Collections[_tmp._coll];
            _tmp._id = Alloy.Collections[_tmp._coll].config.adapter.idAttribute;
            _tmp._dbsql = 'SELECT * FROM ' + Alloy.Collections[_tmp._coll].config.adapter.collection_name + ' WHERE ' + _tmp._id + ' = ' + e.itemId;
            _tmp._db = Ti.Database.open(Alloy.Collections[_tmp._coll].config.adapter.db_name);
            _modelo = _tmp._db.execute(_tmp._dbsql);
            var values = [],
                fieldNames = [];
            var fieldCount = _.isFunction(_modelo.fieldCount) ? _modelo.fieldCount() : _modelo.fieldCount;
            var getField = _modelo.field;
            var i = 0;
            for (; fieldCount > i; i++) fieldNames.push(_modelo.fieldName(i));
            while (_modelo.isValidRow()) {
                var o = {};
                for (i = 0; fieldCount > i; i++) o[fieldNames[i]] = getField(i);
                values.push(o);
                _modelo.next();
            }
            _modelo = values;
            _tmp._db.close();
        } else {
            _tmp._search = {};
            _tmp._search[_tmp._id] = e.itemId + '';
            _modelo = Alloy.Collections[_tmp._coll].fetch(_tmp._search);
        }
    }
    var findVariables = require('fvariables');
    _.each(_list_templates[info._template], function(obj_id, id) {
        _.each(obj_id, function(valor, prop) {
            var llaves = findVariables(valor, '{', '}');
            _.each(llaves, function(llave) {
                _tmp.objmap[llave] = {
                    id: id,
                    prop: prop
                };
                fila[llave] = objeto[id][prop];
                if (id == e.bindId) info._what.push(llave);
            });
        });
    });
    info._what = info._what.join(',');
    fila_bak = JSON.parse(JSON.stringify(fila));
    /** 
     * Usamos este flag para evitar que la pantalla se abra en mas de una oportunidad 
     */
    var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
    if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
        require('vars')['var_abriendo'] = 'true';
        var nulo = Alloy.createController("detalletarea_index", {
            '_id': fila.idlocal,
            '__master_model': (typeof modelo !== 'undefined') ? modelo : {},
            '__modelo': (typeof _modelo !== 'undefined' && _modelo.length > 0) ? _modelo[0] : {}
        }).getView();
        nulo.open({
            modal: true
        });

        nulo = null;
    }
    _tmp.changed = false;
    _tmp.diff_keys = [];
    _.each(fila, function(value1, prop) {
        var had_samekey = false;
        _.each(fila_bak, function(value2, prop2) {
            if (prop == prop2 && value1 == value2) {
                had_samekey = true;
            } else if (!_.has(fila_bak, prop) || !_.has(fila, prop2)) {
                has_samekey = true;
            }
        });
        if (!had_samekey) _tmp.diff_keys.push(prop);
    });
    if (_tmp.diff_keys.length > 0) _tmp.changed = true;
    if (_tmp.changed == true) {
        _.each(_tmp.diff_keys, function(llave) {
            objeto[_tmp.objmap[llave].id][_tmp.objmap[llave].prop] = fila[llave];
        });
        e.section.updateItemAt(e.itemIndex, objeto);
    }

}

(function() {

    _my_events['_refrescar_tareas_mistareas,ID_199664348'] = function(evento) {

        var ID_521921285_trycatch = {
            error: function(e) {
                if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
            }
        };
        try {
            ID_521921285_trycatch.error = function(evento) {
                if (Ti.App.deployType != 'production') console.log('error refrescando mis tareas (pantalla mistareas)', {});
            };
            var inspeccion_encurso = ('inspeccion_encurso' in require('vars')) ? require('vars')['inspeccion_encurso'] : '';
            if (inspeccion_encurso == false || inspeccion_encurso == 'false') {
                /** 
                 * Si no hay inspeccion en curso, limpiamos la lista de tareas, crear secciones para hoy, manana y otro dia. Consultamos a la tabla e ingresamos las tareas segun fecha 
                 */
                var ID_308810445_borrar = false;

                var limpiarListado = function(animar) {
                    var a_nimar = (typeof animar == 'undefined') ? false : animar;
                    if (OS_IOS && a_nimar == true) {
                        var s_ecciones = $.ID_308810445.getSections();
                        _.each(s_ecciones, function(obj_id, pos) {
                            $.ID_308810445.deleteSectionAt(0, {
                                animated: true
                            });
                        });
                    } else {
                        $.ID_308810445.setSections([]);
                    }
                };
                limpiarListado(ID_308810445_borrar);

                var ID_784741595 = Titanium.UI.createListSection({
                    headerTitle: 'hoy'
                });
                var ID_1009847172 = Titanium.UI.createView({
                    height: '25dp',
                    width: Ti.UI.FILL
                });
                var ID_1453353504 = Titanium.UI.createView({
                    height: '25dp',
                    layout: 'composite',
                    width: Ti.UI.FILL,
                    backgroundColor: '#F7F7F7'
                });
                var ID_620812511 = Titanium.UI.createLabel({
                    text: 'PARA HOY',
                    color: '#999999',
                    touchEnabled: false,
                    font: {
                        fontFamily: 'Roboto-Medium',
                        fontSize: '14dp'
                    }

                });
                ID_1453353504.add(ID_620812511);

                ID_1009847172.add(ID_1453353504);
                ID_784741595.setHeaderView(ID_1009847172);
                $.ID_308810445.appendSection(ID_784741595);
                var ID_1264274795 = Titanium.UI.createListSection({
                    headerTitle: 'manana'
                });
                var ID_285270039 = Titanium.UI.createView({
                    height: '25dp',
                    width: Ti.UI.FILL
                });
                var ID_1152615331 = Titanium.UI.createView({
                    height: '25dp',
                    layout: 'composite',
                    width: Ti.UI.FILL,
                    backgroundColor: '#F7F7F7'
                });
                var ID_1204908311 = Titanium.UI.createLabel({
                    text: 'MAÑANA',
                    color: '#999999',
                    touchEnabled: false,
                    font: {
                        fontFamily: 'Roboto-Medium',
                        fontSize: '14dp'
                    }

                });
                ID_1152615331.add(ID_1204908311);

                ID_285270039.add(ID_1152615331);
                ID_1264274795.setHeaderView(ID_285270039);
                $.ID_308810445.appendSection(ID_1264274795);
                var ID_378876316_i = Alloy.createCollection('tareas');
                var ID_378876316_i_where = 'ORDER BY FECHA_TAREA ASC';
                ID_378876316_i.fetch({
                    query: 'SELECT * FROM tareas ORDER BY FECHA_TAREA ASC'
                });
                var tareas = require('helper').query2array(ID_378876316_i);
                /** 
                 * Inicializamos la variable de ultima fecha en vacio para usarla en el recorrido de tareas 
                 */
                require('vars')['ultima_fecha'] = '';
                /** 
                 * Formateamos fecha de hoy y manana para cargar la tarea dependiendo de la fecha de realizacion 
                 */
                var moment = require('alloy/moment');
                var fecha_hoy = moment(new Date()).format('YYYY-MM-DD');
                var manana = new Date();
                manana.setDate(manana.getDate() + 1);;
                var moment = require('alloy/moment');
                var ID_348062720 = manana;
                var fecha_manana = moment(ID_348062720).format('YYYY-MM-DD');
                var tarea_index = 0;
                _.each(tareas, function(tarea, tarea_pos, tarea_list) {
                    tarea_index += 1;
                    if (tarea.estado_tarea == 4) {
                        /** 
                         * Si estado_tarea es 4 seteamos variable para enviar_ubicacion de inspector 
                         */
                        require('vars')['seguir_tarea'] = tarea.id_server;
                    }
                    /** 
                     * Sector de otras fechas 
                     */
                    var ultima_fecha = ('ultima_fecha' in require('vars')) ? require('vars')['ultima_fecha'] : '';
                    if (ultima_fecha != tarea.fecha_tarea) {
                        /** 
                         * Generamos una seccion nueva para las fechas que no sean de hoy, ni de manana 
                         */
                        if (tarea.fecha_tarea == fecha_hoy) {} else if (tarea.fecha_tarea == fecha_manana) {} else {
                            var formatfecha = ('formatfecha' in require('vars')) ? require('vars')['formatfecha'] : '';
                            var moment = require('alloy/moment');
                            var ID_1065996523 = tarea.fecha_tarea;
                            if (typeof ID_1065996523 === 'string' || typeof ID_1065996523 === 'number') {
                                var fecha_titulo = moment(ID_1065996523, 'YYYY-MM-DD').format('DD//MM/YYYY');
                            } else {
                                var fecha_titulo = moment(ID_1065996523).format('DD//MM/YYYY');
                            }
                            var ID_600083325 = Titanium.UI.createListSection({
                                headerTitle: 'fecha'
                            });
                            var ID_740235814 = Titanium.UI.createView({
                                height: '25dp',
                                width: Ti.UI.FILL
                            });
                            var ID_1205916566 = Titanium.UI.createView({
                                height: '25dp',
                                layout: 'composite',
                                width: Ti.UI.FILL,
                                backgroundColor: '#F7F7F7'
                            });
                            var ID_786775786 = Titanium.UI.createLabel({
                                text: fecha_titulo,
                                color: '#999999',
                                touchEnabled: false,
                                font: {
                                    fontFamily: 'Roboto-Medium',
                                    fontSize: '14dp'
                                }

                            });
                            ID_1205916566.add(ID_786775786);

                            ID_740235814.add(ID_1205916566);
                            ID_600083325.setHeaderView(ID_740235814);
                            $.ID_308810445.appendSection(ID_600083325);
                        }
                        /** 
                         * Actualizamos la variable con&#160;&#160;la ultima fecha de la tarea 
                         */
                        require('vars')['ultima_fecha'] = tarea.fecha_tarea;
                    }
                    if ((_.isObject(tarea.nivel_2) || _.isString(tarea.nivel_2)) && _.isEmpty(tarea.nivel_2)) {
                        /** 
                         * Revisamos cual es el ultimo nivel disponible y lo fijamos dentro de la variable tarea como ultimo_nivel 
                         */
                        var tarea = _.extend(tarea, {
                            ultimo_nivel: tarea.nivel_1
                        });
                    } else if ((_.isObject(tarea.nivel_3) || _.isString(tarea.nivel_3)) && _.isEmpty(tarea.nivel_3)) {
                        var tarea = _.extend(tarea, {
                            ultimo_nivel: tarea.nivel_2
                        });
                    } else if ((_.isObject(tarea.nivel_4) || _.isString(tarea.nivel_4)) && _.isEmpty(tarea.nivel_4)) {
                        var tarea = _.extend(tarea, {
                            ultimo_nivel: tarea.nivel_3
                        });
                    } else if ((_.isObject(tarea.nivel_5) || _.isString(tarea.nivel_5)) && _.isEmpty(tarea.nivel_5)) {
                        var tarea = _.extend(tarea, {
                            ultimo_nivel: tarea.nivel_4
                        });
                    } else {
                        var tarea = _.extend(tarea, {
                            ultimo_nivel: tarea.nivel_5
                        });
                    }
                    if (tarea.estado_tarea == 4) {
                        /** 
                         * Preguntamos si esta en seguimiento (estado es 4) 
                         */
                        require('vars')['seguimiento'] = 'true';
                    } else {
                        require('vars')['seguimiento'] = 'false';
                    }
                    var seguimiento = ('seguimiento' in require('vars')) ? require('vars')['seguimiento'] : '';
                    if (tarea.fecha_tarea == fecha_hoy) {
                        /** 
                         * Dependiendo de la fecha de la tarea, es donde apuntaremos a la seccion que corresponde (segun fecha) 
                         */
                        var ID_514443896 = [{
                            ID_1575074457: {},
                            ID_1503294346: {
                                text: tarea.ultimo_nivel
                            },
                            ID_1152838109: {
                                idlocal: tarea.id
                            },
                            ID_1205965249: {
                                text: 'a ' + tarea.distance + ' km'
                            },
                            ID_1729551944: {},
                            ID_1201332772: {},
                            ID_2063494569: {},
                            ID_1272820672: {
                                text: tarea.nivel_2 + ', ' + tarea.pais
                            },
                            ID_1138222879: {},
                            ID_1048592298: {},
                            ID_1695073981: {},
                            template: 'tareas_mistareas',
                            ID_2134521582: {},
                            ID_1071163621: {},
                            ID_1299156409: {
                                text: tarea.direccion
                            },
                            ID_1676413215: {
                                visible: seguimiento
                            },
                            ID_1629587369: {}

                        }];
                        var ID_514443896_secs = {};
                        _.map($.ID_308810445.getSections(), function(ID_514443896_valor, ID_514443896_indice) {
                            ID_514443896_secs[ID_514443896_valor.getHeaderTitle()] = ID_514443896_indice;
                            return ID_514443896_valor;
                        });
                        if ('hoy' in ID_514443896_secs) {
                            $.ID_308810445.sections[ID_514443896_secs['hoy']].appendItems(ID_514443896);
                        } else {
                            console.log('DEBUG:dynamic listItem points to a invalid section id: check attribute _seccion.');
                        }
                    } else if (tarea.fecha_tarea == fecha_manana) {
                        var ID_1379951202 = [{
                            ID_1575074457: {},
                            ID_1503294346: {
                                text: tarea.ultimo_nivel
                            },
                            ID_1152838109: {
                                idlocal: tarea.id
                            },
                            ID_1205965249: {
                                text: 'a ' + tarea.distance + ' km'
                            },
                            ID_1729551944: {},
                            ID_1201332772: {},
                            ID_2063494569: {},
                            ID_1272820672: {
                                text: tarea.nivel_2 + ', ' + tarea.pais
                            },
                            ID_1138222879: {},
                            ID_1048592298: {},
                            ID_1695073981: {},
                            template: 'tareas_mistareas',
                            ID_2134521582: {},
                            ID_1071163621: {},
                            ID_1299156409: {
                                text: tarea.direccion
                            },
                            ID_1676413215: {
                                visible: seguimiento
                            },
                            ID_1629587369: {}

                        }];
                        var ID_1379951202_secs = {};
                        _.map($.ID_308810445.getSections(), function(ID_1379951202_valor, ID_1379951202_indice) {
                            ID_1379951202_secs[ID_1379951202_valor.getHeaderTitle()] = ID_1379951202_indice;
                            return ID_1379951202_valor;
                        });
                        if ('manana' in ID_1379951202_secs) {
                            $.ID_308810445.sections[ID_1379951202_secs['manana']].appendItems(ID_1379951202);
                        } else {
                            console.log('DEBUG:dynamic listItem points to a invalid section id: check attribute _seccion.');
                        }
                    } else {
                        var ID_723947171 = [{
                            ID_1575074457: {},
                            ID_1503294346: {
                                text: tarea.ultimo_nivel
                            },
                            ID_1152838109: {
                                idlocal: tarea.id
                            },
                            ID_1205965249: {
                                text: 'a ' + tarea.distance + ' km'
                            },
                            ID_1729551944: {},
                            ID_1201332772: {},
                            ID_2063494569: {},
                            ID_1272820672: {
                                text: tarea.nivel_2 + ', ' + tarea.pais
                            },
                            ID_1138222879: {},
                            ID_1048592298: {},
                            ID_1695073981: {},
                            template: 'tareas_mistareas',
                            ID_2134521582: {},
                            ID_1071163621: {},
                            ID_1299156409: {
                                text: tarea.direccion
                            },
                            ID_1676413215: {
                                visible: seguimiento
                            },
                            ID_1629587369: {}

                        }];
                        var ID_723947171_secs = {};
                        _.map($.ID_308810445.getSections(), function(ID_723947171_valor, ID_723947171_indice) {
                            ID_723947171_secs[ID_723947171_valor.getHeaderTitle()] = ID_723947171_indice;
                            return ID_723947171_valor;
                        });
                        if ('fecha' in ID_723947171_secs) {
                            $.ID_308810445.sections[ID_723947171_secs['fecha']].appendItems(ID_723947171);
                        } else {
                            console.log('DEBUG:dynamic listItem points to a invalid section id: check attribute _seccion.');
                        }
                    }
                });
                /** 
                 * Limpieza memoria 
                 */
                fecha_hoy = null, manana = null, tarea = null;
            }
        } catch (e) {
            ID_521921285_trycatch.error(e);
        }
    };
    Alloy.Events.on('_refrescar_tareas_mistareas', _my_events['_refrescar_tareas_mistareas,ID_199664348']);

    _my_events['_refrescar_tareas,ID_268924730'] = function(evento) {
        var inspeccion_encurso = ('inspeccion_encurso' in require('vars')) ? require('vars')['inspeccion_encurso'] : '';
        if (inspeccion_encurso == false || inspeccion_encurso == 'false') {

            Alloy.Events.trigger('_refrescar_tareas_mistareas');
        }
    };
    Alloy.Events.on('_refrescar_tareas', _my_events['_refrescar_tareas,ID_268924730']);
    /** 
     * Esto se llama al logearse 
     */
    var ID_1359791285_func = function() {

        Alloy.Events.trigger('_refrescar_tareas_mistareas');
    };
    var ID_1359791285 = setTimeout(ID_1359791285_func, 1000 * 0.1);
})();

function Androidback_ID_1943206439(e) {
    /** 
     * Sin funcionalidad para evitar que el usuario cierre la pantalla 
     */

    e.cancelBubble = true;
    var elemento = e.source;

}
if (OS_IOS || OS_ANDROID) {
    $.ID_304530978.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}
//$.ID_304530978.open();