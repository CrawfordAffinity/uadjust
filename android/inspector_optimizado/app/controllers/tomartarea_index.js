var _bind4section = {};
var _list_templates = {};

var _activity;
if (OS_ANDROID) {
    _activity = $.ID_1560093094.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_1560093094';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_1560093094.addEventListener('open', function(e) {});
}
$.ID_1560093094.orientationModes = [Titanium.UI.PORTRAIT];


$.ID_1426322055.init({
    titulo: 'TOMAR TAREA',
    __id: 'ALL1426322055',
    fondo: 'fondoblanco',
    colortitulo: 'negro',
    modal: ''
});


$.ID_658148125.init({
    __id: 'ALL658148125',
    onlisto: Listo_ID_557189579,
    label_a: 'a',
    onerror: Error_ID_934220026,
    _bono: 'Bono adicional:',
    bono: '',
    ondatos: Datos_ID_1762014414
});

function Listo_ID_557189579(e) {

    var evento = e;
    if (Ti.App.deployType != 'production') console.log('widget mapa ha llamado evento \'listo\'', {});

}

function Error_ID_934220026(e) {

    var evento = e;
    if (Ti.App.deployType != 'production') console.log('ha ocurrido un error con el mapa', {
        "evento": evento
    });

}

function Datos_ID_1762014414(e) {

    var evento = e;
    if (Ti.App.deployType != 'production') console.log('datos recibidos desde widget mapa: ' + evento.ruta_distancia, {
        "evento": evento
    });

}

$.ID_886082434.init({
    __id: 'ALL886082434',
    texto: 'PRESIONE PARA VERIFICAR',
    vertexto: true,
    verprogreso: false,
    onpresiono: Presiono_ID_1203848861
});

function Presiono_ID_1203848861(e) {

    var evento = e;
    /** 
     * Activamos animacion de progreso en boton 
     */

    $.ID_886082434.iniciar_progreso({});
    if (evento.estado == 1 || evento.estado == '1') {
        /** 
         * Llamamos servicios obtenerMisTareas al aceptarTarea para reordenar posici&#243;n seg&#250;n distancias y tener rutas correctas. 
         */
        /** 
         * De animacion de progreso en boton 
         */

        $.ID_886082434.detener_progreso({});
        $.ID_1560093094.close();
        /** 
         * Necesitamos saber la posicion del inspector para poder avisarle al servidor que vamos a tomar una tarea 
         */
        var _ifunc_res_ID_692055987 = function(e) {
            if (e.error) {
                var posicion = {
                    error: true,
                    latitude: -1,
                    longitude: -1,
                    reason: (e.reason) ? e.reason : 'unknown',
                    accuracy: 0,
                    speed: 0,
                    error_compass: false
                };
            } else {
                var posicion = e;
            }
            if (posicion.error == true || posicion.error == 'true') {
                var ID_736863222_opts = ['Aceptar'];
                var ID_736863222 = Ti.UI.createAlertDialog({
                    title: 'Alerta',
                    message: 'No se pudo obtener la ubicación',
                    buttonNames: ID_736863222_opts
                });
                ID_736863222.addEventListener('click', function(e) {
                    var suu = ID_736863222_opts[e.index];
                    suu = null;

                    e.source.removeEventListener("click", arguments.callee);
                });
                ID_736863222.show();
            } else {
                if (Ti.Network.networkType == Ti.Network.NETWORK_NONE) {
                    var ID_1990796407_opts = ['Aceptar', 'Cancelar'];
                    var ID_1990796407 = Ti.UI.createAlertDialog({
                        title: 'Alerta',
                        message: 'No tiene internet, por favor conectese a una red para realizar esta accion.',
                        buttonNames: ID_1990796407_opts
                    });
                    ID_1990796407.addEventListener('click', function(e) {
                        var suu = ID_1990796407_opts[e.index];
                        suu = null;

                        e.source.removeEventListener("click", arguments.callee);
                    });
                    ID_1990796407.show();
                } else {
                    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
                    var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
                    var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
                    if (Ti.App.deployType != 'production') console.log('consultando servicio aceptarTarea', {
                        "inspector": inspector,
                        "seltarea": seltarea
                    });
                    var ID_556808183 = {};

                    ID_556808183.success = function(e) {
                        var elemento = e,
                            valor = e;
                        if (Ti.App.deployType != 'production') console.log('estoy en success pero fuera de la condicion', {
                            "datos": elemento
                        });
                        if (elemento.error == 0 || elemento.error == '0') {

                            $.ID_886082434.datos({
                                verprogreso: false,
                                vertexto: true
                            });

                            $.ID_886082434.datos({
                                verprogreso: false,
                                vertexto: true,
                                texto: 'PRESIONE PARA ACEPTAR',
                                estilo: 'fondoverde',
                                estado: 1
                            });
                            var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
                            var ID_1099030330_i = Alloy.Collections.tareas_entrantes;
                            var sql = 'DELETE FROM ' + ID_1099030330_i.config.adapter.collection_name + ' WHERE id_server=\'' + seltarea.id_server + '\'';
                            var db = Ti.Database.open(ID_1099030330_i.config.adapter.db_name);
                            db.execute(sql);
                            db.close();
                            ID_1099030330_i.trigger('delete');
                            var ID_1751591597_i = Alloy.Collections.emergencia;
                            var sql = 'DELETE FROM ' + ID_1751591597_i.config.adapter.collection_name + ' WHERE id_server=\'' + seltarea.id_server + '\'';
                            var db = Ti.Database.open(ID_1751591597_i.config.adapter.db_name);
                            db.execute(sql);
                            db.close();
                            ID_1751591597_i.trigger('delete');
                            if (_.isNull(seltarea.fecha_tarea)) {
                                if (Ti.App.deployType != 'production') console.log('Agregando fecha', {});
                                hoy = new Date();
                                var fecha_hoy = null;
                                if ('formatear_fecha' in require('funciones')) {
                                    fecha_hoy = require('funciones').formatear_fecha({
                                        'fecha': hoy,
                                        'formato': 'YYYY-MM-DD'
                                    });
                                } else {
                                    try {
                                        fecha_hoy = f_formatear_fecha({
                                            'fecha': hoy,
                                            'formato': 'YYYY-MM-DD'
                                        });
                                    } catch (ee) {}
                                }
                                var seltarea = _.extend(seltarea, {
                                    fecha_tarea: fecha_hoy
                                });
                            }
                            var ID_1478210158_m = Alloy.Collections.tareas;
                            var ID_1478210158_fila = Alloy.createModel('tareas', {
                                fecha_tarea: seltarea.fecha_tarea,
                                id_inspeccion: seltarea.id_inspeccion,
                                nivel_2: seltarea.nivel_2,
                                id_asegurado: seltarea.id_asegurado,
                                comentario_can_o_rech: seltarea.comentario_can_o_rech,
                                estado_tarea: seltarea.estado_tarea,
                                bono: seltarea.bono,
                                id_inspector: seltarea.id_inspector,
                                lat: seltarea.lat,
                                nivel_1: seltarea.nivel_1,
                                pais: seltarea.pais,
                                direccion: seltarea.direccion,
                                asegurador: seltarea.asegurador,
                                fecha_ingreso: seltarea.fecha_ingreso,
                                fecha_siniestro: seltarea.fecha_siniestro,
                                nivel_1_texto: seltarea.nivel_1_texto,
                                distance: seltarea.distance,
                                nivel_4: seltarea.nivel_4,
                                id_server: seltarea.id_server,
                                pais_texto: seltarea.pais_texto,
                                categoria: seltarea.categoria,
                                nivel_3: seltarea.nivel_3,
                                num_caso: seltarea.num_caso,
                                lon: seltarea.lon,
                                tipo_tarea: seltarea.tipo_tarea,
                                nivel_5: seltarea.nivel_5
                            });
                            ID_1478210158_m.add(ID_1478210158_fila);
                            ID_1478210158_fila.save();
                            var ID_1419963851_func = function() {

                                Alloy.Events.trigger('_refrescar_tareas');
                            };
                            var ID_1419963851 = setTimeout(ID_1419963851_func, 1000 * 0.2);
                        } else {
                            if (Ti.App.deployType != 'production') console.log('estoy en la otra condicion', {});
                            var ID_1072658575_opts = ['Aceptar'];
                            var ID_1072658575 = Ti.UI.createAlertDialog({
                                title: '' + 'Error ' + elemento.error + '',
                                message: '' + elemento.mensaje + '',
                                buttonNames: ID_1072658575_opts
                            });
                            ID_1072658575.addEventListener('click', function(e) {
                                var suu = ID_1072658575_opts[e.index];
                                suu = null;

                                e.source.removeEventListener("click", arguments.callee);
                            });
                            ID_1072658575.show();
                        }
                        elemento = null, valor = null;
                    };

                    ID_556808183.error = function(e) {
                        var elemento = e,
                            valor = e;
                        if (Ti.App.deployType != 'production') console.log('Respuesta servicio', {
                            "datos": elemento
                        });
                        var ID_1848839976_opts = ['Aceptar'];
                        var ID_1848839976 = Ti.UI.createAlertDialog({
                            title: 'Error',
                            message: 'Error al conectarse al servidor',
                            buttonNames: ID_1848839976_opts
                        });
                        ID_1848839976.addEventListener('click', function(e) {
                            var suu = ID_1848839976_opts[e.index];
                            suu = null;

                            e.source.removeEventListener("click", arguments.callee);
                        });
                        ID_1848839976.show();

                        $.ID_886082434.datos({
                            verprogreso: false,
                            vertexto: true
                        });
                        elemento = null, valor = null;
                    };
                    require('helper').ajaxUnico('ID_556808183', '' + url_server + 'aceptarTarea' + '', 'POST', {
                        id_inspector: inspector.id_server,
                        codigo_identificador: inspector.codigo_identificador,
                        id_tarea: seltarea.id_server,
                        num_caso: seltarea.num_caso,
                        categoria: seltarea.categoria,
                        tipo_tarea: seltarea.tipo_tarea
                    }, 15000, ID_556808183);
                }
            }
        };
        Ti.Geolocation.setAccuracy(Ti.Geolocation.ACCURACY_HIGH);
        if (Ti.Geolocation.hasLocationPermissions(Ti.Geolocation.AUTHORIZATION_WHEN_IN_USE)) {
            Ti.Geolocation.getCurrentPosition(function(ee) {
                if (Ti.Geolocation.getHasCompass() == false) {
                    var pposicion = ('coords' in ee) ? ee.coords : {};
                    pposicion.error = ('coords' in ee) ? false : true;
                    pposicion.reason = '';
                    pposicion.speed = 0;
                    pposicion.compass = -1;
                    pposicion.compass_accuracy = -1;
                    pposicion.error_compass = true;
                    _ifunc_res_ID_692055987(pposicion);
                } else {
                    Ti.Geolocation.getCurrentHeading(function(yy) {
                        var pposicion = ('coords' in ee) ? ee.coords : {};
                        pposicion.error = ('coords' in ee) ? false : true;
                        pposicion.reason = ('error' in ee) ? ee.error : '';
                        if (yy.error) {
                            pposicion.error_compass = true;
                        } else {
                            pposicion.compass = yy.heading;
                            pposicion.compass_accuracy = yy.heading.accuracy;
                        }
                        _ifunc_res_ID_692055987(pposicion);
                    });
                }
            });
        } else {
            Ti.Geolocation.requestLocationPermissions(Ti.Geolocation.AUTHORIZATION_WHEN_IN_USE, function(u) {
                if (u.success) {
                    Ti.Geolocation.getCurrentPosition(function(ee) {
                        if (Ti.Geolocation.getHasCompass() == false) {
                            var pposicion = ('coords' in ee) ? ee.coords : {};
                            pposicion.error = ('coords' in ee) ? false : true;
                            pposicion.reason = '';
                            pposicion.speed = 0;
                            pposicion.compass = -1;
                            pposicion.compass_accuracy = -1;
                            pposicion.error_compass = true;
                            _ifunc_res_ID_692055987(pposicion);
                        } else {
                            Ti.Geolocation.getCurrentHeading(function(yy) {
                                var pposicion = ('coords' in ee) ? ee.coords : {};
                                pposicion.error = ('coords' in ee) ? false : true;
                                pposicion.reason = '';
                                if (yy.error) {
                                    pposicion.error_compass = true;
                                } else {
                                    pposicion.compass = yy.heading;
                                    pposicion.compass_accuracy = yy.heading.accuracy;
                                }
                                _ifunc_res_ID_692055987(pposicion);
                            });
                        }
                    });
                } else {
                    _ifunc_res_ID_692055987({
                        error: true,
                        latitude: -1,
                        longitude: -1,
                        reason: 'permission_denied',
                        accuracy: 0,
                        speed: 0,
                        error_compass: false
                    });
                }
            });
        }
    } else if (evento.estado == 2) {

        $.ID_886082434.detener_progreso({});
        var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
        var gps_error = ('gps_error' in require('vars')) ? require('vars')['gps_error'] : '';
        var gps_latitud = ('gps_latitud' in require('vars')) ? require('vars')['gps_latitud'] : '';
        var gps_longitud = ('gps_longitud' in require('vars')) ? require('vars')['gps_longitud'] : '';
        var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
        var ID_455229195_func = function() {

            Alloy.Events.trigger('_refrescar_tareas');
        };
        var ID_455229195 = setTimeout(ID_455229195_func, 1000 * 0.2);
        $.ID_1560093094.close();
    } else {
        var _ifunc_res_ID_1724873155 = function(e) {
            if (e.error) {
                var posicion = {
                    error: true,
                    latitude: -1,
                    longitude: -1,
                    reason: (e.reason) ? e.reason : 'unknown',
                    accuracy: 0,
                    speed: 0,
                    error_compass: false
                };
            } else {
                var posicion = e;
            }

            $.ID_886082434.detener_progreso({});
            if (posicion.error == true || posicion.error == 'true') {
                /** 
                 * Si se puede obtener la posicion gps, hacemos una consulta al servidor para ver la disponibilidad de tarea, caso contrario avisamos al inspector que no se pudo obtener la ubicacion 
                 */
                var ID_1014743017_opts = ['Aceptar'];
                var ID_1014743017 = Ti.UI.createAlertDialog({
                    title: 'Alerta',
                    message: 'No se pudo obtener la ubicación',
                    buttonNames: ID_1014743017_opts
                });
                ID_1014743017.addEventListener('click', function(e) {
                    var suu = ID_1014743017_opts[e.index];
                    suu = null;

                    e.source.removeEventListener("click", arguments.callee);
                });
                ID_1014743017.show();
            } else {
                if (Ti.Network.networkType == Ti.Network.NETWORK_NONE) {
                    var ID_607887917_opts = ['Aceptar', 'Cancelar'];
                    var ID_607887917 = Ti.UI.createAlertDialog({
                        title: 'Alerta',
                        message: 'No tiene internet, por favor conectese a una red para realizar esta accion.',
                        buttonNames: ID_607887917_opts
                    });
                    ID_607887917.addEventListener('click', function(e) {
                        var suu = ID_607887917_opts[e.index];
                        suu = null;

                        e.source.removeEventListener("click", arguments.callee);
                    });
                    ID_607887917.show();
                } else {
                    /** 
                     * Activamos animacion de progreso en boton 
                     */

                    $.ID_886082434.iniciar_progreso({});
                    if (Ti.App.deployType != 'production') console.log('llamando disponiblidad de tarea', {});
                    /** 
                     * Recuperamos variable 
                     */
                    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
                    var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
                    var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
                    /** 
                     * Consultamos al servidor si el inspector tiene disponibilidad para tomar la tarea. 
                     */
                    var ID_1820266035 = {};

                    ID_1820266035.success = function(e) {
                        var elemento = e,
                            valor = e;
                        if (Ti.App.deployType != 'production') console.log('estoy en success pero fuera de la condicion', {
                            "datos": elemento
                        });
                        /** 
                         * Activamos animacion de progreso en boton 
                         */

                        $.ID_886082434.detener_progreso({});
                        /** 
                         * Escondemos la animacion de progreso y mostramos texto en el boton 
                         */

                        $.ID_886082434.datos({
                            verprogreso: false,
                            vertexto: true
                        });
                        if (elemento.error == 0 || elemento.error == '0') {
                            /** 
                             * Si la respuesta del servidor es correcta (o sea, error 0). Si la disponibilidad es 0, no se puede tomar por exceso de tareas tomadas, disponibilidad es 1, la tarea es tomada y registrada en el servidor 
                             */
                            if (elemento.disponibilidad == 0 || elemento.disponibilidad == '0') {
                                var cerrar = 'CERRAR';

                                $.ID_886082434.datos({
                                    verprogreso: false,
                                    vertexto: true,
                                    texto: cerrar,
                                    estilo: 'fondorojo',
                                    estado: 2
                                });
                                var toma_max_tareas = 'SUPERASTE TU TOMA MAXIMA DE TAREAS';
                                $.ID_179299802.setText(toma_max_tareas);

                                var cap_max_tareas = 'Tienes una capacidad maxima de tareas por dia pero aun puedes tomar en otros dias';

                                $.ID_1902218260.update({
                                    texto: cap_max_tareas
                                });
                            } else if (elemento.disponibilidad == 1 || elemento.disponibilidad == '1') {

                                $.ID_886082434.datos({
                                    verprogreso: false,
                                    vertexto: true
                                });
                                var quedara_mistareas = 'Quedara guardada en Mis Tareas';
                                $.ID_179299802.setText(quedara_mistareas);

                                var presione_aceptar = 'PRESIONE PARA ACEPTAR';

                                $.ID_886082434.datos({
                                    verprogreso: false,
                                    vertexto: true,
                                    texto: presione_aceptar,
                                    estilo: 'fondoverde',
                                    estado: 1
                                });
                            }
                        } else {
                            var ID_1120038612_opts = ['Aceptar'];
                            var ID_1120038612 = Ti.UI.createAlertDialog({
                                title: 'Error',
                                message: 'Error al conectarse al servidor',
                                buttonNames: ID_1120038612_opts
                            });
                            ID_1120038612.addEventListener('click', function(e) {
                                var suu = ID_1120038612_opts[e.index];
                                suu = null;

                                e.source.removeEventListener("click", arguments.callee);
                            });
                            ID_1120038612.show();
                        }
                        elemento = null, valor = null;
                    };

                    ID_1820266035.error = function(e) {
                        var elemento = e,
                            valor = e;
                        if (Ti.App.deployType != 'production') console.log('Respuesta servicio', {
                            "datos": elemento
                        });
                        var ID_359911163_opts = ['Aceptar'];
                        var ID_359911163 = Ti.UI.createAlertDialog({
                            title: 'Error',
                            message: 'Error al conectarse al servidor',
                            buttonNames: ID_359911163_opts
                        });
                        ID_359911163.addEventListener('click', function(e) {
                            var suu = ID_359911163_opts[e.index];
                            suu = null;

                            e.source.removeEventListener("click", arguments.callee);
                        });
                        ID_359911163.show();

                        $.ID_886082434.datos({
                            verprogreso: false,
                            vertexto: true
                        });
                        elemento = null, valor = null;
                    };
                    require('helper').ajaxUnico('ID_1820266035', '' + url_server + 'dispInspector' + '', 'POST', {
                        id_inspector: inspector.id_server,
                        codigo_identificador: inspector.codigo_identificador,
                        id_tarea: seltarea.id_server,
                        num_caso: seltarea.num_caso
                    }, 15000, ID_1820266035);
                }
            }
        };
        Ti.Geolocation.setAccuracy(Ti.Geolocation.ACCURACY_HIGH);
        if (Ti.Geolocation.hasLocationPermissions(Ti.Geolocation.AUTHORIZATION_WHEN_IN_USE)) {
            Ti.Geolocation.getCurrentPosition(function(ee) {
                if (Ti.Geolocation.getHasCompass() == false) {
                    var pposicion = ('coords' in ee) ? ee.coords : {};
                    pposicion.error = ('coords' in ee) ? false : true;
                    pposicion.reason = '';
                    pposicion.speed = 0;
                    pposicion.compass = -1;
                    pposicion.compass_accuracy = -1;
                    pposicion.error_compass = true;
                    _ifunc_res_ID_1724873155(pposicion);
                } else {
                    Ti.Geolocation.getCurrentHeading(function(yy) {
                        var pposicion = ('coords' in ee) ? ee.coords : {};
                        pposicion.error = ('coords' in ee) ? false : true;
                        pposicion.reason = ('error' in ee) ? ee.error : '';
                        if (yy.error) {
                            pposicion.error_compass = true;
                        } else {
                            pposicion.compass = yy.heading;
                            pposicion.compass_accuracy = yy.heading.accuracy;
                        }
                        _ifunc_res_ID_1724873155(pposicion);
                    });
                }
            });
        } else {
            Ti.Geolocation.requestLocationPermissions(Ti.Geolocation.AUTHORIZATION_WHEN_IN_USE, function(u) {
                if (u.success) {
                    Ti.Geolocation.getCurrentPosition(function(ee) {
                        if (Ti.Geolocation.getHasCompass() == false) {
                            var pposicion = ('coords' in ee) ? ee.coords : {};
                            pposicion.error = ('coords' in ee) ? false : true;
                            pposicion.reason = '';
                            pposicion.speed = 0;
                            pposicion.compass = -1;
                            pposicion.compass_accuracy = -1;
                            pposicion.error_compass = true;
                            _ifunc_res_ID_1724873155(pposicion);
                        } else {
                            Ti.Geolocation.getCurrentHeading(function(yy) {
                                var pposicion = ('coords' in ee) ? ee.coords : {};
                                pposicion.error = ('coords' in ee) ? false : true;
                                pposicion.reason = '';
                                if (yy.error) {
                                    pposicion.error_compass = true;
                                } else {
                                    pposicion.compass = yy.heading;
                                    pposicion.compass_accuracy = yy.heading.accuracy;
                                }
                                _ifunc_res_ID_1724873155(pposicion);
                            });
                        }
                    });
                } else {
                    _ifunc_res_ID_1724873155({
                        error: true,
                        latitude: -1,
                        longitude: -1,
                        reason: 'permission_denied',
                        accuracy: 0,
                        speed: 0,
                        error_compass: false
                    });
                }
            });
        }
    }
}

$.ID_1902218260.init({
    __id: 'ALL1902218260',
    texto: 'Tip: No aceptes tareas si no estás seguro que puedes llegar a tiempo',
    top: 10,
    tipo: '_tip'
});


(function() {
    if (Ti.App.deployType != 'production') console.log('argumentos tomartarea', {
        "modelo": args
    });
    if (!_.isUndefined(args._tipo)) {
        /** 
         * Dependiendo de donde venga la tarea, guardamos una variable con el detalle de la tarea 
         */
        if (args._tipo == 'emergencias') {
            var ID_954492358_i = Alloy.createCollection('emergencia');
            var ID_954492358_i_where = 'id=\'' + args._id + '\'';
            ID_954492358_i.fetch({
                query: 'SELECT * FROM emergencia WHERE id=\'' + args._id + '\''
            });
            var tareas = require('helper').query2array(ID_954492358_i);
            require('vars')['tareas'] = tareas;
        } else if (args._tipo == 'entrantes') {
            var ID_622206359_i = Alloy.createCollection('tareas_entrantes');
            var ID_622206359_i_where = 'id=\'' + args._id + '\'';
            ID_622206359_i.fetch({
                query: 'SELECT * FROM tareas_entrantes WHERE id=\'' + args._id + '\''
            });
            var tareas = require('helper').query2array(ID_622206359_i);
            require('vars')['tareas'] = tareas;
        }
    }
    var tareas = ('tareas' in require('vars')) ? require('vars')['tareas'] : '';
    if (tareas && tareas.length) {
        /** 
         * Revisamos si tareas contiene datos 
         */
        /** 
         * Guardamos en una variable los detalles de la tarea. Servira tambien para el resto de la inspeccion 
         */
        require('vars')['seltarea'] = tareas[0];
        /** 
         * Creamos estructura con datos de la tarea 
         */
        var info = {
            direccion: tareas[0].direccion,
            ciudad_pais: tareas[0].nivel_2 + ', ' + tareas[0].pais_texto,
            ciudad: tareas[0].nivel_1,
            distancia: tareas[0].distance
        };
        if ((_.isObject(tareas[0].nivel_2) || _.isString(tareas[0].nivel_2)) && _.isEmpty(tareas[0].nivel_2)) {
            /** 
             * Agregamos el valor de la comuna como ultimo nivel 
             */
            var info = _.extend(info, {
                comuna: tareas[0].nivel_1
            });
        } else if ((_.isObject(tareas[0].nivel_3) || _.isString(tareas[0].nivel_3)) && _.isEmpty(tareas[0].nivel_3)) {
            var info = _.extend(info, {
                comuna: tareas[0].nivel_2
            });
        } else if ((_.isObject(tareas[0].nivel_4) || _.isString(tareas[0].nivel_4)) && _.isEmpty(tareas[0].nivel_4)) {
            var info = _.extend(info, {
                comuna: tareas[0].nivel_3
            });
        } else if ((_.isObject(tareas[0].nivel_5) || _.isString(tareas[0].nivel_5)) && _.isEmpty(tareas[0].nivel_5)) {
            var info = _.extend(info, {
                comuna: tareas[0].nivel_4
            });
        } else {
            var info = _.extend(info, {
                comuna: tareas[0].nivel_5
            });
        }
        if (Ti.App.deployType != 'production') console.log('actualizamos mapa con direccion', {});
        if (tareas[0].perfil == 'casa') {
            /** 
             * Dependiendo del donde venga la tarea, desde la casa registrada o ubicacion gps se actualiza el widget del mapa 
             */
            var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';

            $.ID_658148125.update({
                tipo: 'origen',
                origen_latitud: inspector.lat_dir,
                origen_longitud: inspector.lon_dir,
                destino_latitud: tareas[0].lat,
                destino_longitud: tareas[0].lon,
                ruta: true,
                direccion: info.direccion,
                comuna: info.comuna,
                ciudad: info.ciudad_pais,
                distancia: info.distancia
            });
        } else {

            $.ID_658148125.update({
                tipo: 'ubicacion',
                latitud: tareas[0].lat,
                longitud: tareas[0].lon,
                ruta: true,
                direccion: info.direccion,
                comuna: info.comuna,
                ciudad: info.ciudad_pais,
                distancia: info.distancia
            });
        }
        /** 
         * transforma valor decimal de bono a numero entero al dividirlo por 1. 
         */
        tareas[0].bono = (tareas[0].bono / 1);
        if (tareas[0].bono != 0.0 && tareas[0].bono != '0.0') {
            /** 
             * Actualizamos el widget de los bonos en caso de que exista algo 
             */

            $.ID_658148125.update({
                bono: tareas[0].bono
            });
        } else {

            $.ID_658148125.update({
                bono: ''
            });
        }
    }
})();

function Postlayout_ID_1036973027(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var ID_1162019012_func = function() {
        /** 
         * Una vez ya cargada la tarea, dejamos la variable limpia para poder abrir otras pantallas 
         */
        require('vars')['var_abriendo'] = '';
    };
    var ID_1162019012 = setTimeout(ID_1162019012_func, 1000 * 0.2);

}
if (OS_IOS || OS_ANDROID) {
    $.ID_1560093094.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}
//$.ID_1560093094.open();