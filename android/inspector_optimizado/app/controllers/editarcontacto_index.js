var _bind4section = {};
var _list_templates = {};

var _activity;
if (OS_ANDROID) {
    _activity = $.ID_1534880474.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_1534880474';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_1534880474.addEventListener('open', function(e) {});
}
$.ID_1534880474.orientationModes = [Titanium.UI.PORTRAIT];


$.ID_1819849455.init({
    titulo: 'CONTACTO',
    __id: 'ALL1819849455',
    textoderecha: 'Guardar',
    fondo: 'fondoblanco',
    colortitulo: 'negro',
    modal: '',
    onpresiono: Presiono_ID_537586411,
    colortextoderecha: 'azul'
});

function Presiono_ID_537586411(e) {

    var evento = e;
    if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
        /** 
         * Obtenemos los valores de los campos ingresados en la pantalla 
         */
        var correo;
        correo = $.ID_983090064.getValue();

        var correo_aux;
        correo_aux = $.ID_627845946.getValue();

        var telefono;
        telefono = $.ID_1726261643.getValue();

        var telefono_aux;
        telefono_aux = $.ID_1377483554.getValue();

        if ((_.isObject(correo) || _.isString(correo)) && _.isEmpty(correo)) {
            /** 
             * Revisamos que los campos no esten vacios o que no coincidan 
             */
            var ID_801337114_opts = ['Aceptar'];
            var ID_801337114 = Ti.UI.createAlertDialog({
                title: 'Alerta',
                message: 'Debe ingresar un correo',
                buttonNames: ID_801337114_opts
            });
            ID_801337114.addEventListener('click', function(e) {
                var suu = ID_801337114_opts[e.index];
                suu = null;

                e.source.removeEventListener("click", arguments.callee);
            });
            ID_801337114.show();
        } else if (!(_.isString(correo) && /\S+@\S+\.\S+/.test(correo))) {
            var ID_1434870769_opts = ['Aceptar'];
            var ID_1434870769 = Ti.UI.createAlertDialog({
                title: 'Alerta',
                message: 'Debe ingresar un correo valido',
                buttonNames: ID_1434870769_opts
            });
            ID_1434870769.addEventListener('click', function(e) {
                var suu = ID_1434870769_opts[e.index];
                suu = null;

                e.source.removeEventListener("click", arguments.callee);
            });
            ID_1434870769.show();
        } else if (correo != correo_aux) {
            var ID_235055574_opts = ['Aceptar'];
            var ID_235055574 = Ti.UI.createAlertDialog({
                title: 'Alerta',
                message: 'Los correos no coinciden',
                buttonNames: ID_235055574_opts
            });
            ID_235055574.addEventListener('click', function(e) {
                var suu = ID_235055574_opts[e.index];
                suu = null;

                e.source.removeEventListener("click", arguments.callee);
            });
            ID_235055574.show();
        } else if ((_.isObject(telefono) || _.isString(telefono)) && _.isEmpty(telefono)) {
            var ID_1739791955_opts = ['Aceptar'];
            var ID_1739791955 = Ti.UI.createAlertDialog({
                title: 'Alerta',
                message: 'Debe ingresar un teléfono',
                buttonNames: ID_1739791955_opts
            });
            ID_1739791955.addEventListener('click', function(e) {
                var suu = ID_1739791955_opts[e.index];
                suu = null;

                e.source.removeEventListener("click", arguments.callee);
            });
            ID_1739791955.show();
        } else if (_.isNumber((telefono.length)) && _.isNumber(3) && (telefono.length) < 3) {
            var ID_1206134399_opts = ['Aceptar'];
            var ID_1206134399 = Ti.UI.createAlertDialog({
                title: 'Alerta',
                message: 'El telefono debe tener más digitos',
                buttonNames: ID_1206134399_opts
            });
            ID_1206134399.addEventListener('click', function(e) {
                var suu = ID_1206134399_opts[e.index];
                suu = null;

                e.source.removeEventListener("click", arguments.callee);
            });
            ID_1206134399.show();
        } else if (telefono != telefono_aux) {
            var ID_557649581_opts = ['Aceptar'];
            var ID_557649581 = Ti.UI.createAlertDialog({
                title: 'Alerta',
                message: 'Los teléfonos no coinciden',
                buttonNames: ID_557649581_opts
            });
            ID_557649581.addEventListener('click', function(e) {
                var suu = ID_557649581_opts[e.index];
                suu = null;

                e.source.removeEventListener("click", arguments.callee);
            });
            ID_557649581.show();
        } else {
            /** 
             * Guardamos correo y telefono en caso de que la consulta al servidor sea exitosa 
             */
            require('vars')['correo'] = correo;
            require('vars')['telefono'] = telefono;
            /** 
             * Recuperamos variable url de uadjust 
             */
            var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
            var ID_516580855 = {};

            ID_516580855.success = function(e) {
                var elemento = e,
                    valor = e;
                if (Ti.App.deployType != 'production') console.log('Servidor responde editarPerfilTipo3', {
                    "elemento": elemento
                });
                /** 
                 * Actualizamos datos cambiados en la tabla y tambien actualizamos la variable del inspector con los datos nuevos 
                 */
                var ID_391043314_i = Alloy.createCollection('inspectores');
                var ID_391043314_i_where = '';
                ID_391043314_i.fetch();
                var inspector_list = require('helper').query2array(ID_391043314_i);
                var correo = ('correo' in require('vars')) ? require('vars')['correo'] : '';
                var telefono = ('telefono' in require('vars')) ? require('vars')['telefono'] : '';
                if (Ti.App.deployType != 'production') console.log('hare modificacion', {
                    "datos": correo + ' y ' + telefono
                });
                var db = Ti.Database.open(ID_391043314_i.config.adapter.db_name);
                if (ID_391043314_i_where == '') {
                    var sql = 'UPDATE ' + ID_391043314_i.config.adapter.collection_name + ' SET correo=\'' + correo + '\'';
                } else {
                    var sql = 'UPDATE ' + ID_391043314_i.config.adapter.collection_name + ' SET correo=\'' + correo + '\' WHERE ' + ID_391043314_i_where;
                }
                db.execute(sql);
                sql = null;
                if (ID_391043314_i_where == '') {
                    var sql = 'UPDATE ' + ID_391043314_i.config.adapter.collection_name + ' SET telefono=\'' + telefono + '\'';
                } else {
                    var sql = 'UPDATE ' + ID_391043314_i.config.adapter.collection_name + ' SET telefono=\'' + telefono + '\' WHERE ' + ID_391043314_i_where;
                }
                db.execute(sql);
                sql = null;
                db.close();
                db = null;
                /** 
                 * Actualizamos la variable inspector con los datos recien ingresados 
                 */
                var ID_152353124_i = Alloy.createCollection('inspectores');
                var ID_152353124_i_where = '';
                ID_152353124_i.fetch();
                var inspector_list = require('helper').query2array(ID_152353124_i);
                require('vars')['inspector'] = inspector_list[0];
                /** 
                 * Limpiamos memoria 
                 */
                inspector_list = null;
                /** 
                 * Mostramos mensaje de exito de cambio 
                 */
                var ID_710102279_opts = ['ACEPTAR'];
                var ID_710102279 = Ti.UI.createAlertDialog({
                    title: 'Atención',
                    message: 'Cambios realizados',
                    buttonNames: ID_710102279_opts
                });
                ID_710102279.addEventListener('click', function(e) {
                    var abc = ID_710102279_opts[e.index];
                    $.ID_1534880474.close();
                    abc = null;

                    e.source.removeEventListener("click", arguments.callee);
                });
                ID_710102279.show();
                elemento = null, valor = null;
            };

            ID_516580855.error = function(e) {
                var elemento = e,
                    valor = e;
                if (Ti.App.deployType != 'production') console.log('Hubo un error', {
                    "elemento": elemento
                });
                elemento = null, valor = null;
            };
            require('helper').ajaxUnico('ID_516580855', '' + url_server + 'editarPerfilTipo3' + '', 'POST', {
                id_inspector: inspector.id_server,
                correo: correo,
                telefono: telefono
            }, 15000, ID_516580855);
        }
    } else {
        var ID_1766319835_opts = ['Aceptar'];
        var ID_1766319835 = Ti.UI.createAlertDialog({
            title: 'Alerta',
            message: 'No se pueden efectuar los cambios, ya que no hay internet',
            buttonNames: ID_1766319835_opts
        });
        ID_1766319835.addEventListener('click', function(e) {
            var suu = ID_1766319835_opts[e.index];
            suu = null;

            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1766319835.show();
    }
}

function Click_ID_1372066254(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    $.ID_983090064.blur();
    $.ID_627845946.blur();
    $.ID_1726261643.blur();
    $.ID_1377483554.blur();

}

(function() {
    /** 
     * Recuperamos variable de inspector y cargamos los datos en los campos de texto, para que sepa cuales eran sus datos 
     */
    var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
    $.ID_983090064.setValue(inspector.correo);

    $.ID_1726261643.setValue(inspector.telefono);

})();

function Postlayout_ID_1620368888(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var ID_1534533147_func = function() {
        /** 
         * Una vez ya cargada la tarea, dejamos la variable limpia para poder abrir otras pantallas 
         */
        require('vars')['var_abriendo'] = '';
    };
    var ID_1534533147 = setTimeout(ID_1534533147_func, 1000 * 0.2);

}
if (OS_IOS || OS_ANDROID) {
    $.ID_1534880474.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}
//$.ID_1534880474.open();