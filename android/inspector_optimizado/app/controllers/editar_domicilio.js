var _bind4section = {};
var _list_templates = {};
var $otro_inspector = $.otro_inspector.toJSON();

var _activity;
if (OS_ANDROID) {
    _activity = $.ID_1809462296.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_1809462296';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_1809462296.addEventListener('open', function(e) {});
}
$.ID_1809462296.orientationModes = [Titanium.UI.PORTRAIT];


$.ID_1966017936.init({
    titulo: 'EDITAR',
    __id: 'ALL1966017936',
    textoderecha: 'Guardar',
    fondo: 'fondoblanco',
    colortitulo: 'negro',
    modal: '',
    onpresiono: Presiono_ID_306683037,
    colortextoderecha: 'azul'
});

function Presiono_ID_306683037(e) {

    var evento = e;
    if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
        /** 
         * Revisamos si el smartphone tiene internet 
         */
        /** 
         * Obtenemos los valores ingresados en los campos de texto y guardamos en variables 
         */
        var direccion;
        direccion = $.ID_1762146332.getValue();

        var data_nivel_2;
        data_nivel_2 = $.ID_2047500265.getValue();

        var data_nivel_3;
        data_nivel_3 = $.ID_1167282670.getValue();

        var data_nivel_4;
        data_nivel_4 = $.ID_1909855472.getValue();

        var data_nivel_5;
        data_nivel_5 = $.ID_1388286681.getValue();

        /** 
         * Recuperamos el detalle del pais seleccionado 
         */
        var pais = ('pais' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['pais'] : '';
        /** 
         * Creamos variable para identificar que no falten datos por ser ingresado en la pantalla 
         */

        var falta_algo =
            (data_nivel_2 == "" && pais.label_nivel2 != "") ||
            (data_nivel_3 == "" && pais.label_nivel3 != "") ||
            (data_nivel_4 == "" && pais.label_nivel4 != "") ||
            (data_nivel_5 == "" && pais.label_nivel5 != "") ||
            (direccion == "")
            /** 
             * Recuperamos la variable del nivel_1 seleccionado 
             */
        var data_nivel_1 = ('data_nivel_1' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['data_nivel_1'] : '';
        if (Ti.App.deployType != 'production') console.log('detalle de niveles', {
            "nivel4": data_nivel_4,
            "nivel5": data_nivel_5,
            "nivel2": data_nivel_2,
            "nivel1": data_nivel_1,
            "direccion": direccion,
            "nivel3": data_nivel_3
        });
        if ((_.isObject(data_nivel_1) || _.isString(data_nivel_1)) && _.isEmpty(data_nivel_1)) {
            /** 
             * Revisamos que no falten datos, y en caso que falten, avisamos al inspector 
             */
            var ID_1865450481_opts = ['Aceptar'];
            var ID_1865450481 = Ti.UI.createAlertDialog({
                title: 'Atención',
                message: 'Seleccione región',
                buttonNames: ID_1865450481_opts
            });
            ID_1865450481.addEventListener('click', function(e) {
                var suu = ID_1865450481_opts[e.index];
                suu = null;
                e.source.removeEventListener("click", arguments.callee);
            });
            ID_1865450481.show();
        } else if ((_.isObject(direccion) || _.isString(direccion)) && _.isEmpty(direccion)) {
            var ID_1611536031_opts = ['Aceptar'];
            var ID_1611536031 = Ti.UI.createAlertDialog({
                title: 'Atención',
                message: 'Ingrese dirección',
                buttonNames: ID_1611536031_opts
            });
            ID_1611536031.addEventListener('click', function(e) {
                var suu = ID_1611536031_opts[e.index];
                suu = null;
                e.source.removeEventListener("click", arguments.callee);
            });
            ID_1611536031.show();
        } else if (falta_algo == true || falta_algo == 'true') {
            var ID_1307348037_opts = ['Aceptar'];
            var ID_1307348037 = Ti.UI.createAlertDialog({
                title: 'Atención',
                message: 'Ingrese datos faltantes',
                buttonNames: ID_1307348037_opts
            });
            ID_1307348037.addEventListener('click', function(e) {
                var suu = ID_1307348037_opts[e.index];
                suu = null;
                e.source.removeEventListener("click", arguments.callee);
            });
            ID_1307348037.show();
        } else {
            var data_nivel_1 = ('data_nivel_1' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['data_nivel_1'] : '';
            if (_.isString(data_nivel_1)) {
                var url = data_nivel_1.replace(" ", "+");
                url = url + "+" + data_nivel_2.replace(" ", "+");
                url = url + "+" + data_nivel_3.replace(" ", "+");
                url = url + "+" + data_nivel_4.replace(" ", "+");
                url = url + "+" + data_nivel_5.replace(" ", "+");
                url = url + "+" + direccion.replace(" ", "+");
                url = url + "+" + pais.nombre;
                url2 = "https://maps.googleapis.com/maps/api/geocode/json?address=" + url
            } else {
                var url = data_nivel_1.valor.replace(" ", "+");
                url = url + "+" + data_nivel_2.replace(" ", "+");
                url = url + "+" + data_nivel_3.replace(" ", "+");
                url = url + "+" + data_nivel_4.replace(" ", "+");
                url = url + "+" + data_nivel_5.replace(" ", "+");
                url = url + "+" + direccion.replace(" ", "+");
                url = url + "+" + pais.nombre;
                url2 = "https://maps.googleapis.com/maps/api/geocode/json?address=" + url
            }
            if (Ti.App.deployType != 'production') console.log('Consultando a Google si direccion es valida', {
                "url": url2
            });
            var ID_1916620719 = {};
            ID_1916620719.success = function(e) {
                var elemento = e,
                    valor = e;
                if (Ti.App.deployType != 'production') console.log('respuesta de google', {
                    "datos": elemento
                });
                if (elemento.status == 'OK') {
                    /** 
                     * Revisamos que la respuesta de google sea ok y asi verificar que existan datos 
                     */
                    if (elemento.results[0].formatted_address.toLowerCase().indexOf(direccion.toLowerCase()) != -1) {
                        /** 
                         * Si google retorno la misma direccion que ingreso el usuario 
                         */
                        if (Ti.App.deployType != 'production') console.log('contiene direccion', {});
                        /** 
                         * Si la consulta al servidor fue exitosa, limpiamos la tabla del inspector, guardamos los datos del binding y finalmente una consulta a la tabla para poder actualizar la variable del inspector 
                         */
                        /** 
                         * Si la consulta al servidor fue exitosa, limpiamos la tabla del inspector, guardamos los datos del binding y finalmente una consulta a la tabla para poder actualizar la variable del inspector 
                         */
                        var ID_1221074222_i = Alloy.Collections.inspectores;
                        var sql = "DELETE FROM " + ID_1221074222_i.config.adapter.collection_name;
                        var db = Ti.Database.open(ID_1221074222_i.config.adapter.db_name);
                        db.execute(sql);
                        db.close();
                        sql = null;
                        db = null;
                        ID_1221074222_i.trigger('delete');
                        /** 
                         * Recuperamos los datos del nivel1 para actualizarlo en la tabla del inspector, cargamos los datos en la tabla, limpiamos los datos del inspector viejo, guardamos los datos nuevos y actualizamos los datos de la variable inspector con los datos nuevo 
                         */
                        var data_nivel_1 = ('data_nivel_1' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['data_nivel_1'] : '';
                        var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
                        var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
                        if (Ti.App.deployType != 'production') console.log('contiene direccion', {
                            "nivel_4": data_nivel_4,
                            "nivel_2": data_nivel_2,
                            "google": JSON.stringify(elemento.results[0].address_components),
                            "lat_dir": elemento.results[0].geometry.location.lat,
                            "lon_dir": elemento.results[0].geometry.location.lng,
                            "id_inspector": inspector.id_server,
                            "nivel_3": data_nivel_3,
                            "nivel_1": data_nivel_1.id_server,
                            "pais": inspector.pais,
                            "direccion": direccion,
                            "correccion_direccion": 2,
                            "_url": url_server + 'editarPerfilTipo1',
                            "nivel_5": data_nivel_5
                        });
                        $.otro_inspector.set({
                            nivel4: data_nivel_4,
                            id_nivel1: data_nivel_1.id_server,
                            nivel2: data_nivel_2,
                            nivel5: data_nivel_5,
                            lat_dir: elemento.results[0].geometry.location.lat,
                            lon_dir: elemento.results[0].geometry.location.lat,
                            direccion: elemento.results[0].formatted_address.split(',')[0],
                            correccion_direccion: 2,
                            nivel3: data_nivel_3
                        });
                        if ('otro_inspector' in $) $otro_inspector = $.otro_inspector.toJSON();
                        var ID_800109325 = {};
                        ID_800109325.success = function(e) {
                            var elemento = e,
                                valor = e;
                            Alloy.Collections[$.otro_inspector.config.adapter.collection_name].add($.otro_inspector);
                            $.otro_inspector.save();
                            Alloy.Collections[$.otro_inspector.config.adapter.collection_name].fetch();
                            var ID_1031717296_func = function() {
                                var ID_1530997824_i = Alloy.createCollection('inspectores');
                                var ID_1530997824_i_where = '';
                                ID_1530997824_i.fetch();
                                var inspector = require('helper').query2array(ID_1530997824_i);
                                if (inspector && inspector.length) {
                                    /** 
                                     * guardamos la variable: inspector con los datos del inspector activo para no tener que consultarla cada vez. 
                                     */
                                    require('vars')['inspector'] = inspector[0];
                                    if (Ti.App.deployType != 'production') console.log('detalle del nuevo inspector', {
                                        "datos": inspector[0]
                                    });
                                }
                            };
                            var ID_1031717296 = setTimeout(ID_1031717296_func, 1000 * 0.5);
                            $.ID_1809462296.close();
                            elemento = null, valor = null;
                        };
                        ID_800109325.error = function(e) {
                            var elemento = e,
                                valor = e;
                            /** 
                             * Si hubo error, no actualizamos nada y solo mostramos un mensaje de que no se pudo actualizar la informacion 
                             */
                            var ID_1010222687_opts = ['Aceptar'];
                            var ID_1010222687 = Ti.UI.createAlertDialog({
                                title: 'Alerta',
                                message: 'Problema de conexión por favor intentar después',
                                buttonNames: ID_1010222687_opts
                            });
                            ID_1010222687.addEventListener('click', function(e) {
                                var suu = ID_1010222687_opts[e.index];
                                suu = null;
                                e.source.removeEventListener("click", arguments.callee);
                            });
                            ID_1010222687.show();
                            elemento = null, valor = null;
                        };
                        require('helper').ajaxUnico('ID_800109325', '' + url_server + 'editarPerfilTipo1' + '', 'POST', {
                            id_inspector: inspector.id_server,
                            pais: inspector.pais,
                            nivel_1: data_nivel_1.id_server,
                            nivel_2: data_nivel_2,
                            nivel_3: data_nivel_3,
                            nivel_4: data_nivel_4,
                            nivel_5: data_nivel_5,
                            direccion: direccion,
                            correccion_direccion: 2,
                            lat_dir: elemento.results[0].geometry.location.lat,
                            lon_dir: elemento.results[0].geometry.location.lng,
                            google: JSON.stringify(elemento.results[0].address_components)
                        }, 15000, ID_800109325);
                    } else {
                        if (Ti.App.deployType != 'production') console.log('no contiene direccion', {});
                        require('vars')[_var_scopekey]['resultado_google'] = elemento.results[0];
                        /** 
                         * Modificamos la tabla con los datos que encuentra google 
                         */
                        $.otro_inspector.set({
                            nivel4: data_nivel_4,
                            nivel2: data_nivel_2,
                            nivel5: data_nivel_5,
                            lat_dir: elemento.results[0].geometry.location.lat,
                            lon_dir: elemento.results[0].geometry.location.lat,
                            nivel1: data_nivel_1.id_server,
                            direccion: direccion,
                            correccion_direccion: 1,
                            nivel3: data_nivel_3
                        });
                        if ('otro_inspector' in $) $otro_inspector = $.otro_inspector.toJSON();
                        /** 
                         * Abrimos dialogo de confirmar direccion que encontro google 
                         */
                        var ID_1458205287_opts = ['Si', 'No', 'Continuar'];
                        var ID_1458205287 = Ti.UI.createAlertDialog({
                            title: 'La direccion indicada no parece valida',
                            message: '' + 'Es esta la correcta? ' + elemento.results[0].formatted_address + '',
                            buttonNames: ID_1458205287_opts
                        });
                        ID_1458205287.addEventListener('click', function(e) {
                            var resp_pregunta = ID_1458205287_opts[e.index];
                            var data_nivel_1 = ('data_nivel_1' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['data_nivel_1'] : '';
                            if (_.isString(data_nivel_1)) {
                                $.otro_inspector.set({
                                    nivel_1: data_nivel_1
                                });
                                if ('otro_inspector' in $) $otro_inspector = $.otro_inspector.toJSON();
                            } else {
                                $.otro_inspector.set({
                                    nivel_1: data_nivel_1.id_server
                                });
                                if ('otro_inspector' in $) $otro_inspector = $.otro_inspector.toJSON();
                            }
                            if (resp_pregunta == 'Si') {
                                /** 
                                 * Si la respuesta del usuario es un si, recuperamos variables del detalle de informacion de google, la url de uadjust, y los datos del inspector 
                                 */
                                var resultado_google = ('resultado_google' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['resultado_google'] : '';
                                var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
                                var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
                                var ID_1614590099 = {};
                                ID_1614590099.success = function(e) {
                                    var elemento = e,
                                        valor = e;
                                    /** 
                                     * Si la consulta al servidor fue exitosa, limpiamos la tabla del inspector, guardamos los datos del binding y finalmente una consulta a la tabla para poder actualizar la variable del inspector 
                                     */
                                    /** 
                                     * Si la consulta al servidor fue exitosa, limpiamos la tabla del inspector, guardamos los datos del binding y finalmente una consulta a la tabla para poder actualizar la variable del inspector 
                                     */
                                    var ID_1088994422_i = Alloy.Collections.inspectores;
                                    var sql = "DELETE FROM " + ID_1088994422_i.config.adapter.collection_name;
                                    var db = Ti.Database.open(ID_1088994422_i.config.adapter.db_name);
                                    db.execute(sql);
                                    db.close();
                                    sql = null;
                                    db = null;
                                    ID_1088994422_i.trigger('delete');
                                    Alloy.Collections[$.otro_inspector.config.adapter.collection_name].add($.otro_inspector);
                                    $.otro_inspector.save();
                                    Alloy.Collections[$.otro_inspector.config.adapter.collection_name].fetch();
                                    var ID_986649692_i = Alloy.createCollection('inspectores');
                                    var ID_986649692_i_where = '';
                                    ID_986649692_i.fetch();
                                    var inspector = require('helper').query2array(ID_986649692_i);
                                    if (inspector && inspector.length) {}
                                    $.ID_1809462296.close();
                                    elemento = null, valor = null;
                                };
                                ID_1614590099.error = function(e) {
                                    var elemento = e,
                                        valor = e;
                                    /** 
                                     * Si hubo error, no actualizamos nada y solo mostramos un mensaje de que no se pudo actualizar la informacion 
                                     */
                                    var ID_1273605181_opts = ['Aceptar'];
                                    var ID_1273605181 = Ti.UI.createAlertDialog({
                                        title: 'Alerta',
                                        message: 'Problema de conexión por favor intentar después',
                                        buttonNames: ID_1273605181_opts
                                    });
                                    ID_1273605181.addEventListener('click', function(e) {
                                        var suu = ID_1273605181_opts[e.index];
                                        suu = null;
                                        e.source.removeEventListener("click", arguments.callee);
                                    });
                                    ID_1273605181.show();
                                    elemento = null, valor = null;
                                };
                                require('helper').ajaxUnico('ID_1614590099', '' + url_server + 'editarPerfilTipo1' + '', 'POST', {
                                    id_inspector: inspector.id_server,
                                    pais: inspector.pais,
                                    nivel1: data_nivel_1.id_server,
                                    nivel2: data_nivel_2,
                                    nivel3: data_nivel_3,
                                    nivel4: data_nivel_4,
                                    nivel5: data_nivel_5,
                                    direccion: direccion,
                                    correccion_direccion: 1,
                                    lat_dir: resultado_google.geometry.location.lat,
                                    lon_dir: resultado_google.geometry.location.lng,
                                    google: JSON.stringify(resultado_google.address_components)
                                }, 15000, ID_1614590099);
                            } else if (resp_pregunta == 'No') {
                                /** 
                                 * Dejamos los datos vacios para que el usuario vuelva a escribir la informacion 
                                 */
                                $.otro_inspector.set({
                                    lat_dir: '',
                                    lon_dir: '',
                                    direccion: ''
                                });
                                if ('otro_inspector' in $) $otro_inspector = $.otro_inspector.toJSON();
                                ID_1458205287.hide();
                            } else {
                                $.otro_inspector.set({
                                    google: '',
                                    lat_dir: '',
                                    lon_dir: ''
                                });
                                if ('otro_inspector' in $) $otro_inspector = $.otro_inspector.toJSON();
                            }
                            resp_pregunta = null;
                            e.source.removeEventListener("click", arguments.callee);
                        });
                        ID_1458205287.show();
                    }
                } else {
                    var ID_1095666084_opts = ['OK'];
                    var ID_1095666084 = Ti.UI.createAlertDialog({
                        title: 'Atencion',
                        message: 'Error al verificar la dirección, reintente',
                        buttonNames: ID_1095666084_opts
                    });
                    ID_1095666084.addEventListener('click', function(e) {
                        var xdd = ID_1095666084_opts[e.index];
                        xdd = null;
                        e.source.removeEventListener("click", arguments.callee);
                    });
                    ID_1095666084.show();
                }
                elemento = null, valor = null;
            };
            ID_1916620719.error = function(e) {
                var elemento = e,
                    valor = e;
                var ID_561870315_opts = ['OK'];
                var ID_561870315 = Ti.UI.createAlertDialog({
                    title: 'Atencion',
                    message: 'Error al conectarse a internet, reintente',
                    buttonNames: ID_561870315_opts
                });
                ID_561870315.addEventListener('click', function(e) {
                    var xdd = ID_561870315_opts[e.index];
                    xdd = null;
                    e.source.removeEventListener("click", arguments.callee);
                });
                ID_561870315.show();
                elemento = null, valor = null;
            };
            require('helper').ajaxUnico('ID_1916620719', 'https://maps.googleapis.com/maps/api/geocode/json?address=' + url + '&key=AIzaSyDdRVLOtyNvZUAleL0mDO-mAWd2M9AircQ', 'GET', {}, 15000, ID_1916620719);
        }
    } else {
        var ID_373093644_opts = ['Aceptar'];
        var ID_373093644 = Ti.UI.createAlertDialog({
            title: 'Alerta',
            message: 'No hay internet, no se pueden efectuar los cambios',
            buttonNames: ID_373093644_opts
        });
        ID_373093644.addEventListener('click', function(e) {
            var suu = ID_373093644_opts[e.index];
            suu = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_373093644.show();
    }
}

$.ID_2061977320.init({
    titulo: 'Editar domicilio',
    __id: 'ALL2061977320',
    avance: '',
    onclick: Click_ID_2115858906
});

function Click_ID_2115858906(e) {

    var evento = e;
    $.ID_2047500265.blur();
    $.ID_1167282670.blur();
    $.ID_1909855472.blur();
    $.ID_1388286681.blur();

}

$.ID_1452648891.init({
    titulo: 'SELECCIONE REGION',
    __id: 'ALL1452648891',
    left: 0,
    onrespuesta: Respuesta_ID_1849254911,
    campo: 'Region',
    onabrir: Abrir_ID_1074989478,
    right: 0,
    seleccione: 'seleccione region',
    onafterinit: Afterinit_ID_1729803962
});

function Afterinit_ID_1729803962(e) {

    var evento = e;
    /** 
     * Recuperamos los datos de la region del pais y mandamos informacion al widget 
     */
    var data_region = ('data_region' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['data_region'] : '';
    $.ID_1452648891.data({
        data: data_region
    });

}

function Abrir_ID_1074989478(e) {

    var evento = e;
    /** 
     * Desenfocamos campos de texto 
     */
    $.ID_2047500265.blur();
    $.ID_1167282670.blur();
    $.ID_1909855472.blur();
    $.ID_1388286681.blur();

}

function Respuesta_ID_1849254911(e) {

    var evento = e;
    /** 
     * Editamos el texto para mostrar la opcion seleccionada en widget 
     */
    $.ID_1452648891.labels({
        valor: evento.valor
    });
    /** 
     * Guardamos en la variable el id de la region seleccionada 
     */
    require('vars')[_var_scopekey]['data_nivel_1'] = evento.item;

}

(function() {
    var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
    var ID_1005238871 = {};
    ID_1005238871.success = function(e) {
        var elemento = e,
            valor = e;
        if (elemento == false || elemento == 'false') {
            /** 
             * Si en la consulta los datos obtenidos no existen, mostramos mensaje para decir que hubo problemas al obtener los datos. Caso contrario, limpiamos tablas y cargamos los datos obtenidos desde el servidor en las tablas 
             */
            var ID_1645377110_opts = ['Aceptar'];
            var ID_1645377110 = Ti.UI.createAlertDialog({
                title: 'Alerta',
                message: 'Problema de conexión por favor intentar después',
                buttonNames: ID_1645377110_opts
            });
            ID_1645377110.addEventListener('click', function(e) {
                var suu = ID_1645377110_opts[e.index];
                suu = null;
                e.source.removeEventListener("click", arguments.callee);
            });
            ID_1645377110.show();
        } else {
            var ID_1260190325_i = Alloy.Collections.pais;
            var sql = "DELETE FROM " + ID_1260190325_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_1260190325_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_1260190325_i.trigger('delete');
            var ID_1261928710_i = Alloy.Collections.nivel1;
            var sql = "DELETE FROM " + ID_1261928710_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_1261928710_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_1261928710_i.trigger('delete');
            var ID_1021432474_i = Alloy.Collections.experiencia_oficio;
            var sql = "DELETE FROM " + ID_1021432474_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_1021432474_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_1021432474_i.trigger('delete');
            var elemento_regiones = elemento.regiones;
            var ID_1442925833_m = Alloy.Collections.nivel1;
            var db_ID_1442925833 = Ti.Database.open(ID_1442925833_m.config.adapter.db_name);
            db_ID_1442925833.execute('BEGIN');
            _.each(elemento_regiones, function(ID_1442925833_fila, pos) {
                db_ID_1442925833.execute('INSERT INTO nivel1 (id_label, id_server, dif_horaria, id_pais) VALUES (?,?,?,?)', ID_1442925833_fila.subdivision_name, ID_1442925833_fila.id, 2, ID_1442925833_fila.id_pais);
            });
            db_ID_1442925833.execute('COMMIT');
            db_ID_1442925833.close();
            db_ID_1442925833 = null;
            ID_1442925833_m.trigger('change');
            var elemento_paises = elemento.paises;
            var ID_1750078656_m = Alloy.Collections.pais;
            var db_ID_1750078656 = Ti.Database.open(ID_1750078656_m.config.adapter.db_name);
            db_ID_1750078656.execute('BEGIN');
            _.each(elemento_paises, function(ID_1750078656_fila, pos) {
                db_ID_1750078656.execute('INSERT INTO pais (label_nivel2, moneda, nombre, label_nivel4, label_codigo_identificador, label_nivel3, id_server, label_nivel1, iso, sis_metrico, niveles_pais, id_pais, label_nivel5, lenguaje) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)', ID_1750078656_fila.nivel_2, ID_1750078656_fila.moneda, ID_1750078656_fila.nombre, ID_1750078656_fila.nivel_4, ID_1750078656_fila.label_codigo_identificador, ID_1750078656_fila.nivel_3, ID_1750078656_fila.id, ID_1750078656_fila.nivel_1, ID_1750078656_fila.iso, ID_1750078656_fila.sis_metrico, ID_1750078656_fila.niveles_pais, ID_1750078656_fila.idpais, ID_1750078656_fila.nivel_5, ID_1750078656_fila.lenguaje);
            });
            db_ID_1750078656.execute('COMMIT');
            db_ID_1750078656.close();
            db_ID_1750078656 = null;
            ID_1750078656_m.trigger('change');
            var elemento_experiencia_oficio = elemento.experiencia_oficio;
            var ID_1833041638_m = Alloy.Collections.experiencia_oficio;
            var db_ID_1833041638 = Ti.Database.open(ID_1833041638_m.config.adapter.db_name);
            db_ID_1833041638.execute('BEGIN');
            _.each(elemento_experiencia_oficio, function(ID_1833041638_fila, pos) {
                db_ID_1833041638.execute('INSERT INTO experiencia_oficio (nombre, id_server, id_pais) VALUES (?,?,?)', ID_1833041638_fila.nombre, ID_1833041638_fila.id, ID_1833041638_fila.idpais);
            });
            db_ID_1833041638.execute('COMMIT');
            db_ID_1833041638.close();
            db_ID_1833041638 = null;
            ID_1833041638_m.trigger('change');
        }
        elemento = null, valor = null;
    };
    ID_1005238871.error = function(e) {
        var elemento = e,
            valor = e;
        var ID_1903447670_opts = ['Aceptar'];
        var ID_1903447670 = Ti.UI.createAlertDialog({
            title: 'Alerta',
            message: 'Problema de conexión por favor intentar después',
            buttonNames: ID_1903447670_opts
        });
        ID_1903447670.addEventListener('click', function(e) {
            var suu = ID_1903447670_opts[e.index];
            suu = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1903447670.show();
        elemento = null, valor = null;
    };
    require('helper').ajaxUnico('ID_1005238871', '' + url_server + 'obtenerPais' + '', 'POST', {}, 15000, ID_1005238871);
    /** 
     * Consultamos la tabla del inspector para cargarla en binding, y guardamos en una variable el id del nivel_1 del inspector, y consultamos la tabla de pais para saber cuantos niveles tiene para desplegar la informacion correcta 
     */
    var ID_253176565_i = Alloy.createCollection('inspectores');
    var ID_253176565_i_where = '';
    ID_253176565_i.fetch();
    var inspector = require('helper').query2array(ID_253176565_i);
    require('vars')[_var_scopekey]['data_nivel_1'] = inspector[0].id_nivel1;
    if (Ti.App.deployType != 'production') console.log('carga de los datos del inspector', {
        "asd": inspector[0]
    });
    var ID_2084839912_i = Alloy.createCollection('pais');
    var ID_2084839912_i_where = 'id_server=\'' + inspector[0].pais + '\'';
    ID_2084839912_i.fetch({
        query: 'SELECT * FROM pais WHERE id_server=\'' + inspector[0].pais + '\''
    });
    var pais_iter = require('helper').query2array(ID_2084839912_i);
    var pais = pais_iter[0];
    if (Ti.App.deployType != 'production') console.log('detalle del pais', {
        "datos": pais
    });
    var niveles_pais = 1;
    $.ID_1452648891.labels({
        nivel1: pais.label_nivel1
    });
    if ((_.isObject(pais.label_nivel2) || _.isString(pais.label_nivel2)) && _.isEmpty(pais.label_nivel2)) {
        var scroll_view;
        scroll_view = $.ID_1242267269;
        var nivel_2_view;
        nivel_2_view = $.ID_1658344386;
        scroll_view.remove(nivel_2_view);
    } else {
        $.ID_2014154115.setText(pais.label_nivel2);

        $.ID_2047500265.setHintText('escribir ' + pais.label_nivel2);

        $.ID_2047500265.setValue(inspector[0].nivel2);

    }
    if ((_.isObject(pais.label_nivel3) || _.isString(pais.label_nivel3)) && _.isEmpty(pais.label_nivel3)) {
        var scroll_view;
        scroll_view = $.ID_1242267269;
        var nivel_3_view;
        nivel_3_view = $.ID_1034413585;
        scroll_view.remove(nivel_3_view);
    } else {
        $.ID_1011451716.setText(pais.label_nivel3);

        $.ID_1167282670.setHintText('escribir ' + pais.label_nivel3);

        $.ID_1167282670.setValue(inspector[0].nivel3);

    }
    if ((_.isObject(pais.label_nivel4) || _.isString(pais.label_nivel4)) && _.isEmpty(pais.label_nivel4)) {
        var scroll_view;
        scroll_view = $.ID_1242267269;
        var nivel_4_view;
        nivel_4_view = $.ID_1673020681;
        scroll_view.remove(nivel_4_view);
    } else {
        $.ID_1902806573.setText(pais.label_nivel4);

        $.ID_1909855472.setHintText('escribir ' + pais.label_nivel4);

        $.ID_1909855472.setValue(inspector[0].nivel4);

    }
    if ((_.isObject(pais.label_nivel5) || _.isString(pais.label_nivel5)) && _.isEmpty(pais.label_nivel5)) {
        var scroll_view;
        scroll_view = $.ID_1242267269;
        var nivel_5_view;
        nivel_5_view = $.ID_1065975049;
        scroll_view.remove(nivel_5_view);
    } else {
        label_texto = pais.label_nivel5;
        $.ID_1516842860.setText(pais.label_nivel5);

        $.ID_1388286681.setHintText('escribir ' + pais.label_nivel5);

        $.ID_1388286681.setValue(inspector[0].nivel5);

    }
    $.ID_1762146332.setValue(inspector[0].direccion);

    var ID_2115988721_i = Alloy.createCollection('nivel1');
    var ID_2115988721_i_where = 'id_pais=\'' + pais.id_pais + '\'';
    ID_2115988721_i.fetch({
        query: 'SELECT * FROM nivel1 WHERE id_pais=\'' + pais.id_pais + '\''
    });
    var region_list = require('helper').query2array(ID_2115988721_i);
    var item_index = 0;
    _.each(region_list, function(item, item_pos, item_list) {
        item_index += 1;
        item.valor = item.id_label;
    });
    require('vars')[_var_scopekey]['data_region'] = region_list;
    var ID_1122488460_func = function() {
        var ID_1102749342_i = Alloy.createCollection('nivel1');
        var ID_1102749342_i_where = 'id_pais=\'' + pais.id_pais + '\' AND id_server=\'' + inspector[0].id_nivel1 + '\'';
        ID_1102749342_i.fetch({
            query: 'SELECT * FROM nivel1 WHERE id_pais=\'' + pais.id_pais + '\' AND id_server=\'' + inspector[0].id_nivel1 + '\''
        });
        var dato_n1 = require('helper').query2array(ID_1102749342_i);
        if (Ti.App.deployType != 'production') console.log('detalle de dato_n1', {
            "datos": dato_n1[0]
        });
        if (Ti.App.deployType != 'production') console.log('detalle de dato_n1', {
            "datos": dato_n1[0].id_label
        });
        $.ID_1452648891.labels({
            valor: dato_n1[0].id_label
        });
    };
    var ID_1122488460 = setTimeout(ID_1122488460_func, 1000 * 0.2);
    require('vars')[_var_scopekey]['pais'] = pais;
    $.otro_inspector.set({
        apellido_materno: inspector[0].apellido_materno,
        id_nivel1: inspector[0].id_nivel1,
        lat_dir: inspector[0].lat_dir,
        disponibilidad_viajar_pais: inspector[0].disponibilidad_viajar_pais,
        uuid: inspector[0].uidd,
        fecha_nacimiento: inspector[0].fecha_nacimiento,
        d1: inspector[0].d1,
        d2: inspector[0].d2,
        password: inspector[0].password,
        pais: inspector[0].pais,
        direccion: inspector[0].direccion,
        d3: inspector[0].d3,
        nivel3: inspector[0].nivel3,
        d5: inspector[0].d5,
        d4: inspector[0].d4,
        disponibilidad_fechas: inspector[0].disponibilidad_fechas,
        d7: inspector[0].d7,
        nivel4: inspector[0].nivel4,
        nombre: inspector[0].nombre,
        nivel5: inspector[0].nivel5,
        nivel2: inspector[0].nivel2,
        disponibilidad_horas: inspector[0].disponibilidad_horas,
        disponibilidad_viajar_ciudad: inspector[0].disponibilidad_viajar_ciudad,
        lon_dir: inspector[0].lon_dir,
        id_server: inspector[0].id_server,
        d6: inspector[0].d6,
        telefono: inspector[0].telefono,
        experiencia_detalle: inspector[0].experiencia_detalle,
        disponibilidad: inspector[0].disponibilidad,
        codigo_identificador: inspector[0].codigo_identificador,
        experiencia_oficio: inspector[0].experiencia_oficio,
        direccion_correccion: inspector[0].direccion_correccion,
        apellido_paterno: inspector[0].apellido_paterno,
        correo: inspector[0].correo
    });
    if ('otro_inspector' in $) $otro_inspector = $.otro_inspector.toJSON();
    /** 
     * Guardamos variables con datos en blanco, esto sirve para poder hacer la consulta a google y mandar los datos que el usuario ingreso en la pantalla 
     */
    require('vars')[_var_scopekey]['data_nivel_1'] = '';
    require('vars')[_var_scopekey]['data_nivel_2'] = '';
    require('vars')[_var_scopekey]['data_nivel_3'] = '';
    require('vars')[_var_scopekey]['data_nivel_4'] = '';
    require('vars')[_var_scopekey]['data_nivel_5'] = '';
})();

function Postlayout_ID_1884260906(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var ID_515670629_func = function() {
        /** 
         * Una vez ya cargada la tarea, dejamos la variable limpia para poder abrir otras pantallas 
         */
        require('vars')['var_abriendo'] = '';
    };
    var ID_515670629 = setTimeout(ID_515670629_func, 1000 * 0.2);

}
if (OS_IOS || OS_ANDROID) {
    $.ID_1809462296.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}