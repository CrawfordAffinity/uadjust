var _bind4section = {};
var _list_templates = {};
var $contenido = $.contenido.toJSON();

var _activity;
if (OS_ANDROID) {
    _activity = $.ID_488097301.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_488097301';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_488097301.addEventListener('open', function(e) {});
}
$.ID_488097301.orientationModes = [Titanium.UI.PORTRAIT];


$.ID_758725651.init({
    titulo: 'NUEVO CONTENIDO',
    __id: 'ALL758725651',
    textoderecha: 'Guardar',
    oncerrar: Cerrar_ID_465184354,
    fondo: 'fondoceleste',
    top: 0,
    modal: '',
    onpresiono: Presiono_ID_324906638,
    colortextoderecha: 'blanco'
});

function Cerrar_ID_465184354(e) {

    var evento = e;
    /** 
     * Limpiamos widgets multiples (memoria ram) 
     */
    $.ID_1908023149.limpiar({});
    $.ID_1996770915.limpiar({});
    $.ID_1413999243.limpiar({});
    $.ID_488097301.close();

}

function Presiono_ID_324906638(e) {

    var evento = e;
    /** 
     * Obtenemos la fecha actual 
     */

    var hoy = new Date();
    if ($contenido.fecha_compra > hoy == true || $contenido.fecha_compra > hoy == 'true') {
        /** 
         * Validamos que los campos ingresados sean correctos 
         */
        var ID_1492161658_opts = ['Aceptar'];
        var ID_1492161658 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'La fecha de compra no puede ser superior a hoy',
            buttonNames: ID_1492161658_opts
        });
        ID_1492161658.addEventListener('click', function(e) {
            var nulo = ID_1492161658_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1492161658.show();
    } else if (_.isUndefined($contenido.nombre)) {
        var ID_1272791922_opts = ['Aceptar'];
        var ID_1272791922 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Seleccione producto',
            buttonNames: ID_1272791922_opts
        });
        ID_1272791922.addEventListener('click', function(e) {
            var nulo = ID_1272791922_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1272791922.show();
    } else if (_.isUndefined($contenido.id_marca)) {
        var ID_153336346_opts = ['Aceptar'];
        var ID_153336346 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Seleccione marca del producto',
            buttonNames: ID_153336346_opts
        });
        ID_153336346.addEventListener('click', function(e) {
            var nulo = ID_153336346_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_153336346.show();
    } else if (_.isUndefined($contenido.id_recinto)) {
        var ID_693653122_opts = ['Aceptar'];
        var ID_693653122 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Seleccione el recinto donde estaba el producto',
            buttonNames: ID_693653122_opts
        });
        ID_693653122.addEventListener('click', function(e) {
            var nulo = ID_693653122_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_693653122.show();
    } else if (_.isUndefined($contenido.cantidad)) {
        var ID_555055808_opts = ['Aceptar'];
        var ID_555055808 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese cantidad de productos',
            buttonNames: ID_555055808_opts
        });
        ID_555055808.addEventListener('click', function(e) {
            var nulo = ID_555055808_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_555055808.show();
    } else if ((_.isObject($contenido.cantidad) || _.isString($contenido.cantidad)) && _.isEmpty($contenido.cantidad)) {
        var ID_597377093_opts = ['Aceptar'];
        var ID_597377093 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese cantidad de productos',
            buttonNames: ID_597377093_opts
        });
        ID_597377093.addEventListener('click', function(e) {
            var nulo = ID_597377093_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_597377093.show();
    } else if (_.isUndefined($contenido.fecha_compra)) {
        var ID_759817000_opts = ['Aceptar'];
        var ID_759817000 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese fecha de compra aproximada (año)',
            buttonNames: ID_759817000_opts
        });
        ID_759817000.addEventListener('click', function(e) {
            var nulo = ID_759817000_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_759817000.show();
    } else if (_.isUndefined($contenido.id_moneda)) {
        var ID_1568938720_opts = ['Aceptar'];
        var ID_1568938720 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Seleccione el tipo de moneda del producto',
            buttonNames: ID_1568938720_opts
        });
        ID_1568938720.addEventListener('click', function(e) {
            var nulo = ID_1568938720_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1568938720.show();
    } else if (_.isUndefined($contenido.valor)) {
        var ID_1322743803_opts = ['Aceptar'];
        var ID_1322743803 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese valor del producto',
            buttonNames: ID_1322743803_opts
        });
        ID_1322743803.addEventListener('click', function(e) {
            var nulo = ID_1322743803_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1322743803.show();
    } else if ((_.isObject($contenido.valor) || _.isString($contenido.valor)) && _.isEmpty($contenido.valor)) {
        var ID_5058228_opts = ['Aceptar'];
        var ID_5058228 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese valor del producto',
            buttonNames: ID_5058228_opts
        });
        ID_5058228.addEventListener('click', function(e) {
            var nulo = ID_5058228_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_5058228.show();
    } else if (_.isUndefined($contenido.descripcion)) {
        var ID_15109800_opts = ['Aceptar'];
        var ID_15109800 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Describa el producto brevemente, con un mínimo de 30 caracteres',
            buttonNames: ID_15109800_opts
        });
        ID_15109800.addEventListener('click', function(e) {
            var nulo = ID_15109800_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_15109800.show();
    } else if (_.isNumber($contenido.descripcion.length) && _.isNumber(29) && $contenido.descripcion.length <= 29) {
        var ID_1758996172_opts = ['Aceptar'];
        var ID_1758996172 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Describa el producto brevemente, con un mínimo de 30 caracteres',
            buttonNames: ID_1758996172_opts
        });
        ID_1758996172.addEventListener('click', function(e) {
            var nulo = ID_1758996172_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1758996172.show();
    } else {
        /** 
         * Guardamos modelo nuevo 
         */
        Alloy.Collections[$.contenido.config.adapter.collection_name].add($.contenido);
        $.contenido.save();
        Alloy.Collections[$.contenido.config.adapter.collection_name].fetch();
        /** 
         * limpiamos widgets multiples (memoria ram) 
         */
        $.ID_1908023149.limpiar({});
        $.ID_1996770915.limpiar({});
        $.ID_1413999243.limpiar({});
        $.ID_488097301.close();
    }
}

function Click_ID_847020488(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    /** 
     * Ocupamos esta variable para impedir que la pantalla siguente se cargue dos veces 
     */
    var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
    if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
        require('vars')['var_abriendo'] = 'true';
        Alloy.createController("listadocontenidos", {}).getView().open();
    }

}

$.ID_1413999243.init({
    titulo: 'MARCAS',
    cargando: 'cargando ..',
    __id: 'ALL1413999243',
    left: 0,
    onrespuesta: Respuesta_ID_661260707,
    campo: 'Marca del Item',
    onabrir: Abrir_ID_1926204680,
    color: 'celeste',
    right: 0,
    seleccione: 'seleccione marca',
    activo: true,
    onafterinit: Afterinit_ID_1843803247
});

function Afterinit_ID_1843803247(e) {

    var evento = e;
    var ID_1686389527_func = function() {
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
            var ID_1416587127_i = Alloy.createCollection('marcas');
            var ID_1416587127_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
            ID_1416587127_i.fetch({
                query: 'SELECT * FROM marcas WHERE pais_texto=\'' + pais[0].nombre + '\''
            });
            var marcas = require('helper').query2array(ID_1416587127_i);
            var datos = [];
            _.each(marcas, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'valor';
                    if (llave == 'id_segured') newkey = 'id_interno';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                datos.push(new_row);
            });
        } else {
            var ID_91489196_i = Alloy.Collections.marcas;
            var sql = "DELETE FROM " + ID_91489196_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_91489196_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_91489196_i.trigger('delete');
            var item_index = 0;
            _.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
                item_index += 1;
                var ID_652253309_m = Alloy.Collections.marcas;
                var ID_652253309_fila = Alloy.createModel('marcas', {
                    nombre: 'HomeDepot' + item,
                    id_server: '0' + item,
                    id_segured: '10' + item,
                    pais: 1
                });
                ID_652253309_m.add(ID_652253309_fila);
                ID_652253309_fila.save();
            });
            var ID_1756640692_i = Alloy.createCollection('marcas');
            ID_1756640692_i.fetch();
            var ID_1756640692_src = require('helper').query2array(ID_1756640692_i);
            var datos = [];
            _.each(ID_1756640692_src, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'valor';
                    if (llave == 'id_segured') newkey = 'id_interno';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                datos.push(new_row);
            });
        }
        $.ID_1413999243.data({
            data: datos
        });
    };
    var ID_1686389527 = setTimeout(ID_1686389527_func, 1000 * 0.2);

}

function Abrir_ID_1926204680(e) {

    var evento = e;

}

function Respuesta_ID_661260707(e) {

    var evento = e;
    $.ID_1413999243.labels({
        valor: evento.valor
    });
    $.contenido.set({
        id_marca: evento.item.id_interno
    });
    if ('contenido' in $) $contenido = $.contenido.toJSON();
    if (Ti.App.deployType != 'production') console.log('datos recibidos de modal marca item', {
        "datos": evento
    });

}

$.ID_1420842518.init({
    caja: 55,
    __id: 'ALL1420842518',
    onlisto: Listo_ID_1674339813,
    onclick: Click_ID_639064828
});

function Click_ID_639064828(e) {

    var evento = e;
    /** 
     * Definimos que estamos capturando foto en el primer item 
     */
    require('vars')['cual_foto'] = '1';
    /** 
     * Abrimos camara 
     */
    $.ID_544827748.disparar({});
    /** 
     * Detenemos las animaciones de los widget 
     */
    $.ID_1818394314.detener({});
    $.ID_745114037.detener({});
    $.ID_1420842518.detener({});

}

function Listo_ID_1674339813(e) {

    var evento = e;
    /** 
     * Recuperamos variables para poder definir la estructura de las carpetas donde se guardara la miniatura 
     */
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    var nuevoid_f1 = ('nuevoid_f1' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['nuevoid_f1'] : '';
    if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
        var ID_1194766412_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + seltarea.id_server);
        if (ID_1194766412_d.exists() == false) ID_1194766412_d.createDirectory();
        var ID_1194766412_f = Ti.Filesystem.getFile(ID_1194766412_d.resolve(), 'mini' + nuevoid_f1.id + '.jpg');
        if (ID_1194766412_f.exists() == true) ID_1194766412_f.deleteFile();
        ID_1194766412_f.write(evento.mini);
        ID_1194766412_d = null;
        ID_1194766412_f = null;
    } else {
        var ID_1143267424_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + 0);
        if (ID_1143267424_d.exists() == false) ID_1143267424_d.createDirectory();
        var ID_1143267424_f = Ti.Filesystem.getFile(ID_1143267424_d.resolve(), 'mini' + nuevoid_f1.id + '.jpg');
        if (ID_1143267424_f.exists() == true) ID_1143267424_f.deleteFile();
        ID_1143267424_f.write(evento.mini);
        ID_1143267424_d = null;
        ID_1143267424_f = null;
    }
}

$.ID_1818394314.init({
    caja: 55,
    __id: 'ALL1818394314',
    onlisto: Listo_ID_543349695,
    onclick: Click_ID_1261640872
});

function Click_ID_1261640872(e) {

    var evento = e;
    $.ID_1420842518.detener({});
    $.ID_745114037.detener({});
    $.ID_1818394314.detener({});
    require('vars')['cual_foto'] = 2;
    $.ID_544827748.disparar({});

}

function Listo_ID_543349695(e) {

    var evento = e;
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    var nuevoid_f2 = ('nuevoid_f2' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['nuevoid_f2'] : '';
    if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
        var ID_766059429_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + seltarea.id_server);
        if (ID_766059429_d.exists() == false) ID_766059429_d.createDirectory();
        var ID_766059429_f = Ti.Filesystem.getFile(ID_766059429_d.resolve(), 'mini' + nuevoid_f2.id + '.jpg');
        if (ID_766059429_f.exists() == true) ID_766059429_f.deleteFile();
        ID_766059429_f.write(evento.mini);
        ID_766059429_d = null;
        ID_766059429_f = null;
    } else {
        var ID_1011270433_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + 0);
        if (ID_1011270433_d.exists() == false) ID_1011270433_d.createDirectory();
        var ID_1011270433_f = Ti.Filesystem.getFile(ID_1011270433_d.resolve(), 'mini' + nuevoid_f2.id + '.jpg');
        if (ID_1011270433_f.exists() == true) ID_1011270433_f.deleteFile();
        ID_1011270433_f.write(evento.mini);
        ID_1011270433_d = null;
        ID_1011270433_f = null;
    }
}

$.ID_745114037.init({
    caja: 55,
    __id: 'ALL745114037',
    onlisto: Listo_ID_816038514,
    onclick: Click_ID_1587856648
});

function Click_ID_1587856648(e) {

    var evento = e;
    $.ID_1420842518.detener({});
    $.ID_1818394314.detener({});
    $.ID_745114037.detener({});
    require('vars')['cual_foto'] = 3;
    $.ID_544827748.disparar({});

}

function Listo_ID_816038514(e) {

    var evento = e;
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    var nuevoid_f3 = ('nuevoid_f3' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['nuevoid_f3'] : '';
    if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
        var ID_944576973_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + seltarea.id_server);
        if (ID_944576973_d.exists() == false) ID_944576973_d.createDirectory();
        var ID_944576973_f = Ti.Filesystem.getFile(ID_944576973_d.resolve(), 'mini' + nuevoid_f3.id + '.jpg');
        if (ID_944576973_f.exists() == true) ID_944576973_f.deleteFile();
        ID_944576973_f.write(evento.mini);
        ID_944576973_d = null;
        ID_944576973_f = null;
    } else {
        var ID_1878835932_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + 0);
        if (ID_1878835932_d.exists() == false) ID_1878835932_d.createDirectory();
        var ID_1878835932_f = Ti.Filesystem.getFile(ID_1878835932_d.resolve(), 'mini' + nuevoid_f3.id + '.jpg');
        if (ID_1878835932_f.exists() == true) ID_1878835932_f.deleteFile();
        ID_1878835932_f.write(evento.mini);
        ID_1878835932_d = null;
        ID_1878835932_f = null;
    }
}

$.ID_1908023149.init({
    titulo: 'RECINTOS',
    cargando: 'cargando...',
    __id: 'ALL1908023149',
    onrespuesta: Respuesta_ID_1901278964,
    campo: 'Ubicación',
    onabrir: Abrir_ID_202281478,
    color: 'celeste',
    seleccione: 'recinto',
    activo: true,
    onafterinit: Afterinit_ID_1459788538
});

function Afterinit_ID_1459788538(e) {

    var evento = e;
    var ID_590204985_func = function() {
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
            var ID_17383129_i = Alloy.createCollection('insp_recintos');
            var ID_17383129_i_where = 'id_inspeccion=\'' + seltarea.id_server + '\'';
            ID_17383129_i.fetch({
                query: 'SELECT * FROM insp_recintos WHERE id_inspeccion=\'' + seltarea.id_server + '\''
            });
            var recintos = require('helper').query2array(ID_17383129_i);
            var datos = [];
            _.each(recintos, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'valor';
                    if (llave == 'id_recinto') newkey = 'id_interno';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                datos.push(new_row);
            });
        } else {
            var ID_727668450_i = Alloy.Collections.insp_recintos;
            var sql = "DELETE FROM " + ID_727668450_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_727668450_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_727668450_i.trigger('delete');
            var item_index = 0;
            _.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
                item_index += 1;
                var ID_1452773782_m = Alloy.Collections.insp_recintos;
                var ID_1452773782_fila = Alloy.createModel('insp_recintos', {
                    nombre: 'Pieza' + item,
                    id_recinto: item
                });
                ID_1452773782_m.add(ID_1452773782_fila);
                ID_1452773782_fila.save();
            });
            var ID_134991602_i = Alloy.createCollection('insp_recintos');
            ID_134991602_i.fetch();
            var ID_134991602_src = require('helper').query2array(ID_134991602_i);
            var datos = [];
            _.each(ID_134991602_src, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'valor';
                    if (llave == 'id_recinto') newkey = 'id_interno';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                datos.push(new_row);
            });
        }
        $.ID_1908023149.data({
            data: datos
        });
    };
    var ID_590204985 = setTimeout(ID_590204985_func, 1000 * 0.1);

}

function Abrir_ID_202281478(e) {

    var evento = e;

}

function Respuesta_ID_1901278964(e) {

    var evento = e;
    $.ID_1908023149.labels({
        valor: evento.valor
    });
    $.contenido.set({
        id_recinto: evento.item.id_interno
    });
    if ('contenido' in $) $contenido = $.contenido.toJSON();
    if (Ti.App.deployType != 'production') console.log('datos recibidos de modal ubicacion', {
        "datos": evento
    });

}

function Change_ID_1529362900(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    $.contenido.set({
        cantidad: elemento
    });
    if ('contenido' in $) $contenido = $.contenido.toJSON();
    elemento = null, source = null;

}

function Click_ID_1779921517(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    $.ID_1266631229.blur();
    $.ID_1205471497.blur();
    $.ID_662931237.blur();
    $.ID_1585064981.abrir({});

}

$.ID_1996770915.init({
    titulo: 'MONEDAS',
    cargando: 'cargando...',
    __id: 'ALL1996770915',
    onrespuesta: Respuesta_ID_450691452,
    campo: 'Moneda',
    onabrir: Abrir_ID_774637536,
    color: 'celeste',
    seleccione: '-',
    activo: true,
    onafterinit: Afterinit_ID_1097145720
});

function Afterinit_ID_1097145720(e) {

    var evento = e;
    var ID_1973605241_func = function() {
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
            var ID_520270707_i = Alloy.createCollection('monedas');
            var ID_520270707_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
            ID_520270707_i.fetch({
                query: 'SELECT * FROM monedas WHERE pais_texto=\'' + pais[0].nombre + '\''
            });
            var monedas = require('helper').query2array(ID_520270707_i);
            var datos = [];
            _.each(monedas, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'valor';
                    if (llave == 'id_segured') newkey = 'id_interno';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                datos.push(new_row);
            });
        } else {
            var ID_781747413_i = Alloy.Collections.monedas;
            var sql = "DELETE FROM " + ID_781747413_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_781747413_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_781747413_i.trigger('delete');
            var item_index = 0;
            _.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
                item_index += 1;
                var ID_1313163701_m = Alloy.Collections.monedas;
                var ID_1313163701_fila = Alloy.createModel('monedas', {
                    nombre: 'CLP' + item,
                    id_segured: '10' + item
                });
                ID_1313163701_m.add(ID_1313163701_fila);
                ID_1313163701_fila.save();
            });
            var ID_1532241386_i = Alloy.createCollection('monedas');
            ID_1532241386_i.fetch();
            var ID_1532241386_src = require('helper').query2array(ID_1532241386_i);
            var datos = [];
            _.each(ID_1532241386_src, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'valor';
                    if (llave == 'id_segured') newkey = 'id_interno';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                datos.push(new_row);
            });
        }
        $.ID_1996770915.data({
            data: datos
        });
    };
    var ID_1973605241 = setTimeout(ID_1973605241_func, 1000 * 0.1);

}

function Abrir_ID_774637536(e) {

    var evento = e;
    $.ID_1585064981.cerrar({});

}

function Respuesta_ID_450691452(e) {

    var evento = e;
    $.ID_1996770915.labels({
        valor: evento.valor
    });
    $.contenido.set({
        id_moneda: evento.item.id_interno
    });
    if ('contenido' in $) $contenido = $.contenido.toJSON();
    if (Ti.App.deployType != 'production') console.log('datos recibidos de modal monedas', {
        "datos": evento
    });

}

function Change_ID_767645452(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    $.contenido.set({
        valor: elemento
    });
    if ('contenido' in $) $contenido = $.contenido.toJSON();
    elemento = null, source = null;

}

function Return_ID_1956331658(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    $.ID_1205471497.blur();
    elemento = null, source = null;

}

function Change_ID_1966754296(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    if (elemento == true || elemento == 'true') {
        var si = 'SI';
        $.ID_324890359.setText(si);

        $.contenido.set({
            recupero: 1
        });
        if ('contenido' in $) $contenido = $.contenido.toJSON();
    } else {
        var no = 'NO';
        $.ID_324890359.setText(no);

        $.contenido.set({
            recupero: 0
        });
        if ('contenido' in $) $contenido = $.contenido.toJSON();
    }
    elemento = null;

}

function Change_ID_1552750607(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    $.contenido.set({
        descripcion: elemento
    });
    if ('contenido' in $) $contenido = $.contenido.toJSON();
    elemento = null, source = null;

}

$.ID_544827748.init({
    onfotolista: Fotolista_ID_358045397,
    __id: 'ALL544827748'
});

function Fotolista_ID_358045397(e) {

    var evento = e;
    /** 
     * Recuperamos cual fue la foto seleccionada 
     */
    var cual_foto = ('cual_foto' in require('vars')) ? require('vars')['cual_foto'] : '';
    if (cual_foto == 1 || cual_foto == '1') {
        var ID_1055410876_m = Alloy.Collections.numero_unico;
        var ID_1055410876_fila = Alloy.createModel('numero_unico', {
            comentario: 'nuevo danocontenido foto1'
        });
        ID_1055410876_m.add(ID_1055410876_fila);
        ID_1055410876_fila.save();
        var nuevoid_f1 = require('helper').model2object(ID_1055410876_m.last());
        /** 
         * Guardamos en variable el detalle del modelo numero_unico para asignar informacion de foto1 del dano 
         */
        require('vars')[_var_scopekey]['nuevoid_f1'] = nuevoid_f1;
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        var gps_coords = ('gps_coords' in require('vars')) ? require('vars')['gps_coords'] : '';
        var gps_latitud = ('gps_latitud' in require('vars')) ? require('vars')['gps_latitud'] : '';
        var gps_longitud = ('gps_longitud' in require('vars')) ? require('vars')['gps_longitud'] : '';
        var resultado = null;
        if ('distanceinmtbetweenearthcoordinates' in require('funciones')) {
            resultado = require('funciones').distanceinmtbetweenearthcoordinates({
                'lat1': gps_latitud,
                'lon1': gps_longitud,
                'lat2': seltarea.lat,
                'lon2': seltarea.lon
            });
        } else {
            try {
                resultado = f_distanceinmtbetweenearthcoordinates({
                    'lat1': gps_latitud,
                    'lon1': gps_longitud,
                    'lat2': seltarea.lat,
                    'lon2': seltarea.lon
                });
            } catch (ee) {}
        }
        if (_.isNumber(gps_coords.accuracy) && _.isNumber(resultado) && gps_coords.accuracy > resultado) {
            /** 
             * Pablo me comento que el flag fuera_rango se determina comparando precision vs la distancia de ubicacion con la definida en tarea 
             */
            var json_datosfoto = {
                lat: gps_latitud,
                lng: gps_longitud,
                fuera_rango: 0,
                distancia: Math.round(resultado),
                precision: Math.round(gps_coords.accuracy)
            };
        } else {
            var json_datosfoto = {
                lat: gps_latitud,
                lng: gps_longitud,
                fuera_rango: 1,
                distancia: Math.round(resultado),
                precision: Math.round(gps_coords.accuracy)
            };
        }
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var ID_486747829_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
            if (ID_486747829_d.exists() == false) ID_486747829_d.createDirectory();
            var ID_486747829_f = Ti.Filesystem.getFile(ID_486747829_d.resolve(), 'cap' + nuevoid_f1.id + '.jpg');
            if (ID_486747829_f.exists() == true) ID_486747829_f.deleteFile();
            ID_486747829_f.write(evento.foto);
            ID_486747829_d = null;
            ID_486747829_f = null;
            var ID_1134111687_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
            if (ID_1134111687_d.exists() == false) ID_1134111687_d.createDirectory();
            var ID_1134111687_f = Ti.Filesystem.getFile(ID_1134111687_d.resolve(), 'cap' + nuevoid_f1.id + '.json');
            if (ID_1134111687_f.exists() == true) ID_1134111687_f.deleteFile();
            console.log('contenido f1 ncontenido', JSON.stringify(json_datosfoto));
            ID_1134111687_f.write(JSON.stringify(json_datosfoto));
            ID_1134111687_d = null;
            ID_1134111687_f = null;
        } else {
            var ID_1981240202_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + 0);
            if (ID_1981240202_d.exists() == false) ID_1981240202_d.createDirectory();
            var ID_1981240202_f = Ti.Filesystem.getFile(ID_1981240202_d.resolve(), 'cap' + nuevoid_f1.id + '.jpg');
            if (ID_1981240202_f.exists() == true) ID_1981240202_f.deleteFile();
            ID_1981240202_f.write(evento.foto);
            ID_1981240202_d = null;
            ID_1981240202_f = null;
        }
        /** 
         * Actualizamos el modelo indicando cual es el nombre de la imagen 
         */
        $.contenido.set({
            foto1: 'cap' + nuevoid_f1.id + '.jpg'
        });
        if ('contenido' in $) $contenido = $.contenido.toJSON();
        /** 
         * Enviamos la foto para que genere la miniatura (y tambien la guarde) y muestre en pantalla 
         */
        $.ID_1420842518.procesar({
            imagen: evento.foto,
            nueva: 640,
            calidad: 91,
            camara: 'trasera'
        });
        /** 
         * Limpiamos memoria ram 
         */
        evento = null;
    } else if (cual_foto == 2) {
        var ID_1877472109_m = Alloy.Collections.numero_unico;
        var ID_1877472109_fila = Alloy.createModel('numero_unico', {
            comentario: 'nuevo danocontenido foto2'
        });
        ID_1877472109_m.add(ID_1877472109_fila);
        ID_1877472109_fila.save();
        var nuevoid_f2 = require('helper').model2object(ID_1877472109_m.last());
        /** 
         * Guardamos en variable el detalle del modelo numero_unico para asignar informacion de foto1 del dano 
         */
        require('vars')[_var_scopekey]['nuevoid_f2'] = nuevoid_f2;
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        var gps_coords = ('gps_coords' in require('vars')) ? require('vars')['gps_coords'] : '';
        var gps_latitud = ('gps_latitud' in require('vars')) ? require('vars')['gps_latitud'] : '';
        var gps_longitud = ('gps_longitud' in require('vars')) ? require('vars')['gps_longitud'] : '';
        var resultado = null;
        if ('distanceinmtbetweenearthcoordinates' in require('funciones')) {
            resultado = require('funciones').distanceinmtbetweenearthcoordinates({
                'lat1': gps_latitud,
                'lon1': gps_longitud,
                'lat2': seltarea.lat,
                'lon2': seltarea.lon
            });
        } else {
            try {
                resultado = f_distanceinmtbetweenearthcoordinates({
                    'lat1': gps_latitud,
                    'lon1': gps_longitud,
                    'lat2': seltarea.lat,
                    'lon2': seltarea.lon
                });
            } catch (ee) {}
        }
        if (_.isNumber(gps_coords.accuracy) && _.isNumber(resultado) && gps_coords.accuracy > resultado) {
            /** 
             * Pablo me comento que el flag fuera_rango se determina comparando precision vs la distancia de ubicacion con la definida en tarea 
             */
            var json_datosfoto = {
                lat: gps_latitud,
                lng: gps_longitud,
                fuera_rango: 0,
                distancia: Math.round(resultado),
                precision: Math.round(gps_coords.accuracy)
            };
        } else {
            var json_datosfoto = {
                lat: gps_latitud,
                lng: gps_longitud,
                fuera_rango: 1,
                distancia: Math.round(resultado),
                precision: Math.round(gps_coords.accuracy)
            };
        }
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var ID_365394566_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
            if (ID_365394566_d.exists() == false) ID_365394566_d.createDirectory();
            var ID_365394566_f = Ti.Filesystem.getFile(ID_365394566_d.resolve(), 'cap' + nuevoid_f2.id + '.jpg');
            if (ID_365394566_f.exists() == true) ID_365394566_f.deleteFile();
            ID_365394566_f.write(evento.foto);
            ID_365394566_d = null;
            ID_365394566_f = null;
            var ID_370706149_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
            if (ID_370706149_d.exists() == false) ID_370706149_d.createDirectory();
            var ID_370706149_f = Ti.Filesystem.getFile(ID_370706149_d.resolve(), 'cap' + nuevoid_f2.id + '.json');
            if (ID_370706149_f.exists() == true) ID_370706149_f.deleteFile();
            console.log('contenido f2 ncontenido', JSON.stringify(json_datosfoto));
            ID_370706149_f.write(JSON.stringify(json_datosfoto));
            ID_370706149_d = null;
            ID_370706149_f = null;
        } else {
            var ID_1990857449_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + 0);
            if (ID_1990857449_d.exists() == false) ID_1990857449_d.createDirectory();
            var ID_1990857449_f = Ti.Filesystem.getFile(ID_1990857449_d.resolve(), 'cap' + nuevoid_f2.id + '.jpg');
            if (ID_1990857449_f.exists() == true) ID_1990857449_f.deleteFile();
            ID_1990857449_f.write(evento.foto);
            ID_1990857449_d = null;
            ID_1990857449_f = null;
        }
        $.contenido.set({
            foto2: 'cap' + nuevoid_f2.id + '.jpg'
        });
        if ('contenido' in $) $contenido = $.contenido.toJSON();
        $.ID_1818394314.procesar({
            imagen: evento.foto,
            nueva: 640,
            calidad: 91,
            camara: 'trasera'
        });
        evento = null;
    } else if (cual_foto == 3) {
        var ID_574390726_m = Alloy.Collections.numero_unico;
        var ID_574390726_fila = Alloy.createModel('numero_unico', {
            comentario: 'nuevo danocontenido foto3'
        });
        ID_574390726_m.add(ID_574390726_fila);
        ID_574390726_fila.save();
        var nuevoid_f3 = require('helper').model2object(ID_574390726_m.last());
        /** 
         * Guardamos en variable el detalle del modelo numero_unico para asignar informacion de foto1 del dano 
         */
        require('vars')[_var_scopekey]['nuevoid_f3'] = nuevoid_f3;
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        var gps_coords = ('gps_coords' in require('vars')) ? require('vars')['gps_coords'] : '';
        var gps_latitud = ('gps_latitud' in require('vars')) ? require('vars')['gps_latitud'] : '';
        var gps_longitud = ('gps_longitud' in require('vars')) ? require('vars')['gps_longitud'] : '';
        var resultado = null;
        if ('distanceinmtbetweenearthcoordinates' in require('funciones')) {
            resultado = require('funciones').distanceinmtbetweenearthcoordinates({
                'lat1': gps_latitud,
                'lon1': gps_longitud,
                'lat2': seltarea.lat,
                'lon2': seltarea.lon
            });
        } else {
            try {
                resultado = f_distanceinmtbetweenearthcoordinates({
                    'lat1': gps_latitud,
                    'lon1': gps_longitud,
                    'lat2': seltarea.lat,
                    'lon2': seltarea.lon
                });
            } catch (ee) {}
        }
        if (_.isNumber(gps_coords.accuracy) && _.isNumber(resultado) && gps_coords.accuracy > resultado) {
            /** 
             * Pablo me comento que el flag fuera_rango se determina comparando precision vs la distancia de ubicacion con la definida en tarea 
             */
            var json_datosfoto = {
                lat: gps_latitud,
                lng: gps_longitud,
                fuera_rango: 0,
                distancia: Math.round(resultado),
                precision: Math.round(gps_coords.accuracy)
            };
        } else {
            var json_datosfoto = {
                lat: gps_latitud,
                lng: gps_longitud,
                fuera_rango: 1,
                distancia: Math.round(resultado),
                precision: Math.round(gps_coords.accuracy)
            };
        }
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var ID_446863422_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
            if (ID_446863422_d.exists() == false) ID_446863422_d.createDirectory();
            var ID_446863422_f = Ti.Filesystem.getFile(ID_446863422_d.resolve(), 'cap' + nuevoid_f3.id + '.jpg');
            if (ID_446863422_f.exists() == true) ID_446863422_f.deleteFile();
            ID_446863422_f.write(evento.foto);
            ID_446863422_d = null;
            ID_446863422_f = null;
            var ID_603593117_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
            if (ID_603593117_d.exists() == false) ID_603593117_d.createDirectory();
            var ID_603593117_f = Ti.Filesystem.getFile(ID_603593117_d.resolve(), 'cap' + nuevoid_f3.id + '.json');
            if (ID_603593117_f.exists() == true) ID_603593117_f.deleteFile();
            console.log('contenido f3 ncontenido', JSON.stringify(json_datosfoto));
            ID_603593117_f.write(JSON.stringify(json_datosfoto));
            ID_603593117_d = null;
            ID_603593117_f = null;
        } else {
            var ID_475531772_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + 0);
            if (ID_475531772_d.exists() == false) ID_475531772_d.createDirectory();
            var ID_475531772_f = Ti.Filesystem.getFile(ID_475531772_d.resolve(), 'cap' + nuevoid_f2.id + '.jpg');
            if (ID_475531772_f.exists() == true) ID_475531772_f.deleteFile();
            ID_475531772_f.write(evento.foto);
            ID_475531772_d = null;
            ID_475531772_f = null;
        }
        $.contenido.set({
            foto3: 'cap' + nuevoid_f3.id + '.jpg'
        });
        if ('contenido' in $) $contenido = $.contenido.toJSON();
        $.ID_745114037.procesar({
            imagen: evento.foto,
            nueva: 640,
            calidad: 91,
            camara: 'trasera'
        });
        evento = null;
    }
}

$.ID_1585064981.init({
    __id: 'ALL1585064981',
    aceptar: 'Aceptar',
    cancelar: 'Cancelar',
    onaceptar: Aceptar_ID_1254289041,
    oncancelar: Cancelar_ID_873546370
});

function Aceptar_ID_1254289041(e) {

    var evento = e;
    /** 
     * Formateamos la fecha que responde el widget, mostramos en pantalla y actualizamos la fecha de compra 
     */
    var moment = require('alloy/moment');
    var ID_310768707 = evento.valor;
    var resp = moment(ID_310768707).format('DD-MM-YYYY');
    $.ID_1240322270.setText(resp);

    var ID_1240322270_estilo = 'estilo12';

    var _tmp_a4w = require('a4w');
    if ((typeof ID_1240322270_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (ID_1240322270_estilo in _tmp_a4w.styles['classes'])) {
        try {
            ID_1240322270_estilo = _tmp_a4w.styles['classes'][ID_1240322270_estilo];
        } catch (st_val_err) {}
    }
    _tmp_a4w = null;
    $.ID_1240322270.applyProperties(ID_1240322270_estilo);

    $.contenido.set({
        fecha_compra: evento.valor
    });
    if ('contenido' in $) $contenido = $.contenido.toJSON();

}

function Cancelar_ID_873546370(e) {

    var evento = e;
    if (Ti.App.deployType != 'production') console.log('selector de fecha cancelado', {});

}

(function() {
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
        /** 
         * Actualizamos el id de la inspeccion en la tabla de contenidos 
         */
        $.contenido.set({
            id_inspeccion: seltarea.id_server,
            recupero: 0
        });
        if ('contenido' in $) $contenido = $.contenido.toJSON();
    }
    /** 
     * Dejamos el scroll en el top y desenfocamos los campos de texto 
     */
    var ID_1356571988_func = function() {
        $.ID_842087781.scrollToTop();
        $.ID_1266631229.blur();
        $.ID_1205471497.blur();
        $.ID_662931237.blur();
    };
    var ID_1356571988 = setTimeout(ID_1356571988_func, 1000 * 0.2);
    /** 
     * Modificamos el statusbar 
     */
    var ID_338108629_func = function() {
        var ID_488097301_statusbar = '#239EC4';

        var setearStatusColor = function(ID_488097301_statusbar) {
            if (OS_IOS) {
                if (ID_488097301_statusbar == 'light' || ID_488097301_statusbar == 'claro') {
                    $.ID_488097301_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.LIGHT_CONTENT);
                } else if (ID_488097301_statusbar == 'grey' || ID_488097301_statusbar == 'gris' || ID_488097301_statusbar == 'gray') {
                    $.ID_488097301_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.GREY);
                } else if (ID_488097301_statusbar == 'oscuro' || ID_488097301_statusbar == 'dark') {
                    $.ID_488097301_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.DEFAULT);
                }
            } else if (OS_ANDROID) {
                abx.setStatusbarColor(ID_488097301_statusbar);
            }
        };
        setearStatusColor(ID_488097301_statusbar);

    };
    var ID_338108629 = setTimeout(ID_338108629_func, 1000 * 0.1);
    _my_events['resp_dato1,ID_1243902372'] = function(evento) {
        if (Ti.App.deployType != 'production') console.log('detalle de la respuesta', {
            "datos": evento
        });
        $.ID_800868967.setText(evento.nombre);

        $.ID_800868967.setColor('#000000');

        /** 
         * Asumimos que el valor mostrado seleccionado es el nombre de la fila dano, ya que no existe el campo nombre en esta pantalla. 
         */
        $.contenido.set({
            nombre: evento.nombre,
            id_nombre: evento.id_segured
        });
        if ('contenido' in $) $contenido = $.contenido.toJSON();
    };
    Alloy.Events.on('resp_dato1', _my_events['resp_dato1,ID_1243902372']);
})();

function Postlayout_ID_1011484147(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var ID_2081891065_func = function() {
        /** 
         * Una vez ya cargada la tarea, dejamos la variable limpia para poder abrir otras pantallas 
         */
        require('vars')['var_abriendo'] = '';
    };
    var ID_2081891065 = setTimeout(ID_2081891065_func, 1000 * 0.2);

}