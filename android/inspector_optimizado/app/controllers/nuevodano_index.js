var _bind4section = {};
var _list_templates = {
    "elemento": {
        "ID_432412973": {},
        "ID_291370846": {
            "text": "{nombre}"
        },
        "ID_1925487537": {
            "text": "{id_server}"
        },
        "ID_975786586": {
            "text": "{id_segured}"
        },
        "ID_1546190443": {}
    }
};
var $danos1 = $.danos1.toJSON();

var _activity;
if (OS_ANDROID) {
    _activity = $.ID_1540530499.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_1540530499';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_1540530499.addEventListener('open', function(e) {});
}
$.ID_1540530499.orientationModes = [Titanium.UI.PORTRAIT];


$.ID_1424700576.init({
    titulo: 'NUEVO DAÑO',
    __id: 'ALL1424700576',
    textoderecha: 'Guardar',
    oncerrar: Cerrar_ID_1349203717,
    fondo: 'fondomorado',
    top: 0,
    modal: '',
    onpresiono: Presiono_ID_1748880333,
    colortextoderecha: 'blanco'
});

function Cerrar_ID_1349203717(e) {

    var evento = e;
    $.ID_1540530499.close();
    /** 
     * Limpiamos widget 
     */
    $.ID_1867144665.limpiar({});
    $.ID_1384858606.limpiar({});

}

function Presiono_ID_1748880333(e) {

    var evento = e;
    if (_.isUndefined($danos1.id_partida)) {
        /** 
         * Validamos que lo ingresado sea correcto 
         */
        var ID_1069274406_opts = ['Aceptar'];
        var ID_1069274406 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Seleccione partida',
            buttonNames: ID_1069274406_opts
        });
        ID_1069274406.addEventListener('click', function(e) {
            var nulo = ID_1069274406_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1069274406.show();
    } else if ((_.isObject($danos1.id_tipodano) || _.isString($danos1.id_tipodano)) && _.isEmpty($danos1.id_tipodano)) {
        require('vars')[_var_scopekey]['todobien'] = 'false';
        var ID_289807824_opts = ['Aceptar'];
        var ID_289807824 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Seleccione el tipo de daño',
            buttonNames: ID_289807824_opts
        });
        ID_289807824.addEventListener('click', function(e) {
            var nulo = ID_289807824_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_289807824.show();
    } else if ((_.isObject($danos1.id_unidadmedida) || _.isString($danos1.id_unidadmedida)) && _.isEmpty($danos1.id_unidadmedida)) {
        require('vars')[_var_scopekey]['todobien'] = 'false';
        var ID_690784665_opts = ['Aceptar'];
        var ID_690784665 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Seleccione la unidad de medida del daño',
            buttonNames: ID_690784665_opts
        });
        ID_690784665.addEventListener('click', function(e) {
            var nulo = ID_690784665_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_690784665.show();
    } else if (_.isUndefined($danos1.id_tipodano)) {
        var ID_1948077288_opts = ['Aceptar'];
        var ID_1948077288 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Seleccione el tipo de daño',
            buttonNames: ID_1948077288_opts
        });
        ID_1948077288.addEventListener('click', function(e) {
            var nulo = ID_1948077288_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1948077288.show();
    } else if (_.isUndefined($danos1.id_unidadmedida)) {
        var ID_578337846_opts = ['Aceptar'];
        var ID_578337846 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Seleccione la unidad de medida del daño',
            buttonNames: ID_578337846_opts
        });
        ID_578337846.addEventListener('click', function(e) {
            var nulo = ID_578337846_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_578337846.show();
    } else if (_.isUndefined($danos1.superficie)) {
        require('vars')[_var_scopekey]['todobien'] = 'false';
        var ID_1575796091_opts = ['Aceptar'];
        var ID_1575796091 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese la cubicación del daño',
            buttonNames: ID_1575796091_opts
        });
        ID_1575796091.addEventListener('click', function(e) {
            var nulo = ID_1575796091_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1575796091.show();
    } else if ((_.isObject($danos1.superficie) || _.isString($danos1.superficie)) && _.isEmpty($danos1.superficie)) {
        require('vars')[_var_scopekey]['todobien'] = 'false';
        var ID_1920873984_opts = ['Aceptar'];
        var ID_1920873984 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese la cubicación del daño',
            buttonNames: ID_1920873984_opts
        });
        ID_1920873984.addEventListener('click', function(e) {
            var nulo = ID_1920873984_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1920873984.show();
    } else {
        /** 
         * Desenfocamos campos de texto 
         */
        $.ID_1650528667.blur();
        var ID_1804013171_func = function() {
            /** 
             * Guardamos en modelo el dano ingresado 
             */
            Alloy.Collections[$.danos1.config.adapter.collection_name].add($.danos1);
            $.danos1.save();
            Alloy.Collections[$.danos1.config.adapter.collection_name].fetch();
            /** 
             * Limpiamos widget 
             */
            $.ID_1867144665.limpiar({});
            $.ID_1384858606.limpiar({});
            $.ID_1540530499.close();
        };
        var ID_1804013171 = setTimeout(ID_1804013171_func, 1000 * 0.2);
    }
}

function Click_ID_776367526(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    /** 
     * Ocupamos esta variable para impedir que la pantalla siguente se cargue dos veces 
     */
    var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
    if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
        require('vars')['var_abriendo'] = 'true';
        Alloy.createController("listadodanos_index", {}).getView().open();
    }

}

$.ID_1384858606.init({
    titulo: 'TIPO DAÑO',
    cargando: 'cargando...',
    __id: 'ALL1384858606',
    left: 0,
    onrespuesta: Respuesta_ID_1736004744,
    campo: 'Tipo de Daño',
    onabrir: Abrir_ID_1523178070,
    color: 'morado',
    right: 0,
    seleccione: 'seleccione tipo',
    activo: true,
    onafterinit: Afterinit_ID_1072972205
});

function Abrir_ID_1523178070(e) {

    var evento = e;

}

function Respuesta_ID_1736004744(e) {

    var evento = e;
    /** 
     * Mostramos el valor seleccionado desde el widget 
     */
    $.ID_1384858606.labels({
        valor: evento.valor
    });
    /** 
     * Actualizamos el id_tipodano e id_unidadmedida 
     */
    $.danos1.set({
        id_tipodano: evento.item.id_interno,
        id_unidadmedida: ''
    });
    if ('danos1' in $) $danos1 = $.danos1.toJSON();
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
        /** 
         * Consultamos id_gerardo en tipo_accion, para obtener unidades de medida 
         */
        var ID_1752700999_i = Alloy.createCollection('tipo_accion');
        var ID_1752700999_i_where = 'id_tipo_dano=\'' + evento.item.id_gerardo + '\'';
        ID_1752700999_i.fetch({
            query: 'SELECT * FROM tipo_accion WHERE id_tipo_dano=\'' + evento.item.id_gerardo + '\''
        });
        var acciones = require('helper').query2array(ID_1752700999_i);
        if (acciones && acciones.length) {
            var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
            var ID_1138954688_i = Alloy.createCollection('unidad_medida');
            var ID_1138954688_i_where = 'pais_texto=\'' + pais[0].nombre + '\' AND id_accion=\'' + acciones[0].id_server + '\'';
            ID_1138954688_i.fetch({
                query: 'SELECT * FROM unidad_medida WHERE pais_texto=\'' + pais[0].nombre + '\' AND id_accion=\'' + acciones[0].id_server + '\''
            });
            var unidad = require('helper').query2array(ID_1138954688_i);
            var datos = [];
            _.each(unidad, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'valor';
                    if (llave == 'id_segured') newkey = 'id_interno';
                    if (llave == 'id_server') newkey = 'id_gerardo';
                    if (llave == 'abrev') newkey = 'abrev';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                datos.push(new_row);
            });
            $.ID_1867144665.data({
                data: datos
            });
        }
    }
    /** 
     * Actualizamos texto de widget unidad de medida 
     */
    $.ID_1867144665.labels({
        seleccione: 'unidad'
    });
    $.ID_1215120869.setValue('');

    $.ID_821385421.setText('-');


}

function Afterinit_ID_1072972205(e) {

    var evento = e;
    var ID_1312200333_func = function() {
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
            /** 
             * Obtenemos datos para selectores (solo los de este pais) 
             */
            var ID_1906304539_i = Alloy.createCollection('tipo_dano');
            var ID_1906304539_i_where = 'pais_texto=\'' + pais[0].nombre + '\' AND id_partida=0';
            ID_1906304539_i.fetch({
                query: 'SELECT * FROM tipo_dano WHERE pais_texto=\'' + pais[0].nombre + '\' AND id_partida=0'
            });
            var danos = require('helper').query2array(ID_1906304539_i);
            var datos = [];
            _.each(danos, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'valor';
                    if (llave == 'id_segured') newkey = 'id_interno';
                    if (llave == 'id_server') newkey = 'id_gerardo';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                datos.push(new_row);
            });
        } else {
            /** 
             * Insertamos dummies 
             */
            /** 
             * Insertamos dummies 
             */
            var ID_1225545214_i = Alloy.Collections.tipo_dano;
            var sql = "DELETE FROM " + ID_1225545214_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_1225545214_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_1225545214_i.trigger('delete');
            var item_index = 0;
            _.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
                item_index += 1;
                var ID_1705527813_m = Alloy.Collections.tipo_dano;
                var ID_1705527813_fila = Alloy.createModel('tipo_dano', {
                    nombre: 'Picada' + item,
                    id_partida: 0,
                    id_server: '0' + item,
                    id_segured: '10' + item,
                    pais: 1
                });
                ID_1705527813_m.add(ID_1705527813_fila);
                ID_1705527813_fila.save();
            });
            var ID_1234765394_i = Alloy.createCollection('tipo_dano');
            ID_1234765394_i.fetch();
            var ID_1234765394_src = require('helper').query2array(ID_1234765394_i);
            var datos = [];
            _.each(ID_1234765394_src, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'valor';
                    if (llave == 'id_segured') newkey = 'id_interno';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                datos.push(new_row);
            });
        }
        $.ID_1384858606.data({
            data: datos
        });
    };
    var ID_1312200333 = setTimeout(ID_1312200333_func, 1000 * 0.2);

}

$.ID_1070295433.init({
    caja: 45,
    __id: 'ALL1070295433',
    onlisto: Listo_ID_260362366,
    left: 0,
    top: 0,
    onclick: Click_ID_22885491
});

function Click_ID_22885491(e) {

    var evento = e;
    /** 
     * Definimos que estamos capturando foto en el primer item 
     */
    require('vars')[_var_scopekey]['cual_foto'] = '1';
    /** 
     * Abrimos camara 
     */
    $.ID_143760440.disparar({});
    /** 
     * Detenemos las animaciones de los widget 
     */
    $.ID_1464141874.detener({});
    $.ID_437144584.detener({});
    $.ID_1070295433.detener({});

}

function Listo_ID_260362366(e) {

    var evento = e;
    /** 
     * Recuperamos variables para poder definir la estructura de las carpetas donde se guardara la miniatura 
     */
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    var nuevoid_f1 = ('nuevoid_f1' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['nuevoid_f1'] : '';
    if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
        var ID_1482108514_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + seltarea.id_server);
        if (ID_1482108514_d.exists() == false) ID_1482108514_d.createDirectory();
        var ID_1482108514_f = Ti.Filesystem.getFile(ID_1482108514_d.resolve(), 'mini' + nuevoid_f1.id + '.jpg');
        if (ID_1482108514_f.exists() == true) ID_1482108514_f.deleteFile();
        ID_1482108514_f.write(evento.mini);
        ID_1482108514_d = null;
        ID_1482108514_f = null;
    } else {
        var ID_1791326058_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + 0);
        if (ID_1791326058_d.exists() == false) ID_1791326058_d.createDirectory();
        var ID_1791326058_f = Ti.Filesystem.getFile(ID_1791326058_d.resolve(), 'mini' + nuevoid_f1.id + '.jpg');
        if (ID_1791326058_f.exists() == true) ID_1791326058_f.deleteFile();
        ID_1791326058_f.write(evento.mini);
        ID_1791326058_d = null;
        ID_1791326058_f = null;
    }
}

$.ID_1464141874.init({
    caja: 45,
    __id: 'ALL1464141874',
    onlisto: Listo_ID_1804976504,
    left: 5,
    top: 0,
    onclick: Click_ID_1395201745
});

function Click_ID_1395201745(e) {

    var evento = e;
    require('vars')[_var_scopekey]['cual_foto'] = 2;
    $.ID_143760440.disparar({});
    $.ID_1070295433.detener({});
    $.ID_437144584.detener({});
    $.ID_1464141874.detener({});

}

function Listo_ID_1804976504(e) {

    var evento = e;
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    var nuevoid_f2 = ('nuevoid_f2' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['nuevoid_f2'] : '';
    if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
        var ID_1070774099_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + seltarea.id_server);
        if (ID_1070774099_d.exists() == false) ID_1070774099_d.createDirectory();
        var ID_1070774099_f = Ti.Filesystem.getFile(ID_1070774099_d.resolve(), 'mini' + nuevoid_f2.id + '.jpg');
        if (ID_1070774099_f.exists() == true) ID_1070774099_f.deleteFile();
        ID_1070774099_f.write(evento.mini);
        ID_1070774099_d = null;
        ID_1070774099_f = null;
    } else {
        var ID_1933980157_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + 0);
        if (ID_1933980157_d.exists() == false) ID_1933980157_d.createDirectory();
        var ID_1933980157_f = Ti.Filesystem.getFile(ID_1933980157_d.resolve(), 'mini' + nuevoid_f2.id + '.jpg');
        if (ID_1933980157_f.exists() == true) ID_1933980157_f.deleteFile();
        ID_1933980157_f.write(evento.mini);
        ID_1933980157_d = null;
        ID_1933980157_f = null;
    }
}

$.ID_437144584.init({
    caja: 45,
    __id: 'ALL437144584',
    onlisto: Listo_ID_503979492,
    left: 5,
    top: 0,
    onclick: Click_ID_1153916125
});

function Click_ID_1153916125(e) {

    var evento = e;
    require('vars')[_var_scopekey]['cual_foto'] = 3;
    $.ID_143760440.disparar({});
    $.ID_1070295433.detener({});
    $.ID_1464141874.detener({});
    $.ID_437144584.detener({});

}

function Listo_ID_503979492(e) {

    var evento = e;
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    var nuevoid_f3 = ('nuevoid_f3' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['nuevoid_f3'] : '';
    if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
        var ID_1536703765_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + seltarea.id_server);
        if (ID_1536703765_d.exists() == false) ID_1536703765_d.createDirectory();
        var ID_1536703765_f = Ti.Filesystem.getFile(ID_1536703765_d.resolve(), 'mini' + nuevoid_f3.id + '.jpg');
        if (ID_1536703765_f.exists() == true) ID_1536703765_f.deleteFile();
        ID_1536703765_f.write(evento.mini);
        ID_1536703765_d = null;
        ID_1536703765_f = null;
    } else {
        var ID_37771523_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + 0);
        if (ID_37771523_d.exists() == false) ID_37771523_d.createDirectory();
        var ID_37771523_f = Ti.Filesystem.getFile(ID_37771523_d.resolve(), 'mini' + nuevoid_f3.id + '.jpg');
        if (ID_37771523_f.exists() == true) ID_37771523_f.deleteFile();
        ID_37771523_f.write(evento.mini);
        ID_37771523_d = null;
        ID_37771523_f = null;
    }
}

$.ID_1867144665.init({
    titulo: 'UNIDAD',
    cargando: 'cargando...',
    __id: 'ALL1867144665',
    onrespuesta: Respuesta_ID_1952022010,
    campo: 'Unidad de medida',
    onabrir: Abrir_ID_1514381940,
    color: 'morado',
    seleccione: 'unidad',
    activo: true,
    onafterinit: Afterinit_ID_1748035591
});

function Abrir_ID_1514381940(e) {

    var evento = e;

}

function Respuesta_ID_1952022010(e) {

    var evento = e;
    /** 
     * Mostramos el valor seleccionado desde el widget 
     */
    $.ID_1867144665.labels({
        valor: evento.valor
    });
    /** 
     * Actualizamos el id de la unidad de medida 
     */
    $.danos1.set({
        id_unidadmedida: evento.item.id_interno
    });
    if ('danos1' in $) $danos1 = $.danos1.toJSON();
    /** 
     * modifica el label con la abreviatura de la unidad de medida seleccionada. 
     */
    $.ID_821385421.setText(evento.item.abrev);


}

function Afterinit_ID_1748035591(e) {

    var evento = e;
    var ID_631716302_func = function() {
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            /** 
             * Viene de login 
             */
            var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
            var ID_1602119787_i = Alloy.createCollection('unidad_medida');
            var ID_1602119787_i_where = 'pais_texto=\'' + pais[0].nombre + '\' AND id_accion=0';
            ID_1602119787_i.fetch({
                query: 'SELECT * FROM unidad_medida WHERE pais_texto=\'' + pais[0].nombre + '\' AND id_accion=0'
            });
            var unidad = require('helper').query2array(ID_1602119787_i);
            /** 
             * obtenemos datos para selectores (solo los de este pais) 
             */
            var datos = [];
            _.each(unidad, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'valor';
                    if (llave == 'id_segured') newkey = 'id_interno';
                    if (llave == 'id_server') newkey = 'id_gerardo';
                    if (llave == 'abrev') newkey = 'abrev';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                datos.push(new_row);
            });
        } else {
            /** 
             * Insertamos dummies 
             */
            /** 
             * Insertamos dummies 
             */
            var ID_1736176208_i = Alloy.Collections.unidad_medida;
            var sql = "DELETE FROM " + ID_1736176208_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_1736176208_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_1736176208_i.trigger('delete');
            var item_index = 0;
            _.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
                item_index += 1;
                var ID_1105591719_m = Alloy.Collections.unidad_medida;
                var ID_1105591719_fila = Alloy.createModel('unidad_medida', {
                    nombre: 'Metro lineal' + item,
                    id_server: '0' + item,
                    id_segured: '10' + item,
                    pais: 1,
                    abrev: 'ML' + item
                });
                ID_1105591719_m.add(ID_1105591719_fila);
                ID_1105591719_fila.save();
            });
            /** 
             * Transformamos nombres de tablas 
             */
            var ID_890084745_i = Alloy.createCollection('unidad_medida');
            ID_890084745_i.fetch();
            var ID_890084745_src = require('helper').query2array(ID_890084745_i);
            var datos = [];
            _.each(ID_890084745_src, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'valor';
                    if (llave == 'id_segured') newkey = 'id_interno';
                    if (llave == 'id_server') newkey = 'id_gerardo';
                    if (llave == 'abrev') newkey = 'abrev';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                datos.push(new_row);
            });
        }
        /** 
         * Cargamos el widget con los datos de tipo de unidad de medida 
         */
        $.ID_1867144665.data({
            data: datos
        });
    };
    var ID_631716302 = setTimeout(ID_631716302_func, 1000 * 0.2);

}

function Change_ID_1656305424(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    /** 
     * Al escribir la descripcion, vamos actualizando en la tabla de danos la superficie 
     */
    $.danos1.set({
        superficie: elemento
    });
    if ('danos1' in $) $danos1 = $.danos1.toJSON();
    elemento = null, source = null;

}

function Return_ID_655322173(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    /** 
     * Al presionar enter en el teclado del telefono, desenfocamos el campo 
     */
    $.ID_1215120869.blur();
    elemento = null, source = null;

}

function Change_ID_2057804827(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    /** 
     * Al escribir la descripcion, vamos actualizando en la tabla de danos la descripcion del dano 
     */
    $.danos1.set({
        descripcion_dano: elemento
    });
    if ('danos1' in $) $danos1 = $.danos1.toJSON();
    elemento = null, source = null;

}

$.ID_143760440.init({
    onfotolista: Fotolista_ID_640397343,
    __id: 'ALL143760440'
});

function Fotolista_ID_640397343(e) {

    var evento = e;
    /** 
     * Revisamos en que widget se solicito la foto 
     */
    var cual_foto = ('cual_foto' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['cual_foto'] : '';
    if (cual_foto == 1 || cual_foto == '1') {
        var ID_793909866_m = Alloy.Collections.numero_unico;
        var ID_793909866_fila = Alloy.createModel('numero_unico', {
            comentario: 'nuevo itemdano foto1'
        });
        ID_793909866_m.add(ID_793909866_fila);
        ID_793909866_fila.save();
        var nuevoid_f1 = require('helper').model2object(ID_793909866_m.last());
        /** 
         * Guardamos en variable el detalle del modelo numero_unico para asignar informacion de foto1 del dano 
         */
        require('vars')[_var_scopekey]['nuevoid_f1'] = nuevoid_f1;
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        var gps_coords = ('gps_coords' in require('vars')) ? require('vars')['gps_coords'] : '';
        var gps_latitud = ('gps_latitud' in require('vars')) ? require('vars')['gps_latitud'] : '';
        var gps_longitud = ('gps_longitud' in require('vars')) ? require('vars')['gps_longitud'] : '';
        var resultado = null;
        if ('distanceinmtbetweenearthcoordinates' in require('funciones')) {
            resultado = require('funciones').distanceinmtbetweenearthcoordinates({
                'lat1': gps_latitud,
                'lon1': gps_longitud,
                'lat2': seltarea.lat,
                'lon2': seltarea.lon
            });
        } else {
            try {
                resultado = f_distanceinmtbetweenearthcoordinates({
                    'lat1': gps_latitud,
                    'lon1': gps_longitud,
                    'lat2': seltarea.lat,
                    'lon2': seltarea.lon
                });
            } catch (ee) {}
        }
        if (_.isNumber(gps_coords.accuracy) && _.isNumber(resultado) && gps_coords.accuracy > resultado) {
            /** 
             * Pablo me comento que el flag fuera_rango se determina comparando precision vs la distancia de ubicacion con la definida en tarea 
             */
            var json_datosfoto = {
                lat: gps_latitud,
                lng: gps_longitud,
                fuera_rango: 0,
                distancia: Math.round(resultado),
                precision: Math.round(gps_coords.accuracy)
            };
        } else {
            var json_datosfoto = {
                lat: gps_latitud,
                lng: gps_longitud,
                fuera_rango: 1,
                distancia: Math.round(resultado),
                precision: Math.round(gps_coords.accuracy)
            };
        }
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var ID_1182736960_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
            if (ID_1182736960_d.exists() == false) ID_1182736960_d.createDirectory();
            var ID_1182736960_f = Ti.Filesystem.getFile(ID_1182736960_d.resolve(), 'cap' + nuevoid_f1.id + '.jpg');
            if (ID_1182736960_f.exists() == true) ID_1182736960_f.deleteFile();
            ID_1182736960_f.write(evento.foto);
            ID_1182736960_d = null;
            ID_1182736960_f = null;
            var ID_1103277653_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
            if (ID_1103277653_d.exists() == false) ID_1103277653_d.createDirectory();
            var ID_1103277653_f = Ti.Filesystem.getFile(ID_1103277653_d.resolve(), 'cap' + nuevoid_f1.id + '.json');
            if (ID_1103277653_f.exists() == true) ID_1103277653_f.deleteFile();
            console.log('contenido f1 ndano', JSON.stringify(json_datosfoto));
            ID_1103277653_f.write(JSON.stringify(json_datosfoto));
            ID_1103277653_d = null;
            ID_1103277653_f = null;
        } else {
            var ID_598482667_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + 0);
            if (ID_598482667_d.exists() == false) ID_598482667_d.createDirectory();
            var ID_598482667_f = Ti.Filesystem.getFile(ID_598482667_d.resolve(), 'cap' + nuevoid_f1.id + '.jpg');
            if (ID_598482667_f.exists() == true) ID_598482667_f.deleteFile();
            ID_598482667_f.write(evento.foto);
            ID_598482667_d = null;
            ID_598482667_f = null;
        }
        /** 
         * Actualizamos el modelo indicando cual es el nombre de la imagen 
         */
        $.danos1.set({
            foto1: 'cap' + nuevoid_f1.id + '.jpg'
        });
        if ('danos1' in $) $danos1 = $.danos1.toJSON();
        /** 
         * Enviamos la foto para que genere la miniatura (y tambien la guarde) y muestre en pantalla 
         */
        $.ID_1070295433.procesar({
            imagen: evento.foto,
            nueva: 640,
            calidad: 91,
            camara: 'trasera'
        });
        /** 
         * Limpiamos memoria ram 
         */
        evento = null;
    } else if (cual_foto == 2) {
        var ID_1250883617_m = Alloy.Collections.numero_unico;
        var ID_1250883617_fila = Alloy.createModel('numero_unico', {
            comentario: 'nuevo itemdano foto2'
        });
        ID_1250883617_m.add(ID_1250883617_fila);
        ID_1250883617_fila.save();
        var nuevoid_f2 = require('helper').model2object(ID_1250883617_m.last());
        /** 
         * Guardamos en variable el detalle del modelo numero_unico para asignar informacion de foto1 del dano 
         */
        require('vars')[_var_scopekey]['nuevoid_f2'] = nuevoid_f2;
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        var gps_coords = ('gps_coords' in require('vars')) ? require('vars')['gps_coords'] : '';
        var gps_latitud = ('gps_latitud' in require('vars')) ? require('vars')['gps_latitud'] : '';
        var gps_longitud = ('gps_longitud' in require('vars')) ? require('vars')['gps_longitud'] : '';
        var resultado = null;
        if ('distanceinmtbetweenearthcoordinates' in require('funciones')) {
            resultado = require('funciones').distanceinmtbetweenearthcoordinates({
                'lat1': gps_latitud,
                'lon1': gps_longitud,
                'lat2': seltarea.lat,
                'lon2': seltarea.lon
            });
        } else {
            try {
                resultado = f_distanceinmtbetweenearthcoordinates({
                    'lat1': gps_latitud,
                    'lon1': gps_longitud,
                    'lat2': seltarea.lat,
                    'lon2': seltarea.lon
                });
            } catch (ee) {}
        }
        if (_.isNumber(gps_coords.accuracy) && _.isNumber(resultado) && gps_coords.accuracy > resultado) {
            /** 
             * Pablo me comento que el flag fuera_rango se determina comparando precision vs la distancia de ubicacion con la definida en tarea 
             */
            var json_datosfoto = {
                lat: gps_latitud,
                lng: gps_longitud,
                fuera_rango: 0,
                distancia: Math.round(resultado),
                precision: Math.round(gps_coords.accuracy)
            };
        } else {
            var json_datosfoto = {
                lat: gps_latitud,
                lng: gps_longitud,
                fuera_rango: 1,
                distancia: Math.round(resultado),
                precision: Math.round(gps_coords.accuracy)
            };
        }
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var ID_1001672872_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
            if (ID_1001672872_d.exists() == false) ID_1001672872_d.createDirectory();
            var ID_1001672872_f = Ti.Filesystem.getFile(ID_1001672872_d.resolve(), 'cap' + nuevoid_f2.id + '.jpg');
            if (ID_1001672872_f.exists() == true) ID_1001672872_f.deleteFile();
            ID_1001672872_f.write(evento.foto);
            ID_1001672872_d = null;
            ID_1001672872_f = null;
            var ID_633641756_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
            if (ID_633641756_d.exists() == false) ID_633641756_d.createDirectory();
            var ID_633641756_f = Ti.Filesystem.getFile(ID_633641756_d.resolve(), 'cap' + nuevoid_f2.id + '.json');
            if (ID_633641756_f.exists() == true) ID_633641756_f.deleteFile();
            console.log('contenido f2 ndano', JSON.stringify(json_datosfoto));
            ID_633641756_f.write(JSON.stringify(json_datosfoto));
            ID_633641756_d = null;
            ID_633641756_f = null;
        } else {
            var ID_1224361304_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + 0);
            if (ID_1224361304_d.exists() == false) ID_1224361304_d.createDirectory();
            var ID_1224361304_f = Ti.Filesystem.getFile(ID_1224361304_d.resolve(), 'cap' + nuevoid_f1.id + '.jpg');
            if (ID_1224361304_f.exists() == true) ID_1224361304_f.deleteFile();
            ID_1224361304_f.write(evento.foto);
            ID_1224361304_d = null;
            ID_1224361304_f = null;
        }
        $.danos1.set({
            foto2: 'cap' + nuevoid_f2.id + '.jpg'
        });
        if ('danos1' in $) $danos1 = $.danos1.toJSON();
        $.ID_1464141874.procesar({
            imagen: evento.foto,
            nueva: 640,
            calidad: 91,
            camara: 'trasera'
        });
        evento = null;
    } else if (cual_foto == 3) {
        var ID_1546491761_m = Alloy.Collections.numero_unico;
        var ID_1546491761_fila = Alloy.createModel('numero_unico', {
            comentario: 'nuevo itemdano foto3'
        });
        ID_1546491761_m.add(ID_1546491761_fila);
        ID_1546491761_fila.save();
        var nuevoid_f3 = require('helper').model2object(ID_1546491761_m.last());
        /** 
         * Guardamos en variable el detalle del modelo numero_unico para asignar informacion de foto1 del dano 
         */
        require('vars')[_var_scopekey]['nuevoid_f3'] = nuevoid_f3;
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        var gps_coords = ('gps_coords' in require('vars')) ? require('vars')['gps_coords'] : '';
        var gps_latitud = ('gps_latitud' in require('vars')) ? require('vars')['gps_latitud'] : '';
        var gps_longitud = ('gps_longitud' in require('vars')) ? require('vars')['gps_longitud'] : '';
        var resultado = null;
        if ('distanceinmtbetweenearthcoordinates' in require('funciones')) {
            resultado = require('funciones').distanceinmtbetweenearthcoordinates({
                'lat1': gps_latitud,
                'lon1': gps_longitud,
                'lat2': seltarea.lat,
                'lon2': seltarea.lon
            });
        } else {
            try {
                resultado = f_distanceinmtbetweenearthcoordinates({
                    'lat1': gps_latitud,
                    'lon1': gps_longitud,
                    'lat2': seltarea.lat,
                    'lon2': seltarea.lon
                });
            } catch (ee) {}
        }
        if (_.isNumber(gps_coords.accuracy) && _.isNumber(resultado) && gps_coords.accuracy > resultado) {
            /** 
             * Pablo me comento que el flag fuera_rango se determina comparando precision vs la distancia de ubicacion con la definida en tarea 
             */
            var json_datosfoto = {
                lat: gps_latitud,
                lng: gps_longitud,
                fuera_rango: 0,
                distancia: Math.round(resultado),
                precision: Math.round(gps_coords.accuracy)
            };
        } else {
            var json_datosfoto = {
                lat: gps_latitud,
                lng: gps_longitud,
                fuera_rango: 1,
                distancia: Math.round(resultado),
                precision: Math.round(gps_coords.accuracy)
            };
        }
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var ID_251167257_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
            if (ID_251167257_d.exists() == false) ID_251167257_d.createDirectory();
            var ID_251167257_f = Ti.Filesystem.getFile(ID_251167257_d.resolve(), 'cap' + nuevoid_f3.id + '.jpg');
            if (ID_251167257_f.exists() == true) ID_251167257_f.deleteFile();
            ID_251167257_f.write(evento.foto);
            ID_251167257_d = null;
            ID_251167257_f = null;
            var ID_1597933114_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
            if (ID_1597933114_d.exists() == false) ID_1597933114_d.createDirectory();
            var ID_1597933114_f = Ti.Filesystem.getFile(ID_1597933114_d.resolve(), 'cap' + nuevoid_f3.id + '.json');
            if (ID_1597933114_f.exists() == true) ID_1597933114_f.deleteFile();
            console.log('contenido f3 ndano', JSON.stringify(json_datosfoto));
            ID_1597933114_f.write(JSON.stringify(json_datosfoto));
            ID_1597933114_d = null;
            ID_1597933114_f = null;
        } else {
            var ID_1881347677_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + 0);
            if (ID_1881347677_d.exists() == false) ID_1881347677_d.createDirectory();
            var ID_1881347677_f = Ti.Filesystem.getFile(ID_1881347677_d.resolve(), 'cap' + nuevoid_f3.id + '.jpg');
            if (ID_1881347677_f.exists() == true) ID_1881347677_f.deleteFile();
            ID_1881347677_f.write(evento.foto);
            ID_1881347677_d = null;
            ID_1881347677_f = null;
        }
        $.danos1.set({
            foto3: 'cap' + nuevoid_f3.id + '.jpg'
        });
        if ('danos1' in $) $danos1 = $.danos1.toJSON();
        $.ID_437144584.procesar({
            imagen: evento.foto,
            nueva: 640,
            calidad: 91,
            camara: 'trasera'
        });
        evento = null;
    }
}

(function() {
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
        /** 
         * Modificamos la idinspeccion con el idserver de la tarea 
         */
        $.danos1.set({
            id_inspeccion: seltarea.id_server
        });
        if ('danos1' in $) $danos1 = $.danos1.toJSON();
    }
    /** 
     * Recuperamos la variable del id_recinto que tiene danos relacionados y actualizamos el modelo 
     */
    var temp_idrecinto = ('temp_idrecinto' in require('vars')) ? require('vars')['temp_idrecinto'] : '';
    if ((_.isObject(temp_idrecinto) || (_.isString(temp_idrecinto)) && !_.isEmpty(temp_idrecinto)) || _.isNumber(temp_idrecinto) || _.isBoolean(temp_idrecinto)) {
        $.danos1.set({
            id_recinto: temp_idrecinto
        });
        if ('danos1' in $) $danos1 = $.danos1.toJSON();
    }
    /** 
     * Desenfocamos todos los campos de texto en 0.2 segundos 
     */
    var ID_268744191_func = function() {
        $.ID_1215120869.blur();
        $.ID_1650528667.blur();
    };
    var ID_268744191 = setTimeout(ID_268744191_func, 1000 * 0.2);
    /** 
     * Fijamos el scroll al inicio 
     */
    $.ID_2131245501.scrollToTop();
    /** 
     * Modificamos en 0.1 segundos el color del statusbar 
     */
    var ID_475082989_func = function() {
        var ID_1540530499_statusbar = '#7E6EE0';

        var setearStatusColor = function(ID_1540530499_statusbar) {
            if (OS_IOS) {
                if (ID_1540530499_statusbar == 'light' || ID_1540530499_statusbar == 'claro') {
                    $.ID_1540530499_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.LIGHT_CONTENT);
                } else if (ID_1540530499_statusbar == 'grey' || ID_1540530499_statusbar == 'gris' || ID_1540530499_statusbar == 'gray') {
                    $.ID_1540530499_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.GREY);
                } else if (ID_1540530499_statusbar == 'oscuro' || ID_1540530499_statusbar == 'dark') {
                    $.ID_1540530499_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.DEFAULT);
                }
            } else if (OS_ANDROID) {
                abx.setStatusbarColor(ID_1540530499_statusbar);
            }
        };
        setearStatusColor(ID_1540530499_statusbar);

    };
    var ID_475082989 = setTimeout(ID_475082989_func, 1000 * 0.1);
    _my_events['resp_dato,ID_1433682884'] = function(evento) {
        if (Ti.App.deployType != 'production') console.log('detalle de la respuesta', {
            "datos": evento
        });
        $.ID_1446995015.setText(evento.nombre);

        $.ID_1446995015.setColor('#000000');

        /** 
         * Asumimos que el valor mostrado seleccionado es el nombre de la fila dano, ya que no existe el campo nombre en esta pantalla. 
         */
        $.danos1.set({
            nombre: evento.nombre,
            id_partida: evento.id_partida,
            id_tipodano: '',
            id_unidadmedida: ''
        });
        if ('danos1' in $) $danos1 = $.danos1.toJSON();
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
            var ID_1819177394_i = Alloy.createCollection('tipo_dano');
            var ID_1819177394_i_where = 'pais_texto=\'' + pais[0].nombre + '\' AND id_partida=\'' + evento.id_server + '\'';
            ID_1819177394_i.fetch({
                query: 'SELECT * FROM tipo_dano WHERE pais_texto=\'' + pais[0].nombre + '\' AND id_partida=\'' + evento.id_server + '\''
            });
            var danos = require('helper').query2array(ID_1819177394_i);
            if (Ti.App.deployType != 'production') console.log('predanos', {
                "datos": danos
            });
            /** 
             * Obtenemos datos para selectores (solo los de este pais y este tipo de partida) 
             */
            var datos = [];
            _.each(danos, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'valor';
                    if (llave == 'id_segured') newkey = 'id_interno';
                    if (llave == 'id_server') newkey = 'id_gerardo';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                datos.push(new_row);
            });
            if (Ti.App.deployType != 'production') console.log('postdanos', {
                "datos": datos
            });
            $.ID_1384858606.data({
                data: datos
            });
            if (Ti.App.deployType != 'production') console.log('termino de llamar', {});
        }
        /** 
         * Editamos y limpiamos los campos que estan siendo filtrados con este campo 
         */
        $.ID_1384858606.labels({
            seleccione: 'seleccione tipo'
        });
        $.ID_1867144665.labels({
            seleccione: 'unidad'
        });
        $.ID_1215120869.setValue('');

        $.ID_821385421.setText('-');

    };
    Alloy.Events.on('resp_dato', _my_events['resp_dato,ID_1433682884']);
})();

function Postlayout_ID_1429110082(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var ID_1581404676_func = function() {
        /** 
         * Una vez ya cargada la tarea, dejamos la variable limpia para poder abrir otras pantallas 
         */
        require('vars')['var_abriendo'] = '';
    };
    var ID_1581404676 = setTimeout(ID_1581404676_func, 1000 * 0.2);

}
if (OS_IOS || OS_ANDROID) {
    $.ID_1540530499.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}
//$.ID_1540530499.open();