var _bind4section = {};
var _list_templates = {};
var $nivel = $.nivel.toJSON();

var _activity;
if (OS_ANDROID) {
    _activity = $.ID_1917979189.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_1917979189';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_1917979189.addEventListener('open', function(e) {});
}
$.ID_1917979189.orientationModes = [Titanium.UI.PORTRAIT];


$.ID_136168330.init({
    titulo: 'EDITAR ZONA',
    onsalirinsp: Salirinsp_ID_1932019351,
    __id: 'ALL136168330',
    textoderecha: 'Guardar',
    salir_insp: '',
    fondo: 'fondoverde',
    top: 0,
    onpresiono: Presiono_ID_1397011661,
    colortextoderecha: 'blanco'
});

function Salirinsp_ID_1932019351(e) {

    var evento = e;
    /** 
     * Limpiamos widget para liberar memoria ram 
     */
    $.ID_145212507.limpiar({});
    $.ID_1401525657.limpiar({});
    $.ID_937114202.limpiar({});
    $.ID_1489080142.limpiar({});
    $.ID_129921645.limpiar({});
    $.ID_296719183.limpiar({});
    $.ID_629771872.limpiar({});
    $.ID_1917979189.close();

}

function Presiono_ID_1397011661(e) {

    var evento = e;
    /** 
     * Desenfocamos todos los campos de texto 
     */
    $.ID_1553156874.blur();
    $.ID_103828995.blur();
    $.ID_379385469.blur();
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
        /** 
         * si tarea requiere seleccionar medidas de seguridad, probamos si hay seleccionadas aqui para respetar orden de validacion. 
         */
        if (seltarea.exige_medidas == 10) {
            if (_.isUndefined($nivel.ids_medidas_seguridad)) {
                require('vars')[_var_scopekey]['faltan_medidas'] = 'true';
            } else if ((_.isObject($nivel.ids_medidas_seguridad) || _.isString($nivel.ids_medidas_seguridad)) && _.isEmpty($nivel.ids_medidas_seguridad)) {
                require('vars')[_var_scopekey]['faltan_medidas'] = 'true';
            }
        }
    }
    var faltan_medidas = ('faltan_medidas' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['faltan_medidas'] : '';
    var moment = require('alloy/moment');
    var anoactual = moment(new Date()).format('YYYY');
    if (_.isUndefined($nivel.nombre)) {
        /** 
         * Verificamos que los campos ingresados esten correctos 
         */
        var ID_1351757773_opts = ['Aceptar'];
        var ID_1351757773 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese nombre de la zona',
            buttonNames: ID_1351757773_opts
        });
        ID_1351757773.addEventListener('click', function(e) {
            var nulo = ID_1351757773_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1351757773.show();
    } else if ((_.isObject($nivel.nombre) || _.isString($nivel.nombre)) && _.isEmpty($nivel.nombre)) {
        var ID_1012540782_opts = ['Aceptar'];
        var ID_1012540782 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese nombre de la zona',
            buttonNames: ID_1012540782_opts
        });
        ID_1012540782.addEventListener('click', function(e) {
            var nulo = ID_1012540782_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1012540782.show();
    } else if (_.isUndefined($nivel.piso)) {
        var ID_1387179532_opts = ['Aceptar'];
        var ID_1387179532 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese Nº de piso',
            buttonNames: ID_1387179532_opts
        });
        ID_1387179532.addEventListener('click', function(e) {
            var nulo = ID_1387179532_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1387179532.show();
    } else if ($nivel.piso.length == 0 || $nivel.piso.length == '0') {
        var ID_468609373_opts = ['Aceptar'];
        var ID_468609373 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese Nº de piso',
            buttonNames: ID_468609373_opts
        });
        ID_468609373.addEventListener('click', function(e) {
            var nulo = ID_468609373_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_468609373.show();
    } else if (_.isUndefined($nivel.ano)) {
        var ID_312161567_opts = ['Aceptar'];
        var ID_312161567 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese año de construcción de la zona',
            buttonNames: ID_312161567_opts
        });
        ID_312161567.addEventListener('click', function(e) {
            var nulo = ID_312161567_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_312161567.show();
    } else if ((_.isObject($nivel.ano) || _.isString($nivel.ano)) && _.isEmpty($nivel.ano)) {
        var ID_1553059527_opts = ['Aceptar'];
        var ID_1553059527 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese año de construcción de la zona',
            buttonNames: ID_1553059527_opts
        });
        ID_1553059527.addEventListener('click', function(e) {
            var nulo = ID_1553059527_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1553059527.show();
    } else if ($nivel.ano < (anoactual - 100) == true || $nivel.ano < (anoactual - 100) == 'true') {
        if (Ti.App.deployType != 'production') console.log('ano mayor', {});
        require('vars')[_var_scopekey]['todobien'] = 'false';
        var ID_1834361486_opts = ['Aceptar'];
        var ID_1834361486 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Tiene que tener máximo 100 años de antigüedad',
            buttonNames: ID_1834361486_opts
        });
        ID_1834361486.addEventListener('click', function(e) {
            var nulo = ID_1834361486_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1834361486.show();
    } else if ($nivel.ano > anoactual == true || $nivel.ano > anoactual == 'true') {
        if (Ti.App.deployType != 'production') console.log('ano mayor', {});
        require('vars')[_var_scopekey]['todobien'] = 'false';
        var ID_212384257_opts = ['Aceptar'];
        var ID_212384257 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'El año tiene que ser menor al año actual',
            buttonNames: ID_212384257_opts
        });
        ID_212384257.addEventListener('click', function(e) {
            var nulo = ID_212384257_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_212384257.show();
    } else if (_.isUndefined($nivel.largo)) {
        var ID_679865790_opts = ['Aceptar'];
        var ID_679865790 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese largo de la zona',
            buttonNames: ID_679865790_opts
        });
        ID_679865790.addEventListener('click', function(e) {
            var nulo = ID_679865790_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_679865790.show();
    } else if ((_.isObject($nivel.largo) || _.isString($nivel.largo)) && _.isEmpty($nivel.largo)) {
        var ID_860906486_opts = ['Aceptar'];
        var ID_860906486 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese largo de la zona',
            buttonNames: ID_860906486_opts
        });
        ID_860906486.addEventListener('click', function(e) {
            var nulo = ID_860906486_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_860906486.show();
    } else if (_.isUndefined($nivel.ancho)) {
        var ID_1169421375_opts = ['Aceptar'];
        var ID_1169421375 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese ancho de la zona',
            buttonNames: ID_1169421375_opts
        });
        ID_1169421375.addEventListener('click', function(e) {
            var nulo = ID_1169421375_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1169421375.show();
    } else if ((_.isObject($nivel.ancho) || _.isString($nivel.ancho)) && _.isEmpty($nivel.ancho)) {
        var ID_2841982_opts = ['Aceptar'];
        var ID_2841982 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese ancho de la zona',
            buttonNames: ID_2841982_opts
        });
        ID_2841982.addEventListener('click', function(e) {
            var nulo = ID_2841982_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_2841982.show();
    } else if (_.isUndefined($nivel.alto)) {
        var ID_429247252_opts = ['Aceptar'];
        var ID_429247252 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese la altura de la zona',
            buttonNames: ID_429247252_opts
        });
        ID_429247252.addEventListener('click', function(e) {
            var nulo = ID_429247252_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_429247252.show();
    } else if ((_.isObject($nivel.alto) || _.isString($nivel.alto)) && _.isEmpty($nivel.alto)) {
        var ID_1489231380_opts = ['Aceptar'];
        var ID_1489231380 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese la altura de la zona',
            buttonNames: ID_1489231380_opts
        });
        ID_1489231380.addEventListener('click', function(e) {
            var nulo = ID_1489231380_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1489231380.show();
    } else if (faltan_medidas == true || faltan_medidas == 'true') {
        var ID_1307798309_opts = ['Aceptar'];
        var ID_1307798309 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Seleccione las medidas de seguridad',
            buttonNames: ID_1307798309_opts
        });
        ID_1307798309.addEventListener('click', function(e) {
            var nulo = ID_1307798309_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1307798309.show();
    } else if (_.isUndefined($nivel.ids_estructuras_soportantes)) {
        var ID_1151831132_opts = ['Aceptar'];
        var ID_1151831132 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Seleccione la estructura soportante de la zona',
            buttonNames: ID_1151831132_opts
        });
        ID_1151831132.addEventListener('click', function(e) {
            var nulo = ID_1151831132_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1151831132.show();
    } else if ((_.isObject($nivel.ids_estructuras_soportantes) || _.isString($nivel.ids_estructuras_soportantes)) && _.isEmpty($nivel.ids_estructuras_soportantes)) {
        var ID_1668586889_opts = ['Aceptar'];
        var ID_1668586889 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Seleccione la estructura soportante de la zona',
            buttonNames: ID_1668586889_opts
        });
        ID_1668586889.addEventListener('click', function(e) {
            var nulo = ID_1668586889_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1668586889.show();
    } else {
        /** 
         * Eliminamos el nivel que estamos editando de la tabla y reemplazamos por el que tenemos actualmente con datos editados 
         */
        /** 
         * Eliminamos el nivel que estamos editando de la tabla y reemplazamos por el que tenemos actualmente con datos editados 
         */
        var ID_1680197355_i = Alloy.Collections.insp_niveles;
        var sql = 'DELETE FROM ' + ID_1680197355_i.config.adapter.collection_name + ' WHERE id=\'' + args._dato.id + '\'';
        var db = Ti.Database.open(ID_1680197355_i.config.adapter.db_name);
        db.execute(sql);
        db.close();
        sql = null;
        db = null;
        ID_1680197355_i.trigger('delete');
        Alloy.Collections[$.nivel.config.adapter.collection_name].add($.nivel);
        $.nivel.save();
        Alloy.Collections[$.nivel.config.adapter.collection_name].fetch();
        /** 
         * Limpiamos widget para liberar memoria ram 
         */
        $.ID_145212507.limpiar({});
        $.ID_1401525657.limpiar({});
        $.ID_937114202.limpiar({});
        $.ID_1489080142.limpiar({});
        $.ID_129921645.limpiar({});
        $.ID_296719183.limpiar({});
        $.ID_629771872.limpiar({});
        $.ID_1917979189.close();
    }
}

function Blur_ID_269095859(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    $.nivel.set({
        nombre: elemento
    });
    if ('nivel' in $) $nivel = $.nivel.toJSON();
    elemento = null, source = null;

}

function Blur_ID_1428503915(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    /** 
     * Actualizamos el modelo con el piso ingresado 
     */
    $.nivel.set({
        piso: elemento
    });
    if ('nivel' in $) $nivel = $.nivel.toJSON();
    elemento = null, source = null;

}

function Return_ID_1395509025(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    /** 
     * Cuando el inspector presiona el enter en el teclado, desenfocamos el campo 
     */
    $.ID_103828995.blur();
    elemento = null, source = null;

}

function Blur_ID_928467309(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    $.nivel.set({
        ano: elemento
    });
    if ('nivel' in $) $nivel = $.nivel.toJSON();
    elemento = null, source = null;

}

function Change_ID_1290779402(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    /** 
     * Obtenemos el valor escrito en el campo de ancho 
     */
    var ancho;
    ancho = $.ID_465957073.getValue();

    if ((_.isObject(ancho) || (_.isString(ancho)) && !_.isEmpty(ancho)) || _.isNumber(ancho) || _.isBoolean(ancho)) {
        /** 
         * Calculamos la superficie 
         */
        if ((_.isObject(elemento) || (_.isString(elemento)) && !_.isEmpty(elemento)) || _.isNumber(elemento) || _.isBoolean(elemento)) {
            /** 
             * nos aseguramos que ancho y valor de campo sean decimales con puntos (flotante), y no comas (espa&#241;ol) 
             */
            var nuevo = parseFloat(ancho.split(',').join('.')) * parseFloat(elemento.split(',').join('.'));
            $.nivel.set({
                superficie: nuevo
            });
            if ('nivel' in $) $nivel = $.nivel.toJSON();
        }
    }
    /** 
     * Actualizamos el valor del largo del recinto 
     */
    $.nivel.set({
        largo: elemento
    });
    if ('nivel' in $) $nivel = $.nivel.toJSON();
    elemento = null, source = null;

}

function Change_ID_1604147283(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    var largo;
    largo = $.ID_946615753.getValue();

    if ((_.isObject(largo) || (_.isString(largo)) && !_.isEmpty(largo)) || _.isNumber(largo) || _.isBoolean(largo)) {
        if ((_.isObject(elemento) || (_.isString(elemento)) && !_.isEmpty(elemento)) || _.isNumber(elemento) || _.isBoolean(elemento)) {
            /** 
             * nos aseguramos que largo y valor de campo sean decimales con puntos (flotante), y no comas (espa&#241;ol) 
             */
            var nuevo = parseFloat(largo.split(',').join('.')) * parseFloat(elemento.split(',').join('.'));
            $.nivel.set({
                superficie: nuevo
            });
            if ('nivel' in $) $nivel = $.nivel.toJSON();
        }
    }
    $.nivel.set({
        ancho: elemento
    });
    if ('nivel' in $) $nivel = $.nivel.toJSON();
    elemento = null, source = null;

}

function Change_ID_1592950374(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    $.nivel.set({
        alto: elemento
    });
    if ('nivel' in $) $nivel = $.nivel.toJSON();
    elemento = null, source = null;

}

$.ID_629771872.init({
    titulo: 'Medidas de seguridad',
    cargando: 'cargando...',
    __id: 'ALL629771872',
    oncerrar: Cerrar_ID_1004442479,
    left: 0,
    hint: 'Seleccione medidas',
    color: 'verde',
    subtitulo: 'Indique las medidas',
    right: 0,
    onclick: Click_ID_597183433,
    onafterinit: Afterinit_ID_719805228
});

function Click_ID_597183433(e) {

    var evento = e;

}

function Cerrar_ID_1004442479(e) {

    var evento = e;
    $.ID_629771872.update({});
    $.nivel.set({
        ids_medidas_seguridad: evento.valores
    });
    if ('nivel' in $) $nivel = $.nivel.toJSON();

}

function Afterinit_ID_719805228(e) {

    var evento = e;
    var ID_12432852_func = function() {
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            if (Ti.App.deployType != 'production') console.log('prueba1', {});
            var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
            var ID_591849945_i = Alloy.createCollection('insp_niveles');
            var ID_591849945_i_where = 'id=\'' + args._dato.id + '\'';
            ID_591849945_i.fetch({
                query: 'SELECT * FROM insp_niveles WHERE id=\'' + args._dato.id + '\''
            });
            var insp_datosa = require('helper').query2array(ID_591849945_i);
            if (insp_datosa && insp_datosa.length) {
                var ID_1991823595_i = Alloy.createCollection('medidas_seguridad');
                var ID_1991823595_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
                ID_1991823595_i.fetch({
                    query: 'SELECT * FROM medidas_seguridad WHERE pais_texto=\'' + pais[0].nombre + '\''
                });
                var medidas_seguridad = require('helper').query2array(ID_1991823595_i);
                var padrea_index = 0;
                _.each(medidas_seguridad, function(padrea, padrea_pos, padrea_list) {
                    padrea_index += 1;
                    if (_.isNull(insp_datosa[0].ids_medidas_seguridad)) {
                        padrea._estado = 0;
                    } else if (insp_datosa[0].ids_medidas_seguridad.indexOf(padrea.id_segured) != -1) {
                        padrea._estado = 1;
                    } else {
                        padrea._estado = 0;
                    }
                });
                var datos = [];
                _.each(medidas_seguridad, function(fila, pos) {
                    var new_row = {};
                    _.each(fila, function(x, llave) {
                        var newkey = '';
                        if (llave == 'nombre') newkey = 'label';
                        if (llave == 'id_segured') newkey = 'valor';
                        if (llave == '_estado') newkey = 'estado';
                        if (newkey != '') new_row[newkey] = fila[llave];
                    });
                    datos.push(new_row);
                });
            } else {
                var ID_1996408906_i = Alloy.createCollection('medidas_seguridad');
                ID_1996408906_i.fetch();
                var ID_1996408906_src = require('helper').query2array(ID_1996408906_i);
                var datos = [];
                _.each(ID_1996408906_src, function(fila, pos) {
                    var new_row = {};
                    _.each(fila, function(x, llave) {
                        var newkey = '';
                        if (llave == 'nombre') newkey = 'label';
                        if (llave == 'id_segured') newkey = 'valor';
                        if (newkey != '') new_row[newkey] = fila[llave];
                    });
                    new_row['estado'] = 0;
                    datos.push(new_row);
                });
            }
            $.ID_629771872.update({
                data: datos
            });
        } else {
            var ID_323662400_i = Alloy.Collections.medidas_seguridad;
            var sql = "DELETE FROM " + ID_323662400_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_323662400_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_323662400_i.trigger('delete');
            var item_index = 0;
            _.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
                item_index += 1;
                var ID_1559014509_m = Alloy.Collections.medidas_seguridad;
                var ID_1559014509_fila = Alloy.createModel('medidas_seguridad', {
                    nombre: 'Med seguridad ' + item,
                    id_server: '0' + item,
                    pais_texto: 'Chile',
                    id_segured: '10' + item,
                    pais: 1
                });
                ID_1559014509_m.add(ID_1559014509_fila);
                ID_1559014509_fila.save();
            });
            var ID_923613856_i = Alloy.createCollection('medidas_seguridad');
            var ID_923613856_i_where = '';
            ID_923613856_i.fetch();
            var mseguridad2 = require('helper').query2array(ID_923613856_i);
            var padreb_index = 0;
            _.each(mseguridad2, function(padreb, padreb_pos, padreb_list) {
                padreb_index += 1;
                if (_.isNull(nivel_editado.ids_medidas_seguridad)) {
                    padreb._estado = 0;
                } else if (nivel_editado.ids_medidas_seguridad.toLowerCase().indexOf(padreb.id_segured.toLowerCase()) != -1) {
                    padreb._estado = 1;
                } else {
                    padreb._estado = 0;
                }
            });
            var datos2 = [];
            _.each(mseguridad2, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'label';
                    if (llave == 'id_segured') newkey = 'valor';
                    if (llave == '_estado') newkey = 'estado';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                datos2.push(new_row);
            });
            $.ID_629771872.update({
                data: datos2
            });
        }
    };
    var ID_12432852 = setTimeout(ID_12432852_func, 1000 * 0.2);

}

$.ID_145212507.init({
    titulo: 'Estructura Soportante',
    cargando: 'cargando...',
    __id: 'ALL145212507',
    oncerrar: Cerrar_ID_1946759557,
    left: 0,
    hint: 'Seleccione estructura',
    color: 'verde',
    subtitulo: 'Indique los tipos',
    right: 0,
    onclick: Click_ID_278507892,
    onafterinit: Afterinit_ID_1598388462
});

function Click_ID_278507892(e) {

    var evento = e;

}

function Cerrar_ID_1946759557(e) {

    var evento = e;
    $.ID_145212507.update({});
    $.nivel.set({
        ids_estructuras_soportantes: evento.valores
    });
    if ('nivel' in $) $nivel = $.nivel.toJSON();

}

function Afterinit_ID_1598388462(e) {

    var evento = e;
    /** 
     * ejecutamos desfasado para no congelar el hilo. 
     */
    var ID_1079129098_func = function() {
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
            var ID_1956490462_i = Alloy.createCollection('insp_niveles');
            var ID_1956490462_i_where = 'id=\'' + args._dato.id + '\'';
            ID_1956490462_i.fetch({
                query: 'SELECT * FROM insp_niveles WHERE id=\'' + args._dato.id + '\''
            });
            var insp_datos = require('helper').query2array(ID_1956490462_i);
            if (insp_datos && insp_datos.length) {
                var ID_1721238000_i = Alloy.createCollection('estructura_soportante');
                var ID_1721238000_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
                ID_1721238000_i.fetch({
                    query: 'SELECT * FROM estructura_soportante WHERE pais_texto=\'' + pais[0].nombre + '\''
                });
                var estructura = require('helper').query2array(ID_1721238000_i);
                var padre_index = 0;
                _.each(estructura, function(padre, padre_pos, padre_list) {
                    padre_index += 1;
                    if (_.isNull(insp_datos[0].ids_estructuras_soportantes)) {
                        /** 
                         * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
                         */
                        padre._estado = 0;
                    } else if (insp_datos[0].ids_estructuras_soportantes.indexOf(padre.id_segured) != -1) {
                        padre._estado = 1;
                    } else {
                        padre._estado = 0;
                    }
                });
                var datos = [];
                _.each(estructura, function(fila, pos) {
                    var new_row = {};
                    _.each(fila, function(x, llave) {
                        var newkey = '';
                        if (llave == 'nombre') newkey = 'label';
                        if (llave == 'id_segured') newkey = 'valor';
                        if (llave == '_estado') newkey = 'estado';
                        if (newkey != '') new_row[newkey] = fila[llave];
                    });
                    datos.push(new_row);
                });
            } else {
                var ID_1172300818_i = Alloy.createCollection('estructura_soportante');
                ID_1172300818_i.fetch();
                var ID_1172300818_src = require('helper').query2array(ID_1172300818_i);
                var datos = [];
                _.each(ID_1172300818_src, function(fila, pos) {
                    var new_row = {};
                    _.each(fila, function(x, llave) {
                        var newkey = '';
                        if (llave == 'nombre') newkey = 'label';
                        if (llave == 'id_segured') newkey = 'valor';
                        if (newkey != '') new_row[newkey] = fila[llave];
                    });
                    new_row['estado'] = 0;
                    datos.push(new_row);
                });
            }
            $.ID_145212507.update({
                data: datos
            });
        } else {
            /** 
             * cargamos datos dummy para cuando compilamos de forma individual. 
             */
            /** 
             * Insertamos dummies 
             */
            /** 
             * Insertamos dummies 
             */
            var ID_1101733045_i = Alloy.Collections.estructura_soportante;
            var sql = "DELETE FROM " + ID_1101733045_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_1101733045_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_1101733045_i.trigger('delete');
            var item_index = 0;
            _.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
                item_index += 1;
                var ID_1484035947_m = Alloy.Collections.estructura_soportante;
                var ID_1484035947_fila = Alloy.createModel('estructura_soportante', {
                    nombre: 'Estructura' + item,
                    id_server: '0' + item,
                    id_segured: '10' + item,
                    pais: 1
                });
                ID_1484035947_m.add(ID_1484035947_fila);
                ID_1484035947_fila.save();
            });
            var ID_486541324_i = Alloy.createCollection('insp_niveles');
            var ID_486541324_i_where = 'id=\'' + args._dato.id + '\'';
            ID_486541324_i.fetch({
                query: 'SELECT * FROM insp_niveles WHERE id=\'' + args._dato.id + '\''
            });
            var insp_datos = require('helper').query2array(ID_486541324_i);
            if (insp_datos && insp_datos.length) {
                /** 
                 * Obtenemos datos para selectores 
                 */
                var ID_1486035008_i = Alloy.createCollection('estructura_soportante');
                var ID_1486035008_i_where = '';
                ID_1486035008_i.fetch();
                var estructura = require('helper').query2array(ID_1486035008_i);
                var padre_index = 0;
                _.each(estructura, function(padre, padre_pos, padre_list) {
                    padre_index += 1;
                    if (_.isNull(insp_datos[0].ids_estructuras_soportantes)) {
                        /** 
                         * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
                         */
                        padre._estado = 0;
                    } else if (insp_datos[0].ids_estructuras_soportantes.indexOf(padre.id_segured) != -1) {
                        padre._estado = 1;
                    } else {
                        padre._estado = 0;
                    }
                });
                var datos = [];
                _.each(estructura, function(fila, pos) {
                    var new_row = {};
                    _.each(fila, function(x, llave) {
                        var newkey = '';
                        if (llave == 'nombre') newkey = 'label';
                        if (llave == 'id_segured') newkey = 'valor';
                        if (llave == '_estado') newkey = 'estado';
                        if (newkey != '') new_row[newkey] = fila[llave];
                    });
                    datos.push(new_row);
                });
            } else {
                var ID_1471680869_i = Alloy.createCollection('estructura_soportante');
                ID_1471680869_i.fetch();
                var ID_1471680869_src = require('helper').query2array(ID_1471680869_i);
                var datos = [];
                _.each(ID_1471680869_src, function(fila, pos) {
                    var new_row = {};
                    _.each(fila, function(x, llave) {
                        var newkey = '';
                        if (llave == 'nombre') newkey = 'label';
                        if (llave == 'id_segured') newkey = 'valor';
                        if (newkey != '') new_row[newkey] = fila[llave];
                    });
                    new_row['estado'] = 0;
                    datos.push(new_row);
                });
            }
            $.ID_145212507.update({
                data: datos
            });
        }
    };
    var ID_1079129098 = setTimeout(ID_1079129098_func, 1000 * 0.2);

}

$.ID_1401525657.init({
    titulo: 'Muros / Tabiques',
    cargando: 'cargando...',
    __id: 'ALL1401525657',
    oncerrar: Cerrar_ID_552898950,
    left: 0,
    hint: 'Seleccione muros',
    color: 'verde',
    subtitulo: 'Indique los tipos',
    right: 0,
    onclick: Click_ID_1604347138,
    onafterinit: Afterinit_ID_1214974460
});

function Click_ID_1604347138(e) {

    var evento = e;

}

function Cerrar_ID_552898950(e) {

    var evento = e;
    $.ID_1401525657.update({});
    $.nivel.set({
        ids_muros: evento.valores
    });
    if ('nivel' in $) $nivel = $.nivel.toJSON();

}

function Afterinit_ID_1214974460(e) {

    var evento = e;
    /** 
     * ejecutamos desfasado para no congelar el hilo. 
     */
    var ID_1365479032_func = function() {
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
            var ID_1109064313_i = Alloy.createCollection('insp_niveles');
            var ID_1109064313_i_where = 'id=\'' + args._dato.id + '\'';
            ID_1109064313_i.fetch({
                query: 'SELECT * FROM insp_niveles WHERE id=\'' + args._dato.id + '\''
            });
            var insp_datos = require('helper').query2array(ID_1109064313_i);
            if (insp_datos && insp_datos.length) {
                var ID_871263747_i = Alloy.createCollection('muros_tabiques');
                var ID_871263747_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
                ID_871263747_i.fetch({
                    query: 'SELECT * FROM muros_tabiques WHERE pais_texto=\'' + pais[0].nombre + '\''
                });
                var muros_tabiques = require('helper').query2array(ID_871263747_i);
                var padre_index = 0;
                _.each(muros_tabiques, function(padre, padre_pos, padre_list) {
                    padre_index += 1;
                    if (_.isNull(insp_datos[0].ids_muros)) {
                        /** 
                         * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
                         */
                        padre._estado = 0;
                    } else if (insp_datos[0].ids_muros.indexOf(padre.id_segured) != -1) {
                        padre._estado = 1;
                    } else {
                        padre._estado = 0;
                    }
                });
                var datos = [];
                _.each(muros_tabiques, function(fila, pos) {
                    var new_row = {};
                    _.each(fila, function(x, llave) {
                        var newkey = '';
                        if (llave == 'nombre') newkey = 'label';
                        if (llave == 'id_segured') newkey = 'valor';
                        if (llave == '_estado') newkey = 'estado';
                        if (newkey != '') new_row[newkey] = fila[llave];
                    });
                    datos.push(new_row);
                });
            } else {
                var ID_115148083_i = Alloy.createCollection('muros_tabiques');
                ID_115148083_i.fetch();
                var ID_115148083_src = require('helper').query2array(ID_115148083_i);
                var datos = [];
                _.each(ID_115148083_src, function(fila, pos) {
                    var new_row = {};
                    _.each(fila, function(x, llave) {
                        var newkey = '';
                        if (llave == 'nombre') newkey = 'label';
                        if (llave == 'id_segured') newkey = 'valor';
                        if (newkey != '') new_row[newkey] = fila[llave];
                    });
                    new_row['estado'] = 0;
                    datos.push(new_row);
                });
            }
            $.ID_1401525657.update({
                data: datos
            });
        } else {
            /** 
             * cargamos datos dummy para cuando compilamos de forma individual. 
             */
            var ID_1520972002_i = Alloy.Collections.muros_tabiques;
            var sql = "DELETE FROM " + ID_1520972002_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_1520972002_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_1520972002_i.trigger('delete');
            var item_index = 0;
            _.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
                item_index += 1;
                var ID_1006833970_m = Alloy.Collections.muros_tabiques;
                var ID_1006833970_fila = Alloy.createModel('muros_tabiques', {
                    nombre: 'Muros tabiques ' + item,
                    id_server: '0' + item,
                    id_segured: '10' + item,
                    pais: 1
                });
                ID_1006833970_m.add(ID_1006833970_fila);
                ID_1006833970_fila.save();
            });
            var ID_1499865215_i = Alloy.createCollection('insp_niveles');
            var ID_1499865215_i_where = 'id=\'' + args._dato.id + '\'';
            ID_1499865215_i.fetch({
                query: 'SELECT * FROM insp_niveles WHERE id=\'' + args._dato.id + '\''
            });
            var insp_datos = require('helper').query2array(ID_1499865215_i);
            if (insp_datos && insp_datos.length) {
                var ID_18659198_i = Alloy.createCollection('muros_tabiques');
                var ID_18659198_i_where = '';
                ID_18659198_i.fetch();
                var muros_tabiques = require('helper').query2array(ID_18659198_i);
                var padre_index = 0;
                _.each(muros_tabiques, function(padre, padre_pos, padre_list) {
                    padre_index += 1;
                    if (_.isNull(insp_datos[0].ids_muros)) {
                        /** 
                         * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
                         */
                        padre._estado = 0;
                    } else if (insp_datos[0].ids_muros.indexOf(padre.id_segured) != -1) {
                        padre._estado = 1;
                    } else {
                        padre._estado = 0;
                    }
                });
                var datos = [];
                _.each(muros_tabiques, function(fila, pos) {
                    var new_row = {};
                    _.each(fila, function(x, llave) {
                        var newkey = '';
                        if (llave == 'nombre') newkey = 'label';
                        if (llave == 'id_segured') newkey = 'valor';
                        if (llave == '_estado') newkey = 'estado';
                        if (newkey != '') new_row[newkey] = fila[llave];
                    });
                    datos.push(new_row);
                });
            } else {
                var ID_1302434909_i = Alloy.createCollection('muros_tabiques');
                ID_1302434909_i.fetch();
                var ID_1302434909_src = require('helper').query2array(ID_1302434909_i);
                var datos = [];
                _.each(ID_1302434909_src, function(fila, pos) {
                    var new_row = {};
                    _.each(fila, function(x, llave) {
                        var newkey = '';
                        if (llave == 'nombre') newkey = 'label';
                        if (llave == 'id_segured') newkey = 'valor';
                        if (newkey != '') new_row[newkey] = fila[llave];
                    });
                    new_row['estado'] = 0;
                    datos.push(new_row);
                });
            }
            $.ID_1401525657.update({
                data: datos
            });
        }
    };
    var ID_1365479032 = setTimeout(ID_1365479032_func, 1000 * 0.2);

}

$.ID_937114202.init({
    titulo: 'Entrepisos',
    cargando: 'cargando...',
    __id: 'ALL937114202',
    oncerrar: Cerrar_ID_477244264,
    left: 0,
    hint: 'Seleccione entrepisos',
    color: 'verde',
    subtitulo: 'Indique los tipos',
    right: 0,
    onclick: Click_ID_956306880,
    onafterinit: Afterinit_ID_1781027546
});

function Click_ID_956306880(e) {

    var evento = e;

}

function Cerrar_ID_477244264(e) {

    var evento = e;
    $.ID_937114202.update({});
    $.nivel.set({
        ids_entrepisos: evento.valores
    });
    if ('nivel' in $) $nivel = $.nivel.toJSON();

}

function Afterinit_ID_1781027546(e) {

    var evento = e;
    /** 
     * ejecutamos desfasado para no congelar el hilo. 
     */
    var ID_909342769_func = function() {
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
            var ID_1808221193_i = Alloy.createCollection('insp_niveles');
            var ID_1808221193_i_where = 'id=\'' + args._dato.id + '\'';
            ID_1808221193_i.fetch({
                query: 'SELECT * FROM insp_niveles WHERE id=\'' + args._dato.id + '\''
            });
            var insp_datos = require('helper').query2array(ID_1808221193_i);
            if (insp_datos && insp_datos.length) {
                var ID_1244365283_i = Alloy.createCollection('entrepisos');
                var ID_1244365283_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
                ID_1244365283_i.fetch({
                    query: 'SELECT * FROM entrepisos WHERE pais_texto=\'' + pais[0].nombre + '\''
                });
                var entrepisos = require('helper').query2array(ID_1244365283_i);
                var padre_index = 0;
                _.each(entrepisos, function(padre, padre_pos, padre_list) {
                    padre_index += 1;
                    if (_.isNull(insp_datos[0].ids_entrepisos)) {
                        /** 
                         * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
                         */
                        padre._estado = 0;
                    } else if (insp_datos[0].ids_entrepisos.indexOf(padre.id_segured) != -1) {
                        padre._estado = 1;
                    } else {
                        padre._estado = 0;
                    }
                });
                var datos = [];
                _.each(entrepisos, function(fila, pos) {
                    var new_row = {};
                    _.each(fila, function(x, llave) {
                        var newkey = '';
                        if (llave == 'nombre') newkey = 'label';
                        if (llave == 'id_segured') newkey = 'valor';
                        if (llave == '_estado') newkey = 'estado';
                        if (newkey != '') new_row[newkey] = fila[llave];
                    });
                    datos.push(new_row);
                });
            } else {
                var ID_465895524_i = Alloy.createCollection('entrepisos');
                ID_465895524_i.fetch();
                var ID_465895524_src = require('helper').query2array(ID_465895524_i);
                var datos = [];
                _.each(ID_465895524_src, function(fila, pos) {
                    var new_row = {};
                    _.each(fila, function(x, llave) {
                        var newkey = '';
                        if (llave == 'nombre') newkey = 'label';
                        if (llave == 'id_segured') newkey = 'valor';
                        if (newkey != '') new_row[newkey] = fila[llave];
                    });
                    new_row['estado'] = 0;
                    datos.push(new_row);
                });
            }
            $.ID_937114202.update({
                data: datos
            });
        } else {
            /** 
             * cargamos datos dummy para cuando compilamos de forma individual. 
             */
            var ID_1988102895_i = Alloy.Collections.entrepisos;
            var sql = "DELETE FROM " + ID_1988102895_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_1988102895_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_1988102895_i.trigger('delete');
            var item_index = 0;
            _.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
                item_index += 1;
                var ID_115814080_m = Alloy.Collections.entrepisos;
                var ID_115814080_fila = Alloy.createModel('entrepisos', {
                    nombre: 'Entrepiso ' + item,
                    id_server: '0' + item,
                    id_segured: '10' + item,
                    pais: 1
                });
                ID_115814080_m.add(ID_115814080_fila);
                ID_115814080_fila.save();
            });
            var ID_551033632_i = Alloy.createCollection('insp_niveles');
            var ID_551033632_i_where = 'id=\'' + args._dato.id + '\'';
            ID_551033632_i.fetch({
                query: 'SELECT * FROM insp_niveles WHERE id=\'' + args._dato.id + '\''
            });
            var insp_datos = require('helper').query2array(ID_551033632_i);
            if (insp_datos && insp_datos.length) {
                var ID_179826138_i = Alloy.createCollection('entrepisos');
                var ID_179826138_i_where = '';
                ID_179826138_i.fetch();
                var entrepisos = require('helper').query2array(ID_179826138_i);
                var padre_index = 0;
                _.each(entrepisos, function(padre, padre_pos, padre_list) {
                    padre_index += 1;
                    if (_.isNull(insp_datos[0].ids_entrepisos)) {
                        /** 
                         * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
                         */
                        padre._estado = 0;
                    } else if (insp_datos[0].ids_entrepisos.indexOf(padre.id_segured) != -1) {
                        padre._estado = 1;
                    } else {
                        padre._estado = 0;
                    }
                });
                var datos = [];
                _.each(entrepisos, function(fila, pos) {
                    var new_row = {};
                    _.each(fila, function(x, llave) {
                        var newkey = '';
                        if (llave == 'nombre') newkey = 'label';
                        if (llave == 'id_segured') newkey = 'valor';
                        if (llave == '_estado') newkey = 'estado';
                        if (newkey != '') new_row[newkey] = fila[llave];
                    });
                    datos.push(new_row);
                });
            } else {
                var ID_1700203619_i = Alloy.createCollection('entrepisos');
                ID_1700203619_i.fetch();
                var ID_1700203619_src = require('helper').query2array(ID_1700203619_i);
                var datos = [];
                _.each(ID_1700203619_src, function(fila, pos) {
                    var new_row = {};
                    _.each(fila, function(x, llave) {
                        var newkey = '';
                        if (llave == 'nombre') newkey = 'label';
                        if (llave == 'id_segured') newkey = 'valor';
                        if (newkey != '') new_row[newkey] = fila[llave];
                    });
                    new_row['estado'] = 0;
                    datos.push(new_row);
                });
            }
            $.ID_937114202.update({
                data: datos
            });
        }
    };
    var ID_909342769 = setTimeout(ID_909342769_func, 1000 * 0.2);

}

$.ID_1489080142.init({
    titulo: 'Pavimentos',
    cargando: 'cargando...',
    __id: 'ALL1489080142',
    oncerrar: Cerrar_ID_1900911647,
    left: 0,
    hint: 'Seleccione pavimentos',
    color: 'verde',
    subtitulo: 'Indique los tipos',
    right: 0,
    onclick: Click_ID_747978606,
    onafterinit: Afterinit_ID_19811522
});

function Click_ID_747978606(e) {

    var evento = e;

}

function Cerrar_ID_1900911647(e) {

    var evento = e;
    $.ID_1489080142.update({});
    $.nivel.set({
        ids_pavimentos: evento.valores
    });
    if ('nivel' in $) $nivel = $.nivel.toJSON();

}

function Afterinit_ID_19811522(e) {

    var evento = e;
    /** 
     * ejecutamos desfasado para no congelar el hilo. 
     */
    var ID_941907377_func = function() {
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
            var ID_1539152570_i = Alloy.createCollection('insp_niveles');
            var ID_1539152570_i_where = 'id=\'' + args._dato.id + '\'';
            ID_1539152570_i.fetch({
                query: 'SELECT * FROM insp_niveles WHERE id=\'' + args._dato.id + '\''
            });
            var insp_datos = require('helper').query2array(ID_1539152570_i);
            if (insp_datos && insp_datos.length) {
                var ID_384769901_i = Alloy.createCollection('pavimento');
                var ID_384769901_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
                ID_384769901_i.fetch({
                    query: 'SELECT * FROM pavimento WHERE pais_texto=\'' + pais[0].nombre + '\''
                });
                var pavimento = require('helper').query2array(ID_384769901_i);
                var padre_index = 0;
                _.each(pavimento, function(padre, padre_pos, padre_list) {
                    padre_index += 1;
                    if (_.isNull(insp_datos[0].ids_pavimentos)) {
                        /** 
                         * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
                         */
                        padre._estado = 0;
                    } else if (insp_datos[0].ids_pavimentos.indexOf(padre.id_segured) != -1) {
                        padre._estado = 1;
                    } else {
                        padre._estado = 0;
                    }
                });
                var datos = [];
                _.each(pavimento, function(fila, pos) {
                    var new_row = {};
                    _.each(fila, function(x, llave) {
                        var newkey = '';
                        if (llave == 'nombre') newkey = 'label';
                        if (llave == 'id_segured') newkey = 'valor';
                        if (llave == '_estado') newkey = 'estado';
                        if (newkey != '') new_row[newkey] = fila[llave];
                    });
                    datos.push(new_row);
                });
            } else {
                var ID_415567811_i = Alloy.createCollection('pavimento');
                ID_415567811_i.fetch();
                var ID_415567811_src = require('helper').query2array(ID_415567811_i);
                var datos = [];
                _.each(ID_415567811_src, function(fila, pos) {
                    var new_row = {};
                    _.each(fila, function(x, llave) {
                        var newkey = '';
                        if (llave == 'nombre') newkey = 'label';
                        if (llave == 'id_segured') newkey = 'valor';
                        if (newkey != '') new_row[newkey] = fila[llave];
                    });
                    new_row['estado'] = 0;
                    datos.push(new_row);
                });
            }
            $.ID_1489080142.update({
                data: datos
            });
        } else {
            /** 
             * cargamos datos dummy para cuando compilamos de forma individual. 
             */
            var ID_438637791_i = Alloy.Collections.pavimento;
            var sql = "DELETE FROM " + ID_438637791_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_438637791_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_438637791_i.trigger('delete');
            var item_index = 0;
            _.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
                item_index += 1;
                var ID_1497496484_m = Alloy.Collections.pavimento;
                var ID_1497496484_fila = Alloy.createModel('pavimento', {
                    nombre: 'Pavimento ' + item,
                    id_server: '0' + item,
                    id_segured: '10' + item,
                    pais: 1
                });
                ID_1497496484_m.add(ID_1497496484_fila);
                ID_1497496484_fila.save();
            });
            var ID_716708520_i = Alloy.createCollection('insp_niveles');
            var ID_716708520_i_where = 'id=\'' + args._dato.id + '\'';
            ID_716708520_i.fetch({
                query: 'SELECT * FROM insp_niveles WHERE id=\'' + args._dato.id + '\''
            });
            var insp_datos = require('helper').query2array(ID_716708520_i);
            if (insp_datos && insp_datos.length) {
                var ID_472030820_i = Alloy.createCollection('pavimento');
                var ID_472030820_i_where = '';
                ID_472030820_i.fetch();
                var pavimento = require('helper').query2array(ID_472030820_i);
                var padre_index = 0;
                _.each(pavimento, function(padre, padre_pos, padre_list) {
                    padre_index += 1;
                    if (_.isNull(insp_datos[0].ids_pavimentos)) {
                        /** 
                         * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
                         */
                        padre._estado = 0;
                    } else if (insp_datos[0].ids_pavimentos.indexOf(padre.id_segured) != -1) {
                        padre._estado = 1;
                    } else {
                        padre._estado = 0;
                    }
                });
                var datos = [];
                _.each(pavimento, function(fila, pos) {
                    var new_row = {};
                    _.each(fila, function(x, llave) {
                        var newkey = '';
                        if (llave == 'nombre') newkey = 'label';
                        if (llave == 'id_segured') newkey = 'valor';
                        if (llave == '_estado') newkey = 'estado';
                        if (newkey != '') new_row[newkey] = fila[llave];
                    });
                    datos.push(new_row);
                });
            } else {
                var ID_439160555_i = Alloy.createCollection('pavimento');
                ID_439160555_i.fetch();
                var ID_439160555_src = require('helper').query2array(ID_439160555_i);
                var datos = [];
                _.each(ID_439160555_src, function(fila, pos) {
                    var new_row = {};
                    _.each(fila, function(x, llave) {
                        var newkey = '';
                        if (llave == 'nombre') newkey = 'label';
                        if (llave == 'id_segured') newkey = 'valor';
                        if (newkey != '') new_row[newkey] = fila[llave];
                    });
                    new_row['estado'] = 0;
                    datos.push(new_row);
                });
            }
            $.ID_1489080142.update({
                data: datos
            });
        }
    };
    var ID_941907377 = setTimeout(ID_941907377_func, 1000 * 0.2);

}

$.ID_129921645.init({
    titulo: 'Estruct. cubierta',
    cargando: 'cargando...',
    __id: 'ALL129921645',
    oncerrar: Cerrar_ID_165135819,
    left: 0,
    hint: 'Seleccione e.cubiertas',
    color: 'verde',
    subtitulo: 'Indique los tipos',
    right: 0,
    onclick: Click_ID_1248569898,
    onafterinit: Afterinit_ID_961613364
});

function Click_ID_1248569898(e) {

    var evento = e;

}

function Cerrar_ID_165135819(e) {

    var evento = e;
    $.ID_129921645.update({});
    $.nivel.set({
        ids_estructura_cubiera: evento.valores
    });
    if ('nivel' in $) $nivel = $.nivel.toJSON();

}

function Afterinit_ID_961613364(e) {

    var evento = e;
    /** 
     * ejecutamos desfasado para no congelar el hilo. 
     */
    var ID_1394456967_func = function() {
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
            var ID_1770285294_i = Alloy.createCollection('insp_niveles');
            var ID_1770285294_i_where = 'id=\'' + args._dato.id + '\'';
            ID_1770285294_i.fetch({
                query: 'SELECT * FROM insp_niveles WHERE id=\'' + args._dato.id + '\''
            });
            var insp_datos = require('helper').query2array(ID_1770285294_i);
            if (insp_datos && insp_datos.length) {
                var ID_646832600_i = Alloy.createCollection('estructura_cubierta');
                var ID_646832600_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
                ID_646832600_i.fetch({
                    query: 'SELECT * FROM estructura_cubierta WHERE pais_texto=\'' + pais[0].nombre + '\''
                });
                var estructura_cubierta = require('helper').query2array(ID_646832600_i);
                var padre_index = 0;
                _.each(estructura_cubierta, function(padre, padre_pos, padre_list) {
                    padre_index += 1;
                    if (_.isNull(insp_datos[0].ids_estructura_cubiera)) {
                        /** 
                         * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
                         */
                        padre._estado = 0;
                    } else if (insp_datos[0].ids_estructura_cubiera.indexOf(padre.id_segured) != -1) {
                        padre._estado = 1;
                    } else {
                        padre._estado = 0;
                    }
                });
                var datos = [];
                _.each(estructura_cubierta, function(fila, pos) {
                    var new_row = {};
                    _.each(fila, function(x, llave) {
                        var newkey = '';
                        if (llave == 'nombre') newkey = 'label';
                        if (llave == 'id_segured') newkey = 'valor';
                        if (llave == '_estado') newkey = 'estado';
                        if (newkey != '') new_row[newkey] = fila[llave];
                    });
                    datos.push(new_row);
                });
            } else {
                var ID_1068012317_i = Alloy.createCollection('estructura_cubierta');
                ID_1068012317_i.fetch();
                var ID_1068012317_src = require('helper').query2array(ID_1068012317_i);
                var datos = [];
                _.each(ID_1068012317_src, function(fila, pos) {
                    var new_row = {};
                    _.each(fila, function(x, llave) {
                        var newkey = '';
                        if (llave == 'nombre') newkey = 'label';
                        if (llave == 'id_segured') newkey = 'valor';
                        if (newkey != '') new_row[newkey] = fila[llave];
                    });
                    new_row['estado'] = 0;
                    datos.push(new_row);
                });
            }
            $.ID_129921645.update({
                data: datos
            });
        } else {
            /** 
             * cargamos datos dummy para cuando compilamos de forma individual. 
             */
            var ID_814914083_i = Alloy.Collections.estructura_cubierta;
            var sql = "DELETE FROM " + ID_814914083_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_814914083_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_814914083_i.trigger('delete');
            var item_index = 0;
            _.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
                item_index += 1;
                var ID_1375112466_m = Alloy.Collections.estructura_cubierta;
                var ID_1375112466_fila = Alloy.createModel('estructura_cubierta', {
                    nombre: 'Estructura cubierta ' + item,
                    id_server: '0' + item,
                    id_segured: '10' + item,
                    pais: 1
                });
                ID_1375112466_m.add(ID_1375112466_fila);
                ID_1375112466_fila.save();
            });
            var ID_1563781435_i = Alloy.createCollection('insp_niveles');
            var ID_1563781435_i_where = 'id=\'' + args._dato.id + '\'';
            ID_1563781435_i.fetch({
                query: 'SELECT * FROM insp_niveles WHERE id=\'' + args._dato.id + '\''
            });
            var insp_datos = require('helper').query2array(ID_1563781435_i);
            if (insp_datos && insp_datos.length) {
                var ID_628141787_i = Alloy.createCollection('estructura_cubierta');
                var ID_628141787_i_where = '';
                ID_628141787_i.fetch();
                var estructura_cubierta = require('helper').query2array(ID_628141787_i);
                if (Ti.App.deployType != 'production') console.log('log pio', {
                    "insp": insp_datos,
                    "estruc": estructura_cubierta[0]
                });
                var padre_index = 0;
                _.each(estructura_cubierta, function(padre, padre_pos, padre_list) {
                    padre_index += 1;
                    if (_.isNull(insp_datos[0].ids_estructura_cubiera)) {
                        /** 
                         * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
                         */
                        padre._estado = 0;
                    } else if (insp_datos[0].ids_estructura_cubiera.indexOf(padre.id_segured) != -1) {
                        padre._estado = 1;
                    } else {
                        padre._estado = 0;
                    }
                });
                var datos = [];
                _.each(estructura_cubierta, function(fila, pos) {
                    var new_row = {};
                    _.each(fila, function(x, llave) {
                        var newkey = '';
                        if (llave == 'nombre') newkey = 'label';
                        if (llave == 'id_segured') newkey = 'valor';
                        if (llave == '_estado') newkey = 'estado';
                        if (newkey != '') new_row[newkey] = fila[llave];
                    });
                    datos.push(new_row);
                });
            } else {
                var ID_114334984_i = Alloy.createCollection('estructura_cubierta');
                ID_114334984_i.fetch();
                var ID_114334984_src = require('helper').query2array(ID_114334984_i);
                var datos = [];
                _.each(ID_114334984_src, function(fila, pos) {
                    var new_row = {};
                    _.each(fila, function(x, llave) {
                        var newkey = '';
                        if (llave == 'nombre') newkey = 'label';
                        if (llave == 'id_segured') newkey = 'valor';
                        if (newkey != '') new_row[newkey] = fila[llave];
                    });
                    new_row['estado'] = 0;
                    datos.push(new_row);
                });
            }
            $.ID_129921645.update({
                data: datos
            });
        }
    };
    var ID_1394456967 = setTimeout(ID_1394456967_func, 1000 * 0.2);

}

$.ID_296719183.init({
    titulo: 'Cubierta',
    cargando: 'cargando...',
    __id: 'ALL296719183',
    oncerrar: Cerrar_ID_276538721,
    left: 0,
    hint: 'Seleccione cubiertas',
    color: 'verde',
    subtitulo: 'Indique los tipos',
    right: 0,
    onclick: Click_ID_1908652444,
    onafterinit: Afterinit_ID_97347643
});

function Click_ID_1908652444(e) {

    var evento = e;

}

function Cerrar_ID_276538721(e) {

    var evento = e;
    $.ID_296719183.update({});
    $.nivel.set({
        ids_cubierta: evento.valores
    });
    if ('nivel' in $) $nivel = $.nivel.toJSON();

}

function Afterinit_ID_97347643(e) {

    var evento = e;
    /** 
     * ejecutamos desfasado para no congelar el hilo. 
     */
    var ID_453858558_func = function() {
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
            var ID_458809751_i = Alloy.createCollection('insp_niveles');
            var ID_458809751_i_where = 'id=\'' + args._dato.id + '\'';
            ID_458809751_i.fetch({
                query: 'SELECT * FROM insp_niveles WHERE id=\'' + args._dato.id + '\''
            });
            var insp_datos = require('helper').query2array(ID_458809751_i);
            if (insp_datos && insp_datos.length) {
                var ID_1800901159_i = Alloy.createCollection('cubierta');
                var ID_1800901159_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
                ID_1800901159_i.fetch({
                    query: 'SELECT * FROM cubierta WHERE pais_texto=\'' + pais[0].nombre + '\''
                });
                var cubierta = require('helper').query2array(ID_1800901159_i);
                var padre_index = 0;
                _.each(cubierta, function(padre, padre_pos, padre_list) {
                    padre_index += 1;
                    if (_.isNull(insp_datos[0].ids_cubierta)) {
                        /** 
                         * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
                         */
                        padre._estado = 0;
                    } else if (insp_datos[0].ids_cubierta.indexOf(padre.id_segured) != -1) {
                        padre._estado = 1;
                    } else {
                        padre._estado = 0;
                    }
                });
                var datos = [];
                _.each(cubierta, function(fila, pos) {
                    var new_row = {};
                    _.each(fila, function(x, llave) {
                        var newkey = '';
                        if (llave == 'nombre') newkey = 'label';
                        if (llave == 'id_segured') newkey = 'valor';
                        if (llave == '_estado') newkey = 'estado';
                        if (newkey != '') new_row[newkey] = fila[llave];
                    });
                    datos.push(new_row);
                });
            } else {
                var ID_430771790_i = Alloy.createCollection('cubierta');
                ID_430771790_i.fetch();
                var ID_430771790_src = require('helper').query2array(ID_430771790_i);
                var datos = [];
                _.each(ID_430771790_src, function(fila, pos) {
                    var new_row = {};
                    _.each(fila, function(x, llave) {
                        var newkey = '';
                        if (llave == 'nombre') newkey = 'label';
                        if (llave == 'id_segured') newkey = 'valor';
                        if (newkey != '') new_row[newkey] = fila[llave];
                    });
                    new_row['estado'] = 0;
                    datos.push(new_row);
                });
            }
            $.ID_296719183.update({
                data: datos
            });
        } else {
            /** 
             * cargamos datos dummy para cuando compilamos de forma individual. 
             */
            var ID_1371905429_i = Alloy.Collections.cubierta;
            var sql = "DELETE FROM " + ID_1371905429_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_1371905429_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_1371905429_i.trigger('delete');
            var item_index = 0;
            _.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
                item_index += 1;
                var ID_12733595_m = Alloy.Collections.cubierta;
                var ID_12733595_fila = Alloy.createModel('cubierta', {
                    nombre: 'Cubierta ' + item,
                    id_server: '0' + item,
                    id_segured: '10' + item,
                    pais: 1
                });
                ID_12733595_m.add(ID_12733595_fila);
                ID_12733595_fila.save();
            });
            var ID_564421186_i = Alloy.createCollection('insp_niveles');
            var ID_564421186_i_where = 'id=\'' + args._dato.id + '\'';
            ID_564421186_i.fetch({
                query: 'SELECT * FROM insp_niveles WHERE id=\'' + args._dato.id + '\''
            });
            var insp_datos = require('helper').query2array(ID_564421186_i);
            if (insp_datos && insp_datos.length) {
                var ID_1105163729_i = Alloy.createCollection('cubierta');
                var ID_1105163729_i_where = '';
                ID_1105163729_i.fetch();
                var cubierta = require('helper').query2array(ID_1105163729_i);
                var padre_index = 0;
                _.each(cubierta, function(padre, padre_pos, padre_list) {
                    padre_index += 1;
                    if (_.isNull(insp_datos[0].ids_cubierta)) {
                        /** 
                         * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
                         */
                        padre._estado = 0;
                    } else if (insp_datos[0].ids_cubierta.indexOf(padre.id_segured) != -1) {
                        padre._estado = 1;
                    } else {
                        padre._estado = 0;
                    }
                });
                var datos = [];
                _.each(cubierta, function(fila, pos) {
                    var new_row = {};
                    _.each(fila, function(x, llave) {
                        var newkey = '';
                        if (llave == 'nombre') newkey = 'label';
                        if (llave == 'id_segured') newkey = 'valor';
                        if (llave == '_estado') newkey = 'estado';
                        if (newkey != '') new_row[newkey] = fila[llave];
                    });
                    datos.push(new_row);
                });
            } else {
                var ID_1370199092_i = Alloy.createCollection('cubierta');
                ID_1370199092_i.fetch();
                var ID_1370199092_src = require('helper').query2array(ID_1370199092_i);
                var datos = [];
                _.each(ID_1370199092_src, function(fila, pos) {
                    var new_row = {};
                    _.each(fila, function(x, llave) {
                        var newkey = '';
                        if (llave == 'nombre') newkey = 'label';
                        if (llave == 'id_segured') newkey = 'valor';
                        if (newkey != '') new_row[newkey] = fila[llave];
                    });
                    new_row['estado'] = 0;
                    datos.push(new_row);
                });
            }
            $.ID_296719183.update({
                data: datos
            });
        }
    };
    var ID_453858558 = setTimeout(ID_453858558_func, 1000 * 0.2);

}

(function() {
    /** 
     * heredamos datos previos de nivel en modelo nuevo con binding de esta pantalla 
     */
    var ID_1794709068_i = Alloy.createCollection('insp_niveles');
    var ID_1794709068_i_where = 'id=\'' + args._dato.id + '\'';
    ID_1794709068_i.fetch({
        query: 'SELECT * FROM insp_niveles WHERE id=\'' + args._dato.id + '\''
    });
    var insp_n = require('helper').query2array(ID_1794709068_i);
    /** 
     * Cargamos los datos en la tabla 
     */
    $.nivel.set({
        ids_entrepisos: insp_n[0].ids_entrepisos,
        id_inspeccion: insp_n[0].id_inspeccion,
        nombre: insp_n[0].nombre,
        superficie: insp_n[0].superficie,
        largo: insp_n[0].largo,
        ids_pavimentos: insp_n[0].ids_pavimentos,
        alto: insp_n[0].alto,
        ano: insp_n[0].ano,
        ids_muros: insp_n[0].ids_muros,
        ancho: insp_n[0].ancho,
        ids_estructura_cubiera: insp_n[0].ids_estructura_cubiera,
        piso: insp_n[0].piso,
        ids_cubierta: insp_n[0].ids_cubierta,
        ids_estructuras_soportantes: insp_n[0].ids_estructuras_soportantes
    });
    if ('nivel' in $) $nivel = $.nivel.toJSON();
    /** 
     * Cargamos textos en la pantalla 
     */
    $.ID_1553156874.setValue(insp_n[0].nombre);

    $.ID_103828995.setValue(insp_n[0].piso);

    $.ID_379385469.setValue(insp_n[0].ano);

    $.ID_946615753.setValue(insp_n[0].largo);

    $.ID_465957073.setValue(insp_n[0].ancho);

    $.ID_495858379.setValue(insp_n[0].alto);

    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    if (seltarea.exige_medidas == 0 || seltarea.exige_medidas == '0') {
        var scrollview;
        scrollview = $.ID_1467197025;
        var vista_mseguridad;
        vista_mseguridad = $.ID_1537811073;
        scrollview.remove(vista_mseguridad);
    } else {
        $.nivel.set({
            ids_medidas_seguridad: insp_n[0].ids_medidas_seguridad
        });
        if ('nivel' in $) $nivel = $.nivel.toJSON();
        var ID_1537811073_visible = true;

        if (ID_1537811073_visible == 'si') {
            ID_1537811073_visible = true;
        } else if (ID_1537811073_visible == 'no') {
            ID_1537811073_visible = false;
        }
        $.ID_1537811073.setVisible(ID_1537811073_visible);

    }
    /** 
     * Dejamos el scroll en el top y desenfocamos los campos de texto 
     */
    var ID_1200217363_func = function() {
        $.ID_1553156874.blur();
        $.ID_103828995.blur();
        $.ID_379385469.blur();
        $.ID_946615753.blur();
        $.ID_465957073.blur();
        $.ID_495858379.blur();
        $.ID_1467197025.scrollToTop();
    };
    var ID_1200217363 = setTimeout(ID_1200217363_func, 1000 * 0.2);
    /** 
     * Mostramos statusbar 
     */
    var ID_463357042_func = function() {
        var ID_1917979189_statusbar = '#57BC8B';

        var setearStatusColor = function(ID_1917979189_statusbar) {
            if (OS_IOS) {
                if (ID_1917979189_statusbar == 'light' || ID_1917979189_statusbar == 'claro') {
                    $.ID_1917979189_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.LIGHT_CONTENT);
                } else if (ID_1917979189_statusbar == 'grey' || ID_1917979189_statusbar == 'gris' || ID_1917979189_statusbar == 'gray') {
                    $.ID_1917979189_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.GREY);
                } else if (ID_1917979189_statusbar == 'oscuro' || ID_1917979189_statusbar == 'dark') {
                    $.ID_1917979189_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.DEFAULT);
                }
            } else if (OS_ANDROID) {
                abx.setStatusbarColor(ID_1917979189_statusbar);
            }
        };
        setearStatusColor(ID_1917979189_statusbar);

    };
    var ID_463357042 = setTimeout(ID_463357042_func, 1000 * 0.1);
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
        $.nivel.set({
            id_inspeccion: seltarea.id_server
        });
        if ('nivel' in $) $nivel = $.nivel.toJSON();
        require('vars')[_var_scopekey]['faltan_medidas'] = 'false';
    }
})();

function Postlayout_ID_308024503(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var ID_1688288258_func = function() {
        /** 
         * Una vez ya cargada la tarea, dejamos la variable limpia para poder abrir otras pantallas 
         */
        require('vars')['var_abriendo'] = '';
    };
    var ID_1688288258 = setTimeout(ID_1688288258_func, 1000 * 0.2);

}
if (OS_IOS || OS_ANDROID) {
    $.ID_1917979189.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}