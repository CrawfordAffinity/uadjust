var _bind4section = {};
var _list_templates = {};

var _activity;
if (OS_ANDROID) {
    _activity = $.ID_1124023962.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_1124023962';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_1124023962.addEventListener('open', function(e) {});
}
$.ID_1124023962.orientationModes = [Titanium.UI.PORTRAIT];


$.ID_59432441.init({
    titulo: 'FINALIZAR',
    __id: 'ALL59432441',
    color: 'verde',
    onclick: Click_ID_65453509
});

function Click_ID_65453509(e) {

    var evento = e;
    var ID_863352743_opts = ['si', 'limpiar', 'cancelar'];
    var ID_863352743 = Ti.UI.createAlertDialog({
        title: 'Por favor, confirme',
        message: 'Esta conforme con la firma?',
        buttonNames: ID_863352743_opts
    });
    ID_863352743.addEventListener('click', function(e) {
        var resp = ID_863352743_opts[e.index];
        if (resp == 'si') {
            /** 
             * Llamamos al evento dentro de la vista web para obtener la imagen de la forma 
             */

            Ti.App.fireEvent('canvasGetImage', {
                message: 'event fired from Titanium, handled in WebView'
            });
            /** 
             * Al string 
             */

            Ti.App.addEventListener("canvasGetImageResponse", function(e) {
                var stripped = e.image.replace("data:image/png;base64,", "");
                var click_prox = ('click_prox' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['click_prox'] : '';
                if (click_prox == false || click_prox == 'false') {
                    /** 
                     * Flag para que no se ejecute dos veces el click 
                     */
                    require('vars')[_var_scopekey]['click_prox'] = 'true';
                    var ID_614759559_m = Alloy.Collections.numero_unico;
                    var ID_614759559_fila = Alloy.createModel('numero_unico', {
                        comentario: 'firma'
                    });
                    ID_614759559_m.add(ID_614759559_fila);
                    ID_614759559_fila.save();
                    var nuevoid = require('helper').model2object(ID_614759559_m.last());
                    /** 
                     * Convertimos de string a imagen para comprimir 
                     */
                    var firma_png = Ti.Utils.base64decode(stripped);
                    /** 
                     * Comprimimos a JPG 
                     */
                    if (OS_ANDROID) {
                        var ID_375516339_imagefactory = require('ti.imagefactory');
                        var firma_jpg = ID_375516339_imagefactory.compress(firma_png, 0.9);
                    } else if (OS_IOS) {
                        var ID_375516339_imagefactory = require('ti.imagefactory');
                        var firma_jpg = ID_375516339_imagefactory.compress(firma_png, 0.9);
                    }
                    var id_insp = ('id_insp' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['id_insp'] : '';
                    /** 
                     * Guardamos en disco de telefono 
                     */
                    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
                    if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
                        var ID_1153703423_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + id_insp);
                        if (ID_1153703423_d.exists() == false) ID_1153703423_d.createDirectory();
                        var ID_1153703423_f = Ti.Filesystem.getFile(ID_1153703423_d.resolve(), 'firma' + nuevoid.id + '.jpg');
                        if (ID_1153703423_f.exists() == true) ID_1153703423_f.deleteFile();
                        ID_1153703423_f.write(firma_jpg);
                        ID_1153703423_d = null;
                        ID_1153703423_f = null;
                    } else {
                        var ID_1633907683_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + 0);
                        if (ID_1633907683_d.exists() == false) ID_1633907683_d.createDirectory();
                        var ID_1633907683_f = Ti.Filesystem.getFile(ID_1633907683_d.resolve(), 'firma' + nuevoid.id + '.jpg');
                        if (ID_1633907683_f.exists() == true) ID_1633907683_f.deleteFile();
                        ID_1633907683_f.write(firma_jpg);
                        ID_1633907683_d = null;
                        ID_1633907683_f = null;
                    }
                    var ID_327988762_m = Alloy.Collections.insp_firma;
                    var ID_327988762_fila = Alloy.createModel('insp_firma', {
                        firma64: 'firma' + nuevoid.id + '.jpg',
                        id_inspeccion: id_insp
                    });
                    ID_327988762_m.add(ID_327988762_fila);
                    ID_327988762_fila.save();
                    /** 
                     * Limpiamos memoria 
                     */
                    var seltarea = null,
                        firma_jpg = null,
                        firma_png = null,
                        evento = null;
                    /** 
                     * Limpiamos variable 
                     */
                    require('vars')['firma_accion'] = '';
                    /** 
                     * Tratamos de cerrar todas las pantallas de inspeccion 
                     */

                    var ID_1042874148_trycatch = {
                        error: function(e) {
                            if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
                        }
                    };
                    try {
                        ID_1042874148_trycatch.error = function(evento) {};

                        Alloy.Events.trigger('_cerrar_insp');
                    } catch (e) {
                        ID_1042874148_trycatch.error(e);
                    }
                    var ID_1508962524_i = Alloy.createCollection('insp_firma');
                    var ID_1508962524_i_where = '';
                    ID_1508962524_i.fetch();
                    var cantfirma = require('helper').query2array(ID_1508962524_i);
                    if (Ti.App.deployType != 'production') console.log('firmas en total', {
                        "datos": cantfirma
                    });
                    try {
                        console.log('cerrando en el click');
                        $.ID_1124023962.close();
                        console.log('cerro en el click');
                    } catch (err) {
                        console.log('Se cayo en el click', err);
                    }

                    Alloy.createController("fin_index", {}).getView().open();
                }

            });
        } else if (resp == 'limpiar') {
            /** 
             * Recargamos la vista web, y asi limpiamos la firma 
             */
            var webview;
            webview = $.ID_22026903;
            webview.reload();
        } else {}
        resp = null;

        e.source.removeEventListener("click", arguments.callee);
    });
    ID_863352743.show();

}

function Postlayout_ID_1335244247(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var insp_cancelada = ('insp_cancelada' in require('vars')) ? require('vars')['insp_cancelada'] : '';
    if ((_.isObject(insp_cancelada) || (_.isString(insp_cancelada)) && !_.isEmpty(insp_cancelada)) || _.isNumber(insp_cancelada) || _.isBoolean(insp_cancelada)) {
        /** 
         * Dependiendo del flag (para saber si se cancelo la inspeccion) cerramos pantalla de insp cancelada o documentos 
         */

        Alloy.Events.trigger('_cerrar_insp', {
            pantalla: insp_cancelada
        });
    } else {
        try {
            Alloy.Events.trigger('_cerrar_insp', {
                pantalla: 'vistaweb'
            });
        } catch (err) {
            console.log('no cerro la wea de vista web en la firma');
        }

    }
    if (Ti.App.deployType != 'production') console.log('vamos a refrescar la vista', {});
    var cargada = ('cargada' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['cargada'] : '';
    if (cargada == false || cargada == 'false') {
        require('vars')[_var_scopekey]['cargada'] = 'true';
        var ID_1879861421_func = function() {
            var webview;
            webview = $.ID_22026903;
            webview.reload();
            if (Ti.App.deployType != 'production') console.log('recargando vistaweb', {});
        };
        var ID_1879861421 = setTimeout(ID_1879861421_func, 1000 * 0.5);
    }

}

(function() {
    require('vars')[_var_scopekey]['cargada'] = 'false';
    require('vars')[_var_scopekey]['click_prox'] = 'false';
    /** 
     * Ejecutamos en 0.1 segundos para cambiar el color del statusbar 
     */
    var ID_71342109_func = function() {
        var ID_1124023962_statusbar = '#006C9B';

        var setearStatusColor = function(ID_1124023962_statusbar) {
            if (OS_IOS) {
                if (ID_1124023962_statusbar == 'light' || ID_1124023962_statusbar == 'claro') {
                    $.ID_1124023962_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.LIGHT_CONTENT);
                } else if (ID_1124023962_statusbar == 'grey' || ID_1124023962_statusbar == 'gris' || ID_1124023962_statusbar == 'gray') {
                    $.ID_1124023962_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.GREY);
                } else if (ID_1124023962_statusbar == 'oscuro' || ID_1124023962_statusbar == 'dark') {
                    $.ID_1124023962_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.DEFAULT);
                }
            } else if (OS_ANDROID) {
                abx.setStatusbarColor(ID_1124023962_statusbar);
            }
        };
        setearStatusColor(ID_1124023962_statusbar);

    };
    var ID_71342109 = setTimeout(ID_71342109_func, 1000 * 0.1);
})();


/** 
 * Completamos datos de persona presente 
 */
var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
    var ID_1509619879_i = Alloy.createCollection('inspecciones');
    var ID_1509619879_i_where = '';
    ID_1509619879_i.fetch();
    var ultima = require('helper').query2array(ID_1509619879_i);
    if (Ti.App.deployType != 'production') console.log('id server asignado a firma', {
        "ultima": ultima[ultima.length - 1].id_server,
        "previo": seltarea.id_server
    });
    require('vars')[_var_scopekey]['id_insp'] = ultima[ultima.length - 1].id_server;
    var ID_843062733_i = Alloy.createCollection('insp_datosbasicos');
    var ID_843062733_i_where = 'id_inspeccion=\'' + seltarea.id_server + '\'';
    ID_843062733_i.fetch({
        query: 'SELECT * FROM insp_datosbasicos WHERE id_inspeccion=\'' + seltarea.id_server + '\''
    });
    var previos = require('helper').query2array(ID_843062733_i);
    if (Ti.App.deployType != 'production') console.log('datos basicos en firma', {
        "datos": previos
    });
    if (previos && previos.length) {
        $.ID_56241598.setText(previos[0].presente_nombre);

        $.ID_58192199.setText(previos[0].presente_rut);

    } else {
        if (Ti.App.deployType != 'production') console.log('ERROR FIRMA: insp_datosbasicos no tiene datos de inspeccion', {});
        var ID_455319011_i = Alloy.createCollection('insp_datosbasicos');
        var ID_455319011_i_where = '';
        ID_455319011_i.fetch();
        var ptodos = require('helper').query2array(ID_455319011_i);
        if (Ti.App.deployType != 'production') console.log('ERROR FIRMA: debug insp_datosbasicos full', {
            "full": ptodos
        });
    }
} else {
    $.ID_56241598.setText('Pablo Martinez Herrera');

    $.ID_58192199.setText('5.555.555-5');

}
require('vars')['firma_accion'] = 'nada';
/** 
 * Cerramos esta pantalla cuando se haya llamado el evento desde pantalla finalizaod 
 */

_my_events['_cerrar_insp,ID_1618628689'] = function(evento) {
    //evento.pantalla = 'firma';
    if (!_.isUndefined(evento.pantalla)) {
        if (evento.pantalla == 'firma') {
            if (Ti.App.deployType != 'production') console.log('debug cerrando firma', {});

            var ID_2003061186_trycatch = {
                error: function(e) {
                    if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
                }
            };
            try {
                ID_2003061186_trycatch.error = function(evento) {
                    if (Ti.App.deployType != 'production') console.log('error cerrando firma', {});
                };
                console.log('cerrando en evento1');
                $.ID_56241598.setText('Sigue viva');
                $.ID_1124023962.close();
                console.log('cerro en evento1');
            } catch (e) {
                console.log('catch en evento1');
                ID_2003061186_trycatch.error(e);
            }
        }
    } else {
        if (Ti.App.deployType != 'production') console.log('debug cerrando (todas) firma', {});

        var ID_1367362437_trycatch = {
            error: function(e) {
                if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
            }
        };
        try {
            ID_1367362437_trycatch.error = function(evento) {
                if (Ti.App.deployType != 'production') console.log('error cerrando firma', {});
            };
            console.log('cerrando en evento2');
            $.ID_56241598.setText('Sigue viva');
            $.ID_1124023962.close();
            console.log('cerro en evento2');

        } catch (e) {
            console.log('catch en evento2');
            ID_1367362437_trycatch.error(e);
        }
    }
};
Alloy.Events.on('_cerrar_insp', _my_events['_cerrar_insp,ID_1618628689']);
/** 
 * Revisamos flag para saber si el inspector cancelo la inspeccion 
 */
var insp_cancelada = ('insp_cancelada' in require('vars')) ? require('vars')['insp_cancelada'] : '';
if ((_.isObject(insp_cancelada) || (_.isString(insp_cancelada)) && !_.isEmpty(insp_cancelada)) || _.isNumber(insp_cancelada) || _.isBoolean(insp_cancelada)) {
    var ID_1410235970_visible = true;

    if (ID_1410235970_visible == 'si') {
        ID_1410235970_visible = true;
    } else if (ID_1410235970_visible == 'no') {
        ID_1410235970_visible = false;
    }
    $.ID_1410235970.setVisible(ID_1410235970_visible);

} else {
    var ID_1410235970_visible = false;

    if (ID_1410235970_visible == 'si') {
        ID_1410235970_visible = true;
    } else if (ID_1410235970_visible == 'no') {
        ID_1410235970_visible = false;
    }
    $.ID_1410235970.setVisible(ID_1410235970_visible);

}

function Androidback_ID_1975756856(e) {

    e.cancelBubble = true;
    var elemento = e.source;

}
if (OS_IOS || OS_ANDROID) {
    $.ID_1124023962.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}
//$.ID_1124023962.open();