var _bind4section = {};
var _list_templates = {
    "elemento": {
        "ID_432412973": {},
        "ID_291370846": {
            "text": "{nombre}"
        },
        "ID_1925487537": {
            "text": "{id_server}"
        },
        "ID_975786586": {
            "text": "{id_segured}"
        },
        "ID_1546190443": {}
    },
    "dano": {
        "ID_1569764493": {
            "text": "{id}"
        },
        "ID_304218376": {},
        "ID_1912165224": {
            "text": "{nombre}"
        }
    },
    "pborrar": {
        "ID_1236907100": {
            "text": "{id}"
        },
        "ID_1506538078": {},
        "ID_2024360383": {},
        "ID_1765564351": {},
        "ID_1699280427": {
            "text": "{nombre}"
        },
        "ID_1445903745": {},
        "ID_1795050796": {},
        "ID_1752901998": {},
        "ID_1914367022": {
            "text": "{nombre}"
        },
        "ID_1249074865": {
            "text": "{id}"
        },
        "ID_1131557960": {},
        "ID_1361874287": {},
        "ID_1750057271": {},
        "ID_1962135207": {},
        "ID_1947166298": {
            "text": "{nombre}"
        },
        "ID_2008605730": {
            "text": "{id}"
        },
        "ID_1788775425": {},
        "ID_1246915111": {}
    },
    "contenido": {
        "ID_630800887": {},
        "ID_862352109": {
            "text": "{id}"
        },
        "ID_560452274": {
            "text": "{nombre}"
        }
    },
    "recinto": {
        "ID_1960582596": {},
        "ID_871581291": {
            "text": "{nombre}"
        },
        "ID_310297848": {
            "text": "{id}"
        }
    },
    "nivel": {
        "ID_1021786928": {
            "text": "{id}"
        },
        "ID_1241655463": {
            "text": "{nombre}"
        },
        "ID_599581692": {}
    }
};
var $fotosr = $.fotosr.toJSON();

var _activity;
if (OS_ANDROID) {
    _activity = $.ID_1465840618.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_1465840618';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_1465840618.addEventListener('open', function(e) {});
}
$.ID_1465840618.orientationModes = [Titanium.UI.PORTRAIT];


var ID_46832842 = Ti.UI.createView({
    height: Ti.UI.FILL,
    left: 0,
    layout: 'composite',
    width: Ti.UI.FILL,
    top: 0,
    tipo: 'imagen'
});
//$.ID_1465840618.open();
function camaraID_46832842() {
    require('com.skypanther.picatsize').showCamera({
        success: function(event) {
            if (event.mediaType == require('com.skypanther.picatsize').MEDIA_TYPE_PHOTO) {
                ID_46832842.fireEvent("success", {
                    valor: {
                        error: false,
                        cancel: false,
                        data: event.media,
                        type: 'photo'
                    }
                });
            } else if (event.mediaType == require('com.skypanther.picatsize').MEDIA_TYPE_VIDEO) {
                ID_46832842.fireEvent("success", {
                    valor: {
                        error: false,
                        cancel: false,
                        data: event.media,
                        type: 'video'
                    }
                });
            }
        },
        cancel: function() {
            ID_46832842.fireEvent("cancel", {
                valor: {
                    error: false,
                    cancel: true,
                    data: '',
                    reason: 'cancelled'
                }
            });
        },
        error: function(error) {
            ID_46832842.fireEvent("error", {
                valor: {
                    error: true,
                    cancel: false,
                    data: error,
                    reason: error.error,
                    type: 'camera'
                }
            });
        },
        autohide: false,
        showControls: false,
        targetWidth: 480,
        targetHeight: 640,
        saveToPhotoGallery: false,
        overlay: ID_46832842,
    });
}
require('vars')['_estadoflash_'] = 'auto';
require('vars')['_tipocamara_'] = 'trasera';
require('vars')['_hayflash_'] = true;
if (Ti.Media.hasCameraPermissions()) {
    camaraID_46832842();
} else {
    Ti.Media.requestCameraPermissions(function(ercp) {
        if (ercp.success) {
            camaraID_46832842();
        } else {
            ID_46832842.fireEvent("error", {
                valor: {
                    error: true,
                    cancel: false,
                    data: 'nopermission',
                    type: 'permission',
                    reason: 'camera doesnt have permissions'
                }
            });
        }
    });
}
var ID_1783885338 = Alloy.createWidget('barra');

function Cerrar_ID_1709673785(e) {
    var evento = e;
    /** 
     * Cerramos la vista, camara, y pantalla 
     */
    var ID_46832842_esconder = 'camara';

    var esconderVistaCamara = function(tipo) {
        if (tipo == 'camara' || tipo == 'cam' || tipo == 'Camara') {
            console.log('tipo1');
            require('com.skypanther.picatsize').hideCamera();
        } else if (tipo == 'vista' || tipo == 'view') {
            console.log('tipo2');
            ID_46832842.hide();
        }
    };
    esconderVistaCamara(ID_46832842_esconder);

    var ID_46832842_esconder = 'vista';

    var esconderVistaCamara = function(tipo) {
        if (tipo == 'camara' || tipo == 'cam' || tipo == 'Camara') {
            console.log('tipo1');
            require('com.skypanther.picatsize').hideCamera();
        } else if (tipo == 'vista' || tipo == 'view') {
            console.log('tipo2');
            ID_46832842.hide();
        }
    };
    esconderVistaCamara(ID_46832842_esconder);

    $.ID_1465840618.close();
}

function Presiono_ID_868039172(e) {
    var evento = e;
    /** 
     * Recuperamos variable de el objeto para saber si las fotos minimas fueron capturadas, si no estan, avisamos al usuario que foto falta 
     */
    var requeridas = ('requeridas' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['requeridas'] : '';
    if (requeridas.foto1 == true || requeridas.foto1 == 'true') {
        var ID_1967487035_opts = ['Aceptar'];
        var ID_1967487035 = Ti.UI.createAlertDialog({
            title: 'Alerta',
            message: 'Falta la foto de fachada',
            buttonNames: ID_1967487035_opts
        });
        ID_1967487035.addEventListener('click', function(e) {
            var pre = ID_1967487035_opts[e.index];
            pre = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1967487035.show();
    } else if (requeridas.foto2 == true || requeridas.foto2 == 'true') {
        var ID_662469891_opts = ['Aceptar'];
        var ID_662469891 = Ti.UI.createAlertDialog({
            title: 'Alerta',
            message: 'Falta la foto del barrio',
            buttonNames: ID_662469891_opts
        });
        ID_662469891.addEventListener('click', function(e) {
            var pre = ID_662469891_opts[e.index];
            pre = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_662469891.show();
    } else if (requeridas.foto3 == true || requeridas.foto3 == 'true') {
        var ID_317679681_opts = ['Aceptar'];
        var ID_317679681 = Ti.UI.createAlertDialog({
            title: 'Alerta',
            message: 'Falta la foto de número de domicilio',
            buttonNames: ID_317679681_opts
        });
        ID_317679681.addEventListener('click', function(e) {
            var pre = ID_317679681_opts[e.index];
            pre = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_317679681.show();
    } else {
        /** 
         * Ocupamos esta variable para impedir que la pantalla siguente se cargue dos veces 
         */
        var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
        if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
            require('vars')['var_abriendo'] = 'true';
            /** 
             * Guardamos el modelo 
             */
            Alloy.Collections[$.fotosr.config.adapter.collection_name].add($.fotosr);
            $.fotosr.save();
            Alloy.Collections[$.fotosr.config.adapter.collection_name].fetch();
            /** 
             * Cerramos vista y camara 
             */
            var ID_46832842_esconder = 'camara';

            var esconderVistaCamara = function(tipo) {
                if (tipo == 'camara' || tipo == 'cam' || tipo == 'Camara') {
                    console.log('tipo1');
                    require('com.skypanther.picatsize').hideCamera();
                } else if (tipo == 'vista' || tipo == 'view') {
                    console.log('tipo2');
                    ID_46832842.hide();
                }
            };
            esconderVistaCamara(ID_46832842_esconder);

            var ID_46832842_esconder = 'vista';

            var esconderVistaCamara = function(tipo) {
                if (tipo == 'camara' || tipo == 'cam' || tipo == 'Camara') {
                    console.log('tipo1');
                    require('com.skypanther.picatsize').hideCamera();
                } else if (tipo == 'vista' || tipo == 'view') {
                    console.log('tipo2');
                    ID_46832842.hide();
                }
            };
            esconderVistaCamara(ID_46832842_esconder);

            Alloy.createController("hayalguien_index", {}).getView().open();
        }
    }
}
ID_1783885338.init({
    titulo: 'FOTOS REQUERIDAS',
    __id: 'ALL1783885338',
    oncerrar: Cerrar_ID_1709673785,
    continuar: '',
    cerrar: '',
    fondo: 'fondoazul',
    top: 0,
    onpresiono: Presiono_ID_868039172
});
ID_46832842.add(ID_1783885338.getView());
var ID_184683244 = Titanium.UI.createView({
    height: '73dp',
    bottom: '100dp',
    layout: 'composite',
    backgroundColor: '#FFFFFF'
});
var ID_1234096039 = Titanium.UI.createView({
    height: Ti.UI.FILL,
    layout: 'composite',
    width: '80dp',
    backgroundColor: '#C8C7CC',
    font: {
        fontSize: '12dp'
    }
});
var ID_1503332472 = Titanium.UI.createLabel({
    height: Ti.UI.SIZE,
    text: '\uE80c',
    color: '#FFFFFF',
    width: Ti.UI.SIZE,
    font: {
        fontFamily: 'beta1',
        fontSize: '36dp'
    }
});
ID_1234096039.add(ID_1503332472);
ID_184683244.add(ID_1234096039);
var ID_1135315933 = Titanium.UI.createView({
    height: Ti.UI.FILL,
    visible: false,
    layout: 'composite',
    width: '80dp',
    backgroundColor: '#EE7F7E',
    font: {
        fontSize: '12dp'
    }
});
var ID_905374707 = Titanium.UI.createLabel({
    height: Ti.UI.SIZE,
    text: '\uE80c',
    color: '#FFFFFF',
    width: Ti.UI.SIZE,
    font: {
        fontFamily: 'beta1',
        fontSize: '36dp'
    }
});
ID_1135315933.add(ID_905374707);
ID_1135315933.addEventListener('click', function(e) {
    e.cancelBubble = true;
    var elemento = e.source;
    /** 
     * Capturamos la foto 
     */
    require('com.skypanther.picatsize').takePicture();
    /** 
     * Escondemos la vista para prevenir que el usuario tome multiples fotos a la vez, y evitar que la aplicacion se cierre 
     */
    var ID_1135315933_visible = false;

    if (ID_1135315933_visible == 'si') {
        ID_1135315933_visible = true;
    } else if (ID_1135315933_visible == 'no') {
        ID_1135315933_visible = false;
    }
    ID_1135315933.setVisible(ID_1135315933_visible);

});
ID_184683244.add(ID_1135315933);
var ID_958869452 = Titanium.UI.createView({
    height: Ti.UI.FILL,
    layout: 'composite',
    width: '80dp',
    right: '0dp'
});
var ID_1227389373 = Titanium.UI.createLabel({
    height: Ti.UI.SIZE,
    text: '\uE81d',
    color: '#2D9EDB',
    width: Ti.UI.SIZE,
    font: {
        fontFamily: 'beta1',
        fontSize: '24dp'
    }
});
ID_958869452.add(ID_1227389373);
ID_958869452.addEventListener('click', function(e) {
    e.cancelBubble = true;
    var elemento = e.source;
    /** 
     * Revisamos el flag de posicion de camara (frontal o trasera) y cambiamos la camara 
     */
    var camara = ('camara' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['camara'] : '';
    if (camara == 'trasera') {
        /** 
         * Flag para saber que tiene que cambiar a frontal 
         */
        require('vars')[_var_scopekey]['camara'] = 'frontal';
        var ID_46832842_camara = 'frontal';

        if (ID_46832842_camara == 'front' || ID_46832842_camara == 'frontal' || ID_46832842_camara == 'normal') {
            ID_46832842_camara = require('com.skypanther.picatsize').CAMERA_FRONT;
            require('vars')['_hayflash_'] = false;
            require('vars')['_tipocamara_'] = 'frontal';
            require('vars')['_estadoflash_'] = 'auto';
        } else if (ID_46832842_camara == 'trasera' || ID_46832842_camara == 'back' || ID_46832842_camara == 'rear') {
            ID_46832842_camara = require('com.skypanther.picatsize').CAMERA_REAR;
            require('vars')['_hayflash_'] = true;
            require('vars')['_tipocamara_'] = 'trasera';
            require('vars')['_estadoflash_'] = 'auto';
        }
        require('com.skypanther.picatsize').switchCamera(ID_46832842_camara);

    }
    if (camara == 'frontal') {
        require('vars')[_var_scopekey]['camara'] = 'trasera';
        var ID_46832842_camara = 'trasera';

        if (ID_46832842_camara == 'front' || ID_46832842_camara == 'frontal' || ID_46832842_camara == 'normal') {
            ID_46832842_camara = require('com.skypanther.picatsize').CAMERA_FRONT;
            require('vars')['_hayflash_'] = false;
            require('vars')['_tipocamara_'] = 'frontal';
            require('vars')['_estadoflash_'] = 'auto';
        } else if (ID_46832842_camara == 'trasera' || ID_46832842_camara == 'back' || ID_46832842_camara == 'rear') {
            ID_46832842_camara = require('com.skypanther.picatsize').CAMERA_REAR;
            require('vars')['_hayflash_'] = true;
            require('vars')['_tipocamara_'] = 'trasera';
            require('vars')['_estadoflash_'] = 'auto';
        }
        require('com.skypanther.picatsize').switchCamera(ID_46832842_camara);

    }
});
ID_184683244.add(ID_958869452);
ID_46832842.add(ID_184683244);
var ID_483354561 = Titanium.UI.createView({
    height: '100dp',
    bottom: '0dp',
    layout: 'vertical'
});
var ID_1189437781 = Alloy.createWidget('4fotos');

function Click_ID_1657845092(e) {
    var evento = e;
    require('vars')[_var_scopekey]['foto'] = evento.pos;
    /** 
     * Habilitamos boton tomar foto 
     */
    var ID_1135315933_visible = true;

    if (ID_1135315933_visible == 'si') {
        ID_1135315933_visible = true;
    } else if (ID_1135315933_visible == 'no') {
        ID_1135315933_visible = false;
    }
    ID_1135315933.setVisible(ID_1135315933_visible);

}

function Listo_ID_572098029(e) {
    var evento = e;
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    var gps_coords = ('gps_coords' in require('vars')) ? require('vars')['gps_coords'] : '';
    var gps_latitud = ('gps_latitud' in require('vars')) ? require('vars')['gps_latitud'] : '';
    var gps_longitud = ('gps_longitud' in require('vars')) ? require('vars')['gps_longitud'] : '';
    var resultado = null;

    console.log(JSON.stringify(gps_coords));
    if ('distanceinmtbetweenearthcoordinates' in require('funciones')) {
        resultado = require('funciones').distanceinmtbetweenearthcoordinates({
            'lat1': gps_latitud,
            'lon1': gps_longitud,
            'lat2': seltarea.lat,
            'lon2': seltarea.lon
        });
    } else {
        try {
            resultado = f_distanceinmtbetweenearthcoordinates({
                'lat1': gps_latitud,
                'lon1': gps_longitud,
                'lat2': seltarea.lat,
                'lon2': seltarea.lon
            });
        } catch (ee) {}
    }
    if (_.isNumber(gps_coords.accuracy) && _.isNumber(resultado) && gps_coords.accuracy > resultado) {
        /** 
         * Pablo me comento que el flag fuera_rango se determina comparando precision vs la distancia de ubicacion con la definida en tarea 
         */
        var json_datosfoto = {
            lat: gps_latitud,
            lng: gps_longitud,
            fuera_rango: 0,
            distancia: Math.round(resultado),
            precision: gps_coords.accuracy
        };
    } else {
        var json_datosfoto = {
            lat: gps_latitud,
            lng: gps_longitud,
            fuera_rango: 1,
            distancia: Math.round(resultado),
            precision: gps_coords.accuracy
        };
    }
    if (_.isString(seltarea)) {
        /** 
         * Definimos valor inventado id_server para testing 
         */
        var seltarea = {
            id_server: 123
        };
    }
    var previos = [];
    var ID_587641544_f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
    if (ID_587641544_f.exists() == true) {
        previos = ID_587641544_f.getDirectoryListing();
    }
    if (Ti.App.deployType != 'production') console.log('archivos previos existentes en telefono', {
        "previos": previos
    });
    var requeridas = ('requeridas' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['requeridas'] : '';
    if (evento.pos == 1 || evento.pos == '1') {
        var ID_1158408928_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
        if (ID_1158408928_d.exists() == false) ID_1158408928_d.createDirectory();
        var ID_1158408928_f = Ti.Filesystem.getFile(ID_1158408928_d.resolve(), 'foto1.json');
        if (ID_1158408928_f.exists() == true) ID_1158408928_f.deleteFile();
        console.log('contenido f1 frequeridas', JSON.stringify(json_datosfoto));
        ID_1158408928_f.write(JSON.stringify(json_datosfoto));
        ID_1158408928_d = null;
        ID_1158408928_f = null;
        /** 
         * Cambiamos estado de foto1 para avisar que tenemos foto capturada 
         */
        var requeridas = _.extend(requeridas, {
            foto1: 'false'
        });
        /** 
         * Guardamos la foto en la memoria del celular 
         */
        var ID_870492955_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
        if (ID_870492955_d.exists() == false) ID_870492955_d.createDirectory();
        var ID_870492955_f = Ti.Filesystem.getFile(ID_870492955_d.resolve(), 'foto1.jpg');
        if (ID_870492955_f.exists() == true) ID_870492955_f.deleteFile();
        ID_870492955_f.write(evento.nueva);
        ID_870492955_d = null;
        ID_870492955_f = null;
        /** 
         * Actualizamos el modelo avisando que la foto de fachada, el archivo se llama foto1 
         */
        $.fotosr.set({
            fachada: 'foto1.jpg'
        });
        if ('fotosr' in $) $fotosr = $.fotosr.toJSON();
    } else if (evento.pos == 2) {
        var ID_1877386143_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
        if (ID_1877386143_d.exists() == false) ID_1877386143_d.createDirectory();
        var ID_1877386143_f = Ti.Filesystem.getFile(ID_1877386143_d.resolve(), 'foto2.json');
        if (ID_1877386143_f.exists() == true) ID_1877386143_f.deleteFile();
        console.log('contenido f2 frequeridas', JSON.stringify(json_datosfoto));
        ID_1877386143_f.write(JSON.stringify(json_datosfoto));
        ID_1877386143_d = null;
        ID_1877386143_f = null;
        var requeridas = _.extend(requeridas, {
            foto2: 'false'
        });
        var ID_1597663409_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
        if (ID_1597663409_d.exists() == false) ID_1597663409_d.createDirectory();
        var ID_1597663409_f = Ti.Filesystem.getFile(ID_1597663409_d.resolve(), 'foto2.jpg');
        if (ID_1597663409_f.exists() == true) ID_1597663409_f.deleteFile();
        ID_1597663409_f.write(evento.nueva);
        ID_1597663409_d = null;
        ID_1597663409_f = null;
        $.fotosr.set({
            barrio: 'foto2.jpg'
        });
        if ('fotosr' in $) $fotosr = $.fotosr.toJSON();
    } else if (evento.pos == 3) {
        var ID_651636939_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
        if (ID_651636939_d.exists() == false) ID_651636939_d.createDirectory();
        var ID_651636939_f = Ti.Filesystem.getFile(ID_651636939_d.resolve(), 'foto3.json');
        if (ID_651636939_f.exists() == true) ID_651636939_f.deleteFile();
        console.log('contenido f3 frequeridas', JSON.stringify(json_datosfoto));
        ID_651636939_f.write(JSON.stringify(json_datosfoto));
        ID_651636939_d = null;
        ID_651636939_f = null;
        var requeridas = _.extend(requeridas, {
            foto3: 'false'
        });
        var ID_372013953_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
        if (ID_372013953_d.exists() == false) ID_372013953_d.createDirectory();
        var ID_372013953_f = Ti.Filesystem.getFile(ID_372013953_d.resolve(), 'foto3.jpg');
        if (ID_372013953_f.exists() == true) ID_372013953_f.deleteFile();
        ID_372013953_f.write(evento.nueva);
        ID_372013953_d = null;
        ID_372013953_f = null;
        $.fotosr.set({
            numero: 'foto3.jpg'
        });
        if ('fotosr' in $) $fotosr = $.fotosr.toJSON();
    } else if (evento.pos == 4) {
        var ID_781702524_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
        if (ID_781702524_d.exists() == false) ID_781702524_d.createDirectory();
        var ID_781702524_f = Ti.Filesystem.getFile(ID_781702524_d.resolve(), 'foto4.json');
        if (ID_781702524_f.exists() == true) ID_781702524_f.deleteFile();
        console.log('contenido f4 frequeridas', JSON.stringify(json_datosfoto));
        ID_781702524_f.write(JSON.stringify(json_datosfoto));
        ID_781702524_d = null;
        ID_781702524_f = null;
        var requeridas = _.extend(requeridas, {
            foto4: 'false'
        });
        var ID_562700504_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
        if (ID_562700504_d.exists() == false) ID_562700504_d.createDirectory();
        var ID_562700504_f = Ti.Filesystem.getFile(ID_562700504_d.resolve(), 'foto4.jpg');
        if (ID_562700504_f.exists() == true) ID_562700504_f.deleteFile();
        ID_562700504_f.write(evento.nueva);
        ID_562700504_d = null;
        ID_562700504_f = null;
        $.fotosr.set({
            depto: 'foto4.jpg'
        });
        if ('fotosr' in $) $fotosr = $.fotosr.toJSON();
    }
    require('vars')[_var_scopekey]['requeridas'] = requeridas;
    /** 
     * Habilitamos boton tomar foto 
     */
    var ID_1135315933_visible = true;

    if (ID_1135315933_visible == 'si') {
        ID_1135315933_visible = true;
    } else if (ID_1135315933_visible == 'no') {
        ID_1135315933_visible = false;
    }
    ID_1135315933.setVisible(ID_1135315933_visible);

    require('vars')[_var_scopekey]['foto'] = 'null';
    /** 
     * Limpiamos memoria ram 
     */
    evento = null;
    json_datosfoto = null;
}
ID_1189437781.init({
    __id: 'ALL1189437781',
    onlisto: Listo_ID_572098029,
    opcional: 'Opcional',
    label1: 'Fachada',
    label4: 'Nº Depto',
    label3: 'Numero',
    onclick: Click_ID_1657845092,
    label2: 'Barrio'
});
ID_483354561.add(ID_1189437781.getView());
ID_46832842.add(ID_483354561);
ID_46832842.addEventListener('success', function(e) {
    e.cancelBubble = true;
    var elemento = e.valor;
    /** 
     * Creamos alias para la foto recibida 
     */
    var foto_original = elemento.data;
    var foto = ('foto' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['foto'] : '';
    var camara = ('camara' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['camara'] : '';
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    if (foto == 1 || foto == '1') {
        ID_1189437781.procesar({
            imagen1: foto_original,
            nueva: 640,
            camara: camara
        });
    } else if (foto == 2) {
        ID_1189437781.procesar({
            imagen2: foto_original,
            nueva: 640,
            camara: camara
        });
    } else if (foto == 3) {
        ID_1189437781.procesar({
            imagen3: foto_original,
            nueva: 640,
            camara: camara
        });
    } else if (foto == 4) {
        ID_1189437781.procesar({
            imagen4: foto_original,
            nueva: 640,
            camara: camara
        });
    }
    /** 
     * Limpiamos memoria ram 
     */
    foto_original = comprimida = mini = elemento = null
});
ID_46832842.addEventListener('cancel', function(e) {
    e.cancelBubble = true;
    var elemento = e.valor;
    if (Ti.App.deployType != 'production') console.log('no voy a hacer nada. solo cancelar', {});
});


(function() {
    /** 
     * Avisamos que se debe cerrar pantalla detalle de tarea 
     */
    Alloy.Events.trigger('_cerrar_insp', {
        pantalla: 'detalle'
    });
    /** 
     * Seteamos flag para saber que se inicia en la camara trasera 
     */
    require('vars')[_var_scopekey]['camara'] = 'trasera';
    /** 
     * Creamos objeto para poder saber si el minimo de fotos obligatorias existen 
     */
    var requeridas = {
        foto1: 'true',
        foto2: 'true',
        foto3: 'true',
        foto4: 'false'
    };
    require('vars')[_var_scopekey]['requeridas'] = requeridas;
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
        /** 
         * Verificamos que estemos en una inspeccion para poder modificar el id_inspeccion del modelo de fotos requeridas 
         */
        $.fotosr.set({
            id_inspeccion: seltarea.id_server
        });
        if ('fotosr' in $) $fotosr = $.fotosr.toJSON();
    }
})();


/** 
 * Cerramos esta pantalla cuando es llamada en la pantalla siguente (hayalguien) 
 */
_my_events['_cerrar_insp,ID_1276768626'] = function(evento) {
    if (!_.isUndefined(evento.pantalla)) {
        if (evento.pantalla == 'frequeridas') {
            if (Ti.App.deployType != 'production') console.log('debug cerrando fotos requeridas', {});
            var ID_1919045070_trycatch = {
                error: function(e) {
                    if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
                }
            };
            try {
                ID_1919045070_trycatch.error = function(evento) {
                    if (Ti.App.deployType != 'production') console.log('error cerrando fotos requeridas', {});
                };
                console.log('prueba elias');
                $.ID_1465840618.remove(ID_46832842);
                console.log('fin prueba elias');
                if (Ti.App.deployType != 'production') console.log('cerrando fotos requeridas', {});
                var ID_46832842_esconder = 'camara';

                var esconderVistaCamara = function(tipo) {
                    if (tipo == 'camara' || tipo == 'cam' || tipo == 'Camara') {
                        console.log('tipo1');
                        require('com.skypanther.picatsize').hideCamera();
                    } else if (tipo == 'vista' || tipo == 'view') {
                        console.log('tipo2');
                        ID_46832842.hide();
                    }
                };
                esconderVistaCamara(ID_46832842_esconder);

                var ID_46832842_esconder = 'vista';

                var esconderVistaCamara = function(tipo) {
                    if (tipo == 'camara' || tipo == 'cam' || tipo == 'Camara') {
                        console.log('tipo1');
                        require('com.skypanther.picatsize').hideCamera();
                    } else if (tipo == 'vista' || tipo == 'view') {
                        console.log('tipo2');
                        ID_46832842.hide();
                    }
                };
                esconderVistaCamara(ID_46832842_esconder);

                $.ID_1465840618.close();
                if (Ti.App.deployType != 'production') console.log('cerradas fotos requeridas', {});
            } catch (e) {
                ID_1919045070_trycatch.error(e);
            }
        }
    } else {
        if (Ti.App.deployType != 'production') console.log('debug cerrando (todas) fotos requeridas', {});
        var ID_1709240990_trycatch = {
            error: function(e) {
                if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
            }
        };
        try {
            ID_1709240990_trycatch.error = function(evento) {
                if (Ti.App.deployType != 'production') console.log('error cerrando fotos requeridas', {});
            };
            $.ID_1465840618.close();
        } catch (e) {
            ID_1709240990_trycatch.error(e);
        }
    }
};
Alloy.Events.on('_cerrar_insp', _my_events['_cerrar_insp,ID_1276768626']);

if (OS_IOS || OS_ANDROID) {
    $.ID_1465840618.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}