var _bind4section = {
    "ref1": "insp_itemdanos"
};
var _list_templates = {
    "elemento": {
        "ID_432412973": {},
        "ID_291370846": {
            "text": "{nombre}"
        },
        "ID_1925487537": {
            "text": "{id_server}"
        },
        "ID_975786586": {
            "text": "{id_segured}"
        },
        "ID_1546190443": {}
    },
    "dano": {
        "ID_1411679491": {
            "text": "{id}"
        },
        "ID_89142909": {},
        "ID_677303796": {
            "text": "{nombre}"
        }
    },
    "pborrar": {
        "ID_1075802659": {},
        "ID_1248687037": {},
        "ID_1008172879": {},
        "ID_1812298368": {
            "text": "{id}"
        },
        "ID_1619593179": {},
        "ID_2123821302": {
            "text": "{nombre}"
        }
    }
};
var $recinto1 = $.recinto1.toJSON();

var _activity;
if (OS_ANDROID) {
    _activity = $.ID_1168881001.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_1168881001';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_1168881001.addEventListener('open', function(e) {});
}
$.ID_1168881001.orientationModes = [Titanium.UI.PORTRAIT];


var ID_1733573059_like = function(search) {
    if (typeof search !== 'string' || this === null) {
        return false;
    }
    search = search.replace(new RegExp("([\\.\\\\\\+\\*\\?\\[\\^\\]\\$\\(\\)\\{\\}\\=\\!\\<\\>\\|\\:\\-])", "g"), "\\$1");
    search = search.replace(/%/g, '.*').replace(/_/g, '.');
    return RegExp('^' + search + '$', 'gi').test(this);
};
var ID_1733573059_filter = function(coll) {
    var filtered = coll.filter(function(m) {
        var _tests = [],
            _all_true = false,
            model = m.toJSON();
        _tests.push((model.id_recinto == '0'));
        var _all_true_s = _.uniq(_tests);
        _all_true = (_all_true_s.length == 1 && _all_true_s[0] == true) ? true : false;
        return _all_true;
    });
    filtered = _.toArray(filtered);
    return filtered;
};
var ID_1733573059_transform = function(model) {
    var modelo = model.toJSON();
    return modelo;
};
_.defer(function() {
    Alloy.Collections.insp_itemdanos.fetch();
});
Alloy.Collections.insp_itemdanos.on('add change delete', function(ee) {
    ID_1733573059_update(ee);
});
var ID_1733573059_update = function(evento) {
    if ((_.isObject(temp_idrecinto) || (_.isString(temp_idrecinto)) && !_.isEmpty(temp_idrecinto)) || _.isNumber(temp_idrecinto) || _.isBoolean(temp_idrecinto)) {
        var temp_idrecinto = ('temp_idrecinto' in require('vars')) ? require('vars')['temp_idrecinto'] : '';
        var ID_915441220_i = Alloy.createCollection('insp_itemdanos');
        var ID_915441220_i_where = 'id_recinto=\'' + temp_idrecinto + '\'';
        ID_915441220_i.fetch({
            query: 'SELECT * FROM insp_itemdanos WHERE id_recinto=\'' + temp_idrecinto + '\''
        });
        var insp_n = require('helper').query2array(ID_915441220_i);
        var alto = 55 * insp_n.length;
        var ID_93872718_alto = alto;

        if (ID_93872718_alto == '*') {
            ID_93872718_alto = Ti.UI.FILL;
        } else if (!isNaN(ID_93872718_alto)) {
            ID_93872718_alto = ID_93872718_alto + 'dp';
        }
        $.ID_93872718.setHeight(ID_93872718_alto);

    } else {
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        var ID_918658580_i = Alloy.createCollection('insp_itemdanos');
        var ID_918658580_i_where = 'id_inspeccion=\'' + seltarea.id_server + '\'';
        ID_918658580_i.fetch({
            query: 'SELECT * FROM insp_itemdanos WHERE id_inspeccion=\'' + seltarea.id_server + '\''
        });
        var insp_n = require('helper').query2array(ID_918658580_i);
        var alto = 55 * insp_n.length;
        var ID_93872718_alto = alto;

        if (ID_93872718_alto == '*') {
            ID_93872718_alto = Ti.UI.FILL;
        } else if (!isNaN(ID_93872718_alto)) {
            ID_93872718_alto = ID_93872718_alto + 'dp';
        }
        $.ID_93872718.setHeight(ID_93872718_alto);

    }
};
Alloy.Collections.insp_itemdanos.fetch();


$.ID_347300010.init({
    titulo: 'NUEVO RECINTO',
    __id: 'ALL347300010',
    textoderecha: 'Guardar',
    oncerrar: Cerrar_ID_115477709,
    fondo: 'fondomorado',
    top: 0,
    modal: '',
    onpresiono: Presiono_ID_1076820460,
    colortextoderecha: 'blanco'
});

function Cerrar_ID_115477709(e) {

    var evento = e;
    /** 
     * Limpiamos widgets multiples (memoria) 
     */
    $.ID_927397228.limpiar({});
    $.ID_346546200.limpiar({});
    $.ID_625792866.limpiar({});
    $.ID_1586767745.limpiar({});
    $.ID_1781249673.limpiar({});
    $.ID_1240971093.limpiar({});
    $.ID_1168881001.close();

}

function Presiono_ID_1076820460(e) {

    var evento = e;
    var temp_idrecinto = ('temp_idrecinto' in require('vars')) ? require('vars')['temp_idrecinto'] : '';
    /** 
     * Consultamos la tabla de danos, filtrando por el id del recinto y saber si tiene danos 
     */
    var ID_173041128_i = Alloy.createCollection('insp_itemdanos');
    var ID_173041128_i_where = 'id_recinto=\'' + temp_idrecinto + '\'';
    ID_173041128_i.fetch({
        query: 'SELECT * FROM insp_itemdanos WHERE id_recinto=\'' + temp_idrecinto + '\''
    });
    var danos_verificacion = require('helper').query2array(ID_173041128_i);
    if (_.isUndefined($recinto1.nombre)) {
        /** 
         * Validamos que existan los datos ingresados y que esten bien 
         */
        var ID_1905058147_opts = ['Aceptar'];
        var ID_1905058147 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese el nombre del recinto',
            buttonNames: ID_1905058147_opts
        });
        ID_1905058147.addEventListener('click', function(e) {
            var nulo = ID_1905058147_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1905058147.show();
    } else if ((_.isObject($recinto1.nombre) || _.isString($recinto1.nombre)) && _.isEmpty($recinto1.nombre)) {
        var ID_1044287971_opts = ['Aceptar'];
        var ID_1044287971 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese el nombre del recinto',
            buttonNames: ID_1044287971_opts
        });
        ID_1044287971.addEventListener('click', function(e) {
            var nulo = ID_1044287971_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1044287971.show();
    } else if (_.isUndefined($recinto1.id_nivel)) {
        var ID_1195275260_opts = ['Aceptar'];
        var ID_1195275260 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Seleccione la zona al cual pertenece el nuevo recinto',
            buttonNames: ID_1195275260_opts
        });
        ID_1195275260.addEventListener('click', function(e) {
            var nulo = ID_1195275260_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1195275260.show();
    } else if (_.isUndefined($recinto1.largo)) {
        var ID_1162705651_opts = ['Aceptar'];
        var ID_1162705651 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese largo del recinto',
            buttonNames: ID_1162705651_opts
        });
        ID_1162705651.addEventListener('click', function(e) {
            var nulo = ID_1162705651_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1162705651.show();
    } else if ((_.isObject($recinto1.largo) || _.isString($recinto1.largo)) && _.isEmpty($recinto1.largo)) {
        var ID_1521726391_opts = ['Aceptar'];
        var ID_1521726391 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese largo del recinto',
            buttonNames: ID_1521726391_opts
        });
        ID_1521726391.addEventListener('click', function(e) {
            var nulo = ID_1521726391_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1521726391.show();
    } else if (_.isUndefined($recinto1.ancho)) {
        var ID_1853057559_opts = ['Aceptar'];
        var ID_1853057559 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese ancho del recinto',
            buttonNames: ID_1853057559_opts
        });
        ID_1853057559.addEventListener('click', function(e) {
            var nulo = ID_1853057559_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1853057559.show();
    } else if ((_.isObject($recinto1.ancho) || _.isString($recinto1.ancho)) && _.isEmpty($recinto1.ancho)) {
        var ID_924481334_opts = ['Aceptar'];
        var ID_924481334 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese ancho de la zona',
            buttonNames: ID_924481334_opts
        });
        ID_924481334.addEventListener('click', function(e) {
            var nulo = ID_924481334_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_924481334.show();
    } else if (_.isUndefined($recinto1.alto)) {
        var ID_1547824742_opts = ['Aceptar'];
        var ID_1547824742 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese la altura de la zona',
            buttonNames: ID_1547824742_opts
        });
        ID_1547824742.addEventListener('click', function(e) {
            var nulo = ID_1547824742_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1547824742.show();
    } else if ((_.isObject($recinto1.alto) || _.isString($recinto1.alto)) && _.isEmpty($recinto1.alto)) {
        var ID_1008435922_opts = ['Aceptar'];
        var ID_1008435922 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese la altura de la zona',
            buttonNames: ID_1008435922_opts
        });
        ID_1008435922.addEventListener('click', function(e) {
            var nulo = ID_1008435922_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1008435922.show();
    } else if (_.isUndefined($recinto1.ids_estructuras_soportantes)) {
        var ID_1277073507_opts = ['Aceptar'];
        var ID_1277073507 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Seleccione la estructura soportante del recinto',
            buttonNames: ID_1277073507_opts
        });
        ID_1277073507.addEventListener('click', function(e) {
            var nulo = ID_1277073507_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1277073507.show();
    } else if ((_.isObject($recinto1.ids_estructuras_soportantes) || _.isString($recinto1.ids_estructuras_soportantes)) && _.isEmpty($recinto1.ids_estructuras_soportantes)) {
        var ID_1210100675_opts = ['Aceptar'];
        var ID_1210100675 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Seleccione la estructura soportante del recinto',
            buttonNames: ID_1210100675_opts
        });
        ID_1210100675.addEventListener('click', function(e) {
            var nulo = ID_1210100675_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1210100675.show();
    } else if (danos_verificacion && danos_verificacion.length == 0) {
        var ID_363684051_opts = ['GUARDAR', 'VOLVER'];
        var ID_363684051 = Ti.UI.createAlertDialog({
            title: '¿Guardar sin daños?',
            message: 'Estas guardando el recinto sin daños asociados, si esta correcto presiona guardar',
            buttonNames: ID_363684051_opts
        });
        ID_363684051.addEventListener('click', function(e) {
            var msj = ID_363684051_opts[e.index];
            if (msj == 'GUARDAR') {
                Alloy.Collections[$.recinto1.config.adapter.collection_name].add($.recinto1);
                $.recinto1.save();
                Alloy.Collections[$.recinto1.config.adapter.collection_name].fetch();
                test = null;
                $.ID_927397228.limpiar({});
                $.ID_346546200.limpiar({});
                $.ID_625792866.limpiar({});
                $.ID_1586767745.limpiar({});
                $.ID_1781249673.limpiar({});
                $.ID_1240971093.limpiar({});
                $.ID_1168881001.close();
            }
            msj = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_363684051.show();
    } else {
        /** 
         * Guardamos el modelo con los datos ingresado 
         */
        Alloy.Collections[$.recinto1.config.adapter.collection_name].add($.recinto1);
        $.recinto1.save();
        Alloy.Collections[$.recinto1.config.adapter.collection_name].fetch();
        /** 
         * Limpiamos widgets multiples (memoria) 
         */
        $.ID_927397228.limpiar({});
        $.ID_346546200.limpiar({});
        $.ID_625792866.limpiar({});
        $.ID_1586767745.limpiar({});
        $.ID_1781249673.limpiar({});
        $.ID_1240971093.limpiar({});
        $.ID_1168881001.close();
    }
}

function Change_ID_1295769528(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    /** 
     * Guardamos el nombre del recinto en el modelo 
     */
    $.recinto1.set({
        nombre: elemento
    });
    if ('recinto1' in $) $recinto1 = $.recinto1.toJSON();
    require('vars')['nombrerecinto'] = elemento;
    elemento = null, source = null;

}

function Return_ID_407965901(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    /** 
     * Al presionar enter en el teclado del equipo, cerramos el teclado y evitamos pasar al prox campo 
     */
    $.ID_1061691243.blur();
    elemento = null, source = null;

}

$.ID_318354873.init({
    titulo: 'ZONA',
    cargando: 'cargando...',
    __id: 'ALL318354873',
    left: 0,
    onrespuesta: Respuesta_ID_1631529599,
    campo: 'Zona del Recinto',
    onabrir: Abrir_ID_692277200,
    color: 'morado',
    right: 0,
    seleccione: 'seleccione zona',
    activo: true,
    onafterinit: Afterinit_ID_1185179224
});

function Abrir_ID_692277200(e) {

    var evento = e;

}

function Respuesta_ID_1631529599(e) {

    var evento = e;
    /** 
     * Ponemos el texto del valor escogido en el widget 
     */
    $.ID_318354873.labels({
        valor: evento.valor
    });
    /** 
     * Guardamos el id del nivel que seleccionamos 
     */
    $.recinto1.set({
        id_nivel: evento.item.id_interno
    });
    if ('recinto1' in $) $recinto1 = $.recinto1.toJSON();

}

function Afterinit_ID_1185179224(e) {

    var evento = e;
    var ID_5498125_func = function() {
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            /** 
             * Revisamos si estamos haciendo una inspeccion o es un dummy 
             */
            var ID_939507791_i = Alloy.createCollection('insp_niveles');
            var ID_939507791_i_where = 'id_inspeccion=\'' + seltarea.id_server + '\'';
            ID_939507791_i.fetch({
                query: 'SELECT * FROM insp_niveles WHERE id_inspeccion=\'' + seltarea.id_server + '\''
            });
            var datosniveles = require('helper').query2array(ID_939507791_i);
            /** 
             * Obtenemos datos para selectores (solo los de esta inspeccion) 
             */
            var datos = [];
            _.each(datosniveles, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'valor';
                    if (llave == 'id') newkey = 'id_interno';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                datos.push(new_row);
            });
            $.ID_318354873.data({
                data: datos
            });
        } else {
            var ID_444425091_i = Alloy.Collections.insp_niveles;
            var sql = "DELETE FROM " + ID_444425091_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_444425091_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_444425091_i.trigger('delete');
            var item_index = 0;
            _.each('1,2,3,4,5'.split(','), function(item, item_pos, item_list) {
                item_index += 1;
                var ID_338786726_m = Alloy.Collections.insp_niveles;
                var ID_338786726_fila = Alloy.createModel('insp_niveles', {
                    id_inspeccion: -1,
                    nombre: 'Nivel' + item,
                    piso: item
                });
                ID_338786726_m.add(ID_338786726_fila);
                ID_338786726_fila.save();
                _.defer(function() {});
            });
            var ID_289148595_i = Alloy.createCollection('insp_niveles');
            ID_289148595_i.fetch();
            var ID_289148595_src = require('helper').query2array(ID_289148595_i);
            var datos = [];
            _.each(ID_289148595_src, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'valor';
                    if (llave == 'id') newkey = 'id_interno';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                datos.push(new_row);
            });
            $.ID_318354873.data({
                data: datos
            });
        }
    };
    var ID_5498125 = setTimeout(ID_5498125_func, 1000 * 0.2);

}

$.ID_710129457.init({
    caja: 45,
    __id: 'ALL710129457',
    onlisto: Listo_ID_1484159627,
    left: 0,
    top: 0,
    onclick: Click_ID_1325097596
});

function Click_ID_1325097596(e) {

    var evento = e;
    /** 
     * Definimos que estamos capturando foto en el primer item 
     */
    require('vars')[_var_scopekey]['cual_foto'] = '1';
    /** 
     * Abrimos camara 
     */
    $.ID_523196316.disparar({});
    /** 
     * Detenemos las animaciones de los widget 
     */
    $.ID_574301926.detener({});
    $.ID_222932900.detener({});
    $.ID_710129457.detener({});

}

function Listo_ID_1484159627(e) {

    var evento = e;
    /** 
     * Recuperamos variables para poder definir la estructura de las carpetas donde se guardara la miniatura 
     */
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    var nuevoid_f1 = ('nuevoid_f1' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['nuevoid_f1'] : '';
    if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
        var ID_153663287_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + seltarea.id_server);
        if (ID_153663287_d.exists() == false) ID_153663287_d.createDirectory();
        var ID_153663287_f = Ti.Filesystem.getFile(ID_153663287_d.resolve(), 'mini' + nuevoid_f1.id + '.jpg');
        if (ID_153663287_f.exists() == true) ID_153663287_f.deleteFile();
        ID_153663287_f.write(evento.mini);
        ID_153663287_d = null;
        ID_153663287_f = null;
    } else {
        var ID_327315158_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + 0);
        if (ID_327315158_d.exists() == false) ID_327315158_d.createDirectory();
        var ID_327315158_f = Ti.Filesystem.getFile(ID_327315158_d.resolve(), 'mini' + nuevoid_f1.id + '.jpg');
        if (ID_327315158_f.exists() == true) ID_327315158_f.deleteFile();
        ID_327315158_f.write(evento.mini);
        ID_327315158_d = null;
        ID_327315158_f = null;
    }
}

$.ID_574301926.init({
    caja: 45,
    __id: 'ALL574301926',
    onlisto: Listo_ID_1079566210,
    left: 0,
    top: 0,
    onclick: Click_ID_421046353
});

function Click_ID_421046353(e) {

    var evento = e;
    require('vars')[_var_scopekey]['cual_foto'] = 2;
    $.ID_523196316.disparar({});
    $.ID_222932900.detener({});
    $.ID_710129457.detener({});
    $.ID_574301926.detener({});

}

function Listo_ID_1079566210(e) {

    var evento = e;
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    var nuevoid_f2 = ('nuevoid_f2' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['nuevoid_f2'] : '';
    if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
        var ID_462637464_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + seltarea.id_server);
        if (ID_462637464_d.exists() == false) ID_462637464_d.createDirectory();
        var ID_462637464_f = Ti.Filesystem.getFile(ID_462637464_d.resolve(), 'mini' + nuevoid_f2.id + '.jpg');
        if (ID_462637464_f.exists() == true) ID_462637464_f.deleteFile();
        ID_462637464_f.write(evento.mini);
        ID_462637464_d = null;
        ID_462637464_f = null;
    } else {
        var ID_1888580742_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + 0);
        if (ID_1888580742_d.exists() == false) ID_1888580742_d.createDirectory();
        var ID_1888580742_f = Ti.Filesystem.getFile(ID_1888580742_d.resolve(), 'mini' + nuevoid_f2.id + '.jpg');
        if (ID_1888580742_f.exists() == true) ID_1888580742_f.deleteFile();
        ID_1888580742_f.write(evento.mini);
        ID_1888580742_d = null;
        ID_1888580742_f = null;
    }
}

$.ID_222932900.init({
    caja: 45,
    __id: 'ALL222932900',
    onlisto: Listo_ID_1713850383,
    left: 0,
    top: 0,
    onclick: Click_ID_1563335826
});

function Click_ID_1563335826(e) {

    var evento = e;
    require('vars')[_var_scopekey]['cual_foto'] = 3;
    $.ID_523196316.disparar({});
    $.ID_574301926.detener({});
    $.ID_710129457.detener({});
    $.ID_222932900.detener({});

}

function Listo_ID_1713850383(e) {

    var evento = e;
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    var nuevoid_f3 = ('nuevoid_f3' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['nuevoid_f3'] : '';
    if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
        var ID_1959180332_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + seltarea.id_server);
        if (ID_1959180332_d.exists() == false) ID_1959180332_d.createDirectory();
        var ID_1959180332_f = Ti.Filesystem.getFile(ID_1959180332_d.resolve(), 'mini' + nuevoid_f3.id + '.jpg');
        if (ID_1959180332_f.exists() == true) ID_1959180332_f.deleteFile();
        ID_1959180332_f.write(evento.mini);
        ID_1959180332_d = null;
        ID_1959180332_f = null;
    } else {
        var ID_946227262_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + 0);
        if (ID_946227262_d.exists() == false) ID_946227262_d.createDirectory();
        var ID_946227262_f = Ti.Filesystem.getFile(ID_946227262_d.resolve(), 'mini' + nuevoid_f3.id + '.jpg');
        if (ID_946227262_f.exists() == true) ID_946227262_f.deleteFile();
        ID_946227262_f.write(evento.mini);
        ID_946227262_d = null;
        ID_946227262_f = null;
    }
}

function Change_ID_1886291990(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    /** 
     * Obtenemos el valor ingresado en campo de ancho, para poder calcular la superficie 
     */
    var ancho;
    ancho = $.ID_312976671.getValue();

    if ((_.isObject(ancho) || (_.isString(ancho)) && !_.isEmpty(ancho)) || _.isNumber(ancho) || _.isBoolean(ancho)) {
        if ((_.isObject(elemento) || (_.isString(elemento)) && !_.isEmpty(elemento)) || _.isNumber(elemento) || _.isBoolean(elemento)) {
            /** 
             * nos aseguramos que ancho y valor de campo sean decimales con puntos (flotante), y no comas (espa&#241;ol) 
             */
            var nuevo = parseFloat(ancho.split(',').join('.')) * parseFloat(elemento.split(',').join('.'));
            $.recinto1.set({
                superficie: nuevo
            });
            if ('recinto1' in $) $recinto1 = $.recinto1.toJSON();
        }
    }
    /** 
     * Guardamos el largo del recinto 
     */
    $.recinto1.set({
        largo: elemento
    });
    if ('recinto1' in $) $recinto1 = $.recinto1.toJSON();
    elemento = null, source = null;

}

function Change_ID_337904684(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    var largo;
    largo = $.ID_989128261.getValue();

    if ((_.isObject(largo) || (_.isString(largo)) && !_.isEmpty(largo)) || _.isNumber(largo) || _.isBoolean(largo)) {
        if ((_.isObject(elemento) || (_.isString(elemento)) && !_.isEmpty(elemento)) || _.isNumber(elemento) || _.isBoolean(elemento)) {
            /** 
             * nos aseguramos que largo y valor de campo sean decimales con puntos (flotante), y no comas (espa&#241;ol) 
             */
            var nuevo = parseFloat(largo.split(',').join('.')) * parseFloat(elemento.split(',').join('.'));
            $.recinto1.set({
                superficie: nuevo
            });
            if ('recinto1' in $) $recinto1 = $.recinto1.toJSON();
        }
    }
    $.recinto1.set({
        ancho: elemento
    });
    if ('recinto1' in $) $recinto1 = $.recinto1.toJSON();
    elemento = null, source = null;

}

function Change_ID_481726049(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    $.recinto1.set({
        alto: elemento
    });
    if ('recinto1' in $) $recinto1 = $.recinto1.toJSON();
    elemento = null, source = null;

}

$.ID_927397228.init({
    titulo: 'Estructura Soportante',
    cargando: 'cargando...',
    __id: 'ALL927397228',
    oncerrar: Cerrar_ID_830805378,
    left: 0,
    hint: 'Seleccione estructura',
    color: 'morado',
    subtitulo: 'Indique los tipos',
    right: 0,
    onclick: Click_ID_157591423,
    onafterinit: Afterinit_ID_1390643755
});

function Click_ID_157591423(e) {

    var evento = e;

}

function Cerrar_ID_830805378(e) {

    var evento = e;
    $.ID_927397228.update({});
    $.recinto1.set({
        ids_estructuras_soportantes: evento.valores
    });
    if ('recinto1' in $) $recinto1 = $.recinto1.toJSON();

}

function Afterinit_ID_1390643755(e) {

    var evento = e;
    var ID_557613849_func = function() {
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
            var ID_635387054_i = Alloy.createCollection('estructura_soportante');
            var ID_635387054_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
            ID_635387054_i.fetch({
                query: 'SELECT * FROM estructura_soportante WHERE pais_texto=\'' + pais[0].nombre + '\''
            });
            var estructura = require('helper').query2array(ID_635387054_i);
            var datos = [];
            _.each(estructura, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'label';
                    if (llave == 'id_segured') newkey = 'valor';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                new_row['estado'] = 0;
                datos.push(new_row);
            });
            $.ID_927397228.update({
                data: datos
            });
        } else {
            var ID_1645265541_i = Alloy.Collections.estructura_soportante;
            var sql = "DELETE FROM " + ID_1645265541_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_1645265541_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_1645265541_i.trigger('delete');
            var item_index = 0;
            _.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
                item_index += 1;
                var ID_119396799_m = Alloy.Collections.estructura_soportante;
                var ID_119396799_fila = Alloy.createModel('estructura_soportante', {
                    nombre: 'Estructura' + item,
                    id_server: '0' + item,
                    id_segured: '10' + item,
                    pais: 1
                });
                ID_119396799_m.add(ID_119396799_fila);
                ID_119396799_fila.save();
                _.defer(function() {});
            });
            var ID_1789599019_i = Alloy.createCollection('estructura_soportante');
            ID_1789599019_i.fetch();
            var ID_1789599019_src = require('helper').query2array(ID_1789599019_i);
            var datos = [];
            _.each(ID_1789599019_src, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'label';
                    if (llave == 'id_segured') newkey = 'valor';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                new_row['estado'] = 0;
                datos.push(new_row);
            });
            $.ID_927397228.update({
                data: datos
            });
        }
    };
    var ID_557613849 = setTimeout(ID_557613849_func, 1000 * 0.2);

}

$.ID_346546200.init({
    titulo: 'Muros / Tabiques',
    cargando: 'cargando...',
    __id: 'ALL346546200',
    oncerrar: Cerrar_ID_834877674,
    left: 0,
    hint: 'Seleccione muros',
    color: 'morado',
    subtitulo: 'Indique los tipos',
    right: 0,
    onclick: Click_ID_202656539,
    onafterinit: Afterinit_ID_132420686
});

function Click_ID_202656539(e) {

    var evento = e;

}

function Cerrar_ID_834877674(e) {

    var evento = e;
    $.ID_346546200.update({});
    $.recinto1.set({
        ids_muros: evento.valores
    });
    if ('recinto1' in $) $recinto1 = $.recinto1.toJSON();

}

function Afterinit_ID_132420686(e) {

    var evento = e;
    var ID_1891160530_func = function() {
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
            var ID_1950438853_i = Alloy.createCollection('muros_tabiques');
            var ID_1950438853_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
            ID_1950438853_i.fetch({
                query: 'SELECT * FROM muros_tabiques WHERE pais_texto=\'' + pais[0].nombre + '\''
            });
            var tabiques = require('helper').query2array(ID_1950438853_i);
            var datos = [];
            _.each(tabiques, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'label';
                    if (llave == 'id_segured') newkey = 'valor';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                new_row['estado'] = 0;
                datos.push(new_row);
            });
            $.ID_346546200.update({
                data: datos
            });
        } else {
            var ID_82614408_i = Alloy.Collections.muros_tabiques;
            var sql = "DELETE FROM " + ID_82614408_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_82614408_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_82614408_i.trigger('delete');
            var item_index = 0;
            _.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
                item_index += 1;
                var ID_1909960548_m = Alloy.Collections.muros_tabiques;
                var ID_1909960548_fila = Alloy.createModel('muros_tabiques', {
                    nombre: 'Muros' + item,
                    id_server: '0' + item,
                    id_segured: '10' + item,
                    pais: 1
                });
                ID_1909960548_m.add(ID_1909960548_fila);
                ID_1909960548_fila.save();
                _.defer(function() {});
            });
            var ID_415873659_i = Alloy.createCollection('muros_tabiques');
            ID_415873659_i.fetch();
            var ID_415873659_src = require('helper').query2array(ID_415873659_i);
            var datos = [];
            _.each(ID_415873659_src, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'label';
                    if (llave == 'id_segured') newkey = 'valor';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                new_row['estado'] = 0;
                datos.push(new_row);
            });
            $.ID_346546200.update({
                data: datos
            });
        }
    };
    var ID_1891160530 = setTimeout(ID_1891160530_func, 1000 * 0.2);

}

$.ID_625792866.init({
    titulo: 'Entrepisos',
    cargando: 'cargando...',
    __id: 'ALL625792866',
    oncerrar: Cerrar_ID_972457547,
    left: 0,
    hint: 'Seleccione entrepisos',
    color: 'morado',
    subtitulo: 'Indique los tipos',
    right: 0,
    onclick: Click_ID_147312300,
    onafterinit: Afterinit_ID_726345732
});

function Click_ID_147312300(e) {

    var evento = e;

}

function Cerrar_ID_972457547(e) {

    var evento = e;
    $.ID_625792866.update({});
    $.recinto1.set({
        ids_entrepisos: evento.valores
    });
    if ('recinto1' in $) $recinto1 = $.recinto1.toJSON();

}

function Afterinit_ID_726345732(e) {

    var evento = e;
    var ID_1011543706_func = function() {
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
            var ID_1400398066_i = Alloy.createCollection('entrepisos');
            var ID_1400398066_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
            ID_1400398066_i.fetch({
                query: 'SELECT * FROM entrepisos WHERE pais_texto=\'' + pais[0].nombre + '\''
            });
            var entrepisos = require('helper').query2array(ID_1400398066_i);
            var datos = [];
            _.each(entrepisos, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'label';
                    if (llave == 'id_segured') newkey = 'valor';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                new_row['estado'] = 0;
                datos.push(new_row);
            });
            $.ID_625792866.update({
                data: datos
            });
        } else {
            var ID_1047326134_i = Alloy.Collections.entrepisos;
            var sql = "DELETE FROM " + ID_1047326134_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_1047326134_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_1047326134_i.trigger('delete');
            var item_index = 0;
            _.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
                item_index += 1;
                var ID_776252387_m = Alloy.Collections.entrepisos;
                var ID_776252387_fila = Alloy.createModel('entrepisos', {
                    nombre: 'Entrepiso' + item,
                    id_server: '0' + item,
                    id_segured: '10' + item,
                    pais: 1
                });
                ID_776252387_m.add(ID_776252387_fila);
                ID_776252387_fila.save();
                _.defer(function() {});
            });
            var ID_1062047321_i = Alloy.createCollection('entrepisos');
            ID_1062047321_i.fetch();
            var ID_1062047321_src = require('helper').query2array(ID_1062047321_i);
            var datos = [];
            _.each(ID_1062047321_src, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'label';
                    if (llave == 'id_segured') newkey = 'valor';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                new_row['estado'] = 0;
                datos.push(new_row);
            });
            $.ID_625792866.update({
                data: datos
            });
        }
    };
    var ID_1011543706 = setTimeout(ID_1011543706_func, 1000 * 0.2);

}

$.ID_1586767745.init({
    titulo: 'Pavimentos',
    cargando: 'cargando...',
    __id: 'ALL1586767745',
    oncerrar: Cerrar_ID_1074213826,
    left: 0,
    hint: 'Seleccione pavimentos',
    color: 'morado',
    subtitulo: 'Indique los tipos',
    right: 0,
    onclick: Click_ID_215582552,
    onafterinit: Afterinit_ID_1732309091
});

function Click_ID_215582552(e) {

    var evento = e;

}

function Cerrar_ID_1074213826(e) {

    var evento = e;
    $.ID_1586767745.update({});
    $.recinto1.set({
        ids_pavimentos: evento.valores
    });
    if ('recinto1' in $) $recinto1 = $.recinto1.toJSON();

}

function Afterinit_ID_1732309091(e) {

    var evento = e;
    var ID_1194607943_func = function() {
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
            var ID_206164326_i = Alloy.createCollection('pavimento');
            var ID_206164326_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
            ID_206164326_i.fetch({
                query: 'SELECT * FROM pavimento WHERE pais_texto=\'' + pais[0].nombre + '\''
            });
            var pavimentos = require('helper').query2array(ID_206164326_i);
            var datos = [];
            _.each(pavimentos, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'label';
                    if (llave == 'id_segured') newkey = 'valor';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                new_row['estado'] = 0;
                datos.push(new_row);
            });
            $.ID_1586767745.update({
                data: datos
            });
        } else {
            var ID_769290379_i = Alloy.Collections.pavimento;
            var sql = "DELETE FROM " + ID_769290379_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_769290379_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_769290379_i.trigger('delete');
            var item_index = 0;
            _.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
                item_index += 1;
                var ID_1568439666_m = Alloy.Collections.pavimento;
                var ID_1568439666_fila = Alloy.createModel('pavimento', {
                    nombre: 'Pavimento' + item,
                    id_server: '0' + item,
                    id_segured: '10' + item,
                    pais: 1
                });
                ID_1568439666_m.add(ID_1568439666_fila);
                ID_1568439666_fila.save();
                _.defer(function() {});
            });
            var ID_164096330_i = Alloy.createCollection('pavimento');
            ID_164096330_i.fetch();
            var ID_164096330_src = require('helper').query2array(ID_164096330_i);
            var datos = [];
            _.each(ID_164096330_src, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'label';
                    if (llave == 'id_segured') newkey = 'valor';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                new_row['estado'] = 0;
                datos.push(new_row);
            });
            $.ID_1586767745.update({
                data: datos
            });
        }
    };
    var ID_1194607943 = setTimeout(ID_1194607943_func, 1000 * 0.2);

}

$.ID_1781249673.init({
    titulo: 'Estruct. cubierta',
    cargando: 'cargando...',
    __id: 'ALL1781249673',
    oncerrar: Cerrar_ID_350978229,
    left: 0,
    hint: 'Seleccione e.cubiertas',
    color: 'morado',
    subtitulo: 'Indique los tipos',
    right: 0,
    onclick: Click_ID_479853667,
    onafterinit: Afterinit_ID_1399222471
});

function Click_ID_479853667(e) {

    var evento = e;

}

function Cerrar_ID_350978229(e) {

    var evento = e;
    $.ID_1781249673.update({});
    $.recinto1.set({
        ids_estructura_cubiera: evento.valores
    });
    if ('recinto1' in $) $recinto1 = $.recinto1.toJSON();

}

function Afterinit_ID_1399222471(e) {

    var evento = e;
    var ID_1382842841_func = function() {
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
            var ID_1132523308_i = Alloy.createCollection('estructura_cubierta');
            var ID_1132523308_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
            ID_1132523308_i.fetch({
                query: 'SELECT * FROM estructura_cubierta WHERE pais_texto=\'' + pais[0].nombre + '\''
            });
            var ecubiertas = require('helper').query2array(ID_1132523308_i);
            var datos = [];
            _.each(ecubiertas, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'label';
                    if (llave == 'id_segured') newkey = 'valor';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                new_row['estado'] = 0;
                datos.push(new_row);
            });
            $.ID_1781249673.update({
                data: datos
            });
        } else {
            var ID_574445548_i = Alloy.Collections.estructura_cubierta;
            var sql = "DELETE FROM " + ID_574445548_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_574445548_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_574445548_i.trigger('delete');
            var item_index = 0;
            _.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
                item_index += 1;
                var ID_1208356661_m = Alloy.Collections.estructura_cubierta;
                var ID_1208356661_fila = Alloy.createModel('estructura_cubierta', {
                    nombre: 'Estru Cubierta ' + item,
                    id_server: '0' + item,
                    id_segured: '10' + item,
                    pais: 1
                });
                ID_1208356661_m.add(ID_1208356661_fila);
                ID_1208356661_fila.save();
                _.defer(function() {});
            });
            var ID_265059048_i = Alloy.createCollection('estructura_cubierta');
            ID_265059048_i.fetch();
            var ID_265059048_src = require('helper').query2array(ID_265059048_i);
            var datos = [];
            _.each(ID_265059048_src, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'label';
                    if (llave == 'id_segured') newkey = 'valor';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                new_row['estado'] = 0;
                datos.push(new_row);
            });
            $.ID_1781249673.update({
                data: datos
            });
        }
    };
    var ID_1382842841 = setTimeout(ID_1382842841_func, 1000 * 0.2);

}

$.ID_1240971093.init({
    titulo: 'Cubierta',
    cargando: 'cargando...',
    __id: 'ALL1240971093',
    oncerrar: Cerrar_ID_803973850,
    left: 0,
    hint: 'Seleccione cubiertas',
    color: 'morado',
    subtitulo: 'Indique los tipos',
    right: 0,
    onclick: Click_ID_218535665,
    onafterinit: Afterinit_ID_582322610
});

function Click_ID_218535665(e) {

    var evento = e;

}

function Cerrar_ID_803973850(e) {

    var evento = e;
    $.ID_1240971093.update({});
    $.recinto1.set({
        ids_cubierta: evento.valores
    });
    if ('recinto1' in $) $recinto1 = $.recinto1.toJSON();

}

function Afterinit_ID_582322610(e) {

    var evento = e;
    var ID_752822451_func = function() {
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
            var ID_1775572924_i = Alloy.createCollection('cubierta');
            var ID_1775572924_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
            ID_1775572924_i.fetch({
                query: 'SELECT * FROM cubierta WHERE pais_texto=\'' + pais[0].nombre + '\''
            });
            var cubiertas = require('helper').query2array(ID_1775572924_i);
            var datos = [];
            _.each(cubiertas, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'label';
                    if (llave == 'id_segured') newkey = 'valor';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                new_row['estado'] = 0;
                datos.push(new_row);
            });
            $.ID_1240971093.update({
                data: datos
            });
        } else {
            var ID_974369323_i = Alloy.Collections.cubierta;
            var sql = "DELETE FROM " + ID_974369323_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_974369323_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_974369323_i.trigger('delete');
            var item_index = 0;
            _.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
                item_index += 1;
                var ID_1829268277_m = Alloy.Collections.cubierta;
                var ID_1829268277_fila = Alloy.createModel('cubierta', {
                    nombre: 'Cubierta ' + item,
                    id_server: '0' + item,
                    id_segured: '10' + item,
                    pais: 1
                });
                ID_1829268277_m.add(ID_1829268277_fila);
                ID_1829268277_fila.save();
                _.defer(function() {});
            });
            var ID_290521862_i = Alloy.createCollection('cubierta');
            ID_290521862_i.fetch();
            var ID_290521862_src = require('helper').query2array(ID_290521862_i);
            var datos = [];
            _.each(ID_290521862_src, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'label';
                    if (llave == 'id_segured') newkey = 'valor';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                new_row['estado'] = 0;
                datos.push(new_row);
            });
            $.ID_1240971093.update({
                data: datos
            });
        }
    };
    var ID_752822451 = setTimeout(ID_752822451_func, 1000 * 0.2);

}

function Click_ID_14046110(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    /** 
     * Ocupamos esta variable para impedir que la pantalla siguente se cargue dos veces 
     */
    var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
    if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
        require('vars')['var_abriendo'] = 'true';
        Alloy.createController("nuevodano_index", {}).getView().open();
    }

}

function Swipe_ID_1756922308(e) {

    e.cancelBubble = true;
    var elemento = e.section.getItemAt(e.itemIndex);
    var findVariables = require('fvariables');
    var fila = {};
    _.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
        _.each(obj_id, function(valor, prop) {
            fila[prop] = elemento[id_field][prop];
            var llaves = findVariables(valor, '{', '}');
            _.each(llaves, function(llave) {
                fila[llave] = elemento[id_field][prop];
            });
        });
    });
    if (Ti.App.deployType != 'production') console.log('en normal', {
        "e": e
    });
    if (e.direction == 'left') {
        var findVariables = require('fvariables');
        elemento.template = 'pborrar';
        _.each(_list_templates['pborrar'], function(obj_id, id_field) {
            _.each(obj_id, function(valor, prop) {
                var llaves = findVariables(valor, '{', '}');
                _.each(llaves, function(llave) {
                    elemento[id_field] = {};
                    elemento[id_field][prop] = fila[llave];
                });
            });
        });
        if (OS_IOS) {
            e.section.updateItemAt(e.itemIndex, elemento, {
                animated: true
            });
        } else if (OS_ANDROID) {
            e.section.updateItemAt(e.itemIndex, elemento);
        }
    }

}

function Click_ID_1034904688(e) {

    e.cancelBubble = true;
    var elemento = e.section.getItemAt(e.itemIndex);
    var findVariables = require('fvariables');
    var fila = {};
    _.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
        _.each(obj_id, function(valor, prop) {
            fila[prop] = elemento[id_field][prop];
            var llaves = findVariables(valor, '{', '}');
            _.each(llaves, function(llave) {
                fila[llave] = elemento[id_field][prop];
            });
        });
    });
    /** 
     * Enviamos el parametro _dato para indicar cual sera el id del dano a editar 
     */
    Alloy.createController("editardano_index", {
        '_dato': fila
    }).getView().open();

}

function Click_ID_1378885532(e) {

    e.cancelBubble = true;
    var elemento = e.section.getItemAt(e.itemIndex);
    var findVariables = require('fvariables');
    var fila = {};
    _.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
        _.each(obj_id, function(valor, prop) {
            fila[prop] = elemento[id_field][prop];
            var llaves = findVariables(valor, '{', '}');
            _.each(llaves, function(llave) {
                fila[llave] = elemento[id_field][prop];
            });
        });
    });
    if (Ti.App.deployType != 'production') console.log('click en borrar', {
        "e": e
    });
    var ID_2066883194_opts = ['Si', 'No'];
    var ID_2066883194 = Ti.UI.createAlertDialog({
        title: 'ALERTA',
        message: '' + '¿ Seguro desea eliminar: ' + fila.nombre + ' ?' + '',
        buttonNames: ID_2066883194_opts
    });
    ID_2066883194.addEventListener('click', function(e) {
        var xd = ID_2066883194_opts[e.index];
        if (xd == 'Si') {
            if (Ti.App.deployType != 'production') console.log('borrando xd', {});
            var ID_2082690396_i = Alloy.Collections.insp_itemdanos;
            var sql = 'DELETE FROM ' + ID_2082690396_i.config.adapter.collection_name + ' WHERE id=\'' + fila.id + '\'';
            var db = Ti.Database.open(ID_2082690396_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_2082690396_i.trigger('delete');
            _.defer(function() {
                Alloy.Collections.insp_itemdanos.fetch();
            });
        }
        xd = null;
        e.source.removeEventListener("click", arguments.callee);
    });
    ID_2066883194.show();

}

function Swipe_ID_2015164508(e) {

    e.cancelBubble = true;
    var elemento = e.section.getItemAt(e.itemIndex);
    var findVariables = require('fvariables');
    var fila = {};
    _.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
        _.each(obj_id, function(valor, prop) {
            fila[prop] = elemento[id_field][prop];
            var llaves = findVariables(valor, '{', '}');
            _.each(llaves, function(llave) {
                fila[llave] = elemento[id_field][prop];
            });
        });
    });
    if (Ti.App.deployType != 'production') console.log('en borrar', {
        "e": e
    });
    if (e.direction == 'right') {
        if (Ti.App.deployType != 'production') console.log('swipe hacia derecha (desde borrar)', {});
        var findVariables = require('fvariables');
        elemento.template = 'dano';
        _.each(_list_templates['dano'], function(obj_id, id_field) {
            _.each(obj_id, function(valor, prop) {
                var llaves = findVariables(valor, '{', '}');
                _.each(llaves, function(llave) {
                    elemento[id_field] = {};
                    elemento[id_field][prop] = fila[llave];
                });
            });
        });
        if (OS_IOS) {
            e.section.updateItemAt(e.itemIndex, elemento, {
                animated: true
            });
        } else if (OS_ANDROID) {
            e.section.updateItemAt(e.itemIndex, elemento);
        }
    }

}

function Click_ID_1678728058(e) {

    e.cancelBubble = true;
    var elemento = e.section.getItemAt(e.itemIndex);
    var findVariables = require('fvariables');
    var fila = {};
    _.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
        _.each(obj_id, function(valor, prop) {
            fila[prop] = elemento[id_field][prop];
            var llaves = findVariables(valor, '{', '}');
            _.each(llaves, function(llave) {
                fila[llave] = elemento[id_field][prop];
            });
        });
    });
    if (Ti.App.deployType != 'production') console.log('en borrar', {
        "e": e
    });
    var findVariables = require('fvariables');
    elemento.template = 'dano';
    _.each(_list_templates['dano'], function(obj_id, id_field) {
        _.each(obj_id, function(valor, prop) {
            var llaves = findVariables(valor, '{', '}');
            _.each(llaves, function(llave) {
                elemento[id_field] = {};
                elemento[id_field][prop] = fila[llave];
            });
        });
    });
    if (OS_IOS) {
        e.section.updateItemAt(e.itemIndex, elemento, {
            animated: true
        });
    } else if (OS_ANDROID) {
        e.section.updateItemAt(e.itemIndex, elemento);
    }

}

$.ID_523196316.init({
    onfotolista: Fotolista_ID_1524395882,
    __id: 'ALL523196316'
});

function Fotolista_ID_1524395882(e) {

    var evento = e;
    /** 
     * Recuperamos cual fue la foto seleccionada 
     */
    var cual_foto = ('cual_foto' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['cual_foto'] : '';
    if (cual_foto == 1 || cual_foto == '1') {
        var ID_616043033_m = Alloy.Collections.numero_unico;
        var ID_616043033_fila = Alloy.createModel('numero_unico', {
            comentario: 'nuevo recinto foto1'
        });
        ID_616043033_m.add(ID_616043033_fila);
        ID_616043033_fila.save();
        var nuevoid_f1 = require('helper').model2object(ID_616043033_m.last());
        _.defer(function() {});
        /** 
         * Guardamos en variable el detalle del modelo numero_unico para asignar informacion de foto1 del recinto 
         */
        require('vars')[_var_scopekey]['nuevoid_f1'] = nuevoid_f1;
        /** 
         * Recuperamos variable para saber si estamos haciendo una inspeccion o dummy y guardar la foto en la memoria del celular 
         */
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        var gps_coords = ('gps_coords' in require('vars')) ? require('vars')['gps_coords'] : '';
        var gps_latitud = ('gps_latitud' in require('vars')) ? require('vars')['gps_latitud'] : '';
        var gps_longitud = ('gps_longitud' in require('vars')) ? require('vars')['gps_longitud'] : '';
        var resultado = null;
        if ('distanceinmtbetweenearthcoordinates' in require('funciones')) {
            resultado = require('funciones').distanceinmtbetweenearthcoordinates({
                'lat1': gps_latitud,
                'lon1': gps_longitud,
                'lat2': seltarea.lat,
                'lon2': seltarea.lon
            });
        } else {
            try {
                resultado = f_distanceinmtbetweenearthcoordinates({
                    'lat1': gps_latitud,
                    'lon1': gps_longitud,
                    'lat2': seltarea.lat,
                    'lon2': seltarea.lon
                });
            } catch (ee) {}
        }
        if (_.isNumber(gps_coords.accuracy) && _.isNumber(resultado) && gps_coords.accuracy > resultado) {
            /** 
             * Pablo me comento que el flag fuera_rango se determina comparando precision vs la distancia de ubicacion con la definida en tarea 
             */
            var json_datosfoto = {
                lat: gps_latitud,
                lng: gps_longitud,
                fuera_rango: 0,
                distancia: Math.round(resultado),
                precision: Math.round(gps_coords.accuracy)
            };
        } else {
            var json_datosfoto = {
                lat: gps_latitud,
                lng: gps_longitud,
                fuera_rango: 1,
                distancia: Math.round(resultado),
                precision: Math.round(gps_coords.accuracy)
            };
        }
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var ID_697735347_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
            if (ID_697735347_d.exists() == false) ID_697735347_d.createDirectory();
            var ID_697735347_f = Ti.Filesystem.getFile(ID_697735347_d.resolve(), 'cap' + nuevoid_f1.id + '.jpg');
            if (ID_697735347_f.exists() == true) ID_697735347_f.deleteFile();
            ID_697735347_f.write(evento.foto);
            ID_697735347_d = null;
            ID_697735347_f = null;
            var ID_393008998_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
            if (ID_393008998_d.exists() == false) ID_393008998_d.createDirectory();
            var ID_393008998_f = Ti.Filesystem.getFile(ID_393008998_d.resolve(), 'cap' + nuevoid_f1.id + '.json');
            if (ID_393008998_f.exists() == true) ID_393008998_f.deleteFile();
            console.log('contenido f1 nrecinto', JSON.stringify(json_datosfoto));
            ID_393008998_f.write(JSON.stringify(json_datosfoto));
            ID_393008998_d = null;
            ID_393008998_f = null;
        } else {
            var ID_1281614579_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + 0);
            if (ID_1281614579_d.exists() == false) ID_1281614579_d.createDirectory();
            var ID_1281614579_f = Ti.Filesystem.getFile(ID_1281614579_d.resolve(), 'cap' + nuevoid_f1.id + '.jpg');
            if (ID_1281614579_f.exists() == true) ID_1281614579_f.deleteFile();
            ID_1281614579_f.write(evento.foto);
            ID_1281614579_d = null;
            ID_1281614579_f = null;
        }
        /** 
         * Actualizamos el modelo indicando cual es el nombre de la imagen 
         */
        $.recinto1.set({
            foto1: 'cap' + nuevoid_f1.id + '.jpg'
        });
        if ('recinto1' in $) $recinto1 = $.recinto1.toJSON();
        /** 
         * Enviamos la foto para que genere la miniatura (y tambien la guarde) y muestre en pantalla 
         */
        $.ID_710129457.procesar({
            imagen: evento.foto,
            nueva: 640,
            calidad: 91,
            camara: 'trasera'
        });
        /** 
         * Limpiamos memoria ram 
         */
        evento = null;
    } else if (cual_foto == 2) {
        var ID_498790765_m = Alloy.Collections.numero_unico;
        var ID_498790765_fila = Alloy.createModel('numero_unico', {
            comentario: 'nuevo recinto foto2'
        });
        ID_498790765_m.add(ID_498790765_fila);
        ID_498790765_fila.save();
        var nuevoid_f2 = require('helper').model2object(ID_498790765_m.last());
        _.defer(function() {});
        require('vars')[_var_scopekey]['nuevoid_f2'] = nuevoid_f2;
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        var gps_coords = ('gps_coords' in require('vars')) ? require('vars')['gps_coords'] : '';
        var gps_latitud = ('gps_latitud' in require('vars')) ? require('vars')['gps_latitud'] : '';
        var gps_longitud = ('gps_longitud' in require('vars')) ? require('vars')['gps_longitud'] : '';
        var resultado = null;
        if ('distanceinmtbetweenearthcoordinates' in require('funciones')) {
            resultado = require('funciones').distanceinmtbetweenearthcoordinates({
                'lat1': gps_latitud,
                'lon1': gps_longitud,
                'lat2': seltarea.lat,
                'lon2': seltarea.lon
            });
        } else {
            try {
                resultado = f_distanceinmtbetweenearthcoordinates({
                    'lat1': gps_latitud,
                    'lon1': gps_longitud,
                    'lat2': seltarea.lat,
                    'lon2': seltarea.lon
                });
            } catch (ee) {}
        }
        if (_.isNumber(gps_coords.accuracy) && _.isNumber(resultado) && gps_coords.accuracy > resultado) {
            /** 
             * Pablo me comento que el flag fuera_rango se determina comparando precision vs la distancia de ubicacion con la definida en tarea 
             */
            var json_datosfoto = {
                lat: gps_latitud,
                lng: gps_longitud,
                fuera_rango: 0,
                distancia: Math.round(resultado),
                precision: Math.round(gps_coords.accuracy)
            };
        } else {
            var json_datosfoto = {
                lat: gps_latitud,
                lng: gps_longitud,
                fuera_rango: 1,
                distancia: Math.round(resultado),
                precision: Math.round(gps_coords.accuracy)
            };
        }
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var ID_750880193_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
            if (ID_750880193_d.exists() == false) ID_750880193_d.createDirectory();
            var ID_750880193_f = Ti.Filesystem.getFile(ID_750880193_d.resolve(), 'cap' + nuevoid_f2.id + '.jpg');
            if (ID_750880193_f.exists() == true) ID_750880193_f.deleteFile();
            ID_750880193_f.write(evento.foto);
            ID_750880193_d = null;
            ID_750880193_f = null;
            var ID_1764535243_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
            if (ID_1764535243_d.exists() == false) ID_1764535243_d.createDirectory();
            var ID_1764535243_f = Ti.Filesystem.getFile(ID_1764535243_d.resolve(), 'cap' + nuevoid_f2.id + '.json');
            if (ID_1764535243_f.exists() == true) ID_1764535243_f.deleteFile();
            console.log('contenido f2 nrecinto', JSON.stringify(json_datosfoto));
            ID_1764535243_f.write(JSON.stringify(json_datosfoto));
            ID_1764535243_d = null;
            ID_1764535243_f = null;
        } else {
            var ID_1514258240_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + 0);
            if (ID_1514258240_d.exists() == false) ID_1514258240_d.createDirectory();
            var ID_1514258240_f = Ti.Filesystem.getFile(ID_1514258240_d.resolve(), 'cap' + nuevoid_f2.id + '.jpg');
            if (ID_1514258240_f.exists() == true) ID_1514258240_f.deleteFile();
            ID_1514258240_f.write(evento.foto);
            ID_1514258240_d = null;
            ID_1514258240_f = null;
        }
        $.recinto1.set({
            foto2: 'cap' + nuevoid_f2.id + '.jpg'
        });
        if ('recinto1' in $) $recinto1 = $.recinto1.toJSON();
        $.ID_574301926.procesar({
            imagen: evento.foto,
            nueva: 640,
            calidad: 91,
            camara: 'trasera'
        });
        evento = null;
    } else if (cual_foto == 3) {
        var ID_963509381_m = Alloy.Collections.numero_unico;
        var ID_963509381_fila = Alloy.createModel('numero_unico', {
            comentario: 'nuevo recinto foto3'
        });
        ID_963509381_m.add(ID_963509381_fila);
        ID_963509381_fila.save();
        var nuevoid_f3 = require('helper').model2object(ID_963509381_m.last());
        _.defer(function() {});
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        require('vars')[_var_scopekey]['nuevoid_f3'] = nuevoid_f3;
        var gps_coords = ('gps_coords' in require('vars')) ? require('vars')['gps_coords'] : '';
        var gps_latitud = ('gps_latitud' in require('vars')) ? require('vars')['gps_latitud'] : '';
        var gps_longitud = ('gps_longitud' in require('vars')) ? require('vars')['gps_longitud'] : '';
        var resultado = null;
        if ('distanceinmtbetweenearthcoordinates' in require('funciones')) {
            resultado = require('funciones').distanceinmtbetweenearthcoordinates({
                'lat1': gps_latitud,
                'lon1': gps_longitud,
                'lat2': seltarea.lat,
                'lon2': seltarea.lon
            });
        } else {
            try {
                resultado = f_distanceinmtbetweenearthcoordinates({
                    'lat1': gps_latitud,
                    'lon1': gps_longitud,
                    'lat2': seltarea.lat,
                    'lon2': seltarea.lon
                });
            } catch (ee) {}
        }
        if (_.isNumber(gps_coords.accuracy) && _.isNumber(resultado) && gps_coords.accuracy > resultado) {
            /** 
             * Pablo me comento que el flag fuera_rango se determina comparando precision vs la distancia de ubicacion con la definida en tarea 
             */
            var json_datosfoto = {
                lat: gps_latitud,
                lng: gps_longitud,
                fuera_rango: 0,
                distancia: Math.round(resultado),
                precision: Math.round(gps_coords.accuracy)
            };
        } else {
            var json_datosfoto = {
                lat: gps_latitud,
                lng: gps_longitud,
                fuera_rango: 1,
                distancia: Math.round(resultado),
                precision: Math.round(gps_coords.accuracy)
            };
        }
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var ID_1182736960_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
            if (ID_1182736960_d.exists() == false) ID_1182736960_d.createDirectory();
            var ID_1182736960_f = Ti.Filesystem.getFile(ID_1182736960_d.resolve(), 'cap' + nuevoid_f3.id + '.jpg');
            if (ID_1182736960_f.exists() == true) ID_1182736960_f.deleteFile();
            ID_1182736960_f.write(evento.foto);
            ID_1182736960_d = null;
            ID_1182736960_f = null;
            var ID_1103277653_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
            if (ID_1103277653_d.exists() == false) ID_1103277653_d.createDirectory();
            var ID_1103277653_f = Ti.Filesystem.getFile(ID_1103277653_d.resolve(), 'cap' + nuevoid_f3.id + '.json');
            if (ID_1103277653_f.exists() == true) ID_1103277653_f.deleteFile();
            console.log('contenido f3 nrecinto', JSON.stringify(json_datosfoto));
            ID_1103277653_f.write(JSON.stringify(json_datosfoto));
            ID_1103277653_d = null;
            ID_1103277653_f = null;
        } else {
            var ID_598482667_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + 0);
            if (ID_598482667_d.exists() == false) ID_598482667_d.createDirectory();
            var ID_598482667_f = Ti.Filesystem.getFile(ID_598482667_d.resolve(), 'cap' + nuevoid_f3.id + '.jpg');
            if (ID_598482667_f.exists() == true) ID_598482667_f.deleteFile();
            ID_598482667_f.write(evento.foto);
            ID_598482667_d = null;
            ID_598482667_f = null;
        }
        $.recinto1.set({
            foto3: 'cap' + nuevoid_f3.id + '.jpg'
        });
        if ('recinto1' in $) $recinto1 = $.recinto1.toJSON();
        $.ID_222932900.procesar({
            imagen: evento.foto,
            nueva: 640,
            calidad: 91,
            camara: 'trasera'
        });
        evento = null;
    }
}

(function() {
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
        /** 
         * Modificamos la idinspeccion con el idserver de la tarea 
         */
        $.recinto1.set({
            id_inspeccion: seltarea.id_server
        });
        if ('recinto1' in $) $recinto1 = $.recinto1.toJSON();
    }
    var ID_1918205425_m = Alloy.Collections.numero_unico;
    var ID_1918205425_fila = Alloy.createModel('numero_unico', {
        comentario: 'nuevo recinto id'
    });
    ID_1918205425_m.add(ID_1918205425_fila);
    ID_1918205425_fila.save();
    var nuevoid = require('helper').model2object(ID_1918205425_m.last());
    _.defer(function() {});
    /** 
     * guardamos variable pq aun no existe este recinto al agregar da&#241;os 
     */
    require('vars')['temp_idrecinto'] = nuevoid.id;
    /** 
     * Agregamos un id al recinto con el nuevo id que se asigna 
     */
    $.recinto1.set({
        id_recinto: nuevoid.id
    });
    if ('recinto1' in $) $recinto1 = $.recinto1.toJSON();
    /** 
     * filtramos items de dano, para el id_recinto recien creado. 
     */
    ID_1733573059_filter = function(coll) {
        var filtered = coll.filter(function(m) {
            var _tests = [],
                _all_true = false,
                model = m.toJSON();
            _tests.push((model.id_recinto == nuevoid.id));
            var _all_true_s = _.uniq(_tests);
            _all_true = (_all_true_s.length == 1 && _all_true_s[0] == true) ? true : false;
            return _all_true;
        });
        filtered = _.toArray(filtered);
        return filtered;
    };
    _.defer(function() {
        Alloy.Collections.insp_itemdanos.fetch();
    });
    /** 
     * Modificamos en 0.1 segundos el color del statusbar 
     */
    var ID_1272213430_func = function() {
        var ID_1168881001_statusbar = '#7E6EE0';

        var setearStatusColor = function(ID_1168881001_statusbar) {
            if (OS_IOS) {
                if (ID_1168881001_statusbar == 'light' || ID_1168881001_statusbar == 'claro') {
                    $.ID_1168881001_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.LIGHT_CONTENT);
                } else if (ID_1168881001_statusbar == 'grey' || ID_1168881001_statusbar == 'gris' || ID_1168881001_statusbar == 'gray') {
                    $.ID_1168881001_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.GREY);
                } else if (ID_1168881001_statusbar == 'oscuro' || ID_1168881001_statusbar == 'dark') {
                    $.ID_1168881001_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.DEFAULT);
                }
            } else if (OS_ANDROID) {
                abx.setStatusbarColor(ID_1168881001_statusbar);
            }
        };
        setearStatusColor(ID_1168881001_statusbar);

    };
    var ID_1272213430 = setTimeout(ID_1272213430_func, 1000 * 0.1);
})();

function Postlayout_ID_1782803398(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var ID_1821657941_func = function() {
        /** 
         * Una vez ya cargada la tarea, dejamos la variable limpia para poder abrir otras pantallas 
         */
        require('vars')['var_abriendo'] = '';
    };
    var ID_1821657941 = setTimeout(ID_1821657941_func, 1000 * 0.2);

}
if (OS_IOS || OS_ANDROID) {
    $.ID_1168881001.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}
//$.ID_1168881001.open();