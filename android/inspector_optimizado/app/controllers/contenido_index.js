var _bind4section = {
    "ref1": "insp_contenido"
};
var _list_templates = {
    "pborrar": {
        "ID_1236907100": {
            "text": "{id}"
        },
        "ID_1506538078": {},
        "ID_2024360383": {},
        "ID_1765564351": {},
        "ID_1699280427": {
            "text": "{nombre}"
        },
        "ID_1445903745": {}
    },
    "contenido": {
        "ID_630800887": {},
        "ID_862352109": {
            "text": "{id}"
        },
        "ID_560452274": {
            "text": "{nombre}"
        }
    }
};

var _activity;
if (OS_ANDROID) {
    _activity = $.ID_1058387122.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_1058387122';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_1058387122.addEventListener('open', function(e) {});
}
$.ID_1058387122.orientationModes = [Titanium.UI.PORTRAIT];


var ID_2014823490_like = function(search) {
    if (typeof search !== 'string' || this === null) {
        return false;
    }
    search = search.replace(new RegExp("([\\.\\\\\\+\\*\\?\\[\\^\\]\\$\\(\\)\\{\\}\\=\\!\\<\\>\\|\\:\\-])", "g"), "\\$1");
    search = search.replace(/%/g, '.*').replace(/_/g, '.');
    return RegExp('^' + search + '$', 'gi').test(this);
};
var ID_2014823490_filter = function(coll) {
    var filtered = _.toArray(coll.filter(function(m) {
        return true;
    }));
    return filtered;
};
var ID_2014823490_transform = function(model) {
    var fila = model.toJSON();
    return fila;
};
var ID_2014823490_update = function(e) {};
_.defer(function() {
    Alloy.Collections.insp_contenido.fetch();
});
Alloy.Collections.insp_contenido.on('add change delete', function(ee) {
    ID_2014823490_update(ee);
});
Alloy.Collections.insp_contenido.fetch();


$.ID_1809183737.init({
    titulo: 'DAÑOS EN CONTENIDOS',
    onsalirinsp: Salirinsp_ID_1128350973,
    __id: 'ALL1809183737',
    salir_insp: '',
    fondo: 'fondoceleste',
    top: 0,
    agregar_dano: '',
    onagregar_dano: Agregar_dano_ID_1287182980
});

function Salirinsp_ID_1128350973(e) {

    var evento = e;
    var ID_1086016081_opts = ['Asegurado no puede seguir', 'Se me acabo la bateria', 'Tuve un accidente', 'Cancelar'];
    var ID_1086016081 = Ti.UI.createOptionDialog({
        title: 'RAZON PARA CANCELAR INSPECCION ACTUAL',
        options: ID_1086016081_opts
    });
    ID_1086016081.addEventListener('click', function(e) {
        var resp = ID_1086016081_opts[e.index];
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var razon = "";
            if (resp == 'Asegurado no puede seguir') {
                razon = "Asegurado no puede seguir";
            }
            if (resp == 'Se me acabo la bateria') {
                razon = "Se me acabo la bateria";
            }
            if (resp == 'Tuve un accidente') {
                razon = "Tuve un accidente";
            }
            if (Ti.App.deployType != 'production') console.log('mi razon es', {
                "datos": razon
            });
            if ((_.isObject(razon) || (_.isString(razon)) && !_.isEmpty(razon)) || _.isNumber(razon) || _.isBoolean(razon)) {
                if (Ti.App.deployType != 'production') console.log('llamando servicio cancelarTarea', {});
                require('vars')['insp_cancelada'] = 'siniestro';
                var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
                var ID_1024291951_visible = true;

                if (ID_1024291951_visible == 'si') {
                    ID_1024291951_visible = true;
                } else if (ID_1024291951_visible == 'no') {
                    ID_1024291951_visible = false;
                }
                $.ID_1024291951.setVisible(ID_1024291951_visible);

                var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
                var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
                if (Ti.App.deployType != 'production') console.log('detalle de seltarea', {
                    "data": seltarea
                });
                var datos = {
                    id_inspector: inspector.id_server,
                    codigo_identificador: inspector.codigo_identificador,
                    id_server: seltarea.id_server,
                    num_caso: seltarea.num_caso,
                    razon: razon
                };
                require('vars')[_var_scopekey]['datos'] = datos;
                var ID_1901063634 = {};
                ID_1901063634.success = function(e) {
                    var elemento = e,
                        valor = e;
                    var ID_1024291951_visible = false;

                    if (ID_1024291951_visible == 'si') {
                        ID_1024291951_visible = true;
                    } else if (ID_1024291951_visible == 'no') {
                        ID_1024291951_visible = false;
                    }
                    $.ID_1024291951.setVisible(ID_1024291951_visible);

                    Alloy.createController("firma_index", {}).getView().open();
                    var ID_2070647751_func = function() {
                        Alloy.Events.trigger('_cerrar_insp', {
                            pantalla: 'contenidos'
                        });
                    };
                    var ID_2070647751 = setTimeout(ID_2070647751_func, 1000 * 0.5);
                    elemento = null, valor = null;
                };
                ID_1901063634.error = function(e) {
                    var elemento = e,
                        valor = e;
                    if (Ti.App.deployType != 'production') console.log('hubo error llamando cancelarTarea, la agregamos a cola de salida', {
                        "elemento": elemento
                    });
                    if (Ti.App.deployType != 'production') console.log('agregando servicio cancelarTarea a cola', {});
                    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
                    var datos = ('datos' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['datos'] : '';
                    var ID_1644205866_m = Alloy.Collections.cola;
                    var ID_1644205866_fila = Alloy.createModel('cola', {
                        data: JSON.stringify(datos),
                        id_tarea: seltarea.id_server,
                        tipo: 'cancelar'
                    });
                    ID_1644205866_m.add(ID_1644205866_fila);
                    ID_1644205866_fila.save();
                    _.defer(function() {});
                    var ID_1024291951_visible = false;

                    if (ID_1024291951_visible == 'si') {
                        ID_1024291951_visible = true;
                    } else if (ID_1024291951_visible == 'no') {
                        ID_1024291951_visible = false;
                    }
                    $.ID_1024291951.setVisible(ID_1024291951_visible);

                    Alloy.createController("firma_index", {}).getView().open();
                    var ID_918462859_func = function() {
                        Alloy.Events.trigger('_cerrar_insp', {
                            pantalla: 'contenidos'
                        });
                    };
                    var ID_918462859 = setTimeout(ID_918462859_func, 1000 * 0.5);
                    elemento = null, valor = null;
                };
                require('helper').ajaxUnico('ID_1901063634', '' + url_server + 'cancelarTarea' + '', 'POST', {
                    id_inspector: seltarea.id_inspector,
                    codigo_identificador: inspector.codigo_identificador,
                    id_tarea: seltarea.id_server,
                    num_caso: seltarea.num_caso,
                    mensaje: razon,
                    opcion: 0,
                    tipo: 1
                }, 15000, ID_1901063634);
            }
        }
        resp = null;
        e.source.removeEventListener("click", arguments.callee);
    });
    ID_1086016081.show();

}

function Agregar_dano_ID_1287182980(e) {

    var evento = e;
    /** 
     * Ocupamos esta variable para impedir que la pantalla siguente se cargue dos veces 
     */
    var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
    if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
        require('vars')['var_abriendo'] = 'true';
        Alloy.createController("nuevocontenido", {}).getView().open();
    }

}

function Swipe_ID_1417149214(e) {

    e.cancelBubble = true;
    var elemento = e.section.getItemAt(e.itemIndex);
    var findVariables = require('fvariables');
    var fila = {};
    _.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
        _.each(obj_id, function(valor, prop) {
            fila[prop] = elemento[id_field][prop];
            var llaves = findVariables(valor, '{', '}');
            _.each(llaves, function(llave) {
                fila[llave] = elemento[id_field][prop];
            });
        });
    });
    if (Ti.App.deployType != 'production') console.log('en normal', {
        "e": e
    });
    if (e.direction == 'left') {
        var findVariables = require('fvariables');
        elemento.template = 'pborrar';
        _.each(_list_templates['pborrar'], function(obj_id, id_field) {
            _.each(obj_id, function(valor, prop) {
                var llaves = findVariables(valor, '{', '}');
                _.each(llaves, function(llave) {
                    elemento[id_field] = {};
                    elemento[id_field][prop] = fila[llave];
                });
            });
        });
        if (OS_IOS) {
            e.section.updateItemAt(e.itemIndex, elemento, {
                animated: true
            });
        } else if (OS_ANDROID) {
            e.section.updateItemAt(e.itemIndex, elemento);
        }
    }

}

function Click_ID_172215600(e) {

    e.cancelBubble = true;
    var elemento = e.section.getItemAt(e.itemIndex);
    var findVariables = require('fvariables');
    var fila = {};
    _.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
        _.each(obj_id, function(valor, prop) {
            fila[prop] = elemento[id_field][prop];
            var llaves = findVariables(valor, '{', '}');
            _.each(llaves, function(llave) {
                fila[llave] = elemento[id_field][prop];
            });
        });
    });
    /** 
     * Ocupamos esta variable para impedir que la pantalla siguente se cargue dos veces 
     */
    var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
    if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
        require('vars')['var_abriendo'] = 'true';
        /** 
         * Enviamos a editar contenido, y pasamos parametro _dato para enviarle el id del contenido a editar 
         */
        Alloy.createController("editarcontenido", {
            '_dato': fila
        }).getView().open();
    }

}

function Click_ID_1552898141(e) {

    e.cancelBubble = true;
    var elemento = e.section.getItemAt(e.itemIndex);
    var findVariables = require('fvariables');
    var fila = {};
    _.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
        _.each(obj_id, function(valor, prop) {
            fila[prop] = elemento[id_field][prop];
            var llaves = findVariables(valor, '{', '}');
            _.each(llaves, function(llave) {
                fila[llave] = elemento[id_field][prop];
            });
        });
    });
    if (Ti.App.deployType != 'production') console.log('click en borrar', {
        "e": e
    });
    var ID_1440860877_opts = ['Si', 'No'];
    var ID_1440860877 = Ti.UI.createAlertDialog({
        title: 'ALERTA',
        message: '' + '¿ Seguro desea eliminar: ' + fila.nombre + ' ?' + '',
        buttonNames: ID_1440860877_opts
    });
    ID_1440860877.addEventListener('click', function(e) {
        var xd = ID_1440860877_opts[e.index];
        if (xd == 'Si') {
            if (Ti.App.deployType != 'production') console.log('borrando xd', {});
            var ID_1666567712_i = Alloy.Collections.insp_contenido;
            var sql = 'DELETE FROM ' + ID_1666567712_i.config.adapter.collection_name + ' WHERE id=\'' + fila.id + '\'';
            var db = Ti.Database.open(ID_1666567712_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_1666567712_i.trigger('delete');
            _.defer(function() {
                Alloy.Collections.insp_contenido.fetch();
            });
        }
        xd = null;
        e.source.removeEventListener("click", arguments.callee);
    });
    ID_1440860877.show();

}

function Swipe_ID_2080575849(e) {

    e.cancelBubble = true;
    var elemento = e.section.getItemAt(e.itemIndex);
    var findVariables = require('fvariables');
    var fila = {};
    _.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
        _.each(obj_id, function(valor, prop) {
            fila[prop] = elemento[id_field][prop];
            var llaves = findVariables(valor, '{', '}');
            _.each(llaves, function(llave) {
                fila[llave] = elemento[id_field][prop];
            });
        });
    });
    if (Ti.App.deployType != 'production') console.log('en borrar', {
        "e": e
    });
    if (e.direction == 'right') {
        if (Ti.App.deployType != 'production') console.log('swipe hacia derecha (desde borrar)', {});
        var findVariables = require('fvariables');
        elemento.template = 'contenido';
        _.each(_list_templates['contenido'], function(obj_id, id_field) {
            _.each(obj_id, function(valor, prop) {
                var llaves = findVariables(valor, '{', '}');
                _.each(llaves, function(llave) {
                    elemento[id_field] = {};
                    elemento[id_field][prop] = fila[llave];
                });
            });
        });
        if (OS_IOS) {
            e.section.updateItemAt(e.itemIndex, elemento, {
                animated: true
            });
        } else if (OS_ANDROID) {
            e.section.updateItemAt(e.itemIndex, elemento);
        }
    }

}

function Click_ID_1063890189(e) {

    e.cancelBubble = true;
    var elemento = e.section.getItemAt(e.itemIndex);
    var findVariables = require('fvariables');
    var fila = {};
    _.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
        _.each(obj_id, function(valor, prop) {
            fila[prop] = elemento[id_field][prop];
            var llaves = findVariables(valor, '{', '}');
            _.each(llaves, function(llave) {
                fila[llave] = elemento[id_field][prop];
            });
        });
    });
    if (Ti.App.deployType != 'production') console.log('en borrar', {
        "e": e
    });
    var findVariables = require('fvariables');
    elemento.template = 'contenido';
    _.each(_list_templates['contenido'], function(obj_id, id_field) {
        _.each(obj_id, function(valor, prop) {
            var llaves = findVariables(valor, '{', '}');
            _.each(llaves, function(llave) {
                elemento[id_field] = {};
                elemento[id_field][prop] = fila[llave];
            });
        });
    });
    if (OS_IOS) {
        e.section.updateItemAt(e.itemIndex, elemento, {
            animated: true
        });
    } else if (OS_ANDROID) {
        e.section.updateItemAt(e.itemIndex, elemento);
    }

}

$.ID_2012598288.init({
    titulo: 'GUARDAR DAÑOS',
    __id: 'ALL2012598288',
    color: 'verde',
    onclick: Click_ID_381969225
});

function Click_ID_381969225(e) {

    var evento = e;
    /** 
     * Abrimos pantalla esta de acuerdo 
     */
    $.ID_1516180472.abrir({
        color: 'celeste'
    });
    /** 
     * Mostramos animacion de progreso 
     */
    $.ID_2012598288.iniciar_progreso({});

}

$.ID_1516180472.init({
    titulo: '¿EL ASEGURADO CONFIRMA ESTOS DATOS?',
    __id: 'ALL1516180472',
    si: 'SI, Están correctos',
    texto: 'El asegurado debe confirmar que los datos de esta sección están correctos',
    pantalla: '¿ESTA DE ACUERDO?',
    onno: no_ID_1134351985,
    color: 'morado',
    onsi: si_ID_1427022722,
    no: 'NO, Hay que modificar algo'
});

function si_ID_1427022722(e) {

    var evento = e;
    /** 
     * Ocupamos esta variable para impedir que la pantalla siguente se cargue dos veces 
     */
    var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
    if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
        require('vars')['var_abriendo'] = 'true';
        /** 
         * Ocultamos animacion de progreso 
         */
        $.ID_2012598288.detener_progreso({});
        Alloy.createController("vistaweb_index", {}).getView().open();
    }

}

function no_ID_1134351985(e) {

    var evento = e;
    var ID_1820551755_func = function() {
        /** 
         * Detenemos el progreso en el boton 
         */
        $.ID_2012598288.detener_progreso({});
    };
    var ID_1820551755 = setTimeout(ID_1820551755_func, 1000 * 0.5);

}

function Postlayout_ID_1761135583(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    $.ID_1972859178.show();

}

function Postlayout_ID_1639140718(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    /** 
     * Limpieza memoria ram 
     */
    Alloy.Events.trigger('_cerrar_insp', {
        pantalla: 'recintos'
    });
    var ID_538989377_func = function() {
        /** 
         * Una vez ya cargada la tarea, dejamos la variable limpia para poder abrir otras pantallas 
         */
        require('vars')['var_abriendo'] = '';
    };
    var ID_538989377 = setTimeout(ID_538989377_func, 1000 * 0.2);

}

_my_events['_cerrar_insp,ID_1448429619'] = function(evento) {
    if (!_.isUndefined(evento.pantalla)) {
        if (evento.pantalla == 'contenidos') {
            if (Ti.App.deployType != 'production') console.log('debug cerrando contenidos', {});
            var ID_1366511412_trycatch = {
                error: function(e) {
                    if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
                }
            };
            try {
                ID_1366511412_trycatch.error = function(evento) {
                    if (Ti.App.deployType != 'production') console.log('error cerrando contenidos', {});
                };
                $.ID_1058387122.close();
            } catch (e) {
                ID_1366511412_trycatch.error(e);
            }
        }
    } else {
        if (Ti.App.deployType != 'production') console.log('debug cerrando (todas) contenidos', {});
        var ID_1739640864_trycatch = {
            error: function(e) {
                if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
            }
        };
        try {
            ID_1739640864_trycatch.error = function(evento) {
                if (Ti.App.deployType != 'production') console.log('error cerrando contenidos', {});
            };
            $.ID_1058387122.close();
        } catch (e) {
            ID_1739640864_trycatch.error(e);
        }
    }
};
Alloy.Events.on('_cerrar_insp', _my_events['_cerrar_insp,ID_1448429619']);
var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
    /** 
     * restringimos contenidos mostrados a la inspeccion actual (por si los temporales aun tienen datos) 
     */
    ID_2014823490_filter = function(coll) {
        var filtered = coll.filter(function(m) {
            var _tests = [],
                _all_true = false,
                model = m.toJSON();
            _tests.push((model.id_inspeccion == seltarea.id_server));
            var _all_true_s = _.uniq(_tests);
            _all_true = (_all_true_s.length == 1 && _all_true_s[0] == true) ? true : false;
            return _all_true;
        });
        filtered = _.toArray(filtered);
        return filtered;
    };
    _.defer(function() {
        Alloy.Collections.insp_contenido.fetch();
    });
} else {
    /** 
     * Borramos dummies 
     */
    /** 
     * Borramos dummies 
     */
    var ID_325451425_i = Alloy.Collections.insp_contenido;
    var sql = "DELETE FROM " + ID_325451425_i.config.adapter.collection_name;
    var db = Ti.Database.open(ID_325451425_i.config.adapter.db_name);
    db.execute(sql);
    db.close();
    sql = null;
    db = null;
    ID_325451425_i.trigger('delete');
    _.defer(function() {
        Alloy.Collections.insp_contenido.fetch();
    });
}

(function() {
    /** 
     * Modificamos el statusbar 
     */
    var ID_1061396234_func = function() {
        var ID_1058387122_statusbar = '#239EC4';

        var setearStatusColor = function(ID_1058387122_statusbar) {
            if (OS_IOS) {
                if (ID_1058387122_statusbar == 'light' || ID_1058387122_statusbar == 'claro') {
                    $.ID_1058387122_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.LIGHT_CONTENT);
                } else if (ID_1058387122_statusbar == 'grey' || ID_1058387122_statusbar == 'gris' || ID_1058387122_statusbar == 'gray') {
                    $.ID_1058387122_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.GREY);
                } else if (ID_1058387122_statusbar == 'oscuro' || ID_1058387122_statusbar == 'dark') {
                    $.ID_1058387122_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.DEFAULT);
                }
            } else if (OS_ANDROID) {
                abx.setStatusbarColor(ID_1058387122_statusbar);
            }
        };
        setearStatusColor(ID_1058387122_statusbar);

    };
    var ID_1061396234 = setTimeout(ID_1061396234_func, 1000 * 0.1);
})();

function Androidback_ID_1748911183(e) {

    e.cancelBubble = true;
    var elemento = e.source;

}
if (OS_IOS || OS_ANDROID) {
    $.ID_1058387122.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}
//$.ID_1058387122.open();