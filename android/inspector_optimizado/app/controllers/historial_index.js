var _bind4section = {};
var _list_templates = {
    "tarea_historia": {
        "ID_1680574104": {
            "text": "{comuna}"
        },
        "ID_1304747472": {},
        "ID_676600495": {},
        "ID_1147033032": {},
        "ID_1810186930": {
            "text": "{hora_termino}"
        },
        "ID_911340427": {},
        "ID_1680180472": {
            "visible": "{bt_enviartarea}"
        },
        "ID_452289613": {},
        "ID_930474704": {
            "text": "{direccion}"
        },
        "ID_428063592": {
            "idlocal": "{id}",
            "estado": "{estado_tarea}"
        },
        "ID_1404040740": {},
        "ID_1184366498": {},
        "ID_459591478": {
            "text": "{ciudad}, {pais}"
        },
        "ID_362564056": {
            "visible": "{enviando_tarea}"
        },
        "ID_165401298": {},
        "ID_1987088054": {},
        "ID_500305974": {},
        "ID_124513836": {},
        "ID_404048727": {}
    }
};

var _activity;
if (OS_ANDROID) {
    _activity = $.ID_1579519092.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_1579519092';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_1579519092.addEventListener('open', function(e) {});
}
$.ID_1579519092.orientationModes = [Titanium.UI.PORTRAIT];


$.ID_385503646.init({
    titulo: 'HISTORIAL',
    __id: 'ALL385503646',
    fondo: 'fondoblanco',
    enviar_insp: '',
    top: 0,
    colortitulo: 'negro',
    onpresiono: Presiono_ID_1888999051
});

function Presiono_ID_1888999051(e) {

    var evento = e;
    /* Mostramos popup */
    var ID_8660388_visible = true;

    if (ID_8660388_visible == 'si') {
        ID_8660388_visible = true;
    } else if (ID_8660388_visible == 'no') {
        ID_8660388_visible = false;
    }
    $.ID_8660388.setVisible(ID_8660388_visible);

    require('vars')['enviando_inspecciones'] = 'true';
    require('vars')[_var_scopekey]['enviar_id'] = '';
    /* Ocultamos enviar todos */
    $.ID_385503646.esconder_envio({});

}

function Load_ID_1338547280(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var evento = e;
    /* Elemento es el objeto que llamo el evento, como es virtual no hay referencia de nodo para un modificar */
    elemento.start();
}

function Itemclick_ID_795143143(e) {

    e.cancelBubble = true;
    var objeto = e.section.getItemAt(e.itemIndex);
    var modelo = {},
        _modelo = [];
    var fila = {},
        fila_bak = {},
        info = {
            _template: objeto.template,
            _what: [],
            _seccion_ref: e.section.getHeaderTitle(),
            _model_id: -1
        },
        _tmp = {
            objmap: {}
        };
    if ('itemId' in e) {
        info._model_id = e.itemId;
        modelo._id = info._model_id;
        if (info._seccion_ref != '' && info._seccion_ref in _bind4section) {
            modelo._collection = _bind4section[info._seccion_ref];
            _tmp._coll = modelo._collection;
        }
    }
    if (info._seccion_ref != '' && info._seccion_ref in _bind4section) {
        if (Alloy.Collections[_tmp._coll].config.adapter.type == 'sql') {
            _tmp._inst = Alloy.Collections[_tmp._coll];
            _tmp._id = Alloy.Collections[_tmp._coll].config.adapter.idAttribute;
            _tmp._dbsql = 'SELECT * FROM ' + Alloy.Collections[_tmp._coll].config.adapter.collection_name + ' WHERE ' + _tmp._id + ' = ' + e.itemId;
            _tmp._db = Ti.Database.open(Alloy.Collections[_tmp._coll].config.adapter.db_name);
            _modelo = _tmp._db.execute(_tmp._dbsql);
            var values = [],
                fieldNames = [];
            var fieldCount = _.isFunction(_modelo.fieldCount) ? _modelo.fieldCount() : _modelo.fieldCount;
            var getField = _modelo.field;
            var i = 0;
            for (; fieldCount > i; i++) fieldNames.push(_modelo.fieldName(i));
            while (_modelo.isValidRow()) {
                var o = {};
                for (i = 0; fieldCount > i; i++) o[fieldNames[i]] = getField(i);
                values.push(o);
                _modelo.next();
            }
            _modelo = values;
            _tmp._db.close();
        } else {
            _tmp._search = {};
            _tmp._search[_tmp._id] = e.itemId + '';
            _modelo = Alloy.Collections[_tmp._coll].fetch(_tmp._search);
        }
    }
    var findVariables = require('fvariables');
    _.each(_list_templates[info._template], function(obj_id, id) {
        _.each(obj_id, function(valor, prop) {
            var llaves = findVariables(valor, '{', '}');
            _.each(llaves, function(llave) {
                _tmp.objmap[llave] = {
                    id: id,
                    prop: prop
                };
                fila[llave] = objeto[id][prop];
                if (id == e.bindId) info._what.push(llave);
            });
        });
    });
    info._what = info._what.join(',');
    fila_bak = JSON.parse(JSON.stringify(fila));
    if (fila.bt_enviartarea == true || fila.bt_enviartarea == 'true') {
        /* Solo ejecutamos si esta el boton */
        var fila = _.extend(fila, {
            enviando_tarea: true,
            bt_enviartarea: false
        });
        require('vars')[_var_scopekey]['enviar_id'] = fila.id;
        if (Ti.App.deployType != 'production') console.log('fila tarea por enviar pinchada', {
            "fila": fila
        });
        /* Modificamos registro en tabla por enviando tarea */
        var ID_356907424_i = Alloy.createCollection('historial_tareas');
        var ID_356907424_i_where = 'id=\'' + fila.id + '\'';
        ID_356907424_i.fetch({
            query: 'SELECT * FROM historial_tareas WHERE id=\'' + fila.id + '\''
        });
        var tareaclick = require('helper').query2array(ID_356907424_i);
        var db = Ti.Database.open(ID_356907424_i.config.adapter.db_name);
        if (ID_356907424_i_where == '') {
            var sql = 'UPDATE ' + ID_356907424_i.config.adapter.collection_name + ' SET estado_envio=1';
        } else {
            var sql = 'UPDATE ' + ID_356907424_i.config.adapter.collection_name + ' SET estado_envio=1 WHERE ' + ID_356907424_i_where;
        }
        db.execute(sql);
        db.close();
        $.ID_1344963939.setText('No cierre la aplicacion y asegurese de estar conectado a internet\nProgreso 0 %');
        require('vars')['enviando_inspecciones'] = 'true';
        /* Mostramos popup */
        var ID_8660388_visible = true;

        if (ID_8660388_visible == 'si') {
            ID_8660388_visible = true;
        } else if (ID_8660388_visible == 'no') {
            ID_8660388_visible = false;
        }
        $.ID_8660388.setVisible(ID_8660388_visible);

        /* Ocultamos enviar todos */
        $.ID_385503646.esconder_envio({});
    }
    _tmp.changed = false;
    _tmp.diff_keys = [];
    _.each(fila, function(value1, prop) {
        var had_samekey = false;
        _.each(fila_bak, function(value2, prop2) {
            if (prop == prop2 && value1 == value2) {
                had_samekey = true;
            } else if (!_.has(fila_bak, prop) || !_.has(fila, prop2)) {
                has_samekey = true;
            }
        });
        if (!had_samekey) _tmp.diff_keys.push(prop);
    });
    if (_tmp.diff_keys.length > 0) _tmp.changed = true;
    if (_tmp.changed == true) {
        _.each(_tmp.diff_keys, function(llave) {
            objeto[_tmp.objmap[llave].id][_tmp.objmap[llave].prop] = fila[llave];
        });
        console.log('actualizando estado de item:', objeto);
        e.section.updateItemAt(e.itemIndex, objeto);
    }

}

function Load_ID_1020345449(e) {
    /* Evento que se ejecuta una vez cargada la vista */
    e.cancelBubble = true;
    var elemento = e.source;
    var evento = e;
    /* Hace que la animacion se ejecute */
    elemento.start();

}

function Longpress_ID_1959077469(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var ID_757532626_opts = ['Aceptar', 'Cancelar'];
    var ID_757532626 = Ti.UI.createAlertDialog({
        title: 'Abortar envio',
        message: '¿ Esta seguro que desea cancelar los envios ?',
        buttonNames: ID_757532626_opts
    });
    ID_757532626.addEventListener('click', function(e) {
        var seguro = ID_757532626_opts[e.index];
        if (seguro == 'Aceptar') {
            require('vars')[_var_scopekey]['pidio_cancelar'] = 'true';
            require('vars')['enviando_inspecciones'] = 'false';
            var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
            /* Desactivamos estado_envio de todas las tareas de este inspector */
            var ID_198256704_i = Alloy.createCollection('historial_tareas');
            var ID_198256704_i_where = 'id_inspector=\'' + inspector.id_server + '\'';
            ID_198256704_i.fetch({
                query: 'SELECT * FROM historial_tareas WHERE id_inspector=\'' + inspector.id_server + '\''
            });
            var cancelar_envios = require('helper').query2array(ID_198256704_i);
            var db = Ti.Database.open(ID_198256704_i.config.adapter.db_name);
            if (ID_198256704_i_where == '') {
                var sql = 'UPDATE ' + ID_198256704_i.config.adapter.collection_name + ' SET estado_envio=0';
            } else {
                var sql = 'UPDATE ' + ID_198256704_i.config.adapter.collection_name + ' SET estado_envio=0 WHERE ' + ID_198256704_i_where;
            }
            db.execute(sql);
            db.close();
            var ID_1279375049 = null;
            if ('refrescar_tareas' in require('funciones')) {
                ID_1279375049 = require('funciones').refrescar_tareas({});
            } else {
                try {
                    ID_1279375049 = f_refrescar_tareas({});
                } catch (ee) {}
            }
            /* Ocultamos popup */
            var ID_8660388_visible = false;

            if (ID_8660388_visible == 'si') {
                ID_8660388_visible = true;
            } else if (ID_8660388_visible == 'no') {
                ID_8660388_visible = false;
            }
            $.ID_8660388.setVisible(ID_8660388_visible);

            /* Mostramos enviar todos */
            $.ID_385503646.mostrar_envio({});
        }
        seguro = null;

        e.source.removeEventListener("click", arguments.callee);
    });
    ID_757532626.show();

}

var f_refrescar_tareas = function(x_params) {
    var xyz = x_params['xyz'];
    /* Limpiamos la tabla de tareas */
    var ID_513906812_borrar = false;

    var limpiarListado = function(animar) {
        var a_nimar = (typeof animar == 'undefined') ? false : animar;
        if (OS_IOS && a_nimar == true) {
            var s_ecciones = $.ID_513906812.getSections();
            _.each(s_ecciones, function(obj_id, pos) {
                $.ID_513906812.deleteSectionAt(0, {
                    animated: true
                });
            });
        } else {
            $.ID_513906812.setSections([]);
        }
    };
    limpiarListado(ID_513906812_borrar);

    var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
    /* Consultamos esta tabla para ir ingresando las tareas que estan pendientes de envio, por orden de fecha descendiente */
    var ID_962190970_i = Alloy.createCollection('historial_tareas');
    var ID_962190970_i_where = 'ORDER BY FECHA_TERMINO DESC';
    ID_962190970_i.fetch({
        query: 'SELECT * FROM historial_tareas ORDER BY FECHA_TERMINO DESC'
    });
    var tareas = require('helper').query2array(ID_962190970_i);
    /* Inicializamos la variable ultima_fecha en vacio, esto servira por si encontramos una tarea en la misma fecha y la podremos guardar en la misma seccion que la tarea anterior (si fueron realizadas el mismo dia) */
    require('vars')[_var_scopekey]['ultima_fecha'] = '';
    var tarea_index = 0;
    _.each(tareas, function(tarea, tarea_pos, tarea_list) {
        tarea_index += 1;
        /* Recuperamos la variable para ver si la tarea anterior tiene la misma fecha que la tarea que estamos recorriendo */
        var ultima_fecha = ('ultima_fecha' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['ultima_fecha'] : '';
        /* Formateamos fecha para mostrar en pantalla */
        var moment = require('alloy/moment');
        var fecha_hoy = moment(new Date()).format('YYYY-MM-DD');
        /* Creamos variable ayer con la fecha del dia de ayer (segun la que tenga el smartphone) */
        var ayer = new Date();
        ayer.setDate(ayer.getDate() - 1);;
        /* Formateamos la fecha de ayer para mostrar en pantalla */
        var moment = require('alloy/moment');
        var ID_451415992 = ayer;
        var fecha_ayer = moment(ID_451415992).format('YYYY-MM-DD');
        if (ultima_fecha != tarea.fecha_termino) {
            /* Revisamos la fecha de la tarea, si es la misma que la anterior, no nos cambiamos de seccion, caso contrario vemos de que dia es y dejamos esa tarea en la fecha que corresponde */
            if (tarea.fecha_termino == fecha_hoy) {
                var ID_124478889 = Titanium.UI.createListSection({
                    headerTitle: 'hoy'
                });
                var ID_1443297420 = Titanium.UI.createView({
                    height: '25dp',
                    width: Ti.UI.FILL
                });
                var ID_1826389456 = Titanium.UI.createView({
                    height: '25dp',
                    layout: 'vertical',
                    width: Ti.UI.FILL,
                    backgroundColor: '#F7F7F7'
                });
                var ID_1087737808 = Titanium.UI.createLabel({
                    text: 'HOY',
                    color: '#999999',
                    touchEnabled: false,
                    font: {
                        fontFamily: 'Roboto-Medium',
                        fontSize: '14dp'
                    }

                });
                ID_1826389456.add(ID_1087737808);

                ID_1443297420.add(ID_1826389456);
                ID_124478889.setHeaderView(ID_1443297420);
                $.ID_513906812.appendSection(ID_124478889);
                require('vars')[_var_scopekey]['ultima_fecha'] = tarea.fecha_termino;
            } else if (tarea.fecha_termino == fecha_ayer) {
                var ID_1416248162 = Titanium.UI.createListSection({
                    headerTitle: 'ayer'
                });
                var ID_1163960984 = Titanium.UI.createView({
                    height: '25dp',
                    width: Ti.UI.FILL
                });
                var ID_1344907100 = Titanium.UI.createView({
                    height: '25dp',
                    layout: 'vertical',
                    width: Ti.UI.FILL,
                    backgroundColor: '#F7F7F7'
                });
                var ID_1196300264 = Titanium.UI.createLabel({
                    text: 'AYER',
                    color: '#999999',
                    touchEnabled: false,
                    font: {
                        fontFamily: 'Roboto-Medium',
                        fontSize: '14dp'
                    }

                });
                ID_1344907100.add(ID_1196300264);

                ID_1163960984.add(ID_1344907100);
                ID_1416248162.setHeaderView(ID_1163960984);
                $.ID_513906812.appendSection(ID_1416248162);
                require('vars')[_var_scopekey]['ultima_fecha'] = tarea.fecha_termino;
            } else {
                var ID_712317737 = Titanium.UI.createListSection({
                    headerTitle: 'otra'
                });
                var ID_1488272693 = Titanium.UI.createView({
                    height: '25dp',
                    width: Ti.UI.FILL
                });
                var ID_834266152 = Titanium.UI.createView({
                    height: '25dp',
                    layout: 'vertical',
                    width: Ti.UI.FILL,
                    backgroundColor: '#F7F7F7'
                });
                var ID_1911156043 = Titanium.UI.createLabel({
                    text: tarea.fecha_termino,
                    color: '#999999',
                    touchEnabled: false,
                    font: {
                        fontFamily: 'Roboto-Medium',
                        fontSize: '14dp'
                    }

                });
                ID_834266152.add(ID_1911156043);

                ID_1488272693.add(ID_834266152);
                ID_712317737.setHeaderView(ID_1488272693);
                $.ID_513906812.appendSection(ID_712317737);
                require('vars')[_var_scopekey]['ultima_fecha'] = tarea.fecha_termino;
            }
        }
        if ((_.isObject(tarea.nivel_2) || _.isString(tarea.nivel_2)) && _.isEmpty(tarea.nivel_2)) {
            /* Define ultimo nivel de la tarea */
            var tarea = _.extend(tarea, {
                ultimo_nivel: tarea.nivel_1
            });
        } else if ((_.isObject(tarea.nivel_3) || _.isString(tarea.nivel_3)) && _.isEmpty(tarea.nivel_3)) {
            var tarea = _.extend(tarea, {
                ultimo_nivel: tarea.nivel_2
            });
        } else if ((_.isObject(tarea.nivel_4) || _.isString(tarea.nivel_4)) && _.isEmpty(tarea.nivel_4)) {
            var tarea = _.extend(tarea, {
                ultimo_nivel: tarea.nivel_3
            });
        } else if ((_.isObject(tarea.nivel_5) || _.isString(tarea.nivel_5)) && _.isEmpty(tarea.nivel_5)) {
            var tarea = _.extend(tarea, {
                ultimo_nivel: tarea.nivel_4
            });
        } else {
            var tarea = _.extend(tarea, {
                ultimo_nivel: tarea.nivel_5
            });
        } /* Formateamos hora_termino de hh:mm:ss a HH:MM hrs en la variable de tarea */
        var hora_t = tarea.hora_termino.split(':');
        /* Actualizamos la variable de la tarea con el estado de bt_enviartarea, enviando_tarea y la hora de termino */
        var tarea = _.extend(tarea, {
            bt_enviartarea: false,
            enviando_tarea: false,
            hora_termino: hora_t[0] + ':' + hora_t[1] + 'hrs'
        });
        if (tarea.estado_tarea == 8) {
            /* Revisamos el estado de la tarea */
            /* Revisamos que el directorio de la inspeccion exista en el telefono y tenga archivos */
            var tarea = _.extend(tarea, {
                bt_enviartarea: true,
                enviando_tarea: false
            });
        } else if (tarea.estado_tarea == 9) {
            /* Revisamos que el directorio de la inspeccion exista en el telefono y tenga archivos */
            var tarea = _.extend(tarea, {
                bt_enviartarea: true,
                enviando_tarea: false
            });
        }
        if (tarea.bt_enviartarea == true || tarea.bt_enviartarea == 'true') {
            if (tarea.estado_envio == 0 || tarea.estado_envio == '0') {
                /* Revisamos que el equipo tenga imagenes guardadas para la tarea actual */
                var fotos = [];
                var ID_1988302164_f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + tarea.id_server + '/');
                if (ID_1988302164_f.exists() == true) {
                    fotos = ID_1988302164_f.getDirectoryListing();
                }
                if (Ti.App.deployType != 'production') console.log('probando si hay fotos para id ' + tarea.id_server, {
                    "fotos": fotos
                });
                if (fotos && fotos.length) {
                    /* Cambiamos el estado de bt_enviartarea y enviando_tarea */
                    var tarea = _.extend(tarea, {
                        bt_enviartarea: true,
                        enviando_tarea: false
                    });
                } else {
                    var tarea = _.extend(tarea, {
                        bt_enviartarea: false,
                        enviando_tarea: false
                    });
                }
            } else if (tarea.estado_envio == 1 || tarea.estado_envio == '1') {
                /* Cambiamos el estado de bt_enviartarea y enviando_tarea */
                var tarea = _.extend(tarea, {
                    bt_enviartarea: false,
                    enviando_tarea: true
                });
            } else {
                /* Caso estado:null */
                /* Revisamos que el equipo tenga imagenes guardadas para la tarea actual */
                var fotos = [];
                var ID_516777710_f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + tarea.id_server + '/');
                if (ID_516777710_f.exists() == true) {
                    fotos = ID_516777710_f.getDirectoryListing();
                }
                if (Ti.App.deployType != 'production') console.log('probando si hay fotos para id ' + tarea.id_server, {
                    "fotos": fotos
                });
                if (fotos && fotos.length) {
                    /* Cambiamos el estado de bt_enviartarea y enviando_tarea */
                    var tarea = _.extend(tarea, {
                        bt_enviartarea: true,
                        enviando_tarea: false
                    });
                } else {
                    var tarea = _.extend(tarea, {
                        bt_enviartarea: false,
                        enviando_tarea: false
                    });
                }
            }
        }
        if (tarea.fecha_termino == fecha_hoy) {
            /* Agregamos items tareas a la seccion de las tareas del dia de hoy, ayer u otra fecha */
            var ID_1606804930 = [{
                ID_1680574104: {
                    text: tarea.ultimo_nivel
                },
                ID_1304747472: {},
                ID_676600495: {},
                ID_1147033032: {},
                ID_1810186930: {
                    text: tarea.hora_termino
                },
                ID_911340427: {},
                ID_1680180472: {
                    visible: tarea.bt_enviartarea
                },
                ID_452289613: {},
                ID_930474704: {
                    text: tarea.direccion
                },
                ID_428063592: {
                    idlocal: tarea.id_server,
                    estado: tarea.estado_tarea
                },
                ID_1404040740: {},
                template: 'tarea_historia',
                ID_1184366498: {},
                ID_459591478: {
                    text: tarea.nivel_2 + ', ' + tarea.pais
                },
                ID_362564056: {
                    visible: tarea.enviando_tarea
                },
                ID_165401298: {},
                ID_1987088054: {},
                ID_500305974: {},
                ID_124513836: {},
                ID_404048727: {}

            }];
            var ID_1606804930_secs = {};
            _.map($.ID_513906812.getSections(), function(ID_1606804930_valor, ID_1606804930_indice) {
                ID_1606804930_secs[ID_1606804930_valor.getHeaderTitle()] = ID_1606804930_indice;
                return ID_1606804930_valor;
            });
            if ('hoy' in ID_1606804930_secs) {
                $.ID_513906812.sections[ID_1606804930_secs['hoy']].appendItems(ID_1606804930);
            } else {
                console.log('DEBUG:dynamic listItem points to a invalid section id: check attribute _seccion.');
            }
        } else if (tarea.fecha_termino == fecha_ayer) {
            var ID_1576501282 = [{
                ID_1680574104: {
                    text: tarea.ultimo_nivel
                },
                ID_1304747472: {},
                ID_676600495: {},
                ID_1147033032: {},
                ID_1810186930: {
                    text: tarea.hora_termino
                },
                ID_911340427: {},
                ID_1680180472: {
                    visible: tarea.bt_enviartarea
                },
                ID_452289613: {},
                ID_930474704: {
                    text: tarea.direccion
                },
                ID_428063592: {
                    idlocal: tarea.id_server,
                    estado: tarea.estado_tarea
                },
                ID_1404040740: {},
                template: 'tarea_historia',
                ID_1184366498: {},
                ID_459591478: {
                    text: tarea.nivel_2 + ', ' + tarea.pais
                },
                ID_362564056: {
                    visible: tarea.enviando_tarea
                },
                ID_165401298: {},
                ID_1987088054: {},
                ID_500305974: {},
                ID_124513836: {},
                ID_404048727: {}

            }];
            var ID_1576501282_secs = {};
            _.map($.ID_513906812.getSections(), function(ID_1576501282_valor, ID_1576501282_indice) {
                ID_1576501282_secs[ID_1576501282_valor.getHeaderTitle()] = ID_1576501282_indice;
                return ID_1576501282_valor;
            });
            if ('ayer' in ID_1576501282_secs) {
                $.ID_513906812.sections[ID_1576501282_secs['ayer']].appendItems(ID_1576501282);
            } else {
                console.log('DEBUG:dynamic listItem points to a invalid section id: check attribute _seccion.');
            }
        } else {
            var ID_402653296 = [{
                ID_1680574104: {
                    text: tarea.ultimo_nivel
                },
                ID_1304747472: {},
                ID_676600495: {},
                ID_1147033032: {},
                ID_1810186930: {
                    text: tarea.hora_termino
                },
                ID_911340427: {},
                ID_1680180472: {
                    visible: tarea.bt_enviartarea
                },
                ID_452289613: {},
                ID_930474704: {
                    text: tarea.direccion
                },
                ID_428063592: {
                    idlocal: tarea.id_server,
                    estado: tarea.estado_tarea
                },
                ID_1404040740: {},
                template: 'tarea_historia',
                ID_1184366498: {},
                ID_459591478: {
                    text: tarea.nivel_2 + ', ' + tarea.pais
                },
                ID_362564056: {
                    visible: tarea.enviando_tarea
                },
                ID_165401298: {},
                ID_1987088054: {},
                ID_500305974: {},
                ID_124513836: {},
                ID_404048727: {}

            }];
            var ID_402653296_secs = {};
            _.map($.ID_513906812.getSections(), function(ID_402653296_valor, ID_402653296_indice) {
                ID_402653296_secs[ID_402653296_valor.getHeaderTitle()] = ID_402653296_indice;
                return ID_402653296_valor;
            });
            if ('otra' in ID_402653296_secs) {
                $.ID_513906812.sections[ID_402653296_secs['otra']].appendItems(ID_402653296);
            } else {
                console.log('DEBUG:dynamic listItem points to a invalid section id: check attribute _seccion.');
            }
        } /* Limpiamos memoria */
        fechahoy = null, ayer = null, fecha_ayer = null, hora_t = null;
    });
    /* Limpiar memoria */
    tarea = null, tareas = null;
    return null;
};
var ciclo = 0;
var ID_875505225_continuar = true;
_out_vars['ID_875505225'] = {
    _remove: ["clearTimeout(_out_vars['ID_875505225']._run)"] 
};
var qfoto_index = 0;
var ep_enviarFoto = function(fotos, enviar_id, url_server) {
    qfoto = fotos[qfoto_index];
    var enviar_id = ('enviar_id' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['enviar_id'] : '';
    var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
    console.log('psb intentando enviar con desfase foto ' + qfoto_index + ' con datos', qfoto);
    var porce = Math.round(((qfoto_index) * 100) / fotos.length);
    $.ID_1344963939.setText('No cierre la aplicacion y asegurese de estar conectado a internet\nProgreso ' + porce + ' %');
    qfoto_index += 1;
    if (Ti.App.deployType != 'production') console.log('leyendo binario', {});
    /* Leemos la imagen y la almacenamos en variable */
    var firmabin = '';
    var ID_502219582_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + enviar_id);
    if (ID_502219582_d.exists() == true) {
        var ID_502219582_f = Ti.Filesystem.getFile(ID_502219582_d.resolve(), qfoto);
        if (ID_502219582_f.exists() == true) {
            firmabin = ID_502219582_f.read();
        }
        ID_502219582_f = null;
    }
    ID_502219582_d = null;
    if (Ti.App.deployType != 'production') console.log('enviando a servidor', {});
    /* Hacemos consulta al servidor mandando las fotos de la inspeccion */
    var resp = null;
    console.log('DEBUG WEB: requesting url:' + url_server + 'subirImagenes' + ' with data:', {
        _method: 'POST',
        _params: {
            id_tarea: enviar_id,
            archivo: qfoto,
            imagen: firmabin,
            app_id: qfoto + ',' + enviar_id
        },
        _timeout: '20000'
    });
    require('helper').ajaxUnico(qfoto + ',' + enviar_id, '' + url_server + 'subirImagenes' + '', 'POST', {
        id_tarea: enviar_id,
        archivo: qfoto,
        imagen: firmabin,
        app_id: qfoto + ',' + enviar_id
    }, 600000, {
        abort: function(elid) {
            console.log('psb se esta solicitando cancelar el envio del id ' + elid + ', porque aun no llega su exito previo, y re-llamando');
            return true;
        },
        success: function(x9) {
            resp = x9;
            console.log('psb respuesta bruta de subirImagenes recibida', resp);
            if (resp.error != 0 && resp.error != '0') {
                /* Si el servidor responde con error */
                if (Ti.App.deployType != 'production') console.log('fallo envio de imagen (en servidor)', {
                    "qfoto": qfoto,
                    "resp": resp
                });
                /* Recuperamos variable */
                var enviar_id = ('enviar_id' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['enviar_id'] : '';
                var pendientes = ('pendientes' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['pendientes'] : '';
                if (resp.mensaje.toLowerCase().indexOf('insertar la imagen'.toLowerCase()) != -1 ||
                    resp.mensaje.toLowerCase().indexOf('imagen no existe'.toLowerCase()) != -1) {
                    /* Revisamos la respuesta del servidor */
                    console.log('psb recibido error (ya existente o no registrada) al subirImagenes (enviar_id=' + enviar_id + '), borrando de equipo y marcando como enviada.');
                    /* imagen ya existe en servidor, borramos de equipo y marcamos como enviada */
                    var info = resp.app_id.split(',');
                    /* Borramos archivo */
                    var ID_1001674794_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + info[1]);
                    var ID_1001674794_f = Ti.Filesystem.getFile(ID_1001674794_d.resolve(), info[0]);
                    if (ID_1001674794_f.exists() == true) ID_1001674794_f.deleteFile();
                    if (Ti.App.deployType != 'production') console.log('foto enviada borrada de equipo', {
                        "info": info
                    });
                    /* Modificamos la tarea actualizando el estado de envio */
                    var ID_1936135963_i = Alloy.createCollection('historial_tareas');
                    var ID_1936135963_i_where = 'id=\'' + enviar_id + '\'';
                    ID_1936135963_i.fetch({
                        query: 'SELECT * FROM historial_tareas WHERE id=\'' + enviar_id + '\''
                    });
                    var tareaclick = require('helper').query2array(ID_1936135963_i);
                    var db = Ti.Database.open(ID_1936135963_i.config.adapter.db_name);
                    if (ID_1936135963_i_where == '') {
                        var sql = 'UPDATE ' + ID_1936135963_i.config.adapter.collection_name + ' SET estado_envio=0';
                    } else {
                        var sql = 'UPDATE ' + ID_1936135963_i.config.adapter.collection_name + ' SET estado_envio=0 WHERE ' + ID_1936135963_i_where;
                    }
                    db.execute(sql);
                    if (ID_1936135963_i_where == '') {
                        var sql = 'UPDATE ' + ID_1936135963_i.config.adapter.collection_name + ' SET estado_tarea=10';
                    } else {
                        var sql = 'UPDATE ' + ID_1936135963_i.config.adapter.collection_name + ' SET estado_tarea=10 WHERE ' + ID_1936135963_i_where;
                    }
                    db.execute(sql);
                    db.close();
                } else {
                    /* otro error, modificamos la tarea actualizando el estado de envio */
                    var ID_1283904867_i = Alloy.createCollection('historial_tareas');
                    var ID_1283904867_i_where = 'id=\'' + enviar_id + '\'';
                    ID_1283904867_i.fetch({
                        query: 'SELECT * FROM historial_tareas WHERE id=\'' + enviar_id + '\''
                    });
                    var tareaclick = require('helper').query2array(ID_1283904867_i);
                    var db = Ti.Database.open(ID_1283904867_i.config.adapter.db_name);
                    if (ID_1283904867_i_where == '') {
                        var sql = 'UPDATE ' + ID_1283904867_i.config.adapter.collection_name + ' SET estado_envio=0';
                    } else {
                        var sql = 'UPDATE ' + ID_1283904867_i.config.adapter.collection_name + ' SET estado_envio=0 WHERE ' + ID_1283904867_i_where;
                    }
                    db.execute(sql);
                    db.close();
                    // borra enviar_id (tarea) cuya foto fallo.
                    require('vars')[_var_scopekey]['enviar_id'] = '';
                }
                if ((_.has(pendientes, enviar_id)) == true || (_.has(pendientes, enviar_id)) == 'true') {
                    /* Revisamos que el objeto pendientes contenga la propiedad enviar_id, si contiene, borramos la propiedad enviar_id del objeto y actualizamos */
                    /* Eliminamos la propiedad enviar_id del objeto pendientes y actualizamos la variable */
                    delete pendientes[enviar_id];
                    require('vars')[_var_scopekey]['pendientes'] = pendientes;
                }
                var pendientes = ('pendientes' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['pendientes'] : '';
                if ((Object.keys(pendientes).length) == 0 || (Object.keys(pendientes).length) == '0') {
                    /* Revisamos si el objeto tiene propiedades */
                    /* Tratamos de cancelar envio y quitamos popup y refrescamos listado */
                    require('vars')['enviando_inspecciones'] = 'false';
                    var ID_1867246625 = null;
                    if ('refrescar_tareas' in require('funciones')) {
                        ID_1867246625 = require('funciones').refrescar_tareas({});
                    } else {
                        try {
                            ID_1867246625 = f_refrescar_tareas({});
                        } catch (ee) {}
                    }
                    /* Ocultamos popup */
                    var ID_8660388_visible = false;

                    if (ID_8660388_visible == 'si') {
                        ID_8660388_visible = true;
                    } else if (ID_8660388_visible == 'no') {
                        ID_8660388_visible = false;
                    }
                    $.ID_8660388.setVisible(ID_8660388_visible);

                    /* Mostramos boton enviar todos */
                    $.ID_385503646.mostrar_envio({});
                    /* rervisamos si no queda ninguna imagen por enviar (desactivamos enviar_todos) */
                    var faltan = false;
                    /* 27-feb esto se podria mejorar, agregando soporte para filtrar por estado_tarea = 8,9 y contar los registros. */
                    var ID_1390575608_i = Alloy.createCollection('historial_tareas');
                    var ID_1390575608_i_where = '';
                    ID_1390575608_i.fetch();
                    var tareastest = require('helper').query2array(ID_1390575608_i);
                    var test_index = 0;
                    _.each(tareastest, function(test, test_pos, test_list) {
                        test_index += 1;
                        try {
                            if (require('helper').arr_contains('8,9'.toLowerCase().split(','), test.estado_tarea.toString().toLowerCase())) {
                                /* Revisamos que el equipo tenga imagenes guardadas para la tarea actual y actualizamos la variable faltan */
                                var fotos = [];
                                var ID_668109677_f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + test.id_server + '/');
                                if (ID_668109677_f.exists() == true) {
                                    fotos = ID_668109677_f.getDirectoryListing();
                                }

                                if (fotos && fotos.length == 0) {
                                    faltan = true;
                                }
                            }
                        } catch (err) {
                            //Block of code to handle errors
                        }

                    });
                    if (faltan == false || faltan == 'false') {
                        /* Ocultamos boton enviar todos */
                        $.ID_385503646.esconder_envio({});
                        if (Ti.App.deployType != 'production') console.log('esconder en la consulta web', {});
                    }
                    /* esto es para refrescar badge de pantalla perfil */
                    Alloy.Events.trigger('refrescar_historial');
                }
            } else if (_.has(resp, "app_id")) {
                var pendientes = ('pendientes' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['pendientes'] : '';
                var info = resp.app_id.split(',');
                var ID_1203936130_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + info[1]);
                var ID_1203936130_f = Ti.Filesystem.getFile(ID_1203936130_d.resolve(), info[0]);
                if (ID_1203936130_f.exists() == true) ID_1203936130_f.deleteFile();
                if (Ti.App.deployType != 'production') console.log('foto enviada borrada de equipo', {
                    "info": info
                });
                var testmas = [];
                var ID_1036625800_f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + info[1] + '/');
                if (ID_1036625800_f.exists() == true) {
                    testmas = ID_1036625800_f.getDirectoryListing();
                }
                var ID_985199740_i = Alloy.createCollection('historial_tareas');
                var ID_985199740_i_where = 'id=\'' + info[1] + '\'';
                ID_985199740_i.fetch({
                    query: 'SELECT * FROM historial_tareas WHERE id=\'' + info[1] + '\''
                });
                var tareaclick = require('helper').query2array(ID_985199740_i);
                var db = Ti.Database.open(ID_985199740_i.config.adapter.db_name);
                if (ID_985199740_i_where == '') {
                    var sql = 'UPDATE ' + ID_985199740_i.config.adapter.collection_name + ' SET estado_envio=0';
                } else {
                    var sql = 'UPDATE ' + ID_985199740_i.config.adapter.collection_name + ' SET estado_envio=0 WHERE ' + ID_985199740_i_where;
                }
                db.execute(sql);
                if (ID_985199740_i_where == '') {
                    var sql = 'UPDATE ' + ID_985199740_i.config.adapter.collection_name + ' SET estado_tarea=10';
                } else {
                    var sql = 'UPDATE ' + ID_985199740_i.config.adapter.collection_name + ' SET estado_tarea=10 WHERE ' + ID_985199740_i_where;
                }
                db.execute(sql);
                db.close();
                info = null;
                if ((_.has(pendientes, enviar_id)) == true || (_.has(pendientes, enviar_id)) == 'true') {
                    delete pendientes[enviar_id];
                    require('vars')[_var_scopekey]['pendientes'] = pendientes;
                }
                var pendientes = ('pendientes' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['pendientes'] : '';
                if ((Object.keys(pendientes).length) == 0 || (Object.keys(pendientes).length) == '0') {
                    /* Revisamos que pendientes no tenga mas registros pendientes */
                    if (testmas && testmas.length == 0) {
                        if (Ti.App.deployType != 'production') console.log('no hay mas imagenes para tarea activa, desactivamos popup', {});
                        /* Recuperamos variables */
                        require('vars')[_var_scopekey]['enviar_id'] = '';
                        require('vars')['enviando_inspecciones'] = 'false';
                        /* Refrescamos la lista de tareas */
                        var ID_1985286020 = null;
                        if ('refrescar_tareas' in require('funciones')) {
                            ID_1985286020 = require('funciones').refrescar_tareas({});
                        } else {
                            try {
                                ID_1985286020 = f_refrescar_tareas({});
                            } catch (ee) {}
                        }
                        /* Ocultamos popup */
                        var ID_8660388_visible = false;

                        if (ID_8660388_visible == 'si') {
                            ID_8660388_visible = true;
                        } else if (ID_8660388_visible == 'no') {
                            ID_8660388_visible = false;
                        }
                        $.ID_8660388.setVisible(ID_8660388_visible);

                        /* Mostramos boton enviar todos */
                        $.ID_385503646.mostrar_envio({});
                        /* Revisamos si no queda ninguna imagen por enviar (desactivamos enviar_todos) */
                        var faltan = false;
                        /* Consultamos la tabla de historial tareas, revisamos que cada item tenga el estado_tarea en 8 o 9, revisamos que exista un directorio con la tarea. Esto es cuando mandamos todas las inspecciones pendientes */
                        var ID_828104227_i = Alloy.createCollection('historial_tareas');
                        var ID_828104227_i_where = '';
                        ID_828104227_i.fetch();
                        var tareastest = require('helper').query2array(ID_828104227_i);
                        var test_index = 0;
                        _.each(tareastest, function(test, test_pos, test_list) {
                            test_index += 1;

                            try {
                                if (require('helper').arr_contains('8,9'.toLowerCase().split(','), test.estado_tarea.toString().toLowerCase())) {
                                    /* Revisamos que el equipo tenga imagenes guardadas para la tarea actual */
                                    var fotos = [];
                                    var ID_1873863856_f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + test.id_server + '/');
                                    if (ID_1873863856_f.exists() == true) {
                                        fotos = ID_1873863856_f.getDirectoryListing();
                                    }
                                    if (fotos && fotos.length == 0) {}
                                }
                            } catch (err) {
                                //Block of code to handle errors
                            }

                        });
                        if (faltan == false || faltan == 'false') {
                            /* Ocultamos boton enviar todos */
                            $.ID_385503646.esconder_envio({});
                            if (Ti.App.deployType != 'production') console.log('esconder en la otra consulta web', {});
                        }
                        /* esto es para refrescar badge de pantalla perfil */
                        Alloy.Events.trigger('refrescar_historial');
                    }
                } else {
                    if (testmas && testmas.length == 0) {
                        /* Esta tarea no tiene mas fotos, borramos enviar_id para continuar con pendientes */
                        require('vars')[_var_scopekey]['enviar_id'] = '';
                        var ID_213243253 = null;
                        if ('refrescar_tareas' in require('funciones')) {
                            ID_213243253 = require('funciones').refrescar_tareas({});
                        } else {
                            try {
                                ID_213243253 = f_refrescar_tareas({});
                            } catch (ee) {}
                        }
                    }
                }
            }
            if (fotos.length >= qfoto_index && require('vars')[_var_scopekey]['enviar_id'] != '') {
                var pidio_cancelar = ('pidio_cancelar' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['pidio_cancelar'] : '';
                if (pidio_cancelar != '') {
                    require('vars')[_var_scopekey]['pidio_cancelar'] = '';
                    console.log('PSB se pidio cancelar, por lo que no seguimos enviando imagenes de tarea actual');
                } else {
                    setTimeout(ep_enviarFoto, 500, fotos);
                }
            } else if (require('vars')[_var_scopekey]['enviar_id'] == '') {
                var pidio_cancelar = ('pidio_cancelar' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['pidio_cancelar'] : '';
                if (pidio_cancelar != '') {
                    require('vars')[_var_scopekey]['pidio_cancelar'] = '';
                    console.log('PSB se pidio cancelar, por lo que hacemos nada');
                } else {
                    console.log('PSB enviar_id en blanco, significa que se quiso todas si hay pendientes');
                    var pendientes = ('pendientes' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['pendientes'] : '';
                    if ((Object.keys(pendientes).length) == 0 || (Object.keys(pendientes).length) == '0') {
                        // si no hay nada pendiente y no hay enviar_id ni fotos actuales
                        // escondemos dialogo
                        require('vars')['enviando_inspecciones'] = false;
                        var ID_1867246625 = null;
                        f_refrescar_tareas({});
                        /* Ocultamos popup */
                        $.ID_8660388.setVisible(false);
                        /* esto es para refrescar badge de pantalla perfil */
                        Alloy.Events.trigger('refrescar_historial');
                    } else {
                        console.log('PSB hay pendientes, continuamos intervalo general', pendientes);
                        require('vars')['enviando_inspecciones'] = true; // activamos para que intervalo de chequeo interrumpa prosiga.
                    }
                }
            } else {
                // se acabaron fotos de tarea actual
                console.log('PSB continuando con intervalo normal');
                require('vars')['enviando_inspecciones'] = true; // activamos para que intervalo de chequeo interrumpa prosiga.
            }
        }
    });
    if (Ti.App.deployType != 'production') console.log('debug qfoto', {
        "ciclo": ciclo,
        "qfoto": qfoto
    });
    /* Limpiamos memoria */
    firma64 = null;
};
var ID_875505225_func = function() {
    ciclo = ciclo + 1;
    var enviando_inspecciones = ('enviando_inspecciones' in require('vars')) ? require('vars')['enviando_inspecciones'] : '';
    if (enviando_inspecciones == true || enviando_inspecciones == 'true') {
        if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
            /* Tratamos de enviar imagenes */
            var enviar_id = ('enviar_id' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['enviar_id'] : '';
            if ((_.isObject(enviar_id) || (_.isString(enviar_id)) && !_.isEmpty(enviar_id)) || _.isNumber(enviar_id) || _.isBoolean(enviar_id)) {
                /* si enviar_id esta definido es porque se solicito un envio en particular. */
                /* Revisamos que la inspeccion tenga archivos en el directorio */
                var fotos = [];
                var ID_1233051231_f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + enviar_id + '/');
                if (ID_1233051231_f.exists() == true) {
                    fotos = ID_1233051231_f.getDirectoryListing();
                }
                if (Ti.App.deployType != 'production') console.log('fotos de ' + enviar_id, {
                    "fotos": fotos
                });
                qfoto_index = 0; // reseteamos contador pq array fotos cambio.
                var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
                /* Actualizamos el estado de envio de la inspeccion en la tabla */
                var ID_191960600_i = Alloy.createCollection('historial_tareas');
                var ID_191960600_i_where = 'id=\'' + enviar_id + '\'';
                ID_191960600_i.fetch({
                    query: 'SELECT * FROM historial_tareas WHERE id=\'' + enviar_id + '\''
                });
                var tareaclick = require('helper').query2array(ID_191960600_i);
                var db = Ti.Database.open(ID_191960600_i.config.adapter.db_name);
                if (ID_191960600_i_where == '') {
                    var sql = 'UPDATE ' + ID_191960600_i.config.adapter.collection_name + ' SET estado_envio=1';
                } else {
                    var sql = 'UPDATE ' + ID_191960600_i.config.adapter.collection_name + ' SET estado_envio=1 WHERE ' + ID_191960600_i_where;
                }
                db.execute(sql);
                db.close();
                //require('helper').ajaxReset();
                require('vars')['enviando_inspecciones'] = false; // simulamos false para evitar que intervalo de chequeo interrumpa envio.
                setTimeout(ep_enviarFoto, 500 * 1, fotos);
                /* Llamamos la funcion refrescar_tareas */
                /*var ID_972710997 = null;
                if ('refrescar_tareas' in require('funciones')) {
                    ID_972710997 = require('funciones').refrescar_tareas({});
                } else {
                    try {
                        ID_972710997 = f_refrescar_tareas({});
                    } catch (ee) {}
                }*/
            } else {
                /* si enviar_id no esta definido, se solicito enviar_todas */
                /* Enviamos todas las fotos disponibles */
                var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
                if (Ti.App.deployType != 'production') console.log('enviamos las fotos de todas las inspecciones con fotos', {});
                /* Consultamos la tabla de historial tareas, revisamos que cada item tenga el estado_tarea en 8 o 9, revisamos que exista un directorio con la tarea. Esto es cuando mandamos todas las inspecciones pendientes */
                var ID_940703440_i = Alloy.createCollection('historial_tareas');
                var ID_940703440_i_where = 'ORDER BY FECHA_TERMINO DESC';
                ID_940703440_i.fetch({
                    query: 'SELECT * FROM historial_tareas ORDER BY FECHA_TERMINO DESC'
                });
                var tareas = require('helper').query2array(ID_940703440_i);
                var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
                var lista = {};
                var tarea_index = 0;
                _.each(tareas, function(tarea, tarea_pos, tarea_list) {
                    tarea_index += 1;
                    try {
                        if (require('helper').arr_contains('8,9'.toLowerCase().split(','), tarea.estado_tarea.toString().toLowerCase())) {
                            /* Revisamos que cada tarea tenga un estado_tarea en 8 o 9 */
                            var fotos = [];
                            var ID_792171103_f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + tarea.id_server + '/');
                            if (ID_792171103_f.exists() == true) {
                                fotos = ID_792171103_f.getDirectoryListing();
                            }
                            qfoto_index = 0; // reset foto index pq array fotos cambio.
                            if (fotos && fotos.length) {
                                /* Revisamos que el directorio tenga archivos */
                                /* Actualizamos la variable enviar_id con el id de la tarea que estamos enviando */
                                var enviar_id = ('enviar_id' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['enviar_id'] : '';
                                if ((_.isObject(enviar_id) || _.isString(enviar_id)) && _.isEmpty(enviar_id)) {
                                    require('vars')[_var_scopekey]['enviar_id'] = tarea.id_server;
                                    lista[tarea.id_server] = true;
                                } else {
                                    lista[enviar_id] = true;
                                }
                            }
                        }
                    } catch (err) {
                        //Block of code to handle errors
                    }

                });
                if (Ti.App.deployType != 'production') console.log('detalle de la lista', {
                    "datos": lista
                });
                /* Actualizamos variable pendientes con la lista de tareas */
                require('vars')[_var_scopekey]['pendientes'] = lista;
                if ((Object.keys(lista).length) == 0 || (Object.keys(lista).length) == '0') {
                    /* Revisamos que la variable de lista este vacia */
                    /* Detenemos popup enviando */
                    require('vars')['enviando_inspecciones'] = 'false';
                    /* Limpiamos variable de enviar_id */
                    require('vars')[_var_scopekey]['enviar_id'] = '';
                    /* Llamamos la funcion de refrescar_tareas */
                    var ID_1150953943 = null;
                    if ('refrescar_tareas' in require('funciones')) {
                        ID_1150953943 = require('funciones').refrescar_tareas({});
                    } else {
                        try {
                            ID_1150953943 = f_refrescar_tareas({});
                        } catch (ee) {}
                    }
                    /* Ocultamos popup */
                    var ID_8660388_visible = false;

                    if (ID_8660388_visible == 'si') {
                        ID_8660388_visible = true;
                    } else if (ID_8660388_visible == 'no') {
                        ID_8660388_visible = false;
                    }
                    $.ID_8660388.setVisible(ID_8660388_visible);

                    /* Mostramos boton enviar todos */
                    $.ID_385503646.mostrar_envio({});
                    /* Revisamos si no queda ninguna imagen por enviar (desactivamos enviar_todos) */
                    var faltan = false;
                    /* Consultamos la tabla de historial tareas, revisamos que cada item tenga el estado_tarea en 8 o 9, revisamos que exista un directorio con la tarea */
                    var ID_145864665_i = Alloy.createCollection('historial_tareas');
                    var ID_145864665_i_where = '';
                    ID_145864665_i.fetch();
                    var tareastest = require('helper').query2array(ID_145864665_i);
                    var test_index = 0;
                    _.each(tareastest, function(test, test_pos, test_list) {
                        test_index += 1;

                        try {
                            if (require('helper').arr_contains('8,9'.toLowerCase().split(','), test.estado_tarea.toString().toLowerCase())) {
                                /* Revisamos que el equipo tenga imagenes guardadas para la tarea actual */
                                var fotos = [];
                                var ID_1405073794_f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + test.id_server + '/');
                                if (ID_1405073794_f.exists() == true) {
                                    fotos = ID_1405073794_f.getDirectoryListing();
                                }
                                qfoto_index = 0; // reset foto index pq array fotos cambio.
                                if (fotos && fotos.length == 0) {
                                    /* Si no existen fotos en el equipo, actualizamos la variable de que no hay fotos en el equipo */
                                } else {
                                    require('vars')[_var_scopekey]['faltan'] = true;
                                    faltan = true;
                                }
                            }
                        } catch (err) {
                            //Block of code to handle errors
                        }

                    });
                    /* esto es para refrescar badge de pantalla perfil */
                    Alloy.Events.trigger('refrescar_historial');
                    //
                    if (faltan == false || faltan == 'false') {
                        /* Ocultamos boton enviar todos */
                        $.ID_385503646.esconder_envio({});
                        if (Ti.App.deployType != 'production') console.log('esconder en la consulta web', {});
                    }
                }
            }
        }
    }
    if (ID_875505225_continuar == true) {
        // para continuar con el 'enviar todas' (llama el servicio por cada enviar_id seteado cuando este esta vacio)
        _out_vars['ID_875505225']._run = setTimeout(ID_875505225_func, 1000 * 5);
    }
};
_out_vars['ID_875505225']._run = setTimeout(ID_875505225_func, 1000 * 5);


(function() {
    require('vars')['enviando_inspecciones'] = 'false';
    require('vars')['pendientes'] = {};
    /* Recuperamos variable inspector */
    var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
    if ((_.isObject(inspector) || _.isString(inspector)) && _.isEmpty(inspector)) {
        /* Revisamos que la variable inspector tenga datos, si no hacemos una consulta a la tabla y actualizamos la variable con los datos */
        var ID_1686499574_i = Alloy.createCollection('inspectores');
        var ID_1686499574_i_where = '';
        ID_1686499574_i.fetch();
        var inspector_list = require('helper').query2array(ID_1686499574_i);
        require('vars')['inspector'] = inspector_list[0];
        var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
    }
    require('vars')[_var_scopekey]['faltan'] = false;
    /* Consultamos la tabla de historial tareas, revisamos que cada item tenga el estado_tarea en 8 o 9, revisamos que exista un directorio con la tarea */
    var ID_1979992491_i = Alloy.createCollection('historial_tareas');
    var ID_1979992491_i_where = '';
    ID_1979992491_i.fetch();
    var tareastest = require('helper').query2array(ID_1979992491_i);
    var test_index = 0;
    _.each(tareastest, function(test, test_pos, test_list) {
        test_index += 1;
        try {
            if (require('helper').arr_contains('8,9'.toLowerCase().split(','), test.estado_tarea.toString().toLowerCase())) {
                /* Revisamos que el equipo tenga imagenes guardadas para la tarea actual */
                var fotos = [];
                var ID_287152173_f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + test.id_server + '/');
                if (ID_287152173_f.exists() == true) {
                    fotos = ID_287152173_f.getDirectoryListing();
                }
                if (fotos && fotos.length == 0) {
                    /* Si no existen fotos en el equipo, actualizamos la variable de que no hay fotos en el equipo */
                } else {
                    require('vars')[_var_scopekey]['faltan'] = true;
                }
            }
        } catch (err) {

        }

    });
    var ID_897259323_func = function() {
        var faltan = ('faltan' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['faltan'] : '';
        if (faltan == true || faltan == 'true') {
            /* Revisamos si no queda ninguna imagen por enviar (desactivamos enviar_todos) */
            if (Ti.App.deployType != 'production') console.log('faltan es true', {});
            /* mostramos boton enviar todos */
            $.ID_385503646.mostrar_envio({});
        } else {
            if (Ti.App.deployType != 'production') console.log('faltan es falso', {});
            /* Ocultamos boton enviar todos */
            $.ID_385503646.esconder_envio({});
        }
        var ID_1815814068 = null;
        if ('refrescar_tareas' in require('funciones')) {
            ID_1815814068 = require('funciones').refrescar_tareas({});
        } else {
            try {
                ID_1815814068 = f_refrescar_tareas({});
            } catch (ee) {}
        }
    };
    var ID_897259323 = setTimeout(ID_897259323_func, 1000 * 0.2);
})();

function Androidback_ID_352177078(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    $.ID_1579519092.close();

}

function Postlayout_ID_1156648359(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var ID_1294585966_func = function() {
        /* Una vez ya cargada la tarea, dejamos la variable limpia para poder abrir otras pantallas */
        require('vars')['var_abriendo'] = '';
    };
    var ID_1294585966 = setTimeout(ID_1294585966_func, 1000 * 0.2);

}
if (OS_IOS || OS_ANDROID) {
    $.ID_1579519092.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}
//$.ID_1579519092.open();