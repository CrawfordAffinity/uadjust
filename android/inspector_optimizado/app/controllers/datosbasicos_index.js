var _bind4section = {};
var _list_templates = {
    "elemento": {
        "ID_432412973": {},
        "ID_291370846": {
            "text": "{nombre}"
        },
        "ID_1925487537": {
            "text": "{id_server}"
        },
        "ID_975786586": {
            "text": "{id_segured}"
        },
        "ID_1546190443": {}
    },
    "dano": {
        "ID_1569764493": {
            "text": "{id}"
        },
        "ID_304218376": {},
        "ID_1912165224": {
            "text": "{nombre}"
        }
    },
    "pborrar": {
        "ID_1236907100": {
            "text": "{id}"
        },
        "ID_1506538078": {},
        "ID_2024360383": {},
        "ID_1765564351": {},
        "ID_1699280427": {
            "text": "{nombre}"
        },
        "ID_1445903745": {},
        "ID_1795050796": {},
        "ID_1752901998": {},
        "ID_1914367022": {
            "text": "{nombre}"
        },
        "ID_1249074865": {
            "text": "{id}"
        },
        "ID_1131557960": {},
        "ID_1361874287": {},
        "ID_1750057271": {},
        "ID_1962135207": {},
        "ID_1947166298": {
            "text": "{nombre}"
        },
        "ID_2008605730": {
            "text": "{id}"
        },
        "ID_1788775425": {},
        "ID_1246915111": {}
    },
    "contenido": {
        "ID_630800887": {},
        "ID_862352109": {
            "text": "{id}"
        },
        "ID_560452274": {
            "text": "{nombre}"
        }
    },
    "recinto": {
        "ID_1960582596": {},
        "ID_871581291": {
            "text": "{nombre}"
        },
        "ID_310297848": {
            "text": "{id}"
        }
    },
    "nivel": {
        "ID_1021786928": {
            "text": "{id}"
        },
        "ID_1241655463": {
            "text": "{nombre}"
        },
        "ID_599581692": {}
    }
};
var $datos = $.datos.toJSON();

var _activity;
if (OS_ANDROID) {
    _activity = $.ID_198296088.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_198296088';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_198296088.addEventListener('open', function(e) {});
}
$.ID_198296088.orientationModes = [Titanium.UI.PORTRAIT];


$.ID_138747237.init({
    titulo: 'DATOS BASICOS',
    onsalirinsp: Salirinsp_ID_1311983161,
    __id: 'ALL138747237',
    continuar: '',
    salir_insp: '',
    fondo: 'fondoazul',
    top: 0,
    onpresiono: Presiono_ID_879490664
});

function Salirinsp_ID_1311983161(e) {

    var evento = e;
    var ID_662151190_opts = ['Asegurado no puede seguir', 'Se me acabo la bateria', 'Tuve un accidente', 'Cancelar'];
    var ID_662151190 = Ti.UI.createOptionDialog({
        title: 'RAZON PARA CANCELAR INSPECCION ACTUAL',
        options: ID_662151190_opts
    });
    ID_662151190.addEventListener('click', function(e) {
        var resp = ID_662151190_opts[e.index];
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var razon = "";
            if (resp == 'Asegurado no puede seguir') {
                razon = "Asegurado no puede seguir";
            }
            if (resp == 'Se me acabo la bateria') {
                razon = "Se me acabo la bateria";
            }
            if (resp == 'Tuve un accidente') {
                razon = "Tuve un accidente";
            }
            if (Ti.App.deployType != 'production') console.log('mi razon es', {
                "datos": razon
            });
            if ((_.isObject(razon) || (_.isString(razon)) && !_.isEmpty(razon)) || _.isNumber(razon) || _.isBoolean(razon)) {
                if (Ti.App.deployType != 'production') console.log('llamando servicio cancelarTarea', {});
                require('vars')['insp_cancelada'] = 'siniestro';
                Alloy.Collections[$.datos.config.adapter.collection_name].add($.datos);
                $.datos.save();
                Alloy.Collections[$.datos.config.adapter.collection_name].fetch();
                var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
                var ID_198695387_visible = true;

                if (ID_198695387_visible == 'si') {
                    ID_198695387_visible = true;
                } else if (ID_198695387_visible == 'no') {
                    ID_198695387_visible = false;
                }
                $.ID_198695387.setVisible(ID_198695387_visible);

                var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
                var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
                if (Ti.App.deployType != 'production') console.log('detalle de seltarea', {
                    "data": seltarea
                });
                var datos = {
                    id_inspector: inspector.id_server,
                    codigo_identificador: inspector.codigo_identificador,
                    id_server: seltarea.id_server,
                    num_caso: seltarea.num_caso,
                    razon: razon
                };
                require('vars')[_var_scopekey]['datos'] = datos;
                var ID_1502339310 = {};
                ID_1502339310.success = function(e) {
                    var elemento = e,
                        valor = e;
                    var ID_198695387_visible = false;

                    if (ID_198695387_visible == 'si') {
                        ID_198695387_visible = true;
                    } else if (ID_198695387_visible == 'no') {
                        ID_198695387_visible = false;
                    }
                    $.ID_198695387.setVisible(ID_198695387_visible);

                    Alloy.createController("firma_index", {}).getView().open();
                    var ID_1502479695_func = function() {
                        /** 
                         * Cerramos pantalla datos basicos 
                         */
                        Alloy.Events.trigger('_cerrar_insp', {
                            pantalla: 'basicos'
                        });
                    };
                    var ID_1502479695 = setTimeout(ID_1502479695_func, 1000 * 0.5);
                    elemento = null, valor = null;
                };
                ID_1502339310.error = function(e) {
                    var elemento = e,
                        valor = e;
                    if (Ti.App.deployType != 'production') console.log('hubo error llamando cancelarTarea, la agregamos a cola de salida', {
                        "elemento": elemento
                    });
                    if (Ti.App.deployType != 'production') console.log('agregando servicio cancelarTarea a cola', {});
                    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
                    var datos = ('datos' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['datos'] : '';
                    var ID_62284646_m = Alloy.Collections.cola;
                    var ID_62284646_fila = Alloy.createModel('cola', {
                        data: JSON.stringify(datos),
                        id_tarea: seltarea.id_server,
                        tipo: 'cancelar'
                    });
                    ID_62284646_m.add(ID_62284646_fila);
                    ID_62284646_fila.save();
                    var ID_198695387_visible = false;

                    if (ID_198695387_visible == 'si') {
                        ID_198695387_visible = true;
                    } else if (ID_198695387_visible == 'no') {
                        ID_198695387_visible = false;
                    }
                    $.ID_198695387.setVisible(ID_198695387_visible);

                    Alloy.createController("firma_index", {}).getView().open();
                    /** 
                     * Cerramos pantalla datos basicos 
                     */
                    Alloy.Events.trigger('_cerrar_insp', {
                        pantalla: 'basicos'
                    });
                    elemento = null, valor = null;
                };
                require('helper').ajaxUnico('ID_1502339310', '' + url_server + 'cancelarTarea' + '', 'POST', {
                    id_inspector: seltarea.id_inspector,
                    codigo_identificador: inspector.codigo_identificador,
                    id_tarea: seltarea.id_server,
                    num_caso: seltarea.num_caso,
                    mensaje: razon,
                    opcion: 0,
                    tipo: 1
                }, 15000, ID_1502339310);
            }
        }
        resp = null;
        e.source.removeEventListener("click", arguments.callee);
    });
    ID_662151190.show();

}

function Presiono_ID_879490664(e) {

    var evento = e;
    /** 
     * desenfoca campos de texto para ocultar teclado al presionar en partes blancas. 
     */
    $.ID_1130272126.blur();
    $.ID_1267847770.blur();
    $.ID_494849736.blur();
    $.ID_1712023120.blur();
    $.ID_636596927.blur();
    $.ID_1993726132.blur();
    $.ID_926493919.blur();
    $.ID_301031689.blur();
    var ID_716533446_func = function() {
        require('vars')[_var_scopekey]['todobien'] = 'true';
        if (_.isUndefined($datos.presente_nombre)) {
            require('vars')[_var_scopekey]['todobien'] = 'false';
            var ID_378680658_opts = ['Aceptar'];
            var ID_378680658 = Ti.UI.createAlertDialog({
                title: 'Atención',
                message: 'Ingrese nombre de la persona presente',
                buttonNames: ID_378680658_opts
            });
            ID_378680658.addEventListener('click', function(e) {
                var nulo = ID_378680658_opts[e.index];
                nulo = null;
                e.source.removeEventListener("click", arguments.callee);
            });
            ID_378680658.show();
        } else if ($datos.presente_nombre.length == 0 || $datos.presente_nombre.length == '0') {
            require('vars')[_var_scopekey]['todobien'] = 'false';
            var ID_880914267_opts = ['Aceptar'];
            var ID_880914267 = Ti.UI.createAlertDialog({
                title: 'Atención',
                message: 'Ingrese nombre de la persona presente',
                buttonNames: ID_880914267_opts
            });
            ID_880914267.addEventListener('click', function(e) {
                var nulo = ID_880914267_opts[e.index];
                nulo = null;
                e.source.removeEventListener("click", arguments.callee);
            });
            ID_880914267.show();
        } else if (_.isUndefined($datos.presente_rut)) {
            require('vars')[_var_scopekey]['todobien'] = 'false';
            var ID_1829926154_opts = ['Aceptar'];
            var ID_1829926154 = Ti.UI.createAlertDialog({
                title: 'Atención',
                message: 'Ingrese Rut de la persona presente',
                buttonNames: ID_1829926154_opts
            });
            ID_1829926154.addEventListener('click', function(e) {
                var nulo = ID_1829926154_opts[e.index];
                nulo = null;
                e.source.removeEventListener("click", arguments.callee);
            });
            ID_1829926154.show();
        } else if ($datos.presente_rut.length == 0 || $datos.presente_rut.length == '0') {
            require('vars')[_var_scopekey]['todobien'] = 'false';
            var ID_1135862644_opts = ['Aceptar'];
            var ID_1135862644 = Ti.UI.createAlertDialog({
                title: 'Atención',
                message: 'Ingrese Rut de la persona presente',
                buttonNames: ID_1135862644_opts
            });
            ID_1135862644.addEventListener('click', function(e) {
                var nulo = ID_1135862644_opts[e.index];
                nulo = null;
                e.source.removeEventListener("click", arguments.callee);
            });
            ID_1135862644.show();
        } else if (!_.isUndefined($datos.direccion_correcta)) {
            if ($datos.direccion_correcta == false || $datos.direccion_correcta == 'false') {
                if (_.isUndefined($datos.direccion_riesgo)) {
                    require('vars')[_var_scopekey]['todobien'] = 'false';
                    var ID_176720367_opts = ['Aceptar'];
                    var ID_176720367 = Ti.UI.createAlertDialog({
                        title: 'Atención',
                        message: 'Debe indicar la dirección correcta',
                        buttonNames: ID_176720367_opts
                    });
                    ID_176720367.addEventListener('click', function(e) {
                        var nulo = ID_176720367_opts[e.index];
                        nulo = null;
                        e.source.removeEventListener("click", arguments.callee);
                    });
                    ID_176720367.show();
                } else if (_.isUndefined($datos.direccion_comuna)) {
                    require('vars')[_var_scopekey]['todobien'] = 'false';
                    var ID_584210670_opts = ['Aceptar'];
                    var ID_584210670 = Ti.UI.createAlertDialog({
                        title: 'Atención',
                        message: 'Debe indicar la comuna de la dirección',
                        buttonNames: ID_584210670_opts
                    });
                    ID_584210670.addEventListener('click', function(e) {
                        var nulo = ID_584210670_opts[e.index];
                        nulo = null;
                        e.source.removeEventListener("click", arguments.callee);
                    });
                    ID_584210670.show();
                } else if (_.isUndefined($datos.direccion_ciudad)) {
                    require('vars')[_var_scopekey]['todobien'] = 'false';
                    var ID_1689138946_opts = ['Aceptar'];
                    var ID_1689138946 = Ti.UI.createAlertDialog({
                        title: 'Atención',
                        message: 'Debe indicar la ciudad de la dirección',
                        buttonNames: ID_1689138946_opts
                    });
                    ID_1689138946.addEventListener('click', function(e) {
                        var nulo = ID_1689138946_opts[e.index];
                        nulo = null;
                        e.source.removeEventListener("click", arguments.callee);
                    });
                    ID_1689138946.show();
                }
            }
        }
        var todobien = ('todobien' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['todobien'] : '';
        if (todobien == true || todobien == 'true') {
            $.ID_516906299.abrir({
                color: 'azul'
            });
        }
    };
    var ID_716533446 = setTimeout(ID_716533446_func, 1000 * 0.2);

}

$.ID_516906299.init({
    titulo: '¿EL ASEGURADO CONFIRMA ESTOS DATOS?',
    __id: 'ALL516906299',
    si: 'SI, Están correctos',
    texto: 'El asegurado debe confirmar que los datos de esta sección están correctos',
    pantalla: '¿ESTA DE ACUERDO?',
    onno: no_ID_1621495016,
    color: 'azul',
    onsi: si_ID_587901510,
    no: 'NO, Hay que modificar algo'
});

function no_ID_1621495016(e) {

    var evento = e;

}

function si_ID_587901510(e) {

    var evento = e;
    /** 
     * Ocupamos esta variable para impedir que la pantalla siguente se cargue dos veces 
     */
    var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
    if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
        require('vars')['var_abriendo'] = 'true';
        /** 
         * Guardamos los datos de la tabla en el modelo 
         */
        Alloy.Collections[$.datos.config.adapter.collection_name].add($.datos);
        $.datos.save();
        Alloy.Collections[$.datos.config.adapter.collection_name].fetch();
        var ID_1186276553_func = function() {
            $.ID_198296088.close();
        };
        var ID_1186276553 = setTimeout(ID_1186276553_func, 1000 * 0.5);
        Alloy.createController("caracteristicas_index", {}).getView().open();
    }

}

function Blur_ID_1908333488(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    $.datos.set({
        fono_fijo: elemento
    });
    if ('datos' in $) $datos = $.datos.toJSON();
    elemento = null, source = null;

}

function Blur_ID_1416588784(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    $.datos.set({
        fono_movil: elemento
    });
    if ('datos' in $) $datos = $.datos.toJSON();
    elemento = null, source = null;

}

function Blur_ID_845378117(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    $.datos.set({
        email: elemento
    });
    if ('datos' in $) $datos = $.datos.toJSON();
    elemento = null, source = null;

}

function Blur_ID_838048861(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    $.datos.set({
        presente_nombre: elemento
    });
    if ('datos' in $) $datos = $.datos.toJSON();
    elemento = null, source = null;

}

function Blur_ID_1495593922(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    $.datos.set({
        presente_rut: elemento
    });
    if ('datos' in $) $datos = $.datos.toJSON();
    elemento = null, source = null;

}

function Return_ID_1944768397(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    /** 
     * Cuando el inspector presiona el enter en el teclado, desenfocamos el campo 
     */
    $.ID_636596927.blur();
    elemento = null, source = null;

}

function Change_ID_31855985(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    if (elemento == true || elemento == 'true') {
        $.ID_1632444658.setText('SI');

        /** 
         * Re-activamos campos direccion, comuna, ciudad 
         */
        $.ID_1993726132.setEditable('false');

        $.ID_926493919.setEditable('false');

        $.ID_301031689.setEditable('false');

        require('vars')[_var_scopekey]['direccioncorrecta'] = '1';
    } else {
        $.ID_1632444658.setText('NO');

        /** 
         * Desactivamos campos direccion, comuna, ciudad 
         */
        require('vars')[_var_scopekey]['direccioncorrecta'] = '0';
        $.ID_1993726132.setEditable(true);

        $.ID_926493919.setEditable(true);

        $.ID_301031689.setEditable(true);

    }
    /** 
     * Modificamos el campo direccion_correcta 
     */
    $.datos.set({
        direccion_correcta: elemento
    });
    if ('datos' in $) $datos = $.datos.toJSON();
    elemento = null;

}

function Blur_ID_298318017(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    $.datos.set({
        direccion_riesgo: elemento
    });
    if ('datos' in $) $datos = $.datos.toJSON();
    elemento = null, source = null;

}

function Blur_ID_1898891136(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    $.datos.set({
        direccion_comuna: elemento
    });
    if ('datos' in $) $datos = $.datos.toJSON();
    elemento = null, source = null;

}

function Blur_ID_1478554101(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    $.datos.set({
        direccion_ciudad: elemento
    });
    if ('datos' in $) $datos = $.datos.toJSON();
    elemento = null, source = null;

}

function Click_ID_454606560(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    /** 
     * desenfoca campos de texto para ocultar teclado al presionar en partes blancas. 
     */
    $.ID_1130272126.blur();
    $.ID_1267847770.blur();
    $.ID_494849736.blur();
    $.ID_1712023120.blur();
    $.ID_636596927.blur();
    $.ID_1993726132.blur();
    $.ID_926493919.blur();
    $.ID_301031689.blur();

}

function Postlayout_ID_944831471(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    $.ID_459111696.show();

}

function Postlayout_ID_1924806703(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    /** 
     * Cerramos fotosrequeridas (limpieza memoria) 
     */
    Alloy.Events.trigger('_cerrar_insp', {
        pantalla: 'frequeridas'
    });
    /** 
     * Cerramos pantalla hayalguien 
     */
    Alloy.Events.trigger('_cerrar_insp', {
        pantalla: 'hayalguien'
    });
    var ID_1941683100_func = function() {
        /** 
         * Una vez ya cargada la tarea, dejamos la variable limpia para poder abrir otras pantallas 
         */
        require('vars')['var_abriendo'] = '';
    };
    var ID_1941683100 = setTimeout(ID_1941683100_func, 1000 * 0.2);

}

(function() {
    /** 
     * Recuperamos variable para verificar si tiene datos, siendo asi, cargamos los datos en la tabla de datosbasicos 
     */
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
        var moment = require('alloy/moment');
        var ID_617194935 = seltarea.fecha_siniestro;
        var ID_617194935 = moment(ID_617194935).format('dd-mm-yyyy');
        /** 
         * Seteamos datos a vista 
         */
        $.datos.set({
            id_inspeccion: seltarea.id_server,
            fono_movil: seltarea.asegurado_tel_movil,
            fono_fijo: seltarea.asegurado_tel_fijo,
            direccion_ciudad: seltarea.nivel_2,
            rut_asegurado: seltarea.asegurado_codigo_identificador,
            direccion_riesgo: seltarea.direccion,
            direccion_correcta: true,
            direccion_comuna: seltarea.nivel_3,
            nro_caso: seltarea.num_caso,
            asegurado: seltarea.asegurado_nombre,
            fecha_siniestro: seltarea.fecha_siniestro,
            email: seltarea.asegurado_correo
        });
        if ('datos' in $) $datos = $.datos.toJSON();
        /** 
         * Consultamos fecha/hora inicio de inspeccion de inspecciones 
         */
        var ID_412711721_i = Alloy.createCollection('inspecciones');
        var ID_412711721_i_where = 'id_server=\'' + seltarea.id_server + '\'';
        ID_412711721_i.fetch({
            query: 'SELECT * FROM inspecciones WHERE id_server=\'' + seltarea.id_server + '\''
        });
        var insp = require('helper').query2array(ID_412711721_i);
        if (insp && insp.length) {
            /** 
             * Traspasamos informacion a datosbasicos 
             */
            $.datos.set({
                hora_inspeccion: insp[0].hora,
                fecha_inspeccion: insp[0].fecha,
                fecha_full_inspeccion: insp[0].fecha_inspeccion_inicio
            });
            if ('datos' in $) $datos = $.datos.toJSON();
        } else {
            /** 
             * Formateamos hora/fecha inspeccion 
             */
            var moment = require('alloy/moment');
            var hora = moment(new Date()).format('HH:mm');
            var moment = require('alloy/moment');
            var fecha = moment(new Date()).format('DD-MM-YYYY');
            $.datos.set({
                hora_inspeccion: hora,
                fecha_inspeccion: fecha,
                fecha_full_inspeccion: new Date()
            });
            if ('datos' in $) $datos = $.datos.toJSON();
        }
    } else {
        var fecha_siniestro = "2018-01-24";
        var moment = require('alloy/moment');
        var ID_855802875 = moment(new Date()).format('dd-mm-yyyy');
        /** 
         * Datos dummies para test 
         */
        $.datos.set({
            id_inspeccion: 112233,
            fono_movil: 56999990070,
            fono_fijo: 56280908070,
            direccion_riesgo: 'Av. Las Condes 9460',
            direccion_correcta: true,
            asegurador: 'Falabella',
            nro_caso: 99999,
            asegurado: 'Pepito',
            fecha_siniestro: '17-01-2017',
            email: 'pepito@gmail.com'
        });
        if ('datos' in $) $datos = $.datos.toJSON();
        /** 
         * Formateamos hora/fecha inspeccion 
         */
        var moment = require('alloy/moment');
        var hora = moment(new Date()).format('HH:mm');
        var moment = require('alloy/moment');
        var fecha = moment(new Date()).format('DD-MM-YYYY');
        /** 
         * Traspasamos informacion a datosbasicos 
         */
        $.datos.set({
            hora_inspeccion: hora,
            fecha_inspeccion: fecha,
            fecha_full_inspeccion: new Date()
        });
        if ('datos' in $) $datos = $.datos.toJSON();
    }
    /** 
     * Desenfocamos los campos de texto y cambiamos el color del statusbar 
     */
    var ID_1001548790_func = function() {
        $.ID_146846830.blur();
        var ID_198296088_statusbar = '#006C9B';

        var setearStatusColor = function(ID_198296088_statusbar) {
            if (OS_IOS) {
                if (ID_198296088_statusbar == 'light' || ID_198296088_statusbar == 'claro') {
                    $.ID_198296088_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.LIGHT_CONTENT);
                } else if (ID_198296088_statusbar == 'grey' || ID_198296088_statusbar == 'gris' || ID_198296088_statusbar == 'gray') {
                    $.ID_198296088_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.GREY);
                } else if (ID_198296088_statusbar == 'oscuro' || ID_198296088_statusbar == 'dark') {
                    $.ID_198296088_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.DEFAULT);
                }
            } else if (OS_ANDROID) {
                abx.setStatusbarColor(ID_198296088_statusbar);
            }
        };
        setearStatusColor(ID_198296088_statusbar);

    };
    var ID_1001548790 = setTimeout(ID_1001548790_func, 1000 * 0.1);
})();


/** 
 * Cerramos esta pantalla cuando es llamada en la pantalla siguente (siniestro) 
 */
_my_events['_cerrar_insp,ID_231485652'] = function(evento) {
    if (!_.isUndefined(evento.pantalla)) {
        if (evento.pantalla == 'basicos') {
            if (Ti.App.deployType != 'production') console.log('debug cerrando datosbasicos', {});
            var ID_878785134_trycatch = {
                error: function(e) {
                    if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
                }
            };
            try {
                ID_878785134_trycatch.error = function(evento) {
                    if (Ti.App.deployType != 'production') console.log('error cerrando datosbasicos', {});
                };
                $.ID_198296088.close();
            } catch (e) {
                ID_878785134_trycatch.error(e);
            }
        }
    } else {
        if (Ti.App.deployType != 'production') console.log('debug cerrando (todas) datosbasicos', {});
        var ID_1962200830_trycatch = {
            error: function(e) {
                if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
            }
        };
        try {
            ID_1962200830_trycatch.error = function(evento) {
                if (Ti.App.deployType != 'production') console.log('error cerrando datosbasicos', {});
            };
            $.ID_198296088.close();
        } catch (e) {
            ID_1962200830_trycatch.error(e);
        }
    }
};
Alloy.Events.on('_cerrar_insp', _my_events['_cerrar_insp,ID_231485652']);

function Androidback_ID_1283132292(e) {

    e.cancelBubble = true;
    var elemento = e.source;

}
if (OS_IOS || OS_ANDROID) {
    $.ID_198296088.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}
//$.ID_198296088.open();