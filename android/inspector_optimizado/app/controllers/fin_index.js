var _bind4section = {};
var _list_templates = {};

var _activity;
if (OS_ANDROID) {
    _activity = $.ID_1393871432.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_1393871432';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_1393871432.addEventListener('open', function(e) {});
}
$.ID_1393871432.orientationModes = [Titanium.UI.PORTRAIT];


$.ID_1672616358.init({
    titulo: 'FINALIZADO',
    __id: 'ALL1672616358',
    fondo: 'fondoazul',
    top: 0,
    modal: ''
});


$.ID_395419113.init({
    titulo: '¡FELICIDADES EN TERMINAR CON EXITO TU INSPECCION!',
    __id: 'ALL395419113',
    texto: 'Tu inspección será recibida por nosotros, gracias por tu trabajo.',
    top: 40,
    tipo: '_info'
});


$.ID_1012442242.init({
    titulo: 'SALIR',
    __id: 'ALL1012442242',
    color: 'verde',
    onclick: Click_ID_1575422634
});

function Click_ID_1575422634(e) {

    var evento = e;
    /** 
     * Avisamos que ya no estamos en inspeccion 
     */
    require('vars')['inspeccion_encurso'] = 'false';
    var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
    var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    var ID_1853456445_i = Alloy.Collections.tareas;
    var sql = 'DELETE FROM ' + ID_1853456445_i.config.adapter.collection_name + ' WHERE id_server=\'' + seltarea.id_server + '\'';
    var db = Ti.Database.open(ID_1853456445_i.config.adapter.db_name);
    db.execute(sql);
    db.close();
    ID_1853456445_i.trigger('delete');
    if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
        var ID_1606035638 = {};
        console.log('DEBUG WEB: requesting url:' + url_server + 'obtenerHistorialTareas' + ' with data:', {
            _method: 'POST',
            _params: {
                id_inspector: inspector.id_server
            },
            _timeout: '15000'
        });

        ID_1606035638.success = function(e) {
            var elemento = e,
                valor = e;
            var ID_1380543856_i = Alloy.Collections.historial_tareas;
            var sql = "DELETE FROM " + ID_1380543856_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_1380543856_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_1380543856_i.trigger('delete');
            var elemento_historial_tareas = elemento.historial_tareas;
            var ID_1952135821_m = Alloy.Collections.historial_tareas;
            var db_ID_1952135821 = Ti.Database.open(ID_1952135821_m.config.adapter.db_name);
            db_ID_1952135821.execute('BEGIN');
            _.each(elemento_historial_tareas, function(ID_1952135821_fila, pos) {
                db_ID_1952135821.execute('INSERT INTO historial_tareas (fecha_tarea, id_inspeccion, id_asegurado, nivel_2, comentario_can_o_rech, asegurado_tel_fijo, estado_tarea, bono, evento, id_inspector, asegurado_codigo_identificador, lat, nivel_1, asegurado_nombre, pais, direccion, asegurador, fecha_ingreso, fecha_siniestro, nivel_1_texto, distance, fecha_termino, nivel_4, perfil, asegurado_id, id_server, categoria, nivel_3, asegurado_correo, num_caso, lon, asegurado_tel_movil, nivel_5, tipo_tarea, hora_termino) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', ID_1952135821_fila.fecha_tarea, ID_1952135821_fila.id_inspeccion, ID_1952135821_fila.id_asegurado, ID_1952135821_fila.nivel_2, ID_1952135821_fila.comentario_can_o_rech, ID_1952135821_fila.asegurado_tel_fijo, ID_1952135821_fila.estado_tarea, ID_1952135821_fila.bono, ID_1952135821_fila.evento, ID_1952135821_fila.id_inspector, ID_1952135821_fila.asegurado_codigo_identificador, ID_1952135821_fila.lat, ID_1952135821_fila.nivel_1, ID_1952135821_fila.asegurado_nombre, ID_1952135821_fila.pais, ID_1952135821_fila.direccion, ID_1952135821_fila.asegurador, ID_1952135821_fila.fecha_ingreso, ID_1952135821_fila.fecha_siniestro, ID_1952135821_fila.nivel_1_, ID_1952135821_fila.distancia, ID_1952135821_fila.fecha_finalizacion, ID_1952135821_fila.nivel_4, 'ubicacion', ID_1952135821_fila.asegurado_id, ID_1952135821_fila.id, ID_1952135821_fila.categoria, ID_1952135821_fila.nivel_3, ID_1952135821_fila.asegurado_correo, ID_1952135821_fila.num_caso, ID_1952135821_fila.lon, ID_1952135821_fila.asegurado_tel_movil, ID_1952135821_fila.nivel_5, ID_1952135821_fila.tipo_tarea, ID_1952135821_fila.hora_finalizacion);
            });
            db_ID_1952135821.execute('COMMIT');
            db_ID_1952135821.close();
            db_ID_1952135821 = null;
            ID_1952135821_m.trigger('change');
            /** 
             * Llamo refresco tareas 
             */

            Alloy.Events.trigger('refrescar_historial');
            var ID_983129480_func = function() {
                /** 
                 * Para refrescar mistareas y otras ventanas 
                 */

                Alloy.Events.trigger('_refrescar_tareas_mistareas');
                Alloy.Events.trigger('_cerrar_insp', {
                    pantalla: 'firma'
                });
                /** 
                 * Refrescar pantalla hoy 
                 */

                Alloy.Events.trigger('_calcular_ruta');
                if (Ti.App.deployType != 'production') console.log('ejecutado el delay refescar mistareas', {});
                if (Ti.App.deployType != 'production') console.log('con internet success cerrando pantalla', {});
                /** 
                 * Cerramos pantalla fin 
                 */
                $.ID_1393871432.close();
            };
            var ID_983129480 = setTimeout(ID_983129480_func, 1000 * 0.2);
            elemento = null, valor = null;
        };

        ID_1606035638.error = function(e) {
            var elemento = e,
                valor = e;
            var ID_1938634339_func = function() {
                /** 
                 * Para refrescar mistareas y otras ventanas 
                 */

                Alloy.Events.trigger('_refrescar_tareas_mistareas');
                /** 
                 * Refrescar pantalla hoy 
                 */

                Alloy.Events.trigger('_calcular_ruta');
                if (Ti.App.deployType != 'production') console.log('ejecutado el delay refescar mistareas', {});
                if (Ti.App.deployType != 'production') console.log('con internet error cerrando pantalla', {});
                /** 
                 * Cerramos pantalla fin 
                 */
                $.ID_1393871432.close();
            };
            var ID_1938634339 = setTimeout(ID_1938634339_func, 1000 * 0.2);
            elemento = null, valor = null;
        };
        require('helper').ajaxUnico('ID_1606035638', '' + url_server + 'obtenerHistorialTareas' + '', 'POST', {
            id_inspector: inspector.id_server
        }, 15000, ID_1606035638);
    } else {
        var ID_1195852335_func = function() {
            /** 
             * Para refrescar mistareas y otras ventanas 
             */

            Alloy.Events.trigger('_refrescar_tareas_mistareas');
            /** 
             * Refrescar pantalla hoy 
             */

            Alloy.Events.trigger('_calcular_ruta');
            if (Ti.App.deployType != 'production') console.log('ejecutado el delay refescar mistareas', {});
            if (Ti.App.deployType != 'production') console.log('sin internet cerrando pantalla', {});
            /** 
             * Cerramos pantalla fin 
             */
            $.ID_1393871432.close();
        };
        var ID_1195852335 = setTimeout(ID_1195852335_func, 1000 * 0.2);
    }
}

function Postlayout_ID_1528626781(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    /** 
     * Limpieza memoria 
     */

    Alloy.Events.trigger('_cerrar_insp', {
        pantalla: 'firma'
    });

}

(function() {
    require('vars')['var_abriendo'] = '';
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
        /** 
         * Enviamos datos de inspeccion a servidor 
         */
        /** 
         * Enviamos datos 
         */

        $.ID_1012442242.iniciar_progreso({});
        var consultando_datos = 'status: consultando datos...';
        $.ID_1844477155.setText(consultando_datos);

        /** 
         * Consultamos todas las tablas de la inspeccion 
         */
        var ID_653169010_i = Alloy.createCollection('insp_fotosrequeridas');
        var ID_653169010_i_where = 'id_inspeccion=\'' + seltarea.id_server + '\'';
        ID_653169010_i.fetch({
            query: 'SELECT * FROM insp_fotosrequeridas WHERE id_inspeccion=\'' + seltarea.id_server + '\''
        });
        var fotos = require('helper').query2array(ID_653169010_i);
        var ID_1607527457_i = Alloy.createCollection('insp_datosbasicos');
        var ID_1607527457_i_where = 'id_inspeccion=\'' + seltarea.id_server + '\'';
        ID_1607527457_i.fetch({
            query: 'SELECT * FROM insp_datosbasicos WHERE id_inspeccion=\'' + seltarea.id_server + '\''
        });
        var basicos = require('helper').query2array(ID_1607527457_i);
        var ID_1365975782_i = Alloy.createCollection('insp_caracteristicas');
        var ID_1365975782_i_where = 'id_inspeccion=\'' + seltarea.id_server + '\'';
        ID_1365975782_i.fetch({
            query: 'SELECT * FROM insp_caracteristicas WHERE id_inspeccion=\'' + seltarea.id_server + '\''
        });
        var cara = require('helper').query2array(ID_1365975782_i);
        var ID_1550027652_i = Alloy.createCollection('insp_niveles');
        var ID_1550027652_i_where = 'id_inspeccion=\'' + seltarea.id_server + '\'';
        ID_1550027652_i.fetch({
            query: 'SELECT * FROM insp_niveles WHERE id_inspeccion=\'' + seltarea.id_server + '\''
        });
        var niveles = require('helper').query2array(ID_1550027652_i);
        var ID_435034175_i = Alloy.createCollection('insp_siniestro');
        var ID_435034175_i_where = 'id_inspeccion=\'' + seltarea.id_server + '\'';
        ID_435034175_i.fetch({
            query: 'SELECT * FROM insp_siniestro WHERE id_inspeccion=\'' + seltarea.id_server + '\''
        });
        var siniestro = require('helper').query2array(ID_435034175_i);
        var ID_1559663382_i = Alloy.createCollection('insp_recintos');
        var ID_1559663382_i_where = 'id_inspeccion=\'' + seltarea.id_server + '\'';
        ID_1559663382_i.fetch({
            query: 'SELECT * FROM insp_recintos WHERE id_inspeccion=\'' + seltarea.id_server + '\''
        });
        var recintos = require('helper').query2array(ID_1559663382_i);
        var ID_575881791_i = Alloy.createCollection('insp_itemdanos');
        var ID_575881791_i_where = 'id_inspeccion=\'' + seltarea.id_server + '\'';
        ID_575881791_i.fetch({
            query: 'SELECT * FROM insp_itemdanos WHERE id_inspeccion=\'' + seltarea.id_server + '\''
        });
        var danos = require('helper').query2array(ID_575881791_i);
        var ID_614892672_i = Alloy.createCollection('insp_contenido');
        var ID_614892672_i_where = 'id_inspeccion=\'' + seltarea.id_server + '\'';
        ID_614892672_i.fetch({
            query: 'SELECT * FROM insp_contenido WHERE id_inspeccion=\'' + seltarea.id_server + '\''
        });
        var contenidos = require('helper').query2array(ID_614892672_i);
        var ID_310720857_i = Alloy.createCollection('insp_documentos');
        var ID_310720857_i_where = 'id_inspeccion=\'' + seltarea.id_server + '\'';
        ID_310720857_i.fetch({
            query: 'SELECT * FROM insp_documentos WHERE id_inspeccion=\'' + seltarea.id_server + '\''
        });
        var documentos = require('helper').query2array(ID_310720857_i);
        var ID_1324521257_i = Alloy.createCollection('insp_firma');
        var ID_1324521257_i_where = 'id_inspeccion=\'' + seltarea.id_server + '\'';
        ID_1324521257_i.fetch({
            query: 'SELECT * FROM insp_firma WHERE id_inspeccion=\'' + seltarea.id_server + '\''
        });
        var firma = require('helper').query2array(ID_1324521257_i);
        /** 
         * Actualizamos fecha fin de inspeccion actual 
         */
        var ID_147105424_i = Alloy.createCollection('inspecciones');
        var ID_147105424_i_where = 'id_server=\'' + seltarea.id_server + '\'';
        ID_147105424_i.fetch({
            query: 'SELECT * FROM inspecciones WHERE id_server=\'' + seltarea.id_server + '\''
        });
        var nula = require('helper').query2array(ID_147105424_i);
        var db = Ti.Database.open(ID_147105424_i.config.adapter.db_name);
        if (ID_147105424_i_where == '') {
            var sql = 'UPDATE ' + ID_147105424_i.config.adapter.collection_name + ' SET fecha_inspeccion_fin=\'' + new Date() + '\'';
        } else {
            var sql = 'UPDATE ' + ID_147105424_i.config.adapter.collection_name + ' SET fecha_inspeccion_fin=\'' + new Date() + '\' WHERE ' + ID_147105424_i_where;
        }
        db.execute(sql);
        sql = null;
        db.close();
        db = null;
        var ID_1762904708_i = Alloy.createCollection('inspecciones');
        var ID_1762904708_i_where = 'id_server=\'' + seltarea.id_server + '\'';
        ID_1762904708_i.fetch({
            query: 'SELECT * FROM inspecciones WHERE id_server=\'' + seltarea.id_server + '\''
        });
        var inspecciones = require('helper').query2array(ID_1762904708_i);
        var enviando = 'status: enviando...';
        $.ID_1844477155.setText(enviando);

        var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
        if ((_.isObject(url_server) || (_.isString(url_server)) && !_.isEmpty(url_server)) || _.isNumber(url_server) || _.isBoolean(url_server)) {
            /** 
             * Se crea una estructura con los datos que iban a ser enviados al servidor, notificando que la inspeccion fue cancelada por algun motivo 
             */
            var datos = {
                fotosrequeridas: fotos,
                datosbasicos: basicos,
                caracteristicas: cara,
                niveles: niveles,
                siniestro: siniestro,
                recintos: recintos,
                itemdanos: danos,
                contenido: contenidos,
                documentos: documentos,
                inspecciones: inspecciones,
                firma: firma
            };
            require('vars')[_var_scopekey]['datos'] = datos;
            if (Ti.App.deployType != 'production') console.log('no es un simulador EBH', {});
            if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
                var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
                var ID_1729436480 = {};
                console.log('DEBUG WEB: requesting url:' + url_server + 'finalizarTarea' + ' with JSON data:', {
                    _method: 'POSTJSON',
                    _params: {
                        fotosrequeridas: fotos,
                        datosbasicos: basicos,
                        caracteristicas: cara,
                        niveles: niveles,
                        siniestro: siniestro,
                        recintos: recintos,
                        itemdanos: danos,
                        contenido: contenidos,
                        documentos: documentos,
                        inspecciones: inspecciones,
                        firma: firma,
                        id_app: seltarea.id_server
                    },
                    _timeout: '15000'
                });

                ID_1729436480.success = function(e) {
                    var elemento = e,
                        valor = e;
                    if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
                        /** 
                         * Llamamos funcion enviarFirmas 
                         */
                        var ID_1483271950 = null;
                        if ('enviarfirmas' in require('funciones')) {
                            ID_1483271950 = require('funciones').enviarfirmas({});
                        } else {
                            try {
                                ID_1483271950 = f_enviarfirmas({});
                            } catch (ee) {}
                        }
                    } else {}
                    if (Ti.App.deployType != 'production') console.log('success de finalizar tarea EBH', {});
                    var limp_temporales = 'status: limpiando temporales';
                    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
                    $.ID_1844477155.setText(limp_temporales);

                    var ID_1876659041_i = Alloy.Collections.insp_datosbasicos;
                    var sql = 'DELETE FROM ' + ID_1876659041_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
                    var db = Ti.Database.open(ID_1876659041_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_1876659041_i.trigger('delete');
                    var ID_857284691_i = Alloy.Collections.insp_caracteristicas;
                    var sql = 'DELETE FROM ' + ID_857284691_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
                    var db = Ti.Database.open(ID_857284691_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_857284691_i.trigger('delete');
                    var ID_847124238_i = Alloy.Collections.insp_niveles;
                    var sql = 'DELETE FROM ' + ID_847124238_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
                    var db = Ti.Database.open(ID_847124238_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_847124238_i.trigger('delete');
                    var ID_1576424448_i = Alloy.Collections.insp_siniestro;
                    var sql = 'DELETE FROM ' + ID_1576424448_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
                    var db = Ti.Database.open(ID_1576424448_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_1576424448_i.trigger('delete');
                    var ID_1173498101_i = Alloy.Collections.insp_recintos;
                    var sql = 'DELETE FROM ' + ID_1173498101_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
                    var db = Ti.Database.open(ID_1173498101_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_1173498101_i.trigger('delete');
                    var ID_1380739516_i = Alloy.Collections.insp_itemdanos;
                    var sql = 'DELETE FROM ' + ID_1380739516_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
                    var db = Ti.Database.open(ID_1380739516_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_1380739516_i.trigger('delete');
                    var ID_1602887330_i = Alloy.Collections.insp_contenido;
                    var sql = 'DELETE FROM ' + ID_1602887330_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
                    var db = Ti.Database.open(ID_1602887330_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_1602887330_i.trigger('delete');
                    var ID_1008090978_i = Alloy.Collections.insp_documentos;
                    var sql = 'DELETE FROM ' + ID_1008090978_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
                    var db = Ti.Database.open(ID_1008090978_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_1008090978_i.trigger('delete');
                    var ID_602296366_i = Alloy.Collections.inspecciones;
                    var sql = 'DELETE FROM ' + ID_602296366_i.config.adapter.collection_name + ' WHERE id_server=\'' + seltarea.id_server + '\'';
                    var db = Ti.Database.open(ID_602296366_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_602296366_i.trigger('delete');
                    /** 
                     * Eliminando datos enviados 
                     */
                    /** 
                     * Eliminando datos enviados 
                     */

                    var ID_1116389501_i = Alloy.Collections.insp_fotosrequeridas;
                    var sql = "DELETE FROM " + ID_1116389501_i.config.adapter.collection_name;
                    var db = Ti.Database.open(ID_1116389501_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    sql = null;
                    db = null;
                    ID_1116389501_i.trigger('delete');
                    if (Ti.App.deployType != 'production') console.log('eliminados todas las tareas EBH', {});
                    /** 
                     * Limpieza de memoria 
                     */
                    insp_fotosrequeridas = null, insp_datosbasicos = null, insp_caracteristicas = null, insp_niveles = null, insp_siniestro = null, insp_recintos = null, insp_itemdanos = null, insp_contenido = null, insp_documentos = null, insp_firma = null, inspecciones = null;
                    if (Ti.App.deployType != 'production') console.log('limpieza variables EBH', {});
                    var ID_1546591205_func = function() {
                        if (Ti.App.deployType != 'production') console.log('ejecutado con desface... pero enviado EBH', {});
                        var sta_listo = 'status: listo';
                        $.ID_1844477155.setText(sta_listo);


                        $.ID_1012442242.detener_progreso({});
                    };
                    var ID_1546591205 = setTimeout(ID_1546591205_func, 1000 * 0.5);
                    elemento = null, valor = null;
                };

                ID_1729436480.error = function(e) {
                    var elemento = e,
                        valor = e;
                    if (Ti.App.deployType != 'production') console.log('hubo error enviando inspeccion', {
                        "elemento": elemento
                    });
                    if (Ti.App.deployType != 'production') console.log('agregando servicio finalizarTarea a cola', {});
                    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
                    var datos = ('datos' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['datos'] : '';
                    var ID_1464512910_m = Alloy.Collections.cola;
                    var ID_1464512910_fila = Alloy.createModel('cola', {
                        data: JSON.stringify(datos),
                        id_tarea: seltarea.id_server,
                        tipo: 'enviar'
                    });
                    ID_1464512910_m.add(ID_1464512910_fila);
                    ID_1464512910_fila.save();
                    var ID_701254128_i = Alloy.Collections.insp_datosbasicos;
                    var sql = 'DELETE FROM ' + ID_701254128_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
                    var db = Ti.Database.open(ID_701254128_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_701254128_i.trigger('delete');
                    var ID_375317958_i = Alloy.Collections.insp_caracteristicas;
                    var sql = 'DELETE FROM ' + ID_375317958_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
                    var db = Ti.Database.open(ID_375317958_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_375317958_i.trigger('delete');
                    var ID_686059277_i = Alloy.Collections.insp_niveles;
                    var sql = 'DELETE FROM ' + ID_686059277_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
                    var db = Ti.Database.open(ID_686059277_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_686059277_i.trigger('delete');
                    var ID_1509613562_i = Alloy.Collections.insp_siniestro;
                    var sql = 'DELETE FROM ' + ID_1509613562_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
                    var db = Ti.Database.open(ID_1509613562_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_1509613562_i.trigger('delete');
                    var ID_875757008_i = Alloy.Collections.insp_recintos;
                    var sql = 'DELETE FROM ' + ID_875757008_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
                    var db = Ti.Database.open(ID_875757008_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_875757008_i.trigger('delete');
                    var ID_933984122_i = Alloy.Collections.insp_itemdanos;
                    var sql = 'DELETE FROM ' + ID_933984122_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
                    var db = Ti.Database.open(ID_933984122_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_933984122_i.trigger('delete');
                    var ID_890921391_i = Alloy.Collections.insp_contenido;
                    var sql = 'DELETE FROM ' + ID_890921391_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
                    var db = Ti.Database.open(ID_890921391_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_890921391_i.trigger('delete');
                    var ID_580716423_i = Alloy.Collections.insp_documentos;
                    var sql = 'DELETE FROM ' + ID_580716423_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
                    var db = Ti.Database.open(ID_580716423_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_580716423_i.trigger('delete');
                    var ID_269948483_i = Alloy.Collections.inspecciones;
                    var sql = 'DELETE FROM ' + ID_269948483_i.config.adapter.collection_name + ' WHERE id_server=\'' + seltarea.id_server + '\'';
                    var db = Ti.Database.open(ID_269948483_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    ID_269948483_i.trigger('delete');
                    /** 
                     * Limpieza de memoria 
                     */
                    insp_fotosrequeridas = null, insp_datosbasicos = null, insp_caracteristicas = null, insp_niveles = null, insp_siniestro = null, insp_recintos = null, insp_itemdanos = null, insp_contenido = null, insp_documentos = null, insp_firma = null, inspecciones = null;
                    var ID_1164055122_func = function() {
                        var sta_sininternet = 'status: sin internet, agregado a cola de salida';
                        $.ID_1844477155.setText(sta_sininternet);


                        $.ID_1012442242.detener_progreso({});
                    };
                    var ID_1164055122 = setTimeout(ID_1164055122_func, 1000 * 0.5);
                    elemento = null, valor = null;
                };
                require('helper').ajaxUnico('ID_1729436480', '' + url_server + 'finalizarTarea' + '', 'POSTJSON', {
                    fotosrequeridas: fotos,
                    datosbasicos: basicos,
                    caracteristicas: cara,
                    niveles: niveles,
                    siniestro: siniestro,
                    recintos: recintos,
                    itemdanos: danos,
                    contenido: contenidos,
                    documentos: documentos,
                    inspecciones: inspecciones,
                    firma: firma,
                    id_app: seltarea.id_server
                }, 15000, ID_1729436480);
            } else {
                if (Ti.App.deployType != 'production') console.log('El celular no detecto internet... agregando a la cola de envio', {});
                var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
                var datos = ('datos' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['datos'] : '';
                var ID_459750892_m = Alloy.Collections.cola;
                var ID_459750892_fila = Alloy.createModel('cola', {
                    data: JSON.stringify(datos),
                    id_tarea: seltarea.id_server,
                    tipo: 'enviar'
                });
                ID_459750892_m.add(ID_459750892_fila);
                ID_459750892_fila.save();
                var ID_816473526_i = Alloy.Collections.insp_datosbasicos;
                var sql = 'DELETE FROM ' + ID_816473526_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
                var db = Ti.Database.open(ID_816473526_i.config.adapter.db_name);
                db.execute(sql);
                db.close();
                ID_816473526_i.trigger('delete');
                var ID_1666320191_i = Alloy.Collections.insp_caracteristicas;
                var sql = 'DELETE FROM ' + ID_1666320191_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
                var db = Ti.Database.open(ID_1666320191_i.config.adapter.db_name);
                db.execute(sql);
                db.close();
                ID_1666320191_i.trigger('delete');
                var ID_1306810181_i = Alloy.Collections.insp_niveles;
                var sql = 'DELETE FROM ' + ID_1306810181_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
                var db = Ti.Database.open(ID_1306810181_i.config.adapter.db_name);
                db.execute(sql);
                db.close();
                ID_1306810181_i.trigger('delete');
                var ID_1775946550_i = Alloy.Collections.insp_siniestro;
                var sql = 'DELETE FROM ' + ID_1775946550_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
                var db = Ti.Database.open(ID_1775946550_i.config.adapter.db_name);
                db.execute(sql);
                db.close();
                ID_1775946550_i.trigger('delete');
                var ID_1498318955_i = Alloy.Collections.insp_recintos;
                var sql = 'DELETE FROM ' + ID_1498318955_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
                var db = Ti.Database.open(ID_1498318955_i.config.adapter.db_name);
                db.execute(sql);
                db.close();
                ID_1498318955_i.trigger('delete');
                var ID_1986858185_i = Alloy.Collections.insp_itemdanos;
                var sql = 'DELETE FROM ' + ID_1986858185_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
                var db = Ti.Database.open(ID_1986858185_i.config.adapter.db_name);
                db.execute(sql);
                db.close();
                ID_1986858185_i.trigger('delete');
                var ID_1644471654_i = Alloy.Collections.insp_contenido;
                var sql = 'DELETE FROM ' + ID_1644471654_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
                var db = Ti.Database.open(ID_1644471654_i.config.adapter.db_name);
                db.execute(sql);
                db.close();
                ID_1644471654_i.trigger('delete');
                var ID_1633227210_i = Alloy.Collections.insp_documentos;
                var sql = 'DELETE FROM ' + ID_1633227210_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
                var db = Ti.Database.open(ID_1633227210_i.config.adapter.db_name);
                db.execute(sql);
                db.close();
                ID_1633227210_i.trigger('delete');
                var ID_1743814137_i = Alloy.Collections.inspecciones;
                var sql = 'DELETE FROM ' + ID_1743814137_i.config.adapter.collection_name + ' WHERE id_server=\'' + seltarea.id_server + '\'';
                var db = Ti.Database.open(ID_1743814137_i.config.adapter.db_name);
                db.execute(sql);
                db.close();
                ID_1743814137_i.trigger('delete');
                /** 
                 * Limpieza de memoria 
                 */
                insp_fotosrequeridas = null, insp_datosbasicos = null, insp_caracteristicas = null, insp_niveles = null, insp_siniestro = null, insp_recintos = null, insp_itemdanos = null, insp_contenido = null, insp_documentos = null, insp_firma = null, inspecciones = null;
                var ID_93492704_func = function() {
                    if (Ti.App.deployType != 'production') console.log('ejecute con desface para ver si lo soluciona', {});
                    var sta_sininternet = 'status: sin internet, agregado a cola de salida';
                    $.ID_1844477155.setText(sta_sininternet);


                    $.ID_1012442242.detener_progreso({});
                };
                var ID_93492704 = setTimeout(ID_93492704_func, 1000 * 0.5);
            }
        } else {
            $.ID_1844477155.setText('status: simulador ..');


            $.ID_1012442242.detener_progreso({});
        }
    } else {
        $.ID_1844477155.setText('status: simulador ..');

    }
    var ID_557059512_func = function() {
        var ID_1393871432_statusbar = '#006C9B';

        var setearStatusColor = function(ID_1393871432_statusbar) {
            if (OS_IOS) {
                if (ID_1393871432_statusbar == 'light' || ID_1393871432_statusbar == 'claro') {
                    $.ID_1393871432_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.LIGHT_CONTENT);
                } else if (ID_1393871432_statusbar == 'grey' || ID_1393871432_statusbar == 'gris' || ID_1393871432_statusbar == 'gray') {
                    $.ID_1393871432_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.GREY);
                } else if (ID_1393871432_statusbar == 'oscuro' || ID_1393871432_statusbar == 'dark') {
                    $.ID_1393871432_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.DEFAULT);
                }
            } else if (OS_ANDROID) {
                abx.setStatusbarColor(ID_1393871432_statusbar);
            }
        };
        setearStatusColor(ID_1393871432_statusbar);

    };
    var ID_557059512 = setTimeout(ID_557059512_func, 1000 * 0.1);
})();

if (OS_IOS || OS_ANDROID) {
    $.ID_1393871432.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}
//$.ID_1393871432.open();