var _bind4section = {
    "ref1": "insp_recintos"
};
var _list_templates = {
    "elemento": {
        "ID_432412973": {},
        "ID_291370846": {
            "text": "{nombre}"
        },
        "ID_1925487537": {
            "text": "{id_server}"
        },
        "ID_975786586": {
            "text": "{id_segured}"
        },
        "ID_1546190443": {}
    },
    "dano": {
        "ID_1569764493": {
            "text": "{id}"
        },
        "ID_304218376": {},
        "ID_1912165224": {
            "text": "{nombre}"
        }
    },
    "pborrar": {
        "ID_1236907100": {
            "text": "{id}"
        },
        "ID_1506538078": {},
        "ID_2024360383": {},
        "ID_1765564351": {},
        "ID_1699280427": {
            "text": "{nombre}"
        },
        "ID_1445903745": {},
        "ID_1795050796": {},
        "ID_1752901998": {},
        "ID_1914367022": {
            "text": "{nombre}"
        },
        "ID_1249074865": {
            "text": "{id}"
        },
        "ID_1131557960": {},
        "ID_1361874287": {}
    },
    "contenido": {
        "ID_630800887": {},
        "ID_862352109": {
            "text": "{id}"
        },
        "ID_560452274": {
            "text": "{nombre}"
        }
    },
    "recinto": {
        "ID_1960582596": {},
        "ID_871581291": {
            "text": "{nombre}"
        },
        "ID_310297848": {
            "text": "{id}"
        }
    }
};

var _activity;
if (OS_ANDROID) {
    _activity = $.ID_916746267.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_916746267';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_916746267.addEventListener('open', function(e) {});
}
$.ID_916746267.orientationModes = [Titanium.UI.PORTRAIT];


var ID_553809963_like = function(search) {
    if (typeof search !== 'string' || this === null) {
        return false;
    }
    search = search.replace(new RegExp("([\\.\\\\\\+\\*\\?\\[\\^\\]\\$\\(\\)\\{\\}\\=\\!\\<\\>\\|\\:\\-])", "g"), "\\$1");
    search = search.replace(/%/g, '.*').replace(/_/g, '.');
    return RegExp('^' + search + '$', 'gi').test(this);
};
var ID_553809963_filter = function(coll) {
    var filtered = _.toArray(coll.filter(function(m) {
        return true;
    }));
    return filtered;
};
var ID_553809963_transform = function(model) {
    var fila = model.toJSON();
    return fila;
};
var ID_553809963_update = function(e) {};
_.defer(function() {
    Alloy.Collections.insp_recintos.fetch();
});
Alloy.Collections.insp_recintos.on('add change delete', function(ee) {
    ID_553809963_update(ee);
});
Alloy.Collections.insp_recintos.fetch();


$.ID_1066160090.init({
    titulo: 'RECINTOS',
    onsalirinsp: Salirinsp_ID_95724110,
    __id: 'ALL1066160090',
    salir_insp: '',
    fondo: 'fondomorado',
    top: 0,
    agregar_dano: '',
    onagregar_dano: Agregar_dano_ID_148516318
});

function Salirinsp_ID_95724110(e) {

    var evento = e;
    var ID_2050763722_opts = ['Asegurado no puede seguir', 'Se me acabo la bateria', 'Tuve un accidente', 'Cancelar'];
    var ID_2050763722 = Ti.UI.createOptionDialog({
        title: 'RAZON PARA CANCELAR INSPECCION ACTUAL',
        options: ID_2050763722_opts
    });
    ID_2050763722.addEventListener('click', function(e) {
        var resp = ID_2050763722_opts[e.index];
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var razon = "";
            if (resp == 'Asegurado no puede seguir') {
                razon = "Asegurado no puede seguir";
            }
            if (resp == 'Se me acabo la bateria') {
                razon = "Se me acabo la bateria";
            }
            if (resp == 'Tuve un accidente') {
                razon = "Tuve un accidente";
            }
            if (Ti.App.deployType != 'production') console.log('mi razon es', {
                "datos": razon
            });
            if ((_.isObject(razon) || (_.isString(razon)) && !_.isEmpty(razon)) || _.isNumber(razon) || _.isBoolean(razon)) {
                if (Ti.App.deployType != 'production') console.log('llamando servicio cancelarTarea', {});
                require('vars')['insp_cancelada'] = 'siniestro';
                var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
                var ID_1811386692_visible = true;

                if (ID_1811386692_visible == 'si') {
                    ID_1811386692_visible = true;
                } else if (ID_1811386692_visible == 'no') {
                    ID_1811386692_visible = false;
                }
                $.ID_1811386692.setVisible(ID_1811386692_visible);

                var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
                var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
                if (Ti.App.deployType != 'production') console.log('detalle de seltarea', {
                    "data": seltarea
                });
                var datos = {
                    id_inspector: inspector.id_server,
                    codigo_identificador: inspector.codigo_identificador,
                    id_server: seltarea.id_server,
                    num_caso: seltarea.num_caso,
                    razon: razon
                };
                require('vars')[_var_scopekey]['datos'] = datos;
                var ID_1426934072 = {};
                ID_1426934072.success = function(e) {
                    var elemento = e,
                        valor = e;
                    var ID_1811386692_visible = false;

                    if (ID_1811386692_visible == 'si') {
                        ID_1811386692_visible = true;
                    } else if (ID_1811386692_visible == 'no') {
                        ID_1811386692_visible = false;
                    }
                    $.ID_1811386692.setVisible(ID_1811386692_visible);

                    Alloy.createController("firma_index", {}).getView().open();
                    var ID_860845987_func = function() {
                        Alloy.Events.trigger('_cerrar_insp', {
                            pantalla: 'recintos'
                        });
                    };
                    var ID_860845987 = setTimeout(ID_860845987_func, 1000 * 0.5);
                    elemento = null, valor = null;
                };
                ID_1426934072.error = function(e) {
                    var elemento = e,
                        valor = e;
                    if (Ti.App.deployType != 'production') console.log('hubo error llamando cancelarTarea, la agregamos a cola de salida', {
                        "elemento": elemento
                    });
                    if (Ti.App.deployType != 'production') console.log('agregando servicio cancelarTarea a cola', {});
                    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
                    var datos = ('datos' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['datos'] : '';
                    var ID_1428482629_m = Alloy.Collections.cola;
                    var ID_1428482629_fila = Alloy.createModel('cola', {
                        data: JSON.stringify(datos),
                        id_tarea: seltarea.id_server,
                        tipo: 'cancelar'
                    });
                    ID_1428482629_m.add(ID_1428482629_fila);
                    ID_1428482629_fila.save();
                    _.defer(function() {});
                    var ID_1811386692_visible = false;

                    if (ID_1811386692_visible == 'si') {
                        ID_1811386692_visible = true;
                    } else if (ID_1811386692_visible == 'no') {
                        ID_1811386692_visible = false;
                    }
                    $.ID_1811386692.setVisible(ID_1811386692_visible);

                    Alloy.createController("firma_index", {}).getView().open();
                    var ID_639446967_func = function() {
                        Alloy.Events.trigger('_cerrar_insp', {
                            pantalla: 'recintos'
                        });
                    };
                    var ID_639446967 = setTimeout(ID_639446967_func, 1000 * 0.5);
                    elemento = null, valor = null;
                };
                require('helper').ajaxUnico('ID_1426934072', '' + url_server + 'cancelarTarea' + '', 'POST', {
                    id_inspector: seltarea.id_inspector,
                    codigo_identificador: inspector.codigo_identificador,
                    id_tarea: seltarea.id_server,
                    num_caso: seltarea.num_caso,
                    mensaje: razon,
                    opcion: 0,
                    tipo: 1
                }, 15000, ID_1426934072);
            }
        }
        resp = null;
        e.source.removeEventListener("click", arguments.callee);
    });
    ID_2050763722.show();

}

function Agregar_dano_ID_148516318(e) {

    var evento = e;
    /** 
     * Ocupamos esta variable para impedir que la pantalla siguente se cargue dos veces 
     */
    var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
    if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
        require('vars')['var_abriendo'] = 'true';
        Alloy.createController("nuevorecinto_index", {}).getView().open();
    }

}

function Swipe_ID_1361471894(e) {

    e.cancelBubble = true;
    var elemento = e.section.getItemAt(e.itemIndex);
    var findVariables = require('fvariables');
    var fila = {};
    _.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
        _.each(obj_id, function(valor, prop) {
            fila[prop] = elemento[id_field][prop];
            var llaves = findVariables(valor, '{', '}');
            _.each(llaves, function(llave) {
                fila[llave] = elemento[id_field][prop];
            });
        });
    });
    if (Ti.App.deployType != 'production') console.log('en normal', {
        "e": e
    });
    if (e.direction == 'left') {
        var findVariables = require('fvariables');
        elemento.template = 'pborrar';
        _.each(_list_templates['pborrar'], function(obj_id, id_field) {
            _.each(obj_id, function(valor, prop) {
                var llaves = findVariables(valor, '{', '}');
                _.each(llaves, function(llave) {
                    elemento[id_field] = {};
                    elemento[id_field][prop] = fila[llave];
                });
            });
        });
        if (OS_IOS) {
            e.section.updateItemAt(e.itemIndex, elemento, {
                animated: true
            });
        } else if (OS_ANDROID) {
            e.section.updateItemAt(e.itemIndex, elemento);
        }
    }

}

function Click_ID_404942004(e) {

    e.cancelBubble = true;
    var elemento = e.section.getItemAt(e.itemIndex);
    var findVariables = require('fvariables');
    var fila = {};
    _.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
        _.each(obj_id, function(valor, prop) {
            fila[prop] = elemento[id_field][prop];
            var llaves = findVariables(valor, '{', '}');
            _.each(llaves, function(llave) {
                fila[llave] = elemento[id_field][prop];
            });
        });
    });
    /** 
     * Ocupamos esta variable para impedir que la pantalla siguente se cargue dos veces 
     */
    var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
    if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
        require('vars')['var_abriendo'] = 'true';
        /** 
         * Enviamos el parametro _dato para indicar cual sera el id del recinto a editar 
         */
        Alloy.createController("editarrecinto_index", {
            '_dato': fila
        }).getView().open();
    }

}

function Click_ID_678725846(e) {

    e.cancelBubble = true;
    var elemento = e.section.getItemAt(e.itemIndex);
    var findVariables = require('fvariables');
    var fila = {};
    _.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
        _.each(obj_id, function(valor, prop) {
            fila[prop] = elemento[id_field][prop];
            var llaves = findVariables(valor, '{', '}');
            _.each(llaves, function(llave) {
                fila[llave] = elemento[id_field][prop];
            });
        });
    });
    if (Ti.App.deployType != 'production') console.log('click en borrar', {
        "e": e
    });
    var ID_1942302623_opts = ['Si', 'No'];
    var ID_1942302623 = Ti.UI.createAlertDialog({
        title: 'ALERTA',
        message: '' + '¿ Seguro desea eliminar: ' + fila.nombre + ' ?' + '',
        buttonNames: ID_1942302623_opts
    });
    ID_1942302623.addEventListener('click', function(e) {
        var xd = ID_1942302623_opts[e.index];
        if (xd == 'Si') {
            if (Ti.App.deployType != 'production') console.log('borrando xd', {});
            var ID_1192637905_i = Alloy.Collections.insp_recintos;
            var sql = 'DELETE FROM ' + ID_1192637905_i.config.adapter.collection_name + ' WHERE id=\'' + fila.id + '\'';
            var db = Ti.Database.open(ID_1192637905_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_1192637905_i.trigger('delete');
            _.defer(function() {
                Alloy.Collections.insp_recintos.fetch();
            });
        }
        xd = null;
        e.source.removeEventListener("click", arguments.callee);
    });
    ID_1942302623.show();

}

function Swipe_ID_746041771(e) {

    e.cancelBubble = true;
    var elemento = e.section.getItemAt(e.itemIndex);
    var findVariables = require('fvariables');
    var fila = {};
    _.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
        _.each(obj_id, function(valor, prop) {
            fila[prop] = elemento[id_field][prop];
            var llaves = findVariables(valor, '{', '}');
            _.each(llaves, function(llave) {
                fila[llave] = elemento[id_field][prop];
            });
        });
    });
    if (Ti.App.deployType != 'production') console.log('en borrar', {
        "e": e
    });
    if (e.direction == 'right') {
        if (Ti.App.deployType != 'production') console.log('swipe hacia derecha (desde borrar)', {});
        var findVariables = require('fvariables');
        elemento.template = 'recinto';
        _.each(_list_templates['recinto'], function(obj_id, id_field) {
            _.each(obj_id, function(valor, prop) {
                var llaves = findVariables(valor, '{', '}');
                _.each(llaves, function(llave) {
                    elemento[id_field] = {};
                    elemento[id_field][prop] = fila[llave];
                });
            });
        });
        if (OS_IOS) {
            e.section.updateItemAt(e.itemIndex, elemento, {
                animated: true
            });
        } else if (OS_ANDROID) {
            e.section.updateItemAt(e.itemIndex, elemento);
        }
    }

}

function Click_ID_1533715671(e) {

    e.cancelBubble = true;
    var elemento = e.section.getItemAt(e.itemIndex);
    var findVariables = require('fvariables');
    var fila = {};
    _.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
        _.each(obj_id, function(valor, prop) {
            fila[prop] = elemento[id_field][prop];
            var llaves = findVariables(valor, '{', '}');
            _.each(llaves, function(llave) {
                fila[llave] = elemento[id_field][prop];
            });
        });
    });
    if (Ti.App.deployType != 'production') console.log('en borrar', {
        "e": e
    });
    var findVariables = require('fvariables');
    elemento.template = 'recinto';
    _.each(_list_templates['recinto'], function(obj_id, id_field) {
        _.each(obj_id, function(valor, prop) {
            var llaves = findVariables(valor, '{', '}');
            _.each(llaves, function(llave) {
                elemento[id_field] = {};
                elemento[id_field][prop] = fila[llave];
            });
        });
    });
    if (OS_IOS) {
        e.section.updateItemAt(e.itemIndex, elemento, {
            animated: true
        });
    } else if (OS_ANDROID) {
        e.section.updateItemAt(e.itemIndex, elemento);
    }

}

$.ID_1007469789.init({
    titulo: 'GUARDAR RECINTOS',
    __id: 'ALL1007469789',
    color: 'verde',
    onclick: Click_ID_1166599192
});

function Click_ID_1166599192(e) {

    var evento = e;
    require('vars')[_var_scopekey]['todobien'] = 'true';
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
        /** 
         * Revisamos que haya al menos un registro de recinto ingresado 
         */
        var ID_1829089847_i = Alloy.createCollection('insp_recintos');
        var ID_1829089847_i_where = 'id_inspeccion=\'' + seltarea.id_server + '\'';
        ID_1829089847_i.fetch({
            query: 'SELECT * FROM insp_recintos WHERE id_inspeccion=\'' + seltarea.id_server + '\''
        });
        var cantidad = require('helper').query2array(ID_1829089847_i);
        if (cantidad && cantidad.length == 0) {
            require('vars')[_var_scopekey]['todobien'] = 'false';
            var ID_367898160_opts = ['Aceptar'];
            var ID_367898160 = Ti.UI.createAlertDialog({
                title: 'Atención',
                message: 'Debe ingresar al menos un recinto',
                buttonNames: ID_367898160_opts
            });
            ID_367898160.addEventListener('click', function(e) {
                var nulo = ID_367898160_opts[e.index];
                nulo = null;
                e.source.removeEventListener("click", arguments.callee);
            });
            ID_367898160.show();
        }
        /** 
         * Limpiamos memoria 
         */
        cantidad = null;
    }
    var todobien = ('todobien' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['todobien'] : '';
    if (todobien == true || todobien == 'true') {
        /** 
         * Limpiamos memoria 
         */
        seltarea = null;
        /** 
         * Abrimos pantalla esta de acuerdo 
         */
        $.ID_1863810846.abrir({
            color: 'morado'
        });
    }

}

$.ID_1863810846.init({
    titulo: '¿EL ASEGURADO CONFIRMA ESTOS DATOS?',
    __id: 'ALL1863810846',
    si: 'SI, Están correctos',
    texto: 'El asegurado debe confirmar que los datos de esta sección están correctos',
    pantalla: '¿ESTA DE ACUERDO?',
    color: 'morado',
    onsi: si_ID_1708023968,
    no: 'NO, Hay que modificar algo'
});

function si_ID_1708023968(e) {

    var evento = e;
    /** 
     * Ocupamos esta variable para impedir que la pantalla siguente se cargue dos veces 
     */
    var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
    if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
        require('vars')['var_abriendo'] = 'true';
        /** 
         * Detenemos el progreso en el boton 
         */
        $.ID_1007469789.detener_progreso({});
        Alloy.createController("contenido_index", {}).getView().open();
    }

}

function Postlayout_ID_1396369289(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    $.ID_1720473650.show();

}

function Postlayout_ID_910543596(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    /** 
     * Avisamos a la pantalla anterior que debe cerrarse (siniestro) 
     */
    Alloy.Events.trigger('_cerrar_insp', {
        pantalla: 'siniestro'
    });
    var ID_2146964782_func = function() {
        /** 
         * Una vez ya cargada la tarea, dejamos la variable limpia para poder abrir otras pantallas 
         */
        require('vars')['var_abriendo'] = '';
    };
    var ID_2146964782 = setTimeout(ID_2146964782_func, 1000 * 0.2);

}

/** 
 * Cerramos esta pantalla cuando es llamada en la pantalla siguente (contenidos) 
 */
_my_events['_cerrar_insp,ID_1550055464'] = function(evento) {
    if (!_.isUndefined(evento.pantalla)) {
        if (evento.pantalla == 'recintos') {
            if (Ti.App.deployType != 'production') console.log('debug cerrando recintos', {});
            var ID_1781710521_trycatch = {
                error: function(e) {
                    if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
                }
            };
            try {
                ID_1781710521_trycatch.error = function(evento) {
                    if (Ti.App.deployType != 'production') console.log('error cerrando recintos', {});
                };
                $.ID_916746267.close();
            } catch (e) {
                ID_1781710521_trycatch.error(e);
            }
        }
    } else {
        if (Ti.App.deployType != 'production') console.log('debug cerrando (todas) recintos', {});
        var ID_1852694136_trycatch = {
            error: function(e) {
                if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
            }
        };
        try {
            ID_1852694136_trycatch.error = function(evento) {
                if (Ti.App.deployType != 'production') console.log('error cerrando recintos', {});
            };
            $.ID_916746267.close();
        } catch (e) {
            ID_1852694136_trycatch.error(e);
        }
    }
};
Alloy.Events.on('_cerrar_insp', _my_events['_cerrar_insp,ID_1550055464']);
var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
    /** 
     * restringimos recintos mostrados a la inspeccion actual (por si los temporales aun tienen datos) 
     */
    ID_553809963_filter = function(coll) {
        var filtered = coll.filter(function(m) {
            var _tests = [],
                _all_true = false,
                model = m.toJSON();
            _tests.push((model.id_inspeccion == seltarea.id_server));
            var _all_true_s = _.uniq(_tests);
            _all_true = (_all_true_s.length == 1 && _all_true_s[0] == true) ? true : false;
            return _all_true;
        });
        filtered = _.toArray(filtered);
        return filtered;
    };
    _.defer(function() {
        Alloy.Collections.insp_recintos.fetch();
    });
}


(function() {
    /** 
     * Modificamos en 0.1 segundos el color del statusbar 
     */
    var ID_1884837920_func = function() {
        var ID_916746267_statusbar = '#7E6EE0';

        var setearStatusColor = function(ID_916746267_statusbar) {
            if (OS_IOS) {
                if (ID_916746267_statusbar == 'light' || ID_916746267_statusbar == 'claro') {
                    $.ID_916746267_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.LIGHT_CONTENT);
                } else if (ID_916746267_statusbar == 'grey' || ID_916746267_statusbar == 'gris' || ID_916746267_statusbar == 'gray') {
                    $.ID_916746267_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.GREY);
                } else if (ID_916746267_statusbar == 'oscuro' || ID_916746267_statusbar == 'dark') {
                    $.ID_916746267_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.DEFAULT);
                }
            } else if (OS_ANDROID) {
                abx.setStatusbarColor(ID_916746267_statusbar);
            }
        };
        setearStatusColor(ID_916746267_statusbar);

    };
    var ID_1884837920 = setTimeout(ID_1884837920_func, 1000 * 0.1);
})();

function Androidback_ID_1720777262(e) {

    e.cancelBubble = true;
    var elemento = e.source;

}
if (OS_IOS || OS_ANDROID) {
    $.ID_916746267.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}
//$.ID_916746267.open();