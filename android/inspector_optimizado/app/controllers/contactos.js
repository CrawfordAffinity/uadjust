var _bind4section = {};
var _list_templates = {};

var _activity;
if (OS_ANDROID) {
    _activity = $.ID_1488618388.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_1488618388';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_1488618388.addEventListener('open', function(e) {
        abx.setBackgroundColor("white");
    });
}
$.ID_1488618388.orientationModes = [Titanium.UI.PORTRAIT];


$.ID_1811984849.init({
    titulo: 'ENROLAMIENTO',
    __id: 'ALL1811984849',
    fondo: 'fondoblanco',
    colortitulo: 'negro',
    modal: ''
});


$.ID_1741978890.init({
    titulo: 'PARTE 5: Contactos',
    __id: 'ALL1741978890',
    avance: '5/6',
    onclick: Click_ID_1439855743
});

function Click_ID_1439855743(e) {

    var evento = e;
    $.ID_1139316662.blur();
    $.ID_1528282890.blur();
    $.ID_1308259195.blur();
    $.ID_1991898351.blur();

}

$.ID_23912785.init({
    titulo: 'CONTINUAR',
    __id: 'ALL23912785',
    onclick: Click_ID_574800281
});

function Click_ID_574800281(e) {

    var evento = e;
    /** 
     * Obtenemos los valores de los campos de texto 
     */
    var correo;
    correo = $.ID_1139316662.getValue();

    var correo_aux;
    correo_aux = $.ID_1528282890.getValue();

    var telefono;
    telefono = $.ID_1308259195.getValue();

    var telefono_aux;
    telefono_aux = $.ID_1991898351.getValue();

    if ((_.isObject(correo) || _.isString(correo)) && _.isEmpty(correo)) {
        /** 
         * Validamos que los datos ingresados existan, esten correctos, y que coincidan 
         */
        var ID_1133057164_opts = ['Aceptar'];
        var ID_1133057164 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese correo electrónico',
            buttonNames: ID_1133057164_opts
        });
        ID_1133057164.addEventListener('click', function(e) {
            var suu = ID_1133057164_opts[e.index];
            suu = null;

            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1133057164.show();
    } else if (!(_.isString(correo) && /\S+@\S+\.\S+/.test(correo))) {
        var ID_1091423964_opts = ['Aceptar'];
        var ID_1091423964 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese correo electrónico',
            buttonNames: ID_1091423964_opts
        });
        ID_1091423964.addEventListener('click', function(e) {
            var suu = ID_1091423964_opts[e.index];
            suu = null;

            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1091423964.show();
    } else if (correo != correo_aux) {
        var ID_1609709036_opts = ['Aceptar'];
        var ID_1609709036 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Verifique que los correos electrónicos coincidan',
            buttonNames: ID_1609709036_opts
        });
        ID_1609709036.addEventListener('click', function(e) {
            var suu = ID_1609709036_opts[e.index];
            suu = null;

            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1609709036.show();
    } else if ((_.isObject(telefono) || _.isString(telefono)) && _.isEmpty(telefono)) {
        var ID_868748226_opts = ['Aceptar'];
        var ID_868748226 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese número de teléfono de contacto',
            buttonNames: ID_868748226_opts
        });
        ID_868748226.addEventListener('click', function(e) {
            var suu = ID_868748226_opts[e.index];
            suu = null;

            e.source.removeEventListener("click", arguments.callee);
        });
        ID_868748226.show();
    } else if (_.isNumber((telefono.length)) && _.isNumber(3) && (telefono.length) <= 3) {
        var ID_1137488923_opts = ['Aceptar'];
        var ID_1137488923 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese número de teléfono de contacto',
            buttonNames: ID_1137488923_opts
        });
        ID_1137488923.addEventListener('click', function(e) {
            var suu = ID_1137488923_opts[e.index];
            suu = null;

            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1137488923.show();
    } else if (telefono != telefono_aux) {
        var ID_1668039868_opts = ['Aceptar'];
        var ID_1668039868 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Verifique que los teléfonos coincidan',
            buttonNames: ID_1668039868_opts
        });
        ID_1668039868.addEventListener('click', function(e) {
            var suu = ID_1668039868_opts[e.index];
            suu = null;

            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1668039868.show();
    } else {

        $.ID_23912785.iniciar_progreso({});
        /** 
         * Recuperamos variable, ingresamos los valores de contacto y guardamos en la variable 
         */
        var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
        var registro = _.extend(registro, {
            correo: correo,
            telefono: telefono
        });
        require('vars')['registro'] = registro;
        var ID_716318260_func = function() {

            $.ID_23912785.detener_progreso({});
            /** 
             * Enviamos a pantalla de oficio 
             */
            Alloy.createController("oficio", {}).getView().open();
        };
        var ID_716318260 = setTimeout(ID_716318260_func, 1000 * 0.3);
    }
}

(function() {
    /** 
     * Creamos evento que al cerrar desde la ultima pantalla, esta tambi&#233;n se cierre. Evitamos el uso excesivo de memoria ram 
     */

    _my_events['_close_enrolamiento,ID_363799807'] = function(evento) {
        $.ID_1488618388.close();
        if (Ti.App.deployType != 'production') console.log('escuchando cerrar enrolamiento contactos', {});
    };
    Alloy.Events.on('_close_enrolamiento', _my_events['_close_enrolamiento,ID_363799807']);
})();

function Androidback_ID_577336028(e) {
    /** 
     * Dejamos esta accion vacia para que no pueda volver a la pantalla anterior 
     */

    e.cancelBubble = true;
    var elemento = e.source;

}
if (OS_IOS || OS_ANDROID) {
    $.ID_1488618388.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}