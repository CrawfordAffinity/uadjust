var _bind4section = {};
var _list_templates = {};

var _activity;
if (OS_ANDROID) {
    _activity = $.ID_375599278.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_375599278';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_375599278.addEventListener('open', function(e) {});
}
$.ID_375599278.orientationModes = [Titanium.UI.PORTRAIT];


$.ID_1333942819.init({
    titulo: 'DISPONIBILIDAD',
    __id: 'ALL1333942819',
    textoderecha: 'Guardar',
    fondo: 'fondoblanco',
    colortitulo: 'negro',
    modal: '',
    onpresiono: Presiono_ID_1948273248,
    colortextoderecha: 'azul'
});

function Presiono_ID_1948273248(e) {

    var evento = e;
    /** 
     * recuperamos variables de las horas y dias 
     */
    var desde = ('desde' in require('vars')) ? require('vars')['desde'] : '';
    var hasta = ('hasta' in require('vars')) ? require('vars')['hasta'] : '';
    var dias = ('dias' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['dias'] : '';
    /** 
     * Creamos una variable para validar que al menos un dia este seleccionado 
     */

    //validaciones
    var al_menos_uno = dias.d1 || dias.d2 || dias.d3 || dias.d4 || dias.d5 || dias.d6 || dias.d7;
    desde = parseInt(desde);
    hasta = parseInt(hasta)
    if (al_menos_uno == false || al_menos_uno == 'false') {
        /** 
         * Mostramos mensajes en caso de que exista un problema, ejemplo, que no hayan dias seleccionados o que las horas no tengan sentido 
         */
        var ID_1556728246_opts = ['Aceptar'];
        var ID_1556728246 = Ti.UI.createAlertDialog({
            title: 'Alerta',
            message: 'Seleccione al menos un día',
            buttonNames: ID_1556728246_opts
        });
        ID_1556728246.addEventListener('click', function(e) {
            var suu = ID_1556728246_opts[e.index];
            suu = null;

            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1556728246.show();
    } else if (_.isNumber(desde) && _.isNumber(hasta) && desde > hasta) {
        var ID_1358577017_opts = ['Aceptar'];
        var ID_1358577017 = Ti.UI.createAlertDialog({
            title: 'Alerta',
            message: 'El rango de horas está mal ingresado',
            buttonNames: ID_1358577017_opts
        });
        ID_1358577017.addEventListener('click', function(e) {
            var suu = ID_1358577017_opts[e.index];
            suu = null;

            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1358577017.show();
    } else if (desde == hasta) {
        var ID_2029310034_opts = ['Aceptar'];
        var ID_2029310034 = Ti.UI.createAlertDialog({
            title: 'Alerta',
            message: 'El rango debe ser superior a una hora',
            buttonNames: ID_2029310034_opts
        });
        ID_2029310034.addEventListener('click', function(e) {
            var suu = ID_2029310034_opts[e.index];
            suu = null;

            e.source.removeEventListener("click", arguments.callee);
        });
        ID_2029310034.show();
    } else {
        if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
            /** 
             * Recuperamos variables de la url de uadjust, fuerapais, fueraciudad desde la pantalla anterior 
             */
            var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
            var fuerapais = ('fuerapais' in require('vars')) ? require('vars')['fuerapais'] : '';
            var fueraciudad = ('fueraciudad' in require('vars')) ? require('vars')['fueraciudad'] : '';
            var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
            /** 
             * Creamos una variable para poder mandar el dato completo de las horas disponibles al servidor 
             */
            var disp_horas = desde + ':00 ' + hasta + ':00';
            if (Ti.App.deployType != 'production') console.log('los datos parttime', {
                "ciudad": fueraciudad,
                "diass": dias,
                "horas": disp_horas,
                "idinsp": inspector.id_server,
                "pais": fuerapais
            });
            var ID_158818601 = {};
            console.log('DEBUG WEB: requesting url:' + url_server + 'editarPerfilTipo2' + ' with data:', {
                _method: 'POST',
                _params: {
                    id_inspector: inspector.id_server,
                    disponibilidad_viajar_pais: fuerapais,
                    disponibilidad_viajar_ciudad: fueraciudad,
                    disponibilidad_horas: disp_horas,
                    disponibilidad: 0,
                    d1: dias.d1,
                    d2: dias.d2,
                    d3: dias.d3,
                    d4: dias.d4,
                    d5: dias.d5,
                    d6: dias.d6,
                    d7: dias.d7
                },
                _timeout: '15000'
            });

            ID_158818601.success = function(e) {
                var elemento = e,
                    valor = e;
                if (Ti.App.deployType != 'production') console.log('no se cae la web', {
                    "detalle": elemento
                });
                if (elemento.error == 0 || elemento.error == '0') {
                    /** 
                     * Modificamos la tabla con los datos nuevos 
                     */
                    var ID_1254086448_i = Alloy.createCollection('inspectores');
                    var ID_1254086448_i_where = '';
                    ID_1254086448_i.fetch();
                    var inspector_list = require('helper').query2array(ID_1254086448_i);
                    var fueraciudad = ('fueraciudad' in require('vars')) ? require('vars')['fueraciudad'] : '';
                    var fuerapais = ('fuerapais' in require('vars')) ? require('vars')['fuerapais'] : '';
                    var db = Ti.Database.open(ID_1254086448_i.config.adapter.db_name);
                    if (ID_1254086448_i_where == '') {
                        var sql = 'UPDATE ' + ID_1254086448_i.config.adapter.collection_name + ' SET disponibilidad_viajar_pais=\'' + fuerapais + '\'';
                    } else {
                        var sql = 'UPDATE ' + ID_1254086448_i.config.adapter.collection_name + ' SET disponibilidad_viajar_pais=\'' + fuerapais + '\' WHERE ' + ID_1254086448_i_where;
                    }
                    db.execute(sql);
                    sql = null;
                    if (ID_1254086448_i_where == '') {
                        var sql = 'UPDATE ' + ID_1254086448_i.config.adapter.collection_name + ' SET disponibilidad_viajar_ciudad=\'' + fueraciudad + '\'';
                    } else {
                        var sql = 'UPDATE ' + ID_1254086448_i.config.adapter.collection_name + ' SET disponibilidad_viajar_ciudad=\'' + fueraciudad + '\' WHERE ' + ID_1254086448_i_where;
                    }
                    db.execute(sql);
                    sql = null;
                    if (ID_1254086448_i_where == '') {
                        var sql = 'UPDATE ' + ID_1254086448_i.config.adapter.collection_name + ' SET disponibilidad=\'' + 0 + '\'';
                    } else {
                        var sql = 'UPDATE ' + ID_1254086448_i.config.adapter.collection_name + ' SET disponibilidad=\'' + 0 + '\' WHERE ' + ID_1254086448_i_where;
                    }
                    db.execute(sql);
                    sql = null;
                    if (ID_1254086448_i_where == '') {
                        var sql = 'UPDATE ' + ID_1254086448_i.config.adapter.collection_name + ' SET disponibilidad_horas=\'' + disp_horas + '\'';
                    } else {
                        var sql = 'UPDATE ' + ID_1254086448_i.config.adapter.collection_name + ' SET disponibilidad_horas=\'' + disp_horas + '\' WHERE ' + ID_1254086448_i_where;
                    }
                    db.execute(sql);
                    sql = null;
                    if (ID_1254086448_i_where == '') {
                        var sql = 'UPDATE ' + ID_1254086448_i.config.adapter.collection_name + ' SET d1=\'' + dias.d1 + '\'';
                    } else {
                        var sql = 'UPDATE ' + ID_1254086448_i.config.adapter.collection_name + ' SET d1=\'' + dias.d1 + '\' WHERE ' + ID_1254086448_i_where;
                    }
                    db.execute(sql);
                    sql = null;
                    if (ID_1254086448_i_where == '') {
                        var sql = 'UPDATE ' + ID_1254086448_i.config.adapter.collection_name + ' SET d2=\'' + dias.d2 + '\'';
                    } else {
                        var sql = 'UPDATE ' + ID_1254086448_i.config.adapter.collection_name + ' SET d2=\'' + dias.d2 + '\' WHERE ' + ID_1254086448_i_where;
                    }
                    db.execute(sql);
                    sql = null;
                    if (ID_1254086448_i_where == '') {
                        var sql = 'UPDATE ' + ID_1254086448_i.config.adapter.collection_name + ' SET d3=\'' + dias.d3 + '\'';
                    } else {
                        var sql = 'UPDATE ' + ID_1254086448_i.config.adapter.collection_name + ' SET d3=\'' + dias.d3 + '\' WHERE ' + ID_1254086448_i_where;
                    }
                    db.execute(sql);
                    sql = null;
                    if (ID_1254086448_i_where == '') {
                        var sql = 'UPDATE ' + ID_1254086448_i.config.adapter.collection_name + ' SET d4=\'' + dias.d4 + '\'';
                    } else {
                        var sql = 'UPDATE ' + ID_1254086448_i.config.adapter.collection_name + ' SET d4=\'' + dias.d4 + '\' WHERE ' + ID_1254086448_i_where;
                    }
                    db.execute(sql);
                    sql = null;
                    if (ID_1254086448_i_where == '') {
                        var sql = 'UPDATE ' + ID_1254086448_i.config.adapter.collection_name + ' SET d5=\'' + dias.d5 + '\'';
                    } else {
                        var sql = 'UPDATE ' + ID_1254086448_i.config.adapter.collection_name + ' SET d5=\'' + dias.d5 + '\' WHERE ' + ID_1254086448_i_where;
                    }
                    db.execute(sql);
                    sql = null;
                    if (ID_1254086448_i_where == '') {
                        var sql = 'UPDATE ' + ID_1254086448_i.config.adapter.collection_name + ' SET d6=\'' + dias.d6 + '\'';
                    } else {
                        var sql = 'UPDATE ' + ID_1254086448_i.config.adapter.collection_name + ' SET d6=\'' + dias.d6 + '\' WHERE ' + ID_1254086448_i_where;
                    }
                    db.execute(sql);
                    sql = null;
                    if (ID_1254086448_i_where == '') {
                        var sql = 'UPDATE ' + ID_1254086448_i.config.adapter.collection_name + ' SET d7=\'' + dias.d7 + '\'';
                    } else {
                        var sql = 'UPDATE ' + ID_1254086448_i.config.adapter.collection_name + ' SET d7=\'' + dias.d7 + '\' WHERE ' + ID_1254086448_i_where;
                    }
                    db.execute(sql);
                    sql = null;
                    db.close();
                    db = null;
                    /** 
                     * Actualizamos la variable del inspector 
                     */
                    var ID_1182993078_i = Alloy.createCollection('inspectores');
                    var ID_1182993078_i_where = '';
                    ID_1182993078_i.fetch();
                    var inspector_list = require('helper').query2array(ID_1182993078_i);
                    require('vars')['inspector'] = inspector_list[0];
                    /** 
                     * Limpiamos memoria 
                     */
                    inspector_list = null;
                    /** 
                     * Cerramos la pantalla anterior y la actual 
                     */

                    Alloy.Events.trigger('_close_editar');
                    $.ID_375599278.close();
                }
                elemento = null, valor = null;
            };

            ID_158818601.error = function(e) {
                var elemento = e,
                    valor = e;
                if (Ti.App.deployType != 'production') console.log('Hubo un error', {
                    "elemento": elemento
                });
                elemento = null, valor = null;
            };
            require('helper').ajaxUnico('ID_158818601', '' + url_server + 'editarPerfilTipo2' + '', 'POST', {
                id_inspector: inspector.id_server,
                disponibilidad_viajar_pais: fuerapais,
                disponibilidad_viajar_ciudad: fueraciudad,
                disponibilidad_horas: disp_horas,
                disponibilidad: 0,
                d1: dias.d1,
                d2: dias.d2,
                d3: dias.d3,
                d4: dias.d4,
                d5: dias.d5,
                d6: dias.d6,
                d7: dias.d7
            }, 15000, ID_158818601);
        } else {
            var ID_922158419_opts = ['Aceptar'];
            var ID_922158419 = Ti.UI.createAlertDialog({
                title: 'No hay internet',
                message: 'No se pueden efectuar los cambios sin conexion.',
                buttonNames: ID_922158419_opts
            });
            ID_922158419.addEventListener('click', function(e) {
                var suu = ID_922158419_opts[e.index];
                suu = null;

                e.source.removeEventListener("click", arguments.callee);
            });
            ID_922158419.show();
        }
    }
}

$.ID_1830769168.init({
    titulo: 'Editar Disponibilidad de trabajo',
    __id: 'ALL1830769168',
    avance: '',
    onclick: Click_ID_526629820
});

function Click_ID_526629820(e) {

    var evento = e;

}

$.ID_1285778581.init({
    __id: 'ALL1285778581',
    letra: 'L',
    onon: on_ID_1831192119,
    onoff: Off_ID_1544370596
});

function on_ID_1831192119(e) {
    /** 
     * Recuperamos la variable de los dias, actualizamos la estructura con el dato fijado por el inspector y guardamos la variable 
     */

    var evento = e;
    var dias = ('dias' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['dias'] : '';
    var dias = _.extend(dias, {
        d1: 1
    });
    require('vars')[_var_scopekey]['dias'] = dias;

}

function Off_ID_1544370596(e) {

    var evento = e;
    var dias = ('dias' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['dias'] : '';
    var dias = _.extend(dias, {
        d1: 0
    });
    require('vars')[_var_scopekey]['dias'] = dias;

}

$.ID_1033348405.init({
    __id: 'ALL1033348405',
    letra: 'M',
    onon: on_ID_199380194,
    onoff: Off_ID_1042707636
});

function on_ID_199380194(e) {
    /** 
     * Recuperamos la variable de los dias, actualizamos la estructura con el dato fijado por el inspector y guardamos la variable 
     */

    var evento = e;
    var dias = ('dias' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['dias'] : '';
    var dias = _.extend(dias, {
        d2: 1
    });
    require('vars')[_var_scopekey]['dias'] = dias;

}

function Off_ID_1042707636(e) {

    var evento = e;
    var dias = ('dias' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['dias'] : '';
    var dias = _.extend(dias, {
        d2: 0
    });
    require('vars')[_var_scopekey]['dias'] = dias;

}

$.ID_1633517487.init({
    __id: 'ALL1633517487',
    letra: 'MI',
    onon: on_ID_666152959,
    onoff: Off_ID_1148297090
});

function on_ID_666152959(e) {
    /** 
     * Recuperamos la variable de los dias, actualizamos la estructura con el dato fijado por el inspector y guardamos la variable 
     */

    var evento = e;
    var dias = ('dias' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['dias'] : '';
    var dias = _.extend(dias, {
        d3: 1
    });
    require('vars')[_var_scopekey]['dias'] = dias;

}

function Off_ID_1148297090(e) {

    var evento = e;
    var dias = ('dias' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['dias'] : '';
    var dias = _.extend(dias, {
        d3: 0
    });
    require('vars')[_var_scopekey]['dias'] = dias;

}

$.ID_1951530425.init({
    __id: 'ALL1951530425',
    letra: 'J',
    onon: on_ID_1802509101,
    onoff: Off_ID_239278743
});

function on_ID_1802509101(e) {
    /** 
     * Recuperamos la variable de los dias, actualizamos la estructura con el dato fijado por el inspector y guardamos la variable 
     */

    var evento = e;
    var dias = ('dias' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['dias'] : '';
    var dias = _.extend(dias, {
        d4: 1
    });
    require('vars')[_var_scopekey]['dias'] = dias;

}

function Off_ID_239278743(e) {

    var evento = e;
    var dias = ('dias' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['dias'] : '';
    var dias = _.extend(dias, {
        d4: 0
    });
    require('vars')[_var_scopekey]['dias'] = dias;

}

$.ID_1398866748.init({
    __id: 'ALL1398866748',
    letra: 'V',
    onon: on_ID_824484198,
    onoff: Off_ID_81763569
});

function on_ID_824484198(e) {
    /** 
     * Recuperamos la variable de los dias, actualizamos la estructura con el dato fijado por el inspector y guardamos la variable 
     */

    var evento = e;
    var dias = ('dias' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['dias'] : '';
    var dias = _.extend(dias, {
        d5: 1
    });
    require('vars')[_var_scopekey]['dias'] = dias;

}

function Off_ID_81763569(e) {

    var evento = e;
    var dias = ('dias' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['dias'] : '';
    var dias = _.extend(dias, {
        d5: 0
    });
    require('vars')[_var_scopekey]['dias'] = dias;

}

$.ID_1737804109.init({
    __id: 'ALL1737804109',
    letra: 'S',
    onon: on_ID_1105140601,
    onoff: Off_ID_437559888
});

function on_ID_1105140601(e) {
    /** 
     * Recuperamos la variable de los dias, actualizamos la estructura con el dato fijado por el inspector y guardamos la variable 
     */

    var evento = e;
    var dias = ('dias' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['dias'] : '';
    var dias = _.extend(dias, {
        d6: 1
    });
    require('vars')[_var_scopekey]['dias'] = dias;

}

function Off_ID_437559888(e) {

    var evento = e;
    var dias = ('dias' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['dias'] : '';
    var dias = _.extend(dias, {
        d6: 0
    });
    require('vars')[_var_scopekey]['dias'] = dias;

}

$.ID_1819418283.init({
    __id: 'ALL1819418283',
    letra: 'D',
    onon: on_ID_156039398,
    onoff: Off_ID_748552299
});

function on_ID_156039398(e) {
    /** 
     * Recuperamos la variable de los dias, actualizamos la estructura con el dato fijado por el inspector y guardamos la variable 
     */

    var evento = e;
    var dias = ('dias' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['dias'] : '';
    var dias = _.extend(dias, {
        d7: 1
    });
    require('vars')[_var_scopekey]['dias'] = dias;

}

function Off_ID_748552299(e) {

    var evento = e;
    var dias = ('dias' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['dias'] : '';
    var dias = _.extend(dias, {
        d7: 0
    });
    require('vars')[_var_scopekey]['dias'] = dias;

}

$.ID_1749240493.init({
    __id: 'ALL1749240493',
    onchange: Change_ID_1802043385,
    mins: true,
    a: 'a'
});

function Change_ID_1802043385(e) {

    var evento = e;
    /** 
     * Actualizamos las variables con las horas indicadas por el inspector 
     */
    require('vars')['desde'] = evento.desde;
    require('vars')['hasta'] = evento.hasta;

}

(function() {
    /** 
     * Recuperamos la variable del inspector 
     */
    var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
    if (!_.isNull(inspector.disponibilidad_horas)) {
        /** 
         * Revisamos que el inspector tenga la disponibilidad de horas, revisamos que contenga datos, separamos los valores de la hora y mandamos los datos al widget de la disponibilidad de horas, de otro modo fijamos los valores con datos dummy 
         */
        if ((_.isObject(inspector.disponibilidad_horas) || (_.isString(inspector.disponibilidad_horas)) && !_.isEmpty(inspector.disponibilidad_horas)) || _.isNumber(inspector.disponibilidad_horas) || _.isBoolean(inspector.disponibilidad_horas)) {

            horas = inspector.disponibilidad_horas.split(" ");
            desde_horas = horas[0].split(":");
            hasta_horas = horas[1].split(":")
            require('vars')['desde'] = desde_horas[0];
            require('vars')['hasta'] = hasta_horas[0];
            if (Ti.App.deployType != 'production') console.log('horas insp', {
                "dispo": horas
            });

            $.ID_1749240493.set({
                desde: desde_horas[0],
                hasta: hasta_horas[0]
            });
        } else {
            require('vars')['desde'] = '1';
            require('vars')['hasta'] = '1';
        }
        if (Ti.App.deployType != 'production') console.log('NO ES NULO', {});
    }
    /** 
     * Encendemos los dias que el inspector tenia marcado como disponibles 
     */

    $.ID_1285778581.set({
        on: inspector.d1
    });

    $.ID_1033348405.set({
        on: inspector.d2
    });

    $.ID_1633517487.set({
        on: inspector.d3
    });

    $.ID_1951530425.set({
        on: inspector.d4
    });

    $.ID_1398866748.set({
        on: inspector.d5
    });

    $.ID_1737804109.set({
        on: inspector.d6
    });

    $.ID_1819418283.set({
        on: inspector.d7
    });
    /** 
     * Creamos una variable de estructura con los dias disponibles y los guardamos en una variable para acceder desde otro lado 
     */
    var dias = {
        d1: inspector.d1,
        d2: inspector.d2,
        d3: inspector.d3,
        d4: inspector.d4,
        d5: inspector.d5,
        d6: inspector.d6,
        d7: inspector.d7
    };
    require('vars')[_var_scopekey]['dias'] = dias;
    if (Ti.App.deployType != 'production') console.log('quiero saber si los dias estan bien cargados en la estructura', {
        "datos": dias
    });
})();

function Postlayout_ID_835780064(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var ID_1482413520_func = function() {
        /** 
         * Una vez ya cargada la tarea, dejamos la variable limpia para poder abrir otras pantallas 
         */
        require('vars')['var_abriendo'] = '';
    };
    var ID_1482413520 = setTimeout(ID_1482413520_func, 1000 * 0.2);

}
if (OS_IOS || OS_ANDROID) {
    $.ID_375599278.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}