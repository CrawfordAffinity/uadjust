var _bind4section = {};
var _list_templates = {
    "elemento": {
        "ID_432412973": {},
        "ID_291370846": {
            "text": "{nombre}"
        },
        "ID_1925487537": {
            "text": "{id_server}"
        },
        "ID_975786586": {
            "text": "{id_segured}"
        },
        "ID_1546190443": {}
    },
    "dano": {
        "ID_1569764493": {
            "text": "{id}"
        },
        "ID_304218376": {},
        "ID_1912165224": {
            "text": "{nombre}"
        }
    },
    "pborrar": {
        "ID_1236907100": {
            "text": "{id}"
        },
        "ID_1506538078": {},
        "ID_2024360383": {},
        "ID_1765564351": {},
        "ID_1699280427": {
            "text": "{nombre}"
        },
        "ID_1445903745": {},
        "ID_1795050796": {},
        "ID_1752901998": {},
        "ID_1914367022": {
            "text": "{nombre}"
        },
        "ID_1249074865": {
            "text": "{id}"
        },
        "ID_1131557960": {},
        "ID_1361874287": {},
        "ID_1750057271": {},
        "ID_1962135207": {},
        "ID_1947166298": {
            "text": "{nombre}"
        },
        "ID_2008605730": {
            "text": "{id}"
        },
        "ID_1788775425": {},
        "ID_1246915111": {}
    },
    "contenido": {
        "ID_630800887": {},
        "ID_862352109": {
            "text": "{id}"
        },
        "ID_560452274": {
            "text": "{nombre}"
        }
    },
    "recinto": {
        "ID_1960582596": {},
        "ID_871581291": {
            "text": "{nombre}"
        },
        "ID_310297848": {
            "text": "{id}"
        }
    },
    "nivel": {
        "ID_1021786928": {
            "text": "{id}"
        },
        "ID_1241655463": {
            "text": "{nombre}"
        },
        "ID_599581692": {}
    }
};

var _activity;
if (OS_ANDROID) {
    _activity = $.ID_1800226270.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_1800226270';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_1800226270.addEventListener('open', function(e) {});
}
$.ID_1800226270.orientationModes = [Titanium.UI.PORTRAIT];


$.ID_1257889779.init({
    titulo: '¿HAY ALGUIEN?',
    __id: 'ALL1257889779',
    fondo: 'fondoazul',
    top: 80
});


$.ID_1979309565.init({
    titulo: '¿PUEDE CONTINUAR CON LA INSPECCION?',
    __id: 'ALL1979309565',
    si: 'SI puedo continuar',
    texto: 'Si está el asegurado en el domicilio presione SI para continuar',
    onno: no_ID_1254157688,
    onsi: si_ID_1837036663,
    top: 80,
    no: 'NO se pudo realizar la inspección'
});

function si_ID_1837036663(e) {

    var evento = e;
    Alloy.createController("datosbasicos_index", {}).getView().open();

}

function no_ID_1254157688(e) {

    var evento = e;
    var ID_1219670720_opts = ['Asegurado no puede seguir', 'Se me acabo la bateria', 'Tuve un accidente', 'Cancelar'];
    var ID_1219670720 = Ti.UI.createOptionDialog({
        title: 'RAZON PARA CANCELAR INSPECCION ACTUAL',
        options: ID_1219670720_opts
    });
    ID_1219670720.addEventListener('click', function(e) {
        var resp = ID_1219670720_opts[e.index];
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var razon = "";
            if (resp == 'Asegurado no puede seguir') {
                /** 
                 * Esto parece redundante, pero es para escapar de i18n el texto 
                 */
                razon = "Asegurado no puede seguir";
            }
            if (resp == 'Se me acabo la bateria') {
                razon = "Se me acabo la bateria";
            }
            if (resp == 'Tuve un accidente') {
                razon = "Tuve un accidente";
            }
            if ((_.isObject(razon) || (_.isString(razon)) && !_.isEmpty(razon)) || _.isNumber(razon) || _.isBoolean(razon)) {
                require('vars')['insp_cancelada'] = 'recintos';
                var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
                /** 
                 * Mostramos popup avisando que se esta cancelando la inspeccion 
                 */
                var ID_2101947678_visible = true;

                if (ID_2101947678_visible == 'si') {
                    ID_2101947678_visible = true;
                } else if (ID_2101947678_visible == 'no') {
                    ID_2101947678_visible = false;
                }
                $.ID_2101947678.setVisible(ID_2101947678_visible);

                var ID_1267243180 = {};

                ID_1267243180.success = function(e) {
                    var elemento = e,
                        valor = e;
                    /** 
                     * Ocultamos popup avisando que se esta cancelando la inspeccion 
                     */
                    var ID_2101947678_visible = false;

                    if (ID_2101947678_visible == 'si') {
                        ID_2101947678_visible = true;
                    } else if (ID_2101947678_visible == 'no') {
                        ID_2101947678_visible = false;
                    }
                    $.ID_2101947678.setVisible(ID_2101947678_visible);

                    /** 
                     * Enviamos a firma 
                     */
                    Alloy.createController("firma_index", {}).getView().open();
                    var ID_1408913884_func = function() {

                        Alloy.Events.trigger('_cerrar_insp', {
                            pantalla: 'hayalguien'
                        });
                    };
                    var ID_1408913884 = setTimeout(ID_1408913884_func, 1000 * 0.5);
                    elemento = null, valor = null;
                };

                ID_1267243180.error = function(e) {
                    var elemento = e,
                        valor = e;
                    if (Ti.App.deployType != 'production') console.log('hubo error llamando cancelarTarea, la agregamos a cola de salida', {
                        "elemento": elemento
                    });
                    if (Ti.App.deployType != 'production') console.log('agregando servicio cancelarTarea a cola', {});
                    /** 
                     * En el caso que haya habido un problema llamando al servicio cancelarTarea, guardamos los datos en un objeto 
                     */
                    var datos = {
                        id_inspector: seltarea.id_inspector,
                        codigo_identificador: inspector.codigo_identificador,
                        id_server: seltarea.id_server,
                        num_caso: seltarea.num_caso,
                        razon: razon
                    };
                    var ID_1857413448_m = Alloy.Collections.cola;
                    var ID_1857413448_fila = Alloy.createModel('cola', {
                        data: JSON.stringify(datos),
                        id_tarea: seltarea.id_server,
                        tipo: 'cancelar'
                    });
                    ID_1857413448_m.add(ID_1857413448_fila);
                    ID_1857413448_fila.save();
                    /** 
                     * Ocultamos popup avisando que se esta cancelando la inspeccion 
                     */
                    var ID_2101947678_visible = false;

                    if (ID_2101947678_visible == 'si') {
                        ID_2101947678_visible = true;
                    } else if (ID_2101947678_visible == 'no') {
                        ID_2101947678_visible = false;
                    }
                    $.ID_2101947678.setVisible(ID_2101947678_visible);

                    /** 
                     * Enviamos a firma 
                     */
                    Alloy.createController("firma_index", {}).getView().open();
                    var ID_1869065227_func = function() {

                        Alloy.Events.trigger('_cerrar_insp', {
                            pantalla: 'hayalguien'
                        });
                    };
                    var ID_1869065227 = setTimeout(ID_1869065227_func, 1000 * 0.5);
                    elemento = null, valor = null;
                };
                require('helper').ajaxUnico('ID_1267243180', '' + url_server + 'cancelarTarea' + '', 'POST', {
                    id_inspector: seltarea.id_inspector,
                    codigo_identificador: inspector.codigo_identificador,
                    id_tarea: seltarea.id_server,
                    num_caso: seltarea.num_caso,
                    mensaje: razon,
                    opcion: 0,
                    tipo: 1
                }, 15000, ID_1267243180);
            }
        }
        resp = null;

        e.source.removeEventListener("click", arguments.callee);
    });
    ID_1219670720.show();

}

function Postlayout_ID_2057023630(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    $.ID_1017752952.show();

}

(function() {
    /** 
     * Llamamos servicio iniciarInspeccion aqui, no hacemos nada con respuesta 
     */
    var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    /** 
     * Esto es para indicar en firma ult pantalla a cerrar en cancelada 
     */
    require('vars')['insp_cancelada'] = '';
    /** 
     * Desactivamos seguimiento de tarea 
     */
    require('vars')['seguir_tarea'] = '';
    /** 
     * api de iniciar tarea: solo si estamos en compilacion full 
     */
    var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
    var datos = {
        id_inspector: inspector.id_server,
        codigo_identificador: inspector.codigo_identificador,
        id_server: seltarea.id_server,
        num_caso: seltarea.num_caso
    };
    require('vars')[_var_scopekey]['datos'] = datos;
    if ((_.isObject(url_server) || (_.isString(url_server)) && !_.isEmpty(url_server)) || _.isNumber(url_server) || _.isBoolean(url_server)) {
        /** 
         * Revisamos si la variable url_server contiene datos para poder avisar al servidor que iniciaremos la inspeccion 
         */
        /** 
         * Inicializamos inspeccion 
         */
        require('vars')['inspeccion_encurso'] = 'true';
        var moment = require('alloy/moment');
        var hora = moment(new Date()).format('HH:mm');
        var moment = require('alloy/moment');
        var fecha = moment(new Date()).format('DD-MM-YYYY');
        var ID_1648086850_m = Alloy.Collections.inspecciones;
        var ID_1648086850_fila = Alloy.createModel('inspecciones', {
            hora: hora,
            fecha: fecha,
            id_server: seltarea.id_server,
            fecha_inspeccion_inicio: new Date()
        });
        ID_1648086850_m.add(ID_1648086850_fila);
        ID_1648086850_fila.save();
        if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
            var ID_1230260747 = {};

            ID_1230260747.success = function(e) {
                var elemento = e,
                    valor = e;
                datos = null;
                elemento = null, valor = null;
            };

            ID_1230260747.error = function(e) {
                var elemento = e,
                    valor = e;
                if (Ti.App.deployType != 'production') console.log('evento iniciar tarea se fue a la cola', {});
                var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
                var datos = ('datos' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['datos'] : '';
                var ID_1447723275_m = Alloy.Collections.cola;
                var ID_1447723275_fila = Alloy.createModel('cola', {
                    data: JSON.stringify(datos),
                    id_tarea: seltarea.id_server,
                    tipo: 'iniciar'
                });
                ID_1447723275_m.add(ID_1447723275_fila);
                ID_1447723275_fila.save();
                datos = null;
                elemento = null, valor = null;
            };
            require('helper').ajaxUnico('ID_1230260747', '' + url_server + 'iniciarTarea' + '', 'POST', {
                id_inspector: inspector.id_server,
                codigo_identificador: inspector.codigo_identificador,
                id_tarea: seltarea.id_server,
                num_caso: seltarea.num_caso
            }, 15000, ID_1230260747);
        } else {
            if (Ti.App.deployType != 'production') console.log('evento iniciar tarea se fue a la cola', {});
            var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
            var datos = ('datos' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['datos'] : '';
            var ID_1825740455_m = Alloy.Collections.cola;
            var ID_1825740455_fila = Alloy.createModel('cola', {
                data: JSON.stringify(datos),
                id_tarea: seltarea.id_server,
                tipo: 'iniciar'
            });
            ID_1825740455_m.add(ID_1825740455_fila);
            ID_1825740455_fila.save();
            datos = null;
        }
    }
    require('vars')['seguir_tarea'] = '';
    /** 
     * Modificamos en 0.1 segundos el color del statusbar 
     */
    var ID_583987458_func = function() {
        var ID_1800226270_statusbar = '#006C9B';

        var setearStatusColor = function(ID_1800226270_statusbar) {
            if (OS_IOS) {
                if (ID_1800226270_statusbar == 'light' || ID_1800226270_statusbar == 'claro') {
                    $.ID_1800226270_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.LIGHT_CONTENT);
                } else if (ID_1800226270_statusbar == 'grey' || ID_1800226270_statusbar == 'gris' || ID_1800226270_statusbar == 'gray') {
                    $.ID_1800226270_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.GREY);
                } else if (ID_1800226270_statusbar == 'oscuro' || ID_1800226270_statusbar == 'dark') {
                    $.ID_1800226270_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.DEFAULT);
                }
            } else if (OS_ANDROID) {
                abx.setStatusbarColor(ID_1800226270_statusbar);
            }
        };
        setearStatusColor(ID_1800226270_statusbar);

    };
    var ID_583987458 = setTimeout(ID_583987458_func, 1000 * 0.1);
})();


/** 
 * Cerramos esta pantalla cuando es llamada en la pantalla siguente (datosbasicos) 
 */

_my_events['_cerrar_insp,ID_1201771449'] = function(evento) {
    if (!_.isUndefined(evento.pantalla)) {
        if (evento.pantalla == 'hayalguien') {
            if (Ti.App.deployType != 'production') console.log('debug cerrando hayalguien', {});

            var ID_1436370236_trycatch = {
                error: function(e) {
                    if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
                }
            };
            try {
                ID_1436370236_trycatch.error = function(evento) {
                    if (Ti.App.deployType != 'production') console.log('error cerrando hayalguien', {});
                };
                $.ID_1800226270.close();
            } catch (e) {
                ID_1436370236_trycatch.error(e);
            }
        }
    } else {
        if (Ti.App.deployType != 'production') console.log('debug cerrando (todas) hayalguien', {});

        var ID_1534703081_trycatch = {
            error: function(e) {
                if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
            }
        };
        try {
            ID_1534703081_trycatch.error = function(evento) {
                if (Ti.App.deployType != 'production') console.log('error cerrando hayalguien', {});
            };
            $.ID_1800226270.close();
        } catch (e) {
            ID_1534703081_trycatch.error(e);
        }
    }
};
Alloy.Events.on('_cerrar_insp', _my_events['_cerrar_insp,ID_1201771449']);

function Androidback_ID_398704526(e) {

    e.cancelBubble = true;
    var elemento = e.source;

}

function Postlayout_ID_318159321(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var ID_1784406524_func = function() {
        /** 
         * Una vez ya cargada la tarea, dejamos la variable limpia para poder abrir otras pantallas 
         */
        require('vars')['var_abriendo'] = '';
    };
    var ID_1784406524 = setTimeout(ID_1784406524_func, 1000 * 0.2);

}
if (OS_IOS || OS_ANDROID) {
    $.ID_1800226270.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}
//$.ID_1800226270.open();