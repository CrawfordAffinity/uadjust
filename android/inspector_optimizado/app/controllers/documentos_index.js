var _bind4section = {};
var _list_templates = {};
var $documentos = $.documentos.toJSON();

var _activity;
if (OS_ANDROID) {
    _activity = $.ID_1068546356.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_1068546356';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_1068546356.addEventListener('open', function(e) {});
}
$.ID_1068546356.orientationModes = [Titanium.UI.PORTRAIT];


$.ID_1709091774.init({
    titulo: 'DOCUMENTOS',
    onsalirinsp: Salirinsp_ID_2069184044,
    __id: 'ALL1709091774',
    continuar: '',
    salir_insp: '',
    fondo: 'fondorosado',
    top: 0,
    onpresiono: Presiono_ID_80270477
});

function Salirinsp_ID_2069184044(e) {

    var evento = e;
    var ID_2045532719_opts = ['Asegurado no puede seguir', 'Se me acabo la bateria', 'Tuve un accidente', 'Cancelar'];
    var ID_2045532719 = Ti.UI.createOptionDialog({
        title: 'RAZON PARA CANCELAR INSPECCION ACTUAL',
        options: ID_2045532719_opts
    });
    ID_2045532719.addEventListener('click', function(e) {
        var resp = ID_2045532719_opts[e.index];
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var razon = "";
            if (resp == 'Asegurado no puede seguir') {
                razon = "Asegurado no puede seguir";
            }
            if (resp == 'Se me acabo la bateria') {
                razon = "Se me acabo la bateria";
            }
            if (resp == 'Tuve un accidente') {
                razon = "Tuve un accidente";
            }
            if (Ti.App.deployType != 'production') console.log('mi razon es', {
                "datos": razon
            });
            if ((_.isObject(razon) || (_.isString(razon)) && !_.isEmpty(razon)) || _.isNumber(razon) || _.isBoolean(razon)) {
                if (Ti.App.deployType != 'production') console.log('llamando servicio cancelarTarea', {});
                require('vars')['insp_cancelada'] = 'siniestro';
                Alloy.Collections[$.documentos.config.adapter.collection_name].add($.documentos);
                $.documentos.save();
                Alloy.Collections[$.documentos.config.adapter.collection_name].fetch();
                var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
                var ID_1809090752_visible = true;

                if (ID_1809090752_visible == 'si') {
                    ID_1809090752_visible = true;
                } else if (ID_1809090752_visible == 'no') {
                    ID_1809090752_visible = false;
                }
                $.ID_1809090752.setVisible(ID_1809090752_visible);

                var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
                var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
                if (Ti.App.deployType != 'production') console.log('detalle de seltarea', {
                    "data": seltarea
                });
                var datos = {
                    id_inspector: inspector.id_server,
                    codigo_identificador: inspector.codigo_identificador,
                    id_server: seltarea.id_server,
                    num_caso: seltarea.num_caso,
                    razon: razon
                };
                require('vars')[_var_scopekey]['datos'] = datos;
                var ID_1178779290 = {};
                ID_1178779290.success = function(e) {
                    var elemento = e,
                        valor = e;
                    var ID_1809090752_visible = false;

                    if (ID_1809090752_visible == 'si') {
                        ID_1809090752_visible = true;
                    } else if (ID_1809090752_visible == 'no') {
                        ID_1809090752_visible = false;
                    }
                    $.ID_1809090752.setVisible(ID_1809090752_visible);

                    Alloy.createController("firma_index", {}).getView().open();
                    var ID_1530763560_func = function() {
                        Alloy.Events.trigger('_cerrar_insp', {
                            pantalla: 'documentos'
                        });
                    };
                    var ID_1530763560 = setTimeout(ID_1530763560_func, 1000 * 0.5);
                    elemento = null, valor = null;
                };
                ID_1178779290.error = function(e) {
                    var elemento = e,
                        valor = e;
                    if (Ti.App.deployType != 'production') console.log('hubo error llamando cancelarTarea, la agregamos a cola de salida', {
                        "elemento": elemento
                    });
                    if (Ti.App.deployType != 'production') console.log('agregando servicio cancelarTarea a cola', {});
                    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
                    var datos = ('datos' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['datos'] : '';
                    var ID_1913824379_m = Alloy.Collections.cola;
                    var ID_1913824379_fila = Alloy.createModel('cola', {
                        data: JSON.stringify(datos),
                        id_tarea: seltarea.id_server,
                        tipo: 'cancelar'
                    });
                    ID_1913824379_m.add(ID_1913824379_fila);
                    ID_1913824379_fila.save();
                    var ID_1809090752_visible = false;

                    if (ID_1809090752_visible == 'si') {
                        ID_1809090752_visible = true;
                    } else if (ID_1809090752_visible == 'no') {
                        ID_1809090752_visible = false;
                    }
                    $.ID_1809090752.setVisible(ID_1809090752_visible);

                    Alloy.createController("firma_index", {}).getView().open();
                    var ID_851467821_func = function() {
                        Alloy.Events.trigger('_cerrar_insp', {
                            pantalla: 'documentos'
                        });
                    };
                    var ID_851467821 = setTimeout(ID_851467821_func, 1000 * 0.5);
                    elemento = null, valor = null;
                };
                require('helper').ajaxUnico('ID_1178779290', '' + url_server + 'cancelarTarea' + '', 'POST', {
                    id_inspector: seltarea.id_inspector,
                    codigo_identificador: inspector.codigo_identificador,
                    id_tarea: seltarea.id_server,
                    num_caso: seltarea.num_caso,
                    mensaje: razon,
                    opcion: 0,
                    tipo: 1
                }, 15000, ID_1178779290);
            }
        }
        resp = null;
        e.source.removeEventListener("click", arguments.callee);
    });
    ID_2045532719.show();

}

function Presiono_ID_80270477(e) {

    var evento = e;
    require('vars')[_var_scopekey]['alguno_on'] = 'false';
    if ($documentos.doc1 == 1 || $documentos.doc1 == '1') {
        require('vars')[_var_scopekey]['alguno_on'] = 'true';
    } else if ($documentos.doc2 == 1 || $documentos.doc2 == '1') {
        require('vars')[_var_scopekey]['alguno_on'] = 'true';
    } else if ($documentos.doc3 == 1 || $documentos.doc3 == '1') {
        require('vars')[_var_scopekey]['alguno_on'] = 'true';
    } else if ($documentos.doc4 == 1 || $documentos.doc4 == '1') {
        require('vars')[_var_scopekey]['alguno_on'] = 'true';
    } else if ($documentos.doc5 == 1 || $documentos.doc5 == '1') {
        require('vars')[_var_scopekey]['alguno_on'] = 'true';
    } else if ($documentos.doc6 == 1 || $documentos.doc6 == '1') {
        require('vars')[_var_scopekey]['alguno_on'] = 'true';
    } else if ($documentos.doc7 == 1 || $documentos.doc7 == '1') {
        require('vars')[_var_scopekey]['alguno_on'] = 'true';
    } else if ($documentos.doc8 == 1 || $documentos.doc8 == '1') {
        require('vars')[_var_scopekey]['alguno_on'] = 'true';
    } else if ($documentos.doc9 == 1 || $documentos.doc9 == '1') {
        require('vars')[_var_scopekey]['alguno_on'] = 'true';
    } else if ($documentos.doc10 == 1 || $documentos.doc10 == '1') {
        require('vars')[_var_scopekey]['alguno_on'] = 'true';
    }
    var alguno_on = ('alguno_on' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['alguno_on'] : '';
    if (Ti.App.deployType != 'production') console.log('PROBANDO1', {
        "prueba": alguno_on
    });
    if (alguno_on == true || alguno_on == 'true') {
        if (Ti.App.deployType != 'production') console.log('PROBANDO2', {
            "prueba": alguno_on,
            "los dias": $documentos.dias
        });
        if (_.isUndefined($documentos.dias)) {
            var ID_204137070_opts = ['Aceptar'];
            var ID_204137070 = Ti.UI.createAlertDialog({
                title: 'Atencion',
                message: 'Ingrese días de compromiso',
                buttonNames: ID_204137070_opts
            });
            ID_204137070.addEventListener('click', function(e) {
                var nulo = ID_204137070_opts[e.index];
                nulo = null;
                e.source.removeEventListener("click", arguments.callee);
            });
            ID_204137070.show();
            if (Ti.App.deployType != 'production') console.log('los dias no existen', {});
        } else if ((_.isObject($documentos.dias) || _.isString($documentos.dias)) && _.isEmpty($documentos.dias)) {
            var ID_1393433961_opts = ['Aceptar'];
            var ID_1393433961 = Ti.UI.createAlertDialog({
                title: 'Atencion',
                message: 'Ingrese días de compromiso',
                buttonNames: ID_1393433961_opts
            });
            ID_1393433961.addEventListener('click', function(e) {
                var nulo = ID_1393433961_opts[e.index];
                nulo = null;
                e.source.removeEventListener("click", arguments.callee);
            });
            ID_1393433961.show();
        } else {
            $.ID_1532893912.abrir({
                color: 'rosado'
            });
        }
    } else {
        $.ID_1532893912.abrir({
            color: 'rosado'
        });
    }
}

$.ID_1532893912.init({
    titulo: '¿EL ASEGURADO CONFIRMA ESTOS DATOS?',
    __id: 'ALL1532893912',
    si: 'SI, Están correctos',
    texto: 'El asegurado debe confirmar que los datos de esta sección están correctos',
    pantalla: '¿ESTA DE ACUERDO?',
    color: 'rosado',
    onsi: si_ID_2137507650,
    no: 'NO, Hay que modificar algo'
});

function si_ID_2137507650(e) {

    var evento = e;
    /** 
     * Ocupamos esta variable para impedir que la pantalla siguente se cargue dos veces 
     */
    var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
    if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
        require('vars')['var_abriendo'] = 'true';
        /** 
         * Guardamos el modelo con los datos del domicilio 
         */
        Alloy.Collections[$.documentos.config.adapter.collection_name].add($.documentos);
        $.documentos.save();
        Alloy.Collections[$.documentos.config.adapter.collection_name].fetch();
        Alloy.createController("firma_index", {}).getView().open();
    }

}

function Change_ID_2057149(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    if (elemento == true || elemento == 'true') {
        /** 
         * Dependiendo del estado del switch, actualizamos el estado del documento 
         */
        $.documentos.set({
            doc1: 1
        });
        if ('documentos' in $) $documentos = $.documentos.toJSON();
    } else {
        $.documentos.set({
            doc1: 0
        });
        if ('documentos' in $) $documentos = $.documentos.toJSON();
    }
    elemento = null;

}

function Change_ID_957493404(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    if (elemento == true || elemento == 'true') {
        $.documentos.set({
            doc2: 1
        });
        if ('documentos' in $) $documentos = $.documentos.toJSON();
    } else {
        if (Ti.App.deployType != 'production') console.log('el doc1 es falso', {});
        $.documentos.set({
            doc2: 0
        });
        if ('documentos' in $) $documentos = $.documentos.toJSON();
    }
    elemento = null;

}

function Change_ID_1344067892(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    if (elemento == true || elemento == 'true') {
        $.documentos.set({
            doc3: 1
        });
        if ('documentos' in $) $documentos = $.documentos.toJSON();
    } else {
        if (Ti.App.deployType != 'production') console.log('el doc1 es falso', {});
        $.documentos.set({
            doc3: 0
        });
        if ('documentos' in $) $documentos = $.documentos.toJSON();
    }
    elemento = null;

}

function Change_ID_330422946(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    if (elemento == true || elemento == 'true') {
        $.documentos.set({
            doc4: 1
        });
        if ('documentos' in $) $documentos = $.documentos.toJSON();
    } else {
        $.documentos.set({
            doc4: 0
        });
        if ('documentos' in $) $documentos = $.documentos.toJSON();
    }
    elemento = null;

}

function Change_ID_326579611(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    if (elemento == true || elemento == 'true') {
        $.documentos.set({
            doc5: 1
        });
        if ('documentos' in $) $documentos = $.documentos.toJSON();
    } else {
        $.documentos.set({
            doc5: 0
        });
        if ('documentos' in $) $documentos = $.documentos.toJSON();
    }
    elemento = null;

}

function Change_ID_1255141694(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    if (elemento == true || elemento == 'true') {
        $.documentos.set({
            doc6: 1
        });
        if ('documentos' in $) $documentos = $.documentos.toJSON();
    } else {
        $.documentos.set({
            doc6: 0
        });
        if ('documentos' in $) $documentos = $.documentos.toJSON();
    }
    elemento = null;

}

function Change_ID_477955829(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    if (elemento == true || elemento == 'true') {
        $.documentos.set({
            doc7: 1
        });
        if ('documentos' in $) $documentos = $.documentos.toJSON();
    } else {
        $.documentos.set({
            doc7: 0
        });
        if ('documentos' in $) $documentos = $.documentos.toJSON();
    }
    elemento = null;

}

function Change_ID_1252403414(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    if (elemento == true || elemento == 'true') {
        $.documentos.set({
            doc8: 1
        });
        if ('documentos' in $) $documentos = $.documentos.toJSON();
    } else {
        $.documentos.set({
            doc8: 0
        });
        if ('documentos' in $) $documentos = $.documentos.toJSON();
    }
    elemento = null;

}

function Change_ID_1867556353(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    if (elemento == true || elemento == 'true') {
        $.documentos.set({
            doc9: 1
        });
        if ('documentos' in $) $documentos = $.documentos.toJSON();
    } else {
        $.documentos.set({
            doc9: 0
        });
        if ('documentos' in $) $documentos = $.documentos.toJSON();
    }
    elemento = null;

}

function Change_ID_1106870128(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    if (elemento == true || elemento == 'true') {
        /** 
         * Actualizamos edificio y doc10 
         */
        $.documentos.set({
            doc10: 1,
            edificio: 1
        });
        if ('documentos' in $) $documentos = $.documentos.toJSON();
        require('vars')[_var_scopekey]['s1'] = '1';
    } else {
        $.documentos.set({
            edificio: 0
        });
        if ('documentos' in $) $documentos = $.documentos.toJSON();
        /** 
         * Recuperamos variable s2 para saber si el doc10 debe estar en 0 o 1 
         */
        var s2 = ('s2' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['s2'] : '';
        require('vars')[_var_scopekey]['s1'] = '0';
        if (s2 == 0 || s2 == '0') {
            $.documentos.set({
                doc10: 0
            });
            if ('documentos' in $) $documentos = $.documentos.toJSON();
        }
    }
    elemento = null;

}

function Change_ID_1880893238(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    if (elemento == true || elemento == 'true') {
        /** 
         * Actualizamos los contenidos y doc10 
         */
        $.documentos.set({
            doc10: 1,
            contenidos: 1
        });
        if ('documentos' in $) $documentos = $.documentos.toJSON();
        require('vars')[_var_scopekey]['s2'] = '1';
    } else {
        $.documentos.set({
            contenidos: 0
        });
        if ('documentos' in $) $documentos = $.documentos.toJSON();
        /** 
         * Recuperamos variable s1 para saber si el doc10 debe estar en 0 o 1 
         */
        var s1 = ('s1' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['s1'] : '';
        require('vars')[_var_scopekey]['s2'] = '0';
        if (s1 == 0 || s1 == '0') {
            $.documentos.set({
                doc10: 0
            });
            if ('documentos' in $) $documentos = $.documentos.toJSON();
        }
    }
    elemento = null;

}

function Change_ID_809938355(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    $.documentos.set({
        otros: elemento
    });
    if ('documentos' in $) $documentos = $.documentos.toJSON();
    elemento = null, source = null;

}

function Change_ID_503279996(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    /** 
     * Actualizamos la tabla con los dias para enviar los documentos 
     */
    $.documentos.set({
        dias: elemento
    });
    if ('documentos' in $) $documentos = $.documentos.toJSON();
    elemento = null, source = null;

}

function Return_ID_286897182(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    /** 
     * Al presionar enter en el teclado del equipo, desenfocamos la caja 
     */
    $.ID_744186086.blur();
    elemento = null, source = null;

}

function Click_ID_1654314198(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    /** 
     * Desenfocamos las cajas de texto 
     */
    $.ID_266265196.blur();
    $.ID_744186086.blur();

}

function Postlayout_ID_1349461170(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    $.ID_1216361550.show();

}

function Postlayout_ID_1636040712(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var ID_1707636476_func = function() {
        /** 
         * Una vez ya cargada la tarea, dejamos la variable limpia para poder abrir otras pantallas 
         */
        require('vars')['var_abriendo'] = '';
    };
    var ID_1707636476 = setTimeout(ID_1707636476_func, 1000 * 0.2);
    /** 
     * Cerramos pantalla contenidos 
     */
    Alloy.Events.trigger('_cerrar_insp', {
        pantalla: 'contenidos'
    });

}

var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
/** 
 * Cerramos esta pantalla cuando la firma se ha ejecutado 
 */
_my_events['_cerrar_insp,ID_511339808'] = function(evento) {
    if (evento.pantalla == 'documentos') {
        var ID_962933245_trycatch = {
            error: function(e) {
                if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
            }
        };
        try {
            ID_962933245_trycatch.error = function(evento) {};
            $.ID_1068546356.close();
        } catch (e) {
            ID_962933245_trycatch.error(e);
        }
    }
};
Alloy.Events.on('_cerrar_insp', _my_events['_cerrar_insp,ID_511339808']);
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
    /** 
     * Inicializamos la tabla con datos default 
     */
    $.documentos.set({
        id_inspeccion: seltarea.id_server,
        doc10: 0,
        doc6: 0,
        doc4: 0,
        doc7: 0,
        doc3: 0,
        edificio: 0,
        doc9: 0,
        dias: '',
        doc1: 0,
        doc5: 0,
        contenidos: 0,
        otros: '',
        doc2: 0,
        doc8: 0
    });
    if ('documentos' in $) $documentos = $.documentos.toJSON();
}

function Androidback_ID_2139645792(e) {

    e.cancelBubble = true;
    var elemento = e.source;

}

(function() {
    /** 
     * Inicializamos flag para determinar si el doc10 tiene que estar en 0 o 1 
     */
    require('vars')[_var_scopekey]['s1'] = '0';
    require('vars')[_var_scopekey]['s2'] = '0';
    /** 
     * Avisamos que la inspeccion no fue cancelada 
     */
    require('vars')['insp_cancelada'] = '';
    /** 
     * Cambiamos el color del statusbar 
     */
    var ID_1714814641_func = function() {
        var ID_1068546356_statusbar = '#E87C7C';

        var setearStatusColor = function(ID_1068546356_statusbar) {
            if (OS_IOS) {
                if (ID_1068546356_statusbar == 'light' || ID_1068546356_statusbar == 'claro') {
                    $.ID_1068546356_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.LIGHT_CONTENT);
                } else if (ID_1068546356_statusbar == 'grey' || ID_1068546356_statusbar == 'gris' || ID_1068546356_statusbar == 'gray') {
                    $.ID_1068546356_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.GREY);
                } else if (ID_1068546356_statusbar == 'oscuro' || ID_1068546356_statusbar == 'dark') {
                    $.ID_1068546356_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.DEFAULT);
                }
            } else if (OS_ANDROID) {
                abx.setStatusbarColor(ID_1068546356_statusbar);
            }
        };
        setearStatusColor(ID_1068546356_statusbar);

    };
    var ID_1714814641 = setTimeout(ID_1714814641_func, 1000 * 0.1);
})();

if (OS_IOS || OS_ANDROID) {
    $.ID_1068546356.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}
//$.ID_1068546356.open();