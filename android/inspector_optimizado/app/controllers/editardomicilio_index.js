var _bind4section = {};
var _list_templates = {};
var $inspector = $.inspector.toJSON();

var _activity;
if (OS_ANDROID) {
    _activity = $.ID_2113107325.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_2113107325';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_2113107325.addEventListener('open', function(e) {});
}
$.ID_2113107325.orientationModes = [Titanium.UI.PORTRAIT];


var ID_1217880739_like = function(search) {
    if (typeof search !== 'string' || this === null) {
        return false;
    }
    search = search.replace(new RegExp("([\\.\\\\\\+\\*\\?\\[\\^\\]\\$\\(\\)\\{\\}\\=\\!\\<\\>\\|\\:\\-])", "g"), "\\$1");
    search = search.replace(/%/g, '.*').replace(/_/g, '.');
    return RegExp('^' + search + '$', 'gi').test(this);
};
var ID_1217880739_filter = function(coll) {
    var filtered = _.toArray(coll.filter(function(m) {
        return true;
    }));
    return filtered;
};
var ID_1217880739_transform = function(model) {
    var fila = model.toJSON();
    return fila;
};
var ID_1217880739_update = function(e) {};
_.defer(function() {
    Alloy.Collections.pais.fetch();
});
Alloy.Collections.pais.on('add change delete', function(ee) {
    ID_1217880739_update(ee);
});
Alloy.Collections.pais.fetch();


$.ID_1388317411.init({
    titulo: 'EDITAR',
    __id: 'ALL1388317411',
    fondo: 'fondoblanco',
    continuarazul: '',
    colortitulo: 'negro',
    modal: '',
    onpresiono: Presiono_ID_1840280451
});

function Presiono_ID_1840280451(e) {

    var evento = e;
    var pais_seleccionado = ('pais_seleccionado' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['pais_seleccionado'] : '';
    if (Ti.App.deployType != 'production') console.log('el pais seleccionado es', {
        "datos": pais_seleccionado
    });
    if ((_.isObject(pais_seleccionado) || _.isString(pais_seleccionado)) && _.isEmpty(pais_seleccionado)) {
        /** 
         * Avisamos al usuario en el caso de que no haya seleccionado un pais, caso contrario actualizamos la tabla con el pais seleccionado y continuamos a la proxima pantalla 
         */
        var ID_1976772314_opts = ['Aceptar'];
        var ID_1976772314 = Ti.UI.createAlertDialog({
            title: 'Atencion',
            message: 'Debe seleccionar un pais',
            buttonNames: ID_1976772314_opts
        });
        ID_1976772314.addEventListener('click', function(e) {
            var abc = ID_1976772314_opts[e.index];
            abc = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1976772314.show();
    } else {
        $.inspector.set({
            pais: pais_seleccionado.id_server
        });
        if ('inspector' in $) $inspector = $.inspector.toJSON();
        if (Ti.App.deployType != 'production') console.log('el pais a guardar es', {
            "datos": pais_seleccionado.id_server
        });
        Alloy.Collections[$.inspector.config.adapter.collection_name].add($.inspector);
        $.inspector.save();
        Alloy.Collections[$.inspector.config.adapter.collection_name].fetch();
        /** 
         * Creamos un flag para evitar que la pantalla se abra dos veces 
         */
        var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
        if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
            require('vars')['var_abriendo'] = 'true';
            Alloy.createController("editar_domicilio", {}).getView().open();
            $.ID_2113107325.close();
        }
    }
}

$.ID_1923821039.init({
    titulo: 'Editar país',
    __id: 'ALL1923821039',
    avance: '',
    onclick: Click_ID_1306933895
});

function Click_ID_1306933895(e) {

    var evento = e;

}


function Change_ID_1154562684(e) {

    e.cancelBubble = true;
    var elemento = e;
    var _columna = e.columnIndex;
    var columna = e.columnIndex + 1;
    var _fila = e.rowIndex;
    var fila = e.rowIndex + 1;
    var modelo = require('helper').query2array(Alloy.Collections.pais)[e.rowIndex];
    _.defer(function(modelo) {
        /** 
         * Guardamos el pais seleccionado en el picker 
         */
        require('vars')[_var_scopekey]['pais_seleccionado'] = modelo;
    }, modelo);

}

(function() {
    var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
    var ID_244825631 = {};
    ID_244825631.success = function(e) {
        var elemento = e,
            valor = e;
        if (elemento == false || elemento == 'false') {
            /** 
             * Si en la consulta los datos obtenidos no existen, mostramos mensaje para decir que hubo problemas al obtener los datos. Caso contrario, limpiamos tablas y cargamos los datos obtenidos desde el servidor en las tablas 
             */
            var ID_433094214_opts = ['Aceptar'];
            var ID_433094214 = Ti.UI.createAlertDialog({
                title: 'Alerta',
                message: 'Problema de conexión por favor intentar después',
                buttonNames: ID_433094214_opts
            });
            ID_433094214.addEventListener('click', function(e) {
                var suu = ID_433094214_opts[e.index];
                suu = null;
                e.source.removeEventListener("click", arguments.callee);
            });
            ID_433094214.show();
        } else {
            var ID_1183843717_i = Alloy.Collections.pais;
            var sql = "DELETE FROM " + ID_1183843717_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_1183843717_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_1183843717_i.trigger('delete');
            var ID_900784705_i = Alloy.Collections.nivel1;
            var sql = "DELETE FROM " + ID_900784705_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_900784705_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_900784705_i.trigger('delete');
            var ID_524627090_i = Alloy.Collections.experiencia_oficio;
            var sql = "DELETE FROM " + ID_524627090_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_524627090_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_524627090_i.trigger('delete');
            var elemento_regiones = elemento.regiones;
            var ID_1042041424_m = Alloy.Collections.nivel1;
            var db_ID_1042041424 = Ti.Database.open(ID_1042041424_m.config.adapter.db_name);
            db_ID_1042041424.execute('BEGIN');
            _.each(elemento_regiones, function(ID_1042041424_fila, pos) {
                db_ID_1042041424.execute('INSERT INTO nivel1 (id_label, id_server, dif_horaria, id_pais) VALUES (?,?,?,?)', ID_1042041424_fila.subdivision_name, ID_1042041424_fila.id, 2, ID_1042041424_fila.id_pais);
            });
            db_ID_1042041424.execute('COMMIT');
            db_ID_1042041424.close();
            db_ID_1042041424 = null;
            ID_1042041424_m.trigger('change');
            var elemento_paises = elemento.paises;
            var ID_711357707_m = Alloy.Collections.pais;
            var db_ID_711357707 = Ti.Database.open(ID_711357707_m.config.adapter.db_name);
            db_ID_711357707.execute('BEGIN');
            _.each(elemento_paises, function(ID_711357707_fila, pos) {
                db_ID_711357707.execute('INSERT INTO pais (label_nivel2, moneda, nombre, label_nivel4, label_codigo_identificador, label_nivel3, id_server, label_nivel1, iso, sis_metrico, niveles_pais, id_pais, label_nivel5, lenguaje) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)', ID_711357707_fila.nivel_2, ID_711357707_fila.moneda, ID_711357707_fila.nombre, ID_711357707_fila.nivel_4, ID_711357707_fila.label_codigo_identificador, ID_711357707_fila.nivel_3, ID_711357707_fila.id, ID_711357707_fila.nivel_1, ID_711357707_fila.iso, ID_711357707_fila.sis_metrico, ID_711357707_fila.niveles_pais, ID_711357707_fila.idpais, ID_711357707_fila.nivel_5, ID_711357707_fila.lenguaje);
            });
            db_ID_711357707.execute('COMMIT');
            db_ID_711357707.close();
            db_ID_711357707 = null;
            ID_711357707_m.trigger('change');
            var elemento_experiencia_oficio = elemento.experiencia_oficio;
            var ID_1581092805_m = Alloy.Collections.experiencia_oficio;
            var db_ID_1581092805 = Ti.Database.open(ID_1581092805_m.config.adapter.db_name);
            db_ID_1581092805.execute('BEGIN');
            _.each(elemento_experiencia_oficio, function(ID_1581092805_fila, pos) {
                db_ID_1581092805.execute('INSERT INTO experiencia_oficio (nombre, id_server, id_pais) VALUES (?,?,?)', ID_1581092805_fila.nombre, ID_1581092805_fila.id, ID_1581092805_fila.idpais);
            });
            db_ID_1581092805.execute('COMMIT');
            db_ID_1581092805.close();
            db_ID_1581092805 = null;
            ID_1581092805_m.trigger('change');
        }
        if (Ti.App.deployType != 'production') console.log('trajo datos de obtenerpais', {});
        elemento = null, valor = null;
    };
    ID_244825631.error = function(e) {
        var elemento = e,
            valor = e;
        var ID_370190194_opts = ['Aceptar'];
        var ID_370190194 = Ti.UI.createAlertDialog({
            title: 'Alerta',
            message: 'Problema de conexión por favor intentar después',
            buttonNames: ID_370190194_opts
        });
        ID_370190194.addEventListener('click', function(e) {
            var suu = ID_370190194_opts[e.index];
            suu = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_370190194.show();
        elemento = null, valor = null;
    };
    require('helper').ajaxUnico('ID_244825631', '' + url_server + 'obtenerPais' + '', 'POST', {}, 15000, ID_244825631);
    /** 
     * heredamos datos previos del inspector en modelo nuevo con binding de esta pantalla 
     */
    var ID_1505784545_i = Alloy.createCollection('inspectores');
    var ID_1505784545_i_where = '';
    ID_1505784545_i.fetch();
    var inspector_viejo = require('helper').query2array(ID_1505784545_i);
    if (inspector_viejo && inspector_viejo.length) {
        /** 
         * Guardamos variable con el dato previo del pais del inspector y cargamos los datos en binding 
         */
        require('vars')[_var_scopekey]['pais_seleccionado'] = inspector_viejo[0].pais;
        $.inspector.set({
            apellido_materno: inspector_viejo[0].apellido_materno,
            id_nivel1: inspector_viejo[0].id_nivel1,
            lat_dir: inspector_viejo[0].lat_dir,
            disponibilidad_viajar_pais: inspector_viejo[0].disponibilidad_viajar_pais,
            uuid: inspector_viejo[0].uidd,
            fecha_nacimiento: inspector_viejo[0].fecha_nacimiento,
            d1: inspector_viejo[0].d1,
            d2: inspector_viejo[0].d2,
            password: inspector_viejo[0].password,
            pais: inspector_viejo[0].pais,
            direccion: inspector_viejo[0].direccion,
            d3: inspector_viejo[0].d3,
            nivel3: inspector_viejo[0].nivel3,
            d5: inspector_viejo[0].d5,
            d4: inspector_viejo[0].d4,
            disponibilidad_fechas: inspector_viejo[0].disponibilidad_fechas,
            d7: inspector_viejo[0].d7,
            nivel4: inspector_viejo[0].nivel4,
            nombre: inspector_viejo[0].nombre,
            nivel5: inspector_viejo[0].nivel5,
            nivel2: inspector_viejo[0].nivel2,
            disponibilidad_horas: inspector_viejo[0].disponibilidad_horas,
            disponibilidad_viajar_ciudad: inspector_viejo[0].disponibilidad_viajar_ciudad,
            lon_dir: inspector_viejo[0].lon_dir,
            id_server: inspector_viejo[0].id_server,
            d6: inspector_viejo[0].d6,
            telefono: inspector_viejo[0].telefono,
            experiencia_detalle: inspector_viejo[0].experiencia_detalle,
            disponibilidad: inspector_viejo[0].disponibilidad,
            codigo_identificador: inspector_viejo[0].codigo_identificador,
            experiencia_oficio: inspector_viejo[0].experiencia_oficio,
            direccion_correccion: inspector_viejo[0].direccion_correccion,
            apellido_paterno: inspector_viejo[0].apellido_paterno,
            correo: inspector_viejo[0].correo
        });
        if ('inspector' in $) $inspector = $.inspector.toJSON();
    } else {
        /** 
         * Si no existe inspector (en el caso de que esten haciendo pruebas locales) cargamos datos dummy 
         */
        $.inspector.set({
            apellido_materno: 'Huerta',
            id_nivel1: 21,
            lat_dir: -123123,
            disponibilidad_viajar_pais: 0,
            uuid: 'uuid-telefono-pruebas',
            fecha_nacimiento: '09-05-1994',
            d1: 1,
            d2: 1,
            password: 'test',
            pais: 1,
            direccion: 'Sta Isabel 951',
            d3: 1,
            nivel3: 'Santiago',
            d5: 1,
            d4: 1,
            disponibilidad_fechas: 1,
            d7: 1,
            nivel4: 4,
            nombre: 'Elias',
            nivel5: 5,
            nivel2: 'Santiago',
            disponibilidad_horas: 0,
            disponibilidad_viajar_ciudad: 1,
            lon_dir: -123123,
            id_server: 123,
            d6: 1,
            telefono: 950093248,
            experiencia_detalle: 'Sin experiencia',
            disponibilidad: 1,
            codigo_identificador: '18.373.278-1',
            experiencia_oficio: 1,
            direccion_correccion: 1,
            apellido_paterno: 'Baeza',
            correo: 'elias@creador.cl'
        });
        if ('inspector' in $) $inspector = $.inspector.toJSON();
    }
})();

function Postlayout_ID_1674491374(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var ID_1286223546_func = function() {
        /** 
         * Una vez ya cargada la tarea, dejamos la variable limpia para poder abrir otras pantallas 
         */
        require('vars')['var_abriendo'] = '';
    };
    var ID_1286223546 = setTimeout(ID_1286223546_func, 1000 * 0.2);

}
if (OS_IOS || OS_ANDROID) {
    $.ID_2113107325.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}
$.ID_2113107325.open();