var _bind4section = {};
var _list_templates = {
    "elemento": {
        "ID_432412973": {},
        "ID_291370846": {
            "text": "{nombre}"
        },
        "ID_1925487537": {
            "text": "{id_server}"
        },
        "ID_975786586": {
            "text": "{id_segured}"
        },
        "ID_1546190443": {}
    },
    "dano": {
        "ID_1569764493": {
            "text": "{id}"
        },
        "ID_304218376": {},
        "ID_1912165224": {
            "text": "{nombre}"
        }
    },
    "pborrar": {
        "ID_1236907100": {
            "text": "{id}"
        },
        "ID_1506538078": {},
        "ID_2024360383": {},
        "ID_1765564351": {},
        "ID_1699280427": {
            "text": "{nombre}"
        },
        "ID_1445903745": {},
        "ID_1795050796": {},
        "ID_1752901998": {},
        "ID_1914367022": {
            "text": "{nombre}"
        },
        "ID_1249074865": {
            "text": "{id}"
        },
        "ID_1131557960": {},
        "ID_1361874287": {},
        "ID_1750057271": {},
        "ID_1962135207": {},
        "ID_1947166298": {
            "text": "{nombre}"
        },
        "ID_2008605730": {
            "text": "{id}"
        },
        "ID_1788775425": {},
        "ID_1246915111": {}
    },
    "contenido": {
        "ID_630800887": {},
        "ID_862352109": {
            "text": "{id}"
        },
        "ID_560452274": {
            "text": "{nombre}"
        }
    },
    "recinto": {
        "ID_1960582596": {},
        "ID_871581291": {
            "text": "{nombre}"
        },
        "ID_310297848": {
            "text": "{id}"
        }
    },
    "nivel": {
        "ID_1021786928": {
            "text": "{id}"
        },
        "ID_1241655463": {
            "text": "{nombre}"
        },
        "ID_599581692": {}
    }
};

var _activity;
if (OS_ANDROID) {
    _activity = $.ID_411389157.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_411389157';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_411389157.addEventListener('open', function(e) {});
}
$.ID_411389157.orientationModes = [Titanium.UI.PORTRAIT];


$.ID_1120264064.init({
    titulo: 'DETALLE DE TAREA',
    __id: 'ALL1120264064',
    info: '',
    fondo: 'fondoblanco',
    colortitulo: 'negro',
    modal: '',
    onpresiono: Presiono_ID_2025187906
});

function Presiono_ID_2025187906(e) {

    var evento = e;
    /** 
     * Eenviamos a ver datos del asegurado 
     */
    var nulo = Alloy.createController("datosasegurado_index", {}).getView();
    nulo.open({
        modal: true
    });

    nulo = null;

}

$.ID_1000885851.init({
    __id: 'ALL1000885851',
    externo: true,
    onlisto: Listo_ID_2120119169,
    label_a: 'a',
    onerror: Error_ID_1598769213,
    _bono: 'Bono adicional:',
    bono: '',
    ondatos: Datos_ID_1357484229
});

function Listo_ID_2120119169(e) {

    var evento = e;
    if (Ti.App.deployType != 'production') console.log('widget mapa ha llamado evento \'listo\'', {});

}

function Error_ID_1598769213(e) {

    var evento = e;
    if (Ti.App.deployType != 'production') console.log('ha ocurrido un error con el mapa', {
        "evento": evento
    });

}

function Datos_ID_1357484229(e) {

    var evento = e;
    if (Ti.App.deployType != 'production') console.log('datos recibidos desde widget mapa: ' + evento.ruta_distancia, {
        "evento": evento
    });

}

function Longpress_ID_1611955468(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var bloquear_inspeccion = ('bloquear_inspeccion' in require('vars')) ? require('vars')['bloquear_inspeccion'] : '';
    if (Ti.App.deployType != 'production') console.log('psb longpress en boton', {
        "bloquear": bloquear_inspeccion
    });
    if (bloquear_inspeccion == false || bloquear_inspeccion == 'false') {
        /** 
         * Recuperamos variables. seltarea contiene el detalle de la tarea seleccionada, url_server contiene la url de uadjust, inspector contiene el detalle del inspector, iniciar_seguimiento flag para cambiar el estado del seguimiento, long_activo flag para el cambio de color del boton 
         */
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
        var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
        var iniciar_seguimiento = ('iniciar_seguimiento' in require('vars')) ? require('vars')['iniciar_seguimiento'] : '';
        require('vars')['long_activo'] = 'true';
        if (Ti.App.deployType != 'production') console.log('psb longpress en boton insp-false', {
            "bloquear": bloquear_inspeccion
        });
        if (iniciar_seguimiento == true || iniciar_seguimiento == 'true') {
            if (Ti.App.deployType != 'production') console.log('psb boton dice iniciar_seguimiento en true, cambiamos cosas', {
                "bloquear": bloquear_inspeccion
            });
            /** 
             * Informamos que debemos seguir esta tarea 
             */
            require('vars')['seguir_tarea'] = seltarea.id_server;
            /** 
             * Necesario para refrescar tareas en cierre de ventana 
             */
            require('vars')['seguimiento_cambiado'] = 'true';
            /** 
             * Actualizamos tabla local, refrescamos tareas y transformamos en mantener para iniciar. Cambiamos todas las tareas nuestras que tienen estado 4 a estado 3 (por si otra tarea tenia seguimiento antes) 
             */
            var ID_892467957_i = Alloy.createCollection('tareas');
            var ID_892467957_i_where = 'estado_tarea=4';
            ID_892467957_i.fetch({
                query: 'SELECT * FROM tareas WHERE estado_tarea=4'
            });
            var tareas = require('helper').query2array(ID_892467957_i);
            var db = Ti.Database.open(ID_892467957_i.config.adapter.db_name);
            if (ID_892467957_i_where == '') {
                var sql = 'UPDATE ' + ID_892467957_i.config.adapter.collection_name + ' SET estado_tarea=3';
            } else {
                var sql = 'UPDATE ' + ID_892467957_i.config.adapter.collection_name + ' SET estado_tarea=3 WHERE ' + ID_892467957_i_where;
            }
            db.execute(sql);
            sql = null;
            db.close();
            db = null;
            var ID_247745160_i = Alloy.createCollection('tareas');
            var ID_247745160_i_where = 'id=\'' + seltarea.id + '\'';
            ID_247745160_i.fetch({
                query: 'SELECT * FROM tareas WHERE id=\'' + seltarea.id + '\''
            });
            var tareas = require('helper').query2array(ID_247745160_i);
            var db = Ti.Database.open(ID_247745160_i.config.adapter.db_name);
            if (ID_247745160_i_where == '') {
                var sql = 'UPDATE ' + ID_247745160_i.config.adapter.collection_name + ' SET estado_tarea=4';
            } else {
                var sql = 'UPDATE ' + ID_247745160_i.config.adapter.collection_name + ' SET estado_tarea=4 WHERE ' + ID_247745160_i_where;
            }
            db.execute(sql);
            sql = null;
            db.close();
            db = null;
            /** 
             * Refrescamos tareas de menu 
             */
            var ID_1674305326_func = function() {

                Alloy.Events.trigger('_refrescar_tareas');
            };
            var ID_1674305326 = setTimeout(ID_1674305326_func, 1000 * 0.2);
            /** 
             * Mostramos monito 
             */

            $.ID_1000885851.update({
                monito: true
            });
            require('vars')['long_activo'] = 'false';
            /** 
             * transformamos en boton azul de mantener para iniciar 
             */
            require('vars')['iniciar_seguimiento'] = 'false';
            if (Ti.App.deployType != 'production') console.log('iniciar seguimiento en false', {});
            var ID_1935223870_estilo = 'fondoazul';

            var setEstilo = function(clase) {
                if ('styles' in require('a4w') && clase in require('a4w').styles['classes']) {
                    try {
                        $.ID_1935223870.applyProperties(require('a4w').styles['classes'][clase]);
                    } catch (sete_err) {}
                }
            };
            setEstilo(ID_1935223870_estilo);

            $.ID_1935223870.setBorderColor('#2D9EDB');

            var mantener_iniciar = 'MANTENER PARA INICIAR';
            $.ID_1289130430.setText(mantener_iniciar);

            var tarea_inspeccionar = 'Nunca dejes una tarea sin inspeccionar';

            $.ID_1041134629.update({
                texto: tarea_inspeccionar
            });
            var proceso_insp = 'Se iniciara el proceso de inspeccion';
            $.ID_509734203.setText(proceso_insp);

            var gps_error = ('gps_error' in require('vars')) ? require('vars')['gps_error'] : '';
            if (gps_error == true || gps_error == 'true') {
                /** 
                 * Avisamos a backend la ubicacion del inspector 
                 */
                var ID_122407932 = {};

                ID_122407932.success = function(e) {
                    var elemento = e,
                        valor = e;
                    if (Ti.App.deployType != 'production') console.log('respuesta exitosa de guardarUbicacion en iniciarSeguimiento', {
                        "datos": elemento
                    });
                    elemento = null, valor = null;
                };

                ID_122407932.error = function(e) {
                    var elemento = e,
                        valor = e;
                    if (Ti.App.deployType != 'production') console.log('respuesta fallida de guardarUbicacion en iniciarSeguimiento', {
                        "datos": elemento
                    });
                    elemento = null, valor = null;
                };
                require('helper').ajaxUnico('ID_122407932', '' + url_server + 'guardarUbicacion' + '', 'POST', {
                    id_inspector: inspector.id_server,
                    id_tarea: seltarea.id_server,
                    lat: -1,
                    lon: -1
                }, 15000, ID_122407932);
            } else {
                var gps_latitud = ('gps_latitud' in require('vars')) ? require('vars')['gps_latitud'] : '';
                var gps_longitud = ('gps_longitud' in require('vars')) ? require('vars')['gps_longitud'] : '';
                var ID_1054961310 = {};

                ID_1054961310.success = function(e) {
                    var elemento = e,
                        valor = e;
                    if (Ti.App.deployType != 'production') console.log('respuesta exitosa de guardarUbicacion en iniciarSeguimiento', {
                        "datos": elemento
                    });
                    elemento = null, valor = null;
                };

                ID_1054961310.error = function(e) {
                    var elemento = e,
                        valor = e;
                    if (Ti.App.deployType != 'production') console.log('respuesta fallida de guardarUbicacion en iniciarSeguimiento', {
                        "datos": elemento
                    });
                    elemento = null, valor = null;
                };
                require('helper').ajaxUnico('ID_1054961310', '' + url_server + 'guardarUbicacion' + '', 'POST', {
                    id_inspector: inspector.id_server,
                    id_tarea: seltarea.id_server,
                    lat: gps_latitud,
                    lon: gps_longitud
                }, 15000, ID_1054961310);
            }
        } else {
            var ID_1935223870_estilo = 'fondoazul';

            var setEstilo = function(clase) {
                if ('styles' in require('a4w') && clase in require('a4w').styles['classes']) {
                    try {
                        $.ID_1935223870.applyProperties(require('a4w').styles['classes'][clase]);
                    } catch (sete_err) {}
                }
            };
            setEstilo(ID_1935223870_estilo);

            $.ID_1935223870.setBorderColor('#2D9EDB');

            if (Ti.App.deployType != 'production') console.log('alguna razon para que se cambie el monito', {});
            Alloy.createController("fotosrequeridas_index", {
                '_open': 'false'
            }).getView();
        }
    }

}

function Touchstart_ID_774746224(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var bloquear_inspeccion = ('bloquear_inspeccion' in require('vars')) ? require('vars')['bloquear_inspeccion'] : '';
    if (bloquear_inspeccion == false || bloquear_inspeccion == 'false') {
        var iniciar_seguimiento = ('iniciar_seguimiento' in require('vars')) ? require('vars')['iniciar_seguimiento'] : '';
        if (iniciar_seguimiento == true || iniciar_seguimiento == 'true') {
            var ID_1935223870_estilo = 'fondonaranjo';

            var setEstilo = function(clase) {
                if ('styles' in require('a4w') && clase in require('a4w').styles['classes']) {
                    try {
                        $.ID_1935223870.applyProperties(require('a4w').styles['classes'][clase]);
                    } catch (sete_err) {}
                }
            };
            setEstilo(ID_1935223870_estilo);

            $.ID_1935223870.setBorderColor('#FCBD83');

        } else {
            var ID_1935223870_estilo = 'fondoceleste';

            var setEstilo = function(clase) {
                if ('styles' in require('a4w') && clase in require('a4w').styles['classes']) {
                    try {
                        $.ID_1935223870.applyProperties(require('a4w').styles['classes'][clase]);
                    } catch (sete_err) {}
                }
            };
            setEstilo(ID_1935223870_estilo);

            $.ID_1935223870.setBorderColor('#8BC9E8');

        }
    }

}

function Touchend_ID_1434380729(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    /** 
     * Dejamos boton como estaba 
     */
    var bloquear_inspeccion = ('bloquear_inspeccion' in require('vars')) ? require('vars')['bloquear_inspeccion'] : '';
    var long_activo = ('long_activo' in require('vars')) ? require('vars')['long_activo'] : '';
    if (bloquear_inspeccion == false || bloquear_inspeccion == 'false') {
        if (long_activo == false || long_activo == 'false') {
            var iniciar_seguimiento = ('iniciar_seguimiento' in require('vars')) ? require('vars')['iniciar_seguimiento'] : '';
            if (iniciar_seguimiento == true || iniciar_seguimiento == 'true') {
                var ID_1935223870_estilo = 'fondoamarillo';

                var setEstilo = function(clase) {
                    if ('styles' in require('a4w') && clase in require('a4w').styles['classes']) {
                        try {
                            $.ID_1935223870.applyProperties(require('a4w').styles['classes'][clase]);
                        } catch (sete_err) {}
                    }
                };
                setEstilo(ID_1935223870_estilo);

                $.ID_1935223870.setBorderColor('#F8DA54');

            } else {
                var ID_1935223870_estilo = 'fondoazul';

                var setEstilo = function(clase) {
                    if ('styles' in require('a4w') && clase in require('a4w').styles['classes']) {
                        try {
                            $.ID_1935223870.applyProperties(require('a4w').styles['classes'][clase]);
                        } catch (sete_err) {}
                    }
                };
                setEstilo(ID_1935223870_estilo);

                $.ID_1935223870.setBorderColor('#2D9EDB');

            }
        }
    }

}

$.ID_1041134629.init({
    __id: 'ALL1041134629',
    texto: 'Tip: Nunca dejes una tarea sin inspeccionar. El no hacerlo te afecta directamente',
    top: 10,
    tipo: '_tip'
});


(function() {
    if (Ti.App.deployType != 'production') console.log('argumentos detalletarea', {
        "modelo": args
    });
    require('vars')['long_activo'] = 'false';
    require('vars')['seguimiento_cambiado'] = 'false';
    /** 
     * Filtramos el id de la tarea con los parameros desde la pantalla anterior 
     */
    var ID_509195037_i = Alloy.createCollection('tareas');
    var ID_509195037_i_where = 'id=\'' + args._id + '\'';
    ID_509195037_i.fetch({
        query: 'SELECT * FROM tareas WHERE id=\'' + args._id + '\''
    });
    var tareas = require('helper').query2array(ID_509195037_i);
    if (tareas && tareas.length) {
        /** 
         * Esto es util para la sub-pantalla datos del asegurado 
         */
        require('vars')['seltarea'] = tareas[0];
        /** 
         * Recuperamos los valores defaults 
         */
        var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
        require('vars')['bloquear_inspeccion'] = 'false';
        if (Ti.App.deployType != 'production') console.log('adaptamos datos', {});
        /** 
         * Creamos variable de estructura para cargar datos de la tarea 
         */
        var info = {
            direccion: tareas[0].direccion,
            ciudad_pais: tareas[0].nivel_2 + ', ' + tareas[0].pais_texto,
            distancia: tareas[0].distance
        };
        if ((_.isObject(tareas[0].nivel_2) || _.isString(tareas[0].nivel_2)) && _.isEmpty(tareas[0].nivel_2)) {
            /** 
             * Definimos el ultimo nivel de la tarea y lo guardamos en la estructura como comuna 
             */
            var info = _.extend(info, {
                comuna: tareas[0].nivel_1
            });
        } else if ((_.isObject(tareas[0].nivel_3) || _.isString(tareas[0].nivel_3)) && _.isEmpty(tareas[0].nivel_3)) {
            var info = _.extend(info, {
                comuna: tareas[0].nivel_2
            });
        } else if ((_.isObject(tareas[0].nivel_4) || _.isString(tareas[0].nivel_4)) && _.isEmpty(tareas[0].nivel_4)) {
            var info = _.extend(info, {
                comuna: tareas[0].nivel_3
            });
        } else if ((_.isObject(tareas[0].nivel_5) || _.isString(tareas[0].nivel_5)) && _.isEmpty(tareas[0].nivel_5)) {
            var info = _.extend(info, {
                comuna: tareas[0].nivel_4
            });
        } else {
            var info = _.extend(info, {
                comuna: tareas[0].nivel_5
            });
        }
        if (Ti.App.deployType != 'production') console.log('actualizamos mapa con direccion', {});
        /** 
         * Actualizamos widget de mapa con los datos de la tarea 
         */

        $.ID_1000885851.update({
            tipo: 'ubicacion',
            latitud: tareas[0].lat,
            longitud: tareas[0].lon,
            ruta: true,
            direccion: info.direccion,
            comuna: info.comuna,
            ciudad: info.ciudad_pais,
            distancia: info.distancia,
            externo: true
        });
        /** 
         * Si seguir_tarea no es igual a nuestro id_server: significa que no estamos siguiendola por lo que boton debe decir iniciar seguimiento, en caso contrario, debemos mostrar monito porque se esta siguiendo. 
         */
        var seguir_tarea = ('seguir_tarea' in require('vars')) ? require('vars')['seguir_tarea'] : '';
        if (seguir_tarea != tareas[0].id_server) {
            var ID_1935223870_estilo = 'fondoamarillo';

            var setEstilo = function(clase) {
                if ('styles' in require('a4w') && clase in require('a4w').styles['classes']) {
                    try {
                        $.ID_1935223870.applyProperties(require('a4w').styles['classes'][clase]);
                    } catch (sete_err) {}
                }
            };
            setEstilo(ID_1935223870_estilo);

            $.ID_1935223870.setBorderColor('#F8DA54');

            var ini_seguimiento = 'INICIAR TRAYECTO';
            $.ID_1289130430.setText(ini_seguimiento);

            var ini_aviso = 'Tip: Al iniciar seguimiento, le avisaremos al cliente que vas en camino.';

            $.ID_1041134629.update({
                texto: ini_aviso
            });
            var presionado_iniciar = 'Mantenga presionado para iniciar Trayecto';
            $.ID_509734203.setText(presionado_iniciar);

            require('vars')['iniciar_seguimiento'] = 'true';

            $.ID_1000885851.update({
                monito: false
            });
        } else {
            /** 
             * esta tarea esta actualmente siendo seguida, en este estado debe decir mantener para iniciar (default) 
             */
            require('vars')['iniciar_seguimiento'] = 'false';
            /** 
             * Mostramos monito de caminando en la pantalla 
             */

            $.ID_1000885851.update({
                monito: true
            });
        }
        var moment = require('alloy/moment');
        var fecha_hoy = moment(new Date()).format('YYYY-MM-DD');
        if (tareas[0].fecha_tarea != fecha_hoy) {
            /** 
             * La fecha no es de hoy, se bloquea boton y se indica razon 
             */
            var ID_1935223870_estilo = 'fondoplomo2';

            var setEstilo = function(clase) {
                if ('styles' in require('a4w') && clase in require('a4w').styles['classes']) {
                    try {
                        $.ID_1935223870.applyProperties(require('a4w').styles['classes'][clase]);
                    } catch (sete_err) {}
                }
            };
            setEstilo(ID_1935223870_estilo);

            $.ID_1935223870.setBorderColor('#E6E6E6');

            var bloqueo_fecha = 'BLOQUEADA POR FECHA';
            $.ID_1289130430.setText(bloqueo_fecha);

            var insp_otrodia = 'Tip: No puedes iniciar inspecciones para tareas que no son para el dia';

            $.ID_1041134629.update({
                texto: insp_otrodia
            });
            var moment = require('alloy/moment');
            var ID_805159815 = tareas[0].fecha_tarea;
            if (typeof ID_805159815 === 'string' || typeof ID_805159815 === 'number') {
                var fecha_mostrar = moment(ID_805159815, 'YYYY-MM-DD').format('DD-MM-YYYY');
            } else {
                var fecha_mostrar = moment(ID_805159815).format('DD-MM-YYYY');
            }
            var tarea_realizar_fecha = 'Esta tarea se debe realizar el';
            $.ID_509734203.setText(tarea_realizar_fecha + ' ' + fecha_mostrar);

            require('vars')['bloquear_inspeccion'] = 'true';
        }
        var confirmarpush = JSON.parse(Ti.App.Properties.getString('confirmarpush'));
        if (Ti.App.deployType != 'production') console.log('detalle tarea: confirmar push dice', {
            "confirmar_push": confirmarpush
        });
        if (confirmarpush == true || confirmarpush == 'true') {
            require('vars')['bloquear_inspeccion'] = 'true';
            var ID_1935223870_estilo = 'fondoplomo2';

            var setEstilo = function(clase) {
                if ('styles' in require('a4w') && clase in require('a4w').styles['classes']) {
                    try {
                        $.ID_1935223870.applyProperties(require('a4w').styles['classes'][clase]);
                    } catch (sete_err) {}
                }
            };
            setEstilo(ID_1935223870_estilo);

            $.ID_1935223870.setBorderColor('#E6E6E6');

            var bloqueo_confirmacion = 'BLOQUEADA POR CONFIRMACION';
            $.ID_1289130430.setText(bloqueo_confirmacion);

            var confirmar_tarea = 'Tip: No puedes iniciar inspecciones sin confirmar tus tareas del dia';

            $.ID_1041134629.update({
                texto: confirmar_tarea
            });
            var confirmar_tareas_hoy = 'Se debe confirmar primero las tareas';
            $.ID_509734203.setText(confirmar_tarea_hoy);

        }
        if (_.isNumber(tareas[0].bono) && _.isNumber(0) && tareas[0].bono > 0) {

            $.ID_1000885851.update({
                bono: tareas[0].bono
            });
        }
    }
})();



_my_events['_cerrar_insp,ID_1102348505'] = function(evento) {
    if (!_.isUndefined(evento.pantalla)) {
        if (evento.pantalla == 'detalle') {
            if (Ti.App.deployType != 'production') console.log('debug cerrando detalle tarea', {});

            var ID_1632651147_trycatch = {
                error: function(e) {
                    if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
                }
            };
            try {
                ID_1632651147_trycatch.error = function(evento) {
                    if (Ti.App.deployType != 'production') console.log('error cerrando detalle tarea', {});
                };
                $.ID_411389157.close();
            } catch (e) {
                ID_1632651147_trycatch.error(e);
            }
        }
    } else {
        if (Ti.App.deployType != 'production') console.log('debug cerrando (todas) detalle tarea', {});

        var ID_1135554330_trycatch = {
            error: function(e) {
                if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
            }
        };
        try {
            ID_1135554330_trycatch.error = function(evento) {
                if (Ti.App.deployType != 'production') console.log('error cerrando detalle tarea', {});
            };
            $.ID_411389157.close();
        } catch (e) {
            ID_1135554330_trycatch.error(e);
        }
    }
};
Alloy.Events.on('_cerrar_insp', _my_events['_cerrar_insp,ID_1102348505']);

function Postlayout_ID_25820778(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var ID_1562663501_func = function() {
        require('vars')['var_abriendo'] = '';
    };
    var ID_1562663501 = setTimeout(ID_1562663501_func, 1000 * 0.2);

}
if (OS_IOS || OS_ANDROID) {
    $.ID_411389157.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}
//$.ID_411389157.open();