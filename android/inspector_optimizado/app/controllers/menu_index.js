var _bind4section = {};
var _list_templates = {
    "elemento": {
        "ID_432412973": {},
        "ID_291370846": {
            "text": "{nombre}"
        },
        "ID_1925487537": {
            "text": "{id_server}"
        },
        "ID_975786586": {
            "text": "{id_segured}"
        },
        "ID_1546190443": {}
    },
    "dano": {
        "ID_1569764493": {
            "text": "{id}"
        },
        "ID_304218376": {},
        "ID_1912165224": {
            "text": "{nombre}"
        }
    },
    "pborrar": {
        "ID_1236907100": {
            "text": "{id}"
        },
        "ID_1506538078": {},
        "ID_2024360383": {},
        "ID_1765564351": {},
        "ID_1699280427": {
            "text": "{nombre}"
        },
        "ID_1445903745": {},
        "ID_1795050796": {},
        "ID_1752901998": {},
        "ID_1914367022": {
            "text": "{nombre}"
        },
        "ID_1249074865": {
            "text": "{id}"
        },
        "ID_1131557960": {},
        "ID_1361874287": {},
        "ID_1750057271": {},
        "ID_1962135207": {},
        "ID_1947166298": {
            "text": "{nombre}"
        },
        "ID_2008605730": {
            "text": "{id}"
        },
        "ID_1788775425": {},
        "ID_1246915111": {}
    },
    "contenido": {
        "ID_630800887": {},
        "ID_862352109": {
            "text": "{id}"
        },
        "ID_560452274": {
            "text": "{nombre}"
        }
    },
    "recinto": {
        "ID_1960582596": {},
        "ID_871581291": {
            "text": "{nombre}"
        },
        "ID_310297848": {
            "text": "{id}"
        }
    },
    "nivel": {
        "ID_1021786928": {
            "text": "{id}"
        },
        "ID_1241655463": {
            "text": "{nombre}"
        },
        "ID_599581692": {}
    },
    "tarea": {
        "ID_1668871679": {},
        "ID_700219020": {},
        "ID_135101015": {
            "text": "{direccion}"
        },
        "ID_1113843363": {},
        "ID_1291192599": {
            "text": "{id}"
        },
        "ID_184328649": {},
        "ID_1320539277": {},
        "ID_260293613": {
            "text": "a {distance} km"
        },
        "ID_1225246470": {},
        "ID_1933295287": {},
        "ID_1584607898": {},
        "ID_634115674": {
            "text": "{comuna}"
        },
        "ID_1129660084": {
            "text": "{nivel_2}, {pais_texto}"
        },
        "ID_986890171": {
            "text": "a {distance} km"
        },
        "ID_75175": {},
        "ID_1124829140": {},
        "ID_22021502": {},
        "ID_1565487815": {},
        "ID_1188474916": {},
        "ID_1043900892": {},
        "ID_380606864": {},
        "ID_375542781": {},
        "ID_1653124778": {},
        "ID_1021365371": {
            "text": "{nivel_2}, {pais_texto}"
        },
        "ID_839820231": {
            "text": "{comuna}"
        },
        "ID_1650788926": {},
        "ID_904588626": {},
        "ID_1247657077": {},
        "ID_1634484552": {
            "text": "{comuna}"
        },
        "ID_148052687": {
            "backgroundColor": "{bgcolor}"
        },
        "ID_1469752290": {
            "visible": "{vis_tipo9}"
        },
        "ID_1082463670": {},
        "ID_1903120505": {
            "text": "{comuna}"
        },
        "ID_23206496": {
            "text": "{direccion}"
        },
        "ID_1436234452": {
            "backgroundColor": "{bgcolor}"
        },
        "ID_1262877465": {
            "text": "{comuna}"
        },
        "ID_1680788099": {},
        "ID_1848781491": {
            "text": "{comuna}"
        },
        "ID_532471930": {},
        "ID_1336684321": {},
        "ID_466372042": {
            "visible": "{vis_tipo6}"
        },
        "ID_598257926": {
            "text": "{direccion}"
        },
        "ID_1401335837": {},
        "ID_915725625": {},
        "ID_1012297385": {
            "text": "{nivel_2}, {pais_texto}"
        },
        "ID_976141257": {},
        "ID_639687765": {
            "text": "{direccion}"
        },
        "ID_73578511": {},
        "ID_295209697": {},
        "ID_1109750739": {
            "text": "a {distance} km"
        },
        "ID_586914833": {
            "text": "a {distance} km"
        },
        "ID_1008671603": {
            "backgroundColor": "{bgcolor}"
        },
        "ID_640168083": {},
        "ID_134526248": {},
        "ID_1453555994": {
            "text": "{nivel_2}, {pais_texto}"
        },
        "ID_1654995362": {
            "text": "{direccion}"
        },
        "ID_675490908": {},
        "ID_483618405": {},
        "ID_902659266": {
            "visible": "{vis_tipo4}"
        },
        "ID_177113531": {},
        "ID_348289297": {},
        "ID_269021738": {
            "text": "a {distance} km"
        },
        "ID_1685866714": {},
        "ID_466319634": {},
        "ID_905199894": {},
        "ID_995067518": {
            "text": "{direccion}"
        },
        "ID_1654146588": {
            "text": "{nivel_2}, {pais_texto}"
        },
        "ID_732636877": {},
        "ID_653858230": {},
        "ID_1639540465": {
            "text": "{nivel_2}, {pais_texto}"
        },
        "ID_34151484": {
            "backgroundColor": "{bgcolor}"
        },
        "ID_1157133699": {},
        "ID_579741095": {},
        "ID_1749522495": {
            "backgroundColor": "{bgcolor}"
        },
        "ID_1834569554": {},
        "ID_1983604525": {},
        "ID_1148296760": {},
        "ID_1779783039": {
            "backgroundColor": "{bgcolor}"
        },
        "ID_1935971476": {
            "text": "{nivel_2}, {pais_texto}"
        },
        "ID_1225247169": {
            "backgroundColor": "{bgcolor}"
        },
        "ID_708673244": {
            "text": "{comuna}"
        },
        "ID_1121279285": {
            "text": "{comuna}"
        },
        "ID_1187902687": {
            "text": "{nivel_2}, {pais_texto}"
        },
        "ID_1363673994": {},
        "ID_1980913443": {
            "visible": "{seguir}"
        },
        "ID_1829606280": {},
        "ID_644610354": {},
        "ID_135376617": {
            "text": "{nivel_2}, {pais_texto}"
        },
        "ID_26705836": {},
        "ID_1275917191": {
            "text": "{direccion}"
        },
        "ID_261159715": {},
        "ID_149472566": {
            "text": "{comuna}"
        },
        "ID_609601012": {
            "text": "a {distance} km"
        },
        "ID_767590028": {},
        "ID_826069369": {},
        "ID_1918649305": {},
        "ID_574730052": {},
        "ID_364330445": {},
        "ID_1434863225": {
            "text": "{comuna}"
        },
        "ID_1379816612": {
            "visible": "{vis_tipo10}"
        },
        "ID_480945686": {},
        "ID_309328842": {},
        "ID_720678066": {},
        "ID_511925926": {},
        "ID_1588457992": {},
        "ID_710734301": {},
        "ID_647346064": {},
        "ID_1281228744": {},
        "ID_1991358719": {},
        "ID_1366590855": {},
        "ID_511362493": {
            "visible": "{vis_tipo8}"
        },
        "ID_1297800494": {
            "text": "{nivel_2}, {pais_texto}"
        },
        "ID_828476175": {},
        "ID_1244150292": {
            "backgroundColor": "{bgcolor}"
        },
        "ID_1292045533": {
            "backgroundColor": "{bgcolor}"
        },
        "ID_1433100065": {
            "text": "{prioridad_tiempo}"
        },
        "ID_1683440013": {
            "visible": "{vis_otro}"
        },
        "ID_1398519606": {},
        "ID_1854942384": {
            "text": "{comuna}"
        },
        "ID_1267642540": {},
        "ID_870734514": {},
        "ID_1343703596": {},
        "ID_1502072898": {
            "visible": "{vis_tipo5}"
        },
        "ID_1688585922": {
            "text": "a {distance} km"
        },
        "ID_987598921": {},
        "ID_1498624444": {
            "backgroundColor": "{bgcolor}"
        },
        "ID_1939249430": {
            "text": "{direccion}"
        },
        "ID_1094583180": {},
        "ID_101815200": {},
        "ID_1417402594": {
            "visible": "{vis_tipo2}"
        },
        "ID_13800216": {
            "text": "{nivel_2}, {pais_texto}"
        },
        "ID_1556692359": {},
        "ID_682930065": {
            "text": "a {distance} km"
        },
        "ID_1586191918": {},
        "ID_733552058": {},
        "ID_231988172": {
            "text": "a {distance} km"
        },
        "ID_1565528410": {
            "visible": "{vis_tipo7}"
        },
        "ID_412416512": {},
        "ID_1787687658": {
            "text": "{direccion}"
        },
        "ID_1526803322": {},
        "ID_1655100522": {
            "backgroundColor": "{bgcolor}"
        },
        "ID_1376011318": {
            "text": "a {distance} km"
        },
        "ID_878804940": {
            "visible": "{vis_tipo1}"
        },
        "ID_789021647": {
            "text": "a {distance} km"
        },
        "ID_1412225440": {},
        "ID_794184744": {
            "text": "{direccion}"
        },
        "ID_1149338911": {
            "text": "{direccion}"
        },
        "ID_1642833684": {
            "visible": "{vis_tipo3}"
        }
    },
    "tareas_mistareas": {
        "ID_1575074457": {},
        "ID_1503294346": {
            "text": "{comuna}"
        },
        "ID_1152838109": {
            "idlocal": "{idlocal}"
        },
        "ID_1205965249": {
            "text": "a {distancia} km"
        },
        "ID_1729551944": {},
        "ID_1201332772": {},
        "ID_2063494569": {},
        "ID_1272820672": {
            "text": "{ciudad}, {pais}"
        },
        "ID_1138222879": {},
        "ID_1048592298": {},
        "ID_1695073981": {},
        "ID_2134521582": {},
        "ID_1071163621": {},
        "ID_1299156409": {
            "text": "{direccion}"
        },
        "ID_1676413215": {
            "visible": "{seguirvisible}"
        },
        "ID_1629587369": {}
    },
    "tareas": {
        "ID_1271969677": {},
        "ID_628062793": {
            "text": "{direcciontarea}"
        },
        "ID_568454310": {},
        "ID_926529622": {},
        "ID_1388054861": {
            "text": "{ciudadtarea}, {paistarea} "
        },
        "ID_369983217": {},
        "ID_75895559": {},
        "ID_360694343": {
            "text": "{comunatarea}"
        },
        "ID_1217465633": {
            "text": "a {ubicaciontarea} km"
        },
        "ID_477443044": {},
        "ID_1228388310": {},
        "ID_871033850": {},
        "ID_422941248": {
            "myid": "{myid}"
        }
    },
    "criticas": {
        "ID_202820149": {},
        "ID_1737707475": {
            "text": "{ciudadcritica}, {paiscritica}"
        },
        "ID_422128048": {},
        "ID_403273726": {},
        "ID_620140124": {
            "text": "{direccioncritica}"
        },
        "ID_1438432281": {},
        "ID_1119172715": {},
        "ID_1977608142": {
            "text": "{comunacritica}"
        },
        "ID_1622346619": {},
        "ID_1445023664": {},
        "ID_521530515": {
            "text": "a {ubicacioncritica} km"
        },
        "ID_704055711": {
            "myid": "{myid}"
        },
        "ID_715095938": {}
    },
    "tarea_historia": {
        "ID_1680574104": {
            "text": "{comuna}"
        },
        "ID_1304747472": {},
        "ID_676600495": {},
        "ID_1147033032": {},
        "ID_1810186930": {
            "text": "{hora_termino}"
        },
        "ID_911340427": {},
        "ID_1680180472": {
            "visible": "{bt_enviartarea}"
        },
        "ID_452289613": {},
        "ID_930474704": {
            "text": "{direccion}"
        },
        "ID_428063592": {
            "idlocal": "{id}",
            "estado": "{estado_tarea}"
        },
        "ID_1404040740": {},
        "ID_1184366498": {},
        "ID_459591478": {
            "text": "{ciudad}, {pais}"
        },
        "ID_362564056": {
            "visible": "{enviando_tarea}"
        },
        "ID_165401298": {},
        "ID_1987088054": {},
        "ID_500305974": {},
        "ID_124513836": {},
        "ID_404048727": {}
    }
};
Alloy.Globals["ID_216174848"] = $.ID_216174848;
var _activity;
if (OS_ANDROID) {
    _activity = $.ID_216174848.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    args = arguments[0] || {};

function Click_ID_1612127947(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    var gps_error = ('gps_error' in require('vars')) ? require('vars')['gps_error'] : '';
    if (gps_error == true || gps_error == 'true') {
        var ID_962412672_opts = ['Aceptar'];
        var ID_962412672 = Ti.UI.createAlertDialog({
            title: 'Error geolocalizando',
            message: 'Ha ocurrido un error al geolocalizarlo',
            buttonNames: ID_962412672_opts
        });
        ID_962412672.addEventListener('click', function(e) {
            var errori = ID_962412672_opts[e.index];
            errori = null;

            e.source.removeEventListener("click", arguments.callee);
        });
        ID_962412672.show();
    } else {
        var bt_refrescando = ('bt_refrescando' in require('vars')) ? require('vars')['bt_refrescando'] : '';
        var obtenermistareas = ('obtenermistareas' in require('vars')) ? require('vars')['obtenermistareas'] : '';
        var obtenerentrantes = ('obtenerentrantes' in require('vars')) ? require('vars')['obtenerentrantes'] : '';
        var obteneremergencias = ('obteneremergencias' in require('vars')) ? require('vars')['obteneremergencias'] : '';
        if (obtenermistareas == true || obtenermistareas == 'true') {
            if (Ti.App.deployType != 'production') console.log('obteniendo aun mistareas', {});
        } else if (obtenerentrantes == true || obtenerentrantes == 'true') {
            if (Ti.App.deployType != 'production') console.log('obteniendo aun entrantes', {});
        } else if (obteneremergencias == true || obteneremergencias == 'true') {
            if (Ti.App.deployType != 'production') console.log('obteniendo aun emergencias', {});
        } else {
            require('vars')['obtenermistareas'] = 'true';
            require('vars')['obtenerentrantes'] = 'true';
            var gps_latitud = ('gps_latitud' in require('vars')) ? require('vars')['gps_latitud'] : '';
            var gps_longitud = ('gps_longitud' in require('vars')) ? require('vars')['gps_longitud'] : '';
            var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
            if (Ti.App.deployType != 'production') console.log('Refrescando desde el menu', {});
            var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
            var ID_398815612 = {};

            ID_398815612.success = function(e) {
                var elemento = e,
                    valor = e;
                if (elemento.error == 0 || elemento.error == '0') {
                    var ID_1787502895_i = Alloy.Collections.tareas;
                    var sql = "DELETE FROM " + ID_1787502895_i.config.adapter.collection_name;
                    var db = Ti.Database.open(ID_1787502895_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    sql = null;
                    db = null;
                    ID_1787502895_i.trigger('delete');
                    /** 
                     * guardamos elemento.mistareas en var local, para ejecutar desfasada la insercion a la base de datos. 
                     */
                    require('vars')['mistareas'] = elemento.mistareas;
                    var ID_1089888790_func = function() {};
                    var ID_1089888790 = setTimeout(ID_1089888790_func, 1000 * 0.1);
                    var elemento_mistareas = elemento.mistareas;
                    var ID_643651375_m = Alloy.Collections.tareas;
                    var db_ID_643651375 = Ti.Database.open(ID_643651375_m.config.adapter.db_name);
                    db_ID_643651375.execute('BEGIN');
                    _.each(elemento_mistareas, function(ID_643651375_fila, pos) {
                        db_ID_643651375.execute('INSERT INTO tareas (fecha_tarea, id_inspeccion, id_asegurado, nivel_2, comentario_can_o_rech, asegurado_tel_fijo, estado_tarea, bono, evento, id_inspector, asegurado_codigo_identificador, lat, nivel_1, asegurado_nombre, pais, direccion, asegurador, fecha_ingreso, fecha_siniestro, nivel_1_texto, distance, nivel_4, perfil, asegurado_id, pais_texto, id_server, categoria, nivel_3, asegurado_correo, num_caso, lon, asegurado_tel_movil, exige_medidas, nivel_5, tipo_tarea) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', ID_643651375_fila.fecha_tarea, ID_643651375_fila.id_inspeccion, ID_643651375_fila.id_asegurado, ID_643651375_fila.nivel_2, ID_643651375_fila.comentario_can_o_rech, ID_643651375_fila.asegurado_tel_fijo, ID_643651375_fila.estado_tarea, ID_643651375_fila.bono, ID_643651375_fila.evento, ID_643651375_fila.id_inspector, ID_643651375_fila.asegurado_codigo_identificador, ID_643651375_fila.lat, ID_643651375_fila.nivel_1, ID_643651375_fila.asegurado_nombre, ID_643651375_fila.pais, ID_643651375_fila.direccion, ID_643651375_fila.asegurador, ID_643651375_fila.fecha_ingreso, ID_643651375_fila.fecha_siniestro, ID_643651375_fila.nivel_1_, ID_643651375_fila.distancia, ID_643651375_fila.nivel_4, 'ubicacion', ID_643651375_fila.asegurado_id, ID_643651375_fila.pais, ID_643651375_fila.id, ID_643651375_fila.categoria, ID_643651375_fila.nivel_3, ID_643651375_fila.asegurado_correo, ID_643651375_fila.num_caso, ID_643651375_fila.lon, ID_643651375_fila.asegurado_tel_movil, ID_643651375_fila.exige_medidas, ID_643651375_fila.nivel_5, ID_643651375_fila.tipo_tarea);
                    });
                    db_ID_643651375.execute('COMMIT');
                    db_ID_643651375.close();
                    db_ID_643651375 = null;
                    ID_643651375_m.trigger('change');
                    if (Ti.App.deployType != 'production') console.log('cambiando obtenermistareas a false', {});
                    require('vars')['obtenermistareas'] = 'false';

                    var ID_1241004686_trycatch = {
                        error: function(e) {
                            if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
                        }
                    };
                    try {
                        ID_1241004686_trycatch.error = function(evento) {};

                        Alloy.Events.trigger('_refrescar_tareas_mistareas');

                        Alloy.Events.trigger('_calcular_ruta');
                    } catch (e) {
                        ID_1241004686_trycatch.error(e);
                    }
                }
                elemento = null, valor = null;
            };
            require('helper').ajaxUnico('ID_398815612', '' + url_server + 'obtenerMisTareas' + '', 'POST', {
                id_inspector: inspector.id_server,
                lat: gps_latitud,
                lon: gps_longitud
            }, 15000, ID_398815612);
            var ID_523472170 = {};

            ID_523472170.success = function(e) {
                var elemento = e,
                    valor = e;
                if (elemento.error != 0 && elemento.error != '0') {
                    var ID_1133124779_opts = ['Aceptar'];
                    var ID_1133124779 = Ti.UI.createAlertDialog({
                        title: 'Alerta',
                        message: 'Error con el servidor',
                        buttonNames: ID_1133124779_opts
                    });
                    ID_1133124779.addEventListener('click', function(e) {
                        var errori = ID_1133124779_opts[e.index];
                        errori = null;

                        e.source.removeEventListener("click", arguments.callee);
                    });
                    ID_1133124779.show();
                } else {
                    var ID_1117237334_i = Alloy.Collections.tareas_entrantes;
                    var sql = "DELETE FROM " + ID_1117237334_i.config.adapter.collection_name;
                    var db = Ti.Database.open(ID_1117237334_i.config.adapter.db_name);
                    db.execute(sql);
                    db.close();
                    sql = null;
                    db = null;
                    ID_1117237334_i.trigger('delete');
                    var elemento_critica = elemento.critica;
                    var ID_1379163566_m = Alloy.Collections.tareas_entrantes;
                    var db_ID_1379163566 = Ti.Database.open(ID_1379163566_m.config.adapter.db_name);
                    db_ID_1379163566.execute('BEGIN');
                    _.each(elemento_critica, function(ID_1379163566_fila, pos) {
                        db_ID_1379163566.execute('INSERT INTO tareas_entrantes (fecha_tarea, id_inspeccion, id_asegurado, nivel_2, comentario_can_o_rech, asegurado_tel_fijo, estado_tarea, bono, evento, id_inspector, asegurado_codigo_identificador, lat, nivel_1, asegurado_nombre, pais, direccion, asegurador, fecha_ingreso, fecha_siniestro, nivel_1_texto, distance, nivel_4, perfil, asegurado_id, pais_texto, id_server, categoria, nivel_3, asegurado_correo, num_caso, lon, asegurado_tel_movil, exige_medidas, tipo_tarea, nivel_5) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', ID_1379163566_fila.fecha_tarea, ID_1379163566_fila.id_inspeccion, ID_1379163566_fila.id_asegurado, ID_1379163566_fila.nivel_2, '-', ID_1379163566_fila.asegurado_tel_fijo, ID_1379163566_fila.estado_tarea, ID_1379163566_fila.bono, ID_1379163566_fila.evento, ID_1379163566_fila.id_inspector, ID_1379163566_fila.asegurado_codigo_identificador, ID_1379163566_fila.lat, ID_1379163566_fila.nivel_1, ID_1379163566_fila.asegurado_nombre, -1, ID_1379163566_fila.direccion, ID_1379163566_fila.asegurador, ID_1379163566_fila.fecha_ingreso, ID_1379163566_fila.fecha_siniestro, ID_1379163566_fila.nivel_1_, ID_1379163566_fila.distancia, ID_1379163566_fila.nivel_4, 'casa', ID_1379163566_fila.asegurado_id, ID_1379163566_fila.pais_, ID_1379163566_fila.id, ID_1379163566_fila.categoria, ID_1379163566_fila.nivel_3, ID_1379163566_fila.asegurado_correo, ID_1379163566_fila.num_caso, ID_1379163566_fila.lon, ID_1379163566_fila.asegurado_tel_movil, ID_1379163566_fila.exige_medidas, ID_1379163566_fila.tipo_tarea, ID_1379163566_fila.nivel_5);
                    });
                    db_ID_1379163566.execute('COMMIT');
                    db_ID_1379163566.close();
                    db_ID_1379163566 = null;
                    ID_1379163566_m.trigger('change');
                    var elemento_normal = elemento.normal;
                    var ID_1002536627_m = Alloy.Collections.tareas_entrantes;
                    var db_ID_1002536627 = Ti.Database.open(ID_1002536627_m.config.adapter.db_name);
                    db_ID_1002536627.execute('BEGIN');
                    _.each(elemento_normal, function(ID_1002536627_fila, pos) {
                        db_ID_1002536627.execute('INSERT INTO tareas_entrantes (fecha_tarea, id_inspeccion, id_asegurado, nivel_2, comentario_can_o_rech, asegurado_tel_fijo, estado_tarea, bono, evento, id_inspector, asegurado_codigo_identificador, lat, nivel_1, asegurado_nombre, pais, direccion, asegurador, fecha_ingreso, fecha_siniestro, nivel_1_texto, distance, nivel_4, perfil, asegurado_id, pais_texto, id_server, categoria, nivel_3, asegurado_correo, num_caso, lon, asegurado_tel_movil, exige_medidas, tipo_tarea, nivel_5) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', ID_1002536627_fila.fecha_tarea, ID_1002536627_fila.id_inspeccion, ID_1002536627_fila.id_asegurado, ID_1002536627_fila.nivel_2, '-', ID_1002536627_fila.asegurado_tel_fijo, ID_1002536627_fila.estado_tarea, ID_1002536627_fila.bono, ID_1002536627_fila.evento, ID_1002536627_fila.id_inspector, ID_1002536627_fila.asegurado_codigo_identificador, ID_1002536627_fila.lat, ID_1002536627_fila.nivel_1, ID_1002536627_fila.asegurado_nombre, -1, ID_1002536627_fila.direccion, ID_1002536627_fila.asegurador, ID_1002536627_fila.fecha_ingreso, ID_1002536627_fila.fecha_siniestro, ID_1002536627_fila.nivel_1_, ID_1002536627_fila.distancia, ID_1002536627_fila.nivel_4, 'casa', ID_1002536627_fila.asegurado_id, ID_1002536627_fila.pais_, ID_1002536627_fila.id, ID_1002536627_fila.categoria, ID_1002536627_fila.nivel_3, ID_1002536627_fila.asegurado_correo, ID_1002536627_fila.num_caso, ID_1002536627_fila.lon, ID_1002536627_fila.asegurado_tel_movil, ID_1002536627_fila.exige_medidas, ID_1002536627_fila.tipo_tarea, ID_1002536627_fila.nivel_5);
                    });
                    db_ID_1002536627.execute('COMMIT');
                    db_ID_1002536627.close();
                    db_ID_1002536627 = null;
                    ID_1002536627_m.trigger('change');
                    require('vars')['obtenerentrantes'] = 'false';
                    if (Ti.App.deployType != 'production') console.log('cambiando obtenerentrantes a false', {});

                    Alloy.Events.trigger('_refrescar_tareas_entrantes');
                }
                elemento = null, valor = null;
            };

            ID_523472170.error = function(e) {
                var elemento = e,
                    valor = e;
                var ID_922935356_opts = ['Aceptar'];
                var ID_922935356 = Ti.UI.createAlertDialog({
                    title: 'Alerta',
                    message: 'No se pudo conectar con el servidor',
                    buttonNames: ID_922935356_opts
                });
                ID_922935356.addEventListener('click', function(e) {
                    var errori = ID_922935356_opts[e.index];
                    errori = null;

                    e.source.removeEventListener("click", arguments.callee);
                });
                ID_922935356.show();
                elemento = null, valor = null;
            };
            require('helper').ajaxUnico('ID_523472170', '' + url_server + 'obtenerTareasEntrantes' + '', 'POST', {
                id_inspector: inspector.id_server,
                lat: gps_latitud,
                lon: gps_longitud
            }, 15000, ID_523472170);
        }
    }
    source = null, elemento = null;

}

var _activity;
if (OS_ANDROID) {
    _activity = $.ID_786813367.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
if (OS_ANDROID) {
    $.ID_786813367.addEventListener('open', function(e) {});
}


$.ID_374340032.init({
    titulo: '¡ESTAS SIN CONEXION!',
    __id: 'ALL374340032',
    mensaje: 'No puedes ver las tareas entrantes',
    onon: on_ID_1500851550,
    onoff: Off_ID_1153942941
});

function on_ID_1500851550(e) {

    var evento = e;
    var ID_1619877339_visible = true;

    if (ID_1619877339_visible == 'si') {
        ID_1619877339_visible = true;
    } else if (ID_1619877339_visible == 'no') {
        ID_1619877339_visible = false;
    }
    $.ID_1619877339.setVisible(ID_1619877339_visible);


    Alloy.Events.trigger('_refrescar_tareas_entrantes');

}

function Off_ID_1153942941(e) {

    var evento = e;
    var ID_1619877339_visible = false;

    if (ID_1619877339_visible == 'si') {
        ID_1619877339_visible = true;
    } else if (ID_1619877339_visible == 'no') {
        ID_1619877339_visible = false;
    }
    $.ID_1619877339.setVisible(ID_1619877339_visible);


}

function Itemclick_ID_1524522670(e) {

    e.cancelBubble = true;
    var objeto = e.section.getItemAt(e.itemIndex);
    var modelo = {},
        _modelo = [];
    var fila = {},
        fila_bak = {},
        info = {
            _template: objeto.template,
            _what: [],
            _seccion_ref: e.section.getHeaderTitle(),
            _model_id: -1
        },
        _tmp = {
            objmap: {}
        };
    if ('itemId' in e) {
        info._model_id = e.itemId;
        modelo._id = info._model_id;
        if (info._seccion_ref != '' && info._seccion_ref in _bind4section) {
            modelo._collection = _bind4section[info._seccion_ref];
            _tmp._coll = modelo._collection;
        }
    }
    if (info._seccion_ref != '' && info._seccion_ref in _bind4section) {
        if (Alloy.Collections[_tmp._coll].config.adapter.type == 'sql') {
            _tmp._inst = Alloy.Collections[_tmp._coll];
            _tmp._id = Alloy.Collections[_tmp._coll].config.adapter.idAttribute;
            _tmp._dbsql = 'SELECT * FROM ' + Alloy.Collections[_tmp._coll].config.adapter.collection_name + ' WHERE ' + _tmp._id + ' = ' + e.itemId;
            _tmp._db = Ti.Database.open(Alloy.Collections[_tmp._coll].config.adapter.db_name);
            _modelo = _tmp._db.execute(_tmp._dbsql);
            var values = [],
                fieldNames = [];
            var fieldCount = _.isFunction(_modelo.fieldCount) ? _modelo.fieldCount() : _modelo.fieldCount;
            var getField = _modelo.field;
            var i = 0;
            for (; fieldCount > i; i++) fieldNames.push(_modelo.fieldName(i));
            while (_modelo.isValidRow()) {
                var o = {};
                for (i = 0; fieldCount > i; i++) o[fieldNames[i]] = getField(i);
                values.push(o);
                _modelo.next();
            }
            _modelo = values;
            _tmp._db.close();
        } else {
            _tmp._search = {};
            _tmp._search[_tmp._id] = e.itemId + '';
            _modelo = Alloy.Collections[_tmp._coll].fetch(_tmp._search);
        }
    }
    var findVariables = require('fvariables');
    _.each(_list_templates[info._template], function(obj_id, id) {
        _.each(obj_id, function(valor, prop) {
            var llaves = findVariables(valor, '{', '}');
            _.each(llaves, function(llave) {
                _tmp.objmap[llave] = {
                    id: id,
                    prop: prop
                };
                fila[llave] = objeto[id][prop];
                if (id == e.bindId) info._what.push(llave);
            });
        });
    });
    info._what = info._what.join(',');
    fila_bak = JSON.parse(JSON.stringify(fila));
    /** 
     * Creamos un flag para evitar que la pantalla se abra dos veces 
     */
    var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
    if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
        require('vars')['var_abriendo'] = 'true';
        if ("ID_216174848" in Alloy.Globals) {
            Alloy.Globals["ID_216174848"].activeTab.open(Alloy.createController("tomartarea_index", {
                '_objeto': fila,
                '_id': fila.myid,
                '_tipo': 'entrantes',
                '__master_model': (typeof modelo !== 'undefined') ? modelo : {},
                '__modelo': (typeof _modelo !== 'undefined' && _modelo.length > 0) ? _modelo[0] : {}
            }).getView());
        } else {
            Alloy.Globals["ID_216174848"] = $.ID_216174848;
            Alloy.Globals["ID_216174848"].activeTab.open(Alloy.createController("tomartarea_index", {
                '_objeto': fila,
                '_id': fila.myid,
                '_tipo': 'entrantes',
                '__master_model': (typeof modelo !== 'undefined') ? modelo : {},
                '__modelo': (typeof _modelo !== 'undefined' && _modelo.length > 0) ? _modelo[0] : {}
            }).getView());
        }

    }
    _tmp.changed = false;
    _tmp.diff_keys = [];
    _.each(fila, function(value1, prop) {
        var had_samekey = false;
        _.each(fila_bak, function(value2, prop2) {
            if (prop == prop2 && value1 == value2) {
                had_samekey = true;
            } else if (!_.has(fila_bak, prop) || !_.has(fila, prop2)) {
                has_samekey = true;
            }
        });
        if (!had_samekey) _tmp.diff_keys.push(prop);
    });
    if (_tmp.diff_keys.length > 0) _tmp.changed = true;
    if (_tmp.changed == true) {
        _.each(_tmp.diff_keys, function(llave) {
            objeto[_tmp.objmap[llave].id][_tmp.objmap[llave].prop] = fila[llave];
        });
        e.section.updateItemAt(e.itemIndex, objeto);
    }

}


_my_events['_refrescar_tareas_entrantes,ID_1636319035'] = function(evento) {
    /** 
     * Limpiamos el listado para ingresar las tareas refrescadas 
     */
    var ID_879692163_borrar = false;

    var limpiarListado = function(animar) {
        var a_nimar = (typeof animar == 'undefined') ? false : animar;
        if (OS_IOS && a_nimar == true) {
            var s_ecciones = $.ID_879692163.getSections();
            _.each(s_ecciones, function(obj_id, pos) {
                $.ID_879692163.deleteSectionAt(0, {
                    animated: true
                });
            });
        } else {
            $.ID_879692163.setSections([]);
        }
    };
    limpiarListado(ID_879692163_borrar);

    var ID_41461518 = Titanium.UI.createListSection({
        headerTitle: 'hoy_entrantes'
    });
    var ID_639457848 = Titanium.UI.createView({
        height: '25dp',
        width: Ti.UI.FILL
    });
    var ID_1235937068 = Titanium.UI.createView({
        height: '25dp',
        layout: 'composite',
        width: Ti.UI.FILL,
        backgroundColor: '#F7F7F7'
    });
    var ID_1680499516 = Titanium.UI.createLabel({
        text: 'PARA HOY',
        color: '#999999',
        touchEnabled: false,
        font: {
            fontFamily: 'Roboto-Medium',
            fontSize: '14dp'
        }

    });
    ID_1235937068.add(ID_1680499516);

    ID_639457848.add(ID_1235937068);
    ID_41461518.setHeaderView(ID_639457848);
    $.ID_879692163.appendSection(ID_41461518);
    var ID_813613027 = Titanium.UI.createListSection({
        headerTitle: 'manana_entrantes'
    });
    var ID_1297791130 = Titanium.UI.createView({
        height: '25dp',
        width: Ti.UI.FILL
    });
    var ID_825962635 = Titanium.UI.createView({
        height: '25dp',
        layout: 'composite',
        width: Ti.UI.FILL,
        backgroundColor: '#F7F7F7'
    });
    var ID_1184933740 = Titanium.UI.createLabel({
        text: 'MAÑANA',
        color: '#999999',
        touchEnabled: false,
        font: {
            fontFamily: 'Roboto-Medium',
            fontSize: '14dp'
        }

    });
    ID_825962635.add(ID_1184933740);

    ID_1297791130.add(ID_825962635);
    ID_813613027.setHeaderView(ID_1297791130);
    $.ID_879692163.appendSection(ID_813613027);
    /** 
     * Consultamos la tabla de las tareas criticas y guardamos en variable normales 
     */
    var ID_893838287_i = Alloy.createCollection('tareas_entrantes');
    var ID_893838287_i_where = 'tipo_tarea=0 ORDER BY FECHA_TAREA ASC';
    ID_893838287_i.fetch({
        query: 'SELECT * FROM tareas_entrantes WHERE tipo_tarea=0 ORDER BY FECHA_TAREA ASC'
    });
    var normales = require('helper').query2array(ID_893838287_i);
    if (normales && normales.length) {
        if (Ti.App.deployType != 'production') console.log('tareas normales existentes', {
            "normales": normales
        });
        /** 
         * Formateamos fecha de hoy y manana para cargar la tarea dependiendo de la fecha de realizacion 
         */
        var moment = require('alloy/moment');
        var fecha_hoy = moment(new Date()).format('YYYY-MM-DD');
        manana = new Date();
        manana.setDate(manana.getDate() + 1);;
        var moment = require('alloy/moment');
        var ID_125583866 = manana;
        var fecha_manana = moment(ID_125583866).format('YYYY-MM-DD');
        /** 
         * Inicializamos la variable de ultima fecha en vacio para usarla en el recorrido de tareas 
         */
        require('vars')['ultima_fecha'] = '';
        var tarea_index = 0;
        _.each(normales, function(tarea, tarea_pos, tarea_list) {
            tarea_index += 1;
            /** 
             * Variable usada para poder filtrar las tareas segun la fecha y no se vayan todas a una sola seccion 
             */
            var ultima_fecha = ('ultima_fecha' in require('vars')) ? require('vars')['ultima_fecha'] : '';
            if (ultima_fecha != tarea.fecha_tarea) {
                /** 
                 * Generamos una seccion nueva para las fechas que no sean de hoy, ni de manana 
                 */
                if (tarea.fecha_tarea == fecha_hoy) {} else if (tarea.fecha_tarea == fecha_manana) {} else {
                    var moment = require('alloy/moment');
                    var ID_54484833 = tarea.fecha_tarea;
                    if (typeof ID_54484833 === 'string' || typeof ID_54484833 === 'number') {
                        var fecha_titulo = moment(ID_54484833, 'YYYY-MM-DD').format('DD/MM/YYYY');
                    } else {
                        var fecha_titulo = moment(ID_54484833).format('DD/MM/YYYY');
                    }
                    var ID_887257458 = Titanium.UI.createListSection({
                        headerTitle: 'fecha_entrantes'
                    });
                    var ID_819042770 = Titanium.UI.createView({
                        height: '25dp',
                        width: Ti.UI.FILL
                    });
                    var ID_1319827803 = Titanium.UI.createView({
                        height: '25dp',
                        layout: 'composite',
                        width: Ti.UI.FILL,
                        backgroundColor: '#F7F7F7'
                    });
                    var ID_1955950352 = Titanium.UI.createLabel({
                        text: fecha_titulo,
                        color: '#999999',
                        touchEnabled: false,
                        font: {
                            fontFamily: 'Roboto-Medium',
                            fontSize: '14dp'
                        }

                    });
                    ID_1319827803.add(ID_1955950352);

                    ID_819042770.add(ID_1319827803);
                    ID_887257458.setHeaderView(ID_819042770);
                    $.ID_879692163.appendSection(ID_887257458);
                }
                /** 
                 * Actualizamos la variable con&#160;la ultima fecha de la tarea 
                 */
                require('vars')['ultima_fecha'] = tarea.fecha_tarea;
            }
            if ((_.isObject(tarea.nivel_2) || _.isString(tarea.nivel_2)) && _.isEmpty(tarea.nivel_2)) {
                /** 
                 * Revisamos cual es el ultimo nivel disponible y lo fijamos dentro de la variable tarea como ultimo_nivel 
                 */
                var tarea = _.extend(tarea, {
                    ultimo_nivel: tarea.nivel_1
                });
            } else if ((_.isObject(tarea.nivel_3) || _.isString(tarea.nivel_3)) && _.isEmpty(tarea.nivel_3)) {
                var tarea = _.extend(tarea, {
                    ultimo_nivel: tarea.nivel_2
                });
            } else if ((_.isObject(tarea.nivel_4) || _.isString(tarea.nivel_4)) && _.isEmpty(tarea.nivel_4)) {
                var tarea = _.extend(tarea, {
                    ultimo_nivel: tarea.nivel_3
                });
            } else if ((_.isObject(tarea.nivel_5) || _.isString(tarea.nivel_5)) && _.isEmpty(tarea.nivel_5)) {
                var tarea = _.extend(tarea, {
                    ultimo_nivel: tarea.nivel_4
                });
            } else {
                var tarea = _.extend(tarea, {
                    ultimo_nivel: tarea.nivel_5
                });
            }
            if (tarea.fecha_tarea == fecha_hoy) {
                /** 
                 * Dependiendo de la fecha de la tarea, es donde apuntaremos a la seccion que corresponde (segun fecha) 
                 */
                var ID_481046448 = [{
                    ID_1271969677: {},
                    ID_628062793: {
                        text: tarea.direccion
                    },
                    ID_568454310: {},
                    ID_926529622: {},
                    ID_1388054861: {
                        text: tarea.nivel_2 + ', ' + tarea.pais_texto
                    },
                    ID_369983217: {},
                    ID_75895559: {},
                    ID_360694343: {
                        text: tarea.ultimo_nivel
                    },
                    ID_1217465633: {
                        text: 'a ' + tarea.distance + ' km'
                    },
                    template: 'tareas',
                    ID_477443044: {},
                    ID_1228388310: {},
                    ID_871033850: {},
                    ID_422941248: {
                        myid: tarea.id
                    }

                }];
                var ID_481046448_secs = {};
                _.map($.ID_879692163.getSections(), function(ID_481046448_valor, ID_481046448_indice) {
                    ID_481046448_secs[ID_481046448_valor.getHeaderTitle()] = ID_481046448_indice;
                    return ID_481046448_valor;
                });
                if ('hoy_entrantes' in ID_481046448_secs) {
                    $.ID_879692163.sections[ID_481046448_secs['hoy_entrantes']].appendItems(ID_481046448);
                } else {
                    console.log('DEBUG:dynamic listItem points to a invalid section id: check attribute _seccion.');
                }
            } else if (tarea.fecha_tarea == fecha_manana) {
                var ID_1401951081 = [{
                    ID_1271969677: {},
                    ID_628062793: {
                        text: tarea.direccion
                    },
                    ID_568454310: {},
                    ID_926529622: {},
                    ID_1388054861: {
                        text: tarea.nivel_2 + ', ' + tarea.pais_texto
                    },
                    ID_369983217: {},
                    ID_75895559: {},
                    ID_360694343: {
                        text: tarea.ultimo_nivel
                    },
                    ID_1217465633: {
                        text: 'a ' + tarea.distance + ' km'
                    },
                    template: 'tareas',
                    ID_477443044: {},
                    ID_1228388310: {},
                    ID_871033850: {},
                    ID_422941248: {
                        myid: tarea.id
                    }

                }];
                var ID_1401951081_secs = {};
                _.map($.ID_879692163.getSections(), function(ID_1401951081_valor, ID_1401951081_indice) {
                    ID_1401951081_secs[ID_1401951081_valor.getHeaderTitle()] = ID_1401951081_indice;
                    return ID_1401951081_valor;
                });
                if ('manana_entrantes' in ID_1401951081_secs) {
                    $.ID_879692163.sections[ID_1401951081_secs['manana_entrantes']].appendItems(ID_1401951081);
                } else {
                    console.log('DEBUG:dynamic listItem points to a invalid section id: check attribute _seccion.');
                }
            } else {
                var ID_1079731371 = [{
                    ID_1271969677: {},
                    ID_628062793: {
                        text: tarea.direccion
                    },
                    ID_568454310: {},
                    ID_926529622: {},
                    ID_1388054861: {
                        text: tarea.nivel_2 + ', ' + tarea.pais_texto
                    },
                    ID_369983217: {},
                    ID_75895559: {},
                    ID_360694343: {
                        text: tarea.ultimo_nivel
                    },
                    ID_1217465633: {
                        text: 'a ' + tarea.distance + ' km'
                    },
                    template: 'tareas',
                    ID_477443044: {},
                    ID_1228388310: {},
                    ID_871033850: {},
                    ID_422941248: {
                        myid: tarea.id
                    }

                }];
                var ID_1079731371_secs = {};
                _.map($.ID_879692163.getSections(), function(ID_1079731371_valor, ID_1079731371_indice) {
                    ID_1079731371_secs[ID_1079731371_valor.getHeaderTitle()] = ID_1079731371_indice;
                    return ID_1079731371_valor;
                });
                if ('fecha_entrantes' in ID_1079731371_secs) {
                    $.ID_879692163.sections[ID_1079731371_secs['fecha_entrantes']].appendItems(ID_1079731371);
                } else {
                    console.log('DEBUG:dynamic listItem points to a invalid section id: check attribute _seccion.');
                }
            }
        });
    }
};
Alloy.Events.on('_refrescar_tareas_entrantes', _my_events['_refrescar_tareas_entrantes,ID_1636319035']);

_my_events['_refrescar_tareas,ID_411683779'] = function(evento) {

    Alloy.Events.trigger('_refrescar_tareas_entrantes');
};
Alloy.Events.on('_refrescar_tareas', _my_events['_refrescar_tareas,ID_411683779']);
var ID_170851302_func = function() {

    Alloy.Events.trigger('_refrescar_tareas_entrantes');
};
var ID_170851302 = setTimeout(ID_170851302_func, 1000 * 0.2);

function Androidback_ID_1178411846(e) {
    /** 
     * Sin funcionalidad para evitar que el usuario cierre la pantalla 
     */

    e.cancelBubble = true;
    var elemento = e.source;

}
//$.ID_216174848.open();