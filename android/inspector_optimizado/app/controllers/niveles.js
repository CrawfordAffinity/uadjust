var _bind4section = {};
var _list_templates = {};
var $nivel = $.nivel.toJSON();

var _activity;
if (OS_ANDROID) {
    _activity = $.ID_1407323156.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_1407323156';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_1407323156.addEventListener('open', function(e) {});
}
$.ID_1407323156.orientationModes = [Titanium.UI.PORTRAIT];


$.ID_1471408157.init({
    titulo: 'NUEVA ZONA',
    onsalirinsp: Salirinsp_ID_1575858257,
    __id: 'ALL1471408157',
    textoderecha: 'Guardar',
    fondo: 'fondoverde',
    top: 0,
    onpresiono: Presiono_ID_359752291,
    colortextoderecha: 'blanco'
});

function Salirinsp_ID_1575858257(e) {

    var evento = e;
    /** 
     * Limpiamos widget para liberar memoria ram 
     */
    $.ID_1638019983.limpiar({});
    $.ID_1163781653.limpiar({});
    $.ID_1436403784.limpiar({});
    $.ID_1826404914.limpiar({});
    $.ID_2057626398.limpiar({});
    $.ID_2073900147.limpiar({});
    $.ID_954507458.limpiar({});
    $.ID_1407323156.close();

}

function Presiono_ID_359752291(e) {

    var evento = e;
    /** 
     * Desenfocamos todos los campos de texto 
     */
    $.ID_1611114284.blur();
    $.ID_1947669263.blur();
    $.ID_1260479449.blur();
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
        /** 
         * si tarea requiere seleccionar medidas de seguridad, probamos si hay seleccionadas aqui para respetar orden de validacion. 
         */
        if (seltarea.exige_medidas == 10) {
            if (_.isUndefined($nivel.ids_medidas_seguridad)) {
                require('vars')[_var_scopekey]['faltan_medidas'] = 'true';
            } else if ((_.isObject($nivel.ids_medidas_seguridad) || _.isString($nivel.ids_medidas_seguridad)) && _.isEmpty($nivel.ids_medidas_seguridad)) {
                require('vars')[_var_scopekey]['faltan_medidas'] = 'true';
            } else {
                require('vars')[_var_scopekey]['faltan_medidas'] = 'false';
            }
        }
    }
    var faltan_medidas = ('faltan_medidas' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['faltan_medidas'] : '';
    var moment = require('alloy/moment');
    var anoactual = moment(new Date()).format('YYYY');
    if (_.isUndefined($nivel.nombre)) {
        /** 
         * Verificamos que los campos ingresados esten correctos 
         */
        var ID_1707261968_opts = ['Aceptar'];
        var ID_1707261968 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese nombre de la zona',
            buttonNames: ID_1707261968_opts
        });
        ID_1707261968.addEventListener('click', function(e) {
            var nulo = ID_1707261968_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1707261968.show();
    } else if ((_.isObject($nivel.nombre) || _.isString($nivel.nombre)) && _.isEmpty($nivel.nombre)) {
        var ID_1478130880_opts = ['Aceptar'];
        var ID_1478130880 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese nombre de la zona',
            buttonNames: ID_1478130880_opts
        });
        ID_1478130880.addEventListener('click', function(e) {
            var nulo = ID_1478130880_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1478130880.show();
    } else if (_.isUndefined($nivel.piso)) {
        var ID_1087460829_opts = ['Aceptar'];
        var ID_1087460829 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese Nº de piso',
            buttonNames: ID_1087460829_opts
        });
        ID_1087460829.addEventListener('click', function(e) {
            var nulo = ID_1087460829_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1087460829.show();
    } else if ($nivel.piso.length == 0 || $nivel.piso.length == '0') {
        var ID_1760909484_opts = ['Aceptar'];
        var ID_1760909484 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese Nº de piso',
            buttonNames: ID_1760909484_opts
        });
        ID_1760909484.addEventListener('click', function(e) {
            var nulo = ID_1760909484_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1760909484.show();
    } else if (_.isUndefined($nivel.ano)) {
        var ID_363040379_opts = ['Aceptar'];
        var ID_363040379 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese año de construcción de la zona',
            buttonNames: ID_363040379_opts
        });
        ID_363040379.addEventListener('click', function(e) {
            var nulo = ID_363040379_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_363040379.show();
    } else if ((_.isObject($nivel.ano) || _.isString($nivel.ano)) && _.isEmpty($nivel.ano)) {
        var ID_465669023_opts = ['Aceptar'];
        var ID_465669023 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese año de construcción de la zona',
            buttonNames: ID_465669023_opts
        });
        ID_465669023.addEventListener('click', function(e) {
            var nulo = ID_465669023_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_465669023.show();
    } else if ($nivel.ano < (anoactual - 100) == true || $nivel.ano < (anoactual - 100) == 'true') {
        if (Ti.App.deployType != 'production') console.log('ano mayor', {});
        require('vars')[_var_scopekey]['todobien'] = 'false';
        var ID_271729525_opts = ['Aceptar'];
        var ID_271729525 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Tiene que tener máximo 100 años de antigüedad',
            buttonNames: ID_271729525_opts
        });
        ID_271729525.addEventListener('click', function(e) {
            var nulo = ID_271729525_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_271729525.show();
    } else if ($nivel.ano > anoactual == true || $nivel.ano > anoactual == 'true') {
        if (Ti.App.deployType != 'production') console.log('ano mayor', {});
        require('vars')[_var_scopekey]['todobien'] = 'false';
        var ID_1795243898_opts = ['Aceptar'];
        var ID_1795243898 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'El año tiene que ser menor al año actual',
            buttonNames: ID_1795243898_opts
        });
        ID_1795243898.addEventListener('click', function(e) {
            var nulo = ID_1795243898_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1795243898.show();
    } else if (_.isUndefined($nivel.largo)) {
        var ID_1178503984_opts = ['Aceptar'];
        var ID_1178503984 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese largo de la zona',
            buttonNames: ID_1178503984_opts
        });
        ID_1178503984.addEventListener('click', function(e) {
            var nulo = ID_1178503984_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1178503984.show();
    } else if ((_.isObject($nivel.largo) || _.isString($nivel.largo)) && _.isEmpty($nivel.largo)) {
        var ID_1363217727_opts = ['Aceptar'];
        var ID_1363217727 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese largo de la zona',
            buttonNames: ID_1363217727_opts
        });
        ID_1363217727.addEventListener('click', function(e) {
            var nulo = ID_1363217727_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1363217727.show();
    } else if (_.isUndefined($nivel.ancho)) {
        var ID_173165960_opts = ['Aceptar'];
        var ID_173165960 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese ancho de la zona',
            buttonNames: ID_173165960_opts
        });
        ID_173165960.addEventListener('click', function(e) {
            var nulo = ID_173165960_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_173165960.show();
    } else if ((_.isObject($nivel.ancho) || _.isString($nivel.ancho)) && _.isEmpty($nivel.ancho)) {
        var ID_1132963601_opts = ['Aceptar'];
        var ID_1132963601 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese ancho de la zona',
            buttonNames: ID_1132963601_opts
        });
        ID_1132963601.addEventListener('click', function(e) {
            var nulo = ID_1132963601_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1132963601.show();
    } else if (_.isUndefined($nivel.alto)) {
        var ID_215888724_opts = ['Aceptar'];
        var ID_215888724 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese la altura de la zona',
            buttonNames: ID_215888724_opts
        });
        ID_215888724.addEventListener('click', function(e) {
            var nulo = ID_215888724_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_215888724.show();
    } else if ((_.isObject($nivel.alto) || _.isString($nivel.alto)) && _.isEmpty($nivel.alto)) {
        var ID_1754346647_opts = ['Aceptar'];
        var ID_1754346647 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese la altura de la zona',
            buttonNames: ID_1754346647_opts
        });
        ID_1754346647.addEventListener('click', function(e) {
            var nulo = ID_1754346647_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1754346647.show();
    } else if (faltan_medidas == true || faltan_medidas == 'true') {
        var ID_698224009_opts = ['Aceptar'];
        var ID_698224009 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Seleccione las medidas de seguridad',
            buttonNames: ID_698224009_opts
        });
        ID_698224009.addEventListener('click', function(e) {
            var nulo = ID_698224009_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_698224009.show();
    } else if (_.isUndefined($nivel.ids_estructuras_soportantes)) {
        var ID_1356709795_opts = ['Aceptar'];
        var ID_1356709795 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Seleccione la estructura soportante de la zona',
            buttonNames: ID_1356709795_opts
        });
        ID_1356709795.addEventListener('click', function(e) {
            var nulo = ID_1356709795_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1356709795.show();
    } else if ((_.isObject($nivel.ids_estructuras_soportantes) || _.isString($nivel.ids_estructuras_soportantes)) && _.isEmpty($nivel.ids_estructuras_soportantes)) {
        var ID_522815177_opts = ['Aceptar'];
        var ID_522815177 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Seleccione la estructura soportante de la zona',
            buttonNames: ID_522815177_opts
        });
        ID_522815177.addEventListener('click', function(e) {
            var nulo = ID_522815177_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_522815177.show();
    } else {
        /** 
         * Guardamos los campos ingresados en el modelo 
         */
        Alloy.Collections[$.nivel.config.adapter.collection_name].add($.nivel);
        $.nivel.save();
        Alloy.Collections[$.nivel.config.adapter.collection_name].fetch();
        /** 
         * Limpiamos widget para liberar memoria ram 
         */
        $.ID_1638019983.limpiar({});
        $.ID_1163781653.limpiar({});
        $.ID_1436403784.limpiar({});
        $.ID_1826404914.limpiar({});
        $.ID_2057626398.limpiar({});
        $.ID_2073900147.limpiar({});
        $.ID_954507458.limpiar({});
        $.ID_1407323156.close();
    }
}

function Blur_ID_1391196026(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    $.nivel.set({
        nombre: elemento
    });
    if ('nivel' in $) $nivel = $.nivel.toJSON();
    elemento = null, source = null;

}

function Change_ID_764031777(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    $.nivel.set({
        piso: elemento
    });
    if ('nivel' in $) $nivel = $.nivel.toJSON();
    elemento = null, source = null;

}

function Change_ID_525249444(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    $.nivel.set({
        ano: elemento
    });
    if ('nivel' in $) $nivel = $.nivel.toJSON();
    elemento = null, source = null;

}

function Return_ID_1354422881(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    $.ID_1260479449.blur();
    elemento = null, source = null;

}

function Change_ID_1641877675(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    var ancho;
    ancho = $.ID_1403876333.getValue();

    if ((_.isObject(ancho) || (_.isString(ancho)) && !_.isEmpty(ancho)) || _.isNumber(ancho) || _.isBoolean(ancho)) {
        if ((_.isObject(elemento) || (_.isString(elemento)) && !_.isEmpty(elemento)) || _.isNumber(elemento) || _.isBoolean(elemento)) {
            /** 
             * nos aseguramos que ancho y valor de campo sean decimales con puntos (flotante), y no comas (espa&#241;ol) 
             */
            var nuevo = parseFloat(ancho.split(',').join('.')) * parseFloat(elemento.split(',').join('.'));
            $.nivel.set({
                superficie: nuevo
            });
            if ('nivel' in $) $nivel = $.nivel.toJSON();
        }
    }
    $.nivel.set({
        largo: elemento
    });
    if ('nivel' in $) $nivel = $.nivel.toJSON();
    elemento = null, source = null;

}

function Change_ID_1900365175(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    var largo;
    largo = $.ID_1942756801.getValue();

    if ((_.isObject(largo) || (_.isString(largo)) && !_.isEmpty(largo)) || _.isNumber(largo) || _.isBoolean(largo)) {
        if ((_.isObject(elemento) || (_.isString(elemento)) && !_.isEmpty(elemento)) || _.isNumber(elemento) || _.isBoolean(elemento)) {
            /** 
             * nos aseguramos que largo y valor de campo sean decimales con puntos (flotante), y no comas (espa&#241;ol) 
             */
            var nuevo = parseFloat(largo.split(',').join('.')) * parseFloat(elemento.split(',').join('.'));
            $.nivel.set({
                superficie: nuevo
            });
            if ('nivel' in $) $nivel = $.nivel.toJSON();
        }
    }
    $.nivel.set({
        ancho: elemento
    });
    if ('nivel' in $) $nivel = $.nivel.toJSON();
    elemento = null, source = null;

}

function Change_ID_1500572394(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    $.nivel.set({
        alto: elemento
    });
    if ('nivel' in $) $nivel = $.nivel.toJSON();
    elemento = null, source = null;

}

$.ID_954507458.init({
    titulo: 'Medidas de seguridad',
    cargando: 'cargando...',
    __id: 'ALL954507458',
    oncerrar: Cerrar_ID_182200124,
    left: 0,
    hint: 'Seleccione medidas',
    color: 'verde',
    subtitulo: 'Indique las medidas',
    right: 0,
    onclick: Click_ID_1999744147,
    onafterinit: Afterinit_ID_1300874053
});

function Click_ID_1999744147(e) {

    var evento = e;

}

function Cerrar_ID_182200124(e) {

    var evento = e;
    $.ID_954507458.update({});
    $.nivel.set({
        ids_medidas_seguridad: evento.valores
    });
    if ('nivel' in $) $nivel = $.nivel.toJSON();

}

function Afterinit_ID_1300874053(e) {

    var evento = e;
    var ID_127788395_func = function() {
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
            var ID_910434379_i = Alloy.createCollection('medidas_seguridad');
            var ID_910434379_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
            ID_910434379_i.fetch({
                query: 'SELECT * FROM medidas_seguridad WHERE pais_texto=\'' + pais[0].nombre + '\''
            });
            var medidas_seguridad = require('helper').query2array(ID_910434379_i);
            var datos = [];
            _.each(medidas_seguridad, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'label';
                    if (llave == 'id_segured') newkey = 'valor';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                new_row['estado'] = 0;
                datos.push(new_row);
            });
            $.ID_954507458.update({
                data: datos
            });
        } else {
            var ID_987333996_i = Alloy.Collections.medidas_seguridad;
            var sql = "DELETE FROM " + ID_987333996_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_987333996_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_987333996_i.trigger('delete');
            var item_index = 0;
            _.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
                item_index += 1;
                var ID_552612206_m = Alloy.Collections.medidas_seguridad;
                var ID_552612206_fila = Alloy.createModel('medidas_seguridad', {
                    nombre: 'Medida ' + item,
                    id_server: '0' + item,
                    id_segured: '10' + item,
                    pais: 1
                });
                ID_552612206_m.add(ID_552612206_fila);
                ID_552612206_fila.save();
            });
            var ID_1353835673_i = Alloy.createCollection('medidas_seguridad');
            ID_1353835673_i.fetch();
            var ID_1353835673_src = require('helper').query2array(ID_1353835673_i);
            var datos = [];
            _.each(ID_1353835673_src, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'label';
                    if (llave == 'id_segured') newkey = 'valor';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                new_row['estado'] = 0;
                datos.push(new_row);
            });
            $.ID_954507458.update({
                data: datos
            });
        }
    };
    var ID_127788395 = setTimeout(ID_127788395_func, 1000 * 0.2);

}

$.ID_1638019983.init({
    titulo: 'Estructura Soportante',
    cargando: 'cargando...',
    __id: 'ALL1638019983',
    oncerrar: Cerrar_ID_1634931984,
    left: 0,
    hint: 'Seleccione estructura',
    color: 'verde',
    subtitulo: 'Indique los tipos',
    right: 0,
    onclick: Click_ID_1061207482,
    onafterinit: Afterinit_ID_1719673259
});

function Click_ID_1061207482(e) {

    var evento = e;

}

function Cerrar_ID_1634931984(e) {

    var evento = e;
    $.ID_1638019983.update({});
    $.nivel.set({
        ids_estructuras_soportantes: evento.valores
    });
    if ('nivel' in $) $nivel = $.nivel.toJSON();

}

function Afterinit_ID_1719673259(e) {

    var evento = e;
    var ID_848598486_func = function() {
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
            var ID_1428751800_i = Alloy.createCollection('estructura_soportante');
            var ID_1428751800_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
            ID_1428751800_i.fetch({
                query: 'SELECT * FROM estructura_soportante WHERE pais_texto=\'' + pais[0].nombre + '\''
            });
            var estructura = require('helper').query2array(ID_1428751800_i);
            var datos = [];
            _.each(estructura, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'label';
                    if (llave == 'id_segured') newkey = 'valor';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                new_row['estado'] = 0;
                datos.push(new_row);
            });
            if (Ti.App.deployType != 'production') console.log('contenido datos', {
                "cont": datos
            });
            $.ID_1638019983.update({
                data: datos
            });
        } else {
            var ID_965895026_i = Alloy.Collections.estructura_soportante;
            var sql = "DELETE FROM " + ID_965895026_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_965895026_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_965895026_i.trigger('delete');
            var item_index = 0;
            _.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
                item_index += 1;
                var ID_1326868287_m = Alloy.Collections.estructura_soportante;
                var ID_1326868287_fila = Alloy.createModel('estructura_soportante', {
                    nombre: 'Estructura' + item,
                    id_server: '0' + item,
                    id_segured: '10' + item,
                    pais: 1
                });
                ID_1326868287_m.add(ID_1326868287_fila);
                ID_1326868287_fila.save();
            });
            var ID_1560607667_i = Alloy.createCollection('estructura_soportante');
            ID_1560607667_i.fetch();
            var ID_1560607667_src = require('helper').query2array(ID_1560607667_i);
            var datos = [];
            _.each(ID_1560607667_src, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'label';
                    if (llave == 'id_segured') newkey = 'valor';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                new_row['estado'] = 0;
                datos.push(new_row);
            });
            if (Ti.App.deployType != 'production') console.log('contenido datos', {
                "cont": datos
            });
            $.ID_1638019983.update({
                data: datos
            });
        }
    };
    var ID_848598486 = setTimeout(ID_848598486_func, 1000 * 0.2);

}

$.ID_2073900147.init({
    titulo: 'Muros / Tabiques',
    cargando: 'cargando...',
    __id: 'ALL2073900147',
    oncerrar: Cerrar_ID_1582800533,
    left: 0,
    hint: 'Seleccione muros',
    color: 'verde',
    subtitulo: 'Indique los tipos',
    right: 0,
    onclick: Click_ID_1702522936,
    onafterinit: Afterinit_ID_1217341344
});

function Click_ID_1702522936(e) {

    var evento = e;

}

function Cerrar_ID_1582800533(e) {

    var evento = e;
    $.ID_2073900147.update({});
    $.nivel.set({
        ids_muros: evento.valores
    });
    if ('nivel' in $) $nivel = $.nivel.toJSON();

}

function Afterinit_ID_1217341344(e) {

    var evento = e;
    var ID_761628958_func = function() {
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
            var ID_226882694_i = Alloy.createCollection('muros_tabiques');
            var ID_226882694_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
            ID_226882694_i.fetch({
                query: 'SELECT * FROM muros_tabiques WHERE pais_texto=\'' + pais[0].nombre + '\''
            });
            var tabiques = require('helper').query2array(ID_226882694_i);
            var datos = [];
            _.each(tabiques, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'label';
                    if (llave == 'id_segured') newkey = 'valor';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                new_row['estado'] = 0;
                datos.push(new_row);
            });
            $.ID_2073900147.update({
                data: datos
            });
        } else {
            var ID_1632798819_i = Alloy.Collections.muros_tabiques;
            var sql = "DELETE FROM " + ID_1632798819_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_1632798819_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_1632798819_i.trigger('delete');
            var item_index = 0;
            _.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
                item_index += 1;
                var ID_1716569383_m = Alloy.Collections.muros_tabiques;
                var ID_1716569383_fila = Alloy.createModel('muros_tabiques', {
                    nombre: 'Muros' + item,
                    id_server: '0' + item,
                    id_segured: '10' + item,
                    pais: 1
                });
                ID_1716569383_m.add(ID_1716569383_fila);
                ID_1716569383_fila.save();
            });
            var ID_1768795363_i = Alloy.createCollection('muros_tabiques');
            ID_1768795363_i.fetch();
            var ID_1768795363_src = require('helper').query2array(ID_1768795363_i);
            var datos = [];
            _.each(ID_1768795363_src, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'label';
                    if (llave == 'id_segured') newkey = 'valor';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                new_row['estado'] = 0;
                datos.push(new_row);
            });
            $.ID_2073900147.update({
                data: datos
            });
        }
    };
    var ID_761628958 = setTimeout(ID_761628958_func, 1000 * 0.2);

}

$.ID_2057626398.init({
    titulo: 'Entrepisos',
    cargando: 'cargando...',
    __id: 'ALL2057626398',
    oncerrar: Cerrar_ID_1778738355,
    left: 0,
    hint: 'Seleccione entrepisos',
    color: 'verde',
    subtitulo: 'Indique los tipos',
    right: 0,
    onclick: Click_ID_2041349884,
    onafterinit: Afterinit_ID_537546359
});

function Click_ID_2041349884(e) {

    var evento = e;

}

function Cerrar_ID_1778738355(e) {

    var evento = e;
    $.ID_2057626398.update({});
    $.nivel.set({
        ids_entrepisos: evento.valores
    });
    if ('nivel' in $) $nivel = $.nivel.toJSON();

}

function Afterinit_ID_537546359(e) {

    var evento = e;
    var ID_1534965449_func = function() {
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
            var ID_303315958_i = Alloy.createCollection('entrepisos');
            var ID_303315958_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
            ID_303315958_i.fetch({
                query: 'SELECT * FROM entrepisos WHERE pais_texto=\'' + pais[0].nombre + '\''
            });
            var entrepisos = require('helper').query2array(ID_303315958_i);
            var datos = [];
            _.each(entrepisos, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'label';
                    if (llave == 'id_segured') newkey = 'valor';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                new_row['estado'] = 0;
                datos.push(new_row);
            });
            $.ID_2057626398.update({
                data: datos
            });
        } else {
            var ID_1944345357_i = Alloy.Collections.entrepisos;
            var sql = "DELETE FROM " + ID_1944345357_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_1944345357_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_1944345357_i.trigger('delete');
            var item_index = 0;
            _.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
                item_index += 1;
                var ID_1499058541_m = Alloy.Collections.entrepisos;
                var ID_1499058541_fila = Alloy.createModel('entrepisos', {
                    nombre: 'Entrepiso' + item,
                    id_server: '0' + item,
                    id_segured: '10' + item,
                    pais: 1
                });
                ID_1499058541_m.add(ID_1499058541_fila);
                ID_1499058541_fila.save();
            });
            var ID_44835984_i = Alloy.createCollection('entrepisos');
            ID_44835984_i.fetch();
            var ID_44835984_src = require('helper').query2array(ID_44835984_i);
            var datos = [];
            _.each(ID_44835984_src, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'label';
                    if (llave == 'id_segured') newkey = 'valor';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                new_row['estado'] = 0;
                datos.push(new_row);
            });
            $.ID_2057626398.update({
                data: datos
            });
        }
    };
    var ID_1534965449 = setTimeout(ID_1534965449_func, 1000 * 0.2);

}

$.ID_1163781653.init({
    titulo: 'Pavimentos',
    cargando: 'cargando...',
    __id: 'ALL1163781653',
    oncerrar: Cerrar_ID_1883198280,
    left: 0,
    hint: 'Seleccione pavimentos',
    color: 'verde',
    subtitulo: 'Indique los tipos',
    right: 0,
    onclick: Click_ID_1342426981,
    onafterinit: Afterinit_ID_594392290
});

function Click_ID_1342426981(e) {

    var evento = e;

}

function Cerrar_ID_1883198280(e) {

    var evento = e;
    $.ID_1163781653.update({});
    $.nivel.set({
        ids_pavimentos: evento.valores
    });
    if ('nivel' in $) $nivel = $.nivel.toJSON();

}

function Afterinit_ID_594392290(e) {

    var evento = e;
    var ID_60762849_func = function() {
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
            var ID_920912481_i = Alloy.createCollection('pavimento');
            var ID_920912481_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
            ID_920912481_i.fetch({
                query: 'SELECT * FROM pavimento WHERE pais_texto=\'' + pais[0].nombre + '\''
            });
            var pavimentos = require('helper').query2array(ID_920912481_i);
            var datos = [];
            _.each(pavimentos, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'label';
                    if (llave == 'id_segured') newkey = 'valor';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                new_row['estado'] = 0;
                datos.push(new_row);
            });
            $.ID_1163781653.update({
                data: datos
            });
        } else {
            var ID_1152477302_i = Alloy.Collections.pavimento;
            var sql = "DELETE FROM " + ID_1152477302_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_1152477302_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_1152477302_i.trigger('delete');
            var item_index = 0;
            _.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
                item_index += 1;
                var ID_1812019101_m = Alloy.Collections.pavimento;
                var ID_1812019101_fila = Alloy.createModel('pavimento', {
                    nombre: 'Pavimento' + item,
                    id_server: '0' + item,
                    id_segured: '10' + item,
                    pais: 1
                });
                ID_1812019101_m.add(ID_1812019101_fila);
                ID_1812019101_fila.save();
            });
            var ID_888785974_i = Alloy.createCollection('pavimento');
            ID_888785974_i.fetch();
            var ID_888785974_src = require('helper').query2array(ID_888785974_i);
            var datos = [];
            _.each(ID_888785974_src, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'label';
                    if (llave == 'id_segured') newkey = 'valor';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                new_row['estado'] = 0;
                datos.push(new_row);
            });
            $.ID_1163781653.update({
                data: datos
            });
        }
    };
    var ID_60762849 = setTimeout(ID_60762849_func, 1000 * 0.2);

}

$.ID_1436403784.init({
    titulo: 'Estruct. cubierta',
    cargando: 'cargando...',
    __id: 'ALL1436403784',
    oncerrar: Cerrar_ID_1796375605,
    left: 0,
    hint: 'Seleccione e.cubiertas',
    color: 'verde',
    subtitulo: 'Indique los tipos',
    right: 0,
    onclick: Click_ID_1805253974,
    onafterinit: Afterinit_ID_301236740
});

function Click_ID_1805253974(e) {

    var evento = e;

}

function Cerrar_ID_1796375605(e) {

    var evento = e;
    $.ID_1436403784.update({});
    $.nivel.set({
        ids_estructura_cubiera: evento.valores
    });
    if ('nivel' in $) $nivel = $.nivel.toJSON();

}

function Afterinit_ID_301236740(e) {

    var evento = e;
    var ID_602043695_func = function() {
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
            var ID_1623473944_i = Alloy.createCollection('estructura_cubierta');
            var ID_1623473944_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
            ID_1623473944_i.fetch({
                query: 'SELECT * FROM estructura_cubierta WHERE pais_texto=\'' + pais[0].nombre + '\''
            });
            var ecubiertas = require('helper').query2array(ID_1623473944_i);
            var datos = [];
            _.each(ecubiertas, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'label';
                    if (llave == 'id_segured') newkey = 'valor';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                new_row['estado'] = 0;
                datos.push(new_row);
            });
            $.ID_1436403784.update({
                data: datos
            });
        } else {
            var ID_465022339_i = Alloy.Collections.estructura_cubierta;
            var sql = "DELETE FROM " + ID_465022339_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_465022339_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_465022339_i.trigger('delete');
            var item_index = 0;
            _.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
                item_index += 1;
                var ID_289810078_m = Alloy.Collections.estructura_cubierta;
                var ID_289810078_fila = Alloy.createModel('estructura_cubierta', {
                    nombre: 'Estru Cubierta ' + item,
                    id_server: '0' + item,
                    id_segured: '10' + item,
                    pais: 1
                });
                ID_289810078_m.add(ID_289810078_fila);
                ID_289810078_fila.save();
            });
            var ID_1472411243_i = Alloy.createCollection('estructura_cubierta');
            ID_1472411243_i.fetch();
            var ID_1472411243_src = require('helper').query2array(ID_1472411243_i);
            var datos = [];
            _.each(ID_1472411243_src, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'label';
                    if (llave == 'id_segured') newkey = 'valor';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                new_row['estado'] = 0;
                datos.push(new_row);
            });
            $.ID_1436403784.update({
                data: datos
            });
        }
    };
    var ID_602043695 = setTimeout(ID_602043695_func, 1000 * 0.2);

}

$.ID_1826404914.init({
    titulo: 'Cubierta',
    cargando: 'cargando...',
    __id: 'ALL1826404914',
    oncerrar: Cerrar_ID_1373806933,
    left: 0,
    hint: 'Seleccione cubiertas',
    color: 'verde',
    subtitulo: 'Indique los tipos',
    right: 0,
    onclick: Click_ID_1224758586,
    onafterinit: Afterinit_ID_1701721686
});

function Click_ID_1224758586(e) {

    var evento = e;

}

function Cerrar_ID_1373806933(e) {

    var evento = e;
    $.ID_1826404914.update({});
    $.nivel.set({
        ids_cubierta: evento.valores
    });
    if ('nivel' in $) $nivel = $.nivel.toJSON();

}

function Afterinit_ID_1701721686(e) {

    var evento = e;
    var ID_1301596767_func = function() {
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
            var ID_793433432_i = Alloy.createCollection('cubierta');
            var ID_793433432_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
            ID_793433432_i.fetch({
                query: 'SELECT * FROM cubierta WHERE pais_texto=\'' + pais[0].nombre + '\''
            });
            var cubiertas = require('helper').query2array(ID_793433432_i);
            var datos = [];
            _.each(cubiertas, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'label';
                    if (llave == 'id_segured') newkey = 'valor';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                new_row['estado'] = 0;
                datos.push(new_row);
            });
            $.ID_1826404914.update({
                data: datos
            });
        } else {
            var ID_1032916351_i = Alloy.Collections.cubierta;
            var sql = "DELETE FROM " + ID_1032916351_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_1032916351_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_1032916351_i.trigger('delete');
            var item_index = 0;
            _.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
                item_index += 1;
                var ID_1913652567_m = Alloy.Collections.cubierta;
                var ID_1913652567_fila = Alloy.createModel('cubierta', {
                    nombre: 'Cubierta ' + item,
                    id_server: '0' + item,
                    id_segured: '10' + item,
                    pais: 1
                });
                ID_1913652567_m.add(ID_1913652567_fila);
                ID_1913652567_fila.save();
            });
            var ID_259258747_i = Alloy.createCollection('cubierta');
            ID_259258747_i.fetch();
            var ID_259258747_src = require('helper').query2array(ID_259258747_i);
            var datos = [];
            _.each(ID_259258747_src, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'label';
                    if (llave == 'id_segured') newkey = 'valor';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                new_row['estado'] = 0;
                datos.push(new_row);
            });
            $.ID_1826404914.update({
                data: datos
            });
        }
    };
    var ID_1301596767 = setTimeout(ID_1301596767_func, 1000 * 0.2);

}

(function() {
    if ((_.isObject(args) || _.isString(args)) && _.isEmpty(args)) {
        if (Ti.App.deployType != 'production') console.log('puta... esta vacio po', {});
    } else {
        if (Ti.App.deployType != 'production') console.log('el nombre que tengo es', {
            "asd": args._data.id
        });
        var ID_1675419462_i = Alloy.createCollection('insp_niveles');
        var ID_1675419462_i_where = 'id=\'' + args._data.id + '\'';
        ID_1675419462_i.fetch({
            query: 'SELECT * FROM insp_niveles WHERE id=\'' + args._data.id + '\''
        });
        var insp_n = require('helper').query2array(ID_1675419462_i);
        if (Ti.App.deployType != 'production') console.log('quiero que muestre todo', {
            "asd": insp_n
        });
        $.nivel.set({
            nombre: insp_n[0].nombre,
            superficie: insp_n[0].superficie,
            largo: insp_n[0].largo,
            alto: insp_n[0].alto,
            ancho: insp_n[0].ancho,
            piso: insp_n[0].piso
        });
        if ('nivel' in $) $nivel = $.nivel.toJSON();
    }
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
        $.nivel.set({
            id_inspeccion: seltarea.id_server
        });
        if ('nivel' in $) $nivel = $.nivel.toJSON();
        require('vars')[_var_scopekey]['faltan_medidas'] = 'false';
        if (seltarea.exige_medidas == 0 || seltarea.exige_medidas == '0') {
            var scroll_view;
            scroll_view = $.ID_668723115;
            var vista_mseguridad;
            vista_mseguridad = $.ID_1390025337;
            scroll_view.remove(vista_mseguridad);
        } else {
            var ID_1390025337_visible = true;

            if (ID_1390025337_visible == 'si') {
                ID_1390025337_visible = true;
            } else if (ID_1390025337_visible == 'no') {
                ID_1390025337_visible = false;
            }
            $.ID_1390025337.setVisible(ID_1390025337_visible);

        }
    }
    require('vars')[_var_scopekey]['largo'] = '0';
    require('vars')[_var_scopekey]['ancho'] = '0';
    $.ID_668723115.scrollToTop();
    var ID_867366348_func = function() {
        $.ID_1947669263.blur();
        $.ID_1260479449.blur();
        $.ID_1942756801.blur();
        $.ID_1403876333.blur();
        $.ID_1350705723.blur();
        $.ID_1611114284.blur();
    };
    var ID_867366348 = setTimeout(ID_867366348_func, 1000 * 0.2);
    var ID_666932273_func = function() {
        var ID_1407323156_statusbar = '#57BC8B';

        var setearStatusColor = function(ID_1407323156_statusbar) {
            if (OS_IOS) {
                if (ID_1407323156_statusbar == 'light' || ID_1407323156_statusbar == 'claro') {
                    $.ID_1407323156_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.LIGHT_CONTENT);
                } else if (ID_1407323156_statusbar == 'grey' || ID_1407323156_statusbar == 'gris' || ID_1407323156_statusbar == 'gray') {
                    $.ID_1407323156_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.GREY);
                } else if (ID_1407323156_statusbar == 'oscuro' || ID_1407323156_statusbar == 'dark') {
                    $.ID_1407323156_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.DEFAULT);
                }
            } else if (OS_ANDROID) {
                abx.setStatusbarColor(ID_1407323156_statusbar);
            }
        };
        setearStatusColor(ID_1407323156_statusbar);

    };
    var ID_666932273 = setTimeout(ID_666932273_func, 1000 * 0.1);
})();

function Postlayout_ID_1442980789(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var ID_1840862384_func = function() {
        /** 
         * Una vez ya cargada la tarea, dejamos la variable limpia para poder abrir otras pantallas 
         */
        require('vars')['var_abriendo'] = '';
    };
    var ID_1840862384 = setTimeout(ID_1840862384_func, 1000 * 0.2);

}
if (OS_IOS || OS_ANDROID) {
    $.ID_1407323156.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}