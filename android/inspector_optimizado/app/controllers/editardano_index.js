var _bind4section = {};
var _list_templates = {
    "elemento": {
        "ID_432412973": {},
        "ID_291370846": {
            "text": "{nombre}"
        },
        "ID_1925487537": {
            "text": "{id_server}"
        },
        "ID_975786586": {
            "text": "{id_segured}"
        },
        "ID_1546190443": {}
    }
};
var $dano = $.dano.toJSON();

var _activity;
if (OS_ANDROID) {
    _activity = $.ID_975866950.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_975866950';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_975866950.addEventListener('open', function(e) {});
}
$.ID_975866950.orientationModes = [Titanium.UI.PORTRAIT];


$.ID_1312087199.init({
    titulo: 'EDITAR DAÑO',
    __id: 'ALL1312087199',
    textoderecha: 'Guardar',
    oncerrar: Cerrar_ID_675876015,
    fondo: 'fondomorado',
    top: 0,
    modal: '',
    onpresiono: Presiono_ID_974796395,
    colortextoderecha: 'blanco'
});

function Cerrar_ID_675876015(e) {

    var evento = e;
    /** 
     * Limpiamos widget 
     */
    $.ID_836121132.limpiar({});
    $.ID_1552750321.limpiar({});
    $.ID_975866950.close();

}

function Presiono_ID_974796395(e) {

    var evento = e;
    /** 
     * Desenfocamos todos los campos de texto 
     */
    $.ID_144768712.blur();
    /** 
     * Flag para revisar que todo este en orden 
     */
    require('vars')[_var_scopekey]['todobien'] = 'true';
    if (_.isUndefined($dano.id_partida)) {
        /** 
         * Validamos que los campos ingresados sean correctos 
         */
        require('vars')[_var_scopekey]['todobien'] = 'false';
        var ID_1547981690_opts = ['Aceptar'];
        var ID_1547981690 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Debe indicar la partida',
            buttonNames: ID_1547981690_opts
        });
        ID_1547981690.addEventListener('click', function(e) {
            var nulo = ID_1547981690_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1547981690.show();
    } else if ((_.isObject($dano.id_tipodano) || _.isString($dano.id_tipodano)) && _.isEmpty($dano.id_tipodano)) {
        require('vars')[_var_scopekey]['todobien'] = 'false';
        var ID_1977522461_opts = ['Aceptar'];
        var ID_1977522461 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Seleccione el tipo de daño',
            buttonNames: ID_1977522461_opts
        });
        ID_1977522461.addEventListener('click', function(e) {
            var nulo = ID_1977522461_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1977522461.show();
    } else if ((_.isObject($dano.id_unidadmedida) || _.isString($dano.id_unidadmedida)) && _.isEmpty($dano.id_unidadmedida)) {
        require('vars')[_var_scopekey]['todobien'] = 'false';
        var ID_904979675_opts = ['Aceptar'];
        var ID_904979675 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Seleccione la unidad de medida del daño',
            buttonNames: ID_904979675_opts
        });
        ID_904979675.addEventListener('click', function(e) {
            var nulo = ID_904979675_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_904979675.show();
    } else if (_.isUndefined($dano.id_unidadmedida)) {
        require('vars')[_var_scopekey]['todobien'] = 'false';
        var ID_1258246206_opts = ['Aceptar'];
        var ID_1258246206 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Debe indicar la unidad de medida',
            buttonNames: ID_1258246206_opts
        });
        ID_1258246206.addEventListener('click', function(e) {
            var nulo = ID_1258246206_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1258246206.show();
    } else if (_.isUndefined($dano.superficie)) {
        require('vars')[_var_scopekey]['todobien'] = 'false';
        var ID_1022824055_opts = ['Aceptar'];
        var ID_1022824055 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Debe indicar la superficie del recinto',
            buttonNames: ID_1022824055_opts
        });
        ID_1022824055.addEventListener('click', function(e) {
            var nulo = ID_1022824055_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1022824055.show();
    } else if ((_.isObject($dano.superficie) || _.isString($dano.superficie)) && _.isEmpty($dano.superficie)) {
        require('vars')[_var_scopekey]['todobien'] = 'false';
        var ID_1371429567_opts = ['Aceptar'];
        var ID_1371429567 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese la cubicación del daño',
            buttonNames: ID_1371429567_opts
        });
        ID_1371429567.addEventListener('click', function(e) {
            var nulo = ID_1371429567_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1371429567.show();
    }
    var todobien = ('todobien' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['todobien'] : '';
    if (todobien == true || todobien == 'true') {
        /** 
         * Eliminamos modelo previo 
         */
        /** 
         * Eliminamos modelo previo 
         */
        var ID_1197838253_i = Alloy.Collections.insp_itemdanos;
        var sql = 'DELETE FROM ' + ID_1197838253_i.config.adapter.collection_name + ' WHERE id=\'' + args._dato.id + '\'';
        var db = Ti.Database.open(ID_1197838253_i.config.adapter.db_name);
        db.execute(sql);
        db.close();
        sql = null;
        db = null;
        ID_1197838253_i.trigger('delete');
        /** 
         * Guardamos modelo nuevo 
         */
        Alloy.Collections[$.dano.config.adapter.collection_name].add($.dano);
        $.dano.save();
        Alloy.Collections[$.dano.config.adapter.collection_name].fetch();
        var ID_216701314_func = function() {
            /** 
             * limpiamos widgets multiples (memoria) 
             */
            $.ID_1552750321.limpiar({});
            $.ID_975866950.close();
        };
        var ID_216701314 = setTimeout(ID_216701314_func, 1000 * 0.1);
    }

}

function Click_ID_1074330048(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    /** 
     * Ocupamos esta variable para impedir que la pantalla siguente se cargue dos veces 
     */
    var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
    if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
        require('vars')['var_abriendo'] = 'true';
        Alloy.createController("listadodanos_index", {}).getView().open();
    }

}

$.ID_1552750321.init({
    titulo: 'TIPO DAÑO',
    cargando: 'cargando...',
    __id: 'ALL1552750321',
    left: 0,
    onrespuesta: Respuesta_ID_1874815480,
    campo: 'Tipo de Daño',
    onabrir: Abrir_ID_1258345720,
    color: 'morado',
    right: 0,
    seleccione: 'seleccione tipo',
    activo: true,
    onafterinit: Afterinit_ID_1491420104
});

function Abrir_ID_1258345720(e) {

    var evento = e;

}

function Respuesta_ID_1874815480(e) {

    var evento = e;
    /** 
     * Mostramos el valor seleccionado desde el widget 
     */
    $.ID_1552750321.labels({
        valor: evento.valor
    });
    /** 
     * Actualizamos el id_tipodano e id_unidadmedida 
     */
    $.dano.set({
        id_tipodano: evento.item.id_interno,
        id_unidadmedida: ''
    });
    if ('dano' in $) $dano = $.dano.toJSON();
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
        /** 
         * Consultamos id_gerardo en tipo_accion, para obtener unidades de medida 
         */
        var ID_1294120908_i = Alloy.createCollection('tipo_accion');
        var ID_1294120908_i_where = 'id_tipo_dano=\'' + evento.item.id_gerardo + '\'';
        ID_1294120908_i.fetch({
            query: 'SELECT * FROM tipo_accion WHERE id_tipo_dano=\'' + evento.item.id_gerardo + '\''
        });
        var acciones = require('helper').query2array(ID_1294120908_i);
        if (acciones && acciones.length) {
            var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
            var ID_1719798568_i = Alloy.createCollection('unidad_medida');
            var ID_1719798568_i_where = 'pais_texto=\'' + pais[0].nombre + '\' AND id_accion=\'' + acciones[0].id_server + '\'';
            ID_1719798568_i.fetch({
                query: 'SELECT * FROM unidad_medida WHERE pais_texto=\'' + pais[0].nombre + '\' AND id_accion=\'' + acciones[0].id_server + '\''
            });
            var unidad = require('helper').query2array(ID_1719798568_i);
            /** 
             * Transformamos nombres de tablas 
             */
            var datos = [];
            _.each(unidad, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'valor';
                    if (llave == 'id_segured') newkey = 'id_interno';
                    if (llave == 'id_server') newkey = 'id_gerardo';
                    if (llave == 'abrev') newkey = 'abrev';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                datos.push(new_row);
            });
            /** 
             * Cargamos el widget con los datos de tipo de unidad de medida 
             */
            $.ID_836121132.data({
                data: datos
            });
        }
    }
    /** 
     * Actualizamos texto de widget unidad de medida 
     */
    $.ID_836121132.labels({
        seleccione: 'unidad'
    });
    /** 
     * Limpiamos texto en superficie 
     */
    $.ID_1938962380.setValue('');

    $.ID_859260223.setText('-');


}

function Afterinit_ID_1491420104(e) {

    var evento = e;
    var ID_1171014616_func = function() {
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        /** 
         * obtenemos valor seleccionado previamente y lo mostramos en selector con desface para que pueda mostrar bien el valor en pantalla, si no, carga un valor erroneo 
         */
        var ID_1383954509_i = Alloy.createCollection('insp_itemdanos');
        var ID_1383954509_i_where = 'id=\'' + args._dato.id + '\'';
        ID_1383954509_i.fetch({
            query: 'SELECT * FROM insp_itemdanos WHERE id=\'' + args._dato.id + '\''
        });
        var insp_datos = require('helper').query2array(ID_1383954509_i);
        if (insp_datos && insp_datos.length) {
            var ID_300384885_i = Alloy.createCollection('tipo_dano');
            var ID_300384885_i_where = 'id_segured=\'' + insp_datos[0].id_tipodano + '\'';
            ID_300384885_i.fetch({
                query: 'SELECT * FROM tipo_dano WHERE id_segured=\'' + insp_datos[0].id_tipodano + '\''
            });
            var tipo_dano = require('helper').query2array(ID_300384885_i);
            if (tipo_dano && tipo_dano.length) {
                var ID_1940471085_func = function() {
                    $.ID_1552750321.labels({
                        valor: tipo_dano[0].nombre
                    });
                    if (Ti.App.deployType != 'production') console.log('el nombre de la dano es', {
                        "datos": tipo_dano[0].nombre
                    });
                };
                var ID_1940471085 = setTimeout(ID_1940471085_func, 1000 * 0.21);
            }
        }
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
            /** 
             * obtenemos datos para selectores (solo los de este pais) 
             */
            var ID_920681116_i = Alloy.createCollection('tipo_dano');
            var ID_920681116_i_where = 'pais_texto=\'' + pais[0].nombre + '\' AND id_partida=0';
            ID_920681116_i.fetch({
                query: 'SELECT * FROM tipo_dano WHERE pais_texto=\'' + pais[0].nombre + '\' AND id_partida=0'
            });
            var danos = require('helper').query2array(ID_920681116_i);
            var datos = [];
            _.each(danos, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'valor';
                    if (llave == 'id_segured') newkey = 'id_interno';
                    if (llave == 'id_server') newkey = 'id_gerardo';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                datos.push(new_row);
            });
        } else {
            /** 
             * Insertamos dummies 
             */
            /** 
             * Insertamos dummies 
             */
            var ID_320226351_i = Alloy.Collections.tipo_dano;
            var sql = "DELETE FROM " + ID_320226351_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_320226351_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_320226351_i.trigger('delete');
            var item_index = 0;
            _.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
                item_index += 1;
                var ID_168087130_m = Alloy.Collections.tipo_dano;
                var ID_168087130_fila = Alloy.createModel('tipo_dano', {
                    nombre: 'Picada' + item,
                    id_partida: 0,
                    id_server: '0' + item,
                    id_segured: '10' + item,
                    pais: 1
                });
                ID_168087130_m.add(ID_168087130_fila);
                ID_168087130_fila.save();
            });
            var ID_226186081_i = Alloy.createCollection('tipo_dano');
            ID_226186081_i.fetch();
            var ID_226186081_src = require('helper').query2array(ID_226186081_i);
            var datos = [];
            _.each(ID_226186081_src, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'valor';
                    if (llave == 'id_segured') newkey = 'id_interno';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                datos.push(new_row);
            });
        }
        /** 
         * Cargamos el widget con los datos de tipo de unidad de medida 
         */
        $.ID_1552750321.data({
            data: datos
        });
    };
    var ID_1171014616 = setTimeout(ID_1171014616_func, 1000 * 0.2);

}

$.ID_1179966976.init({
    caja: 45,
    __id: 'ALL1179966976',
    onlisto: Listo_ID_111394375,
    left: 0,
    top: 0,
    onclick: Click_ID_1296369179
});

function Click_ID_1296369179(e) {

    var evento = e;
    /** 
     * Definimos que estamos capturando foto en el primer item 
     */
    require('vars')[_var_scopekey]['cual_foto'] = '1';
    /** 
     * Abrimos camara 
     */
    $.ID_1198539205.disparar({});
    /** 
     * Detenemos las animaciones de los widget 
     */
    $.ID_1179966976.detener({});
    $.ID_1092563406.detener({});
    $.ID_1431154103.detener({});

}

function Listo_ID_111394375(e) {

    var evento = e;
    /** 
     * Recuperamos variables para poder definir la estructura de las carpetas donde se guardara la miniatura 
     */
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    var nuevoid_f1 = ('nuevoid_f1' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['nuevoid_f1'] : '';
    if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
        var ID_153663287_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + seltarea.id_server);
        if (ID_153663287_d.exists() == false) ID_153663287_d.createDirectory();
        var ID_153663287_f = Ti.Filesystem.getFile(ID_153663287_d.resolve(), 'mini' + nuevoid_f1.id + '.jpg');
        if (ID_153663287_f.exists() == true) ID_153663287_f.deleteFile();
        ID_153663287_f.write(evento.mini);
        ID_153663287_d = null;
        ID_153663287_f = null;
    } else {
        var ID_327315158_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + 0);
        if (ID_327315158_d.exists() == false) ID_327315158_d.createDirectory();
        var ID_327315158_f = Ti.Filesystem.getFile(ID_327315158_d.resolve(), 'mini' + nuevoid_f1.id + '.jpg');
        if (ID_327315158_f.exists() == true) ID_327315158_f.deleteFile();
        ID_327315158_f.write(evento.mini);
        ID_327315158_d = null;
        ID_327315158_f = null;
    }
}

function Click_ID_605054968(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var evento = e;
    /** 
     * Definimos que estamos capturando foto en el primer item 
     */
    require('vars')[_var_scopekey]['cual_foto'] = '1';
    /** 
     * Abrimos camara 
     */
    $.ID_1198539205.disparar({});
    /** 
     * Detenemos las animaciones de los widget 
     */
    $.ID_1179966976.detener({});
    $.ID_1092563406.detener({});
    $.ID_1431154103.detener({});
    /** 
     * Ocultamos la imagen obtenida desde la memoria del telefono para actualizar con la que acabamos de obtener 
     */
    var ID_1470272109_visible = false;

    if (ID_1470272109_visible == 'si') {
        ID_1470272109_visible = true;
    } else if (ID_1470272109_visible == 'no') {
        ID_1470272109_visible = false;
    }
    $.ID_1470272109.setVisible(ID_1470272109_visible);


}

$.ID_1092563406.init({
    caja: 45,
    __id: 'ALL1092563406',
    onlisto: Listo_ID_288108398,
    left: 5,
    top: 0,
    onclick: Click_ID_1334667756
});

function Click_ID_1334667756(e) {

    var evento = e;
    require('vars')[_var_scopekey]['cual_foto'] = 2;
    $.ID_1198539205.disparar({});
    $.ID_1092563406.detener({});
    $.ID_1179966976.detener({});
    $.ID_1431154103.detener({});

}

function Listo_ID_288108398(e) {

    var evento = e;
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    var nuevoid_f2 = ('nuevoid_f2' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['nuevoid_f2'] : '';
    if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
        var ID_462637464_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + seltarea.id_server);
        if (ID_462637464_d.exists() == false) ID_462637464_d.createDirectory();
        var ID_462637464_f = Ti.Filesystem.getFile(ID_462637464_d.resolve(), 'mini' + nuevoid_f2.id + '.jpg');
        if (ID_462637464_f.exists() == true) ID_462637464_f.deleteFile();
        ID_462637464_f.write(evento.mini);
        ID_462637464_d = null;
        ID_462637464_f = null;
    } else {
        var ID_1888580742_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + 0);
        if (ID_1888580742_d.exists() == false) ID_1888580742_d.createDirectory();
        var ID_1888580742_f = Ti.Filesystem.getFile(ID_1888580742_d.resolve(), 'mini' + nuevoid_f2.id + '.jpg');
        if (ID_1888580742_f.exists() == true) ID_1888580742_f.deleteFile();
        ID_1888580742_f.write(evento.mini);
        ID_1888580742_d = null;
        ID_1888580742_f = null;
    }
}

function Click_ID_744487200(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var evento = e;
    require('vars')[_var_scopekey]['cual_foto'] = 2;
    $.ID_1198539205.disparar({});
    $.ID_1179966976.detener({});
    $.ID_1092563406.detener({});
    $.ID_1431154103.detener({});
    var ID_135662569_visible = false;

    if (ID_135662569_visible == 'si') {
        ID_135662569_visible = true;
    } else if (ID_135662569_visible == 'no') {
        ID_135662569_visible = false;
    }
    $.ID_135662569.setVisible(ID_135662569_visible);


}

$.ID_1431154103.init({
    caja: 45,
    __id: 'ALL1431154103',
    onlisto: Listo_ID_1693266204,
    left: 5,
    top: 0,
    onclick: Click_ID_2023033229
});

function Click_ID_2023033229(e) {

    var evento = e;
    require('vars')[_var_scopekey]['cual_foto'] = 3;
    $.ID_1198539205.disparar({});
    $.ID_1431154103.detener({});
    $.ID_1179966976.detener({});
    $.ID_1092563406.detener({});

}

function Listo_ID_1693266204(e) {

    var evento = e;
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    var nuevoid_f3 = ('nuevoid_f3' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['nuevoid_f3'] : '';
    if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
        var ID_1959180332_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + seltarea.id_server);
        if (ID_1959180332_d.exists() == false) ID_1959180332_d.createDirectory();
        var ID_1959180332_f = Ti.Filesystem.getFile(ID_1959180332_d.resolve(), 'mini' + nuevoid_f3.id + '.jpg');
        if (ID_1959180332_f.exists() == true) ID_1959180332_f.deleteFile();
        ID_1959180332_f.write(evento.mini);
        ID_1959180332_d = null;
        ID_1959180332_f = null;
    } else {
        var ID_946227262_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + 0);
        if (ID_946227262_d.exists() == false) ID_946227262_d.createDirectory();
        var ID_946227262_f = Ti.Filesystem.getFile(ID_946227262_d.resolve(), 'mini' + nuevoid_f3.id + '.jpg');
        if (ID_946227262_f.exists() == true) ID_946227262_f.deleteFile();
        ID_946227262_f.write(evento.mini);
        ID_946227262_d = null;
        ID_946227262_f = null;
    }
}

function Click_ID_654111818(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var evento = e;
    require('vars')[_var_scopekey]['cual_foto'] = 3;
    $.ID_1198539205.disparar({});
    $.ID_1179966976.detener({});
    $.ID_1092563406.detener({});
    $.ID_1431154103.detener({});
    var ID_719771746_visible = false;

    if (ID_719771746_visible == 'si') {
        ID_719771746_visible = true;
    } else if (ID_719771746_visible == 'no') {
        ID_719771746_visible = false;
    }
    $.ID_719771746.setVisible(ID_719771746_visible);


}

$.ID_836121132.init({
    titulo: 'UNIDAD',
    cargando: 'cargando...',
    __id: 'ALL836121132',
    onrespuesta: Respuesta_ID_1933786536,
    campo: 'Unidad de medida',
    onabrir: Abrir_ID_1700750023,
    color: 'morado',
    seleccione: 'unidad',
    activo: true,
    onafterinit: Afterinit_ID_397127805
});

function Abrir_ID_1700750023(e) {

    var evento = e;

}

function Respuesta_ID_1933786536(e) {

    var evento = e;
    /** 
     * Mostramos el valor seleccionado desde el widget 
     */
    $.ID_836121132.labels({
        valor: evento.valor
    });
    /** 
     * Actualizamos el id de la unidad de medida 
     */
    $.dano.set({
        id_unidadmedida: evento.item.id_interno
    });
    if ('dano' in $) $dano = $.dano.toJSON();
    /** 
     * modifica el label con la abreviatura de la unidad de medida seleccionada. 
     */
    $.ID_859260223.setText(evento.item.abrev);


}

function Afterinit_ID_397127805(e) {

    var evento = e;
    var ID_1083031413_func = function() {
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            /** 
             * Viene de login 
             */
            var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
            /** 
             * Consultamos modelo unidad de medida y filtramos por el pais y el id_accion, posteriormente cambiamos nombres de tablas 
             */
            var ID_1231934327_i = Alloy.createCollection('unidad_medida');
            var ID_1231934327_i_where = 'pais_texto=\'' + pais[0].nombre + '\' AND id_accion=0';
            ID_1231934327_i.fetch({
                query: 'SELECT * FROM unidad_medida WHERE pais_texto=\'' + pais[0].nombre + '\' AND id_accion=0'
            });
            var unidad = require('helper').query2array(ID_1231934327_i);
            /** 
             * Obtenemos datos para selectores (solo los de este pais) 
             */
            var datos = [];
            _.each(unidad, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'valor';
                    if (llave == 'id_segured') newkey = 'id_interno';
                    if (llave == 'id_server') newkey = 'id_gerardo';
                    if (llave == 'abrev') newkey = 'abrev';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                datos.push(new_row);
            });
        }
        /** 
         * obtenemos valor seleccionado previamente y lo mostramos en selector con desface para que pueda mostrar bien el valor en pantalla, si no, carga un valor erroneo 
         */
        var ID_543035304_i = Alloy.createCollection('insp_itemdanos');
        var ID_543035304_i_where = 'id=\'' + args._dato.id + '\'';
        ID_543035304_i.fetch({
            query: 'SELECT * FROM insp_itemdanos WHERE id=\'' + args._dato.id + '\''
        });
        var insp_datos = require('helper').query2array(ID_543035304_i);
        if (insp_datos && insp_datos.length) {
            var ID_1939692606_i = Alloy.createCollection('unidad_medida');
            var ID_1939692606_i_where = 'id_segured=\'' + insp_datos[0].id_unidadmedida + '\'';
            ID_1939692606_i.fetch({
                query: 'SELECT * FROM unidad_medida WHERE id_segured=\'' + insp_datos[0].id_unidadmedida + '\''
            });
            var unidad_medida = require('helper').query2array(ID_1939692606_i);
            if (unidad_medida && unidad_medida.length) {
                var ID_818615281_func = function() {
                    $.ID_836121132.labels({
                        valor: unidad_medida[0].nombre
                    });
                    if (Ti.App.deployType != 'production') console.log('el nombre de la unidad es', {
                        "datos": unidad_medida[0]
                    });
                    $.ID_859260223.setText(unidad_medida[0].abrev);

                };
                var ID_818615281 = setTimeout(ID_818615281_func, 1000 * 0.21);
            }
        }
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            /** 
             * Viene de login 
             */
            var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
            var ID_199047975_i = Alloy.createCollection('unidad_medida');
            var ID_199047975_i_where = 'pais_texto=\'' + pais[0].nombre + '\' AND id_accion=0';
            ID_199047975_i.fetch({
                query: 'SELECT * FROM unidad_medida WHERE pais_texto=\'' + pais[0].nombre + '\' AND id_accion=0'
            });
            var unidad = require('helper').query2array(ID_199047975_i);
            /** 
             * obtenemos datos para selectores (solo los de este pais) 
             */
            var datos = [];
            _.each(unidad, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'valor';
                    if (llave == 'id_segured') newkey = 'id_interno';
                    if (llave == 'id_server') newkey = 'id_gerardo';
                    if (llave == 'abrev') newkey = 'abrev';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                datos.push(new_row);
            });
        } else {
            /** 
             * Insertamos dummies 
             */
            /** 
             * Insertamos dummies 
             */
            var ID_525725175_i = Alloy.Collections.unidad_medida;
            var sql = "DELETE FROM " + ID_525725175_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_525725175_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_525725175_i.trigger('delete');
            var item_index = 0;
            _.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
                item_index += 1;
                var ID_1629045991_m = Alloy.Collections.unidad_medida;
                var ID_1629045991_fila = Alloy.createModel('unidad_medida', {
                    nombre: 'Metro lineal' + item,
                    id_server: '0' + item,
                    id_segured: '10' + item,
                    pais: 1,
                    abrev: 'ML' + item
                });
                ID_1629045991_m.add(ID_1629045991_fila);
                ID_1629045991_fila.save();
            });
            /** 
             * Transformamos nombres de tablas 
             */
            var ID_17900159_i = Alloy.createCollection('unidad_medida');
            ID_17900159_i.fetch();
            var ID_17900159_src = require('helper').query2array(ID_17900159_i);
            var datos = [];
            _.each(ID_17900159_src, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'valor';
                    if (llave == 'id_segured') newkey = 'id_interno';
                    if (llave == 'id_server') newkey = 'id_gerardo';
                    if (llave == 'abrev') newkey = 'abrev';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                datos.push(new_row);
            });
        }
        /** 
         * Cargamos el widget con los datos de tipo de unidad de medida 
         */
        $.ID_836121132.data({
            data: datos
        });
    };
    var ID_1083031413 = setTimeout(ID_1083031413_func, 1000 * 0.2);

}

function Change_ID_604509885(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    /** 
     * Al escribir la descripcion, vamos actualizando en la tabla de danos la superficie 
     */
    $.dano.set({
        superficie: elemento
    });
    if ('dano' in $) $dano = $.dano.toJSON();
    elemento = null, source = null;

}

function Return_ID_1773065281(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    /** 
     * Al presionar enter en el teclado del telefono, desenfocamos el campo 
     */
    $.ID_1938962380.blur();
    elemento = null, source = null;

}

function Change_ID_26494007(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    /** 
     * Al escribir la descripcion, vamos actualizando en la tabla de danos la descripcion del dano 
     */
    $.dano.set({
        descripcion_dano: elemento
    });
    if ('dano' in $) $dano = $.dano.toJSON();
    elemento = null, source = null;

}

$.ID_1198539205.init({
    onfotolista: Fotolista_ID_1544242732,
    __id: 'ALL1198539205'
});

function Fotolista_ID_1544242732(e) {

    var evento = e;
    /** 
     * Recuperamos cual fue la foto seleccionada 
     */
    var cual_foto = ('cual_foto' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['cual_foto'] : '';
    if (cual_foto == 1 || cual_foto == '1') {
        var ID_793909866_m = Alloy.Collections.numero_unico;
        var ID_793909866_fila = Alloy.createModel('numero_unico', {
            comentario: 'nuevo itemdano foto1'
        });
        ID_793909866_m.add(ID_793909866_fila);
        ID_793909866_fila.save();
        var nuevoid_f1 = require('helper').model2object(ID_793909866_m.last());
        /** 
         * Guardamos en variable el detalle del modelo numero_unico para asignar informacion de foto1 del dano 
         */
        require('vars')[_var_scopekey]['nuevoid_f1'] = nuevoid_f1;
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        var gps_coords = ('gps_coords' in require('vars')) ? require('vars')['gps_coords'] : '';
        var gps_latitud = ('gps_latitud' in require('vars')) ? require('vars')['gps_latitud'] : '';
        var gps_longitud = ('gps_longitud' in require('vars')) ? require('vars')['gps_longitud'] : '';
        var resultado = null;
        if ('distanceinmtbetweenearthcoordinates' in require('funciones')) {
            resultado = require('funciones').distanceinmtbetweenearthcoordinates({
                'lat1': gps_latitud,
                'lon1': gps_longitud,
                'lat2': seltarea.lat,
                'lon2': seltarea.lon
            });
        } else {
            try {
                resultado = f_distanceinmtbetweenearthcoordinates({
                    'lat1': gps_latitud,
                    'lon1': gps_longitud,
                    'lat2': seltarea.lat,
                    'lon2': seltarea.lon
                });
            } catch (ee) {}
        }
        if (_.isNumber(gps_coords.accuracy) && _.isNumber(resultado) && gps_coords.accuracy > resultado) {
            /** 
             * Pablo me comento que el flag fuera_rango se determina comparando precision vs la distancia de ubicacion con la definida en tarea 
             */
            var json_datosfoto = {
                lat: gps_latitud,
                lng: gps_longitud,
                fuera_rango: 0,
                distancia: Math.round(resultado),
                precision: Math.round(gps_coords.accuracy)
            };
        } else {
            var json_datosfoto = {
                lat: gps_latitud,
                lng: gps_longitud,
                fuera_rango: 1,
                distancia: Math.round(resultado),
                precision: Math.round(gps_coords.accuracy)
            };
        }
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var ID_1182736960_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
            if (ID_1182736960_d.exists() == false) ID_1182736960_d.createDirectory();
            var ID_1182736960_f = Ti.Filesystem.getFile(ID_1182736960_d.resolve(), 'cap' + nuevoid_f1.id + '.jpg');
            if (ID_1182736960_f.exists() == true) ID_1182736960_f.deleteFile();
            ID_1182736960_f.write(evento.foto);
            ID_1182736960_d = null;
            ID_1182736960_f = null;
            var ID_1103277653_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
            if (ID_1103277653_d.exists() == false) ID_1103277653_d.createDirectory();
            var ID_1103277653_f = Ti.Filesystem.getFile(ID_1103277653_d.resolve(), 'cap' + nuevoid_f1.id + '.json');
            if (ID_1103277653_f.exists() == true) ID_1103277653_f.deleteFile();
            console.log('contenido f1 edano', JSON.stringify(json_datosfoto));
            ID_1103277653_f.write(JSON.stringify(json_datosfoto));
            ID_1103277653_d = null;
            ID_1103277653_f = null;
        } else {
            var ID_598482667_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + 0);
            if (ID_598482667_d.exists() == false) ID_598482667_d.createDirectory();
            var ID_598482667_f = Ti.Filesystem.getFile(ID_598482667_d.resolve(), 'cap' + nuevoid_f1.id + '.jpg');
            if (ID_598482667_f.exists() == true) ID_598482667_f.deleteFile();
            ID_598482667_f.write(evento.foto);
            ID_598482667_d = null;
            ID_598482667_f = null;
        }
        /** 
         * Actualizamos el modelo indicando cual es el nombre de la imagen 
         */
        $.dano.set({
            foto1: 'cap' + nuevoid_f1.id + '.jpg'
        });
        if ('dano' in $) $dano = $.dano.toJSON();
        /** 
         * Enviamos la foto para que genere la miniatura (y tambien la guarde) y muestre en pantalla 
         */
        $.ID_1179966976.procesar({
            imagen: evento.foto,
            nueva: 640,
            calidad: 91,
            camara: 'trasera'
        });
        /** 
         * Limpiamos memoria ram 
         */
        evento = null;
    } else if (cual_foto == 2) {
        var ID_1250883617_m = Alloy.Collections.numero_unico;
        var ID_1250883617_fila = Alloy.createModel('numero_unico', {
            comentario: 'nuevo itemdano foto2'
        });
        ID_1250883617_m.add(ID_1250883617_fila);
        ID_1250883617_fila.save();
        var nuevoid_f2 = require('helper').model2object(ID_1250883617_m.last());
        /** 
         * Guardamos en variable el detalle del modelo numero_unico para asignar informacion de foto1 del dano 
         */
        require('vars')[_var_scopekey]['nuevoid_f2'] = nuevoid_f2;
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        var gps_coords = ('gps_coords' in require('vars')) ? require('vars')['gps_coords'] : '';
        var gps_latitud = ('gps_latitud' in require('vars')) ? require('vars')['gps_latitud'] : '';
        var gps_longitud = ('gps_longitud' in require('vars')) ? require('vars')['gps_longitud'] : '';
        var resultado = null;
        if ('distanceinmtbetweenearthcoordinates' in require('funciones')) {
            resultado = require('funciones').distanceinmtbetweenearthcoordinates({
                'lat1': gps_latitud,
                'lon1': gps_longitud,
                'lat2': seltarea.lat,
                'lon2': seltarea.lon
            });
        } else {
            try {
                resultado = f_distanceinmtbetweenearthcoordinates({
                    'lat1': gps_latitud,
                    'lon1': gps_longitud,
                    'lat2': seltarea.lat,
                    'lon2': seltarea.lon
                });
            } catch (ee) {}
        }
        if (_.isNumber(gps_coords.accuracy) && _.isNumber(resultado) && gps_coords.accuracy > resultado) {
            /** 
             * Pablo me comento que el flag fuera_rango se determina comparando precision vs la distancia de ubicacion con la definida en tarea 
             */
            var json_datosfoto = {
                lat: gps_latitud,
                lng: gps_longitud,
                fuera_rango: 0,
                distancia: Math.round(resultado),
                precision: Math.round(gps_coords.accuracy)
            };
        } else {
            var json_datosfoto = {
                lat: gps_latitud,
                lng: gps_longitud,
                fuera_rango: 1,
                distancia: Math.round(resultado),
                precision: Math.round(gps_coords.accuracy)
            };
        }
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var ID_1001672872_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
            if (ID_1001672872_d.exists() == false) ID_1001672872_d.createDirectory();
            var ID_1001672872_f = Ti.Filesystem.getFile(ID_1001672872_d.resolve(), 'cap' + nuevoid_f2.id + '.jpg');
            if (ID_1001672872_f.exists() == true) ID_1001672872_f.deleteFile();
            ID_1001672872_f.write(evento.foto);
            ID_1001672872_d = null;
            ID_1001672872_f = null;
            var ID_633641756_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
            if (ID_633641756_d.exists() == false) ID_633641756_d.createDirectory();
            var ID_633641756_f = Ti.Filesystem.getFile(ID_633641756_d.resolve(), 'cap' + nuevoid_f2.id + '.json');
            if (ID_633641756_f.exists() == true) ID_633641756_f.deleteFile();
            console.log('contenido f2 edano', JSON.stringify(json_datosfoto));
            ID_633641756_f.write(JSON.stringify(json_datosfoto));
            ID_633641756_d = null;
            ID_633641756_f = null;
        } else {
            var ID_1224361304_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + 0);
            if (ID_1224361304_d.exists() == false) ID_1224361304_d.createDirectory();
            var ID_1224361304_f = Ti.Filesystem.getFile(ID_1224361304_d.resolve(), 'cap' + nuevoid_f1.id + '.jpg');
            if (ID_1224361304_f.exists() == true) ID_1224361304_f.deleteFile();
            ID_1224361304_f.write(evento.foto);
            ID_1224361304_d = null;
            ID_1224361304_f = null;
        }
        $.dano.set({
            foto2: 'cap' + nuevoid_f2.id + '.jpg'
        });
        if ('dano' in $) $dano = $.dano.toJSON();
        $.ID_1092563406.procesar({
            imagen: evento.foto,
            nueva: 640,
            calidad: 91,
            camara: 'trasera'
        });
        evento = null;
    } else if (cual_foto == 3) {
        var ID_1546491761_m = Alloy.Collections.numero_unico;
        var ID_1546491761_fila = Alloy.createModel('numero_unico', {
            comentario: 'nuevo itemdano foto3'
        });
        ID_1546491761_m.add(ID_1546491761_fila);
        ID_1546491761_fila.save();
        var nuevoid_f3 = require('helper').model2object(ID_1546491761_m.last());
        /** 
         * Guardamos en variable el detalle del modelo numero_unico para asignar informacion de foto1 del dano 
         */
        require('vars')[_var_scopekey]['nuevoid_f3'] = nuevoid_f3;
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        var gps_coords = ('gps_coords' in require('vars')) ? require('vars')['gps_coords'] : '';
        var gps_latitud = ('gps_latitud' in require('vars')) ? require('vars')['gps_latitud'] : '';
        var gps_longitud = ('gps_longitud' in require('vars')) ? require('vars')['gps_longitud'] : '';
        var resultado = null;
        if ('distanceinmtbetweenearthcoordinates' in require('funciones')) {
            resultado = require('funciones').distanceinmtbetweenearthcoordinates({
                'lat1': gps_latitud,
                'lon1': gps_longitud,
                'lat2': seltarea.lat,
                'lon2': seltarea.lon
            });
        } else {
            try {
                resultado = f_distanceinmtbetweenearthcoordinates({
                    'lat1': gps_latitud,
                    'lon1': gps_longitud,
                    'lat2': seltarea.lat,
                    'lon2': seltarea.lon
                });
            } catch (ee) {}
        }
        if (_.isNumber(gps_coords.accuracy) && _.isNumber(resultado) && gps_coords.accuracy > resultado) {
            /** 
             * Pablo me comento que el flag fuera_rango se determina comparando precision vs la distancia de ubicacion con la definida en tarea 
             */
            var json_datosfoto = {
                lat: gps_latitud,
                lng: gps_longitud,
                fuera_rango: 0,
                distancia: Math.round(resultado),
                precision: Math.round(gps_coords.accuracy)
            };
        } else {
            var json_datosfoto = {
                lat: gps_latitud,
                lng: gps_longitud,
                fuera_rango: 1,
                distancia: Math.round(resultado),
                precision: Math.round(gps_coords.accuracy)
            };
        }
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var ID_251167257_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
            if (ID_251167257_d.exists() == false) ID_251167257_d.createDirectory();
            var ID_251167257_f = Ti.Filesystem.getFile(ID_251167257_d.resolve(), 'cap' + nuevoid_f3.id + '.jpg');
            if (ID_251167257_f.exists() == true) ID_251167257_f.deleteFile();
            ID_251167257_f.write(evento.foto);
            ID_251167257_d = null;
            ID_251167257_f = null;
            var ID_1597933114_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
            if (ID_1597933114_d.exists() == false) ID_1597933114_d.createDirectory();
            var ID_1597933114_f = Ti.Filesystem.getFile(ID_1597933114_d.resolve(), 'cap' + nuevoid_f3.id + '.json');
            if (ID_1597933114_f.exists() == true) ID_1597933114_f.deleteFile();
            console.log('contenido f3 edano', JSON.stringify(json_datosfoto));
            ID_1597933114_f.write(JSON.stringify(json_datosfoto));
            ID_1597933114_d = null;
            ID_1597933114_f = null;
        } else {
            var ID_1881347677_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + 0);
            if (ID_1881347677_d.exists() == false) ID_1881347677_d.createDirectory();
            var ID_1881347677_f = Ti.Filesystem.getFile(ID_1881347677_d.resolve(), 'cap' + nuevoid_f3.id + '.jpg');
            if (ID_1881347677_f.exists() == true) ID_1881347677_f.deleteFile();
            ID_1881347677_f.write(evento.foto);
            ID_1881347677_d = null;
            ID_1881347677_f = null;
        }
        $.dano.set({
            foto3: 'cap' + nuevoid_f3.id + '.jpg'
        });
        if ('dano' in $) $dano = $.dano.toJSON();
        $.ID_1431154103.procesar({
            imagen: evento.foto,
            nueva: 640,
            calidad: 91,
            camara: 'trasera'
        });
        evento = null;
    }
}

(function() {
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
        /** 
         * Modificamos la idinspeccion con el idserver de la tarea 
         */
        $.dano.set({
            id_inspeccion: seltarea.id_server
        });
        if ('dano' in $) $dano = $.dano.toJSON();
    }
    /** 
     * Recuperamos la variable del id_recinto que tiene danos relacionados y actualizamos el modelo 
     */
    var temp_idrecinto = ('temp_idrecinto' in require('vars')) ? require('vars')['temp_idrecinto'] : '';
    if ((_.isObject(temp_idrecinto) || (_.isString(temp_idrecinto)) && !_.isEmpty(temp_idrecinto)) || _.isNumber(temp_idrecinto) || _.isBoolean(temp_idrecinto)) {
        $.dano.set({
            id_recinto: temp_idrecinto
        });
        if ('dano' in $) $dano = $.dano.toJSON();
    }
    /** 
     * heredamos datos previos de nivel en modelo nuevo con binding de esta pantalla 
     */
    var ID_1577952986_i = Alloy.createCollection('insp_itemdanos');
    var ID_1577952986_i_where = 'id=\'' + args._dato.id + '\'';
    ID_1577952986_i.fetch({
        query: 'SELECT * FROM insp_itemdanos WHERE id=\'' + args._dato.id + '\''
    });
    var insp_n = require('helper').query2array(ID_1577952986_i);
    /** 
     * Cargamos el modelo con los datos obtenidos desde la consulta 
     */
    $.dano.set({
        id_inspeccion: insp_n[0].id_inspeccion,
        nombre: insp_n[0].nombre,
        superficie: insp_n[0].superficie,
        foto1: insp_n[0].foto1,
        id_partida: insp_n[0].id_partida,
        foto2: insp_n[0].foto2,
        descripcion_dano: insp_n[0].descripcion_dano,
        foto3: insp_n[0].foto3,
        id_tipodano: insp_n[0].id_tipodano,
        id_unidadmedida: insp_n[0].id_unidadmedida
    });
    if ('dano' in $) $dano = $.dano.toJSON();
    $.ID_1390803671.setText(insp_n[0].nombre);

    $.ID_1390803671.setColor('#000000');

    /** 
     * Cargamos los textos en los campos de texto 
     */
    $.ID_144768712.setValue(insp_n[0].descripcion_dano);

    $.ID_1938962380.setValue(insp_n[0].superficie);

    if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
        /** 
         * Revisamos si existe variable seltarea para saber si las fotos son desde la inspeccion o son datos dummy 
         */
        var ID_801567210_trycatch = {
            error: function(e) {
                if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
            }
        };
        try {
            ID_801567210_trycatch.error = function(evento) {};
            /** 
             * Cambiamos el nombre del archivo para cargar miniaturas. Ya que trae por nombre &quot;cap001&quot; y lo dejamos como &quot;mini001&quot; y asi poder leer las miniaturas 
             */
            insp_n[0].foto1 = "mini" + insp_n[0].foto1.substring(3);
            var foto_1 = '';
            var ID_1995142177_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + seltarea.id_server);
            if (ID_1995142177_d.exists() == true) {
                var ID_1995142177_f = Ti.Filesystem.getFile(ID_1995142177_d.resolve(), insp_n[0].foto1);
                if (ID_1995142177_f.exists() == true) {
                    foto_1 = ID_1995142177_f.read();
                }
                ID_1995142177_f = null;
            }
            ID_1995142177_d = null;
        } catch (e) {
            ID_801567210_trycatch.error(e);
        }
        var ID_1474873156_trycatch = {
            error: function(e) {
                if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
            }
        };
        try {
            ID_1474873156_trycatch.error = function(evento) {};
            insp_n[0].foto2 = "mini" + insp_n[0].foto2.substring(3);
            var foto_2 = '';
            var ID_1026543236_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + seltarea.id_server);
            if (ID_1026543236_d.exists() == true) {
                var ID_1026543236_f = Ti.Filesystem.getFile(ID_1026543236_d.resolve(), insp_n[0].foto2);
                if (ID_1026543236_f.exists() == true) {
                    foto_2 = ID_1026543236_f.read();
                }
                ID_1026543236_f = null;
            }
            ID_1026543236_d = null;
        } catch (e) {
            ID_1474873156_trycatch.error(e);
        }
        var ID_1334390975_trycatch = {
            error: function(e) {
                if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
            }
        };
        try {
            ID_1334390975_trycatch.error = function(evento) {
                if (Ti.App.deployType != 'production') console.log('no carga la 3ra foto', {});
            };
            insp_n[0].foto3 = "mini" + insp_n[0].foto3.substring(3);
            var foto_3 = '';
            var ID_1869665282_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + seltarea.id_server);
            if (ID_1869665282_d.exists() == true) {
                var ID_1869665282_f = Ti.Filesystem.getFile(ID_1869665282_d.resolve(), insp_n[0].foto3);
                if (ID_1869665282_f.exists() == true) {
                    foto_3 = ID_1869665282_f.read();
                }
                ID_1869665282_f = null;
            }
            ID_1869665282_d = null;
            if (Ti.App.deployType != 'production') console.log('cargada la 3ra foto', {});
        } catch (e) {
            ID_1334390975_trycatch.error(e);
        }
    } else {
        if (Ti.App.deployType != 'production') console.log('las fotos NO estan en seltarea', {});
        var ID_1434209567_trycatch = {
            error: function(e) {
                if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
            }
        };
        try {
            ID_1434209567_trycatch.error = function(evento) {};
            insp_n[0].foto1 = "mini" + insp_n[0].foto1.substring(3);
            var foto_1 = '';
            var ID_1126856474_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + 0);
            if (ID_1126856474_d.exists() == true) {
                var ID_1126856474_f = Ti.Filesystem.getFile(ID_1126856474_d.resolve(), insp_n[0].foto1);
                if (ID_1126856474_f.exists() == true) {
                    foto_1 = ID_1126856474_f.read();
                }
                ID_1126856474_f = null;
            }
            ID_1126856474_d = null;
        } catch (e) {
            ID_1434209567_trycatch.error(e);
        }
        var ID_1235328145_trycatch = {
            error: function(e) {
                if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
            }
        };
        try {
            ID_1235328145_trycatch.error = function(evento) {};
            insp_n[0].foto2 = "mini" + insp_n[0].foto2.substring(3);
            var foto_2 = '';
            var ID_1752519057_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + 0);
            if (ID_1752519057_d.exists() == true) {
                var ID_1752519057_f = Ti.Filesystem.getFile(ID_1752519057_d.resolve(), insp_n[0].foto2);
                if (ID_1752519057_f.exists() == true) {
                    foto_2 = ID_1752519057_f.read();
                }
                ID_1752519057_f = null;
            }
            ID_1752519057_d = null;
        } catch (e) {
            ID_1235328145_trycatch.error(e);
        }
        var ID_1157833311_trycatch = {
            error: function(e) {
                if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
            }
        };
        try {
            ID_1157833311_trycatch.error = function(evento) {};
            insp_n[0].foto3 = "mini" + insp_n[0].foto3.substring(3);
            var foto_3 = '';
            var ID_1316576132_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + 0);
            if (ID_1316576132_d.exists() == true) {
                var ID_1316576132_f = Ti.Filesystem.getFile(ID_1316576132_d.resolve(), insp_n[0].foto3);
                if (ID_1316576132_f.exists() == true) {
                    foto_3 = ID_1316576132_f.read();
                }
                ID_1316576132_f = null;
            }
            ID_1316576132_d = null;
        } catch (e) {
            ID_1157833311_trycatch.error(e);
        }
    }
    if ((_.isObject(foto_1) || (_.isString(foto_1)) && !_.isEmpty(foto_1)) || _.isNumber(foto_1) || _.isBoolean(foto_1)) {
        /** 
         * Revisamos que foto1, foto2 y foto3 contengan texto, por lo que si es asi, modificamos la imagen previa y ponemos la foto miniatura obtenida desde la memoria 
         */
        var ID_1470272109_visible = true;

        if (ID_1470272109_visible == 'si') {
            ID_1470272109_visible = true;
        } else if (ID_1470272109_visible == 'no') {
            ID_1470272109_visible = false;
        }
        $.ID_1470272109.setVisible(ID_1470272109_visible);

        var ID_1737146733_imagen = foto_1;

        if (typeof ID_1737146733_imagen == 'string' && 'styles' in require('a4w') && ID_1737146733_imagen in require('a4w').styles['images']) {
            ID_1737146733_imagen = require('a4w').styles['images'][ID_1737146733_imagen];
        }
        $.ID_1737146733.setImage(ID_1737146733_imagen);

        if ((Ti.Platform.manufacturer) == 'samsung') {
            /** 
             * Revisamos si el equipo es samsung, si es asi, rotamos la imagen ya que es la unica marca que saca mal rotadas las imagenes 
             */
            var ID_1470272109_rotar = 90;

            var setRotacion = function(angulo) {
                $.ID_1470272109.setTransform(Ti.UI.create2DMatrix().rotate(angulo));
            };
            setRotacion(ID_1470272109_rotar);

        }
    }
    if ((_.isObject(foto_2) || (_.isString(foto_2)) && !_.isEmpty(foto_2)) || _.isNumber(foto_2) || _.isBoolean(foto_2)) {
        var ID_135662569_visible = true;

        if (ID_135662569_visible == 'si') {
            ID_135662569_visible = true;
        } else if (ID_135662569_visible == 'no') {
            ID_135662569_visible = false;
        }
        $.ID_135662569.setVisible(ID_135662569_visible);

        var ID_1054467024_imagen = foto_2;

        if (typeof ID_1054467024_imagen == 'string' && 'styles' in require('a4w') && ID_1054467024_imagen in require('a4w').styles['images']) {
            ID_1054467024_imagen = require('a4w').styles['images'][ID_1054467024_imagen];
        }
        $.ID_1054467024.setImage(ID_1054467024_imagen);

        if ((Ti.Platform.manufacturer) == 'samsung') {
            var ID_135662569_rotar = 90;

            var setRotacion = function(angulo) {
                $.ID_135662569.setTransform(Ti.UI.create2DMatrix().rotate(angulo));
            };
            setRotacion(ID_135662569_rotar);

        }
    }
    if ((_.isObject(foto_3) || (_.isString(foto_3)) && !_.isEmpty(foto_3)) || _.isNumber(foto_3) || _.isBoolean(foto_3)) {
        var ID_719771746_visible = true;

        if (ID_719771746_visible == 'si') {
            ID_719771746_visible = true;
        } else if (ID_719771746_visible == 'no') {
            ID_719771746_visible = false;
        }
        $.ID_719771746.setVisible(ID_719771746_visible);

        var ID_1227047425_imagen = foto_3;

        if (typeof ID_1227047425_imagen == 'string' && 'styles' in require('a4w') && ID_1227047425_imagen in require('a4w').styles['images']) {
            ID_1227047425_imagen = require('a4w').styles['images'][ID_1227047425_imagen];
        }
        $.ID_1227047425.setImage(ID_1227047425_imagen);

        if ((Ti.Platform.manufacturer) == 'samsung') {
            var ID_719771746_rotar = 90;

            var setRotacion = function(angulo) {
                $.ID_719771746.setTransform(Ti.UI.create2DMatrix().rotate(angulo));
            };
            setRotacion(ID_719771746_rotar);

        }
        if (Ti.App.deployType != 'production') console.log('mostrando la 3ra foto', {});
    }
    /** 
     * Limpiamos variables 
     */
    foto_1 = null;
    foto_2 = null;
    foto_3 = null;
    /** 
     * Desenfocamos todos los campos de texto en 0.2 segundos 
     */
    var ID_56702640_func = function() {
        $.ID_1938962380.blur();
        $.ID_144768712.blur();
    };
    var ID_56702640 = setTimeout(ID_56702640_func, 1000 * 0.2);
    /** 
     * Fijamos el scroll al inicio 
     */
    $.ID_1178522935.scrollToTop();
    /** 
     * Modificamos en 0.1 segundos el color del statusbar 
     */
    var ID_1295290924_func = function() {
        var ID_975866950_statusbar = '#7E6EE0';

        var setearStatusColor = function(ID_975866950_statusbar) {
            if (OS_IOS) {
                if (ID_975866950_statusbar == 'light' || ID_975866950_statusbar == 'claro') {
                    $.ID_975866950_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.LIGHT_CONTENT);
                } else if (ID_975866950_statusbar == 'grey' || ID_975866950_statusbar == 'gris' || ID_975866950_statusbar == 'gray') {
                    $.ID_975866950_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.GREY);
                } else if (ID_975866950_statusbar == 'oscuro' || ID_975866950_statusbar == 'dark') {
                    $.ID_975866950_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.DEFAULT);
                }
            } else if (OS_ANDROID) {
                abx.setStatusbarColor(ID_975866950_statusbar);
            }
        };
        setearStatusColor(ID_975866950_statusbar);

    };
    var ID_1295290924 = setTimeout(ID_1295290924_func, 1000 * 0.1);
    _my_events['resp_dato,ID_1998732988'] = function(evento) {
        if (Ti.App.deployType != 'production') console.log('detalle de la respuesta', {
            "datos": evento
        });
        $.ID_1390803671.setText(evento.nombre);

        $.ID_1390803671.setColor('#000000');

        /** 
         * Asumimos que el valor mostrado seleccionado es el nombre de la fila dano, ya que no existe el campo nombre en esta pantalla. 
         */
        $.dano.set({
            nombre: evento.nombre,
            id_partida: evento.id_partida,
            id_tipodano: '',
            id_unidadmedida: ''
        });
        if ('dano' in $) $dano = $.dano.toJSON();
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
            var ID_325613042_i = Alloy.createCollection('tipo_dano');
            var ID_325613042_i_where = 'pais_texto=\'' + pais[0].nombre + '\' AND id_partida=\'' + evento.id_server + '\'';
            ID_325613042_i.fetch({
                query: 'SELECT * FROM tipo_dano WHERE pais_texto=\'' + pais[0].nombre + '\' AND id_partida=\'' + evento.id_server + '\''
            });
            var danos = require('helper').query2array(ID_325613042_i);
            /** 
             * Obtenemos datos para selectores (solo los de este pais y este tipo de partida) 
             */
            var datos = [];
            _.each(danos, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'valor';
                    if (llave == 'id_segured') newkey = 'id_interno';
                    if (llave == 'id_server') newkey = 'id_gerardo';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                datos.push(new_row);
            });
            $.ID_1552750321.data({
                data: datos
            });
            if (Ti.App.deployType != 'production') console.log('termino de llamar', {});
        }
        /** 
         * Editamos y limpiamos los campos que estan siendo filtrados con este campo 
         */
        $.ID_1552750321.labels({
            seleccione: 'seleccione tipo'
        });
        $.ID_836121132.labels({
            seleccione: 'unidad'
        });
        $.ID_1938962380.setValue('');

        $.ID_859260223.setText('-');

    };
    Alloy.Events.on('resp_dato', _my_events['resp_dato,ID_1998732988']);
})();

function Postlayout_ID_1343833511(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var ID_1915411750_func = function() {
        /** 
         * Una vez ya cargada la tarea, dejamos la variable limpia para poder abrir otras pantallas 
         */
        require('vars')['var_abriendo'] = '';
    };
    var ID_1915411750 = setTimeout(ID_1915411750_func, 1000 * 0.2);

}
if (OS_IOS || OS_ANDROID) {
    $.ID_975866950.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}
//$.ID_975866950.open();