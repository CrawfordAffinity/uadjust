var _bind4section = {
    "ref1": "insp_itemdanos"
};
var _list_templates = {
    "elemento": {
        "ID_432412973": {},
        "ID_291370846": {
            "text": "{nombre}"
        },
        "ID_1925487537": {
            "text": "{id_server}"
        },
        "ID_975786586": {
            "text": "{id_segured}"
        },
        "ID_1546190443": {}
    },
    "dano": {
        "ID_1569764493": {
            "text": "{id}"
        },
        "ID_304218376": {},
        "ID_1912165224": {
            "text": "{nombre}"
        }
    },
    "pborrar": {
        "ID_911129691": {
            "text": "{id}"
        },
        "ID_123385123": {},
        "ID_1325519397": {
            "text": "{nombre}"
        },
        "ID_1575624962": {},
        "ID_1839254956": {},
        "ID_333012614": {}
    }
};
var $recinto2 = $.recinto2.toJSON();

var _activity;
if (OS_ANDROID) {
    _activity = $.ID_1713672648.activity;
    var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
    _out_vars = {},
    $item = {},
    args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
    $.item.set(args.__modelo);
    $item = $.item.toJSON();
}
var _var_scopekey = 'ID_1713672648';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
    $.ID_1713672648.addEventListener('open', function(e) {});
}
$.ID_1713672648.orientationModes = [Titanium.UI.PORTRAIT];


var ID_1733573059_like = function(search) {
    if (typeof search !== 'string' || this === null) {
        return false;
    }
    search = search.replace(new RegExp("([\\.\\\\\\+\\*\\?\\[\\^\\]\\$\\(\\)\\{\\}\\=\\!\\<\\>\\|\\:\\-])", "g"), "\\$1");
    search = search.replace(/%/g, '.*').replace(/_/g, '.');
    return RegExp('^' + search + '$', 'gi').test(this);
};
var ID_1733573059_filter = function(coll) {
    var filtered = coll.filter(function(m) {
        var _tests = [],
            _all_true = false,
            model = m.toJSON();
        _tests.push((model.id_recinto == '0'));
        var _all_true_s = _.uniq(_tests);
        _all_true = (_all_true_s.length == 1 && _all_true_s[0] == true) ? true : false;
        return _all_true;
    });
    filtered = _.toArray(filtered);
    return filtered;
};
var ID_1733573059_transform = function(model) {
    var modelo = model.toJSON();
    return modelo;
};
_.defer(function() {
    Alloy.Collections.insp_itemdanos.fetch();
});
Alloy.Collections.insp_itemdanos.on('add change delete', function(ee) {
    ID_1733573059_update(ee);
});
var ID_1733573059_update = function(evento) {
    var temp_idrecinto = ('temp_idrecinto' in require('vars')) ? require('vars')['temp_idrecinto'] : '';
    if ((_.isObject(temp_idrecinto) || (_.isString(temp_idrecinto)) && !_.isEmpty(temp_idrecinto)) || _.isNumber(temp_idrecinto) || _.isBoolean(temp_idrecinto)) {
        var ID_915441220_i = Alloy.createCollection('insp_itemdanos');
        var ID_915441220_i_where = 'id_recinto=\'' + temp_idrecinto + '\'';
        ID_915441220_i.fetch({
            query: 'SELECT * FROM insp_itemdanos WHERE id_recinto=\'' + temp_idrecinto + '\''
        });
        var insp_n = require('helper').query2array(ID_915441220_i);
        var alto = 55 * insp_n.length;
        var ID_1133111069_alto = alto;

        if (ID_1133111069_alto == '*') {
            ID_1133111069_alto = Ti.UI.FILL;
        } else if (!isNaN(ID_1133111069_alto)) {
            ID_1133111069_alto = ID_1133111069_alto + 'dp';
        }
        $.ID_1133111069.setHeight(ID_1133111069_alto);

    } else {
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        var ID_918658580_i = Alloy.createCollection('insp_itemdanos');
        var ID_918658580_i_where = 'id_inspeccion=\'' + seltarea.id_server + '\'';
        ID_918658580_i.fetch({
            query: 'SELECT * FROM insp_itemdanos WHERE id_inspeccion=\'' + seltarea.id_server + '\''
        });
        var insp_n = require('helper').query2array(ID_918658580_i);
        var alto = 50 * insp_n.length;
        var ID_1133111069_alto = alto;

        if (ID_1133111069_alto == '*') {
            ID_1133111069_alto = Ti.UI.FILL;
        } else if (!isNaN(ID_1133111069_alto)) {
            ID_1133111069_alto = ID_1133111069_alto + 'dp';
        }
        $.ID_1133111069.setHeight(ID_1133111069_alto);

    }
};
Alloy.Collections.insp_itemdanos.fetch();


$.ID_21378405.init({
    titulo: 'EDITAR RECINTO',
    onsalirinsp: Salirinsp_ID_777188340,
    __id: 'ALL21378405',
    textoderecha: 'Guardar',
    fondo: 'fondomorado',
    top: 0,
    modal: '',
    onpresiono: Presiono_ID_895566448,
    colortextoderecha: 'blanco'
});

function Salirinsp_ID_777188340(e) {

    var evento = e;
    /** 
     * Limpiamos widgets multiples (memoria) 
     */
    $.ID_321122430.limpiar({});
    $.ID_1975428302.limpiar({});
    $.ID_399652068.limpiar({});
    $.ID_1990255213.limpiar({});
    $.ID_1536081210.limpiar({});
    $.ID_363463535.limpiar({});
    $.ID_1713672648.close();

}

function Presiono_ID_895566448(e) {

    var evento = e;
    var temp_idrecinto = ('temp_idrecinto' in require('vars')) ? require('vars')['temp_idrecinto'] : '';
    /** 
     * Consultamos los danos que tenga ese recinto y desplegamos mensaje para advertir que esta ingresando un recinto sin danos 
     */
    var ID_1357411979_i = Alloy.createCollection('insp_itemdanos');
    var ID_1357411979_i_where = 'id_recinto=\'' + temp_idrecinto + '\'';
    ID_1357411979_i.fetch({
        query: 'SELECT * FROM insp_itemdanos WHERE id_recinto=\'' + temp_idrecinto + '\''
    });
    var danos_verificacion = require('helper').query2array(ID_1357411979_i);
    if (_.isUndefined($recinto2.nombre)) {
        /** 
         * Verificamos que los campos obligatorios esten bien 
         */
        var ID_4814022_opts = ['Aceptar'];
        var ID_4814022 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese el nombre del recinto',
            buttonNames: ID_4814022_opts
        });
        ID_4814022.addEventListener('click', function(e) {
            var nulo = ID_4814022_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_4814022.show();
    } else if ((_.isObject($recinto2.nombre) || _.isString($recinto2.nombre)) && _.isEmpty($recinto2.nombre)) {
        var ID_1675774209_opts = ['Aceptar'];
        var ID_1675774209 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese el nombre del recinto',
            buttonNames: ID_1675774209_opts
        });
        ID_1675774209.addEventListener('click', function(e) {
            var nulo = ID_1675774209_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1675774209.show();
    } else if (_.isUndefined($recinto2.id_nivel)) {
        var ID_743930263_opts = ['Aceptar'];
        var ID_743930263 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Seleccione la zona al cual pertenece el nuevo recinto',
            buttonNames: ID_743930263_opts
        });
        ID_743930263.addEventListener('click', function(e) {
            var nulo = ID_743930263_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_743930263.show();
    } else if (_.isUndefined($recinto2.largo)) {
        var ID_1254937641_opts = ['Aceptar'];
        var ID_1254937641 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese largo del recinto',
            buttonNames: ID_1254937641_opts
        });
        ID_1254937641.addEventListener('click', function(e) {
            var nulo = ID_1254937641_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1254937641.show();
    } else if ((_.isObject($recinto2.largo) || _.isString($recinto2.largo)) && _.isEmpty($recinto2.largo)) {
        var ID_1831403046_opts = ['Aceptar'];
        var ID_1831403046 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese largo del recinto',
            buttonNames: ID_1831403046_opts
        });
        ID_1831403046.addEventListener('click', function(e) {
            var nulo = ID_1831403046_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1831403046.show();
    } else if (_.isUndefined($recinto2.ancho)) {
        var ID_1574866465_opts = ['Aceptar'];
        var ID_1574866465 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese ancho del recinto',
            buttonNames: ID_1574866465_opts
        });
        ID_1574866465.addEventListener('click', function(e) {
            var nulo = ID_1574866465_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1574866465.show();
    } else if ((_.isObject($recinto2.ancho) || _.isString($recinto2.ancho)) && _.isEmpty($recinto2.ancho)) {
        var ID_286891949_opts = ['Aceptar'];
        var ID_286891949 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese ancho del recinto',
            buttonNames: ID_286891949_opts
        });
        ID_286891949.addEventListener('click', function(e) {
            var nulo = ID_286891949_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_286891949.show();
    } else if (_.isUndefined($recinto2.alto)) {
        var ID_278705220_opts = ['Aceptar'];
        var ID_278705220 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese la altura del recinto',
            buttonNames: ID_278705220_opts
        });
        ID_278705220.addEventListener('click', function(e) {
            var nulo = ID_278705220_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_278705220.show();
    } else if ((_.isObject($recinto2.alto) || _.isString($recinto2.alto)) && _.isEmpty($recinto2.alto)) {
        var ID_924458486_opts = ['Aceptar'];
        var ID_924458486 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Ingrese la altura del recinto',
            buttonNames: ID_924458486_opts
        });
        ID_924458486.addEventListener('click', function(e) {
            var nulo = ID_924458486_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_924458486.show();
    } else if (_.isUndefined($recinto2.ids_estructuras_soportantes)) {
        var ID_534010194_opts = ['Aceptar'];
        var ID_534010194 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Seleccione la estructura soportante del recinto',
            buttonNames: ID_534010194_opts
        });
        ID_534010194.addEventListener('click', function(e) {
            var nulo = ID_534010194_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_534010194.show();
    } else if ((_.isObject($recinto2.ids_estructuras_soportantes) || _.isString($recinto2.ids_estructuras_soportantes)) && _.isEmpty($recinto2.ids_estructuras_soportantes)) {
        var ID_1098070893_opts = ['Aceptar'];
        var ID_1098070893 = Ti.UI.createAlertDialog({
            title: 'Atención',
            message: 'Seleccione la estructura soportante del recinto',
            buttonNames: ID_1098070893_opts
        });
        ID_1098070893.addEventListener('click', function(e) {
            var nulo = ID_1098070893_opts[e.index];
            nulo = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_1098070893.show();
    } else if (danos_verificacion && danos_verificacion.length == 0) {
        var ID_287335839_opts = ['GUARDAR', 'VOLVER'];
        var ID_287335839 = Ti.UI.createAlertDialog({
            title: '¿Guardar sin daños?',
            message: 'Estas guardando el recinto sin daños asociados, si esta correcto presiona guardar',
            buttonNames: ID_287335839_opts
        });
        ID_287335839.addEventListener('click', function(e) {
            var msj = ID_287335839_opts[e.index];
            if (msj == 'GUARDAR') {
                var ID_1003108636_i = Alloy.Collections.insp_recintos;
                var sql = 'DELETE FROM ' + ID_1003108636_i.config.adapter.collection_name + ' WHERE id=\'' + args._dato.id + '\'';
                var db = Ti.Database.open(ID_1003108636_i.config.adapter.db_name);
                db.execute(sql);
                db.close();
                sql = null;
                db = null;
                ID_1003108636_i.trigger('delete');
                Alloy.Collections[$.recinto2.config.adapter.collection_name].add($.recinto2);
                $.recinto2.save();
                Alloy.Collections[$.recinto2.config.adapter.collection_name].fetch();
                test = null;
                $.ID_321122430.limpiar({});
                $.ID_1975428302.limpiar({});
                $.ID_399652068.limpiar({});
                $.ID_1990255213.limpiar({});
                $.ID_1536081210.limpiar({});
                $.ID_363463535.limpiar({});
                $.ID_1713672648.close();
            }
            msj = null;
            e.source.removeEventListener("click", arguments.callee);
        });
        ID_287335839.show();
    } else {
        /** 
         * Eliminamos el recinto que estamos editando... Es para evitar duplicaciones de recintos 
         */
        /** 
         * Eliminamos el recinto que estamos editando... Es para evitar duplicaciones de recintos 
         */
        var ID_1169537632_i = Alloy.Collections.insp_recintos;
        var sql = 'DELETE FROM ' + ID_1169537632_i.config.adapter.collection_name + ' WHERE id=\'' + args._dato.id + '\'';
        var db = Ti.Database.open(ID_1169537632_i.config.adapter.db_name);
        db.execute(sql);
        db.close();
        sql = null;
        db = null;
        ID_1169537632_i.trigger('delete');
        /** 
         * Guardamos recinto editado 
         */
        Alloy.Collections[$.recinto2.config.adapter.collection_name].add($.recinto2);
        $.recinto2.save();
        Alloy.Collections[$.recinto2.config.adapter.collection_name].fetch();
        /** 
         * Limpiamos widgets multiples (memoria) 
         */
        $.ID_321122430.limpiar({});
        $.ID_1975428302.limpiar({});
        $.ID_399652068.limpiar({});
        $.ID_1990255213.limpiar({});
        $.ID_1536081210.limpiar({});
        $.ID_363463535.limpiar({});
        $.ID_1713672648.close();
    }
}

function Change_ID_1896774805(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    /** 
     * Al escribir la descripcion, vamos actualizando en la tabla de recintos el nombre 
     */
    $.recinto2.set({
        nombre: elemento
    });
    if ('recinto2' in $) $recinto2 = $.recinto2.toJSON();
    require('vars')['nombrerecinto'] = elemento;
    elemento = null, source = null;

}

function Return_ID_1921526017(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    $.ID_1077768092.blur();
    elemento = null, source = null;

}

$.ID_1379917834.init({
    titulo: 'Zona',
    cargando: 'cargando...',
    __id: 'ALL1379917834',
    left: 0,
    onrespuesta: Respuesta_ID_1827220738,
    campo: 'Zona del Recinto',
    onabrir: Abrir_ID_593866641,
    color: 'morado',
    right: 0,
    seleccione: 'seleccione zona',
    activo: true,
    onafterinit: Afterinit_ID_795893003
});

function Abrir_ID_593866641(e) {

    var evento = e;

}

function Respuesta_ID_1827220738(e) {

    var evento = e;
    $.ID_1379917834.labels({
        valor: evento.valor
    });
    $.recinto2.set({
        id_nivel: evento.item.id_interno
    });
    if ('recinto2' in $) $recinto2 = $.recinto2.toJSON();

}

function Afterinit_ID_795893003(e) {

    var evento = e;
    var ID_1130215393_func = function() {
        /** 
         * obtenemos valor seleccionado previamente y lo mostramos en selector con desface para que pueda mostrar bien el valor en pantalla, si no, carga un valor erroneo 
         */
        var ID_1476542468_i = Alloy.createCollection('insp_recintos');
        var ID_1476542468_i_where = 'id=\'' + args._dato.id + '\'';
        ID_1476542468_i.fetch({
            query: 'SELECT * FROM insp_recintos WHERE id=\'' + args._dato.id + '\''
        });
        var insp_datos = require('helper').query2array(ID_1476542468_i);
        if (Ti.App.deployType != 'production') console.log('detalle del recinto a editar', {
            "datos": insp_datos
        });
        if (insp_datos && insp_datos.length) {
            /** 
             * obtenemos valor seleccionado previamente y lo mostramos en selector. 
             */
            var ID_791661424_i = Alloy.createCollection('insp_niveles');
            var ID_791661424_i_where = 'id=\'' + insp_datos[0].id_nivel + '\'';
            ID_791661424_i.fetch({
                query: 'SELECT * FROM insp_niveles WHERE id=\'' + insp_datos[0].id_nivel + '\''
            });
            var insp_niveles = require('helper').query2array(ID_791661424_i);
            if (insp_niveles && insp_niveles.length) {
                var ID_52538855_func = function() {
                    $.ID_1379917834.labels({
                        valor: insp_niveles[0].nombre
                    });
                    if (Ti.App.deployType != 'production') console.log('el nombre del recinto es', {
                        "datos": insp_niveles[0].nombre
                    });
                };
                var ID_52538855 = setTimeout(ID_52538855_func, 1000 * 0.21);
            }
        }
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            /** 
             * Cargamos el widget con los niveles del recinto 
             */
            var ID_1333606337_i = Alloy.createCollection('insp_niveles');
            var ID_1333606337_i_where = 'id_inspeccion=\'' + seltarea.id_server + '\'';
            ID_1333606337_i.fetch({
                query: 'SELECT * FROM insp_niveles WHERE id_inspeccion=\'' + seltarea.id_server + '\''
            });
            var datosniveles = require('helper').query2array(ID_1333606337_i);
            /** 
             * Obtenemos datos para selectores (solo los de esta inspeccion) 
             */
            var datos = [];
            _.each(datosniveles, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'valor';
                    if (llave == 'id') newkey = 'id_interno';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                datos.push(new_row);
            });
        } else {
            /** 
             * Insertamos dummies 
             */
            /** 
             * Insertamos dummies 
             */
            var ID_1826787656_i = Alloy.Collections.insp_niveles;
            var sql = "DELETE FROM " + ID_1826787656_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_1826787656_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_1826787656_i.trigger('delete');
            var item_index = 0;
            _.each('1,2,3,4,5'.split(','), function(item, item_pos, item_list) {
                item_index += 1;
                var ID_43694979_m = Alloy.Collections.insp_niveles;
                var ID_43694979_fila = Alloy.createModel('insp_niveles', {
                    id_inspeccion: -1,
                    nombre: 'Nivel' + item,
                    piso: item
                });
                ID_43694979_m.add(ID_43694979_fila);
                ID_43694979_fila.save();
                _.defer(function() {});
            });
            /** 
             * Obtenemos datos para selectores (todos) 
             */
            var ID_985447822_i = Alloy.createCollection('insp_niveles');
            ID_985447822_i.fetch();
            var ID_985447822_src = require('helper').query2array(ID_985447822_i);
            var datos = [];
            _.each(ID_985447822_src, function(fila, pos) {
                var new_row = {};
                _.each(fila, function(x, llave) {
                    var newkey = '';
                    if (llave == 'nombre') newkey = 'valor';
                    if (llave == 'id') newkey = 'id_interno';
                    if (newkey != '') new_row[newkey] = fila[llave];
                });
                datos.push(new_row);
            });
        }
        /** 
         * Cargamos el widget con los niveles del recinto 
         */
        $.ID_1379917834.data({
            data: datos
        });
    };
    var ID_1130215393 = setTimeout(ID_1130215393_func, 1000 * 0.2);

}

$.ID_1650576465.init({
    caja: 45,
    __id: 'ALL1650576465',
    onlisto: Listo_ID_1181097315,
    left: 0,
    top: 0,
    onclick: Click_ID_1941304860
});

function Click_ID_1941304860(e) {

    var evento = e;
    /** 
     * Definimos que estamos capturando foto en el primer item 
     */
    require('vars')[_var_scopekey]['cual_foto'] = '1';
    /** 
     * Abrimos camara 
     */
    $.ID_824520076.disparar({});
    /** 
     * Detenemos las animaciones de los widget 
     */
    $.ID_1650576465.detener({});
    $.ID_245423636.detener({});
    $.ID_277122098.detener({});

}

function Listo_ID_1181097315(e) {

    var evento = e;
    /** 
     * Recuperamos variables para poder definir la estructura de las carpetas donde se guardara la miniatura 
     */
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    var nuevoid_f1 = ('nuevoid_f1' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['nuevoid_f1'] : '';
    if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
        var ID_153663287_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + seltarea.id_server);
        if (ID_153663287_d.exists() == false) ID_153663287_d.createDirectory();
        var ID_153663287_f = Ti.Filesystem.getFile(ID_153663287_d.resolve(), 'mini' + nuevoid_f1.id + '.jpg');
        if (ID_153663287_f.exists() == true) ID_153663287_f.deleteFile();
        ID_153663287_f.write(evento.mini);
        ID_153663287_d = null;
        ID_153663287_f = null;
    } else {
        var ID_327315158_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + 0);
        if (ID_327315158_d.exists() == false) ID_327315158_d.createDirectory();
        var ID_327315158_f = Ti.Filesystem.getFile(ID_327315158_d.resolve(), 'mini' + nuevoid_f1.id + '.jpg');
        if (ID_327315158_f.exists() == true) ID_327315158_f.deleteFile();
        ID_327315158_f.write(evento.mini);
        ID_327315158_d = null;
        ID_327315158_f = null;
    }
}

function Click_ID_1173836768(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var evento = e;
    /** 
     * Definimos que estamos capturando foto en el primer item 
     */
    require('vars')[_var_scopekey]['cual_foto'] = '1';
    /** 
     * Ocultamos la vista para dejar visible al widget 
     */
    var ID_949530997_visible = false;

    if (ID_949530997_visible == 'si') {
        ID_949530997_visible = true;
    } else if (ID_949530997_visible == 'no') {
        ID_949530997_visible = false;
    }
    $.ID_949530997.setVisible(ID_949530997_visible);

    /** 
     * Detenemos las animaciones de los widget 
     */
    $.ID_1650576465.detener({});
    $.ID_245423636.detener({});
    $.ID_277122098.detener({});
    /** 
     * Abrimos camara 
     */
    $.ID_824520076.disparar({});

}

$.ID_245423636.init({
    caja: 45,
    __id: 'ALL245423636',
    onlisto: Listo_ID_1794916674,
    left: 5,
    top: 0,
    onclick: Click_ID_1879898037
});

function Click_ID_1879898037(e) {

    var evento = e;
    require('vars')[_var_scopekey]['cual_foto'] = 2;
    $.ID_824520076.disparar({});
    $.ID_245423636.detener({});
    $.ID_1650576465.detener({});
    $.ID_277122098.detener({});

}

function Listo_ID_1794916674(e) {

    var evento = e;
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    var nuevoid_f2 = ('nuevoid_f2' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['nuevoid_f2'] : '';
    if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
        var ID_462637464_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + seltarea.id_server);
        if (ID_462637464_d.exists() == false) ID_462637464_d.createDirectory();
        var ID_462637464_f = Ti.Filesystem.getFile(ID_462637464_d.resolve(), 'mini' + nuevoid_f2.id + '.jpg');
        if (ID_462637464_f.exists() == true) ID_462637464_f.deleteFile();
        ID_462637464_f.write(evento.mini);
        ID_462637464_d = null;
        ID_462637464_f = null;
    } else {
        var ID_1888580742_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + 0);
        if (ID_1888580742_d.exists() == false) ID_1888580742_d.createDirectory();
        var ID_1888580742_f = Ti.Filesystem.getFile(ID_1888580742_d.resolve(), 'mini' + nuevoid_f2.id + '.jpg');
        if (ID_1888580742_f.exists() == true) ID_1888580742_f.deleteFile();
        ID_1888580742_f.write(evento.mini);
        ID_1888580742_d = null;
        ID_1888580742_f = null;
    }
}

function Click_ID_1644956819(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var evento = e;
    require('vars')[_var_scopekey]['cual_foto'] = 2;
    var ID_66332676_visible = false;

    if (ID_66332676_visible == 'si') {
        ID_66332676_visible = true;
    } else if (ID_66332676_visible == 'no') {
        ID_66332676_visible = false;
    }
    $.ID_66332676.setVisible(ID_66332676_visible);

    $.ID_1650576465.detener({});
    $.ID_245423636.detener({});
    $.ID_277122098.detener({});
    $.ID_824520076.disparar({});

}

$.ID_277122098.init({
    caja: 45,
    __id: 'ALL277122098',
    onlisto: Listo_ID_1713850383,
    left: 5,
    top: 0,
    onclick: Click_ID_737625943
});

function Click_ID_737625943(e) {

    var evento = e;
    require('vars')[_var_scopekey]['cual_foto'] = 3;
    $.ID_824520076.disparar({});
    $.ID_277122098.detener({});
    $.ID_1650576465.detener({});
    $.ID_245423636.detener({});

}

function Listo_ID_1713850383(e) {

    var evento = e;
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    var nuevoid_f3 = ('nuevoid_f3' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['nuevoid_f3'] : '';
    if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
        var ID_1959180332_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + seltarea.id_server);
        if (ID_1959180332_d.exists() == false) ID_1959180332_d.createDirectory();
        var ID_1959180332_f = Ti.Filesystem.getFile(ID_1959180332_d.resolve(), 'mini' + nuevoid_f3.id + '.jpg');
        if (ID_1959180332_f.exists() == true) ID_1959180332_f.deleteFile();
        ID_1959180332_f.write(evento.mini);
        ID_1959180332_d = null;
        ID_1959180332_f = null;
    } else {
        var ID_946227262_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + 0);
        if (ID_946227262_d.exists() == false) ID_946227262_d.createDirectory();
        var ID_946227262_f = Ti.Filesystem.getFile(ID_946227262_d.resolve(), 'mini' + nuevoid_f3.id + '.jpg');
        if (ID_946227262_f.exists() == true) ID_946227262_f.deleteFile();
        ID_946227262_f.write(evento.mini);
        ID_946227262_d = null;
        ID_946227262_f = null;
    }
}

function Click_ID_368058326(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var evento = e;
    require('vars')[_var_scopekey]['cual_foto'] = 3;
    var ID_738207149_visible = false;

    if (ID_738207149_visible == 'si') {
        ID_738207149_visible = true;
    } else if (ID_738207149_visible == 'no') {
        ID_738207149_visible = false;
    }
    $.ID_738207149.setVisible(ID_738207149_visible);

    $.ID_1650576465.detener({});
    $.ID_245423636.detener({});
    $.ID_277122098.detener({});
    $.ID_824520076.disparar({});

}

function Change_ID_1514893135(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    /** 
     * Obtenemos el valor escrito en el campo de ancho 
     */
    var ancho;
    ancho = $.ID_1051579768.getValue();

    if ((_.isObject(ancho) || (_.isString(ancho)) && !_.isEmpty(ancho)) || _.isNumber(ancho) || _.isBoolean(ancho)) {
        /** 
         * Calculamos la superficie 
         */
        if ((_.isObject(elemento) || (_.isString(elemento)) && !_.isEmpty(elemento)) || _.isNumber(elemento) || _.isBoolean(elemento)) {
            /** 
             * nos aseguramos que ancho y valor de campo sean decimales con puntos (flotante), y no comas (espa&#241;ol) 
             */
            var nuevo = parseFloat(ancho.split(',').join('.')) * parseFloat(elemento.split(',').join('.'));
            $.recinto2.set({
                superficie: nuevo
            });
            if ('recinto2' in $) $recinto2 = $.recinto2.toJSON();
        }
    }
    /** 
     * Actualizamos el valor del largo del recinto 
     */
    $.recinto2.set({
        largo: elemento
    });
    if ('recinto2' in $) $recinto2 = $.recinto2.toJSON();
    elemento = null, source = null;

}

function Change_ID_473892946(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    var largo;
    largo = $.ID_1253568597.getValue();

    if ((_.isObject(largo) || (_.isString(largo)) && !_.isEmpty(largo)) || _.isNumber(largo) || _.isBoolean(largo)) {
        if ((_.isObject(elemento) || (_.isString(elemento)) && !_.isEmpty(elemento)) || _.isNumber(elemento) || _.isBoolean(elemento)) {
            /** 
             * nos aseguramos que largo y valor de campo sean decimales con puntos (flotante), y no comas (espa&#241;ol) 
             */
            var nuevo = parseFloat(largo.split(',').join('.')) * parseFloat(elemento.split(',').join('.'));
            $.recinto2.set({
                superficie: nuevo
            });
            if ('recinto2' in $) $recinto2 = $.recinto2.toJSON();
        }
    }
    $.recinto2.set({
        ancho: elemento
    });
    if ('recinto2' in $) $recinto2 = $.recinto2.toJSON();
    elemento = null, source = null;

}

function Change_ID_740070738(e) {

    e.cancelBubble = true;
    var elemento = e.value;
    var source = e.source;
    $.recinto2.set({
        alto: elemento
    });
    if ('recinto2' in $) $recinto2 = $.recinto2.toJSON();
    elemento = null, source = null;

}

$.ID_321122430.init({
    titulo: 'Estructura Soportante',
    cargando: 'cargando...',
    __id: 'ALL321122430',
    oncerrar: Cerrar_ID_866581192,
    left: 0,
    hint: 'Seleccione estructura',
    color: 'morado',
    subtitulo: 'Indique los tipos',
    right: 0,
    onclick: Click_ID_547046452,
    onafterinit: Afterinit_ID_219389529
});

function Click_ID_547046452(e) {

    var evento = e;

}

function Cerrar_ID_866581192(e) {

    var evento = e;
    $.ID_321122430.update({});
    $.recinto2.set({
        ids_estructuras_soportantes: evento.valores
    });
    if ('recinto2' in $) $recinto2 = $.recinto2.toJSON();

}

function Afterinit_ID_219389529(e) {

    var evento = e;
    /** 
     * ejecutamos desfasado para no congelar el hilo. 
     */
    var ID_431750065_func = function() {
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
            var ID_1714629853_i = Alloy.createCollection('insp_recintos');
            var ID_1714629853_i_where = 'id=\'' + args._dato.id + '\'';
            ID_1714629853_i.fetch({
                query: 'SELECT * FROM insp_recintos WHERE id=\'' + args._dato.id + '\''
            });
            var insp_datos = require('helper').query2array(ID_1714629853_i);
            if (insp_datos && insp_datos.length) {
                var ID_703063462_i = Alloy.createCollection('estructura_soportante');
                var ID_703063462_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
                ID_703063462_i.fetch({
                    query: 'SELECT * FROM estructura_soportante WHERE pais_texto=\'' + pais[0].nombre + '\''
                });
                var estructura = require('helper').query2array(ID_703063462_i);
                var padre_index = 0;
                _.each(estructura, function(padre, padre_pos, padre_list) {
                    padre_index += 1;
                    if (_.isNull(insp_datos[0].ids_estructuras_soportantes)) {
                        /** 
                         * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
                         */
                        padre._estado = 0;
                    } else if (insp_datos[0].ids_estructuras_soportantes.indexOf(padre.id_segured) != -1) {
                        padre._estado = 1;
                    } else {
                        padre._estado = 0;
                    }
                });
                var datos = [];
                _.each(estructura, function(fila, pos) {
                    var new_row = {};
                    _.each(fila, function(x, llave) {
                        var newkey = '';
                        if (llave == 'nombre') newkey = 'label';
                        if (llave == 'id_segured') newkey = 'valor';
                        if (llave == '_estado') newkey = 'estado';
                        if (newkey != '') new_row[newkey] = fila[llave];
                    });
                    datos.push(new_row);
                });
            } else {
                var ID_652686606_i = Alloy.createCollection('estructura_soportante');
                ID_652686606_i.fetch();
                var ID_652686606_src = require('helper').query2array(ID_652686606_i);
                var datos = [];
                _.each(ID_652686606_src, function(fila, pos) {
                    var new_row = {};
                    _.each(fila, function(x, llave) {
                        var newkey = '';
                        if (llave == 'nombre') newkey = 'label';
                        if (llave == 'id_segured') newkey = 'valor';
                        if (newkey != '') new_row[newkey] = fila[llave];
                    });
                    new_row['estado'] = 0;
                    datos.push(new_row);
                });
            }
            $.ID_321122430.update({
                data: datos
            });
        } else {
            /** 
             * cargamos datos dummy para cuando compilamos de forma individual. 
             */
            var ID_790603216_i = Alloy.Collections.estructura_soportante;
            var sql = "DELETE FROM " + ID_790603216_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_790603216_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_790603216_i.trigger('delete');
            var item_index = 0;
            _.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
                item_index += 1;
                var ID_1410801258_m = Alloy.Collections.estructura_soportante;
                var ID_1410801258_fila = Alloy.createModel('estructura_soportante', {
                    nombre: 'Estructura' + item,
                    id_server: '0' + item,
                    id_segured: '10' + item,
                    pais: 1
                });
                ID_1410801258_m.add(ID_1410801258_fila);
                ID_1410801258_fila.save();
                _.defer(function() {});
            });
            var ID_287328287_i = Alloy.createCollection('insp_recintos');
            var ID_287328287_i_where = 'id=\'' + args._dato.id + '\'';
            ID_287328287_i.fetch({
                query: 'SELECT * FROM insp_recintos WHERE id=\'' + args._dato.id + '\''
            });
            var insp_datos = require('helper').query2array(ID_287328287_i);
            if (Ti.App.deployType != 'production') console.log('contenido datos de todo pio', {
                "insp": insp_datos,
                "cont": datos,
                "args": args
            });
            if (insp_datos && insp_datos.length) {
                var ID_364675137_i = Alloy.createCollection('estructura_soportante');
                var ID_364675137_i_where = '';
                ID_364675137_i.fetch();
                var estructura = require('helper').query2array(ID_364675137_i);
                var padre_index = 0;
                _.each(estructura, function(padre, padre_pos, padre_list) {
                    padre_index += 1;
                    if (_.isNull(insp_datos[0].ids_estructuras_soportantes)) {
                        /** 
                         * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
                         */
                        padre._estado = 0;
                    } else if (insp_datos[0].ids_estructuras_soportantes.indexOf(padre.id_segured) != -1) {
                        padre._estado = 1;
                    } else {
                        padre._estado = 0;
                    }
                });
                var datos = [];
                _.each(estructura, function(fila, pos) {
                    var new_row = {};
                    _.each(fila, function(x, llave) {
                        var newkey = '';
                        if (llave == 'nombre') newkey = 'label';
                        if (llave == 'id_segured') newkey = 'valor';
                        if (llave == '_estado') newkey = 'estado';
                        if (newkey != '') new_row[newkey] = fila[llave];
                    });
                    datos.push(new_row);
                });
            } else {
                var ID_1188230604_i = Alloy.createCollection('estructura_soportante');
                ID_1188230604_i.fetch();
                var ID_1188230604_src = require('helper').query2array(ID_1188230604_i);
                var datos = [];
                _.each(ID_1188230604_src, function(fila, pos) {
                    var new_row = {};
                    _.each(fila, function(x, llave) {
                        var newkey = '';
                        if (llave == 'nombre') newkey = 'label';
                        if (llave == 'id_segured') newkey = 'valor';
                        if (newkey != '') new_row[newkey] = fila[llave];
                    });
                    new_row['estado'] = 0;
                    datos.push(new_row);
                });
            }
            $.ID_321122430.update({
                data: datos
            });
        }
    };
    var ID_431750065 = setTimeout(ID_431750065_func, 1000 * 0.2);

}

$.ID_1975428302.init({
    titulo: 'Muros / Tabiques',
    cargando: 'cargando...',
    __id: 'ALL1975428302',
    oncerrar: Cerrar_ID_430468783,
    left: 0,
    hint: 'Seleccione muros',
    color: 'morado',
    subtitulo: 'Indique los tipos',
    right: 0,
    onclick: Click_ID_38762254,
    onafterinit: Afterinit_ID_936952663
});

function Click_ID_38762254(e) {

    var evento = e;

}

function Cerrar_ID_430468783(e) {

    var evento = e;
    $.ID_1975428302.update({});
    $.recinto2.set({
        ids_muros: evento.valores
    });
    if ('recinto2' in $) $recinto2 = $.recinto2.toJSON();

}

function Afterinit_ID_936952663(e) {

    var evento = e;
    /** 
     * ejecutamos desfasado para no congelar el hilo. 
     */
    var ID_7512644_func = function() {
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
            var ID_1227058643_i = Alloy.createCollection('insp_recintos');
            var ID_1227058643_i_where = 'id=\'' + args._dato.id + '\'';
            ID_1227058643_i.fetch({
                query: 'SELECT * FROM insp_recintos WHERE id=\'' + args._dato.id + '\''
            });
            var insp_datos = require('helper').query2array(ID_1227058643_i);
            if (insp_datos && insp_datos.length) {
                var ID_434057837_i = Alloy.createCollection('muros_tabiques');
                var ID_434057837_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
                ID_434057837_i.fetch({
                    query: 'SELECT * FROM muros_tabiques WHERE pais_texto=\'' + pais[0].nombre + '\''
                });
                var muros_tabiques = require('helper').query2array(ID_434057837_i);
                var padre_index = 0;
                _.each(muros_tabiques, function(padre, padre_pos, padre_list) {
                    padre_index += 1;
                    if (_.isNull(insp_datos[0].ids_muros)) {
                        /** 
                         * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
                         */
                        padre._estado = 0;
                    } else if (insp_datos[0].ids_muros.indexOf(padre.id_segured) != -1) {
                        padre._estado = 1;
                    } else {
                        padre._estado = 0;
                    }
                });
                var datos = [];
                _.each(muros_tabiques, function(fila, pos) {
                    var new_row = {};
                    _.each(fila, function(x, llave) {
                        var newkey = '';
                        if (llave == 'nombre') newkey = 'label';
                        if (llave == 'id_segured') newkey = 'valor';
                        if (llave == '_estado') newkey = 'estado';
                        if (newkey != '') new_row[newkey] = fila[llave];
                    });
                    datos.push(new_row);
                });
            } else {
                var ID_1485715063_i = Alloy.createCollection('muros_tabiques');
                ID_1485715063_i.fetch();
                var ID_1485715063_src = require('helper').query2array(ID_1485715063_i);
                var datos = [];
                _.each(ID_1485715063_src, function(fila, pos) {
                    var new_row = {};
                    _.each(fila, function(x, llave) {
                        var newkey = '';
                        if (llave == 'nombre') newkey = 'label';
                        if (llave == 'id_segured') newkey = 'valor';
                        if (newkey != '') new_row[newkey] = fila[llave];
                    });
                    new_row['estado'] = 0;
                    datos.push(new_row);
                });
            }
            $.ID_1975428302.update({
                data: datos
            });
        } else {
            /** 
             * cargamos datos dummy para cuando compilamos de forma individual. 
             */
            var ID_183248906_i = Alloy.Collections.muros_tabiques;
            var sql = "DELETE FROM " + ID_183248906_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_183248906_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_183248906_i.trigger('delete');
            var item_index = 0;
            _.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
                item_index += 1;
                var ID_1728051457_m = Alloy.Collections.muros_tabiques;
                var ID_1728051457_fila = Alloy.createModel('muros_tabiques', {
                    nombre: 'muros_tabiques ' + item,
                    id_server: '0' + item,
                    id_segured: '10' + item,
                    pais: 1
                });
                ID_1728051457_m.add(ID_1728051457_fila);
                ID_1728051457_fila.save();
                _.defer(function() {});
            });
            var ID_1251981848_i = Alloy.createCollection('insp_recintos');
            var ID_1251981848_i_where = 'id=\'' + args._dato.id + '\'';
            ID_1251981848_i.fetch({
                query: 'SELECT * FROM insp_recintos WHERE id=\'' + args._dato.id + '\''
            });
            var insp_datos = require('helper').query2array(ID_1251981848_i);
            if (Ti.App.deployType != 'production') console.log('contenido datos de todo pio', {
                "insp": insp_datos,
                "cont": datos,
                "args": args
            });
            if (insp_datos && insp_datos.length) {
                var ID_893030032_i = Alloy.createCollection('muros_tabiques');
                var ID_893030032_i_where = '';
                ID_893030032_i.fetch();
                var muros_tabiques = require('helper').query2array(ID_893030032_i);
                var padre_index = 0;
                _.each(muros_tabiques, function(padre, padre_pos, padre_list) {
                    padre_index += 1;
                    if (_.isNull(insp_datos[0].ids_muros)) {
                        /** 
                         * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
                         */
                        padre._estado = 0;
                    } else if (insp_datos[0].ids_muros.indexOf(padre.id_segured) != -1) {
                        padre._estado = 1;
                    } else {
                        padre._estado = 0;
                    }
                });
                var datos = [];
                _.each(muros_tabiques, function(fila, pos) {
                    var new_row = {};
                    _.each(fila, function(x, llave) {
                        var newkey = '';
                        if (llave == 'nombre') newkey = 'label';
                        if (llave == 'id_segured') newkey = 'valor';
                        if (llave == '_estado') newkey = 'estado';
                        if (newkey != '') new_row[newkey] = fila[llave];
                    });
                    datos.push(new_row);
                });
            } else {
                var ID_1197980604_i = Alloy.createCollection('muros_tabiques');
                ID_1197980604_i.fetch();
                var ID_1197980604_src = require('helper').query2array(ID_1197980604_i);
                var datos = [];
                _.each(ID_1197980604_src, function(fila, pos) {
                    var new_row = {};
                    _.each(fila, function(x, llave) {
                        var newkey = '';
                        if (llave == 'nombre') newkey = 'label';
                        if (llave == 'id_segured') newkey = 'valor';
                        if (newkey != '') new_row[newkey] = fila[llave];
                    });
                    new_row['estado'] = 0;
                    datos.push(new_row);
                });
            }
            $.ID_1975428302.update({
                data: datos
            });
        }
    };
    var ID_7512644 = setTimeout(ID_7512644_func, 1000 * 0.2);

}

$.ID_399652068.init({
    titulo: 'Entrepisos',
    cargando: 'cargando...',
    __id: 'ALL399652068',
    oncerrar: Cerrar_ID_1223292786,
    left: 0,
    hint: 'Seleccione entrepisos',
    color: 'morado',
    subtitulo: 'Indique los tipos',
    right: 0,
    onclick: Click_ID_1631472609,
    onafterinit: Afterinit_ID_1298981253
});

function Click_ID_1631472609(e) {

    var evento = e;

}

function Cerrar_ID_1223292786(e) {

    var evento = e;
    $.ID_399652068.update({});
    $.recinto2.set({
        ids_entrepisos: evento.valores
    });
    if ('recinto2' in $) $recinto2 = $.recinto2.toJSON();

}

function Afterinit_ID_1298981253(e) {

    var evento = e;
    /** 
     * ejecutamos desfasado para no congelar el hilo. 
     */
    var ID_847564113_func = function() {
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
            var ID_1099398694_i = Alloy.createCollection('insp_recintos');
            var ID_1099398694_i_where = 'id=\'' + args._dato.id + '\'';
            ID_1099398694_i.fetch({
                query: 'SELECT * FROM insp_recintos WHERE id=\'' + args._dato.id + '\''
            });
            var insp_datos = require('helper').query2array(ID_1099398694_i);
            if (insp_datos && insp_datos.length) {
                var ID_658921339_i = Alloy.createCollection('entrepisos');
                var ID_658921339_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
                ID_658921339_i.fetch({
                    query: 'SELECT * FROM entrepisos WHERE pais_texto=\'' + pais[0].nombre + '\''
                });
                var entrepisos = require('helper').query2array(ID_658921339_i);
                var padre_index = 0;
                _.each(entrepisos, function(padre, padre_pos, padre_list) {
                    padre_index += 1;
                    if (_.isNull(insp_datos[0].ids_entrepisos)) {
                        /** 
                         * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
                         */
                        padre._estado = 0;
                    } else if (insp_datos[0].ids_entrepisos.indexOf(padre.id_segured) != -1) {
                        padre._estado = 1;
                    } else {
                        padre._estado = 0;
                    }
                });
                var datos = [];
                _.each(entrepisos, function(fila, pos) {
                    var new_row = {};
                    _.each(fila, function(x, llave) {
                        var newkey = '';
                        if (llave == 'nombre') newkey = 'label';
                        if (llave == 'id_segured') newkey = 'valor';
                        if (llave == '_estado') newkey = 'estado';
                        if (newkey != '') new_row[newkey] = fila[llave];
                    });
                    datos.push(new_row);
                });
            } else {
                var ID_36564061_i = Alloy.createCollection('entrepisos');
                ID_36564061_i.fetch();
                var ID_36564061_src = require('helper').query2array(ID_36564061_i);
                var datos = [];
                _.each(ID_36564061_src, function(fila, pos) {
                    var new_row = {};
                    _.each(fila, function(x, llave) {
                        var newkey = '';
                        if (llave == 'nombre') newkey = 'label';
                        if (llave == 'id_segured') newkey = 'valor';
                        if (newkey != '') new_row[newkey] = fila[llave];
                    });
                    new_row['estado'] = 0;
                    datos.push(new_row);
                });
            }
            $.ID_399652068.update({
                data: datos
            });
        } else {
            /** 
             * cargamos datos dummy para cuando compilamos de forma individual. 
             */
            var ID_619849197_i = Alloy.Collections.entrepisos;
            var sql = "DELETE FROM " + ID_619849197_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_619849197_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_619849197_i.trigger('delete');
            var item_index = 0;
            _.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
                item_index += 1;
                var ID_98051655_m = Alloy.Collections.entrepisos;
                var ID_98051655_fila = Alloy.createModel('entrepisos', {
                    nombre: 'entrepisos ' + item,
                    id_server: '0' + item,
                    id_segured: '10' + item,
                    pais: 1
                });
                ID_98051655_m.add(ID_98051655_fila);
                ID_98051655_fila.save();
                _.defer(function() {});
            });
            var ID_1140913964_i = Alloy.createCollection('insp_recintos');
            var ID_1140913964_i_where = 'id=\'' + args._dato.id + '\'';
            ID_1140913964_i.fetch({
                query: 'SELECT * FROM insp_recintos WHERE id=\'' + args._dato.id + '\''
            });
            var insp_datos = require('helper').query2array(ID_1140913964_i);
            if (Ti.App.deployType != 'production') console.log('contenido datos de todo pio', {
                "insp": insp_datos,
                "cont": datos,
                "args": args
            });
            if (insp_datos && insp_datos.length) {
                var ID_1727899040_i = Alloy.createCollection('entrepisos');
                var ID_1727899040_i_where = '';
                ID_1727899040_i.fetch();
                var entrepisos = require('helper').query2array(ID_1727899040_i);
                var padre_index = 0;
                _.each(entrepisos, function(padre, padre_pos, padre_list) {
                    padre_index += 1;
                    if (_.isNull(insp_datos[0].ids_entrepisos)) {
                        /** 
                         * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
                         */
                        padre._estado = 0;
                        if (Ti.App.deployType != 'production') console.log('habia nulo', {});
                    } else if (insp_datos[0].ids_entrepisos.indexOf(padre.id_segured) != -1) {
                        padre._estado = 1;
                        if (Ti.App.deployType != 'production') console.log('contiene algo', {});
                    } else {
                        padre._estado = 0;
                        if (Ti.App.deployType != 'production') console.log('otra cosa', {});
                    }
                });
                var datos = [];
                _.each(entrepisos, function(fila, pos) {
                    var new_row = {};
                    _.each(fila, function(x, llave) {
                        var newkey = '';
                        if (llave == 'nombre') newkey = 'label';
                        if (llave == 'id_segured') newkey = 'valor';
                        if (llave == '_estado') newkey = 'estado';
                        if (newkey != '') new_row[newkey] = fila[llave];
                    });
                    datos.push(new_row);
                });
            } else {
                var ID_1123234666_i = Alloy.createCollection('entrepisos');
                ID_1123234666_i.fetch();
                var ID_1123234666_src = require('helper').query2array(ID_1123234666_i);
                var datos = [];
                _.each(ID_1123234666_src, function(fila, pos) {
                    var new_row = {};
                    _.each(fila, function(x, llave) {
                        var newkey = '';
                        if (llave == 'nombre') newkey = 'label';
                        if (llave == 'id_segured') newkey = 'valor';
                        if (newkey != '') new_row[newkey] = fila[llave];
                    });
                    new_row['estado'] = 0;
                    datos.push(new_row);
                });
            }
            $.ID_399652068.update({
                data: datos
            });
        }
    };
    var ID_847564113 = setTimeout(ID_847564113_func, 1000 * 0.2);

}

$.ID_1990255213.init({
    titulo: 'Pavimentos',
    cargando: 'cargando...',
    __id: 'ALL1990255213',
    oncerrar: Cerrar_ID_1979957644,
    left: 0,
    hint: 'Seleccione pavimentos',
    color: 'morado',
    subtitulo: 'Indique los tipos',
    right: 0,
    onclick: Click_ID_1433183121,
    onafterinit: Afterinit_ID_129700751
});

function Click_ID_1433183121(e) {

    var evento = e;

}

function Cerrar_ID_1979957644(e) {

    var evento = e;
    $.ID_1990255213.update({});
    $.recinto2.set({
        ids_pavimentos: evento.valores
    });
    if ('recinto2' in $) $recinto2 = $.recinto2.toJSON();

}

function Afterinit_ID_129700751(e) {

    var evento = e;
    /** 
     * ejecutamos desfasado para no congelar el hilo. 
     */
    var ID_502523396_func = function() {
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
            var ID_579789269_i = Alloy.createCollection('insp_recintos');
            var ID_579789269_i_where = 'id=\'' + args._dato.id + '\'';
            ID_579789269_i.fetch({
                query: 'SELECT * FROM insp_recintos WHERE id=\'' + args._dato.id + '\''
            });
            var insp_datos = require('helper').query2array(ID_579789269_i);
            if (insp_datos && insp_datos.length) {
                var ID_1654445525_i = Alloy.createCollection('pavimento');
                var ID_1654445525_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
                ID_1654445525_i.fetch({
                    query: 'SELECT * FROM pavimento WHERE pais_texto=\'' + pais[0].nombre + '\''
                });
                var pavimento = require('helper').query2array(ID_1654445525_i);
                var padre_index = 0;
                _.each(pavimento, function(padre, padre_pos, padre_list) {
                    padre_index += 1;
                    if (_.isNull(insp_datos[0].ids_pavimentos)) {
                        /** 
                         * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
                         */
                        padre._estado = 0;
                    } else if (insp_datos[0].ids_pavimentos.indexOf(padre.id_segured) != -1) {
                        padre._estado = 1;
                    } else {
                        padre._estado = 0;
                    }
                });
                var datos = [];
                _.each(pavimento, function(fila, pos) {
                    var new_row = {};
                    _.each(fila, function(x, llave) {
                        var newkey = '';
                        if (llave == 'nombre') newkey = 'label';
                        if (llave == 'id_segured') newkey = 'valor';
                        if (llave == '_estado') newkey = 'estado';
                        if (newkey != '') new_row[newkey] = fila[llave];
                    });
                    datos.push(new_row);
                });
            } else {
                var ID_380065937_i = Alloy.createCollection('pavimento');
                ID_380065937_i.fetch();
                var ID_380065937_src = require('helper').query2array(ID_380065937_i);
                var datos = [];
                _.each(ID_380065937_src, function(fila, pos) {
                    var new_row = {};
                    _.each(fila, function(x, llave) {
                        var newkey = '';
                        if (llave == 'nombre') newkey = 'label';
                        if (llave == 'id_segured') newkey = 'valor';
                        if (newkey != '') new_row[newkey] = fila[llave];
                    });
                    new_row['estado'] = 0;
                    datos.push(new_row);
                });
            }
            $.ID_1990255213.update({
                data: datos
            });
        } else {
            /** 
             * cargamos datos dummy para cuando compilamos de forma individual. 
             */
            var ID_1025311997_i = Alloy.Collections.pavimento;
            var sql = "DELETE FROM " + ID_1025311997_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_1025311997_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_1025311997_i.trigger('delete');
            var item_index = 0;
            _.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
                item_index += 1;
                var ID_956180352_m = Alloy.Collections.pavimento;
                var ID_956180352_fila = Alloy.createModel('pavimento', {
                    nombre: 'pavimento ' + item,
                    id_server: '0' + item,
                    id_segured: '10' + item,
                    pais: 1
                });
                ID_956180352_m.add(ID_956180352_fila);
                ID_956180352_fila.save();
                _.defer(function() {});
            });
            var ID_981671904_i = Alloy.createCollection('insp_recintos');
            var ID_981671904_i_where = 'id=\'' + args._dato.id + '\'';
            ID_981671904_i.fetch({
                query: 'SELECT * FROM insp_recintos WHERE id=\'' + args._dato.id + '\''
            });
            var insp_datos = require('helper').query2array(ID_981671904_i);
            if (Ti.App.deployType != 'production') console.log('contenido datos de todo pio', {
                "insp": insp_datos,
                "cont": datos,
                "args": args
            });
            if (insp_datos && insp_datos.length) {
                var ID_1689423368_i = Alloy.createCollection('pavimento');
                var ID_1689423368_i_where = '';
                ID_1689423368_i.fetch();
                var pavimento = require('helper').query2array(ID_1689423368_i);
                var padre_index = 0;
                _.each(pavimento, function(padre, padre_pos, padre_list) {
                    padre_index += 1;
                    if (_.isNull(insp_datos[0].ids_pavimentos)) {
                        /** 
                         * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
                         */
                        padre._estado = 0;
                        if (Ti.App.deployType != 'production') console.log('habia nulo', {});
                    } else if (insp_datos[0].ids_pavimentos.indexOf(padre.id_segured) != -1) {
                        padre._estado = 1;
                        if (Ti.App.deployType != 'production') console.log('contiene algo', {});
                    } else {
                        padre._estado = 0;
                        if (Ti.App.deployType != 'production') console.log('otra cosa', {});
                    }
                });
                var datos = [];
                _.each(pavimento, function(fila, pos) {
                    var new_row = {};
                    _.each(fila, function(x, llave) {
                        var newkey = '';
                        if (llave == 'nombre') newkey = 'label';
                        if (llave == 'id_segured') newkey = 'valor';
                        if (llave == '_estado') newkey = 'estado';
                        if (newkey != '') new_row[newkey] = fila[llave];
                    });
                    datos.push(new_row);
                });
            } else {
                var ID_1538251607_i = Alloy.createCollection('pavimento');
                ID_1538251607_i.fetch();
                var ID_1538251607_src = require('helper').query2array(ID_1538251607_i);
                var datos = [];
                _.each(ID_1538251607_src, function(fila, pos) {
                    var new_row = {};
                    _.each(fila, function(x, llave) {
                        var newkey = '';
                        if (llave == 'nombre') newkey = 'label';
                        if (llave == 'id_segured') newkey = 'valor';
                        if (newkey != '') new_row[newkey] = fila[llave];
                    });
                    new_row['estado'] = 0;
                    datos.push(new_row);
                });
            }
            $.ID_1990255213.update({
                data: datos
            });
        }
    };
    var ID_502523396 = setTimeout(ID_502523396_func, 1000 * 0.2);

}

$.ID_1536081210.init({
    titulo: 'Estruct. cubierta',
    cargando: 'cargando...',
    __id: 'ALL1536081210',
    oncerrar: Cerrar_ID_307547788,
    left: 0,
    hint: 'Seleccione e.cubiertas',
    color: 'morado',
    subtitulo: 'Indique los tipos',
    right: 0,
    onclick: Click_ID_1479345319,
    onafterinit: Afterinit_ID_34391467
});

function Click_ID_1479345319(e) {

    var evento = e;

}

function Cerrar_ID_307547788(e) {

    var evento = e;
    $.ID_1536081210.update({});
    $.recinto2.set({
        ids_estructura_cubiera: evento.valores
    });
    if ('recinto2' in $) $recinto2 = $.recinto2.toJSON();

}

function Afterinit_ID_34391467(e) {

    var evento = e;
    /** 
     * ejecutamos desfasado para no congelar el hilo. 
     */
    var ID_1552516105_func = function() {
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
            var ID_783472236_i = Alloy.createCollection('insp_recintos');
            var ID_783472236_i_where = 'id=\'' + args._dato.id + '\'';
            ID_783472236_i.fetch({
                query: 'SELECT * FROM insp_recintos WHERE id=\'' + args._dato.id + '\''
            });
            var insp_datos = require('helper').query2array(ID_783472236_i);
            if (insp_datos && insp_datos.length) {
                var ID_426614964_i = Alloy.createCollection('estructura_cubierta');
                var ID_426614964_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
                ID_426614964_i.fetch({
                    query: 'SELECT * FROM estructura_cubierta WHERE pais_texto=\'' + pais[0].nombre + '\''
                });
                var estructura_cubierta = require('helper').query2array(ID_426614964_i);
                var padre_index = 0;
                _.each(estructura_cubierta, function(padre, padre_pos, padre_list) {
                    padre_index += 1;
                    if (_.isNull(insp_datos[0].ids_estructura_cubiera)) {
                        /** 
                         * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
                         */
                        padre._estado = 0;
                    } else if (insp_datos[0].ids_estructura_cubiera.indexOf(padre.id_segured) != -1) {
                        padre._estado = 1;
                    } else {
                        padre._estado = 0;
                    }
                });
                var datos = [];
                _.each(estructura_cubierta, function(fila, pos) {
                    var new_row = {};
                    _.each(fila, function(x, llave) {
                        var newkey = '';
                        if (llave == 'nombre') newkey = 'label';
                        if (llave == 'id_segured') newkey = 'valor';
                        if (llave == '_estado') newkey = 'estado';
                        if (newkey != '') new_row[newkey] = fila[llave];
                    });
                    datos.push(new_row);
                });
            } else {
                var ID_1771479164_i = Alloy.createCollection('estructura_cubierta');
                ID_1771479164_i.fetch();
                var ID_1771479164_src = require('helper').query2array(ID_1771479164_i);
                var datos = [];
                _.each(ID_1771479164_src, function(fila, pos) {
                    var new_row = {};
                    _.each(fila, function(x, llave) {
                        var newkey = '';
                        if (llave == 'nombre') newkey = 'label';
                        if (llave == 'id_segured') newkey = 'valor';
                        if (newkey != '') new_row[newkey] = fila[llave];
                    });
                    new_row['estado'] = 0;
                    datos.push(new_row);
                });
            }
            $.ID_1536081210.update({
                data: datos
            });
        } else {
            /** 
             * cargamos datos dummy para cuando compilamos de forma individual. 
             */
            var ID_1749460613_i = Alloy.Collections.estructura_cubierta;
            var sql = "DELETE FROM " + ID_1749460613_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_1749460613_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_1749460613_i.trigger('delete');
            var item_index = 0;
            _.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
                item_index += 1;
                var ID_1030121615_m = Alloy.Collections.estructura_cubierta;
                var ID_1030121615_fila = Alloy.createModel('estructura_cubierta', {
                    nombre: 'estructura_cubierta ' + item,
                    id_server: '0' + item,
                    id_segured: '10' + item,
                    pais: 1
                });
                ID_1030121615_m.add(ID_1030121615_fila);
                ID_1030121615_fila.save();
                _.defer(function() {});
            });
            var ID_1351373951_i = Alloy.createCollection('insp_recintos');
            var ID_1351373951_i_where = 'id=\'' + args._dato.id + '\'';
            ID_1351373951_i.fetch({
                query: 'SELECT * FROM insp_recintos WHERE id=\'' + args._dato.id + '\''
            });
            var insp_datos = require('helper').query2array(ID_1351373951_i);
            if (Ti.App.deployType != 'production') console.log('contenido datos de todo pio', {
                "insp": insp_datos,
                "cont": datos,
                "args": args
            });
            if (insp_datos && insp_datos.length) {
                var ID_121588200_i = Alloy.createCollection('estructura_cubierta');
                var ID_121588200_i_where = '';
                ID_121588200_i.fetch();
                var estructura_cubierta = require('helper').query2array(ID_121588200_i);
                var padre_index = 0;
                _.each(estructura_cubierta, function(padre, padre_pos, padre_list) {
                    padre_index += 1;
                    if (_.isNull(insp_datos[0].ids_estructura_cubiera)) {
                        /** 
                         * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
                         */
                        padre._estado = 0;
                    } else if (insp_datos[0].ids_estructura_cubiera.indexOf(padre.id_segured) != -1) {
                        padre._estado = 1;
                    } else {
                        padre._estado = 0;
                    }
                });
                var datos = [];
                _.each(estructura_cubierta, function(fila, pos) {
                    var new_row = {};
                    _.each(fila, function(x, llave) {
                        var newkey = '';
                        if (llave == 'nombre') newkey = 'label';
                        if (llave == 'id_segured') newkey = 'valor';
                        if (llave == '_estado') newkey = 'estado';
                        if (newkey != '') new_row[newkey] = fila[llave];
                    });
                    datos.push(new_row);
                });
            } else {
                var ID_925591115_i = Alloy.createCollection('estructura_cubierta');
                ID_925591115_i.fetch();
                var ID_925591115_src = require('helper').query2array(ID_925591115_i);
                var datos = [];
                _.each(ID_925591115_src, function(fila, pos) {
                    var new_row = {};
                    _.each(fila, function(x, llave) {
                        var newkey = '';
                        if (llave == 'nombre') newkey = 'label';
                        if (llave == 'id_segured') newkey = 'valor';
                        if (newkey != '') new_row[newkey] = fila[llave];
                    });
                    new_row['estado'] = 0;
                    datos.push(new_row);
                });
            }
            $.ID_1536081210.update({
                data: datos
            });
        }
    };
    var ID_1552516105 = setTimeout(ID_1552516105_func, 1000 * 0.2);

}

$.ID_363463535.init({
    titulo: 'Cubierta',
    cargando: 'cargando...',
    __id: 'ALL363463535',
    oncerrar: Cerrar_ID_443341336,
    left: 0,
    hint: 'Seleccione cubiertas',
    color: 'morado',
    subtitulo: 'Indique los tipos',
    right: 0,
    onclick: Click_ID_157900844,
    onafterinit: Afterinit_ID_1810892148
});

function Click_ID_157900844(e) {

    var evento = e;

}

function Cerrar_ID_443341336(e) {

    var evento = e;
    $.ID_363463535.update({});
    $.recinto2.set({
        ids_cubierta: evento.valores
    });
    if ('recinto2' in $) $recinto2 = $.recinto2.toJSON();

}

function Afterinit_ID_1810892148(e) {

    var evento = e;
    /** 
     * ejecutamos desfasado para no congelar el hilo. 
     */
    var ID_1647553309_func = function() {
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
            var ID_1675994683_i = Alloy.createCollection('insp_recintos');
            var ID_1675994683_i_where = 'id=\'' + args._dato.id + '\'';
            ID_1675994683_i.fetch({
                query: 'SELECT * FROM insp_recintos WHERE id=\'' + args._dato.id + '\''
            });
            var insp_datos = require('helper').query2array(ID_1675994683_i);
            if (insp_datos && insp_datos.length) {
                var ID_94778579_i = Alloy.createCollection('cubierta');
                var ID_94778579_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
                ID_94778579_i.fetch({
                    query: 'SELECT * FROM cubierta WHERE pais_texto=\'' + pais[0].nombre + '\''
                });
                var cubierta = require('helper').query2array(ID_94778579_i);
                var padre_index = 0;
                _.each(cubierta, function(padre, padre_pos, padre_list) {
                    padre_index += 1;
                    if (_.isNull(insp_datos[0].ids_cubierta)) {
                        /** 
                         * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
                         */
                        padre._estado = 0;
                    } else if (insp_datos[0].ids_cubierta.indexOf(padre.id_segured) != -1) {
                        padre._estado = 1;
                    } else {
                        padre._estado = 0;
                    }
                });
                var datos = [];
                _.each(cubierta, function(fila, pos) {
                    var new_row = {};
                    _.each(fila, function(x, llave) {
                        var newkey = '';
                        if (llave == 'nombre') newkey = 'label';
                        if (llave == 'id_segured') newkey = 'valor';
                        if (llave == '_estado') newkey = 'estado';
                        if (newkey != '') new_row[newkey] = fila[llave];
                    });
                    datos.push(new_row);
                });
            } else {
                var ID_1726519784_i = Alloy.createCollection('cubierta');
                ID_1726519784_i.fetch();
                var ID_1726519784_src = require('helper').query2array(ID_1726519784_i);
                var datos = [];
                _.each(ID_1726519784_src, function(fila, pos) {
                    var new_row = {};
                    _.each(fila, function(x, llave) {
                        var newkey = '';
                        if (llave == 'nombre') newkey = 'label';
                        if (llave == 'id_segured') newkey = 'valor';
                        if (newkey != '') new_row[newkey] = fila[llave];
                    });
                    new_row['estado'] = 0;
                    datos.push(new_row);
                });
            }
            $.ID_363463535.update({
                data: datos
            });
        } else {
            /** 
             * cargamos datos dummy para cuando compilamos de forma individual. 
             */
            var ID_522625302_i = Alloy.Collections.cubierta;
            var sql = "DELETE FROM " + ID_522625302_i.config.adapter.collection_name;
            var db = Ti.Database.open(ID_522625302_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_522625302_i.trigger('delete');
            var item_index = 0;
            _.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
                item_index += 1;
                var ID_617693973_m = Alloy.Collections.cubierta;
                var ID_617693973_fila = Alloy.createModel('cubierta', {
                    nombre: 'cubierta ' + item,
                    id_server: '0' + item,
                    id_segured: '10' + item,
                    pais: 1
                });
                ID_617693973_m.add(ID_617693973_fila);
                ID_617693973_fila.save();
                _.defer(function() {});
            });
            var ID_1496036347_i = Alloy.createCollection('insp_recintos');
            var ID_1496036347_i_where = 'id=\'' + args._dato.id + '\'';
            ID_1496036347_i.fetch({
                query: 'SELECT * FROM insp_recintos WHERE id=\'' + args._dato.id + '\''
            });
            var insp_datos = require('helper').query2array(ID_1496036347_i);
            if (Ti.App.deployType != 'production') console.log('contenido datos de todo pio', {
                "insp": insp_datos,
                "cont": datos,
                "args": args
            });
            if (insp_datos && insp_datos.length) {
                var ID_721488106_i = Alloy.createCollection('cubierta');
                var ID_721488106_i_where = '';
                ID_721488106_i.fetch();
                var cubierta = require('helper').query2array(ID_721488106_i);
                var padre_index = 0;
                _.each(cubierta, function(padre, padre_pos, padre_list) {
                    padre_index += 1;
                    if (_.isNull(insp_datos[0].ids_cubierta)) {
                        /** 
                         * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
                         */
                        padre._estado = 0;
                    } else if (insp_datos[0].ids_cubierta.indexOf(padre.id_segured) != -1) {
                        padre._estado = 1;
                    } else {
                        padre._estado = 0;
                    }
                });
                var datos = [];
                _.each(cubierta, function(fila, pos) {
                    var new_row = {};
                    _.each(fila, function(x, llave) {
                        var newkey = '';
                        if (llave == 'nombre') newkey = 'label';
                        if (llave == 'id_segured') newkey = 'valor';
                        if (llave == '_estado') newkey = 'estado';
                        if (newkey != '') new_row[newkey] = fila[llave];
                    });
                    datos.push(new_row);
                });
            } else {
                var ID_540346592_i = Alloy.createCollection('cubierta');
                ID_540346592_i.fetch();
                var ID_540346592_src = require('helper').query2array(ID_540346592_i);
                var datos = [];
                _.each(ID_540346592_src, function(fila, pos) {
                    var new_row = {};
                    _.each(fila, function(x, llave) {
                        var newkey = '';
                        if (llave == 'nombre') newkey = 'label';
                        if (llave == 'id_segured') newkey = 'valor';
                        if (newkey != '') new_row[newkey] = fila[llave];
                    });
                    new_row['estado'] = 0;
                    datos.push(new_row);
                });
            }
            $.ID_363463535.update({
                data: datos
            });
        }
    };
    var ID_1647553309 = setTimeout(ID_1647553309_func, 1000 * 0.2);

}

function Touchstart_ID_655428204(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    $.ID_49063077.setColor('#999999');


}

function Touchend_ID_808324098(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    $.ID_49063077.setColor('#8383db');

    /** 
     * Ocupamos esta variable para impedir que la pantalla siguente se cargue dos veces 
     */
    var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
    if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
        require('vars')['var_abriendo'] = 'true';
        Alloy.createController("nuevodano_index", {}).getView().open();
    }

}

function Swipe_ID_616394471(e) {

    e.cancelBubble = true;
    var elemento = e.section.getItemAt(e.itemIndex);
    var findVariables = require('fvariables');
    var fila = {};
    _.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
        _.each(obj_id, function(valor, prop) {
            fila[prop] = elemento[id_field][prop];
            var llaves = findVariables(valor, '{', '}');
            _.each(llaves, function(llave) {
                fila[llave] = elemento[id_field][prop];
            });
        });
    });
    if (Ti.App.deployType != 'production') console.log('en normal', {
        "e": e
    });
    if (e.direction == 'left') {
        var findVariables = require('fvariables');
        elemento.template = 'pborrar';
        _.each(_list_templates['pborrar'], function(obj_id, id_field) {
            _.each(obj_id, function(valor, prop) {
                var llaves = findVariables(valor, '{', '}');
                _.each(llaves, function(llave) {
                    elemento[id_field] = {};
                    elemento[id_field][prop] = fila[llave];
                });
            });
        });
        if (OS_IOS) {
            e.section.updateItemAt(e.itemIndex, elemento, {
                animated: true
            });
        } else if (OS_ANDROID) {
            e.section.updateItemAt(e.itemIndex, elemento);
        }
    }

}

function Click_ID_1244881555(e) {

    e.cancelBubble = true;
    var elemento = e.section.getItemAt(e.itemIndex);
    var findVariables = require('fvariables');
    var fila = {};
    _.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
        _.each(obj_id, function(valor, prop) {
            fila[prop] = elemento[id_field][prop];
            var llaves = findVariables(valor, '{', '}');
            _.each(llaves, function(llave) {
                fila[llave] = elemento[id_field][prop];
            });
        });
    });
    /** 
     * Ocupamos esta variable para impedir que la pantalla siguente se cargue dos veces 
     */
    var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
    if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
        require('vars')['var_abriendo'] = 'true';
        /** 
         * Enviamos el parametro _dato para indicar cual sera el id del dano a editar 
         */
        Alloy.createController("editardano_index", {
            '_dato': fila
        }).getView().open();
    }

}

function Click_ID_1260268716(e) {

    e.cancelBubble = true;
    var elemento = e.section.getItemAt(e.itemIndex);
    var findVariables = require('fvariables');
    var fila = {};
    _.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
        _.each(obj_id, function(valor, prop) {
            fila[prop] = elemento[id_field][prop];
            var llaves = findVariables(valor, '{', '}');
            _.each(llaves, function(llave) {
                fila[llave] = elemento[id_field][prop];
            });
        });
    });
    if (Ti.App.deployType != 'production') console.log('click en borrar', {
        "e": e
    });
    var ID_1455191021_opts = ['Si', 'No'];
    var ID_1455191021 = Ti.UI.createAlertDialog({
        title: 'ALERTA',
        message: '' + 'Seguro quiere borrar ' + fila.nombre + ' ?' + '',
        buttonNames: ID_1455191021_opts
    });
    ID_1455191021.addEventListener('click', function(e) {
        var xd = ID_1455191021_opts[e.index];
        if (xd == 'Si') {
            if (Ti.App.deployType != 'production') console.log('borrando xd', {});
            var ID_570776821_i = Alloy.Collections.insp_itemdanos;
            var sql = 'DELETE FROM ' + ID_570776821_i.config.adapter.collection_name + ' WHERE id=\'' + fila.id + '\'';
            var db = Ti.Database.open(ID_570776821_i.config.adapter.db_name);
            db.execute(sql);
            db.close();
            sql = null;
            db = null;
            ID_570776821_i.trigger('delete');
            _.defer(function() {
                Alloy.Collections.insp_itemdanos.fetch();
            });
        }
        xd = null;
        e.source.removeEventListener("click", arguments.callee);
    });
    ID_1455191021.show();

}

function Swipe_ID_892281804(e) {

    e.cancelBubble = true;
    var elemento = e.section.getItemAt(e.itemIndex);
    var findVariables = require('fvariables');
    var fila = {};
    _.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
        _.each(obj_id, function(valor, prop) {
            fila[prop] = elemento[id_field][prop];
            var llaves = findVariables(valor, '{', '}');
            _.each(llaves, function(llave) {
                fila[llave] = elemento[id_field][prop];
            });
        });
    });
    if (Ti.App.deployType != 'production') console.log('en borrar', {
        "e": e
    });
    if (e.direction == 'right') {
        if (Ti.App.deployType != 'production') console.log('swipe hacia derecha (desde borrar)', {});
        var findVariables = require('fvariables');
        elemento.template = 'dano';
        _.each(_list_templates['dano'], function(obj_id, id_field) {
            _.each(obj_id, function(valor, prop) {
                var llaves = findVariables(valor, '{', '}');
                _.each(llaves, function(llave) {
                    elemento[id_field] = {};
                    elemento[id_field][prop] = fila[llave];
                });
            });
        });
        if (OS_IOS) {
            e.section.updateItemAt(e.itemIndex, elemento, {
                animated: true
            });
        } else if (OS_ANDROID) {
            e.section.updateItemAt(e.itemIndex, elemento);
        }
    }

}

function Click_ID_821791307(e) {

    e.cancelBubble = true;
    var elemento = e.section.getItemAt(e.itemIndex);
    var findVariables = require('fvariables');
    var fila = {};
    _.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
        _.each(obj_id, function(valor, prop) {
            fila[prop] = elemento[id_field][prop];
            var llaves = findVariables(valor, '{', '}');
            _.each(llaves, function(llave) {
                fila[llave] = elemento[id_field][prop];
            });
        });
    });
    if (Ti.App.deployType != 'production') console.log('en borrar', {
        "e": e
    });
    var findVariables = require('fvariables');
    elemento.template = 'dano';
    _.each(_list_templates['dano'], function(obj_id, id_field) {
        _.each(obj_id, function(valor, prop) {
            var llaves = findVariables(valor, '{', '}');
            _.each(llaves, function(llave) {
                elemento[id_field] = {};
                elemento[id_field][prop] = fila[llave];
            });
        });
    });
    if (OS_IOS) {
        e.section.updateItemAt(e.itemIndex, elemento, {
            animated: true
        });
    } else if (OS_ANDROID) {
        e.section.updateItemAt(e.itemIndex, elemento);
    }

}

$.ID_824520076.init({
    onfotolista: Fotolista_ID_1992006047,
    __id: 'ALL824520076'
});

function Fotolista_ID_1992006047(e) {

    var evento = e;
    /** 
     * Recuperamos cual fue la foto seleccionada 
     */
    var cual_foto = ('cual_foto' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['cual_foto'] : '';
    if (cual_foto == 1 || cual_foto == '1') {
        var ID_616043033_m = Alloy.Collections.numero_unico;
        var ID_616043033_fila = Alloy.createModel('numero_unico', {
            comentario: 'nuevo recinto foto1'
        });
        ID_616043033_m.add(ID_616043033_fila);
        ID_616043033_fila.save();
        var nuevoid_f1 = require('helper').model2object(ID_616043033_m.last());
        _.defer(function() {});
        /** 
         * Guardamos en variable el detalle del modelo numero_unico para asignar informacion de foto1 del recinto 
         */
        require('vars')[_var_scopekey]['nuevoid_f1'] = nuevoid_f1;
        /** 
         * Recuperamos variable para saber si estamos haciendo una inspeccion o dummy y guardar la foto en la memoria del celular 
         */
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        var gps_coords = ('gps_coords' in require('vars')) ? require('vars')['gps_coords'] : '';
        var gps_latitud = ('gps_latitud' in require('vars')) ? require('vars')['gps_latitud'] : '';
        var gps_longitud = ('gps_longitud' in require('vars')) ? require('vars')['gps_longitud'] : '';
        var resultado = null;
        if ('distanceinmtbetweenearthcoordinates' in require('funciones')) {
            resultado = require('funciones').distanceinmtbetweenearthcoordinates({
                'lat1': gps_latitud,
                'lon1': gps_longitud,
                'lat2': seltarea.lat,
                'lon2': seltarea.lon
            });
        } else {
            try {
                resultado = f_distanceinmtbetweenearthcoordinates({
                    'lat1': gps_latitud,
                    'lon1': gps_longitud,
                    'lat2': seltarea.lat,
                    'lon2': seltarea.lon
                });
            } catch (ee) {}
        }
        if (_.isNumber(gps_coords.accuracy) && _.isNumber(resultado) && gps_coords.accuracy > resultado) {
            /** 
             * Pablo me comento que el flag fuera_rango se determina comparando precision vs la distancia de ubicacion con la definida en tarea 
             */
            var json_datosfoto = {
                lat: gps_latitud,
                lng: gps_longitud,
                fuera_rango: 0,
                distancia: Math.round(resultado),
                precision: Math.round(gps_coords.accuracy)
            };
        } else {
            var json_datosfoto = {
                lat: gps_latitud,
                lng: gps_longitud,
                fuera_rango: 1,
                distancia: Math.round(resultado),
                precision: Math.round(gps_coords.accuracy)
            };
        }
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var ID_697735347_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
            if (ID_697735347_d.exists() == false) ID_697735347_d.createDirectory();
            var ID_697735347_f = Ti.Filesystem.getFile(ID_697735347_d.resolve(), 'cap' + nuevoid_f1.id + '.jpg');
            if (ID_697735347_f.exists() == true) ID_697735347_f.deleteFile();
            ID_697735347_f.write(evento.foto);
            ID_697735347_d = null;
            ID_697735347_f = null;
            var ID_393008998_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
            if (ID_393008998_d.exists() == false) ID_393008998_d.createDirectory();
            var ID_393008998_f = Ti.Filesystem.getFile(ID_393008998_d.resolve(), 'cap' + nuevoid_f1.id + '.json');
            if (ID_393008998_f.exists() == true) ID_393008998_f.deleteFile();
            console.log('contenido f1 erecinto', JSON.stringify(json_datosfoto));
            ID_393008998_f.write(JSON.stringify(json_datosfoto));
            ID_393008998_d = null;
            ID_393008998_f = null;
        } else {
            var ID_1281614579_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + 0);
            if (ID_1281614579_d.exists() == false) ID_1281614579_d.createDirectory();
            var ID_1281614579_f = Ti.Filesystem.getFile(ID_1281614579_d.resolve(), 'cap' + nuevoid_f1.id + '.jpg');
            if (ID_1281614579_f.exists() == true) ID_1281614579_f.deleteFile();
            ID_1281614579_f.write(evento.foto);
            ID_1281614579_d = null;
            ID_1281614579_f = null;
        }
        /** 
         * Enviamos la foto para que genere la miniatura (y tambien la guarde) y muestre en pantalla 
         */
        $.ID_1650576465.procesar({
            imagen: evento.foto,
            nueva: 640,
            calidad: 91,
            camara: 'trasera'
        });
        /** 
         * Limpiamos memoria ram 
         */
        evento = null;
    } else if (cual_foto == 2) {
        var ID_498790765_m = Alloy.Collections.numero_unico;
        var ID_498790765_fila = Alloy.createModel('numero_unico', {
            comentario: 'nuevo recinto foto2'
        });
        ID_498790765_m.add(ID_498790765_fila);
        ID_498790765_fila.save();
        var nuevoid_f2 = require('helper').model2object(ID_498790765_m.last());
        _.defer(function() {});
        require('vars')[_var_scopekey]['nuevoid_f2'] = nuevoid_f2;
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        var gps_coords = ('gps_coords' in require('vars')) ? require('vars')['gps_coords'] : '';
        var gps_latitud = ('gps_latitud' in require('vars')) ? require('vars')['gps_latitud'] : '';
        var gps_longitud = ('gps_longitud' in require('vars')) ? require('vars')['gps_longitud'] : '';
        var resultado = null;
        if ('distanceinmtbetweenearthcoordinates' in require('funciones')) {
            resultado = require('funciones').distanceinmtbetweenearthcoordinates({
                'lat1': gps_latitud,
                'lon1': gps_longitud,
                'lat2': seltarea.lat,
                'lon2': seltarea.lon
            });
        } else {
            try {
                resultado = f_distanceinmtbetweenearthcoordinates({
                    'lat1': gps_latitud,
                    'lon1': gps_longitud,
                    'lat2': seltarea.lat,
                    'lon2': seltarea.lon
                });
            } catch (ee) {}
        }
        if (_.isNumber(gps_coords.accuracy) && _.isNumber(resultado) && gps_coords.accuracy > resultado) {
            /** 
             * Pablo me comento que el flag fuera_rango se determina comparando precision vs la distancia de ubicacion con la definida en tarea 
             */
            var json_datosfoto = {
                lat: gps_latitud,
                lng: gps_longitud,
                fuera_rango: 0,
                distancia: Math.round(resultado),
                precision: Math.round(gps_coords.accuracy)
            };
        } else {
            var json_datosfoto = {
                lat: gps_latitud,
                lng: gps_longitud,
                fuera_rango: 1,
                distancia: Math.round(resultado),
                precision: Math.round(gps_coords.accuracy)
            };
        }
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var ID_750880193_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
            if (ID_750880193_d.exists() == false) ID_750880193_d.createDirectory();
            var ID_750880193_f = Ti.Filesystem.getFile(ID_750880193_d.resolve(), 'cap' + nuevoid_f2.id + '.jpg');
            if (ID_750880193_f.exists() == true) ID_750880193_f.deleteFile();
            ID_750880193_f.write(evento.foto);
            ID_750880193_d = null;
            ID_750880193_f = null;
            var ID_1764535243_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
            if (ID_1764535243_d.exists() == false) ID_1764535243_d.createDirectory();
            var ID_1764535243_f = Ti.Filesystem.getFile(ID_1764535243_d.resolve(), 'cap' + nuevoid_f2.id + '.json');
            if (ID_1764535243_f.exists() == true) ID_1764535243_f.deleteFile();
            console.log('contenido f2 erecinto', JSON.stringify(json_datosfoto));
            ID_1764535243_f.write(JSON.stringify(json_datosfoto));
            ID_1764535243_d = null;
            ID_1764535243_f = null;
        } else {
            var ID_1514258240_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + 0);
            if (ID_1514258240_d.exists() == false) ID_1514258240_d.createDirectory();
            var ID_1514258240_f = Ti.Filesystem.getFile(ID_1514258240_d.resolve(), 'cap' + nuevoid_f2.id + '.jpg');
            if (ID_1514258240_f.exists() == true) ID_1514258240_f.deleteFile();
            ID_1514258240_f.write(evento.foto);
            ID_1514258240_d = null;
            ID_1514258240_f = null;
        }
        $.recinto2.set({
            foto2: 'cap' + nuevoid_f2.id + '.jpg'
        });
        if ('recinto2' in $) $recinto2 = $.recinto2.toJSON();
        $.ID_245423636.procesar({
            imagen: evento.foto,
            nueva: 640,
            calidad: 91,
            camara: 'trasera'
        });
        evento = null;
    } else if (cual_foto == 3) {
        var ID_963509381_m = Alloy.Collections.numero_unico;
        var ID_963509381_fila = Alloy.createModel('numero_unico', {
            comentario: 'nuevo recinto foto3'
        });
        ID_963509381_m.add(ID_963509381_fila);
        ID_963509381_fila.save();
        var nuevoid_f3 = require('helper').model2object(ID_963509381_m.last());
        _.defer(function() {});
        var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
        require('vars')[_var_scopekey]['nuevoid_f3'] = nuevoid_f3;
        var gps_coords = ('gps_coords' in require('vars')) ? require('vars')['gps_coords'] : '';
        var gps_latitud = ('gps_latitud' in require('vars')) ? require('vars')['gps_latitud'] : '';
        var gps_longitud = ('gps_longitud' in require('vars')) ? require('vars')['gps_longitud'] : '';
        var resultado = null;
        if ('distanceinmtbetweenearthcoordinates' in require('funciones')) {
            resultado = require('funciones').distanceinmtbetweenearthcoordinates({
                'lat1': gps_latitud,
                'lon1': gps_longitud,
                'lat2': seltarea.lat,
                'lon2': seltarea.lon
            });
        } else {
            try {
                resultado = f_distanceinmtbetweenearthcoordinates({
                    'lat1': gps_latitud,
                    'lon1': gps_longitud,
                    'lat2': seltarea.lat,
                    'lon2': seltarea.lon
                });
            } catch (ee) {}
        }
        if (_.isNumber(gps_coords.accuracy) && _.isNumber(resultado) && gps_coords.accuracy > resultado) {
            /** 
             * Pablo me comento que el flag fuera_rango se determina comparando precision vs la distancia de ubicacion con la definida en tarea 
             */
            var json_datosfoto = {
                lat: gps_latitud,
                lng: gps_longitud,
                fuera_rango: 0,
                distancia: Math.round(resultado),
                precision: Math.round(gps_coords.accuracy)
            };
        } else {
            var json_datosfoto = {
                lat: gps_latitud,
                lng: gps_longitud,
                fuera_rango: 1,
                distancia: Math.round(resultado),
                precision: Math.round(gps_coords.accuracy)
            };
        }
        if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
            var ID_1182736960_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
            if (ID_1182736960_d.exists() == false) ID_1182736960_d.createDirectory();
            var ID_1182736960_f = Ti.Filesystem.getFile(ID_1182736960_d.resolve(), 'cap' + nuevoid_f3.id + '.jpg');
            if (ID_1182736960_f.exists() == true) ID_1182736960_f.deleteFile();
            ID_1182736960_f.write(evento.foto);
            ID_1182736960_d = null;
            ID_1182736960_f = null;
            var ID_1103277653_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
            if (ID_1103277653_d.exists() == false) ID_1103277653_d.createDirectory();
            var ID_1103277653_f = Ti.Filesystem.getFile(ID_1103277653_d.resolve(), 'cap' + nuevoid_f3.id + '.json');
            if (ID_1103277653_f.exists() == true) ID_1103277653_f.deleteFile();
            console.log('contenido f3 erecinto', JSON.stringify(json_datosfoto));
            ID_1103277653_f.write(JSON.stringify(json_datosfoto));
            ID_1103277653_d = null;
            ID_1103277653_f = null;
        } else {
            var ID_598482667_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + 0);
            if (ID_598482667_d.exists() == false) ID_598482667_d.createDirectory();
            var ID_598482667_f = Ti.Filesystem.getFile(ID_598482667_d.resolve(), 'cap' + nuevoid_f3.id + '.jpg');
            if (ID_598482667_f.exists() == true) ID_598482667_f.deleteFile();
            ID_598482667_f.write(evento.foto);
            ID_598482667_d = null;
            ID_598482667_f = null;
        }
        $.recinto2.set({
            foto3: 'cap' + nuevoid_f3.id + '.jpg'
        });
        if ('recinto2' in $) $recinto2 = $.recinto2.toJSON();
        $.ID_277122098.procesar({
            imagen: evento.foto,
            nueva: 640,
            calidad: 91,
            camara: 'trasera'
        });
        evento = null;
    }
}

(function() {
    var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
    if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
        /** 
         * Modificamos la idinspeccion con el idserver de la tarea 
         */
        $.recinto2.set({
            id_inspeccion: seltarea.id_server
        });
        if ('recinto2' in $) $recinto2 = $.recinto2.toJSON();
    }
    /** 
     * heredamos datos previos de nivel en modelo nuevo con binding de esta pantalla 
     */
    var ID_135900937_i = Alloy.createCollection('insp_recintos');
    var ID_135900937_i_where = 'id=\'' + args._dato.id + '\'';
    ID_135900937_i.fetch({
        query: 'SELECT * FROM insp_recintos WHERE id=\'' + args._dato.id + '\''
    });
    var insp_r = require('helper').query2array(ID_135900937_i);
    /** 
     * filtramos items de dano, para el id_recinto activo. 
     */
    ID_1733573059_filter = function(coll) {
        var filtered = coll.filter(function(m) {
            var _tests = [],
                _all_true = false,
                model = m.toJSON();
            _tests.push((model.id_recinto == insp_r[0].id_recinto));
            var _all_true_s = _.uniq(_tests);
            _all_true = (_all_true_s.length == 1 && _all_true_s[0] == true) ? true : false;
            return _all_true;
        });
        filtered = _.toArray(filtered);
        return filtered;
    };
    _.defer(function() {
        Alloy.Collections.insp_itemdanos.fetch();
    });
    if (Ti.App.deployType != 'production') console.log('filtrando el id del recitno', {
        "datos": insp_r[0]
    });
    /** 
     * Cargamos el modelo con los datos obtenidos desde la consulta 
     */
    $.recinto2.set({
        ids_entrepisos: insp_r[0].ids_entrepisos,
        id_inspeccion: insp_r[0].id_inspeccion,
        nombre: insp_r[0].nombre,
        superficie: insp_r[0].superficie,
        largo: insp_r[0].largo,
        foto1: insp_r[0].foto1,
        foto2: insp_r[0].foto2,
        id_recinto: insp_r[0].id_recinto,
        foto3: insp_r[0].foto3,
        ids_pavimentos: insp_r[0].ids_pavimentos,
        alto: insp_r[0].alto,
        ids_muros: insp_r[0].ids_muros,
        ancho: insp_r[0].ancho,
        ids_estructura_cubiera: insp_r[0].ids_estructura_cubiera,
        ids_cubierta: insp_r[0].ids_cubierta,
        id_nivel: insp_r[0].id_nivel,
        ids_estructuras_soportantes: insp_r[0].ids_estructuras_soportantes
    });
    if ('recinto2' in $) $recinto2 = $.recinto2.toJSON();
    /** 
     * guardamos variable pq aun no existe este recinto al agregar da&#241;os 
     */
    require('vars')['temp_idrecinto'] = insp_r[0].id_recinto;
    /** 
     * Cargamos los textos en los campos de texto 
     */
    $.ID_1253568597.setValue(insp_r[0].largo);

    $.ID_1051579768.setValue(insp_r[0].ancho);

    $.ID_1174191051.setValue(insp_r[0].alto);

    $.ID_1077768092.setValue(insp_r[0].nombre);

    if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
        /** 
         * Revisamos si existe variable seltarea para saber si las fotos son desde la inspeccion o son datos dummy 
         */
        var ID_1579296770_trycatch = {
            error: function(e) {
                if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
            }
        };
        try {
            ID_1579296770_trycatch.error = function(evento) {};
            insp_r[0].foto1 = "mini" + insp_r[0].foto1.substring(3);
            var foto_1 = '';
            var ID_73302648_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + seltarea.id_server);
            if (ID_73302648_d.exists() == true) {
                var ID_73302648_f = Ti.Filesystem.getFile(ID_73302648_d.resolve(), insp_r[0].foto1);
                if (ID_73302648_f.exists() == true) {
                    foto_1 = ID_73302648_f.read();
                }
                ID_73302648_f = null;
            }
            ID_73302648_d = null;
        } catch (e) {
            ID_1579296770_trycatch.error(e);
        }
        var ID_1107845015_trycatch = {
            error: function(e) {
                if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
            }
        };
        try {
            ID_1107845015_trycatch.error = function(evento) {};
            insp_r[0].foto2 = "mini" + insp_r[0].foto2.substring(3);
            var foto_2 = '';
            var ID_1678191401_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + seltarea.id_server);
            if (ID_1678191401_d.exists() == true) {
                var ID_1678191401_f = Ti.Filesystem.getFile(ID_1678191401_d.resolve(), insp_r[0].foto2);
                if (ID_1678191401_f.exists() == true) {
                    foto_2 = ID_1678191401_f.read();
                }
                ID_1678191401_f = null;
            }
            ID_1678191401_d = null;
        } catch (e) {
            ID_1107845015_trycatch.error(e);
        }
        var ID_545308520_trycatch = {
            error: function(e) {
                if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
            }
        };
        try {
            ID_545308520_trycatch.error = function(evento) {};
            insp_r[0].foto3 = "mini" + insp_r[0].foto3.substring(3);
            var foto_3 = '';
            var ID_1187156852_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + seltarea.id_server);
            if (ID_1187156852_d.exists() == true) {
                var ID_1187156852_f = Ti.Filesystem.getFile(ID_1187156852_d.resolve(), insp_r[0].foto3);
                if (ID_1187156852_f.exists() == true) {
                    foto_3 = ID_1187156852_f.read();
                }
                ID_1187156852_f = null;
            }
            ID_1187156852_d = null;
        } catch (e) {
            ID_545308520_trycatch.error(e);
        }
    } else {
        var ID_660206607_trycatch = {
            error: function(e) {
                if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
            }
        };
        try {
            ID_660206607_trycatch.error = function(evento) {};
            insp_r[0].foto1 = "mini" + insp_r[0].foto1.substring(3);
            var foto_1 = '';
            var ID_1315028819_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + 0);
            if (ID_1315028819_d.exists() == true) {
                var ID_1315028819_f = Ti.Filesystem.getFile(ID_1315028819_d.resolve(), insp_r[0].foto1);
                if (ID_1315028819_f.exists() == true) {
                    foto_1 = ID_1315028819_f.read();
                }
                ID_1315028819_f = null;
            }
            ID_1315028819_d = null;
        } catch (e) {
            ID_660206607_trycatch.error(e);
        }
        var ID_878821508_trycatch = {
            error: function(e) {
                if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
            }
        };
        try {
            ID_878821508_trycatch.error = function(evento) {};
            insp_r[0].foto2 = "mini" + insp_r[0].foto2.substring(3);
            var foto_2 = '';
            var ID_1050482797_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + 0);
            if (ID_1050482797_d.exists() == true) {
                var ID_1050482797_f = Ti.Filesystem.getFile(ID_1050482797_d.resolve(), insp_r[0].foto2);
                if (ID_1050482797_f.exists() == true) {
                    foto_2 = ID_1050482797_f.read();
                }
                ID_1050482797_f = null;
            }
            ID_1050482797_d = null;
        } catch (e) {
            ID_878821508_trycatch.error(e);
        }
        var ID_1874717670_trycatch = {
            error: function(e) {
                if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
            }
        };
        try {
            ID_1874717670_trycatch.error = function(evento) {};
            insp_r[0].foto3 = "mini" + insp_r[0].foto3.substring(3);
            var foto_3 = '';
            var ID_1242935605_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + 0);
            if (ID_1242935605_d.exists() == true) {
                var ID_1242935605_f = Ti.Filesystem.getFile(ID_1242935605_d.resolve(), insp_r[0].foto3);
                if (ID_1242935605_f.exists() == true) {
                    foto_3 = ID_1242935605_f.read();
                }
                ID_1242935605_f = null;
            }
            ID_1242935605_d = null;
        } catch (e) {
            ID_1874717670_trycatch.error(e);
        }
    }
    if ((_.isObject(foto_1) || (_.isString(foto_1)) && !_.isEmpty(foto_1)) || _.isNumber(foto_1) || _.isBoolean(foto_1)) {
        /** 
         * Revisamos que foto1, foto2 y foto3 contengan texto, por lo que si es asi, modificamos la imagen previa y ponemos la foto miniatura obtenida desde la memoria 
         */
        var ID_949530997_visible = true;

        if (ID_949530997_visible == 'si') {
            ID_949530997_visible = true;
        } else if (ID_949530997_visible == 'no') {
            ID_949530997_visible = false;
        }
        $.ID_949530997.setVisible(ID_949530997_visible);

        var ID_806455898_imagen = foto_1;

        if (typeof ID_806455898_imagen == 'string' && 'styles' in require('a4w') && ID_806455898_imagen in require('a4w').styles['images']) {
            ID_806455898_imagen = require('a4w').styles['images'][ID_806455898_imagen];
        }
        $.ID_806455898.setImage(ID_806455898_imagen);

        if ((Ti.Platform.manufacturer) == 'samsung') {
            /** 
             * Revisamos si el equipo es samsung, si es asi, rotamos la imagen ya que es la unica marca que saca mal rotadas las imagenes 
             */
            var ID_949530997_rotar = 90;

            var setRotacion = function(angulo) {
                $.ID_949530997.setTransform(Ti.UI.create2DMatrix().rotate(angulo));
            };
            setRotacion(ID_949530997_rotar);

        }
    }
    if ((_.isObject(foto_2) || (_.isString(foto_2)) && !_.isEmpty(foto_2)) || _.isNumber(foto_2) || _.isBoolean(foto_2)) {
        var ID_66332676_visible = true;

        if (ID_66332676_visible == 'si') {
            ID_66332676_visible = true;
        } else if (ID_66332676_visible == 'no') {
            ID_66332676_visible = false;
        }
        $.ID_66332676.setVisible(ID_66332676_visible);

        var ID_598255384_imagen = foto_2;

        if (typeof ID_598255384_imagen == 'string' && 'styles' in require('a4w') && ID_598255384_imagen in require('a4w').styles['images']) {
            ID_598255384_imagen = require('a4w').styles['images'][ID_598255384_imagen];
        }
        $.ID_598255384.setImage(ID_598255384_imagen);

        if ((Ti.Platform.manufacturer) == 'samsung') {
            var ID_66332676_rotar = 90;

            var setRotacion = function(angulo) {
                $.ID_66332676.setTransform(Ti.UI.create2DMatrix().rotate(angulo));
            };
            setRotacion(ID_66332676_rotar);

        }
    }
    if ((_.isObject(foto_3) || (_.isString(foto_3)) && !_.isEmpty(foto_3)) || _.isNumber(foto_3) || _.isBoolean(foto_3)) {
        var ID_738207149_visible = true;

        if (ID_738207149_visible == 'si') {
            ID_738207149_visible = true;
        } else if (ID_738207149_visible == 'no') {
            ID_738207149_visible = false;
        }
        $.ID_738207149.setVisible(ID_738207149_visible);

        var ID_1792194859_imagen = foto_3;

        if (typeof ID_1792194859_imagen == 'string' && 'styles' in require('a4w') && ID_1792194859_imagen in require('a4w').styles['images']) {
            ID_1792194859_imagen = require('a4w').styles['images'][ID_1792194859_imagen];
        }
        $.ID_1792194859.setImage(ID_1792194859_imagen);

        if ((Ti.Platform.manufacturer) == 'samsung') {
            var ID_738207149_rotar = 90;

            var setRotacion = function(angulo) {
                $.ID_738207149.setTransform(Ti.UI.create2DMatrix().rotate(angulo));
            };
            setRotacion(ID_738207149_rotar);

        }
    }
    /** 
     * Limpiamos variables 
     */
    foto_1 = null;
    foto_2 = null;
    foto_3 = null;
    /** 
     * Modificamos en 0.1 segundos el color del statusbar 
     */
    var ID_1795638484_func = function() {
        var ID_1713672648_statusbar = '#7E6EE0';

        var setearStatusColor = function(ID_1713672648_statusbar) {
            if (OS_IOS) {
                if (ID_1713672648_statusbar == 'light' || ID_1713672648_statusbar == 'claro') {
                    $.ID_1713672648_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.LIGHT_CONTENT);
                } else if (ID_1713672648_statusbar == 'grey' || ID_1713672648_statusbar == 'gris' || ID_1713672648_statusbar == 'gray') {
                    $.ID_1713672648_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.GREY);
                } else if (ID_1713672648_statusbar == 'oscuro' || ID_1713672648_statusbar == 'dark') {
                    $.ID_1713672648_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.DEFAULT);
                }
            } else if (OS_ANDROID) {
                abx.setStatusbarColor(ID_1713672648_statusbar);
            }
        };
        setearStatusColor(ID_1713672648_statusbar);

    };
    var ID_1795638484 = setTimeout(ID_1795638484_func, 1000 * 0.1);
})();

function Postlayout_ID_1050296756(e) {

    e.cancelBubble = true;
    var elemento = e.source;
    var temp_idrecinto = ('temp_idrecinto' in require('vars')) ? require('vars')['temp_idrecinto'] : '';
    var ID_847965665_i = Alloy.createCollection('insp_itemdanos');
    var ID_847965665_i_where = 'id_recinto=\'' + temp_idrecinto + '\'';
    ID_847965665_i.fetch({
        query: 'SELECT * FROM insp_itemdanos WHERE id_recinto=\'' + temp_idrecinto + '\''
    });
    var cant_itemdanos = require('helper').query2array(ID_847965665_i);
    var alto = 55 * cant_itemdanos.length;
    var ID_1133111069_alto = alto;

    if (ID_1133111069_alto == '*') {
        ID_1133111069_alto = Ti.UI.FILL;
    } else if (!isNaN(ID_1133111069_alto)) {
        ID_1133111069_alto = ID_1133111069_alto + 'dp';
    }
    $.ID_1133111069.setHeight(ID_1133111069_alto);

    var ID_1932812212_func = function() {
        /** 
         * Una vez ya cargada la tarea, dejamos la variable limpia para poder abrir otras pantallas 
         */
        require('vars')['var_abriendo'] = '';
    };
    var ID_1932812212 = setTimeout(ID_1932812212_func, 1000 * 0.2);

}
if (OS_IOS || OS_ANDROID) {
    $.ID_1713672648.addEventListener('close', function() {
        $.destroy(); // cleanup bindings
        $.off(); //remove backbone events of this controller
        var _ev_tmp = null,
            _ev_rem = null;
        if (_my_events) {
            for (_ev_tmp in _my_events) {
                try {
                    if (_ev_tmp.indexOf('_web') != -1) {
                        Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    } else {
                        Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
                    }
                } catch (err10) {}
            }
            _my_events = null;
            //delete _my_events;
        }
        if (_out_vars) {
            for (_ev_tmp in _out_vars) {
                for (_ev_rem in _out_vars[_ev_tmp]._remove) {
                    try {
                        eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
                    } catch (_errt) {}
                }
                _out_vars[_ev_tmp] = null;
            }
            _ev_tmp = null;
            //delete _out_vars;
        }
    });
}
//$.ID_1713672648.open();