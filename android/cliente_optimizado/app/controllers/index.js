var _bind4section={};
var _list_templates={};

var _activity; 
if (OS_ANDROID) { _activity = $.ID_1575514239.activity; var abx = require('com.alcoapps.actionbarextras'); }
var _my_events = {}, _out_vars = {}, $item = {}, args = arguments[0] || {}; if ('__args' in args && '__id' in args['__args']) args.__id=args['__args'].__id; if ('__modelo' in args && _.keys(args.__modelo).length>0 && 'item' in $) { $.item.set(args.__modelo); $item = $.item.toJSON(); }
var _var_scopekey = 'ID_1575514239';
require('vars')[_var_scopekey]={};
if (OS_ANDROID) {
   $.ID_1575514239.addEventListener('open', function(e) {
   });
}

function Load_ID_487912265(e) {

e.cancelBubble=true;
var elemento=e.source;
var evento=e;
 elemento.start();

}
function Click_ID_524878625(e) {

e.cancelBubble=true;
var elemento=e.source;
Alloy.createController("parte1_index",{}).getView().open();

}
function Click_ID_884485678(e) {

e.cancelBubble=true;
var elemento=e.source;
Alloy.createController("passwordrecover_index",{}).getView().open();

}
function Touchstart_ID_879757922(e) {

e.cancelBubble=true;
var elemento=e.source;
$.ID_1990962553.setBackgroundColor('#f7ae57');

}
function Touchend_ID_1269184390(e) {

e.cancelBubble=true;
var elemento=e.source;
var ID_1990962553_estilo = 'fondoamarillo';

									var setEstilo = function(clase) {
										if ('styles' in require('a4w') && clase in require('a4w').styles['classes']) {
											try {
												$.ID_1990962553.applyProperties(require('a4w').styles['classes'][clase]);
											} catch(sete_err) {
											}
										}
									};setEstilo(ID_1990962553_estilo);

if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
/** 
* Revisamos si hay internet 
*/
var email;
email = $.ID_1477419901.getValue();

if ((_.isObject(email) || (_.isString(email)) &&  !_.isEmpty(email)) || _.isNumber(email) || _.isBoolean(email)) {
/** 
* Validamos que el mail, y password exista, consultamos al servidor los datos y si esta bien, ingresamos al menu con los casos. Ademas de almacenar el correo, nombre rut y fecha de nacimiento en el modelo usuario 
*/
var regemail = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
 var validaremail = regemail.test(email);
if (validaremail==true||validaremail=='true') {
var password;
password = $.ID_423384131.getValue();

if (_.isNumber(password.length) && _.isNumber(3) && password.length > 3) {
var urlcrawford = ('urlcrawford' in require('vars'))?require('vars')['urlcrawford']:'';
var uuid = Titanium.Platform.id;
var device_token = ('device_token' in require('vars'))?require('vars')['device_token']:'';
if (Ti.App.deployType != 'production') console.log('voy a enviar',{"dtoken":device_token,"uuid":uuid,"password":password,"mail":email});
require('vars')['correo_login']=email;
var ID_1941618008={};

ID_1941618008.success = function(e) {
var elemento=e, valor=e;
if ((_.isObject(elemento.nombre) || (_.isString(elemento.nombre)) &&  !_.isEmpty(elemento.nombre)) || _.isNumber(elemento.nombre) || _.isBoolean(elemento.nombre)) {
var ID_892567713_i=Alloy.Collections.usuario;
var sql = "DELETE FROM " + ID_892567713_i.config.adapter.collection_name;
var db = Ti.Database.open(ID_892567713_i.config.adapter.db_name);
db.execute(sql);
db.close();
sql=null;
db=null;
ID_892567713_i.trigger('delete');
var ID_1564480051_m=Alloy.Collections.usuario;
var ID_1564480051_fila = Alloy.createModel('usuario', {
birthdate : elemento.fecha_nacimiento,
name : String.format(L('x1445533071','%1$s %2$s %3$s'), (elemento.nombre)?elemento.nombre.toString():'',(elemento.apellido_paterno)?elemento.apellido_paterno.toString():'',(elemento.apellido_materno)?elemento.apellido_materno.toString():''),
rut : elemento.rut,
email : elemento.correo
}
 );
ID_1564480051_m.add(ID_1564480051_fila);
ID_1564480051_fila.save();
var ID_1440595422={};

ID_1440595422.success = function(e) {
var elemento=e, valor=e;
var ID_2060560600_i=Alloy.Collections.casos;
var sql = "DELETE FROM " + ID_2060560600_i.config.adapter.collection_name;
var db = Ti.Database.open(ID_2060560600_i.config.adapter.db_name);
db.execute(sql);
db.close();
sql=null;
db=null;
ID_2060560600_i.trigger('delete');
if (Ti.App.deployType != 'production') console.log('respuesta de siniestros',{"datos":elemento});
var insp_caso_index = 0;
_.each(elemento.inspecciones, function(insp_caso, insp_caso_pos, insp_caso_list) {
insp_caso_index += 1;
var ID_1934539342_m=Alloy.Collections.casos;
var ID_1934539342_fila = Alloy.createModel('casos', {
siniestro : insp_caso.siniestro,
tipotexto : insp_caso.tipo,
rut_inspector : insp_caso.datos_estado.rut_inspector,
documentos : JSON.stringify(insp_caso.datos_estado.documentos),
calificacion : insp_caso.datos_estado.calificacion,
compania : insp_caso.compania,
imagen_inspector : insp_caso.datos_estado.imagen_inspector,
legal : insp_caso.legal,
estimado : insp_caso.estimado,
inspector : insp_caso.datos_estado.inspector,
fecha_agendamiento : insp_caso.datos_estado.fecha_agendamiento,
realizado_fecha : insp_caso.realizado_fecha,
num_caso : insp_caso.datos_estado.num_caso,
estado : insp_caso.estado,
realizado_dias : insp_caso.realizado_dias,
estadotexto : insp_caso.estado,
tipo : insp_caso.tipo,
correo : insp_caso.datos_estado.correo,
descripcion : insp_caso.datos_estado.descripcion
}
 );
ID_1934539342_m.add(ID_1934539342_fila);
ID_1934539342_fila.save();
});
var ID_1708387698_visible = true;

								  if (ID_1708387698_visible=='si') {
									  ID_1708387698_visible=true;
								  } else if (ID_1708387698_visible=='no') {
									  ID_1708387698_visible=false;
								  }
								  $.ID_1708387698.setVisible(ID_1708387698_visible);

$.ID_1433580554.hide();
Alloy.createController("menu_index",{}).getView().open();
elemento=null, valor=null;
};

ID_1440595422.error = function(e) {
var elemento=e, valor=e;
var ID_1350131062_opts=[L('x3610695981_traducir','OK')];
var ID_1350131062 = Ti.UI.createAlertDialog({
   title: L('x3769205873_traducir','ALERT'),
   message: L('x3064151243_traducir','ERROR OBTAINING THE CASES'),
   buttonNames: ID_1350131062_opts
});
ID_1350131062.addEventListener('click', function(e) {
   var xd=ID_1350131062_opts[e.index];
xd = null;

   e.source.removeEventListener("click", arguments.callee);
});
ID_1350131062.show();
elemento=null, valor=null;
};
require('helper').ajaxUnico('ID_1440595422', ''+String.format(L('x4094662162','%1$ssiniestrosAsegurado/'), urlcrawford.toString())+'', 'POST', { correo:elemento.correo }, 15000, ID_1440595422);
}
 else {
var ID_627395636_opts=[L('x3610695981_traducir','OK')];
var ID_627395636 = Ti.UI.createAlertDialog({
   title: L('x3769205873_traducir','ALERT'),
   message: L('x3594877300_traducir','USER OR PASSWORD INVALID'),
   buttonNames: ID_627395636_opts
});
ID_627395636.addEventListener('click', function(e) {
   var xdx=ID_627395636_opts[e.index];
xdx = null;

   e.source.removeEventListener("click", arguments.callee);
});
ID_627395636.show();
$.ID_1433580554.hide();
var ID_1708387698_visible = true;

								  if (ID_1708387698_visible=='si') {
									  ID_1708387698_visible=true;
								  } else if (ID_1708387698_visible=='no') {
									  ID_1708387698_visible=false;
								  }
								  $.ID_1708387698.setVisible(ID_1708387698_visible);

}if (Ti.App.deployType != 'production') console.log('respuesta login',{"asd":elemento});
elemento=null, valor=null;
};

ID_1941618008.error = function(e) {
var elemento=e, valor=e;
var ID_10303137_opts=[L('x3610695981_traducir','OK')];
var ID_10303137 = Ti.UI.createAlertDialog({
   title: L('x3769205873_traducir','ALERT'),
   message: L('x904051258_traducir','ERROR CONNECTING'),
   buttonNames: ID_10303137_opts
});
ID_10303137.addEventListener('click', function(e) {
   var zxc=ID_10303137_opts[e.index];
zxc = null;

   e.source.removeEventListener("click", arguments.callee);
});
ID_10303137.show();
$.ID_1433580554.hide();
var ID_1708387698_visible = true;

								  if (ID_1708387698_visible=='si') {
									  ID_1708387698_visible=true;
								  } else if (ID_1708387698_visible=='no') {
									  ID_1708387698_visible=false;
								  }
								  $.ID_1708387698.setVisible(ID_1708387698_visible);

elemento=null, valor=null;
};
require('helper').ajaxUnico('ID_1941618008', ''+String.format(L('x3079903409','%1$slogin/'), urlcrawford.toString())+'', 'POST', { correo:email,password:password,uuid:uuid,device_token:device_token }, 15000, ID_1941618008);
$.ID_1433580554.show();
var ID_1708387698_visible = false;

								  if (ID_1708387698_visible=='si') {
									  ID_1708387698_visible=true;
								  } else if (ID_1708387698_visible=='no') {
									  ID_1708387698_visible=false;
								  }
								  $.ID_1708387698.setVisible(ID_1708387698_visible);

}
 else {
var ID_1597622241_opts=[L('x3610695981_traducir','OK')];
var ID_1597622241 = Ti.UI.createAlertDialog({
   title: L('x3769205873_traducir','ALERT'),
   message: L('x2728743456_traducir','PASSWORD TOO SHORT'),
   buttonNames: ID_1597622241_opts
});
ID_1597622241.addEventListener('click', function(e) {
   var zxc=ID_1597622241_opts[e.index];
zxc = null;

   e.source.removeEventListener("click", arguments.callee);
});
ID_1597622241.show();
}}
 else {
var ID_1602850567_opts=[L('x3610695981_traducir','OK')];
var ID_1602850567 = Ti.UI.createAlertDialog({
   title: L('x2861137601_traducir','ERROR'),
   message: L('x1647703686_traducir','INVALID EMAIL'),
   buttonNames: ID_1602850567_opts
});
ID_1602850567.addEventListener('click', function(e) {
   var xdd=ID_1602850567_opts[e.index];
xdd = null;

   e.source.removeEventListener("click", arguments.callee);
});
ID_1602850567.show();
}}
 else {
var ID_1459102278_opts=[L('x3610695981_traducir','OK')];
var ID_1459102278 = Ti.UI.createAlertDialog({
   title: L('x2861137601_traducir','ERROR'),
   message: L('x1676137616_traducir','EMPTY EMAIL'),
   buttonNames: ID_1459102278_opts
});
ID_1459102278.addEventListener('click', function(e) {
   var xd=ID_1459102278_opts[e.index];
xd = null;

   e.source.removeEventListener("click", arguments.callee);
});
ID_1459102278.show();
}}
 else {
var ID_1319157771_opts=[L('x3610695981_traducir','OK')];
var ID_1319157771 = Ti.UI.createAlertDialog({
   title: L('x3769205873_traducir','ALERT'),
   message: L('x1867626468_traducir','YOU DONT HAVE INTERNET CONNECTION'),
   buttonNames: ID_1319157771_opts
});
ID_1319157771.addEventListener('click', function(e) {
   var asd=ID_1319157771_opts[e.index];
asd = null;

   e.source.removeEventListener("click", arguments.callee);
});
ID_1319157771.show();
}
}

(function() {
/** 
* Cambiamos ancho de vista de registro 
*/
var ID_414134419_ancho = '-';

								  if (ID_414134419_ancho=='*') {
									  ID_414134419_ancho=Ti.UI.FILL;
								  } else if (ID_414134419_ancho=='-') {
									  ID_414134419_ancho=Ti.UI.SIZE;
								  } else if (!isNaN(ID_414134419_ancho)) {
									  ID_414134419_ancho=ID_414134419_ancho+'dp';
								  }
								  $.ID_414134419.setWidth(ID_414134419_ancho);

/** 
* Desenfocamos campos de texto 
*/
$.ID_1477419901.blur();
$.ID_423384131.blur();
/** 
* Ocultamos la barra que indica la posicion del scroll 
*/
$.ID_1351033224.setShowVerticalScrollIndicator(false);
})();

if (OS_IOS || OS_ANDROID) {
$.ID_1575514239.addEventListener('close',function(){
   $.destroy(); // cleanup bindings
   $.off(); //remove backbone events of this controller
   var _ev_tmp = null, _ev_rem = null;
   if (_my_events) {
      for(_ev_tmp in _my_events) { 
   		try {
   		    if (_ev_tmp.indexOf('_web')!=-1) {
   			   Ti.App.removeEventListener(_ev_tmp.split(',')[0],_my_events[_ev_tmp]);
   		    } else {
   			   Alloy.Events.off(_ev_tmp.split(',')[0],_my_events[_ev_tmp]);
   		    }
   		} catch(err10) {
   		}
      }
      _my_events = null;
      //delete _my_events;
   }
   if (_out_vars) {
      for(_ev_tmp in _out_vars) { 
         for (_ev_rem in _out_vars[_ev_tmp]._remove) {
            try {
 eval(_out_vars[_ev_tmp]._remove[_ev_rem]); 
 } catch(_errt) {}
         }
         _out_vars[_ev_tmp] = null;
      }
      _ev_tmp = null;
      //delete _out_vars;
   }
});
}
$.ID_1575514239.open();