var _bind4section={"titulo":"casos"};
var _list_templates={"casos1":{"ID_923484212":{"text":"{tipo}"},"ID_853575261":{},"ID_877203508":{"text":"{estado}"},"ID_247226025":{},"ID_555733476":{"text":"{rut_inspector}"},"ID_1048154264":{},"ID_1778865562":{},"ID_378618338":{},"ID_1058664237":{},"ID_882211798":{"text":"{siniestro}"},"ID_514163814":{"text":"{estadotexto}"},"ID_382088194":{"text":"{num_caso}"},"ID_544163570":{"text":"{imagen_inspector}"},"ID_322609251":{"text":"{realizado_dias}"},"ID_555212209":{},"ID_201785329":{"text":"{compania}"},"ID_1012523009":{},"ID_1642474614":{},"ID_494690396":{},"ID_764024818":{"text":"{realizado_fecha}"},"ID_429943992":{},"ID_48648805":{"text":"{fecha_agendamiento}"},"ID_622359318":{"text":"{inspector}"},"ID_1375133572":{"text":"{descripcion}"},"ID_1144123809":{"text":"{legal} "},"ID_1539314311":{"text":"{documentos}"},"ID_1542432033":{"text":"{tipotexto}"},"ID_149148931":{"text":"{calificacion}"},"ID_870355169":{},"ID_1162235703":{},"ID_1844210260":{"text":"{estimado} "},"ID_194254598":{},"ID_1767631682":{"text":"{correo}"}}};

var _activity; 
if (OS_ANDROID) { _activity = $.ID_1737225565.activity; var abx = require('com.alcoapps.actionbarextras'); }
var _my_events = {}, _out_vars = {}, $item = {}, args = arguments[0] || {}; if ('__args' in args && '__id' in args['__args']) args.__id=args['__args'].__id; if ('__modelo' in args && _.keys(args.__modelo).length>0 && 'item' in $) { $.item.set(args.__modelo); $item = $.item.toJSON(); }
var _var_scopekey = 'ID_1737225565';
require('vars')[_var_scopekey]={};
if (OS_ANDROID) {
   $.ID_1737225565.addEventListener('open', function(e) {
   });
}


var ID_1163770763_like = function(search) {
  if (typeof search !== 'string' || this === null) {return false; }
  search = search.replace(new RegExp("([\\.\\\\\\+\\*\\?\\[\\^\\]\\$\\(\\)\\{\\}\\=\\!\\<\\>\\|\\:\\-])", "g"), "\\$1");
  search = search.replace(/%/g, '.*').replace(/_/g, '.');
  return RegExp('^' + search + '$', 'gi').test(this);
};
var ID_1163770763_filter = function(coll) {
var filtered = _.toArray(coll.filter(function(m) { return true; }));
return filtered;
};
var ID_1163770763_update = function(e) {
};
_.defer(function() { 
 Alloy.Collections.casos.fetch(); 
 });
Alloy.Collections.casos.on('add change delete', function(ee) { ID_1163770763_update(ee); });
var ID_1163770763_transform = function(model) {
var fila = model.toJSON();
if (fila.tipotexto==1||fila.tipotexto=='1') {
/** 
* Revisamos cada caso en la tabla... Definiendo el tipo y estado, le agregamos la funcionalidad para i18n 
*/
var home = L('x3521422318_traducir','Home');
fila.tipotexto = home;
}
 else if (fila.tipotexto==2) {
var car = L('x1332781181_traducir','Car');
 fila.tipotexto = car;
} else if (fila.tipotexto==3) {
var fraud = L('x2166733574_traducir','Fraud');
 fila.tipotexto = fraud;
}if (fila.estadotexto==1||fila.estadotexto=='1') {
var phase = L('x1887238607_traducir','Phase');
fila.estadotexto = phase;
}
 else if (fila.estadotexto==2) {
var assignment = L('x2140686186_traducir','Assignment');
fila.estadotexto = assignment;
} else if (fila.estadotexto==3) {
var inspection = L('x3064739669_traducir','Inspection');
fila.estadotexto = inspection;
} else if (fila.estadotexto==4) {
var evaluating = L('x1018769216_traducir','Evaluating');
fila.estadotexto = evaluating;
} else if (fila.estadotexto==5) {
var end = L('x951154001_traducir','End');
fila.estadotexto = end;
}return fila;
};
Alloy.Collections.casos.fetch();

function Itemclick_ID_1481600886(e) {

e.cancelBubble=true;
var objeto=e.section.getItemAt(e.itemIndex); var modelo={}, _modelo=[];
var fila={}, fila_bak={}, info={ _template:objeto.template, _what:[], _seccion_ref:e.section.getHeaderTitle(), _model_id:-1 }, _tmp={ objmap:{} };
if ('itemId' in e) {
   info._model_id=e.itemId;
   modelo._id=info._model_id;
   if (info._seccion_ref!='' && info._seccion_ref in _bind4section) {
      modelo._collection=_bind4section[info._seccion_ref];
      _tmp._coll=modelo._collection;
   }
}
var findVariables = require('fvariables');
_.each(_list_templates[info._template], function(obj_id, id) {
   _.each(obj_id, function(valor, prop) {
      var llaves = findVariables(valor,'{','}');
         _.each(llaves, function(llave) {
            _tmp.objmap[llave] = { id:id, prop:prop };
            fila[llave] = objeto[id][prop];
            if (id==e.bindId) info._what.push(llave);
         });
   });
});
info._what = info._what.join(',');
fila_bak = JSON.parse(JSON.stringify(fila));
if (Ti.App.deployType != 'production') console.log('Click en listado',{"detalle":fila});
if (fila.tipo==1||fila.tipo=='1') {
/** 
* Dependiendo del tipo de caso...
* 1. hogar
* 2. fraude
* 3.&#160;auto 
*/
if (fila.estado==1||fila.estado=='1') {
/** 
* Dependiendo del estado de caso...
* 1. Caso recibido
* 2. Datos de agendamiento
* 3.&#160;Calificacion del inspector
* 4. Documentos
* 5. Inspeccion finalizada 
*/
Alloy.createController("hogar1_index",{'_data' : fila,'__master_model' : (typeof modelo!=='undefined')?modelo:{},'__modelo': (typeof _modelo!=='undefined' && _modelo.length>0)?_modelo[0]:{}}).getView().open();
}
 else if (fila.estado==2) {
Alloy.createController("hogar2_index",{'_data' : fila,'__master_model' : (typeof modelo!=='undefined')?modelo:{},'__modelo': (typeof _modelo!=='undefined' && _modelo.length>0)?_modelo[0]:{}}).getView().open();
} else if (fila.estado==3) {
Alloy.createController("hogar3_index",{'_data' : fila,'__master_model' : (typeof modelo!=='undefined')?modelo:{},'__modelo': (typeof _modelo!=='undefined' && _modelo.length>0)?_modelo[0]:{}}).getView().open();
} else if (fila.estado==4) {
Alloy.createController("hogar4_index",{'_data' : fila,'__master_model' : (typeof modelo!=='undefined')?modelo:{},'__modelo': (typeof _modelo!=='undefined' && _modelo.length>0)?_modelo[0]:{}}).getView().open();
} else if (fila.estado==5) {
Alloy.createController("hogar5_index",{'_data' : fila,'__master_model' : (typeof modelo!=='undefined')?modelo:{},'__modelo': (typeof _modelo!=='undefined' && _modelo.length>0)?_modelo[0]:{}}).getView().open();
}}
 else if (fila.tipo==2) {
if (fila.estado==1||fila.estado=='1') {
Alloy.createController("fraude1_index",{'_data' : fila,'__master_model' : (typeof modelo!=='undefined')?modelo:{},'__modelo': (typeof _modelo!=='undefined' && _modelo.length>0)?_modelo[0]:{}}).getView().open();
}
 else if (fila.estado==2) {
Alloy.createController("fraude2_index",{'_data' : fila,'__master_model' : (typeof modelo!=='undefined')?modelo:{},'__modelo': (typeof _modelo!=='undefined' && _modelo.length>0)?_modelo[0]:{}}).getView().open();
} else if (fila.estado==3) {
Alloy.createController("fraude3_index",{'_data' : fila,'__master_model' : (typeof modelo!=='undefined')?modelo:{},'__modelo': (typeof _modelo!=='undefined' && _modelo.length>0)?_modelo[0]:{}}).getView().open();
}} else if (fila.tipo==3) {
if (fila.estado==1||fila.estado=='1') {
Alloy.createController("auto1_index",{'_data' : fila,'__master_model' : (typeof modelo!=='undefined')?modelo:{},'__modelo': (typeof _modelo!=='undefined' && _modelo.length>0)?_modelo[0]:{}}).getView().open();
}
 else if (fila.estado==2) {
Alloy.createController("auto2_index",{'_data' : fila,'__master_model' : (typeof modelo!=='undefined')?modelo:{},'__modelo': (typeof _modelo!=='undefined' && _modelo.length>0)?_modelo[0]:{}}).getView().open();
} else if (fila.estado==3) {
Alloy.createController("auto3_index",{'_data' : fila,'__master_model' : (typeof modelo!=='undefined')?modelo:{},'__modelo': (typeof _modelo!=='undefined' && _modelo.length>0)?_modelo[0]:{}}).getView().open();
}}_tmp.changed = false; _tmp.diff_keys = [];
_.each(fila, function(value1,prop) {
   var had_samekey = false;
   _.each(fila_bak, function(value2,prop2) {
      if (prop==prop2 && value1==value2) {
         had_samekey=true;
      } else if (!_.has(fila_bak,prop) || !_.has(fila,prop2)) {
         has_samekey=true;
      }
   });
   if (!had_samekey) _tmp.diff_keys.push(prop);
});
if (_tmp.diff_keys.length>0) _tmp.changed=true;
if (_tmp.changed==true) {
   _.each(_tmp.diff_keys, function(llave) {
      objeto[_tmp.objmap[llave].id][_tmp.objmap[llave].prop] = fila[llave];
   });
   e.section.updateItemAt(e.itemIndex, objeto);
}

}

(function() {
/** 
* Recuperamos la url de crawford 
*/
var urlcrawford = ('urlcrawford' in require('vars'))?require('vars')['urlcrawford']:'';
var cantbanners = ('cantbanners' in require('vars'))?require('vars')['cantbanners']:'';
var ID_623417874={};
ID_623417874.success = function(e) {
var elemento=e, valor=e;
var hola_index = 0;
_.each(elemento.imagenes, function(hola, hola_pos, hola_list) {
hola_index += 1;
if (OS_IOS) {
var ID_1607356917 = Ti.UI.createImageView({
height : Ti.UI.FILL,
width : Ti.UI.FILL,
image : hola.url_imagen,
id : ID_1607356917
}
);
} else {
var ID_1607356917 = Ti.UI.createImageView({
height : Ti.UI.FILL,
width : Ti.UI.FILL,
image : hola.url_imagen,
id : ID_1607356917
}
);
}
require('vars')['_ID_1607356917_original_']=ID_1607356917.getImage();
require('vars')['_ID_1607356917_filtro_']='original';
ID_1607356917.addEventListener('click',function(e) {
e.cancelBubble=true;
var elemento=e.source;
var evento=e;
 Ti.Platform.openURL(hola.link);
});
$.ID_1353765508.addView(ID_1607356917);require('vars')['cantbanners']=hola_pos+1;
});
elemento=null, valor=null;
};
ID_623417874.error = function(e) {
var elemento=e, valor=e;
var ID_790041558_opts=[L('x3610695981_traducir','OK')];
var ID_790041558 = Ti.UI.createAlertDialog({
   title: L('x3769205873_traducir','ALERT'),
   message: L('x3455773058_traducir','ERROR OBTAINING THE BANNERS'),
   buttonNames: ID_790041558_opts
});
ID_790041558.addEventListener('click', function(e) {
   var xd=ID_790041558_opts[e.index];
xd = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_790041558.show();
elemento=null, valor=null;
};
require('helper').ajaxUnico('ID_623417874', ''+String.format(L('x2232578092','%1$sbanners/'), urlcrawford.toString())+'', 'POST', { pais:'Chile' }, 15000, ID_623417874);
/** 
* Cada 5 segundos desliza automaticamente las imagenes en el banner 
*/
var carrusel = 0;
var ID_1856920329_continuar = true;
_out_vars['ID_1856920329'] = { _remove:["clearTimeout(_out_vars['ID_1856920329']._run)"] };
var ID_1856920329_func = function() {
carrusel = carrusel+1;
var cantbanners = ('cantbanners' in require('vars'))?require('vars')['cantbanners']:'';
if (carrusel%cantbanners!=0&&carrusel%cantbanners!='0') {
$.ID_1353765508.moveNext();
}
 else {
$.ID_1353765508.scrollToView(0);
}if (ID_1856920329_continuar==true) {
   _out_vars['ID_1856920329']._run = setTimeout(ID_1856920329_func, 1000*5);
}
};
_out_vars['ID_1856920329']._run = setTimeout(ID_1856920329_func, 1000*5);
var days = L('x1272337240_traducir','Days');
/** 
* Cada una hora refrescamos los casos que tiene el usuario 
*/
var tareas = 0;
var ID_1726701770_continuar = true;
_out_vars['ID_1726701770'] = { _remove:["clearTimeout(_out_vars['ID_1726701770']._run)"] };
var ID_1726701770_func = function() {
tareas = tareas+1;
/** 
* Limpiamos los casos 
*/
/** 
* Limpiamos los casos 
*/
var ID_499911396_i=Alloy.Collections.casos;
var sql = "DELETE FROM " + ID_499911396_i.config.adapter.collection_name;
var db = Ti.Database.open(ID_499911396_i.config.adapter.db_name);
db.execute(sql);
db.close();
sql=null;
db=null;
ID_499911396_i.trigger('delete');
/** 
* Consultamos a traves del mail del usuario los siniestros agendados 
*/
var ID_842823704_i=Alloy.createCollection('usuario');
var ID_842823704_i_where='';
ID_842823704_i.fetch();
var user=require('helper').query2array(ID_842823704_i);
if (Ti.App.deployType != 'production') console.log('usuario',{"asd":user});
var ID_1941841960={};
ID_1941841960.success = function(e) {
var elemento=e, valor=e;
var ID_1600668505_i=Alloy.Collections.casos;
var sql = "DELETE FROM " + ID_1600668505_i.config.adapter.collection_name;
var db = Ti.Database.open(ID_1600668505_i.config.adapter.db_name);
db.execute(sql);
db.close();
sql=null;
db=null;
ID_1600668505_i.trigger('delete');
if (Ti.App.deployType != 'production') console.log('respuesta de siniestros',{"datos":elemento});
var insp_caso_index = 0;
_.each(elemento.inspecciones, function(insp_caso, insp_caso_pos, insp_caso_list) {
insp_caso_index += 1;
var ID_1199177641_m=Alloy.Collections.casos;
var ID_1199177641_fila = Alloy.createModel('casos', {
siniestro : insp_caso.siniestro,
tipotexto : insp_caso.tipotexto,
rut_inspector : insp_caso.datos_estado.rut_inspector,
documentos : JSON.stringify(insp_caso.datos_estado.documentos),
calificacion : insp_caso.datos_estado.calificacion,
compania : insp_caso.compania,
imagen_inspector : insp_caso.datos_estado.imagen_inspector,
legal : insp_caso.legal,
estimado : insp_caso.estimado,
inspector : insp_caso.datos_estado.inspector,
fecha_agendamiento : insp_caso.datos_estado.fecha_agendamiento,
realizado_fecha : insp_caso.realizado_fecha,
num_caso : insp_caso.datos_estado.num_caso,
estado : insp_caso.estado,
realizado_dias : insp_caso.realizado_dias,
estadotexto : insp_caso.estadotexto,
tipo : insp_caso.tipo,
correo : insp_caso.datos_estado.correo,
descripcion : insp_caso.datos_estado.descripcion
}
 );
ID_1199177641_m.add(ID_1199177641_fila);
ID_1199177641_fila.save();
_.defer(function() { 
Alloy.Collections.casos.fetch();
});
});
elemento=null, valor=null;
};
ID_1941841960.error = function(e) {
var elemento=e, valor=e;
var ID_1083293068_opts=[L('x3610695981_traducir','OK')];
var ID_1083293068 = Ti.UI.createAlertDialog({
   title: L('x3769205873_traducir','ALERT'),
   message: L('x3064151243_traducir','ERROR OBTAINING THE CASES'),
   buttonNames: ID_1083293068_opts
});
ID_1083293068.addEventListener('click', function(e) {
   var xd=ID_1083293068_opts[e.index];
xd = null;
   e.source.removeEventListener("click", arguments.callee);
});
ID_1083293068.show();
elemento=null, valor=null;
};
require('helper').ajaxUnico('ID_1941841960', ''+String.format(L('x4094662162','%1$ssiniestrosAsegurado/'), urlcrawford.toString())+'', 'POST', { correo:elemento.correo }, 15000, ID_1941841960);
if (ID_1726701770_continuar==true) {
   _out_vars['ID_1726701770']._run = setTimeout(ID_1726701770_func, 1000*3600);
}
};
_out_vars['ID_1726701770']._run = setTimeout(ID_1726701770_func, 1000*3600);
})();


_my_events['refrescar_casos,ID_1875922489'] = function(evento) {
_.defer(function() { 
 Alloy.Collections.casos.fetch(); 
 });
if (Ti.App.deployType != 'production') console.log('casos refrescasos',{});
};
Alloy.Events.on('refrescar_casos', _my_events['refrescar_casos,ID_1875922489']);

if (OS_IOS || OS_ANDROID) {
$.ID_1737225565.addEventListener('close',function(){
   $.destroy(); // cleanup bindings
   $.off(); //remove backbone events of this controller
   var _ev_tmp = null, _ev_rem = null;
   if (_my_events) {
      for(_ev_tmp in _my_events) { 
   		try {
   		    if (_ev_tmp.indexOf('_web')!=-1) {
   			   Ti.App.removeEventListener(_ev_tmp.split(',')[0],_my_events[_ev_tmp]);
   		    } else {
   			   Alloy.Events.off(_ev_tmp.split(',')[0],_my_events[_ev_tmp]);
   		    }
   		} catch(err10) {
   		}
      }
      _my_events = null;
      //delete _my_events;
   }
   if (_out_vars) {
      for(_ev_tmp in _out_vars) { 
         for (_ev_rem in _out_vars[_ev_tmp]._remove) {
            try {
 eval(_out_vars[_ev_tmp]._remove[_ev_rem]); 
 } catch(_errt) {}
         }
         _out_vars[_ev_tmp] = null;
      }
      _ev_tmp = null;
      //delete _out_vars;
   }
});
}