/** 
* Widget Caja descripcion
* Permite mostrar al usuario una caja con texto descriptorio
* 
* @params titulo Titulo de la descripcion
* @params descripcion Texto de la descripcion en si
* @params valor Cambia el texto que tiene en la descripcion 
*/
var _bind4section={};

var args = arguments[0] || {};


$.init = function(params) {
   for (var tobe in params) args[tobe] = params[tobe];
   if ('__id' in params) require(WPATH('vars'))[params.__id]={};
if (!_.isUndefined(params.titulo)) {
$.ID_1392517248.setText(params.titulo);

}
 else if (!_.isUndefined(params.descripcion)) {
$.ID_1731020866.setText(params.descripcion);

}};


$.texto = function(params) {
if (!_.isUndefined(params.valor)) {
$.ID_1731020866.setText(params.valor);

}
};