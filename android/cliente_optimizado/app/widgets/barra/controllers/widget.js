/** 
* Widget Barra
* La funcion de este widget es crear una vista que parezca una barra. Posee textos que son utilizados a lo largo del proyecto
* 
* @params titulo Texto que se mostrara en la barra
* @params fondo Define el color de fondo que tendra la barra
* @params modal Mueve el titulo hacia la izquierda
* @params nroinicial Indica la posicion en la que esta el usuario en cierta seccion
* @params nrofinal Indica la totalidad de pantallas que tiene cierta seccion 
*/
var _bind4section={};

var args = arguments[0] || {};


$.init = function(params) {
   for (var tobe in params) args[tobe] = params[tobe];
   if ('__id' in params) require(WPATH('vars'))[params.__id]={};
if (!_.isUndefined(params.titulo)) {
$.ID_56241598.setText(params.titulo);

}
if (!_.isUndefined(params.fondo)) {
var ID_748382869_estilo = params.fondo;

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_748382869.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_748382869_estilo);

}
if (!_.isUndefined(params.modal)) {
$.ID_56241598.setLeft(20);

}
if (!_.isUndefined(params.nroinicial)) {
$.ID_1631729973.setText(params.nroinicial);

var ID_569043732_visible = true;

										  if (ID_569043732_visible=='si') {
											  ID_569043732_visible=true;
										  } else if (ID_569043732_visible=='no') {
											  ID_569043732_visible=false;
										  }
										  $.ID_569043732.setVisible(ID_569043732_visible);

}
if (!_.isUndefined(params.nrofinal)) {
$.ID_1484477318.setText(params.nrofinal);

var ID_569043732_visible = true;

										  if (ID_569043732_visible=='si') {
											  ID_569043732_visible=true;
										  } else if (ID_569043732_visible=='no') {
											  ID_569043732_visible=false;
										  }
										  $.ID_569043732.setVisible(ID_569043732_visible);

}
};