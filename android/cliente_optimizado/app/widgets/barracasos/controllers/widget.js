/** 
* Widget Barra de casos
* Permite mostrar al usuario en que parte del proceso se encuentra actualmente
* 
* @params paso Para poder indicarle al usuario en que paso se encuentra actualmente
* @params titulo1 Modifica el texto para ponerle el nombre del paso
* @params titulo2 Modifica el texto para ponerle el nombre del paso
* @params titulo3 Modifica el texto para ponerle el nombre del paso
* @params titulo4 Modifica el texto para ponerle el nombre del paso
* @params titulo5 Modifica el texto para ponerle el nombre del paso 
*/
var _bind4section={};

var args = arguments[0] || {};


$.init = function(params) {
   for (var tobe in params) args[tobe] = params[tobe];
   if ('__id' in params) require(WPATH('vars'))[params.__id]={};
if (params.paso==1||params.paso=='1') {
var ID_1251042013_imagen = '1on';

													if (typeof ID_1251042013_imagen == 'string' && 'styles' in require(WPATH('a4w')) && ID_1251042013_imagen in require(WPATH('a4w')).styles['images']) {
														ID_1251042013_imagen = WPATH(require(WPATH('a4w')).styles['images'][ID_1251042013_imagen]);
													}
													$.ID_1251042013.setImage(ID_1251042013_imagen);

$.ID_2137562978.setColor('#000000');

}
if (params.paso==2) {
var ID_1251042013_imagen = '1off';

													if (typeof ID_1251042013_imagen == 'string' && 'styles' in require(WPATH('a4w')) && ID_1251042013_imagen in require(WPATH('a4w')).styles['images']) {
														ID_1251042013_imagen = WPATH(require(WPATH('a4w')).styles['images'][ID_1251042013_imagen]);
													}
													$.ID_1251042013.setImage(ID_1251042013_imagen);

var ID_1444643858_estilo = 'fondoazul';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_1444643858.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_1444643858_estilo);

var ID_1878478714_estilo = 'fondoazul';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_1878478714.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_1878478714_estilo);

var ID_1062765772_imagen = '2on';

													if (typeof ID_1062765772_imagen == 'string' && 'styles' in require(WPATH('a4w')) && ID_1062765772_imagen in require(WPATH('a4w')).styles['images']) {
														ID_1062765772_imagen = WPATH(require(WPATH('a4w')).styles['images'][ID_1062765772_imagen]);
													}
													$.ID_1062765772.setImage(ID_1062765772_imagen);

$.ID_1593739420.setColor('#000000');

}
if (params.paso==3) {
var ID_1251042013_imagen = '1off';

													if (typeof ID_1251042013_imagen == 'string' && 'styles' in require(WPATH('a4w')) && ID_1251042013_imagen in require(WPATH('a4w')).styles['images']) {
														ID_1251042013_imagen = WPATH(require(WPATH('a4w')).styles['images'][ID_1251042013_imagen]);
													}
													$.ID_1251042013.setImage(ID_1251042013_imagen);

var ID_1062765772_imagen = '2off';

													if (typeof ID_1062765772_imagen == 'string' && 'styles' in require(WPATH('a4w')) && ID_1062765772_imagen in require(WPATH('a4w')).styles['images']) {
														ID_1062765772_imagen = WPATH(require(WPATH('a4w')).styles['images'][ID_1062765772_imagen]);
													}
													$.ID_1062765772.setImage(ID_1062765772_imagen);

var ID_1565588664_imagen = '3on';

													if (typeof ID_1565588664_imagen == 'string' && 'styles' in require(WPATH('a4w')) && ID_1565588664_imagen in require(WPATH('a4w')).styles['images']) {
														ID_1565588664_imagen = WPATH(require(WPATH('a4w')).styles['images'][ID_1565588664_imagen]);
													}
													$.ID_1565588664.setImage(ID_1565588664_imagen);

var ID_1444643858_estilo = 'fondoazul';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_1444643858.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_1444643858_estilo);

var ID_1878478714_estilo = 'fondoazul';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_1878478714.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_1878478714_estilo);

var ID_1319612832_estilo = 'fondoazul';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_1319612832.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_1319612832_estilo);

var ID_1109145780_estilo = 'fondoazul';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_1109145780.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_1109145780_estilo);

$.ID_1927191340.setColor('#000000');

}
if (params.paso==4) {
var ID_1251042013_imagen = '1off';

													if (typeof ID_1251042013_imagen == 'string' && 'styles' in require(WPATH('a4w')) && ID_1251042013_imagen in require(WPATH('a4w')).styles['images']) {
														ID_1251042013_imagen = WPATH(require(WPATH('a4w')).styles['images'][ID_1251042013_imagen]);
													}
													$.ID_1251042013.setImage(ID_1251042013_imagen);

var ID_1062765772_imagen = '2off';

													if (typeof ID_1062765772_imagen == 'string' && 'styles' in require(WPATH('a4w')) && ID_1062765772_imagen in require(WPATH('a4w')).styles['images']) {
														ID_1062765772_imagen = WPATH(require(WPATH('a4w')).styles['images'][ID_1062765772_imagen]);
													}
													$.ID_1062765772.setImage(ID_1062765772_imagen);

var ID_1565588664_imagen = '3off';

													if (typeof ID_1565588664_imagen == 'string' && 'styles' in require(WPATH('a4w')) && ID_1565588664_imagen in require(WPATH('a4w')).styles['images']) {
														ID_1565588664_imagen = WPATH(require(WPATH('a4w')).styles['images'][ID_1565588664_imagen]);
													}
													$.ID_1565588664.setImage(ID_1565588664_imagen);

var ID_1778314790_imagen = '4on';

													if (typeof ID_1778314790_imagen == 'string' && 'styles' in require(WPATH('a4w')) && ID_1778314790_imagen in require(WPATH('a4w')).styles['images']) {
														ID_1778314790_imagen = WPATH(require(WPATH('a4w')).styles['images'][ID_1778314790_imagen]);
													}
													$.ID_1778314790.setImage(ID_1778314790_imagen);

var ID_1444643858_estilo = 'fondoazul';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_1444643858.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_1444643858_estilo);

var ID_1878478714_estilo = 'fondoazul';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_1878478714.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_1878478714_estilo);

var ID_1109145780_estilo = 'fondoazul';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_1109145780.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_1109145780_estilo);

var ID_1319612832_estilo = 'fondoazul';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_1319612832.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_1319612832_estilo);

var ID_1962540837_estilo = 'fondoazul';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_1962540837.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_1962540837_estilo);

var ID_1548105713_estilo = 'fondoazul';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_1548105713.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_1548105713_estilo);

$.ID_1779340269.setColor('#000000');

}
if (params.paso==5) {
var ID_1251042013_imagen = '1off';

													if (typeof ID_1251042013_imagen == 'string' && 'styles' in require(WPATH('a4w')) && ID_1251042013_imagen in require(WPATH('a4w')).styles['images']) {
														ID_1251042013_imagen = WPATH(require(WPATH('a4w')).styles['images'][ID_1251042013_imagen]);
													}
													$.ID_1251042013.setImage(ID_1251042013_imagen);

var ID_1062765772_imagen = '2off';

													if (typeof ID_1062765772_imagen == 'string' && 'styles' in require(WPATH('a4w')) && ID_1062765772_imagen in require(WPATH('a4w')).styles['images']) {
														ID_1062765772_imagen = WPATH(require(WPATH('a4w')).styles['images'][ID_1062765772_imagen]);
													}
													$.ID_1062765772.setImage(ID_1062765772_imagen);

var ID_1565588664_imagen = '3off';

													if (typeof ID_1565588664_imagen == 'string' && 'styles' in require(WPATH('a4w')) && ID_1565588664_imagen in require(WPATH('a4w')).styles['images']) {
														ID_1565588664_imagen = WPATH(require(WPATH('a4w')).styles['images'][ID_1565588664_imagen]);
													}
													$.ID_1565588664.setImage(ID_1565588664_imagen);

var ID_1778314790_imagen = '4off';

													if (typeof ID_1778314790_imagen == 'string' && 'styles' in require(WPATH('a4w')) && ID_1778314790_imagen in require(WPATH('a4w')).styles['images']) {
														ID_1778314790_imagen = WPATH(require(WPATH('a4w')).styles['images'][ID_1778314790_imagen]);
													}
													$.ID_1778314790.setImage(ID_1778314790_imagen);

var ID_800179407_imagen = '5on';

													if (typeof ID_800179407_imagen == 'string' && 'styles' in require(WPATH('a4w')) && ID_800179407_imagen in require(WPATH('a4w')).styles['images']) {
														ID_800179407_imagen = WPATH(require(WPATH('a4w')).styles['images'][ID_800179407_imagen]);
													}
													$.ID_800179407.setImage(ID_800179407_imagen);

var ID_1444643858_estilo = 'fondoazul';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_1444643858.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_1444643858_estilo);

var ID_1878478714_estilo = 'fondoazul';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_1878478714.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_1878478714_estilo);

var ID_1109145780_estilo = 'fondoazul';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_1109145780.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_1109145780_estilo);

var ID_1962540837_estilo = 'fondoazul';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_1962540837.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_1962540837_estilo);

var ID_1548105713_estilo = 'fondoazul';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_1548105713.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_1548105713_estilo);

var ID_1855025197_estilo = 'fondoazul';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_1855025197.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_1855025197_estilo);

var ID_68533462_estilo = 'fondoazul';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_68533462.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_68533462_estilo);

var ID_1319612832_estilo = 'fondoazul';

				var setEstilo = function(clase) {
					if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
						try {
							$.ID_1319612832.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
						} catch(sete_err) {
						}
					}
				};setEstilo(ID_1319612832_estilo);

$.ID_1055421960.setColor('#000000');

}
if (!_.isUndefined(params.titulo1)) {
$.ID_2137562978.setText(params.titulo1);

}
if (!_.isUndefined(params.titulo2)) {
$.ID_1593739420.setText(params.titulo2);

}
if (!_.isUndefined(params.titulo3)) {
$.ID_1927191340.setText(params.titulo3);

}
if (!_.isUndefined(params.titulo4)) {
$.ID_1779340269.setText(params.titulo4);

}
if (!_.isUndefined(params.titulo5)) {
$.ID_1055421960.setText(params.titulo5);

}
};