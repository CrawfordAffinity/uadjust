/** 
 * Widget Pelota Dias
 * Este widget tiene como objetivo mostrar un boton en forma de circulo que se activa y desactiva al ser pulsado para representar un dia de la semana.
 * 
 * @param letra Representa la letra a mostrar dentro del circulo
 * @param radio Indica el ancho del radio en pixeles
 * @param borde Indica el grosor del borde en pixeles
 * @param estado True indica que estado inicial es prendido, false en caso contrario
 * @param on Alias de estado 
 */
var _bind4section = {};

var args = arguments[0] || {};

function Touchend_vista(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var on = ('on' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['on'] : '';
	if (on == true || on == 'true') {
		var vista_estilo = 'fondoplomo';

		var setEstilo = function(clase) {
			if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
				try {
					$.vista.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
				} catch (sete_err) {}
			}
		};
		setEstilo(vista_estilo);

		var label_estilo = 'estilo11';

		if (typeof WPATH != 'undefined') {
			var _tmp_a4w = require(WPATH('a4w'));
		} else {
			var _tmp_a4w = require('a4w');
		}
		if ((typeof label_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (label_estilo in _tmp_a4w.styles['classes'])) {
			try {
				label_estilo = _tmp_a4w.styles['classes'][label_estilo];
			} catch (st_val_err) {}
		}
		$.label.applyProperties(label_estilo);

		require(WPATH('vars'))[args.__id]['on'] = 'false';

		if ('__args' in args) {
			args['__args'].onoff({});
		} else {
			args.onoff({});
		}
	} else {
		var vista_estilo = 'fondoazul';

		var setEstilo = function(clase) {
			if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
				try {
					$.vista.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
				} catch (sete_err) {}
			}
		};
		setEstilo(vista_estilo);

		var label_estilo = 'estilo10';

		if (typeof WPATH != 'undefined') {
			var _tmp_a4w = require(WPATH('a4w'));
		} else {
			var _tmp_a4w = require('a4w');
		}
		if ((typeof label_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (label_estilo in _tmp_a4w.styles['classes'])) {
			try {
				label_estilo = _tmp_a4w.styles['classes'][label_estilo];
			} catch (st_val_err) {}
		}
		$.label.applyProperties(label_estilo);

		require(WPATH('vars'))[args.__id]['on'] = 'true';

		if ('__args' in args) {
			args['__args'].onon({});
		} else {
			args.onon({});
		}
	}
}
/** 
 * Funcion que inicializa el widget 
 */

$.init = function(params) {
	for (var tobe in params) args[tobe] = params[tobe];
	if ('__id' in params) require(WPATH('vars'))[params.__id] = {};
	/** 
	 * Definimos los valores por default 
	 */
	require(WPATH('vars'))[args.__id]['borde'] = 2;
	require(WPATH('vars'))[args.__id]['on'] = 'false';
	require(WPATH('vars'))[args.__id]['radio'] = 12;
	if (!_.isUndefined(params.letra)) {
		/** 
		 * Revisamos parametros obligatorios 
		 */
		$.label.setText(params.letra);

	}
	if (!_.isUndefined(params.radio)) {
		require(WPATH('vars'))[args.__id]['radio'] = params.radio;
	}
	if (!_.isUndefined(params.borde)) {
		require(WPATH('vars'))[args.__id]['borde'] = params.borde;
	}
	if (!_.isUndefined(params.estado)) {
		require(WPATH('vars'))[args.__id]['on'] = params.estado;
	}
	if (!_.isUndefined(params.on)) {
		require(WPATH('vars'))[args.__id]['on'] = params.on;
	}
	var borde = ('borde' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['borde'] : '';
	/** 
	 * Recuperamos variable on, si es true cambiamos color de vista y texto 
	 */
	var on = ('on' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['on'] : '';
	if (on == true || on == 'true') {
		var vista_estilo = 'fondoazul';

		var setEstilo = function(clase) {
			if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
				try {
					$.vista.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
				} catch (sete_err) {}
			}
		};
		setEstilo(vista_estilo);

		var label_estilo = 'estilo10';

		if (typeof WPATH != 'undefined') {
			var _tmp_a4w = require(WPATH('a4w'));
		} else {
			var _tmp_a4w = require('a4w');
		}
		if ((typeof label_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (label_estilo in _tmp_a4w.styles['classes'])) {
			try {
				label_estilo = _tmp_a4w.styles['classes'][label_estilo];
			} catch (st_val_err) {}
		}
		$.label.applyProperties(label_estilo);

		require(WPATH('vars'))[args.__id]['on'] = 'false';
	}
	var radio = ('radio' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['radio'] : '';

	var ancho_final = Ti.Platform.displayCaps.platformWidth * (parseInt(radio) / 100) - parseInt(borde);
	var radio_final = ancho_final / 2
};

/** 
 * Funcion que prende o apaga el estado del boton
 * 
 * @param on Si true, boton se muestra encendido, false en caso contrario 
 */

$.set = function(params) {
	if (!_.isUndefined(params.on)) {
		if (params.on == true || params.on == 'true') {
			/** 
			 * Cambiamos estilo de boton para visualizarlo de forma prendida 
			 */
			/** 
			 * Modificamos color de fondo de circulo 
			 */
			var vista_estilo = 'fondoazul';

			var setEstilo = function(clase) {
				if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
					try {
						$.vista.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
					} catch (sete_err) {}
				}
			};
			setEstilo(vista_estilo);

			/** 
			 * Modificamos color de texto de letra 
			 */
			$.label.setColor('#ffffff');

			require(WPATH('vars'))[args.__id]['on'] = 'true';
		} else {
			/** 
			 * Cambiamos estilo para visualizar boton como apagado 
			 */
			var vista_estilo = 'fondoplomo';

			var setEstilo = function(clase) {
				if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
					try {
						$.vista.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
					} catch (sete_err) {}
				}
			};
			setEstilo(vista_estilo);

			$.label.setColor('#2d9edb');

			require(WPATH('vars'))[args.__id]['on'] = 'false';
		}
	}
};