/** 
 * Widget foto_chica
 * Control para centralizar la compresion y escalamiento de las imagenes capturadas, en una caja
 * 
 * @param caja Indica el ancho-alto de este widget
 * @param label Indica el texto que se muestra sobre la imagen de este widget; solo se muestra si esta definido
 * @param opcional Indica el subtitulo del texto label para mostrar que imagen es opcional; solo se muestra si esta definido
 * @param id Identifica esta instancia para su uso en conjunto con otras en una misma pantalla 
 */
var _bind4section = {};

var args = arguments[0] || {};

function Load_imagen2(e) {
	/** 
	 * Evento que se ejecuta una vez cargada la vista 
	 */

	e.cancelBubble = true;
	var elemento = e.source;
	var evento = e;
	/** 
	 * Hace que la animacion se ejecute 
	 */
	elemento.start();

}

function Change_imagen3(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var evento = e;
	/** 
	 * Recupera la cantidad de frames de la imagen 
	 */
	var conteo_end = ('conteo_end' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['conteo_end'] : '';
	if (evento.index == conteo_end) {
		/** 
		 * Hara un conteo que cuando llegue a la cantidad definida en conteo_end se pausara la animacion 
		 */
		/** 
		 * Deja el contador en vacio 
		 */
		require(WPATH('vars'))[args.__id]['conteo_end'] = '';
		/** 
		 * Detiene la animacion 
		 */
		elemento.pause();
	}

}

function Click_vista2(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var estado = ('estado' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['estado'] : '';
	if (estado == 0 || estado == '0') {
		/** 
		 * Cuando el estado es 0, es cuando el widget es recien ocupado 
		 */
		/** 
		 * Se oculta vista que contiene imagen estatica 
		 */
		var vista3_visible = false;

		if (vista3_visible == 'si') {
			vista3_visible = true;
		} else if (vista3_visible == 'no') {
			vista3_visible = false;
		}
		$.vista3.setVisible(vista3_visible);

		/** 
		 * Se pone visible la vista que tiene la animacion 
		 */
		var vista4_visible = true;

		if (vista4_visible == 'si') {
			vista4_visible = true;
		} else if (vista4_visible == 'no') {
			vista4_visible = false;
		}
		$.vista4.setVisible(vista4_visible);

		/** 
		 * Se inicia la animacion 
		 */
		$.imagen2.start();
		/** 
		 * Se destaca el texto para indicar que es la vista activa 
		 */
		var Label2_estilo = 'destacado';

		if (typeof WPATH != 'undefined') {
			var _tmp_a4w = require(WPATH('a4w'));
		} else {
			var _tmp_a4w = require('a4w');
		}
		if ((typeof Label2_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (Label2_estilo in _tmp_a4w.styles['classes'])) {
			try {
				Label2_estilo = _tmp_a4w.styles['classes'][Label2_estilo];
			} catch (st_val_err) {}
		}
		$.Label2.applyProperties(Label2_estilo);

		/** 
		 * Se guarda el estado en un valor que no se utiliza, y evitar errores 
		 */
		require(WPATH('vars'))[args.__id]['estado'] = '1';

		if ('__args' in args) {
			args['__args'].onclick({});
		} else {
			args.onclick({});
		}
	} else if (estado == 2) {
		/** 
		 * Se activa cuando ya existe una miniatura procesada y cargada 
		 */
		/** 
		 * Se destaca el texto para indicar que es la vista activa 
		 */
		var Label2_estilo = 'destacado';

		if (typeof WPATH != 'undefined') {
			var _tmp_a4w = require(WPATH('a4w'));
		} else {
			var _tmp_a4w = require('a4w');
		}
		if ((typeof Label2_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (Label2_estilo in _tmp_a4w.styles['classes'])) {
			try {
				Label2_estilo = _tmp_a4w.styles['classes'][Label2_estilo];
			} catch (st_val_err) {}
		}
		$.Label2.applyProperties(Label2_estilo);


		if ('__args' in args) {
			args['__args'].onclick({});
		} else {
			args.onclick({});
		}
	}
}
/** 
 * Funcion que inicializa widget 
 */

$.init = function(params) {
	for (var tobe in params) args[tobe] = params[tobe];
	if ('__id' in params) require(WPATH('vars'))[params.__id] = {};
	/** 
	 * Se inicializa el widget con estado 0, ya que no hay imagenes procesadas al comienzo 
	 */
	require(WPATH('vars'))[args.__id]['estado'] = '0';
	/** 
	 * Sirve para definir en el caso de que la caja no se le haya definido un alto/ancho, pondra 55 como por defecto 
	 */
	require(WPATH('vars'))[args.__id]['caja'] = 55;
	if (!_.isUndefined(params.caja)) {
		/** 
		 * Modifica el alto y ancho de la vista con los valores de caja 
		 */
		var vista2_ancho = params.caja;

		if (vista2_ancho == '*') {
			vista2_ancho = Ti.UI.FILL;
		} else if (vista2_ancho == '-') {
			vista2_ancho = Ti.UI.SIZE;
		} else if (!isNaN(vista2_ancho)) {
			vista2_ancho = vista2_ancho + 'dp';
		}
		$.vista2.setWidth(vista2_ancho);

		var vista2_alto = params.caja;

		if (vista2_alto == '*') {
			vista2_alto = Ti.UI.FILL;
		} else if (vista2_alto == '-') {
			vista2_alto = Ti.UI.SIZE;
		} else if (!isNaN(vista2_alto)) {
			vista2_alto = vista2_alto + 'dp';
		}
		$.vista2.setHeight(vista2_alto);

		require(WPATH('vars'))[args.__id]['caja'] = params.caja;
	}
	if (!_.isUndefined(params.label)) {
		/** 
		 * En el caso de que se haya definido un texto de titulo para el widget, esto lo modifica 
		 */
		/** 
		 * Cambia el texto del titulo y lo deja visible 
		 */
		$.Label2.setText(params.label);

		var Label2_visible = true;

		if (Label2_visible == 'si') {
			Label2_visible = true;
		} else if (Label2_visible == 'no') {
			Label2_visible = false;
		}
		$.Label2.setVisible(Label2_visible);

	}
	if (!_.isUndefined(params.opcional)) {
		/** 
		 * En el caso de que exista un titulo opcional, lo carga y deja visible 
		 */
		/** 
		 * Cambia el texto del titulo opcional y lo deja visible 
		 */
		$.Label.setText(params.opcional);

		var Label_visible = true;

		if (Label_visible == 'si') {
			Label_visible = true;
		} else if (Label_visible == 'no') {
			Label_visible = false;
		}
		$.Label.setVisible(Label_visible);

	}
	if (!_.isUndefined(params.id)) {
		require(WPATH('vars'))[args.__id]['id'] = params.id;
	}
};

/** 
 * Funcion que recibe una imagen, la escala y comprime segun sus parametros y retorna una respuesta al padre de la instancia
 * 
 * @param imagen Imagen BLOB a ser procesada
 * @param nueva Pixeles para definir nuevo ancho-alto de imagen escalada
 * @param calidad Si definida, comprime imagen entregada al porcentaje indicado (solo enteros). 
 */

$.procesar = function(params) {
	if (!_.isUndefined(params.imagen)) {
		/** 
		 * Recibe una imagen cuadrada, y la escala (a tama&#241;o 'nueva' y 'mini' (tama&#241;o caja)) y comprime, y envia a respuesta 
		 */
		/** 
		 * Ocultamos widget 
		 */
		var vista4_visible = false;

		if (vista4_visible == 'si') {
			vista4_visible = true;
		} else if (vista4_visible == 'no') {
			vista4_visible = false;
		}
		$.vista4.setVisible(vista4_visible);

		/** 
		 * Mostramos animacion de conteo 
		 */
		var vista5_visible = true;

		if (vista5_visible == 'si') {
			vista5_visible = true;
		} else if (vista5_visible == 'no') {
			vista5_visible = false;
		}
		$.vista5.setVisible(vista5_visible);

		/** 
		 * Guarda la variable con 30 para contar hasta 30 imagenes de la animacion 
		 */
		require(WPATH('vars'))[args.__id]['conteo_end'] = 30;
		/** 
		 * Iniciamos animacion de conteo 
		 */
		$.imagen3.start();
		/** 
		 * Recupera el valor de la caja definida en la inicializacion, para hacer mini imagen 
		 */
		var caja = ('caja' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['caja'] : '';
		/** 
		 * Escala la imagen recibida en los valores definidos en el atributo caja de la inicializacion 
		 */
		if (OS_ANDROID) {
			var escalarImagen_imagefactory = require('ti.imagefactory');
			var mini = escalarImagen_imagefactory.imageAsResized(params.imagen, {
				width: caja,
				height: caja,
				quality: 0.7
			});
		} else {
			var mini = params.imagen.imageAsResized(caja, caja);
		}
		if (!_.isUndefined(params.nueva)) {
			/** 
			 * Deja vacio el contador de la animacion 
			 */
			require(WPATH('vars'))[args.__id]['conteo_end'] = '';
			/** 
			 * Continuamos reproduccion de conteo hasta donde acabe 
			 */
			$.imagen3.resume();
			if (!_.isUndefined(params.calidad)) {
				/** 
				 * Si la funcion define el parametro de calidad se hace lo siguiente 
				 */
				/** 
				 * Comprime la imagen recibida, en la calidad definida 
				 */
				if (OS_ANDROID) {
					var comprimirImagen_imagefactory = require('ti.imagefactory');
					var comprimida = comprimirImagen_imagefactory.compress(params.imagen, params.calidad / 100);
				} else if (OS_IOS) {
					var comprimirImagen_imagefactory = require('ti.imagefactory');
					var comprimida = comprimirImagen_imagefactory.compress(params.imagen, params.calidad / 100);
				}
				/** 
				 * Escala la imagen, recibe la imagen y alto-ancho 
				 */
				if (OS_ANDROID) {
					var escalarImagen2_imagefactory = require('ti.imagefactory');
					var fotofinal = escalarImagen2_imagefactory.imageAsResized(comprimida, {
						width: params.nueva,
						height: params.nueva,
						quality: 0.7
					});
				} else {
					var fotofinal = comprimida.imageAsResized(params.nueva, params.nueva);
				}
				/** 
				 * Actualiza el estado del widget para que haga otra accion dependiendo del click 
				 */
				require(WPATH('vars'))[args.__id]['estado'] = 2;
				/** 
				 * Oculta vista de animacion conteo 
				 */
				var vista5_visible = false;

				if (vista5_visible == 'si') {
					vista5_visible = true;
				} else if (vista5_visible == 'no') {
					vista5_visible = false;
				}
				$.vista5.setVisible(vista5_visible);

				/** 
				 * Oculta vista de animacion wiggle 
				 */
				var vista4_visible = false;

				if (vista4_visible == 'si') {
					vista4_visible = true;
				} else if (vista4_visible == 'no') {
					vista4_visible = false;
				}
				$.vista4.setVisible(vista4_visible);

				/** 
				 * Mostramos thumbnail 
				 */
				var vista6_visible = true;

				if (vista6_visible == 'si') {
					vista6_visible = true;
				} else if (vista6_visible == 'no') {
					vista6_visible = false;
				}
				$.vista6.setVisible(vista6_visible);

				/** 
				 * Modifica la imagen para mostrarla en pantalla 
				 */
				var imagen4_imagen = mini;

				if (typeof imagen4_imagen == 'string' && 'styles' in require(WPATH('a4w')) && imagen4_imagen in require(WPATH('a4w')).styles['images']) {
					imagen4_imagen = WPATH(require(WPATH('a4w')).styles['images'][imagen4_imagen]);
				}
				$.imagen4.setImage(imagen4_imagen);

				if ((Ti.Platform.manufacturer) == 'samsung') {
					/** 
					 * Revisamos si el equipo es samsung, ya que es la unica marca que saca las miniaturas mal rotadas 
					 */
					if (params.camara == 'frontal') {
						var vista6_rotar = 270;

						var setRotacion = function(angulo) {
							$.vista6.setTransform(Ti.UI.create2DMatrix().rotate(angulo));
						};
						setRotacion(vista6_rotar);

					} else if (params.camara == 'trasera') {
						var vista6_rotar = 90;

						var setRotacion = function(angulo) {
							$.vista6.setTransform(Ti.UI.create2DMatrix().rotate(angulo));
						};
						setRotacion(vista6_rotar);

					} else {}
				}
				/** 
				 * Llama al evento listo (donde se utiliza el widget) enviando la imagen escalada, miniatura y comprimida 
				 */

				if ('__args' in args) {
					args['__args'].onlisto({
						escalada: fotofinal,
						mini: mini,
						comprimida: comprimida
					});
				} else {
					args.onlisto({
						escalada: fotofinal,
						mini: mini,
						comprimida: comprimida
					});
				}
			} else {
				/** 
				 * Actualiza el estado del widget para que haga otra accion dependiendo del click 
				 */
				require(WPATH('vars'))[args.__id]['estado'] = 2;
				/** 
				 * Escala la imagen, recibe la imagen y alto/ancho. Retorna la imagen en una variable llamada fotofinal 
				 */
				if (OS_ANDROID) {
					var escalarImagen3_imagefactory = require('ti.imagefactory');
					var fotofinal = escalarImagen3_imagefactory.imageAsResized(params.imagen, {
						width: params.nueva,
						height: params.nueva,
						quality: 0.7
					});
				} else {
					var fotofinal = params.imagen.imageAsResized(params.nueva, params.nueva);
				}
				/** 
				 * Oculta vista de animacion conteo 
				 */
				var vista5_visible = false;

				if (vista5_visible == 'si') {
					vista5_visible = true;
				} else if (vista5_visible == 'no') {
					vista5_visible = false;
				}
				$.vista5.setVisible(vista5_visible);

				/** 
				 * Oculta vista de animacion wiggle 
				 */
				var vista4_visible = false;

				if (vista4_visible == 'si') {
					vista4_visible = true;
				} else if (vista4_visible == 'no') {
					vista4_visible = false;
				}
				$.vista4.setVisible(vista4_visible);

				/** 
				 * Mostramos thumbnail 
				 */
				var vista6_visible = true;

				if (vista6_visible == 'si') {
					vista6_visible = true;
				} else if (vista6_visible == 'no') {
					vista6_visible = false;
				}
				$.vista6.setVisible(vista6_visible);

				/** 
				 * Modifica la imagen para mostrarla en pantalla 
				 */
				var imagen4_imagen = mini;

				if (typeof imagen4_imagen == 'string' && 'styles' in require(WPATH('a4w')) && imagen4_imagen in require(WPATH('a4w')).styles['images']) {
					imagen4_imagen = WPATH(require(WPATH('a4w')).styles['images'][imagen4_imagen]);
				}
				$.imagen4.setImage(imagen4_imagen);

				if ((Ti.Platform.manufacturer) == 'samsung') {
					/** 
					 * Revisamos si el equipo es samsung, ya que es la unica marca que saca las miniaturas mal rotadas 
					 */
					var vista6_rotar = 90;

					var setRotacion = function(angulo) {
						$.vista6.setTransform(Ti.UI.create2DMatrix().rotate(angulo));
					};
					setRotacion(vista6_rotar);

					if (Ti.App.deployType != 'production') console.log('entro a la condicion', {});
				}
				/** 
				 * Llama al evento listo (donde se ocupa el widget) mandando los valores de la foto escalada, miniatura 
				 */

				if ('__args' in args) {
					args['__args'].onlisto({
						escalada: fotofinal,
						mini: mini
					});
				} else {
					args.onlisto({
						escalada: fotofinal,
						mini: mini
					});
				}
			}
		} else {
			/** 
			 * Actualiza el estado del widget para que haga otra accion dependiendo del click 
			 */
			require(WPATH('vars'))[args.__id]['estado'] = 2;
			/** 
			 * Oculta vista de animacion conteo 
			 */
			var vista5_visible = false;

			if (vista5_visible == 'si') {
				vista5_visible = true;
			} else if (vista5_visible == 'no') {
				vista5_visible = false;
			}
			$.vista5.setVisible(vista5_visible);

			/** 
			 * Oculta vista de animacion wiggle 
			 */
			var vista4_visible = false;

			if (vista4_visible == 'si') {
				vista4_visible = true;
			} else if (vista4_visible == 'no') {
				vista4_visible = false;
			}
			$.vista4.setVisible(vista4_visible);

			/** 
			 * Mostramos thumbnail 
			 */
			var vista6_visible = true;

			if (vista6_visible == 'si') {
				vista6_visible = true;
			} else if (vista6_visible == 'no') {
				vista6_visible = false;
			}
			$.vista6.setVisible(vista6_visible);

			/** 
			 * Modifica la imagen para mostrarla en pantalla 
			 */
			var imagen4_imagen = mini;

			if (typeof imagen4_imagen == 'string' && 'styles' in require(WPATH('a4w')) && imagen4_imagen in require(WPATH('a4w')).styles['images']) {
				imagen4_imagen = WPATH(require(WPATH('a4w')).styles['images'][imagen4_imagen]);
			}
			$.imagen4.setImage(imagen4_imagen);

			if ((Ti.Platform.manufacturer) == 'samsung') {
				/** 
				 * Revisamos si el equipo es samsung, ya que es la unica marca que saca las miniaturas mal rotadas 
				 */
				if (params.camara == 'frontal') {
					var vista6_rotar = 270;

					var setRotacion = function(angulo) {
						$.vista6.setTransform(Ti.UI.create2DMatrix().rotate(angulo));
					};
					setRotacion(vista6_rotar);

				} else if (params.camara == 'trasera') {
					var vista6_rotar = 90;

					var setRotacion = function(angulo) {
						$.vista6.setTransform(Ti.UI.create2DMatrix().rotate(angulo));
					};
					setRotacion(vista6_rotar);

				} else {}
			}
			/** 
			 * Llama al evento listo (donde se utiliza el widget), enviando la imagen mini escalada 
			 */

			if ('__args' in args) {
				args['__args'].onlisto({
					mini: mini
				});
			} else {
				args.onlisto({
					mini: mini
				});
			}
		}
		/** 
		 * Se limpian las variables complejas para optimizar memoria 
		 */

		mini = null;
		fotofinal = null;
		comprimida = null;
		params.imagen = null;;
	}
};

/** 
 * Detiene todas las animaciones y muestra la imagen estatica miniatura 
 */

$.detener = function(params) {
	/** 
	 * Detiene wiggle y muestra foto estatica, solo si no tiene imagen 
	 */
	var estado = ('estado' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['estado'] : '';
	if (estado == 1 || estado == '1') {
		/** 
		 * Ocultamos wiggle 
		 */
		var vista4_visible = false;

		if (vista4_visible == 'si') {
			vista4_visible = true;
		} else if (vista4_visible == 'no') {
			vista4_visible = false;
		}
		$.vista4.setVisible(vista4_visible);

		/** 
		 * Ocultamos conteo 
		 */
		var vista5_visible = false;

		if (vista5_visible == 'si') {
			vista5_visible = true;
		} else if (vista5_visible == 'no') {
			vista5_visible = false;
		}
		$.vista5.setVisible(vista5_visible);

		/** 
		 * Ocultamos minifoto (por si acaso) 
		 */
		var vista6_visible = false;

		if (vista6_visible == 'si') {
			vista6_visible = true;
		} else if (vista6_visible == 'no') {
			vista6_visible = false;
		}
		$.vista6.setVisible(vista6_visible);

		/** 
		 * Mostramos imagen estatica 
		 */
		var vista3_visible = true;

		if (vista3_visible == 'si') {
			vista3_visible = true;
		} else if (vista3_visible == 'no') {
			vista3_visible = false;
		}
		$.vista3.setVisible(vista3_visible);

		/** 
		 * Dejamos estilo label normal 
		 */
		var Label2_estilo = 'normal';

		if (typeof WPATH != 'undefined') {
			var _tmp_a4w = require(WPATH('a4w'));
		} else {
			var _tmp_a4w = require('a4w');
		}
		if ((typeof Label2_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (Label2_estilo in _tmp_a4w.styles['classes'])) {
			try {
				Label2_estilo = _tmp_a4w.styles['classes'][Label2_estilo];
			} catch (st_val_err) {}
		}
		$.Label2.applyProperties(Label2_estilo);

		/** 
		 * Deja al widget con estado 0, ya que no hay imagenes 
		 */
		require(WPATH('vars'))[args.__id]['estado'] = '0';
	} else if (estado == 2) {
		/** 
		 * Dejamos estilo label normal 
		 */
		var Label2_estilo = 'normal';

		if (typeof WPATH != 'undefined') {
			var _tmp_a4w = require(WPATH('a4w'));
		} else {
			var _tmp_a4w = require('a4w');
		}
		if ((typeof Label2_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (Label2_estilo in _tmp_a4w.styles['classes'])) {
			try {
				Label2_estilo = _tmp_a4w.styles['classes'][Label2_estilo];
			} catch (st_val_err) {}
		}
		$.Label2.applyProperties(Label2_estilo);

	}
};