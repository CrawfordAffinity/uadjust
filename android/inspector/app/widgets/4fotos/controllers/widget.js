/** 
 * Widget 4 Fotos Chicas
 * Este control representa una vista con 3-4 cajas que contienen version mini de una foto y las procesa.
 * 
 * @param caja Indica el ancho y alto de cada mini caja
 * @param label1 Indica el texto a mostrar bajo la primera mini caja
 * @param label2 Indica el texto a mostrar bajo la segunda mini caja
 * @param label3 Indica el texto a mostrar bajo la tercera mini caja
 * @param label4 Indica el texto a mostrar bajo la cuerta mini caja
 * @param ancho Indica el ancho de este widget, como contenedor de las 4 mini cajas
 * @param alto Indica el alto de este widget, como contenedor de las 4 mini cajas 
 */
var _bind4section = {};

var args = arguments[0] || {};


$.widgetFotochica.init({
	caja: 55,
	__id: 'ALL187601693',
	onlisto: Listo_widgetFotochica,
	onclick: Click_widgetFotochica
});

function Click_widgetFotochica(e) {
	/** 
	 * esta llamada es usada cuando se clickea el widget 
	 */

	var evento = e;
	/** 
	 * Detiene la animacion de los widget y permite que se abra una sola camara a la vez 
	 */

	$.widgetFotochica2.detener({});
	$.widgetFotochica3.detener({});
	$.widgetFotochica4.detener({});
	/** 
	 * manda un numero con la posicion del widget que se esta utilizando y asi procesarlo en la pantalla que la esta usando 
	 */

	if ('__args' in args) {
		args['__args'].onclick({
			pos: 1
		});
	} else {
		args.onclick({
			pos: 1
		});
	}
	var caja1 = ('caja1' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['caja1'] : '';
	if (caja1 == 0 || caja1 == '0') {
		require(WPATH('vars'))[args.__id]['caja1'] = '1';
		require(WPATH('vars'))[args.__id]['caja2'] = '0';
		require(WPATH('vars'))[args.__id]['caja3'] = '0';
		require(WPATH('vars'))[args.__id]['caja4'] = '0';
		$.Fachada.setColor('#EE7F7E');

		var Barrio_estilo = 'normal';

		if (typeof WPATH != 'undefined') {
			var _tmp_a4w = require(WPATH('a4w'));
		} else {
			var _tmp_a4w = require('a4w');
		}
		if ((typeof Barrio_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (Barrio_estilo in _tmp_a4w.styles['classes'])) {
			try {
				Barrio_estilo = _tmp_a4w.styles['classes'][Barrio_estilo];
			} catch (st_val_err) {}
		}
		$.Barrio.applyProperties(Barrio_estilo);

		var Numero_estilo = 'normal';

		if (typeof WPATH != 'undefined') {
			var _tmp_a4w = require(WPATH('a4w'));
		} else {
			var _tmp_a4w = require('a4w');
		}
		if ((typeof Numero_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (Numero_estilo in _tmp_a4w.styles['classes'])) {
			try {
				Numero_estilo = _tmp_a4w.styles['classes'][Numero_estilo];
			} catch (st_val_err) {}
		}
		$.Numero.applyProperties(Numero_estilo);

		var NDepto_estilo = 'normal';

		if (typeof WPATH != 'undefined') {
			var _tmp_a4w = require(WPATH('a4w'));
		} else {
			var _tmp_a4w = require('a4w');
		}
		if ((typeof NDepto_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (NDepto_estilo in _tmp_a4w.styles['classes'])) {
			try {
				NDepto_estilo = _tmp_a4w.styles['classes'][NDepto_estilo];
			} catch (st_val_err) {}
		}
		$.NDepto.applyProperties(NDepto_estilo);

	} else {
		require(WPATH('vars'))[args.__id]['caja1'] = '0';
	}
}

function Listo_widgetFotochica(e) {
	/** 
	 * esta llamada se utiliza cuando se ha procesado una imagen (escalar y comprimir) 
	 */

	var evento = e;
	if (!_.isUndefined(evento.comprimida)) {
		/** 
		 * Si la imagen comprimida desde el widget fue comprimida se hace lo siguiente 
		 */
		/** 
		 * Mando la posicion, imagen en miniatura y otra comprimida a la pantalla que utiliza el widget (4fotos) 
		 */

		if ('__args' in args) {
			args['__args'].onlisto({
				pos: 1,
				mini: evento.mini,
				nueva: evento.comprimida
			});
		} else {
			args.onlisto({
				pos: 1,
				mini: evento.mini,
				nueva: evento.comprimida
			});
		}
	} else if (!_.isUndefined(evento.escalada)) {
		/** 
		 * Si la imagen recibida fue escalada se hace lo siguiente 
		 */
		/** 
		 * Mando la posicion, imagen en miniatura y otra escalada a la pantalla que utiliza el widget (4fotos) 
		 */

		if ('__args' in args) {
			args['__args'].onlisto({
				pos: 1,
				mini: evento.mini,
				nueva: evento.escalada
			});
		} else {
			args.onlisto({
				pos: 1,
				mini: evento.mini,
				nueva: evento.escalada
			});
		}
	} else {
		/** 
		 * Si no se recibe la imagen comprimida o escalada, se hace lo siguiente 
		 */
		/** 
		 * Mando la posicion, imagen en miniatura a la pantalla que utiliza el widget (4fotos) 
		 */

		if ('__args' in args) {
			args['__args'].onlisto({
				pos: 1,
				mini: evento.mini
			});
		} else {
			args.onlisto({
				pos: 1,
				mini: evento.mini
			});
		}
	}
}

$.widgetFotochica2.init({
	caja: 55,
	__id: 'ALL1901394969',
	onlisto: Listo_widgetFotochica2,
	onclick: Click_widgetFotochica2
});

function Click_widgetFotochica2(e) {
	/** 
	 * esta llamada es usada cuando se clickea el widget 
	 */

	var evento = e;
	/** 
	 * Detiene la animacion de los widget y permite que se abra una sola camara a la vez 
	 */

	$.widgetFotochica.detener({});
	$.widgetFotochica3.detener({});
	$.widgetFotochica4.detener({});
	/** 
	 * manda un numero con la posicion del widget que se esta utilizando y asi procesarlo en la pantalla que la esta usando 
	 */

	if ('__args' in args) {
		args['__args'].onclick({
			pos: 2
		});
	} else {
		args.onclick({
			pos: 2
		});
	}
	var caja2 = ('caja2' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['caja2'] : '';
	if (caja2 == 0 || caja2 == '0') {
		require(WPATH('vars'))[args.__id]['caja1'] = '0';
		require(WPATH('vars'))[args.__id]['caja2'] = '1';
		require(WPATH('vars'))[args.__id]['caja3'] = '0';
		require(WPATH('vars'))[args.__id]['caja4'] = '0';
		$.Barrio.setColor('#EE7F7E');

		var Fachada_estilo = 'normal';

		if (typeof WPATH != 'undefined') {
			var _tmp_a4w = require(WPATH('a4w'));
		} else {
			var _tmp_a4w = require('a4w');
		}
		if ((typeof Fachada_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (Fachada_estilo in _tmp_a4w.styles['classes'])) {
			try {
				Fachada_estilo = _tmp_a4w.styles['classes'][Fachada_estilo];
			} catch (st_val_err) {}
		}
		$.Fachada.applyProperties(Fachada_estilo);

		var Numero_estilo = 'normal';

		if (typeof WPATH != 'undefined') {
			var _tmp_a4w = require(WPATH('a4w'));
		} else {
			var _tmp_a4w = require('a4w');
		}
		if ((typeof Numero_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (Numero_estilo in _tmp_a4w.styles['classes'])) {
			try {
				Numero_estilo = _tmp_a4w.styles['classes'][Numero_estilo];
			} catch (st_val_err) {}
		}
		$.Numero.applyProperties(Numero_estilo);

		var NDepto_estilo = 'normal';

		if (typeof WPATH != 'undefined') {
			var _tmp_a4w = require(WPATH('a4w'));
		} else {
			var _tmp_a4w = require('a4w');
		}
		if ((typeof NDepto_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (NDepto_estilo in _tmp_a4w.styles['classes'])) {
			try {
				NDepto_estilo = _tmp_a4w.styles['classes'][NDepto_estilo];
			} catch (st_val_err) {}
		}
		$.NDepto.applyProperties(NDepto_estilo);

	} else {
		require(WPATH('vars'))[args.__id]['caja2'] = '0';
	}
}

function Listo_widgetFotochica2(e) {
	/** 
	 * esta llamada se utiliza cuando se ha procesado una imagen (escalar y comprimir) 
	 */

	var evento = e;
	if (!_.isUndefined(evento.comprimida)) {
		/** 
		 * Si la imagen comprimida desde el widget fue comprimida se hace lo siguiente 
		 */
		/** 
		 * Mando la posicion, imagen en miniatura y otra comprimida a la pantalla que utiliza el widget (4fotos) 
		 */

		if ('__args' in args) {
			args['__args'].onlisto({
				pos: 2,
				mini: evento.mini,
				nueva: evento.comprimida
			});
		} else {
			args.onlisto({
				pos: 2,
				mini: evento.mini,
				nueva: evento.comprimida
			});
		}
	} else if (!_.isUndefined(evento.escalada)) {
		/** 
		 * Si la imagen recibida fue escalada se hace lo siguiente 
		 */
		/** 
		 * Mando la posicion, imagen en miniatura y otra escalada a la pantalla que utiliza el widget (4fotos) 
		 */

		if ('__args' in args) {
			args['__args'].onlisto({
				pos: 2,
				mini: evento.mini,
				nueva: evento.escalada
			});
		} else {
			args.onlisto({
				pos: 2,
				mini: evento.mini,
				nueva: evento.escalada
			});
		}
	} else {
		/** 
		 * Si no se recibe la imagen comprimida o escalada, se hace lo siguiente 
		 */
		/** 
		 * Mando la posicion, imagen en miniatura a la pantalla que utiliza el widget (4fotos) 
		 */

		if ('__args' in args) {
			args['__args'].onlisto({
				pos: 2,
				mini: evento.mini
			});
		} else {
			args.onlisto({
				pos: 2,
				mini: evento.mini
			});
		}
	}
}

$.widgetFotochica3.init({
	caja: 55,
	__id: 'ALL1364996196',
	onlisto: Listo_widgetFotochica3,
	onclick: Click_widgetFotochica3
});

function Click_widgetFotochica3(e) {
	/** 
	 * esta llamada es usada cuando se clickea el widget 
	 */

	var evento = e;
	/** 
	 * Detiene la animacion de los widget y permite que se abra una sola camara a la vez 
	 */

	$.widgetFotochica.detener({});
	$.widgetFotochica2.detener({});
	$.widgetFotochica4.detener({});
	/** 
	 * manda un numero con la posicion del widget que se esta utilizando y asi procesarlo en la pantalla que la esta usando 
	 */

	if ('__args' in args) {
		args['__args'].onclick({
			pos: 3
		});
	} else {
		args.onclick({
			pos: 3
		});
	}
	var caja3 = ('caja3' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['caja3'] : '';
	if (caja3 == 0 || caja3 == '0') {
		require(WPATH('vars'))[args.__id]['caja1'] = '0';
		require(WPATH('vars'))[args.__id]['caja2'] = '0';
		require(WPATH('vars'))[args.__id]['caja3'] = '1';
		require(WPATH('vars'))[args.__id]['caja4'] = '0';
		$.Numero.setColor('#EE7F7E');

		var Fachada_estilo = 'normal';

		if (typeof WPATH != 'undefined') {
			var _tmp_a4w = require(WPATH('a4w'));
		} else {
			var _tmp_a4w = require('a4w');
		}
		if ((typeof Fachada_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (Fachada_estilo in _tmp_a4w.styles['classes'])) {
			try {
				Fachada_estilo = _tmp_a4w.styles['classes'][Fachada_estilo];
			} catch (st_val_err) {}
		}
		$.Fachada.applyProperties(Fachada_estilo);

		var Barrio_estilo = 'normal';

		if (typeof WPATH != 'undefined') {
			var _tmp_a4w = require(WPATH('a4w'));
		} else {
			var _tmp_a4w = require('a4w');
		}
		if ((typeof Barrio_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (Barrio_estilo in _tmp_a4w.styles['classes'])) {
			try {
				Barrio_estilo = _tmp_a4w.styles['classes'][Barrio_estilo];
			} catch (st_val_err) {}
		}
		$.Barrio.applyProperties(Barrio_estilo);

		var NDepto_estilo = 'normal';

		if (typeof WPATH != 'undefined') {
			var _tmp_a4w = require(WPATH('a4w'));
		} else {
			var _tmp_a4w = require('a4w');
		}
		if ((typeof NDepto_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (NDepto_estilo in _tmp_a4w.styles['classes'])) {
			try {
				NDepto_estilo = _tmp_a4w.styles['classes'][NDepto_estilo];
			} catch (st_val_err) {}
		}
		$.NDepto.applyProperties(NDepto_estilo);

	} else {
		require(WPATH('vars'))[args.__id]['caja3'] = '0';
	}
}

function Listo_widgetFotochica3(e) {
	/** 
	 * esta llamada se utiliza cuando se ha procesado una imagen (escalar y comprimir) 
	 */

	var evento = e;
	if (!_.isUndefined(evento.comprimida)) {
		/** 
		 * Si la imagen comprimida desde el widget fue comprimida se hace lo siguiente 
		 */
		/** 
		 * Mando la posicion, imagen en miniatura y otra comprimida a la pantalla que utiliza el widget (4fotos) 
		 */

		if ('__args' in args) {
			args['__args'].onlisto({
				pos: 3,
				mini: evento.mini,
				nueva: evento.comprimida
			});
		} else {
			args.onlisto({
				pos: 3,
				mini: evento.mini,
				nueva: evento.comprimida
			});
		}
	} else if (!_.isUndefined(evento.escalada)) {
		/** 
		 * Si la imagen recibida fue escalada se hace lo siguiente 
		 */
		/** 
		 * Mando la posicion, imagen en miniatura y otra escalada a la pantalla que utiliza el widget (4fotos) 
		 */

		if ('__args' in args) {
			args['__args'].onlisto({
				pos: 3,
				mini: evento.mini,
				nueva: evento.escalada
			});
		} else {
			args.onlisto({
				pos: 3,
				mini: evento.mini,
				nueva: evento.escalada
			});
		}
	} else {
		/** 
		 * Si no se recibe la imagen comprimida o escalada, se hace lo siguiente 
		 */
		/** 
		 * Mando la posicion, imagen en miniatura a la pantalla que utiliza el widget (4fotos) 
		 */

		if ('__args' in args) {
			args['__args'].onlisto({
				pos: 3,
				mini: evento.mini
			});
		} else {
			args.onlisto({
				pos: 3,
				mini: evento.mini
			});
		}
	}
}

$.widgetFotochica4.init({
	caja: 55,
	__id: 'ALL1437730300',
	onlisto: Listo_widgetFotochica4,
	onclick: Click_widgetFotochica4
});

function Click_widgetFotochica4(e) {
	/** 
	 * esta llamada es usada cuando se clickea el widget 
	 */

	var evento = e;
	/** 
	 * Detiene la animacion de los widget y permite que se abra una sola camara a la vez 
	 */

	$.widgetFotochica.detener({});
	$.widgetFotochica2.detener({});
	$.widgetFotochica3.detener({});
	/** 
	 * manda un numero con la posicion del widget que se esta utilizando y asi procesarlo en la pantalla que la esta usando 
	 */

	if ('__args' in args) {
		args['__args'].onclick({
			pos: 4
		});
	} else {
		args.onclick({
			pos: 4
		});
	}
	var caja4 = ('caja4' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['caja4'] : '';
	if (caja4 == 0 || caja4 == '0') {
		require(WPATH('vars'))[args.__id]['caja1'] = '0';
		require(WPATH('vars'))[args.__id]['caja2'] = '0';
		require(WPATH('vars'))[args.__id]['caja3'] = '0';
		require(WPATH('vars'))[args.__id]['caja4'] = '1';
		$.NDepto.setColor('#EE7F7E');

		var Numero_estilo = 'normal';

		if (typeof WPATH != 'undefined') {
			var _tmp_a4w = require(WPATH('a4w'));
		} else {
			var _tmp_a4w = require('a4w');
		}
		if ((typeof Numero_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (Numero_estilo in _tmp_a4w.styles['classes'])) {
			try {
				Numero_estilo = _tmp_a4w.styles['classes'][Numero_estilo];
			} catch (st_val_err) {}
		}
		$.Numero.applyProperties(Numero_estilo);

		var Barrio_estilo = 'normal';

		if (typeof WPATH != 'undefined') {
			var _tmp_a4w = require(WPATH('a4w'));
		} else {
			var _tmp_a4w = require('a4w');
		}
		if ((typeof Barrio_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (Barrio_estilo in _tmp_a4w.styles['classes'])) {
			try {
				Barrio_estilo = _tmp_a4w.styles['classes'][Barrio_estilo];
			} catch (st_val_err) {}
		}
		$.Barrio.applyProperties(Barrio_estilo);

		var Fachada_estilo = 'normal';

		if (typeof WPATH != 'undefined') {
			var _tmp_a4w = require(WPATH('a4w'));
		} else {
			var _tmp_a4w = require('a4w');
		}
		if ((typeof Fachada_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (Fachada_estilo in _tmp_a4w.styles['classes'])) {
			try {
				Fachada_estilo = _tmp_a4w.styles['classes'][Fachada_estilo];
			} catch (st_val_err) {}
		}
		$.Fachada.applyProperties(Fachada_estilo);

	} else {
		require(WPATH('vars'))[args.__id]['caja4'] = '0';
	}
}

function Listo_widgetFotochica4(e) {
	/** 
	 * esta llamada se utiliza cuando se ha procesado una imagen (escalar y comprimir) 
	 */

	var evento = e;
	if (!_.isUndefined(evento.comprimida)) {
		/** 
		 * Si la imagen comprimida desde el widget fue comprimida se hace lo siguiente 
		 */
		/** 
		 * llamar evento &quot;listo&quot; 
		 */

		if ('__args' in args) {
			args['__args'].onlisto({
				pos: 4,
				mini: evento.mini,
				nueva: evento.comprimida
			});
		} else {
			args.onlisto({
				pos: 4,
				mini: evento.mini,
				nueva: evento.comprimida
			});
		}
	} else if (!_.isUndefined(evento.escalada)) {
		/** 
		 * Si la imagen recibida fue escalada se hace lo siguiente 
		 */
		/** 
		 * Mando la posicion, imagen en miniatura y otra escalada a la pantalla que utiliza el widget (4fotos) 
		 */

		if ('__args' in args) {
			args['__args'].onlisto({
				pos: 4,
				mini: evento.mini,
				nueva: evento.escalada
			});
		} else {
			args.onlisto({
				pos: 4,
				mini: evento.mini,
				nueva: evento.escalada
			});
		}
	} else {
		/** 
		 * Si no se recibe la imagen comprimida o escalada, se hace lo siguiente 
		 */
		/** 
		 * Mando la posicion, imagen en miniatura a la pantalla que utiliza el widget (4fotos) 
		 */

		if ('__args' in args) {
			args['__args'].onlisto({
				pos: 4,
				mini: evento.mini
			});
		} else {
			args.onlisto({
				pos: 4,
				mini: evento.mini
			});
		}
	}
}
/** 
 * Funcion que inicializa el widget 
 */

$.init = function(params) {
	for (var tobe in params) args[tobe] = params[tobe];
	if ('__id' in params) require(WPATH('vars'))[params.__id] = {};
	if (!_.isUndefined(params.caja)) {
		/** 
		 * Si en la pantalla utlizada se definio la altura de la caja, esta condicion recibe el parametro para enviarlo al widget que lo ocupa 
		 */
		/** 
		 * Recibe el valor del parametro caja y lo envia al widget 
		 */

		$.widgetFotochica.init({
			caja: params.caja
		});
		$.widgetFotochica2.init({
			caja: params.caja
		});
		$.widgetFotochica3.init({
			caja: params.caja
		});
		$.widgetFotochica4.init({
			caja: params.caja
		});
	}
	if (!_.isUndefined(params.ancho)) {
		/** 
		 * Ancho de este widget 
		 */
		/** 
		 * Define el ancho de la vista que contiene los 4 widget 
		 */
		var vista_ancho = params.ancho;

		if (vista_ancho == '*') {
			vista_ancho = Ti.UI.FILL;
		} else if (vista_ancho == '-') {
			vista_ancho = Ti.UI.SIZE;
		} else if (!isNaN(vista_ancho)) {
			vista_ancho = vista_ancho + 'dp';
		}
		$.vista.setWidth(vista_ancho);

	}
	if (!_.isUndefined(params.alto)) {
		/** 
		 * Alto de este widget 
		 */
		/** 
		 * Define el alto de la vista que contiene los 4 widget 
		 */
		var vista_alto = params.alto;

		if (vista_alto == '*') {
			vista_alto = Ti.UI.FILL;
		} else if (vista_alto == '-') {
			vista_alto = Ti.UI.SIZE;
		} else if (!isNaN(vista_alto)) {
			vista_alto = vista_alto + 'dp';
		}
		$.vista.setHeight(vista_alto);

	}
	/** 
	 * Inicializamos pixeles de cada caja por defecto en 0 pixeles 
	 */
	require(WPATH('vars'))[args.__id]['caja1'] = '0';
	require(WPATH('vars'))[args.__id]['caja2'] = '0';
	require(WPATH('vars'))[args.__id]['caja3'] = '0';
};

/** 
 * Funcion que recibe la imagen a ser procesada segun la posicion definida en la llamada click del widget fotochica
 * 
 * @param imagen1 Imagen para ser procesada en la primera caja (instancia 1 de widget fotochica)
 * @param imagen2 Imagen para ser procesada en la segunda caja (instancia 2 de widget fotochica)
 * @param imagen3 Imagen para ser procesada en la tercera caja (instancia 3 de widget fotochica)
 * @param imagen4 Imagen para ser procesada en la cuarta caja (instancia 4 de widget fotochica)
 * @param camara Traspasa valor a widgets fotochica en las 4 cajas
 * @param nueva Indica resolucion de nueva imagen capturada para las 4 cajas 
 */

$.procesar = function(params) {
	if (!_.isUndefined(params.imagen1)) {
		/** 
		 * En el caso de que la imagen1 exista 
		 */
		if (!_.isUndefined(params.camara)) {
			if (!_.isUndefined(params.nueva)) {
				/** 
				 * si existe el parametro &quot;nueva&quot;, definira la resolucion de la imagen a escalar y/o comprimir 
				 */
				/** 
				 * Se envia la imagen, la resolucion y calidad de la imagen a procesar al widget fotochica 
				 */

				$.widgetFotochica.procesar({
					imagen: params.imagen1,
					nueva: params.nueva,
					calidad: 91,
					camara: params.camara
				});
			} else {
				/** 
				 * Si no existe el parametro &quot;nueva&quot; si existe el parametro &quot;nueva&quot;, solo definira la calidad de la imagen a obtener de regreso 
				 */
				/** 
				 * Se envia la imagen, y calidad de la imagen a procesar al widget fotochica 
				 */

				$.widgetFotochica.procesar({
					imagen: params.imagen1,
					calidad: 91,
					camara: params.camara
				});
			}
		} else {
			if (!_.isUndefined(params.nueva)) {
				/** 
				 * si existe el parametro &quot;nueva&quot;, definira la resolucion de la imagen a escalar y/o comprimir 
				 */
				/** 
				 * Se envia la imagen, la resolucion y calidad de la imagen a procesar al widget fotochica 
				 */

				$.widgetFotochica.procesar({
					imagen: params.imagen1,
					nueva: params.nueva,
					calidad: 91
				});
			} else {
				/** 
				 * Si no existe el parametro &quot;nueva&quot; si existe el parametro &quot;nueva&quot;, solo definira la calidad de la imagen a obtener de regreso 
				 */
				/** 
				 * Se envia la imagen, y calidad de la imagen a procesar al widget fotochica 
				 */

				$.widgetFotochica.procesar({
					imagen: params.imagen1,
					calidad: 91
				});
			}
		}
		/** 
		 * Limpiamos imagen1 de memoria 
		 */
		params.imagen1 = null;
	}
	if (!_.isUndefined(params.imagen2)) {
		/** 
		 * En el caso de que la imagen2 exista 
		 */
		if (!_.isUndefined(params.nueva)) {
			/** 
			 * si existe el parametro &quot;nueva&quot;, definira la resolucion de la imagen a escalar y/o comprimir 
			 */
			/** 
			 * Se envia la imagen, la resolucion y calidad de la imagen a procesar al widget fotochica 
			 */

			$.widgetFotochica2.procesar({
				imagen: params.imagen2,
				nueva: params.nueva,
				calidad: 91,
				camara: params.camara
			});
		} else {
			/** 
			 * Si no existe el parametro &quot;nueva&quot; si existe el parametro &quot;nueva&quot;, solo definira la calidad de la imagen a obtener de regreso 
			 */
			/** 
			 * Se envia la imagen, y calidad de la imagen a procesar al widget fotochica 
			 */

			$.widgetFotochica2.procesar({
				imagen: params.imagen2,
				calidad: 91,
				camara: params.camara
			});
		}
		/** 
		 * Limpiamos imagen2 de memoria 
		 */
		params.imagen2 = null;
	}
	if (!_.isUndefined(params.imagen3)) {
		/** 
		 * En el caso de que la imagen3 exista 
		 */
		if (!_.isUndefined(params.nueva)) {
			/** 
			 * si existe el parametro &quot;nueva&quot;, definira la resolucion de la imagen a escalar y/o comprimir 
			 */
			/** 
			 * Se envia la imagen, la resolucion y calidad de la imagen a procesar al widget fotochica 
			 */

			$.widgetFotochica3.procesar({
				imagen: params.imagen3,
				nueva: params.nueva,
				calidad: 91,
				camara: params.camara
			});
		} else {
			/** 
			 * Si no existe el parametro &quot;nueva&quot; si existe el parametro &quot;nueva&quot;, solo definira la calidad de la imagen a obtener de regreso 
			 */
			/** 
			 * Se envia la imagen, y calidad de la imagen a procesar al widget fotochica 
			 */

			$.widgetFotochica3.procesar({
				imagen: params.imagen3,
				calidad: 91,
				camara: params.camara
			});
		}
		/** 
		 * Limpiamos imagen3 de memoria 
		 */
		params.imagen3 = null;
	}
	if (!_.isUndefined(params.imagen4)) {
		/** 
		 * En el caso de que la imagen4 exista 
		 */
		if (!_.isUndefined(params.nueva)) {
			/** 
			 * si existe el parametro &quot;nueva&quot;, definira la resolucion de la imagen a escalar y/o comprimir 
			 */
			/** 
			 * Se envia la imagen, la resolucion y calidad de la imagen a procesar al widget fotochica 
			 */

			$.widgetFotochica4.procesar({
				imagen: params.imagen4,
				nueva: params.nueva,
				calidad: 91,
				camara: params.camara
			});
		} else {
			/** 
			 * Si no existe el parametro &quot;nueva&quot; si existe el parametro &quot;nueva&quot;, solo definira la calidad de la imagen a obtener de regreso 
			 */
			/** 
			 * Se envia la imagen, y calidad de la imagen a procesar al widget fotochica 
			 */

			$.widgetFotochica4.procesar({
				imagen: params.imagen4,
				calidad: 91,
				camara: params.camara
			});
		}
		/** 
		 * Limpiamos imagen4 de memoria 
		 */
		params.imagen4 = null;
	}
};