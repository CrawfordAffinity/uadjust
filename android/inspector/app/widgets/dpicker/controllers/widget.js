/** 
 * Widget Selector Fechas
 * Permite seleccionar una fecha universalmente
 * 
 * @param desde Define el valor del picker con el valor inicial de las fechas
 * @param hasta Define el valor del picker con el valor final de las fechas
 * @param aceptar Define el texto del boton aceptar
 * @param cancelar Define el texto del boton cancelar 
 */
var _bind4section = {};

var args = arguments[0] || {};

function Click_vista4(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	require(WPATH('vars'))[args.__id]['flag'] = 'cerrado';
	if (OS_IOS) {
		$.vista.animate({
			bottom: -265,
			duration: 300
		});
	}

	if ('__args' in args) {
		args['__args'].oncancelar({});
	} else {
		args.oncancelar({});
	}

}

function Click_vista5(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	require(WPATH('vars'))[args.__id]['flag'] = 'cerrado';
	if (OS_IOS) {
		$.vista.animate({
			bottom: -265,
			duration: 300
		});
	}
	var fechanueva;
	fechanueva = $.picker.getValue();


	if ('__args' in args) {
		args['__args'].onaceptar({
			valor: fechanueva
		});
	} else {
		args.onaceptar({
			valor: fechanueva
		});
	}

}


/** 
 * Funcion que inicializa el widget 
 */

$.init = function(params) {
	for (var tobe in params) args[tobe] = params[tobe];
	if ('__id' in params) require(WPATH('vars'))[params.__id] = {};
	/** 
	 * Iniciamos variable 
	 */
	require(WPATH('vars'))[args.__id]['flag'] = 'cerrado';
	if (OS_IOS) {
		/** 
		 * Definimos que tipo de picker es el que tiene que cargarse 
		 */
		var picker_tipo = 'fecha';

		if (_.contains('countdown,timer,count down,count_down,tiempo'.split(','), picker_tipo)) {
			picker_tipo = Titanium.UI.PICKER_TYPE_COUNT_DOWN_TIMER;
		} else if (_.contains('date,fecha'.split(','), picker_tipo)) {
			picker_tipo = Titanium.UI.PICKER_TYPE_DATE;
		} else if (_.contains('date_and_time,date and time,fecha y hora,fechahora,fecha hora,datetime'.split(','), picker_tipo)) {
			picker_tipo = Titanium.UI.PICKER_TYPE_DATE_AND_TIME;
		} else if (_.contains('tiempo,hora,time'.split(','), picker_tipo)) {
			picker_tipo = Titanium.UI.PICKER_TYPE_TIME;
		} else {
			picker_tipo = Titanium.UI.PICKER_TYPE_PLAIN;
		}
		$.picker.setType(picker_tipo);

	}
	if (!_.isUndefined(params.desde)) {
		var picker_desde = params.desde;

		var setMinDate2 = function(_fecha) {
			var _moment = require('alloy/moment');
			if (_.isString(_fecha) && _fecha.indexOf('/') != -1) {
				$.picker.setMinDate(_moment(_fecha, 'DD/MM/YYYY').toDate());
			} else if (_.isDate(_fecha)) {
				$.picker.setMinDate(_fecha);
			}
		};
		setMinDate2(picker_desde);

	}
	if (!_.isUndefined(params.hasta)) {
		var picker_hasta = params.hasta;

		var setMaxDate2 = function(_fecha) {
			var _moment = require('alloy/moment');
			if (_.isString(_fecha) && _fecha.indexOf('/') != -1) {
				$.picker.setMaxDate(_moment(_fecha, 'DD/MM/YYYY').toDate());
			} else if (_.isDate(_fecha)) {
				$.picker.setMaxDate(_fecha);
			}
		};
		setMaxDate2(picker_hasta);

	}
	if (!_.isUndefined(params.aceptar)) {
		$.Aceptar.setText(params.aceptar);

	}
	if (!_.isUndefined(params.cancelar)) {
		$.Cancelar.setText(params.cancelar);

	}
};

/** 
 * Funcion que abre el picker 
 */

$.abrir = function(params) {
	var flag = ('flag' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['flag'] : '';
	if (OS_IOS) {
		/** 
		 * Revisamos si el equipo es android o iOS, en el caso de que sea un iphone se sube una vista desde la parte baja de la pantalla para seleccionar la fecha, si es android, levanta la vista nativa del celular para seleccionar la fecha 
		 */
		if (flag == 'cerrado') {
			require(WPATH('vars'))[args.__id]['flag'] = 'abierto';
			$.vista.animate({
				bottom: 0,
				duration: 300
			});
		}
	} else {
		var escogerFecha_moment = require('alloy/moment');
		var escogerFecha_datepicker = $.picker
		escogerFecha_datepicker.showDatePickerDialog({
			value: new Date(),
			callback: function(e) {
				if (e.cancel) {
					var resp = {
						cancel: true,
						format: '',
						formato: ''
					};
				} else {
					var resp = {
						valor: e.value,
						cancel: false,
						format: '',
						formato: ''
					};
				}
				if (resp.cancel == true || resp.cancel == 'true') {
					/** 
					 * Escondemos la seleccion de fecha 
					 */

					if ('__args' in args) {
						args['__args'].oncancelar({});
					} else {
						args.oncancelar({});
					}
				} else {
					/** 
					 * Enviamos por evento la fecha seleccionada 
					 */

					if ('__args' in args) {
						args['__args'].onaceptar({
							valor: resp.valor
						});
					} else {
						args.onaceptar({
							valor: resp.valor
						});
					}
				}
			}
		});
	}
};

/** 
 * Funcion que cierra el picker 
 */

$.cerrar = function(params) {
	var flag = ('flag' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['flag'] : '';
	if (OS_IOS) {
		/** 
		 * Al haber seleccionado la fecha, revisamos si es iphone, si lo es, escondemos la vista 
		 */
		if (flag == 'abierto') {
			require(WPATH('vars'))[args.__id]['flag'] = 'cerrado';
			$.vista.animate({
				bottom: -265,
				duration: 300
			});
		}
	}
};