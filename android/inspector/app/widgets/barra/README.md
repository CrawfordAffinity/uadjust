Widget Barra Inspeccion
La funcion de este widget es simular un barra tipo header.
Posee imagenes/textos que son utilizados en el proyecto principal.

@param info Muestra una vista a la derecha con icono de informacion
@param titulo Define el titulo de la pantalla
@param fondo Define el color de fondo de la barra (Definido segun los estilos de este widget)
@param modal Define la posicion del titulo en la barra
@param nroinicial En el caso de que sea una secuencia de pantallas, permite definir el numero en la que esta actualmente
@param nrofinal En el caso de que sea una secuencia de pantallas, permite definir el numero de pantallas que tiene esa secuencia
@param colortitulo Define el color del texto del titulo (Por defecto es blanco), la otra opcion es negro, al utilizar esta opcion se tiene que escribir literalmente la palabra negro
@param textoderecha Define el texto que se mostrara en la parte derecha de la barra
@param colortextoderecha Se define el color del texto que se mostrara en la parte derecha de la barra, las opciones disponibles son blanco, negro, azul
@param salir_insp Muestra una vista a la izquierda con icono de salir
@param continuar Muestra una vista a la derecha con icono color plomo de continuar
@param continuarazul Muestra una vista a la derecha con icono color azul de continuar
@param agregar_dano Muestra una vista a la derecha con icono de agregar dano
@param enviar_insp Muestra una vista a la derecha con texto de enviar todas (pantalla historial)
@param cerrar Muestra una vista a la izquierda con icono de cerrar
