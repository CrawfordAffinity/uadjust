exports.events = _.clone(Backbone.Events);
exports.ajaxkeys = {};
exports.variables = {};
exports.styles = {
	"images": {
		"mas_zoom": "/images/iC7C261216F75FB0B2EEDFEB678D6B75F.png",
		"menos_zoom": "/images/iFE1E6C4FA5DC4857A6CBF751E283DA6F.png",
		"camara_on": "/images/i0167D98FC7E04BBB9DDB764A1AA23EAD.png",
		"camara_off": "/images/iF6DA61D9235A335B8B38B9ADD7061672.png",
		"con_flash": "/images/i3A689C294CBA36F2146F0C662C228410.png",
		"girar_camara_off": "/images/i5643A0473451CADBA311DAFB8064F31F.png",
		"girar_camara_on": "/images/i3F0C90EC5153907734682B62CDCF515B.png"
	},
	"classes": {}
};
exports.fontello = {};