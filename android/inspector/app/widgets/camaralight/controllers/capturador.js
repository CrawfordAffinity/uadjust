var _bind4section = {};

var _activity;
if (OS_ANDROID) {
	_activity = $.capturador.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
if (_.isUndefined(require(WPATH('vars'))[args.__id])) require(WPATH('vars'))[args.__id] = {};
if (OS_ANDROID) {
	$.capturador.addEventListener('open', function(e) {});
}

/** 
 * Se inicia el modulo de camara 
 */

var camaramax = Ti.UI.createView({
	height: Ti.UI.FILL,
	left: 0,
	layout: 'composite',
	width: Ti.UI.FILL,
	top: 0,
	tipo: 'imagen'
});
/* main window must open here */
function camaracamaramax() {
	require('com.skypanther.picatsize').showCamera({
		success: function(event) {
			if (event.mediaType == require('com.skypanther.picatsize').MEDIA_TYPE_PHOTO) {
				camaramax.fireEvent("success", {
					valor: {
						error: false,
						cancel: false,
						data: event.media,
						type: 'photo'
					}
				});
			} else if (event.mediaType == require('com.skypanther.picatsize').MEDIA_TYPE_VIDEO) {
				camaramax.fireEvent("success", {
					valor: {
						error: false,
						cancel: false,
						data: event.media,
						type: 'video'
					}
				});
			}
		},
		cancel: function() {
			camaramax.fireEvent("cancel", {
				valor: {
					error: false,
					cancel: true,
					data: '',
					reason: 'cancelled'
				}
			});
		},
		error: function(error) {
			camaramax.fireEvent("error", {
				valor: {
					error: true,
					cancel: false,
					data: error,
					reason: error.error,
					type: 'camera'
				}
			});
		},
		autohide: false,
		showControls: false,
		targetWidth: 480,
		targetHeight: 640,
		saveToPhotoGallery: false,
		overlay: camaramax,
	});
}
require(WPATH('vars'))['_estadoflash_'] = 'auto';
require(WPATH('vars'))['_tipocamara_'] = 'trasera';
require(WPATH('vars'))['_hayflash_'] = true;
if (Ti.Media.hasCameraPermissions()) {
	camaracamaramax();
} else {
	Ti.Media.requestCameraPermissions(function(ercp) {
		if (ercp.success) {
			camaracamaramax();
		} else {
			camaramax.fireEvent("error", {
				valor: {
					error: true,
					cancel: false,
					data: 'nopermission',
					type: 'permission',
					reason: 'camera doesnt have permissions'
				}
			});
		}
	});
}
/** 
 * boton capturar fotografia 
 */
var vista2 = Titanium.UI.createView({
	height: '50dp',
	bottom: '10dp',
	layout: 'composite',
	width: '50dp',
	borderRadius: 25,
	backgroundColor: '#EE7F7E'
});
var vista3 = Titanium.UI.createView({
	height: Ti.UI.SIZE,
	layout: 'vertical'
});
if (OS_IOS) {
	var imagen = Ti.UI.createImageView({
		width: Ti.UI.FILL,
		image: WPATH('images/iF6DA61D9235A335B8B38B9ADD7061672.png'),
		id: imagen
	});
} else {
	var imagen = Ti.UI.createImageView({
		width: Ti.UI.FILL,
		image: WPATH('images/iF6DA61D9235A335B8B38B9ADD7061672.png'),
		id: imagen
	});
}
require('vars')['_imagen_original_'] = imagen.getImage();
require('vars')['_imagen_filtro_'] = 'original';
vista3.add(imagen);
vista2.add(vista3);

vista2.addEventListener('touchstart', function(e) {
	e.cancelBubble = true;
	var elemento = e.source;
	var imagen_imagen = 'camara_on';

	if (typeof imagen_imagen == 'string' && 'styles' in require(WPATH('a4w')) && imagen_imagen in require(WPATH('a4w')).styles['images']) {
		imagen_imagen = WPATH(require(WPATH('a4w')).styles['images'][imagen_imagen]);
	}
	imagen.setImage(imagen_imagen);

});

vista2.addEventListener('touchend', function(e) {
	e.cancelBubble = true;
	var elemento = e.source;
	/** 
	 * Comando que acciona el obtener la imagen que esta siendo vista en pantalla 
	 */
	require('com.skypanther.picatsize').takePicture();
	var imagen_imagen = 'camara_off';

	if (typeof imagen_imagen == 'string' && 'styles' in require(WPATH('a4w')) && imagen_imagen in require(WPATH('a4w')).styles['images']) {
		imagen_imagen = WPATH(require(WPATH('a4w')).styles['images'][imagen_imagen]);
	}
	imagen.setImage(imagen_imagen);

});
camaramax.add(vista2);
/** 
 * Boton reducir zoom 
 */
var vista4 = Titanium.UI.createView({
	height: '50dp',
	bottom: '10dp',
	left: '30dp',
	layout: 'composite',
	width: '50dp'
});
var vista5 = Titanium.UI.createView({
	height: Ti.UI.SIZE,
	layout: 'vertical'
});
if (OS_IOS) {
	var imagen2 = Ti.UI.createImageView({
		image: WPATH('images/iFE1E6C4FA5DC4857A6CBF751E283DA6F.png'),
		id: imagen2
	});
} else {
	var imagen2 = Ti.UI.createImageView({
		image: WPATH('images/iFE1E6C4FA5DC4857A6CBF751E283DA6F.png'),
		id: imagen2
	});
}
require('vars')['_imagen2_original_'] = imagen2.getImage();
require('vars')['_imagen2_filtro_'] = 'original';
vista5.add(imagen2);
vista4.add(vista5);

vista4.addEventListener('click', function(e) {
	e.cancelBubble = true;
	var elemento = e.source;
	/** 
	 * Recupera el ultimo valor del zoom que se aplico 
	 */
	var zoom = ('zoom' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['zoom'] : '';
	if (_.isNumber(zoom) && _.isNumber(0) && zoom > 0) {
		/** 
		 * Se le resta un 10% al valor que ten&#237;a para aplicar el efecto de zoom 
		 */
		zoom = zoom - 10;
		/** 
		 * Le quita un 10% de aumento visual a lo que se esta capturando a traves de la camara 
		 */
		var camaramax_zoom = zoom + '%';

		var modificarZoom = function(valor) {
			if (!isNaN(valor)) {
				require('com.skypanther.picatsize').setZoom(valor);
			} else if (valor.indexOf('%') != -1) {
				require('com.skypanther.picatsize').setZoomPercentage(parseInt(valor));
			}
		};
		modificarZoom(camaramax_zoom);
		modificarZoom = null;

		/** 
		 * Guarda el valor del zoom que se aplico recien 
		 */
		require(WPATH('vars'))[args.__id]['zoom'] = zoom;
	}
});
camaramax.add(vista4);
/** 
 * Boton ampliar zoom 
 */
var vista6 = Titanium.UI.createView({
	height: '50dp',
	bottom: '10dp',
	layout: 'composite',
	width: '50dp',
	right: '30dp'
});
var vista7 = Titanium.UI.createView({
	height: Ti.UI.SIZE,
	layout: 'vertical'
});
if (OS_IOS) {
	var imagen3 = Ti.UI.createImageView({
		image: WPATH('images/iC7C261216F75FB0B2EEDFEB678D6B75F.png'),
		id: imagen3
	});
} else {
	var imagen3 = Ti.UI.createImageView({
		image: WPATH('images/iC7C261216F75FB0B2EEDFEB678D6B75F.png'),
		id: imagen3
	});
}
require('vars')['_imagen3_original_'] = imagen3.getImage();
require('vars')['_imagen3_filtro_'] = 'original';
vista7.add(imagen3);
vista6.add(vista7);

vista6.addEventListener('click', function(e) {
	e.cancelBubble = true;
	var elemento = e.source;
	/** 
	 * Recupera el ultimo valor del zoom que se aplico 
	 */
	var zoom = ('zoom' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['zoom'] : '';
	if (_.isNumber(zoom) && _.isNumber(100) && zoom < 100) {
		/** 
		 * Se le resta un 10% al valor que ten&#237;a para aplicar el efecto de zoom 
		 */
		zoom = zoom + 10;
		/** 
		 * Le quita un 10% de aumento visual a lo que se esta capturando a traves de la camara 
		 */
		var camaramax_zoom = zoom + '%';

		var modificarZoom = function(valor) {
			if (!isNaN(valor)) {
				require('com.skypanther.picatsize').setZoom(valor);
			} else if (valor.indexOf('%') != -1) {
				require('com.skypanther.picatsize').setZoomPercentage(parseInt(valor));
			}
		};
		modificarZoom(camaramax_zoom);
		modificarZoom = null;

		/** 
		 * Guarda el valor del zoom que se aplico recien 
		 */
		require(WPATH('vars'))[args.__id]['zoom'] = zoom;
	}
});
camaramax.add(vista6);

camaramax.addEventListener('success', function(e) {
	e.cancelBubble = true;
	var elemento = e.valor;
	/** 
	 * Envia imagen capturada al padre de la instancia de este widget 
	 */

	if ('__args' in args) {
		args['__args'].onfotolista({
			foto: elemento.data
		});
	} else {
		args.onfotolista({
			foto: elemento.data
		});
	}
	/** 
	 * Esconde la camara y la vista ya que se capturo la imagen 
	 */
	var camaramax_esconder = 'camara';

	var esconderVistaCamara = function(tipo) {
		if (tipo == 'camara' || tipo == 'cam' || tipo == 'Camara') {
			require('com.skypanther.picatsize').hideCamera();
		} else if (tipo == 'vista' || tipo == 'view') {
			camaramax.hide();
		}
	};
	esconderVistaCamara(camaramax_esconder);

	var camaramax_esconder = 'vista';

	var esconderVistaCamara = function(tipo) {
		if (tipo == 'camara' || tipo == 'cam' || tipo == 'Camara') {
			require('com.skypanther.picatsize').hideCamera();
		} else if (tipo == 'vista' || tipo == 'view') {
			camaramax.hide();
		}
	};
	esconderVistaCamara(camaramax_esconder);

	/** 
	 * Limpiamos variable de capturador para limpiar memoria 
	 */
	elemento = null;
});
var vista8 = Titanium.UI.createView({
	height: '10dp',
	layout: 'vertical',
	top: '0dp',
	elevation: 40
});
camaramax.add(vista8);

function Androidback_capturador(e) {
	/** 
	 * Anulamos boton back de android 
	 */

	e.cancelBubble = true;
	var elemento = e.source;

}

(function() {
	/** 
	 * Se inicializa el numero del zoom en 0 
	 */
	var numero = 0;
	/** 
	 * Se guarda la variable con el valor de numero para poder utlizarla en el zoom 
	 */
	require(WPATH('vars'))[args.__id]['zoom'] = numero;
})();

if (OS_IOS || OS_ANDROID) {
	$.capturador.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this widget controller
		try {
			//require(WPATH('vars'))[args.__id]=null;
			args = null;
			if (OS_ANDROID) {
				abx = null;
			}
			if ($item) $item = null;
			if (_my_events) {
				for (_ev_tmp in _my_events) {
					try {
						if (_ev_tmp.indexOf('_web') != -1) {
							Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
						} else {
							Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
						}
					} catch (err10) {}
				}
				_my_events = null;
				//delete _my_events;
			}
		} catch (err10) {}
		if (_out_vars) {
			var _ev_tmp;
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			_out_vars = null;
			//delete _out_vars;
		}
	});
}