/** 
 * Widget camara light
 * Control que permite capturar fotografias precomprimidas y aplicar zoom si es necesario, optimizando la memoria ram del equipo.
 * No requiere parametros. 
 */
var _bind4section = {};

var args = arguments[0] || {};

/** 
 * Funcion que inicializa el widget 
 */

$.init = function(params) {
	for (var tobe in params) args[tobe] = params[tobe];
	if ('__id' in params) require(WPATH('vars'))[params.__id] = {};
};

/** 
 * Funcion que abre capturador de camara 
 */

$.disparar = function(params) {
	/** 
	 * Redireccion a la pantalla que ocupa una vista para capturar la foto 
	 */
	Widget.createController("capturador", {
		'_open': 'false',
		'__args': (typeof args !== 'undefined') ? args : __args
	}).getView();
};