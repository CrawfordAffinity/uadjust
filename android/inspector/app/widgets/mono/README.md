Widget Mono-Personaje
Muestra diferentes tipos de personajes con un mensaje de texto personalizable

@param visible Indica visibilidad de widget
@param tipo Indica tipo de personaje: tip, info, sorry
