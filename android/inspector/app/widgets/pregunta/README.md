Widget Pregunta
Este control representa una vista que permite escoger entre dos opciones: si y no.

@param top Indica el margen superior de este widget
@param titulo Indica el titulo mostrado sobre el personaje
@param texto Indica el texto de la pregunta
@param si Indica el texto para el boton SI
@param no Indica el texto para el boton NO
