/** 
 * Widget Registro Header
 * Control que muestra un encabezado y botones de avance (nro de pantalla vs cantidad total)
 * 
 * @param titulo Indica texto para titulo de header
 * @param avance Indica texto para pag/total 
 */
var _bind4section = {};

var args = arguments[0] || {};

function Click_vista(e) {

	e.cancelBubble = true;
	var elemento = e.source;

	if ('__args' in args) {
		args['__args'].onclick({});
	} else {
		args.onclick({});
	}

}
/** 
 * Funcion que inicializa el widget 
 */

$.init = function(params) {
	for (var tobe in params) args[tobe] = params[tobe];
	if ('__id' in params) require(WPATH('vars'))[params.__id] = {};
	if (!_.isUndefined(params.titulo)) {
		/** 
		 * Verificamos si existe un titulo y avance para modificar en la vista e indicar al usuario en que pantalla esta 
		 */
		$.PARTEPas.setText(params.titulo);

	}
	if (!_.isUndefined(params.avance)) {
		$.label.setText(params.avance);

	}
};