exports.events = _.clone(Backbone.Events);
exports.ajaxkeys = {};
exports.variables = {};
exports.styles = {
	"images": {
		"linea": "/images/i5B4F6784F2890644E0A60D366A74C0EE.png"
	},
	"classes": {
		"estilo11": {
			"color": "#2d9edb",
			"font": {
				"fontWeight": "bold",
				"fontSize": "15dp"
			}
		},
		"#imagen": {
			"image": "WPATH('images/i5B4F6784F2890644E0A60D366A74C0EE.png')"
		}
	}
};
exports.fontello = {};