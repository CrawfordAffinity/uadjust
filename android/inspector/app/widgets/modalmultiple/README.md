Widget Modal Multiple
Control que muestra un selector tipo combobox con mas un valor simultaneo seleccionable a traves de una sub pantalla con los items y switches.

@param titulo Indica el titulo del campo del combobox (label)
@param hint Indica el texto para cuando aun no se han seleccionado items en el combobox
@param top Indica el margen superior para el contenedor del combobox
@param left Indica el margen izquierdo para el contenedor del combobox
@param right Indica el margen derecho para el contenedor del combobox
@param bottom Indica el margen inferior para el contenedor del combobox
@param ancho Indica el ancho del contenedor del combobox
@param alto Indica el alto del contenedor del combobox
@param cargando Mensaje a mostrar mienstras se carga contenido de items en memoria
