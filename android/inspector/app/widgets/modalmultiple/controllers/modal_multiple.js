var _bind4section = {};

var _activity;
if (OS_ANDROID) {
	_activity = $.modal_multiple.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
if (_.isUndefined(require(WPATH('vars'))[args.__id])) require(WPATH('vars'))[args.__id] = {};
if (OS_ANDROID) {
	$.modal_multiple.addEventListener('open', function(e) {});
}
$.modal_multiple.orientationModes = [Titanium.UI.PORTRAIT];

/** 
 * Genera una consulta al modelo temp_multiple filtrando por id_instancia, la respuesta a la consulta se guarda en una variable llamada lista 
 */
var consultarModelo2_like = function(search) {
	if (typeof search !== 'string' || this === null) {
		return false;
	}
	search = search.replace(new RegExp("([\\.\\\\\\+\\*\\?\\[\\^\\]\\$\\(\\)\\{\\}\\=\\!\\<\\>\\|\\:\\-])", "g"), "\\$1");
	search = search.replace(/%/g, '.*').replace(/_/g, '.');
	return RegExp('^' + search + '$', 'gi').test(this);
};
var consultarModelo2_filter = function(coll) {
	var filtered = coll.filter(function(m) {
		var _tests = [],
			_all_true = false,
			model = m.toJSON();
		_tests.push((model.idinstancia == '0'));
		var _all_true_s = _.uniq(_tests);
		_all_true = (_all_true_s.length == 1 && _all_true_s[0] == true) ? true : false;
		return _all_true;
	});
	filtered = _.toArray(filtered);
	return filtered;
};
var consultarModelo2_update = function(e) {};
_.defer(function() {
	Widget.Collections.temp_multiple.fetch();
});
Widget.Collections.temp_multiple.on('add change delete', function(ee) {
	consultarModelo2_update(ee);
});
var consultarModelo2_transform = function(model) {
	var fila = model.toJSON();
	if (fila.estado == 1 || fila.estado == '1') {
		fila.activo = true;
	} else {
		fila.activo = false;
	}
	return fila;
};
Widget.Collections.temp_multiple.fetch();


$.widgetBarra.init({
	titulo: '',
	__id: 'ALL138747237',
	textoderecha: 'Guardar',
	fondo: 'fondoblanco',
	top: 0,
	colortitulo: 'negro',
	modal: '',
	onpresiono: Presiono_widgetBarra,
	colortextoderecha: 'negro'
});

function Presiono_widgetBarra(e) {

	var evento = e;
	/** 
	 * Recupera la variable id_instacia con el id del widget 
	 */
	var id_instancia = ('id_instancia' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['id_instancia'] : '';
	/** 
	 * Consulta el modelo temp_multiple filtrando por idinstancia y estado en 1 
	 */
	if (Widget.Collections.temp_multiple.models.length == 0) Widget.Collections.temp_multiple.fetch();
	var consultarModelo3_i = Widget.Collections.temp_multiple;
	//filtramos modelos segun consulta (where futuro linkeado) y armamos respuesta
	var consultarModelo3_i_where = [],
		activos = [],
		struct = {}
	for (var reg_consultarModelo3 = 0; reg_consultarModelo3 < consultarModelo3_i.models.length; reg_consultarModelo3++) {
		if ('idinstancia' in consultarModelo3_i.models[reg_consultarModelo3].attributes && consultarModelo3_i.models[reg_consultarModelo3].attributes['idinstancia'] == id_instancia && 'estado' in consultarModelo3_i.models[reg_consultarModelo3].attributes && consultarModelo3_i.models[reg_consultarModelo3].attributes['estado'] == 1) {
			consultarModelo3_i_where.push(Widget.Collections.temp_multiple.at(reg_consultarModelo3));
			struct = {};
			for (var key in consultarModelo3_i.models[reg_consultarModelo3].attributes) {
				struct[key] = consultarModelo3_i.models[reg_consultarModelo3].attributes[key];
			}
			activos.push(struct);
		}
	}
	/** 
	 * Convierte el campo amostrar por label 
	 */
	var resp = [];
	_.each(activos, function(fila, pos) {
		var new_row = {};
		_.each(fila, function(x, llave) {
			var newkey = '';
			if (llave == 'amostrar') newkey = 'label';
			if (llave == 'valor') newkey = 'valor';
			if (newkey != '') new_row[newkey] = fila[llave];
		});
		resp.push(new_row);
	});
	/** 
	 * Obtiene el campo valor que esta en la variable resp y los separa con una coma 
	 */
	var lista = _.pluck(resp, 'valor').join(',');
	/** 
	 * Retorna los valores y la data a la pantalla que utiliza el widget 
	 */
	if ('__args' in args) {
		args['__args'].oncerrar({
			valores: lista,
			data: resp
		});
	} else {
		args.oncerrar({
			valores: lista,
			data: resp
		});
	}
	/** 
	 * Cierra la pantalla 
	 */
	$.modal_multiple.close();

}

function Change_ID_987810158(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	/** 
	 * Hace una consulta al modelo temp_multiple, filtrando con el id de la fila 
	 */
	if (Widget.Collections.temp_multiple.models.length == 0) Widget.Collections.temp_multiple.fetch();
	var consultarModelo4_i = Widget.Collections.temp_multiple;
	//filtramos modelos segun consulta (where futuro linkeado) y armamos respuesta
	var consultarModelo4_i_where = [],
		actual = [],
		struct = {}
	for (var reg_consultarModelo4 = 0; reg_consultarModelo4 < consultarModelo4_i.models.length; reg_consultarModelo4++) {
		if ('id' in consultarModelo4_i.models[reg_consultarModelo4].attributes && consultarModelo4_i.models[reg_consultarModelo4].attributes['id'] == e.source.parent._idfila) {
			consultarModelo4_i_where.push(Widget.Collections.temp_multiple.at(reg_consultarModelo4));
			struct = {};
			for (var key in consultarModelo4_i.models[reg_consultarModelo4].attributes) {
				struct[key] = consultarModelo4_i.models[reg_consultarModelo4].attributes[key];
			}
			actual.push(struct);
		}
	}
	if (Ti.App.deployType != 'production') console.log('switch source cambiado dice (padre)', {
		"basedatos": actual,
		"elemento": e.source.parent._idfila
	});
	if (elemento == true || elemento == 'true') {
		/** 
		 * en el caso que el switch sea true, modifica el modelo en estado 1 
		 */
		/** 
		 * Modifica el estado del id seleccionado 
		 */
		var db = Widget.Collections.temp_multiple;
		if (consultarModelo4_i_where.length > 0) {
			for (var reg_modificarModelo = 0; reg_modificarModelo < consultarModelo4_i_where.length; reg_modificarModelo++) {
				consultarModelo4_i_where[reg_modificarModelo].set({
					estado: '1'
				}, {
					silent: true
				});
				consultarModelo4_i_where[reg_modificarModelo].save();
			}
		}
	} else {
		/** 
		 * Modifica el estado del id seleccionado 
		 */
		var db = Widget.Collections.temp_multiple;
		if (consultarModelo4_i_where.length > 0) {
			for (var reg_modificarModelo2 = 0; reg_modificarModelo2 < consultarModelo4_i_where.length; reg_modificarModelo2++) {
				consultarModelo4_i_where[reg_modificarModelo2].set({
					estado: '0'
				}, {
					silent: true
				});
				consultarModelo4_i_where[reg_modificarModelo2].save();
			}
		}
	}
	elemento = null;

}

(function() {
	/** 
	 * recupera la variable params creada en la pantalla inicio de este widget 
	 */
	var params = ('params' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['params'] : '';
	/** 
	 * Modifica el valor de la idinstacia en el modelo que se esta usando, y lo ordena con amostrar 
	 */
	consultarModelo2_filter = function(coll) {
		var filtered = coll.filter(function(m) {
			var _tests = [],
				_all_true = false,
				model = m.toJSON();
			_tests.push((model.idinstancia == params.__id));
			var _all_true_s = _.uniq(_tests);
			_all_true = (_all_true_s.length == 1 && _all_true_s[0] == true) ? true : false;
			return _all_true;
		});
		filtered = _.toArray(filtered);
		var ordered = _.sortBy(filtered, 'amostrar');
		return ordered;
	};
	_.defer(function() {
		Widget.Collections.temp_multiple.fetch();
	});
	if (!_.isUndefined(params.titulo)) {
		/** 
		 * Actualiza el titulo del header en el caso de que se le haya pasado el valor titulo 
		 */
		$.widgetBarra.update({
			titulo: params.titulo.toUpperCase()
		});
	}
	if (!_.isUndefined(params.subtitulo)) {
		/** 
		 * Cambia el texto en el caso de que el parametro subtitulo exista 
		 */
		$.Seleccioneel.setText(params.subtitulo);

	}
})();

if (OS_IOS || OS_ANDROID) {
	$.modal_multiple.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this widget controller
		try {
			//require(WPATH('vars'))[args.__id]=null;
			args = null;
			if (OS_ANDROID) {
				abx = null;
			}
			if ($item) $item = null;
			if (_my_events) {
				for (_ev_tmp in _my_events) {
					try {
						if (_ev_tmp.indexOf('_web') != -1) {
							Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
						} else {
							Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
						}
					} catch (err10) {}
				}
				_my_events = null;
				//delete _my_events;
			}
		} catch (err10) {}
		if (_out_vars) {
			var _ev_tmp;
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			_out_vars = null;
			//delete _out_vars;
		}
	});
}