/** 
 * Widget Modal Multiple
 * Control que muestra un selector tipo combobox con mas un valor simultaneo seleccionable a traves de una sub pantalla con los items y switches.
 * 
 * @param titulo Indica el titulo del campo del combobox (label)
 * @param hint Indica el texto para cuando aun no se han seleccionado items en el combobox
 * @param top Indica el margen superior para el contenedor del combobox
 * @param left Indica el margen izquierdo para el contenedor del combobox
 * @param right Indica el margen derecho para el contenedor del combobox
 * @param bottom Indica el margen inferior para el contenedor del combobox
 * @param ancho Indica el ancho del contenedor del combobox
 * @param alto Indica el alto del contenedor del combobox
 * @param cargando Mensaje a mostrar mienstras se carga contenido de items en memoria 
 */
var _bind4section = {};

var args = arguments[0] || {};

function Click_vista2(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	/** 
	 * Envia a la pantalla del modal 
	 */
	Widget.createController("modal_multiple", {
		'__args': (typeof args !== 'undefined') ? args : __args
	}).getView().open();

}
/** 
 * Funcion que inicializa el widget 
 */
$.init = function(params) {
	for (var tobe in params) args[tobe] = params[tobe];
	if ('__id' in params) require(WPATH('vars'))[params.__id] = {};
	require(WPATH('vars'))[args.__id]['params'] = params;
	if (!_.isUndefined(params.titulo)) {
		/** 
		 * Revisamos los parametros al iniciar el widget y modificamos lo que sea pertinente 
		 */
		/** 
		 * Cambia el texto del titulo con el valor que se envia desde la pantalla que utiliza el widget 
		 */
		$.Label.setText(params.titulo);

	}
	if (!_.isUndefined(params.hint)) {
		/** 
		 * Cambia el texto de hint con el valor que se envia desde la pantalla que utiliza el widget 
		 */
		$.Valorblanco.setText(params.hint);

		/** 
		 * Mostramos hint 
		 */
		var Valorblanco_visible = true;

		if (Valorblanco_visible == 'si') {
			Valorblanco_visible = true;
		} else if (Valorblanco_visible == 'no') {
			Valorblanco_visible = false;
		}
		$.Valorblanco.setVisible(Valorblanco_visible);

		/** 
		 * Ocultamos valor 
		 */
		var Valorseleccionado_visible = false;

		if (Valorseleccionado_visible == 'si') {
			Valorseleccionado_visible = true;
		} else if (Valorseleccionado_visible == 'no') {
			Valorseleccionado_visible = false;
		}
		$.Valorseleccionado.setVisible(Valorseleccionado_visible);

	}
	if (!_.isUndefined(params.top)) {
		/** 
		 * Define el top que tendra la vista 
		 */
		$.vista.setTop(params.top);

	}
	if (!_.isUndefined(params.left)) {
		/** 
		 * Define el left que tendra la vista 
		 */
		$.vista.setLeft(params.left);

	}
	if (!_.isUndefined(params.subleft)) {
		/** 
		 * Define el left que tendra que tendra el titulo 
		 */
		$.Label.setLeft(params.subleft);

	}
	if (!_.isUndefined(params.right)) {
		/** 
		 * Define el right que tendra la vista 
		 */
		$.vista.setRight(params.right);

	}
	if (!_.isUndefined(params.bottom)) {
		/** 
		 * Define el bottom que tendra la vista 
		 */
		$.vista.setBottom(params.bottom);

	}
	if (!_.isUndefined(params.ancho)) {
		/** 
		 * Define el ancho que tendra la vista 
		 */
		var vista_ancho = params.ancho;

		if (vista_ancho == '*') {
			vista_ancho = Ti.UI.FILL;
		} else if (vista_ancho == '-') {
			vista_ancho = Ti.UI.SIZE;
		} else if (!isNaN(vista_ancho)) {
			vista_ancho = vista_ancho + 'dp';
		}
		$.vista.setWidth(vista_ancho);

	}
	if (!_.isUndefined(params.alto)) {
		/** 
		 * Define el alto que tendra la vista 
		 */
		var vista_alto = params.alto;

		if (vista_alto == '*') {
			vista_alto = Ti.UI.FILL;
		} else if (vista_alto == '-') {
			vista_alto = Ti.UI.SIZE;
		} else if (!isNaN(vista_alto)) {
			vista_alto = vista_alto + 'dp';
		}
		$.vista.setHeight(vista_alto);

	} else if (!_.isUndefined(params.__id)) {
		/** 
		 * Si tenemos id de instancia de widget, guardamos id de instancia en var.&#160; Establecemos scope para datos de modelo local 
		 */
		/** 
		 * Guarda la variable id_instacia con el id del widget 
		 */
		require(WPATH('vars'))[args.__id]['id_instancia'] = params.__id;
	}
	/** 
	 * Llama el evento afterinit por si se requiere ejecutar algo despu&#233;s de que haya cargado el widget 
	 */
	if ('__args' in args) {
		args['__args'].onafterinit({});
	} else {
		args.onafterinit({});
	}
};

/** 
 * Funcion que actualiza los datos del selector
 * 
 * @param data Recibe array de estructuras con campos: label (string), valor (any) y estado (int 0,1) para seleccionar
 * @param valor Si se define, oculta hint y muestra 
 */
$.update = function(params) {
	if (!_.isUndefined(params.data)) {
		/** 
		 * Recibimos datos en caso de que se hayan mandado datos desde la pantalla que ocupa el widget 
		 */
		/** 
		 * Se guardan los datos que trae lo que venga de la pantalla que utiliza el widget 
		 */
		require(WPATH('vars'))[args.__id]['data'] = params.data;
		if (_.isArray(params.data)) {
			/** 
			 * Si los datos que se traen de la pantalla que utiliza el widget es un array 
			 */
			/** 
			 * Recupera la variable id_instacia con el id del widget 
			 */
			var id_instancia = ('id_instancia' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['id_instancia'] : '';
			/** 
			 * Se eliminan los datos que esten cargados en el modelo temp_multiple 
			 */
			/** 
			 * Se eliminan los datos que esten cargados en el modelo temp_multiple 
			 */
			Widget.Collections.temp_multiple.fetch();
			var eliminarModelo_i = Widget.Collections.temp_multiple;
			//filtramos a borrar
			var to_delete_eliminarModelo = [];
			for (var reg_eliminarModelo = 0; reg_eliminarModelo < eliminarModelo_i.models.length; reg_eliminarModelo++) {
				if ('idinstancia' in eliminarModelo_i.models[reg_eliminarModelo].attributes && eliminarModelo_i.models[reg_eliminarModelo].attributes['idinstancia'] === id_instancia) {
					to_delete_eliminarModelo.push(Widget.Collections.temp_multiple.at(reg_eliminarModelo));
				}
			}
			//borramos marcados
			for (var ii_eliminarModelo in to_delete_eliminarModelo) {
				to_delete_eliminarModelo[ii_eliminarModelo].destroy();
				Widget.Collections.temp_multiple.remove(to_delete_eliminarModelo[ii_eliminarModelo], {
					silent: true
				});
				Widget.Collections.temp_multiple.remove(to_delete_eliminarModelo[ii_eliminarModelo]);
			}
			eliminarModelo_i.trigger('remove');
			var params_data = params.data;
			var insertarModelo_m = Widget.Collections.instance("temp_multiple");
			_.each(params_data, function(insertarModelo_fila, pos) {
				var insertarModelo_modelo = Widget.createModel('temp_multiple', {
					valor: insertarModelo_fila.valor,
					idinstancia: id_instancia,
					estado: insertarModelo_fila.estado,
					amostrar: insertarModelo_fila.label
				});
				insertarModelo_m.add(insertarModelo_modelo, {
					silent: true
				});
				insertarModelo_modelo.save();
			});
			_.defer(function() {
				Widget.Collections.instance("temp_multiple").fetch();
			});
		}
	}
	if (!_.isUndefined(params.valor)) {
		/** 
		 * Revisamos si el parametro valor existe 
		 */
		if ((_.isObject(params.valor) || _.isString(params.valor)) && _.isEmpty(params.valor)) {
			/** 
			 * Revisamos si el string de valor esta vacio 
			 */
			/** 
			 * Mostramos hint 
			 */
			var Valorblanco_visible = true;

			if (Valorblanco_visible == 'si') {
				Valorblanco_visible = true;
			} else if (Valorblanco_visible == 'no') {
				Valorblanco_visible = false;
			}
			$.Valorblanco.setVisible(Valorblanco_visible);

			/** 
			 * Ocultamos valor 
			 */
			var Valorseleccionado_visible = false;

			if (Valorseleccionado_visible == 'si') {
				Valorseleccionado_visible = true;
			} else if (Valorseleccionado_visible == 'no') {
				Valorseleccionado_visible = false;
			}
			$.Valorseleccionado.setVisible(Valorseleccionado_visible);

		} else {
			/** 
			 * Mostramos valor indicado 
			 */
			$.Valorseleccionado.setText(params.valor);

			var Valorseleccionado_visible = true;

			if (Valorseleccionado_visible == 'si') {
				Valorseleccionado_visible = true;
			} else if (Valorseleccionado_visible == 'no') {
				Valorseleccionado_visible = false;
			}
			$.Valorseleccionado.setVisible(Valorseleccionado_visible);

			/** 
			 * Ocultamos hint 
			 */
			var Valorblanco_visible = false;

			if (Valorblanco_visible == 'si') {
				Valorblanco_visible = true;
			} else if (Valorblanco_visible == 'no') {
				Valorblanco_visible = false;
			}
			$.Valorblanco.setVisible(Valorblanco_visible);

		}
	} else {
		/** 
		 * Recuperamos los datos que fueron mandados desde la pantalla que utiliza el widget 
		 */
		var data = ('data' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['data'] : '';
		/** 
		 * Creamos una variable local que guardara los valores escogidos 
		 */
		require(WPATH('vars'))[args.__id]['elegidos'] = '';
		/** 
		 * Recupera la variable id_instacia con el id del widget 
		 */
		var id_instancia = ('id_instancia' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['id_instancia'] : '';
		/** 
		 * Consultamos el modelo temp_multiple, filtrando por la id_instancia del widget y ordenados por el valor de amostrar de manera ascendente. Y el resultadoes almacenado en una variable llamada datos 
		 */
		if (Widget.Collections.temp_multiple.models.length == 0) Widget.Collections.temp_multiple.fetch();
		var consultarModelo_i = Widget.Collections.temp_multiple;
		//filtramos modelos segun consulta (where futuro linkeado) y armamos respuesta
		var consultarModelo_i_where = [],
			datos = [],
			struct = {}
		for (var reg_consultarModelo = 0; reg_consultarModelo < consultarModelo_i.models.length; reg_consultarModelo++) {
			if ('idinstancia' in consultarModelo_i.models[reg_consultarModelo].attributes && consultarModelo_i.models[reg_consultarModelo].attributes['idinstancia'] == id_instancia) {
				consultarModelo_i_where.push(Widget.Collections.temp_multiple.at(reg_consultarModelo));
				struct = {};
				for (var key in consultarModelo_i.models[reg_consultarModelo].attributes) {
					struct[key] = consultarModelo_i.models[reg_consultarModelo].attributes[key];
				}
				datos.push(struct);
			}
		}
		/** 
		 * Recuperamos la variable elegidos recien creada 
		 */
		var elegidos = ('elegidos' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['elegidos'] : '';
		var item_index = 0;
		_.each(datos, function(item, item_pos, item_list) {
			item_index += 1;
			if ((_.isObject(elegidos) || _.isString(elegidos)) && _.isEmpty(elegidos)) {
				/** 
				 * Revisamos si la variable elegidos esta vacia 
				 */
				if (item.estado == 1 || item.estado == '1') {
					/** 
					 * Revisamos cada item que este en la variable datos 
					 */
					/** 
					 * Almacena el primer nombre del item 
					 */
					elegidos = item.amostrar;
				}
			} else {
				if (item.estado == 1 || item.estado == '1') {
					/** 
					 * En caso de que exista mas de un valor en elegidos, se va concatenando los nombres 
					 */
					elegidos += ' + ' + item.amostrar;
				}
			}
		});
		/** 
		 * Se actualiza la variable con el/los nombres que estaban activos en los datos 
		 */
		require(WPATH('vars'))[args.__id]['elegidos'] = elegidos;
		/** 
		 * Mostramos valor procesado. Cambiando el texto del valor seleccionado con la variable de elegidos (que traia todos los items activos) y dejando el texto visible 
		 */
		$.Valorseleccionado.setText(elegidos);

		var Valorseleccionado_visible = true;

		if (Valorseleccionado_visible == 'si') {
			Valorseleccionado_visible = true;
		} else if (Valorseleccionado_visible == 'no') {
			Valorseleccionado_visible = false;
		}
		$.Valorseleccionado.setVisible(Valorseleccionado_visible);

		if ((_.isObject(elegidos) || _.isString(elegidos)) && _.isEmpty(elegidos)) {
			/** 
			 * En el caso de que no hayan items elegidos se muestra el hint y se oculta el valor 
			 */
			/** 
			 * Mostramos hint 
			 */
			var Valorblanco_visible = true;

			if (Valorblanco_visible == 'si') {
				Valorblanco_visible = true;
			} else if (Valorblanco_visible == 'no') {
				Valorblanco_visible = false;
			}
			$.Valorblanco.setVisible(Valorblanco_visible);

			/** 
			 * Ocultamos valor 
			 */
			var Valorseleccionado_visible = false;

			if (Valorseleccionado_visible == 'si') {
				Valorseleccionado_visible = true;
			} else if (Valorseleccionado_visible == 'no') {
				Valorseleccionado_visible = false;
			}
			$.Valorseleccionado.setVisible(Valorseleccionado_visible);

		} else {
			/** 
			 * Ocultamos hint 
			 */
			var Valorblanco_visible = false;

			if (Valorblanco_visible == 'si') {
				Valorblanco_visible = true;
			} else if (Valorblanco_visible == 'no') {
				Valorblanco_visible = false;
			}
			$.Valorblanco.setVisible(Valorblanco_visible);

			/** 
			 * Mostramos valor 
			 */
			var Valorseleccionado_visible = true;

			if (Valorseleccionado_visible == 'si') {
				Valorseleccionado_visible = true;
			} else if (Valorseleccionado_visible == 'no') {
				Valorseleccionado_visible = false;
			}
			$.Valorseleccionado.setVisible(Valorseleccionado_visible);

		}
	}
};

/** 
 * Funcion que limpia las variables y contenidos de la memoria luego de utilizar widget 
 */
$.limpiar = function(params) {
	/** 
	 * Limpia variables y contenidos de memoria (solo al cerrar pantalla) 
	 */
	var params = ('params' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['params'] : '';
	Widget.Collections.temp_multiple.fetch();
	var eliminarModelo2_i = Widget.Collections.temp_multiple;
	//filtramos a borrar
	var to_delete_eliminarModelo2 = [];
	for (var reg_eliminarModelo2 = 0; reg_eliminarModelo2 < eliminarModelo2_i.models.length; reg_eliminarModelo2++) {
		if ('idinstancia' in eliminarModelo2_i.models[reg_eliminarModelo2].attributes && eliminarModelo2_i.models[reg_eliminarModelo2].attributes['idinstancia'] === params.__id) {
			to_delete_eliminarModelo2.push(Widget.Collections.temp_multiple.at(reg_eliminarModelo2));
		}
	}
	//borramos marcados
	for (var ii_eliminarModelo2 in to_delete_eliminarModelo2) {
		to_delete_eliminarModelo2[ii_eliminarModelo2].destroy();
		Widget.Collections.temp_multiple.remove(to_delete_eliminarModelo2[ii_eliminarModelo2], {
			silent: true
		});
		Widget.Collections.temp_multiple.remove(to_delete_eliminarModelo2[ii_eliminarModelo2]);
	}
	eliminarModelo2_i.trigger('remove');
};