/** 
 * Widget Picker Horas
 * Control para elegir un horario en disponibilidad
 * 
 * @param mins Si true, agrega los minutos al selector, false los omite 
 */
var _bind4section = {};

var args = arguments[0] || {};



function Change_picker(e) {

	e.cancelBubble = true;
	var elemento = e;
	var _columna = e.columnIndex;
	var columna = e.columnIndex + 1;
	var _fila = e.rowIndex;
	var fila = e.rowIndex + 1;
	/** 
	 * Obtenemos el texto del picker y guardamos en variable 
	 */
	desde_sel = elemento.row.title;
	require(WPATH('vars'))[args.__id]['desde_sel'] = desde_sel;
	var hasta_sel = ('hasta_sel' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['hasta_sel'] : '';
	/** 
	 * Notificamos a padre de instancia en evento change 
	 */

	if ('__args' in args) {
		args['__args'].onchange({
			desde: desde_sel,
			hasta: hasta_sel
		});
	} else {
		args.onchange({
			desde: desde_sel,
			hasta: hasta_sel
		});
	}

}


function Change_picker3(e) {

	e.cancelBubble = true;
	var elemento = e;
	var _columna = e.columnIndex;
	var columna = e.columnIndex + 1;
	var _fila = e.rowIndex;
	var fila = e.rowIndex + 1;
	/** 
	 * Obtenemos el texto del picker y guardamos en variable 
	 */
	hasta_sel = elemento.row.title;
	require(WPATH('vars'))[args.__id]['hasta_sel'] = hasta_sel;
	var desde_sel = ('desde_sel' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['desde_sel'] : '';
	/** 
	 * Notificamos a padre de instancia en evento change 
	 */

	if ('__args' in args) {
		args['__args'].onchange({
			desde: desde_sel,
			hasta: hasta_sel
		});
	} else {
		args.onchange({
			desde: desde_sel,
			hasta: hasta_sel
		});
	}

}
/** 
 * Funcion para inicializar widget 
 */

$.init = function(params) {
	for (var tobe in params) args[tobe] = params[tobe];
	if ('__id' in params) require(WPATH('vars'))[params.__id] = {};
	/** 
	 * Seteamos variable en vacio 
	 */
	require(WPATH('vars'))[args.__id]['mostrar_mins'] = '';
	if (!_.isUndefined(params.mins)) {
		require(WPATH('vars'))[args.__id]['mostrar_mins'] = ':00';
	}
	/** 
	 * Seteamos valores default en 00 
	 */
	var mostrar_mins = ('mostrar_mins' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['mostrar_mins'] : '';
	require(WPATH('vars'))[args.__id]['desde_sel'] = '00';
	require(WPATH('vars'))[args.__id]['hasta_sel'] = '00';
	var desde_index = 0;
	_.each('00,01,02,03,04,05,06,07,08,09,10,11,12,13,14,15,16,17,18,19,20,21,22,23'.split(','), function(desde, desde_pos, desde_list) {
		desde_index += 1;
		if (Ti.App.deployType != 'production') console.log('registro desde', {
			"asd": desde
		});
		var filaPicker = Titanium.UI.createPickerRow({
			value: desde,
			title: desde + mostrar_mins
		});
		$.columnaPicker.addRow(filaPicker);
	});
	var hasta_index = 0;
	_.each('00,01,02,03,04,05,06,07,08,09,10,11,12,13,14,15,16,17,18,19,20,21,22,23'.split(','), function(hasta, hasta_pos, hasta_list) {
		hasta_index += 1;
		var filaPicker2 = Titanium.UI.createPickerRow({
			value: hasta,
			title: hasta + mostrar_mins
		});
		$.columnaPicker2.addRow(filaPicker2);
	});
};

/** 
 * Funcion para ajustar parametros de widget
 * 
 * @param desde Indica desde que hora mostrar el selector
 * @param hasta Indica hasta que hora mostrar el selector 
 */

$.set = function(params) {
	if (!_.isUndefined(params.desde)) {
		/** 
		 * Seteamos valor seleccionado de columna desde 
		 */
		/** 
		 * Obtenemos el id del picker y fijamos el valor obtenido desde el parametro 
		 */
		var picker;
		picker = $.picker;
		var filas = picker.getColumns()[0].getRows();
		var hast_index = 0;
		_.each(filas, function(hast, hast_pos, hast_list) {
			hast_index += 1;
			if (params.desde == parseInt(hast.value)) {
				picker.setSelectedRow(0, hast_pos);
			}
		});
	}
	if (!_.isUndefined(params.hasta)) {
		/** 
		 * Seteamos valor seleccionado de columna hasta 
		 */
		/** 
		 * Obtenemos el id del picker y fijamos el valor obtenido desde el parametro 
		 */
		var picker;
		picker = $.picker3;
		var filas = picker.getColumns()[0].getRows();
		var des_index = 0;
		_.each(filas, function(des, des_pos, des_list) {
			des_index += 1;
			if (params.hasta == parseInt(des.value)) {
				picker.setSelectedRow(0, des_pos);
			}
		});
	}
};