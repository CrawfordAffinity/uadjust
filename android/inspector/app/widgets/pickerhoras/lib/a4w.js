exports.events = _.clone(Backbone.Events);
exports.ajaxkeys = {};
exports.variables = {};
exports.styles = {
	"images": {},
	"classes": {
		"estilo11": {
			"color": "#2d9edb",
			"font": {
				"fontWeight": "bold",
				"fontSize": "15dp"
			}
		}
	}
};
exports.fontello = {};