var _bind4section = {};
var _list_templates = {
	"elemento": {
		"Label": {
			"text": "{valor}",
			"_data": "{data}"
		},
		"vista4": {}
	}
};

var _activity;
if (OS_ANDROID) {
	_activity = $.TIPO_MODAL.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
if (_.isUndefined(require(WPATH('vars'))[args.__id])) require(WPATH('vars'))[args.__id] = {};
if (OS_ANDROID) {
	$.TIPO_MODAL.addEventListener('open', function(e) {});
}
$.TIPO_MODAL.orientationModes = [Titanium.UI.PORTRAIT];


$.widgetBarra.init({
	titulo: 'SELECCIONE',
	onsalirinsp: Salirinsp_widgetBarra,
	__id: 'ALL138747237',
	fondo: 'fondoblanco',
	top: 0,
	colortitulo: 'negro',
	modal: '',
	onpresiono: Presiono_widgetBarra
});

function Salirinsp_widgetBarra(e) {

	var evento = e;
	/** 
	 * Cierra la pantalla tipo_modal 
	 */
	$.TIPO_MODAL.close();

}

function Presiono_widgetBarra(e) {

	var evento = e;

}

function Itemclick_listado(e) {

	e.cancelBubble = true;
	var objeto = e.section.getItemAt(e.itemIndex);
	var modelo = {},
		_modelo = [];
	var fila = {},
		fila_bak = {},
		info = {
			_template: objeto.template,
			_what: [],
			_seccion_ref: e.section.getHeaderTitle(),
			_model_id: -1
		},
		_tmp = {
			objmap: {}
		};
	if ('itemId' in e) {
		info._model_id = e.itemId;
		modelo._id = info._model_id;
		if (info._seccion_ref != '' && info._seccion_ref in _bind4section) {
			modelo._collection = _bind4section[info._seccion_ref];
			_tmp._coll = modelo._collection;
		}
	}
	if (info._seccion_ref != '' && info._seccion_ref in _bind4section) {
		if (Widget.Collections[_tmp._coll].config.adapter.type == 'sql') {
			_tmp._inst = Widget.Collections[_tmp._coll];
			_tmp._id = Widget.Collections[_tmp._coll].config.adapter.idAttribute;
			_tmp._dbsql = 'SELECT * FROM ' + Widget.Collections[_tmp._coll].config.adapter.collection_name + ' WHERE ' + _tmp._id + ' = ' + e.itemId;
			_tmp._db = Ti.Database.open(Widget.Collections[_tmp._coll].config.adapter.db_name);
			_modelo = _tmp._db.execute(_tmp._dbsql);
			var values = [],
				fieldNames = [];
			var fieldCount = _.isFunction(_modelo.fieldCount) ? _modelo.fieldCount() : _modelo.fieldCount;
			var getField = _modelo.field;
			var i = 0;
			for (; fieldCount > i; i++) fieldNames.push(_modelo.fieldName(i));
			while (_modelo.isValidRow()) {
				var o = {};
				for (i = 0; fieldCount > i; i++) o[fieldNames[i]] = getField(i);
				values.push(o);
				_modelo.next();
			}
			_modelo = values;
			_tmp._db.close();
		} else {
			_tmp._search = {};
			_tmp._search[_tmp._id] = e.itemId + '';
			_modelo = Widget.Collections[_tmp._coll].fetch(_tmp._search);
		}
	}
	var findVariables = require(WPATH('fvariables'));
	_.each(_list_templates[info._template], function(obj_id, id) {
		_.each(obj_id, function(valor, prop) {
			var llaves = findVariables(valor, '{', '}');
			_.each(llaves, function(llave) {
				_tmp.objmap[llave] = {
					id: id,
					prop: prop
				};
				fila[llave] = objeto[id][prop];
				if (id == e.bindId) info._what.push(llave);
			});
		});
	});
	info._what = info._what.join(',');
	fila_bak = JSON.parse(JSON.stringify(fila));
	if (Ti.App.deployType != 'production') console.log('click en item de modal', {
		"fila": fila
	});
	/** 
	 * Devuelve la respuesta del item seleccionado a la pantalla que esta utilizando el widget 
	 */

	if ('__args' in args) {
		args['__args'].onrespuesta({
			valor: fila.data.valor,
			item: fila.data
		});
	} else {
		args.onrespuesta({
			valor: fila.data.valor,
			item: fila.data
		});
	}
	/** 
	 * Cerramos la pantalla tipo_modal 
	 */
	$.TIPO_MODAL.close();
	_tmp.changed = false;
	_tmp.diff_keys = [];
	_.each(fila, function(value1, prop) {
		var had_samekey = false;
		_.each(fila_bak, function(value2, prop2) {
			if (prop == prop2 && value1 == value2) {
				had_samekey = true;
			} else if (!_.has(fila_bak, prop) || !_.has(fila, prop2)) {
				has_samekey = true;
			}
		});
		if (!had_samekey) _tmp.diff_keys.push(prop);
	});
	if (_tmp.diff_keys.length > 0) _tmp.changed = true;
	if (_tmp.changed == true) {
		_.each(_tmp.diff_keys, function(llave) {
			objeto[_tmp.objmap[llave].id][_tmp.objmap[llave].prop] = fila[llave];
		});
		e.section.updateItemAt(e.itemIndex, objeto);
	}

}
/** 
 * Recuperamos variables para editar y cargar cosas de la pantalla 
 */

(function() {
	var titulo = ('titulo' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['titulo'] : '';
	var guardar = ('guardar' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['guardar'] : '';
	var color = ('color' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['color'] : '';
	var data = ('data' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['data'] : '';
	var params = ('params' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['params'] : '';
	/** 
	 * Actualizamos el titulo de la pantalla modal en mayusculas 
	 */

	$.widgetBarra.update({
		titulo: titulo.toUpperCase()
	});
	/** 
	 * Ordenamos items de listado con desfase de 0.05 segundos para no congelar interfaz de usuario con proceso 
	 */
	var ID_1476068402_func = function() {
		var data = ('data' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['data'] : '';
		/** 
		 * Ordenamos los datos por letra descendiente - a..z 
		 */

		data = _.sortBy(data, function(item) {
			return item.valor;
		})
		/** 
		 * Cargamos los datos en el listado 
		 */
		var arrayItems = [];
		_.each(data, function(__currItem, __currPos, __currList) {
			arrayItems.push({
				Label: {
					text: __currItem.valor,
					_data: __currItem
				},
				template: 'elemento',
				properties: {
					searchableText: __currItem.valor
				},
				vista4: {}

			});
		});
		$.seccionListadoRef.setItems(arrayItems);
		arrayItems = null;
	};
	var ID_1476068402 = setTimeout(ID_1476068402_func, 1000 * 0.05);
})();

if (OS_IOS || OS_ANDROID) {
	$.TIPO_MODAL.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this widget controller
		try {
			//require(WPATH('vars'))[args.__id]=null;
			args = null;
			if (OS_ANDROID) {
				abx = null;
			}
			if ($item) $item = null;
			if (_my_events) {
				for (_ev_tmp in _my_events) {
					try {
						if (_ev_tmp.indexOf('_web') != -1) {
							Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
						} else {
							Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
						}
					} catch (err10) {}
				}
				_my_events = null;
				//delete _my_events;
			}
		} catch (err10) {}
		if (_out_vars) {
			var _ev_tmp;
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			_out_vars = null;
			//delete _out_vars;
		}
	});
}