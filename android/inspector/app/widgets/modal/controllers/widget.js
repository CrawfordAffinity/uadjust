/** 
 * Widget Modal
 * Representa un combobox que abre una pantalla en forma de modal con items para seleccionar.
 * 
 * @param data Variable con datos para modal
 * @param campo Define el texto del campo
 * @param titulo Define el titulo del modal
 * @param guardar Define si mostrar o no el boton guardar
 * @param color Define el color del modal
 * @param seleccione Modifica el hint del combobox
 * @param left Modifica el margen izquierdo del campo del selector
 * @param right Modifica el margen derecho del campo del selector
 * @param top Modifica el margen superior del campo del selector
 * @param activo Si true, permite seleccionar un valor. En false, esta desactivado
 * @param cargando Texto a mostrar mientras se carga en memoria datos para selector 
 */
var _bind4section = {};

var args = arguments[0] || {};

function Click_vista2(e) {
	/** 
	 * Funcion llamada al presionar sobre campo que simula combobox 
	 */

	e.cancelBubble = true;
	var elemento = e.source;
	var data = ('data' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['data'] : '';
	var activo = ('activo' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['activo'] : '';
	if (activo == true || activo == 'true') {
		/** 
		 * Avisamos a padre de instancia que abrimos modal 
		 */

		if ('__args' in args) {
			args['__args'].onabrir({});
		} else {
			args.onabrir({});
		}
		if (_.isArray(data)) {
			/** 
			 * Si hay datos cargados, abrimos pantalla modal con datos 
			 */
			Widget.createController("tipo_modal", {
				'__args': (typeof args !== 'undefined') ? args : __args
			}).getView().open();
		} else {
			/** 
			 * Si no hay datos y combobox esta activo, mostramos error 
			 */
			var preguntarAlerta_opts = ['Aceptar'];
			var preguntarAlerta = Ti.UI.createAlertDialog({
				title: 'Error',
				message: 'Faltan los datos para modal',
				buttonNames: preguntarAlerta_opts
			});
			preguntarAlerta.addEventListener('click', function(e) {
				var nulo = preguntarAlerta_opts[e.index];
				nulo = null;

				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta.show();
		}
	}

}
/** 
 * Funcion que inicializa el widget 
 */

$.init = function(params) {
	for (var tobe in params) args[tobe] = params[tobe];
	if ('__id' in params) require(WPATH('vars'))[params.__id] = {};
	/** 
	 * Creamos variables de inicializacion por default 
	 */
	require(WPATH('vars'))[args.__id]['data'] = '';
	require(WPATH('vars'))[args.__id]['campo'] = 'no definido';
	require(WPATH('vars'))[args.__id]['titulo'] = 'no definido';
	require(WPATH('vars'))[args.__id]['guardar'] = 'false';
	require(WPATH('vars'))[args.__id]['color'] = '';
	require(WPATH('vars'))[args.__id]['activo'] = 'true';
	require(WPATH('vars'))[args.__id]['cargando'] = 'cargando ..';
	require(WPATH('vars'))[args.__id]['params'] = params;
	if (!_.isUndefined(params.data)) {
		/** 
		 * Revisamos los parametros al iniciar el widget, y cargamos datos o modificamos vistas segun sea 
		 */
		require(WPATH('vars'))[args.__id]['data'] = params.data;
	}
	if (!_.isUndefined(params.campo)) {
		require(WPATH('vars'))[args.__id]['campo'] = params.campo;
		$.Nivel.setText(params.campo);

	}
	if (!_.isUndefined(params.titulo)) {
		require(WPATH('vars'))[args.__id]['titulo'] = params.titulo;
	}
	if (!_.isUndefined(params.seleccione)) {
		$.Seleccione.setText(params.seleccione);

		require(WPATH('vars'))[args.__id]['seleccione'] = params.seleccione;
	}
	if (!_.isUndefined(params.left)) {
		$.Nivel.setLeft(params.left);

		$.vista2.setLeft(params.left);

	}
	if (!_.isUndefined(params.right)) {
		$.vista2.setRight(params.right);

	}
	if (!_.isUndefined(params.top)) {
		$.vista.setTop(params.top);

	}
	if (!_.isUndefined(params.activo)) {
		require(WPATH('vars'))[args.__id]['activo'] = params.activo;
	}
	if (!_.isUndefined(params.cargando)) {
		/** 
		 * Verificamos si el parametro cargando existe, siendo asi, guardamos en una variable el texto, y modifica el subtitulo para que el usuario vea que se estan cargando los datos 
		 */
		$.Seleccione.setText(params.cargando);

		require(WPATH('vars'))[args.__id]['cargando'] = params.cargando;
	}
	/** 
	 * Se genera una espera de 0.2 segundos para que despues que se haya cargado la informacion en el widget, llama el evento afterinit 
	 */
	var ID_346372225_func = function() {

		if ('__args' in args) {
			args['__args'].onafterinit({});
		} else {
			args.onafterinit({});
		}
	};
	var ID_346372225 = setTimeout(ID_346372225_func, 1000 * 0.2);
};

/** 
 * Funcion que modifica el contenido del selector.
 * 
 * @param data Array con valores a mostrar en selector 
 */

$.data = function(params) {
	var cargando = ('cargando' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['cargando'] : '';
	var seleccione = ('seleccione' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['seleccione'] : '';
	if ((_.isObject(cargando) || (_.isString(cargando)) && !_.isEmpty(cargando)) || _.isNumber(cargando) || _.isBoolean(cargando)) {
		/** 
		 * Verificamos que la variable cargando contenga datos 
		 */
		if ((_.isObject(seleccione) || (_.isString(seleccione)) && !_.isEmpty(seleccione)) || _.isNumber(seleccione) || _.isBoolean(seleccione)) {
			/** 
			 * Verificamos que la variable seleccione contenga datos, si contiene datos, se modifica el subtitulo de la vista del widget que se ve en la pantalla que lo utiliza 
			 */
			$.Seleccione.setText(seleccione);

		}
	}
	if (!_.isUndefined(params.data)) {
		/** 
		 * Verificamos que el parametro data exista, siendo asi, guardamos en una variable data para poder ocuparla en la pantalla modal. Si no, llamamos el evento afterinit 
		 */
		require(WPATH('vars'))[args.__id]['data'] = params.data;
	} else {

		if ('__args' in args) {
			args['__args'].onafterinit({});
		} else {
			args.onafterinit({});
		}
	}
};

/** 
 * Funcion que modifica los textos del selector segun sus parametros
 * 
 * @param campo Modifica el texto mostrado al lado del combobox
 * @param titulo Modifica el titulo del modal
 * @param seleccione Modifica el hint del combobox
 * @param valor Modifica el valor mostrado por defecto en el combobox 
 */

$.labels = function(params) {
	if (!_.isUndefined(params.campo)) {
		/** 
		 * Revisamos que parametros son recibidos desde el evento labels y editamos los textos 
		 */
		$.Nivel.setText(params.titulo);

	}
	if (!_.isUndefined(params.titulo)) {
		require(WPATH('vars'))[args.__id]['titulo'] = params.titulo;
	}
	if (!_.isUndefined(params.seleccione)) {
		$.Seleccione.setText(params.seleccione);

		var Seleccione_estilo = 'estilo5_1';

		if (typeof WPATH != 'undefined') {
			var _tmp_a4w = require(WPATH('a4w'));
		} else {
			var _tmp_a4w = require('a4w');
		}
		if ((typeof Seleccione_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (Seleccione_estilo in _tmp_a4w.styles['classes'])) {
			try {
				Seleccione_estilo = _tmp_a4w.styles['classes'][Seleccione_estilo];
			} catch (st_val_err) {}
		}
		$.Seleccione.applyProperties(Seleccione_estilo);

	}
	if (!_.isUndefined(params.valor)) {
		$.Seleccione.setText(params.valor);

		var Seleccione_estilo = 'estilo12';

		if (typeof WPATH != 'undefined') {
			var _tmp_a4w = require(WPATH('a4w'));
		} else {
			var _tmp_a4w = require('a4w');
		}
		if ((typeof Seleccione_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (Seleccione_estilo in _tmp_a4w.styles['classes'])) {
			try {
				Seleccione_estilo = _tmp_a4w.styles['classes'][Seleccione_estilo];
			} catch (st_val_err) {}
		}
		$.Seleccione.applyProperties(Seleccione_estilo);

	}
};

/** 
 * Funcion que desactiva este combobox 
 */

$.desactivar = function(params) {
	/** 
	 * Si el widget recibe el evento desactivar, guarda una variable activo para que la pantalla modal no sea abierta 
	 */
	require(WPATH('vars'))[args.__id]['activo'] = 'false';
	/** 
	 * Cambia el color para que pareciera que esta desactivado 
	 */
	var Seleccione_estilo = 'estilo5_1';

	if (typeof WPATH != 'undefined') {
		var _tmp_a4w = require(WPATH('a4w'));
	} else {
		var _tmp_a4w = require('a4w');
	}
	if ((typeof Seleccione_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (Seleccione_estilo in _tmp_a4w.styles['classes'])) {
		try {
			Seleccione_estilo = _tmp_a4w.styles['classes'][Seleccione_estilo];
		} catch (st_val_err) {}
	}
	$.Seleccione.applyProperties(Seleccione_estilo);

};

/** 
 * Funcion que activa este combobox 
 */

$.activar = function(params) {
	/** 
	 * Si el widget recibe el evento activar, guarda una variable activo para que la pantalla modal sea abierta 
	 */
	require(WPATH('vars'))[args.__id]['activo'] = 'true';
	/** 
	 * Recuperamos la variable seleccione 
	 */
	var seleccione = ('seleccione' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['seleccione'] : '';
	var textosel;
	textosel = $.Seleccione.getText();

	if (seleccione != textosel) {
		/** 
		 * Solo activamos estilo on si ya se ha seleccionado un valor previamente, sino dejamos opaco para hint 
		 */
		var Seleccione_estilo = 'estilo12';

		if (typeof WPATH != 'undefined') {
			var _tmp_a4w = require(WPATH('a4w'));
		} else {
			var _tmp_a4w = require('a4w');
		}
		if ((typeof Seleccione_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (Seleccione_estilo in _tmp_a4w.styles['classes'])) {
			try {
				Seleccione_estilo = _tmp_a4w.styles['classes'][Seleccione_estilo];
			} catch (st_val_err) {}
		}
		$.Seleccione.applyProperties(Seleccione_estilo);

	}
};

/** 
 * Funcion que elimina los datos cargados en combobox, para liberar recursos de memoria 
 */

$.limpiar = function(params) {
	/** 
	 * Limpiamos memoria para evitar fugas de memoria 
	 */
	require(WPATH('vars'))[args.__id]['data'] = '';
};