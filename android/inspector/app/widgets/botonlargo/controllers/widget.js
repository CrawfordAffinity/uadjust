/** 
 * Widget Boton Largo
 * Representa un boton con progreso opcional y diferentes estilos
 * 
 * @param titulo Define el texto del boton
 * @param color Define el color del boton: amarillo, rojo, verde, lila, naranjo, morado, rosado, celeste, cafe
 * @param icono Define el alias del icono a mostrar junto al texto 
 */
var _bind4section = {};

var args = arguments[0] || {};

function Touchstart_vista2(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var color = ('color' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['color'] : '';
	if (color == 'amarillo') {
		var vista_estilo = 'fd_amarillo_on';

		var setEstilo = function(clase) {
			if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
				try {
					$.vista.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
				} catch (sete_err) {}
			}
		};
		setEstilo(vista_estilo);

		$.vista.setBorderColor('#FCBD83');

	} else if (color == 'rojo') {
		var vista_estilo = 'fd_rojo_on';

		var setEstilo = function(clase) {
			if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
				try {
					$.vista.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
				} catch (sete_err) {}
			}
		};
		setEstilo(vista_estilo);

		$.vista.setBorderColor('#FCBD83');

	} else if (color == 'verde') {
		var vista_estilo = 'fd_verde_on';

		var setEstilo = function(clase) {
			if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
				try {
					$.vista.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
				} catch (sete_err) {}
			}
		};
		setEstilo(vista_estilo);

		$.vista.setBorderColor('#8BC9E8');

	} else if (color == 'lila') {
		var vista_estilo = 'fd_lila_on';

		var setEstilo = function(clase) {
			if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
				try {
					$.vista.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
				} catch (sete_err) {}
			}
		};
		setEstilo(vista_estilo);

		$.vista.setBorderColor('#B9AAF3');

	} else if (color == 'naranjo') {
		var vista_estilo = 'fd_naranjo_on';

		var setEstilo = function(clase) {
			if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
				try {
					$.vista.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
				} catch (sete_err) {}
			}
		};
		setEstilo(vista_estilo);

		$.vista.setBorderColor('#F8DA54');

	} else if (color == 'morado') {
		var vista_estilo = 'fd_morado_on';

		var setEstilo = function(clase) {
			if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
				try {
					$.vista.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
				} catch (sete_err) {}
			}
		};
		setEstilo(vista_estilo);

		$.vista.setBorderColor('#8383DB');

	} else if (color == 'rosado') {
		var vista_estilo = 'fd_rosado_on';

		var setEstilo = function(clase) {
			if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
				try {
					$.vista.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
				} catch (sete_err) {}
			}
		};
		setEstilo(vista_estilo);

		$.vista.setBorderColor('#FCBD83');

	} else if (color == 'celeste') {
		var vista_estilo = 'fd_celeste_on';

		var setEstilo = function(clase) {
			if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
				try {
					$.vista.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
				} catch (sete_err) {}
			}
		};
		setEstilo(vista_estilo);

		$.vista.setBorderColor('#8CE5BD');

	} else if (color == 'cafe') {
		var vista_estilo = 'fd_cafe_on';

		var setEstilo = function(clase) {
			if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
				try {
					$.vista.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
				} catch (sete_err) {}
			}
		};
		setEstilo(vista_estilo);

		$.vista.setBorderColor('#FFACAA');

	}
}

function Touchend_vista2(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	/** 
	 * Segun el color que se haya definido al iniciar el widget creamos la sensacion de clickeo para cambio de color al ser presionado y soltado 
	 */
	var color = ('color' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['color'] : '';
	if (color == 'amarillo') {
		var vista_estilo = 'fd_amarillo';

		var setEstilo = function(clase) {
			if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
				try {
					$.vista.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
				} catch (sete_err) {}
			}
		};
		setEstilo(vista_estilo);

		$.vista.setBorderColor('#F8DA54');

	} else if (color == 'rojo') {
		var vista_estilo = 'fd_rojo';

		var setEstilo = function(clase) {
			if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
				try {
					$.vista.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
				} catch (sete_err) {}
			}
		};
		setEstilo(vista_estilo);

		$.vista.setBorderColor('#EE7F7E');

	} else if (color == 'verde') {
		var vista_estilo = 'fd_verde';

		var setEstilo = function(clase) {
			if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
				try {
					$.vista.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
				} catch (sete_err) {}
			}
		};
		setEstilo(vista_estilo);

		$.vista.setBorderColor('#8CE5BD');

	} else if (color == 'lila') {
		var vista_estilo = 'fd_lila';

		var setEstilo = function(clase) {
			if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
				try {
					$.vista.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
				} catch (sete_err) {}
			}
		};
		setEstilo(vista_estilo);

		$.vista.setBorderColor('#8383DB');

	} else if (color == 'naranjo') {
		var vista_estilo = 'fd_naranjo';

		var setEstilo = function(clase) {
			if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
				try {
					$.vista.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
				} catch (sete_err) {}
			}
		};
		setEstilo(vista_estilo);

		$.vista.setBorderColor('#FCBD83');

	} else if (color == 'morado') {
		var vista_estilo = 'fd_morado';

		var setEstilo = function(clase) {
			if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
				try {
					$.vista.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
				} catch (sete_err) {}
			}
		};
		setEstilo(vista_estilo);

		$.vista.setBorderColor('#B9AAF3');

	} else if (color == 'rosado') {
		var vista_estilo = 'fd_rosado';

		var setEstilo = function(clase) {
			if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
				try {
					$.vista.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
				} catch (sete_err) {}
			}
		};
		setEstilo(vista_estilo);

		$.vista.setBorderColor('#FFACAA');

	} else if (color == 'celeste') {
		var vista_estilo = 'fd_celeste';

		var setEstilo = function(clase) {
			if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
				try {
					$.vista.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
				} catch (sete_err) {}
			}
		};
		setEstilo(vista_estilo);

		$.vista.setBorderColor('#8BC9E8');

	} else if (color == 'cafe') {
		var vista_estilo = 'fd_cafe';

		var setEstilo = function(clase) {
			if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
				try {
					$.vista.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
				} catch (sete_err) {}
			}
		};
		setEstilo(vista_estilo);

		$.vista.setBorderColor('#A5876D');

	}
	if ('__args' in args) {
		args['__args'].onclick({});
	} else {
		args.onclick({});
	}

}
/** 
 * Funcion que inicializa el widget 
 */

$.init = function(params) {
	for (var tobe in params) args[tobe] = params[tobe];
	if ('__id' in params) require(WPATH('vars'))[params.__id] = {};
	/** 
	 * Seteamos por defecto el color amarillo 
	 */
	require(WPATH('vars'))[args.__id]['color'] = 'amarillo';
	if (!_.isUndefined(params.titulo)) {
		/** 
		 * Revisamos si reciben parametros para modificar valores de texto, color 
		 */
		$.CONTINUAR.setText(params.titulo);

	}
	if (!_.isUndefined(params.color)) {
		require(WPATH('vars'))[args.__id]['color'] = params.color;
	}
	if (!_.isUndefined(params.icono)) {
		require(WPATH('vars'))[args.__id]['icono'] = params.icono;
		var iconoTelefono_icono = params.icono;

		if ('fontello' in require(WPATH('a4w')) && 'adjust' in require(WPATH('a4w')).fontello && iconoTelefono_icono in require(WPATH('a4w')).fontello['adjust'].CODES) {
			iconoTelefono_icono = require(WPATH('a4w')).fontello['adjust'].CODES[iconoTelefono_icono];
		} else {
			console.log('live/modificar -> error setting new svg icon alias');
		}
		$.iconoTelefono.setText(iconoTelefono_icono);

		var iconoTelefono_visible = true;

		if (iconoTelefono_visible == 'si') {
			iconoTelefono_visible = true;
		} else if (iconoTelefono_visible == 'no') {
			iconoTelefono_visible = false;
		}
		$.iconoTelefono.setVisible(iconoTelefono_visible);

	}
	/** 
	 * Recuperamos variable de color en caso de que por parametro haya cambiado y modificamos el fondo de la vista 
	 */
	var color = ('color' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['color'] : '';
	if (color == 'amarillo') {
		var vista_estilo = 'fd_amarillo';

		var setEstilo = function(clase) {
			if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
				try {
					$.vista.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
				} catch (sete_err) {}
			}
		};
		setEstilo(vista_estilo);

	} else if (color == 'rojo') {
		var vista_estilo = 'fd_rojo';

		var setEstilo = function(clase) {
			if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
				try {
					$.vista.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
				} catch (sete_err) {}
			}
		};
		setEstilo(vista_estilo);

	} else if (color == 'verde') {
		var vista_estilo = 'fd_verde';

		var setEstilo = function(clase) {
			if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
				try {
					$.vista.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
				} catch (sete_err) {}
			}
		};
		setEstilo(vista_estilo);

	} else if (color == 'lila') {
		var vista_estilo = 'fd_lila';

		var setEstilo = function(clase) {
			if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
				try {
					$.vista.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
				} catch (sete_err) {}
			}
		};
		setEstilo(vista_estilo);

	} else if (color == 'naranjo') {
		var vista_estilo = 'fd_naranjo';

		var setEstilo = function(clase) {
			if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
				try {
					$.vista.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
				} catch (sete_err) {}
			}
		};
		setEstilo(vista_estilo);

	} else if (color == 'morado') {
		var vista_estilo = 'fd_morado';

		var setEstilo = function(clase) {
			if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
				try {
					$.vista.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
				} catch (sete_err) {}
			}
		};
		setEstilo(vista_estilo);

	} else if (color == 'rosado') {
		var vista_estilo = 'fd_rosado';

		var setEstilo = function(clase) {
			if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
				try {
					$.vista.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
				} catch (sete_err) {}
			}
		};
		setEstilo(vista_estilo);

	} else if (color == 'celeste') {
		var vista_estilo = 'fd_celeste';

		var setEstilo = function(clase) {
			if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
				try {
					$.vista.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
				} catch (sete_err) {}
			}
		};
		setEstilo(vista_estilo);

	} else if (color == 'cafe') {
		var vista_estilo = 'fd_cafe';

		var setEstilo = function(clase) {
			if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
				try {
					$.vista.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
				} catch (sete_err) {}
			}
		};
		setEstilo(vista_estilo);

	}
};

/** 
 * Funcion que muestra la animacion de progreso 
 */

$.iniciar_progreso = function(params) {
	/** 
	 * oculta el bot&#243;n continuar y muestra la animaci&#243;n de progreso. 
	 */
	var vista2_visible = false;

	if (vista2_visible == 'si') {
		vista2_visible = true;
	} else if (vista2_visible == 'no') {
		vista2_visible = false;
	}
	$.vista2.setVisible(vista2_visible);

	$.progreso.show();
};

/** 
 * Funcion que oculta la animacion de progreso 
 */

$.detener_progreso = function(params) {
	var vista2_visible = true;

	if (vista2_visible == 'si') {
		vista2_visible = true;
	} else if (vista2_visible == 'no') {
		vista2_visible = false;
	}
	$.vista2.setVisible(vista2_visible);

	$.progreso.hide();
};