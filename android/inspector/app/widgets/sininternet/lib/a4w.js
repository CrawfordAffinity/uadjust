exports.events = _.clone(Backbone.Events);
exports.ajaxkeys = {};
exports.variables = {};
exports.styles = {"images":{"mono_7":"/images/iBB0FE490ADE01E0C5F7F04E1FEFC4E70.png","mono_3":"/images/i8B282601EF9B7B696CDAC05451F12BC3.png"},"classes":{"#ID_497503827":{"image":"WPATH('images/iBB0FE490ADE01E0C5F7F04E1FEFC4E70.png')"},"estilo8":{"color":"#ee7f7e","font":{"fontSize":"12dp"}},"#ID_1037140748":{"image":"WPATH('images/i8B282601EF9B7B696CDAC05451F12BC3.png')"},"estilo2_1":{"color":"#a0a1a3","font":{"fontFamily":"Roboto-Light","fontSize":"15dp"}}}};
exports.fontello = {};
