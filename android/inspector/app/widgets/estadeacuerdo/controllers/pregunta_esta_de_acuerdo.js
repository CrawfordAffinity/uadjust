var _bind4section = {};

var _activity;
if (OS_ANDROID) {
	_activity = $.pregunta_esta_de_acuerdo.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
if (_.isUndefined(require(WPATH('vars'))[args.__id])) require(WPATH('vars'))[args.__id] = {};
if (OS_ANDROID) {
	$.pregunta_esta_de_acuerdo.addEventListener('open', function(e) {});
}
$.pregunta_esta_de_acuerdo.orientationModes = [Titanium.UI.PORTRAIT];


$.widgetBarra.init({
	titulo: '¿ESTA DE ACUERDO?',
	__id: 'ALL138747237',
	fondo: 'fondoazul',
	top: 0
});


$.widgetPregunta.init({
	titulo: '¿EL ASEGURADO CONFIRMA ESTOS DATOS?',
	__id: 'ALL581285551',
	si: 'SI, están correctos',
	texto: 'El asegurado debe confirmar que los datos de esta sección están correctos',
	onno: no_widgetPregunta,
	ajustado: -8,
	onsi: si_widgetPregunta,
	top: 80,
	no: 'NO, hay que modificar algo'
});

function si_widgetPregunta(e) {

	var evento = e;
	/** 
	 * Manda a ejecutar el evento &quot;si&quot; en la pantalla que utiliza el widget 
	 */

	if ('__args' in args) {
		args['__args'].onsi({});
	} else {
		args.onsi({});
	}
	/** 
	 * Cierra la pantalla 
	 */
	$.pregunta_esta_de_acuerdo.close();

}

function no_widgetPregunta(e) {

	var evento = e;
	/** 
	 * Manda a ejecutar el evento &quot;si&quot; en la pantalla que utiliza el widget 
	 */

	if ('__args' in args) {
		args['__args'].onno({});
	} else {
		args.onno({});
	}
	/** 
	 * Cierra la pantalla 
	 */
	$.pregunta_esta_de_acuerdo.close();

}

(function() {
	/** 
	 * Recupera los atributos que se enviaron de la pantalla que utiliza el widget y asi hacer las siguientes acciones 
	 */
	var params = ('params' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['params'] : '';
	if (!_.isUndefined(params.pantalla)) {
		/** 
		 * Cambia el nombre del titulo del header, mandando el valor al widget de barra 
		 */

		$.widgetBarra.update({
			titulo: params.pantalla
		});
	}
	if (!_.isUndefined(params.titulo)) {
		/** 
		 * Cambia el titulo del mensaje del globo de mensaje, mandando el valor al widget de pregunta 
		 */

		$.widgetPregunta.update({
			titulo: params.titulo
		});
	}
	if (!_.isUndefined(params.texto)) {
		/** 
		 * Cambia el mensaje del globo de mensaje, mandando el valor al widget de pregunta 
		 */

		$.widgetPregunta.update({
			texto: params.texto
		});
	}
	if (!_.isUndefined(params.si)) {
		/** 
		 * Cambia el mensaje del boton verde, mandando el valor al widget de pregunta 
		 */

		$.widgetPregunta.update({
			si: params.si
		});
	}
	if (!_.isUndefined(params.no)) {
		/** 
		 * Cambia el mensaje del boton rojo, mandando el valor al widget de pregunta 
		 */

		$.widgetPregunta.update({
			no: params.no
		});
	}
	/** 
	 * Recupera el valor del color para poder pintar el statusbar y navbar desde la pantalla que la esta utilizando 
	 */
	var color = ('color' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['color'] : '';
	if (color == 'verde') {
		/** 
		 * Segun el color en la variable es la edicion de el navbar y statusbar 
		 */
		/** 
		 * Actualiza el color de fondo del header, llamando un evento del widget 
		 */

		$.widgetBarra.update({
			fondo: 'fondoverde'
		});
		/** 
		 * Se genera una espera de 0.1 segundos para que se pueda hacer el cambio de color en el statusbar y navbar 
		 */
		var ID_598330316_func = function() {
			/** 
			 * Cambia el color del navbar y statusbar por el color que se solicito 
			 */

			abx.setStatusbarColor("#8CE5BD");
			abx.setNavigationbarColor("#8CE5BD");
		};
		var ID_598330316 = setTimeout(ID_598330316_func, 1000 * 0.1);
	} else if (color == 'naranjo') {
		/** 
		 * Actualiza el color de fondo del header, llamando un evento del widget 
		 */

		$.widgetBarra.update({
			fondo: 'fondonaranjo'
		});
		/** 
		 * Se genera una espera de 0.1 segundos para que se pueda hacer el cambio de color en el statusbar y navbar 
		 */
		var ID_55584160_func = function() {
			/** 
			 * Cambia el color del navbar y statusbar por el color que se solicito 
			 */

			abx.setStatusbarColor("#FCBD83");
			abx.setNavigationbarColor("#FCBD83");;
		};
		var ID_55584160 = setTimeout(ID_55584160_func, 1000 * 0.1);
	} else if (color == 'morado') {
		/** 
		 * Actualiza el color de fondo del header, llamando un evento del widget 
		 */

		$.widgetBarra.update({
			fondo: 'fondomorado'
		});
		/** 
		 * Se genera una espera de 0.1 segundos para que se pueda hacer el cambio de color en el statusbar y navbar 
		 */
		var ID_1103652325_func = function() {
			/** 
			 * Cambia el color del navbar y statusbar por el color que se solicito 
			 */

			abx.setStatusbarColor("#B9AAF3");
			abx.setNavigationbarColor("#B9AAF3");;
		};
		var ID_1103652325 = setTimeout(ID_1103652325_func, 1000 * 0.1);
	} else if (color == 'celeste') {
		/** 
		 * Actualiza el color de fondo del header, llamando un evento del widget 
		 */

		$.widgetBarra.update({
			fondo: 'fondoceleste'
		});
		/** 
		 * Se genera una espera de 0.1 segundos para que se pueda hacer el cambio de color en el statusbar y navbar 
		 */
		var ID_500599583_func = function() {
			/** 
			 * Cambia el color del navbar y statusbar por el color que se solicito 
			 */

			abx.setStatusbarColor("#8BC9E8");
			abx.setNavigationbarColor("#8BC9E8");;
		};
		var ID_500599583 = setTimeout(ID_500599583_func, 1000 * 0.1);
	} else if (color == 'rosado') {
		/** 
		 * Actualiza el color de fondo del header, llamando un evento del widget 
		 */

		$.widgetBarra.update({
			fondo: 'fondorosado'
		});
		/** 
		 * Se genera una espera de 0.1 segundos para que se pueda hacer el cambio de color en el statusbar y navbar 
		 */
		var ID_1015183566_func = function() {
			/** 
			 * Cambia el color del navbar y statusbar por el color que se solicito 
			 */

			abx.setStatusbarColor("#FFACAA");
			abx.setNavigationbarColor("#FFACAA");
		};
		var ID_1015183566 = setTimeout(ID_1015183566_func, 1000 * 0.1);
	}
})();

function Open_pregunta_esta_de_acuerdo(e) {

	e.cancelBubble = true;
	var elemento = e.source;

}
if (OS_IOS || OS_ANDROID) {
	$.pregunta_esta_de_acuerdo.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this widget controller
		try {
			//require(WPATH('vars'))[args.__id]=null;
			args = null;
			if (OS_ANDROID) {
				abx = null;
			}
			if ($item) $item = null;
			if (_my_events) {
				for (_ev_tmp in _my_events) {
					try {
						if (_ev_tmp.indexOf('_web') != -1) {
							Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
						} else {
							Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
						}
					} catch (err10) {}
				}
				_my_events = null;
				//delete _my_events;
			}
		} catch (err10) {}
		if (_out_vars) {
			var _ev_tmp;
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			_out_vars = null;
			//delete _out_vars;
		}
	});
}