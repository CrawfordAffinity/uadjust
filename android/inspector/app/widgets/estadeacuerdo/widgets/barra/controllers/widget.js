/** 
 * Widget Barra Inspeccion
 * La funcion de este widget es simular un barra tipo header.
 * Posee imagenes/textos que son utilizados en el proyecto principal.
 * 
 * @param info Muestra una vista a la derecha con icono de informacion
 * @param titulo Define el titulo de la pantalla
 * @param fondo Define el color de fondo de la barra (Definido segun los estilos de este widget)
 * @param modal Define la posicion del titulo en la barra
 * @param nroinicial En el caso de que sea una secuencia de pantallas, permite definir el numero en la que esta actualmente
 * @param nrofinal En el caso de que sea una secuencia de pantallas, permite definir el numero de pantallas que tiene esa secuencia
 * @param colortitulo Define el color del texto del titulo (Por defecto es blanco), la otra opcion es negro, al utilizar esta opcion se tiene que escribir literalmente la palabra negro
 * @param textoderecha Define el texto que se mostrara en la parte derecha de la barra
 * @param colortextoderecha Se define el color del texto que se mostrara en la parte derecha de la barra, las opciones disponibles son blanco, negro, azul
 * @param salir_insp Muestra una vista a la izquierda con icono de salir
 * @param continuar Muestra una vista a la derecha con icono color plomo de continuar
 * @param continuarazul Muestra una vista a la derecha con icono color azul de continuar
 * @param agregar_dano Muestra una vista a la derecha con icono de agregar dano
 * @param enviar_insp Muestra una vista a la derecha con texto de enviar todas (pantalla historial)
 * @param cerrar Muestra una vista a la izquierda con icono de cerrar 
 */
var _bind4section = {};

var args = arguments[0] || {};

function Touchstart_vista3(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.iconoSalir_insp.setColor('#cccccc');


}

function Touchend_vista3(e) {

	e.cancelBubble = true;
	var elemento = e.source;

	if ('__args' in args) {
		args['__args'].onsalirinsp({});
	} else {
		args.onsalirinsp({});
	}
	if (Ti.App.deployType != 'production') console.log('salir insp widget', {});
	$.iconoSalir_insp.setColor('#ffffff');


}

function Touchstart_vista4(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.iconoCerrar.setColor('#cccccc');


}

function Touchend_vista4(e) {

	e.cancelBubble = true;
	var elemento = e.source;

	if ('__args' in args) {
		args['__args'].oncerrar({});
	} else {
		args.oncerrar({});
	}
	if (Ti.App.deployType != 'production') console.log('salir insp widget', {});
	$.iconoCerrar.setColor('#ffffff');


}

function Touchstart_vista5(e) {

	e.cancelBubble = true;
	var elemento = e.source;

}

function Touchend_vista5(e) {

	e.cancelBubble = true;
	var elemento = e.source;

	if ('__args' in args) {
		args['__args'].onpresiono({});
	} else {
		args.onpresiono({});
	}

}

function Touchstart_vista6(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.iconoContinuar.setColor('#cccccc');


}

function Touchend_vista6(e) {

	e.cancelBubble = true;
	var elemento = e.source;

	if ('__args' in args) {
		args['__args'].onpresiono({});
	} else {
		args.onpresiono({});
	}
	$.iconoContinuar.setColor('#ffffff');


}

function Touchstart_vista7(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.iconoContinuar2.setColor('#cccccc');


}

function Touchend_vista7(e) {

	e.cancelBubble = true;
	var elemento = e.source;

	if ('__args' in args) {
		args['__args'].onpresiono({});
	} else {
		args.onpresiono({});
	}
	$.iconoContinuar2.setColor('#2d9edb');


}

function Touchstart_vista8(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.iconoAgregar_item.setColor('#cccccc');


}

function Touchend_vista8(e) {

	e.cancelBubble = true;
	var elemento = e.source;

	if ('__args' in args) {
		args['__args'].onagregar_dano({});
	} else {
		args.onagregar_dano({});
	}
	$.iconoAgregar_item.setColor('#ffffff');


}

function Touchstart_vista9(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.iconoSalir_insp.setColor('#2dc8ff');


}

function Touchend_vista9(e) {

	e.cancelBubble = true;
	var elemento = e.source;

	if ('__args' in args) {
		args['__args'].onpresiono({});
	} else {
		args.onpresiono({});
	}
	$.iconoInfo.setColor('#2d9edb');


}

function Click_vista10(e) {

	e.cancelBubble = true;
	var elemento = e.source;

	if ('__args' in args) {
		args['__args'].onpresiono({});
	} else {
		args.onpresiono({});
	}

}
/** 
 * Funcion que inicializa el widget segun sus parametros 
 */

$.init = function(params) {
	for (var tobe in params) args[tobe] = params[tobe];
	if ('__id' in params) require(WPATH('vars'))[params.__id] = {};
	if (!_.isUndefined(params.info)) {
		var vista9_visible = true;

		if (vista9_visible == 'si') {
			vista9_visible = true;
		} else if (vista9_visible == 'no') {
			vista9_visible = false;
		}
		$.vista9.setVisible(vista9_visible);

		var vista5_visible = false;

		if (vista5_visible == 'si') {
			vista5_visible = true;
		} else if (vista5_visible == 'no') {
			vista5_visible = false;
		}
		$.vista5.setVisible(vista5_visible);

	}
	if (!_.isUndefined(params.titulo)) {
		$.TITULO.setText(params.titulo);

	}
	if (!_.isUndefined(params.fondo)) {
		var vista_estilo = params.fondo;

		var setEstilo = function(clase) {
			if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
				try {
					$.vista.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
				} catch (sete_err) {}
			}
		};
		setEstilo(vista_estilo);

	}
	if (!_.isUndefined(params.modal)) {
		$.TITULO.setLeft(20);

	}
	if (!_.isUndefined(params.nroinicial)) {
		$.label.setText(params.nroinicial);

		var vista2_visible = true;

		if (vista2_visible == 'si') {
			vista2_visible = true;
		} else if (vista2_visible == 'no') {
			vista2_visible = false;
		}
		$.vista2.setVisible(vista2_visible);

	}
	if (!_.isUndefined(params.nrofinal)) {
		$.label3.setText(params.nrofinal);

		var vista2_visible = true;

		if (vista2_visible == 'si') {
			vista2_visible = true;
		} else if (vista2_visible == 'no') {
			vista2_visible = false;
		}
		$.vista2.setVisible(vista2_visible);

	}
	if (!_.isUndefined(params.colortitulo)) {
		if (params.colortitulo == 'negro') {
			$.TITULO.setColor('#000000');

		}
	}
	if (!_.isUndefined(params.textoderecha)) {
		var vista5_visible = true;

		if (vista5_visible == 'si') {
			vista5_visible = true;
		} else if (vista5_visible == 'no') {
			vista5_visible = false;
		}
		$.vista5.setVisible(vista5_visible);

		$.TextoDer.setText(params.textoderecha);

		var vista9_visible = false;

		if (vista9_visible == 'si') {
			vista9_visible = true;
		} else if (vista9_visible == 'no') {
			vista9_visible = false;
		}
		$.vista9.setVisible(vista9_visible);

	}
	if (!_.isUndefined(params.colortextoderecha)) {
		if (params.colortextoderecha == 'blanco') {
			$.TextoDer.setColor('#ffffff');

		} else if (params.colortextoderecha == 'negro') {
			$.TextoDer.setColor('#000000');

		} else if (params.colortextoderecha == 'azul') {
			$.TextoDer.setColor('#2d9edb');

		}
	}
	if (!_.isUndefined(params.salir_insp)) {
		var vista3_visible = true;

		if (vista3_visible == 'si') {
			vista3_visible = true;
		} else if (vista3_visible == 'no') {
			vista3_visible = false;
		}
		$.vista3.setVisible(vista3_visible);

	}
	if (!_.isUndefined(params.continuar)) {
		var vista6_visible = true;

		if (vista6_visible == 'si') {
			vista6_visible = true;
		} else if (vista6_visible == 'no') {
			vista6_visible = false;
		}
		$.vista6.setVisible(vista6_visible);

	}
	if (!_.isUndefined(params.continuarazul)) {
		var vista7_visible = true;

		if (vista7_visible == 'si') {
			vista7_visible = true;
		} else if (vista7_visible == 'no') {
			vista7_visible = false;
		}
		$.vista7.setVisible(vista7_visible);

	}
	if (!_.isUndefined(params.agregar_dano)) {
		var vista8_visible = true;

		if (vista8_visible == 'si') {
			vista8_visible = true;
		} else if (vista8_visible == 'no') {
			vista8_visible = false;
		}
		$.vista8.setVisible(vista8_visible);

	}
	if (!_.isUndefined(params.enviar_insp)) {
		var vista10_visible = true;

		if (vista10_visible == 'si') {
			vista10_visible = true;
		} else if (vista10_visible == 'no') {
			vista10_visible = false;
		}
		$.vista10.setVisible(vista10_visible);

	}
	if (!_.isUndefined(params.cerrar)) {
		var vista4_visible = true;

		if (vista4_visible == 'si') {
			vista4_visible = true;
		} else if (vista4_visible == 'no') {
			vista4_visible = false;
		}
		$.vista4.setVisible(vista4_visible);

	}
};

/** 
 * Funcion update
 * 
 * @param titulo Define el nuevo titulo de la pantalla
 * @param fondo Define el nuevo color de fondo de la barra (definido segun los estilos de este widget)
 * @param texto Define el texto en la parte derecha de la barra 
 */

$.update = function(params) {
	if (!_.isUndefined(params.titulo)) {
		/** 
		 * En el caso que se tenga que modificar el titulo de la pantalla una vez ya definido, esto permite reeditar ese cambio 
		 */
		$.TITULO.setText(params.titulo);

	}
	if (!_.isUndefined(params.fondo)) {
		/** 
		 * En el caso que se tenga que modificar el color de la barra de la pantalla una vez ya definido, esto permite reeditar ese cambio 
		 */
		var vista_estilo = params.fondo;

		var setEstilo = function(clase) {
			if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
				try {
					$.vista.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
				} catch (sete_err) {}
			}
		};
		setEstilo(vista_estilo);

	}
	if (!_.isUndefined(params.texto)) {
		/** 
		 * En el caso que se tenga que modificar el texto del boton de la barra de la pantalla una vez ya definido, esto permite reeditar ese cambio 
		 */
		$.TextoDer.setText(params.texto);

	}
};

/** 
 * Funcion que muestra una vista a la derecha con texto de 'enviar todas' (pantalla historial) 
 */

$.mostrar_envio = function(params) {
	/** 
	 * Al recibir evento solo mostramos la vista de enviar todos los pendientes en la pantalla historial 
	 */
	var vista10_visible = true;

	if (vista10_visible == 'si') {
		vista10_visible = true;
	} else if (vista10_visible == 'no') {
		vista10_visible = false;
	}
	$.vista10.setVisible(vista10_visible);

};

/** 
 * Funcion que oculta la vista a la derecha con texto de 'enviar todas' (pantalla historial) 
 */

$.esconder_envio = function(params) {
	/** 
	 * Al recibir evento solo escondemos la vista de enviar todos los pendientes en la pantalla historial 
	 */
	var vista10_visible = false;

	if (vista10_visible == 'si') {
		vista10_visible = true;
	} else if (vista10_visible == 'no') {
		vista10_visible = false;
	}
	$.vista10.setVisible(vista10_visible);

};