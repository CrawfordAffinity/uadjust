/** 
 * Widget Mono-Personaje
 * Muestra diferentes tipos de personajes con un mensaje de texto personalizable
 * 
 * @param visible Indica visibilidad de widget
 * @param tipo Indica tipo de personaje: tip, info, sorry 
 */
var _bind4section = {};

var args = arguments[0] || {};

/** 
 * Funcion que inicializa widget 
 */

$.init = function(params) {
	for (var tobe in params) args[tobe] = params[tobe];
	if ('__id' in params) require(WPATH('vars'))[params.__id] = {};
	/** 
	 * Seteamos la variable de la visibilidad del mono en true, a menos que se indique lo contrario al inicializar el widget 
	 */
	require(WPATH('vars'))[args.__id]['visible'] = 'true';
	if (!_.isUndefined(params.visible)) {
		require(WPATH('vars'))[args.__id]['visible'] = params.visible;
	}
	var visible = ('visible' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['visible'] : '';
	if (!_.isUndefined(params.tipo)) {
		if (params.tipo == '_tip') {
			/** 
			 * Dependiendo del tipo de mono a mostrar, ocultamos todas excepto la que necesitamos mostrar 
			 */
			/** 
			 * Definimos el alto de la vista y actualizamos el tipo de mono 
			 */
			require(WPATH('vars'))[args.__id]['alto'] = 120;
			require(WPATH('vars'))[args.__id]['tipo'] = '_tip';
			/** 
			 * mono happy horizontal 
			 */
			var vista_visible = visible;

			if (vista_visible == 'si') {
				vista_visible = true;
			} else if (vista_visible == 'no') {
				vista_visible = false;
			}
			$.vista.setVisible(vista_visible);

			var vista_alto = 120;

			if (vista_alto == '*') {
				vista_alto = Ti.UI.FILL;
			} else if (vista_alto == '-') {
				vista_alto = Ti.UI.SIZE;
			} else if (!isNaN(vista_alto)) {
				vista_alto = vista_alto + 'dp';
			}
			$.vista.setHeight(vista_alto);

			var vista4_visible = false;

			if (vista4_visible == 'si') {
				vista4_visible = true;
			} else if (vista4_visible == 'no') {
				vista4_visible = false;
			}
			$.vista4.setVisible(vista4_visible);

			var vista4_alto = 0;

			if (vista4_alto == '*') {
				vista4_alto = Ti.UI.FILL;
			} else if (vista4_alto == '-') {
				vista4_alto = Ti.UI.SIZE;
			} else if (!isNaN(vista4_alto)) {
				vista4_alto = vista4_alto + 'dp';
			}
			$.vista4.setHeight(vista4_alto);

			var vista6_visible = false;

			if (vista6_visible == 'si') {
				vista6_visible = true;
			} else if (vista6_visible == 'no') {
				vista6_visible = false;
			}
			$.vista6.setVisible(vista6_visible);

			var vista6_alto = 0;

			if (vista6_alto == '*') {
				vista6_alto = Ti.UI.FILL;
			} else if (vista6_alto == '-') {
				vista6_alto = Ti.UI.SIZE;
			} else if (!isNaN(vista6_alto)) {
				vista6_alto = vista6_alto + 'dp';
			}
			$.vista6.setHeight(vista6_alto);

			if (!_.isUndefined(params.texto)) {
				$.TipNoAceptes.setText(params.texto);

			}
			if (!_.isUndefined(params.top)) {
				$.vista.setTop(params.top);

			}
			if (!_.isUndefined(params.bottom)) {
				$.vista.setBottom(params.bottom);

			}
			if (!_.isUndefined(params.ancho)) {
				var vista_ancho = params.ancho;

				if (vista_ancho == '*') {
					vista_ancho = Ti.UI.FILL;
				} else if (vista_ancho == '-') {
					vista_ancho = Ti.UI.SIZE;
				} else if (!isNaN(vista_ancho)) {
					vista_ancho = vista_ancho + 'dp';
				}
				$.vista.setWidth(vista_ancho);

			}
			if (!_.isUndefined(params.alto)) {
				var vista_alto = params.alto;

				if (vista_alto == '*') {
					vista_alto = Ti.UI.FILL;
				} else if (vista_alto == '-') {
					vista_alto = Ti.UI.SIZE;
				} else if (!isNaN(vista_alto)) {
					vista_alto = vista_alto + 'dp';
				}
				$.vista.setHeight(vista_alto);

				require(WPATH('vars'))[args.__id]['alto'] = params.alto;
			}
			if (!_.isUndefined(params.titulo)) {
				$.ESPERATULLAMADA.setText(params.titulo);

				var ESPERATULLAMADA_visible = true;

				if (ESPERATULLAMADA_visible == 'si') {
					ESPERATULLAMADA_visible = true;
				} else if (ESPERATULLAMADA_visible == 'no') {
					ESPERATULLAMADA_visible = false;
				}
				$.ESPERATULLAMADA.setVisible(ESPERATULLAMADA_visible);

			}
		} else if (params.tipo == '_sorry') {
			/** 
			 * Definimos el alto de la vista y actualizamos el tipo de mono 
			 */
			require(WPATH('vars'))[args.__id]['tipo'] = '_sorry';
			require(WPATH('vars'))[args.__id]['alto'] = '-';
			var vista4_visible = visible;

			if (vista4_visible == 'si') {
				vista4_visible = true;
			} else if (vista4_visible == 'no') {
				vista4_visible = false;
			}
			$.vista4.setVisible(vista4_visible);

			var vista4_alto = '-';

			if (vista4_alto == '*') {
				vista4_alto = Ti.UI.FILL;
			} else if (vista4_alto == '-') {
				vista4_alto = Ti.UI.SIZE;
			} else if (!isNaN(vista4_alto)) {
				vista4_alto = vista4_alto + 'dp';
			}
			$.vista4.setHeight(vista4_alto);

			var vista_visible = false;

			if (vista_visible == 'si') {
				vista_visible = true;
			} else if (vista_visible == 'no') {
				vista_visible = false;
			}
			$.vista.setVisible(vista_visible);

			var vista_alto = 0;

			if (vista_alto == '*') {
				vista_alto = Ti.UI.FILL;
			} else if (vista_alto == '-') {
				vista_alto = Ti.UI.SIZE;
			} else if (!isNaN(vista_alto)) {
				vista_alto = vista_alto + 'dp';
			}
			$.vista.setHeight(vista_alto);

			var vista6_visible = false;

			if (vista6_visible == 'si') {
				vista6_visible = true;
			} else if (vista6_visible == 'no') {
				vista6_visible = false;
			}
			$.vista6.setVisible(vista6_visible);

			var vista6_alto = 0;

			if (vista6_alto == '*') {
				vista6_alto = Ti.UI.FILL;
			} else if (vista6_alto == '-') {
				vista6_alto = Ti.UI.SIZE;
			} else if (!isNaN(vista6_alto)) {
				vista6_alto = vista6_alto + 'dp';
			}
			$.vista6.setHeight(vista6_alto);

			if (!_.isUndefined(params.titulo)) {
				$.NOTIENESTAREAS.setText(params.titulo);

			}
			if (!_.isUndefined(params.texto)) {
				$.Aseguratede.setText(params.texto);

			}
			if (!_.isUndefined(params.top)) {
				$.vista4.setTop(params.top);

			}
			if (!_.isUndefined(params.bottom)) {
				$.vista4.setBottom(params.bottom);

			}
			if (!_.isUndefined(params.ancho)) {
				var vista4_ancho = params.ancho;

				if (vista4_ancho == '*') {
					vista4_ancho = Ti.UI.FILL;
				} else if (vista4_ancho == '-') {
					vista4_ancho = Ti.UI.SIZE;
				} else if (!isNaN(vista4_ancho)) {
					vista4_ancho = vista4_ancho + 'dp';
				}
				$.vista4.setWidth(vista4_ancho);

			}
			if (!_.isUndefined(params.alto)) {
				var vista4_alto = params.alto;

				if (vista4_alto == '*') {
					vista4_alto = Ti.UI.FILL;
				} else if (vista4_alto == '-') {
					vista4_alto = Ti.UI.SIZE;
				} else if (!isNaN(vista4_alto)) {
					vista4_alto = vista4_alto + 'dp';
				}
				$.vista4.setHeight(vista4_alto);

				require(WPATH('vars'))[args.__id]['alto'] = params.alto;
			}
		} else if (params.tipo == '_info') {
			/** 
			 * Definimos el alto de la vista y actualizamos el tipo de mono 
			 */
			require(WPATH('vars'))[args.__id]['tipo'] = '_info';
			require(WPATH('vars'))[args.__id]['alto'] = '-';
			var vista6_visible = visible;

			if (vista6_visible == 'si') {
				vista6_visible = true;
			} else if (vista6_visible == 'no') {
				vista6_visible = false;
			}
			$.vista6.setVisible(vista6_visible);

			var vista6_alto = '-';

			if (vista6_alto == '*') {
				vista6_alto = Ti.UI.FILL;
			} else if (vista6_alto == '-') {
				vista6_alto = Ti.UI.SIZE;
			} else if (!isNaN(vista6_alto)) {
				vista6_alto = vista6_alto + 'dp';
			}
			$.vista6.setHeight(vista6_alto);

			var vista_visible = false;

			if (vista_visible == 'si') {
				vista_visible = true;
			} else if (vista_visible == 'no') {
				vista_visible = false;
			}
			$.vista.setVisible(vista_visible);

			var vista_alto = 0;

			if (vista_alto == '*') {
				vista_alto = Ti.UI.FILL;
			} else if (vista_alto == '-') {
				vista_alto = Ti.UI.SIZE;
			} else if (!isNaN(vista_alto)) {
				vista_alto = vista_alto + 'dp';
			}
			$.vista.setHeight(vista_alto);

			var vista4_visible = false;

			if (vista4_visible == 'si') {
				vista4_visible = true;
			} else if (vista4_visible == 'no') {
				vista4_visible = false;
			}
			$.vista4.setVisible(vista4_visible);

			var vista4_alto = 0;

			if (vista4_alto == '*') {
				vista4_alto = Ti.UI.FILL;
			} else if (vista4_alto == '-') {
				vista4_alto = Ti.UI.SIZE;
			} else if (!isNaN(vista4_alto)) {
				vista4_alto = vista4_alto + 'dp';
			}
			$.vista4.setHeight(vista4_alto);

			if (!_.isUndefined(params.titulo)) {
				$.PUEDECONTINUAR.setText(params.titulo);

			}
			if (!_.isUndefined(params.texto)) {
				$.SiEstel.setText(params.texto);

			}
			if (!_.isUndefined(params.top)) {
				$.vista6.setTop(params.top);

			}
			if (!_.isUndefined(params.bottom)) {
				$.vista6.setBottom(params.bottom);

			}
			if (!_.isUndefined(params.ancho)) {
				var vista6_ancho = params.ancho;

				if (vista6_ancho == '*') {
					vista6_ancho = Ti.UI.FILL;
				} else if (vista6_ancho == '-') {
					vista6_ancho = Ti.UI.SIZE;
				} else if (!isNaN(vista6_ancho)) {
					vista6_ancho = vista6_ancho + 'dp';
				}
				$.vista6.setWidth(vista6_ancho);

			}
			if (!_.isUndefined(params.alto)) {
				var vista6_alto = params.alto;

				if (vista6_alto == '*') {
					vista6_alto = Ti.UI.FILL;
				} else if (vista6_alto == '-') {
					vista6_alto = Ti.UI.SIZE;
				} else if (!isNaN(vista6_alto)) {
					vista6_alto = vista6_alto + 'dp';
				}
				$.vista6.setHeight(vista6_alto);

				require(WPATH('vars'))[args.__id]['alto'] = params.alto;
			}
		}
	}
};

/** 
 * Funcion que actualiza texto y/o personaje
 * 
 * @param visible Indica si mostrar o no este widget
 * @param tipo Tipo de personaje: tip, info, sorry 
 */

$.update = function(params) {
	if (!_.isUndefined(params.tipo)) {
		/** 
		 * Revisamos si se tiene que actualizar el tipo de mono que se va a mostrar, en caso de que exista, actualizamos la variable del tipo de mono a mostrar 
		 */
		if (params.tipo == '_tip') {
			require(WPATH('vars'))[args.__id]['tipo'] = '_tip';
		} else if (params.tipo == '_sorry') {
			require(WPATH('vars'))[args.__id]['tipo'] = '_sorry';
		} else if (params.tipo == '_info') {
			require(WPATH('vars'))[args.__id]['tipo'] = '_info';
		}
	}
	if (!_.isUndefined(params.visible)) {
		/** 
		 * Revisamos si el widget sera visible o no 
		 */
		require(WPATH('vars'))[args.__id]['visible'] = params.visible;
	}
	var tipo = ('tipo' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['tipo'] : '';
	var visible = ('visible' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['visible'] : '';
	var alto = ('alto' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['alto'] : '';
	if (params.tipo == '_tip') {
		/** 
		 * Definimos el alto de la vista y guardamos el tipo de vista en una variable 
		 */
		require(WPATH('vars'))[args.__id]['alto'] = 120;
		require(WPATH('vars'))[args.__id]['tipo'] = '_tip';
		/** 
		 * mono happy horizontal 
		 */
		var vista_visible = visible;

		if (vista_visible == 'si') {
			vista_visible = true;
		} else if (vista_visible == 'no') {
			vista_visible = false;
		}
		$.vista.setVisible(vista_visible);

		var vista_alto = 120;

		if (vista_alto == '*') {
			vista_alto = Ti.UI.FILL;
		} else if (vista_alto == '-') {
			vista_alto = Ti.UI.SIZE;
		} else if (!isNaN(vista_alto)) {
			vista_alto = vista_alto + 'dp';
		}
		$.vista.setHeight(vista_alto);

		/** 
		 * ocultamos otros tipos... sorry 
		 */
		var vista4_visible = false;

		if (vista4_visible == 'si') {
			vista4_visible = true;
		} else if (vista4_visible == 'no') {
			vista4_visible = false;
		}
		$.vista4.setVisible(vista4_visible);

		var vista4_alto = 0;

		if (vista4_alto == '*') {
			vista4_alto = Ti.UI.FILL;
		} else if (vista4_alto == '-') {
			vista4_alto = Ti.UI.SIZE;
		} else if (!isNaN(vista4_alto)) {
			vista4_alto = vista4_alto + 'dp';
		}
		$.vista4.setHeight(vista4_alto);

		/** 
		 * Ocultamos info 
		 */
		var vista6_visible = false;

		if (vista6_visible == 'si') {
			vista6_visible = true;
		} else if (vista6_visible == 'no') {
			vista6_visible = false;
		}
		$.vista6.setVisible(vista6_visible);

		var vista6_alto = 0;

		if (vista6_alto == '*') {
			vista6_alto = Ti.UI.FILL;
		} else if (vista6_alto == '-') {
			vista6_alto = Ti.UI.SIZE;
		} else if (!isNaN(vista6_alto)) {
			vista6_alto = vista6_alto + 'dp';
		}
		$.vista6.setHeight(vista6_alto);

		if (!_.isUndefined(params.texto)) {
			/** 
			 * Modificamos vista o texto en caso de que sea recibido algun parametro por pantalla que consume el widget 
			 */
			$.TipNoAceptes.setText(params.texto);

		}
		if (!_.isUndefined(params.top)) {
			$.vista.setTop(params.top);

		}
		if (!_.isUndefined(params.bottom)) {
			$.vista.setBottom(params.bottom);

		}
		if (!_.isUndefined(params.ancho)) {
			var vista_ancho = params.ancho;

			if (vista_ancho == '*') {
				vista_ancho = Ti.UI.FILL;
			} else if (vista_ancho == '-') {
				vista_ancho = Ti.UI.SIZE;
			} else if (!isNaN(vista_ancho)) {
				vista_ancho = vista_ancho + 'dp';
			}
			$.vista.setWidth(vista_ancho);

		}
		if (!_.isUndefined(params.alto)) {
			var vista_alto = params.alto;

			if (vista_alto == '*') {
				vista_alto = Ti.UI.FILL;
			} else if (vista_alto == '-') {
				vista_alto = Ti.UI.SIZE;
			} else if (!isNaN(vista_alto)) {
				vista_alto = vista_alto + 'dp';
			}
			$.vista.setHeight(vista_alto);

			require(WPATH('vars'))[args.__id]['alto'] = params.alto;
		}
		if (!_.isUndefined(params.titulo)) {
			if ((_.isObject(params.titulo) || (_.isString(params.titulo)) && !_.isEmpty(params.titulo)) || _.isNumber(params.titulo) || _.isBoolean(params.titulo)) {
				$.ESPERATULLAMADA.setText(params.titulo);

				var ESPERATULLAMADA_visible = true;

				if (ESPERATULLAMADA_visible == 'si') {
					ESPERATULLAMADA_visible = true;
				} else if (ESPERATULLAMADA_visible == 'no') {
					ESPERATULLAMADA_visible = false;
				}
				$.ESPERATULLAMADA.setVisible(ESPERATULLAMADA_visible);

			} else {
				$.ESPERATULLAMADA.setText(params.titulo);

				var ESPERATULLAMADA_visible = false;

				if (ESPERATULLAMADA_visible == 'si') {
					ESPERATULLAMADA_visible = true;
				} else if (ESPERATULLAMADA_visible == 'no') {
					ESPERATULLAMADA_visible = false;
				}
				$.ESPERATULLAMADA.setVisible(ESPERATULLAMADA_visible);

			}
		}
	} else if (tipo == '_sorry') {
		/** 
		 * Mono triste vertical 
		 */
		var vista4_visible = visible;

		if (vista4_visible == 'si') {
			vista4_visible = true;
		} else if (vista4_visible == 'no') {
			vista4_visible = false;
		}
		$.vista4.setVisible(vista4_visible);

		var vista4_alto = alto;

		if (vista4_alto == '*') {
			vista4_alto = Ti.UI.FILL;
		} else if (vista4_alto == '-') {
			vista4_alto = Ti.UI.SIZE;
		} else if (!isNaN(vista4_alto)) {
			vista4_alto = vista4_alto + 'dp';
		}
		$.vista4.setHeight(vista4_alto);

		/** 
		 * ocultamos otros tipos 
		 */
		var vista_visible = false;

		if (vista_visible == 'si') {
			vista_visible = true;
		} else if (vista_visible == 'no') {
			vista_visible = false;
		}
		$.vista.setVisible(vista_visible);

		var vista_alto = 0;

		if (vista_alto == '*') {
			vista_alto = Ti.UI.FILL;
		} else if (vista_alto == '-') {
			vista_alto = Ti.UI.SIZE;
		} else if (!isNaN(vista_alto)) {
			vista_alto = vista_alto + 'dp';
		}
		$.vista.setHeight(vista_alto);

		var vista6_visible = false;

		if (vista6_visible == 'si') {
			vista6_visible = true;
		} else if (vista6_visible == 'no') {
			vista6_visible = false;
		}
		$.vista6.setVisible(vista6_visible);

		var vista6_alto = 0;

		if (vista6_alto == '*') {
			vista6_alto = Ti.UI.FILL;
		} else if (vista6_alto == '-') {
			vista6_alto = Ti.UI.SIZE;
		} else if (!isNaN(vista6_alto)) {
			vista6_alto = vista6_alto + 'dp';
		}
		$.vista6.setHeight(vista6_alto);

		if (!_.isUndefined(params.titulo)) {
			/** 
			 * Modificamos vista o texto en caso de que sea recibido algun parametro por pantalla que consume el widget 
			 */
			$.NOTIENESTAREAS.setText(params.titulo);

		}
		if (!_.isUndefined(params.texto)) {
			$.Aseguratede.setText(params.texto);

		}
		if (!_.isUndefined(params.top)) {
			$.vista4.setTop(params.top);

		}
		if (!_.isUndefined(params.bottom)) {
			$.vista4.setBottom(params.bottom);

		}
		if (!_.isUndefined(params.ancho)) {
			var vista4_ancho = params.ancho;

			if (vista4_ancho == '*') {
				vista4_ancho = Ti.UI.FILL;
			} else if (vista4_ancho == '-') {
				vista4_ancho = Ti.UI.SIZE;
			} else if (!isNaN(vista4_ancho)) {
				vista4_ancho = vista4_ancho + 'dp';
			}
			$.vista4.setWidth(vista4_ancho);

		}
		if (!_.isUndefined(params.alto)) {
			var vista4_alto = params.alto;

			if (vista4_alto == '*') {
				vista4_alto = Ti.UI.FILL;
			} else if (vista4_alto == '-') {
				vista4_alto = Ti.UI.SIZE;
			} else if (!isNaN(vista4_alto)) {
				vista4_alto = vista4_alto + 'dp';
			}
			$.vista4.setHeight(vista4_alto);

			require(WPATH('vars'))[args.__id]['alto'] = params.alto;
		}
	} else if (tipo == '_info') {
		/** 
		 * mono happy vertical 
		 */
		var vista6_visible = visible;

		if (vista6_visible == 'si') {
			vista6_visible = true;
		} else if (vista6_visible == 'no') {
			vista6_visible = false;
		}
		$.vista6.setVisible(vista6_visible);

		var vista6_alto = alto;

		if (vista6_alto == '*') {
			vista6_alto = Ti.UI.FILL;
		} else if (vista6_alto == '-') {
			vista6_alto = Ti.UI.SIZE;
		} else if (!isNaN(vista6_alto)) {
			vista6_alto = vista6_alto + 'dp';
		}
		$.vista6.setHeight(vista6_alto);

		/** 
		 * ocultamos otros tipos 
		 */
		var vista_visible = false;

		if (vista_visible == 'si') {
			vista_visible = true;
		} else if (vista_visible == 'no') {
			vista_visible = false;
		}
		$.vista.setVisible(vista_visible);

		var vista_alto = 0;

		if (vista_alto == '*') {
			vista_alto = Ti.UI.FILL;
		} else if (vista_alto == '-') {
			vista_alto = Ti.UI.SIZE;
		} else if (!isNaN(vista_alto)) {
			vista_alto = vista_alto + 'dp';
		}
		$.vista.setHeight(vista_alto);

		var vista4_visible = false;

		if (vista4_visible == 'si') {
			vista4_visible = true;
		} else if (vista4_visible == 'no') {
			vista4_visible = false;
		}
		$.vista4.setVisible(vista4_visible);

		var vista4_alto = 0;

		if (vista4_alto == '*') {
			vista4_alto = Ti.UI.FILL;
		} else if (vista4_alto == '-') {
			vista4_alto = Ti.UI.SIZE;
		} else if (!isNaN(vista4_alto)) {
			vista4_alto = vista4_alto + 'dp';
		}
		$.vista4.setHeight(vista4_alto);

		if (!_.isUndefined(params.titulo)) {
			/** 
			 * Modificamos vista o texto en caso de que sea recibido algun parametro por pantalla que consume el widget 
			 */
			$.PUEDECONTINUAR.setText(params.titulo);

		}
		if (!_.isUndefined(params.texto)) {
			$.SiEstel.setText(params.texto);

		}
		if (!_.isUndefined(params.top)) {
			$.vista6.setTop(params.top);

		}
		if (!_.isUndefined(params.bottom)) {
			$.vista6.setBottom(params.bottom);

		}
		if (!_.isUndefined(params.ancho)) {
			var vista6_ancho = params.ancho;

			if (vista6_ancho == '*') {
				vista6_ancho = Ti.UI.FILL;
			} else if (vista6_ancho == '-') {
				vista6_ancho = Ti.UI.SIZE;
			} else if (!isNaN(vista6_ancho)) {
				vista6_ancho = vista6_ancho + 'dp';
			}
			$.vista6.setWidth(vista6_ancho);

		}
		if (!_.isUndefined(params.alto)) {
			var vista6_alto = params.alto;

			if (vista6_alto == '*') {
				vista6_alto = Ti.UI.FILL;
			} else if (vista6_alto == '-') {
				vista6_alto = Ti.UI.SIZE;
			} else if (!isNaN(vista6_alto)) {
				vista6_alto = vista6_alto + 'dp';
			}
			$.vista6.setHeight(vista6_alto);

			require(WPATH('vars'))[args.__id]['alto'] = params.alto;
		}
	}
};

/** 
 * Funcion que oculta widget 
 */

$.ocultar = function(params) {
	/** 
	 * Ocultamos todas...
	 * _tip 
	 */
	var vista_visible = false;

	if (vista_visible == 'si') {
		vista_visible = true;
	} else if (vista_visible == 'no') {
		vista_visible = false;
	}
	$.vista.setVisible(vista_visible);

	var vista_alto = 0;

	if (vista_alto == '*') {
		vista_alto = Ti.UI.FILL;
	} else if (vista_alto == '-') {
		vista_alto = Ti.UI.SIZE;
	} else if (!isNaN(vista_alto)) {
		vista_alto = vista_alto + 'dp';
	}
	$.vista.setHeight(vista_alto);

	/** 
	 * Ocultamos _sorry 
	 */
	var vista4_visible = false;

	if (vista4_visible == 'si') {
		vista4_visible = true;
	} else if (vista4_visible == 'no') {
		vista4_visible = false;
	}
	$.vista4.setVisible(vista4_visible);

	var vista4_alto = 0;

	if (vista4_alto == '*') {
		vista4_alto = Ti.UI.FILL;
	} else if (vista4_alto == '-') {
		vista4_alto = Ti.UI.SIZE;
	} else if (!isNaN(vista4_alto)) {
		vista4_alto = vista4_alto + 'dp';
	}
	$.vista4.setHeight(vista4_alto);

	/** 
	 * Ocultamos _info 
	 */
	var vista6_visible = false;

	if (vista6_visible == 'si') {
		vista6_visible = true;
	} else if (vista6_visible == 'no') {
		vista6_visible = false;
	}
	$.vista6.setVisible(vista6_visible);

	var vista6_alto = 0;

	if (vista6_alto == '*') {
		vista6_alto = Ti.UI.FILL;
	} else if (vista6_alto == '-') {
		vista6_alto = Ti.UI.SIZE;
	} else if (!isNaN(vista6_alto)) {
		vista6_alto = vista6_alto + 'dp';
	}
	$.vista6.setHeight(vista6_alto);

};

/** 
 * Funcion que muestra widget 
 */

$.mostrar = function(params) {
	/** 
	 * Mostramos ultimo tipo 
	 */
	var tipo = ('tipo' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['tipo'] : '';
	var visible = ('visible' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['visible'] : '';
	if (!_.isUndefined(params.tipo)) {
		if (tipo == '_tip') {
			/** 
			 * Dejamos _tip como estaba 
			 */
			var vista_visible = visible;

			if (vista_visible == 'si') {
				vista_visible = true;
			} else if (vista_visible == 'no') {
				vista_visible = false;
			}
			$.vista.setVisible(vista_visible);

			var vista_alto = 120;

			if (vista_alto == '*') {
				vista_alto = Ti.UI.FILL;
			} else if (vista_alto == '-') {
				vista_alto = Ti.UI.SIZE;
			} else if (!isNaN(vista_alto)) {
				vista_alto = vista_alto + 'dp';
			}
			$.vista.setHeight(vista_alto);

			/** 
			 * Ocultamos _sorry 
			 */
			var vista4_visible = false;

			if (vista4_visible == 'si') {
				vista4_visible = true;
			} else if (vista4_visible == 'no') {
				vista4_visible = false;
			}
			$.vista4.setVisible(vista4_visible);

			var vista4_alto = 0;

			if (vista4_alto == '*') {
				vista4_alto = Ti.UI.FILL;
			} else if (vista4_alto == '-') {
				vista4_alto = Ti.UI.SIZE;
			} else if (!isNaN(vista4_alto)) {
				vista4_alto = vista4_alto + 'dp';
			}
			$.vista4.setHeight(vista4_alto);

			/** 
			 * Ocultamos _info 
			 */
			var vista6_visible = false;

			if (vista6_visible == 'si') {
				vista6_visible = true;
			} else if (vista6_visible == 'no') {
				vista6_visible = false;
			}
			$.vista6.setVisible(vista6_visible);

			var vista6_alto = 0;

			if (vista6_alto == '*') {
				vista6_alto = Ti.UI.FILL;
			} else if (vista6_alto == '-') {
				vista6_alto = Ti.UI.SIZE;
			} else if (!isNaN(vista6_alto)) {
				vista6_alto = vista6_alto + 'dp';
			}
			$.vista6.setHeight(vista6_alto);

		} else if (tipo == '_sorry') {
			/** 
			 * Ocultamos _tip 
			 */
			var vista_visible = false;

			if (vista_visible == 'si') {
				vista_visible = true;
			} else if (vista_visible == 'no') {
				vista_visible = false;
			}
			$.vista.setVisible(vista_visible);

			var vista_alto = 0;

			if (vista_alto == '*') {
				vista_alto = Ti.UI.FILL;
			} else if (vista_alto == '-') {
				vista_alto = Ti.UI.SIZE;
			} else if (!isNaN(vista_alto)) {
				vista_alto = vista_alto + 'dp';
			}
			$.vista.setHeight(vista_alto);

			/** 
			 * Ocultamos _info 
			 */
			var vista6_visible = false;

			if (vista6_visible == 'si') {
				vista6_visible = true;
			} else if (vista6_visible == 'no') {
				vista6_visible = false;
			}
			$.vista6.setVisible(vista6_visible);

			var vista6_alto = 0;

			if (vista6_alto == '*') {
				vista6_alto = Ti.UI.FILL;
			} else if (vista6_alto == '-') {
				vista6_alto = Ti.UI.SIZE;
			} else if (!isNaN(vista6_alto)) {
				vista6_alto = vista6_alto + 'dp';
			}
			$.vista6.setHeight(vista6_alto);

			/** 
			 * Dejamos _sorry como estaba 
			 */
			var vista4_visible = visible;

			if (vista4_visible == 'si') {
				vista4_visible = true;
			} else if (vista4_visible == 'no') {
				vista4_visible = false;
			}
			$.vista4.setVisible(vista4_visible);

			var vista4_alto = '-';

			if (vista4_alto == '*') {
				vista4_alto = Ti.UI.FILL;
			} else if (vista4_alto == '-') {
				vista4_alto = Ti.UI.SIZE;
			} else if (!isNaN(vista4_alto)) {
				vista4_alto = vista4_alto + 'dp';
			}
			$.vista4.setHeight(vista4_alto);

		} else if (tipo == '_info') {
			/** 
			 * Ocultamos _tip 
			 */
			var vista_visible = false;

			if (vista_visible == 'si') {
				vista_visible = true;
			} else if (vista_visible == 'no') {
				vista_visible = false;
			}
			$.vista.setVisible(vista_visible);

			var vista_alto = 0;

			if (vista_alto == '*') {
				vista_alto = Ti.UI.FILL;
			} else if (vista_alto == '-') {
				vista_alto = Ti.UI.SIZE;
			} else if (!isNaN(vista_alto)) {
				vista_alto = vista_alto + 'dp';
			}
			$.vista.setHeight(vista_alto);

			/** 
			 * Ocultamos _sorry 
			 */
			var vista4_visible = false;

			if (vista4_visible == 'si') {
				vista4_visible = true;
			} else if (vista4_visible == 'no') {
				vista4_visible = false;
			}
			$.vista4.setVisible(vista4_visible);

			var vista4_alto = 0;

			if (vista4_alto == '*') {
				vista4_alto = Ti.UI.FILL;
			} else if (vista4_alto == '-') {
				vista4_alto = Ti.UI.SIZE;
			} else if (!isNaN(vista4_alto)) {
				vista4_alto = vista4_alto + 'dp';
			}
			$.vista4.setHeight(vista4_alto);

			/** 
			 * Dejamos _info como estaba 
			 */
			var vista6_visible = visible;

			if (vista6_visible == 'si') {
				vista6_visible = true;
			} else if (vista6_visible == 'no') {
				vista6_visible = false;
			}
			$.vista6.setVisible(vista6_visible);

			var vista6_alto = '-';

			if (vista6_alto == '*') {
				vista6_alto = Ti.UI.FILL;
			} else if (vista6_alto == '-') {
				vista6_alto = Ti.UI.SIZE;
			} else if (!isNaN(vista6_alto)) {
				vista6_alto = vista6_alto + 'dp';
			}
			$.vista6.setHeight(vista6_alto);

		}
	} else {
		if (tipo == '_tip') {
			/** 
			 * Dependiendo del tipo de mono a mostrar, ocultamos todas excepto la que necesitamos mostrar 
			 */
			var vista_visible = visible;

			if (vista_visible == 'si') {
				vista_visible = true;
			} else if (vista_visible == 'no') {
				vista_visible = false;
			}
			$.vista.setVisible(vista_visible);

			var vista_alto = 120;

			if (vista_alto == '*') {
				vista_alto = Ti.UI.FILL;
			} else if (vista_alto == '-') {
				vista_alto = Ti.UI.SIZE;
			} else if (!isNaN(vista_alto)) {
				vista_alto = vista_alto + 'dp';
			}
			$.vista.setHeight(vista_alto);

			var vista4_visible = false;

			if (vista4_visible == 'si') {
				vista4_visible = true;
			} else if (vista4_visible == 'no') {
				vista4_visible = false;
			}
			$.vista4.setVisible(vista4_visible);

			var vista4_alto = 0;

			if (vista4_alto == '*') {
				vista4_alto = Ti.UI.FILL;
			} else if (vista4_alto == '-') {
				vista4_alto = Ti.UI.SIZE;
			} else if (!isNaN(vista4_alto)) {
				vista4_alto = vista4_alto + 'dp';
			}
			$.vista4.setHeight(vista4_alto);

			var vista6_visible = false;

			if (vista6_visible == 'si') {
				vista6_visible = true;
			} else if (vista6_visible == 'no') {
				vista6_visible = false;
			}
			$.vista6.setVisible(vista6_visible);

			var vista6_alto = 0;

			if (vista6_alto == '*') {
				vista6_alto = Ti.UI.FILL;
			} else if (vista6_alto == '-') {
				vista6_alto = Ti.UI.SIZE;
			} else if (!isNaN(vista6_alto)) {
				vista6_alto = vista6_alto + 'dp';
			}
			$.vista6.setHeight(vista6_alto);

		} else if (tipo == '_sorry') {
			/** 
			 * Ocultamos _tip 
			 */
			var vista_visible = false;

			if (vista_visible == 'si') {
				vista_visible = true;
			} else if (vista_visible == 'no') {
				vista_visible = false;
			}
			$.vista.setVisible(vista_visible);

			var vista_alto = 0;

			if (vista_alto == '*') {
				vista_alto = Ti.UI.FILL;
			} else if (vista_alto == '-') {
				vista_alto = Ti.UI.SIZE;
			} else if (!isNaN(vista_alto)) {
				vista_alto = vista_alto + 'dp';
			}
			$.vista.setHeight(vista_alto);

			/** 
			 * Ocultamos _info 
			 */
			var vista6_visible = false;

			if (vista6_visible == 'si') {
				vista6_visible = true;
			} else if (vista6_visible == 'no') {
				vista6_visible = false;
			}
			$.vista6.setVisible(vista6_visible);

			var vista6_alto = 0;

			if (vista6_alto == '*') {
				vista6_alto = Ti.UI.FILL;
			} else if (vista6_alto == '-') {
				vista6_alto = Ti.UI.SIZE;
			} else if (!isNaN(vista6_alto)) {
				vista6_alto = vista6_alto + 'dp';
			}
			$.vista6.setHeight(vista6_alto);

			/** 
			 * Dejamos _sorry como estaba 
			 */
			var vista4_visible = visible;

			if (vista4_visible == 'si') {
				vista4_visible = true;
			} else if (vista4_visible == 'no') {
				vista4_visible = false;
			}
			$.vista4.setVisible(vista4_visible);

			var vista4_alto = '-';

			if (vista4_alto == '*') {
				vista4_alto = Ti.UI.FILL;
			} else if (vista4_alto == '-') {
				vista4_alto = Ti.UI.SIZE;
			} else if (!isNaN(vista4_alto)) {
				vista4_alto = vista4_alto + 'dp';
			}
			$.vista4.setHeight(vista4_alto);

		} else if (tipo == '_info') {
			/** 
			 * Ocultamos _tip 
			 */
			var vista_visible = false;

			if (vista_visible == 'si') {
				vista_visible = true;
			} else if (vista_visible == 'no') {
				vista_visible = false;
			}
			$.vista.setVisible(vista_visible);

			var vista_alto = 0;

			if (vista_alto == '*') {
				vista_alto = Ti.UI.FILL;
			} else if (vista_alto == '-') {
				vista_alto = Ti.UI.SIZE;
			} else if (!isNaN(vista_alto)) {
				vista_alto = vista_alto + 'dp';
			}
			$.vista.setHeight(vista_alto);

			/** 
			 * Ocultamos _sorry 
			 */
			var vista4_visible = false;

			if (vista4_visible == 'si') {
				vista4_visible = true;
			} else if (vista4_visible == 'no') {
				vista4_visible = false;
			}
			$.vista4.setVisible(vista4_visible);

			var vista4_alto = 0;

			if (vista4_alto == '*') {
				vista4_alto = Ti.UI.FILL;
			} else if (vista4_alto == '-') {
				vista4_alto = Ti.UI.SIZE;
			} else if (!isNaN(vista4_alto)) {
				vista4_alto = vista4_alto + 'dp';
			}
			$.vista4.setHeight(vista4_alto);

			/** 
			 * Dejamos _info como estaba 
			 */
			var vista6_visible = visible;

			if (vista6_visible == 'si') {
				vista6_visible = true;
			} else if (vista6_visible == 'no') {
				vista6_visible = false;
			}
			$.vista6.setVisible(vista6_visible);

			var vista6_alto = '-';

			if (vista6_alto == '*') {
				vista6_alto = Ti.UI.FILL;
			} else if (vista6_alto == '-') {
				vista6_alto = Ti.UI.SIZE;
			} else if (!isNaN(vista6_alto)) {
				vista6_alto = vista6_alto + 'dp';
			}
			$.vista6.setHeight(vista6_alto);

		}
	}
};