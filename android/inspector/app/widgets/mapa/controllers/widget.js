/** 
 * Widget Mapa
 * Control que muestra el mapa usado en las pantallas de detalle, con soporte de rutas multiples, o desde un punto a otro.
 * Ademas muestra datos como: direccion, distancia, ciudad y comuna. Si hay bono, tambien se muestra.
 * 
 * @param tipo Define el tipo de mapa: ubicacion, origen, multiples
 * @param seguimiento Si true, muestra animacion de tarea en seguimiento
 * @param externo Si true, muestra boton para abrir ruta en apps externas (apple/google maps, waze)
 * @param _externo_titulo Modifica el titulo del popup 'Abrir ruta en:'
 * @param _externo_apple Modifica el titulo del boton 'Apple Maps'
 * @param _externo_google Modifica el titulo del boton 'Google Maps'
 * @param _externo_waze Modifica el titulo del boton 'Waze'
 * @param _externo_cancelar Modifica el texto del boton 'Cancelar'
 * @param direccion Indica el texto de la direccion de destino
 * @param distancia Indica el texto de la distancia hacia el destino
 * @param comuna Indica el texto de la comuna
 * @param ciudad Indica el texto de la ciudad
 * @param bono Indica el valor a mostrar el campo bono (opcional)
 * @param _bono Indica el texto del label del campo bono (opcional)
 * @param monito Si true, muestra monito de seguimiento 
 */
var _bind4section = {};

var args = arguments[0] || {};


$.pinchoMapa.applyProperties({
	latitude: parseFloat('0.0'),
	longitude: parseFloat('0.0'),
	image: WPATH('/images/i0752E598FC723F6708D472F7AB34311A.png')
});


$.pinchoMapa2.applyProperties({
	latitude: parseFloat('0.0'),
	longitude: parseFloat('0.0'),
	image: WPATH('/images/iA4447614C019AF8FFC6E718892E39183.png')
});


$.pinchoMapa3.applyProperties({
	latitude: parseFloat('0.0'),
	longitude: parseFloat('0.0'),
	image: WPATH('/images/iB8F4698ED4EF9DE4D073ADC090AF07A3.png')
});


$.pinchoMapa4.applyProperties({
	latitude: parseFloat('0.0'),
	longitude: parseFloat('0.0'),
	image: WPATH('/images/iA3D34032D37972198904AD753EA1DE26.png')
});


$.pinchoMapa5.applyProperties({
	latitude: parseFloat('0.0'),
	longitude: parseFloat('0.0'),
	image: WPATH('/images/iA7D179C87F7325F3FBDAB36A6CA3FC58.png')
});


$.pinchoMapa6.applyProperties({
	latitude: parseFloat('0.0'),
	longitude: parseFloat('0.0'),
	image: WPATH('/images/i07BD9F7C5EDAEC7FD998A077330516E5.png')
});


$.pinchoMapa7.applyProperties({
	latitude: parseFloat('0.0'),
	longitude: parseFloat('0.0'),
	image: WPATH('/images/iE3B5F64AE888BA3A54B0BC278814961A.png')
});


$.pinchoMapa8.applyProperties({
	latitude: parseFloat('0.0'),
	longitude: parseFloat('0.0'),
	image: WPATH('/images/iBFE4DC825CD3D239883F7A858B1B14D8.png')
});


$.pinchoMapa9.applyProperties({
	latitude: parseFloat('0.0'),
	longitude: parseFloat('0.0'),
	image: WPATH('/images/i733093F68676D2D585523953DFE16DBF.png')
});


$.pinchoMapa10.applyProperties({
	latitude: parseFloat('0.0'),
	longitude: parseFloat('0.0'),
	image: WPATH('/images/i801CAD94710E3C73011A5EF0C6AF55E3.png')
});


$.pinchoMapa11.applyProperties({
	latitude: parseFloat('0.0'),
	longitude: parseFloat('0.0'),
	image: WPATH('/images/iA76649B47890BD38C12BC0A7922891CE.png')
});


$.pinchoMapa12.applyProperties({
	latitude: parseFloat('0.0'),
	longitude: parseFloat('0.0'),
	image: WPATH('/images/i57157C0EED282EF313085642E8C9B543.png')
});


$.mapa.setRegion({
	latitude: -33.392047,
	longitude: -70.542939,
	latitudeDelta: 0.005,
	longitudeDelta: 0.005
});
$.mapa.applyProperties({});
if (OS_IOS) {
	var mapa_camara = require('ti.map').createCamera({
		pitch: 0,
		heading: 0
	});
	$.mapa.setCamera(mapa_camara);
}

function Touchstart_imagen(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var evento = e;
	/** 
	 * Cambiamos imagen cuando es presionado 
	 */
	var imagen_imagen = 'ruta_on';

	if (typeof imagen_imagen == 'string' && 'styles' in require(WPATH('a4w')) && imagen_imagen in require(WPATH('a4w')).styles['images']) {
		imagen_imagen = WPATH(require(WPATH('a4w')).styles['images'][imagen_imagen]);
	}
	$.imagen.setImage(imagen_imagen);


}

function Touchend_imagen(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var evento = e;
	/** 
	 * Dejamos la imagen original, recuperamos variables, y dependiendo del sistema operativo es si cargamos google maps, o apple maps 
	 */
	var imagen_imagen = 'ruta_off';

	if (typeof imagen_imagen == 'string' && 'styles' in require(WPATH('a4w')) && imagen_imagen in require(WPATH('a4w')).styles['images']) {
		imagen_imagen = WPATH(require(WPATH('a4w')).styles['images'][imagen_imagen]);
	}
	$.imagen.setImage(imagen_imagen);

	var _externo_titulo = ('_externo_titulo' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['_externo_titulo'] : '';
	var _externo_apple = ('_externo_apple' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['_externo_apple'] : '';
	var _externo_google = ('_externo_google' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['_externo_google'] : '';
	var _externo_waze = ('_externo_waze' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['_externo_waze'] : '';
	var _externo_cancelar = ('_externo_cancelar' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['_externo_cancelar'] : '';
	var datos_externo = ('datos_externo' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['datos_externo'] : '';
	if (Ti.Platform.osname == 'android') {
		var preguntarOpciones_opts = [_externo_google, _externo_waze, _externo_cancelar];
		var preguntarOpciones = Ti.UI.createOptionDialog({
			title: '' + _externo_titulo + '',
			options: preguntarOpciones_opts
		});
		preguntarOpciones.addEventListener('click', function(e) {
			var x = preguntarOpciones_opts[e.index];
			if (x == _externo_google) {
				if (_.isObject(datos_externo) && !_.isArray(datos_externo) && !_.isFunction(datos_externo)) {
					if (Ti.App.deployType != 'production') console.log('PSB: TODO: AQUI FALTA EL CORRECTO DE GOOGLE MAPS', {});

					Ti.Platform.openURL('http://maps.google.com/?saddr=' + datos_externo.origen_latitud + ',' + datos_externo.origen_longitud + '&daddr=' + datos_externo.destino_latitud + ',' + datos_externo.destino_longitud)
				}
			} else if (x == _externo_waze) {
				if (_.isObject(datos_externo) && !_.isArray(datos_externo) && !_.isFunction(datos_externo)) {

					Ti.Platform.openURL('waze://?ll=' + datos_externo.destino_latitud + ',' + datos_externo.destino_longitud)
				}
			}
			x = null;

			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarOpciones.show();
	} else {
		var preguntarOpciones2_opts = [_externo_apple, _externo_waze, _externo_cancelar];
		var preguntarOpciones2 = Ti.UI.createOptionDialog({
			title: '' + _externo_titulo + '',
			options: preguntarOpciones2_opts
		});
		preguntarOpciones2.addEventListener('click', function(e) {
			var x = preguntarOpciones2_opts[e.index];
			var datos_externo = ('datos_externo' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['datos_externo'] : '';
			if (x == _externo_apple) {
				if (_.isObject(datos_externo) && !_.isArray(datos_externo) && !_.isFunction(datos_externo)) {

					Ti.Platform.openURL('http://maps.apple.com/?saddr=' + datos_externo.origen_latitud + ',' + datos_externo.origen_longitud + '&daddr=' + datos_externo.destino_latitud + ',' + datos_externo.destino_longitud)
				}
			} else if (x == _externo_waze) {
				if (_.isObject(datos_externo) && !_.isArray(datos_externo) && !_.isFunction(datos_externo)) {

					Ti.Platform.openURL('waze://?ll=' + datos_externo.destino_latitud + ',' + datos_externo.destino_longitud)
				}
			}
			x = null;

			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarOpciones2.show();
	}
}

function Load_imagen2(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var evento = e;
	elemento.start();

}
/** 
 * Funcion que inicializa el widget 
 */

$.init = function(params) {
	for (var tobe in params) args[tobe] = params[tobe];
	if ('__id' in params) require(WPATH('vars'))[params.__id] = {};
	/** 
	 * Seteamos variables con defaults 
	 */
	require(WPATH('vars'))[args.__id]['bt_externo'] = 'false';
	require(WPATH('vars'))[args.__id]['_externo_titulo'] = 'Abrir ruta en:';
	require(WPATH('vars'))[args.__id]['_externo_apple'] = 'Apple Maps';
	require(WPATH('vars'))[args.__id]['_externo_google'] = 'Google Maps';
	require(WPATH('vars'))[args.__id]['_externo_waze'] = 'Waze';
	require(WPATH('vars'))[args.__id]['_externo_cancelar'] = 'Cerrar';
	require(WPATH('vars'))[args.__id]['seguimiento'] = 'false';
	require(WPATH('vars'))[args.__id]['label_a'] = 'a';
	if (!_.isUndefined(params.seguimiento)) {
		/** 
		 * Editamos campos segun parametros definidos al iniciar el widget 
		 */
		require(WPATH('vars'))[args.__id]['seguimiento'] = params.seguimiento;
	}
	if (!_.isUndefined(params.externo)) {
		if (!_.isUndefined(params._externo_titulo)) {
			require(WPATH('vars'))[args.__id]['_externo_titulo'] = params._externo_titulo;
		}
		if (!_.isUndefined(params._externo_apple)) {
			require(WPATH('vars'))[args.__id]['_externo_apple'] = params._externo_apple;
		}
		if (!_.isUndefined(params._externo_google)) {
			require(WPATH('vars'))[args.__id]['_externo_google'] = params._externo_google;
		}
		if (!_.isUndefined(params._externo_waze)) {
			require(WPATH('vars'))[args.__id]['_externo_waze'] = params._externo_waze;
		}
		if (!_.isUndefined(params._externo_cancelar)) {
			require(WPATH('vars'))[args.__id]['_externo_cancelar'] = params._externo_cancelar;
		}
		if (params.externo == true || params.externo == 'true') {
			require(WPATH('vars'))[args.__id]['bt_externo'] = 'true';
		} else {
			require(WPATH('vars'))[args.__id]['bt_externo'] = 'false';
		}
	}
	if (!_.isUndefined(params.tipo)) {
		require(WPATH('vars'))[args.__id]['tipo'] = params.tipo;
		if (params.tipo == 'ubicacion') {
			$.pinchoMapa.setLatitude('0.0');

			$.pinchoMapa.setLongitude('0.0');

			var _ifunc_res_obtenerGPS = function(e) {
				if (e.error) {
					var origen = {
						error: true,
						latitude: -1,
						longitude: -1,
						reason: (e.reason) ? e.reason : 'unknown',
						accuracy: 0,
						speed: 0,
						error_compass: false
					};
				} else {
					var origen = e;
				}
				if (origen.error == true || origen.error == 'true') {

					if ('__args' in args) {
						args['__args'].onerror({
							tipo: 'gps'
						});
					} else {
						args.onerror({
							tipo: 'gps'
						});
					}
				} else {
					var mapa_latitud = origen.latitude;

					var mapa_tmp = $.mapa.getRegion();
					mapa_tmp.latitude = mapa_latitud;
					mapa_latitud = mapa_tmp;
					$.mapa.setRegion(mapa_latitud);

					var mapa_longitud = origen.longitude;

					var mapa_tmp = $.mapa.getRegion();
					mapa_tmp.longitude = mapa_longitud;
					mapa_longitud = mapa_tmp;
					$.mapa.setRegion(mapa_longitud);

					if (!_.isUndefined(params.latitud)) {
						if (!_.isUndefined(params.longitud)) {
							$.pinchoMapa2.setLatitude(params.latitud);

							$.pinchoMapa2.setLongitude(params.longitud);

							var datos_externo = {
								origen_latitud: origen.latitude,
								origen_longitud: origen.longitude,
								destino_latitud: params.latitud,
								destino_longitud: params.longitud
							};
							require(WPATH('vars'))[args.__id]['datos_externo'] = datos_externo;
							var bt_externo = ('bt_externo' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['bt_externo'] : '';
							if (params.externo == true || params.externo == 'true') {
								var imagen_visible = true;

								if (imagen_visible == 'si') {
									imagen_visible = true;
								} else if (imagen_visible == 'no') {
									imagen_visible = false;
								}
								$.imagen.setVisible(imagen_visible);

							} else {
								var imagen_visible = false;

								if (imagen_visible == 'si') {
									imagen_visible = true;
								} else if (imagen_visible == 'no') {
									imagen_visible = false;
								}
								$.imagen.setVisible(imagen_visible);

							}
							if (!_.isUndefined(params.ruta)) {
								if (params.ruta == true || params.ruta == 'true') {
									var consultarURL = {};

									consultarURL.success = function(e) {
										var elemento = e,
											valor = e;
										if (elemento.status == 'OK') {
											var ruta_previa = ('ruta_previa' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['ruta_previa'] : '';
											if (_.isObject(ruta_previa)) {
												$.mapa.removeRoute(ruta_previa);

											}

											var rutaMapa_poly = require(WPATH('polyline')).decode(elemento.routes[0].overview_polyline.points);
											var varruta = require('ti.map').createRoute({
												width: 6,
												color: '#2d9edb',
												points: rutaMapa_poly
											});
											require(WPATH('vars'))[args.__id]['ruta_previa'] = varruta;
											$.mapa.addRoute(varruta);

											var label_a = ('label_a' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['label_a'] : '';
											$.Distancia.setText(label_a + ' ' + elemento.routes[0].legs[0].distance.text);


											if ('__args' in args) {
												args['__args'].ondatos({
													ruta_distancia_texto: elemento.routes[0].legs[0].distance.text,
													ruta_distancia: elemento.routes[0].legs[0].distance.value,
													ruta_poly: elemento.routes[0].overview_polyline.points,
													ruta_origen: elemento.routes[0].legs[0].start_address,
													ruta_destino: elemento.routes[0].legs[0].end_address,
													ruta_metros: elemento.routes[0].legs[0].distance.value,
													ruta_tiempo_texto: elemento.routes[0].legs[0].duration.text,
													ruta_tiempo: elemento.routes[0].legs[0].duration.value,
													ruta_origen_calle: elemento.routes[0].legs[0].start_address.split(',')[0],
													ruta_origen_comuna: elemento.routes[0].legs[0].start_address.split(',')[1]
												});
											} else {
												args.ondatos({
													ruta_distancia_texto: elemento.routes[0].legs[0].distance.text,
													ruta_distancia: elemento.routes[0].legs[0].distance.value,
													ruta_poly: elemento.routes[0].overview_polyline.points,
													ruta_origen: elemento.routes[0].legs[0].start_address,
													ruta_destino: elemento.routes[0].legs[0].end_address,
													ruta_metros: elemento.routes[0].legs[0].distance.value,
													ruta_tiempo_texto: elemento.routes[0].legs[0].duration.text,
													ruta_tiempo: elemento.routes[0].legs[0].duration.value,
													ruta_origen_calle: elemento.routes[0].legs[0].start_address.split(',')[0],
													ruta_origen_comuna: elemento.routes[0].legs[0].start_address.split(',')[1]
												});
											}

											if ('__args' in args) {
												args['__args'].onlisto({
													tipo: 'ubicacion'
												});
											} else {
												args.onlisto({
													tipo: 'ubicacion'
												});
											}
										} else {

											if ('__args' in args) {
												args['__args'].onerror({
													tipo: 'ruta',
													last_data: elemento
												});
											} else {
												args.onerror({
													tipo: 'ruta',
													last_data: elemento
												});
											}
										}
										elemento = null, valor = null;
									};

									consultarURL.error = function(e) {
										var elemento = e,
											valor = e;

										if ('__args' in args) {
											args['__args'].onerror({
												tipo: 'ruta',
												last_data: params,
												elemento: elemento
											});
										} else {
											args.onerror({
												tipo: 'ruta',
												last_data: params,
												elemento: elemento
											});
										}
										elemento = null, valor = null;
									};
									require('helper').ajaxUnico('consultarURL', 'https://maps.googleapis.com/maps/api/directions/json?origin=' + origen.latitude + ',' + origen.longitude + '&destination=' + params.latitud + ',' + params.longitud + '&key=AIzaSyAJoATeoDjsJ3fNSe5q7eIKZ7VjT2AojqY', 'get', {}, 15000, consultarURL);
								} else {

									if ('__args' in args) {
										args['__args'].onlisto({
											tipo: 'ubicacion'
										});
									} else {
										args.onlisto({
											tipo: 'ubicacion'
										});
									}
								}
							} else {

								if ('__args' in args) {
									args['__args'].onlisto({
										tipo: 'ubicacion'
									});
								} else {
									args.onlisto({
										tipo: 'ubicacion'
									});
								}
							}
						} else {
							var imagen_visible = false;

							if (imagen_visible == 'si') {
								imagen_visible = true;
							} else if (imagen_visible == 'no') {
								imagen_visible = false;
							}
							$.imagen.setVisible(imagen_visible);


							if ('__args' in args) {
								args['__args'].onerror({
									tipo: 'falta_longitud'
								});
							} else {
								args.onerror({
									tipo: 'falta_longitud'
								});
							}
						}
					} else {

						if ('__args' in args) {
							args['__args'].onlisto({
								tipo: 'ubicacion'
							});
						} else {
							args.onlisto({
								tipo: 'ubicacion'
							});
						}
					}
				}
				if (Ti.App.deployType != 'production') console.log('ubicacion GPS dice', {
					"origen": origen
				});
			};
			Ti.Geolocation.setAccuracy(Ti.Geolocation.ACCURACY_HIGH);
			if (Ti.Geolocation.hasLocationPermissions(Ti.Geolocation.AUTHORIZATION_WHEN_IN_USE)) {
				Ti.Geolocation.getCurrentPosition(function(ee) {
					if (Ti.Geolocation.getHasCompass() == false) {
						var porigen = ('coords' in ee) ? ee.coords : {};
						porigen.error = ('coords' in ee) ? false : true;
						porigen.reason = '';
						porigen.speed = 0;
						porigen.compass = -1;
						porigen.compass_accuracy = -1;
						porigen.error_compass = true;
						_ifunc_res_obtenerGPS(porigen);
					} else {
						Ti.Geolocation.getCurrentHeading(function(yy) {
							var porigen = ('coords' in ee) ? ee.coords : {};
							porigen.error = ('coords' in ee) ? false : true;
							porigen.reason = ('error' in ee) ? ee.error : '';
							if (yy.error) {
								porigen.error_compass = true;
							} else {
								porigen.compass = yy.heading;
								porigen.compass_accuracy = yy.heading.accuracy;
							}
							_ifunc_res_obtenerGPS(porigen);
						});
					}
				});
			} else {
				Ti.Geolocation.requestLocationPermissions(Ti.Geolocation.AUTHORIZATION_WHEN_IN_USE, function(u) {
					if (u.success) {
						Ti.Geolocation.getCurrentPosition(function(ee) {
							if (Ti.Geolocation.getHasCompass() == false) {
								var porigen = ('coords' in ee) ? ee.coords : {};
								porigen.error = ('coords' in ee) ? false : true;
								porigen.reason = '';
								porigen.speed = 0;
								porigen.compass = -1;
								porigen.compass_accuracy = -1;
								porigen.error_compass = true;
								_ifunc_res_obtenerGPS(porigen);
							} else {
								Ti.Geolocation.getCurrentHeading(function(yy) {
									var porigen = ('coords' in ee) ? ee.coords : {};
									porigen.error = ('coords' in ee) ? false : true;
									porigen.reason = '';
									if (yy.error) {
										porigen.error_compass = true;
									} else {
										porigen.compass = yy.heading;
										porigen.compass_accuracy = yy.heading.accuracy;
									}
									_ifunc_res_obtenerGPS(porigen);
								});
							}
						});
					} else {
						_ifunc_res_obtenerGPS({
							error: true,
							latitude: -1,
							longitude: -1,
							reason: 'permission_denied',
							accuracy: 0,
							speed: 0,
							error_compass: false
						});
					}
				});
			}
		} else if (params.tipo == 'origen') {
			if (!_.isUndefined(params.origen_latitud)) {
				if (!_.isUndefined(params.origen_longitud)) {
					$.pinchoMapa.setLatitude(params.origen_latitud);

					$.pinchoMapa.setLongitude(params.origen_longitud);

					var mapa_latitud = params.origen_latitud;

					var mapa_tmp = $.mapa.getRegion();
					mapa_tmp.latitude = mapa_latitud;
					mapa_latitud = mapa_tmp;
					$.mapa.setRegion(mapa_latitud);

					var mapa_longitud = params.origen_longitud;

					var mapa_tmp = $.mapa.getRegion();
					mapa_tmp.longitude = mapa_longitud;
					mapa_longitud = mapa_tmp;
					$.mapa.setRegion(mapa_longitud);

					if (!_.isUndefined(params.destino_latitud)) {
						if (!_.isUndefined(params.destino_longitud)) {
							$.pinchoMapa2.setLatitude(params.destino_latitud);

							$.pinchoMapa2.setLongitude(params.destino_longitud);

							var datos_externo = {
								origen_latitud: params.origen_latitude,
								origen_longitud: params.origen_longitud,
								destino_latitud: params.destino_latitud,
								destino_longitud: params.destino_longitud
							};
							require(WPATH('vars'))[args.__id]['datos_externo'] = datos_externo;
							var bt_externo = ('bt_externo' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['bt_externo'] : '';
							if (params.externo == true || params.externo == 'true') {
								var imagen_visible = true;

								if (imagen_visible == 'si') {
									imagen_visible = true;
								} else if (imagen_visible == 'no') {
									imagen_visible = false;
								}
								$.imagen.setVisible(imagen_visible);

							} else {
								var imagen_visible = false;

								if (imagen_visible == 'si') {
									imagen_visible = true;
								} else if (imagen_visible == 'no') {
									imagen_visible = false;
								}
								$.imagen.setVisible(imagen_visible);

							}
							if (!_.isUndefined(params.ruta)) {
								if (params.ruta == true || params.ruta == 'true') {
									var consultarURL2 = {};

									consultarURL2.success = function(e) {
										var elemento = e,
											valor = e;
										if (elemento.status == 'OK') {
											var ruta_previa = ('ruta_previa' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['ruta_previa'] : '';
											if (_.isObject(ruta_previa)) {
												$.mapa.removeRoute(ruta_previa);

											}

											var rutaMapa2_poly = require(WPATH('polyline')).decode(elemento.routes[0].overview_polyline.points);
											var varruta = require('ti.map').createRoute({
												width: 6,
												color: '#2d9edb',
												points: rutaMapa2_poly
											});
											require(WPATH('vars'))[args.__id]['ruta_previa'] = varruta;
											$.mapa.addRoute(varruta);

											var label_a = ('label_a' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['label_a'] : '';
											$.Distancia.setText(label_a + ' ' + elemento.routes[0].legs[0].distance.text);


											if ('__args' in args) {
												args['__args'].ondatos({
													ruta_distancia_texto: elemento.routes[0].legs[0].distance.text,
													ruta_distancia: elemento.routes[0].legs[0].distance.value,
													ruta_poly: elemento.routes[0].overview_polyline.points,
													ruta_origen: elemento.routes[0].legs[0].start_address,
													ruta_destino: elemento.routes[0].legs[0].end_address,
													ruta_metros: elemento.routes[0].legs[0].distance.value,
													ruta_tiempo_texto: elemento.routes[0].legs[0].duration.text,
													ruta_tiempo: elemento.routes[0].legs[0].duration.value,
													ruta_origen_calle: elemento.routes[0].legs[0].start_address.split(',')[0],
													ruta_origen_comuna: elemento.routes[0].legs[0].start_address.split(',')[1]
												});
											} else {
												args.ondatos({
													ruta_distancia_texto: elemento.routes[0].legs[0].distance.text,
													ruta_distancia: elemento.routes[0].legs[0].distance.value,
													ruta_poly: elemento.routes[0].overview_polyline.points,
													ruta_origen: elemento.routes[0].legs[0].start_address,
													ruta_destino: elemento.routes[0].legs[0].end_address,
													ruta_metros: elemento.routes[0].legs[0].distance.value,
													ruta_tiempo_texto: elemento.routes[0].legs[0].duration.text,
													ruta_tiempo: elemento.routes[0].legs[0].duration.value,
													ruta_origen_calle: elemento.routes[0].legs[0].start_address.split(',')[0],
													ruta_origen_comuna: elemento.routes[0].legs[0].start_address.split(',')[1]
												});
											}

											if ('__args' in args) {
												args['__args'].onlisto({
													tipo: 'origen'
												});
											} else {
												args.onlisto({
													tipo: 'origen'
												});
											}
										} else {

											if ('__args' in args) {
												args['__args'].onerror({
													tipo: 'ruta'
												});
											} else {
												args.onerror({
													tipo: 'ruta'
												});
											}
										}
										elemento = null, valor = null;
									};

									consultarURL2.error = function(e) {
										var elemento = e,
											valor = e;

										if ('__args' in args) {
											args['__args'].onerror({
												tipo: 'ruta'
											});
										} else {
											args.onerror({
												tipo: 'ruta'
											});
										}
										elemento = null, valor = null;
									};
									require('helper').ajaxUnico('consultarURL2', 'https://maps.googleapis.com/maps/api/directions/json?origin=' + params.origen_latitud + ',' + params.origen_longitud + '&destination=' + params.destino_latitud + ',' + params.destino_longitud + '&key=AIzaSyAJoATeoDjsJ3fNSe5q7eIKZ7VjT2AojqY', 'get', {}, 15000, consultarURL2);
								} else {

									if ('__args' in args) {
										args['__args'].onlisto({
											tipo: 'origen'
										});
									} else {
										args.onlisto({
											tipo: 'origen'
										});
									}
								}
							} else {

								if ('__args' in args) {
									args['__args'].onlisto({
										tipo: 'origen'
									});
								} else {
									args.onlisto({
										tipo: 'origen'
									});
								}
							}
						} else {
							var imagen_visible = false;

							if (imagen_visible == 'si') {
								imagen_visible = true;
							} else if (imagen_visible == 'no') {
								imagen_visible = false;
							}
							$.imagen.setVisible(imagen_visible);


							if ('__args' in args) {
								args['__args'].onerror({
									tipo: 'falta_longitud'
								});
							} else {
								args.onerror({
									tipo: 'falta_longitud'
								});
							}
						}
					} else {
						var imagen_visible = false;

						if (imagen_visible == 'si') {
							imagen_visible = true;
						} else if (imagen_visible == 'no') {
							imagen_visible = false;
						}
						$.imagen.setVisible(imagen_visible);


						if ('__args' in args) {
							args['__args'].onerror({
								tipo: 'falta_latitud'
							});
						} else {
							args.onerror({
								tipo: 'falta_latitud'
							});
						}
					}
				} else {
					var imagen_visible = false;

					if (imagen_visible == 'si') {
						imagen_visible = true;
					} else if (imagen_visible == 'no') {
						imagen_visible = false;
					}
					$.imagen.setVisible(imagen_visible);


					if ('__args' in args) {
						args['__args'].onerror({
							tipo: 'falta_longitud_origen'
						});
					} else {
						args.onerror({
							tipo: 'falta_longitud_origen'
						});
					}
				}
			} else {
				var imagen_visible = false;

				if (imagen_visible == 'si') {
					imagen_visible = true;
				} else if (imagen_visible == 'no') {
					imagen_visible = false;
				}
				$.imagen.setVisible(imagen_visible);


				if ('__args' in args) {
					args['__args'].onerror({
						tipo: 'falta_latitud_origen'
					});
				} else {
					args.onerror({
						tipo: 'falta_latitud_origen'
					});
				}
			}
		} else if (params.tipo == 'multiple') {}
	}
	if (!_.isUndefined(params.direccion)) {
		/** 
		 * Mostramos vista direccion, solo si hay algo escrito en ella 
		 */
		if ((_.isObject(params.direccion) || (_.isString(params.direccion)) && !_.isEmpty(params.direccion)) || _.isNumber(params.direccion) || _.isBoolean(params.direccion)) {
			var vista2_visible = true;

			if (vista2_visible == 'si') {
				vista2_visible = true;
			} else if (vista2_visible == 'no') {
				vista2_visible = false;
			}
			$.vista2.setVisible(vista2_visible);

			$.Direccion.setText(params.direccion);

		} else {
			var vista2_visible = false;

			if (vista2_visible == 'si') {
				vista2_visible = true;
			} else if (vista2_visible == 'no') {
				vista2_visible = false;
			}
			$.vista2.setVisible(vista2_visible);

			$.Direccion.setText('-');

		}
	}
	if (!_.isUndefined(params.distancia)) {
		$.Distancia.setText(params.distancia);

	}
	if (!_.isUndefined(params.comuna)) {
		$.Comuna.setText(params.comuna);

	}
	if (!_.isUndefined(params.ciudad)) {
		$.Ciudad.setText(params.ciudad);

	}
	if (!_.isUndefined(params.bono)) {
		/** 
		 * Mostramos vista bono, si hay algo en atributo bono 
		 */
		if ((_.isObject(params.bono) || (_.isString(params.bono)) && !_.isEmpty(params.bono)) || _.isNumber(params.bono) || _.isBoolean(params.bono)) {
			var vista9_visible = true;

			if (vista9_visible == 'si') {
				vista9_visible = true;
			} else if (vista9_visible == 'no') {
				vista9_visible = false;
			}
			$.vista9.setVisible(vista9_visible);

			$.label.setText(params.bono + '%');

			var vista10_ancho = '-';

			if (vista10_ancho == '*') {
				vista10_ancho = Ti.UI.FILL;
			} else if (vista10_ancho == '-') {
				vista10_ancho = Ti.UI.SIZE;
			} else if (!isNaN(vista10_ancho)) {
				vista10_ancho = vista10_ancho + 'dp';
			}
			$.vista10.setWidth(vista10_ancho);

		} else {
			var vista9_visible = false;

			if (vista9_visible == 'si') {
				vista9_visible = true;
			} else if (vista9_visible == 'no') {
				vista9_visible = false;
			}
			$.vista9.setVisible(vista9_visible);

			var vista9_alto = 0;

			if (vista9_alto == '*') {
				vista9_alto = Ti.UI.FILL;
			} else if (vista9_alto == '-') {
				vista9_alto = Ti.UI.SIZE;
			} else if (!isNaN(vista9_alto)) {
				vista9_alto = vista9_alto + 'dp';
			}
			$.vista9.setHeight(vista9_alto);

		}
	}
	if (!_.isUndefined(params._bono)) {
		$.BonoAdicional.setText(params._bono);

	} else if (!_.isUndefined(params.monito)) {
		var vista8_visible = params.monito;

		if (vista8_visible == 'si') {
			vista8_visible = true;
		} else if (vista8_visible == 'no') {
			vista8_visible = false;
		}
		$.vista8.setVisible(vista8_visible);

		if (params.monito == true || params.monito == 'true') {
			$.imagen2.start();

		} else {
			$.imagen2.stop();

		}
		if (Ti.App.deployType != 'production') console.log('cargando monito wid', {});
	}
};

/** 
 * Funcion que centra la posicion del mapa en las coordenadas entregadas
 * 
 * @param latitud Latitud
 * @param longitud Longitud 
 */

$.centrar = function(params) {
	if (!_.isUndefined(params.latitud)) {
		/** 
		 * Centramos el mapa segun los parametros que sean recibidos 
		 */
		if (!_.isUndefined(params.longitud)) {
			var mapa_latitud = params.latitud;

			var mapa_tmp = $.mapa.getRegion();
			mapa_tmp.latitude = mapa_latitud;
			mapa_latitud = mapa_tmp;
			$.mapa.setRegion(mapa_latitud);

			var mapa_longitud = params.longitud;

			var mapa_tmp = $.mapa.getRegion();
			mapa_tmp.longitude = mapa_longitud;
			mapa_longitud = mapa_tmp;
			$.mapa.setRegion(mapa_longitud);

		} else {
			var mapa_latitud = params.latitud;

			var mapa_tmp = $.mapa.getRegion();
			mapa_tmp.latitude = mapa_latitud;
			mapa_latitud = mapa_tmp;
			$.mapa.setRegion(mapa_latitud);

		}
	}
	if (!_.isUndefined(params.longitud)) {
		if (!_.isUndefined(params.latitud)) {
			var mapa_latitud = params.latitud;

			var mapa_tmp = $.mapa.getRegion();
			mapa_tmp.latitude = mapa_latitud;
			mapa_latitud = mapa_tmp;
			$.mapa.setRegion(mapa_latitud);

			var mapa_longitud = params.longitud;

			var mapa_tmp = $.mapa.getRegion();
			mapa_tmp.longitude = mapa_longitud;
			mapa_longitud = mapa_tmp;
			$.mapa.setRegion(mapa_longitud);

		} else {
			var mapa_longitud = params.longitud;

			var mapa_tmp = $.mapa.getRegion();
			mapa_tmp.longitude = mapa_longitud;
			mapa_longitud = mapa_tmp;
			$.mapa.setRegion(mapa_longitud);

		}
	}
};

/** 
 * Funcion que actualiza estado del mapa
 * 
 * @param tipo Define el tipo de mapa: ubicacion, origen, multiples
 * @param seguimiento Si true, muestra animacion de tarea en seguimiento
 * @param externo Si true, muestra boton para abrir ruta en apps externas (apple/google maps, waze)
 * @param _externo_titulo Modifica el titulo del popup 'Abrir ruta en:'
 * @param _externo_apple Modifica el titulo del boton 'Apple Maps'
 * @param _externo_google Modifica el titulo del boton 'Google Maps'
 * @param _externo_waze Modifica el titulo del boton 'Waze'
 * @param _externo_cancelar Modifica el texto del boton 'Cancelar'
 * @param direccion Indica el texto de la direccion de destino
 * @param distancia Indica el texto de la distancia hacia el destino
 * @param comuna Indica el texto de la comuna
 * @param ciudad Indica el texto de la ciudad
 * @param bono Indica el valor a mostrar el campo bono (opcional)
 * @param _bono Indica el texto del label del campo bono (opcional)
 * @param monito Si true, muestra monito de seguimiento 
 */

$.update = function(params) {
	require(WPATH('vars'))[args.__id]['seguimiento'] = 'false';
	if (!_.isUndefined(params.seguimiento)) {
		/** 
		 * Revisamos si por parametros recibimos cosas a editar 
		 */
		require(WPATH('vars'))[args.__id]['seguimiento'] = params.seguimiento;
	}
	if (!_.isUndefined(params.tipo)) {
		require(WPATH('vars'))[args.__id]['tipo'] = params.tipo;
		if (params.tipo == 'ubicacion') {
			$.pinchoMapa.setLatitude('0.0');

			$.pinchoMapa.setLongitude('0.0');

			var _ifunc_res_obtenerGPS2 = function(e) {
				if (e.error) {
					var origen = {
						error: true,
						latitude: -1,
						longitude: -1,
						reason: (e.reason) ? e.reason : 'unknown',
						accuracy: 0,
						speed: 0,
						error_compass: false
					};
				} else {
					var origen = e;
				}
				if (origen.error == true || origen.error == 'true') {

					if ('__args' in args) {
						args['__args'].onerror({
							tipo: 'gps'
						});
					} else {
						args.onerror({
							tipo: 'gps'
						});
					}
				} else {
					var mapa_latitud = origen.latitude;

					var mapa_tmp = $.mapa.getRegion();
					mapa_tmp.latitude = mapa_latitud;
					mapa_latitud = mapa_tmp;
					$.mapa.setRegion(mapa_latitud);

					var mapa_longitud = origen.longitude;

					var mapa_tmp = $.mapa.getRegion();
					mapa_tmp.longitude = mapa_longitud;
					mapa_longitud = mapa_tmp;
					$.mapa.setRegion(mapa_longitud);

					if (!_.isUndefined(params.latitud)) {
						if (!_.isUndefined(params.longitud)) {
							$.pinchoMapa2.setLatitude(params.latitud);

							$.pinchoMapa2.setLongitude(params.longitud);

							var datos_externo = {
								origen_latitud: origen.latitude,
								origen_longitud: origen.longitude,
								destino_latitud: params.latitud,
								destino_longitud: params.longitud
							};
							require(WPATH('vars'))[args.__id]['datos_externo'] = datos_externo;
							var bt_externo = ('bt_externo' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['bt_externo'] : '';
							if (params.externo == true || params.externo == 'true') {
								var imagen_visible = true;

								if (imagen_visible == 'si') {
									imagen_visible = true;
								} else if (imagen_visible == 'no') {
									imagen_visible = false;
								}
								$.imagen.setVisible(imagen_visible);

							} else {
								var imagen_visible = false;

								if (imagen_visible == 'si') {
									imagen_visible = true;
								} else if (imagen_visible == 'no') {
									imagen_visible = false;
								}
								$.imagen.setVisible(imagen_visible);

							}
							if (!_.isUndefined(params.ruta)) {
								if (params.ruta == true || params.ruta == 'true') {
									var consultarURL3 = {};

									consultarURL3.success = function(e) {
										var elemento = e,
											valor = e;
										if (elemento.status == 'OK') {
											var ruta_previa = ('ruta_previa' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['ruta_previa'] : '';
											if (_.isObject(ruta_previa)) {
												$.mapa.removeRoute(ruta_previa);

											}

											var rutaMapa3_poly = require(WPATH('polyline')).decode(elemento.routes[0].overview_polyline.points);
											var varruta = require('ti.map').createRoute({
												width: 6,
												color: '#2d9edb',
												points: rutaMapa3_poly
											});
											require(WPATH('vars'))[args.__id]['ruta_previa'] = varruta;
											$.mapa.addRoute(varruta);

											var label_a = ('label_a' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['label_a'] : '';
											$.Distancia.setText(label_a + ' ' + elemento.routes[0].legs[0].distance.text);


											if ('__args' in args) {
												args['__args'].ondatos({
													ruta_distancia_texto: elemento.routes[0].legs[0].distance.text,
													ruta_distancia: elemento.routes[0].legs[0].distance.value,
													ruta_poly: elemento.routes[0].overview_polyline.points,
													ruta_origen: elemento.routes[0].legs[0].start_address,
													ruta_destino: elemento.routes[0].legs[0].end_address,
													ruta_metros: elemento.routes[0].legs[0].distance.value,
													ruta_tiempo_texto: elemento.routes[0].legs[0].duration.text,
													ruta_tiempo: elemento.routes[0].legs[0].duration.value,
													ruta_origen_calle: elemento.routes[0].legs[0].start_address.split(',')[0],
													ruta_origen_comuna: elemento.routes[0].legs[0].start_address.split(',')[1]
												});
											} else {
												args.ondatos({
													ruta_distancia_texto: elemento.routes[0].legs[0].distance.text,
													ruta_distancia: elemento.routes[0].legs[0].distance.value,
													ruta_poly: elemento.routes[0].overview_polyline.points,
													ruta_origen: elemento.routes[0].legs[0].start_address,
													ruta_destino: elemento.routes[0].legs[0].end_address,
													ruta_metros: elemento.routes[0].legs[0].distance.value,
													ruta_tiempo_texto: elemento.routes[0].legs[0].duration.text,
													ruta_tiempo: elemento.routes[0].legs[0].duration.value,
													ruta_origen_calle: elemento.routes[0].legs[0].start_address.split(',')[0],
													ruta_origen_comuna: elemento.routes[0].legs[0].start_address.split(',')[1]
												});
											}

											if ('__args' in args) {
												args['__args'].onlisto({
													tipo: 'ubicacion'
												});
											} else {
												args.onlisto({
													tipo: 'ubicacion'
												});
											}
										} else {

											if ('__args' in args) {
												args['__args'].onerror({
													tipo: 'ruta',
													last_data: elemento
												});
											} else {
												args.onerror({
													tipo: 'ruta',
													last_data: elemento
												});
											}
										}
										elemento = null, valor = null;
									};

									consultarURL3.error = function(e) {
										var elemento = e,
											valor = e;

										if ('__args' in args) {
											args['__args'].onerror({
												tipo: 'ruta',
												last_data: params,
												elemento: elemento
											});
										} else {
											args.onerror({
												tipo: 'ruta',
												last_data: params,
												elemento: elemento
											});
										}
										elemento = null, valor = null;
									};
									require('helper').ajaxUnico('consultarURL3', 'https://maps.googleapis.com/maps/api/directions/json?origin=' + origen.latitude + ',' + origen.longitude + '&destination=' + params.latitud + ',' + params.longitud + '&key=AIzaSyAJoATeoDjsJ3fNSe5q7eIKZ7VjT2AojqY', 'get', {}, 15000, consultarURL3);
								} else {

									if ('__args' in args) {
										args['__args'].onlisto({
											tipo: 'ubicacion'
										});
									} else {
										args.onlisto({
											tipo: 'ubicacion'
										});
									}
								}
							} else {

								if ('__args' in args) {
									args['__args'].onlisto({
										tipo: 'ubicacion'
									});
								} else {
									args.onlisto({
										tipo: 'ubicacion'
									});
								}
							}
						} else {
							var imagen_visible = false;

							if (imagen_visible == 'si') {
								imagen_visible = true;
							} else if (imagen_visible == 'no') {
								imagen_visible = false;
							}
							$.imagen.setVisible(imagen_visible);


							if ('__args' in args) {
								args['__args'].onerror({
									tipo: 'falta_longitud'
								});
							} else {
								args.onerror({
									tipo: 'falta_longitud'
								});
							}
						}
					} else {

						if ('__args' in args) {
							args['__args'].onlisto({
								tipo: 'ubicacion'
							});
						} else {
							args.onlisto({
								tipo: 'ubicacion'
							});
						}
					}
				}
				if (Ti.App.deployType != 'production') console.log('ubicacion GPS dice', {
					"origen": origen
				});
			};
			Ti.Geolocation.setAccuracy(Ti.Geolocation.ACCURACY_HIGH);
			if (Ti.Geolocation.hasLocationPermissions(Ti.Geolocation.AUTHORIZATION_WHEN_IN_USE)) {
				Ti.Geolocation.getCurrentPosition(function(ee) {
					if (Ti.Geolocation.getHasCompass() == false) {
						var porigen = ('coords' in ee) ? ee.coords : {};
						porigen.error = ('coords' in ee) ? false : true;
						porigen.reason = '';
						porigen.speed = 0;
						porigen.compass = -1;
						porigen.compass_accuracy = -1;
						porigen.error_compass = true;
						_ifunc_res_obtenerGPS2(porigen);
					} else {
						Ti.Geolocation.getCurrentHeading(function(yy) {
							var porigen = ('coords' in ee) ? ee.coords : {};
							porigen.error = ('coords' in ee) ? false : true;
							porigen.reason = ('error' in ee) ? ee.error : '';
							if (yy.error) {
								porigen.error_compass = true;
							} else {
								porigen.compass = yy.heading;
								porigen.compass_accuracy = yy.heading.accuracy;
							}
							_ifunc_res_obtenerGPS2(porigen);
						});
					}
				});
			} else {
				Ti.Geolocation.requestLocationPermissions(Ti.Geolocation.AUTHORIZATION_WHEN_IN_USE, function(u) {
					if (u.success) {
						Ti.Geolocation.getCurrentPosition(function(ee) {
							if (Ti.Geolocation.getHasCompass() == false) {
								var porigen = ('coords' in ee) ? ee.coords : {};
								porigen.error = ('coords' in ee) ? false : true;
								porigen.reason = '';
								porigen.speed = 0;
								porigen.compass = -1;
								porigen.compass_accuracy = -1;
								porigen.error_compass = true;
								_ifunc_res_obtenerGPS2(porigen);
							} else {
								Ti.Geolocation.getCurrentHeading(function(yy) {
									var porigen = ('coords' in ee) ? ee.coords : {};
									porigen.error = ('coords' in ee) ? false : true;
									porigen.reason = '';
									if (yy.error) {
										porigen.error_compass = true;
									} else {
										porigen.compass = yy.heading;
										porigen.compass_accuracy = yy.heading.accuracy;
									}
									_ifunc_res_obtenerGPS2(porigen);
								});
							}
						});
					} else {
						_ifunc_res_obtenerGPS2({
							error: true,
							latitude: -1,
							longitude: -1,
							reason: 'permission_denied',
							accuracy: 0,
							speed: 0,
							error_compass: false
						});
					}
				});
			}
		} else if (params.tipo == 'origen') {
			if (!_.isUndefined(params.origen_latitud)) {
				if (!_.isUndefined(params.origen_longitud)) {
					$.pinchoMapa.setLatitude(params.origen_latitud);

					$.pinchoMapa.setLongitude(params.origen_longitud);

					var mapa_latitud = params.origen_latitud;

					var mapa_tmp = $.mapa.getRegion();
					mapa_tmp.latitude = mapa_latitud;
					mapa_latitud = mapa_tmp;
					$.mapa.setRegion(mapa_latitud);

					var mapa_longitud = params.origen_longitud;

					var mapa_tmp = $.mapa.getRegion();
					mapa_tmp.longitude = mapa_longitud;
					mapa_longitud = mapa_tmp;
					$.mapa.setRegion(mapa_longitud);

					if (!_.isUndefined(params.destino_latitud)) {
						if (!_.isUndefined(params.destino_longitud)) {
							$.pinchoMapa2.setLatitude(params.destino_latitud);

							$.pinchoMapa2.setLongitude(params.destino_longitud);

							var datos_externo = {
								origen_latitud: params.origen_latitude,
								origen_longitud: params.origen_longitud,
								destino_latitud: params.destino_latitud,
								destino_longitud: params.destino_longitud
							};
							require(WPATH('vars'))[args.__id]['datos_externo'] = datos_externo;
							var bt_externo = ('bt_externo' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['bt_externo'] : '';
							if (params.externo == true || params.externo == 'true') {
								var imagen_visible = true;

								if (imagen_visible == 'si') {
									imagen_visible = true;
								} else if (imagen_visible == 'no') {
									imagen_visible = false;
								}
								$.imagen.setVisible(imagen_visible);

							} else {
								var imagen_visible = false;

								if (imagen_visible == 'si') {
									imagen_visible = true;
								} else if (imagen_visible == 'no') {
									imagen_visible = false;
								}
								$.imagen.setVisible(imagen_visible);

							}
							if (!_.isUndefined(params.ruta)) {
								if (params.ruta == true || params.ruta == 'true') {
									var consultarURL4 = {};

									consultarURL4.success = function(e) {
										var elemento = e,
											valor = e;
										if (elemento.status == 'OK') {
											var ruta_previa = ('ruta_previa' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['ruta_previa'] : '';
											if (_.isObject(ruta_previa)) {
												$.mapa.removeRoute(ruta_previa);

											}

											var rutaMapa4_poly = require(WPATH('polyline')).decode(elemento.routes[0].overview_polyline.points);
											var varruta = require('ti.map').createRoute({
												width: 6,
												color: '#2d9edb',
												points: rutaMapa4_poly
											});
											require(WPATH('vars'))[args.__id]['ruta_previa'] = varruta;
											$.mapa.addRoute(varruta);

											var label_a = ('label_a' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['label_a'] : '';
											$.Distancia.setText(label_a + ' ' + elemento.routes[0].legs[0].distance.text);


											if ('__args' in args) {
												args['__args'].ondatos({
													ruta_distancia_texto: elemento.routes[0].legs[0].distance.text,
													ruta_distancia: elemento.routes[0].legs[0].distance.value,
													ruta_poly: elemento.routes[0].overview_polyline.points,
													ruta_origen: elemento.routes[0].legs[0].start_address,
													ruta_destino: elemento.routes[0].legs[0].end_address,
													ruta_metros: elemento.routes[0].legs[0].distance.value,
													ruta_tiempo_texto: elemento.routes[0].legs[0].duration.text,
													ruta_tiempo: elemento.routes[0].legs[0].duration.value,
													ruta_origen_calle: elemento.routes[0].legs[0].start_address.split(',')[0],
													ruta_origen_comuna: elemento.routes[0].legs[0].start_address.split(',')[1]
												});
											} else {
												args.ondatos({
													ruta_distancia_texto: elemento.routes[0].legs[0].distance.text,
													ruta_distancia: elemento.routes[0].legs[0].distance.value,
													ruta_poly: elemento.routes[0].overview_polyline.points,
													ruta_origen: elemento.routes[0].legs[0].start_address,
													ruta_destino: elemento.routes[0].legs[0].end_address,
													ruta_metros: elemento.routes[0].legs[0].distance.value,
													ruta_tiempo_texto: elemento.routes[0].legs[0].duration.text,
													ruta_tiempo: elemento.routes[0].legs[0].duration.value,
													ruta_origen_calle: elemento.routes[0].legs[0].start_address.split(',')[0],
													ruta_origen_comuna: elemento.routes[0].legs[0].start_address.split(',')[1]
												});
											}

											if ('__args' in args) {
												args['__args'].onlisto({
													tipo: 'origen'
												});
											} else {
												args.onlisto({
													tipo: 'origen'
												});
											}
										} else {

											if ('__args' in args) {
												args['__args'].onerror({
													tipo: 'ruta'
												});
											} else {
												args.onerror({
													tipo: 'ruta'
												});
											}
										}
										elemento = null, valor = null;
									};

									consultarURL4.error = function(e) {
										var elemento = e,
											valor = e;

										if ('__args' in args) {
											args['__args'].onerror({
												tipo: 'ruta'
											});
										} else {
											args.onerror({
												tipo: 'ruta'
											});
										}
										elemento = null, valor = null;
									};
									require('helper').ajaxUnico('consultarURL4', 'https://maps.googleapis.com/maps/api/directions/json?origin=' + params.origen_latitud + ',' + params.origen_longitud + '&destination=' + params.destino_latitud + ',' + params.destino_longitud + '&key=AIzaSyAJoATeoDjsJ3fNSe5q7eIKZ7VjT2AojqY', 'get', {}, 15000, consultarURL4);
								} else {

									if ('__args' in args) {
										args['__args'].onlisto({
											tipo: 'origen'
										});
									} else {
										args.onlisto({
											tipo: 'origen'
										});
									}
								}
							} else {

								if ('__args' in args) {
									args['__args'].onlisto({
										tipo: 'origen'
									});
								} else {
									args.onlisto({
										tipo: 'origen'
									});
								}
							}
						} else {
							var imagen_visible = false;

							if (imagen_visible == 'si') {
								imagen_visible = true;
							} else if (imagen_visible == 'no') {
								imagen_visible = false;
							}
							$.imagen.setVisible(imagen_visible);


							if ('__args' in args) {
								args['__args'].onerror({
									tipo: 'falta_longitud'
								});
							} else {
								args.onerror({
									tipo: 'falta_longitud'
								});
							}
						}
					} else {
						var imagen_visible = false;

						if (imagen_visible == 'si') {
							imagen_visible = true;
						} else if (imagen_visible == 'no') {
							imagen_visible = false;
						}
						$.imagen.setVisible(imagen_visible);


						if ('__args' in args) {
							args['__args'].onerror({
								tipo: 'falta_latitud'
							});
						} else {
							args.onerror({
								tipo: 'falta_latitud'
							});
						}
					}
				} else {
					var imagen_visible = false;

					if (imagen_visible == 'si') {
						imagen_visible = true;
					} else if (imagen_visible == 'no') {
						imagen_visible = false;
					}
					$.imagen.setVisible(imagen_visible);


					if ('__args' in args) {
						args['__args'].onerror({
							tipo: 'falta_longitud_origen'
						});
					} else {
						args.onerror({
							tipo: 'falta_longitud_origen'
						});
					}
				}
			} else {
				var imagen_visible = false;

				if (imagen_visible == 'si') {
					imagen_visible = true;
				} else if (imagen_visible == 'no') {
					imagen_visible = false;
				}
				$.imagen.setVisible(imagen_visible);


				if ('__args' in args) {
					args['__args'].onerror({
						tipo: 'falta_latitud_origen'
					});
				} else {
					args.onerror({
						tipo: 'falta_latitud_origen'
					});
				}
			}
		} else if (params.tipo == 'multiple') {}
	}
	if (!_.isUndefined(params.externo)) {
		if (!_.isUndefined(params._externo_titulo)) {
			require(WPATH('vars'))[args.__id]['_externo_titulo'] = params._externo_titulo;
		}
		if (!_.isUndefined(params._externo_apple)) {
			require(WPATH('vars'))[args.__id]['_externo_apple'] = params._externo_apple;
		}
		if (!_.isUndefined(params._externo_google)) {
			require(WPATH('vars'))[args.__id]['_externo_google'] = params._externo_google;
		}
		if (!_.isUndefined(params._externo_waze)) {
			require(WPATH('vars'))[args.__id]['_externo_waze'] = params._externo_waze;
		}
		if (!_.isUndefined(params._externo_cancelar)) {
			require(WPATH('vars'))[args.__id]['_externo_cancelar'] = params._externo_cancelar;
		}
		if (params.externo == true || params.externo == 'true') {
			require(WPATH('vars'))[args.__id]['bt_externo'] = 'true';
		} else {
			require(WPATH('vars'))[args.__id]['bt_externo'] = 'false';
		}
	}
	if (!_.isUndefined(params.direccion)) {
		/** 
		 * mostramos vista direccion, solo si hay algo escrito en ella 
		 */
		if ((_.isObject(params.direccion) || (_.isString(params.direccion)) && !_.isEmpty(params.direccion)) || _.isNumber(params.direccion) || _.isBoolean(params.direccion)) {
			var vista2_visible = true;

			if (vista2_visible == 'si') {
				vista2_visible = true;
			} else if (vista2_visible == 'no') {
				vista2_visible = false;
			}
			$.vista2.setVisible(vista2_visible);

			$.Direccion.setText(params.direccion);

		} else {
			var vista2_visible = false;

			if (vista2_visible == 'si') {
				vista2_visible = true;
			} else if (vista2_visible == 'no') {
				vista2_visible = false;
			}
			$.vista2.setVisible(vista2_visible);

			$.Direccion.setText('-');

		}
	}
	if (!_.isUndefined(params._bono)) {
		$.BonoAdicional.setText(params._bono);

	}
	if (!_.isUndefined(params.bono)) {
		if ((_.isObject(params.bono) || (_.isString(params.bono)) && !_.isEmpty(params.bono)) || _.isNumber(params.bono) || _.isBoolean(params.bono)) {
			var vista9_visible = true;

			if (vista9_visible == 'si') {
				vista9_visible = true;
			} else if (vista9_visible == 'no') {
				vista9_visible = false;
			}
			$.vista9.setVisible(vista9_visible);

			$.label.setText(params.bono + '%');

			var vista10_ancho = '-';

			if (vista10_ancho == '*') {
				vista10_ancho = Ti.UI.FILL;
			} else if (vista10_ancho == '-') {
				vista10_ancho = Ti.UI.SIZE;
			} else if (!isNaN(vista10_ancho)) {
				vista10_ancho = vista10_ancho + 'dp';
			}
			$.vista10.setWidth(vista10_ancho);

		} else {
			var vista9_visible = false;

			if (vista9_visible == 'si') {
				vista9_visible = true;
			} else if (vista9_visible == 'no') {
				vista9_visible = false;
			}
			$.vista9.setVisible(vista9_visible);

		}
	}
	if (!_.isUndefined(params.distancia)) {
		$.Distancia.setText(params.distancia);

	}
	if (!_.isUndefined(params.comuna)) {
		$.Comuna.setText(params.comuna);

	}
	if (!_.isUndefined(params.ciudad)) {
		$.Ciudad.setText(params.ciudad);

	} else if (!_.isUndefined(params.monito)) {
		var vista8_visible = params.monito;

		if (vista8_visible == 'si') {
			vista8_visible = true;
		} else if (vista8_visible == 'no') {
			vista8_visible = false;
		}
		$.vista8.setVisible(vista8_visible);

		if (params.monito == true || params.monito == 'true') {
			$.imagen2.start();

		} else {
			$.imagen2.stop();

		}
	}
};