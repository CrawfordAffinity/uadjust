Widget Mapa
Control que muestra el mapa usado en las pantallas de detalle, con soporte de rutas multiples, o desde un punto a otro.
Ademas muestra datos como: direccion, distancia, ciudad y comuna. Si hay bono, tambien se muestra.

@param tipo Define el tipo de mapa: ubicacion, origen, multiples
@param seguimiento Si true, muestra animacion de tarea en seguimiento
@param externo Si true, muestra boton para abrir ruta en apps externas (apple/google maps, waze)
@param _externo_titulo Modifica el titulo del popup 'Abrir ruta en:'
@param _externo_apple Modifica el titulo del boton 'Apple Maps'
@param _externo_google Modifica el titulo del boton 'Google Maps'
@param _externo_waze Modifica el titulo del boton 'Waze'
@param _externo_cancelar Modifica el texto del boton 'Cancelar'
@param direccion Indica el texto de la direccion de destino
@param distancia Indica el texto de la distancia hacia el destino
@param comuna Indica el texto de la comuna
@param ciudad Indica el texto de la ciudad
@param bono Indica el valor a mostrar el campo bono (opcional)
@param _bono Indica el texto del label del campo bono (opcional)
@param monito Si true, muestra monito de seguimiento
