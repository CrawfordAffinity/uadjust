/** 
 * Widget boton
 * Control que muestra un boton de color amarillo.
 * 
 * @param texto Texto a mostrar en el boton 
 */
var _bind4section = {};

var args = arguments[0] || {};

function Click_vista(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var estado = ('estado' in require(WPATH('vars'))[args.__id]) ? require(WPATH('vars'))[args.__id]['estado'] : '';

	if ('__args' in args) {
		args['__args'].onpresiono({
			estado: estado
		});
	} else {
		args.onpresiono({
			estado: estado
		});
	}

}
/** 
 * Funcion que inicializa widget 
 */

$.init = function(params) {
	for (var tobe in params) args[tobe] = params[tobe];
	if ('__id' in params) require(WPATH('vars'))[params.__id] = {};
	if (!_.isUndefined(params.texto)) {
		$.PRESIONEPARA.setText(params.texto);

	}
	if (!_.isUndefined(params.verprogreso)) {
		if (params.verprogreso == true || params.verprogreso == 'true') {} else if (params.verprogreso == false || params.verprogreso == 'false') {}
	}
	if (!_.isUndefined(params.vertexto)) {
		var PRESIONEPARA_visible = params.vertexto;

		if (PRESIONEPARA_visible == 'si') {
			PRESIONEPARA_visible = true;
		} else if (PRESIONEPARA_visible == 'no') {
			PRESIONEPARA_visible = false;
		}
		$.PRESIONEPARA.setVisible(PRESIONEPARA_visible);

	}
	if (!_.isUndefined(params.estilo)) {
		var vista_estilo = params.estilo;

		var setEstilo = function(clase) {
			if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
				try {
					$.vista.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
				} catch (sete_err) {}
			}
		};
		setEstilo(vista_estilo);

	}
	require(WPATH('vars'))[args.__id]['estado'] = '0';
};

/** 
 * Funcion que actualiza datos de boton
 * 
 * @param texto Indica el nuevo texto para el boton
 * @param verprogreso Si true, muestra animacion de progreso en vez de texto
 * @param vertexto Si true, muestra texto sobre boton, de lo contrario lo oculta 
 */

$.datos = function(params) {
	if (!_.isUndefined(params.texto)) {
		/** 
		 * Revisamos los parametros recibidos y modificamos texto, vista 
		 */
		$.PRESIONEPARA.setText(params.texto);

	}
	if (!_.isUndefined(params.verprogreso)) {
		if (params.verprogreso == true || params.verprogreso == 'true') {} else if (params.verprogreso == false || params.verprogreso == 'false') {}
	}
	if (!_.isUndefined(params.vertexto)) {
		var PRESIONEPARA_visible = params.vertexto;

		if (PRESIONEPARA_visible == 'si') {
			PRESIONEPARA_visible = true;
		} else if (PRESIONEPARA_visible == 'no') {
			PRESIONEPARA_visible = false;
		}
		$.PRESIONEPARA.setVisible(PRESIONEPARA_visible);

	}
	if (!_.isUndefined(params.estilo)) {
		var vista_estilo = params.estilo;

		var setEstilo = function(clase) {
			if ('styles' in require(WPATH('a4w')) && clase in require(WPATH('a4w')).styles['classes']) {
				try {
					$.vista.applyProperties(require(WPATH('a4w')).styles['classes'][clase]);
				} catch (sete_err) {}
			}
		};
		setEstilo(vista_estilo);

		if (Ti.App.deployType != 'production') console.log('color de estilo en widget boton', {
			"datos": params.estilo
		});
		if (params.estilo == 'fondoverde') {
			if (Ti.App.deployType != 'production') console.log('cambiado a fondover', {});
			$.vista.setBorderColor('#8CE5BD');

		} else if (params.estilo == 'fondorojo') {
			if (Ti.App.deployType != 'production') console.log('cambiado a fondoroj', {});
			$.vista.setBorderColor('#EE7F7E');

		} else if (params.estilo == 'fondoamarillo') {
			if (Ti.App.deployType != 'production') console.log('cambiado a fondoamari', {});
			$.vista.setBorderColor('#F8DA54');

		}
	}
	require(WPATH('vars'))[args.__id]['estado'] = params.estado;
};

/** 
 * Funcion que inicia animacion de progreso y oculta el texto del boton 
 */

$.iniciar_progreso = function(params) {
	/** 
	 * oculta el bot&#243;n continuar y muestra la animaci&#243;n de progreso. 
	 */
	var PRESIONEPARA_visible = false;

	if (PRESIONEPARA_visible == 'si') {
		PRESIONEPARA_visible = true;
	} else if (PRESIONEPARA_visible == 'no') {
		PRESIONEPARA_visible = false;
	}
	$.PRESIONEPARA.setVisible(PRESIONEPARA_visible);

	$.progreso.show();
};

/** 
 * Funcion que detiene animacion de progreso y muestra el texto del boton 
 */

$.detener_progreso = function(params) {
	/** 
	 * Muestra el bot&#243;n continuar y oculta la animaci&#243;n de progreso. 
	 */
	var PRESIONEPARA_visible = true;

	if (PRESIONEPARA_visible == 'si') {
		PRESIONEPARA_visible = true;
	} else if (PRESIONEPARA_visible == 'no') {
		PRESIONEPARA_visible = false;
	}
	$.PRESIONEPARA.setVisible(PRESIONEPARA_visible);

	$.progreso.hide();
};