var _bind4section = {
	"ref1": "insp_niveles"
};
var _list_templates = {
	"elemento": {
		"vista2": {},
		"Label3": {
			"text": "{nombre}"
		},
		"Label2": {
			"text": "{id_server}"
		},
		"Label": {
			"text": "{id_segured}"
		},
		"vista": {}
	},
	"dano": {
		"Label2": {
			"text": "{id}"
		},
		"vista29": {},
		"Label": {
			"text": "{nombre}"
		}
	},
	"pborrar": {
		"vista5": {},
		"imagen": {},
		"vista3": {},
		"Label4": {
			"text": "{id}"
		},
		"Label3": {
			"text": "{nombre}"
		},
		"vista4": {},
		"vista22": {},
		"vista23": {},
		"Label3": {
			"text": "{nombre}"
		},
		"Label4": {
			"text": "{id}"
		},
		"vista24": {},
		"imagen2": {}
	},
	"contenido": {
		"vista2": {},
		"Label2": {
			"text": "{id}"
		},
		"Label": {
			"text": "{nombre}"
		}
	},
	"recinto": {
		"vista2": {},
		"Label": {
			"text": "{nombre}"
		},
		"Label2": {
			"text": "{id}"
		}
	},
	"nivel": {
		"Label2": {
			"text": "{id}"
		},
		"Label": {
			"text": "{nombre}"
		},
		"vista21": {}
	}
};
var $datos = $.datos.toJSON();

var _activity;
if (OS_ANDROID) {
	_activity = $.inicio.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'inicio';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.inicio.addEventListener('open', function(e) {});
}
$.inicio.orientationModes = [Titanium.UI.PORTRAIT];

/** 
 * Consulta los datos ingresados en la pantalla de niveles y permite desplegarlo en un listado de esta pantalla 
 */
var consultarModelo_like = function(search) {
	if (typeof search !== 'string' || this === null) {
		return false;
	}
	search = search.replace(new RegExp("([\\.\\\\\\+\\*\\?\\[\\^\\]\\$\\(\\)\\{\\}\\=\\!\\<\\>\\|\\:\\-])", "g"), "\\$1");
	search = search.replace(/%/g, '.*').replace(/_/g, '.');
	return RegExp('^' + search + '$', 'gi').test(this);
};
var consultarModelo_filter = function(coll) {
	var filtered = _.toArray(coll.filter(function(m) {
		return true;
	}));
	return filtered;
};
var consultarModelo_transform = function(model) {
	var modelo = model.toJSON();
	return modelo;
};
_.defer(function() {
	Alloy.Collections.insp_niveles.fetch();
});
Alloy.Collections.insp_niveles.on('add change delete', function(ee) {
	consultarModelo_update(ee);
});
var consultarModelo_update = function(evento) {
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	var consultarModelo2_i = Alloy.createCollection('insp_niveles');
	var consultarModelo2_i_where = 'id_inspeccion=\'' + seltarea.id_server + '\'';
	consultarModelo2_i.fetch({
		query: 'SELECT * FROM insp_niveles WHERE id_inspeccion=\'' + seltarea.id_server + '\''
	});
	var insp_n = require('helper').query2array(consultarModelo2_i);
	var alto = 55 * insp_n.length;
	var listado_alto = alto;

	if (listado_alto == '*') {
		listado_alto = Ti.UI.FILL;
	} else if (!isNaN(listado_alto)) {
		listado_alto = listado_alto + 'dp';
	}
	$.listado.setHeight(listado_alto);

};
Alloy.Collections.insp_niveles.fetch();


$.widgetBarra.init({
	titulo: L('x2004545330_traducir', 'CARACTERISTICAS'),
	onsalirinsp: Salirinsp_widgetBarra,
	__id: 'ALL138747237',
	continuar: L('', ''),
	salir_insp: L('', ''),
	fondo: 'fondoverde',
	top: 0,
	onpresiono: Presiono_widgetBarra
});

function Salirinsp_widgetBarra(e) {
	/** 
	 * Si el usuario desea cancelar la inspeccion se abrira una pantalla de alerta 
	 */
	var evento = e;
	var preguntarOpciones_opts = [L('x3481285698_traducir', 'Asegurado no puede seguir'), L('x3077268071_traducir', ' Se me acabo la bateria'), L('x479989047_traducir', ' Tuve un accidente'), L('x2376009830_traducir', ' Cancelar')];
	var preguntarOpciones = Ti.UI.createOptionDialog({
		title: L('x1670176792_traducir', 'RAZON PARA CANCELAR INSPECCION ACTUAL'),
		options: preguntarOpciones_opts
	});
	preguntarOpciones.addEventListener('click', function(e) {
		var resp = preguntarOpciones_opts[e.index];
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var razon = "";
			if (resp == L('x3481285698_traducir', 'Asegurado no puede seguir')) {
				razon = "Asegurado no puede seguir";
			}
			if (resp == L('x1862794075_traducir', 'Se me acabo la bateria')) {
				razon = "Se me acabo la bateria";
			}
			if (resp == L('x4012805111_traducir', 'Tuve un accidente')) {
				razon = "Tuve un accidente";
			}
			if (Ti.App.deployType != 'production') console.log('mi razon es', {
				"datos": razon
			});
			if ((_.isObject(razon) || (_.isString(razon)) && !_.isEmpty(razon)) || _.isNumber(razon) || _.isBoolean(razon)) {
				if (Ti.App.deployType != 'production') console.log('llamando servicio cancelarTarea', {});
				require('vars')['insp_cancelada'] = L('x2941610362_traducir', 'siniestro');
				Alloy.Collections[$.datos.config.adapter.collection_name].add($.datos);
				$.datos.save();
				Alloy.Collections[$.datos.config.adapter.collection_name].fetch();
				var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
				var vista27_visible = true;

				if (vista27_visible == 'si') {
					vista27_visible = true;
				} else if (vista27_visible == 'no') {
					vista27_visible = false;
				}
				$.vista27.setVisible(vista27_visible);

				var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
				var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
				if (Ti.App.deployType != 'production') console.log('detalle de seltarea', {
					"data": seltarea
				});
				var datos = {
					id_inspector: inspector.id_server,
					codigo_identificador: inspector.codigo_identificador,
					id_server: seltarea.id_server,
					num_caso: seltarea.num_caso,
					razon: razon
				};
				require('vars')[_var_scopekey]['datos'] = datos;
				var consultarURL = {};
				consultarURL.success = function(e) {
					var elemento = e,
						valor = e;
					$.widgetModal.limpiar({});
					var vista27_visible = false;

					if (vista27_visible == 'si') {
						vista27_visible = true;
					} else if (vista27_visible == 'no') {
						vista27_visible = false;
					}
					$.vista27.setVisible(vista27_visible);

					Alloy.createController("firma_index", {}).getView().open();
					var ID_860845987_func = function() {
						Alloy.Events.trigger('_cerrar_insp', {
							pantalla: 'caracteristicas'
						});
					};
					var ID_860845987 = setTimeout(ID_860845987_func, 1000 * 0.5);
					elemento = null, valor = null;
				};
				consultarURL.error = function(e) {
					var elemento = e,
						valor = e;
					if (Ti.App.deployType != 'production') console.log('hubo error llamando cancelarTarea, la agregamos a cola de salida', {
						"elemento": elemento
					});
					$.widgetModal.limpiar({});
					if (Ti.App.deployType != 'production') console.log('agregando servicio cancelarTarea a cola', {});
					var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
					var datos = ('datos' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['datos'] : '';
					var insertarModelo_m = Alloy.Collections.cola;
					var insertarModelo_fila = Alloy.createModel('cola', {
						data: JSON.stringify(datos),
						id_tarea: seltarea.id_server,
						tipo: 'cancelar'
					});
					insertarModelo_m.add(insertarModelo_fila);
					insertarModelo_fila.save();
					_.defer(function() {});
					var vista27_visible = false;

					if (vista27_visible == 'si') {
						vista27_visible = true;
					} else if (vista27_visible == 'no') {
						vista27_visible = false;
					}
					$.vista27.setVisible(vista27_visible);

					Alloy.createController("firma_index", {}).getView().open();
					var ID_1441498527_func = function() {
						Alloy.Events.trigger('_cerrar_insp', {
							pantalla: 'caracteristicas'
						});
					};
					var ID_1441498527 = setTimeout(ID_1441498527_func, 1000 * 0.5);
					elemento = null, valor = null;
				};
				require('helper').ajaxUnico('consultarURL', '' + String.format(L('x3244141284', '%1$scancelarTarea'), url_server.toString()) + '', 'POST', {
					id_inspector: seltarea.id_inspector,
					codigo_identificador: inspector.codigo_identificador,
					id_tarea: seltarea.id_server,
					num_caso: seltarea.num_caso,
					mensaje: razon,
					opcion: 0,
					tipo: 1
				}, 15000, consultarURL);
			}
		}
		resp = null;
		e.source.removeEventListener("click", arguments.callee);
	});
	preguntarOpciones.show();

}

function Presiono_widgetBarra(e) {
	/** 
	 * Si el usuario seguira con la inspeccion, se revisa que todos los datos obligatorios hayan sido ingresados 
	 */
	var evento = e;
	/** 
	 * Se obtiene el a&#241;o actual para comparar que el a&#241;o ingresado no sea superior 
	 */
	var d = new Date();
	var anoactual = d.getFullYear();
	/** 
	 * Flags para verificar que todos los datos ingresados esten correctos 
	 */
	require('vars')[_var_scopekey]['otro_valor'] = L('x734881840_traducir', 'false');
	require('vars')[_var_scopekey]['sin_niveles'] = L('x734881840_traducir', 'false');
	require('vars')[_var_scopekey]['todobien'] = L('x4261170317', 'true');
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
		/** 
		 * Comprobamos si no hay niveles definidos, para mostrar posteriormente dialogo luego de finalizar resto de validaciones. 
		 */
		var consultarModelo3_i = Alloy.createCollection('insp_niveles');
		var consultarModelo3_i_where = 'id_inspeccion=\'' + seltarea.id_server + '\'';
		consultarModelo3_i.fetch({
			query: 'SELECT * FROM insp_niveles WHERE id_inspeccion=\'' + seltarea.id_server + '\''
		});
		var niveles = require('helper').query2array(consultarModelo3_i);
		if (niveles && niveles.length == 0) {
			require('vars')[_var_scopekey]['sin_niveles'] = L('x4261170317', 'true');
		}
	} else {
		var consultarModelo4_i = Alloy.createCollection('insp_niveles');
		var consultarModelo4_i_where = '';
		consultarModelo4_i.fetch();
		var niveles = require('helper').query2array(consultarModelo4_i);
		if (niveles && niveles.length == 0) {
			require('vars')[_var_scopekey]['sin_niveles'] = L('x4261170317', 'true');
		}
	}
	if (Ti.App.deployType != 'production') console.log('info datos', {
		"daot": $.datos.toJSON()
	});
	if (_.isUndefined($datos.destinos)) {
		require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
		var preguntarAlerta_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x3814056646_traducir', 'Seleccione destino de la vivienda'),
			buttonNames: preguntarAlerta_opts
		});
		preguntarAlerta.addEventListener('click', function(e) {
			var nulo = preguntarAlerta_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta.show();
		if (Ti.App.deployType != 'production') console.log('hola1', {});
	} else if ((_.isObject($datos.destinos) || _.isString($datos.destinos)) && _.isEmpty($datos.destinos)) {
		require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
		var preguntarAlerta2_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta2 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x3814056646_traducir', 'Seleccione destino de la vivienda'),
			buttonNames: preguntarAlerta2_opts
		});
		preguntarAlerta2.addEventListener('click', function(e) {
			var nulo = preguntarAlerta2_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta2.show();
		if (Ti.App.deployType != 'production') console.log('hola2', {});
	} else if (_.isUndefined($datos.construccion_ano)) {
		require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
		var preguntarAlerta3_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta3 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x2674242197_traducir', 'Ingrese año de construcción de la vivienda'),
			buttonNames: preguntarAlerta3_opts
		});
		preguntarAlerta3.addEventListener('click', function(e) {
			var nulo = preguntarAlerta3_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta3.show();
		if (Ti.App.deployType != 'production') console.log('hola3', {});
	} else if ((_.isObject($datos.construccion_ano) || _.isString($datos.construccion_ano)) && _.isEmpty($datos.construccion_ano)) {
		require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
		var preguntarAlerta4_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta4 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x2674242197_traducir', 'Ingrese año de construcción de la vivienda'),
			buttonNames: preguntarAlerta4_opts
		});
		preguntarAlerta4.addEventListener('click', function(e) {
			var nulo = preguntarAlerta4_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta4.show();
		if (Ti.App.deployType != 'production') console.log('hola4', {});
	} else if ($datos.construccion_ano > anoactual == true || $datos.construccion_ano > anoactual == 'true') {
		require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
		var preguntarAlerta5_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta5 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x2118147629_traducir', 'El año tiene que ser menor al año actual'),
			buttonNames: preguntarAlerta5_opts
		});
		preguntarAlerta5.addEventListener('click', function(e) {
			var nulo = preguntarAlerta5_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta5.show();
		if (Ti.App.deployType != 'production') console.log('hola5', {});
	} else if ($datos.construccion_ano < (anoactual - 100) == true || $datos.construccion_ano < (anoactual - 100) == 'true') {
		require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
		var preguntarAlerta6_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta6 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x624168147_traducir', 'Tiene que tener máximo 100 años de antigüedad'),
			buttonNames: preguntarAlerta6_opts
		});
		preguntarAlerta6.addEventListener('click', function(e) {
			var nulo = preguntarAlerta6_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta6.show();
		if (Ti.App.deployType != 'production') console.log('hola6', {});
	} else if ($datos.otros_seguros_enlugar == 1 || $datos.otros_seguros_enlugar == '1') {
		if (Ti.App.deployType != 'production') console.log('hola7', {});
		if (_.isUndefined($datos.id_compania)) {
			require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
			var preguntarAlerta7_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta7 = Ti.UI.createAlertDialog({
				title: L('x3237162386_traducir', 'Atencion'),
				message: L('x1200064248_traducir', 'Seleccione compañía de seguros'),
				buttonNames: preguntarAlerta7_opts
			});
			preguntarAlerta7.addEventListener('click', function(e) {
				var nulo = preguntarAlerta7_opts[e.index];
				nulo = null;
				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta7.show();
			if (Ti.App.deployType != 'production') console.log('hola8', {});
		} else if ((_.isObject($datos.id_compania) || _.isString($datos.id_compania)) && _.isEmpty($datos.id_compania)) {
			require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
			var preguntarAlerta8_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta8 = Ti.UI.createAlertDialog({
				title: L('x3237162386_traducir', 'Atencion'),
				message: L('x1200064248_traducir', 'Seleccione compañía de seguros'),
				buttonNames: preguntarAlerta8_opts
			});
			preguntarAlerta8.addEventListener('click', function(e) {
				var nulo = preguntarAlerta8_opts[e.index];
				nulo = null;
				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta8.show();
		} else {
			require('vars')[_var_scopekey]['otro_valor'] = L('x4261170317', 'true');
			if (Ti.App.deployType != 'production') console.log('hola9', {});
		}
	} else {}
	if ($datos.asociado_hipotecario == 1 || $datos.asociado_hipotecario == '1') {
		if (Ti.App.deployType != 'production') console.log('hola10', {});
		if (_.isUndefined($datos.id_entidad_financiera)) {
			if (Ti.App.deployType != 'production') console.log('hola11', {});
			require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
			var preguntarAlerta9_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta9 = Ti.UI.createAlertDialog({
				title: L('x3237162386_traducir', 'Atencion'),
				message: L('x4118601873_traducir', 'Seleccione entidad financiera'),
				buttonNames: preguntarAlerta9_opts
			});
			preguntarAlerta9.addEventListener('click', function(e) {
				var nulo = preguntarAlerta9_opts[e.index];
				nulo = null;
				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta9.show();
		} else if ((_.isObject($datos.id_entidad_financiera) || _.isString($datos.id_entidad_financiera)) && _.isEmpty($datos.id_entidad_financiera)) {
			if (Ti.App.deployType != 'production') console.log('hola12', {});
			require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
			var preguntarAlerta10_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta10 = Ti.UI.createAlertDialog({
				title: L('x3237162386_traducir', 'Atencion'),
				message: L('x4118601873_traducir', 'Seleccione entidad financiera'),
				buttonNames: preguntarAlerta10_opts
			});
			preguntarAlerta10.addEventListener('click', function(e) {
				var nulo = preguntarAlerta10_opts[e.index];
				nulo = null;
				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta10.show();
		} else {
			require('vars')[_var_scopekey]['otro_valor'] = L('x4261170317', 'true');
		}
	} else {}
	if ($datos.inhabitable == 1 || $datos.inhabitable == '1') {
		if (_.isUndefined($datos.estimacion_meses)) {
			if (Ti.App.deployType != 'production') console.log('hola14', {});
			require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
			var preguntarAlerta11_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta11 = Ti.UI.createAlertDialog({
				title: L('x3237162386_traducir', 'Atencion'),
				message: L('x2127433848_traducir', 'Ingrese estimación de meses'),
				buttonNames: preguntarAlerta11_opts
			});
			preguntarAlerta11.addEventListener('click', function(e) {
				var nulo = preguntarAlerta11_opts[e.index];
				nulo = null;
				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta11.show();
		} else if ((_.isObject($datos.estimacion_meses) || _.isString($datos.estimacion_meses)) && _.isEmpty($datos.estimacion_meses)) {
			if (Ti.App.deployType != 'production') console.log('hola15', {});
			require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
			var preguntarAlerta12_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta12 = Ti.UI.createAlertDialog({
				title: L('x3237162386_traducir', 'Atencion'),
				message: L('x2127433848_traducir', 'Ingrese estimación de meses'),
				buttonNames: preguntarAlerta12_opts
			});
			preguntarAlerta12.addEventListener('click', function(e) {
				var nulo = preguntarAlerta12_opts[e.index];
				nulo = null;
				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta12.show();
		} else {
			if (Ti.App.deployType != 'production') console.log('hola16', {});
			require('vars')[_var_scopekey]['otro_valor'] = L('x4261170317', 'true');
		}
	}
	var todobien = ('todobien' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['todobien'] : '';
	if (todobien == true || todobien == 'true') {
		var otro_valor = ('otro_valor' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['otro_valor'] : '';
		if (otro_valor == true || otro_valor == 'true') {
			var sin_niveles = ('sin_niveles' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['sin_niveles'] : '';
			if (sin_niveles == true || sin_niveles == 'true') {
				var preguntarAlerta13_opts = [L('x1518866076_traducir', 'Aceptar')];
				var preguntarAlerta13 = Ti.UI.createAlertDialog({
					title: L('x3237162386_traducir', 'Atencion'),
					message: L('x3117604322_traducir', 'Ingrese al menos un nivel de la vivienda'),
					buttonNames: preguntarAlerta13_opts
				});
				preguntarAlerta13.addEventListener('click', function(e) {
					var nulo = preguntarAlerta13_opts[e.index];
					nulo = null;
					e.source.removeEventListener("click", arguments.callee);
				});
				preguntarAlerta13.show();
			} else {
				/** 
				 * Abrimos pantalla esta de acuerdo 
				 */
				$.widgetEstadeacuerdo.abrir({});
			}
		} else {
			var sin_niveles = ('sin_niveles' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['sin_niveles'] : '';
			if (sin_niveles == true || sin_niveles == 'true') {
				var preguntarAlerta14_opts = [L('x1518866076_traducir', 'Aceptar')];
				var preguntarAlerta14 = Ti.UI.createAlertDialog({
					title: L('x3237162386_traducir', 'Atencion'),
					message: L('x3117604322_traducir', 'Ingrese al menos un nivel de la vivienda'),
					buttonNames: preguntarAlerta14_opts
				});
				preguntarAlerta14.addEventListener('click', function(e) {
					var nulo = preguntarAlerta14_opts[e.index];
					nulo = null;
					e.source.removeEventListener("click", arguments.callee);
				});
				preguntarAlerta14.show();
			} else {
				/** 
				 * Abrimos pantalla esta de acuerdo 
				 */
				$.widgetEstadeacuerdo.abrir({});
			}
		}
	}

}

$.widgetModalmultiple.init({
	titulo: L('x1313568614_traducir', 'Destino'),
	__id: 'ALL1151684903',
	oncerrar: Cerrar_widgetModalmultiple,
	left: 0,
	hint: L('x2017856269_traducir', 'Seleccione destino'),
	color: 'verde',
	subtitulo: L('x1857779775_traducir', 'Indique los destinos'),
	right: 0,
	onafterinit: Afterinit_widgetModalmultiple
});

function Afterinit_widgetModalmultiple(e) {

	var evento = e;
	var ID_1600749054_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {} else {
			/** 
			 * Insertamos dummies 
			 */
			/** 
			 * Insertamos dummies 
			 */
			var eliminarModelo_i = Alloy.Collections.destino;
			var sql = "DELETE FROM " + eliminarModelo_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo2_m = Alloy.Collections.destino;
				var insertarModelo2_fila = Alloy.createModel('destino', {
					nombre: String.format(L('x1956100003_traducir', 'Casa%1$s'), item.toString()),
					id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
					id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item.toString()),
					pais: 1
				});
				insertarModelo2_m.add(insertarModelo2_fila);
				insertarModelo2_fila.save();
				_.defer(function() {});
			});
		}
		/** 
		 * Obtenemos datos para selectores 
		 */
		var transformarModelo_i = Alloy.createCollection('destino');
		transformarModelo_i.fetch();
		var transformarModelo_src = require('helper').query2array(transformarModelo_i);
		var datos = [];
		_.each(transformarModelo_src, function(fila, pos) {
			var new_row = {};
			_.each(fila, function(x, llave) {
				var newkey = '';
				if (llave == 'nombre') newkey = 'label';
				if (llave == 'id_segured') newkey = 'valor';
				if (newkey != '') new_row[newkey] = fila[llave];
			});
			new_row['estado'] = 0;
			datos.push(new_row);
		});
		/** 
		 * Enviamos los datos al widget 
		 */
		$.widgetModalmultiple.update({
			data: datos
		});
	};
	var ID_1600749054 = setTimeout(ID_1600749054_func, 1000 * 0.2);

}

function Cerrar_widgetModalmultiple(e) {

	var evento = e;
	/** 
	 * Mostramos en la pantalla el valor seleccionado y actualizamos el modelo con lo seleccioando 
	 */
	$.widgetModalmultiple.update({});
	$.datos.set({
		destinos: evento.valores
	});
	if ('datos' in $) $datos = $.datos.toJSON();

}

function Change_Ao(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	$.datos.set({
		construccion_ano: elemento
	});
	if ('datos' in $) $datos = $.datos.toJSON();
	elemento = null, source = null;

}

function Return_Ao(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	$.Ao.blur();
	elemento = null, source = null;

}

function Change_ID_1568773770(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	if (elemento == true || elemento == 'true') {
		var si = L('x3746555228_traducir', 'SI');
		$.NO.setText(si);

		$.datos.set({
			construccion_anexa: 1
		});
		if ('datos' in $) $datos = $.datos.toJSON();
	} else {
		var no = L('x3376426101_traducir', 'NO');
		$.NO.setText(no);

		$.datos.set({
			construccion_anexa: 0
		});
		if ('datos' in $) $datos = $.datos.toJSON();
	}
	elemento = null;

}

function Change_ID_129828234(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	if (elemento == true || elemento == 'true') {
		var si = L('x3746555228_traducir', 'SI');
		$.NO3.setText(si);

		$.datos.set({
			otros_seguros_enlugar: 1
		});
		if ('datos' in $) $datos = $.datos.toJSON();
		$.widgetModal.activar({});
	} else {
		var no = L('x3376426101_traducir', 'NO');
		$.NO3.setText(no);

		$.datos.set({
			otros_seguros_enlugar: 0,
			id_compania: ''
		});
		if ('datos' in $) $datos = $.datos.toJSON();
		$.widgetModal.labels({
			valor: 'Seleccione compañía'
		});
		$.widgetModal.desactivar({});
	}
	elemento = null;

}

$.widgetModal.init({
	titulo: L('x86212863_traducir', 'COMPAÑIA'),
	cargando: L('x1831148736', 'cargando...'),
	__id: 'ALL1379917834',
	left: 0,
	onrespuesta: Respuesta_widgetModal,
	campo: L('x3363644313_traducir', 'Compañía'),
	onabrir: Abrir_widgetModal,
	color: 'verde',
	right: 0,
	seleccione: L('x1908690217_traducir', 'Seleccione compañía'),
	activo: false,
	onafterinit: Afterinit_widgetModal
});

function Afterinit_widgetModal(e) {

	var evento = e;
	var ID_1948046515_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {} else {
			var eliminarModelo2_i = Alloy.Collections.compania;
			var sql = "DELETE FROM " + eliminarModelo2_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo2_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo2_i.trigger('remove');
			var item_index = 0;
			_.each('A,A,B,C,D,E,F,F,G'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo3_m = Alloy.Collections.compania;
				var insertarModelo3_fila = Alloy.createModel('compania', {
					nombre: String.format(L('x1904871496', '%1$s Empresa'), item.toString()),
					id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
					id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item_pos.toString()),
					pais: 1
				});
				insertarModelo3_m.add(insertarModelo3_fila);
				insertarModelo3_fila.save();
				_.defer(function() {});
			});
		}
		var transformarModelo2_i = Alloy.createCollection('compania');
		transformarModelo2_i.fetch();
		var transformarModelo2_src = require('helper').query2array(transformarModelo2_i);
		var datos = [];
		_.each(transformarModelo2_src, function(fila, pos) {
			var new_row = {};
			_.each(fila, function(x, llave) {
				var newkey = '';
				if (llave == 'nombre') newkey = 'valor';
				if (llave == 'id_segured') newkey = 'id_segured';
				if (newkey != '') new_row[newkey] = fila[llave];
			});
			datos.push(new_row);
		});
		$.widgetModal.data({
			data: datos
		});
	};
	var ID_1948046515 = setTimeout(ID_1948046515_func, 1000 * 0.2);

}

function Abrir_widgetModal(e) {

	var evento = e;

}

function Respuesta_widgetModal(e) {

	var evento = e;
	$.widgetModal.labels({
		valor: evento.valor
	});
	$.datos.set({
		id_compania: evento.item.id_segured
	});
	if ('datos' in $) $datos = $.datos.toJSON();
	if (Ti.App.deployType != 'production') console.log('datos recibidos de modal compania', {
		"datos": evento
	});

}

function Change_ID_222185281(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	if (elemento == true || elemento == 'true') {
		var si = L('x3746555228_traducir', 'SI');
		$.NO4.setText(si);

		$.datos.set({
			asociado_hipotecario: 1
		});
		if ('datos' in $) $datos = $.datos.toJSON();
		$.widgetModal3.activar({});
	} else {
		var no = L('x3376426101_traducir', 'NO');
		$.NO4.setText(no);

		$.datos.set({
			asociado_hipotecario: 0,
			id_entidad_financiera: ''
		});
		if ('datos' in $) $datos = $.datos.toJSON();
		$.widgetModal3.labels({
			valor: 'Seleccione entidad'
		});
		$.widgetModal3.desactivar({});
	}
	elemento = null;

}

$.widgetModal3.init({
	titulo: L('x2059554513_traducir', 'ENTIDAD'),
	cargando: L('x1831148736', 'cargando...'),
	__id: 'ALL511044183',
	left: 0,
	onrespuesta: Respuesta_widgetModal3,
	campo: L('x2538351238_traducir', 'Entidad Financiera'),
	onabrir: Abrir_widgetModal3,
	color: 'verde',
	right: 0,
	seleccione: L('x3157783868_traducir', 'Seleccione entidad'),
	activo: false,
	onafterinit: Afterinit_widgetModal3
});

function Afterinit_widgetModal3(e) {

	var evento = e;
	var ID_779848452_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {} else {
			var eliminarModelo3_i = Alloy.Collections.entidad_financiera;
			var sql = "DELETE FROM " + eliminarModelo3_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo3_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo3_i.trigger('remove');
			var item_index = 0;
			_.each('A,A,B,C,D,E,F,F,G'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo4_m = Alloy.Collections.entidad_financiera;
				var insertarModelo4_fila = Alloy.createModel('entidad_financiera', {
					nombre: String.format(L('x380244590', '%1$s Entidad'), item.toString()),
					id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
					id_segured: String.format(L('x1929201954_traducir', '20%1$s'), item_pos.toString()),
					pais: 1
				});
				insertarModelo4_m.add(insertarModelo4_fila);
				insertarModelo4_fila.save();
				_.defer(function() {});
			});
		}
		var transformarModelo3_i = Alloy.createCollection('entidad_financiera');
		transformarModelo3_i.fetch();
		var transformarModelo3_src = require('helper').query2array(transformarModelo3_i);
		var datos = [];
		_.each(transformarModelo3_src, function(fila, pos) {
			var new_row = {};
			_.each(fila, function(x, llave) {
				var newkey = '';
				if (llave == 'nombre') newkey = 'valor';
				if (llave == 'id_segured') newkey = 'id_segured';
				if (newkey != '') new_row[newkey] = fila[llave];
			});
			datos.push(new_row);
		});
		$.widgetModal3.data({
			data: datos
		});
	};
	var ID_779848452 = setTimeout(ID_779848452_func, 1000 * 0.2);

}

function Abrir_widgetModal3(e) {

	var evento = e;

}

function Respuesta_widgetModal3(e) {

	var evento = e;
	$.widgetModal3.labels({
		valor: evento.valor
	});
	$.datos.set({
		id_entidad_financiera: evento.item.id_segured
	});
	if ('datos' in $) $datos = $.datos.toJSON();
	if (Ti.App.deployType != 'production') console.log('datos recibidos de modal entidad', {
		"datos": evento
	});

}

function Change_ID_1246703263(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	if (elemento == true || elemento == 'true') {
		var si = L('x3746555228_traducir', 'SI');
		$.NO5.setText(si);

		$.Meses.setEditable(true);

		$.datos.set({
			inhabitable: 1
		});
		if ('datos' in $) $datos = $.datos.toJSON();
	} else {
		var no = L('x3376426101_traducir', 'NO');
		$.NO5.setText(no);

		$.Meses.setEditable('false');

		$.datos.set({
			inhabitable: 0
		});
		if ('datos' in $) $datos = $.datos.toJSON();
		var var1;
		var1 = $.Meses;
		var1.setValue("");
	}
	elemento = null;

}

function Change_Meses(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	$.datos.set({
		estimacion_meses: elemento
	});
	if ('datos' in $) $datos = $.datos.toJSON();
	elemento = null, source = null;

}

function Return_Meses(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	$.Meses.blur();
	elemento = null, source = null;

}

function Touchstart_vista20(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.iconoNuevo_dano.setColor('#cccccc');


}

function Touchend_vista20(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.iconoNuevo_dano.setColor('#8ce5bd');

	/** 
	 * Ocupamos esta variable para impedir que la pantalla siguente se cargue dos veces 
	 */
	var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
	if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
		require('vars')['var_abriendo'] = L('x4261170317', 'true');
		Alloy.createController("niveles", {}).getView().open();
	}

}

function Swipe_vista21(e) {

	e.cancelBubble = true;
	var elemento = e.section.getItemAt(e.itemIndex);
	var findVariables = require('fvariables');
	var fila = {};
	_.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
		_.each(obj_id, function(valor, prop) {
			fila[prop] = elemento[id_field][prop];
			var llaves = findVariables(valor, '{', '}');
			_.each(llaves, function(llave) {
				fila[llave] = elemento[id_field][prop];
			});
		});
	});
	if (Ti.App.deployType != 'production') console.log('en normal', {
		"e": e
	});
	if (e.direction == L('x2053629800_traducir', 'left')) {
		var findVariables = require('fvariables');
		elemento.template = 'pborrar';
		_.each(_list_templates['pborrar'], function(obj_id, id_field) {
			_.each(obj_id, function(valor, prop) {
				var llaves = findVariables(valor, '{', '}');
				_.each(llaves, function(llave) {
					elemento[id_field] = {};
					elemento[id_field][prop] = fila[llave];
				});
			});
		});
		if (OS_IOS) {
			e.section.updateItemAt(e.itemIndex, elemento, {
				animated: true
			});
		} else if (OS_ANDROID) {
			e.section.updateItemAt(e.itemIndex, elemento);
		}
	}

}

function Click_vista21(e) {

	e.cancelBubble = true;
	var elemento = e.section.getItemAt(e.itemIndex);
	var findVariables = require('fvariables');
	var fila = {};
	_.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
		_.each(obj_id, function(valor, prop) {
			fila[prop] = elemento[id_field][prop];
			var llaves = findVariables(valor, '{', '}');
			_.each(llaves, function(llave) {
				fila[llave] = elemento[id_field][prop];
			});
		});
	});
	/** 
	 * Ocupamos esta variable para impedir que la pantalla siguente se cargue dos veces 
	 */
	var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
	if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
		require('vars')['var_abriendo'] = L('x4261170317', 'true');
		/** 
		 * Enviamos el parametro _dato para indicar cual sera el id del nivel a editar 
		 */
		Alloy.createController("editarnivel", {
			'_dato': fila
		}).getView().open();
	}

}

function Click_vista24(e) {

	e.cancelBubble = true;
	var elemento = e.section.getItemAt(e.itemIndex);
	var findVariables = require('fvariables');
	var fila = {};
	_.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
		_.each(obj_id, function(valor, prop) {
			fila[prop] = elemento[id_field][prop];
			var llaves = findVariables(valor, '{', '}');
			_.each(llaves, function(llave) {
				fila[llave] = elemento[id_field][prop];
			});
		});
	});
	if (Ti.App.deployType != 'production') console.log('click en borrar', {
		"e": e
	});
	var preguntarAlerta15_opts = [L('x3827418516_traducir', 'Si'), L('x1962639792_traducir', ' No')];
	var preguntarAlerta15 = Ti.UI.createAlertDialog({
		title: L('x4097537701_traducir', 'ALERTA'),
		message: '' + String.format(L('x4127782287_traducir', '¿ Seguro desea eliminar: %1$s ?'), fila.nombre.toString()) + '',
		buttonNames: preguntarAlerta15_opts
	});
	preguntarAlerta15.addEventListener('click', function(e) {
		var xd = preguntarAlerta15_opts[e.index];
		if (xd == L('x3827418516_traducir', 'Si')) {
			if (Ti.App.deployType != 'production') console.log('borrando xd', {});
			var eliminarModelo4_i = Alloy.Collections.insp_niveles;
			var sql = 'DELETE FROM ' + eliminarModelo4_i.config.adapter.collection_name + ' WHERE id=\'' + fila.id + '\'';
			var db = Ti.Database.open(eliminarModelo4_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo4_i.trigger('remove');
			_.defer(function() {
				Alloy.Collections.insp_niveles.fetch();
			});
		}
		xd = null;
		e.source.removeEventListener("click", arguments.callee);
	});
	preguntarAlerta15.show();

}

function Swipe_vista22(e) {

	e.cancelBubble = true;
	var elemento = e.section.getItemAt(e.itemIndex);
	var findVariables = require('fvariables');
	var fila = {};
	_.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
		_.each(obj_id, function(valor, prop) {
			fila[prop] = elemento[id_field][prop];
			var llaves = findVariables(valor, '{', '}');
			_.each(llaves, function(llave) {
				fila[llave] = elemento[id_field][prop];
			});
		});
	});
	if (Ti.App.deployType != 'production') console.log('en borrar', {
		"e": e
	});
	if (e.direction == L('x3033167124_traducir', 'right')) {
		if (Ti.App.deployType != 'production') console.log('swipe hacia derecha (desde borrar)', {});
		var findVariables = require('fvariables');
		elemento.template = 'nivel';
		_.each(_list_templates['nivel'], function(obj_id, id_field) {
			_.each(obj_id, function(valor, prop) {
				var llaves = findVariables(valor, '{', '}');
				_.each(llaves, function(llave) {
					elemento[id_field] = {};
					elemento[id_field][prop] = fila[llave];
				});
			});
		});
		if (OS_IOS) {
			e.section.updateItemAt(e.itemIndex, elemento, {
				animated: true
			});
		} else if (OS_ANDROID) {
			e.section.updateItemAt(e.itemIndex, elemento);
		}
	}

}

function Click_vista22(e) {

	e.cancelBubble = true;
	var elemento = e.section.getItemAt(e.itemIndex);
	var findVariables = require('fvariables');
	var fila = {};
	_.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
		_.each(obj_id, function(valor, prop) {
			fila[prop] = elemento[id_field][prop];
			var llaves = findVariables(valor, '{', '}');
			_.each(llaves, function(llave) {
				fila[llave] = elemento[id_field][prop];
			});
		});
	});
	if (Ti.App.deployType != 'production') console.log('en borrar', {
		"e": e
	});
	var findVariables = require('fvariables');
	elemento.template = 'nivel';
	_.each(_list_templates['nivel'], function(obj_id, id_field) {
		_.each(obj_id, function(valor, prop) {
			var llaves = findVariables(valor, '{', '}');
			_.each(llaves, function(llave) {
				elemento[id_field] = {};
				elemento[id_field][prop] = fila[llave];
			});
		});
	});
	if (OS_IOS) {
		e.section.updateItemAt(e.itemIndex, elemento, {
			animated: true
		});
	} else if (OS_ANDROID) {
		e.section.updateItemAt(e.itemIndex, elemento);
	}

}

$.widgetEstadeacuerdo.init({
	titulo: L('x404129330_traducir', '¿EL ASEGURADO CONFIRMA ESTOS DATOS?'),
	__id: 'ALL516906299',
	si: L('x1723413441_traducir', 'SI, Están correctos'),
	texto: L('x2083765231_traducir', 'El asegurado debe confirmar que los datos de esta sección están correctos'),
	pantalla: L('x2851883104_traducir', '¿ESTA DE ACUERDO?'),
	color: 'verde',
	onsi: si_widgetEstadeacuerdo,
	no: L('x55492959_traducir', 'NO, Hay que modificar algo')
});

function si_widgetEstadeacuerdo(e) {

	var evento = e;
	/** 
	 * Ocupamos esta variable para impedir que la pantalla siguente se cargue dos veces 
	 */
	var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
	if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
		require('vars')['var_abriendo'] = L('x4261170317', 'true');
		/** 
		 * Guardamos lo ingresado en el modelo 
		 */
		Alloy.Collections[$.datos.config.adapter.collection_name].add($.datos);
		$.datos.save();
		Alloy.Collections[$.datos.config.adapter.collection_name].fetch();
		/** 
		 * Limpiamos widget para liberar memoria ram 
		 */
		$.widgetModalmultiple.limpiar({});
		$.widgetModal.limpiar({});
		$.widgetModal3.limpiar({});
		/** 
		 * Enviamos a pantalla siniestro 
		 */
		Alloy.createController("siniestro_index", {}).getView().open();
	}

}

function Postlayout_vista27(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.progreso.show();

}

function Postlayout_inicio(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	/** 
	 * Cerramos pantalla datos basicos 
	 */
	Alloy.Events.trigger('_cerrar_insp', {
		pantalla: 'basicos'
	});
	var ID_1941683100_func = function() {
		/** 
		 * Una vez ya cargada la tarea, dejamos la variable limpia para poder abrir otras pantallas 
		 */
		require('vars')['var_abriendo'] = '';
	};
	var ID_1941683100 = setTimeout(ID_1941683100_func, 1000 * 0.2);

}

var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
	$.datos.set({
		id_inspeccion: seltarea.id_server
	});
	if ('datos' in $) $datos = $.datos.toJSON();
	/** 
	 * restringimos niveles mostrados a la inspeccion actual (por si los temporales aun tienen datos sin enviar) 
	 */
	consultarModelo_filter = function(coll) {
		var filtered = coll.filter(function(m) {
			var _tests = [],
				_all_true = false,
				model = m.toJSON();
			_tests.push((model.id_inspeccion == seltarea.id_server));
			var _all_true_s = _.uniq(_tests);
			_all_true = (_all_true_s.length == 1 && _all_true_s[0] == true) ? true : false;
			return _all_true;
		});
		filtered = _.toArray(filtered);
		return filtered;
	};
	_.defer(function() {
		Alloy.Collections.insp_niveles.fetch();
	});
	/** 
	 * Definimos campos defaults 
	 */
	$.datos.set({
		construccion_anexa: 0,
		otros_seguros_enlugar: 0,
		asociado_hipotecario: 0,
		inhabitable: 0
	});
	if ('datos' in $) $datos = $.datos.toJSON();
}
/** 
 * Cerramos esta pantalla cuando es llamada en la pantalla siguente (Siniestro) 
 */
_my_events['_cerrar_insp,ID_1362138204'] = function(evento) {
	if (!_.isUndefined(evento.pantalla)) {
		if (evento.pantalla == L('x345484177_traducir', 'caracteristicas')) {
			if (Ti.App.deployType != 'production') console.log('debug cerrando caracteristicas', {});
			var ID_1701237886_trycatch = {
				error: function(e) {
					if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
				}
			};
			try {
				ID_1701237886_trycatch.error = function(evento) {
					if (Ti.App.deployType != 'production') console.log('error cerrando caracteristicas', {});
				};
				$.widgetModalmultiple.limpiar({});
				$.widgetModal.limpiar({});
				$.widgetModal3.limpiar({});
				$.inicio.close();
			} catch (e) {
				ID_1701237886_trycatch.error(e);
			}
		}
	} else {
		if (Ti.App.deployType != 'production') console.log('debug cerrando (todas) caracteristicas', {});
		var ID_959866127_trycatch = {
			error: function(e) {
				if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
			}
		};
		try {
			ID_959866127_trycatch.error = function(evento) {
				if (Ti.App.deployType != 'production') console.log('error cerrando caracteristicas', {});
			};
			$.widgetModalmultiple.limpiar({});
			$.widgetModal.limpiar({});
			$.widgetModal3.limpiar({});
			$.inicio.close();
		} catch (e) {
			ID_959866127_trycatch.error(e);
		}
	}
};
Alloy.Events.on('_cerrar_insp', _my_events['_cerrar_insp,ID_1362138204']);

function Androidback_inicio(e) {

	e.cancelBubble = true;
	var elemento = e.source;

}

(function() {
	/** 
	 * Dejamos el scroll en el top y desenfocamos los campos de texto 
	 */
	var ID_1575921980_func = function() {
		$.scroll.scrollToTop();
		$.Ao.blur();
		$.Meses.blur();
	};
	var ID_1575921980 = setTimeout(ID_1575921980_func, 1000 * 0.2);
	/** 
	 * Modificamos el color del statusbar 
	 */
	var ID_323040650_func = function() {
		var inicio_statusbar = '#57BC8B';

		var setearStatusColor = function(inicio_statusbar) {
			if (OS_IOS) {
				if (inicio_statusbar == 'light' || inicio_statusbar == 'claro') {
					$.inicio_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.LIGHT_CONTENT);
				} else if (inicio_statusbar == 'grey' || inicio_statusbar == 'gris' || inicio_statusbar == 'gray') {
					$.inicio_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.GREY);
				} else if (inicio_statusbar == 'oscuro' || inicio_statusbar == 'dark') {
					$.inicio_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.DEFAULT);
				}
			} else if (OS_ANDROID) {
				abx.setStatusbarColor(inicio_statusbar);
			}
		};
		setearStatusColor(inicio_statusbar);

	};
	var ID_323040650 = setTimeout(ID_323040650_func, 1000 * 0.1);
})();

if (OS_IOS || OS_ANDROID) {
	$.inicio.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}
//$.inicio.open();
