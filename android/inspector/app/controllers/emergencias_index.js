var _bind4section = {
	"ref1": "emergencia"
};
var _list_templates = {
	"tarea": {
		"vista8": {},
		"vista9": {},
		"vista10": {},
		"vista2": {
			"id_server": "{id_server}"
		},
		"Label2": {
			"text": "{direccion}"
		},
		"Adistance": {
			"text": "a {distance} km"
		},
		"vista6": {},
		"vista4": {},
		"vista7": {},
		"Label": {
			"text": "{id}"
		},
		"Label4": {
			"text": "{nivel_2}, {pais}"
		},
		"vista3": {},
		"vista5": {},
		"Label3": {
			"text": "{comuna}"
		}
	}
};

var _activity;
if (OS_ANDROID) {
	_activity = $.inicio.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'inicio';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.inicio.addEventListener('open', function(e) {});
}
$.inicio.orientationModes = [Titanium.UI.PORTRAIT];


var consultarModelo_like = function(search) {
	if (typeof search !== 'string' || this === null) {
		return false;
	}
	search = search.replace(new RegExp("([\\.\\\\\\+\\*\\?\\[\\^\\]\\$\\(\\)\\{\\}\\=\\!\\<\\>\\|\\:\\-])", "g"), "\\$1");
	search = search.replace(/%/g, '.*').replace(/_/g, '.');
	return RegExp('^' + search + '$', 'gi').test(this);
};
var consultarModelo_filter = function(coll) {
	var filtered = coll.filter(function(m) {
		var _tests = [],
			_all_true = false,
			model = m.toJSON();
		_tests.push((model.perfil == 'casa'));
		var _all_true_s = _.uniq(_tests);
		_all_true = (_all_true_s.length == 1 && _all_true_s[0] == true) ? true : false;
		return _all_true;
	});
	filtered = _.toArray(filtered);
	var ordered = _.sortBy(filtered, 'distancia_2');
	return ordered;
};
var consultarModelo_update = function(e) {};
_.defer(function() {
	Alloy.Collections.emergencia.fetch();
});
Alloy.Collections.emergencia.on('add change delete', function(ee) {
	consultarModelo_update(ee);
});
var consultarModelo_transform = function(model) {
	var fila = model.toJSON();
	if ((_.isObject(fila.nivel_2) || _.isString(fila.nivel_2)) && _.isEmpty(fila.nivel_2)) {
		/** 
		 * El nivel_2 (region) no deber&#237;a estar nunca en blanco, por lo que marcamos los campos comuna y ciudad con guiones para marcar falla en datos. 
		 */
		var fila = _.extend(fila, {
			comuna: fila.nivel_1
		});
	} else if ((_.isObject(fila.nivel_3) || _.isString(fila.nivel_3)) && _.isEmpty(fila.nivel_3)) {
		/** 
		 * El nivel_3 (ciudad) tampoco deberia estar nunca en blanco. 
		 */
		var fila = _.extend(fila, {
			comuna: fila.nivel_2
		});
	} else if ((_.isObject(fila.nivel_4) || _.isString(fila.nivel_4)) && _.isEmpty(fila.nivel_4)) {
		var fila = _.extend(fila, {
			comuna: fila.nivel_3
		});
	} else if ((_.isObject(fila.nivel_5) || _.isString(fila.nivel_5)) && _.isEmpty(fila.nivel_5)) {
		var fila = _.extend(fila, {
			comuna: fila.nivel_4
		});
	} else {
		var fila = _.extend(fila, {
			comuna: fila.nivel_5
		});
	}
	return fila;
};
Alloy.Collections.emergencia.fetch();


$.widgetSininternet.init({
	titulo: L('x2828751865_traducir', '¡ESTAS SIN CONEXION!'),
	__id: 'ALL1512316616',
	mensaje: L('x1855928898_traducir', 'No puedes ver las tareas de emergencias de hoy'),
	onon: on_widgetSininternet,
	onoff: Off_widgetSininternet
});

function on_widgetSininternet(e) {

	var evento = e;
	var vista_visible = true;

	if (vista_visible == 'si') {
		vista_visible = true;
	} else if (vista_visible == 'no') {
		vista_visible = false;
	}
	$.vista.setVisible(vista_visible);


}

function Off_widgetSininternet(e) {

	var evento = e;
	var vista_visible = false;

	if (vista_visible == 'si') {
		vista_visible = true;
	} else if (vista_visible == 'no') {
		vista_visible = false;
	}
	$.vista.setVisible(vista_visible);


}

function Itemclick_listado(e) {

	e.cancelBubble = true;
	var objeto = e.section.getItemAt(e.itemIndex);
	var modelo = {},
		_modelo = [];
	var fila = {},
		fila_bak = {},
		info = {
			_template: objeto.template,
			_what: [],
			_seccion_ref: e.section.getHeaderTitle(),
			_model_id: -1
		},
		_tmp = {
			objmap: {}
		};
	if ('itemId' in e) {
		info._model_id = e.itemId;
		modelo._id = info._model_id;
		if (info._seccion_ref != '' && info._seccion_ref in _bind4section) {
			modelo._collection = _bind4section[info._seccion_ref];
			_tmp._coll = modelo._collection;
		}
	}
	var findVariables = require('fvariables');
	_.each(_list_templates[info._template], function(obj_id, id) {
		_.each(obj_id, function(valor, prop) {
			var llaves = findVariables(valor, '{', '}');
			_.each(llaves, function(llave) {
				_tmp.objmap[llave] = {
					id: id,
					prop: prop
				};
				fila[llave] = objeto[id][prop];
				if (id == e.bindId) info._what.push(llave);
			});
		});
	});
	info._what = info._what.join(',');
	fila_bak = JSON.parse(JSON.stringify(fila));
	if (Ti.App.deployType != 'production') console.log('Mi objecto es', {
		"info": info,
		"objeto": fila
	});
	/** 
	 * Ocupamos esta variable para impedir que la pantalla siguente se cargue dos veces 
	 */
	var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
	if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
		require('vars')['var_abriendo'] = L('x4261170317', 'true');
		/** 
		 * Nombre Proyecto: Emergencias
		 * Descripci&#243;n: Este proyecto se encarga de mostrar un listado con las tareas disponibles que tengan caracter de emergencia.
		 * Consume las siguientes tablas locales:
		 * emergencia: Consulta y guarda registros diponibles.
		 * inspectores: Consulta datos de inspector logueado.
		 * Utiliza los siguientes estilos locales:
		 * estilo2
		 * estilo4
		 * estilo5
		 * Predecedor del Proyecto:
		 * Proyecto Login
		 * Sucesor del Proyecto
		 * Proyecto Tomar Tarea
		 * Pantallas:
		 * Inicio: Se encarga de mostrar una lista con las tareas que estan disponibles para ser tomadas con caracter de emergencia.
		 * Widgets:
		 * sininternet: en caso de que el dispositivo no este conectado a internet se oculta la lista de tareas y muestra un texto indicando que no tiene acceso a internet 
		 */
		var nulo = Alloy.createController("tomartarea_index", {
			'_objeto': fila,
			'_id': fila.id,
			'_tipo': 'emergencias',
			'__master_model': (typeof modelo !== 'undefined') ? modelo : {},
			'__modelo': (typeof _modelo !== 'undefined' && _modelo.length > 0) ? _modelo[0] : {}
		}).getView();
		nulo.open({
			modal: true
		});
		nulo = null;
	}
	_tmp.changed = false;
	_tmp.diff_keys = [];
	_.each(fila, function(value1, prop) {
		var had_samekey = false;
		_.each(fila_bak, function(value2, prop2) {
			if (prop == prop2 && value1 == value2) {
				had_samekey = true;
			} else if (!_.has(fila_bak, prop) || !_.has(fila, prop2)) {
				has_samekey = true;
			}
		});
		if (!had_samekey) _tmp.diff_keys.push(prop);
	});
	if (_tmp.diff_keys.length > 0) _tmp.changed = true;
	if (_tmp.changed == true) {
		_.each(_tmp.diff_keys, function(llave) {
			objeto[_tmp.objmap[llave].id][_tmp.objmap[llave].prop] = fila[llave];
		});
		e.section.updateItemAt(e.itemIndex, objeto);
	}

}

function Click_imagen(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var evento = e;
	var imagen_imagen = 'domicilio_on';

	if (typeof imagen_imagen == 'string' && 'styles' in require('a4w') && imagen_imagen in require('a4w').styles['images']) {
		imagen_imagen = require('a4w').styles['images'][imagen_imagen];
	}
	$.imagen.setImage(imagen_imagen);

	var imagen2_imagen = 'ubicacion_off';

	if (typeof imagen2_imagen == 'string' && 'styles' in require('a4w') && imagen2_imagen in require('a4w').styles['images']) {
		imagen2_imagen = require('a4w').styles['images'][imagen2_imagen];
	}
	$.imagen2.setImage(imagen2_imagen);

	consultarModelo_filter = function(coll) {
		var filtered = coll.filter(function(m) {
			var _tests = [],
				_all_true = false,
				model = m.toJSON();
			_tests.push((model.perfil == 'casa'));
			var _all_true_s = _.uniq(_tests);
			_all_true = (_all_true_s.length == 1 && _all_true_s[0] == true) ? true : false;
			return _all_true;
		});
		filtered = _.toArray(filtered);
		var ordered = _.sortBy(filtered, 'distancia_2');
		return ordered;
	};
	_.defer(function() {
		Alloy.Collections.emergencia.fetch();
	});

}

function Click_imagen2(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var evento = e;
	var imagen2_imagen = 'ubicacion_on';

	if (typeof imagen2_imagen == 'string' && 'styles' in require('a4w') && imagen2_imagen in require('a4w').styles['images']) {
		imagen2_imagen = require('a4w').styles['images'][imagen2_imagen];
	}
	$.imagen2.setImage(imagen2_imagen);

	var imagen_imagen = 'domicilio_off';

	if (typeof imagen_imagen == 'string' && 'styles' in require('a4w') && imagen_imagen in require('a4w').styles['images']) {
		imagen_imagen = require('a4w').styles['images'][imagen_imagen];
	}
	$.imagen.setImage(imagen_imagen);

	consultarModelo_filter = function(coll) {
		var filtered = coll.filter(function(m) {
			var _tests = [],
				_all_true = false,
				model = m.toJSON();
			_tests.push((model.perfil == 'ubicacion'));
			var _all_true_s = _.uniq(_tests);
			_all_true = (_all_true_s.length == 1 && _all_true_s[0] == true) ? true : false;
			return _all_true;
		});
		filtered = _.toArray(filtered);
		var ordered = _.sortBy(filtered, 'distancia_2');
		return ordered;
	};
	_.defer(function() {
		Alloy.Collections.emergencia.fetch();
	});

}

(function() {
	_my_events['_refrescar_tareas,ID_1146039108'] = function(evento) {
		/** 
		 * Revisamos si hay inspecciones en curso, si no hay ninguna, se refresca la lista 
		 */
		var inspeccion_encurso = ('inspeccion_encurso' in require('vars')) ? require('vars')['inspeccion_encurso'] : '';
		if (inspeccion_encurso == false || inspeccion_encurso == 'false') {
			_.defer(function() {
				Alloy.Collections.emergencia.fetch();
			});
		}
	};
	Alloy.Events.on('_refrescar_tareas', _my_events['_refrescar_tareas,ID_1146039108']);
	_my_events['_refrescaremergencias,ID_314337948'] = function(evento) {
		_.defer(function() {
			Alloy.Collections.emergencia.fetch();
		});
	};
	Alloy.Events.on('_refrescaremergencias', _my_events['_refrescaremergencias,ID_314337948']);
})();

function Androidback_inicio(e) {
	/** 
	 * No tiene acciones para impedir que el usuario cierre salga del menu por error 
	 */
	e.cancelBubble = true;
	var elemento = e.source;

}
if (OS_IOS || OS_ANDROID) {
	$.inicio.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}
//$.inicio.open();
