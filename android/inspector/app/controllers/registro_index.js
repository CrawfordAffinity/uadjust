var _bind4section = {};
var _list_templates = {};

var _activity;
if (OS_ANDROID) {
	_activity = $.ENROLAMIENTO.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'ENROLAMIENTO';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.ENROLAMIENTO.addEventListener('open', function(e) {});
}
$.ENROLAMIENTO.orientationModes = [Titanium.UI.PORTRAIT];


$.widgetBarra.init({
	titulo: L('x2146494644_traducir', 'ENROLAMIENTO'),
	__id: 'ALL1426322055',
	fondo: 'fondoblanco',
	colortitulo: 'negro',
	modal: L('', '')
});


$.widgetHeader.init({
	titulo: L('x587441556_traducir', 'PARTE 1: País de residencia'),
	__id: 'ALL673432332',
	avance: '1/6',
	onclick: Click_widgetHeader
});

function Click_widgetHeader(e) {

	var evento = e;

}

var consultarModelo_like = function(search) {
	if (typeof search !== 'string' || this === null) {
		return false;
	}
	search = search.replace(new RegExp("([\\.\\\\\\+\\*\\?\\[\\^\\]\\$\\(\\)\\{\\}\\=\\!\\<\\>\\|\\:\\-])", "g"), "\\$1");
	search = search.replace(/%/g, '.*').replace(/_/g, '.');
	return RegExp('^' + search + '$', 'gi').test(this);
};
var consultarModelo_filter = function(coll) {
	var filtered = _.toArray(coll.filter(function(m) {
		return true;
	}));
	return filtered;
};
var consultarModelo_transform = function(model) {
	var fila = model.toJSON();
	return fila;
};
var consultarModelo_update = function(e) {};
_.defer(function() {
	Alloy.Collections.pais.fetch();
});
Alloy.Collections.pais.on('add change delete', function(ee) {
	consultarModelo_update(ee);
});
Alloy.Collections.pais.fetch();



function Change_picker(e) {

	e.cancelBubble = true;
	var elemento = e;
	var _columna = e.columnIndex;
	var columna = e.columnIndex + 1;
	var _fila = e.rowIndex;
	var fila = e.rowIndex + 1;
	var modelo = require('helper').query2array(Alloy.Collections.pais)[e.rowIndex];
	_.defer(function(modelo) {
		require('vars')['pais_seleccionado'] = modelo;
		if (Ti.App.deployType != 'production') console.log('pais seleccionado1', {
			"asd": modelo
		});
	}, modelo);

}

$.widgetBotonlargo.init({
	titulo: L('x1524107289_traducir', 'CONTINUAR'),
	__id: 'ALL1007469789',
	onclick: Click_widgetBotonlargo
});

function Click_widgetBotonlargo(e) {

	var evento = e;
	/** 
	 * Mostramos animacion de progreso en el boton continuar 
	 */
	$.widgetBotonlargo.iniciar_progreso({});
	var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
	var pais_seleccionado = ('pais_seleccionado' in require('vars')) ? require('vars')['pais_seleccionado'] : '';
	if (pais_seleccionado == -1) {
		var consultarModelo2_i = Alloy.createCollection('pais');
		var consultarModelo2_i_where = '';
		consultarModelo2_i.fetch();
		var predet = require('helper').query2array(consultarModelo2_i);
		require('vars')['pais_seleccionado'] = predet[0];
		var pais_seleccionado = ('pais_seleccionado' in require('vars')) ? require('vars')['pais_seleccionado'] : '';
	}
	if (Ti.App.deployType != 'production') console.log('cambiando idioma a', {
		"lenguaje": pais_seleccionado.lenguaje
	});
	/** 
	 * Cambiamos el idioma de la app segun sea el pais seleccionado por el inspector 
	 */
	if (OS_ANDROID) {
		Ti.Locale.setLanguage(pais_seleccionado.lenguaje.split('_').join('-'));
		var intent = Ti.Android.createIntent({
			flags: Ti.Android.FLAG_ACTIVITY_CLEAR_TOP | Ti.Android.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED | Ti.Android.FLAG_ACTIVITY_NEW_TASK,
			action: Ti.Android.ACTION_MAIN,
			url: 'index.js'
		});
	} else {
		Ti.Locale.setLanguage(pais_seleccionado.lenguaje.split('_').join('-'));
	}
	/** 
	 * Agregamos campo pais al objeto registro 
	 */
	var registro = _.extend(registro, {
		pais: pais_seleccionado.id_server
	});
	/** 
	 * Y lo guardamos en una variable global 
	 */
	require('vars')['registro'] = registro;
	var ID_314435633_func = function() {
		/** 
		 * Detenemos animacion de progreso en el boton continuar 
		 */
		$.widgetBotonlargo.detener_progreso({});
		/** 
		 * Enviamos a la proxima pantalla, datos basicos 
		 */
		Alloy.createController("datos_personales", {}).getView().open();
	};
	var ID_314435633 = setTimeout(ID_314435633_func, 1000 * 0.3);

}

(function() {
	/** 
	 * Inicializamos de registro 
	 */
	var registro = {
		pais: 1
	};
	/** 
	 * Actualizamos la variable de registro 
	 */
	require('vars')['registro'] = registro;
	/** 
	 * Predefinimos un pais nulo por defecto 
	 */
	require('vars')['pais_seleccionado'] = -1;
	/** 
	 * Creamos evento que al cerrar desde la ultima pantalla, esta tambi&#233;n se cierre. Evitamos el uso excesivo de memoria ram 
	 */
	_my_events['_close_enrolamiento,ID_222275329'] = function(evento) {
		$.ENROLAMIENTO.close();
		if (Ti.App.deployType != 'production') console.log('escuchando cerrar enrolamiento pais_residencia', {});
	};
	Alloy.Events.on('_close_enrolamiento', _my_events['_close_enrolamiento,ID_222275329']);
})();

function Androidback_ENROLAMIENTO(e) {
	/** 
	 * Dejamos esta accion vacia para que no pueda volver a la pantalla anterior 
	 */
	e.cancelBubble = true;
	var elemento = e.source;

}
if (OS_IOS || OS_ANDROID) {
	$.ENROLAMIENTO.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}
//$.ENROLAMIENTO.open();
