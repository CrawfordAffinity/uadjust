var _bind4section = {
	"ref1": "insp_contenido"
};
var _list_templates = {
	"pborrar": {
		"vista5": {},
		"imagen": {},
		"vista3": {},
		"Label4": {
			"text": "{id}"
		},
		"Label3": {
			"text": "{nombre}"
		},
		"vista4": {}
	},
	"contenido": {
		"vista2": {},
		"Label2": {
			"text": "{id}"
		},
		"Label": {
			"text": "{nombre}"
		}
	}
};

var _activity;
if (OS_ANDROID) {
	_activity = $.inicio.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'inicio';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.inicio.addEventListener('open', function(e) {});
}
$.inicio.orientationModes = [Titanium.UI.PORTRAIT];


var consultarModelo_like = function(search) {
	if (typeof search !== 'string' || this === null) {
		return false;
	}
	search = search.replace(new RegExp("([\\.\\\\\\+\\*\\?\\[\\^\\]\\$\\(\\)\\{\\}\\=\\!\\<\\>\\|\\:\\-])", "g"), "\\$1");
	search = search.replace(/%/g, '.*').replace(/_/g, '.');
	return RegExp('^' + search + '$', 'gi').test(this);
};
var consultarModelo_filter = function(coll) {
	var filtered = _.toArray(coll.filter(function(m) {
		return true;
	}));
	return filtered;
};
var consultarModelo_transform = function(model) {
	var fila = model.toJSON();
	return fila;
};
var consultarModelo_update = function(e) {};
_.defer(function() {
	Alloy.Collections.insp_contenido.fetch();
});
Alloy.Collections.insp_contenido.on('add change delete', function(ee) {
	consultarModelo_update(ee);
});
Alloy.Collections.insp_contenido.fetch();


$.widgetBarra.init({
	titulo: L('x70824419_traducir', 'DAÑOS EN CONTENIDOS'),
	onsalirinsp: Salirinsp_widgetBarra,
	__id: 'ALL138747237',
	salir_insp: L('', ''),
	fondo: 'fondoceleste',
	top: 0,
	agregar_dano: L('', ''),
	onagregar_dano: Agregar_dano_widgetBarra
});

function Salirinsp_widgetBarra(e) {

	var evento = e;
	var preguntarOpciones_opts = [L('x3481285698_traducir', 'Asegurado no puede seguir'), L('x3077268071_traducir', ' Se me acabo la bateria'), L('x479989047_traducir', ' Tuve un accidente'), L('x2376009830_traducir', ' Cancelar')];
	var preguntarOpciones = Ti.UI.createOptionDialog({
		title: L('x1670176792_traducir', 'RAZON PARA CANCELAR INSPECCION ACTUAL'),
		options: preguntarOpciones_opts
	});
	preguntarOpciones.addEventListener('click', function(e) {
		var resp = preguntarOpciones_opts[e.index];
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var razon = "";
			if (resp == L('x3481285698_traducir', 'Asegurado no puede seguir')) {
				razon = "Asegurado no puede seguir";
			}
			if (resp == L('x1862794075_traducir', 'Se me acabo la bateria')) {
				razon = "Se me acabo la bateria";
			}
			if (resp == L('x4012805111_traducir', 'Tuve un accidente')) {
				razon = "Tuve un accidente";
			}
			if (Ti.App.deployType != 'production') console.log('mi razon es', {
				"datos": razon
			});
			if ((_.isObject(razon) || (_.isString(razon)) && !_.isEmpty(razon)) || _.isNumber(razon) || _.isBoolean(razon)) {
				if (Ti.App.deployType != 'production') console.log('llamando servicio cancelarTarea', {});
				require('vars')['insp_cancelada'] = L('x2941610362_traducir', 'siniestro');
				var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
				var vista8_visible = true;

				if (vista8_visible == 'si') {
					vista8_visible = true;
				} else if (vista8_visible == 'no') {
					vista8_visible = false;
				}
				$.vista8.setVisible(vista8_visible);

				var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
				var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
				if (Ti.App.deployType != 'production') console.log('detalle de seltarea', {
					"data": seltarea
				});
				var datos = {
					id_inspector: inspector.id_server,
					codigo_identificador: inspector.codigo_identificador,
					id_server: seltarea.id_server,
					num_caso: seltarea.num_caso,
					razon: razon
				};
				require('vars')[_var_scopekey]['datos'] = datos;
				var consultarURL = {};
				consultarURL.success = function(e) {
					var elemento = e,
						valor = e;
					var vista8_visible = false;

					if (vista8_visible == 'si') {
						vista8_visible = true;
					} else if (vista8_visible == 'no') {
						vista8_visible = false;
					}
					$.vista8.setVisible(vista8_visible);

					Alloy.createController("firma_index", {}).getView().open();
					var ID_860845987_func = function() {
						Alloy.Events.trigger('_cerrar_insp', {
							pantalla: 'contenidos'
						});
					};
					var ID_860845987 = setTimeout(ID_860845987_func, 1000 * 0.5);
					elemento = null, valor = null;
				};
				consultarURL.error = function(e) {
					var elemento = e,
						valor = e;
					if (Ti.App.deployType != 'production') console.log('hubo error llamando cancelarTarea, la agregamos a cola de salida', {
						"elemento": elemento
					});
					if (Ti.App.deployType != 'production') console.log('agregando servicio cancelarTarea a cola', {});
					var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
					var datos = ('datos' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['datos'] : '';
					var insertarModelo_m = Alloy.Collections.cola;
					var insertarModelo_fila = Alloy.createModel('cola', {
						data: JSON.stringify(datos),
						id_tarea: seltarea.id_server,
						tipo: 'cancelar'
					});
					insertarModelo_m.add(insertarModelo_fila);
					insertarModelo_fila.save();
					_.defer(function() {});
					var vista8_visible = false;

					if (vista8_visible == 'si') {
						vista8_visible = true;
					} else if (vista8_visible == 'no') {
						vista8_visible = false;
					}
					$.vista8.setVisible(vista8_visible);

					Alloy.createController("firma_index", {}).getView().open();
					var ID_918462859_func = function() {
						Alloy.Events.trigger('_cerrar_insp', {
							pantalla: 'contenidos'
						});
					};
					var ID_918462859 = setTimeout(ID_918462859_func, 1000 * 0.5);
					elemento = null, valor = null;
				};
				require('helper').ajaxUnico('consultarURL', '' + String.format(L('x3244141284', '%1$scancelarTarea'), url_server.toString()) + '', 'POST', {
					id_inspector: seltarea.id_inspector,
					codigo_identificador: inspector.codigo_identificador,
					id_tarea: seltarea.id_server,
					num_caso: seltarea.num_caso,
					mensaje: razon,
					opcion: 0,
					tipo: 1
				}, 15000, consultarURL);
			}
		}
		resp = null;
		e.source.removeEventListener("click", arguments.callee);
	});
	preguntarOpciones.show();

}

function Agregar_dano_widgetBarra(e) {

	var evento = e;
	/** 
	 * Ocupamos esta variable para impedir que la pantalla siguente se cargue dos veces 
	 */
	var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
	if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
		require('vars')['var_abriendo'] = L('x4261170317', 'true');
		Alloy.createController("nuevocontenido", {}).getView().open();
	}

}

function Swipe_vista2(e) {

	e.cancelBubble = true;
	var elemento = e.section.getItemAt(e.itemIndex);
	var findVariables = require('fvariables');
	var fila = {};
	_.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
		_.each(obj_id, function(valor, prop) {
			fila[prop] = elemento[id_field][prop];
			var llaves = findVariables(valor, '{', '}');
			_.each(llaves, function(llave) {
				fila[llave] = elemento[id_field][prop];
			});
		});
	});
	if (Ti.App.deployType != 'production') console.log('en normal', {
		"e": e
	});
	if (e.direction == L('x2053629800_traducir', 'left')) {
		var findVariables = require('fvariables');
		elemento.template = 'pborrar';
		_.each(_list_templates['pborrar'], function(obj_id, id_field) {
			_.each(obj_id, function(valor, prop) {
				var llaves = findVariables(valor, '{', '}');
				_.each(llaves, function(llave) {
					elemento[id_field] = {};
					elemento[id_field][prop] = fila[llave];
				});
			});
		});
		if (OS_IOS) {
			e.section.updateItemAt(e.itemIndex, elemento, {
				animated: true
			});
		} else if (OS_ANDROID) {
			e.section.updateItemAt(e.itemIndex, elemento);
		}
	}

}

function Click_vista2(e) {

	e.cancelBubble = true;
	var elemento = e.section.getItemAt(e.itemIndex);
	var findVariables = require('fvariables');
	var fila = {};
	_.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
		_.each(obj_id, function(valor, prop) {
			fila[prop] = elemento[id_field][prop];
			var llaves = findVariables(valor, '{', '}');
			_.each(llaves, function(llave) {
				fila[llave] = elemento[id_field][prop];
			});
		});
	});
	/** 
	 * Ocupamos esta variable para impedir que la pantalla siguente se cargue dos veces 
	 */
	var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
	if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
		require('vars')['var_abriendo'] = L('x4261170317', 'true');
		/** 
		 * Enviamos a editar contenido, y pasamos parametro _dato para enviarle el id del contenido a editar 
		 */
		Alloy.createController("editarcontenido", {
			'_dato': fila
		}).getView().open();
	}

}

function Click_vista5(e) {

	e.cancelBubble = true;
	var elemento = e.section.getItemAt(e.itemIndex);
	var findVariables = require('fvariables');
	var fila = {};
	_.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
		_.each(obj_id, function(valor, prop) {
			fila[prop] = elemento[id_field][prop];
			var llaves = findVariables(valor, '{', '}');
			_.each(llaves, function(llave) {
				fila[llave] = elemento[id_field][prop];
			});
		});
	});
	if (Ti.App.deployType != 'production') console.log('click en borrar', {
		"e": e
	});
	var preguntarAlerta_opts = [L('x3827418516_traducir', 'Si'), L('x1962639792_traducir', ' No')];
	var preguntarAlerta = Ti.UI.createAlertDialog({
		title: L('x4097537701_traducir', 'ALERTA'),
		message: '' + String.format(L('x4127782287_traducir', '¿ Seguro desea eliminar: %1$s ?'), fila.nombre.toString()) + '',
		buttonNames: preguntarAlerta_opts
	});
	preguntarAlerta.addEventListener('click', function(e) {
		var xd = preguntarAlerta_opts[e.index];
		if (xd == L('x3827418516_traducir', 'Si')) {
			if (Ti.App.deployType != 'production') console.log('borrando xd', {});
			var eliminarModelo_i = Alloy.Collections.insp_contenido;
			var sql = 'DELETE FROM ' + eliminarModelo_i.config.adapter.collection_name + ' WHERE id=\'' + fila.id + '\'';
			var db = Ti.Database.open(eliminarModelo_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo_i.trigger('remove');
			_.defer(function() {
				Alloy.Collections.insp_contenido.fetch();
			});
		}
		xd = null;
		e.source.removeEventListener("click", arguments.callee);
	});
	preguntarAlerta.show();

}

function Swipe_vista3(e) {

	e.cancelBubble = true;
	var elemento = e.section.getItemAt(e.itemIndex);
	var findVariables = require('fvariables');
	var fila = {};
	_.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
		_.each(obj_id, function(valor, prop) {
			fila[prop] = elemento[id_field][prop];
			var llaves = findVariables(valor, '{', '}');
			_.each(llaves, function(llave) {
				fila[llave] = elemento[id_field][prop];
			});
		});
	});
	if (Ti.App.deployType != 'production') console.log('en borrar', {
		"e": e
	});
	if (e.direction == L('x3033167124_traducir', 'right')) {
		if (Ti.App.deployType != 'production') console.log('swipe hacia derecha (desde borrar)', {});
		var findVariables = require('fvariables');
		elemento.template = 'contenido';
		_.each(_list_templates['contenido'], function(obj_id, id_field) {
			_.each(obj_id, function(valor, prop) {
				var llaves = findVariables(valor, '{', '}');
				_.each(llaves, function(llave) {
					elemento[id_field] = {};
					elemento[id_field][prop] = fila[llave];
				});
			});
		});
		if (OS_IOS) {
			e.section.updateItemAt(e.itemIndex, elemento, {
				animated: true
			});
		} else if (OS_ANDROID) {
			e.section.updateItemAt(e.itemIndex, elemento);
		}
	}

}

function Click_vista3(e) {

	e.cancelBubble = true;
	var elemento = e.section.getItemAt(e.itemIndex);
	var findVariables = require('fvariables');
	var fila = {};
	_.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
		_.each(obj_id, function(valor, prop) {
			fila[prop] = elemento[id_field][prop];
			var llaves = findVariables(valor, '{', '}');
			_.each(llaves, function(llave) {
				fila[llave] = elemento[id_field][prop];
			});
		});
	});
	if (Ti.App.deployType != 'production') console.log('en borrar', {
		"e": e
	});
	var findVariables = require('fvariables');
	elemento.template = 'contenido';
	_.each(_list_templates['contenido'], function(obj_id, id_field) {
		_.each(obj_id, function(valor, prop) {
			var llaves = findVariables(valor, '{', '}');
			_.each(llaves, function(llave) {
				elemento[id_field] = {};
				elemento[id_field][prop] = fila[llave];
			});
		});
	});
	if (OS_IOS) {
		e.section.updateItemAt(e.itemIndex, elemento, {
			animated: true
		});
	} else if (OS_ANDROID) {
		e.section.updateItemAt(e.itemIndex, elemento);
	}

}

$.widgetBotonlargo.init({
	titulo: L('x8960895_traducir', 'GUARDAR DAÑOS'),
	__id: 'ALL1007469789',
	color: 'verde',
	onclick: Click_widgetBotonlargo
});

function Click_widgetBotonlargo(e) {

	var evento = e;
	/** 
	 * Abrimos pantalla esta de acuerdo 
	 */
	$.widgetEstadeacuerdo.abrir({
		color: 'celeste'
	});
	/** 
	 * Mostramos animacion de progreso 
	 */
	$.widgetBotonlargo.iniciar_progreso({});

}

$.widgetEstadeacuerdo.init({
	titulo: L('x404129330_traducir', '¿EL ASEGURADO CONFIRMA ESTOS DATOS?'),
	__id: 'ALL516906299',
	si: L('x1723413441_traducir', 'SI, Están correctos'),
	texto: L('x2083765231_traducir', 'El asegurado debe confirmar que los datos de esta sección están correctos'),
	pantalla: L('x2851883104_traducir', '¿ESTA DE ACUERDO?'),
	onno: no_widgetEstadeacuerdo,
	color: 'morado',
	onsi: si_widgetEstadeacuerdo,
	no: L('x55492959_traducir', 'NO, Hay que modificar algo')
});

function si_widgetEstadeacuerdo(e) {

	var evento = e;
	/** 
	 * Ocupamos esta variable para impedir que la pantalla siguente se cargue dos veces 
	 */
	var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
	if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
		require('vars')['var_abriendo'] = L('x4261170317', 'true');
		/** 
		 * Ocultamos animacion de progreso 
		 */
		$.widgetBotonlargo.detener_progreso({});
		Alloy.createController("documentos_index", {}).getView().open();
	}

}

function no_widgetEstadeacuerdo(e) {

	var evento = e;
	var ID_1631360655_func = function() {
		/** 
		 * Detenemos el progreso en el boton 
		 */
		$.widgetBotonlargo.detener_progreso({});
	};
	var ID_1631360655 = setTimeout(ID_1631360655_func, 1000 * 0.5);

}

function Postlayout_vista8(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.progreso.show();

}

function Postlayout_inicio(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	/** 
	 * Limpieza memoria ram 
	 */
	Alloy.Events.trigger('_cerrar_insp', {
		pantalla: 'recintos'
	});
	var ID_538989377_func = function() {
		/** 
		 * Una vez ya cargada la tarea, dejamos la variable limpia para poder abrir otras pantallas 
		 */
		require('vars')['var_abriendo'] = '';
	};
	var ID_538989377 = setTimeout(ID_538989377_func, 1000 * 0.2);

}

_my_events['_cerrar_insp,ID_231485652'] = function(evento) {
	if (!_.isUndefined(evento.pantalla)) {
		if (evento.pantalla == L('x3680765087_traducir', 'contenidos')) {
			if (Ti.App.deployType != 'production') console.log('debug cerrando contenidos', {});
			var ID_1781710521_trycatch = {
				error: function(e) {
					if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
				}
			};
			try {
				ID_1781710521_trycatch.error = function(evento) {
					if (Ti.App.deployType != 'production') console.log('error cerrando contenidos', {});
				};
				$.inicio.close();
			} catch (e) {
				ID_1781710521_trycatch.error(e);
			}
		}
	} else {
		if (Ti.App.deployType != 'production') console.log('debug cerrando (todas) contenidos', {});
		var ID_1962200830_trycatch = {
			error: function(e) {
				if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
			}
		};
		try {
			ID_1962200830_trycatch.error = function(evento) {
				if (Ti.App.deployType != 'production') console.log('error cerrando contenidos', {});
			};
			$.inicio.close();
		} catch (e) {
			ID_1962200830_trycatch.error(e);
		}
	}
};
Alloy.Events.on('_cerrar_insp', _my_events['_cerrar_insp,ID_231485652']);
var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
	/** 
	 * restringimos contenidos mostrados a la inspeccion actual (por si los temporales aun tienen datos) 
	 */
	consultarModelo_filter = function(coll) {
		var filtered = coll.filter(function(m) {
			var _tests = [],
				_all_true = false,
				model = m.toJSON();
			_tests.push((model.id_inspeccion == seltarea.id_server));
			var _all_true_s = _.uniq(_tests);
			_all_true = (_all_true_s.length == 1 && _all_true_s[0] == true) ? true : false;
			return _all_true;
		});
		filtered = _.toArray(filtered);
		return filtered;
	};
	_.defer(function() {
		Alloy.Collections.insp_contenido.fetch();
	});
} else {
	/** 
	 * Borramos dummies 
	 */
	/** 
	 * Borramos dummies 
	 */
	var eliminarModelo2_i = Alloy.Collections.insp_contenido;
	var sql = "DELETE FROM " + eliminarModelo2_i.config.adapter.collection_name;
	var db = Ti.Database.open(eliminarModelo2_i.config.adapter.db_name);
	db.execute(sql);
	db.close();
	eliminarModelo2_i.trigger('remove');
	_.defer(function() {
		Alloy.Collections.insp_contenido.fetch();
	});
}

(function() {
	/** 
	 * Modificamos el statusbar 
	 */
	var ID_1061396234_func = function() {
		var inicio_statusbar = '#239EC4';

		var setearStatusColor = function(inicio_statusbar) {
			if (OS_IOS) {
				if (inicio_statusbar == 'light' || inicio_statusbar == 'claro') {
					$.inicio_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.LIGHT_CONTENT);
				} else if (inicio_statusbar == 'grey' || inicio_statusbar == 'gris' || inicio_statusbar == 'gray') {
					$.inicio_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.GREY);
				} else if (inicio_statusbar == 'oscuro' || inicio_statusbar == 'dark') {
					$.inicio_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.DEFAULT);
				}
			} else if (OS_ANDROID) {
				abx.setStatusbarColor(inicio_statusbar);
			}
		};
		setearStatusColor(inicio_statusbar);

	};
	var ID_1061396234 = setTimeout(ID_1061396234_func, 1000 * 0.1);
})();

function Androidback_inicio(e) {

	e.cancelBubble = true;
	var elemento = e.source;

}
if (OS_IOS || OS_ANDROID) {
	$.inicio.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}
//$.inicio.open();
