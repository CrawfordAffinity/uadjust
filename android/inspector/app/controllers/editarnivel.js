var _bind4section = {};
var _list_templates = {};
var $nivel = $.nivel.toJSON();

var _activity;
if (OS_ANDROID) {
	_activity = $.editarnivel.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'editarnivel';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.editarnivel.addEventListener('open', function(e) {});
}
$.editarnivel.orientationModes = [Titanium.UI.PORTRAIT];


$.widgetBarra3.init({
	titulo: L('x902724175_traducir', 'EDITAR NIVEL'),
	onsalirinsp: Salirinsp_widgetBarra3,
	__id: 'ALL136168330',
	textoderecha: L('x2943883035_traducir', 'Guardar'),
	salir_insp: L('', ''),
	fondo: 'fondoverde',
	top: 0,
	onpresiono: Presiono_widgetBarra3,
	colortextoderecha: 'blanco'
});

function Salirinsp_widgetBarra3(e) {

	var evento = e;
	/** 
	 * Limpiamos widget para liberar memoria ram 
	 */
	$.widgetModalmultiple8.limpiar({});
	$.widgetModalmultiple9.limpiar({});
	$.widgetModalmultiple10.limpiar({});
	$.widgetModalmultiple11.limpiar({});
	$.widgetModalmultiple12.limpiar({});
	$.widgetModalmultiple13.limpiar({});
	$.editarnivel.close();

}

function Presiono_widgetBarra3(e) {

	var evento = e;
	/** 
	 * Obtenemos el ano actual 
	 */
	var d = new Date();
	var anoactual = d.getFullYear();
	/** 
	 * Desenfocamos todos los campos de texto 
	 */
	$.EscribaNombre2.blur();
	$.EscribaNumero2.blur();
	$.IndiqueAo2.blur();
	var ID_1219764672_func = function() {
		if (_.isUndefined($nivel.nombre)) {
			/** 
			 * Verificamos que los campos ingresados esten correctos 
			 */
			var preguntarAlerta31_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta31 = Ti.UI.createAlertDialog({
				title: L('x3237162386_traducir', 'Atencion'),
				message: L('x1734272038_traducir', 'Ingrese nombre del nivel'),
				buttonNames: preguntarAlerta31_opts
			});
			preguntarAlerta31.addEventListener('click', function(e) {
				var nulo = preguntarAlerta31_opts[e.index];
				nulo = null;
				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta31.show();
		} else if ((_.isObject($nivel.nombre) || _.isString($nivel.nombre)) && _.isEmpty($nivel.nombre)) {
			var preguntarAlerta32_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta32 = Ti.UI.createAlertDialog({
				title: L('x3237162386_traducir', 'Atencion'),
				message: L('x1734272038_traducir', 'Ingrese nombre del nivel'),
				buttonNames: preguntarAlerta32_opts
			});
			preguntarAlerta32.addEventListener('click', function(e) {
				var nulo = preguntarAlerta32_opts[e.index];
				nulo = null;
				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta32.show();
		} else if (_.isUndefined($nivel.piso)) {
			var preguntarAlerta33_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta33 = Ti.UI.createAlertDialog({
				title: L('x3237162386_traducir', 'Atencion'),
				message: L('x2669747147_traducir', 'Ingrese Nº de piso'),
				buttonNames: preguntarAlerta33_opts
			});
			preguntarAlerta33.addEventListener('click', function(e) {
				var nulo = preguntarAlerta33_opts[e.index];
				nulo = null;
				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta33.show();
		} else if ($nivel.piso.length == 0 || $nivel.piso.length == '0') {
			var preguntarAlerta34_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta34 = Ti.UI.createAlertDialog({
				title: L('x3237162386_traducir', 'Atencion'),
				message: L('x2669747147_traducir', 'Ingrese Nº de piso'),
				buttonNames: preguntarAlerta34_opts
			});
			preguntarAlerta34.addEventListener('click', function(e) {
				var nulo = preguntarAlerta34_opts[e.index];
				nulo = null;
				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta34.show();
		} else if (_.isUndefined($nivel.ano)) {
			var preguntarAlerta35_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta35 = Ti.UI.createAlertDialog({
				title: L('x3237162386_traducir', 'Atencion'),
				message: L('x853726930_traducir', 'Ingrese año de construcción del nivel'),
				buttonNames: preguntarAlerta35_opts
			});
			preguntarAlerta35.addEventListener('click', function(e) {
				var nulo = preguntarAlerta35_opts[e.index];
				nulo = null;
				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta35.show();
		} else if ((_.isObject($nivel.ano) || _.isString($nivel.ano)) && _.isEmpty($nivel.ano)) {
			var preguntarAlerta36_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta36 = Ti.UI.createAlertDialog({
				title: L('x3237162386_traducir', 'Atencion'),
				message: L('x853726930_traducir', 'Ingrese año de construcción del nivel'),
				buttonNames: preguntarAlerta36_opts
			});
			preguntarAlerta36.addEventListener('click', function(e) {
				var nulo = preguntarAlerta36_opts[e.index];
				nulo = null;
				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta36.show();
		} else if ($nivel.ano < (anoactual - 100) == true || $nivel.ano < (anoactual - 100) == 'true') {
			if (Ti.App.deployType != 'production') console.log('ano mayor', {});
			require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
			var preguntarAlerta37_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta37 = Ti.UI.createAlertDialog({
				title: L('x3237162386_traducir', 'Atencion'),
				message: L('x624168147_traducir', 'Tiene que tener máximo 100 años de antigüedad'),
				buttonNames: preguntarAlerta37_opts
			});
			preguntarAlerta37.addEventListener('click', function(e) {
				var nulo = preguntarAlerta37_opts[e.index];
				nulo = null;
				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta37.show();
		} else if ($nivel.ano > anoactual == true || $nivel.ano > anoactual == 'true') {
			if (Ti.App.deployType != 'production') console.log('ano mayor', {});
			require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
			var preguntarAlerta38_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta38 = Ti.UI.createAlertDialog({
				title: L('x3237162386_traducir', 'Atencion'),
				message: L('x2118147629_traducir', 'El año tiene que ser menor al año actual'),
				buttonNames: preguntarAlerta38_opts
			});
			preguntarAlerta38.addEventListener('click', function(e) {
				var nulo = preguntarAlerta38_opts[e.index];
				nulo = null;
				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta38.show();
		} else if (_.isUndefined($nivel.largo)) {
			var preguntarAlerta39_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta39 = Ti.UI.createAlertDialog({
				title: L('x3237162386_traducir', 'Atencion'),
				message: L('x779559340_traducir', 'Ingrese largo del nivel'),
				buttonNames: preguntarAlerta39_opts
			});
			preguntarAlerta39.addEventListener('click', function(e) {
				var nulo = preguntarAlerta39_opts[e.index];
				nulo = null;
				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta39.show();
		} else if ((_.isObject($nivel.largo) || _.isString($nivel.largo)) && _.isEmpty($nivel.largo)) {
			var preguntarAlerta40_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta40 = Ti.UI.createAlertDialog({
				title: L('x3237162386_traducir', 'Atencion'),
				message: L('x779559340_traducir', 'Ingrese largo del nivel'),
				buttonNames: preguntarAlerta40_opts
			});
			preguntarAlerta40.addEventListener('click', function(e) {
				var nulo = preguntarAlerta40_opts[e.index];
				nulo = null;
				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta40.show();
		} else if (_.isUndefined($nivel.ancho)) {
			var preguntarAlerta41_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta41 = Ti.UI.createAlertDialog({
				title: L('x3237162386_traducir', 'Atencion'),
				message: L('x2682783608_traducir', 'Ingrese ancho del nivel'),
				buttonNames: preguntarAlerta41_opts
			});
			preguntarAlerta41.addEventListener('click', function(e) {
				var nulo = preguntarAlerta41_opts[e.index];
				nulo = null;
				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta41.show();
		} else if ((_.isObject($nivel.ancho) || _.isString($nivel.ancho)) && _.isEmpty($nivel.ancho)) {
			var preguntarAlerta42_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta42 = Ti.UI.createAlertDialog({
				title: L('x3237162386_traducir', 'Atencion'),
				message: L('x2682783608_traducir', 'Ingrese ancho del nivel'),
				buttonNames: preguntarAlerta42_opts
			});
			preguntarAlerta42.addEventListener('click', function(e) {
				var nulo = preguntarAlerta42_opts[e.index];
				nulo = null;
				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta42.show();
		} else if (_.isUndefined($nivel.alto)) {
			var preguntarAlerta43_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta43 = Ti.UI.createAlertDialog({
				title: L('x3237162386_traducir', 'Atencion'),
				message: L('x4120715490_traducir', 'Ingrese la altura del nivel'),
				buttonNames: preguntarAlerta43_opts
			});
			preguntarAlerta43.addEventListener('click', function(e) {
				var nulo = preguntarAlerta43_opts[e.index];
				nulo = null;
				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta43.show();
		} else if ((_.isObject($nivel.alto) || _.isString($nivel.alto)) && _.isEmpty($nivel.alto)) {
			var preguntarAlerta44_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta44 = Ti.UI.createAlertDialog({
				title: L('x3237162386_traducir', 'Atencion'),
				message: L('x4120715490_traducir', 'Ingrese la altura del nivel'),
				buttonNames: preguntarAlerta44_opts
			});
			preguntarAlerta44.addEventListener('click', function(e) {
				var nulo = preguntarAlerta44_opts[e.index];
				nulo = null;
				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta44.show();
		} else if (_.isUndefined($nivel.ids_estructuras_soportantes)) {
			var preguntarAlerta45_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta45 = Ti.UI.createAlertDialog({
				title: L('x3237162386_traducir', 'Atencion'),
				message: L('x3045996758_traducir', 'Seleccione la estructura soportante del nivel'),
				buttonNames: preguntarAlerta45_opts
			});
			preguntarAlerta45.addEventListener('click', function(e) {
				var nulo = preguntarAlerta45_opts[e.index];
				nulo = null;
				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta45.show();
		} else if ((_.isObject($nivel.ids_estructuras_soportantes) || _.isString($nivel.ids_estructuras_soportantes)) && _.isEmpty($nivel.ids_estructuras_soportantes)) {
			var preguntarAlerta46_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta46 = Ti.UI.createAlertDialog({
				title: L('x3237162386_traducir', 'Atencion'),
				message: L('x3045996758_traducir', 'Seleccione la estructura soportante del nivel'),
				buttonNames: preguntarAlerta46_opts
			});
			preguntarAlerta46.addEventListener('click', function(e) {
				var nulo = preguntarAlerta46_opts[e.index];
				nulo = null;
				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta46.show();
		} else {
			/** 
			 * Eliminamos el nivel que estamos editando de la tabla y reemplazamos por el que tenemos actualmente con datos editados 
			 */
			/** 
			 * Eliminamos el nivel que estamos editando de la tabla y reemplazamos por el que tenemos actualmente con datos editados 
			 */
			var eliminarModelo11_i = Alloy.Collections.insp_niveles;
			var sql = 'DELETE FROM ' + eliminarModelo11_i.config.adapter.collection_name + ' WHERE id=\'' + args._dato.id + '\'';
			var db = Ti.Database.open(eliminarModelo11_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo11_i.trigger('remove');
			Alloy.Collections[$.nivel.config.adapter.collection_name].add($.nivel);
			$.nivel.save();
			Alloy.Collections[$.nivel.config.adapter.collection_name].fetch();
			/** 
			 * Limpiamos widget para liberar memoria ram 
			 */
			$.widgetModalmultiple8.limpiar({});
			$.widgetModalmultiple9.limpiar({});
			$.widgetModalmultiple10.limpiar({});
			$.widgetModalmultiple11.limpiar({});
			$.widgetModalmultiple12.limpiar({});
			$.widgetModalmultiple13.limpiar({});
			$.editarnivel.close();
		}
	};
	var ID_1219764672 = setTimeout(ID_1219764672_func, 1000 * 0.3);

}

function Blur_EscribaNombre2(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	$.nivel.set({
		nombre: elemento
	});
	if ('nivel' in $) $nivel = $.nivel.toJSON();
	elemento = null, source = null;

}

function Blur_EscribaNumero2(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	/** 
	 * Actualizamos el modelo con el piso ingresado 
	 */
	$.nivel.set({
		piso: elemento
	});
	if ('nivel' in $) $nivel = $.nivel.toJSON();
	elemento = null, source = null;

}

function Return_EscribaNumero2(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	/** 
	 * Cuando el inspector presiona el enter en el teclado, desenfocamos el campo 
	 */
	$.EscribaNumero2.blur();
	elemento = null, source = null;

}

function Blur_IndiqueAo2(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	$.nivel.set({
		ano: elemento
	});
	if ('nivel' in $) $nivel = $.nivel.toJSON();
	elemento = null, source = null;

}

function Change_campo5(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	/** 
	 * Obtenemos el valor escrito en el campo de ancho 
	 */
	var ancho;
	ancho = $.campo6.getValue();

	if ((_.isObject(ancho) || (_.isString(ancho)) && !_.isEmpty(ancho)) || _.isNumber(ancho) || _.isBoolean(ancho)) {
		/** 
		 * Calculamos la superficie 
		 */
		if ((_.isObject(elemento) || (_.isString(elemento)) && !_.isEmpty(elemento)) || _.isNumber(elemento) || _.isBoolean(elemento)) {
			/** 
			 * nos aseguramos que ancho y valor de campo sean decimales con puntos (flotante), y no comas (espa&#241;ol) 
			 */
			var nuevo = parseFloat(ancho.split(',').join('.')) * parseFloat(elemento.split(',').join('.'));
			$.nivel.set({
				superficie: nuevo
			});
			if ('nivel' in $) $nivel = $.nivel.toJSON();
		}
	}
	/** 
	 * Actualizamos el valor del largo del recinto 
	 */
	$.nivel.set({
		largo: elemento
	});
	if ('nivel' in $) $nivel = $.nivel.toJSON();
	elemento = null, source = null;

}

function Change_campo6(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	var largo;
	largo = $.campo5.getValue();

	if ((_.isObject(largo) || (_.isString(largo)) && !_.isEmpty(largo)) || _.isNumber(largo) || _.isBoolean(largo)) {
		if ((_.isObject(elemento) || (_.isString(elemento)) && !_.isEmpty(elemento)) || _.isNumber(elemento) || _.isBoolean(elemento)) {
			/** 
			 * nos aseguramos que largo y valor de campo sean decimales con puntos (flotante), y no comas (espa&#241;ol) 
			 */
			var nuevo = parseFloat(largo.split(',').join('.')) * parseFloat(elemento.split(',').join('.'));
			$.nivel.set({
				superficie: nuevo
			});
			if ('nivel' in $) $nivel = $.nivel.toJSON();
		}
	}
	$.nivel.set({
		ancho: elemento
	});
	if ('nivel' in $) $nivel = $.nivel.toJSON();
	elemento = null, source = null;

}

function Change_campo7(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	$.nivel.set({
		alto: elemento
	});
	if ('nivel' in $) $nivel = $.nivel.toJSON();
	elemento = null, source = null;

}

$.widgetModalmultiple8.init({
	titulo: L('x1975271086_traducir', 'Estructura Soportante'),
	cargando: L('x1831148736', 'cargando...'),
	__id: 'ALL145212507',
	oncerrar: Cerrar_widgetModalmultiple8,
	left: 0,
	hint: L('x2898603391_traducir', 'Seleccione estructura'),
	color: 'verde',
	subtitulo: L('x4011106049_traducir', 'Indique los tipos'),
	right: 0,
	onclick: Click_widgetModalmultiple8,
	onafterinit: Afterinit_widgetModalmultiple8
});

function Click_widgetModalmultiple8(e) {

	var evento = e;

}

function Cerrar_widgetModalmultiple8(e) {

	var evento = e;
	$.widgetModalmultiple8.update({});
	$.nivel.set({
		ids_estructuras_soportantes: evento.valores
	});
	if ('nivel' in $) $nivel = $.nivel.toJSON();

}

function Afterinit_widgetModalmultiple8(e) {

	var evento = e;
	/** 
	 * ejecutamos desfasado para no congelar el hilo. 
	 */
	var ID_431750065_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			var consultarModelo13_i = Alloy.createCollection('insp_niveles');
			var consultarModelo13_i_where = 'id=\'' + args._dato.id + '\'';
			consultarModelo13_i.fetch({
				query: 'SELECT * FROM insp_niveles WHERE id=\'' + args._dato.id + '\''
			});
			var insp_datos = require('helper').query2array(consultarModelo13_i);
			if (insp_datos && insp_datos.length) {
				var consultarModelo14_i = Alloy.createCollection('estructura_soportante');
				var consultarModelo14_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
				consultarModelo14_i.fetch({
					query: 'SELECT * FROM estructura_soportante WHERE pais_texto=\'' + pais[0].nombre + '\''
				});
				var estructura = require('helper').query2array(consultarModelo14_i);
				var padre_index = 0;
				_.each(estructura, function(padre, padre_pos, padre_list) {
					padre_index += 1;
					if (_.isNull(insp_datos[0].ids_estructuras_soportantes)) {
						/** 
						 * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
						 */
						padre._estado = 0;
					} else if (insp_datos[0].ids_estructuras_soportantes.indexOf(padre.id_segured) != -1) {
						padre._estado = 1;
					} else {
						padre._estado = 0;
					}
				});
				var datos = [];
				_.each(estructura, function(fila, pos) {
					var new_row = {};
					_.each(fila, function(x, llave) {
						var newkey = '';
						if (llave == 'nombre') newkey = 'label';
						if (llave == 'id_segured') newkey = 'valor';
						if (llave == '_estado') newkey = 'estado';
						if (newkey != '') new_row[newkey] = fila[llave];
					});
					datos.push(new_row);
				});
			} else {
				var transformarModelo17_i = Alloy.createCollection('estructura_soportante');
				transformarModelo17_i.fetch();
				var transformarModelo17_src = require('helper').query2array(transformarModelo17_i);
				var datos = [];
				_.each(transformarModelo17_src, function(fila, pos) {
					var new_row = {};
					_.each(fila, function(x, llave) {
						var newkey = '';
						if (llave == 'nombre') newkey = 'label';
						if (llave == 'id_segured') newkey = 'valor';
						if (newkey != '') new_row[newkey] = fila[llave];
					});
					new_row['estado'] = 0;
					datos.push(new_row);
				});
			}
			$.widgetModalmultiple8.update({
				data: datos
			});
		} else {
			/** 
			 * cargamos datos dummy para cuando compilamos de forma individual. 
			 */
			/** 
			 * Insertamos dummies 
			 */
			/** 
			 * Insertamos dummies 
			 */
			var eliminarModelo12_i = Alloy.Collections.estructura_soportante;
			var sql = "DELETE FROM " + eliminarModelo12_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo12_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo12_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo11_m = Alloy.Collections.estructura_soportante;
				var insertarModelo11_fila = Alloy.createModel('estructura_soportante', {
					nombre: String.format(L('x1088195980_traducir', 'Estructura%1$s'), item.toString()),
					id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
					id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item.toString()),
					pais: 1
				});
				insertarModelo11_m.add(insertarModelo11_fila);
				insertarModelo11_fila.save();
			});
			var consultarModelo15_i = Alloy.createCollection('insp_niveles');
			var consultarModelo15_i_where = 'id=\'' + args._dato.id + '\'';
			consultarModelo15_i.fetch({
				query: 'SELECT * FROM insp_niveles WHERE id=\'' + args._dato.id + '\''
			});
			var insp_datos = require('helper').query2array(consultarModelo15_i);
			if (insp_datos && insp_datos.length) {
				/** 
				 * Obtenemos datos para selectores 
				 */
				var consultarModelo16_i = Alloy.createCollection('estructura_soportante');
				var consultarModelo16_i_where = '';
				consultarModelo16_i.fetch();
				var estructura = require('helper').query2array(consultarModelo16_i);
				var padre_index = 0;
				_.each(estructura, function(padre, padre_pos, padre_list) {
					padre_index += 1;
					if (_.isNull(insp_datos[0].ids_estructuras_soportantes)) {
						/** 
						 * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
						 */
						padre._estado = 0;
					} else if (insp_datos[0].ids_estructuras_soportantes.indexOf(padre.id_segured) != -1) {
						padre._estado = 1;
					} else {
						padre._estado = 0;
					}
				});
				var datos = [];
				_.each(estructura, function(fila, pos) {
					var new_row = {};
					_.each(fila, function(x, llave) {
						var newkey = '';
						if (llave == 'nombre') newkey = 'label';
						if (llave == 'id_segured') newkey = 'valor';
						if (llave == '_estado') newkey = 'estado';
						if (newkey != '') new_row[newkey] = fila[llave];
					});
					datos.push(new_row);
				});
			} else {
				var transformarModelo19_i = Alloy.createCollection('estructura_soportante');
				transformarModelo19_i.fetch();
				var transformarModelo19_src = require('helper').query2array(transformarModelo19_i);
				var datos = [];
				_.each(transformarModelo19_src, function(fila, pos) {
					var new_row = {};
					_.each(fila, function(x, llave) {
						var newkey = '';
						if (llave == 'nombre') newkey = 'label';
						if (llave == 'id_segured') newkey = 'valor';
						if (newkey != '') new_row[newkey] = fila[llave];
					});
					new_row['estado'] = 0;
					datos.push(new_row);
				});
			}
			$.widgetModalmultiple8.update({
				data: datos
			});
		}
	};
	var ID_431750065 = setTimeout(ID_431750065_func, 1000 * 0.2);

}

$.widgetModalmultiple9.init({
	titulo: L('x1219835481_traducir', 'Muros / Tabiques'),
	cargando: L('x1831148736', 'cargando...'),
	__id: 'ALL1401525657',
	oncerrar: Cerrar_widgetModalmultiple9,
	left: 0,
	hint: L('x2879998099_traducir', 'Seleccione muros'),
	color: 'verde',
	subtitulo: L('x4011106049_traducir', 'Indique los tipos'),
	right: 0,
	onclick: Click_widgetModalmultiple9,
	onafterinit: Afterinit_widgetModalmultiple9
});

function Click_widgetModalmultiple9(e) {

	var evento = e;

}

function Cerrar_widgetModalmultiple9(e) {

	var evento = e;
	$.widgetModalmultiple9.update({});
	$.nivel.set({
		ids_muros: evento.valores
	});
	if ('nivel' in $) $nivel = $.nivel.toJSON();

}

function Afterinit_widgetModalmultiple9(e) {

	var evento = e;
	/** 
	 * ejecutamos desfasado para no congelar el hilo. 
	 */
	var ID_1365479032_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			var consultarModelo17_i = Alloy.createCollection('insp_niveles');
			var consultarModelo17_i_where = 'id=\'' + args._dato.id + '\'';
			consultarModelo17_i.fetch({
				query: 'SELECT * FROM insp_niveles WHERE id=\'' + args._dato.id + '\''
			});
			var insp_datos = require('helper').query2array(consultarModelo17_i);
			if (insp_datos && insp_datos.length) {
				var consultarModelo18_i = Alloy.createCollection('muros_tabiques');
				var consultarModelo18_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
				consultarModelo18_i.fetch({
					query: 'SELECT * FROM muros_tabiques WHERE pais_texto=\'' + pais[0].nombre + '\''
				});
				var muros_tabiques = require('helper').query2array(consultarModelo18_i);
				var padre_index = 0;
				_.each(muros_tabiques, function(padre, padre_pos, padre_list) {
					padre_index += 1;
					if (_.isNull(insp_datos[0].ids_muros)) {
						/** 
						 * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
						 */
						padre._estado = 0;
					} else if (insp_datos[0].ids_muros.indexOf(padre.id_segured) != -1) {
						padre._estado = 1;
					} else {
						padre._estado = 0;
					}
				});
				var datos = [];
				_.each(muros_tabiques, function(fila, pos) {
					var new_row = {};
					_.each(fila, function(x, llave) {
						var newkey = '';
						if (llave == 'nombre') newkey = 'label';
						if (llave == 'id_segured') newkey = 'valor';
						if (llave == '_estado') newkey = 'estado';
						if (newkey != '') new_row[newkey] = fila[llave];
					});
					datos.push(new_row);
				});
			} else {
				var transformarModelo21_i = Alloy.createCollection('muros_tabiques');
				transformarModelo21_i.fetch();
				var transformarModelo21_src = require('helper').query2array(transformarModelo21_i);
				var datos = [];
				_.each(transformarModelo21_src, function(fila, pos) {
					var new_row = {};
					_.each(fila, function(x, llave) {
						var newkey = '';
						if (llave == 'nombre') newkey = 'label';
						if (llave == 'id_segured') newkey = 'valor';
						if (newkey != '') new_row[newkey] = fila[llave];
					});
					new_row['estado'] = 0;
					datos.push(new_row);
				});
			}
			$.widgetModalmultiple9.update({
				data: datos
			});
		} else {
			/** 
			 * cargamos datos dummy para cuando compilamos de forma individual. 
			 */
			var eliminarModelo13_i = Alloy.Collections.muros_tabiques;
			var sql = "DELETE FROM " + eliminarModelo13_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo13_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo13_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo12_m = Alloy.Collections.muros_tabiques;
				var insertarModelo12_fila = Alloy.createModel('muros_tabiques', {
					nombre: String.format(L('x2354211911_traducir', 'Muros tabiques %1$s'), item.toString()),
					id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
					id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item.toString()),
					pais: 1
				});
				insertarModelo12_m.add(insertarModelo12_fila);
				insertarModelo12_fila.save();
			});
			var consultarModelo19_i = Alloy.createCollection('insp_niveles');
			var consultarModelo19_i_where = 'id=\'' + args._dato.id + '\'';
			consultarModelo19_i.fetch({
				query: 'SELECT * FROM insp_niveles WHERE id=\'' + args._dato.id + '\''
			});
			var insp_datos = require('helper').query2array(consultarModelo19_i);
			if (insp_datos && insp_datos.length) {
				var consultarModelo20_i = Alloy.createCollection('muros_tabiques');
				var consultarModelo20_i_where = '';
				consultarModelo20_i.fetch();
				var muros_tabiques = require('helper').query2array(consultarModelo20_i);
				var padre_index = 0;
				_.each(muros_tabiques, function(padre, padre_pos, padre_list) {
					padre_index += 1;
					if (_.isNull(insp_datos[0].ids_muros)) {
						/** 
						 * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
						 */
						padre._estado = 0;
					} else if (insp_datos[0].ids_muros.indexOf(padre.id_segured) != -1) {
						padre._estado = 1;
					} else {
						padre._estado = 0;
					}
				});
				var datos = [];
				_.each(muros_tabiques, function(fila, pos) {
					var new_row = {};
					_.each(fila, function(x, llave) {
						var newkey = '';
						if (llave == 'nombre') newkey = 'label';
						if (llave == 'id_segured') newkey = 'valor';
						if (llave == '_estado') newkey = 'estado';
						if (newkey != '') new_row[newkey] = fila[llave];
					});
					datos.push(new_row);
				});
			} else {
				var transformarModelo23_i = Alloy.createCollection('muros_tabiques');
				transformarModelo23_i.fetch();
				var transformarModelo23_src = require('helper').query2array(transformarModelo23_i);
				var datos = [];
				_.each(transformarModelo23_src, function(fila, pos) {
					var new_row = {};
					_.each(fila, function(x, llave) {
						var newkey = '';
						if (llave == 'nombre') newkey = 'label';
						if (llave == 'id_segured') newkey = 'valor';
						if (newkey != '') new_row[newkey] = fila[llave];
					});
					new_row['estado'] = 0;
					datos.push(new_row);
				});
			}
			$.widgetModalmultiple9.update({
				data: datos
			});
		}
	};
	var ID_1365479032 = setTimeout(ID_1365479032_func, 1000 * 0.2);

}

$.widgetModalmultiple10.init({
	titulo: L('x3327059844_traducir', 'Entrepisos'),
	cargando: L('x1831148736', 'cargando...'),
	__id: 'ALL937114202',
	oncerrar: Cerrar_widgetModalmultiple10,
	left: 0,
	hint: L('x2146928948_traducir', 'Seleccione entrepisos'),
	color: 'verde',
	subtitulo: L('x4011106049_traducir', 'Indique los tipos'),
	right: 0,
	onclick: Click_widgetModalmultiple10,
	onafterinit: Afterinit_widgetModalmultiple10
});

function Click_widgetModalmultiple10(e) {

	var evento = e;

}

function Cerrar_widgetModalmultiple10(e) {

	var evento = e;
	$.widgetModalmultiple10.update({});
	$.nivel.set({
		ids_entrepisos: evento.valores
	});
	if ('nivel' in $) $nivel = $.nivel.toJSON();

}

function Afterinit_widgetModalmultiple10(e) {

	var evento = e;
	/** 
	 * ejecutamos desfasado para no congelar el hilo. 
	 */
	var ID_909342769_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			var consultarModelo21_i = Alloy.createCollection('insp_niveles');
			var consultarModelo21_i_where = 'id=\'' + args._dato.id + '\'';
			consultarModelo21_i.fetch({
				query: 'SELECT * FROM insp_niveles WHERE id=\'' + args._dato.id + '\''
			});
			var insp_datos = require('helper').query2array(consultarModelo21_i);
			if (insp_datos && insp_datos.length) {
				var consultarModelo22_i = Alloy.createCollection('entrepisos');
				var consultarModelo22_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
				consultarModelo22_i.fetch({
					query: 'SELECT * FROM entrepisos WHERE pais_texto=\'' + pais[0].nombre + '\''
				});
				var entrepisos = require('helper').query2array(consultarModelo22_i);
				var padre_index = 0;
				_.each(entrepisos, function(padre, padre_pos, padre_list) {
					padre_index += 1;
					if (_.isNull(insp_datos[0].ids_entrepisos)) {
						/** 
						 * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
						 */
						padre._estado = 0;
					} else if (insp_datos[0].ids_entrepisos.indexOf(padre.id_segured) != -1) {
						padre._estado = 1;
					} else {
						padre._estado = 0;
					}
				});
				var datos = [];
				_.each(entrepisos, function(fila, pos) {
					var new_row = {};
					_.each(fila, function(x, llave) {
						var newkey = '';
						if (llave == 'nombre') newkey = 'label';
						if (llave == 'id_segured') newkey = 'valor';
						if (llave == '_estado') newkey = 'estado';
						if (newkey != '') new_row[newkey] = fila[llave];
					});
					datos.push(new_row);
				});
			} else {
				var transformarModelo25_i = Alloy.createCollection('entrepisos');
				transformarModelo25_i.fetch();
				var transformarModelo25_src = require('helper').query2array(transformarModelo25_i);
				var datos = [];
				_.each(transformarModelo25_src, function(fila, pos) {
					var new_row = {};
					_.each(fila, function(x, llave) {
						var newkey = '';
						if (llave == 'nombre') newkey = 'label';
						if (llave == 'id_segured') newkey = 'valor';
						if (newkey != '') new_row[newkey] = fila[llave];
					});
					new_row['estado'] = 0;
					datos.push(new_row);
				});
			}
			$.widgetModalmultiple10.update({
				data: datos
			});
		} else {
			/** 
			 * cargamos datos dummy para cuando compilamos de forma individual. 
			 */
			var eliminarModelo14_i = Alloy.Collections.entrepisos;
			var sql = "DELETE FROM " + eliminarModelo14_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo14_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo14_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo13_m = Alloy.Collections.entrepisos;
				var insertarModelo13_fila = Alloy.createModel('entrepisos', {
					nombre: String.format(L('x2853615613_traducir', 'Entrepiso %1$s'), item.toString()),
					id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
					id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item.toString()),
					pais: 1
				});
				insertarModelo13_m.add(insertarModelo13_fila);
				insertarModelo13_fila.save();
			});
			var consultarModelo23_i = Alloy.createCollection('insp_niveles');
			var consultarModelo23_i_where = 'id=\'' + args._dato.id + '\'';
			consultarModelo23_i.fetch({
				query: 'SELECT * FROM insp_niveles WHERE id=\'' + args._dato.id + '\''
			});
			var insp_datos = require('helper').query2array(consultarModelo23_i);
			if (insp_datos && insp_datos.length) {
				var consultarModelo24_i = Alloy.createCollection('entrepisos');
				var consultarModelo24_i_where = '';
				consultarModelo24_i.fetch();
				var entrepisos = require('helper').query2array(consultarModelo24_i);
				var padre_index = 0;
				_.each(entrepisos, function(padre, padre_pos, padre_list) {
					padre_index += 1;
					if (_.isNull(insp_datos[0].ids_entrepisos)) {
						/** 
						 * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
						 */
						padre._estado = 0;
					} else if (insp_datos[0].ids_entrepisos.indexOf(padre.id_segured) != -1) {
						padre._estado = 1;
					} else {
						padre._estado = 0;
					}
				});
				var datos = [];
				_.each(entrepisos, function(fila, pos) {
					var new_row = {};
					_.each(fila, function(x, llave) {
						var newkey = '';
						if (llave == 'nombre') newkey = 'label';
						if (llave == 'id_segured') newkey = 'valor';
						if (llave == '_estado') newkey = 'estado';
						if (newkey != '') new_row[newkey] = fila[llave];
					});
					datos.push(new_row);
				});
			} else {
				var transformarModelo27_i = Alloy.createCollection('entrepisos');
				transformarModelo27_i.fetch();
				var transformarModelo27_src = require('helper').query2array(transformarModelo27_i);
				var datos = [];
				_.each(transformarModelo27_src, function(fila, pos) {
					var new_row = {};
					_.each(fila, function(x, llave) {
						var newkey = '';
						if (llave == 'nombre') newkey = 'label';
						if (llave == 'id_segured') newkey = 'valor';
						if (newkey != '') new_row[newkey] = fila[llave];
					});
					new_row['estado'] = 0;
					datos.push(new_row);
				});
			}
			$.widgetModalmultiple10.update({
				data: datos
			});
		}
	};
	var ID_909342769 = setTimeout(ID_909342769_func, 1000 * 0.2);

}

$.widgetModalmultiple11.init({
	titulo: L('x591862035_traducir', 'Pavimentos'),
	cargando: L('x1831148736', 'cargando...'),
	__id: 'ALL1489080142',
	oncerrar: Cerrar_widgetModalmultiple11,
	left: 0,
	hint: L('x2600368035_traducir', 'Seleccione pavimentos'),
	color: 'verde',
	subtitulo: L('x4011106049_traducir', 'Indique los tipos'),
	right: 0,
	onclick: Click_widgetModalmultiple11,
	onafterinit: Afterinit_widgetModalmultiple11
});

function Click_widgetModalmultiple11(e) {

	var evento = e;

}

function Cerrar_widgetModalmultiple11(e) {

	var evento = e;
	$.widgetModalmultiple11.update({});
	$.nivel.set({
		ids_pavimentos: evento.valores
	});
	if ('nivel' in $) $nivel = $.nivel.toJSON();

}

function Afterinit_widgetModalmultiple11(e) {

	var evento = e;
	/** 
	 * ejecutamos desfasado para no congelar el hilo. 
	 */
	var ID_941907377_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			var consultarModelo25_i = Alloy.createCollection('insp_niveles');
			var consultarModelo25_i_where = 'id=\'' + args._dato.id + '\'';
			consultarModelo25_i.fetch({
				query: 'SELECT * FROM insp_niveles WHERE id=\'' + args._dato.id + '\''
			});
			var insp_datos = require('helper').query2array(consultarModelo25_i);
			if (insp_datos && insp_datos.length) {
				var consultarModelo26_i = Alloy.createCollection('pavimento');
				var consultarModelo26_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
				consultarModelo26_i.fetch({
					query: 'SELECT * FROM pavimento WHERE pais_texto=\'' + pais[0].nombre + '\''
				});
				var pavimento = require('helper').query2array(consultarModelo26_i);
				var padre_index = 0;
				_.each(pavimento, function(padre, padre_pos, padre_list) {
					padre_index += 1;
					if (_.isNull(insp_datos[0].ids_pavimentos)) {
						/** 
						 * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
						 */
						padre._estado = 0;
					} else if (insp_datos[0].ids_pavimentos.indexOf(padre.id_segured) != -1) {
						padre._estado = 1;
					} else {
						padre._estado = 0;
					}
				});
				var datos = [];
				_.each(pavimento, function(fila, pos) {
					var new_row = {};
					_.each(fila, function(x, llave) {
						var newkey = '';
						if (llave == 'nombre') newkey = 'label';
						if (llave == 'id_segured') newkey = 'valor';
						if (llave == '_estado') newkey = 'estado';
						if (newkey != '') new_row[newkey] = fila[llave];
					});
					datos.push(new_row);
				});
			} else {
				var transformarModelo29_i = Alloy.createCollection('pavimento');
				transformarModelo29_i.fetch();
				var transformarModelo29_src = require('helper').query2array(transformarModelo29_i);
				var datos = [];
				_.each(transformarModelo29_src, function(fila, pos) {
					var new_row = {};
					_.each(fila, function(x, llave) {
						var newkey = '';
						if (llave == 'nombre') newkey = 'label';
						if (llave == 'id_segured') newkey = 'valor';
						if (newkey != '') new_row[newkey] = fila[llave];
					});
					new_row['estado'] = 0;
					datos.push(new_row);
				});
			}
			$.widgetModalmultiple11.update({
				data: datos
			});
		} else {
			/** 
			 * cargamos datos dummy para cuando compilamos de forma individual. 
			 */
			var eliminarModelo15_i = Alloy.Collections.pavimento;
			var sql = "DELETE FROM " + eliminarModelo15_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo15_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo15_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo14_m = Alloy.Collections.pavimento;
				var insertarModelo14_fila = Alloy.createModel('pavimento', {
					nombre: String.format(L('x2203128143_traducir', 'Pavimento %1$s'), item.toString()),
					id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
					id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item.toString()),
					pais: 1
				});
				insertarModelo14_m.add(insertarModelo14_fila);
				insertarModelo14_fila.save();
			});
			var consultarModelo27_i = Alloy.createCollection('insp_niveles');
			var consultarModelo27_i_where = 'id=\'' + args._dato.id + '\'';
			consultarModelo27_i.fetch({
				query: 'SELECT * FROM insp_niveles WHERE id=\'' + args._dato.id + '\''
			});
			var insp_datos = require('helper').query2array(consultarModelo27_i);
			if (insp_datos && insp_datos.length) {
				var consultarModelo28_i = Alloy.createCollection('pavimento');
				var consultarModelo28_i_where = '';
				consultarModelo28_i.fetch();
				var pavimento = require('helper').query2array(consultarModelo28_i);
				var padre_index = 0;
				_.each(pavimento, function(padre, padre_pos, padre_list) {
					padre_index += 1;
					if (_.isNull(insp_datos[0].ids_pavimentos)) {
						/** 
						 * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
						 */
						padre._estado = 0;
					} else if (insp_datos[0].ids_pavimentos.indexOf(padre.id_segured) != -1) {
						padre._estado = 1;
					} else {
						padre._estado = 0;
					}
				});
				var datos = [];
				_.each(pavimento, function(fila, pos) {
					var new_row = {};
					_.each(fila, function(x, llave) {
						var newkey = '';
						if (llave == 'nombre') newkey = 'label';
						if (llave == 'id_segured') newkey = 'valor';
						if (llave == '_estado') newkey = 'estado';
						if (newkey != '') new_row[newkey] = fila[llave];
					});
					datos.push(new_row);
				});
			} else {
				var transformarModelo31_i = Alloy.createCollection('pavimento');
				transformarModelo31_i.fetch();
				var transformarModelo31_src = require('helper').query2array(transformarModelo31_i);
				var datos = [];
				_.each(transformarModelo31_src, function(fila, pos) {
					var new_row = {};
					_.each(fila, function(x, llave) {
						var newkey = '';
						if (llave == 'nombre') newkey = 'label';
						if (llave == 'id_segured') newkey = 'valor';
						if (newkey != '') new_row[newkey] = fila[llave];
					});
					new_row['estado'] = 0;
					datos.push(new_row);
				});
			}
			$.widgetModalmultiple11.update({
				data: datos
			});
		}
	};
	var ID_941907377 = setTimeout(ID_941907377_func, 1000 * 0.2);

}

$.widgetModalmultiple12.init({
	titulo: L('x1866523485_traducir', 'Estruct. cubierta'),
	cargando: L('x1831148736', 'cargando...'),
	__id: 'ALL129921645',
	oncerrar: Cerrar_widgetModalmultiple12,
	left: 0,
	hint: L('x2460890829_traducir', 'Seleccione e.cubiertas'),
	color: 'verde',
	subtitulo: L('x4011106049_traducir', 'Indique los tipos'),
	right: 0,
	onclick: Click_widgetModalmultiple12,
	onafterinit: Afterinit_widgetModalmultiple12
});

function Click_widgetModalmultiple12(e) {

	var evento = e;

}

function Cerrar_widgetModalmultiple12(e) {

	var evento = e;
	$.widgetModalmultiple12.update({});
	$.nivel.set({
		ids_estructura_cubiera: evento.valores
	});
	if ('nivel' in $) $nivel = $.nivel.toJSON();

}

function Afterinit_widgetModalmultiple12(e) {

	var evento = e;
	/** 
	 * ejecutamos desfasado para no congelar el hilo. 
	 */
	var ID_1394456967_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			var consultarModelo29_i = Alloy.createCollection('insp_niveles');
			var consultarModelo29_i_where = 'id=\'' + args._dato.id + '\'';
			consultarModelo29_i.fetch({
				query: 'SELECT * FROM insp_niveles WHERE id=\'' + args._dato.id + '\''
			});
			var insp_datos = require('helper').query2array(consultarModelo29_i);
			if (insp_datos && insp_datos.length) {
				var consultarModelo30_i = Alloy.createCollection('estructura_cubierta');
				var consultarModelo30_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
				consultarModelo30_i.fetch({
					query: 'SELECT * FROM estructura_cubierta WHERE pais_texto=\'' + pais[0].nombre + '\''
				});
				var estructura_cubierta = require('helper').query2array(consultarModelo30_i);
				var padre_index = 0;
				_.each(estructura_cubierta, function(padre, padre_pos, padre_list) {
					padre_index += 1;
					if (_.isNull(insp_datos[0].ids_estructura_cubiera)) {
						/** 
						 * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
						 */
						padre._estado = 0;
					} else if (insp_datos[0].ids_estructura_cubiera.indexOf(padre.id_segured) != -1) {
						padre._estado = 1;
					} else {
						padre._estado = 0;
					}
				});
				var datos = [];
				_.each(estructura_cubierta, function(fila, pos) {
					var new_row = {};
					_.each(fila, function(x, llave) {
						var newkey = '';
						if (llave == 'nombre') newkey = 'label';
						if (llave == 'id_segured') newkey = 'valor';
						if (llave == '_estado') newkey = 'estado';
						if (newkey != '') new_row[newkey] = fila[llave];
					});
					datos.push(new_row);
				});
			} else {
				var transformarModelo33_i = Alloy.createCollection('estructura_cubierta');
				transformarModelo33_i.fetch();
				var transformarModelo33_src = require('helper').query2array(transformarModelo33_i);
				var datos = [];
				_.each(transformarModelo33_src, function(fila, pos) {
					var new_row = {};
					_.each(fila, function(x, llave) {
						var newkey = '';
						if (llave == 'nombre') newkey = 'label';
						if (llave == 'id_segured') newkey = 'valor';
						if (newkey != '') new_row[newkey] = fila[llave];
					});
					new_row['estado'] = 0;
					datos.push(new_row);
				});
			}
			$.widgetModalmultiple12.update({
				data: datos
			});
		} else {
			/** 
			 * cargamos datos dummy para cuando compilamos de forma individual. 
			 */
			var eliminarModelo16_i = Alloy.Collections.estructura_cubierta;
			var sql = "DELETE FROM " + eliminarModelo16_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo16_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo16_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo15_m = Alloy.Collections.estructura_cubierta;
				var insertarModelo15_fila = Alloy.createModel('estructura_cubierta', {
					nombre: String.format(L('x2607483718_traducir', 'Estructura cubierta %1$s'), item.toString()),
					id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
					id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item.toString()),
					pais: 1
				});
				insertarModelo15_m.add(insertarModelo15_fila);
				insertarModelo15_fila.save();
			});
			var consultarModelo31_i = Alloy.createCollection('insp_niveles');
			var consultarModelo31_i_where = 'id=\'' + args._dato.id + '\'';
			consultarModelo31_i.fetch({
				query: 'SELECT * FROM insp_niveles WHERE id=\'' + args._dato.id + '\''
			});
			var insp_datos = require('helper').query2array(consultarModelo31_i);
			if (insp_datos && insp_datos.length) {
				var consultarModelo32_i = Alloy.createCollection('estructura_cubierta');
				var consultarModelo32_i_where = '';
				consultarModelo32_i.fetch();
				var estructura_cubierta = require('helper').query2array(consultarModelo32_i);
				if (Ti.App.deployType != 'production') console.log('log pio', {
					"insp": insp_datos,
					"estruc": estructura_cubierta[0]
				});
				var padre_index = 0;
				_.each(estructura_cubierta, function(padre, padre_pos, padre_list) {
					padre_index += 1;
					if (_.isNull(insp_datos[0].ids_estructura_cubiera)) {
						/** 
						 * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
						 */
						padre._estado = 0;
					} else if (insp_datos[0].ids_estructura_cubiera.indexOf(padre.id_segured) != -1) {
						padre._estado = 1;
					} else {
						padre._estado = 0;
					}
				});
				var datos = [];
				_.each(estructura_cubierta, function(fila, pos) {
					var new_row = {};
					_.each(fila, function(x, llave) {
						var newkey = '';
						if (llave == 'nombre') newkey = 'label';
						if (llave == 'id_segured') newkey = 'valor';
						if (llave == '_estado') newkey = 'estado';
						if (newkey != '') new_row[newkey] = fila[llave];
					});
					datos.push(new_row);
				});
			} else {
				var transformarModelo35_i = Alloy.createCollection('estructura_cubierta');
				transformarModelo35_i.fetch();
				var transformarModelo35_src = require('helper').query2array(transformarModelo35_i);
				var datos = [];
				_.each(transformarModelo35_src, function(fila, pos) {
					var new_row = {};
					_.each(fila, function(x, llave) {
						var newkey = '';
						if (llave == 'nombre') newkey = 'label';
						if (llave == 'id_segured') newkey = 'valor';
						if (newkey != '') new_row[newkey] = fila[llave];
					});
					new_row['estado'] = 0;
					datos.push(new_row);
				});
			}
			$.widgetModalmultiple12.update({
				data: datos
			});
		}
	};
	var ID_1394456967 = setTimeout(ID_1394456967_func, 1000 * 0.2);

}

$.widgetModalmultiple13.init({
	titulo: L('x2266302645_traducir', 'Cubierta'),
	cargando: L('x1831148736', 'cargando...'),
	__id: 'ALL296719183',
	oncerrar: Cerrar_widgetModalmultiple13,
	left: 0,
	hint: L('x2134385782_traducir', 'Seleccione cubiertas'),
	color: 'verde',
	subtitulo: L('x4011106049_traducir', 'Indique los tipos'),
	right: 0,
	onclick: Click_widgetModalmultiple13,
	onafterinit: Afterinit_widgetModalmultiple13
});

function Click_widgetModalmultiple13(e) {

	var evento = e;

}

function Cerrar_widgetModalmultiple13(e) {

	var evento = e;
	$.widgetModalmultiple13.update({});
	$.nivel.set({
		ids_cubierta: evento.valores
	});
	if ('nivel' in $) $nivel = $.nivel.toJSON();

}

function Afterinit_widgetModalmultiple13(e) {

	var evento = e;
	/** 
	 * ejecutamos desfasado para no congelar el hilo. 
	 */
	var ID_453858558_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			var consultarModelo33_i = Alloy.createCollection('insp_niveles');
			var consultarModelo33_i_where = 'id=\'' + args._dato.id + '\'';
			consultarModelo33_i.fetch({
				query: 'SELECT * FROM insp_niveles WHERE id=\'' + args._dato.id + '\''
			});
			var insp_datos = require('helper').query2array(consultarModelo33_i);
			if (insp_datos && insp_datos.length) {
				var consultarModelo34_i = Alloy.createCollection('cubierta');
				var consultarModelo34_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
				consultarModelo34_i.fetch({
					query: 'SELECT * FROM cubierta WHERE pais_texto=\'' + pais[0].nombre + '\''
				});
				var cubierta = require('helper').query2array(consultarModelo34_i);
				var padre_index = 0;
				_.each(cubierta, function(padre, padre_pos, padre_list) {
					padre_index += 1;
					if (_.isNull(insp_datos[0].ids_cubierta)) {
						/** 
						 * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
						 */
						padre._estado = 0;
					} else if (insp_datos[0].ids_cubierta.indexOf(padre.id_segured) != -1) {
						padre._estado = 1;
					} else {
						padre._estado = 0;
					}
				});
				var datos = [];
				_.each(cubierta, function(fila, pos) {
					var new_row = {};
					_.each(fila, function(x, llave) {
						var newkey = '';
						if (llave == 'nombre') newkey = 'label';
						if (llave == 'id_segured') newkey = 'valor';
						if (llave == '_estado') newkey = 'estado';
						if (newkey != '') new_row[newkey] = fila[llave];
					});
					datos.push(new_row);
				});
			} else {
				var transformarModelo37_i = Alloy.createCollection('cubierta');
				transformarModelo37_i.fetch();
				var transformarModelo37_src = require('helper').query2array(transformarModelo37_i);
				var datos = [];
				_.each(transformarModelo37_src, function(fila, pos) {
					var new_row = {};
					_.each(fila, function(x, llave) {
						var newkey = '';
						if (llave == 'nombre') newkey = 'label';
						if (llave == 'id_segured') newkey = 'valor';
						if (newkey != '') new_row[newkey] = fila[llave];
					});
					new_row['estado'] = 0;
					datos.push(new_row);
				});
			}
			$.widgetModalmultiple13.update({
				data: datos
			});
		} else {
			/** 
			 * cargamos datos dummy para cuando compilamos de forma individual. 
			 */
			var eliminarModelo17_i = Alloy.Collections.cubierta;
			var sql = "DELETE FROM " + eliminarModelo17_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo17_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo17_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo16_m = Alloy.Collections.cubierta;
				var insertarModelo16_fila = Alloy.createModel('cubierta', {
					nombre: String.format(L('x2246230604_traducir', 'Cubierta %1$s'), item.toString()),
					id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
					id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item.toString()),
					pais: 1
				});
				insertarModelo16_m.add(insertarModelo16_fila);
				insertarModelo16_fila.save();
			});
			var consultarModelo35_i = Alloy.createCollection('insp_niveles');
			var consultarModelo35_i_where = 'id=\'' + args._dato.id + '\'';
			consultarModelo35_i.fetch({
				query: 'SELECT * FROM insp_niveles WHERE id=\'' + args._dato.id + '\''
			});
			var insp_datos = require('helper').query2array(consultarModelo35_i);
			if (insp_datos && insp_datos.length) {
				var consultarModelo36_i = Alloy.createCollection('cubierta');
				var consultarModelo36_i_where = '';
				consultarModelo36_i.fetch();
				var cubierta = require('helper').query2array(consultarModelo36_i);
				var padre_index = 0;
				_.each(cubierta, function(padre, padre_pos, padre_list) {
					padre_index += 1;
					if (_.isNull(insp_datos[0].ids_cubierta)) {
						/** 
						 * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
						 */
						padre._estado = 0;
					} else if (insp_datos[0].ids_cubierta.indexOf(padre.id_segured) != -1) {
						padre._estado = 1;
					} else {
						padre._estado = 0;
					}
				});
				var datos = [];
				_.each(cubierta, function(fila, pos) {
					var new_row = {};
					_.each(fila, function(x, llave) {
						var newkey = '';
						if (llave == 'nombre') newkey = 'label';
						if (llave == 'id_segured') newkey = 'valor';
						if (llave == '_estado') newkey = 'estado';
						if (newkey != '') new_row[newkey] = fila[llave];
					});
					datos.push(new_row);
				});
			} else {
				var transformarModelo39_i = Alloy.createCollection('cubierta');
				transformarModelo39_i.fetch();
				var transformarModelo39_src = require('helper').query2array(transformarModelo39_i);
				var datos = [];
				_.each(transformarModelo39_src, function(fila, pos) {
					var new_row = {};
					_.each(fila, function(x, llave) {
						var newkey = '';
						if (llave == 'nombre') newkey = 'label';
						if (llave == 'id_segured') newkey = 'valor';
						if (newkey != '') new_row[newkey] = fila[llave];
					});
					new_row['estado'] = 0;
					datos.push(new_row);
				});
			}
			$.widgetModalmultiple13.update({
				data: datos
			});
		}
	};
	var ID_453858558 = setTimeout(ID_453858558_func, 1000 * 0.2);

}

(function() {
	/** 
	 * heredamos datos previos de nivel en modelo nuevo con binding de esta pantalla 
	 */
	var consultarModelo37_i = Alloy.createCollection('insp_niveles');
	var consultarModelo37_i_where = 'id=\'' + args._dato.id + '\'';
	consultarModelo37_i.fetch({
		query: 'SELECT * FROM insp_niveles WHERE id=\'' + args._dato.id + '\''
	});
	var insp_n = require('helper').query2array(consultarModelo37_i);
	/** 
	 * Cargamos los datos en la tabla 
	 */
	$.nivel.set({
		ids_entrepisos: insp_n[0].ids_entrepisos,
		id_inspeccion: insp_n[0].id_inspeccion,
		nombre: insp_n[0].nombre,
		superficie: insp_n[0].superficie,
		largo: insp_n[0].largo,
		ids_pavimentos: insp_n[0].ids_pavimentos,
		alto: insp_n[0].alto,
		ano: insp_n[0].ano,
		ids_muros: insp_n[0].ids_muros,
		ancho: insp_n[0].ancho,
		ids_estructura_cubiera: insp_n[0].ids_estructura_cubiera,
		piso: insp_n[0].piso,
		ids_cubierta: insp_n[0].ids_cubierta,
		ids_estructuras_soportantes: insp_n[0].ids_estructuras_soportantes
	});
	if ('nivel' in $) $nivel = $.nivel.toJSON();
	/** 
	 * Cargamos textos en la pantalla 
	 */
	$.EscribaNombre2.setValue(insp_n[0].nombre);

	$.EscribaNumero2.setValue(insp_n[0].piso);

	$.IndiqueAo2.setValue(insp_n[0].ano);

	$.campo5.setValue(insp_n[0].largo);

	$.campo6.setValue(insp_n[0].ancho);

	$.campo7.setValue(insp_n[0].alto);

	/** 
	 * Dejamos el scroll en el top y desenfocamos los campos de texto 
	 */
	var ID_1200217363_func = function() {
		$.EscribaNombre2.blur();
		$.EscribaNumero2.blur();
		$.IndiqueAo2.blur();
		$.campo5.blur();
		$.campo6.blur();
		$.campo7.blur();
		$.scroll3.scrollToTop();
	};
	var ID_1200217363 = setTimeout(ID_1200217363_func, 1000 * 0.2);
	/** 
	 * Mostramos statusbar 
	 */
	var ID_463357042_func = function() {
		var editarnivel_statusbar = '#57BC8B';

		var setearStatusColor = function(editarnivel_statusbar) {
			if (OS_IOS) {
				if (editarnivel_statusbar == 'light' || editarnivel_statusbar == 'claro') {
					$.editarnivel_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.LIGHT_CONTENT);
				} else if (editarnivel_statusbar == 'grey' || editarnivel_statusbar == 'gris' || editarnivel_statusbar == 'gray') {
					$.editarnivel_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.GREY);
				} else if (editarnivel_statusbar == 'oscuro' || editarnivel_statusbar == 'dark') {
					$.editarnivel_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.DEFAULT);
				}
			} else if (OS_ANDROID) {
				abx.setStatusbarColor(editarnivel_statusbar);
			}
		};
		setearStatusColor(editarnivel_statusbar);

	};
	var ID_463357042 = setTimeout(ID_463357042_func, 1000 * 0.1);
})();

function Postlayout_editarnivel(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var ID_1688288258_func = function() {
		/** 
		 * Una vez ya cargada la tarea, dejamos la variable limpia para poder abrir otras pantallas 
		 */
		require('vars')['var_abriendo'] = '';
	};
	var ID_1688288258 = setTimeout(ID_1688288258_func, 1000 * 0.2);

}
if (OS_IOS || OS_ANDROID) {
	$.editarnivel.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}