var _bind4section = {};
var _list_templates = {
	"elemento": {
		"vista2": {},
		"Label3": {
			"text": "{nombre}"
		},
		"Label2": {
			"text": "{id_server}"
		},
		"Label": {
			"text": "{id_segured}"
		},
		"vista": {}
	},
	"dano": {
		"Label2": {
			"text": "{id}"
		},
		"vista29": {},
		"Label": {
			"text": "{nombre}"
		}
	},
	"pborrar": {
		"vista5": {},
		"imagen": {},
		"vista3": {},
		"Label4": {
			"text": "{id}"
		},
		"Label3": {
			"text": "{nombre}"
		},
		"vista4": {},
		"vista22": {},
		"vista23": {},
		"vista24": {},
		"imagen2": {}
	},
	"contenido": {
		"vista2": {},
		"Label2": {
			"text": "{id}"
		},
		"Label": {
			"text": "{nombre}"
		}
	},
	"recinto": {
		"vista2": {},
		"Label": {
			"text": "{nombre}"
		},
		"Label2": {
			"text": "{id}"
		}
	},
	"nivel": {
		"Label2": {
			"text": "{id}"
		},
		"Label": {
			"text": "{nombre}"
		},
		"vista21": {}
	}
};
var $datos = $.datos.toJSON();

var _activity;
if (OS_ANDROID) {
	_activity = $.DATOS_BASICOS.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'DATOS_BASICOS';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.DATOS_BASICOS.addEventListener('open', function(e) {});
}
$.DATOS_BASICOS.orientationModes = [Titanium.UI.PORTRAIT];


$.widgetBarra.init({
	titulo: L('x3046757478_traducir', 'DATOS BASICOS'),
	onsalirinsp: Salirinsp_widgetBarra,
	__id: 'ALL138747237',
	continuar: L('', ''),
	salir_insp: L('', ''),
	fondo: 'fondoazul',
	top: 0,
	onpresiono: Presiono_widgetBarra
});

function Salirinsp_widgetBarra(e) {

	var evento = e;
	var preguntarOpciones_opts = [L('x3481285698_traducir', 'Asegurado no puede seguir'), L('x3077268071_traducir', ' Se me acabo la bateria'), L('x479989047_traducir', ' Tuve un accidente'), L('x2376009830_traducir', ' Cancelar')];
	var preguntarOpciones = Ti.UI.createOptionDialog({
		title: L('x1670176792_traducir', 'RAZON PARA CANCELAR INSPECCION ACTUAL'),
		options: preguntarOpciones_opts
	});
	preguntarOpciones.addEventListener('click', function(e) {
		var resp = preguntarOpciones_opts[e.index];
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var razon = "";
			if (resp == L('x3481285698_traducir', 'Asegurado no puede seguir')) {
				razon = "Asegurado no puede seguir";
			}
			if (resp == L('x1862794075_traducir', 'Se me acabo la bateria')) {
				razon = "Se me acabo la bateria";
			}
			if (resp == L('x4012805111_traducir', 'Tuve un accidente')) {
				razon = "Tuve un accidente";
			}
			if (Ti.App.deployType != 'production') console.log('mi razon es', {
				"datos": razon
			});
			if ((_.isObject(razon) || (_.isString(razon)) && !_.isEmpty(razon)) || _.isNumber(razon) || _.isBoolean(razon)) {
				if (Ti.App.deployType != 'production') console.log('llamando servicio cancelarTarea', {});
				require('vars')['insp_cancelada'] = L('x2941610362_traducir', 'siniestro');
				Alloy.Collections[$.datos.config.adapter.collection_name].add($.datos);
				$.datos.save();
				Alloy.Collections[$.datos.config.adapter.collection_name].fetch();
				var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
				var vista26_visible = true;

				if (vista26_visible == 'si') {
					vista26_visible = true;
				} else if (vista26_visible == 'no') {
					vista26_visible = false;
				}
				$.vista26.setVisible(vista26_visible);

				var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
				var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
				if (Ti.App.deployType != 'production') console.log('detalle de seltarea', {
					"data": seltarea
				});
				var datos = {
					id_inspector: inspector.id_server,
					codigo_identificador: inspector.codigo_identificador,
					id_server: seltarea.id_server,
					num_caso: seltarea.num_caso,
					razon: razon
				};
				require('vars')[_var_scopekey]['datos'] = datos;
				var consultarURL = {};
				consultarURL.success = function(e) {
					var elemento = e,
						valor = e;
					var vista26_visible = false;

					if (vista26_visible == 'si') {
						vista26_visible = true;
					} else if (vista26_visible == 'no') {
						vista26_visible = false;
					}
					$.vista26.setVisible(vista26_visible);

					Alloy.createController("firma_index", {}).getView().open();
					var ID_1502479695_func = function() {
						/** 
						 * Cerramos pantalla datos basicos 
						 */
						Alloy.Events.trigger('_cerrar_insp', {
							pantalla: 'basicos'
						});
					};
					var ID_1502479695 = setTimeout(ID_1502479695_func, 1000 * 0.5);
					elemento = null, valor = null;
				};
				consultarURL.error = function(e) {
					var elemento = e,
						valor = e;
					if (Ti.App.deployType != 'production') console.log('hubo error llamando cancelarTarea, la agregamos a cola de salida', {
						"elemento": elemento
					});
					if (Ti.App.deployType != 'production') console.log('agregando servicio cancelarTarea a cola', {});
					var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
					var datos = ('datos' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['datos'] : '';
					var insertarModelo_m = Alloy.Collections.cola;
					var insertarModelo_fila = Alloy.createModel('cola', {
						data: JSON.stringify(datos),
						id_tarea: seltarea.id_server,
						tipo: 'cancelar'
					});
					insertarModelo_m.add(insertarModelo_fila);
					insertarModelo_fila.save();
					var vista26_visible = false;

					if (vista26_visible == 'si') {
						vista26_visible = true;
					} else if (vista26_visible == 'no') {
						vista26_visible = false;
					}
					$.vista26.setVisible(vista26_visible);

					Alloy.createController("firma_index", {}).getView().open();
					/** 
					 * Cerramos pantalla datos basicos 
					 */
					Alloy.Events.trigger('_cerrar_insp', {
						pantalla: 'basicos'
					});
					elemento = null, valor = null;
				};
				require('helper').ajaxUnico('consultarURL', '' + String.format(L('x3244141284', '%1$scancelarTarea'), url_server.toString()) + '', 'POST', {
					id_inspector: seltarea.id_inspector,
					codigo_identificador: inspector.codigo_identificador,
					id_tarea: seltarea.id_server,
					num_caso: seltarea.num_caso,
					mensaje: razon,
					opcion: 0,
					tipo: 1
				}, 15000, consultarURL);
			}
		}
		resp = null;
		e.source.removeEventListener("click", arguments.callee);
	});
	preguntarOpciones.show();

}

function Presiono_widgetBarra(e) {

	var evento = e;
	/** 
	 * desenfoca campos de texto para ocultar teclado al presionar en partes blancas. 
	 */
	$.campo5.blur();
	$.campo6.blur();
	$.campo7.blur();
	$.Nombredela.blur();
	$.RUT.blur();
	$.campo8.blur();
	$.campo9.blur();
	$.campo10.blur();
	var ID_716533446_func = function() {
		require('vars')[_var_scopekey]['todobien'] = L('x4261170317', 'true');
		if (_.isUndefined($datos.presente_nombre)) {
			require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
			var preguntarAlerta_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta = Ti.UI.createAlertDialog({
				title: L('x3237162386_traducir', 'Atencion'),
				message: L('x3689875543_traducir', 'Ingrese nombre de la persona presente'),
				buttonNames: preguntarAlerta_opts
			});
			preguntarAlerta.addEventListener('click', function(e) {
				var nulo = preguntarAlerta_opts[e.index];
				nulo = null;
				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta.show();
		} else if ($datos.presente_nombre.length == 0 || $datos.presente_nombre.length == '0') {
			require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
			var preguntarAlerta2_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta2 = Ti.UI.createAlertDialog({
				title: L('x3237162386_traducir', 'Atencion'),
				message: L('x3689875543_traducir', 'Ingrese nombre de la persona presente'),
				buttonNames: preguntarAlerta2_opts
			});
			preguntarAlerta2.addEventListener('click', function(e) {
				var nulo = preguntarAlerta2_opts[e.index];
				nulo = null;
				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta2.show();
		} else if (_.isUndefined($datos.presente_rut)) {
			require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
			var preguntarAlerta3_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta3 = Ti.UI.createAlertDialog({
				title: L('x3237162386_traducir', 'Atencion'),
				message: L('x2263964467_traducir', 'Ingrese Rut de la persona presente'),
				buttonNames: preguntarAlerta3_opts
			});
			preguntarAlerta3.addEventListener('click', function(e) {
				var nulo = preguntarAlerta3_opts[e.index];
				nulo = null;
				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta3.show();
		} else if ($datos.presente_rut.length == 0 || $datos.presente_rut.length == '0') {
			require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
			var preguntarAlerta4_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta4 = Ti.UI.createAlertDialog({
				title: L('x3237162386_traducir', 'Atencion'),
				message: L('x2263964467_traducir', 'Ingrese Rut de la persona presente'),
				buttonNames: preguntarAlerta4_opts
			});
			preguntarAlerta4.addEventListener('click', function(e) {
				var nulo = preguntarAlerta4_opts[e.index];
				nulo = null;
				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta4.show();
		} else if (!_.isUndefined($datos.direccion_correcta)) {
			if ($datos.direccion_correcta == false || $datos.direccion_correcta == 'false') {
				if (_.isUndefined($datos.direccion_riesgo)) {
					require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
					var preguntarAlerta5_opts = [L('x1518866076_traducir', 'Aceptar')];
					var preguntarAlerta5 = Ti.UI.createAlertDialog({
						title: L('x3237162386_traducir', 'Atencion'),
						message: L('x2359666818_traducir', 'Debe indicar la dirección correcta'),
						buttonNames: preguntarAlerta5_opts
					});
					preguntarAlerta5.addEventListener('click', function(e) {
						var nulo = preguntarAlerta5_opts[e.index];
						nulo = null;
						e.source.removeEventListener("click", arguments.callee);
					});
					preguntarAlerta5.show();
				} else if (_.isUndefined($datos.direccion_comuna)) {
					require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
					var preguntarAlerta6_opts = [L('x1518866076_traducir', 'Aceptar')];
					var preguntarAlerta6 = Ti.UI.createAlertDialog({
						title: L('x3237162386_traducir', 'Atencion'),
						message: L('x4270715747_traducir', 'Debe indicar la comuna de la dirección'),
						buttonNames: preguntarAlerta6_opts
					});
					preguntarAlerta6.addEventListener('click', function(e) {
						var nulo = preguntarAlerta6_opts[e.index];
						nulo = null;
						e.source.removeEventListener("click", arguments.callee);
					});
					preguntarAlerta6.show();
				} else if (_.isUndefined($datos.direccion_ciudad)) {
					require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
					var preguntarAlerta7_opts = [L('x1518866076_traducir', 'Aceptar')];
					var preguntarAlerta7 = Ti.UI.createAlertDialog({
						title: L('x3237162386_traducir', 'Atencion'),
						message: L('x3232683315_traducir', 'Debe indicar la ciudad de la dirección'),
						buttonNames: preguntarAlerta7_opts
					});
					preguntarAlerta7.addEventListener('click', function(e) {
						var nulo = preguntarAlerta7_opts[e.index];
						nulo = null;
						e.source.removeEventListener("click", arguments.callee);
					});
					preguntarAlerta7.show();
				}
			}
		}
		var todobien = ('todobien' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['todobien'] : '';
		if (todobien == true || todobien == 'true') {
			$.widgetEstadeacuerdo.abrir({
				color: 'azul'
			});
		}
	};
	var ID_716533446 = setTimeout(ID_716533446_func, 1000 * 0.2);

}

$.widgetEstadeacuerdo.init({
	titulo: L('x404129330_traducir', '¿EL ASEGURADO CONFIRMA ESTOS DATOS?'),
	__id: 'ALL516906299',
	si: L('x1723413441_traducir', 'SI, Están correctos'),
	texto: L('x2083765231_traducir', 'El asegurado debe confirmar que los datos de esta sección están correctos'),
	pantalla: L('x2851883104_traducir', '¿ESTA DE ACUERDO?'),
	color: 'azul',
	onsi: si_widgetEstadeacuerdo,
	no: L('x55492959_traducir', 'NO, Hay que modificar algo')
});

function si_widgetEstadeacuerdo(e) {

	var evento = e;
	/** 
	 * Ocupamos esta variable para impedir que la pantalla siguente se cargue dos veces 
	 */
	var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
	if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
		require('vars')['var_abriendo'] = L('x4261170317', 'true');
		/** 
		 * Guardamos los datos de la tabla en el modelo 
		 */
		Alloy.Collections[$.datos.config.adapter.collection_name].add($.datos);
		$.datos.save();
		Alloy.Collections[$.datos.config.adapter.collection_name].fetch();
		var ID_1186276553_func = function() {
			$.DATOS_BASICOS.close();
		};
		var ID_1186276553 = setTimeout(ID_1186276553_func, 1000 * 0.5);
		Alloy.createController("caracteristicas_index", {}).getView().open();
	}

}

function Blur_campo5(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	$.datos.set({
		fono_fijo: elemento
	});
	if ('datos' in $) $datos = $.datos.toJSON();
	elemento = null, source = null;

}

function Blur_campo6(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	$.datos.set({
		fono_movil: elemento
	});
	if ('datos' in $) $datos = $.datos.toJSON();
	elemento = null, source = null;

}

function Blur_campo7(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	$.datos.set({
		email: elemento
	});
	if ('datos' in $) $datos = $.datos.toJSON();
	elemento = null, source = null;

}

function Blur_Nombredela(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	$.datos.set({
		presente_nombre: elemento
	});
	if ('datos' in $) $datos = $.datos.toJSON();
	elemento = null, source = null;

}

function Blur_RUT(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	$.datos.set({
		presente_rut: elemento
	});
	if ('datos' in $) $datos = $.datos.toJSON();
	elemento = null, source = null;

}

function Return_RUT(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	/** 
	 * Cuando el inspector presiona el enter en el teclado, desenfocamos el campo 
	 */
	$.RUT.blur();
	elemento = null, source = null;

}

function Change_ID_1568773770(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	if (elemento == true || elemento == 'true') {
		$.SI.setText('SI');

		/** 
		 * Re-activamos campos direccion, comuna, ciudad 
		 */
		$.campo8.setEditable('false');

		$.campo9.setEditable('false');

		$.campo10.setEditable('false');

		require('vars')[_var_scopekey]['direccioncorrecta'] = L('x2212294583', '1');
	} else {
		$.SI.setText('NO');

		/** 
		 * Desactivamos campos direccion, comuna, ciudad 
		 */
		require('vars')[_var_scopekey]['direccioncorrecta'] = L('x4108050209', '0');
		$.campo8.setEditable(true);

		$.campo9.setEditable(true);

		$.campo10.setEditable(true);

	}
	/** 
	 * Modificamos el campo direccion_correcta 
	 */
	$.datos.set({
		direccion_correcta: elemento
	});
	if ('datos' in $) $datos = $.datos.toJSON();
	elemento = null;

}

function Blur_campo8(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	$.datos.set({
		direccion_riesgo: elemento
	});
	if ('datos' in $) $datos = $.datos.toJSON();
	elemento = null, source = null;

}

function Blur_campo9(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	$.datos.set({
		direccion_comuna: elemento
	});
	if ('datos' in $) $datos = $.datos.toJSON();
	elemento = null, source = null;

}

function Blur_campo10(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	$.datos.set({
		direccion_ciudad: elemento
	});
	if ('datos' in $) $datos = $.datos.toJSON();
	elemento = null, source = null;

}

function Click_vista(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	/** 
	 * desenfoca campos de texto para ocultar teclado al presionar en partes blancas. 
	 */
	$.campo5.blur();
	$.campo6.blur();
	$.campo7.blur();
	$.Nombredela.blur();
	$.RUT.blur();
	$.campo8.blur();
	$.campo9.blur();
	$.campo10.blur();

}

function Postlayout_vista26(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.progreso.show();

}

function Postlayout_DATOS_BASICOS(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	/** 
	 * Cerramos fotosrequeridas (limpieza memoria) 
	 */
	Alloy.Events.trigger('_cerrar_insp', {
		pantalla: 'frequeridas'
	});
	/** 
	 * Cerramos pantalla hayalguien 
	 */
	Alloy.Events.trigger('_cerrar_insp', {
		pantalla: 'hayalguien'
	});
	var ID_1941683100_func = function() {
		/** 
		 * Una vez ya cargada la tarea, dejamos la variable limpia para poder abrir otras pantallas 
		 */
		require('vars')['var_abriendo'] = '';
	};
	var ID_1941683100 = setTimeout(ID_1941683100_func, 1000 * 0.2);

}

(function() {
	/** 
	 * Recuperamos variable para verificar si tiene datos, siendo asi, cargamos los datos en la tabla de datosbasicos 
	 */
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
		var moment = require('alloy/moment');
		var formatearFecha = seltarea.fecha_siniestro;
		var formatearFecha = moment(formatearFecha).format('dd-mm-yyyy');
		/** 
		 * Seteamos datos a vista 
		 */
		$.datos.set({
			id_inspeccion: seltarea.id_server,
			fono_movil: seltarea.asegurado_tel_movil,
			fono_fijo: seltarea.asegurado_tel_fijo,
			direccion_ciudad: seltarea.nivel_2,
			rut_asegurado: seltarea.asegurado_codigo_identificador,
			direccion_riesgo: seltarea.direccion,
			direccion_correcta: true,
			direccion_comuna: seltarea.nivel_3,
			asegurador: seltarea.asegurador,
			nro_caso: seltarea.num_caso,
			asegurado: seltarea.asegurado_nombre,
			fecha_siniestro: seltarea.fecha_siniestro,
			email: seltarea.asegurado_correo
		});
		if ('datos' in $) $datos = $.datos.toJSON();
		/** 
		 * Consultamos fecha/hora inicio de inspeccion de inspecciones 
		 */
		var consultarModelo_i = Alloy.createCollection('inspecciones');
		var consultarModelo_i_where = 'id_server=\'' + seltarea.id_server + '\'';
		consultarModelo_i.fetch({
			query: 'SELECT * FROM inspecciones WHERE id_server=\'' + seltarea.id_server + '\''
		});
		var insp = require('helper').query2array(consultarModelo_i);
		if (insp && insp.length) {
			/** 
			 * Traspasamos informacion a datosbasicos 
			 */
			$.datos.set({
				hora_inspeccion: insp[0].hora,
				fecha_inspeccion: insp[0].fecha,
				fecha_full_inspeccion: insp[0].fecha_inspeccion_inicio
			});
			if ('datos' in $) $datos = $.datos.toJSON();
		} else {
			/** 
			 * Formateamos hora/fecha inspeccion 
			 */
			var moment = require('alloy/moment');
			var hora = moment(new Date()).format('HH:mm');
			var moment = require('alloy/moment');
			var fecha = moment(new Date()).format('DD-MM-YYYY');
			$.datos.set({
				hora_inspeccion: hora,
				fecha_inspeccion: fecha,
				fecha_full_inspeccion: new Date()
			});
			if ('datos' in $) $datos = $.datos.toJSON();
		}
	} else {
		var fecha_siniestro = "2018-01-24";
		var moment = require('alloy/moment');
		var formatearFecha4 = moment(new Date()).format('dd-mm-yyyy');
		/** 
		 * Datos dummies para test 
		 */
		$.datos.set({
			id_inspeccion: 112233,
			fono_movil: 56999990070,
			fono_fijo: 56280908070,
			direccion_riesgo: 'Av. Las Condes 9460',
			direccion_correcta: true,
			asegurador: 'Falabella',
			nro_caso: 99999,
			asegurado: 'Pepito',
			fecha_siniestro: '17-01-2017',
			email: 'pepito@gmail.com'
		});
		if ('datos' in $) $datos = $.datos.toJSON();
		/** 
		 * Formateamos hora/fecha inspeccion 
		 */
		var moment = require('alloy/moment');
		var hora = moment(new Date()).format('HH:mm');
		var moment = require('alloy/moment');
		var fecha = moment(new Date()).format('DD-MM-YYYY');
		/** 
		 * Traspasamos informacion a datosbasicos 
		 */
		$.datos.set({
			hora_inspeccion: hora,
			fecha_inspeccion: fecha,
			fecha_full_inspeccion: new Date()
		});
		if ('datos' in $) $datos = $.datos.toJSON();
	}
	/** 
	 * Desenfocamos los campos de texto y cambiamos el color del statusbar 
	 */
	var ID_1001548790_func = function() {
		$.campo.blur();
		var DATOS_BASICOS_statusbar = '#006C9B';

		var setearStatusColor = function(DATOS_BASICOS_statusbar) {
			if (OS_IOS) {
				if (DATOS_BASICOS_statusbar == 'light' || DATOS_BASICOS_statusbar == 'claro') {
					$.DATOS_BASICOS_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.LIGHT_CONTENT);
				} else if (DATOS_BASICOS_statusbar == 'grey' || DATOS_BASICOS_statusbar == 'gris' || DATOS_BASICOS_statusbar == 'gray') {
					$.DATOS_BASICOS_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.GREY);
				} else if (DATOS_BASICOS_statusbar == 'oscuro' || DATOS_BASICOS_statusbar == 'dark') {
					$.DATOS_BASICOS_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.DEFAULT);
				}
			} else if (OS_ANDROID) {
				abx.setStatusbarColor(DATOS_BASICOS_statusbar);
			}
		};
		setearStatusColor(DATOS_BASICOS_statusbar);

	};
	var ID_1001548790 = setTimeout(ID_1001548790_func, 1000 * 0.1);
})();


/** 
 * Cerramos esta pantalla cuando es llamada en la pantalla siguente (siniestro) 
 */
_my_events['_cerrar_insp,ID_231485652'] = function(evento) {
	if (!_.isUndefined(evento.pantalla)) {
		if (evento.pantalla == L('x3679018572_traducir', 'basicos')) {
			if (Ti.App.deployType != 'production') console.log('debug cerrando datosbasicos', {});
			var ID_878785134_trycatch = {
				error: function(e) {
					if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
				}
			};
			try {
				ID_878785134_trycatch.error = function(evento) {
					if (Ti.App.deployType != 'production') console.log('error cerrando datosbasicos', {});
				};
				$.DATOS_BASICOS.close();
			} catch (e) {
				ID_878785134_trycatch.error(e);
			}
		}
	} else {
		if (Ti.App.deployType != 'production') console.log('debug cerrando (todas) datosbasicos', {});
		var ID_1962200830_trycatch = {
			error: function(e) {
				if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
			}
		};
		try {
			ID_1962200830_trycatch.error = function(evento) {
				if (Ti.App.deployType != 'production') console.log('error cerrando datosbasicos', {});
			};
			$.DATOS_BASICOS.close();
		} catch (e) {
			ID_1962200830_trycatch.error(e);
		}
	}
};
Alloy.Events.on('_cerrar_insp', _my_events['_cerrar_insp,ID_231485652']);

function Androidback_DATOS_BASICOS(e) {

	e.cancelBubble = true;
	var elemento = e.source;

}
if (OS_IOS || OS_ANDROID) {
	$.DATOS_BASICOS.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}
//$.DATOS_BASICOS.open();
