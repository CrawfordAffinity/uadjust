var _bind4section = {
	"ref1": "insp_itemdanos"
};
var _list_templates = {
	"elemento": {
		"vista2": {},
		"Label3": {
			"text": "{nombre}"
		},
		"Label2": {
			"text": "{id_server}"
		},
		"Label": {
			"text": "{id_segured}"
		},
		"vista": {}
	},
	"dano": {
		"Label2": {
			"text": "{id}"
		},
		"vista29": {},
		"Label": {
			"text": "{nombre}"
		}
	},
	"pborrar": {
		"Label4": {
			"text": "{id}"
		},
		"vista31": {},
		"Label3": {
			"text": "{nombre}"
		},
		"vista30": {},
		"vista32": {},
		"imagen5": {}
	}
};
var $recinto2 = $.recinto2.toJSON();

var _activity;
if (OS_ANDROID) {
	_activity = $.editarrecinto.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'editarrecinto';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.editarrecinto.addEventListener('open', function(e) {});
}
$.editarrecinto.orientationModes = [Titanium.UI.PORTRAIT];


var consultarModelo_like = function(search) {
	if (typeof search !== 'string' || this === null) {
		return false;
	}
	search = search.replace(new RegExp("([\\.\\\\\\+\\*\\?\\[\\^\\]\\$\\(\\)\\{\\}\\=\\!\\<\\>\\|\\:\\-])", "g"), "\\$1");
	search = search.replace(/%/g, '.*').replace(/_/g, '.');
	return RegExp('^' + search + '$', 'gi').test(this);
};
var consultarModelo_filter = function(coll) {
	var filtered = _.toArray(coll.filter(function(m) {
		return true;
	}));
	return filtered;
};
var consultarModelo_transform = function(model) {
	var modelo = model.toJSON();
	return modelo;
};
_.defer(function() {
	Alloy.Collections.insp_itemdanos.fetch();
});
Alloy.Collections.insp_itemdanos.on('add change delete', function(ee) {
	consultarModelo_update(ee);
});
var consultarModelo_update = function(evento) {
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	var consultarModelo2_i = Alloy.createCollection('insp_itemdanos');
	var consultarModelo2_i_where = 'id_inspeccion=\'' + seltarea.id_server + '\'';
	consultarModelo2_i.fetch({
		query: 'SELECT * FROM insp_itemdanos WHERE id_inspeccion=\'' + seltarea.id_server + '\''
	});
	var insp_n = require('helper').query2array(consultarModelo2_i);
	var alto = 55 * insp_n.length;
	var listado_alto = alto;

	if (listado_alto == '*') {
		listado_alto = Ti.UI.FILL;
	} else if (!isNaN(listado_alto)) {
		listado_alto = listado_alto + 'dp';
	}
	$.listado.setHeight(listado_alto);

};
Alloy.Collections.insp_itemdanos.fetch();


$.widgetBarra.init({
	titulo: L('x2293394797_traducir', 'EDITAR RECINTO'),
	onsalirinsp: Salirinsp_widgetBarra,
	__id: 'ALL21378405',
	textoderecha: L('x2943883035_traducir', 'Guardar'),
	fondo: 'fondomorado',
	top: 0,
	modal: L('', ''),
	onpresiono: Presiono_widgetBarra,
	colortextoderecha: 'blanco'
});

function Salirinsp_widgetBarra(e) {

	var evento = e;
	/** 
	 * Limpiamos widgets multiples (memoria) 
	 */
	$.widgetModalmultiple.limpiar({});
	$.widgetModalmultiple2.limpiar({});
	$.widgetModalmultiple3.limpiar({});
	$.widgetModalmultiple4.limpiar({});
	$.widgetModalmultiple5.limpiar({});
	$.widgetModalmultiple6.limpiar({});
	$.editarrecinto.close();

}

function Presiono_widgetBarra(e) {

	var evento = e;
	var temp_idrecinto = ('temp_idrecinto' in require('vars')) ? require('vars')['temp_idrecinto'] : '';
	/** 
	 * Consultamos los danos que tenga ese recinto y desplegamos mensaje para advertir que esta ingresando un recinto sin danos 
	 */
	var consultarModelo3_i = Alloy.createCollection('insp_itemdanos');
	var consultarModelo3_i_where = 'id_recinto=\'' + temp_idrecinto + '\'';
	consultarModelo3_i.fetch({
		query: 'SELECT * FROM insp_itemdanos WHERE id_recinto=\'' + temp_idrecinto + '\''
	});
	var danos_verificacion = require('helper').query2array(consultarModelo3_i);
	if (_.isUndefined($recinto2.nombre)) {
		/** 
		 * Verificamos que los campos obligatorios esten bien 
		 */
		var preguntarAlerta_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x995453488_traducir', 'Ingrese el nombre del recinto'),
			buttonNames: preguntarAlerta_opts
		});
		preguntarAlerta.addEventListener('click', function(e) {
			var nulo = preguntarAlerta_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta.show();
	} else if ((_.isObject($recinto2.nombre) || _.isString($recinto2.nombre)) && _.isEmpty($recinto2.nombre)) {
		var preguntarAlerta2_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta2 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x995453488_traducir', 'Ingrese el nombre del recinto'),
			buttonNames: preguntarAlerta2_opts
		});
		preguntarAlerta2.addEventListener('click', function(e) {
			var nulo = preguntarAlerta2_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta2.show();
	} else if (_.isUndefined($recinto2.id_nivel)) {
		var preguntarAlerta3_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta3 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x2221207910_traducir', 'Seleccione el nivel al cual pertenece el nuevo recinto'),
			buttonNames: preguntarAlerta3_opts
		});
		preguntarAlerta3.addEventListener('click', function(e) {
			var nulo = preguntarAlerta3_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta3.show();
	} else if (_.isUndefined($recinto2.largo)) {
		var preguntarAlerta4_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta4 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x2947701485_traducir', 'Ingrese largo del recinto'),
			buttonNames: preguntarAlerta4_opts
		});
		preguntarAlerta4.addEventListener('click', function(e) {
			var nulo = preguntarAlerta4_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta4.show();
	} else if ((_.isObject($recinto2.largo) || _.isString($recinto2.largo)) && _.isEmpty($recinto2.largo)) {
		var preguntarAlerta5_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta5 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x2947701485_traducir', 'Ingrese largo del recinto'),
			buttonNames: preguntarAlerta5_opts
		});
		preguntarAlerta5.addEventListener('click', function(e) {
			var nulo = preguntarAlerta5_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta5.show();
	} else if (_.isUndefined($recinto2.ancho)) {
		var preguntarAlerta6_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta6 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x2565566100_traducir', 'Ingrese ancho del recinto'),
			buttonNames: preguntarAlerta6_opts
		});
		preguntarAlerta6.addEventListener('click', function(e) {
			var nulo = preguntarAlerta6_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta6.show();
	} else if ((_.isObject($recinto2.ancho) || _.isString($recinto2.ancho)) && _.isEmpty($recinto2.ancho)) {
		var preguntarAlerta7_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta7 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x2565566100_traducir', 'Ingrese ancho del recinto'),
			buttonNames: preguntarAlerta7_opts
		});
		preguntarAlerta7.addEventListener('click', function(e) {
			var nulo = preguntarAlerta7_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta7.show();
	} else if (_.isUndefined($recinto2.alto)) {
		var preguntarAlerta8_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta8 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x3058684698_traducir', 'Ingrese la altura del recinto'),
			buttonNames: preguntarAlerta8_opts
		});
		preguntarAlerta8.addEventListener('click', function(e) {
			var nulo = preguntarAlerta8_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta8.show();
	} else if ((_.isObject($recinto2.alto) || _.isString($recinto2.alto)) && _.isEmpty($recinto2.alto)) {
		var preguntarAlerta9_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta9 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x3058684698_traducir', 'Ingrese la altura del recinto'),
			buttonNames: preguntarAlerta9_opts
		});
		preguntarAlerta9.addEventListener('click', function(e) {
			var nulo = preguntarAlerta9_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta9.show();
	} else if (_.isUndefined($recinto2.ids_estructuras_soportantes)) {
		var preguntarAlerta10_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta10 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x503969960_traducir', 'Seleccione la estructura soportante del recinto'),
			buttonNames: preguntarAlerta10_opts
		});
		preguntarAlerta10.addEventListener('click', function(e) {
			var nulo = preguntarAlerta10_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta10.show();
	} else if ((_.isObject($recinto2.ids_estructuras_soportantes) || _.isString($recinto2.ids_estructuras_soportantes)) && _.isEmpty($recinto2.ids_estructuras_soportantes)) {
		var preguntarAlerta11_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta11 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x503969960_traducir', 'Seleccione la estructura soportante del recinto'),
			buttonNames: preguntarAlerta11_opts
		});
		preguntarAlerta11.addEventListener('click', function(e) {
			var nulo = preguntarAlerta11_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta11.show();
	} else if (danos_verificacion && danos_verificacion.length == 0) {
		var preguntarAlerta12_opts = [L('x1602277533_traducir', 'GUARDAR'), L('x3812490383_traducir', ' VOLVER')];
		var preguntarAlerta12 = Ti.UI.createAlertDialog({
			title: L('x1927199828_traducir', '¿Guardar sin daños?'),
			message: L('x2099678332_traducir', 'Estas guardando el recinto sin daños asociados, si esta correcto presiona guardar'),
			buttonNames: preguntarAlerta12_opts
		});
		preguntarAlerta12.addEventListener('click', function(e) {
			var msj = preguntarAlerta12_opts[e.index];
			if (msj == L('x1602277533_traducir', 'GUARDAR')) {
				var eliminarModelo_i = Alloy.Collections.insp_recintos;
				var sql = 'DELETE FROM ' + eliminarModelo_i.config.adapter.collection_name + ' WHERE id=\'' + args._dato.id + '\'';
				var db = Ti.Database.open(eliminarModelo_i.config.adapter.db_name);
				db.execute(sql);
				db.close();
				eliminarModelo_i.trigger('remove');
				Alloy.Collections[$.recinto2.config.adapter.collection_name].add($.recinto2);
				$.recinto2.save();
				Alloy.Collections[$.recinto2.config.adapter.collection_name].fetch();
				test = null;
				$.widgetModalmultiple.limpiar({});
				$.widgetModalmultiple2.limpiar({});
				$.widgetModalmultiple3.limpiar({});
				$.widgetModalmultiple4.limpiar({});
				$.widgetModalmultiple5.limpiar({});
				$.widgetModalmultiple6.limpiar({});
				$.editarrecinto.close();
			}
			msj = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta12.show();
	} else {
		/** 
		 * Eliminamos el recinto que estamos editando... Es para evitar duplicaciones de recintos 
		 */
		/** 
		 * Eliminamos el recinto que estamos editando... Es para evitar duplicaciones de recintos 
		 */
		var eliminarModelo2_i = Alloy.Collections.insp_recintos;
		var sql = 'DELETE FROM ' + eliminarModelo2_i.config.adapter.collection_name + ' WHERE id=\'' + args._dato.id + '\'';
		var db = Ti.Database.open(eliminarModelo2_i.config.adapter.db_name);
		db.execute(sql);
		db.close();
		eliminarModelo2_i.trigger('remove');
		/** 
		 * Guardamos recinto editado 
		 */
		Alloy.Collections[$.recinto2.config.adapter.collection_name].add($.recinto2);
		$.recinto2.save();
		Alloy.Collections[$.recinto2.config.adapter.collection_name].fetch();
		/** 
		 * Limpiamos widgets multiples (memoria) 
		 */
		$.widgetModalmultiple.limpiar({});
		$.widgetModalmultiple2.limpiar({});
		$.widgetModalmultiple3.limpiar({});
		$.widgetModalmultiple4.limpiar({});
		$.widgetModalmultiple5.limpiar({});
		$.widgetModalmultiple6.limpiar({});
		$.editarrecinto.close();
	}
}

function Change_EscribaNombre(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	/** 
	 * Al escribir la descripcion, vamos actualizando en la tabla de recintos el nombre 
	 */
	$.recinto2.set({
		nombre: elemento
	});
	if ('recinto2' in $) $recinto2 = $.recinto2.toJSON();
	require('vars')['nombrerecinto'] = elemento;
	elemento = null, source = null;

}

function Return_EscribaNombre(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	$.EscribaNombre.blur();
	elemento = null, source = null;

}

$.widgetModal.init({
	titulo: L('x1571349115_traducir', 'NIVEL'),
	cargando: L('x1831148736', 'cargando...'),
	__id: 'ALL1379917834',
	left: 0,
	onrespuesta: Respuesta_widgetModal,
	campo: L('x2771220443_traducir', 'Nivel del Recinto'),
	onabrir: Abrir_widgetModal,
	color: 'morado',
	right: 0,
	seleccione: L('x2434264250_traducir', 'seleccione nivel'),
	activo: true,
	onafterinit: Afterinit_widgetModal
});

function Abrir_widgetModal(e) {

	var evento = e;

}

function Respuesta_widgetModal(e) {

	var evento = e;
	$.widgetModal.labels({
		valor: evento.valor
	});
	$.recinto2.set({
		id_nivel: evento.item.id_interno
	});
	if ('recinto2' in $) $recinto2 = $.recinto2.toJSON();

}

function Afterinit_widgetModal(e) {

	var evento = e;
	var ID_1130215393_func = function() {
		/** 
		 * obtenemos valor seleccionado previamente y lo mostramos en selector con desface para que pueda mostrar bien el valor en pantalla, si no, carga un valor erroneo 
		 */
		var consultarModelo4_i = Alloy.createCollection('insp_recintos');
		var consultarModelo4_i_where = 'id=\'' + args._dato.id + '\'';
		consultarModelo4_i.fetch({
			query: 'SELECT * FROM insp_recintos WHERE id=\'' + args._dato.id + '\''
		});
		var insp_datos = require('helper').query2array(consultarModelo4_i);
		if (Ti.App.deployType != 'production') console.log('detalle del recinto a editar', {
			"datos": insp_datos
		});
		if (insp_datos && insp_datos.length) {
			/** 
			 * obtenemos valor seleccionado previamente y lo mostramos en selector. 
			 */
			var consultarModelo5_i = Alloy.createCollection('insp_niveles');
			var consultarModelo5_i_where = 'id=\'' + insp_datos[0].id_nivel + '\'';
			consultarModelo5_i.fetch({
				query: 'SELECT * FROM insp_niveles WHERE id=\'' + insp_datos[0].id_nivel + '\''
			});
			var insp_niveles = require('helper').query2array(consultarModelo5_i);
			if (insp_niveles && insp_niveles.length) {
				var ID_52538855_func = function() {
					$.widgetModal.labels({
						valor: insp_niveles[0].nombre
					});
					if (Ti.App.deployType != 'production') console.log('el nombre del recinto es', {
						"datos": insp_niveles[0].nombre
					});
				};
				var ID_52538855 = setTimeout(ID_52538855_func, 1000 * 0.21);
			}
		}
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			/** 
			 * Cargamos el widget con los niveles del recinto 
			 */
			var consultarModelo6_i = Alloy.createCollection('insp_niveles');
			var consultarModelo6_i_where = 'id_inspeccion=\'' + seltarea.id_server + '\'';
			consultarModelo6_i.fetch({
				query: 'SELECT * FROM insp_niveles WHERE id_inspeccion=\'' + seltarea.id_server + '\''
			});
			var datosniveles = require('helper').query2array(consultarModelo6_i);
			/** 
			 * Obtenemos datos para selectores (solo los de esta inspeccion) 
			 */
			var datos = [];
			_.each(datosniveles, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'valor';
					if (llave == 'id') newkey = 'id_interno';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
		} else {
			/** 
			 * Insertamos dummies 
			 */
			/** 
			 * Insertamos dummies 
			 */
			var eliminarModelo3_i = Alloy.Collections.insp_niveles;
			var sql = "DELETE FROM " + eliminarModelo3_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo3_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo3_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4,5'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo_m = Alloy.Collections.insp_niveles;
				var insertarModelo_fila = Alloy.createModel('insp_niveles', {
					id_inspeccion: -1,
					nombre: String.format(L('x2818462194_traducir', 'Nivel%1$s'), item.toString()),
					piso: item
				});
				insertarModelo_m.add(insertarModelo_fila);
				insertarModelo_fila.save();
				_.defer(function() {});
			});
			/** 
			 * Obtenemos datos para selectores (todos) 
			 */
			var transformarModelo2_i = Alloy.createCollection('insp_niveles');
			transformarModelo2_i.fetch();
			var transformarModelo2_src = require('helper').query2array(transformarModelo2_i);
			var datos = [];
			_.each(transformarModelo2_src, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'valor';
					if (llave == 'id') newkey = 'id_interno';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
		}
		/** 
		 * Cargamos el widget con los niveles del recinto 
		 */
		$.widgetModal.data({
			data: datos
		});
	};
	var ID_1130215393 = setTimeout(ID_1130215393_func, 1000 * 0.2);

}

$.widgetFotochica.init({
	caja: 45,
	__id: 'ALL1650576465',
	onlisto: Listo_widgetFotochica,
	left: 0,
	top: 0,
	onclick: Click_widgetFotochica
});

function Click_widgetFotochica(e) {

	var evento = e;
	/** 
	 * Definimos que estamos capturando foto en el primer item 
	 */
	require('vars')[_var_scopekey]['cual_foto'] = L('x2212294583', '1');
	/** 
	 * Abrimos camara 
	 */
	$.widgetCamaralight.disparar({});
	/** 
	 * Detenemos las animaciones de los widget 
	 */
	$.widgetFotochica.detener({});
	$.widgetFotochica2.detener({});
	$.widgetFotochica3.detener({});

}

function Listo_widgetFotochica(e) {

	var evento = e;
	/** 
	 * Recuperamos variables para poder definir la estructura de las carpetas donde se guardara la miniatura 
	 */
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	var nuevoid_f1 = ('nuevoid_f1' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['nuevoid_f1'] : '';
	if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
		var ID_627897085_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + seltarea.id_server);
		if (ID_627897085_d.exists() == false) ID_627897085_d.createDirectory();
		var ID_627897085_f = Ti.Filesystem.getFile(ID_627897085_d.resolve(), 'mini' + nuevoid_f1.id + '.jpg');
		if (ID_627897085_f.exists() == true) ID_627897085_f.deleteFile();
		ID_627897085_f.write(evento.mini);
		ID_627897085_d = null;
		ID_627897085_f = null;
	} else {
		var ID_954194152_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + 0);
		if (ID_954194152_d.exists() == false) ID_954194152_d.createDirectory();
		var ID_954194152_f = Ti.Filesystem.getFile(ID_954194152_d.resolve(), 'mini' + nuevoid_f1.id + '.jpg');
		if (ID_954194152_f.exists() == true) ID_954194152_f.deleteFile();
		ID_954194152_f.write(evento.mini);
		ID_954194152_d = null;
		ID_954194152_f = null;
	}
}

function Click_imagen(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var evento = e;
	/** 
	 * Definimos que estamos capturando foto en el primer item 
	 */
	require('vars')[_var_scopekey]['cual_foto'] = L('x2212294583', '1');
	/** 
	 * Ocultamos la vista para dejar visible al widget 
	 */
	var vista17_visible = false;

	if (vista17_visible == 'si') {
		vista17_visible = true;
	} else if (vista17_visible == 'no') {
		vista17_visible = false;
	}
	$.vista17.setVisible(vista17_visible);

	/** 
	 * Detenemos las animaciones de los widget 
	 */
	$.widgetFotochica.detener({});
	$.widgetFotochica2.detener({});
	$.widgetFotochica3.detener({});
	/** 
	 * Abrimos camara 
	 */
	$.widgetCamaralight.disparar({});

}

$.widgetFotochica2.init({
	caja: 45,
	__id: 'ALL245423636',
	onlisto: Listo_widgetFotochica2,
	left: 5,
	top: 0,
	onclick: Click_widgetFotochica2
});

function Click_widgetFotochica2(e) {

	var evento = e;
	require('vars')[_var_scopekey]['cual_foto'] = 2;
	$.widgetCamaralight.disparar({});
	$.widgetFotochica2.detener({});
	$.widgetFotochica.detener({});
	$.widgetFotochica3.detener({});

}

function Listo_widgetFotochica2(e) {

	var evento = e;
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	var nuevoid_f2 = ('nuevoid_f2' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['nuevoid_f2'] : '';
	if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
		var ID_41571261_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + seltarea.id_server);
		if (ID_41571261_d.exists() == false) ID_41571261_d.createDirectory();
		var ID_41571261_f = Ti.Filesystem.getFile(ID_41571261_d.resolve(), 'mini' + nuevoid_f2.id + '.jpg');
		if (ID_41571261_f.exists() == true) ID_41571261_f.deleteFile();
		ID_41571261_f.write(evento.mini);
		ID_41571261_d = null;
		ID_41571261_f = null;
	} else {
		var ID_1641740840_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + 0);
		if (ID_1641740840_d.exists() == false) ID_1641740840_d.createDirectory();
		var ID_1641740840_f = Ti.Filesystem.getFile(ID_1641740840_d.resolve(), 'mini' + nuevoid_f2.id + '.jpg');
		if (ID_1641740840_f.exists() == true) ID_1641740840_f.deleteFile();
		ID_1641740840_f.write(evento.mini);
		ID_1641740840_d = null;
		ID_1641740840_f = null;
	}
}

function Click_imagen2(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var evento = e;
	require('vars')[_var_scopekey]['cual_foto'] = 2;
	var vista18_visible = false;

	if (vista18_visible == 'si') {
		vista18_visible = true;
	} else if (vista18_visible == 'no') {
		vista18_visible = false;
	}
	$.vista18.setVisible(vista18_visible);

	$.widgetFotochica.detener({});
	$.widgetFotochica2.detener({});
	$.widgetFotochica3.detener({});
	$.widgetCamaralight.disparar({});

}

$.widgetFotochica3.init({
	caja: 45,
	__id: 'ALL277122098',
	onlisto: Listo_widgetFotochica3,
	left: 5,
	top: 0,
	onclick: Click_widgetFotochica3
});

function Click_widgetFotochica3(e) {

	var evento = e;
	require('vars')[_var_scopekey]['cual_foto'] = 3;
	$.widgetCamaralight.disparar({});
	$.widgetFotochica3.detener({});
	$.widgetFotochica.detener({});
	$.widgetFotochica2.detener({});

}

function Listo_widgetFotochica3(e) {

	var evento = e;
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	var nuevoid_f3 = ('nuevoid_f3' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['nuevoid_f3'] : '';
	if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
		var ID_1843631350_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + seltarea.id_server);
		if (ID_1843631350_d.exists() == false) ID_1843631350_d.createDirectory();
		var ID_1843631350_f = Ti.Filesystem.getFile(ID_1843631350_d.resolve(), 'mini' + nuevoid_f3.id + '.jpg');
		if (ID_1843631350_f.exists() == true) ID_1843631350_f.deleteFile();
		ID_1843631350_f.write(evento.mini);
		ID_1843631350_d = null;
		ID_1843631350_f = null;
	} else {
		var ID_1948120750_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + 0);
		if (ID_1948120750_d.exists() == false) ID_1948120750_d.createDirectory();
		var ID_1948120750_f = Ti.Filesystem.getFile(ID_1948120750_d.resolve(), 'mini' + nuevoid_f3.id + '.jpg');
		if (ID_1948120750_f.exists() == true) ID_1948120750_f.deleteFile();
		ID_1948120750_f.write(evento.mini);
		ID_1948120750_d = null;
		ID_1948120750_f = null;
	}
}

function Click_imagen3(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var evento = e;
	require('vars')[_var_scopekey]['cual_foto'] = 3;
	var vista19_visible = false;

	if (vista19_visible == 'si') {
		vista19_visible = true;
	} else if (vista19_visible == 'no') {
		vista19_visible = false;
	}
	$.vista19.setVisible(vista19_visible);

	$.widgetFotochica.detener({});
	$.widgetFotochica2.detener({});
	$.widgetFotochica3.detener({});
	$.widgetCamaralight.disparar({});

}

function Change_campo(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	/** 
	 * Obtenemos el valor escrito en el campo de ancho 
	 */
	var ancho;
	ancho = $.campo2.getValue();

	if ((_.isObject(ancho) || (_.isString(ancho)) && !_.isEmpty(ancho)) || _.isNumber(ancho) || _.isBoolean(ancho)) {
		/** 
		 * Calculamos la superficie 
		 */
		if ((_.isObject(elemento) || (_.isString(elemento)) && !_.isEmpty(elemento)) || _.isNumber(elemento) || _.isBoolean(elemento)) {
			/** 
			 * nos aseguramos que ancho y valor de campo sean decimales con puntos (flotante), y no comas (espa&#241;ol) 
			 */
			var nuevo = parseFloat(ancho.split(',').join('.')) * parseFloat(elemento.split(',').join('.'));
			$.recinto2.set({
				superficie: nuevo
			});
			if ('recinto2' in $) $recinto2 = $.recinto2.toJSON();
		}
	}
	/** 
	 * Actualizamos el valor del largo del recinto 
	 */
	$.recinto2.set({
		largo: elemento
	});
	if ('recinto2' in $) $recinto2 = $.recinto2.toJSON();
	elemento = null, source = null;

}

function Change_campo2(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	var largo;
	largo = $.campo.getValue();

	if ((_.isObject(largo) || (_.isString(largo)) && !_.isEmpty(largo)) || _.isNumber(largo) || _.isBoolean(largo)) {
		if ((_.isObject(elemento) || (_.isString(elemento)) && !_.isEmpty(elemento)) || _.isNumber(elemento) || _.isBoolean(elemento)) {
			/** 
			 * nos aseguramos que largo y valor de campo sean decimales con puntos (flotante), y no comas (espa&#241;ol) 
			 */
			var nuevo = parseFloat(largo.split(',').join('.')) * parseFloat(elemento.split(',').join('.'));
			$.recinto2.set({
				superficie: nuevo
			});
			if ('recinto2' in $) $recinto2 = $.recinto2.toJSON();
		}
	}
	$.recinto2.set({
		ancho: elemento
	});
	if ('recinto2' in $) $recinto2 = $.recinto2.toJSON();
	elemento = null, source = null;

}

function Change_campo3(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	$.recinto2.set({
		alto: elemento
	});
	if ('recinto2' in $) $recinto2 = $.recinto2.toJSON();
	elemento = null, source = null;

}

$.widgetModalmultiple.init({
	titulo: L('x1975271086_traducir', 'Estructura Soportante'),
	cargando: L('x1831148736', 'cargando...'),
	__id: 'ALL321122430',
	oncerrar: Cerrar_widgetModalmultiple,
	left: 0,
	hint: L('x2898603391_traducir', 'Seleccione estructura'),
	color: 'morado',
	subtitulo: L('x4011106049_traducir', 'Indique los tipos'),
	right: 0,
	onclick: Click_widgetModalmultiple,
	onafterinit: Afterinit_widgetModalmultiple
});

function Click_widgetModalmultiple(e) {

	var evento = e;

}

function Cerrar_widgetModalmultiple(e) {

	var evento = e;
	$.widgetModalmultiple.update({});
	$.recinto2.set({
		ids_estructuras_soportantes: evento.valores
	});
	if ('recinto2' in $) $recinto2 = $.recinto2.toJSON();

}

function Afterinit_widgetModalmultiple(e) {

	var evento = e;
	/** 
	 * ejecutamos desfasado para no congelar el hilo. 
	 */
	var ID_431750065_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			var consultarModelo7_i = Alloy.createCollection('insp_recintos');
			var consultarModelo7_i_where = 'id=\'' + args._dato.id + '\'';
			consultarModelo7_i.fetch({
				query: 'SELECT * FROM insp_recintos WHERE id=\'' + args._dato.id + '\''
			});
			var insp_datos = require('helper').query2array(consultarModelo7_i);
			if (insp_datos && insp_datos.length) {
				var consultarModelo8_i = Alloy.createCollection('estructura_soportante');
				var consultarModelo8_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
				consultarModelo8_i.fetch({
					query: 'SELECT * FROM estructura_soportante WHERE pais_texto=\'' + pais[0].nombre + '\''
				});
				var estructura = require('helper').query2array(consultarModelo8_i);
				var padre_index = 0;
				_.each(estructura, function(padre, padre_pos, padre_list) {
					padre_index += 1;
					if (_.isNull(insp_datos[0].ids_estructuras_soportantes)) {
						/** 
						 * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
						 */
						padre._estado = 0;
					} else if (insp_datos[0].ids_estructuras_soportantes.indexOf(padre.id_segured) != -1) {
						padre._estado = 1;
					} else {
						padre._estado = 0;
					}
				});
				var datos = [];
				_.each(estructura, function(fila, pos) {
					var new_row = {};
					_.each(fila, function(x, llave) {
						var newkey = '';
						if (llave == 'nombre') newkey = 'label';
						if (llave == 'id_segured') newkey = 'valor';
						if (llave == '_estado') newkey = 'estado';
						if (newkey != '') new_row[newkey] = fila[llave];
					});
					datos.push(new_row);
				});
			} else {
				var transformarModelo4_i = Alloy.createCollection('estructura_soportante');
				transformarModelo4_i.fetch();
				var transformarModelo4_src = require('helper').query2array(transformarModelo4_i);
				var datos = [];
				_.each(transformarModelo4_src, function(fila, pos) {
					var new_row = {};
					_.each(fila, function(x, llave) {
						var newkey = '';
						if (llave == 'nombre') newkey = 'label';
						if (llave == 'id_segured') newkey = 'valor';
						if (newkey != '') new_row[newkey] = fila[llave];
					});
					new_row['estado'] = 0;
					datos.push(new_row);
				});
			}
			$.widgetModalmultiple.update({
				data: datos
			});
		} else {
			/** 
			 * cargamos datos dummy para cuando compilamos de forma individual. 
			 */
			var eliminarModelo4_i = Alloy.Collections.estructura_soportante;
			var sql = "DELETE FROM " + eliminarModelo4_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo4_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo4_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo2_m = Alloy.Collections.estructura_soportante;
				var insertarModelo2_fila = Alloy.createModel('estructura_soportante', {
					nombre: String.format(L('x1088195980_traducir', 'Estructura%1$s'), item.toString()),
					id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
					id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item.toString()),
					pais: 1
				});
				insertarModelo2_m.add(insertarModelo2_fila);
				insertarModelo2_fila.save();
				_.defer(function() {});
			});
			var consultarModelo9_i = Alloy.createCollection('insp_recintos');
			var consultarModelo9_i_where = 'id=\'' + args._dato.id + '\'';
			consultarModelo9_i.fetch({
				query: 'SELECT * FROM insp_recintos WHERE id=\'' + args._dato.id + '\''
			});
			var insp_datos = require('helper').query2array(consultarModelo9_i);
			if (Ti.App.deployType != 'production') console.log('contenido datos de todo pio', {
				"insp": insp_datos,
				"cont": datos,
				"args": args
			});
			if (insp_datos && insp_datos.length) {
				var consultarModelo10_i = Alloy.createCollection('estructura_soportante');
				var consultarModelo10_i_where = '';
				consultarModelo10_i.fetch();
				var estructura = require('helper').query2array(consultarModelo10_i);
				var padre_index = 0;
				_.each(estructura, function(padre, padre_pos, padre_list) {
					padre_index += 1;
					if (_.isNull(insp_datos[0].ids_estructuras_soportantes)) {
						/** 
						 * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
						 */
						padre._estado = 0;
					} else if (insp_datos[0].ids_estructuras_soportantes.indexOf(padre.id_segured) != -1) {
						padre._estado = 1;
					} else {
						padre._estado = 0;
					}
				});
				var datos = [];
				_.each(estructura, function(fila, pos) {
					var new_row = {};
					_.each(fila, function(x, llave) {
						var newkey = '';
						if (llave == 'nombre') newkey = 'label';
						if (llave == 'id_segured') newkey = 'valor';
						if (llave == '_estado') newkey = 'estado';
						if (newkey != '') new_row[newkey] = fila[llave];
					});
					datos.push(new_row);
				});
			} else {
				var transformarModelo6_i = Alloy.createCollection('estructura_soportante');
				transformarModelo6_i.fetch();
				var transformarModelo6_src = require('helper').query2array(transformarModelo6_i);
				var datos = [];
				_.each(transformarModelo6_src, function(fila, pos) {
					var new_row = {};
					_.each(fila, function(x, llave) {
						var newkey = '';
						if (llave == 'nombre') newkey = 'label';
						if (llave == 'id_segured') newkey = 'valor';
						if (newkey != '') new_row[newkey] = fila[llave];
					});
					new_row['estado'] = 0;
					datos.push(new_row);
				});
			}
			$.widgetModalmultiple.update({
				data: datos
			});
		}
	};
	var ID_431750065 = setTimeout(ID_431750065_func, 1000 * 0.2);

}

$.widgetModalmultiple2.init({
	titulo: L('x1219835481_traducir', 'Muros / Tabiques'),
	cargando: L('x1831148736', 'cargando...'),
	__id: 'ALL1975428302',
	oncerrar: Cerrar_widgetModalmultiple2,
	left: 0,
	hint: L('x2879998099_traducir', 'Seleccione muros'),
	color: 'morado',
	subtitulo: L('x4011106049_traducir', 'Indique los tipos'),
	right: 0,
	onclick: Click_widgetModalmultiple2,
	onafterinit: Afterinit_widgetModalmultiple2
});

function Click_widgetModalmultiple2(e) {

	var evento = e;

}

function Cerrar_widgetModalmultiple2(e) {

	var evento = e;
	$.widgetModalmultiple2.update({});
	$.recinto2.set({
		ids_muros: evento.valores
	});
	if ('recinto2' in $) $recinto2 = $.recinto2.toJSON();

}

function Afterinit_widgetModalmultiple2(e) {

	var evento = e;
	/** 
	 * ejecutamos desfasado para no congelar el hilo. 
	 */
	var ID_7512644_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			var consultarModelo11_i = Alloy.createCollection('insp_recintos');
			var consultarModelo11_i_where = 'id=\'' + args._dato.id + '\'';
			consultarModelo11_i.fetch({
				query: 'SELECT * FROM insp_recintos WHERE id=\'' + args._dato.id + '\''
			});
			var insp_datos = require('helper').query2array(consultarModelo11_i);
			if (insp_datos && insp_datos.length) {
				var consultarModelo12_i = Alloy.createCollection('muros_tabiques');
				var consultarModelo12_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
				consultarModelo12_i.fetch({
					query: 'SELECT * FROM muros_tabiques WHERE pais_texto=\'' + pais[0].nombre + '\''
				});
				var muros_tabiques = require('helper').query2array(consultarModelo12_i);
				var padre_index = 0;
				_.each(muros_tabiques, function(padre, padre_pos, padre_list) {
					padre_index += 1;
					if (_.isNull(insp_datos[0].ids_muros)) {
						/** 
						 * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
						 */
						padre._estado = 0;
					} else if (insp_datos[0].ids_muros.indexOf(padre.id_segured) != -1) {
						padre._estado = 1;
					} else {
						padre._estado = 0;
					}
				});
				var datos = [];
				_.each(muros_tabiques, function(fila, pos) {
					var new_row = {};
					_.each(fila, function(x, llave) {
						var newkey = '';
						if (llave == 'nombre') newkey = 'label';
						if (llave == 'id_segured') newkey = 'valor';
						if (llave == '_estado') newkey = 'estado';
						if (newkey != '') new_row[newkey] = fila[llave];
					});
					datos.push(new_row);
				});
			} else {
				var transformarModelo8_i = Alloy.createCollection('muros_tabiques');
				transformarModelo8_i.fetch();
				var transformarModelo8_src = require('helper').query2array(transformarModelo8_i);
				var datos = [];
				_.each(transformarModelo8_src, function(fila, pos) {
					var new_row = {};
					_.each(fila, function(x, llave) {
						var newkey = '';
						if (llave == 'nombre') newkey = 'label';
						if (llave == 'id_segured') newkey = 'valor';
						if (newkey != '') new_row[newkey] = fila[llave];
					});
					new_row['estado'] = 0;
					datos.push(new_row);
				});
			}
			$.widgetModalmultiple2.update({
				data: datos
			});
		} else {
			/** 
			 * cargamos datos dummy para cuando compilamos de forma individual. 
			 */
			var eliminarModelo5_i = Alloy.Collections.muros_tabiques;
			var sql = "DELETE FROM " + eliminarModelo5_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo5_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo5_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo3_m = Alloy.Collections.muros_tabiques;
				var insertarModelo3_fila = Alloy.createModel('muros_tabiques', {
					nombre: String.format(L('x3296339325_traducir', 'muros_tabiques %1$s'), item.toString()),
					id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
					id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item.toString()),
					pais: 1
				});
				insertarModelo3_m.add(insertarModelo3_fila);
				insertarModelo3_fila.save();
				_.defer(function() {});
			});
			var consultarModelo13_i = Alloy.createCollection('insp_recintos');
			var consultarModelo13_i_where = 'id=\'' + args._dato.id + '\'';
			consultarModelo13_i.fetch({
				query: 'SELECT * FROM insp_recintos WHERE id=\'' + args._dato.id + '\''
			});
			var insp_datos = require('helper').query2array(consultarModelo13_i);
			if (Ti.App.deployType != 'production') console.log('contenido datos de todo pio', {
				"insp": insp_datos,
				"cont": datos,
				"args": args
			});
			if (insp_datos && insp_datos.length) {
				var consultarModelo14_i = Alloy.createCollection('muros_tabiques');
				var consultarModelo14_i_where = '';
				consultarModelo14_i.fetch();
				var muros_tabiques = require('helper').query2array(consultarModelo14_i);
				var padre_index = 0;
				_.each(muros_tabiques, function(padre, padre_pos, padre_list) {
					padre_index += 1;
					if (_.isNull(insp_datos[0].ids_muros)) {
						/** 
						 * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
						 */
						padre._estado = 0;
					} else if (insp_datos[0].ids_muros.indexOf(padre.id_segured) != -1) {
						padre._estado = 1;
					} else {
						padre._estado = 0;
					}
				});
				var datos = [];
				_.each(muros_tabiques, function(fila, pos) {
					var new_row = {};
					_.each(fila, function(x, llave) {
						var newkey = '';
						if (llave == 'nombre') newkey = 'label';
						if (llave == 'id_segured') newkey = 'valor';
						if (llave == '_estado') newkey = 'estado';
						if (newkey != '') new_row[newkey] = fila[llave];
					});
					datos.push(new_row);
				});
			} else {
				var transformarModelo10_i = Alloy.createCollection('muros_tabiques');
				transformarModelo10_i.fetch();
				var transformarModelo10_src = require('helper').query2array(transformarModelo10_i);
				var datos = [];
				_.each(transformarModelo10_src, function(fila, pos) {
					var new_row = {};
					_.each(fila, function(x, llave) {
						var newkey = '';
						if (llave == 'nombre') newkey = 'label';
						if (llave == 'id_segured') newkey = 'valor';
						if (newkey != '') new_row[newkey] = fila[llave];
					});
					new_row['estado'] = 0;
					datos.push(new_row);
				});
			}
			$.widgetModalmultiple2.update({
				data: datos
			});
		}
	};
	var ID_7512644 = setTimeout(ID_7512644_func, 1000 * 0.2);

}

$.widgetModalmultiple3.init({
	titulo: L('x3327059844_traducir', 'Entrepisos'),
	cargando: L('x1831148736', 'cargando...'),
	__id: 'ALL399652068',
	oncerrar: Cerrar_widgetModalmultiple3,
	left: 0,
	hint: L('x2146928948_traducir', 'Seleccione entrepisos'),
	color: 'morado',
	subtitulo: L('x4011106049_traducir', 'Indique los tipos'),
	right: 0,
	onclick: Click_widgetModalmultiple3,
	onafterinit: Afterinit_widgetModalmultiple3
});

function Click_widgetModalmultiple3(e) {

	var evento = e;

}

function Cerrar_widgetModalmultiple3(e) {

	var evento = e;
	$.widgetModalmultiple3.update({});
	$.recinto2.set({
		ids_entrepisos: evento.valores
	});
	if ('recinto2' in $) $recinto2 = $.recinto2.toJSON();

}

function Afterinit_widgetModalmultiple3(e) {

	var evento = e;
	/** 
	 * ejecutamos desfasado para no congelar el hilo. 
	 */
	var ID_847564113_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			var consultarModelo15_i = Alloy.createCollection('insp_recintos');
			var consultarModelo15_i_where = 'id=\'' + args._dato.id + '\'';
			consultarModelo15_i.fetch({
				query: 'SELECT * FROM insp_recintos WHERE id=\'' + args._dato.id + '\''
			});
			var insp_datos = require('helper').query2array(consultarModelo15_i);
			if (insp_datos && insp_datos.length) {
				var consultarModelo16_i = Alloy.createCollection('entrepisos');
				var consultarModelo16_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
				consultarModelo16_i.fetch({
					query: 'SELECT * FROM entrepisos WHERE pais_texto=\'' + pais[0].nombre + '\''
				});
				var entrepisos = require('helper').query2array(consultarModelo16_i);
				var padre_index = 0;
				_.each(entrepisos, function(padre, padre_pos, padre_list) {
					padre_index += 1;
					if (_.isNull(insp_datos[0].ids_entrepisos)) {
						/** 
						 * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
						 */
						padre._estado = 0;
					} else if (insp_datos[0].ids_entrepisos.indexOf(padre.id_segured) != -1) {
						padre._estado = 1;
					} else {
						padre._estado = 0;
					}
				});
				var datos = [];
				_.each(entrepisos, function(fila, pos) {
					var new_row = {};
					_.each(fila, function(x, llave) {
						var newkey = '';
						if (llave == 'nombre') newkey = 'label';
						if (llave == 'id_segured') newkey = 'valor';
						if (llave == '_estado') newkey = 'estado';
						if (newkey != '') new_row[newkey] = fila[llave];
					});
					datos.push(new_row);
				});
			} else {
				var transformarModelo12_i = Alloy.createCollection('entrepisos');
				transformarModelo12_i.fetch();
				var transformarModelo12_src = require('helper').query2array(transformarModelo12_i);
				var datos = [];
				_.each(transformarModelo12_src, function(fila, pos) {
					var new_row = {};
					_.each(fila, function(x, llave) {
						var newkey = '';
						if (llave == 'nombre') newkey = 'label';
						if (llave == 'id_segured') newkey = 'valor';
						if (newkey != '') new_row[newkey] = fila[llave];
					});
					new_row['estado'] = 0;
					datos.push(new_row);
				});
			}
			$.widgetModalmultiple3.update({
				data: datos
			});
		} else {
			/** 
			 * cargamos datos dummy para cuando compilamos de forma individual. 
			 */
			var eliminarModelo6_i = Alloy.Collections.entrepisos;
			var sql = "DELETE FROM " + eliminarModelo6_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo6_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo6_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo4_m = Alloy.Collections.entrepisos;
				var insertarModelo4_fila = Alloy.createModel('entrepisos', {
					nombre: String.format(L('x2766335390_traducir', 'entrepisos %1$s'), item.toString()),
					id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
					id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item.toString()),
					pais: 1
				});
				insertarModelo4_m.add(insertarModelo4_fila);
				insertarModelo4_fila.save();
				_.defer(function() {});
			});
			var consultarModelo17_i = Alloy.createCollection('insp_recintos');
			var consultarModelo17_i_where = 'id=\'' + args._dato.id + '\'';
			consultarModelo17_i.fetch({
				query: 'SELECT * FROM insp_recintos WHERE id=\'' + args._dato.id + '\''
			});
			var insp_datos = require('helper').query2array(consultarModelo17_i);
			if (Ti.App.deployType != 'production') console.log('contenido datos de todo pio', {
				"insp": insp_datos,
				"cont": datos,
				"args": args
			});
			if (insp_datos && insp_datos.length) {
				var consultarModelo18_i = Alloy.createCollection('entrepisos');
				var consultarModelo18_i_where = '';
				consultarModelo18_i.fetch();
				var entrepisos = require('helper').query2array(consultarModelo18_i);
				var padre_index = 0;
				_.each(entrepisos, function(padre, padre_pos, padre_list) {
					padre_index += 1;
					if (_.isNull(insp_datos[0].ids_entrepisos)) {
						/** 
						 * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
						 */
						padre._estado = 0;
						if (Ti.App.deployType != 'production') console.log('habia nulo', {});
					} else if (insp_datos[0].ids_entrepisos.indexOf(padre.id_segured) != -1) {
						padre._estado = 1;
						if (Ti.App.deployType != 'production') console.log('contiene algo', {});
					} else {
						padre._estado = 0;
						if (Ti.App.deployType != 'production') console.log('otra cosa', {});
					}
				});
				var datos = [];
				_.each(entrepisos, function(fila, pos) {
					var new_row = {};
					_.each(fila, function(x, llave) {
						var newkey = '';
						if (llave == 'nombre') newkey = 'label';
						if (llave == 'id_segured') newkey = 'valor';
						if (llave == '_estado') newkey = 'estado';
						if (newkey != '') new_row[newkey] = fila[llave];
					});
					datos.push(new_row);
				});
			} else {
				var transformarModelo14_i = Alloy.createCollection('entrepisos');
				transformarModelo14_i.fetch();
				var transformarModelo14_src = require('helper').query2array(transformarModelo14_i);
				var datos = [];
				_.each(transformarModelo14_src, function(fila, pos) {
					var new_row = {};
					_.each(fila, function(x, llave) {
						var newkey = '';
						if (llave == 'nombre') newkey = 'label';
						if (llave == 'id_segured') newkey = 'valor';
						if (newkey != '') new_row[newkey] = fila[llave];
					});
					new_row['estado'] = 0;
					datos.push(new_row);
				});
			}
			$.widgetModalmultiple3.update({
				data: datos
			});
		}
	};
	var ID_847564113 = setTimeout(ID_847564113_func, 1000 * 0.2);

}

$.widgetModalmultiple4.init({
	titulo: L('x591862035_traducir', 'Pavimentos'),
	cargando: L('x1831148736', 'cargando...'),
	__id: 'ALL1990255213',
	oncerrar: Cerrar_widgetModalmultiple4,
	left: 0,
	hint: L('x2600368035_traducir', 'Seleccione pavimentos'),
	color: 'morado',
	subtitulo: L('x4011106049_traducir', 'Indique los tipos'),
	right: 0,
	onclick: Click_widgetModalmultiple4,
	onafterinit: Afterinit_widgetModalmultiple4
});

function Click_widgetModalmultiple4(e) {

	var evento = e;

}

function Cerrar_widgetModalmultiple4(e) {

	var evento = e;
	$.widgetModalmultiple4.update({});
	$.recinto2.set({
		ids_pavimentos: evento.valores
	});
	if ('recinto2' in $) $recinto2 = $.recinto2.toJSON();

}

function Afterinit_widgetModalmultiple4(e) {

	var evento = e;
	/** 
	 * ejecutamos desfasado para no congelar el hilo. 
	 */
	var ID_502523396_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			var consultarModelo19_i = Alloy.createCollection('insp_recintos');
			var consultarModelo19_i_where = 'id=\'' + args._dato.id + '\'';
			consultarModelo19_i.fetch({
				query: 'SELECT * FROM insp_recintos WHERE id=\'' + args._dato.id + '\''
			});
			var insp_datos = require('helper').query2array(consultarModelo19_i);
			if (insp_datos && insp_datos.length) {
				var consultarModelo20_i = Alloy.createCollection('pavimento');
				var consultarModelo20_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
				consultarModelo20_i.fetch({
					query: 'SELECT * FROM pavimento WHERE pais_texto=\'' + pais[0].nombre + '\''
				});
				var pavimento = require('helper').query2array(consultarModelo20_i);
				var padre_index = 0;
				_.each(pavimento, function(padre, padre_pos, padre_list) {
					padre_index += 1;
					if (_.isNull(insp_datos[0].ids_pavimentos)) {
						/** 
						 * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
						 */
						padre._estado = 0;
					} else if (insp_datos[0].ids_pavimentos.indexOf(padre.id_segured) != -1) {
						padre._estado = 1;
					} else {
						padre._estado = 0;
					}
				});
				var datos = [];
				_.each(pavimento, function(fila, pos) {
					var new_row = {};
					_.each(fila, function(x, llave) {
						var newkey = '';
						if (llave == 'nombre') newkey = 'label';
						if (llave == 'id_segured') newkey = 'valor';
						if (llave == '_estado') newkey = 'estado';
						if (newkey != '') new_row[newkey] = fila[llave];
					});
					datos.push(new_row);
				});
			} else {
				var transformarModelo16_i = Alloy.createCollection('pavimento');
				transformarModelo16_i.fetch();
				var transformarModelo16_src = require('helper').query2array(transformarModelo16_i);
				var datos = [];
				_.each(transformarModelo16_src, function(fila, pos) {
					var new_row = {};
					_.each(fila, function(x, llave) {
						var newkey = '';
						if (llave == 'nombre') newkey = 'label';
						if (llave == 'id_segured') newkey = 'valor';
						if (newkey != '') new_row[newkey] = fila[llave];
					});
					new_row['estado'] = 0;
					datos.push(new_row);
				});
			}
			$.widgetModalmultiple4.update({
				data: datos
			});
		} else {
			/** 
			 * cargamos datos dummy para cuando compilamos de forma individual. 
			 */
			var eliminarModelo7_i = Alloy.Collections.pavimento;
			var sql = "DELETE FROM " + eliminarModelo7_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo7_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo7_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo5_m = Alloy.Collections.pavimento;
				var insertarModelo5_fila = Alloy.createModel('pavimento', {
					nombre: String.format(L('x194260945_traducir', 'pavimento %1$s'), item.toString()),
					id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
					id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item.toString()),
					pais: 1
				});
				insertarModelo5_m.add(insertarModelo5_fila);
				insertarModelo5_fila.save();
				_.defer(function() {});
			});
			var consultarModelo21_i = Alloy.createCollection('insp_recintos');
			var consultarModelo21_i_where = 'id=\'' + args._dato.id + '\'';
			consultarModelo21_i.fetch({
				query: 'SELECT * FROM insp_recintos WHERE id=\'' + args._dato.id + '\''
			});
			var insp_datos = require('helper').query2array(consultarModelo21_i);
			if (Ti.App.deployType != 'production') console.log('contenido datos de todo pio', {
				"insp": insp_datos,
				"cont": datos,
				"args": args
			});
			if (insp_datos && insp_datos.length) {
				var consultarModelo22_i = Alloy.createCollection('pavimento');
				var consultarModelo22_i_where = '';
				consultarModelo22_i.fetch();
				var pavimento = require('helper').query2array(consultarModelo22_i);
				var padre_index = 0;
				_.each(pavimento, function(padre, padre_pos, padre_list) {
					padre_index += 1;
					if (_.isNull(insp_datos[0].ids_pavimentos)) {
						/** 
						 * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
						 */
						padre._estado = 0;
						if (Ti.App.deployType != 'production') console.log('habia nulo', {});
					} else if (insp_datos[0].ids_pavimentos.indexOf(padre.id_segured) != -1) {
						padre._estado = 1;
						if (Ti.App.deployType != 'production') console.log('contiene algo', {});
					} else {
						padre._estado = 0;
						if (Ti.App.deployType != 'production') console.log('otra cosa', {});
					}
				});
				var datos = [];
				_.each(pavimento, function(fila, pos) {
					var new_row = {};
					_.each(fila, function(x, llave) {
						var newkey = '';
						if (llave == 'nombre') newkey = 'label';
						if (llave == 'id_segured') newkey = 'valor';
						if (llave == '_estado') newkey = 'estado';
						if (newkey != '') new_row[newkey] = fila[llave];
					});
					datos.push(new_row);
				});
			} else {
				var transformarModelo18_i = Alloy.createCollection('pavimento');
				transformarModelo18_i.fetch();
				var transformarModelo18_src = require('helper').query2array(transformarModelo18_i);
				var datos = [];
				_.each(transformarModelo18_src, function(fila, pos) {
					var new_row = {};
					_.each(fila, function(x, llave) {
						var newkey = '';
						if (llave == 'nombre') newkey = 'label';
						if (llave == 'id_segured') newkey = 'valor';
						if (newkey != '') new_row[newkey] = fila[llave];
					});
					new_row['estado'] = 0;
					datos.push(new_row);
				});
			}
			$.widgetModalmultiple4.update({
				data: datos
			});
		}
	};
	var ID_502523396 = setTimeout(ID_502523396_func, 1000 * 0.2);

}

$.widgetModalmultiple5.init({
	titulo: L('x1866523485_traducir', 'Estruct. cubierta'),
	cargando: L('x1831148736', 'cargando...'),
	__id: 'ALL1536081210',
	oncerrar: Cerrar_widgetModalmultiple5,
	left: 0,
	hint: L('x2460890829_traducir', 'Seleccione e.cubiertas'),
	color: 'morado',
	subtitulo: L('x4011106049_traducir', 'Indique los tipos'),
	right: 0,
	onclick: Click_widgetModalmultiple5,
	onafterinit: Afterinit_widgetModalmultiple5
});

function Click_widgetModalmultiple5(e) {

	var evento = e;

}

function Cerrar_widgetModalmultiple5(e) {

	var evento = e;
	$.widgetModalmultiple5.update({});
	$.recinto2.set({
		ids_estructura_cubiera: evento.valores
	});
	if ('recinto2' in $) $recinto2 = $.recinto2.toJSON();

}

function Afterinit_widgetModalmultiple5(e) {

	var evento = e;
	/** 
	 * ejecutamos desfasado para no congelar el hilo. 
	 */
	var ID_1552516105_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			var consultarModelo23_i = Alloy.createCollection('insp_recintos');
			var consultarModelo23_i_where = 'id=\'' + args._dato.id + '\'';
			consultarModelo23_i.fetch({
				query: 'SELECT * FROM insp_recintos WHERE id=\'' + args._dato.id + '\''
			});
			var insp_datos = require('helper').query2array(consultarModelo23_i);
			if (insp_datos && insp_datos.length) {
				var consultarModelo24_i = Alloy.createCollection('estructura_cubierta');
				var consultarModelo24_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
				consultarModelo24_i.fetch({
					query: 'SELECT * FROM estructura_cubierta WHERE pais_texto=\'' + pais[0].nombre + '\''
				});
				var estructura_cubierta = require('helper').query2array(consultarModelo24_i);
				var padre_index = 0;
				_.each(estructura_cubierta, function(padre, padre_pos, padre_list) {
					padre_index += 1;
					if (_.isNull(insp_datos[0].ids_estructura_cubiera)) {
						/** 
						 * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
						 */
						padre._estado = 0;
					} else if (insp_datos[0].ids_estructura_cubiera.indexOf(padre.id_segured) != -1) {
						padre._estado = 1;
					} else {
						padre._estado = 0;
					}
				});
				var datos = [];
				_.each(estructura_cubierta, function(fila, pos) {
					var new_row = {};
					_.each(fila, function(x, llave) {
						var newkey = '';
						if (llave == 'nombre') newkey = 'label';
						if (llave == 'id_segured') newkey = 'valor';
						if (llave == '_estado') newkey = 'estado';
						if (newkey != '') new_row[newkey] = fila[llave];
					});
					datos.push(new_row);
				});
			} else {
				var transformarModelo20_i = Alloy.createCollection('estructura_cubierta');
				transformarModelo20_i.fetch();
				var transformarModelo20_src = require('helper').query2array(transformarModelo20_i);
				var datos = [];
				_.each(transformarModelo20_src, function(fila, pos) {
					var new_row = {};
					_.each(fila, function(x, llave) {
						var newkey = '';
						if (llave == 'nombre') newkey = 'label';
						if (llave == 'id_segured') newkey = 'valor';
						if (newkey != '') new_row[newkey] = fila[llave];
					});
					new_row['estado'] = 0;
					datos.push(new_row);
				});
			}
			$.widgetModalmultiple5.update({
				data: datos
			});
		} else {
			/** 
			 * cargamos datos dummy para cuando compilamos de forma individual. 
			 */
			var eliminarModelo8_i = Alloy.Collections.estructura_cubierta;
			var sql = "DELETE FROM " + eliminarModelo8_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo8_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo8_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo6_m = Alloy.Collections.estructura_cubierta;
				var insertarModelo6_fila = Alloy.createModel('estructura_cubierta', {
					nombre: String.format(L('x2039569579_traducir', 'estructura_cubierta %1$s'), item.toString()),
					id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
					id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item.toString()),
					pais: 1
				});
				insertarModelo6_m.add(insertarModelo6_fila);
				insertarModelo6_fila.save();
				_.defer(function() {});
			});
			var consultarModelo25_i = Alloy.createCollection('insp_recintos');
			var consultarModelo25_i_where = 'id=\'' + args._dato.id + '\'';
			consultarModelo25_i.fetch({
				query: 'SELECT * FROM insp_recintos WHERE id=\'' + args._dato.id + '\''
			});
			var insp_datos = require('helper').query2array(consultarModelo25_i);
			if (Ti.App.deployType != 'production') console.log('contenido datos de todo pio', {
				"insp": insp_datos,
				"cont": datos,
				"args": args
			});
			if (insp_datos && insp_datos.length) {
				var consultarModelo26_i = Alloy.createCollection('estructura_cubierta');
				var consultarModelo26_i_where = '';
				consultarModelo26_i.fetch();
				var estructura_cubierta = require('helper').query2array(consultarModelo26_i);
				var padre_index = 0;
				_.each(estructura_cubierta, function(padre, padre_pos, padre_list) {
					padre_index += 1;
					if (_.isNull(insp_datos[0].ids_estructura_cubiera)) {
						/** 
						 * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
						 */
						padre._estado = 0;
					} else if (insp_datos[0].ids_estructura_cubiera.indexOf(padre.id_segured) != -1) {
						padre._estado = 1;
					} else {
						padre._estado = 0;
					}
				});
				var datos = [];
				_.each(estructura_cubierta, function(fila, pos) {
					var new_row = {};
					_.each(fila, function(x, llave) {
						var newkey = '';
						if (llave == 'nombre') newkey = 'label';
						if (llave == 'id_segured') newkey = 'valor';
						if (llave == '_estado') newkey = 'estado';
						if (newkey != '') new_row[newkey] = fila[llave];
					});
					datos.push(new_row);
				});
			} else {
				var transformarModelo22_i = Alloy.createCollection('estructura_cubierta');
				transformarModelo22_i.fetch();
				var transformarModelo22_src = require('helper').query2array(transformarModelo22_i);
				var datos = [];
				_.each(transformarModelo22_src, function(fila, pos) {
					var new_row = {};
					_.each(fila, function(x, llave) {
						var newkey = '';
						if (llave == 'nombre') newkey = 'label';
						if (llave == 'id_segured') newkey = 'valor';
						if (newkey != '') new_row[newkey] = fila[llave];
					});
					new_row['estado'] = 0;
					datos.push(new_row);
				});
			}
			$.widgetModalmultiple5.update({
				data: datos
			});
		}
	};
	var ID_1552516105 = setTimeout(ID_1552516105_func, 1000 * 0.2);

}

$.widgetModalmultiple6.init({
	titulo: L('x2266302645_traducir', 'Cubierta'),
	cargando: L('x1831148736', 'cargando...'),
	__id: 'ALL363463535',
	oncerrar: Cerrar_widgetModalmultiple6,
	left: 0,
	hint: L('x2134385782_traducir', 'Seleccione cubiertas'),
	color: 'morado',
	subtitulo: L('x4011106049_traducir', 'Indique los tipos'),
	right: 0,
	onclick: Click_widgetModalmultiple6,
	onafterinit: Afterinit_widgetModalmultiple6
});

function Click_widgetModalmultiple6(e) {

	var evento = e;

}

function Cerrar_widgetModalmultiple6(e) {

	var evento = e;
	$.widgetModalmultiple6.update({});
	$.recinto2.set({
		ids_cubierta: evento.valores
	});
	if ('recinto2' in $) $recinto2 = $.recinto2.toJSON();

}

function Afterinit_widgetModalmultiple6(e) {

	var evento = e;
	/** 
	 * ejecutamos desfasado para no congelar el hilo. 
	 */
	var ID_1647553309_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			var consultarModelo27_i = Alloy.createCollection('insp_recintos');
			var consultarModelo27_i_where = 'id=\'' + args._dato.id + '\'';
			consultarModelo27_i.fetch({
				query: 'SELECT * FROM insp_recintos WHERE id=\'' + args._dato.id + '\''
			});
			var insp_datos = require('helper').query2array(consultarModelo27_i);
			if (insp_datos && insp_datos.length) {
				var consultarModelo28_i = Alloy.createCollection('cubierta');
				var consultarModelo28_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
				consultarModelo28_i.fetch({
					query: 'SELECT * FROM cubierta WHERE pais_texto=\'' + pais[0].nombre + '\''
				});
				var cubierta = require('helper').query2array(consultarModelo28_i);
				var padre_index = 0;
				_.each(cubierta, function(padre, padre_pos, padre_list) {
					padre_index += 1;
					if (_.isNull(insp_datos[0].ids_cubierta)) {
						/** 
						 * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
						 */
						padre._estado = 0;
					} else if (insp_datos[0].ids_cubierta.indexOf(padre.id_segured) != -1) {
						padre._estado = 1;
					} else {
						padre._estado = 0;
					}
				});
				var datos = [];
				_.each(cubierta, function(fila, pos) {
					var new_row = {};
					_.each(fila, function(x, llave) {
						var newkey = '';
						if (llave == 'nombre') newkey = 'label';
						if (llave == 'id_segured') newkey = 'valor';
						if (llave == '_estado') newkey = 'estado';
						if (newkey != '') new_row[newkey] = fila[llave];
					});
					datos.push(new_row);
				});
			} else {
				var transformarModelo24_i = Alloy.createCollection('cubierta');
				transformarModelo24_i.fetch();
				var transformarModelo24_src = require('helper').query2array(transformarModelo24_i);
				var datos = [];
				_.each(transformarModelo24_src, function(fila, pos) {
					var new_row = {};
					_.each(fila, function(x, llave) {
						var newkey = '';
						if (llave == 'nombre') newkey = 'label';
						if (llave == 'id_segured') newkey = 'valor';
						if (newkey != '') new_row[newkey] = fila[llave];
					});
					new_row['estado'] = 0;
					datos.push(new_row);
				});
			}
			$.widgetModalmultiple6.update({
				data: datos
			});
		} else {
			/** 
			 * cargamos datos dummy para cuando compilamos de forma individual. 
			 */
			var eliminarModelo9_i = Alloy.Collections.cubierta;
			var sql = "DELETE FROM " + eliminarModelo9_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo9_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo9_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo7_m = Alloy.Collections.cubierta;
				var insertarModelo7_fila = Alloy.createModel('cubierta', {
					nombre: String.format(L('x1216789140_traducir', 'cubierta %1$s'), item.toString()),
					id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
					id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item.toString()),
					pais: 1
				});
				insertarModelo7_m.add(insertarModelo7_fila);
				insertarModelo7_fila.save();
				_.defer(function() {});
			});
			var consultarModelo29_i = Alloy.createCollection('insp_recintos');
			var consultarModelo29_i_where = 'id=\'' + args._dato.id + '\'';
			consultarModelo29_i.fetch({
				query: 'SELECT * FROM insp_recintos WHERE id=\'' + args._dato.id + '\''
			});
			var insp_datos = require('helper').query2array(consultarModelo29_i);
			if (Ti.App.deployType != 'production') console.log('contenido datos de todo pio', {
				"insp": insp_datos,
				"cont": datos,
				"args": args
			});
			if (insp_datos && insp_datos.length) {
				var consultarModelo30_i = Alloy.createCollection('cubierta');
				var consultarModelo30_i_where = '';
				consultarModelo30_i.fetch();
				var cubierta = require('helper').query2array(consultarModelo30_i);
				var padre_index = 0;
				_.each(cubierta, function(padre, padre_pos, padre_list) {
					padre_index += 1;
					if (_.isNull(insp_datos[0].ids_cubierta)) {
						/** 
						 * esto no deberia ser nunca, pero si no se selecciono ninguna estructura soportante, debemos asumir que no esta marcada. 
						 */
						padre._estado = 0;
					} else if (insp_datos[0].ids_cubierta.indexOf(padre.id_segured) != -1) {
						padre._estado = 1;
					} else {
						padre._estado = 0;
					}
				});
				var datos = [];
				_.each(cubierta, function(fila, pos) {
					var new_row = {};
					_.each(fila, function(x, llave) {
						var newkey = '';
						if (llave == 'nombre') newkey = 'label';
						if (llave == 'id_segured') newkey = 'valor';
						if (llave == '_estado') newkey = 'estado';
						if (newkey != '') new_row[newkey] = fila[llave];
					});
					datos.push(new_row);
				});
			} else {
				var transformarModelo26_i = Alloy.createCollection('cubierta');
				transformarModelo26_i.fetch();
				var transformarModelo26_src = require('helper').query2array(transformarModelo26_i);
				var datos = [];
				_.each(transformarModelo26_src, function(fila, pos) {
					var new_row = {};
					_.each(fila, function(x, llave) {
						var newkey = '';
						if (llave == 'nombre') newkey = 'label';
						if (llave == 'id_segured') newkey = 'valor';
						if (newkey != '') new_row[newkey] = fila[llave];
					});
					new_row['estado'] = 0;
					datos.push(new_row);
				});
			}
			$.widgetModalmultiple6.update({
				data: datos
			});
		}
	};
	var ID_1647553309 = setTimeout(ID_1647553309_func, 1000 * 0.2);

}

function Touchstart_vista28(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.iconoNuevo_dano.setColor('#999999');


}

function Touchend_vista28(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.iconoNuevo_dano.setColor('#8383db');

	/** 
	 * Ocupamos esta variable para impedir que la pantalla siguente se cargue dos veces 
	 */
	var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
	if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
		require('vars')['var_abriendo'] = L('x4261170317', 'true');
		Alloy.createController("nuevodano_index", {}).getView().open();
	}

}

function Swipe_vista29(e) {

	e.cancelBubble = true;
	var elemento = e.section.getItemAt(e.itemIndex);
	var findVariables = require('fvariables');
	var fila = {};
	_.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
		_.each(obj_id, function(valor, prop) {
			fila[prop] = elemento[id_field][prop];
			var llaves = findVariables(valor, '{', '}');
			_.each(llaves, function(llave) {
				fila[llave] = elemento[id_field][prop];
			});
		});
	});
	if (Ti.App.deployType != 'production') console.log('en normal', {
		"e": e
	});
	if (e.direction == L('x2053629800_traducir', 'left')) {
		var findVariables = require('fvariables');
		elemento.template = 'pborrar';
		_.each(_list_templates['pborrar'], function(obj_id, id_field) {
			_.each(obj_id, function(valor, prop) {
				var llaves = findVariables(valor, '{', '}');
				_.each(llaves, function(llave) {
					elemento[id_field] = {};
					elemento[id_field][prop] = fila[llave];
				});
			});
		});
		if (OS_IOS) {
			e.section.updateItemAt(e.itemIndex, elemento, {
				animated: true
			});
		} else if (OS_ANDROID) {
			e.section.updateItemAt(e.itemIndex, elemento);
		}
	}

}

function Click_vista29(e) {

	e.cancelBubble = true;
	var elemento = e.section.getItemAt(e.itemIndex);
	var findVariables = require('fvariables');
	var fila = {};
	_.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
		_.each(obj_id, function(valor, prop) {
			fila[prop] = elemento[id_field][prop];
			var llaves = findVariables(valor, '{', '}');
			_.each(llaves, function(llave) {
				fila[llave] = elemento[id_field][prop];
			});
		});
	});
	/** 
	 * Ocupamos esta variable para impedir que la pantalla siguente se cargue dos veces 
	 */
	var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
	if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
		require('vars')['var_abriendo'] = L('x4261170317', 'true');
		/** 
		 * Enviamos el parametro _dato para indicar cual sera el id del dano a editar 
		 */
		Alloy.createController("editardano_index", {
			'_dato': fila
		}).getView().open();
	}

}

function Click_vista32(e) {

	e.cancelBubble = true;
	var elemento = e.section.getItemAt(e.itemIndex);
	var findVariables = require('fvariables');
	var fila = {};
	_.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
		_.each(obj_id, function(valor, prop) {
			fila[prop] = elemento[id_field][prop];
			var llaves = findVariables(valor, '{', '}');
			_.each(llaves, function(llave) {
				fila[llave] = elemento[id_field][prop];
			});
		});
	});
	if (Ti.App.deployType != 'production') console.log('click en borrar', {
		"e": e
	});
	var preguntarAlerta13_opts = [L('x3827418516_traducir', 'Si'), L('x1962639792_traducir', ' No')];
	var preguntarAlerta13 = Ti.UI.createAlertDialog({
		title: L('x4097537701_traducir', 'ALERTA'),
		message: '' + String.format(L('x614696086_traducir', 'Seguro quiere borrar %1$s ?'), fila.nombre.toString()) + '',
		buttonNames: preguntarAlerta13_opts
	});
	preguntarAlerta13.addEventListener('click', function(e) {
		var xd = preguntarAlerta13_opts[e.index];
		if (xd == L('x3827418516_traducir', 'Si')) {
			if (Ti.App.deployType != 'production') console.log('borrando xd', {});
			var eliminarModelo10_i = Alloy.Collections.insp_itemdanos;
			var sql = 'DELETE FROM ' + eliminarModelo10_i.config.adapter.collection_name + ' WHERE id=\'' + fila.id + '\'';
			var db = Ti.Database.open(eliminarModelo10_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo10_i.trigger('remove');
			_.defer(function() {
				Alloy.Collections.insp_itemdanos.fetch();
			});
		}
		xd = null;
		e.source.removeEventListener("click", arguments.callee);
	});
	preguntarAlerta13.show();

}

function Swipe_vista30(e) {

	e.cancelBubble = true;
	var elemento = e.section.getItemAt(e.itemIndex);
	var findVariables = require('fvariables');
	var fila = {};
	_.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
		_.each(obj_id, function(valor, prop) {
			fila[prop] = elemento[id_field][prop];
			var llaves = findVariables(valor, '{', '}');
			_.each(llaves, function(llave) {
				fila[llave] = elemento[id_field][prop];
			});
		});
	});
	if (Ti.App.deployType != 'production') console.log('en borrar', {
		"e": e
	});
	if (e.direction == L('x3033167124_traducir', 'right')) {
		if (Ti.App.deployType != 'production') console.log('swipe hacia derecha (desde borrar)', {});
		var findVariables = require('fvariables');
		elemento.template = 'dano';
		_.each(_list_templates['dano'], function(obj_id, id_field) {
			_.each(obj_id, function(valor, prop) {
				var llaves = findVariables(valor, '{', '}');
				_.each(llaves, function(llave) {
					elemento[id_field] = {};
					elemento[id_field][prop] = fila[llave];
				});
			});
		});
		if (OS_IOS) {
			e.section.updateItemAt(e.itemIndex, elemento, {
				animated: true
			});
		} else if (OS_ANDROID) {
			e.section.updateItemAt(e.itemIndex, elemento);
		}
	}

}

function Click_vista30(e) {

	e.cancelBubble = true;
	var elemento = e.section.getItemAt(e.itemIndex);
	var findVariables = require('fvariables');
	var fila = {};
	_.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
		_.each(obj_id, function(valor, prop) {
			fila[prop] = elemento[id_field][prop];
			var llaves = findVariables(valor, '{', '}');
			_.each(llaves, function(llave) {
				fila[llave] = elemento[id_field][prop];
			});
		});
	});
	if (Ti.App.deployType != 'production') console.log('en borrar', {
		"e": e
	});
	var findVariables = require('fvariables');
	elemento.template = 'dano';
	_.each(_list_templates['dano'], function(obj_id, id_field) {
		_.each(obj_id, function(valor, prop) {
			var llaves = findVariables(valor, '{', '}');
			_.each(llaves, function(llave) {
				elemento[id_field] = {};
				elemento[id_field][prop] = fila[llave];
			});
		});
	});
	if (OS_IOS) {
		e.section.updateItemAt(e.itemIndex, elemento, {
			animated: true
		});
	} else if (OS_ANDROID) {
		e.section.updateItemAt(e.itemIndex, elemento);
	}

}

$.widgetCamaralight.init({
	onfotolista: Fotolista_widgetCamaralight,
	__id: 'ALL824520076'
});

function Fotolista_widgetCamaralight(e) {

	var evento = e;
	/** 
	 * Recuperamos cual fue la foto seleccionada 
	 */
	var cual_foto = ('cual_foto' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['cual_foto'] : '';
	if (cual_foto == 1 || cual_foto == '1') {
		var insertarModelo8_m = Alloy.Collections.numero_unico;
		var insertarModelo8_fila = Alloy.createModel('numero_unico', {
			comentario: 'nuevo recinto foto1'
		});
		insertarModelo8_m.add(insertarModelo8_fila);
		insertarModelo8_fila.save();
		var nuevoid_f1 = require('helper').model2object(insertarModelo8_m.last());
		_.defer(function() {});
		/** 
		 * Guardamos en variable el detalle del modelo numero_unico para asignar informacion de foto1 del recinto 
		 */
		require('vars')[_var_scopekey]['nuevoid_f1'] = nuevoid_f1;
		/** 
		 * Recuperamos variable para saber si estamos haciendo una inspeccion o dummy y guardar la foto en la memoria del celular 
		 */
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var ID_977124211_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
			if (ID_977124211_d.exists() == false) ID_977124211_d.createDirectory();
			var ID_977124211_f = Ti.Filesystem.getFile(ID_977124211_d.resolve(), 'cap' + nuevoid_f1.id + '.jpg');
			if (ID_977124211_f.exists() == true) ID_977124211_f.deleteFile();
			ID_977124211_f.write(evento.foto);
			ID_977124211_d = null;
			ID_977124211_f = null;
		} else {
			var ID_1689009349_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + 0);
			if (ID_1689009349_d.exists() == false) ID_1689009349_d.createDirectory();
			var ID_1689009349_f = Ti.Filesystem.getFile(ID_1689009349_d.resolve(), 'cap' + nuevoid_f1.id + '.jpg');
			if (ID_1689009349_f.exists() == true) ID_1689009349_f.deleteFile();
			ID_1689009349_f.write(evento.foto);
			ID_1689009349_d = null;
			ID_1689009349_f = null;
		}
		/** 
		 * Actualizamos el modelo indicando cual es el nombre de la imagen 
		 */
		$.recinto2.set({
			foto1: String.format(L('x1070475521', 'cap%1$s.jpg'), nuevoid_f1.id.toString())
		});
		if ('recinto2' in $) $recinto2 = $.recinto2.toJSON();
		/** 
		 * Enviamos la foto para que genere la miniatura (y tambien la guarde) y muestre en pantalla 
		 */
		$.widgetFotochica.procesar({
			imagen: evento.foto,
			nueva: 640,
			calidad: 91,
			camara: 'trasera'
		});
		/** 
		 * Limpiamos memoria ram 
		 */
		evento = null;
	} else if (cual_foto == 2) {
		var insertarModelo9_m = Alloy.Collections.numero_unico;
		var insertarModelo9_fila = Alloy.createModel('numero_unico', {
			comentario: 'nuevo recinto foto2'
		});
		insertarModelo9_m.add(insertarModelo9_fila);
		insertarModelo9_fila.save();
		var nuevoid_f2 = require('helper').model2object(insertarModelo9_m.last());
		_.defer(function() {});
		require('vars')[_var_scopekey]['nuevoid_f2'] = nuevoid_f2;
		if (Ti.App.deployType != 'production') console.log(String.format(L('x2429469022', 'imagen capturada y asignada como cap%1$s.jpg'), nuevoid_f2.id.toString()), {});
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var ID_1949908973_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
			if (ID_1949908973_d.exists() == false) ID_1949908973_d.createDirectory();
			var ID_1949908973_f = Ti.Filesystem.getFile(ID_1949908973_d.resolve(), 'cap' + nuevoid_f2.id + '.jpg');
			if (ID_1949908973_f.exists() == true) ID_1949908973_f.deleteFile();
			ID_1949908973_f.write(evento.foto);
			ID_1949908973_d = null;
			ID_1949908973_f = null;
		} else {
			var ID_1908146111_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + 0);
			if (ID_1908146111_d.exists() == false) ID_1908146111_d.createDirectory();
			var ID_1908146111_f = Ti.Filesystem.getFile(ID_1908146111_d.resolve(), 'cap' + nuevoid_f2.id + '.jpg');
			if (ID_1908146111_f.exists() == true) ID_1908146111_f.deleteFile();
			ID_1908146111_f.write(evento.foto);
			ID_1908146111_d = null;
			ID_1908146111_f = null;
		}
		$.recinto2.set({
			foto2: String.format(L('x1070475521', 'cap%1$s.jpg'), nuevoid_f2.id.toString())
		});
		if ('recinto2' in $) $recinto2 = $.recinto2.toJSON();
		$.widgetFotochica2.procesar({
			imagen: evento.foto,
			nueva: 640,
			calidad: 91,
			camara: 'trasera'
		});
		evento = null;
	} else if (cual_foto == 3) {
		var insertarModelo10_m = Alloy.Collections.numero_unico;
		var insertarModelo10_fila = Alloy.createModel('numero_unico', {
			comentario: 'nuevo recinto foto3'
		});
		insertarModelo10_m.add(insertarModelo10_fila);
		insertarModelo10_fila.save();
		var nuevoid_f3 = require('helper').model2object(insertarModelo10_m.last());
		_.defer(function() {});
		require('vars')[_var_scopekey]['nuevoid_f3'] = nuevoid_f3;
		if (Ti.App.deployType != 'production') console.log(String.format(L('x2429469022', 'imagen capturada y asignada como cap%1$s.jpg'), nuevoid_f3.id.toString()), {});
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var ID_1943128755_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
			if (ID_1943128755_d.exists() == false) ID_1943128755_d.createDirectory();
			var ID_1943128755_f = Ti.Filesystem.getFile(ID_1943128755_d.resolve(), 'cap' + nuevoid_f3.id + '.jpg');
			if (ID_1943128755_f.exists() == true) ID_1943128755_f.deleteFile();
			ID_1943128755_f.write(evento.foto);
			ID_1943128755_d = null;
			ID_1943128755_f = null;
		} else {
			var ID_270171166_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + 0);
			if (ID_270171166_d.exists() == false) ID_270171166_d.createDirectory();
			var ID_270171166_f = Ti.Filesystem.getFile(ID_270171166_d.resolve(), 'cap' + nuevoid_f3.id + '.jpg');
			if (ID_270171166_f.exists() == true) ID_270171166_f.deleteFile();
			ID_270171166_f.write(evento.foto);
			ID_270171166_d = null;
			ID_270171166_f = null;
		}
		$.recinto2.set({
			foto3: String.format(L('x1070475521', 'cap%1$s.jpg'), nuevoid_f3.id.toString())
		});
		if ('recinto2' in $) $recinto2 = $.recinto2.toJSON();
		$.widgetFotochica3.procesar({
			imagen: evento.foto,
			nueva: 640,
			calidad: 91,
			camara: 'trasera'
		});
		evento = null;
	}
}

(function() {
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
		/** 
		 * Modificamos la idinspeccion con el idserver de la tarea 
		 */
		$.recinto2.set({
			id_inspeccion: seltarea.id_server
		});
		if ('recinto2' in $) $recinto2 = $.recinto2.toJSON();
	}
	/** 
	 * heredamos datos previos de nivel en modelo nuevo con binding de esta pantalla 
	 */
	var consultarModelo31_i = Alloy.createCollection('insp_recintos');
	var consultarModelo31_i_where = 'id=\'' + args._dato.id + '\'';
	consultarModelo31_i.fetch({
		query: 'SELECT * FROM insp_recintos WHERE id=\'' + args._dato.id + '\''
	});
	var insp_r = require('helper').query2array(consultarModelo31_i);
	/** 
	 * filtramos items de dano, para el id_recinto activo. 
	 */
	consultarModelo_filter = function(coll) {
		var filtered = coll.filter(function(m) {
			var _tests = [],
				_all_true = false,
				model = m.toJSON();
			_tests.push((model.id_recinto == insp_r[0].id_recinto));
			var _all_true_s = _.uniq(_tests);
			_all_true = (_all_true_s.length == 1 && _all_true_s[0] == true) ? true : false;
			return _all_true;
		});
		filtered = _.toArray(filtered);
		return filtered;
	};
	_.defer(function() {
		Alloy.Collections.insp_itemdanos.fetch();
	});
	if (Ti.App.deployType != 'production') console.log('filtrando el id del recitno', {
		"datos": insp_r[0]
	});
	/** 
	 * Cargamos el modelo con los datos obtenidos desde la consulta 
	 */
	$.recinto2.set({
		ids_entrepisos: insp_r[0].ids_entrepisos,
		id_inspeccion: insp_r[0].id_inspeccion,
		nombre: insp_r[0].nombre,
		superficie: insp_r[0].superficie,
		largo: insp_r[0].largo,
		foto1: insp_r[0].foto1,
		foto2: insp_r[0].foto2,
		id_recinto: insp_r[0].id_recinto,
		foto3: insp_r[0].foto3,
		ids_pavimentos: insp_r[0].ids_pavimentos,
		alto: insp_r[0].alto,
		ids_muros: insp_r[0].ids_muros,
		ancho: insp_r[0].ancho,
		ids_estructura_cubiera: insp_r[0].ids_estructura_cubiera,
		ids_cubierta: insp_r[0].ids_cubierta,
		id_nivel: insp_r[0].id_nivel,
		ids_estructuras_soportantes: insp_r[0].ids_estructuras_soportantes
	});
	if ('recinto2' in $) $recinto2 = $.recinto2.toJSON();
	/** 
	 * guardamos variable pq aun no existe este recinto al agregar da&#241;os 
	 */
	require('vars')['temp_idrecinto'] = insp_r[0].id_recinto;
	/** 
	 * Cargamos los textos en los campos de texto 
	 */
	$.campo.setValue(insp_r[0].largo);

	$.campo2.setValue(insp_r[0].ancho);

	$.campo3.setValue(insp_r[0].alto);

	$.EscribaNombre.setValue(insp_r[0].nombre);

	if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
		/** 
		 * Revisamos si existe variable seltarea para saber si las fotos son desde la inspeccion o son datos dummy 
		 */
		var ID_150567229_trycatch = {
			error: function(e) {
				if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
			}
		};
		try {
			ID_150567229_trycatch.error = function(evento) {};
			insp_r[0].foto1 = "mini" + insp_r[0].foto1.substring(3);
			var foto_1 = '';
			var ID_73302648_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + seltarea.id_server);
			if (ID_73302648_d.exists() == true) {
				var ID_73302648_f = Ti.Filesystem.getFile(ID_73302648_d.resolve(), insp_r[0].foto1);
				if (ID_73302648_f.exists() == true) {
					foto_1 = ID_73302648_f.read();
				}
				ID_73302648_f = null;
			}
			ID_73302648_d = null;
		} catch (e) {
			ID_150567229_trycatch.error(e);
		}
		var ID_1107845015_trycatch = {
			error: function(e) {
				if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
			}
		};
		try {
			ID_1107845015_trycatch.error = function(evento) {};
			insp_r[0].foto2 = "mini" + insp_r[0].foto2.substring(3);
			var foto_2 = '';
			var ID_1678191401_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + seltarea.id_server);
			if (ID_1678191401_d.exists() == true) {
				var ID_1678191401_f = Ti.Filesystem.getFile(ID_1678191401_d.resolve(), insp_r[0].foto2);
				if (ID_1678191401_f.exists() == true) {
					foto_2 = ID_1678191401_f.read();
				}
				ID_1678191401_f = null;
			}
			ID_1678191401_d = null;
		} catch (e) {
			ID_1107845015_trycatch.error(e);
		}
		var ID_545308520_trycatch = {
			error: function(e) {
				if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
			}
		};
		try {
			ID_545308520_trycatch.error = function(evento) {};
			insp_r[0].foto3 = "mini" + insp_r[0].foto3.substring(3);
			var foto_3 = '';
			var ID_1187156852_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + seltarea.id_server);
			if (ID_1187156852_d.exists() == true) {
				var ID_1187156852_f = Ti.Filesystem.getFile(ID_1187156852_d.resolve(), insp_r[0].foto3);
				if (ID_1187156852_f.exists() == true) {
					foto_3 = ID_1187156852_f.read();
				}
				ID_1187156852_f = null;
			}
			ID_1187156852_d = null;
		} catch (e) {
			ID_545308520_trycatch.error(e);
		}
	} else {
		var ID_660206607_trycatch = {
			error: function(e) {
				if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
			}
		};
		try {
			ID_660206607_trycatch.error = function(evento) {};
			insp_r[0].foto1 = "mini" + insp_r[0].foto1.substring(3);
			var foto_1 = '';
			var ID_1315028819_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + 0);
			if (ID_1315028819_d.exists() == true) {
				var ID_1315028819_f = Ti.Filesystem.getFile(ID_1315028819_d.resolve(), insp_r[0].foto1);
				if (ID_1315028819_f.exists() == true) {
					foto_1 = ID_1315028819_f.read();
				}
				ID_1315028819_f = null;
			}
			ID_1315028819_d = null;
		} catch (e) {
			ID_660206607_trycatch.error(e);
		}
		var ID_878821508_trycatch = {
			error: function(e) {
				if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
			}
		};
		try {
			ID_878821508_trycatch.error = function(evento) {};
			insp_r[0].foto2 = "mini" + insp_r[0].foto2.substring(3);
			var foto_2 = '';
			var ID_1050482797_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + 0);
			if (ID_1050482797_d.exists() == true) {
				var ID_1050482797_f = Ti.Filesystem.getFile(ID_1050482797_d.resolve(), insp_r[0].foto2);
				if (ID_1050482797_f.exists() == true) {
					foto_2 = ID_1050482797_f.read();
				}
				ID_1050482797_f = null;
			}
			ID_1050482797_d = null;
		} catch (e) {
			ID_878821508_trycatch.error(e);
		}
		var ID_1874717670_trycatch = {
			error: function(e) {
				if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
			}
		};
		try {
			ID_1874717670_trycatch.error = function(evento) {};
			insp_r[0].foto3 = "mini" + insp_r[0].foto3.substring(3);
			var foto_3 = '';
			var ID_1242935605_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + 0);
			if (ID_1242935605_d.exists() == true) {
				var ID_1242935605_f = Ti.Filesystem.getFile(ID_1242935605_d.resolve(), insp_r[0].foto3);
				if (ID_1242935605_f.exists() == true) {
					foto_3 = ID_1242935605_f.read();
				}
				ID_1242935605_f = null;
			}
			ID_1242935605_d = null;
		} catch (e) {
			ID_1874717670_trycatch.error(e);
		}
	}
	if ((_.isObject(foto_1) || (_.isString(foto_1)) && !_.isEmpty(foto_1)) || _.isNumber(foto_1) || _.isBoolean(foto_1)) {
		/** 
		 * Revisamos que foto1, foto2 y foto3 contengan texto, por lo que si es asi, modificamos la imagen previa y ponemos la foto miniatura obtenida desde la memoria 
		 */
		var vista17_visible = true;

		if (vista17_visible == 'si') {
			vista17_visible = true;
		} else if (vista17_visible == 'no') {
			vista17_visible = false;
		}
		$.vista17.setVisible(vista17_visible);

		var imagen_imagen = foto_1;

		if (typeof imagen_imagen == 'string' && 'styles' in require('a4w') && imagen_imagen in require('a4w').styles['images']) {
			imagen_imagen = require('a4w').styles['images'][imagen_imagen];
		}
		$.imagen.setImage(imagen_imagen);

		if ((Ti.Platform.manufacturer) == L('x3791547856_traducir', 'samsung')) {
			/** 
			 * Revisamos si el equipo es samsung, si es asi, rotamos la imagen ya que es la unica marca que saca mal rotadas las imagenes 
			 */
			var vista17_rotar = 90;

			var setRotacion = function(angulo) {
				$.vista17.setTransform(Ti.UI.create2DMatrix().rotate(angulo));
			};
			setRotacion(vista17_rotar);

		}
	}
	if ((_.isObject(foto_2) || (_.isString(foto_2)) && !_.isEmpty(foto_2)) || _.isNumber(foto_2) || _.isBoolean(foto_2)) {
		var vista18_visible = true;

		if (vista18_visible == 'si') {
			vista18_visible = true;
		} else if (vista18_visible == 'no') {
			vista18_visible = false;
		}
		$.vista18.setVisible(vista18_visible);

		var imagen2_imagen = foto_2;

		if (typeof imagen2_imagen == 'string' && 'styles' in require('a4w') && imagen2_imagen in require('a4w').styles['images']) {
			imagen2_imagen = require('a4w').styles['images'][imagen2_imagen];
		}
		$.imagen2.setImage(imagen2_imagen);

		if ((Ti.Platform.manufacturer) == L('x3791547856_traducir', 'samsung')) {
			var vista18_rotar = 90;

			var setRotacion = function(angulo) {
				$.vista18.setTransform(Ti.UI.create2DMatrix().rotate(angulo));
			};
			setRotacion(vista18_rotar);

		}
	}
	if ((_.isObject(foto_3) || (_.isString(foto_3)) && !_.isEmpty(foto_3)) || _.isNumber(foto_3) || _.isBoolean(foto_3)) {
		var vista19_visible = true;

		if (vista19_visible == 'si') {
			vista19_visible = true;
		} else if (vista19_visible == 'no') {
			vista19_visible = false;
		}
		$.vista19.setVisible(vista19_visible);

		var imagen3_imagen = foto_3;

		if (typeof imagen3_imagen == 'string' && 'styles' in require('a4w') && imagen3_imagen in require('a4w').styles['images']) {
			imagen3_imagen = require('a4w').styles['images'][imagen3_imagen];
		}
		$.imagen3.setImage(imagen3_imagen);

		if ((Ti.Platform.manufacturer) == L('x3791547856_traducir', 'samsung')) {
			var vista19_rotar = 90;

			var setRotacion = function(angulo) {
				$.vista19.setTransform(Ti.UI.create2DMatrix().rotate(angulo));
			};
			setRotacion(vista19_rotar);

		}
	}
	/** 
	 * Limpiamos variables 
	 */
	foto_1 = null;
	foto_2 = null;
	foto_3 = null;
	/** 
	 * Modificamos en 0.1 segundos el color del statusbar 
	 */
	var ID_1795638484_func = function() {
		var editarrecinto_statusbar = '#7E6EE0';

		var setearStatusColor = function(editarrecinto_statusbar) {
			if (OS_IOS) {
				if (editarrecinto_statusbar == 'light' || editarrecinto_statusbar == 'claro') {
					$.editarrecinto_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.LIGHT_CONTENT);
				} else if (editarrecinto_statusbar == 'grey' || editarrecinto_statusbar == 'gris' || editarrecinto_statusbar == 'gray') {
					$.editarrecinto_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.GREY);
				} else if (editarrecinto_statusbar == 'oscuro' || editarrecinto_statusbar == 'dark') {
					$.editarrecinto_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.DEFAULT);
				}
			} else if (OS_ANDROID) {
				abx.setStatusbarColor(editarrecinto_statusbar);
			}
		};
		setearStatusColor(editarrecinto_statusbar);

	};
	var ID_1795638484 = setTimeout(ID_1795638484_func, 1000 * 0.1);
})();

function Postlayout_editarrecinto(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var ID_1294585966_func = function() {
		/** 
		 * Una vez ya cargada la tarea, dejamos la variable limpia para poder abrir otras pantallas 
		 */
		require('vars')['var_abriendo'] = '';
	};
	var ID_1294585966 = setTimeout(ID_1294585966_func, 1000 * 0.2);

}
if (OS_IOS || OS_ANDROID) {
	$.editarrecinto.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}
//$.editarrecinto.open();
