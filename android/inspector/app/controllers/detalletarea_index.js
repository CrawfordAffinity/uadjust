var _bind4section = {};
var _list_templates = {
	"elemento": {
		"vista2": {},
		"Label3": {
			"text": "{nombre}"
		},
		"Label2": {
			"text": "{id_server}"
		},
		"Label": {
			"text": "{id_segured}"
		},
		"vista": {}
	},
	"dano": {
		"Label2": {
			"text": "{id}"
		},
		"vista29": {},
		"Label": {
			"text": "{nombre}"
		}
	},
	"pborrar": {
		"vista5": {},
		"imagen": {},
		"vista3": {},
		"Label4": {
			"text": "{id}"
		},
		"Label3": {
			"text": "{nombre}"
		},
		"vista4": {},
		"vista22": {},
		"vista23": {},
		"vista24": {},
		"imagen2": {}
	},
	"contenido": {
		"vista2": {},
		"Label2": {
			"text": "{id}"
		},
		"Label": {
			"text": "{nombre}"
		}
	},
	"recinto": {
		"vista2": {},
		"Label": {
			"text": "{nombre}"
		},
		"Label2": {
			"text": "{id}"
		}
	},
	"nivel": {
		"Label2": {
			"text": "{id}"
		},
		"Label": {
			"text": "{nombre}"
		},
		"vista21": {}
	}
};

var _activity;
if (OS_ANDROID) {
	_activity = $.inicio.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'inicio';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.inicio.addEventListener('open', function(e) {});
}
$.inicio.orientationModes = [Titanium.UI.PORTRAIT];


$.widgetBarra.init({
	titulo: L('x334754396_traducir', 'DETALLE DE TAREA'),
	__id: 'ALL1426322055',
	info: L('', ''),
	fondo: 'fondoblanco',
	colortitulo: 'negro',
	modal: L('', ''),
	onpresiono: Presiono_widgetBarra
});

function Presiono_widgetBarra(e) {

	var evento = e;
	/** 
	 * Eenviamos a ver datos del asegurado 
	 */
	var nulo = Alloy.createController("datosasegurado_index", {}).getView();
	nulo.open({
		modal: true
	});

	nulo = null;

}

$.widgetMapa.init({
	__id: 'ALL658148125',
	onlisto: Listo_widgetMapa,
	externo: true,
	label_a: L('x3904355907', 'a'),
	onerror: Error_widgetMapa,
	_bono: L('x2764662954_traducir', 'Bono adicional:'),
	bono: L('', ''),
	ondatos: Datos_widgetMapa
});

function Listo_widgetMapa(e) {

	var evento = e;
	if (Ti.App.deployType != 'production') console.log('widget mapa ha llamado evento \'listo\'', {});

}

function Error_widgetMapa(e) {

	var evento = e;
	if (Ti.App.deployType != 'production') console.log('ha ocurrido un error con el mapa', {
		"evento": evento
	});

}

function Datos_widgetMapa(e) {

	var evento = e;
	if (Ti.App.deployType != 'production') console.log(String.format(L('x3211431303_traducir', 'datos recibidos desde widget mapa: %1$s'), evento.ruta_distancia.toString()), {
		"evento": evento
	});

}

function Longpress_vista2(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var bloquear_inspeccion = ('bloquear_inspeccion' in require('vars')) ? require('vars')['bloquear_inspeccion'] : '';
	if (Ti.App.deployType != 'production') console.log('psb longpress en boton', {
		"bloquear": bloquear_inspeccion
	});
	if (bloquear_inspeccion == false || bloquear_inspeccion == 'false') {
		/** 
		 * Recuperamos variables. seltarea contiene el detalle de la tarea seleccionada, url_server contiene la url de uadjust, inspector contiene el detalle del inspector, iniciar_seguimiento flag para cambiar el estado del seguimiento, long_activo flag para el cambio de color del boton 
		 */
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
		var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
		var iniciar_seguimiento = ('iniciar_seguimiento' in require('vars')) ? require('vars')['iniciar_seguimiento'] : '';
		require('vars')['long_activo'] = L('x4261170317', 'true');
		if (Ti.App.deployType != 'production') console.log('psb longpress en boton insp-false', {
			"bloquear": bloquear_inspeccion
		});
		if (iniciar_seguimiento == true || iniciar_seguimiento == 'true') {
			if (Ti.App.deployType != 'production') console.log('psb boton dice iniciar_seguimiento en true, cambiamos cosas', {
				"bloquear": bloquear_inspeccion
			});
			/** 
			 * Informamos que debemos seguir esta tarea 
			 */
			require('vars')['seguir_tarea'] = seltarea.id_server;
			/** 
			 * Necesario para refrescar tareas en cierre de ventana 
			 */
			require('vars')['seguimiento_cambiado'] = L('x4261170317', 'true');
			/** 
			 * Actualizamos tabla local, refrescamos tareas y transformamos en mantener para iniciar. Cambiamos todas las tareas nuestras que tienen estado 4 a estado 3 (por si otra tarea tenia seguimiento antes) 
			 */
			var consultarModelo_i = Alloy.createCollection('tareas');
			var consultarModelo_i_where = 'estado_tarea=4';
			consultarModelo_i.fetch({
				query: 'SELECT * FROM tareas WHERE estado_tarea=4'
			});
			var tareas = require('helper').query2array(consultarModelo_i);
			var db = Ti.Database.open(consultarModelo_i.config.adapter.db_name);
			if (consultarModelo_i_where == '') {
				var sql = 'UPDATE ' + consultarModelo_i.config.adapter.collection_name + ' SET estado_tarea=3';
			} else {
				var sql = 'UPDATE ' + consultarModelo_i.config.adapter.collection_name + ' SET estado_tarea=3 WHERE ' + consultarModelo_i_where;
			}
			db.execute(sql);
			db.close();
			var consultarModelo2_i = Alloy.createCollection('tareas');
			var consultarModelo2_i_where = 'id=\'' + seltarea.id + '\'';
			consultarModelo2_i.fetch({
				query: 'SELECT * FROM tareas WHERE id=\'' + seltarea.id + '\''
			});
			var tareas = require('helper').query2array(consultarModelo2_i);
			var db = Ti.Database.open(consultarModelo2_i.config.adapter.db_name);
			if (consultarModelo2_i_where == '') {
				var sql = 'UPDATE ' + consultarModelo2_i.config.adapter.collection_name + ' SET estado_tarea=4';
			} else {
				var sql = 'UPDATE ' + consultarModelo2_i.config.adapter.collection_name + ' SET estado_tarea=4 WHERE ' + consultarModelo2_i_where;
			}
			db.execute(sql);
			db.close();
			/** 
			 * Refrescamos tareas de menu 
			 */
			var ID_170851302_func = function() {

				Alloy.Events.trigger('_refrescar_tareas');
			};
			var ID_170851302 = setTimeout(ID_170851302_func, 1000 * 0.2);
			/** 
			 * Mostramos monito 
			 */

			$.widgetMapa.update({
				monito: 'true'
			});
			require('vars')['long_activo'] = L('x734881840_traducir', 'false');
			/** 
			 * transformamos en boton azul de mantener para iniciar 
			 */
			require('vars')['iniciar_seguimiento'] = L('x734881840_traducir', 'false');
			if (Ti.App.deployType != 'production') console.log('iniciar seguimiento en false', {});
			var vista2_estilo = 'fondoazul';

			var setEstilo = function(clase) {
				if ('styles' in require('a4w') && clase in require('a4w').styles['classes']) {
					try {
						$.vista2.applyProperties(require('a4w').styles['classes'][clase]);
					} catch (sete_err) {}
				}
			};
			setEstilo(vista2_estilo);

			$.vista2.setBorderColor('#2D9EDB');

			var mantener_iniciar = L('x2905032041_traducir', 'MANTENER PARA INICIAR');
			$.MANTENERPARA.setText(mantener_iniciar);

			var tarea_inspeccionar = L('x4245846284_traducir', 'Tip. Nunca dejes una tarea sin inspeccionar. El no hacerlo te afecta directamente');

			$.widgetMono.update({
				texto: tarea_inspeccionar
			});
			var proceso_insp = L('x3843115318_traducir', 'Se iniciara el proceso de inspeccion');
			$.SeIniciarel.setText(proceso_insp);

			var gps_error = ('gps_error' in require('vars')) ? require('vars')['gps_error'] : '';
			if (gps_error == true || gps_error == 'true') {
				/** 
				 * Avisamos a backend la ubicacion del inspector 
				 */
				var consultarURL = {};

				consultarURL.success = function(e) {
					var elemento = e,
						valor = e;
					if (Ti.App.deployType != 'production') console.log('respuesta exitosa de guardarUbicacion en iniciarSeguimiento', {
						"datos": elemento
					});
					elemento = null, valor = null;
				};

				consultarURL.error = function(e) {
					var elemento = e,
						valor = e;
					if (Ti.App.deployType != 'production') console.log('respuesta fallida de guardarUbicacion en iniciarSeguimiento', {
						"datos": elemento
					});
					elemento = null, valor = null;
				};
				require('helper').ajaxUnico('consultarURL', '' + String.format(L('x4011689324', '%1$sguardarUbicacion'), url_server.toString()) + '', 'POST', {
					id_inspector: inspector.id_server,
					id_tarea: seltarea.id_server,
					lat: -1,
					lon: -1
				}, 15000, consultarURL);
			} else {
				var gps_latitud = ('gps_latitud' in require('vars')) ? require('vars')['gps_latitud'] : '';
				var gps_longitud = ('gps_longitud' in require('vars')) ? require('vars')['gps_longitud'] : '';
				var consultarURL2 = {};

				consultarURL2.success = function(e) {
					var elemento = e,
						valor = e;
					if (Ti.App.deployType != 'production') console.log('respuesta exitosa de guardarUbicacion en iniciarSeguimiento', {
						"datos": elemento
					});
					elemento = null, valor = null;
				};

				consultarURL2.error = function(e) {
					var elemento = e,
						valor = e;
					if (Ti.App.deployType != 'production') console.log('respuesta fallida de guardarUbicacion en iniciarSeguimiento', {
						"datos": elemento
					});
					elemento = null, valor = null;
				};
				require('helper').ajaxUnico('consultarURL2', '' + String.format(L('x4011689324', '%1$sguardarUbicacion'), url_server.toString()) + '', 'POST', {
					id_inspector: inspector.id_server,
					id_tarea: seltarea.id_server,
					lat: gps_latitud,
					lon: gps_longitud
				}, 15000, consultarURL2);
			}
		} else {
			var vista2_estilo = 'fondoazul';

			var setEstilo = function(clase) {
				if ('styles' in require('a4w') && clase in require('a4w').styles['classes']) {
					try {
						$.vista2.applyProperties(require('a4w').styles['classes'][clase]);
					} catch (sete_err) {}
				}
			};
			setEstilo(vista2_estilo);

			$.vista2.setBorderColor('#2D9EDB');

			if (Ti.App.deployType != 'production') console.log('alguna razon para que se cambie el monito', {});
			Alloy.createController("fotosrequeridas_index", {
				'_open': 'false'
			}).getView();
		}
	}

}

function Touchstart_vista2(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var bloquear_inspeccion = ('bloquear_inspeccion' in require('vars')) ? require('vars')['bloquear_inspeccion'] : '';
	if (bloquear_inspeccion == false || bloquear_inspeccion == 'false') {
		var iniciar_seguimiento = ('iniciar_seguimiento' in require('vars')) ? require('vars')['iniciar_seguimiento'] : '';
		if (iniciar_seguimiento == true || iniciar_seguimiento == 'true') {
			var vista2_estilo = 'fondonaranjo';

			var setEstilo = function(clase) {
				if ('styles' in require('a4w') && clase in require('a4w').styles['classes']) {
					try {
						$.vista2.applyProperties(require('a4w').styles['classes'][clase]);
					} catch (sete_err) {}
				}
			};
			setEstilo(vista2_estilo);

			$.vista2.setBorderColor('#FCBD83');

		} else {
			var vista2_estilo = 'fondoceleste';

			var setEstilo = function(clase) {
				if ('styles' in require('a4w') && clase in require('a4w').styles['classes']) {
					try {
						$.vista2.applyProperties(require('a4w').styles['classes'][clase]);
					} catch (sete_err) {}
				}
			};
			setEstilo(vista2_estilo);

			$.vista2.setBorderColor('#8BC9E8');

		}
	}

}

function Touchend_vista2(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	/** 
	 * Dejamos boton como estaba 
	 */
	var bloquear_inspeccion = ('bloquear_inspeccion' in require('vars')) ? require('vars')['bloquear_inspeccion'] : '';
	var long_activo = ('long_activo' in require('vars')) ? require('vars')['long_activo'] : '';
	if (bloquear_inspeccion == false || bloquear_inspeccion == 'false') {
		if (long_activo == false || long_activo == 'false') {
			var iniciar_seguimiento = ('iniciar_seguimiento' in require('vars')) ? require('vars')['iniciar_seguimiento'] : '';
			if (iniciar_seguimiento == true || iniciar_seguimiento == 'true') {
				var vista2_estilo = 'fondoamarillo';

				var setEstilo = function(clase) {
					if ('styles' in require('a4w') && clase in require('a4w').styles['classes']) {
						try {
							$.vista2.applyProperties(require('a4w').styles['classes'][clase]);
						} catch (sete_err) {}
					}
				};
				setEstilo(vista2_estilo);

				$.vista2.setBorderColor('#F8DA54');

			} else {
				var vista2_estilo = 'fondoazul';

				var setEstilo = function(clase) {
					if ('styles' in require('a4w') && clase in require('a4w').styles['classes']) {
						try {
							$.vista2.applyProperties(require('a4w').styles['classes'][clase]);
						} catch (sete_err) {}
					}
				};
				setEstilo(vista2_estilo);

				$.vista2.setBorderColor('#2D9EDB');

			}
		}
	}

}

$.widgetMono.init({
	__id: 'ALL859578408',
	texto: L('x1739881219_traducir', 'Tip: Nunca dejes una tarea sin inspeccionar. El no hacerlo te afecta directamente'),
	top: 10,
	tipo: '_tip'
});


(function() {
	if (Ti.App.deployType != 'production') console.log('argumentos detalletarea', {
		"modelo": args
	});
	require('vars')['long_activo'] = L('x734881840_traducir', 'false');
	require('vars')['seguimiento_cambiado'] = L('x734881840_traducir', 'false');
	/** 
	 * Filtramos el id de la tarea con los parameros desde la pantalla anterior 
	 */
	var consultarModelo3_i = Alloy.createCollection('tareas');
	var consultarModelo3_i_where = 'id=\'' + args._id + '\'';
	consultarModelo3_i.fetch({
		query: 'SELECT * FROM tareas WHERE id=\'' + args._id + '\''
	});
	var tareas = require('helper').query2array(consultarModelo3_i);
	if (tareas && tareas.length) {
		/** 
		 * Esto es util para la sub-pantalla datos del asegurado 
		 */
		require('vars')['seltarea'] = tareas[0];
		/** 
		 * Recuperamos los valores defaults 
		 */
		var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
		require('vars')['bloquear_inspeccion'] = L('x734881840_traducir', 'false');
		if (Ti.App.deployType != 'production') console.log('adaptamos datos', {});
		/** 
		 * Creamos variable de estructura para cargar datos de la tarea 
		 */
		var info = {
			direccion: tareas[0].direccion,
			ciudad_pais: String.format(L('x1487588533_traducir', '%1$s, %2$s'), (tareas[0].nivel_2) ? tareas[0].nivel_2.toString() : '', (tareas[0].pais_texto) ? tareas[0].pais_texto.toString() : ''),
			distancia: tareas[0].distance
		};
		if ((_.isObject(tareas[0].nivel_2) || _.isString(tareas[0].nivel_2)) && _.isEmpty(tareas[0].nivel_2)) {
			/** 
			 * Definimos el ultimo nivel de la tarea y lo guardamos en la estructura como comuna 
			 */
			var info = _.extend(info, {
				comuna: tareas[0].nivel_1
			});
		} else if ((_.isObject(tareas[0].nivel_3) || _.isString(tareas[0].nivel_3)) && _.isEmpty(tareas[0].nivel_3)) {
			var info = _.extend(info, {
				comuna: tareas[0].nivel_2
			});
		} else if ((_.isObject(tareas[0].nivel_4) || _.isString(tareas[0].nivel_4)) && _.isEmpty(tareas[0].nivel_4)) {
			var info = _.extend(info, {
				comuna: tareas[0].nivel_3
			});
		} else if ((_.isObject(tareas[0].nivel_5) || _.isString(tareas[0].nivel_5)) && _.isEmpty(tareas[0].nivel_5)) {
			var info = _.extend(info, {
				comuna: tareas[0].nivel_4
			});
		} else {
			var info = _.extend(info, {
				comuna: tareas[0].nivel_5
			});
		}
		if (Ti.App.deployType != 'production') console.log('actualizamos mapa con direccion', {});
		/** 
		 * Actualizamos widget de mapa con los datos de la tarea 
		 */

		$.widgetMapa.update({
			tipo: 'ubicacion',
			latitud: tareas[0].lat,
			longitud: tareas[0].lon,
			ruta: 'true',
			direccion: info.direccion,
			comuna: info.comuna,
			ciudad: info.ciudad_pais,
			distancia: info.distancia,
			externo: 'true'
		});
		/** 
		 * Si seguir_tarea no es igual a nuestro id_server: significa que no estamos siguiendola por lo que boton debe decir iniciar seguimiento, en caso contrario, debemos mostrar monito porque se esta siguiendo. 
		 */
		var seguir_tarea = ('seguir_tarea' in require('vars')) ? require('vars')['seguir_tarea'] : '';
		if (seguir_tarea != tareas[0].id_server) {
			var vista2_estilo = 'fondoamarillo';

			var setEstilo = function(clase) {
				if ('styles' in require('a4w') && clase in require('a4w').styles['classes']) {
					try {
						$.vista2.applyProperties(require('a4w').styles['classes'][clase]);
					} catch (sete_err) {}
				}
			};
			setEstilo(vista2_estilo);

			$.vista2.setBorderColor('#F8DA54');

			var ini_seguimiento = L('x659251484_traducir', 'INICIAR SEGUIMIENTO');
			$.MANTENERPARA.setText(ini_seguimiento);

			var ini_aviso = L('x2536488944_traducir', 'Tip: Al iniciar seguimiento, le avisaremos al cliente que vas en camino.');

			$.widgetMono.update({
				texto: ini_aviso
			});
			var presionado_iniciar = L('x4123109422_traducir', 'Mantenga presionado para iniciar el seguimiento');
			$.SeIniciarel.setText(presionado_iniciar);

			require('vars')['iniciar_seguimiento'] = L('x4261170317', 'true');

			$.widgetMapa.update({
				monito: 'false'
			});
		} else {
			/** 
			 * esta tarea esta actualmente siendo seguida, en este estado debe decir mantener para iniciar (default) 
			 */
			require('vars')['iniciar_seguimiento'] = L('x734881840_traducir', 'false');
			/** 
			 * Mostramos monito de caminando en la pantalla 
			 */

			$.widgetMapa.update({
				monito: 'true'
			});
		}
		var moment = require('alloy/moment');
		var fecha_hoy = moment(new Date()).format('YYYY-MM-DD');
		if (tareas[0].fecha_tarea != fecha_hoy) {
			/** 
			 * La fecha no es de hoy, se bloquea boton y se indica razon 
			 */
			var vista2_estilo = 'fondoplomo2';

			var setEstilo = function(clase) {
				if ('styles' in require('a4w') && clase in require('a4w').styles['classes']) {
					try {
						$.vista2.applyProperties(require('a4w').styles['classes'][clase]);
					} catch (sete_err) {}
				}
			};
			setEstilo(vista2_estilo);

			$.vista2.setBorderColor('#E6E6E6');

			var bloqueo_fecha = L('x3389196696_traducir', 'BLOQUEADA POR FECHA');
			$.MANTENERPARA.setText(bloqueo_fecha);

			var insp_otrodia = L('x3359418254_traducir', 'Tip: No puedes iniciar inspecciones para tareas que no son para el dia');

			$.widgetMono.update({
				texto: insp_otrodia
			});
			var moment = require('alloy/moment');
			var formatearFecha2 = tareas[0].fecha_tarea;
			if (typeof formatearFecha2 === 'string' || typeof formatearFecha2 === 'number') {
				var fecha_mostrar = moment(formatearFecha2, 'YYYY-MM-DD').format('DD-MM-YYYY');
			} else {
				var fecha_mostrar = moment(formatearFecha2).format('DD-MM-YYYY');
			}
			var tarea_realizar_fecha = L('x4270263955_traducir', 'Esta tarea se debe realizar el');
			$.SeIniciarel.setText(String.format(L('x1045525933', '%1$s %2$s'), (tarea_realizar_fecha) ? tarea_realizar_fecha.toString() : '', (fecha_mostrar) ? fecha_mostrar.toString() : ''));

			require('vars')['bloquear_inspeccion'] = L('x4261170317', 'true');
		}
		var confirmarpush = JSON.parse(Ti.App.Properties.getString('confirmarpush'));
		if (Ti.App.deployType != 'production') console.log('detalle tarea: confirmar push dice', {
			"confirmar_push": confirmarpush
		});
		if (confirmarpush == true || confirmarpush == 'true') {
			require('vars')['bloquear_inspeccion'] = L('x4261170317', 'true');
			var vista2_estilo = 'fondoplomo2';

			var setEstilo = function(clase) {
				if ('styles' in require('a4w') && clase in require('a4w').styles['classes']) {
					try {
						$.vista2.applyProperties(require('a4w').styles['classes'][clase]);
					} catch (sete_err) {}
				}
			};
			setEstilo(vista2_estilo);

			$.vista2.setBorderColor('#E6E6E6');

			var bloqueo_confirmacion = L('x3736160328_traducir', 'BLOQUEADA POR CONFIRMACION');
			$.MANTENERPARA.setText(bloqueo_confirmacion);

			var confirmar_tarea = L('x3795248976_traducir', 'Tip: No puedes iniciar inspecciones sin confirmar tus tareas del dia');

			$.widgetMono.update({
				texto: confirmar_tarea
			});
			var confirmar_tareas_hoy = L('x2795361479_traducir', 'Se debe confirmar primero las tareas');
			$.SeIniciarel.setText(confirmar_tarea_hoy);

		}
		if (_.isNumber(tareas[0].bono) && _.isNumber(0) && tareas[0].bono > 0) {

			$.widgetMapa.update({
				bono: tareas[0].bono
			});
		}
	}
})();



_my_events['_cerrar_insp,ID_231485652'] = function(evento) {
	if (!_.isUndefined(evento.pantalla)) {
		if (evento.pantalla == L('x2151250992_traducir', 'detalle')) {
			if (Ti.App.deployType != 'production') console.log('debug cerrando detalle tarea', {});

			var ID_962933245_trycatch = {
				error: function(e) {
					if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
				}
			};
			try {
				ID_962933245_trycatch.error = function(evento) {
					if (Ti.App.deployType != 'production') console.log('error cerrando detalle tarea', {});
				};
				$.inicio.close();
			} catch (e) {
				ID_962933245_trycatch.error(e);
			}
		}
	} else {
		if (Ti.App.deployType != 'production') console.log('debug cerrando (todas) detalle tarea', {});

		var ID_1962200830_trycatch = {
			error: function(e) {
				if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
			}
		};
		try {
			ID_1962200830_trycatch.error = function(evento) {
				if (Ti.App.deployType != 'production') console.log('error cerrando detalle tarea', {});
			};
			$.inicio.close();
		} catch (e) {
			ID_1962200830_trycatch.error(e);
		}
	}
};
Alloy.Events.on('_cerrar_insp', _my_events['_cerrar_insp,ID_231485652']);

function Postlayout_inicio(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var ID_1294585966_func = function() {
		require('vars')['var_abriendo'] = '';
	};
	var ID_1294585966 = setTimeout(ID_1294585966_func, 1000 * 0.2);

}
if (OS_IOS || OS_ANDROID) {
	$.inicio.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}
//$.inicio.open();
