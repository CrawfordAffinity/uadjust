var _bind4section = {};
var _list_templates = {};

var _activity;
if (OS_ANDROID) {
	_activity = $.editar_disponibilidad.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'editar_disponibilidad';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.editar_disponibilidad.addEventListener('open', function(e) {});
}
$.editar_disponibilidad.orientationModes = [Titanium.UI.PORTRAIT];


$.widgetBarra.init({
	titulo: L('x2688733458_traducir', 'DISPONIBILIDAD'),
	__id: 'ALL1996257419',
	textoderecha: L('x3683967382_traducir', 'Siguiente'),
	fondo: 'fondoblanco',
	colortitulo: 'negro',
	modal: L('', ''),
	onpresiono: Presiono_widgetBarra,
	colortextoderecha: 'azul'
});

function Presiono_widgetBarra(e) {

	var evento = e;
	/** 
	 * Revisamos cual fue la disponibilidad del inspector, dependiendo de eso es si pasamos a la proxima pantalla o enviamos informacion al servidor 
	 */
	var seleccionado = ('seleccionado' in require('vars')) ? require('vars')['seleccionado'] : '';
	if (seleccionado == 1 || seleccionado == '1') {
		if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
			/** 
			 * Revisamos que haya conexion a internet, recuperamos las variables de la disponibilidad de salir de la ciudad y pais, la url de uadjust y el detalle del inspector. 
			 */
			var fueraciudad = ('fueraciudad' in require('vars')) ? require('vars')['fueraciudad'] : '';
			var fuerapais = ('fuerapais' in require('vars')) ? require('vars')['fuerapais'] : '';
			var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
			var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
			/** 
			 * Llenamos los datos requeridos por el servidor y avisamos que tiene disponibilidad full time y marcamos todos los dias como disponibles 
			 */
			var consultarURL = {};
			console.log('DEBUG WEB: requesting url:' + String.format(L('x2304081372', '%1$seditarPerfilTipo2'), url_server.toString()) + ' with data:', {
				_method: 'POST',
				_params: {
					tipo_cambio: 2,
					id_inspector: inspector.id_server,
					disponibilidad_viajar_pais: fuerapais,
					disponibilidad_viajar_ciudad: fueraciudad,
					d1: 1,
					d2: 1,
					d3: 1,
					d4: 1,
					d5: 1,
					d6: 1,
					d7: 1,
					disponibilidad: 1,
					disponibilidad_horas: inspector.disponibilidad_horas
				},
				_timeout: '15000'
			});

			consultarURL.success = function(e) {
				var elemento = e,
					valor = e;
				if (Ti.App.deployType != 'production') console.log('Resultado consulta perfiltipo2', {
					"elemento": elemento
				});
				if (elemento.error == 0 || elemento.error == '0') {
					/** 
					 * Modificamos la tabla con la disponibilidad fulltime 
					 */
					var consultarModelo_i = Alloy.createCollection('inspectores');
					var consultarModelo_i_where = '';
					consultarModelo_i.fetch();
					var inspector_list = require('helper').query2array(consultarModelo_i);
					var fueraciudad = ('fueraciudad' in require('vars')) ? require('vars')['fueraciudad'] : '';
					var fuerapais = ('fuerapais' in require('vars')) ? require('vars')['fuerapais'] : '';
					var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
					var db = Ti.Database.open(consultarModelo_i.config.adapter.db_name);
					if (consultarModelo_i_where == '') {
						var sql = 'UPDATE ' + consultarModelo_i.config.adapter.collection_name + ' SET disponibilidad_viajar_pais=\'' + fuerapais + '\'';
					} else {
						var sql = 'UPDATE ' + consultarModelo_i.config.adapter.collection_name + ' SET disponibilidad_viajar_pais=\'' + fuerapais + '\' WHERE ' + consultarModelo_i_where;
					}
					db.execute(sql);
					if (consultarModelo_i_where == '') {
						var sql = 'UPDATE ' + consultarModelo_i.config.adapter.collection_name + ' SET disponibilidad_viajar_ciudad=\'' + fueraciudad + '\'';
					} else {
						var sql = 'UPDATE ' + consultarModelo_i.config.adapter.collection_name + ' SET disponibilidad_viajar_ciudad=\'' + fueraciudad + '\' WHERE ' + consultarModelo_i_where;
					}
					db.execute(sql);
					if (consultarModelo_i_where == '') {
						var sql = 'UPDATE ' + consultarModelo_i.config.adapter.collection_name + ' SET disponibilidad=\'' + 1 + '\'';
					} else {
						var sql = 'UPDATE ' + consultarModelo_i.config.adapter.collection_name + ' SET disponibilidad=\'' + 1 + '\' WHERE ' + consultarModelo_i_where;
					}
					db.execute(sql);
					if (consultarModelo_i_where == '') {
						var sql = 'UPDATE ' + consultarModelo_i.config.adapter.collection_name + ' SET d1=\'' + 1 + '\'';
					} else {
						var sql = 'UPDATE ' + consultarModelo_i.config.adapter.collection_name + ' SET d1=\'' + 1 + '\' WHERE ' + consultarModelo_i_where;
					}
					db.execute(sql);
					if (consultarModelo_i_where == '') {
						var sql = 'UPDATE ' + consultarModelo_i.config.adapter.collection_name + ' SET d2=\'' + 1 + '\'';
					} else {
						var sql = 'UPDATE ' + consultarModelo_i.config.adapter.collection_name + ' SET d2=\'' + 1 + '\' WHERE ' + consultarModelo_i_where;
					}
					db.execute(sql);
					if (consultarModelo_i_where == '') {
						var sql = 'UPDATE ' + consultarModelo_i.config.adapter.collection_name + ' SET d3=\'' + 1 + '\'';
					} else {
						var sql = 'UPDATE ' + consultarModelo_i.config.adapter.collection_name + ' SET d3=\'' + 1 + '\' WHERE ' + consultarModelo_i_where;
					}
					db.execute(sql);
					if (consultarModelo_i_where == '') {
						var sql = 'UPDATE ' + consultarModelo_i.config.adapter.collection_name + ' SET d4=\'' + 1 + '\'';
					} else {
						var sql = 'UPDATE ' + consultarModelo_i.config.adapter.collection_name + ' SET d4=\'' + 1 + '\' WHERE ' + consultarModelo_i_where;
					}
					db.execute(sql);
					if (consultarModelo_i_where == '') {
						var sql = 'UPDATE ' + consultarModelo_i.config.adapter.collection_name + ' SET d5=\'' + 1 + '\'';
					} else {
						var sql = 'UPDATE ' + consultarModelo_i.config.adapter.collection_name + ' SET d5=\'' + 1 + '\' WHERE ' + consultarModelo_i_where;
					}
					db.execute(sql);
					if (consultarModelo_i_where == '') {
						var sql = 'UPDATE ' + consultarModelo_i.config.adapter.collection_name + ' SET d6=\'' + 1 + '\'';
					} else {
						var sql = 'UPDATE ' + consultarModelo_i.config.adapter.collection_name + ' SET d6=\'' + 1 + '\' WHERE ' + consultarModelo_i_where;
					}
					db.execute(sql);
					if (consultarModelo_i_where == '') {
						var sql = 'UPDATE ' + consultarModelo_i.config.adapter.collection_name + ' SET d7=\'' + 1 + '\'';
					} else {
						var sql = 'UPDATE ' + consultarModelo_i.config.adapter.collection_name + ' SET d7=\'' + 1 + '\' WHERE ' + consultarModelo_i_where;
					}
					db.execute(sql);
					db.close();
					/** 
					 * Actualizamos la variable inspector con los datos recien ingresados 
					 */
					var consultarModelo2_i = Alloy.createCollection('inspectores');
					var consultarModelo2_i_where = '';
					consultarModelo2_i.fetch();
					var inspector_list = require('helper').query2array(consultarModelo2_i);
					require('vars')['inspector'] = inspector_list[0];
					/** 
					 * Limpiamos memoria 
					 */
					inspector_list = null;
					/** 
					 * Cerramos la pantalla 
					 */
					$.editar_disponibilidad.close();
				} else {
					var preguntarAlerta_opts = [L('x1518866076_traducir', 'Aceptar')];
					var preguntarAlerta = Ti.UI.createAlertDialog({
						title: L('x3237162386_traducir', 'Atencion'),
						message: '' + String.format(L('x921749426_traducir', 'Error %1$s'), elemento.mensaje.toString()) + '',
						buttonNames: preguntarAlerta_opts
					});
					preguntarAlerta.addEventListener('click', function(e) {
						var abcx = preguntarAlerta_opts[e.index];
						abcx = null;

						e.source.removeEventListener("click", arguments.callee);
					});
					preguntarAlerta.show();
				}
				elemento = null, valor = null;
			};

			consultarURL.error = function(e) {
				var elemento = e,
					valor = e;
				if (Ti.App.deployType != 'production') console.log('Hubo un error', {
					"elemento": elemento
				});
				elemento = null, valor = null;
			};
			require('helper').ajaxUnico('consultarURL', '' + String.format(L('x2304081372', '%1$seditarPerfilTipo2'), url_server.toString()) + '', 'POST', {
				tipo_cambio: 2,
				id_inspector: inspector.id_server,
				disponibilidad_viajar_pais: fuerapais,
				disponibilidad_viajar_ciudad: fueraciudad,
				d1: 1,
				d2: 1,
				d3: 1,
				d4: 1,
				d5: 1,
				d6: 1,
				d7: 1,
				disponibilidad: 1,
				disponibilidad_horas: inspector.disponibilidad_horas
			}, 15000, consultarURL);
		} else {
			var preguntarAlerta2_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta2 = Ti.UI.createAlertDialog({
				title: L('x3071602690_traducir', 'No hay internet'),
				message: L('x2846685350_traducir', 'No se pueden efectuar los cambios sin conexion.'),
				buttonNames: preguntarAlerta2_opts
			});
			preguntarAlerta2.addEventListener('click', function(e) {
				var suu = preguntarAlerta2_opts[e.index];
				suu = null;

				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta2.show();
		}
	} else {
		/** 
		 * Creamos un flag para evitar que la pantalla se abra dos veces 
		 */
		var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
		if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
			require('vars')['var_abriendo'] = L('x4261170317', 'true');
			var nulo = Alloy.createController("editar_parttime", {}).getView();
			$.editar_disponibilidad.open(nulo, {
				modal: true
			});

			nulo = null;
			$.editar_disponibilidad.close();
		}
	}
}

$.widgetHeader.init({
	titulo: L('x1574293037_traducir', 'Editar Disponibilidad'),
	__id: 'ALL1222663545',
	avance: L('', ''),
	onclick: Click_widgetHeader
});

function Click_widgetHeader(e) {

	var evento = e;

}


function Change_picker(e) {

	e.cancelBubble = true;
	var elemento = e;
	var _columna = e.columnIndex;
	var columna = e.columnIndex + 1;
	var _fila = e.rowIndex;
	var fila = e.rowIndex + 1;
	if (Ti.App.deployType != 'production') console.log('seleccionado', {
		"fila": _fila
	});
	require('vars')['seleccionado'] = _fila;
	if (_fila == 0 || _fila == '0') {
		/** 
		 * Dependiendo de la disponibilidad del inspector, cambiamos el texto de la barra (para saber si mandamos informacion en esta misma pantalla al servidor, o tiene que escoger los dias que tiene disponibles) 
		 */

		$.widgetBarra.update({
			texto: 'Siguiente'
		});
	} else {

		$.widgetBarra.update({
			texto: 'Guardar'
		});
	}
}

function Change_ID_1568773770(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	if (elemento == true || elemento == 'true') {
		/** 
		 * Cambiamos el detalle de la variable de fuera ciudad 
		 */
		$.NO.setText('SI');

		require('vars')['fueraciudad'] = L('x2212294583', '1');
	} else {
		$.NO.setText('NO');

		require('vars')['fueraciudad'] = L('x4108050209', '0');
	}
	elemento = null;

}

function Change_ID_952608080(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	if (elemento == true || elemento == 'true') {
		/** 
		 * Cambiamos el detalle de la variable de fuera pais 
		 */
		$.NO2.setText('SI');

		require('vars')['fuerapais'] = L('x2212294583', '1');
	} else {
		$.NO2.setText('NO');

		require('vars')['fuerapais'] = L('x4108050209', '0');
	}
	elemento = null;

}

(function() {
	/** 
	 * Recuperamos variable del inspector para saber detalle de la seleccion de disponibilidad (part o full time), si tiene disponibilidad de viajar fuera de la ciudad, pais y modificamos los datos en la pantalla 
	 */
	var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
	require('vars')['seleccionado'] = inspector.disponibilidad;
	require('vars')['fueraciudad'] = inspector.disponibilidad_viajar_ciudad;
	if (inspector.disponibilidad_viajar_ciudad == 1 || inspector.disponibilidad_viajar_ciudad == '1') {
		$.ID_1568773770.setValue(true);

		$.NO.setText('SI');

	} else {
		$.ID_1568773770.setValue('false');

		$.NO.setText('NO');

	}
	require('vars')['fuerapais'] = inspector.disponibilidad_viajar_pais;
	if (inspector.disponibilidad_viajar_pais == 1 || inspector.disponibilidad_viajar_pais == '1') {
		$.ID_952608080.setValue(true);

		$.NO2.setText('SI');

	} else {
		$.ID_952608080.setValue('false');

		$.NO2.setText('NO');

	}
	if (inspector.disponibilidad == 0 || inspector.disponibilidad == '0') {
		/** 
		 * Dependiendo de la disponibilidad del inspector, cambiamos el texto de la barra (para saber si mandamos informacion en esta misma pantalla al servidor, o tiene que escoger los dias que tiene disponibles) 
		 */

		$.widgetBarra.update({
			texto: 'Siguiente'
		});
		if (Ti.App.deployType != 'production') console.log('disponibilidad es cero', {});
	} else {

		$.widgetBarra.update({
			texto: 'Guardar'
		});
		if (Ti.App.deployType != 'production') console.log('disponibilidad es 1', {});
	}
	/** 
	 * Obtenemos el picker para poder modificar la disponibilidad del inspector 
	 */
	var picker;
	picker = $.picker;
	picker.setSelectedRow(0, inspector.disponibilidad);
})();

function Postlayout_editar_disponibilidad(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var ID_1294585966_func = function() {
		/** 
		 * Una vez ya cargada la tarea, dejamos la variable limpia para poder abrir otras pantallas 
		 */
		require('vars')['var_abriendo'] = '';
	};
	var ID_1294585966 = setTimeout(ID_1294585966_func, 1000 * 0.2);

}
if (OS_IOS || OS_ANDROID) {
	$.editar_disponibilidad.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}
//$.editar_disponibilidad.open();
