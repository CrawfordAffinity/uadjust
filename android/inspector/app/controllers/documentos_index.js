var _bind4section = {};
var _list_templates = {};
var $documentos = $.documentos.toJSON();

var _activity;
if (OS_ANDROID) {
	_activity = $.documentos.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'documentos';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.documentos.addEventListener('open', function(e) {});
}
$.documentos.orientationModes = [Titanium.UI.PORTRAIT];


$.widgetBarra.init({
	titulo: L('x354890761_traducir', 'DOCUMENTOS'),
	onsalirinsp: Salirinsp_widgetBarra,
	__id: 'ALL138747237',
	continuar: L('', ''),
	salir_insp: L('', ''),
	fondo: 'fondorosado',
	top: 0,
	onpresiono: Presiono_widgetBarra
});

function Salirinsp_widgetBarra(e) {

	var evento = e;
	var preguntarOpciones_opts = [L('x3481285698_traducir', 'Asegurado no puede seguir'), L('x3077268071_traducir', ' Se me acabo la bateria'), L('x479989047_traducir', ' Tuve un accidente'), L('x2376009830_traducir', ' Cancelar')];
	var preguntarOpciones = Ti.UI.createOptionDialog({
		title: L('x1670176792_traducir', 'RAZON PARA CANCELAR INSPECCION ACTUAL'),
		options: preguntarOpciones_opts
	});
	preguntarOpciones.addEventListener('click', function(e) {
		var resp = preguntarOpciones_opts[e.index];
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var razon = "";
			if (resp == L('x3481285698_traducir', 'Asegurado no puede seguir')) {
				razon = "Asegurado no puede seguir";
			}
			if (resp == L('x1862794075_traducir', 'Se me acabo la bateria')) {
				razon = "Se me acabo la bateria";
			}
			if (resp == L('x4012805111_traducir', 'Tuve un accidente')) {
				razon = "Tuve un accidente";
			}
			if (Ti.App.deployType != 'production') console.log('mi razon es', {
				"datos": razon
			});
			if ((_.isObject(razon) || (_.isString(razon)) && !_.isEmpty(razon)) || _.isNumber(razon) || _.isBoolean(razon)) {
				if (Ti.App.deployType != 'production') console.log('llamando servicio cancelarTarea', {});
				require('vars')['insp_cancelada'] = L('x2941610362_traducir', 'siniestro');
				Alloy.Collections[$.documentos.config.adapter.collection_name].add($.documentos);
				$.documentos.save();
				Alloy.Collections[$.documentos.config.adapter.collection_name].fetch();
				var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
				var vista41_visible = true;

				if (vista41_visible == 'si') {
					vista41_visible = true;
				} else if (vista41_visible == 'no') {
					vista41_visible = false;
				}
				$.vista41.setVisible(vista41_visible);

				var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
				var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
				if (Ti.App.deployType != 'production') console.log('detalle de seltarea', {
					"data": seltarea
				});
				var datos = {
					id_inspector: inspector.id_server,
					codigo_identificador: inspector.codigo_identificador,
					id_server: seltarea.id_server,
					num_caso: seltarea.num_caso,
					razon: razon
				};
				require('vars')[_var_scopekey]['datos'] = datos;
				var consultarURL = {};
				consultarURL.success = function(e) {
					var elemento = e,
						valor = e;
					var vista41_visible = false;

					if (vista41_visible == 'si') {
						vista41_visible = true;
					} else if (vista41_visible == 'no') {
						vista41_visible = false;
					}
					$.vista41.setVisible(vista41_visible);

					Alloy.createController("firma_index", {}).getView().open();
					var ID_860845987_func = function() {
						Alloy.Events.trigger('_cerrar_insp', {
							pantalla: 'documentos'
						});
					};
					var ID_860845987 = setTimeout(ID_860845987_func, 1000 * 0.5);
					elemento = null, valor = null;
				};
				consultarURL.error = function(e) {
					var elemento = e,
						valor = e;
					if (Ti.App.deployType != 'production') console.log('hubo error llamando cancelarTarea, la agregamos a cola de salida', {
						"elemento": elemento
					});
					if (Ti.App.deployType != 'production') console.log('agregando servicio cancelarTarea a cola', {});
					var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
					var datos = ('datos' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['datos'] : '';
					var insertarModelo_m = Alloy.Collections.cola;
					var insertarModelo_fila = Alloy.createModel('cola', {
						data: JSON.stringify(datos),
						id_tarea: seltarea.id_server,
						tipo: 'cancelar'
					});
					insertarModelo_m.add(insertarModelo_fila);
					insertarModelo_fila.save();
					var vista41_visible = false;

					if (vista41_visible == 'si') {
						vista41_visible = true;
					} else if (vista41_visible == 'no') {
						vista41_visible = false;
					}
					$.vista41.setVisible(vista41_visible);

					Alloy.createController("firma_index", {}).getView().open();
					var ID_851467821_func = function() {
						Alloy.Events.trigger('_cerrar_insp', {
							pantalla: 'documentos'
						});
					};
					var ID_851467821 = setTimeout(ID_851467821_func, 1000 * 0.5);
					elemento = null, valor = null;
				};
				require('helper').ajaxUnico('consultarURL', '' + String.format(L('x3244141284', '%1$scancelarTarea'), url_server.toString()) + '', 'POST', {
					id_inspector: seltarea.id_inspector,
					codigo_identificador: inspector.codigo_identificador,
					id_tarea: seltarea.id_server,
					num_caso: seltarea.num_caso,
					mensaje: razon,
					opcion: 0,
					tipo: 1
				}, 15000, consultarURL);
			}
		}
		resp = null;
		e.source.removeEventListener("click", arguments.callee);
	});
	preguntarOpciones.show();

}

function Presiono_widgetBarra(e) {

	var evento = e;
	require('vars')[_var_scopekey]['alguno_on'] = L('x734881840_traducir', 'false');
	if ($documentos.doc1 == 1 || $documentos.doc1 == '1') {
		require('vars')[_var_scopekey]['alguno_on'] = L('x4261170317', 'true');
	} else if ($documentos.doc2 == 1 || $documentos.doc2 == '1') {
		require('vars')[_var_scopekey]['alguno_on'] = L('x4261170317', 'true');
	} else if ($documentos.doc3 == 1 || $documentos.doc3 == '1') {
		require('vars')[_var_scopekey]['alguno_on'] = L('x4261170317', 'true');
	} else if ($documentos.doc4 == 1 || $documentos.doc4 == '1') {
		require('vars')[_var_scopekey]['alguno_on'] = L('x4261170317', 'true');
	} else if ($documentos.doc5 == 1 || $documentos.doc5 == '1') {
		require('vars')[_var_scopekey]['alguno_on'] = L('x4261170317', 'true');
	} else if ($documentos.doc6 == 1 || $documentos.doc6 == '1') {
		require('vars')[_var_scopekey]['alguno_on'] = L('x4261170317', 'true');
	} else if ($documentos.doc7 == 1 || $documentos.doc7 == '1') {
		require('vars')[_var_scopekey]['alguno_on'] = L('x4261170317', 'true');
	} else if ($documentos.doc8 == 1 || $documentos.doc8 == '1') {
		require('vars')[_var_scopekey]['alguno_on'] = L('x4261170317', 'true');
	} else if ($documentos.doc9 == 1 || $documentos.doc9 == '1') {
		require('vars')[_var_scopekey]['alguno_on'] = L('x4261170317', 'true');
	} else if ($documentos.doc10 == 1 || $documentos.doc10 == '1') {
		require('vars')[_var_scopekey]['alguno_on'] = L('x4261170317', 'true');
	}
	var alguno_on = ('alguno_on' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['alguno_on'] : '';
	if (Ti.App.deployType != 'production') console.log('PROBANDO1', {
		"prueba": alguno_on
	});
	if (alguno_on == true || alguno_on == 'true') {
		if (Ti.App.deployType != 'production') console.log('PROBANDO2', {
			"prueba": alguno_on,
			"los dias": $documentos.dias
		});
		if (_.isUndefined($documentos.dias)) {
			var preguntarAlerta_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta = Ti.UI.createAlertDialog({
				title: L('x3237162386_traducir', 'Atencion'),
				message: L('x1870732722_traducir', 'Ingrese días de compromiso'),
				buttonNames: preguntarAlerta_opts
			});
			preguntarAlerta.addEventListener('click', function(e) {
				var nulo = preguntarAlerta_opts[e.index];
				nulo = null;
				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta.show();
			if (Ti.App.deployType != 'production') console.log('los dias no existen', {});
		} else if ((_.isObject($documentos.dias) || _.isString($documentos.dias)) && _.isEmpty($documentos.dias)) {
			var preguntarAlerta2_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta2 = Ti.UI.createAlertDialog({
				title: L('x3237162386_traducir', 'Atencion'),
				message: L('x1870732722_traducir', 'Ingrese días de compromiso'),
				buttonNames: preguntarAlerta2_opts
			});
			preguntarAlerta2.addEventListener('click', function(e) {
				var nulo = preguntarAlerta2_opts[e.index];
				nulo = null;
				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta2.show();
		} else {
			$.widgetEstadeacuerdo.abrir({
				color: 'rosado'
			});
		}
	} else {
		$.widgetEstadeacuerdo.abrir({
			color: 'rosado'
		});
	}
}

$.widgetEstadeacuerdo.init({
	titulo: L('x404129330_traducir', '¿EL ASEGURADO CONFIRMA ESTOS DATOS?'),
	__id: 'ALL516906299',
	si: L('x1723413441_traducir', 'SI, Están correctos'),
	texto: L('x2083765231_traducir', 'El asegurado debe confirmar que los datos de esta sección están correctos'),
	pantalla: L('x2851883104_traducir', '¿ESTA DE ACUERDO?'),
	color: 'rosado',
	onsi: si_widgetEstadeacuerdo,
	no: L('x55492959_traducir', 'NO, Hay que modificar algo')
});

function si_widgetEstadeacuerdo(e) {

	var evento = e;
	/** 
	 * Ocupamos esta variable para impedir que la pantalla siguente se cargue dos veces 
	 */
	var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
	if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
		require('vars')['var_abriendo'] = L('x4261170317', 'true');
		/** 
		 * Guardamos el modelo con los datos del domicilio 
		 */
		Alloy.Collections[$.documentos.config.adapter.collection_name].add($.documentos);
		$.documentos.save();
		Alloy.Collections[$.documentos.config.adapter.collection_name].fetch();
		Alloy.createController("firma_index", {}).getView().open();
	}

}

function Change_ID_1646610398(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	if (elemento == true || elemento == 'true') {
		/** 
		 * Dependiendo del estado del switch, actualizamos el estado del documento 
		 */
		$.documentos.set({
			doc1: 1
		});
		if ('documentos' in $) $documentos = $.documentos.toJSON();
	} else {
		$.documentos.set({
			doc1: 0
		});
		if ('documentos' in $) $documentos = $.documentos.toJSON();
	}
	elemento = null;

}

function Change_ID_61875369(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	if (elemento == true || elemento == 'true') {
		$.documentos.set({
			doc2: 1
		});
		if ('documentos' in $) $documentos = $.documentos.toJSON();
	} else {
		if (Ti.App.deployType != 'production') console.log('el doc1 es falso', {});
		$.documentos.set({
			doc2: 0
		});
		if ('documentos' in $) $documentos = $.documentos.toJSON();
	}
	elemento = null;

}

function Change_ID_978985218(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	if (elemento == true || elemento == 'true') {
		$.documentos.set({
			doc3: 1
		});
		if ('documentos' in $) $documentos = $.documentos.toJSON();
	} else {
		if (Ti.App.deployType != 'production') console.log('el doc1 es falso', {});
		$.documentos.set({
			doc3: 0
		});
		if ('documentos' in $) $documentos = $.documentos.toJSON();
	}
	elemento = null;

}

function Change_ID_1655643973(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	if (elemento == true || elemento == 'true') {
		$.documentos.set({
			doc4: 1
		});
		if ('documentos' in $) $documentos = $.documentos.toJSON();
	} else {
		$.documentos.set({
			doc4: 0
		});
		if ('documentos' in $) $documentos = $.documentos.toJSON();
	}
	elemento = null;

}

function Change_ID_1295047266(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	if (elemento == true || elemento == 'true') {
		$.documentos.set({
			doc5: 1
		});
		if ('documentos' in $) $documentos = $.documentos.toJSON();
	} else {
		$.documentos.set({
			doc5: 0
		});
		if ('documentos' in $) $documentos = $.documentos.toJSON();
	}
	elemento = null;

}

function Change_ID_1158156032(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	if (elemento == true || elemento == 'true') {
		$.documentos.set({
			doc6: 1
		});
		if ('documentos' in $) $documentos = $.documentos.toJSON();
	} else {
		$.documentos.set({
			doc6: 0
		});
		if ('documentos' in $) $documentos = $.documentos.toJSON();
	}
	elemento = null;

}

function Change_ID_416452733(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	if (elemento == true || elemento == 'true') {
		$.documentos.set({
			doc7: 1
		});
		if ('documentos' in $) $documentos = $.documentos.toJSON();
	} else {
		$.documentos.set({
			doc7: 0
		});
		if ('documentos' in $) $documentos = $.documentos.toJSON();
	}
	elemento = null;

}

function Change_ID_796508730(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	if (elemento == true || elemento == 'true') {
		$.documentos.set({
			doc8: 1
		});
		if ('documentos' in $) $documentos = $.documentos.toJSON();
	} else {
		$.documentos.set({
			doc8: 0
		});
		if ('documentos' in $) $documentos = $.documentos.toJSON();
	}
	elemento = null;

}

function Change_ID_1797488946(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	if (elemento == true || elemento == 'true') {
		$.documentos.set({
			doc9: 1
		});
		if ('documentos' in $) $documentos = $.documentos.toJSON();
	} else {
		$.documentos.set({
			doc9: 0
		});
		if ('documentos' in $) $documentos = $.documentos.toJSON();
	}
	elemento = null;

}

function Change_ID_1352736328(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	if (elemento == true || elemento == 'true') {
		/** 
		 * Actualizamos edificio y doc10 
		 */
		$.documentos.set({
			doc10: 1,
			edificio: 1
		});
		if ('documentos' in $) $documentos = $.documentos.toJSON();
		require('vars')[_var_scopekey]['s1'] = L('x2212294583', '1');
	} else {
		$.documentos.set({
			edificio: 0
		});
		if ('documentos' in $) $documentos = $.documentos.toJSON();
		/** 
		 * Recuperamos variable s2 para saber si el doc10 debe estar en 0 o 1 
		 */
		var s2 = ('s2' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['s2'] : '';
		require('vars')[_var_scopekey]['s1'] = L('x4108050209', '0');
		if (s2 == 0 || s2 == '0') {
			$.documentos.set({
				doc10: 0
			});
			if ('documentos' in $) $documentos = $.documentos.toJSON();
		}
	}
	elemento = null;

}

function Change_ID_612551910(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	if (elemento == true || elemento == 'true') {
		/** 
		 * Actualizamos los contenidos y doc10 
		 */
		$.documentos.set({
			doc10: 1,
			contenidos: 1
		});
		if ('documentos' in $) $documentos = $.documentos.toJSON();
		require('vars')[_var_scopekey]['s2'] = L('x2212294583', '1');
	} else {
		$.documentos.set({
			contenidos: 0
		});
		if ('documentos' in $) $documentos = $.documentos.toJSON();
		/** 
		 * Recuperamos variable s1 para saber si el doc10 debe estar en 0 o 1 
		 */
		var s1 = ('s1' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['s1'] : '';
		require('vars')[_var_scopekey]['s2'] = L('x4108050209', '0');
		if (s1 == 0 || s1 == '0') {
			$.documentos.set({
				doc10: 0
			});
			if ('documentos' in $) $documentos = $.documentos.toJSON();
		}
	}
	elemento = null;

}

function Change_Especifique(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	$.documentos.set({
		otros: elemento
	});
	if ('documentos' in $) $documentos = $.documentos.toJSON();
	elemento = null, source = null;

}

function Change_campo(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	/** 
	 * Actualizamos la tabla con los dias para enviar los documentos 
	 */
	$.documentos.set({
		dias: elemento
	});
	if ('documentos' in $) $documentos = $.documentos.toJSON();
	elemento = null, source = null;

}

function Return_campo(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	/** 
	 * Al presionar enter en el teclado del equipo, desenfocamos la caja 
	 */
	$.campo.blur();
	elemento = null, source = null;

}

function Click_scroll(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	/** 
	 * Desenfocamos las cajas de texto 
	 */
	$.Especifique.blur();
	$.campo.blur();

}

function Postlayout_vista41(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.progreso.show();

}

function Postlayout_documentos(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var ID_1294585966_func = function() {
		/** 
		 * Una vez ya cargada la tarea, dejamos la variable limpia para poder abrir otras pantallas 
		 */
		require('vars')['var_abriendo'] = '';
	};
	var ID_1294585966 = setTimeout(ID_1294585966_func, 1000 * 0.2);
	/** 
	 * Cerramos pantalla contenidos 
	 */
	Alloy.Events.trigger('_cerrar_insp', {
		pantalla: 'contenidos'
	});

}

var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
/** 
 * Cerramos esta pantalla cuando la firma se ha ejecutado 
 */
_my_events['_cerrar_insp,ID_511339808'] = function(evento) {
	if (evento.pantalla == L('x515385654_traducir', 'documentos')) {
		var ID_962933245_trycatch = {
			error: function(e) {
				if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
			}
		};
		try {
			ID_962933245_trycatch.error = function(evento) {};
			$.documentos.close();
		} catch (e) {
			ID_962933245_trycatch.error(e);
		}
	}
};
Alloy.Events.on('_cerrar_insp', _my_events['_cerrar_insp,ID_511339808']);
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
	/** 
	 * Inicializamos la tabla con datos default 
	 */
	$.documentos.set({
		id_inspeccion: seltarea.id_server,
		doc10: 0,
		doc6: 0,
		doc4: 0,
		doc7: 0,
		doc3: 0,
		edificio: 0,
		doc9: 0,
		dias: '',
		doc1: 0,
		doc5: 0,
		contenidos: 0,
		otros: '',
		doc2: 0,
		doc8: 0
	});
	if ('documentos' in $) $documentos = $.documentos.toJSON();
}

function Androidback_documentos(e) {

	e.cancelBubble = true;
	var elemento = e.source;

}

(function() {
	/** 
	 * Inicializamos flag para determinar si el doc10 tiene que estar en 0 o 1 
	 */
	require('vars')[_var_scopekey]['s1'] = L('x4108050209', '0');
	require('vars')[_var_scopekey]['s2'] = L('x4108050209', '0');
	/** 
	 * Avisamos que la inspeccion no fue cancelada 
	 */
	require('vars')['insp_cancelada'] = '';
	/** 
	 * Cambiamos el color del statusbar 
	 */
	var ID_1714814641_func = function() {
		var documentos_statusbar = '#E87C7C';

		var setearStatusColor = function(documentos_statusbar) {
			if (OS_IOS) {
				if (documentos_statusbar == 'light' || documentos_statusbar == 'claro') {
					$.documentos_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.LIGHT_CONTENT);
				} else if (documentos_statusbar == 'grey' || documentos_statusbar == 'gris' || documentos_statusbar == 'gray') {
					$.documentos_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.GREY);
				} else if (documentos_statusbar == 'oscuro' || documentos_statusbar == 'dark') {
					$.documentos_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.DEFAULT);
				}
			} else if (OS_ANDROID) {
				abx.setStatusbarColor(documentos_statusbar);
			}
		};
		setearStatusColor(documentos_statusbar);

	};
	var ID_1714814641 = setTimeout(ID_1714814641_func, 1000 * 0.1);
})();

if (OS_IOS || OS_ANDROID) {
	$.documentos.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}
//$.documentos.open();
