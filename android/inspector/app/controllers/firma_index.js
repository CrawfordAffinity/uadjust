var _bind4section = {};
var _list_templates = {};

var _activity;
if (OS_ANDROID) {
	_activity = $.firma.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'firma';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.firma.addEventListener('open', function(e) {});
}
$.firma.orientationModes = [Titanium.UI.PORTRAIT];


$.widgetBotonlargo.init({
	titulo: L('x3679463181_traducir', 'FINALIZAR'),
	__id: 'ALL59432441',
	color: 'verde',
	onclick: Click_widgetBotonlargo
});

function Click_widgetBotonlargo(e) {

	var evento = e;
	var preguntarAlerta_opts = [L('x1906696502_traducir', 'si'), L('x1892641797_traducir', ' limpiar'), L('x1961454128_traducir', ' cancelar')];
	var preguntarAlerta = Ti.UI.createAlertDialog({
		title: L('x2308473092_traducir', 'Por favor, confirme'),
		message: L('x573783118_traducir', 'Esta conforme con la firma?'),
		buttonNames: preguntarAlerta_opts
	});
	preguntarAlerta.addEventListener('click', function(e) {
		var resp = preguntarAlerta_opts[e.index];
		if (resp == L('x1906696502_traducir', 'si')) {
			/** 
			 * Llamamos al evento dentro de la vista web para obtener la imagen de la forma 
			 */

			Ti.App.fireEvent('canvasGetImage', {
				message: 'event fired from Titanium, handled in WebView'
			});
			/** 
			 * Al string 
			 */

			Ti.App.addEventListener("canvasGetImageResponse", function(e) {
				var stripped = e.image.replace("data:image/png;base64,", "");
				var click_prox = ('click_prox' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['click_prox'] : '';
				if (click_prox == false || click_prox == 'false') {
					/** 
					 * Flag para que no se ejecute dos veces el click 
					 */
					require('vars')[_var_scopekey]['click_prox'] = L('x4261170317', 'true');
					var insertarModelo_m = Alloy.Collections.numero_unico;
					var insertarModelo_fila = Alloy.createModel('numero_unico', {
						comentario: 'firma'
					});
					insertarModelo_m.add(insertarModelo_fila);
					insertarModelo_fila.save();
					var nuevoid = require('helper').model2object(insertarModelo_m.last());
					/** 
					 * Convertimos de string a imagen para comprimir 
					 */
					var firma_png = Ti.Utils.base64decode(stripped);
					/** 
					 * Comprimimos a JPG 
					 */
					if (OS_ANDROID) {
						var comprimirImagen_imagefactory = require('ti.imagefactory');
						var firma_jpg = comprimirImagen_imagefactory.compress(firma_png, 0.9);
					} else if (OS_IOS) {
						var comprimirImagen_imagefactory = require('ti.imagefactory');
						var firma_jpg = comprimirImagen_imagefactory.compress(firma_png, 0.9);
					}
					var id_insp = ('id_insp' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['id_insp'] : '';
					/** 
					 * Guardamos en disco de telefono 
					 */
					var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
					if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
						var ID_1153703423_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + id_insp);
						if (ID_1153703423_d.exists() == false) ID_1153703423_d.createDirectory();
						var ID_1153703423_f = Ti.Filesystem.getFile(ID_1153703423_d.resolve(), 'firma' + nuevoid.id + '.jpg');
						if (ID_1153703423_f.exists() == true) ID_1153703423_f.deleteFile();
						ID_1153703423_f.write(firma_jpg);
						ID_1153703423_d = null;
						ID_1153703423_f = null;
					} else {
						var ID_1633907683_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + 0);
						if (ID_1633907683_d.exists() == false) ID_1633907683_d.createDirectory();
						var ID_1633907683_f = Ti.Filesystem.getFile(ID_1633907683_d.resolve(), 'firma' + nuevoid.id + '.jpg');
						if (ID_1633907683_f.exists() == true) ID_1633907683_f.deleteFile();
						ID_1633907683_f.write(firma_jpg);
						ID_1633907683_d = null;
						ID_1633907683_f = null;
					}
					var insertarModelo2_m = Alloy.Collections.insp_firma;
					var insertarModelo2_fila = Alloy.createModel('insp_firma', {
						firma64: String.format(L('x2450709990', 'firma%1$s.jpg'), nuevoid.id.toString()),
						id_inspeccion: id_insp
					});
					insertarModelo2_m.add(insertarModelo2_fila);
					insertarModelo2_fila.save();
					if (Ti.App.deployType != 'production') console.log('id server asignado a firma', {
						"id_insp_firma": id_asignado,
						"nombre_archivo": String.format(L('x2450709990', 'firma%1$s.jpg'), nuevoid.id.toString())
					});
					/** 
					 * Limpiamos memoria 
					 */
					var seltarea = null,
						firma_jpg = null,
						firma_png = null,
						evento = null;
					/** 
					 * Limpiamos variable 
					 */
					require('vars')['firma_accion'] = '';
					/** 
					 * Tratamos de cerrar todas las pantallas de inspeccion 
					 */

					var ID_1042874148_trycatch = {
						error: function(e) {
							if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
						}
					};
					try {
						ID_1042874148_trycatch.error = function(evento) {};

						Alloy.Events.trigger('_cerrar_insp');
					} catch (e) {
						ID_1042874148_trycatch.error(e);
					}
					var consultarModelo_i = Alloy.createCollection('insp_firma');
					var consultarModelo_i_where = '';
					consultarModelo_i.fetch();
					var cantfirma = require('helper').query2array(consultarModelo_i);
					if (Ti.App.deployType != 'production') console.log('firmas en total', {
						"datos": cantfirmas
					});
					var ID_1186276553_func = function() {
						$.firma.close();
					};
					var ID_1186276553 = setTimeout(ID_1186276553_func, 1000 * 0.5);
					Alloy.createController("fin_index", {}).getView().open();
				}

			});
		} else if (resp == L('x1912016452_traducir', 'limpiar')) {
			/** 
			 * Recargamos la vista web, y asi limpiamos la firma 
			 */
			var webview;
			webview = $.webLocal;
			webview.reload();
		} else {}
		resp = null;

		e.source.removeEventListener("click", arguments.callee);
	});
	preguntarAlerta.show();

}

function Postlayout_firma(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var insp_cancelada = ('insp_cancelada' in require('vars')) ? require('vars')['insp_cancelada'] : '';
	if ((_.isObject(insp_cancelada) || (_.isString(insp_cancelada)) && !_.isEmpty(insp_cancelada)) || _.isNumber(insp_cancelada) || _.isBoolean(insp_cancelada)) {
		/** 
		 * Dependiendo del flag (para saber si se cancelo la inspeccion) cerramos pantalla de insp cancelada o documentos 
		 */

		Alloy.Events.trigger('_cerrar_insp', {
			pantalla: insp_cancelada
		});
	} else {

		Alloy.Events.trigger('_cerrar_insp', {
			pantalla: 'documentos'
		});
	}
	if (Ti.App.deployType != 'production') console.log('vamos a refrescar la vista', {});
	var cargada = ('cargada' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['cargada'] : '';
	if (cargada == false || cargada == 'false') {
		require('vars')[_var_scopekey]['cargada'] = L('x4261170317', 'true');
		var ID_1879861421_func = function() {
			var webview;
			webview = $.webLocal;
			webview.reload();
			if (Ti.App.deployType != 'production') console.log('recargando vistaweb', {});
		};
		var ID_1879861421 = setTimeout(ID_1879861421_func, 1000 * 0.5);
	}

}

(function() {
	require('vars')[_var_scopekey]['cargada'] = L('x734881840_traducir', 'false');
	require('vars')[_var_scopekey]['click_prox'] = L('x734881840_traducir', 'false');
	/** 
	 * Ejecutamos en 0.1 segundos para cambiar el color del statusbar 
	 */
	var ID_71342109_func = function() {
		var firma_statusbar = '#006C9B';

		var setearStatusColor = function(firma_statusbar) {
			if (OS_IOS) {
				if (firma_statusbar == 'light' || firma_statusbar == 'claro') {
					$.firma_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.LIGHT_CONTENT);
				} else if (firma_statusbar == 'grey' || firma_statusbar == 'gris' || firma_statusbar == 'gray') {
					$.firma_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.GREY);
				} else if (firma_statusbar == 'oscuro' || firma_statusbar == 'dark') {
					$.firma_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.DEFAULT);
				}
			} else if (OS_ANDROID) {
				abx.setStatusbarColor(firma_statusbar);
			}
		};
		setearStatusColor(firma_statusbar);

	};
	var ID_71342109 = setTimeout(ID_71342109_func, 1000 * 0.1);
})();


/** 
 * Completamos datos de persona presente 
 */
var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
	var consultarModelo2_i = Alloy.createCollection('inspecciones');
	var consultarModelo2_i_where = '';
	consultarModelo2_i.fetch();
	var ultima = require('helper').query2array(consultarModelo2_i);
	if (Ti.App.deployType != 'production') console.log('id server asignado a firma', {
		"ultima": ultima[ultima.length - 1].id_server,
		"previo": seltarea.id_server
	});
	require('vars')[_var_scopekey]['id_insp'] = ultima[ultima.length - 1].id_server;
	var consultarModelo3_i = Alloy.createCollection('insp_datosbasicos');
	var consultarModelo3_i_where = 'id_inspeccion=\'' + seltarea.id_server + '\'';
	consultarModelo3_i.fetch({
		query: 'SELECT * FROM insp_datosbasicos WHERE id_inspeccion=\'' + seltarea.id_server + '\''
	});
	var previos = require('helper').query2array(consultarModelo3_i);
	if (Ti.App.deployType != 'production') console.log('datos basicos en firma', {
		"datos": previos
	});
	if (previos && previos.length) {
		$.Label.setText(previos[0].presente_nombre);

		$.Label2.setText(previos[0].presente_rut);

	} else {
		if (Ti.App.deployType != 'production') console.log('ERROR FIRMA: insp_datosbasicos no tiene datos de inspeccion', {});
		var consultarModelo4_i = Alloy.createCollection('insp_datosbasicos');
		var consultarModelo4_i_where = '';
		consultarModelo4_i.fetch();
		var ptodos = require('helper').query2array(consultarModelo4_i);
		if (Ti.App.deployType != 'production') console.log('ERROR FIRMA: debug insp_datosbasicos full', {
			"full": ptodos
		});
	}
} else {
	$.Label.setText('Pablo Martinez Herrera');

	$.Label2.setText('5.555.555-5');

}
require('vars')['firma_accion'] = L('x2290876758_traducir', 'nada');
/** 
 * Cerramos esta pantalla cuando se haya llamado el evento desde pantalla finalizaod 
 */

_my_events['_cerrar_insp,ID_1618628689'] = function(evento) {
	if (!_.isUndefined(evento.pantalla)) {
		if (evento.pantalla == L('x736965987_traducir', 'firma')) {
			if (Ti.App.deployType != 'production') console.log('debug cerrando firma', {});

			var ID_1781710521_trycatch = {
				error: function(e) {
					if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
				}
			};
			try {
				ID_1781710521_trycatch.error = function(evento) {
					if (Ti.App.deployType != 'production') console.log('error cerrando firma', {});
				};
				$.firma.close();
			} catch (e) {
				ID_1781710521_trycatch.error(e);
			}
		}
	} else {
		if (Ti.App.deployType != 'production') console.log('debug cerrando (todas) firma', {});

		var ID_1962200830_trycatch = {
			error: function(e) {
				if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
			}
		};
		try {
			ID_1962200830_trycatch.error = function(evento) {
				if (Ti.App.deployType != 'production') console.log('error cerrando firma', {});
			};
			$.firma.close();
		} catch (e) {
			ID_1962200830_trycatch.error(e);
		}
	}
};
Alloy.Events.on('_cerrar_insp', _my_events['_cerrar_insp,ID_1618628689']);
/** 
 * Revisamos flag para saber si el inspector cancelo la inspeccion 
 */
var insp_cancelada = ('insp_cancelada' in require('vars')) ? require('vars')['insp_cancelada'] : '';
if ((_.isObject(insp_cancelada) || (_.isString(insp_cancelada)) && !_.isEmpty(insp_cancelada)) || _.isNumber(insp_cancelada) || _.isBoolean(insp_cancelada)) {
	var vista3_visible = true;

	if (vista3_visible == 'si') {
		vista3_visible = true;
	} else if (vista3_visible == 'no') {
		vista3_visible = false;
	}
	$.vista3.setVisible(vista3_visible);

} else {
	var vista3_visible = false;

	if (vista3_visible == 'si') {
		vista3_visible = true;
	} else if (vista3_visible == 'no') {
		vista3_visible = false;
	}
	$.vista3.setVisible(vista3_visible);

}

function Androidback_firma(e) {

	e.cancelBubble = true;
	var elemento = e.source;

}
if (OS_IOS || OS_ANDROID) {
	$.firma.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}
//$.firma.open();
