var _bind4section = {};
var _list_templates = {};
var $contenido = $.contenido.toJSON();

var _activity;
if (OS_ANDROID) {
	_activity = $.nuevocontenido.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'nuevocontenido';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.nuevocontenido.addEventListener('open', function(e) {});
}
$.nuevocontenido.orientationModes = [Titanium.UI.PORTRAIT];


$.widgetBarra2.init({
	titulo: L('x1486275980_traducir', 'NUEVO CONTENIDO'),
	__id: 'ALL758725651',
	oncerrar: Cerrar_widgetBarra2,
	textoderecha: L('x2943883035_traducir', 'Guardar'),
	fondo: 'fondoceleste',
	top: 0,
	modal: L('', ''),
	onpresiono: Presiono_widgetBarra2,
	colortextoderecha: 'blanco'
});

function Cerrar_widgetBarra2(e) {

	var evento = e;
	/** 
	 * Limpiamos widgets multiples (memoria ram) 
	 */
	$.widgetModal.limpiar({});
	$.widgetModal2.limpiar({});
	$.widgetModal3.limpiar({});
	$.nuevocontenido.close();

}

function Presiono_widgetBarra2(e) {

	var evento = e;
	/** 
	 * Obtenemos la fecha actual 
	 */

	var hoy = new Date();
	if ($contenido.fecha_compra > hoy == true || $contenido.fecha_compra > hoy == 'true') {
		/** 
		 * Validamos que los campos ingresados sean correctos 
		 */
		var preguntarAlerta2_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta2 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x2327084729_traducir', 'La fecha de compra no puede ser superior a hoy'),
			buttonNames: preguntarAlerta2_opts
		});
		preguntarAlerta2.addEventListener('click', function(e) {
			var nulo = preguntarAlerta2_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta2.show();
	} else if (_.isUndefined($contenido.nombre)) {
		var preguntarAlerta3_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta3 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x2224804335_traducir', 'Seleccione producto'),
			buttonNames: preguntarAlerta3_opts
		});
		preguntarAlerta3.addEventListener('click', function(e) {
			var nulo = preguntarAlerta3_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta3.show();
	} else if (_.isUndefined($contenido.id_marca)) {
		var preguntarAlerta4_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta4 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x3747506986_traducir', 'Seleccione marca del producto'),
			buttonNames: preguntarAlerta4_opts
		});
		preguntarAlerta4.addEventListener('click', function(e) {
			var nulo = preguntarAlerta4_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta4.show();
	} else if (_.isUndefined($contenido.id_recinto)) {
		var preguntarAlerta5_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta5 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x2405532746_traducir', 'Seleccione el recinto donde estaba el producto'),
			buttonNames: preguntarAlerta5_opts
		});
		preguntarAlerta5.addEventListener('click', function(e) {
			var nulo = preguntarAlerta5_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta5.show();
	} else if (_.isUndefined($contenido.cantidad)) {
		var preguntarAlerta6_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta6 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x4257958740_traducir', 'Ingrese cantidad de productos'),
			buttonNames: preguntarAlerta6_opts
		});
		preguntarAlerta6.addEventListener('click', function(e) {
			var nulo = preguntarAlerta6_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta6.show();
	} else if ((_.isObject($contenido.cantidad) || _.isString($contenido.cantidad)) && _.isEmpty($contenido.cantidad)) {
		var preguntarAlerta7_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta7 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x4257958740_traducir', 'Ingrese cantidad de productos'),
			buttonNames: preguntarAlerta7_opts
		});
		preguntarAlerta7.addEventListener('click', function(e) {
			var nulo = preguntarAlerta7_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta7.show();
	} else if (_.isUndefined($contenido.fecha_compra)) {
		var preguntarAlerta8_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta8 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x1056100694_traducir', 'Ingrese fecha de compra aproximada (año)'),
			buttonNames: preguntarAlerta8_opts
		});
		preguntarAlerta8.addEventListener('click', function(e) {
			var nulo = preguntarAlerta8_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta8.show();
	} else if (_.isUndefined($contenido.id_moneda)) {
		var preguntarAlerta9_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta9 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x2624129841_traducir', 'Seleccione el tipo de moneda del producto'),
			buttonNames: preguntarAlerta9_opts
		});
		preguntarAlerta9.addEventListener('click', function(e) {
			var nulo = preguntarAlerta9_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta9.show();
	} else if (_.isUndefined($contenido.valor)) {
		var preguntarAlerta10_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta10 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x1829790159_traducir', 'Ingrese valor del producto'),
			buttonNames: preguntarAlerta10_opts
		});
		preguntarAlerta10.addEventListener('click', function(e) {
			var nulo = preguntarAlerta10_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta10.show();
	} else if ((_.isObject($contenido.valor) || _.isString($contenido.valor)) && _.isEmpty($contenido.valor)) {
		var preguntarAlerta11_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta11 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x1829790159_traducir', 'Ingrese valor del producto'),
			buttonNames: preguntarAlerta11_opts
		});
		preguntarAlerta11.addEventListener('click', function(e) {
			var nulo = preguntarAlerta11_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta11.show();
	} else if (_.isUndefined($contenido.descripcion)) {
		var preguntarAlerta12_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta12 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x417344283_traducir', 'Describa el producto brevemente, con un mínimo de 30 caracteres'),
			buttonNames: preguntarAlerta12_opts
		});
		preguntarAlerta12.addEventListener('click', function(e) {
			var nulo = preguntarAlerta12_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta12.show();
	} else if (_.isNumber($contenido.descripcion.length) && _.isNumber(29) && $contenido.descripcion.length <= 29) {
		var preguntarAlerta13_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta13 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x417344283_traducir', 'Describa el producto brevemente, con un mínimo de 30 caracteres'),
			buttonNames: preguntarAlerta13_opts
		});
		preguntarAlerta13.addEventListener('click', function(e) {
			var nulo = preguntarAlerta13_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta13.show();
	} else {
		/** 
		 * Guardamos modelo nuevo 
		 */
		Alloy.Collections[$.contenido.config.adapter.collection_name].add($.contenido);
		$.contenido.save();
		Alloy.Collections[$.contenido.config.adapter.collection_name].fetch();
		/** 
		 * limpiamos widgets multiples (memoria ram) 
		 */
		$.widgetModal.limpiar({});
		$.widgetModal2.limpiar({});
		$.widgetModal3.limpiar({});
		$.nuevocontenido.close();
	}
}

function Click_vista14(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	/** 
	 * Ocupamos esta variable para impedir que la pantalla siguente se cargue dos veces 
	 */
	var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
	if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
		require('vars')['var_abriendo'] = L('x4261170317', 'true');
		Alloy.createController("listadocontenidos", {}).getView().open();
	}

}

$.widgetModal3.init({
	titulo: L('x1867465554_traducir', 'MARCAS'),
	cargando: L('x1740321226_traducir', 'cargando ..'),
	__id: 'ALL1413999243',
	left: 0,
	onrespuesta: Respuesta_widgetModal3,
	campo: L('x3837495059_traducir', 'Marca del Item'),
	onabrir: Abrir_widgetModal3,
	color: 'celeste',
	right: 0,
	seleccione: L('x1021431138_traducir', 'seleccione marca'),
	activo: true,
	onafterinit: Afterinit_widgetModal3
});

function Afterinit_widgetModal3(e) {

	var evento = e;
	var ID_1686389527_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			var consultarModelo3_i = Alloy.createCollection('marcas');
			var consultarModelo3_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
			consultarModelo3_i.fetch({
				query: 'SELECT * FROM marcas WHERE pais_texto=\'' + pais[0].nombre + '\''
			});
			var marcas = require('helper').query2array(consultarModelo3_i);
			var datos = [];
			_.each(marcas, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'valor';
					if (llave == 'id_segured') newkey = 'id_interno';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
		} else {
			var eliminarModelo3_i = Alloy.Collections.marcas;
			var sql = "DELETE FROM " + eliminarModelo3_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo3_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo3_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo2_m = Alloy.Collections.marcas;
				var insertarModelo2_fila = Alloy.createModel('marcas', {
					nombre: String.format(L('x391662843_traducir', 'HomeDepot%1$s'), item.toString()),
					id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
					id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item.toString()),
					pais: 1
				});
				insertarModelo2_m.add(insertarModelo2_fila);
				insertarModelo2_fila.save();
			});
			var transformarModelo2_i = Alloy.createCollection('marcas');
			transformarModelo2_i.fetch();
			var transformarModelo2_src = require('helper').query2array(transformarModelo2_i);
			var datos = [];
			_.each(transformarModelo2_src, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'valor';
					if (llave == 'id_segured') newkey = 'id_interno';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
		}
		$.widgetModal3.data({
			data: datos
		});
	};
	var ID_1686389527 = setTimeout(ID_1686389527_func, 1000 * 0.2);

}

function Abrir_widgetModal3(e) {

	var evento = e;

}

function Respuesta_widgetModal3(e) {

	var evento = e;
	$.widgetModal3.labels({
		valor: evento.valor
	});
	$.contenido.set({
		id_marca: evento.item.id_interno
	});
	if ('contenido' in $) $contenido = $.contenido.toJSON();
	if (Ti.App.deployType != 'production') console.log('datos recibidos de modal marca item', {
		"datos": evento
	});

}

$.widgetFotochica.init({
	caja: 55,
	__id: 'ALL1420842518',
	onlisto: Listo_widgetFotochica,
	onclick: Click_widgetFotochica
});

function Click_widgetFotochica(e) {

	var evento = e;
	/** 
	 * Definimos que estamos capturando foto en el primer item 
	 */
	require('vars')['cual_foto'] = L('x2212294583', '1');
	/** 
	 * Abrimos camara 
	 */
	$.widgetCamaralight.disparar({});
	/** 
	 * Detenemos las animaciones de los widget 
	 */
	$.widgetFotochica2.detener({});
	$.widgetFotochica3.detener({});
	$.widgetFotochica.detener({});

}

function Listo_widgetFotochica(e) {

	var evento = e;
	/** 
	 * Recuperamos variables para poder definir la estructura de las carpetas donde se guardara la miniatura 
	 */
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	var nuevoid_f1 = ('nuevoid_f1' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['nuevoid_f1'] : '';
	if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
		var ID_1194766412_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + seltarea.id_server);
		if (ID_1194766412_d.exists() == false) ID_1194766412_d.createDirectory();
		var ID_1194766412_f = Ti.Filesystem.getFile(ID_1194766412_d.resolve(), 'mini' + nuevoid_f1.id + '.jpg');
		if (ID_1194766412_f.exists() == true) ID_1194766412_f.deleteFile();
		ID_1194766412_f.write(evento.mini);
		ID_1194766412_d = null;
		ID_1194766412_f = null;
	} else {
		var ID_1143267424_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + 0);
		if (ID_1143267424_d.exists() == false) ID_1143267424_d.createDirectory();
		var ID_1143267424_f = Ti.Filesystem.getFile(ID_1143267424_d.resolve(), 'mini' + nuevoid_f1.id + '.jpg');
		if (ID_1143267424_f.exists() == true) ID_1143267424_f.deleteFile();
		ID_1143267424_f.write(evento.mini);
		ID_1143267424_d = null;
		ID_1143267424_f = null;
	}
}

$.widgetFotochica2.init({
	caja: 55,
	__id: 'ALL1818394314',
	onlisto: Listo_widgetFotochica2,
	onclick: Click_widgetFotochica2
});

function Click_widgetFotochica2(e) {

	var evento = e;
	$.widgetFotochica.detener({});
	$.widgetFotochica3.detener({});
	$.widgetFotochica2.detener({});
	require('vars')['cual_foto'] = 2;
	$.widgetCamaralight.disparar({});

}

function Listo_widgetFotochica2(e) {

	var evento = e;
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	var nuevoid_f2 = ('nuevoid_f2' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['nuevoid_f2'] : '';
	if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
		var ID_766059429_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + seltarea.id_server);
		if (ID_766059429_d.exists() == false) ID_766059429_d.createDirectory();
		var ID_766059429_f = Ti.Filesystem.getFile(ID_766059429_d.resolve(), 'mini' + nuevoid_f2.id + '.jpg');
		if (ID_766059429_f.exists() == true) ID_766059429_f.deleteFile();
		ID_766059429_f.write(evento.mini);
		ID_766059429_d = null;
		ID_766059429_f = null;
	} else {
		var ID_1011270433_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + 0);
		if (ID_1011270433_d.exists() == false) ID_1011270433_d.createDirectory();
		var ID_1011270433_f = Ti.Filesystem.getFile(ID_1011270433_d.resolve(), 'mini' + nuevoid_f2.id + '.jpg');
		if (ID_1011270433_f.exists() == true) ID_1011270433_f.deleteFile();
		ID_1011270433_f.write(evento.mini);
		ID_1011270433_d = null;
		ID_1011270433_f = null;
	}
}

$.widgetFotochica3.init({
	caja: 55,
	__id: 'ALL745114037',
	onlisto: Listo_widgetFotochica3,
	onclick: Click_widgetFotochica3
});

function Click_widgetFotochica3(e) {

	var evento = e;
	$.widgetFotochica.detener({});
	$.widgetFotochica2.detener({});
	$.widgetFotochica3.detener({});
	require('vars')['cual_foto'] = 3;
	$.widgetCamaralight.disparar({});

}

function Listo_widgetFotochica3(e) {

	var evento = e;
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	var nuevoid_f3 = ('nuevoid_f3' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['nuevoid_f3'] : '';
	if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
		var ID_944576973_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + seltarea.id_server);
		if (ID_944576973_d.exists() == false) ID_944576973_d.createDirectory();
		var ID_944576973_f = Ti.Filesystem.getFile(ID_944576973_d.resolve(), 'mini' + nuevoid_f3.id + '.jpg');
		if (ID_944576973_f.exists() == true) ID_944576973_f.deleteFile();
		ID_944576973_f.write(evento.mini);
		ID_944576973_d = null;
		ID_944576973_f = null;
	} else {
		var ID_1878835932_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + 0);
		if (ID_1878835932_d.exists() == false) ID_1878835932_d.createDirectory();
		var ID_1878835932_f = Ti.Filesystem.getFile(ID_1878835932_d.resolve(), 'mini' + nuevoid_f3.id + '.jpg');
		if (ID_1878835932_f.exists() == true) ID_1878835932_f.deleteFile();
		ID_1878835932_f.write(evento.mini);
		ID_1878835932_d = null;
		ID_1878835932_f = null;
	}
}

$.widgetModal.init({
	titulo: L('x767609104_traducir', 'RECINTOS'),
	cargando: L('x1831148736', 'cargando...'),
	__id: 'ALL1908023149',
	onrespuesta: Respuesta_widgetModal,
	campo: L('x382177638_traducir', 'Ubicación'),
	onabrir: Abrir_widgetModal,
	color: 'celeste',
	seleccione: L('x4060681104_traducir', 'recinto'),
	activo: true,
	onafterinit: Afterinit_widgetModal
});

function Afterinit_widgetModal(e) {

	var evento = e;
	var ID_590204985_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			var consultarModelo4_i = Alloy.createCollection('insp_recintos');
			var consultarModelo4_i_where = 'id_inspeccion=\'' + seltarea.id_server + '\'';
			consultarModelo4_i.fetch({
				query: 'SELECT * FROM insp_recintos WHERE id_inspeccion=\'' + seltarea.id_server + '\''
			});
			var recintos = require('helper').query2array(consultarModelo4_i);
			var datos = [];
			_.each(recintos, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'valor';
					if (llave == 'id_recinto') newkey = 'id_interno';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
		} else {
			var eliminarModelo4_i = Alloy.Collections.insp_recintos;
			var sql = "DELETE FROM " + eliminarModelo4_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo4_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo4_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo3_m = Alloy.Collections.insp_recintos;
				var insertarModelo3_fila = Alloy.createModel('insp_recintos', {
					nombre: String.format(L('x1101343128_traducir', 'Pieza%1$s'), item.toString()),
					id_recinto: item
				});
				insertarModelo3_m.add(insertarModelo3_fila);
				insertarModelo3_fila.save();
			});
			var transformarModelo4_i = Alloy.createCollection('insp_recintos');
			transformarModelo4_i.fetch();
			var transformarModelo4_src = require('helper').query2array(transformarModelo4_i);
			var datos = [];
			_.each(transformarModelo4_src, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'valor';
					if (llave == 'id_recinto') newkey = 'id_interno';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
		}
		$.widgetModal.data({
			data: datos
		});
	};
	var ID_590204985 = setTimeout(ID_590204985_func, 1000 * 0.1);

}

function Abrir_widgetModal(e) {

	var evento = e;

}

function Respuesta_widgetModal(e) {

	var evento = e;
	$.widgetModal.labels({
		valor: evento.valor
	});
	$.contenido.set({
		id_recinto: evento.item.id_interno
	});
	if ('contenido' in $) $contenido = $.contenido.toJSON();
	if (Ti.App.deployType != 'production') console.log('datos recibidos de modal ubicacion', {
		"datos": evento
	});

}

function Change_campo(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	$.contenido.set({
		cantidad: elemento
	});
	if ('contenido' in $) $contenido = $.contenido.toJSON();
	elemento = null, source = null;

}

function Click_vista25(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.DescribaelDao.blur();
	$.campo2.blur();
	$.campo.blur();
	$.widgetDpicker.abrir({});

}

$.widgetModal2.init({
	titulo: L('x1629775439_traducir', 'MONEDAS'),
	cargando: L('x1831148736', 'cargando...'),
	__id: 'ALL1996770915',
	onrespuesta: Respuesta_widgetModal2,
	campo: L('x3081186843_traducir', 'Moneda'),
	onabrir: Abrir_widgetModal2,
	color: 'celeste',
	seleccione: L('x2547889144', '-'),
	activo: true,
	onafterinit: Afterinit_widgetModal2
});

function Afterinit_widgetModal2(e) {

	var evento = e;
	var ID_1973605241_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			var consultarModelo5_i = Alloy.createCollection('monedas');
			var consultarModelo5_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
			consultarModelo5_i.fetch({
				query: 'SELECT * FROM monedas WHERE pais_texto=\'' + pais[0].nombre + '\''
			});
			var monedas = require('helper').query2array(consultarModelo5_i);
			var datos = [];
			_.each(monedas, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'valor';
					if (llave == 'id_segured') newkey = 'id_interno';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
		} else {
			var eliminarModelo5_i = Alloy.Collections.monedas;
			var sql = "DELETE FROM " + eliminarModelo5_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo5_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo5_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo4_m = Alloy.Collections.monedas;
				var insertarModelo4_fila = Alloy.createModel('monedas', {
					nombre: String.format(L('x4148455394_traducir', 'CLP%1$s'), item.toString()),
					id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item.toString())
				});
				insertarModelo4_m.add(insertarModelo4_fila);
				insertarModelo4_fila.save();
			});
			var transformarModelo6_i = Alloy.createCollection('monedas');
			transformarModelo6_i.fetch();
			var transformarModelo6_src = require('helper').query2array(transformarModelo6_i);
			var datos = [];
			_.each(transformarModelo6_src, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'valor';
					if (llave == 'id_segured') newkey = 'id_interno';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
		}
		$.widgetModal2.data({
			data: datos
		});
	};
	var ID_1973605241 = setTimeout(ID_1973605241_func, 1000 * 0.1);

}

function Abrir_widgetModal2(e) {

	var evento = e;
	$.widgetDpicker.cerrar({});

}

function Respuesta_widgetModal2(e) {

	var evento = e;
	$.widgetModal2.labels({
		valor: evento.valor
	});
	$.contenido.set({
		id_moneda: evento.item.id_interno
	});
	if ('contenido' in $) $contenido = $.contenido.toJSON();
	if (Ti.App.deployType != 'production') console.log('datos recibidos de modal monedas', {
		"datos": evento
	});

}

function Change_campo2(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	$.contenido.set({
		valor: elemento
	});
	if ('contenido' in $) $contenido = $.contenido.toJSON();
	elemento = null, source = null;

}

function Return_campo2(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	$.campo2.blur();
	elemento = null, source = null;

}

function Change_ID_274369569(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	if (elemento == true || elemento == 'true') {
		var si = L('x3746555228_traducir', 'SI');
		$.NO.setText(si);

		$.contenido.set({
			recupero: 1
		});
		if ('contenido' in $) $contenido = $.contenido.toJSON();
	} else {
		var no = L('x3376426101_traducir', 'NO');
		$.NO.setText(no);

		$.contenido.set({
			recupero: 0
		});
		if ('contenido' in $) $contenido = $.contenido.toJSON();
	}
	elemento = null;

}

function Change_DescribaelDao(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	$.contenido.set({
		descripcion: elemento
	});
	if ('contenido' in $) $contenido = $.contenido.toJSON();
	elemento = null, source = null;

}

$.widgetCamaralight.init({
	onfotolista: Fotolista_widgetCamaralight,
	__id: 'ALL544827748'
});

function Fotolista_widgetCamaralight(e) {

	var evento = e;
	/** 
	 * Recuperamos cual fue la foto seleccionada 
	 */
	var cual_foto = ('cual_foto' in require('vars')) ? require('vars')['cual_foto'] : '';
	if (cual_foto == 1 || cual_foto == '1') {
		var insertarModelo5_m = Alloy.Collections.numero_unico;
		var insertarModelo5_fila = Alloy.createModel('numero_unico', {
			comentario: 'nuevo danocontenido foto1'
		});
		insertarModelo5_m.add(insertarModelo5_fila);
		insertarModelo5_fila.save();
		var nuevoid_f1 = require('helper').model2object(insertarModelo5_m.last());
		/** 
		 * Guardamos en variable el detalle del modelo numero_unico para asignar informacion de foto1 del recinto 
		 */
		require('vars')[_var_scopekey]['nuevoid_f1'] = nuevoid_f1;
		/** 
		 * Recuperamos variable para saber si estamos haciendo una inspeccion o dummy y guardar la foto en la memoria del celular 
		 */
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var ID_428109606_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
			if (ID_428109606_d.exists() == false) ID_428109606_d.createDirectory();
			var ID_428109606_f = Ti.Filesystem.getFile(ID_428109606_d.resolve(), 'cap' + nuevoid_f1.id + '.jpg');
			if (ID_428109606_f.exists() == true) ID_428109606_f.deleteFile();
			ID_428109606_f.write(evento.foto);
			ID_428109606_d = null;
			ID_428109606_f = null;
		} else {
			var ID_819502760_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + 0);
			if (ID_819502760_d.exists() == false) ID_819502760_d.createDirectory();
			var ID_819502760_f = Ti.Filesystem.getFile(ID_819502760_d.resolve(), 'cap' + nuevoid_f1.id + '.jpg');
			if (ID_819502760_f.exists() == true) ID_819502760_f.deleteFile();
			ID_819502760_f.write(evento.foto);
			ID_819502760_d = null;
			ID_819502760_f = null;
		}
		/** 
		 * Actualizamos el modelo indicando cual es el nombre de la imagen 
		 */
		$.contenido.set({
			foto1: String.format(L('x1070475521', 'cap%1$s.jpg'), nuevoid_f1.id.toString())
		});
		if ('contenido' in $) $contenido = $.contenido.toJSON();
		/** 
		 * Enviamos la foto para que genere la miniatura (y tambien la guarde) y muestre en pantalla 
		 */
		$.widgetFotochica.procesar({
			imagen: evento.foto,
			nueva: 640,
			calidad: 91,
			camara: 'trasera'
		});
		/** 
		 * Limpiamos memoria ram 
		 */
		evento = null;
	} else if (cual_foto == 2) {
		var insertarModelo6_m = Alloy.Collections.numero_unico;
		var insertarModelo6_fila = Alloy.createModel('numero_unico', {
			comentario: 'nuevo danocontenido foto2'
		});
		insertarModelo6_m.add(insertarModelo6_fila);
		insertarModelo6_fila.save();
		var nuevoid_f2 = require('helper').model2object(insertarModelo6_m.last());
		require('vars')[_var_scopekey]['nuevoid_f2'] = nuevoid_f2;
		if (Ti.App.deployType != 'production') console.log(String.format(L('x2429469022', 'imagen capturada y asignada como cap%1$s.jpg'), nuevoid_f2.id.toString()), {});
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var ID_760531096_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
			if (ID_760531096_d.exists() == false) ID_760531096_d.createDirectory();
			var ID_760531096_f = Ti.Filesystem.getFile(ID_760531096_d.resolve(), 'cap' + nuevoid_f2.id + '.jpg');
			if (ID_760531096_f.exists() == true) ID_760531096_f.deleteFile();
			ID_760531096_f.write(evento.foto);
			ID_760531096_d = null;
			ID_760531096_f = null;
		} else {
			var ID_1716448501_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + 0);
			if (ID_1716448501_d.exists() == false) ID_1716448501_d.createDirectory();
			var ID_1716448501_f = Ti.Filesystem.getFile(ID_1716448501_d.resolve(), 'cap' + nuevoid_f2.id + '.jpg');
			if (ID_1716448501_f.exists() == true) ID_1716448501_f.deleteFile();
			ID_1716448501_f.write(evento.foto);
			ID_1716448501_d = null;
			ID_1716448501_f = null;
		}
		$.contenido.set({
			foto2: String.format(L('x1070475521', 'cap%1$s.jpg'), nuevoid_f2.id.toString())
		});
		if ('contenido' in $) $contenido = $.contenido.toJSON();
		$.widgetFotochica2.procesar({
			imagen: evento.foto,
			nueva: 640,
			calidad: 91,
			camara: 'trasera'
		});
		evento = null;
	} else if (cual_foto == 3) {
		var insertarModelo7_m = Alloy.Collections.numero_unico;
		var insertarModelo7_fila = Alloy.createModel('numero_unico', {
			comentario: 'nuevo danocontenido foto3'
		});
		insertarModelo7_m.add(insertarModelo7_fila);
		insertarModelo7_fila.save();
		var nuevoid_f3 = require('helper').model2object(insertarModelo7_m.last());
		require('vars')[_var_scopekey]['nuevoid_f3'] = nuevoid_f3;
		if (Ti.App.deployType != 'production') console.log(String.format(L('x2429469022', 'imagen capturada y asignada como cap%1$s.jpg'), nuevoid_f3.id.toString()), {});
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var ID_863981153_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
			if (ID_863981153_d.exists() == false) ID_863981153_d.createDirectory();
			var ID_863981153_f = Ti.Filesystem.getFile(ID_863981153_d.resolve(), 'cap' + nuevoid_f3.id + '.jpg');
			if (ID_863981153_f.exists() == true) ID_863981153_f.deleteFile();
			ID_863981153_f.write(evento.foto);
			ID_863981153_d = null;
			ID_863981153_f = null;
		} else {
			var ID_448730920_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + 0);
			if (ID_448730920_d.exists() == false) ID_448730920_d.createDirectory();
			var ID_448730920_f = Ti.Filesystem.getFile(ID_448730920_d.resolve(), 'cap' + nuevoid_f3.id + '.jpg');
			if (ID_448730920_f.exists() == true) ID_448730920_f.deleteFile();
			ID_448730920_f.write(evento.foto);
			ID_448730920_d = null;
			ID_448730920_f = null;
		}
		$.contenido.set({
			foto3: String.format(L('x1070475521', 'cap%1$s.jpg'), nuevoid_f3.id.toString())
		});
		if ('contenido' in $) $contenido = $.contenido.toJSON();
		$.widgetFotochica3.procesar({
			imagen: evento.foto,
			nueva: 640,
			calidad: 91,
			camara: 'trasera'
		});
		evento = null;
	}
}

$.widgetDpicker.init({
	__id: 'ALL1585064981',
	aceptar: L('x1518866076_traducir', 'Aceptar'),
	cancelar: L('x2353348866_traducir', 'Cancelar'),
	onaceptar: Aceptar_widgetDpicker,
	oncancelar: Cancelar_widgetDpicker
});

function Aceptar_widgetDpicker(e) {

	var evento = e;
	/** 
	 * Formateamos la fecha que responde el widget, mostramos en pantalla y actualizamos la fecha de compra 
	 */
	var moment = require('alloy/moment');
	var formatearFecha = evento.valor;
	var resp = moment(formatearFecha).format('DD-MM-YYYY');
	$.Fecha.setText(resp);

	var Fecha_estilo = 'estilo12';

	var _tmp_a4w = require('a4w');
	if ((typeof Fecha_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (Fecha_estilo in _tmp_a4w.styles['classes'])) {
		try {
			Fecha_estilo = _tmp_a4w.styles['classes'][Fecha_estilo];
		} catch (st_val_err) {}
	}
	_tmp_a4w = null;
	$.Fecha.applyProperties(Fecha_estilo);

	$.contenido.set({
		fecha_compra: evento.valor
	});
	if ('contenido' in $) $contenido = $.contenido.toJSON();

}

function Cancelar_widgetDpicker(e) {

	var evento = e;
	if (Ti.App.deployType != 'production') console.log('selector de fecha cancelado', {});

}

(function() {
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
		/** 
		 * Actualizamos el id de la inspeccion en la tabla de contenidos 
		 */
		$.contenido.set({
			id_inspeccion: seltarea.id_server,
			recupero: 0
		});
		if ('contenido' in $) $contenido = $.contenido.toJSON();
	}
	/** 
	 * Dejamos el scroll en el top y desenfocamos los campos de texto 
	 */
	var ID_1356571988_func = function() {
		$.scroll.scrollToTop();
		$.DescribaelDao.blur();
		$.campo2.blur();
		$.campo.blur();
	};
	var ID_1356571988 = setTimeout(ID_1356571988_func, 1000 * 0.2);
	/** 
	 * Modificamos el statusbar 
	 */
	var ID_338108629_func = function() {
		var nuevocontenido_statusbar = '#239EC4';

		var setearStatusColor = function(nuevocontenido_statusbar) {
			if (OS_IOS) {
				if (nuevocontenido_statusbar == 'light' || nuevocontenido_statusbar == 'claro') {
					$.nuevocontenido_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.LIGHT_CONTENT);
				} else if (nuevocontenido_statusbar == 'grey' || nuevocontenido_statusbar == 'gris' || nuevocontenido_statusbar == 'gray') {
					$.nuevocontenido_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.GREY);
				} else if (nuevocontenido_statusbar == 'oscuro' || nuevocontenido_statusbar == 'dark') {
					$.nuevocontenido_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.DEFAULT);
				}
			} else if (OS_ANDROID) {
				abx.setStatusbarColor(nuevocontenido_statusbar);
			}
		};
		setearStatusColor(nuevocontenido_statusbar);

	};
	var ID_338108629 = setTimeout(ID_338108629_func, 1000 * 0.1);
	_my_events['resp_dato1,ID_1243902372'] = function(evento) {
		if (Ti.App.deployType != 'production') console.log('detalle de la respuesta', {
			"datos": evento
		});
		$.SeleccioneProducto.setText(evento.nombre);

		$.SeleccioneProducto.setColor('#000000');

		/** 
		 * Asumimos que el valor mostrado seleccionado es el nombre de la fila dano, ya que no existe el campo nombre en esta pantalla. 
		 */
		$.contenido.set({
			nombre: evento.nombre,
			id_nombre: evento.id_segured
		});
		if ('contenido' in $) $contenido = $.contenido.toJSON();
	};
	Alloy.Events.on('resp_dato1', _my_events['resp_dato1,ID_1243902372']);
})();

function Postlayout_nuevocontenido(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var ID_1294585966_func = function() {
		/** 
		 * Una vez ya cargada la tarea, dejamos la variable limpia para poder abrir otras pantallas 
		 */
		require('vars')['var_abriendo'] = '';
	};
	var ID_1294585966 = setTimeout(ID_1294585966_func, 1000 * 0.2);

}