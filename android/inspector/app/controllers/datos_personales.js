var _bind4section = {};
var _list_templates = {};

var _activity;
if (OS_ANDROID) {
	_activity = $.ENROLAMIENTO2.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'ENROLAMIENTO2';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.ENROLAMIENTO2.addEventListener('open', function(e) {});
}
$.ENROLAMIENTO2.orientationModes = [Titanium.UI.PORTRAIT];


$.widgetBarra2.init({
	titulo: L('x2146494644_traducir', 'ENROLAMIENTO'),
	__id: 'ALL129849548',
	fondo: 'fondoblanco',
	colortitulo: 'negro',
	modal: L('', '')
});


$.widgetDpicker.init({
	__id: 'ALL1585064981',
	aceptar: L('x1518866076_traducir', 'Aceptar'),
	cancelar: L('x2353348866_traducir', 'Cancelar'),
	onaceptar: Aceptar_widgetDpicker,
	oncancelar: Cancelar_widgetDpicker
});

function Aceptar_widgetDpicker(e) {

	var evento = e;
	var moment = require('alloy/moment');
	var formatearFecha = evento.valor;
	var resp = moment(formatearFecha).format('DD-MM-YYYY');
	$.FechadeNacimiento2.setText(resp);

	var FechadeNacimiento2_estilo = 'estilo12';

	var _tmp_a4w = require('a4w');
	if ((typeof FechadeNacimiento2_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (FechadeNacimiento2_estilo in _tmp_a4w.styles['classes'])) {
		try {
			FechadeNacimiento2_estilo = _tmp_a4w.styles['classes'][FechadeNacimiento2_estilo];
		} catch (st_val_err) {}
	}
	_tmp_a4w = null;
	$.FechadeNacimiento2.applyProperties(FechadeNacimiento2_estilo);

	require('vars')['fecha_nacimiento'] = evento.valor;

}

function Cancelar_widgetDpicker(e) {

	var evento = e;
	if (Ti.App.deployType != 'production') console.log('selector de fecha cancelado', {});

}

$.widgetHeader2.init({
	titulo: L('x118417065_traducir', 'PARTE 2: Datos personales'),
	__id: 'ALL1146042904',
	avance: '2/6',
	onclick: Click_widgetHeader2
});

function Click_widgetHeader2(e) {

	var evento = e;
	$.EscribirNombre.blur();
	$.ApellidoPaterno2.blur();
	$.ApellidoMaterno2.blur();
	$.Escriba.blur();

}

function Focus_EscribirNombre(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;

	$.widgetDpicker.cerrar({});
	elemento = null, source = null;

}

function Focus_ApellidoPaterno2(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;

	$.widgetDpicker.cerrar({});
	elemento = null, source = null;

}

function Focus_ApellidoMaterno2(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;

	$.widgetDpicker.cerrar({});
	elemento = null, source = null;

}

function Focus_Escriba(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;

	$.widgetDpicker.cerrar({});
	elemento = null, source = null;

}

function Click_vista8(e) {

	e.cancelBubble = true;
	var elemento = e.source;

	$.widgetDpicker.abrir({});
	$.EscribirNombre.blur();
	$.ApellidoPaterno2.blur();
	$.ApellidoMaterno2.blur();
	$.Escriba.blur();

}

$.widgetBotonlargo2.init({
	titulo: L('x1524107289_traducir', 'CONTINUAR'),
	__id: 'ALL1860370347',
	onclick: Click_widgetBotonlargo2
});

function Click_widgetBotonlargo2(e) {

	var evento = e;
	/** 
	 * Obtenemos el ano actual y guardamos en variable 
	 */
	var moment = require('alloy/moment');
	var hoy_ano = moment(new Date()).format('YYYY');
	/** 
	 * Recuperamos variable fecha_nacimiento para obtener el ano 
	 */
	var fecha_nacimiento = ('fecha_nacimiento' in require('vars')) ? require('vars')['fecha_nacimiento'] : '';
	var moment = require('alloy/moment');
	var formatearFecha3 = fecha_nacimiento;
	var sel_ano = moment(formatearFecha3).format('YYYY');
	if (Ti.App.deployType != 'production') console.log('fechas de nacimiento', {
		"fecha_nacimiento": String.format(L('x119665581_traducir', '%1$s y el largo es %2$s'), (sel_ano) ? sel_ano.toString() : '', (fecha_nacimiento.lengh) ? fecha_nacimiento.lengh.toString() : ''),
		"hoy": hoy_ano,
		"tipo de datos": String.format(L('x1045525933', '%1$s %2$s'), (typeof hoy_ano) ? typeof hoy_ano.toString() : '', (typeof sel_ano) ? typeof sel_ano.toString() : '')
	});
	/** 
	 * Obtenemos los datos ingresados en los campos 
	 */
	var nombre;
	nombre = $.EscribirNombre.getValue();

	var apellido_paterno;
	apellido_paterno = $.ApellidoPaterno2.getValue();

	var apellido_materno;
	apellido_materno = $.ApellidoMaterno2.getValue();

	var codigo_verificador;
	codigo_verificador = $.Escriba.getValue();

	if ((_.isObject(nombre) || _.isString(nombre)) && _.isEmpty(nombre)) {
		/** 
		 * Levantamos mensaje de error en caso de que hayan campos vacios o el inspector tenga menos de 18 anos 
		 */
		var preguntarAlerta_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta = Ti.UI.createAlertDialog({
			title: L('x2185084353_traducir', 'Atención'),
			message: L('x281530740_traducir', 'Ingrese nombres'),
			buttonNames: preguntarAlerta_opts
		});
		preguntarAlerta.addEventListener('click', function(e) {
			var suu = preguntarAlerta_opts[e.index];
			suu = null;

			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta.show();
	} else if ((_.isObject(apellido_paterno) || _.isString(apellido_paterno)) && _.isEmpty(apellido_paterno)) {
		var preguntarAlerta2_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta2 = Ti.UI.createAlertDialog({
			title: L('x2185084353_traducir', 'Atención'),
			message: L('x3533941670_traducir', 'Ingrese apellido paterno'),
			buttonNames: preguntarAlerta2_opts
		});
		preguntarAlerta2.addEventListener('click', function(e) {
			var suu = preguntarAlerta2_opts[e.index];
			suu = null;

			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta2.show();
	} else if ((_.isObject(codigo_verificador) || _.isString(codigo_verificador)) && _.isEmpty(codigo_verificador)) {
		var preguntarAlerta3_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta3 = Ti.UI.createAlertDialog({
			title: L('x2185084353_traducir', 'Atención'),
			message: L('x676730691_traducir', 'Ingrese rut'),
			buttonNames: preguntarAlerta3_opts
		});
		preguntarAlerta3.addEventListener('click', function(e) {
			var suu = preguntarAlerta3_opts[e.index];
			suu = null;

			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta3.show();
	} else if (_.isNull(fecha_nacimiento)) {
		var preguntarAlerta4_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta4 = Ti.UI.createAlertDialog({
			title: L('x2185084353_traducir', 'Atención'),
			message: L('x1503656371_traducir', 'Ingrese fecha de nacimiento'),
			buttonNames: preguntarAlerta4_opts
		});
		preguntarAlerta4.addEventListener('click', function(e) {
			var suu = preguntarAlerta4_opts[e.index];
			suu = null;

			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta4.show();
	} else if ((fecha_nacimiento.length) == 0 || (fecha_nacimiento.length) == '0') {
		var preguntarAlerta5_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta5 = Ti.UI.createAlertDialog({
			title: L('x2185084353_traducir', 'Atención'),
			message: L('x1503656371_traducir', 'Ingrese fecha de nacimiento'),
			buttonNames: preguntarAlerta5_opts
		});
		preguntarAlerta5.addEventListener('click', function(e) {
			var suu = preguntarAlerta5_opts[e.index];
			suu = null;

			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta5.show();
	} else if (_.isNumber(hoy_ano - sel_ano) && _.isNumber(18) && hoy_ano - sel_ano < 18) {
		var preguntarAlerta6_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta6 = Ti.UI.createAlertDialog({
			title: L('x2185084353_traducir', 'Atención'),
			message: L('x3243799351_traducir', 'Debe ser mayor de edad'),
			buttonNames: preguntarAlerta6_opts
		});
		preguntarAlerta6.addEventListener('click', function(e) {
			var suu = preguntarAlerta6_opts[e.index];
			suu = null;

			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta6.show();
	} else {
		/** 
		 * Mostramos animacion de progreso en el boton continuar 
		 */

		$.widgetBotonlargo2.iniciar_progreso({});
		/** 
		 * Recuperamos la variable registro y agregamos los campos de la pantalla 
		 */
		var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
		var registro = _.extend(registro, {
			nombre: nombre,
			apellido_paterno: apellido_paterno,
			apellido_materno: apellido_materno,
			codigo_identificador: codigo_verificador,
			fecha_nacimiento: fecha_nacimiento
		});
		require('vars')['registro'] = registro;
		var ID_877726171_func = function() {
			/** 
			 * Detenemos animacion de progreso en el boton continuar 
			 */

			$.widgetBotonlargo2.detener_progreso({});
			/** 
			 * Enviamos a la proxima pantalla, domicilio 
			 */
			Alloy.createController("domicilio", {}).getView().open();
		};
		var ID_877726171 = setTimeout(ID_877726171_func, 1000 * 0.3);
	}
}

(function() {
	var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
	var consultarModelo3_i = Alloy.createCollection('pais');
	var consultarModelo3_i_where = 'id_server=\'' + registro.pais + '\'';
	consultarModelo3_i.fetch({
		query: 'SELECT * FROM pais WHERE id_server=\'' + registro.pais + '\''
	});
	var pais_iter = require('helper').query2array(consultarModelo3_i);
	/** 
	 * Modificamos el label para poner el codigo identificador de una persona 
	 */
	$.RUT.setText(pais_iter[0].label_codigo_identificador);

	var hint;
	hint = $.Escriba.getHintText();

	/** 
	 * Modificamos el hint para poner el codigo identificador de una persona 
	 */
	$.Escriba.setHintText(String.format(L('x1045525933', '%1$s %2$s'), (hint) ? hint.toString() : '', (pais_iter[0].label_codigo_identificador) ? pais_iter[0].label_codigo_identificador.toString() : ''));

	if (Ti.App.deployType != 'production') console.log('DATOS', {
		"del pais": pais_iter[0]
	});
	/** 
	 * Creamos evento que al cerrar desde la ultima pantalla, esta tambi&#233;n se cierre. Evitamos el uso excesivo de memoria ram 
	 */

	_my_events['_close_enrolamiento,ID_131518285'] = function(evento) {
		$.ENROLAMIENTO2.close();
		if (Ti.App.deployType != 'production') console.log('escuchando cerrar enrolamiento datospersonales', {});
	};
	Alloy.Events.on('_close_enrolamiento', _my_events['_close_enrolamiento,ID_131518285']);
})();

function Androidback_ENROLAMIENTO2(e) {
	/** 
	 * Dejamos esta accion vacia para que no pueda volver a la pantalla anterior 
	 */

	e.cancelBubble = true;
	var elemento = e.source;

}
if (OS_IOS || OS_ANDROID) {
	$.ENROLAMIENTO2.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}