var _bind4section = {
	"ref1": "insp_itemdanos"
};
var _list_templates = {
	"elemento": {
		"vista2": {},
		"Label3": {
			"text": "{nombre}"
		},
		"Label2": {
			"text": "{id_server}"
		},
		"Label": {
			"text": "{id_segured}"
		},
		"vista": {}
	},
	"dano": {
		"Label2": {
			"text": "{id}"
		},
		"vista23": {},
		"Label": {
			"text": "{nombre}"
		}
	},
	"pborrar": {
		"Label4": {
			"text": "{id}"
		},
		"vista25": {},
		"Label3": {
			"text": "{nombre}"
		},
		"vista24": {},
		"vista26": {},
		"imagen2": {}
	}
};
var $recinto1 = $.recinto1.toJSON();

var _activity;
if (OS_ANDROID) {
	_activity = $.nuevorecinto.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'nuevorecinto';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.nuevorecinto.addEventListener('open', function(e) {});
}
$.nuevorecinto.orientationModes = [Titanium.UI.PORTRAIT];


var consultarModelo_like = function(search) {
	if (typeof search !== 'string' || this === null) {
		return false;
	}
	search = search.replace(new RegExp("([\\.\\\\\\+\\*\\?\\[\\^\\]\\$\\(\\)\\{\\}\\=\\!\\<\\>\\|\\:\\-])", "g"), "\\$1");
	search = search.replace(/%/g, '.*').replace(/_/g, '.');
	return RegExp('^' + search + '$', 'gi').test(this);
};
var consultarModelo_filter = function(coll) {
	var filtered = _.toArray(coll.filter(function(m) {
		return true;
	}));
	return filtered;
};
var consultarModelo_transform = function(model) {
	var modelo = model.toJSON();
	return modelo;
};
_.defer(function() {
	Alloy.Collections.insp_itemdanos.fetch();
});
Alloy.Collections.insp_itemdanos.on('add change delete', function(ee) {
	consultarModelo_update(ee);
});
var consultarModelo_update = function(evento) {
	var consultarModelo2_i = Alloy.createCollection('insp_niveles');
	var consultarModelo2_i_where = '';
	consultarModelo2_i.fetch();
	var insp_n = require('helper').query2array(consultarModelo2_i);
	var alto = 55 * insp_n.length;
	if (Ti.App.deployType != 'production') console.log('datos al actualizar el alto', {
		"alto": alto,
		"cant_niv": insp_n.lenght
	});
	var listado_alto = alto;

	if (listado_alto == '*') {
		listado_alto = Ti.UI.FILL;
	} else if (!isNaN(listado_alto)) {
		listado_alto = listado_alto + 'dp';
	}
	$.listado.setHeight(listado_alto);

};
Alloy.Collections.insp_itemdanos.fetch();


$.widgetBarra.init({
	titulo: L('x1471097171_traducir', 'NUEVO RECINTO'),
	__id: 'ALL347300010',
	oncerrar: Cerrar_widgetBarra,
	textoderecha: L('x2943883035_traducir', 'Guardar'),
	fondo: 'fondomorado',
	top: 0,
	modal: L('', ''),
	onpresiono: Presiono_widgetBarra,
	colortextoderecha: 'blanco'
});

function Cerrar_widgetBarra(e) {

	var evento = e;
	/** 
	 * Limpiamos widgets multiples (memoria) 
	 */
	$.widgetModalmultiple.limpiar({});
	$.widgetModalmultiple2.limpiar({});
	$.widgetModalmultiple3.limpiar({});
	$.widgetModalmultiple4.limpiar({});
	$.widgetModalmultiple5.limpiar({});
	$.widgetModalmultiple6.limpiar({});
	$.nuevorecinto.close();

}

function Presiono_widgetBarra(e) {

	var evento = e;
	var temp_idrecinto = ('temp_idrecinto' in require('vars')) ? require('vars')['temp_idrecinto'] : '';
	/** 
	 * Consultamos la tabla de danos, filtrando por el id del recinto y saber si tiene danos 
	 */
	var consultarModelo3_i = Alloy.createCollection('insp_itemdanos');
	var consultarModelo3_i_where = 'id_recinto=\'' + temp_idrecinto + '\'';
	consultarModelo3_i.fetch({
		query: 'SELECT * FROM insp_itemdanos WHERE id_recinto=\'' + temp_idrecinto + '\''
	});
	var danos_verificacion = require('helper').query2array(consultarModelo3_i);
	if (_.isUndefined($recinto1.nombre)) {
		/** 
		 * Validamos que existan los datos ingresados y que esten bien 
		 */
		var preguntarAlerta_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x995453488_traducir', 'Ingrese el nombre del recinto'),
			buttonNames: preguntarAlerta_opts
		});
		preguntarAlerta.addEventListener('click', function(e) {
			var nulo = preguntarAlerta_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta.show();
	} else if ((_.isObject($recinto1.nombre) || _.isString($recinto1.nombre)) && _.isEmpty($recinto1.nombre)) {
		var preguntarAlerta2_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta2 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x995453488_traducir', 'Ingrese el nombre del recinto'),
			buttonNames: preguntarAlerta2_opts
		});
		preguntarAlerta2.addEventListener('click', function(e) {
			var nulo = preguntarAlerta2_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta2.show();
	} else if (_.isUndefined($recinto1.id_nivel)) {
		var preguntarAlerta3_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta3 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x2221207910_traducir', 'Seleccione el nivel al cual pertenece el nuevo recinto'),
			buttonNames: preguntarAlerta3_opts
		});
		preguntarAlerta3.addEventListener('click', function(e) {
			var nulo = preguntarAlerta3_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta3.show();
	} else if (_.isUndefined($recinto1.largo)) {
		var preguntarAlerta4_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta4 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x2947701485_traducir', 'Ingrese largo del recinto'),
			buttonNames: preguntarAlerta4_opts
		});
		preguntarAlerta4.addEventListener('click', function(e) {
			var nulo = preguntarAlerta4_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta4.show();
	} else if ((_.isObject($recinto1.largo) || _.isString($recinto1.largo)) && _.isEmpty($recinto1.largo)) {
		var preguntarAlerta5_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta5 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x2947701485_traducir', 'Ingrese largo del recinto'),
			buttonNames: preguntarAlerta5_opts
		});
		preguntarAlerta5.addEventListener('click', function(e) {
			var nulo = preguntarAlerta5_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta5.show();
	} else if (_.isUndefined($recinto1.ancho)) {
		var preguntarAlerta6_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta6 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x2565566100_traducir', 'Ingrese ancho del recinto'),
			buttonNames: preguntarAlerta6_opts
		});
		preguntarAlerta6.addEventListener('click', function(e) {
			var nulo = preguntarAlerta6_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta6.show();
	} else if ((_.isObject($recinto1.ancho) || _.isString($recinto1.ancho)) && _.isEmpty($recinto1.ancho)) {
		var preguntarAlerta7_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta7 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x2682783608_traducir', 'Ingrese ancho del nivel'),
			buttonNames: preguntarAlerta7_opts
		});
		preguntarAlerta7.addEventListener('click', function(e) {
			var nulo = preguntarAlerta7_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta7.show();
	} else if (_.isUndefined($recinto1.alto)) {
		var preguntarAlerta8_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta8 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x4120715490_traducir', 'Ingrese la altura del nivel'),
			buttonNames: preguntarAlerta8_opts
		});
		preguntarAlerta8.addEventListener('click', function(e) {
			var nulo = preguntarAlerta8_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta8.show();
	} else if ((_.isObject($recinto1.alto) || _.isString($recinto1.alto)) && _.isEmpty($recinto1.alto)) {
		var preguntarAlerta9_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta9 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x4120715490_traducir', 'Ingrese la altura del nivel'),
			buttonNames: preguntarAlerta9_opts
		});
		preguntarAlerta9.addEventListener('click', function(e) {
			var nulo = preguntarAlerta9_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta9.show();
	} else if (_.isUndefined($recinto1.ids_estructuras_soportantes)) {
		var preguntarAlerta10_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta10 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x503969960_traducir', 'Seleccione la estructura soportante del recinto'),
			buttonNames: preguntarAlerta10_opts
		});
		preguntarAlerta10.addEventListener('click', function(e) {
			var nulo = preguntarAlerta10_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta10.show();
	} else if ((_.isObject($recinto1.ids_estructuras_soportantes) || _.isString($recinto1.ids_estructuras_soportantes)) && _.isEmpty($recinto1.ids_estructuras_soportantes)) {
		var preguntarAlerta11_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta11 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x503969960_traducir', 'Seleccione la estructura soportante del recinto'),
			buttonNames: preguntarAlerta11_opts
		});
		preguntarAlerta11.addEventListener('click', function(e) {
			var nulo = preguntarAlerta11_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta11.show();
	} else if (danos_verificacion && danos_verificacion.length == 0) {
		var preguntarAlerta12_opts = [L('x1602277533_traducir', 'GUARDAR'), L('x3812490383_traducir', ' VOLVER')];
		var preguntarAlerta12 = Ti.UI.createAlertDialog({
			title: L('x1927199828_traducir', '¿Guardar sin daños?'),
			message: L('x2099678332_traducir', 'Estas guardando el recinto sin daños asociados, si esta correcto presiona guardar'),
			buttonNames: preguntarAlerta12_opts
		});
		preguntarAlerta12.addEventListener('click', function(e) {
			var msj = preguntarAlerta12_opts[e.index];
			if (msj == L('x1602277533_traducir', 'GUARDAR')) {
				Alloy.Collections[$.recinto1.config.adapter.collection_name].add($.recinto1);
				$.recinto1.save();
				Alloy.Collections[$.recinto1.config.adapter.collection_name].fetch();
				test = null;
				$.widgetModalmultiple.limpiar({});
				$.widgetModalmultiple2.limpiar({});
				$.widgetModalmultiple3.limpiar({});
				$.widgetModalmultiple4.limpiar({});
				$.widgetModalmultiple5.limpiar({});
				$.widgetModalmultiple6.limpiar({});
				$.nuevorecinto.close();
			}
			msj = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta12.show();
	} else {
		/** 
		 * Guardamos el modelo con los datos ingresado 
		 */
		Alloy.Collections[$.recinto1.config.adapter.collection_name].add($.recinto1);
		$.recinto1.save();
		Alloy.Collections[$.recinto1.config.adapter.collection_name].fetch();
		/** 
		 * Limpiamos widgets multiples (memoria) 
		 */
		$.widgetModalmultiple.limpiar({});
		$.widgetModalmultiple2.limpiar({});
		$.widgetModalmultiple3.limpiar({});
		$.widgetModalmultiple4.limpiar({});
		$.widgetModalmultiple5.limpiar({});
		$.widgetModalmultiple6.limpiar({});
		$.nuevorecinto.close();
	}
}

function Change_EscribaNombre(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	/** 
	 * Guardamos el nombre del recinto en el modelo 
	 */
	$.recinto1.set({
		nombre: elemento
	});
	if ('recinto1' in $) $recinto1 = $.recinto1.toJSON();
	require('vars')['nombrerecinto'] = elemento;
	elemento = null, source = null;

}

function Return_EscribaNombre(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	/** 
	 * Al presionar enter en el teclado del equipo, cerramos el teclado y evitamos pasar al prox campo 
	 */
	$.EscribaNombre.blur();
	elemento = null, source = null;

}

$.widgetModal.init({
	titulo: L('x1571349115_traducir', 'NIVEL'),
	cargando: L('x1831148736', 'cargando...'),
	__id: 'ALL318354873',
	left: 0,
	onrespuesta: Respuesta_widgetModal,
	campo: L('x2771220443_traducir', 'Nivel del Recinto'),
	onabrir: Abrir_widgetModal,
	color: 'morado',
	right: 0,
	seleccione: L('x2434264250_traducir', 'seleccione nivel'),
	activo: true,
	onafterinit: Afterinit_widgetModal
});

function Abrir_widgetModal(e) {

	var evento = e;

}

function Respuesta_widgetModal(e) {

	var evento = e;
	/** 
	 * Ponemos el texto del valor escogido en el widget 
	 */
	$.widgetModal.labels({
		valor: evento.valor
	});
	/** 
	 * Guardamos el id del nivel que seleccionamos 
	 */
	$.recinto1.set({
		id_nivel: evento.item.id_interno
	});
	if ('recinto1' in $) $recinto1 = $.recinto1.toJSON();

}

function Afterinit_widgetModal(e) {

	var evento = e;
	var ID_5498125_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			/** 
			 * Revisamos si estamos haciendo una inspeccion o es un dummy 
			 */
			var consultarModelo4_i = Alloy.createCollection('insp_niveles');
			var consultarModelo4_i_where = 'id_inspeccion=\'' + seltarea.id_server + '\'';
			consultarModelo4_i.fetch({
				query: 'SELECT * FROM insp_niveles WHERE id_inspeccion=\'' + seltarea.id_server + '\''
			});
			var datosniveles = require('helper').query2array(consultarModelo4_i);
			/** 
			 * Obtenemos datos para selectores (solo los de esta inspeccion) 
			 */
			var datos = [];
			_.each(datosniveles, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'valor';
					if (llave == 'id') newkey = 'id_interno';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
			$.widgetModal.data({
				data: datos
			});
		} else {
			var eliminarModelo_i = Alloy.Collections.insp_niveles;
			var sql = "DELETE FROM " + eliminarModelo_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4,5'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo_m = Alloy.Collections.insp_niveles;
				var insertarModelo_fila = Alloy.createModel('insp_niveles', {
					id_inspeccion: -1,
					nombre: String.format(L('x2818462194_traducir', 'Nivel%1$s'), item.toString()),
					piso: item
				});
				insertarModelo_m.add(insertarModelo_fila);
				insertarModelo_fila.save();
				_.defer(function() {});
			});
			var transformarModelo2_i = Alloy.createCollection('insp_niveles');
			transformarModelo2_i.fetch();
			var transformarModelo2_src = require('helper').query2array(transformarModelo2_i);
			var datos = [];
			_.each(transformarModelo2_src, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'valor';
					if (llave == 'id') newkey = 'id_interno';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
			$.widgetModal.data({
				data: datos
			});
		}
	};
	var ID_5498125 = setTimeout(ID_5498125_func, 1000 * 0.2);

}

$.widgetFotochica.init({
	caja: 45,
	__id: 'ALL710129457',
	onlisto: Listo_widgetFotochica,
	left: 0,
	top: 0,
	onclick: Click_widgetFotochica
});

function Click_widgetFotochica(e) {

	var evento = e;
	/** 
	 * Definimos que estamos capturando foto en el primer item 
	 */
	require('vars')[_var_scopekey]['cual_foto'] = L('x2212294583', '1');
	/** 
	 * Abrimos camara 
	 */
	$.widgetCamaralight.disparar({});
	/** 
	 * Detenemos las animaciones de los widget 
	 */
	$.widgetFotochica2.detener({});
	$.widgetFotochica3.detener({});
	$.widgetFotochica.detener({});

}

function Listo_widgetFotochica(e) {

	var evento = e;
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	var nuevoid_f1 = ('nuevoid_f1' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['nuevoid_f1'] : '';
	if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
		/** 
		 * Revisamos si es inspeccion o dummy, para definir en que carpeta se guardaran los archivos 
		 */
		var ID_1194766412_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + seltarea.id_server);
		if (ID_1194766412_d.exists() == false) ID_1194766412_d.createDirectory();
		var ID_1194766412_f = Ti.Filesystem.getFile(ID_1194766412_d.resolve(), 'mini' + nuevoid_f1.id + '.jpg');
		if (ID_1194766412_f.exists() == true) ID_1194766412_f.deleteFile();
		ID_1194766412_f.write(evento.mini);
		ID_1194766412_d = null;
		ID_1194766412_f = null;
	} else {
		var ID_1143267424_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + 0);
		if (ID_1143267424_d.exists() == false) ID_1143267424_d.createDirectory();
		var ID_1143267424_f = Ti.Filesystem.getFile(ID_1143267424_d.resolve(), 'mini' + nuevoid_f1.id + '.jpg');
		if (ID_1143267424_f.exists() == true) ID_1143267424_f.deleteFile();
		ID_1143267424_f.write(evento.mini);
		ID_1143267424_d = null;
		ID_1143267424_f = null;
	}
}

$.widgetFotochica2.init({
	caja: 45,
	__id: 'ALL574301926',
	onlisto: Listo_widgetFotochica2,
	left: 0,
	top: 0,
	onclick: Click_widgetFotochica2
});

function Click_widgetFotochica2(e) {

	var evento = e;
	require('vars')[_var_scopekey]['cual_foto'] = 2;
	$.widgetCamaralight.disparar({});
	$.widgetFotochica3.detener({});
	$.widgetFotochica.detener({});
	$.widgetFotochica2.detener({});

}

function Listo_widgetFotochica2(e) {

	var evento = e;
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	var nuevoid_f2 = ('nuevoid_f2' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['nuevoid_f2'] : '';
	if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
		var ID_766059429_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + seltarea.id_server);
		if (ID_766059429_d.exists() == false) ID_766059429_d.createDirectory();
		var ID_766059429_f = Ti.Filesystem.getFile(ID_766059429_d.resolve(), 'mini' + nuevoid_f2.id + '.jpg');
		if (ID_766059429_f.exists() == true) ID_766059429_f.deleteFile();
		ID_766059429_f.write(evento.mini);
		ID_766059429_d = null;
		ID_766059429_f = null;
	} else {
		var ID_1011270433_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + 0);
		if (ID_1011270433_d.exists() == false) ID_1011270433_d.createDirectory();
		var ID_1011270433_f = Ti.Filesystem.getFile(ID_1011270433_d.resolve(), 'mini' + nuevoid_f2.id + '.jpg');
		if (ID_1011270433_f.exists() == true) ID_1011270433_f.deleteFile();
		ID_1011270433_f.write(evento.mini);
		ID_1011270433_d = null;
		ID_1011270433_f = null;
	}
}

$.widgetFotochica3.init({
	caja: 45,
	__id: 'ALL222932900',
	onlisto: Listo_widgetFotochica3,
	left: 0,
	top: 0,
	onclick: Click_widgetFotochica3
});

function Click_widgetFotochica3(e) {

	var evento = e;
	require('vars')[_var_scopekey]['cual_foto'] = 3;
	$.widgetCamaralight.disparar({});
	$.widgetFotochica2.detener({});
	$.widgetFotochica.detener({});
	$.widgetFotochica3.detener({});

}

function Listo_widgetFotochica3(e) {

	var evento = e;
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	var nuevoid_f3 = ('nuevoid_f3' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['nuevoid_f3'] : '';
	if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
		var ID_944576973_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + seltarea.id_server);
		if (ID_944576973_d.exists() == false) ID_944576973_d.createDirectory();
		var ID_944576973_f = Ti.Filesystem.getFile(ID_944576973_d.resolve(), 'mini' + nuevoid_f3.id + '.jpg');
		if (ID_944576973_f.exists() == true) ID_944576973_f.deleteFile();
		ID_944576973_f.write(evento.mini);
		ID_944576973_d = null;
		ID_944576973_f = null;
	} else {
		var ID_1878835932_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + 0);
		if (ID_1878835932_d.exists() == false) ID_1878835932_d.createDirectory();
		var ID_1878835932_f = Ti.Filesystem.getFile(ID_1878835932_d.resolve(), 'mini' + nuevoid_f3.id + '.jpg');
		if (ID_1878835932_f.exists() == true) ID_1878835932_f.deleteFile();
		ID_1878835932_f.write(evento.mini);
		ID_1878835932_d = null;
		ID_1878835932_f = null;
	}
}

function Change_campo(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	/** 
	 * Obtenemos el valor ingresado en campo de ancho, para poder calcular la superficie 
	 */
	var ancho;
	ancho = $.campo2.getValue();

	if ((_.isObject(ancho) || (_.isString(ancho)) && !_.isEmpty(ancho)) || _.isNumber(ancho) || _.isBoolean(ancho)) {
		if ((_.isObject(elemento) || (_.isString(elemento)) && !_.isEmpty(elemento)) || _.isNumber(elemento) || _.isBoolean(elemento)) {
			/** 
			 * nos aseguramos que ancho y valor de campo sean decimales con puntos (flotante), y no comas (espa&#241;ol) 
			 */
			var nuevo = parseFloat(ancho.split(',').join('.')) * parseFloat(elemento.split(',').join('.'));
			$.recinto1.set({
				superficie: nuevo
			});
			if ('recinto1' in $) $recinto1 = $.recinto1.toJSON();
		}
	}
	/** 
	 * Guardamos el largo del recinto 
	 */
	$.recinto1.set({
		largo: elemento
	});
	if ('recinto1' in $) $recinto1 = $.recinto1.toJSON();
	elemento = null, source = null;

}

function Change_campo2(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	var largo;
	largo = $.campo.getValue();

	if ((_.isObject(largo) || (_.isString(largo)) && !_.isEmpty(largo)) || _.isNumber(largo) || _.isBoolean(largo)) {
		if ((_.isObject(elemento) || (_.isString(elemento)) && !_.isEmpty(elemento)) || _.isNumber(elemento) || _.isBoolean(elemento)) {
			/** 
			 * nos aseguramos que largo y valor de campo sean decimales con puntos (flotante), y no comas (espa&#241;ol) 
			 */
			var nuevo = parseFloat(largo.split(',').join('.')) * parseFloat(elemento.split(',').join('.'));
			$.recinto1.set({
				superficie: nuevo
			});
			if ('recinto1' in $) $recinto1 = $.recinto1.toJSON();
		}
	}
	$.recinto1.set({
		ancho: elemento
	});
	if ('recinto1' in $) $recinto1 = $.recinto1.toJSON();
	elemento = null, source = null;

}

function Change_campo3(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	$.recinto1.set({
		alto: elemento
	});
	if ('recinto1' in $) $recinto1 = $.recinto1.toJSON();
	elemento = null, source = null;

}

$.widgetModalmultiple.init({
	titulo: L('x1975271086_traducir', 'Estructura Soportante'),
	cargando: L('x1831148736', 'cargando...'),
	__id: 'ALL927397228',
	oncerrar: Cerrar_widgetModalmultiple,
	left: 0,
	hint: L('x2898603391_traducir', 'Seleccione estructura'),
	color: 'morado',
	subtitulo: L('x4011106049_traducir', 'Indique los tipos'),
	right: 0,
	onclick: Click_widgetModalmultiple,
	onafterinit: Afterinit_widgetModalmultiple
});

function Click_widgetModalmultiple(e) {

	var evento = e;

}

function Cerrar_widgetModalmultiple(e) {

	var evento = e;
	$.widgetModalmultiple.update({});
	$.recinto1.set({
		ids_estructuras_soportantes: evento.valores
	});
	if ('recinto1' in $) $recinto1 = $.recinto1.toJSON();

}

function Afterinit_widgetModalmultiple(e) {

	var evento = e;
	var ID_557613849_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			var consultarModelo5_i = Alloy.createCollection('estructura_soportante');
			var consultarModelo5_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
			consultarModelo5_i.fetch({
				query: 'SELECT * FROM estructura_soportante WHERE pais_texto=\'' + pais[0].nombre + '\''
			});
			var estructura = require('helper').query2array(consultarModelo5_i);
			var datos = [];
			_.each(estructura, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'label';
					if (llave == 'id_segured') newkey = 'valor';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				new_row['estado'] = 0;
				datos.push(new_row);
			});
			$.widgetModalmultiple.update({
				data: datos
			});
		} else {
			var eliminarModelo2_i = Alloy.Collections.estructura_soportante;
			var sql = "DELETE FROM " + eliminarModelo2_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo2_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo2_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo2_m = Alloy.Collections.estructura_soportante;
				var insertarModelo2_fila = Alloy.createModel('estructura_soportante', {
					nombre: String.format(L('x1088195980_traducir', 'Estructura%1$s'), item.toString()),
					id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
					id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item.toString()),
					pais: 1
				});
				insertarModelo2_m.add(insertarModelo2_fila);
				insertarModelo2_fila.save();
				_.defer(function() {});
			});
			var transformarModelo4_i = Alloy.createCollection('estructura_soportante');
			transformarModelo4_i.fetch();
			var transformarModelo4_src = require('helper').query2array(transformarModelo4_i);
			var datos = [];
			_.each(transformarModelo4_src, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'label';
					if (llave == 'id_segured') newkey = 'valor';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				new_row['estado'] = 0;
				datos.push(new_row);
			});
			$.widgetModalmultiple.update({
				data: datos
			});
		}
	};
	var ID_557613849 = setTimeout(ID_557613849_func, 1000 * 0.2);

}

$.widgetModalmultiple2.init({
	titulo: L('x1219835481_traducir', 'Muros / Tabiques'),
	cargando: L('x1831148736', 'cargando...'),
	__id: 'ALL346546200',
	oncerrar: Cerrar_widgetModalmultiple2,
	left: 0,
	hint: L('x2879998099_traducir', 'Seleccione muros'),
	color: 'morado',
	subtitulo: L('x4011106049_traducir', 'Indique los tipos'),
	right: 0,
	onclick: Click_widgetModalmultiple2,
	onafterinit: Afterinit_widgetModalmultiple2
});

function Click_widgetModalmultiple2(e) {

	var evento = e;

}

function Cerrar_widgetModalmultiple2(e) {

	var evento = e;
	$.widgetModalmultiple2.update({});
	$.recinto1.set({
		ids_muros: evento.valores
	});
	if ('recinto1' in $) $recinto1 = $.recinto1.toJSON();

}

function Afterinit_widgetModalmultiple2(e) {

	var evento = e;
	var ID_1891160530_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			var consultarModelo6_i = Alloy.createCollection('muros_tabiques');
			var consultarModelo6_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
			consultarModelo6_i.fetch({
				query: 'SELECT * FROM muros_tabiques WHERE pais_texto=\'' + pais[0].nombre + '\''
			});
			var tabiques = require('helper').query2array(consultarModelo6_i);
			var datos = [];
			_.each(tabiques, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'label';
					if (llave == 'id_segured') newkey = 'valor';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				new_row['estado'] = 0;
				datos.push(new_row);
			});
			$.widgetModalmultiple2.update({
				data: datos
			});
		} else {
			var eliminarModelo3_i = Alloy.Collections.muros_tabiques;
			var sql = "DELETE FROM " + eliminarModelo3_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo3_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo3_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo3_m = Alloy.Collections.muros_tabiques;
				var insertarModelo3_fila = Alloy.createModel('muros_tabiques', {
					nombre: String.format(L('x3565664878_traducir', 'Muros%1$s'), item.toString()),
					id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
					id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item.toString()),
					pais: 1
				});
				insertarModelo3_m.add(insertarModelo3_fila);
				insertarModelo3_fila.save();
				_.defer(function() {});
			});
			var transformarModelo6_i = Alloy.createCollection('muros_tabiques');
			transformarModelo6_i.fetch();
			var transformarModelo6_src = require('helper').query2array(transformarModelo6_i);
			var datos = [];
			_.each(transformarModelo6_src, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'label';
					if (llave == 'id_segured') newkey = 'valor';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				new_row['estado'] = 0;
				datos.push(new_row);
			});
			$.widgetModalmultiple2.update({
				data: datos
			});
		}
	};
	var ID_1891160530 = setTimeout(ID_1891160530_func, 1000 * 0.2);

}

$.widgetModalmultiple3.init({
	titulo: L('x3327059844_traducir', 'Entrepisos'),
	cargando: L('x1831148736', 'cargando...'),
	__id: 'ALL625792866',
	oncerrar: Cerrar_widgetModalmultiple3,
	left: 0,
	hint: L('x2146928948_traducir', 'Seleccione entrepisos'),
	color: 'morado',
	subtitulo: L('x4011106049_traducir', 'Indique los tipos'),
	right: 0,
	onclick: Click_widgetModalmultiple3,
	onafterinit: Afterinit_widgetModalmultiple3
});

function Click_widgetModalmultiple3(e) {

	var evento = e;

}

function Cerrar_widgetModalmultiple3(e) {

	var evento = e;
	$.widgetModalmultiple3.update({});
	$.recinto1.set({
		ids_entrepisos: evento.valores
	});
	if ('recinto1' in $) $recinto1 = $.recinto1.toJSON();

}

function Afterinit_widgetModalmultiple3(e) {

	var evento = e;
	var ID_1011543706_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			var consultarModelo7_i = Alloy.createCollection('entrepisos');
			var consultarModelo7_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
			consultarModelo7_i.fetch({
				query: 'SELECT * FROM entrepisos WHERE pais_texto=\'' + pais[0].nombre + '\''
			});
			var entrepisos = require('helper').query2array(consultarModelo7_i);
			var datos = [];
			_.each(entrepisos, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'label';
					if (llave == 'id_segured') newkey = 'valor';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				new_row['estado'] = 0;
				datos.push(new_row);
			});
			$.widgetModalmultiple3.update({
				data: datos
			});
		} else {
			var eliminarModelo4_i = Alloy.Collections.entrepisos;
			var sql = "DELETE FROM " + eliminarModelo4_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo4_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo4_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo4_m = Alloy.Collections.entrepisos;
				var insertarModelo4_fila = Alloy.createModel('entrepisos', {
					nombre: String.format(L('x2266735154_traducir', 'Entrepiso%1$s'), item.toString()),
					id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
					id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item.toString()),
					pais: 1
				});
				insertarModelo4_m.add(insertarModelo4_fila);
				insertarModelo4_fila.save();
				_.defer(function() {});
			});
			var transformarModelo8_i = Alloy.createCollection('entrepisos');
			transformarModelo8_i.fetch();
			var transformarModelo8_src = require('helper').query2array(transformarModelo8_i);
			var datos = [];
			_.each(transformarModelo8_src, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'label';
					if (llave == 'id_segured') newkey = 'valor';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				new_row['estado'] = 0;
				datos.push(new_row);
			});
			$.widgetModalmultiple3.update({
				data: datos
			});
		}
	};
	var ID_1011543706 = setTimeout(ID_1011543706_func, 1000 * 0.2);

}

$.widgetModalmultiple4.init({
	titulo: L('x591862035_traducir', 'Pavimentos'),
	cargando: L('x1831148736', 'cargando...'),
	__id: 'ALL1586767745',
	oncerrar: Cerrar_widgetModalmultiple4,
	left: 0,
	hint: L('x2600368035_traducir', 'Seleccione pavimentos'),
	color: 'morado',
	subtitulo: L('x4011106049_traducir', 'Indique los tipos'),
	right: 0,
	onclick: Click_widgetModalmultiple4,
	onafterinit: Afterinit_widgetModalmultiple4
});

function Click_widgetModalmultiple4(e) {

	var evento = e;

}

function Cerrar_widgetModalmultiple4(e) {

	var evento = e;
	$.widgetModalmultiple4.update({});
	$.recinto1.set({
		ids_pavimentos: evento.valores
	});
	if ('recinto1' in $) $recinto1 = $.recinto1.toJSON();

}

function Afterinit_widgetModalmultiple4(e) {

	var evento = e;
	var ID_1194607943_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			var consultarModelo8_i = Alloy.createCollection('pavimento');
			var consultarModelo8_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
			consultarModelo8_i.fetch({
				query: 'SELECT * FROM pavimento WHERE pais_texto=\'' + pais[0].nombre + '\''
			});
			var pavimentos = require('helper').query2array(consultarModelo8_i);
			var datos = [];
			_.each(pavimentos, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'label';
					if (llave == 'id_segured') newkey = 'valor';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				new_row['estado'] = 0;
				datos.push(new_row);
			});
			$.widgetModalmultiple4.update({
				data: datos
			});
		} else {
			var eliminarModelo5_i = Alloy.Collections.pavimento;
			var sql = "DELETE FROM " + eliminarModelo5_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo5_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo5_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo5_m = Alloy.Collections.pavimento;
				var insertarModelo5_fila = Alloy.createModel('pavimento', {
					nombre: String.format(L('x427067467_traducir', 'Pavimento%1$s'), item.toString()),
					id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
					id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item.toString()),
					pais: 1
				});
				insertarModelo5_m.add(insertarModelo5_fila);
				insertarModelo5_fila.save();
				_.defer(function() {});
			});
			var transformarModelo10_i = Alloy.createCollection('pavimento');
			transformarModelo10_i.fetch();
			var transformarModelo10_src = require('helper').query2array(transformarModelo10_i);
			var datos = [];
			_.each(transformarModelo10_src, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'label';
					if (llave == 'id_segured') newkey = 'valor';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				new_row['estado'] = 0;
				datos.push(new_row);
			});
			$.widgetModalmultiple4.update({
				data: datos
			});
		}
	};
	var ID_1194607943 = setTimeout(ID_1194607943_func, 1000 * 0.2);

}

$.widgetModalmultiple5.init({
	titulo: L('x1866523485_traducir', 'Estruct. cubierta'),
	cargando: L('x1831148736', 'cargando...'),
	__id: 'ALL1781249673',
	oncerrar: Cerrar_widgetModalmultiple5,
	left: 0,
	hint: L('x2460890829_traducir', 'Seleccione e.cubiertas'),
	color: 'morado',
	subtitulo: L('x4011106049_traducir', 'Indique los tipos'),
	right: 0,
	onclick: Click_widgetModalmultiple5,
	onafterinit: Afterinit_widgetModalmultiple5
});

function Click_widgetModalmultiple5(e) {

	var evento = e;

}

function Cerrar_widgetModalmultiple5(e) {

	var evento = e;
	$.widgetModalmultiple5.update({});
	$.recinto1.set({
		ids_estructura_cubiera: evento.valores
	});
	if ('recinto1' in $) $recinto1 = $.recinto1.toJSON();

}

function Afterinit_widgetModalmultiple5(e) {

	var evento = e;
	var ID_1382842841_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			var consultarModelo9_i = Alloy.createCollection('estructura_cubierta');
			var consultarModelo9_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
			consultarModelo9_i.fetch({
				query: 'SELECT * FROM estructura_cubierta WHERE pais_texto=\'' + pais[0].nombre + '\''
			});
			var ecubiertas = require('helper').query2array(consultarModelo9_i);
			var datos = [];
			_.each(ecubiertas, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'label';
					if (llave == 'id_segured') newkey = 'valor';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				new_row['estado'] = 0;
				datos.push(new_row);
			});
			$.widgetModalmultiple5.update({
				data: datos
			});
		} else {
			var eliminarModelo6_i = Alloy.Collections.estructura_cubierta;
			var sql = "DELETE FROM " + eliminarModelo6_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo6_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo6_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo6_m = Alloy.Collections.estructura_cubierta;
				var insertarModelo6_fila = Alloy.createModel('estructura_cubierta', {
					nombre: String.format(L('x1686539481_traducir', 'Estru Cubierta %1$s'), item.toString()),
					id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
					id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item.toString()),
					pais: 1
				});
				insertarModelo6_m.add(insertarModelo6_fila);
				insertarModelo6_fila.save();
				_.defer(function() {});
			});
			var transformarModelo12_i = Alloy.createCollection('estructura_cubierta');
			transformarModelo12_i.fetch();
			var transformarModelo12_src = require('helper').query2array(transformarModelo12_i);
			var datos = [];
			_.each(transformarModelo12_src, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'label';
					if (llave == 'id_segured') newkey = 'valor';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				new_row['estado'] = 0;
				datos.push(new_row);
			});
			$.widgetModalmultiple5.update({
				data: datos
			});
		}
	};
	var ID_1382842841 = setTimeout(ID_1382842841_func, 1000 * 0.2);

}

$.widgetModalmultiple6.init({
	titulo: L('x2266302645_traducir', 'Cubierta'),
	cargando: L('x1831148736', 'cargando...'),
	__id: 'ALL1240971093',
	oncerrar: Cerrar_widgetModalmultiple6,
	left: 0,
	hint: L('x2134385782_traducir', 'Seleccione cubiertas'),
	color: 'morado',
	subtitulo: L('x4011106049_traducir', 'Indique los tipos'),
	right: 0,
	onclick: Click_widgetModalmultiple6,
	onafterinit: Afterinit_widgetModalmultiple6
});

function Click_widgetModalmultiple6(e) {

	var evento = e;

}

function Cerrar_widgetModalmultiple6(e) {

	var evento = e;
	$.widgetModalmultiple6.update({});
	$.recinto1.set({
		ids_cubierta: evento.valores
	});
	if ('recinto1' in $) $recinto1 = $.recinto1.toJSON();

}

function Afterinit_widgetModalmultiple6(e) {

	var evento = e;
	var ID_752822451_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			var consultarModelo10_i = Alloy.createCollection('cubierta');
			var consultarModelo10_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
			consultarModelo10_i.fetch({
				query: 'SELECT * FROM cubierta WHERE pais_texto=\'' + pais[0].nombre + '\''
			});
			var cubiertas = require('helper').query2array(consultarModelo10_i);
			var datos = [];
			_.each(cubiertas, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'label';
					if (llave == 'id_segured') newkey = 'valor';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				new_row['estado'] = 0;
				datos.push(new_row);
			});
			$.widgetModalmultiple6.update({
				data: datos
			});
		} else {
			var eliminarModelo7_i = Alloy.Collections.cubierta;
			var sql = "DELETE FROM " + eliminarModelo7_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo7_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo7_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo7_m = Alloy.Collections.cubierta;
				var insertarModelo7_fila = Alloy.createModel('cubierta', {
					nombre: String.format(L('x2246230604_traducir', 'Cubierta %1$s'), item.toString()),
					id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
					id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item.toString()),
					pais: 1
				});
				insertarModelo7_m.add(insertarModelo7_fila);
				insertarModelo7_fila.save();
				_.defer(function() {});
			});
			var transformarModelo14_i = Alloy.createCollection('cubierta');
			transformarModelo14_i.fetch();
			var transformarModelo14_src = require('helper').query2array(transformarModelo14_i);
			var datos = [];
			_.each(transformarModelo14_src, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'label';
					if (llave == 'id_segured') newkey = 'valor';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				new_row['estado'] = 0;
				datos.push(new_row);
			});
			$.widgetModalmultiple6.update({
				data: datos
			});
		}
	};
	var ID_752822451 = setTimeout(ID_752822451_func, 1000 * 0.2);

}

function Click_vista22(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	/** 
	 * Ocupamos esta variable para impedir que la pantalla siguente se cargue dos veces 
	 */
	var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
	if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
		require('vars')['var_abriendo'] = L('x4261170317', 'true');
		Alloy.createController("nuevodano_index", {}).getView().open();
	}

}

function Swipe_vista23(e) {

	e.cancelBubble = true;
	var elemento = e.section.getItemAt(e.itemIndex);
	var findVariables = require('fvariables');
	var fila = {};
	_.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
		_.each(obj_id, function(valor, prop) {
			fila[prop] = elemento[id_field][prop];
			var llaves = findVariables(valor, '{', '}');
			_.each(llaves, function(llave) {
				fila[llave] = elemento[id_field][prop];
			});
		});
	});
	if (Ti.App.deployType != 'production') console.log('en normal', {
		"e": e
	});
	if (e.direction == L('x2053629800_traducir', 'left')) {
		var findVariables = require('fvariables');
		elemento.template = 'pborrar';
		_.each(_list_templates['pborrar'], function(obj_id, id_field) {
			_.each(obj_id, function(valor, prop) {
				var llaves = findVariables(valor, '{', '}');
				_.each(llaves, function(llave) {
					elemento[id_field] = {};
					elemento[id_field][prop] = fila[llave];
				});
			});
		});
		if (OS_IOS) {
			e.section.updateItemAt(e.itemIndex, elemento, {
				animated: true
			});
		} else if (OS_ANDROID) {
			e.section.updateItemAt(e.itemIndex, elemento);
		}
	}

}

function Click_vista23(e) {

	e.cancelBubble = true;
	var elemento = e.section.getItemAt(e.itemIndex);
	var findVariables = require('fvariables');
	var fila = {};
	_.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
		_.each(obj_id, function(valor, prop) {
			fila[prop] = elemento[id_field][prop];
			var llaves = findVariables(valor, '{', '}');
			_.each(llaves, function(llave) {
				fila[llave] = elemento[id_field][prop];
			});
		});
	});
	/** 
	 * Enviamos el parametro _dato para indicar cual sera el id del dano a editar 
	 */
	Alloy.createController("editardano_index", {
		'_dato': fila
	}).getView().open();

}

function Click_vista26(e) {

	e.cancelBubble = true;
	var elemento = e.section.getItemAt(e.itemIndex);
	var findVariables = require('fvariables');
	var fila = {};
	_.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
		_.each(obj_id, function(valor, prop) {
			fila[prop] = elemento[id_field][prop];
			var llaves = findVariables(valor, '{', '}');
			_.each(llaves, function(llave) {
				fila[llave] = elemento[id_field][prop];
			});
		});
	});
	if (Ti.App.deployType != 'production') console.log('click en borrar', {
		"e": e
	});
	var preguntarAlerta13_opts = [L('x3827418516_traducir', 'Si'), L('x1962639792_traducir', ' No')];
	var preguntarAlerta13 = Ti.UI.createAlertDialog({
		title: L('x4097537701_traducir', 'ALERTA'),
		message: '' + String.format(L('x4127782287_traducir', '¿ Seguro desea eliminar: %1$s ?'), fila.nombre.toString()) + '',
		buttonNames: preguntarAlerta13_opts
	});
	preguntarAlerta13.addEventListener('click', function(e) {
		var xd = preguntarAlerta13_opts[e.index];
		if (xd == L('x3827418516_traducir', 'Si')) {
			if (Ti.App.deployType != 'production') console.log('borrando xd', {});
			var eliminarModelo8_i = Alloy.Collections.insp_itemdanos;
			var sql = 'DELETE FROM ' + eliminarModelo8_i.config.adapter.collection_name + ' WHERE id=\'' + fila.id + '\'';
			var db = Ti.Database.open(eliminarModelo8_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo8_i.trigger('remove');
			_.defer(function() {
				Alloy.Collections.insp_itemdanos.fetch();
			});
		}
		xd = null;
		e.source.removeEventListener("click", arguments.callee);
	});
	preguntarAlerta13.show();

}

function Swipe_vista24(e) {

	e.cancelBubble = true;
	var elemento = e.section.getItemAt(e.itemIndex);
	var findVariables = require('fvariables');
	var fila = {};
	_.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
		_.each(obj_id, function(valor, prop) {
			fila[prop] = elemento[id_field][prop];
			var llaves = findVariables(valor, '{', '}');
			_.each(llaves, function(llave) {
				fila[llave] = elemento[id_field][prop];
			});
		});
	});
	if (Ti.App.deployType != 'production') console.log('en borrar', {
		"e": e
	});
	if (e.direction == L('x3033167124_traducir', 'right')) {
		if (Ti.App.deployType != 'production') console.log('swipe hacia derecha (desde borrar)', {});
		var findVariables = require('fvariables');
		elemento.template = 'dano';
		_.each(_list_templates['dano'], function(obj_id, id_field) {
			_.each(obj_id, function(valor, prop) {
				var llaves = findVariables(valor, '{', '}');
				_.each(llaves, function(llave) {
					elemento[id_field] = {};
					elemento[id_field][prop] = fila[llave];
				});
			});
		});
		if (OS_IOS) {
			e.section.updateItemAt(e.itemIndex, elemento, {
				animated: true
			});
		} else if (OS_ANDROID) {
			e.section.updateItemAt(e.itemIndex, elemento);
		}
	}

}

function Click_vista24(e) {

	e.cancelBubble = true;
	var elemento = e.section.getItemAt(e.itemIndex);
	var findVariables = require('fvariables');
	var fila = {};
	_.each(_list_templates[e.section.items[e.itemIndex].template], function(obj_id, id_field) {
		_.each(obj_id, function(valor, prop) {
			fila[prop] = elemento[id_field][prop];
			var llaves = findVariables(valor, '{', '}');
			_.each(llaves, function(llave) {
				fila[llave] = elemento[id_field][prop];
			});
		});
	});
	if (Ti.App.deployType != 'production') console.log('en borrar', {
		"e": e
	});
	var findVariables = require('fvariables');
	elemento.template = 'dano';
	_.each(_list_templates['dano'], function(obj_id, id_field) {
		_.each(obj_id, function(valor, prop) {
			var llaves = findVariables(valor, '{', '}');
			_.each(llaves, function(llave) {
				elemento[id_field] = {};
				elemento[id_field][prop] = fila[llave];
			});
		});
	});
	if (OS_IOS) {
		e.section.updateItemAt(e.itemIndex, elemento, {
			animated: true
		});
	} else if (OS_ANDROID) {
		e.section.updateItemAt(e.itemIndex, elemento);
	}

}

$.widgetCamaralight.init({
	onfotolista: Fotolista_widgetCamaralight,
	__id: 'ALL523196316'
});

function Fotolista_widgetCamaralight(e) {

	var evento = e;
	/** 
	 * Recuperamos cual fue la foto seleccionada 
	 */
	var cual_foto = ('cual_foto' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['cual_foto'] : '';
	if (cual_foto == 1 || cual_foto == '1') {
		var insertarModelo8_m = Alloy.Collections.numero_unico;
		var insertarModelo8_fila = Alloy.createModel('numero_unico', {
			comentario: 'nuevo recinto foto1'
		});
		insertarModelo8_m.add(insertarModelo8_fila);
		insertarModelo8_fila.save();
		var nuevoid_f1 = require('helper').model2object(insertarModelo8_m.last());
		_.defer(function() {});
		/** 
		 * Guardamos en variable el detalle del modelo numero_unico para asignar informacion de foto1 del recinto 
		 */
		require('vars')[_var_scopekey]['nuevoid_f1'] = nuevoid_f1;
		/** 
		 * Recuperamos variable para saber si estamos haciendo una inspeccion o dummy y guardar la foto en la memoria del celular 
		 */
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var ID_697735347_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
			if (ID_697735347_d.exists() == false) ID_697735347_d.createDirectory();
			var ID_697735347_f = Ti.Filesystem.getFile(ID_697735347_d.resolve(), 'cap' + nuevoid_f1.id + '.jpg');
			if (ID_697735347_f.exists() == true) ID_697735347_f.deleteFile();
			ID_697735347_f.write(evento.foto);
			ID_697735347_d = null;
			ID_697735347_f = null;
		} else {
			var ID_1281614579_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + 0);
			if (ID_1281614579_d.exists() == false) ID_1281614579_d.createDirectory();
			var ID_1281614579_f = Ti.Filesystem.getFile(ID_1281614579_d.resolve(), 'cap' + nuevoid_f1.id + '.jpg');
			if (ID_1281614579_f.exists() == true) ID_1281614579_f.deleteFile();
			ID_1281614579_f.write(evento.foto);
			ID_1281614579_d = null;
			ID_1281614579_f = null;
		}
		/** 
		 * Actualizamos el modelo indicando cual es el nombre de la imagen 
		 */
		$.recinto1.set({
			foto1: String.format(L('x1070475521', 'cap%1$s.jpg'), nuevoid_f1.id.toString())
		});
		if ('recinto1' in $) $recinto1 = $.recinto1.toJSON();
		/** 
		 * Enviamos la foto para que genere la miniatura (y tambien la guarde) y muestre en pantalla 
		 */
		$.widgetFotochica.procesar({
			imagen: evento.foto,
			nueva: 640,
			calidad: 91,
			camara: 'trasera'
		});
		/** 
		 * Limpiamos memoria ram 
		 */
		evento = null;
	} else if (cual_foto == 2) {
		var insertarModelo9_m = Alloy.Collections.numero_unico;
		var insertarModelo9_fila = Alloy.createModel('numero_unico', {
			comentario: 'nuevo recinto foto2'
		});
		insertarModelo9_m.add(insertarModelo9_fila);
		insertarModelo9_fila.save();
		var nuevoid_f2 = require('helper').model2object(insertarModelo9_m.last());
		_.defer(function() {});
		require('vars')[_var_scopekey]['nuevoid_f2'] = nuevoid_f2;
		if (Ti.App.deployType != 'production') console.log(String.format(L('x2429469022', 'imagen capturada y asignada como cap%1$s.jpg'), nuevoid_f2.id.toString()), {});
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var ID_1131692786_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
			if (ID_1131692786_d.exists() == false) ID_1131692786_d.createDirectory();
			var ID_1131692786_f = Ti.Filesystem.getFile(ID_1131692786_d.resolve(), 'cap' + nuevoid_f2.id + '.jpg');
			if (ID_1131692786_f.exists() == true) ID_1131692786_f.deleteFile();
			ID_1131692786_f.write(evento.foto);
			ID_1131692786_d = null;
			ID_1131692786_f = null;
		} else {
			var ID_351204569_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + 0);
			if (ID_351204569_d.exists() == false) ID_351204569_d.createDirectory();
			var ID_351204569_f = Ti.Filesystem.getFile(ID_351204569_d.resolve(), 'cap' + nuevoid_f2.id + '.jpg');
			if (ID_351204569_f.exists() == true) ID_351204569_f.deleteFile();
			ID_351204569_f.write(evento.foto);
			ID_351204569_d = null;
			ID_351204569_f = null;
		}
		$.recinto1.set({
			foto2: String.format(L('x1070475521', 'cap%1$s.jpg'), nuevoid_f2.id.toString())
		});
		if ('recinto1' in $) $recinto1 = $.recinto1.toJSON();
		$.widgetFotochica2.procesar({
			imagen: evento.foto,
			nueva: 640,
			calidad: 91,
			camara: 'trasera'
		});
		evento = null;
	} else if (cual_foto == 3) {
		var insertarModelo10_m = Alloy.Collections.numero_unico;
		var insertarModelo10_fila = Alloy.createModel('numero_unico', {
			comentario: 'nuevo recinto foto3'
		});
		insertarModelo10_m.add(insertarModelo10_fila);
		insertarModelo10_fila.save();
		var nuevoid_f3 = require('helper').model2object(insertarModelo10_m.last());
		_.defer(function() {});
		require('vars')[_var_scopekey]['nuevoid_f3'] = nuevoid_f3;
		if (Ti.App.deployType != 'production') console.log(String.format(L('x2429469022', 'imagen capturada y asignada como cap%1$s.jpg'), nuevoid_f3.id.toString()), {});
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var ID_1879436908_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
			if (ID_1879436908_d.exists() == false) ID_1879436908_d.createDirectory();
			var ID_1879436908_f = Ti.Filesystem.getFile(ID_1879436908_d.resolve(), 'cap' + nuevoid_f3.id + '.jpg');
			if (ID_1879436908_f.exists() == true) ID_1879436908_f.deleteFile();
			ID_1879436908_f.write(evento.foto);
			ID_1879436908_d = null;
			ID_1879436908_f = null;
		} else {
			var ID_1049893814_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + 0);
			if (ID_1049893814_d.exists() == false) ID_1049893814_d.createDirectory();
			var ID_1049893814_f = Ti.Filesystem.getFile(ID_1049893814_d.resolve(), 'cap' + nuevoid_f3.id + '.jpg');
			if (ID_1049893814_f.exists() == true) ID_1049893814_f.deleteFile();
			ID_1049893814_f.write(evento.foto);
			ID_1049893814_d = null;
			ID_1049893814_f = null;
		}
		$.recinto1.set({
			foto3: String.format(L('x1070475521', 'cap%1$s.jpg'), nuevoid_f3.id.toString())
		});
		if ('recinto1' in $) $recinto1 = $.recinto1.toJSON();
		$.widgetFotochica3.procesar({
			imagen: evento.foto,
			nueva: 640,
			calidad: 91,
			camara: 'trasera'
		});
		evento = null;
	}
}

(function() {
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
		/** 
		 * Modificamos la idinspeccion con el idserver de la tarea 
		 */
		$.recinto1.set({
			id_inspeccion: seltarea.id_server
		});
		if ('recinto1' in $) $recinto1 = $.recinto1.toJSON();
	}
	var insertarModelo11_m = Alloy.Collections.numero_unico;
	var insertarModelo11_fila = Alloy.createModel('numero_unico', {
		comentario: 'nuevo recinto id'
	});
	insertarModelo11_m.add(insertarModelo11_fila);
	insertarModelo11_fila.save();
	var nuevoid = require('helper').model2object(insertarModelo11_m.last());
	_.defer(function() {});
	/** 
	 * guardamos variable pq aun no existe este recinto al agregar da&#241;os 
	 */
	require('vars')['temp_idrecinto'] = nuevoid.id;
	/** 
	 * Agregamos un id al recinto con el nuevo id que se asigna 
	 */
	$.recinto1.set({
		id_recinto: nuevoid.id
	});
	if ('recinto1' in $) $recinto1 = $.recinto1.toJSON();
	/** 
	 * filtramos items de dano, para el id_recinto recien creado. 
	 */
	consultarModelo_filter = function(coll) {
		var filtered = coll.filter(function(m) {
			var _tests = [],
				_all_true = false,
				model = m.toJSON();
			_tests.push((model.id_recinto == nuevoid.id));
			var _all_true_s = _.uniq(_tests);
			_all_true = (_all_true_s.length == 1 && _all_true_s[0] == true) ? true : false;
			return _all_true;
		});
		filtered = _.toArray(filtered);
		return filtered;
	};
	_.defer(function() {
		Alloy.Collections.insp_itemdanos.fetch();
	});
	/** 
	 * Modificamos en 0.1 segundos el color del statusbar 
	 */
	var ID_1272213430_func = function() {
		var nuevorecinto_statusbar = '#7E6EE0';

		var setearStatusColor = function(nuevorecinto_statusbar) {
			if (OS_IOS) {
				if (nuevorecinto_statusbar == 'light' || nuevorecinto_statusbar == 'claro') {
					$.nuevorecinto_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.LIGHT_CONTENT);
				} else if (nuevorecinto_statusbar == 'grey' || nuevorecinto_statusbar == 'gris' || nuevorecinto_statusbar == 'gray') {
					$.nuevorecinto_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.GREY);
				} else if (nuevorecinto_statusbar == 'oscuro' || nuevorecinto_statusbar == 'dark') {
					$.nuevorecinto_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.DEFAULT);
				}
			} else if (OS_ANDROID) {
				abx.setStatusbarColor(nuevorecinto_statusbar);
			}
		};
		setearStatusColor(nuevorecinto_statusbar);

	};
	var ID_1272213430 = setTimeout(ID_1272213430_func, 1000 * 0.1);
})();

function Postlayout_nuevorecinto(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var ID_1294585966_func = function() {
		/** 
		 * Una vez ya cargada la tarea, dejamos la variable limpia para poder abrir otras pantallas 
		 */
		require('vars')['var_abriendo'] = '';
	};
	var ID_1294585966 = setTimeout(ID_1294585966_func, 1000 * 0.2);

}
if (OS_IOS || OS_ANDROID) {
	$.nuevorecinto.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}
//$.nuevorecinto.open();
