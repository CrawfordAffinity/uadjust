var _bind4section = {
	"ref1": "bienes"
};
var _list_templates = {
	"elemento": {
		"vista64": {},
		"Label5": {
			"text": "{id_segured}"
		},
		"Label6": {
			"text": "{nombre}"
		},
		"vista63": {}
	}
};

var _activity;
if (OS_ANDROID) {
	_activity = $.listadocontenidos.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'listadocontenidos';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.listadocontenidos.addEventListener('open', function(e) {});
}
$.listadocontenidos.orientationModes = [Titanium.UI.PORTRAIT];


$.widgetBarra4.init({
	titulo: L('x782552716_traducir', 'SELECCIONE'),
	onsalirinsp: Salirinsp_widgetBarra4,
	__id: 'ALL1158163419',
	fondo: 'fondoblanco',
	top: 0,
	colortitulo: 'negro',
	modal: L('', ''),
	onpresiono: Presiono_widgetBarra4
});

function Salirinsp_widgetBarra4(e) {

	var evento = e;
	/** 
	 * Cierra la pantalla tipo_modal 
	 */
	$.listadocontenidos.close();

}

function Presiono_widgetBarra4(e) {

	var evento = e;

}

var consultarModelo16_like = function(search) {
	if (typeof search !== 'string' || this === null) {
		return false;
	}
	search = search.replace(new RegExp("([\\.\\\\\\+\\*\\?\\[\\^\\]\\$\\(\\)\\{\\}\\=\\!\\<\\>\\|\\:\\-])", "g"), "\\$1");
	search = search.replace(/%/g, '.*').replace(/_/g, '.');
	return RegExp('^' + search + '$', 'gi').test(this);
};
var consultarModelo16_filter = function(coll) {
	var filtered = _.toArray(coll.filter(function(m) {
		return true;
	}));
	var ordered = _.sortBy(filtered, 'nombre');
	return ordered;
};
var consultarModelo16_transform = function(model) {
	var modelo = model.toJSON();
	return modelo;
};
var consultarModelo16_update = function(e) {};
_.defer(function() {
	Alloy.Collections.bienes.fetch();
});
Alloy.Collections.bienes.on('add change delete', function(ee) {
	consultarModelo16_update(ee);
});
consultarModelo16_filter = function(coll) {
	var filtered = _.toArray(coll.filter(function(filax) {
		var fila = filax.toJSON();
		var test = true;
		fila = null;
		return test;
	}));
	return filtered;
};
Alloy.Collections.bienes.fetch();

function Itemclick_listado5(e) {

	e.cancelBubble = true;
	var objeto = e.section.getItemAt(e.itemIndex);
	var modelo = {},
		_modelo = [];
	var fila = {},
		fila_bak = {},
		info = {
			_template: objeto.template,
			_what: [],
			_seccion_ref: e.section.getHeaderTitle(),
			_model_id: -1
		},
		_tmp = {
			objmap: {}
		};
	if ('itemId' in e) {
		info._model_id = e.itemId;
		modelo._id = info._model_id;
		if (info._seccion_ref != '' && info._seccion_ref in _bind4section) {
			modelo._collection = _bind4section[info._seccion_ref];
			_tmp._coll = modelo._collection;
		}
	}
	var findVariables = require('fvariables');
	_.each(_list_templates[info._template], function(obj_id, id) {
		_.each(obj_id, function(valor, prop) {
			var llaves = findVariables(valor, '{', '}');
			_.each(llaves, function(llave) {
				_tmp.objmap[llave] = {
					id: id,
					prop: prop
				};
				fila[llave] = objeto[id][prop];
				if (id == e.bindId) info._what.push(llave);
			});
		});
	});
	info._what = info._what.join(',');
	fila_bak = JSON.parse(JSON.stringify(fila));
	Alloy.Events.trigger('resp_dato1', {
		id_segured: fila.id_segured,
		nombre: fila.nombre
	});
	$.listadocontenidos.close();
	_tmp.changed = false;
	_tmp.diff_keys = [];
	_.each(fila, function(value1, prop) {
		var had_samekey = false;
		_.each(fila_bak, function(value2, prop2) {
			if (prop == prop2 && value1 == value2) {
				had_samekey = true;
			} else if (!_.has(fila_bak, prop) || !_.has(fila, prop2)) {
				has_samekey = true;
			}
		});
		if (!had_samekey) _tmp.diff_keys.push(prop);
	});
	if (_tmp.diff_keys.length > 0) _tmp.changed = true;
	if (_tmp.changed == true) {
		_.each(_tmp.diff_keys, function(llave) {
			objeto[_tmp.objmap[llave].id][_tmp.objmap[llave].prop] = fila[llave];
		});
		e.section.updateItemAt(e.itemIndex, objeto);
	}

}
/** 
 * Recuperamos variables para editar y cargar cosas de la pantalla 
 */
(function() {
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
		var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
	} else {
		/** 
		 * Insertamos dummies 
		 */
		/** 
		 * Insertamos dummies 
		 */
		var eliminarModelo10_i = Alloy.Collections.bienes;
		var sql = "DELETE FROM " + eliminarModelo10_i.config.adapter.collection_name;
		var db = Ti.Database.open(eliminarModelo10_i.config.adapter.db_name);
		db.execute(sql);
		db.close();
		eliminarModelo10_i.trigger('remove');
		var item_index = 0;
		_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
			item_index += 1;
			var insertarModelo14_m = Alloy.Collections.bienes;
			var insertarModelo14_fila = Alloy.createModel('bienes', {
				nombre: String.format(L('x3389261753_traducir', 'Lampara%1$s'), item.toString()),
				id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
				id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item.toString()),
				pais: 1
			});
			insertarModelo14_m.add(insertarModelo14_fila);
			insertarModelo14_fila.save();
			_.defer(function() {
				Alloy.Collections.bienes.fetch();
			});
		});
	}
})();

function Postlayout_listadocontenidos(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var ID_1465734174_func = function() {
		/** 
		 * Una vez ya cargada la tarea, dejamos la variable limpia para poder abrir otras pantallas 
		 */
		require('vars')['var_abriendo'] = '';
	};
	var ID_1465734174 = setTimeout(ID_1465734174_func, 1000 * 0.2);

}
if (OS_IOS || OS_ANDROID) {
	$.listadocontenidos.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}