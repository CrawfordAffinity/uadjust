var _bind4section = {};
var _list_templates = {};

var _activity;
if (OS_ANDROID) {
	_activity = $.Editar_PartTime.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'Editar_PartTime';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.Editar_PartTime.addEventListener('open', function(e) {});
}
$.Editar_PartTime.orientationModes = [Titanium.UI.PORTRAIT];


$.widgetBarra2.init({
	titulo: L('x2688733458_traducir', 'DISPONIBILIDAD'),
	__id: 'ALL1333942819',
	textoderecha: L('x2943883035_traducir', 'Guardar'),
	fondo: 'fondoblanco',
	colortitulo: 'negro',
	modal: L('', ''),
	onpresiono: Presiono_widgetBarra2,
	colortextoderecha: 'azul'
});

function Presiono_widgetBarra2(e) {

	var evento = e;
	/** 
	 * recuperamos variables de las horas y dias 
	 */
	var desde = ('desde' in require('vars')) ? require('vars')['desde'] : '';
	var hasta = ('hasta' in require('vars')) ? require('vars')['hasta'] : '';
	var dias = ('dias' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['dias'] : '';
	/** 
	 * Creamos una variable para validar que al menos un dia este seleccionado 
	 */

	//validaciones
	var al_menos_uno = dias.d1 || dias.d2 || dias.d3 || dias.d4 || dias.d5 || dias.d6 || dias.d7;
	desde = parseInt(desde);
	hasta = parseInt(hasta)
	if (al_menos_uno == false || al_menos_uno == 'false') {
		/** 
		 * Mostramos mensajes en caso de que exista un problema, ejemplo, que no hayan dias seleccionados o que las horas no tengan sentido 
		 */
		var preguntarAlerta3_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta3 = Ti.UI.createAlertDialog({
			title: L('x57652245_traducir', 'Alerta'),
			message: L('x2430147855_traducir', 'Seleccione al menos un día'),
			buttonNames: preguntarAlerta3_opts
		});
		preguntarAlerta3.addEventListener('click', function(e) {
			var suu = preguntarAlerta3_opts[e.index];
			suu = null;

			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta3.show();
	} else if (_.isNumber(desde) && _.isNumber(hasta) && desde > hasta) {
		var preguntarAlerta4_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta4 = Ti.UI.createAlertDialog({
			title: L('x57652245_traducir', 'Alerta'),
			message: L('x4068516320_traducir', 'El rango de horas está mal ingresado'),
			buttonNames: preguntarAlerta4_opts
		});
		preguntarAlerta4.addEventListener('click', function(e) {
			var suu = preguntarAlerta4_opts[e.index];
			suu = null;

			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta4.show();
	} else if (desde == hasta) {
		var preguntarAlerta5_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta5 = Ti.UI.createAlertDialog({
			title: L('x57652245_traducir', 'Alerta'),
			message: L('x178692076_traducir', 'El rango debe ser superior a una hora'),
			buttonNames: preguntarAlerta5_opts
		});
		preguntarAlerta5.addEventListener('click', function(e) {
			var suu = preguntarAlerta5_opts[e.index];
			suu = null;

			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta5.show();
	} else {
		if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
			/** 
			 * Recuperamos variables de la url de uadjust, fuerapais, fueraciudad desde la pantalla anterior 
			 */
			var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
			var fuerapais = ('fuerapais' in require('vars')) ? require('vars')['fuerapais'] : '';
			var fueraciudad = ('fueraciudad' in require('vars')) ? require('vars')['fueraciudad'] : '';
			var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
			/** 
			 * Creamos una variable para poder mandar el dato completo de las horas disponibles al servidor 
			 */
			var disp_horas = desde + ':00 ' + hasta + ':00';
			if (Ti.App.deployType != 'production') console.log('los datos parttime', {
				"ciudad": fueraciudad,
				"diass": dias,
				"horas": disp_horas,
				"idinsp": inspector.id_server,
				"pais": fuerapais
			});
			var consultarURL2 = {};
			console.log('DEBUG WEB: requesting url:' + String.format(L('x2304081372', '%1$seditarPerfilTipo2'), url_server.toString()) + ' with data:', {
				_method: 'POST',
				_params: {
					id_inspector: inspector.id_server,
					disponibilidad_viajar_pais: fuerapais,
					disponibilidad_viajar_ciudad: fueraciudad,
					disponibilidad_horas: disp_horas,
					disponibilidad: 0,
					d1: dias.d1,
					d2: dias.d2,
					d3: dias.d3,
					d4: dias.d4,
					d5: dias.d5,
					d6: dias.d6,
					d7: dias.d7
				},
				_timeout: '15000'
			});

			consultarURL2.success = function(e) {
				var elemento = e,
					valor = e;
				if (Ti.App.deployType != 'production') console.log('no se cae la web', {
					"detalle": elemento
				});
				if (elemento.error == 0 || elemento.error == '0') {
					/** 
					 * Modificamos la tabla con los datos nuevos 
					 */
					var consultarModelo3_i = Alloy.createCollection('inspectores');
					var consultarModelo3_i_where = '';
					consultarModelo3_i.fetch();
					var inspector_list = require('helper').query2array(consultarModelo3_i);
					var fueraciudad = ('fueraciudad' in require('vars')) ? require('vars')['fueraciudad'] : '';
					var fuerapais = ('fuerapais' in require('vars')) ? require('vars')['fuerapais'] : '';
					var db = Ti.Database.open(consultarModelo3_i.config.adapter.db_name);
					if (consultarModelo3_i_where == '') {
						var sql = 'UPDATE ' + consultarModelo3_i.config.adapter.collection_name + ' SET disponibilidad_viajar_pais=\'' + fuerapais + '\'';
					} else {
						var sql = 'UPDATE ' + consultarModelo3_i.config.adapter.collection_name + ' SET disponibilidad_viajar_pais=\'' + fuerapais + '\' WHERE ' + consultarModelo3_i_where;
					}
					db.execute(sql);
					if (consultarModelo3_i_where == '') {
						var sql = 'UPDATE ' + consultarModelo3_i.config.adapter.collection_name + ' SET disponibilidad_viajar_ciudad=\'' + fueraciudad + '\'';
					} else {
						var sql = 'UPDATE ' + consultarModelo3_i.config.adapter.collection_name + ' SET disponibilidad_viajar_ciudad=\'' + fueraciudad + '\' WHERE ' + consultarModelo3_i_where;
					}
					db.execute(sql);
					if (consultarModelo3_i_where == '') {
						var sql = 'UPDATE ' + consultarModelo3_i.config.adapter.collection_name + ' SET disponibilidad=\'' + 0 + '\'';
					} else {
						var sql = 'UPDATE ' + consultarModelo3_i.config.adapter.collection_name + ' SET disponibilidad=\'' + 0 + '\' WHERE ' + consultarModelo3_i_where;
					}
					db.execute(sql);
					if (consultarModelo3_i_where == '') {
						var sql = 'UPDATE ' + consultarModelo3_i.config.adapter.collection_name + ' SET disponibilidad_horas=\'' + disp_horas + '\'';
					} else {
						var sql = 'UPDATE ' + consultarModelo3_i.config.adapter.collection_name + ' SET disponibilidad_horas=\'' + disp_horas + '\' WHERE ' + consultarModelo3_i_where;
					}
					db.execute(sql);
					if (consultarModelo3_i_where == '') {
						var sql = 'UPDATE ' + consultarModelo3_i.config.adapter.collection_name + ' SET d1=\'' + dias.d1 + '\'';
					} else {
						var sql = 'UPDATE ' + consultarModelo3_i.config.adapter.collection_name + ' SET d1=\'' + dias.d1 + '\' WHERE ' + consultarModelo3_i_where;
					}
					db.execute(sql);
					if (consultarModelo3_i_where == '') {
						var sql = 'UPDATE ' + consultarModelo3_i.config.adapter.collection_name + ' SET d2=\'' + dias.d2 + '\'';
					} else {
						var sql = 'UPDATE ' + consultarModelo3_i.config.adapter.collection_name + ' SET d2=\'' + dias.d2 + '\' WHERE ' + consultarModelo3_i_where;
					}
					db.execute(sql);
					if (consultarModelo3_i_where == '') {
						var sql = 'UPDATE ' + consultarModelo3_i.config.adapter.collection_name + ' SET d3=\'' + dias.d3 + '\'';
					} else {
						var sql = 'UPDATE ' + consultarModelo3_i.config.adapter.collection_name + ' SET d3=\'' + dias.d3 + '\' WHERE ' + consultarModelo3_i_where;
					}
					db.execute(sql);
					if (consultarModelo3_i_where == '') {
						var sql = 'UPDATE ' + consultarModelo3_i.config.adapter.collection_name + ' SET d4=\'' + dias.d4 + '\'';
					} else {
						var sql = 'UPDATE ' + consultarModelo3_i.config.adapter.collection_name + ' SET d4=\'' + dias.d4 + '\' WHERE ' + consultarModelo3_i_where;
					}
					db.execute(sql);
					if (consultarModelo3_i_where == '') {
						var sql = 'UPDATE ' + consultarModelo3_i.config.adapter.collection_name + ' SET d5=\'' + dias.d5 + '\'';
					} else {
						var sql = 'UPDATE ' + consultarModelo3_i.config.adapter.collection_name + ' SET d5=\'' + dias.d5 + '\' WHERE ' + consultarModelo3_i_where;
					}
					db.execute(sql);
					if (consultarModelo3_i_where == '') {
						var sql = 'UPDATE ' + consultarModelo3_i.config.adapter.collection_name + ' SET d6=\'' + dias.d6 + '\'';
					} else {
						var sql = 'UPDATE ' + consultarModelo3_i.config.adapter.collection_name + ' SET d6=\'' + dias.d6 + '\' WHERE ' + consultarModelo3_i_where;
					}
					db.execute(sql);
					if (consultarModelo3_i_where == '') {
						var sql = 'UPDATE ' + consultarModelo3_i.config.adapter.collection_name + ' SET d7=\'' + dias.d7 + '\'';
					} else {
						var sql = 'UPDATE ' + consultarModelo3_i.config.adapter.collection_name + ' SET d7=\'' + dias.d7 + '\' WHERE ' + consultarModelo3_i_where;
					}
					db.execute(sql);
					db.close();
					/** 
					 * Actualizamos la variable del inspector 
					 */
					var consultarModelo4_i = Alloy.createCollection('inspectores');
					var consultarModelo4_i_where = '';
					consultarModelo4_i.fetch();
					var inspector_list = require('helper').query2array(consultarModelo4_i);
					require('vars')['inspector'] = inspector_list[0];
					/** 
					 * Limpiamos memoria 
					 */
					inspector_list = null;
					/** 
					 * Cerramos la pantalla anterior y la actual 
					 */

					Alloy.Events.trigger('_close_editar');
					$.Editar_PartTime.close();
				}
				elemento = null, valor = null;
			};

			consultarURL2.error = function(e) {
				var elemento = e,
					valor = e;
				if (Ti.App.deployType != 'production') console.log('Hubo un error', {
					"elemento": elemento
				});
				elemento = null, valor = null;
			};
			require('helper').ajaxUnico('consultarURL2', '' + String.format(L('x2304081372', '%1$seditarPerfilTipo2'), url_server.toString()) + '', 'POST', {
				id_inspector: inspector.id_server,
				disponibilidad_viajar_pais: fuerapais,
				disponibilidad_viajar_ciudad: fueraciudad,
				disponibilidad_horas: disp_horas,
				disponibilidad: 0,
				d1: dias.d1,
				d2: dias.d2,
				d3: dias.d3,
				d4: dias.d4,
				d5: dias.d5,
				d6: dias.d6,
				d7: dias.d7
			}, 15000, consultarURL2);
		} else {
			var preguntarAlerta6_opts = [L('x1518866076_traducir', 'Aceptar')];
			var preguntarAlerta6 = Ti.UI.createAlertDialog({
				title: L('x3071602690_traducir', 'No hay internet'),
				message: L('x2846685350_traducir', 'No se pueden efectuar los cambios sin conexion.'),
				buttonNames: preguntarAlerta6_opts
			});
			preguntarAlerta6.addEventListener('click', function(e) {
				var suu = preguntarAlerta6_opts[e.index];
				suu = null;

				e.source.removeEventListener("click", arguments.callee);
			});
			preguntarAlerta6.show();
		}
	}
}

$.widgetHeader2.init({
	titulo: L('x31606107_traducir', 'Editar Disponibilidad de trabajo'),
	__id: 'ALL1614118096',
	avance: L('', ''),
	onclick: Click_widgetHeader2
});

function Click_widgetHeader2(e) {

	var evento = e;

}

$.widgetPelota.init({
	__id: 'ALL1132173110',
	letra: L('x2909332022', 'L'),
	onon: on_widgetPelota,
	onoff: Off_widgetPelota
});

function on_widgetPelota(e) {
	/** 
	 * Recuperamos la variable de los dias, actualizamos la estructura con el dato fijado por el inspector y guardamos la variable 
	 */

	var evento = e;
	var dias = ('dias' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['dias'] : '';
	var dias = _.extend(dias, {
		d1: 1
	});
	require('vars')[_var_scopekey]['dias'] = dias;

}

function Off_widgetPelota(e) {

	var evento = e;
	var dias = ('dias' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['dias'] : '';
	var dias = _.extend(dias, {
		d1: 0
	});
	require('vars')[_var_scopekey]['dias'] = dias;

}

$.widgetPelota2.init({
	__id: 'ALL1396533677',
	letra: L('x3664761504', 'M'),
	onon: on_widgetPelota2,
	onoff: Off_widgetPelota2
});

function on_widgetPelota2(e) {
	/** 
	 * Recuperamos la variable de los dias, actualizamos la estructura con el dato fijado por el inspector y guardamos la variable 
	 */

	var evento = e;
	var dias = ('dias' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['dias'] : '';
	var dias = _.extend(dias, {
		d2: 1
	});
	require('vars')[_var_scopekey]['dias'] = dias;

}

function Off_widgetPelota2(e) {

	var evento = e;
	var dias = ('dias' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['dias'] : '';
	var dias = _.extend(dias, {
		d2: 0
	});
	require('vars')[_var_scopekey]['dias'] = dias;

}

$.widgetPelota3.init({
	__id: 'ALL364580565',
	letra: L('x185522819_traducir', 'MI'),
	onon: on_widgetPelota3,
	onoff: Off_widgetPelota3
});

function on_widgetPelota3(e) {
	/** 
	 * Recuperamos la variable de los dias, actualizamos la estructura con el dato fijado por el inspector y guardamos la variable 
	 */

	var evento = e;
	var dias = ('dias' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['dias'] : '';
	var dias = _.extend(dias, {
		d3: 1
	});
	require('vars')[_var_scopekey]['dias'] = dias;

}

function Off_widgetPelota3(e) {

	var evento = e;
	var dias = ('dias' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['dias'] : '';
	var dias = _.extend(dias, {
		d3: 0
	});
	require('vars')[_var_scopekey]['dias'] = dias;

}

$.widgetPelota4.init({
	__id: 'ALL180804538',
	letra: L('x1141589763', 'J'),
	onon: on_widgetPelota4,
	onoff: Off_widgetPelota4
});

function on_widgetPelota4(e) {
	/** 
	 * Recuperamos la variable de los dias, actualizamos la estructura con el dato fijado por el inspector y guardamos la variable 
	 */

	var evento = e;
	var dias = ('dias' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['dias'] : '';
	var dias = _.extend(dias, {
		d4: 1
	});
	require('vars')[_var_scopekey]['dias'] = dias;

}

function Off_widgetPelota4(e) {

	var evento = e;
	var dias = ('dias' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['dias'] : '';
	var dias = _.extend(dias, {
		d4: 0
	});
	require('vars')[_var_scopekey]['dias'] = dias;

}

$.widgetPelota5.init({
	__id: 'ALL457319757',
	letra: L('x1342839628', 'V'),
	onon: on_widgetPelota5,
	onoff: Off_widgetPelota5
});

function on_widgetPelota5(e) {
	/** 
	 * Recuperamos la variable de los dias, actualizamos la estructura con el dato fijado por el inspector y guardamos la variable 
	 */

	var evento = e;
	var dias = ('dias' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['dias'] : '';
	var dias = _.extend(dias, {
		d5: 1
	});
	require('vars')[_var_scopekey]['dias'] = dias;

}

function Off_widgetPelota5(e) {

	var evento = e;
	var dias = ('dias' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['dias'] : '';
	var dias = _.extend(dias, {
		d5: 0
	});
	require('vars')[_var_scopekey]['dias'] = dias;

}

$.widgetPelota6.init({
	__id: 'ALL993907842',
	letra: L('x543223747', 'S'),
	onon: on_widgetPelota6,
	onoff: Off_widgetPelota6
});

function on_widgetPelota6(e) {
	/** 
	 * Recuperamos la variable de los dias, actualizamos la estructura con el dato fijado por el inspector y guardamos la variable 
	 */

	var evento = e;
	var dias = ('dias' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['dias'] : '';
	var dias = _.extend(dias, {
		d6: 1
	});
	require('vars')[_var_scopekey]['dias'] = dias;

}

function Off_widgetPelota6(e) {

	var evento = e;
	var dias = ('dias' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['dias'] : '';
	var dias = _.extend(dias, {
		d6: 0
	});
	require('vars')[_var_scopekey]['dias'] = dias;

}

$.widgetPelota7.init({
	__id: 'ALL947148270',
	letra: L('x2746444292', 'D'),
	onon: on_widgetPelota7,
	onoff: Off_widgetPelota7
});

function on_widgetPelota7(e) {
	/** 
	 * Recuperamos la variable de los dias, actualizamos la estructura con el dato fijado por el inspector y guardamos la variable 
	 */

	var evento = e;
	var dias = ('dias' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['dias'] : '';
	var dias = _.extend(dias, {
		d7: 1
	});
	require('vars')[_var_scopekey]['dias'] = dias;

}

function Off_widgetPelota7(e) {

	var evento = e;
	var dias = ('dias' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['dias'] : '';
	var dias = _.extend(dias, {
		d7: 0
	});
	require('vars')[_var_scopekey]['dias'] = dias;

}

$.widgetPickerhoras.init({
	__id: 'ALL1517641115',
	onchange: Change_widgetPickerhoras,
	mins: true,
	a: 'a'
});

function Change_widgetPickerhoras(e) {

	var evento = e;
	/** 
	 * Actualizamos las variables con las horas indicadas por el inspector 
	 */
	require('vars')['desde'] = evento.desde;
	require('vars')['hasta'] = evento.hasta;

}

(function() {
	/** 
	 * Recuperamos la variable del inspector 
	 */
	var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
	if (!_.isNull(inspector.disponibilidad_horas)) {
		/** 
		 * Revisamos que el inspector tenga la disponibilidad de horas, revisamos que contenga datos, separamos los valores de la hora y mandamos los datos al widget de la disponibilidad de horas, de otro modo fijamos los valores con datos dummy 
		 */
		if ((_.isObject(inspector.disponibilidad_horas) || (_.isString(inspector.disponibilidad_horas)) && !_.isEmpty(inspector.disponibilidad_horas)) || _.isNumber(inspector.disponibilidad_horas) || _.isBoolean(inspector.disponibilidad_horas)) {

			horas = inspector.disponibilidad_horas.split(" ");
			desde_horas = horas[0].split(":");
			hasta_horas = horas[1].split(":")
			require('vars')['desde'] = desde_horas[0];
			require('vars')['hasta'] = hasta_horas[0];
			if (Ti.App.deployType != 'production') console.log('horas insp', {
				"dispo": horas
			});

			$.widgetPickerhoras.set({
				desde: desde_horas[0],
				hasta: hasta_horas[0]
			});
		} else {
			require('vars')['desde'] = L('x2212294583', '1');
			require('vars')['hasta'] = L('x2212294583', '1');
		}
		if (Ti.App.deployType != 'production') console.log('NO ES NULO', {});
	}
	/** 
	 * Encendemos los dias que el inspector tenia marcado como disponibles 
	 */

	$.widgetPelota.set({
		on: inspector.d1
	});

	$.widgetPelota2.set({
		on: inspector.d2
	});

	$.widgetPelota3.set({
		on: inspector.d3
	});

	$.widgetPelota4.set({
		on: inspector.d4
	});

	$.widgetPelota5.set({
		on: inspector.d5
	});

	$.widgetPelota6.set({
		on: inspector.d6
	});

	$.widgetPelota7.set({
		on: inspector.d7
	});
	/** 
	 * Creamos una variable de estructura con los dias disponibles y los guardamos en una variable para acceder desde otro lado 
	 */
	var dias = {
		d1: inspector.d1,
		d2: inspector.d2,
		d3: inspector.d3,
		d4: inspector.d4,
		d5: inspector.d5,
		d6: inspector.d6,
		d7: inspector.d7
	};
	require('vars')[_var_scopekey]['dias'] = dias;
	if (Ti.App.deployType != 'production') console.log('quiero saber si los dias estan bien cargados en la estructura', {
		"datos": dias
	});
})();

function Postlayout_Editar_PartTime(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var ID_1482413520_func = function() {
		/** 
		 * Una vez ya cargada la tarea, dejamos la variable limpia para poder abrir otras pantallas 
		 */
		require('vars')['var_abriendo'] = '';
	};
	var ID_1482413520 = setTimeout(ID_1482413520_func, 1000 * 0.2);

}
if (OS_IOS || OS_ANDROID) {
	$.Editar_PartTime.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}