var _bind4section = {
	"ref1": "tareas"
};
var _list_templates = {
	"elemento": {
		"vista2": {},
		"Label3": {
			"text": "{nombre}"
		},
		"Label2": {
			"text": "{id_server}"
		},
		"Label": {
			"text": "{id_segured}"
		},
		"vista": {}
	},
	"dano": {
		"Label2": {
			"text": "{id}"
		},
		"vista29": {},
		"Label": {
			"text": "{nombre}"
		}
	},
	"pborrar": {
		"vista5": {},
		"imagen": {},
		"vista3": {},
		"Label4": {
			"text": "{id}"
		},
		"Label3": {
			"text": "{nombre}"
		},
		"vista4": {},
		"vista22": {},
		"vista23": {},
		"vista24": {},
		"imagen2": {}
	},
	"contenido": {
		"vista2": {},
		"Label2": {
			"text": "{id}"
		},
		"Label": {
			"text": "{nombre}"
		}
	},
	"recinto": {
		"vista2": {},
		"Label": {
			"text": "{nombre}"
		},
		"Label2": {
			"text": "{id}"
		}
	},
	"nivel": {
		"Label2": {
			"text": "{id}"
		},
		"Label": {
			"text": "{nombre}"
		},
		"vista21": {}
	},
	"tarea": {
		"vista16": {},
		"vista87": {},
		"Label7": {
			"text": "{direccion}"
		},
		"vista98": {},
		"vista93": {},
		"vista94": {},
		"Adistance4": {
			"text": "a {distance} km"
		},
		"vista107": {},
		"vista103": {},
		"vista11": {},
		"Label11": {
			"text": "{comuna}"
		},
		"Label21": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"Adistance7": {
			"text": "a {distance} km"
		},
		"vista39": {},
		"vista42": {},
		"vista56": {},
		"vista48": {},
		"vista20": {},
		"vista104": {},
		"vista49": {},
		"vista66": {},
		"Label30": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"Label5": {
			"text": "{comuna}"
		},
		"vista14": {},
		"vista65": {},
		"vista62": {},
		"Label14": {
			"text": "{comuna}"
		},
		"vista73": {
			"backgroundColor": "{bgcolor}"
		},
		"vista81": {
			"visible": "{vis_tipo9}"
		},
		"vista84": {},
		"Label23": {
			"text": "{comuna}"
		},
		"Label13": {
			"text": "{direccion}"
		},
		"vista91": {
			"backgroundColor": "{bgcolor}"
		},
		"Label33": {
			"text": "{comuna}"
		},
		"vista58": {},
		"Label8": {
			"text": "{comuna}"
		},
		"vista86": {},
		"vista69": {},
		"vista54": {
			"visible": "{vis_tipo6}"
		},
		"Label28": {
			"text": "{direccion}"
		},
		"vista77": {},
		"vista35": {},
		"Label3": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"vista38": {},
		"Label16": {
			"text": "{direccion}"
		},
		"vista26": {},
		"vista50": {},
		"Adistance6": {
			"text": "a {distance} km"
		},
		"Adistance11": {
			"text": "a {distance} km"
		},
		"vista28": {
			"backgroundColor": "{bgcolor}"
		},
		"vista80": {},
		"vista74": {},
		"Label34": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"Label35": {
			"text": "{id}"
		},
		"Label": {
			"text": "{direccion}"
		},
		"vista61": {},
		"vista83": {},
		"vista36": {
			"visible": "{vis_tipo4}"
		},
		"vista88": {},
		"vista68": {},
		"Adistance": {
			"text": "a {distance} km"
		},
		"vista34": {},
		"vista17": {},
		"vista71": {},
		"Label10": {
			"text": "{direccion}"
		},
		"Label12": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"vista97": {},
		"vista23": {},
		"Label24": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"vista10": {
			"backgroundColor": "{bgcolor}"
		},
		"vista53": {},
		"vista29": {},
		"vista100": {
			"backgroundColor": "{bgcolor}"
		},
		"vista85": {},
		"vista31": {},
		"vista79": {},
		"vista46": {
			"backgroundColor": "{bgcolor}"
		},
		"Label9": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"vista64": {
			"backgroundColor": "{bgcolor}"
		},
		"Label20": {
			"text": "{comuna}"
		},
		"Label29": {
			"text": "{comuna}"
		},
		"Label18": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"vista30": {},
		"vista108": {
			"visible": "{seguir}"
		},
		"vista44": {},
		"vista106": {},
		"Label27": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"vista47": {},
		"Label22": {
			"text": "{direccion}"
		},
		"vista40": {},
		"Label26": {
			"text": "{comuna}"
		},
		"Adistance9": {
			"text": "a {distance} km"
		},
		"vista59": {},
		"vista22": {},
		"vista8": {},
		"vista25": {},
		"vista67": {},
		"Label2": {
			"text": "{comuna}"
		},
		"vista90": {
			"visible": "{vis_tipo10}"
		},
		"vista24": {},
		"vista89": {},
		"vista41": {},
		"vista12": {},
		"vista75": {},
		"vista43": {},
		"vista95": {},
		"vista57": {},
		"vista92": {},
		"vista76": {},
		"vista72": {
			"visible": "{vis_tipo8}"
		},
		"Label15": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"vista96": {},
		"vista82": {
			"backgroundColor": "{bgcolor}"
		},
		"vista37": {
			"backgroundColor": "{bgcolor}"
		},
		"Label31": {
			"text": "{prioridad_tiempo}"
		},
		"vista99": {
			"visible": "{vis_otro}"
		},
		"vista13": {},
		"Label17": {
			"text": "{comuna}"
		},
		"vista15": {},
		"vista101": {},
		"vista52": {},
		"vista45": {
			"visible": "{vis_tipo5}"
		},
		"Adistance8": {
			"text": "a {distance} km"
		},
		"imagen3": {},
		"vista60": {},
		"vista19": {
			"backgroundColor": "{bgcolor}"
		},
		"Label4": {
			"text": "{direccion}"
		},
		"vista102": {},
		"vista32": {},
		"vista18": {
			"visible": "{vis_tipo2}"
		},
		"Label6": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"vista105": {},
		"Adistance2": {
			"text": "a {distance} km"
		},
		"vista33": {},
		"vista70": {},
		"Adistance3": {
			"text": "a {distance} km"
		},
		"vista63": {
			"visible": "{vis_tipo7}"
		},
		"vista51": {},
		"Label25": {
			"text": "{direccion}"
		},
		"vista78": {},
		"vista55": {
			"backgroundColor": "{bgcolor}"
		},
		"Adistance5": {
			"text": "a {distance} km"
		},
		"vista9": {
			"visible": "{vis_tipo1}"
		},
		"Adistance10": {
			"text": "a {distance} km"
		},
		"vista21": {},
		"Label19": {
			"text": "{direccion}"
		},
		"Label32": {
			"text": "{direccion}"
		},
		"vista27": {
			"visible": "{vis_tipo3}"
		}
	}
};

var _activity;
if (OS_ANDROID) {
	_activity = $.HOY.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'HOY';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.HOY.addEventListener('open', function(e) {});
}
$.HOY.orientationModes = [Titanium.UI.PORTRAIT];


var consultarModelo_like = function(search) {
	if (typeof search !== 'string' || this === null) {
		return false;
	}
	search = search.replace(new RegExp("([\\.\\\\\\+\\*\\?\\[\\^\\]\\$\\(\\)\\{\\}\\=\\!\\<\\>\\|\\:\\-])", "g"), "\\$1");
	search = search.replace(/%/g, '.*').replace(/_/g, '.');
	return RegExp('^' + search + '$', 'gi').test(this);
};
var consultarModelo_filter = function(coll) {
	var filtered = _.toArray(coll.filter(function(m) {
		return true;
	}));
	return filtered;
};
var consultarModelo_update = function(e) {};
_.defer(function() {
	Alloy.Collections.tareas.fetch();
});
Alloy.Collections.tareas.on('add change delete', function(ee) {
	consultarModelo_update(ee);
});
var consultarModelo_transform = function(model) {
	var fila = model.toJSON();
	/** 
	 * Valores extra default 
	 */
	var fila = _.extend(fila, {
		vis_tipo1: L('x734881840_traducir', 'false'),
		vis_tipo2: L('x734881840_traducir', 'false'),
		vis_tipo3: L('x734881840_traducir', 'false'),
		vis_tipo4: L('x734881840_traducir', 'false'),
		vis_tipo5: L('x734881840_traducir', 'false'),
		vis_tipo6: L('x734881840_traducir', 'false'),
		vis_tipo7: L('x734881840_traducir', 'false'),
		vis_tipo8: L('x734881840_traducir', 'false'),
		vis_tipo9: L('x734881840_traducir', 'false'),
		vis_tipo10: L('x734881840_traducir', 'false'),
		vis_otro: L('x734881840_traducir', 'false'),
		bgcolor: L('x1492402853', '#CECECE'),
		seguir: L('x734881840_traducir', 'false')
	});
	if (fila.prioridad_tiempo == 1 || fila.prioridad_tiempo == '1') {
		/** 
		 * Movemos pinchos de mapa segun posicion de tareas de hoy 
		 */
		$.pinchoMapa2.setLatitude(fila.lat);

		$.pinchoMapa2.setLongitude(fila.lon);

		var fila = _.extend(fila, {
			vis_tipo1: L('x4261170317', 'true'),
			bgcolor: L('x1884157677', '#2D9EDB')
		});
	} else if (fila.prioridad_tiempo == 2) {
		$.pinchoMapa3.setLatitude(fila.lat);

		$.pinchoMapa3.setLongitude(fila.lon);

		var fila = _.extend(fila, {
			vis_tipo2: L('x4261170317', 'true'),
			bgcolor: L('x602248408', '#EE7F7E')
		});
	} else if (fila.prioridad_tiempo == 3) {
		$.pinchoMapa4.setLatitude(fila.lat);

		$.pinchoMapa4.setLongitude(fila.lon);

		var fila = _.extend(fila, {
			vis_tipo3: L('x4261170317', 'true'),
			bgcolor: L('x1927584467', '#8383DB')
		});
	} else if (fila.prioridad_tiempo == 4) {
		$.pinchoMapa5.setLatitude(fila.lat);

		$.pinchoMapa5.setLongitude(fila.lon);

		var fila = _.extend(fila, {
			vis_tipo4: L('x4261170317', 'true'),
			bgcolor: L('x1269456848', '#FCBD83')
		});
	} else if (fila.prioridad_tiempo == 5) {
		$.pinchoMapa6.setLatitude(fila.lat);

		$.pinchoMapa6.setLongitude(fila.lon);

		var fila = _.extend(fila, {
			vis_tipo5: L('x4261170317', 'true'),
			bgcolor: L('x2849471580', '#8CE5BD')
		});
	} else if (fila.prioridad_tiempo == 6) {
		$.pinchoMapa7.setLatitude(fila.lat);

		$.pinchoMapa7.setLongitude(fila.lon);

		var fila = _.extend(fila, {
			vis_tipo6: L('x4261170317', 'true'),
			bgcolor: L('x3452106967', '#F8DA54')
		});
	} else if (fila.prioridad_tiempo == 7) {
		$.pinchoMapa8.setLatitude(fila.lat);

		$.pinchoMapa8.setLongitude(fila.lon);

		var fila = _.extend(fila, {
			vis_tipo7: L('x4261170317', 'true'),
			bgcolor: L('x3332103381', '#B9AAF3')
		});
	} else if (fila.prioridad_tiempo == 8) {
		$.pinchoMapa9.setLatitude(fila.lat);

		$.pinchoMapa9.setLongitude(fila.lon);

		var fila = _.extend(fila, {
			vis_tipo8: L('x4261170317', 'true'),
			bgcolor: L('x3561405284', '#FFACAA')
		});
	} else if (fila.prioridad_tiempo == 9) {
		$.pinchoMapa10.setLatitude(fila.lat);

		$.pinchoMapa10.setLongitude(fila.lon);

		var fila = _.extend(fila, {
			vis_tipo9: L('x4261170317', 'true'),
			bgcolor: L('x2922666116', '#8BC9E8')
		});
	} else if (fila.prioridad_tiempo == 10) {
		$.pinchoMapa11.setLatitude(fila.lat);

		$.pinchoMapa11.setLongitude(fila.lon);

		var fila = _.extend(fila, {
			vis_tipo10: L('x4261170317', 'true'),
			bgcolor: L('x585770471', '#A5876D')
		});
	} else {
		$.pinchoMapa11.setLatitude(fila.lat);

		$.pinchoMapa11.setLongitude(fila.lon);

		var fila = _.extend(fila, {
			vis_otro: L('x4261170317', 'true')
		});
	}
	if ((_.isObject(fila.nivel_2) || _.isString(fila.nivel_2)) && _.isEmpty(fila.nivel_2)) {
		/** 
		 * Mapeamos campos de niveles 
		 */
		fila.comuna = fila.nivel_1;
	} else if ((_.isObject(fila.nivel_3) || _.isString(fila.nivel_3)) && _.isEmpty(fila.nivel_3)) {
		fila.comuna = fila.nivel_2;
	} else if ((_.isObject(fila.nivel_4) || _.isString(fila.nivel_4)) && _.isEmpty(fila.nivel_4)) {
		fila.comuna = fila.nivel_3;
	} else if ((_.isObject(fila.nivel_5) || _.isString(fila.nivel_5)) && _.isEmpty(fila.nivel_5)) {
		fila.comuna = fila.nivel_4;
	} else {
		fila.comuna = fila.nivel_5;
	}
	if (fila.estado_tarea == 4) {
		/** 
		 * Revisamos si el estado de la tarea esta en seguimiento 
		 */
		fila.seguir = true;
	}
	return fila;
};
consultarModelo_filter = function(coll) {
	var filtered = _.toArray(coll.filter(function(filax) {
		var fila = filax.toJSON();
		var test = true;
		var moment = require('alloy/moment');
		var fecha_hoy = moment(new Date()).format('YYYY-MM-DD');
		if (fila.fecha_tarea == fecha_hoy) {
			test = true;
		} else {
			test = false;
		}
		fila = null;
		return test;
	}));
	var ordered = _.sortBy(filtered, 'prioridad_tiempo');
	return ordered;
};
Alloy.Collections.tareas.fetch();


$.pinchoMapa.applyProperties({
	latitude: parseFloat('0.0'),
	longitude: parseFloat('0.0'),
	image: '/images/i0752E598FC723F6708D472F7AB34311A.png'
});


$.pinchoMapa2.applyProperties({
	latitude: parseFloat('0.0'),
	longitude: parseFloat('0.0'),
	image: '/images/iB8F4698ED4EF9DE4D073ADC090AF07A3.png'
});


$.pinchoMapa3.applyProperties({
	latitude: parseFloat('0.0'),
	longitude: parseFloat('0.0'),
	image: '/images/iA3D34032D37972198904AD753EA1DE26.png'
});


$.pinchoMapa4.applyProperties({
	latitude: parseFloat('0.0'),
	longitude: parseFloat('0.0'),
	image: '/images/iA7D179C87F7325F3FBDAB36A6CA3FC58.png'
});


$.pinchoMapa5.applyProperties({
	latitude: parseFloat('0.0'),
	longitude: parseFloat('0.0'),
	image: '/images/i07BD9F7C5EDAEC7FD998A077330516E5.png'
});


$.pinchoMapa6.applyProperties({
	latitude: parseFloat('0.0'),
	longitude: parseFloat('0.0'),
	image: '/images/iE3B5F64AE888BA3A54B0BC278814961A.png'
});


$.pinchoMapa7.applyProperties({
	latitude: parseFloat('0.0'),
	longitude: parseFloat('0.0'),
	image: '/images/iBFE4DC825CD3D239883F7A858B1B14D8.png'
});


$.pinchoMapa8.applyProperties({
	latitude: parseFloat('0.0'),
	longitude: parseFloat('0.0'),
	image: '/images/i733093F68676D2D585523953DFE16DBF.png'
});


$.pinchoMapa9.applyProperties({
	latitude: parseFloat('0.0'),
	longitude: parseFloat('0.0'),
	image: '/images/i801CAD94710E3C73011A5EF0C6AF55E3.png'
});


$.pinchoMapa10.applyProperties({
	latitude: parseFloat('0.0'),
	longitude: parseFloat('0.0'),
	image: '/images/iA76649B47890BD38C12BC0A7922891CE.png'
});


$.pinchoMapa11.applyProperties({
	latitude: parseFloat('0.0'),
	longitude: parseFloat('0.0'),
	image: '/images/i57157C0EED282EF313085642E8C9B543.png'
});


$.mapa.setRegion({
	latitude: -33.392047,
	longitude: -70.542939,
	latitudeDelta: 0.005,
	longitudeDelta: 0.005
});
$.mapa.applyProperties({});
if (OS_IOS) {
	var mapa_camara = require('ti.map').createCamera({
		pitch: 0,
		heading: 0
	});
	$.mapa.setCamera(mapa_camara);
}


$.widgetMono.init({
	titulo: L('x2349703141_traducir', 'NO TIENES TAREAS'),
	__id: 'ALL859578408',
	texto: L('x2279881512_traducir', 'Asegurate de tomar tareas para hoy y revisar tu ruta'),
	alto: '-',
	top: '10%',
	ancho: '*',
	tipo: '_sorry'
});

function Click_imagen(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var evento = e;
	var imagen_imagen = 'distancia_on';

	if (typeof imagen_imagen == 'string' && 'styles' in require('a4w') && imagen_imagen in require('a4w').styles['images']) {
		imagen_imagen = require('a4w').styles['images'][imagen_imagen];
	}
	$.imagen.setImage(imagen_imagen);

	var imagen2_imagen = 'tiempo_off';

	if (typeof imagen2_imagen == 'string' && 'styles' in require('a4w') && imagen2_imagen in require('a4w').styles['images']) {
		imagen2_imagen = require('a4w').styles['images'][imagen2_imagen];
	}
	$.imagen2.setImage(imagen2_imagen);

	/** 
	 * Guardamos variable para poder filtrar en el mapa si se quiere usar o no las autopistas y despues calculamos la ruta 
	 */
	require('vars')['avoid'] = '';
	Alloy.Events.trigger('_calcular_ruta');

}

function Click_imagen2(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var evento = e;
	var imagen_imagen = 'distancia_off';

	if (typeof imagen_imagen == 'string' && 'styles' in require('a4w') && imagen_imagen in require('a4w').styles['images']) {
		imagen_imagen = require('a4w').styles['images'][imagen_imagen];
	}
	$.imagen.setImage(imagen_imagen);

	var imagen2_imagen = 'tiempo_on';

	if (typeof imagen2_imagen == 'string' && 'styles' in require('a4w') && imagen2_imagen in require('a4w').styles['images']) {
		imagen2_imagen = require('a4w').styles['images'][imagen2_imagen];
	}
	$.imagen2.setImage(imagen2_imagen);

	/** 
	 * Guardamos variable para poder filtrar en el mapa si se quiere usar o no las autopistas y despues calculamos la ruta 
	 */
	require('vars')['avoid'] = L('x1287501108_traducir', 'highways');
	Alloy.Events.trigger('_calcular_ruta');

}

function Load_imagen3(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var evento = e;
	elemento.start();

}

function Itemclick_listado(e) {

	e.cancelBubble = true;
	var objeto = e.section.getItemAt(e.itemIndex);
	var modelo = {},
		_modelo = [];
	var fila = {},
		fila_bak = {},
		info = {
			_template: objeto.template,
			_what: [],
			_seccion_ref: e.section.getHeaderTitle(),
			_model_id: -1
		},
		_tmp = {
			objmap: {}
		};
	if ('itemId' in e) {
		info._model_id = e.itemId;
		modelo._id = info._model_id;
		if (info._seccion_ref != '' && info._seccion_ref in _bind4section) {
			modelo._collection = _bind4section[info._seccion_ref];
			_tmp._coll = modelo._collection;
		}
	}
	var findVariables = require('fvariables');
	_.each(_list_templates[info._template], function(obj_id, id) {
		_.each(obj_id, function(valor, prop) {
			var llaves = findVariables(valor, '{', '}');
			_.each(llaves, function(llave) {
				_tmp.objmap[llave] = {
					id: id,
					prop: prop
				};
				fila[llave] = objeto[id][prop];
				if (id == e.bindId) info._what.push(llave);
			});
		});
	});
	info._what = info._what.join(',');
	fila_bak = JSON.parse(JSON.stringify(fila));
	/** 
	 * Ocupamos variable para impedir que la proxima pantalla se abra dos veces 
	 */
	var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
	if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
		require('vars')['var_abriendo'] = L('x4261170317', 'true');
		Alloy.createController("detalletarea_index", {
			'_id': fila.id,
			'__master_model': (typeof modelo !== 'undefined') ? modelo : {},
			'__modelo': (typeof _modelo !== 'undefined' && _modelo.length > 0) ? _modelo[0] : {}
		}).getView().open();
	}
	_tmp.changed = false;
	_tmp.diff_keys = [];
	_.each(fila, function(value1, prop) {
		var had_samekey = false;
		_.each(fila_bak, function(value2, prop2) {
			if (prop == prop2 && value1 == value2) {
				had_samekey = true;
			} else if (!_.has(fila_bak, prop) || !_.has(fila, prop2)) {
				has_samekey = true;
			}
		});
		if (!had_samekey) _tmp.diff_keys.push(prop);
	});
	if (_tmp.diff_keys.length > 0) _tmp.changed = true;
	if (_tmp.changed == true) {
		_.each(_tmp.diff_keys, function(llave) {
			objeto[_tmp.objmap[llave].id][_tmp.objmap[llave].prop] = fila[llave];
		});
		e.section.updateItemAt(e.itemIndex, objeto);
	}

}

function Longpress_vista111(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	/** 
	 * Armamos listado de casos para confirmar ruta 
	 */
	var moment = require('alloy/moment');
	var hoy_date = moment(new Date()).format('YYYY-MM-DD');
	var consultarModelo2_i = Alloy.createCollection('tareas');
	var consultarModelo2_i_where = 'fecha_tarea=\'' + hoy_date + '\'';
	consultarModelo2_i.fetch({
		query: 'SELECT * FROM tareas WHERE fecha_tarea=\'' + hoy_date + '\''
	});
	var tarea_lista = require('helper').query2array(consultarModelo2_i);
	var listacasos = _.pluck(tarea_lista, 'num_caso').join(',');
	var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
	var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
	Ti.App.Properties.setString('confirmarpush', JSON.stringify(false));
	require('vars')['rutaconfirmada'] = L('x734881840_traducir', 'false');
	var jsonfinal = {
		_method: L('x1814004025_traducir', 'POST'),
		_url: String.format(L('x2181838308', '%1$sconfirmarRuta'), url_server.toString()),
		id_inspector: inspector[0].id_server,
		codigo_identificador: inspector[0].codigo_identificador,
		tareas: listacasos
	};
	var jsonfinal = JSON.stringify(jsonfinal);
	var insertarModelo_m = Alloy.Collections.cola;
	var insertarModelo_fila = Alloy.createModel('cola', {
		data: jsonfinal,
		tipo: 'confirmar_ruta'
	});
	insertarModelo_m.add(insertarModelo_fila);
	insertarModelo_fila.save();
	_.defer(function() {});
	/** 
	 * Oculta boton mantener para iniciar 
	 */
	var vista110_visible = false;

	if (vista110_visible == 'si') {
		vista110_visible = true;
	} else if (vista110_visible == 'no') {
		vista110_visible = false;
	}
	$.vista110.setVisible(vista110_visible);

	/** 
	 * Limpieza de memoria 
	 */
	jsonfinal = null, listacasos = null;

}

(function() {
	_my_events['_refrescar_tareas_hoy,ID_852289061'] = function(evento) {
		/** 
		 * Recuperamos variable para saber si hay una inspeccion en curso 
		 */
		var inspeccion_encurso = ('inspeccion_encurso' in require('vars')) ? require('vars')['inspeccion_encurso'] : '';
		if (inspeccion_encurso == false || inspeccion_encurso == 'false') {
			/** 
			 * Limpiamos datos de tabla con ruta de tareas y reseteamos posiciones de pinchos previas (para ocultarlas con refresco) 
			 */
			$.pinchoMapa2.setLatitude('0.0');

			$.pinchoMapa2.setLongitude('0.0');

			$.pinchoMapa3.setLatitude('0.0');

			$.pinchoMapa3.setLongitude('0.0');

			$.pinchoMapa4.setLatitude('0.0');

			$.pinchoMapa4.setLongitude('0.0');

			$.pinchoMapa5.setLatitude('0.0');

			$.pinchoMapa5.setLongitude('0.0');

			$.pinchoMapa6.setLatitude('0.0');

			$.pinchoMapa6.setLongitude('0.0');

			$.pinchoMapa7.setLatitude('0.0');

			$.pinchoMapa7.setLongitude('0.0');

			$.pinchoMapa8.setLatitude('0.0');

			$.pinchoMapa8.setLongitude('0.0');

			$.pinchoMapa9.setLatitude('0.0');

			$.pinchoMapa9.setLongitude('0.0');

			$.pinchoMapa10.setLatitude('0.0');

			$.pinchoMapa10.setLongitude('0.0');

			$.pinchoMapa11.setLatitude('0.0');

			$.pinchoMapa11.setLongitude('0.0');

			$.pinchoMapa11.setLatitude('0.0');

			$.pinchoMapa11.setLongitude('0.0');

			/** 
			 * Refrescamos consulta con binding 
			 */
			var ID_281516773_func = function() {
				_.defer(function() {
					Alloy.Collections.tareas.fetch();
				});
			};
			var ID_281516773 = setTimeout(ID_281516773_func, 1000 * 0.2);
			var moment = require('alloy/moment');
			var hoy_date = moment(new Date()).format('YYYY-MM-DD');
			/** 
			 * Preguntamos si hay datos para mostrar u ocultar listado 
			 */
			var consultarModelo3_i = Alloy.createCollection('tareas');
			var consultarModelo3_i_where = 'fecha_tarea=\'' + hoy_date + '\' ORDER BY PRIORIDAD_TIEMPO ASC';
			consultarModelo3_i.fetch({
				query: 'SELECT * FROM tareas WHERE fecha_tarea=\'' + hoy_date + '\' ORDER BY PRIORIDAD_TIEMPO ASC'
			});
			var tarea_lista = require('helper').query2array(consultarModelo3_i);
			if (tarea_lista && tarea_lista.length == 0) {
				/** 
				 * Mostramos monito 
				 */
				$.widgetMono.mostrar({
					tipo: '_sorry'
				});
				/** 
				 * Ocultamos tabla de tareas 
				 */
				var vista3_visible = false;

				if (vista3_visible == 'si') {
					vista3_visible = true;
				} else if (vista3_visible == 'no') {
					vista3_visible = false;
				}
				$.vista3.setVisible(vista3_visible);

			} else {
				/** 
				 * Ocultamos monito 
				 */
				$.widgetMono.ocultar({});
				var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
				/** 
				 * Movemos casa de inspector como punto destino 
				 */
				$.pinchoMapa.setLatitude(inspector.lat_dir);

				$.pinchoMapa.setLongitude(inspector.lon_dir);

				/** 
				 * Mostramos tabla con tareas 
				 */
				var vista3_visible = true;

				if (vista3_visible == 'si') {
					vista3_visible = true;
				} else if (vista3_visible == 'no') {
					vista3_visible = false;
				}
				$.vista3.setVisible(vista3_visible);

			}
			/** 
			 * Preguntamos si la ruta ha sido o no confirmada 
			 */
			var confirmarpush = JSON.parse(Ti.App.Properties.getString('confirmarpush'));
			if (confirmarpush == true || confirmarpush == 'true') {
				/** 
				 * Mostramos boton confirmar ruta 
				 */
				var vista110_visible = true;

				if (vista110_visible == 'si') {
					vista110_visible = true;
				} else if (vista110_visible == 'no') {
					vista110_visible = false;
				}
				$.vista110.setVisible(vista110_visible);

			} else {
				/** 
				 * Ocultamos boton confirmar ruta 
				 */
				var vista110_visible = false;

				if (vista110_visible == 'si') {
					vista110_visible = true;
				} else if (vista110_visible == 'no') {
					vista110_visible = false;
				}
				$.vista110.setVisible(vista110_visible);

			}
			/** 
			 * Limpieza de memoria 
			 */
			tarea_lista = null, hoy_date = null, confirmarpush = null, inspeccion_encurso = null, evento = null;
		}
	};
	Alloy.Events.on('_refrescar_tareas_hoy', _my_events['_refrescar_tareas_hoy,ID_852289061']);
	_my_events['_calcular_ruta,ID_1553658638'] = function(evento) {
		var gps_error = ('gps_error' in require('vars')) ? require('vars')['gps_error'] : '';
		if (gps_error == false || gps_error == 'false') {
			var inspeccion_encurso = ('inspeccion_encurso' in require('vars')) ? require('vars')['inspeccion_encurso'] : '';
			if (inspeccion_encurso == false || inspeccion_encurso == 'false') {
				if (Ti.App.deployType != 'production') console.log('calculando ruta optima para el dia de hoy', {});
				var moment = require('alloy/moment');
				var hoy_date = moment(new Date()).format('YYYY-MM-DD');
				var consultarModelo4_i = Alloy.createCollection('tareas');
				var consultarModelo4_i_where = 'fecha_tarea=\'' + hoy_date + '\'';
				consultarModelo4_i.fetch({
					query: 'SELECT * FROM tareas WHERE fecha_tarea=\'' + hoy_date + '\''
				});
				var tarea_lista = require('helper').query2array(consultarModelo4_i);
				if (tarea_lista && tarea_lista.length == 0) {
					Alloy.Events.trigger('_refrescar_tareas_hoy');
					/** 
					 * Mostramos monito 
					 */
					$.widgetMono.mostrar({
						tipo: '_sorry'
					});
				} else {
					var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
					var gps_latitud = ('gps_latitud' in require('vars')) ? require('vars')['gps_latitud'] : '';
					var gps_longitud = ('gps_longitud' in require('vars')) ? require('vars')['gps_longitud'] : '';
					/** 
					 * Recorremos tareas para armar waypoints, considerando tarea seguida como inicial 
					 */

					//mapea el array de tarea_lista considerando las columnas lat y lon en un array de string tipo 'lat,lon'
					var waypoints = _.map(tarea_lista,
						function(num) {
							var item = num.lat + "," + num.lon;
							return item;
						});
					var waypoints = waypoints.join("|");
					/*var destino = _.max(tarea_lista, function(tarea){ return tarea.distancia_2; });*/
					var origen = gps_latitud + ',' + gps_longitud;
					var destino = inspector.lat_dir + ',' + inspector.lon_dir;
					//agregamos destino como ultimo waypoint, porque no lo marca como ruta.
					waypoints = waypoints + '|' + destino
					var avoid = ('avoid' in require('vars')) ? require('vars')['avoid'] : '';
					var googlekeymap = ('googlekeymap' in require('vars')) ? require('vars')['googlekeymap'] : '';
					if (Ti.App.deployType != 'production') console.log('consultando a google rutas para waypoints', {
						"waypoints": waypoints
					});
					/** 
					 * API de Google Directions 
					 */
					var consultarURL = {};
					consultarURL.success = function(e) {
						var elemento = e,
							valor = e;
						if (elemento.status == L('x3610695981_traducir', 'OK')) {
							/** 
							 * Borramos ruta previa si existe 
							 */
							var old_ruta = ('old_ruta' in require('vars')) ? require('vars')['old_ruta'] : '';
							if (old_ruta && old_ruta.length) {
								var item_rutas_index = 0;
								_.each(old_ruta, function(item_rutas, item_rutas_pos, item_rutas_list) {
									item_rutas_index += 1;
									$.mapa.removeRoute(item_rutas);

								});
								/** 
								 * Limpieza de memoria 
								 */
								item_rutas = null;
							}
							/** 
							 * Dibujamos nuevas rutas 
							 */
							var ruta = elemento.routes[0];
							var rutas = [];
							var pincho_index = 0;
							_.each(ruta.legs, function(pincho, pincho_pos, pincho_list) {
								pincho_index += 1;
								var tramo_index = 0;
								_.each(pincho.steps, function(tramo, tramo_pos, tramo_list) {
									tramo_index += 1;
									if (pincho_index == ruta.legs.length) {
										var rutaMapa_poly = require('polyline').decode(tramo.polyline.points);
										var nuevaruta = require('ti.map').createRoute({
											width: 6,
											color: '#f7d952',
											points: rutaMapa_poly
										});
									} else if (pincho_index == 1 || pincho_index == '1') {
										var rutaMapa2_poly = require('polyline').decode(tramo.polyline.points);
										var nuevaruta = require('ti.map').createRoute({
											width: 6,
											color: '#2d9edb',
											points: rutaMapa2_poly
										});
									} else if (pincho_index == 2) {
										var rutaMapa3_poly = require('polyline').decode(tramo.polyline.points);
										var nuevaruta = require('ti.map').createRoute({
											width: 6,
											color: '#ee7f7e',
											points: rutaMapa3_poly
										});
									} else if (pincho_index == 3) {
										var rutaMapa4_poly = require('polyline').decode(tramo.polyline.points);
										var nuevaruta = require('ti.map').createRoute({
											width: 6,
											color: '#b9aaf3',
											points: rutaMapa4_poly
										});
									} else if (pincho_index == 4) {
										var rutaMapa5_poly = require('polyline').decode(tramo.polyline.points);
										var nuevaruta = require('ti.map').createRoute({
											width: 6,
											color: '#efb275',
											points: rutaMapa5_poly
										});
									} else if (pincho_index == 5) {
										var rutaMapa6_poly = require('polyline').decode(tramo.polyline.points);
										var nuevaruta = require('ti.map').createRoute({
											width: 6,
											color: '#8ce5bd',
											points: rutaMapa6_poly
										});
									} else if (pincho_index == 6) {
										var rutaMapa7_poly = require('polyline').decode(tramo.polyline.points);
										var nuevaruta = require('ti.map').createRoute({
											width: 6,
											color: '#f8da54',
											points: rutaMapa7_poly
										});
									} else if (pincho_index == 7) {
										var rutaMapa8_poly = require('polyline').decode(tramo.polyline.points);
										var nuevaruta = require('ti.map').createRoute({
											width: 6,
											color: '#8383db',
											points: rutaMapa8_poly
										});
									} else if (pincho_index == 8) {
										var rutaMapa9_poly = require('polyline').decode(tramo.polyline.points);
										var nuevaruta = require('ti.map').createRoute({
											width: 6,
											color: '#ffacaa',
											points: rutaMapa9_poly
										});
									} else if (pincho_index == 9) {
										var rutaMapa10_poly = require('polyline').decode(tramo.polyline.points);
										var nuevaruta = require('ti.map').createRoute({
											width: 6,
											color: '#8bc9e8',
											points: rutaMapa10_poly
										});
									} else if (pincho_index == 10) {
										var rutaMapa11_poly = require('polyline').decode(tramo.polyline.points);
										var nuevaruta = require('ti.map').createRoute({
											width: 6,
											color: '#a5876d',
											points: rutaMapa11_poly
										});
									} else {
										var rutaMapa12_poly = require('polyline').decode(tramo.polyline.points);
										var nuevaruta = require('ti.map').createRoute({
											width: 6,
											color: '#2d9edb',
											points: rutaMapa12_poly
										});
									}
									/** 
									 * Agregamos la ruta al mapa 
									 */
									$.mapa.addRoute(nuevaruta);

									/** 
									 * guardamos la ruta en una variable 
									 */
									rutas.push(nuevaruta);
									require('vars')['old_ruta'] = rutas;
								});
							});
							/** 
							 * Limpieza de memoria 
							 */
							rutas = null;
							/** 
							 * Ordenamos las tareas 
							 */
							var moment = require('alloy/moment');
							var hoy_date = moment(new Date()).format('YYYY-MM-DD');
							var consultarModelo5_i = Alloy.createCollection('tareas');
							var consultarModelo5_i_where = 'fecha_tarea=\'' + hoy_date + '\'';
							consultarModelo5_i.fetch({
								query: 'SELECT * FROM tareas WHERE fecha_tarea=\'' + hoy_date + '\''
							});
							var tarea_lista = require('helper').query2array(consultarModelo5_i);
							var item_index = 0;
							_.each(ruta.waypoint_order, function(item, item_pos, item_list) {
								item_index += 1;
								var seguir_tarea = ('seguir_tarea' in require('vars')) ? require('vars')['seguir_tarea'] : '';
								if (item_index == ruta.waypoint_order.length) {
									/** 
									 * el ultimo valor lo omitimos porque el camino a casa no es una tarea 
									 */
								} else {
									var tareaid = tarea_lista[item].id;
									var consultarModelo6_i = Alloy.createCollection('tareas');
									var consultarModelo6_i_where = 'id=\'' + tareaid + '\'';
									consultarModelo6_i.fetch({
										query: 'SELECT * FROM tareas WHERE id=\'' + tareaid + '\''
									});
									var tarea_lista_aux = require('helper').query2array(consultarModelo6_i);
									var db = Ti.Database.open(consultarModelo6_i.config.adapter.db_name);
									if (consultarModelo6_i_where == '') {
										var sql = 'UPDATE ' + consultarModelo6_i.config.adapter.collection_name + ' SET prioridad_tiempo=\'' + item_index + '\'';
									} else {
										var sql = 'UPDATE ' + consultarModelo6_i.config.adapter.collection_name + ' SET prioridad_tiempo=\'' + item_index + '\' WHERE ' + consultarModelo6_i_where;
									}
									db.execute(sql);
									db.close();
									/** 
									 * Limpieza de memoria 
									 */
									tarea_lista_aux = null, tareaid = null;
								}
							});
							/** 
							 * Limpieza de memoria 
							 */
							ruta = null, rutas = null, tarea_lista = null, hoy_date = null, old_ruta = null, pincho = null, item = null;
						}
						/** 
						 * Limpieza de memoria 
						 */
						elemento = null;
						if (Ti.App.deployType != 'production') console.log('refrescando con ruta', {});
						Alloy.Events.trigger('_refrescar_tareas_hoy');
						elemento = null, valor = null;
					};
					consultarURL.error = function(e) {
						var elemento = e,
							valor = e;
						if (Ti.App.deployType != 'production') console.log('refrescando con error', {});
						Alloy.Events.trigger('_refrescar_tareas_hoy');
						elemento = null, valor = null;
					};
					require('helper').ajaxUnico('consultarURL', 'https://maps.googleapis.com/maps/api/directions/json?origin=' + origen + '&destination=' + destino + '&waypoints=' + waypoints + '&key=AIzaSyAJoATeoDjsJ3fNSe5q7eIKZ7VjT2AojqY&avoid=' + avoid + '&alternatives=false', 'get', {}, 15000, consultarURL);
					/** 
					 * Evaluar, elemento=null; 
					 */
					waypoints = null, destino = null, origen = null, avoid = null, googlekeymap = null, tarea_lista = null;
				}
			}
		}
	};
	Alloy.Events.on('_calcular_ruta', _my_events['_calcular_ruta,ID_1553658638']);
	_my_events['_refrescar_tareas,ID_1459777814'] = function(evento) {
		var inspeccion_encurso = ('inspeccion_encurso' in require('vars')) ? require('vars')['inspeccion_encurso'] : '';
		if (inspeccion_encurso == false || inspeccion_encurso == 'false') {
			/** 
			 * solo refrescamos si no hay una inspecci&#243;n en curso 
			 */
			if (Ti.App.deployType != 'production') console.log('llamado refrescar ruta de hoy', {});
			Alloy.Events.trigger('_calcular_ruta');
		}
	};
	Alloy.Events.on('_refrescar_tareas', _my_events['_refrescar_tareas,ID_1459777814']);
	/** 
	 * Esto se llama para que parta con informacion si tiene ya tareas para hoy 
	 */
	var ID_975413889_func = function() {
		Alloy.Events.trigger('_calcular_ruta');
	};
	var ID_975413889 = setTimeout(ID_975413889_func, 1000 * 0.1);
})();

function Androidback_HOY(e) {
	/** 
	 * No tiene acciones para prevenir que el usuario salga del menu 
	 */
	e.cancelBubble = true;
	var elemento = e.source;

}
if (OS_IOS || OS_ANDROID) {
	$.HOY.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}
//$.HOY.open();
