var _bind4section = {};
var _list_templates = {};

var _activity;
if (OS_ANDROID) {
	_activity = $.inicio.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'inicio';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.inicio.addEventListener('open', function(e) {});
}
$.inicio.orientationModes = [Titanium.UI.PORTRAIT];


$.widgetBarra.init({
	titulo: L('x3096182311_traducir', 'TOMAR TAREA'),
	__id: 'ALL1426322055',
	fondo: 'fondoblanco',
	colortitulo: 'negro',
	modal: L('', '')
});


$.widgetMapa.init({
	__id: 'ALL658148125',
	onlisto: Listo_widgetMapa,
	label_a: L('x3904355907', 'a'),
	onerror: Error_widgetMapa,
	_bono: L('x2764662954_traducir', 'Bono adicional:'),
	bono: L('', ''),
	ondatos: Datos_widgetMapa
});

function Listo_widgetMapa(e) {

	var evento = e;
	if (Ti.App.deployType != 'production') console.log('widget mapa ha llamado evento \'listo\'', {});

}

function Error_widgetMapa(e) {

	var evento = e;
	if (Ti.App.deployType != 'production') console.log('ha ocurrido un error con el mapa', {
		"evento": evento
	});

}

function Datos_widgetMapa(e) {

	var evento = e;
	if (Ti.App.deployType != 'production') console.log(String.format(L('x3211431303_traducir', 'datos recibidos desde widget mapa: %1$s'), evento.ruta_distancia.toString()), {
		"evento": evento
	});

}

$.widgetBoton.init({
	__id: 'ALL886082434',
	texto: L('x990554165_traducir', 'PRESIONE PARA VERIFICAR'),
	vertexto: true,
	verprogreso: false,
	onpresiono: Presiono_widgetBoton
});

function Presiono_widgetBoton(e) {

	var evento = e;
	/** 
	 * Activamos animacion de progreso en boton 
	 */

	$.widgetBoton.iniciar_progreso({});
	if (evento.estado == 1 || evento.estado == '1') {
		/** 
		 * Llamamos servicios obtenerMisTareas al aceptarTarea para reordenar posici&#243;n seg&#250;n distancias y tener rutas correctas. 
		 */
		/** 
		 * De animacion de progreso en boton 
		 */

		$.widgetBoton.detener_progreso({});
		$.inicio.close();
		/** 
		 * Necesitamos saber la posicion del inspector para poder avisarle al servidor que vamos a tomar una tarea 
		 */
		var _ifunc_res_obtenerGPS = function(e) {
			if (e.error) {
				var posicion = {
					error: true,
					latitude: -1,
					longitude: -1,
					reason: (e.reason) ? e.reason : 'unknown',
					accuracy: 0,
					speed: 0,
					error_compass: false
				};
			} else {
				var posicion = e;
			}
			if (posicion.error == true || posicion.error == 'true') {
				var preguntarAlerta_opts = [L('x1518866076_traducir', 'Aceptar')];
				var preguntarAlerta = Ti.UI.createAlertDialog({
					title: L('x57652245_traducir', 'Alerta'),
					message: L('x3873304135_traducir', 'No se pudo obtener la ubicación'),
					buttonNames: preguntarAlerta_opts
				});
				preguntarAlerta.addEventListener('click', function(e) {
					var suu = preguntarAlerta_opts[e.index];
					suu = null;

					e.source.removeEventListener("click", arguments.callee);
				});
				preguntarAlerta.show();
			} else {
				if (Ti.Network.networkType == Ti.Network.NETWORK_NONE) {
					var preguntarAlerta2_opts = [L('x1518866076_traducir', 'Aceptar'), L('x2376009830_traducir', ' Cancelar')];
					var preguntarAlerta2 = Ti.UI.createAlertDialog({
						title: L('x57652245_traducir', 'Alerta'),
						message: L('x78115213_traducir', 'No tiene internet, por favor conectese a una red para realizar esta accion.'),
						buttonNames: preguntarAlerta2_opts
					});
					preguntarAlerta2.addEventListener('click', function(e) {
						var suu = preguntarAlerta2_opts[e.index];
						suu = null;

						e.source.removeEventListener("click", arguments.callee);
					});
					preguntarAlerta2.show();
				} else {
					var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
					var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
					var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
					if (Ti.App.deployType != 'production') console.log('consultando servicio aceptarTarea', {
						"inspector": inspector,
						"seltarea": seltarea
					});
					var consultarURL = {};

					consultarURL.success = function(e) {
						var elemento = e,
							valor = e;
						if (Ti.App.deployType != 'production') console.log('estoy en success pero fuera de la condicion', {
							"datos": elemento
						});
						if (elemento.error == 0 || elemento.error == '0') {

							$.widgetBoton.datos({
								verprogreso: 'false',
								vertexto: 'true'
							});

							$.widgetBoton.datos({
								verprogreso: 'false',
								vertexto: 'true',
								texto: 'PRESIONE PARA ACEPTAR',
								estilo: 'fondoverde',
								estado: 1
							});
							var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
							var eliminarModelo_i = Alloy.Collections.tareas_entrantes;
							var sql = 'DELETE FROM ' + eliminarModelo_i.config.adapter.collection_name + ' WHERE id_server=\'' + seltarea.id_server + '\'';
							var db = Ti.Database.open(eliminarModelo_i.config.adapter.db_name);
							db.execute(sql);
							db.close();
							eliminarModelo_i.trigger('remove');
							var eliminarModelo2_i = Alloy.Collections.emergencia;
							var sql = 'DELETE FROM ' + eliminarModelo2_i.config.adapter.collection_name + ' WHERE id_server=\'' + seltarea.id_server + '\'';
							var db = Ti.Database.open(eliminarModelo2_i.config.adapter.db_name);
							db.execute(sql);
							db.close();
							eliminarModelo2_i.trigger('remove');
							if (_.isNull(seltarea.fecha_tarea)) {
								if (Ti.App.deployType != 'production') console.log('Agregando fecha', {});
								hoy = new Date();
								var fecha_hoy = null;
								if ('formatear_fecha' in require('funciones')) {
									fecha_hoy = require('funciones').formatear_fecha({
										'fecha': hoy,
										'formato': L('x591439515_traducir', 'YYYY-MM-DD')
									});
								} else {
									try {
										fecha_hoy = f_formatear_fecha({
											'fecha': hoy,
											'formato': L('x591439515_traducir', 'YYYY-MM-DD')
										});
									} catch (ee) {}
								}
								var seltarea = _.extend(seltarea, {
									fecha_tarea: fecha_hoy
								});
							}
							var insertarModelo_m = Alloy.Collections.tareas;
							var insertarModelo_fila = Alloy.createModel('tareas', {
								fecha_tarea: seltarea.fecha_tarea,
								id_inspeccion: seltarea.id_inspeccion,
								nivel_2: seltarea.nivel_2,
								id_asegurado: seltarea.id_asegurado,
								comentario_can_o_rech: seltarea.comentario_can_o_rech,
								estado_tarea: seltarea.estado_tarea,
								bono: seltarea.bono,
								id_inspector: seltarea.id_inspector,
								lat: seltarea.lat,
								nivel_1: seltarea.nivel_1,
								pais: seltarea.pais,
								direccion: seltarea.direccion,
								asegurador: seltarea.asegurador,
								fecha_ingreso: seltarea.fecha_ingreso,
								fecha_siniestro: seltarea.fecha_siniestro,
								nivel_1_texto: seltarea.nivel_1_texto,
								distance: seltarea.distance,
								nivel_4: seltarea.nivel_4,
								id_server: seltarea.id_server,
								pais_texto: seltarea.pais_texto,
								categoria: seltarea.categoria,
								nivel_3: seltarea.nivel_3,
								num_caso: seltarea.num_caso,
								lon: seltarea.lon,
								tipo_tarea: seltarea.tipo_tarea,
								nivel_5: seltarea.nivel_5
							});
							insertarModelo_m.add(insertarModelo_fila);
							insertarModelo_fila.save();
							var ID_1419963851_func = function() {

								Alloy.Events.trigger('_refrescar_tareas');
							};
							var ID_1419963851 = setTimeout(ID_1419963851_func, 1000 * 0.2);
						} else {
							if (Ti.App.deployType != 'production') console.log('estoy en la otra condicion', {});
							var preguntarAlerta3_opts = [L('x1518866076_traducir', 'Aceptar')];
							var preguntarAlerta3 = Ti.UI.createAlertDialog({
								title: '' + String.format(L('x921749426_traducir', 'Error %1$s'), elemento.error.toString()) + '',
								message: '' + elemento.mensaje + '',
								buttonNames: preguntarAlerta3_opts
							});
							preguntarAlerta3.addEventListener('click', function(e) {
								var suu = preguntarAlerta3_opts[e.index];
								suu = null;

								e.source.removeEventListener("click", arguments.callee);
							});
							preguntarAlerta3.show();
						}
						elemento = null, valor = null;
					};

					consultarURL.error = function(e) {
						var elemento = e,
							valor = e;
						if (Ti.App.deployType != 'production') console.log('Respuesta servicio', {
							"datos": elemento
						});
						var preguntarAlerta4_opts = [L('x1518866076_traducir', 'Aceptar')];
						var preguntarAlerta4 = Ti.UI.createAlertDialog({
							title: L('x2619118453_traducir', 'Error'),
							message: L('x41636219_traducir', 'Error al conectarse al servidor'),
							buttonNames: preguntarAlerta4_opts
						});
						preguntarAlerta4.addEventListener('click', function(e) {
							var suu = preguntarAlerta4_opts[e.index];
							suu = null;

							e.source.removeEventListener("click", arguments.callee);
						});
						preguntarAlerta4.show();

						$.widgetBoton.datos({
							verprogreso: 'false',
							vertexto: 'true'
						});
						elemento = null, valor = null;
					};
					require('helper').ajaxUnico('consultarURL', '' + String.format(L('x1051250956', '%1$saceptarTarea'), url_server.toString()) + '', 'POST', {
						id_inspector: inspector.id_server,
						codigo_identificador: inspector.codigo_identificador,
						id_tarea: seltarea.id_server,
						num_caso: seltarea.num_caso,
						categoria: seltarea.categoria,
						tipo_tarea: seltarea.tipo_tarea
					}, 15000, consultarURL);
				}
			}
		};
		Ti.Geolocation.setAccuracy(Ti.Geolocation.ACCURACY_HIGH);
		if (Ti.Geolocation.hasLocationPermissions(Ti.Geolocation.AUTHORIZATION_WHEN_IN_USE)) {
			Ti.Geolocation.getCurrentPosition(function(ee) {
				if (Ti.Geolocation.getHasCompass() == false) {
					var pposicion = ('coords' in ee) ? ee.coords : {};
					pposicion.error = ('coords' in ee) ? false : true;
					pposicion.reason = '';
					pposicion.speed = 0;
					pposicion.compass = -1;
					pposicion.compass_accuracy = -1;
					pposicion.error_compass = true;
					_ifunc_res_obtenerGPS(pposicion);
				} else {
					Ti.Geolocation.getCurrentHeading(function(yy) {
						var pposicion = ('coords' in ee) ? ee.coords : {};
						pposicion.error = ('coords' in ee) ? false : true;
						pposicion.reason = ('error' in ee) ? ee.error : '';
						if (yy.error) {
							pposicion.error_compass = true;
						} else {
							pposicion.compass = yy.heading;
							pposicion.compass_accuracy = yy.heading.accuracy;
						}
						_ifunc_res_obtenerGPS(pposicion);
					});
				}
			});
		} else {
			Ti.Geolocation.requestLocationPermissions(Ti.Geolocation.AUTHORIZATION_WHEN_IN_USE, function(u) {
				if (u.success) {
					Ti.Geolocation.getCurrentPosition(function(ee) {
						if (Ti.Geolocation.getHasCompass() == false) {
							var pposicion = ('coords' in ee) ? ee.coords : {};
							pposicion.error = ('coords' in ee) ? false : true;
							pposicion.reason = '';
							pposicion.speed = 0;
							pposicion.compass = -1;
							pposicion.compass_accuracy = -1;
							pposicion.error_compass = true;
							_ifunc_res_obtenerGPS(pposicion);
						} else {
							Ti.Geolocation.getCurrentHeading(function(yy) {
								var pposicion = ('coords' in ee) ? ee.coords : {};
								pposicion.error = ('coords' in ee) ? false : true;
								pposicion.reason = '';
								if (yy.error) {
									pposicion.error_compass = true;
								} else {
									pposicion.compass = yy.heading;
									pposicion.compass_accuracy = yy.heading.accuracy;
								}
								_ifunc_res_obtenerGPS(pposicion);
							});
						}
					});
				} else {
					_ifunc_res_obtenerGPS({
						error: true,
						latitude: -1,
						longitude: -1,
						reason: 'permission_denied',
						accuracy: 0,
						speed: 0,
						error_compass: false
					});
				}
			});
		}
	} else if (evento.estado == 2) {

		$.widgetBoton.detener_progreso({});
		var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
		var gps_error = ('gps_error' in require('vars')) ? require('vars')['gps_error'] : '';
		var gps_latitud = ('gps_latitud' in require('vars')) ? require('vars')['gps_latitud'] : '';
		var gps_longitud = ('gps_longitud' in require('vars')) ? require('vars')['gps_longitud'] : '';
		var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
		var ID_455229195_func = function() {

			Alloy.Events.trigger('_refrescar_tareas');
		};
		var ID_455229195 = setTimeout(ID_455229195_func, 1000 * 0.2);
		$.inicio.close();
	} else {
		var _ifunc_res_obtenerGPS2 = function(e) {
			if (e.error) {
				var posicion = {
					error: true,
					latitude: -1,
					longitude: -1,
					reason: (e.reason) ? e.reason : 'unknown',
					accuracy: 0,
					speed: 0,
					error_compass: false
				};
			} else {
				var posicion = e;
			}

			$.widgetBoton.detener_progreso({});
			if (posicion.error == true || posicion.error == 'true') {
				/** 
				 * Si se puede obtener la posicion gps, hacemos una consulta al servidor para ver la disponibilidad de tarea, caso contrario avisamos al inspector que no se pudo obtener la ubicacion 
				 */
				var preguntarAlerta5_opts = [L('x1518866076_traducir', 'Aceptar')];
				var preguntarAlerta5 = Ti.UI.createAlertDialog({
					title: L('x57652245_traducir', 'Alerta'),
					message: L('x3873304135_traducir', 'No se pudo obtener la ubicación'),
					buttonNames: preguntarAlerta5_opts
				});
				preguntarAlerta5.addEventListener('click', function(e) {
					var suu = preguntarAlerta5_opts[e.index];
					suu = null;

					e.source.removeEventListener("click", arguments.callee);
				});
				preguntarAlerta5.show();
			} else {
				if (Ti.Network.networkType == Ti.Network.NETWORK_NONE) {
					var preguntarAlerta6_opts = [L('x1518866076_traducir', 'Aceptar'), L('x2376009830_traducir', ' Cancelar')];
					var preguntarAlerta6 = Ti.UI.createAlertDialog({
						title: L('x57652245_traducir', 'Alerta'),
						message: L('x78115213_traducir', 'No tiene internet, por favor conectese a una red para realizar esta accion.'),
						buttonNames: preguntarAlerta6_opts
					});
					preguntarAlerta6.addEventListener('click', function(e) {
						var suu = preguntarAlerta6_opts[e.index];
						suu = null;

						e.source.removeEventListener("click", arguments.callee);
					});
					preguntarAlerta6.show();
				} else {
					/** 
					 * Activamos animacion de progreso en boton 
					 */

					$.widgetBoton.iniciar_progreso({});
					if (Ti.App.deployType != 'production') console.log('llamando disponiblidad de tarea', {});
					/** 
					 * Recuperamos variable 
					 */
					var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
					var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
					var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
					/** 
					 * Consultamos al servidor si el inspector tiene disponibilidad para tomar la tarea. 
					 */
					var consultarURL2 = {};

					consultarURL2.success = function(e) {
						var elemento = e,
							valor = e;
						if (Ti.App.deployType != 'production') console.log('estoy en success pero fuera de la condicion', {
							"datos": elemento
						});
						/** 
						 * Activamos animacion de progreso en boton 
						 */

						$.widgetBoton.detener_progreso({});
						/** 
						 * Escondemos la animacion de progreso y mostramos texto en el boton 
						 */

						$.widgetBoton.datos({
							verprogreso: 'false',
							vertexto: 'true'
						});
						if (elemento.error == 0 || elemento.error == '0') {
							/** 
							 * Si la respuesta del servidor es correcta (o sea, error 0). Si la disponibilidad es 0, no se puede tomar por exceso de tareas tomadas, disponibilidad es 1, la tarea es tomada y registrada en el servidor 
							 */
							if (elemento.disponibilidad == 0 || elemento.disponibilidad == '0') {
								var cerrar = L('x3391520051_traducir', 'CERRAR');

								$.widgetBoton.datos({
									verprogreso: 'false',
									vertexto: 'true',
									texto: cerrar,
									estilo: 'fondorojo',
									estado: 2
								});
								var toma_max_tareas = L('x3766780365_traducir', 'SUPERASTE TU TOMA MAXIMA DE TAREAS');
								$.PrimeroDebemos.setText(toma_max_tareas);

								var cap_max_tareas = L('x2229417697_traducir', 'Tienes una capacidad maxima de tareas por dia pero aun puedes tomar en otros dias');

								$.widgetMono.update({
									texto: cap_max_tareas
								});
							} else if (elemento.disponibilidad == 1 || elemento.disponibilidad == '1') {

								$.widgetBoton.datos({
									verprogreso: 'false',
									vertexto: 'true'
								});
								var quedara_mistareas = L('x2113685590_traducir', 'Quedara guardada en Mis Tareas');
								$.PrimeroDebemos.setText(quedara_mistareas);

								var presione_aceptar = L('x2961973001_traducir', 'PRESIONE PARA ACEPTAR');

								$.widgetBoton.datos({
									verprogreso: 'false',
									vertexto: 'true',
									texto: presione_aceptar,
									estilo: 'fondoverde',
									estado: 1
								});
							}
						} else {
							var preguntarAlerta7_opts = [L('x1518866076_traducir', 'Aceptar')];
							var preguntarAlerta7 = Ti.UI.createAlertDialog({
								title: L('x2619118453_traducir', 'Error'),
								message: L('x41636219_traducir', 'Error al conectarse al servidor'),
								buttonNames: preguntarAlerta7_opts
							});
							preguntarAlerta7.addEventListener('click', function(e) {
								var suu = preguntarAlerta7_opts[e.index];
								suu = null;

								e.source.removeEventListener("click", arguments.callee);
							});
							preguntarAlerta7.show();
						}
						elemento = null, valor = null;
					};

					consultarURL2.error = function(e) {
						var elemento = e,
							valor = e;
						if (Ti.App.deployType != 'production') console.log('Respuesta servicio', {
							"datos": elemento
						});
						var preguntarAlerta8_opts = [L('x1518866076_traducir', 'Aceptar')];
						var preguntarAlerta8 = Ti.UI.createAlertDialog({
							title: L('x2619118453_traducir', 'Error'),
							message: L('x41636219_traducir', 'Error al conectarse al servidor'),
							buttonNames: preguntarAlerta8_opts
						});
						preguntarAlerta8.addEventListener('click', function(e) {
							var suu = preguntarAlerta8_opts[e.index];
							suu = null;

							e.source.removeEventListener("click", arguments.callee);
						});
						preguntarAlerta8.show();

						$.widgetBoton.datos({
							verprogreso: 'false',
							vertexto: 'true'
						});
						elemento = null, valor = null;
					};
					require('helper').ajaxUnico('consultarURL2', '' + String.format(L('x1968401862', '%1$sdispInspector'), url_server.toString()) + '', 'POST', {
						id_inspector: inspector.id_server,
						codigo_identificador: inspector.codigo_identificador,
						id_tarea: seltarea.id_server,
						num_caso: seltarea.num_caso
					}, 15000, consultarURL2);
				}
			}
		};
		Ti.Geolocation.setAccuracy(Ti.Geolocation.ACCURACY_HIGH);
		if (Ti.Geolocation.hasLocationPermissions(Ti.Geolocation.AUTHORIZATION_WHEN_IN_USE)) {
			Ti.Geolocation.getCurrentPosition(function(ee) {
				if (Ti.Geolocation.getHasCompass() == false) {
					var pposicion = ('coords' in ee) ? ee.coords : {};
					pposicion.error = ('coords' in ee) ? false : true;
					pposicion.reason = '';
					pposicion.speed = 0;
					pposicion.compass = -1;
					pposicion.compass_accuracy = -1;
					pposicion.error_compass = true;
					_ifunc_res_obtenerGPS2(pposicion);
				} else {
					Ti.Geolocation.getCurrentHeading(function(yy) {
						var pposicion = ('coords' in ee) ? ee.coords : {};
						pposicion.error = ('coords' in ee) ? false : true;
						pposicion.reason = ('error' in ee) ? ee.error : '';
						if (yy.error) {
							pposicion.error_compass = true;
						} else {
							pposicion.compass = yy.heading;
							pposicion.compass_accuracy = yy.heading.accuracy;
						}
						_ifunc_res_obtenerGPS2(pposicion);
					});
				}
			});
		} else {
			Ti.Geolocation.requestLocationPermissions(Ti.Geolocation.AUTHORIZATION_WHEN_IN_USE, function(u) {
				if (u.success) {
					Ti.Geolocation.getCurrentPosition(function(ee) {
						if (Ti.Geolocation.getHasCompass() == false) {
							var pposicion = ('coords' in ee) ? ee.coords : {};
							pposicion.error = ('coords' in ee) ? false : true;
							pposicion.reason = '';
							pposicion.speed = 0;
							pposicion.compass = -1;
							pposicion.compass_accuracy = -1;
							pposicion.error_compass = true;
							_ifunc_res_obtenerGPS2(pposicion);
						} else {
							Ti.Geolocation.getCurrentHeading(function(yy) {
								var pposicion = ('coords' in ee) ? ee.coords : {};
								pposicion.error = ('coords' in ee) ? false : true;
								pposicion.reason = '';
								if (yy.error) {
									pposicion.error_compass = true;
								} else {
									pposicion.compass = yy.heading;
									pposicion.compass_accuracy = yy.heading.accuracy;
								}
								_ifunc_res_obtenerGPS2(pposicion);
							});
						}
					});
				} else {
					_ifunc_res_obtenerGPS2({
						error: true,
						latitude: -1,
						longitude: -1,
						reason: 'permission_denied',
						accuracy: 0,
						speed: 0,
						error_compass: false
					});
				}
			});
		}
	}
}

$.widgetMono.init({
	__id: 'ALL859578408',
	texto: L('x2523383595_traducir', 'Tip: No aceptes tareas si no estás seguro que puedes llegar a tiempo'),
	top: 10,
	tipo: '_tip'
});


(function() {
	if (Ti.App.deployType != 'production') console.log('argumentos tomartarea', {
		"modelo": args
	});
	if (!_.isUndefined(args._tipo)) {
		/** 
		 * Dependiendo de donde venga la tarea, guardamos una variable con el detalle de la tarea 
		 */
		if (args._tipo == L('x2110366172_traducir', 'emergencias')) {
			var consultarModelo_i = Alloy.createCollection('emergencia');
			var consultarModelo_i_where = 'id=\'' + args._id + '\'';
			consultarModelo_i.fetch({
				query: 'SELECT * FROM emergencia WHERE id=\'' + args._id + '\''
			});
			var tareas = require('helper').query2array(consultarModelo_i);
			require('vars')['tareas'] = tareas;
		} else if (args._tipo == L('x2525272859_traducir', 'entrantes')) {
			var consultarModelo2_i = Alloy.createCollection('tareas_entrantes');
			var consultarModelo2_i_where = 'id=\'' + args._id + '\'';
			consultarModelo2_i.fetch({
				query: 'SELECT * FROM tareas_entrantes WHERE id=\'' + args._id + '\''
			});
			var tareas = require('helper').query2array(consultarModelo2_i);
			require('vars')['tareas'] = tareas;
		}
	}
	var tareas = ('tareas' in require('vars')) ? require('vars')['tareas'] : '';
	if (tareas && tareas.length) {
		/** 
		 * Revisamos si tareas contiene datos 
		 */
		/** 
		 * Guardamos en una variable los detalles de la tarea. Servira tambien para el resto de la inspeccion 
		 */
		require('vars')['seltarea'] = tareas[0];
		/** 
		 * Creamos estructura con datos de la tarea 
		 */
		var info = {
			direccion: tareas[0].direccion,
			ciudad_pais: String.format(L('x1487588533_traducir', '%1$s, %2$s'), (tareas[0].nivel_2) ? tareas[0].nivel_2.toString() : '', (tareas[0].pais_texto) ? tareas[0].pais_texto.toString() : ''),
			ciudad: tareas[0].nivel_1,
			distancia: tareas[0].distance
		};
		if ((_.isObject(tareas[0].nivel_2) || _.isString(tareas[0].nivel_2)) && _.isEmpty(tareas[0].nivel_2)) {
			/** 
			 * Agregamos el valor de la comuna como ultimo nivel 
			 */
			var info = _.extend(info, {
				comuna: tareas[0].nivel_1
			});
		} else if ((_.isObject(tareas[0].nivel_3) || _.isString(tareas[0].nivel_3)) && _.isEmpty(tareas[0].nivel_3)) {
			var info = _.extend(info, {
				comuna: tareas[0].nivel_2
			});
		} else if ((_.isObject(tareas[0].nivel_4) || _.isString(tareas[0].nivel_4)) && _.isEmpty(tareas[0].nivel_4)) {
			var info = _.extend(info, {
				comuna: tareas[0].nivel_3
			});
		} else if ((_.isObject(tareas[0].nivel_5) || _.isString(tareas[0].nivel_5)) && _.isEmpty(tareas[0].nivel_5)) {
			var info = _.extend(info, {
				comuna: tareas[0].nivel_4
			});
		} else {
			var info = _.extend(info, {
				comuna: tareas[0].nivel_5
			});
		}
		if (Ti.App.deployType != 'production') console.log('actualizamos mapa con direccion', {});
		if (tareas[0].perfil == L('x2137349405_traducir', 'casa')) {
			/** 
			 * Dependiendo del donde venga la tarea, desde la casa registrada o ubicacion gps se actualiza el widget del mapa 
			 */
			var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';

			$.widgetMapa.update({
				tipo: 'origen',
				origen_latitud: inspector.lat_dir,
				origen_longitud: inspector.lon_dir,
				destino_latitud: tareas[0].lat,
				destino_longitud: tareas[0].lon,
				ruta: 'true',
				direccion: info.direccion,
				comuna: info.comuna,
				ciudad: info.ciudad_pais,
				distancia: info.distancia
			});
		} else {

			$.widgetMapa.update({
				tipo: 'ubicacion',
				latitud: tareas[0].lat,
				longitud: tareas[0].lon,
				ruta: 'true',
				direccion: info.direccion,
				comuna: info.comuna,
				ciudad: info.ciudad_pais,
				distancia: info.distancia
			});
		}
		/** 
		 * transforma valor decimal de bono a numero entero al dividirlo por 1. 
		 */
		tareas[0].bono = (tareas[0].bono / 1);
		if (tareas[0].bono != 0.0 && tareas[0].bono != '0.0') {
			/** 
			 * Actualizamos el widget de los bonos en caso de que exista algo 
			 */

			$.widgetMapa.update({
				bono: tareas[0].bono
			});
		} else {

			$.widgetMapa.update({
				bono: ''
			});
		}
	}
})();

function Postlayout_inicio(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var ID_1294585966_func = function() {
		/** 
		 * Una vez ya cargada la tarea, dejamos la variable limpia para poder abrir otras pantallas 
		 */
		require('vars')['var_abriendo'] = '';
	};
	var ID_1294585966 = setTimeout(ID_1294585966_func, 1000 * 0.2);

}
if (OS_IOS || OS_ANDROID) {
	$.inicio.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}
//$.inicio.open();
