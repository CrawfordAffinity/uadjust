var _bind4section = {};
var _list_templates = {
	"elemento": {
		"vista2": {},
		"Label3": {
			"text": "{nombre}"
		},
		"Label2": {
			"text": "{id_server}"
		},
		"Label": {
			"text": "{id_segured}"
		},
		"vista": {}
	}
};
var $dano = $.dano.toJSON();

var _activity;
if (OS_ANDROID) {
	_activity = $.editardano.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'editardano';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.editardano.addEventListener('open', function(e) {});
}
$.editardano.orientationModes = [Titanium.UI.PORTRAIT];


$.widgetBarra.init({
	titulo: L('x1028612539_traducir', 'EDITAR DAÑO'),
	__id: 'ALL1312087199',
	oncerrar: Cerrar_widgetBarra,
	textoderecha: L('x2943883035_traducir', 'Guardar'),
	fondo: 'fondomorado',
	top: 0,
	modal: L('', ''),
	onpresiono: Presiono_widgetBarra,
	colortextoderecha: 'blanco'
});

function Cerrar_widgetBarra(e) {

	var evento = e;
	/** 
	 * Limpiamos widget 
	 */
	$.widgetModal.limpiar({});
	$.widgetModal2.limpiar({});
	$.editardano.close();

}

function Presiono_widgetBarra(e) {

	var evento = e;
	/** 
	 * Desenfocamos todos los campos de texto 
	 */
	$.DescribaelDao.blur();
	/** 
	 * Flag para revisar que todo este en orden 
	 */
	require('vars')[_var_scopekey]['todobien'] = L('x4261170317', 'true');
	if (_.isUndefined($dano.id_partida)) {
		/** 
		 * Validamos que los campos ingresados sean correctos 
		 */
		require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
		var preguntarAlerta_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x261137504_traducir', 'Debe indicar la partida'),
			buttonNames: preguntarAlerta_opts
		});
		preguntarAlerta.addEventListener('click', function(e) {
			var nulo = preguntarAlerta_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta.show();
	} else if ((_.isObject($dano.id_tipodano) || _.isString($dano.id_tipodano)) && _.isEmpty($dano.id_tipodano)) {
		require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
		var preguntarAlerta2_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta2 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x2702600033_traducir', 'Seleccione el tipo de daño'),
			buttonNames: preguntarAlerta2_opts
		});
		preguntarAlerta2.addEventListener('click', function(e) {
			var nulo = preguntarAlerta2_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta2.show();
	} else if ((_.isObject($dano.id_unidadmedida) || _.isString($dano.id_unidadmedida)) && _.isEmpty($dano.id_unidadmedida)) {
		require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
		var preguntarAlerta3_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta3 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x4202932850_traducir', 'Seleccione la unidad de medida del daño'),
			buttonNames: preguntarAlerta3_opts
		});
		preguntarAlerta3.addEventListener('click', function(e) {
			var nulo = preguntarAlerta3_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta3.show();
	} else if (_.isUndefined($dano.id_unidadmedida)) {
		require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
		var preguntarAlerta4_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta4 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x893178337_traducir', 'Debe indicar la unidad de medida'),
			buttonNames: preguntarAlerta4_opts
		});
		preguntarAlerta4.addEventListener('click', function(e) {
			var nulo = preguntarAlerta4_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta4.show();
	} else if ((_.isObject($dano.superficie) || _.isString($dano.superficie)) && _.isEmpty($dano.superficie)) {
		require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
		var preguntarAlerta5_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta5 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x1057642944_traducir', 'Ingrese la cubicación del daño'),
			buttonNames: preguntarAlerta5_opts
		});
		preguntarAlerta5.addEventListener('click', function(e) {
			var nulo = preguntarAlerta5_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta5.show();
	} else if ($dano.superficie.length == 0 || $dano.superficie.length == '0') {
		require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
		var preguntarAlerta6_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta6 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x1057642944_traducir', 'Ingrese la cubicación del daño'),
			buttonNames: preguntarAlerta6_opts
		});
		preguntarAlerta6.addEventListener('click', function(e) {
			var nulo = preguntarAlerta6_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta6.show();
	} else if (_.isUndefined($dano.superficie)) {
		require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
		var preguntarAlerta7_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta7 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x1063063406_traducir', 'Debe indicar la superficie del recinto'),
			buttonNames: preguntarAlerta7_opts
		});
		preguntarAlerta7.addEventListener('click', function(e) {
			var nulo = preguntarAlerta7_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta7.show();
	}
	var todobien = ('todobien' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['todobien'] : '';
	if (todobien == true || todobien == 'true') {
		/** 
		 * Eliminamos modelo previo 
		 */
		/** 
		 * Eliminamos modelo previo 
		 */
		var eliminarModelo_i = Alloy.Collections.insp_itemdanos;
		var sql = 'DELETE FROM ' + eliminarModelo_i.config.adapter.collection_name + ' WHERE id=\'' + args._dato.id + '\'';
		var db = Ti.Database.open(eliminarModelo_i.config.adapter.db_name);
		db.execute(sql);
		db.close();
		eliminarModelo_i.trigger('remove');
		/** 
		 * Guardamos modelo nuevo 
		 */
		Alloy.Collections[$.dano.config.adapter.collection_name].add($.dano);
		$.dano.save();
		Alloy.Collections[$.dano.config.adapter.collection_name].fetch();
		var ID_216701314_func = function() {
			/** 
			 * limpiamos widgets multiples (memoria) 
			 */
			$.widgetModal2.limpiar({});
			$.editardano.close();
		};
		var ID_216701314 = setTimeout(ID_216701314_func, 1000 * 0.1);
	}

}

function Click_vista4(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	/** 
	 * Ocupamos esta variable para impedir que la pantalla siguente se cargue dos veces 
	 */
	var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
	if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
		require('vars')['var_abriendo'] = L('x4261170317', 'true');
		Alloy.createController("listadodanos_index", {}).getView().open();
	}

}

$.widgetModal2.init({
	titulo: L('x1384515306_traducir', 'TIPO DAÑO'),
	cargando: L('x1831148736', 'cargando...'),
	__id: 'ALL1552750321',
	left: 0,
	onrespuesta: Respuesta_widgetModal2,
	campo: L('x953397154_traducir', 'Tipo de Daño'),
	onabrir: Abrir_widgetModal2,
	color: 'morado',
	right: 0,
	seleccione: L('x4123108455_traducir', 'seleccione tipo'),
	activo: true,
	onafterinit: Afterinit_widgetModal2
});

function Abrir_widgetModal2(e) {

	var evento = e;

}

function Respuesta_widgetModal2(e) {

	var evento = e;
	/** 
	 * Mostramos el valor seleccionado desde el widget 
	 */
	$.widgetModal2.labels({
		valor: evento.valor
	});
	/** 
	 * Actualizamos el id_tipodano e id_unidadmedida 
	 */
	$.dano.set({
		id_tipodano: evento.item.id_interno,
		id_unidadmedida: ''
	});
	if ('dano' in $) $dano = $.dano.toJSON();
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
		/** 
		 * Consultamos id_gerardo en tipo_accion, para obtener unidades de medida 
		 */
		var consultarModelo_i = Alloy.createCollection('tipo_accion');
		var consultarModelo_i_where = 'id_tipo_dano=\'' + evento.item.id_gerardo + '\'';
		consultarModelo_i.fetch({
			query: 'SELECT * FROM tipo_accion WHERE id_tipo_dano=\'' + evento.item.id_gerardo + '\''
		});
		var acciones = require('helper').query2array(consultarModelo_i);
		if (acciones && acciones.length) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			var consultarModelo2_i = Alloy.createCollection('unidad_medida');
			var consultarModelo2_i_where = 'pais_texto=\'' + pais[0].nombre + '\' AND id_accion=\'' + acciones[0].id_server + '\'';
			consultarModelo2_i.fetch({
				query: 'SELECT * FROM unidad_medida WHERE pais_texto=\'' + pais[0].nombre + '\' AND id_accion=\'' + acciones[0].id_server + '\''
			});
			var unidad = require('helper').query2array(consultarModelo2_i);
			/** 
			 * Transformamos nombres de tablas 
			 */
			var datos = [];
			_.each(unidad, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'valor';
					if (llave == 'id_segured') newkey = 'id_interno';
					if (llave == 'id_server') newkey = 'id_gerardo';
					if (llave == 'abrev') newkey = 'abrev';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
			/** 
			 * Cargamos el widget con los datos de tipo de unidad de medida 
			 */
			$.widgetModal.data({
				data: datos
			});
		}
	}
	/** 
	 * Actualizamos texto de widget unidad de medida 
	 */
	$.widgetModal.labels({
		seleccione: 'unidad'
	});
	/** 
	 * Limpiamos texto en superficie 
	 */
	$.campo.setValue('');

	$.label.setText('-');


}

function Afterinit_widgetModal2(e) {

	var evento = e;
	var ID_1171014616_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		/** 
		 * obtenemos valor seleccionado previamente y lo mostramos en selector con desface para que pueda mostrar bien el valor en pantalla, si no, carga un valor erroneo 
		 */
		var consultarModelo3_i = Alloy.createCollection('insp_itemdanos');
		var consultarModelo3_i_where = 'id=\'' + args._dato.id + '\'';
		consultarModelo3_i.fetch({
			query: 'SELECT * FROM insp_itemdanos WHERE id=\'' + args._dato.id + '\''
		});
		var insp_datos = require('helper').query2array(consultarModelo3_i);
		if (insp_datos && insp_datos.length) {
			var consultarModelo4_i = Alloy.createCollection('tipo_dano');
			var consultarModelo4_i_where = 'id_segured=\'' + insp_datos[0].id_tipodano + '\'';
			consultarModelo4_i.fetch({
				query: 'SELECT * FROM tipo_dano WHERE id_segured=\'' + insp_datos[0].id_tipodano + '\''
			});
			var tipo_dano = require('helper').query2array(consultarModelo4_i);
			if (tipo_dano && tipo_dano.length) {
				var ID_1940471085_func = function() {
					$.widgetModal2.labels({
						valor: tipo_dano[0].nombre
					});
					if (Ti.App.deployType != 'production') console.log('el nombre de la dano es', {
						"datos": tipo_dano[0].nombre
					});
				};
				var ID_1940471085 = setTimeout(ID_1940471085_func, 1000 * 0.21);
			}
		}
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			/** 
			 * obtenemos datos para selectores (solo los de este pais) 
			 */
			var consultarModelo5_i = Alloy.createCollection('tipo_dano');
			var consultarModelo5_i_where = 'pais_texto=\'' + pais[0].nombre + '\' AND id_partida=0';
			consultarModelo5_i.fetch({
				query: 'SELECT * FROM tipo_dano WHERE pais_texto=\'' + pais[0].nombre + '\' AND id_partida=0'
			});
			var danos = require('helper').query2array(consultarModelo5_i);
			var datos = [];
			_.each(danos, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'valor';
					if (llave == 'id_segured') newkey = 'id_interno';
					if (llave == 'id_server') newkey = 'id_gerardo';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
		} else {
			/** 
			 * Insertamos dummies 
			 */
			/** 
			 * Insertamos dummies 
			 */
			var eliminarModelo2_i = Alloy.Collections.tipo_dano;
			var sql = "DELETE FROM " + eliminarModelo2_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo2_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo2_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo_m = Alloy.Collections.tipo_dano;
				var insertarModelo_fila = Alloy.createModel('tipo_dano', {
					nombre: String.format(L('x1478811929_traducir', 'Picada%1$s'), item.toString()),
					id_partida: 0,
					id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
					id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item.toString()),
					pais: 1
				});
				insertarModelo_m.add(insertarModelo_fila);
				insertarModelo_fila.save();
			});
			var transformarModelo3_i = Alloy.createCollection('tipo_dano');
			transformarModelo3_i.fetch();
			var transformarModelo3_src = require('helper').query2array(transformarModelo3_i);
			var datos = [];
			_.each(transformarModelo3_src, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'valor';
					if (llave == 'id_segured') newkey = 'id_interno';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
		}
		/** 
		 * Cargamos el widget con los datos de tipo de unidad de medida 
		 */
		$.widgetModal2.data({
			data: datos
		});
	};
	var ID_1171014616 = setTimeout(ID_1171014616_func, 1000 * 0.2);

}

$.widgetFotochica.init({
	caja: 45,
	__id: 'ALL165543940',
	onlisto: Listo_widgetFotochica,
	left: 0,
	top: 0,
	onclick: Click_widgetFotochica
});

function Click_widgetFotochica(e) {

	var evento = e;
	/** 
	 * Definimos que estamos capturando foto en el primer item 
	 */
	require('vars')[_var_scopekey]['cual_foto'] = L('x2212294583', '1');
	/** 
	 * Abrimos camara 
	 */
	$.widgetCamaralight.disparar({});
	/** 
	 * Detenemos las animaciones de los widget 
	 */
	$.widgetFotochica.detener({});
	$.widgetFotochica2.detener({});
	$.widgetFotochica3.detener({});

}

function Listo_widgetFotochica(e) {

	var evento = e;
	/** 
	 * Recuperamos variables para poder definir la estructura de las carpetas donde se guardara la miniatura 
	 */
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	var nuevoid_f1 = ('nuevoid_f1' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['nuevoid_f1'] : '';
	if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
		var ID_153663287_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + seltarea.id_server);
		if (ID_153663287_d.exists() == false) ID_153663287_d.createDirectory();
		var ID_153663287_f = Ti.Filesystem.getFile(ID_153663287_d.resolve(), 'mini' + nuevoid_f1.id + '.jpg');
		if (ID_153663287_f.exists() == true) ID_153663287_f.deleteFile();
		ID_153663287_f.write(evento.mini);
		ID_153663287_d = null;
		ID_153663287_f = null;
	} else {
		var ID_327315158_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + 0);
		if (ID_327315158_d.exists() == false) ID_327315158_d.createDirectory();
		var ID_327315158_f = Ti.Filesystem.getFile(ID_327315158_d.resolve(), 'mini' + nuevoid_f1.id + '.jpg');
		if (ID_327315158_f.exists() == true) ID_327315158_f.deleteFile();
		ID_327315158_f.write(evento.mini);
		ID_327315158_d = null;
		ID_327315158_f = null;
	}
}

function Click_imagen(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var evento = e;
	/** 
	 * Definimos que estamos capturando foto en el primer item 
	 */
	require('vars')[_var_scopekey]['cual_foto'] = L('x2212294583', '1');
	/** 
	 * Abrimos camara 
	 */
	$.widgetCamaralight.disparar({});
	/** 
	 * Detenemos las animaciones de los widget 
	 */
	$.widgetFotochica.detener({});
	$.widgetFotochica2.detener({});
	$.widgetFotochica3.detener({});
	/** 
	 * Ocultamos la imagen obtenida desde la memoria del telefono para actualizar con la que acabamos de obtener 
	 */
	var vista16_visible = false;

	if (vista16_visible == 'si') {
		vista16_visible = true;
	} else if (vista16_visible == 'no') {
		vista16_visible = false;
	}
	$.vista16.setVisible(vista16_visible);


}

$.widgetFotochica2.init({
	caja: 45,
	__id: 'ALL1692328525',
	onlisto: Listo_widgetFotochica2,
	left: 5,
	top: 0,
	onclick: Click_widgetFotochica2
});

function Click_widgetFotochica2(e) {

	var evento = e;
	require('vars')[_var_scopekey]['cual_foto'] = 2;
	$.widgetCamaralight.disparar({});
	$.widgetFotochica2.detener({});
	$.widgetFotochica.detener({});
	$.widgetFotochica3.detener({});

}

function Listo_widgetFotochica2(e) {

	var evento = e;
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	var nuevoid_f2 = ('nuevoid_f2' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['nuevoid_f2'] : '';
	if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
		var ID_462637464_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + seltarea.id_server);
		if (ID_462637464_d.exists() == false) ID_462637464_d.createDirectory();
		var ID_462637464_f = Ti.Filesystem.getFile(ID_462637464_d.resolve(), 'mini' + nuevoid_f2.id + '.jpg');
		if (ID_462637464_f.exists() == true) ID_462637464_f.deleteFile();
		ID_462637464_f.write(evento.mini);
		ID_462637464_d = null;
		ID_462637464_f = null;
	} else {
		var ID_1888580742_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + 0);
		if (ID_1888580742_d.exists() == false) ID_1888580742_d.createDirectory();
		var ID_1888580742_f = Ti.Filesystem.getFile(ID_1888580742_d.resolve(), 'mini' + nuevoid_f2.id + '.jpg');
		if (ID_1888580742_f.exists() == true) ID_1888580742_f.deleteFile();
		ID_1888580742_f.write(evento.mini);
		ID_1888580742_d = null;
		ID_1888580742_f = null;
	}
}

function Click_imagen2(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var evento = e;
	require('vars')[_var_scopekey]['cual_foto'] = 2;
	$.widgetCamaralight.disparar({});
	$.widgetFotochica.detener({});
	$.widgetFotochica2.detener({});
	$.widgetFotochica3.detener({});
	var vista17_visible = false;

	if (vista17_visible == 'si') {
		vista17_visible = true;
	} else if (vista17_visible == 'no') {
		vista17_visible = false;
	}
	$.vista17.setVisible(vista17_visible);


}

$.widgetFotochica3.init({
	caja: 45,
	__id: 'ALL480946597',
	onlisto: Listo_widgetFotochica3,
	left: 5,
	top: 0,
	onclick: Click_widgetFotochica3
});

function Click_widgetFotochica3(e) {

	var evento = e;
	require('vars')[_var_scopekey]['cual_foto'] = 3;
	$.widgetCamaralight.disparar({});
	$.widgetFotochica3.detener({});
	$.widgetFotochica.detener({});
	$.widgetFotochica2.detener({});

}

function Listo_widgetFotochica3(e) {

	var evento = e;
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	var nuevoid_f3 = ('nuevoid_f3' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['nuevoid_f3'] : '';
	if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
		var ID_1959180332_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + seltarea.id_server);
		if (ID_1959180332_d.exists() == false) ID_1959180332_d.createDirectory();
		var ID_1959180332_f = Ti.Filesystem.getFile(ID_1959180332_d.resolve(), 'mini' + nuevoid_f3.id + '.jpg');
		if (ID_1959180332_f.exists() == true) ID_1959180332_f.deleteFile();
		ID_1959180332_f.write(evento.mini);
		ID_1959180332_d = null;
		ID_1959180332_f = null;
	} else {
		var ID_946227262_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + 0);
		if (ID_946227262_d.exists() == false) ID_946227262_d.createDirectory();
		var ID_946227262_f = Ti.Filesystem.getFile(ID_946227262_d.resolve(), 'mini' + nuevoid_f3.id + '.jpg');
		if (ID_946227262_f.exists() == true) ID_946227262_f.deleteFile();
		ID_946227262_f.write(evento.mini);
		ID_946227262_d = null;
		ID_946227262_f = null;
	}
}

function Click_imagen3(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var evento = e;
	require('vars')[_var_scopekey]['cual_foto'] = 3;
	$.widgetCamaralight.disparar({});
	$.widgetFotochica.detener({});
	$.widgetFotochica2.detener({});
	$.widgetFotochica3.detener({});
	var vista18_visible = false;

	if (vista18_visible == 'si') {
		vista18_visible = true;
	} else if (vista18_visible == 'no') {
		vista18_visible = false;
	}
	$.vista18.setVisible(vista18_visible);


}

$.widgetModal.init({
	titulo: L('x52303785_traducir', 'UNIDAD'),
	cargando: L('x1831148736', 'cargando...'),
	__id: 'ALL836121132',
	onrespuesta: Respuesta_widgetModal,
	campo: L('x2054670809_traducir', 'Unidad de medida'),
	onabrir: Abrir_widgetModal,
	color: 'morado',
	seleccione: L('x4091990063_traducir', 'unidad'),
	activo: true,
	onafterinit: Afterinit_widgetModal
});

function Abrir_widgetModal(e) {

	var evento = e;

}

function Respuesta_widgetModal(e) {

	var evento = e;
	/** 
	 * Mostramos el valor seleccionado desde el widget 
	 */
	$.widgetModal.labels({
		valor: evento.valor
	});
	/** 
	 * Actualizamos el id de la unidad de medida 
	 */
	$.dano.set({
		id_unidadmedida: evento.item.id_interno
	});
	if ('dano' in $) $dano = $.dano.toJSON();
	/** 
	 * modifica el label con la abreviatura de la unidad de medida seleccionada. 
	 */
	$.label.setText(evento.item.abrev);


}

function Afterinit_widgetModal(e) {

	var evento = e;
	var ID_1083031413_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			/** 
			 * Viene de login 
			 */
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			/** 
			 * Consultamos modelo unidad de medida y filtramos por el pais y el id_accion, posteriormente cambiamos nombres de tablas 
			 */
			var consultarModelo6_i = Alloy.createCollection('unidad_medida');
			var consultarModelo6_i_where = 'pais_texto=\'' + pais[0].nombre + '\' AND id_accion=0';
			consultarModelo6_i.fetch({
				query: 'SELECT * FROM unidad_medida WHERE pais_texto=\'' + pais[0].nombre + '\' AND id_accion=0'
			});
			var unidad = require('helper').query2array(consultarModelo6_i);
			/** 
			 * Obtenemos datos para selectores (solo los de este pais) 
			 */
			var datos = [];
			_.each(unidad, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'valor';
					if (llave == 'id_segured') newkey = 'id_interno';
					if (llave == 'id_server') newkey = 'id_gerardo';
					if (llave == 'abrev') newkey = 'abrev';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
		}
		/** 
		 * obtenemos valor seleccionado previamente y lo mostramos en selector con desface para que pueda mostrar bien el valor en pantalla, si no, carga un valor erroneo 
		 */
		var consultarModelo7_i = Alloy.createCollection('insp_itemdanos');
		var consultarModelo7_i_where = 'id=\'' + args._dato.id + '\'';
		consultarModelo7_i.fetch({
			query: 'SELECT * FROM insp_itemdanos WHERE id=\'' + args._dato.id + '\''
		});
		var insp_datos = require('helper').query2array(consultarModelo7_i);
		if (insp_datos && insp_datos.length) {
			var consultarModelo8_i = Alloy.createCollection('unidad_medida');
			var consultarModelo8_i_where = 'id_segured=\'' + insp_datos[0].id_unidadmedida + '\'';
			consultarModelo8_i.fetch({
				query: 'SELECT * FROM unidad_medida WHERE id_segured=\'' + insp_datos[0].id_unidadmedida + '\''
			});
			var unidad_medida = require('helper').query2array(consultarModelo8_i);
			if (unidad_medida && unidad_medida.length) {
				var ID_818615281_func = function() {
					$.widgetModal.labels({
						valor: unidad_medida[0].nombre
					});
					if (Ti.App.deployType != 'production') console.log('el nombre de la unidad es', {
						"datos": unidad_medida[0]
					});
					$.label.setText(unidad_medida[0].abrev);

				};
				var ID_818615281 = setTimeout(ID_818615281_func, 1000 * 0.21);
			}
		}
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			/** 
			 * Viene de login 
			 */
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			var consultarModelo9_i = Alloy.createCollection('unidad_medida');
			var consultarModelo9_i_where = 'pais_texto=\'' + pais[0].nombre + '\' AND id_accion=0';
			consultarModelo9_i.fetch({
				query: 'SELECT * FROM unidad_medida WHERE pais_texto=\'' + pais[0].nombre + '\' AND id_accion=0'
			});
			var unidad = require('helper').query2array(consultarModelo9_i);
			/** 
			 * obtenemos datos para selectores (solo los de este pais) 
			 */
			var datos = [];
			_.each(unidad, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'valor';
					if (llave == 'id_segured') newkey = 'id_interno';
					if (llave == 'id_server') newkey = 'id_gerardo';
					if (llave == 'abrev') newkey = 'abrev';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
		} else {
			/** 
			 * Insertamos dummies 
			 */
			/** 
			 * Insertamos dummies 
			 */
			var eliminarModelo3_i = Alloy.Collections.unidad_medida;
			var sql = "DELETE FROM " + eliminarModelo3_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo3_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo3_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo2_m = Alloy.Collections.unidad_medida;
				var insertarModelo2_fila = Alloy.createModel('unidad_medida', {
					nombre: String.format(L('x2542530260_traducir', 'Metro lineal%1$s'), item.toString()),
					id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
					id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item.toString()),
					pais: 1,
					abrev: String.format(L('x339551322_traducir', 'ML%1$s'), item.toString())
				});
				insertarModelo2_m.add(insertarModelo2_fila);
				insertarModelo2_fila.save();
			});
			/** 
			 * Transformamos nombres de tablas 
			 */
			var transformarModelo6_i = Alloy.createCollection('unidad_medida');
			transformarModelo6_i.fetch();
			var transformarModelo6_src = require('helper').query2array(transformarModelo6_i);
			var datos = [];
			_.each(transformarModelo6_src, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'valor';
					if (llave == 'id_segured') newkey = 'id_interno';
					if (llave == 'id_server') newkey = 'id_gerardo';
					if (llave == 'abrev') newkey = 'abrev';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
		}
		/** 
		 * Cargamos el widget con los datos de tipo de unidad de medida 
		 */
		$.widgetModal.data({
			data: datos
		});
	};
	var ID_1083031413 = setTimeout(ID_1083031413_func, 1000 * 0.2);

}

function Change_campo(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	/** 
	 * Al escribir la descripcion, vamos actualizando en la tabla de danos la superficie 
	 */
	$.dano.set({
		superficie: elemento
	});
	if ('dano' in $) $dano = $.dano.toJSON();
	elemento = null, source = null;

}

function Return_campo(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	/** 
	 * Al presionar enter en el teclado del telefono, desenfocamos el campo 
	 */
	$.campo.blur();
	elemento = null, source = null;

}

function Change_DescribaelDao(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	/** 
	 * Al escribir la descripcion, vamos actualizando en la tabla de danos la descripcion del dano 
	 */
	$.dano.set({
		descripcion_dano: elemento
	});
	if ('dano' in $) $dano = $.dano.toJSON();
	elemento = null, source = null;

}

$.widgetCamaralight.init({
	onfotolista: Fotolista_widgetCamaralight,
	__id: 'ALL1198539205'
});

function Fotolista_widgetCamaralight(e) {

	var evento = e;
	/** 
	 * Recuperamos cual fue la foto seleccionada 
	 */
	var cual_foto = ('cual_foto' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['cual_foto'] : '';
	if (cual_foto == 1 || cual_foto == '1') {
		var insertarModelo3_m = Alloy.Collections.numero_unico;
		var insertarModelo3_fila = Alloy.createModel('numero_unico', {
			comentario: 'nuevo recinto foto1'
		});
		insertarModelo3_m.add(insertarModelo3_fila);
		insertarModelo3_fila.save();
		var nuevoid_f1 = require('helper').model2object(insertarModelo3_m.last());
		/** 
		 * Guardamos en variable el detalle del modelo numero_unico para asignar informacion de foto1 del dano 
		 */
		require('vars')[_var_scopekey]['nuevoid_f1'] = nuevoid_f1;
		/** 
		 * Recuperamos variable para saber si estamos haciendo una inspeccion o dummy y guardar la foto en la memoria del celular 
		 */
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var ID_32162406_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
			if (ID_32162406_d.exists() == false) ID_32162406_d.createDirectory();
			var ID_32162406_f = Ti.Filesystem.getFile(ID_32162406_d.resolve(), 'cap' + nuevoid_f1.id + '.jpg');
			if (ID_32162406_f.exists() == true) ID_32162406_f.deleteFile();
			ID_32162406_f.write(evento.foto);
			ID_32162406_d = null;
			ID_32162406_f = null;
		} else {
			var ID_232547272_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + 0);
			if (ID_232547272_d.exists() == false) ID_232547272_d.createDirectory();
			var ID_232547272_f = Ti.Filesystem.getFile(ID_232547272_d.resolve(), 'cap' + nuevoid_f1.id + '.jpg');
			if (ID_232547272_f.exists() == true) ID_232547272_f.deleteFile();
			ID_232547272_f.write(evento.foto);
			ID_232547272_d = null;
			ID_232547272_f = null;
		}
		/** 
		 * Actualizamos el modelo indicando cual es el nombre de la imagen 
		 */
		$.dano.set({
			foto1: String.format(L('x1070475521', 'cap%1$s.jpg'), nuevoid_f1.id.toString())
		});
		if ('dano' in $) $dano = $.dano.toJSON();
		/** 
		 * Enviamos la foto para que genere la miniatura (y tambien la guarde) y muestre en pantalla 
		 */
		$.widgetFotochica.procesar({
			imagen: evento.foto,
			nueva: 640,
			calidad: 91,
			camara: 'trasera'
		});
		/** 
		 * Limpiamos memoria ram 
		 */
		evento = null;
	} else if (cual_foto == 2) {
		var insertarModelo4_m = Alloy.Collections.numero_unico;
		var insertarModelo4_fila = Alloy.createModel('numero_unico', {
			comentario: 'nuevo recinto foto2'
		});
		insertarModelo4_m.add(insertarModelo4_fila);
		insertarModelo4_fila.save();
		var nuevoid_f2 = require('helper').model2object(insertarModelo4_m.last());
		require('vars')[_var_scopekey]['nuevoid_f2'] = nuevoid_f2;
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var ID_1843822856_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
			if (ID_1843822856_d.exists() == false) ID_1843822856_d.createDirectory();
			var ID_1843822856_f = Ti.Filesystem.getFile(ID_1843822856_d.resolve(), 'cap' + nuevoid_f2.id + '.jpg');
			if (ID_1843822856_f.exists() == true) ID_1843822856_f.deleteFile();
			ID_1843822856_f.write(evento.foto);
			ID_1843822856_d = null;
			ID_1843822856_f = null;
		} else {
			var ID_1829091783_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + 0);
			if (ID_1829091783_d.exists() == false) ID_1829091783_d.createDirectory();
			var ID_1829091783_f = Ti.Filesystem.getFile(ID_1829091783_d.resolve(), 'cap' + nuevoid_f2.id + '.jpg');
			if (ID_1829091783_f.exists() == true) ID_1829091783_f.deleteFile();
			ID_1829091783_f.write(evento.foto);
			ID_1829091783_d = null;
			ID_1829091783_f = null;
		}
		$.dano.set({
			foto2: String.format(L('x1070475521', 'cap%1$s.jpg'), nuevoid_f2.id.toString())
		});
		if ('dano' in $) $dano = $.dano.toJSON();
		$.widgetFotochica2.procesar({
			imagen: evento.foto,
			nueva: 640,
			calidad: 91,
			camara: 'trasera'
		});
		evento = null;
	} else if (cual_foto == 3) {
		var insertarModelo5_m = Alloy.Collections.numero_unico;
		var insertarModelo5_fila = Alloy.createModel('numero_unico', {
			comentario: 'nuevo recinto foto3'
		});
		insertarModelo5_m.add(insertarModelo5_fila);
		insertarModelo5_fila.save();
		var nuevoid_f3 = require('helper').model2object(insertarModelo5_m.last());
		require('vars')[_var_scopekey]['nuevoid_f3'] = nuevoid_f3;
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var ID_142983707_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
			if (ID_142983707_d.exists() == false) ID_142983707_d.createDirectory();
			var ID_142983707_f = Ti.Filesystem.getFile(ID_142983707_d.resolve(), 'cap' + nuevoid_f3.id + '.jpg');
			if (ID_142983707_f.exists() == true) ID_142983707_f.deleteFile();
			ID_142983707_f.write(evento.foto);
			ID_142983707_d = null;
			ID_142983707_f = null;
		} else {
			var ID_1494105746_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + 0);
			if (ID_1494105746_d.exists() == false) ID_1494105746_d.createDirectory();
			var ID_1494105746_f = Ti.Filesystem.getFile(ID_1494105746_d.resolve(), 'cap' + nuevoid_f3.id + '.jpg');
			if (ID_1494105746_f.exists() == true) ID_1494105746_f.deleteFile();
			ID_1494105746_f.write(evento.foto);
			ID_1494105746_d = null;
			ID_1494105746_f = null;
		}
		$.dano.set({
			foto3: String.format(L('x1070475521', 'cap%1$s.jpg'), nuevoid_f3.id.toString())
		});
		if ('dano' in $) $dano = $.dano.toJSON();
		$.widgetFotochica3.procesar({
			imagen: evento.foto,
			nueva: 640,
			calidad: 91,
			camara: 'trasera'
		});
		evento = null;
	}
}

(function() {
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
		/** 
		 * Modificamos la idinspeccion con el idserver de la tarea 
		 */
		$.dano.set({
			id_inspeccion: seltarea.id_server
		});
		if ('dano' in $) $dano = $.dano.toJSON();
	}
	/** 
	 * Recuperamos la variable del id_recinto que tiene danos relacionados y actualizamos el modelo 
	 */
	var temp_idrecinto = ('temp_idrecinto' in require('vars')) ? require('vars')['temp_idrecinto'] : '';
	if ((_.isObject(temp_idrecinto) || (_.isString(temp_idrecinto)) && !_.isEmpty(temp_idrecinto)) || _.isNumber(temp_idrecinto) || _.isBoolean(temp_idrecinto)) {
		$.dano.set({
			id_recinto: temp_idrecinto
		});
		if ('dano' in $) $dano = $.dano.toJSON();
	}
	/** 
	 * heredamos datos previos de nivel en modelo nuevo con binding de esta pantalla 
	 */
	var consultarModelo10_i = Alloy.createCollection('insp_itemdanos');
	var consultarModelo10_i_where = 'id=\'' + args._dato.id + '\'';
	consultarModelo10_i.fetch({
		query: 'SELECT * FROM insp_itemdanos WHERE id=\'' + args._dato.id + '\''
	});
	var insp_n = require('helper').query2array(consultarModelo10_i);
	/** 
	 * Cargamos el modelo con los datos obtenidos desde la consulta 
	 */
	$.dano.set({
		id_inspeccion: insp_n[0].id_inspeccion,
		nombre: insp_n[0].nombre,
		superficie: insp_n[0].superficie,
		foto1: insp_n[0].foto1,
		id_partida: insp_n[0].id_partida,
		foto2: insp_n[0].foto2,
		descripcion_dano: insp_n[0].descripcion_dano,
		foto3: insp_n[0].foto3,
		id_tipodano: insp_n[0].id_tipodano,
		id_unidadmedida: insp_n[0].id_unidadmedida
	});
	if ('dano' in $) $dano = $.dano.toJSON();
	$.SeleccionePartida.setText(insp_n[0].nombre);

	$.SeleccionePartida.setColor('#000000');

	/** 
	 * Cargamos los textos en los campos de texto 
	 */
	$.DescribaelDao.setValue(insp_n[0].descripcion_dano);

	$.campo.setValue(insp_n[0].superficie);

	if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
		/** 
		 * Revisamos si existe variable seltarea para saber si las fotos son desde la inspeccion o son datos dummy 
		 */
		var ID_801567210_trycatch = {
			error: function(e) {
				if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
			}
		};
		try {
			ID_801567210_trycatch.error = function(evento) {};
			/** 
			 * Cambiamos el nombre del archivo para cargar miniaturas. Ya que trae por nombre &quot;cap001&quot; y lo dejamos como &quot;mini001&quot; y asi poder leer las miniaturas 
			 */
			insp_n[0].foto1 = "mini" + insp_n[0].foto1.substring(3);
			var foto_1 = '';
			var ID_1235579640_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + seltarea.id_server);
			if (ID_1235579640_d.exists() == true) {
				var ID_1235579640_f = Ti.Filesystem.getFile(ID_1235579640_d.resolve(), insp_n[0].foto1);
				if (ID_1235579640_f.exists() == true) {
					foto_1 = ID_1235579640_f.read();
				}
				ID_1235579640_f = null;
			}
			ID_1235579640_d = null;
		} catch (e) {
			ID_801567210_trycatch.error(e);
		}
		var ID_1474873156_trycatch = {
			error: function(e) {
				if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
			}
		};
		try {
			ID_1474873156_trycatch.error = function(evento) {};
			insp_n[0].foto2 = "mini" + insp_n[0].foto2.substring(3);
			var foto_2 = '';
			var ID_1749644865_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + seltarea.id_server);
			if (ID_1749644865_d.exists() == true) {
				var ID_1749644865_f = Ti.Filesystem.getFile(ID_1749644865_d.resolve(), insp_n[0].foto2);
				if (ID_1749644865_f.exists() == true) {
					foto_2 = ID_1749644865_f.read();
				}
				ID_1749644865_f = null;
			}
			ID_1749644865_d = null;
		} catch (e) {
			ID_1474873156_trycatch.error(e);
		}
		var ID_1334390975_trycatch = {
			error: function(e) {
				if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
			}
		};
		try {
			ID_1334390975_trycatch.error = function(evento) {
				if (Ti.App.deployType != 'production') console.log('no carga la 3ra foto', {});
			};
			insp_n[0].foto3 = "mini" + insp_n[0].foto3.substring(3);
			var foto_3 = '';
			var ID_61377515_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + seltarea.id_server);
			if (ID_61377515_d.exists() == true) {
				var ID_61377515_f = Ti.Filesystem.getFile(ID_61377515_d.resolve(), insp_n[0].foto3);
				if (ID_61377515_f.exists() == true) {
					foto_3 = ID_61377515_f.read();
				}
				ID_61377515_f = null;
			}
			ID_61377515_d = null;
			if (Ti.App.deployType != 'production') console.log('cargada la 3ra foto', {});
		} catch (e) {
			ID_1334390975_trycatch.error(e);
		}
	} else {
		if (Ti.App.deployType != 'production') console.log('las fotos NO estan en seltarea', {});
		var ID_1434209567_trycatch = {
			error: function(e) {
				if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
			}
		};
		try {
			ID_1434209567_trycatch.error = function(evento) {};
			insp_n[0].foto1 = "mini" + insp_n[0].foto1.substring(3);
			var foto_1 = '';
			var ID_216147007_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + 0);
			if (ID_216147007_d.exists() == true) {
				var ID_216147007_f = Ti.Filesystem.getFile(ID_216147007_d.resolve(), insp_n[0].foto1);
				if (ID_216147007_f.exists() == true) {
					foto_1 = ID_216147007_f.read();
				}
				ID_216147007_f = null;
			}
			ID_216147007_d = null;
		} catch (e) {
			ID_1434209567_trycatch.error(e);
		}
		var ID_1235328145_trycatch = {
			error: function(e) {
				if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
			}
		};
		try {
			ID_1235328145_trycatch.error = function(evento) {};
			insp_n[0].foto2 = "mini" + insp_n[0].foto2.substring(3);
			var foto_2 = '';
			var ID_269412720_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + 0);
			if (ID_269412720_d.exists() == true) {
				var ID_269412720_f = Ti.Filesystem.getFile(ID_269412720_d.resolve(), insp_n[0].foto2);
				if (ID_269412720_f.exists() == true) {
					foto_2 = ID_269412720_f.read();
				}
				ID_269412720_f = null;
			}
			ID_269412720_d = null;
		} catch (e) {
			ID_1235328145_trycatch.error(e);
		}
		var ID_1157833311_trycatch = {
			error: function(e) {
				if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
			}
		};
		try {
			ID_1157833311_trycatch.error = function(evento) {};
			insp_n[0].foto3 = "mini" + insp_n[0].foto3.substring(3);
			var foto_3 = '';
			var ID_1551657778_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + 0);
			if (ID_1551657778_d.exists() == true) {
				var ID_1551657778_f = Ti.Filesystem.getFile(ID_1551657778_d.resolve(), insp_n[0].foto3);
				if (ID_1551657778_f.exists() == true) {
					foto_3 = ID_1551657778_f.read();
				}
				ID_1551657778_f = null;
			}
			ID_1551657778_d = null;
		} catch (e) {
			ID_1157833311_trycatch.error(e);
		}
	}
	if ((_.isObject(foto_1) || (_.isString(foto_1)) && !_.isEmpty(foto_1)) || _.isNumber(foto_1) || _.isBoolean(foto_1)) {
		/** 
		 * Revisamos que foto1, foto2 y foto3 contengan texto, por lo que si es asi, modificamos la imagen previa y ponemos la foto miniatura obtenida desde la memoria 
		 */
		var vista16_visible = true;

		if (vista16_visible == 'si') {
			vista16_visible = true;
		} else if (vista16_visible == 'no') {
			vista16_visible = false;
		}
		$.vista16.setVisible(vista16_visible);

		var imagen_imagen = foto_1;

		if (typeof imagen_imagen == 'string' && 'styles' in require('a4w') && imagen_imagen in require('a4w').styles['images']) {
			imagen_imagen = require('a4w').styles['images'][imagen_imagen];
		}
		$.imagen.setImage(imagen_imagen);

		if ((Ti.Platform.manufacturer) == L('x3791547856_traducir', 'samsung')) {
			/** 
			 * Revisamos si el equipo es samsung, si es asi, rotamos la imagen ya que es la unica marca que saca mal rotadas las imagenes 
			 */
			var vista16_rotar = 90;

			var setRotacion = function(angulo) {
				$.vista16.setTransform(Ti.UI.create2DMatrix().rotate(angulo));
			};
			setRotacion(vista16_rotar);

		}
	}
	if ((_.isObject(foto_2) || (_.isString(foto_2)) && !_.isEmpty(foto_2)) || _.isNumber(foto_2) || _.isBoolean(foto_2)) {
		var vista17_visible = true;

		if (vista17_visible == 'si') {
			vista17_visible = true;
		} else if (vista17_visible == 'no') {
			vista17_visible = false;
		}
		$.vista17.setVisible(vista17_visible);

		var imagen2_imagen = foto_2;

		if (typeof imagen2_imagen == 'string' && 'styles' in require('a4w') && imagen2_imagen in require('a4w').styles['images']) {
			imagen2_imagen = require('a4w').styles['images'][imagen2_imagen];
		}
		$.imagen2.setImage(imagen2_imagen);

		if ((Ti.Platform.manufacturer) == L('x3791547856_traducir', 'samsung')) {
			var vista17_rotar = 90;

			var setRotacion = function(angulo) {
				$.vista17.setTransform(Ti.UI.create2DMatrix().rotate(angulo));
			};
			setRotacion(vista17_rotar);

		}
	}
	if ((_.isObject(foto_3) || (_.isString(foto_3)) && !_.isEmpty(foto_3)) || _.isNumber(foto_3) || _.isBoolean(foto_3)) {
		var vista18_visible = true;

		if (vista18_visible == 'si') {
			vista18_visible = true;
		} else if (vista18_visible == 'no') {
			vista18_visible = false;
		}
		$.vista18.setVisible(vista18_visible);

		var imagen3_imagen = foto_3;

		if (typeof imagen3_imagen == 'string' && 'styles' in require('a4w') && imagen3_imagen in require('a4w').styles['images']) {
			imagen3_imagen = require('a4w').styles['images'][imagen3_imagen];
		}
		$.imagen3.setImage(imagen3_imagen);

		if ((Ti.Platform.manufacturer) == L('x3791547856_traducir', 'samsung')) {
			var vista18_rotar = 90;

			var setRotacion = function(angulo) {
				$.vista18.setTransform(Ti.UI.create2DMatrix().rotate(angulo));
			};
			setRotacion(vista18_rotar);

		}
		if (Ti.App.deployType != 'production') console.log('mostrando la 3ra foto', {});
	}
	/** 
	 * Limpiamos variables 
	 */
	foto_1 = null;
	foto_2 = null;
	foto_3 = null;
	/** 
	 * Desenfocamos todos los campos de texto en 0.2 segundos 
	 */
	var ID_56702640_func = function() {
		$.campo.blur();
		$.DescribaelDao.blur();
	};
	var ID_56702640 = setTimeout(ID_56702640_func, 1000 * 0.2);
	/** 
	 * Fijamos el scroll al inicio 
	 */
	$.scroll.scrollToTop();
	/** 
	 * Modificamos en 0.1 segundos el color del statusbar 
	 */
	var ID_1295290924_func = function() {
		var editardano_statusbar = '#7E6EE0';

		var setearStatusColor = function(editardano_statusbar) {
			if (OS_IOS) {
				if (editardano_statusbar == 'light' || editardano_statusbar == 'claro') {
					$.editardano_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.LIGHT_CONTENT);
				} else if (editardano_statusbar == 'grey' || editardano_statusbar == 'gris' || editardano_statusbar == 'gray') {
					$.editardano_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.GREY);
				} else if (editardano_statusbar == 'oscuro' || editardano_statusbar == 'dark') {
					$.editardano_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.DEFAULT);
				}
			} else if (OS_ANDROID) {
				abx.setStatusbarColor(editardano_statusbar);
			}
		};
		setearStatusColor(editardano_statusbar);

	};
	var ID_1295290924 = setTimeout(ID_1295290924_func, 1000 * 0.1);
	_my_events['resp_dato,ID_1998732988'] = function(evento) {
		if (Ti.App.deployType != 'production') console.log('detalle de la respuesta', {
			"datos": evento
		});
		$.SeleccionePartida.setText(evento.nombre);

		$.SeleccionePartida.setColor('#000000');

		/** 
		 * Asumimos que el valor mostrado seleccionado es el nombre de la fila dano, ya que no existe el campo nombre en esta pantalla. 
		 */
		$.dano.set({
			nombre: evento.nombre,
			id_partida: evento.id_partida,
			id_tipodano: '',
			id_unidadmedida: ''
		});
		if ('dano' in $) $dano = $.dano.toJSON();
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			var consultarModelo11_i = Alloy.createCollection('tipo_dano');
			var consultarModelo11_i_where = 'pais_texto=\'' + pais[0].nombre + '\' AND id_partida=\'' + evento.id_server + '\'';
			consultarModelo11_i.fetch({
				query: 'SELECT * FROM tipo_dano WHERE pais_texto=\'' + pais[0].nombre + '\' AND id_partida=\'' + evento.id_server + '\''
			});
			var danos = require('helper').query2array(consultarModelo11_i);
			/** 
			 * Obtenemos datos para selectores (solo los de este pais y este tipo de partida) 
			 */
			var datos = [];
			_.each(danos, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'valor';
					if (llave == 'id_segured') newkey = 'id_interno';
					if (llave == 'id_server') newkey = 'id_gerardo';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
			$.widgetModal2.data({
				data: datos
			});
			if (Ti.App.deployType != 'production') console.log('termino de llamar', {});
		}
		/** 
		 * Editamos y limpiamos los campos que estan siendo filtrados con este campo 
		 */
		$.widgetModal2.labels({
			seleccione: 'seleccione tipo'
		});
		$.widgetModal.labels({
			seleccione: 'unidad'
		});
		$.campo.setValue('');

		$.label.setText('-');

	};
	Alloy.Events.on('resp_dato', _my_events['resp_dato,ID_1998732988']);
})();

function Postlayout_editardano(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var ID_1294585966_func = function() {
		/** 
		 * Una vez ya cargada la tarea, dejamos la variable limpia para poder abrir otras pantallas 
		 */
		require('vars')['var_abriendo'] = '';
	};
	var ID_1294585966 = setTimeout(ID_1294585966_func, 1000 * 0.2);

}
if (OS_IOS || OS_ANDROID) {
	$.editardano.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}
//$.editardano.open();
