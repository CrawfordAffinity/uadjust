var _bind4section = {};
var _list_templates = {
	"elemento": {
		"vista2": {},
		"Label3": {
			"text": "{nombre}"
		},
		"Label2": {
			"text": "{id_server}"
		},
		"Label": {
			"text": "{id_segured}"
		},
		"vista": {}
	},
	"dano": {
		"Label2": {
			"text": "{id}"
		},
		"vista29": {},
		"Label": {
			"text": "{nombre}"
		}
	},
	"pborrar": {
		"vista5": {},
		"imagen": {},
		"vista3": {},
		"Label4": {
			"text": "{id}"
		},
		"Label3": {
			"text": "{nombre}"
		},
		"vista4": {},
		"vista22": {},
		"vista23": {},
		"vista24": {},
		"imagen2": {}
	},
	"contenido": {
		"vista2": {},
		"Label2": {
			"text": "{id}"
		},
		"Label": {
			"text": "{nombre}"
		}
	},
	"recinto": {
		"vista2": {},
		"Label": {
			"text": "{nombre}"
		},
		"Label2": {
			"text": "{id}"
		}
	},
	"nivel": {
		"Label2": {
			"text": "{id}"
		},
		"Label": {
			"text": "{nombre}"
		},
		"vista21": {}
	},
	"tareas_mistareas": {
		"Label2": {
			"text": "{comuna}"
		},
		"vista4": {},
		"vista6": {},
		"vista2": {},
		"Adistancia": {
			"text": "a {distancia} km"
		},
		"vista8": {},
		"imagen": {},
		"vista10": {},
		"vista12": {
			"visible": "{seguirvisible}"
		},
		"Label": {
			"text": "{direccion}"
		},
		"vista3": {
			"idlocal": "{idlocal}"
		},
		"vista9": {},
		"Label3": {
			"text": "{ciudad}, {pais}"
		},
		"vista11": {},
		"vista5": {},
		"vista7": {}
	},
	"tareas": {
		"vista6": {},
		"Label4": {
			"text": "{direcciontarea}"
		},
		"vista7": {},
		"vista4": {},
		"Label6": {
			"text": "{ciudadtarea}, {paistarea} "
		},
		"vista3": {},
		"vista9": {},
		"Label5": {
			"text": "{comunatarea}"
		},
		"Aubicaciontarea": {
			"text": "a {ubicaciontarea} km"
		},
		"vista8": {},
		"vista5": {},
		"vista10": {},
		"vista2": {
			"myid": "{myid}"
		}
	},
	"criticas": {
		"vista14": {},
		"Label9": {
			"text": "{ciudadcritica}, {paiscritica}"
		},
		"vista13": {},
		"vista17": {},
		"Label7": {
			"text": "{direccioncritica}"
		},
		"vista19": {},
		"vista16": {},
		"Label8": {
			"text": "{comunacritica}"
		},
		"vista15": {},
		"vista18": {},
		"Aubicacioncritica": {
			"text": "a {ubicacioncritica} km"
		},
		"vista11": {
			"myid": "{myid}"
		},
		"vista12": {}
	},
	"tarea": {
		"vista16": {},
		"vista87": {},
		"Label7": {
			"text": "{direccion}"
		},
		"vista98": {},
		"vista93": {},
		"vista94": {},
		"Adistance4": {
			"text": "a {distance} km"
		},
		"vista107": {},
		"vista103": {},
		"vista11": {},
		"Label11": {
			"text": "{comuna}"
		},
		"Label21": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"Adistance7": {
			"text": "a {distance} km"
		},
		"vista39": {},
		"vista42": {},
		"vista56": {},
		"vista48": {},
		"vista20": {},
		"vista104": {},
		"vista49": {},
		"vista66": {},
		"Label30": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"Label5": {
			"text": "{comuna}"
		},
		"vista14": {},
		"vista65": {},
		"vista62": {},
		"Label14": {
			"text": "{comuna}"
		},
		"vista73": {
			"backgroundColor": "{bgcolor}"
		},
		"vista81": {
			"visible": "{vis_tipo9}"
		},
		"vista84": {},
		"Label23": {
			"text": "{comuna}"
		},
		"Label13": {
			"text": "{direccion}"
		},
		"vista91": {
			"backgroundColor": "{bgcolor}"
		},
		"Label33": {
			"text": "{comuna}"
		},
		"vista58": {},
		"Label8": {
			"text": "{comuna}"
		},
		"vista86": {},
		"vista69": {},
		"vista54": {
			"visible": "{vis_tipo6}"
		},
		"Label28": {
			"text": "{direccion}"
		},
		"vista77": {},
		"vista35": {},
		"Label3": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"vista38": {},
		"Label16": {
			"text": "{direccion}"
		},
		"vista26": {},
		"vista50": {},
		"Adistance6": {
			"text": "a {distance} km"
		},
		"Adistance11": {
			"text": "a {distance} km"
		},
		"vista28": {
			"backgroundColor": "{bgcolor}"
		},
		"vista80": {},
		"vista74": {},
		"Label34": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"Label35": {
			"text": "{id}"
		},
		"Label": {
			"text": "{direccion}"
		},
		"vista61": {},
		"vista83": {},
		"vista36": {
			"visible": "{vis_tipo4}"
		},
		"vista88": {},
		"vista68": {},
		"Adistance": {
			"text": "a {distance} km"
		},
		"vista34": {},
		"vista17": {},
		"vista71": {},
		"Label10": {
			"text": "{direccion}"
		},
		"Label12": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"vista97": {},
		"vista23": {},
		"Label24": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"vista10": {
			"backgroundColor": "{bgcolor}"
		},
		"vista53": {},
		"vista29": {},
		"vista100": {
			"backgroundColor": "{bgcolor}"
		},
		"vista85": {},
		"vista31": {},
		"vista79": {},
		"vista46": {
			"backgroundColor": "{bgcolor}"
		},
		"Label9": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"vista64": {
			"backgroundColor": "{bgcolor}"
		},
		"Label20": {
			"text": "{comuna}"
		},
		"Label29": {
			"text": "{comuna}"
		},
		"Label18": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"vista30": {},
		"vista108": {
			"visible": "{seguir}"
		},
		"vista44": {},
		"vista106": {},
		"Label27": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"vista47": {},
		"Label22": {
			"text": "{direccion}"
		},
		"vista40": {},
		"Label26": {
			"text": "{comuna}"
		},
		"Adistance9": {
			"text": "a {distance} km"
		},
		"vista59": {},
		"vista22": {},
		"vista8": {},
		"vista25": {},
		"vista67": {},
		"Label2": {
			"text": "{comuna}"
		},
		"vista90": {
			"visible": "{vis_tipo10}"
		},
		"vista24": {},
		"vista89": {},
		"vista41": {},
		"vista12": {},
		"vista75": {},
		"vista43": {},
		"vista95": {},
		"vista57": {},
		"vista92": {},
		"vista76": {},
		"vista72": {
			"visible": "{vis_tipo8}"
		},
		"Label15": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"vista96": {},
		"vista82": {
			"backgroundColor": "{bgcolor}"
		},
		"vista37": {
			"backgroundColor": "{bgcolor}"
		},
		"Label31": {
			"text": "{prioridad_tiempo}"
		},
		"vista99": {
			"visible": "{vis_otro}"
		},
		"vista13": {},
		"Label17": {
			"text": "{comuna}"
		},
		"vista15": {},
		"vista101": {},
		"vista52": {},
		"vista45": {
			"visible": "{vis_tipo5}"
		},
		"Adistance8": {
			"text": "a {distance} km"
		},
		"imagen3": {},
		"vista60": {},
		"vista19": {
			"backgroundColor": "{bgcolor}"
		},
		"Label4": {
			"text": "{direccion}"
		},
		"vista102": {},
		"vista32": {},
		"vista18": {
			"visible": "{vis_tipo2}"
		},
		"Label6": {
			"text": "{nivel_2}, {pais_texto}"
		},
		"vista105": {},
		"Adistance2": {
			"text": "a {distance} km"
		},
		"vista33": {},
		"vista70": {},
		"Adistance3": {
			"text": "a {distance} km"
		},
		"vista63": {
			"visible": "{vis_tipo7}"
		},
		"vista51": {},
		"Label25": {
			"text": "{direccion}"
		},
		"vista78": {},
		"vista55": {
			"backgroundColor": "{bgcolor}"
		},
		"Adistance5": {
			"text": "a {distance} km"
		},
		"vista9": {
			"visible": "{vis_tipo1}"
		},
		"Adistance10": {
			"text": "a {distance} km"
		},
		"vista21": {},
		"Label19": {
			"text": "{direccion}"
		},
		"Label32": {
			"text": "{direccion}"
		},
		"vista27": {
			"visible": "{vis_tipo3}"
		}
	},
	"tarea_historia": {
		"Label3": {
			"text": "{comuna}"
		},
		"vista4": {},
		"vista6": {},
		"vista2": {},
		"Label2": {
			"text": "{hora_termino}"
		},
		"vista8": {},
		"vista13": {
			"visible": "{bt_enviartarea}"
		},
		"vista10": {},
		"Label": {
			"text": "{direccion}"
		},
		"vista3": {
			"idlocal": "{id}",
			"estado": "{estado_tarea}"
		},
		"vista9": {},
		"vista12": {},
		"Label4": {
			"text": "{ciudad}, {pais}"
		},
		"vista14": {
			"visible": "{enviando_tarea}"
		},
		"vista11": {},
		"vista5": {},
		"ENVIAR": {},
		"imagen": {},
		"vista7": {}
	}
};
Alloy.Globals["menu"] = $.menu;
var _activity;
if (OS_ANDROID) {
	_activity = $.menu.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	args = arguments[0] || {};

function Click_tiMenuItem(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	var gps_error = ('gps_error' in require('vars')) ? require('vars')['gps_error'] : '';
	if (gps_error == true || gps_error == 'true') {
		var preguntarAlerta_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta = Ti.UI.createAlertDialog({
			title: L('x3661800039_traducir', 'Error geolocalizando'),
			message: L('x942436043_traducir', 'Ha ocurrido un error al geolocalizarlo'),
			buttonNames: preguntarAlerta_opts
		});
		preguntarAlerta.addEventListener('click', function(e) {
			var errori = preguntarAlerta_opts[e.index];
			errori = null;

			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta.show();
	} else {
		var bt_refrescando = ('bt_refrescando' in require('vars')) ? require('vars')['bt_refrescando'] : '';
		var obtenermistareas = ('obtenermistareas' in require('vars')) ? require('vars')['obtenermistareas'] : '';
		var obtenerentrantes = ('obtenerentrantes' in require('vars')) ? require('vars')['obtenerentrantes'] : '';
		var obteneremergencias = ('obteneremergencias' in require('vars')) ? require('vars')['obteneremergencias'] : '';
		if (obtenermistareas == true || obtenermistareas == 'true') {
			if (Ti.App.deployType != 'production') console.log('obteniendo aun mistareas', {});
		} else if (obtenerentrantes == true || obtenerentrantes == 'true') {
			if (Ti.App.deployType != 'production') console.log('obteniendo aun entrantes', {});
		} else if (obteneremergencias == true || obteneremergencias == 'true') {
			if (Ti.App.deployType != 'production') console.log('obteniendo aun emergencias', {});
		} else {
			require('vars')['obtenermistareas'] = L('x4261170317', 'true');
			require('vars')['obtenerentrantes'] = L('x4261170317', 'true');
			var gps_latitud = ('gps_latitud' in require('vars')) ? require('vars')['gps_latitud'] : '';
			var gps_longitud = ('gps_longitud' in require('vars')) ? require('vars')['gps_longitud'] : '';
			var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
			if (Ti.App.deployType != 'production') console.log('Refrescando desde el menu', {});
			var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
			var consultarURL = {};

			consultarURL.success = function(e) {
				var elemento = e,
					valor = e;
				if (elemento.error == 0 || elemento.error == '0') {
					var eliminarModelo_i = Alloy.Collections.tareas;
					var sql = "DELETE FROM " + eliminarModelo_i.config.adapter.collection_name;
					var db = Ti.Database.open(eliminarModelo_i.config.adapter.db_name);
					db.execute(sql);
					db.close();
					eliminarModelo_i.trigger('remove');
					/** 
					 * guardamos elemento.mistareas en var local, para ejecutar desfasada la insercion a la base de datos. 
					 */
					require('vars')['mistareas'] = elemento.mistareas;
					var ID_1089888790_func = function() {};
					var ID_1089888790 = setTimeout(ID_1089888790_func, 1000 * 0.1);
					var elemento_mistareas = elemento.mistareas;
					var insertarModelo_m = Alloy.Collections.tareas;
					var db_insertarModelo = Ti.Database.open(insertarModelo_m.config.adapter.db_name);
					db_insertarModelo.execute('BEGIN');
					_.each(elemento_mistareas, function(insertarModelo_fila, pos) {
						db_insertarModelo.execute('INSERT INTO tareas (fecha_tarea, id_inspeccion, id_asegurado, nivel_2, comentario_can_o_rech, asegurado_tel_fijo, estado_tarea, bono, evento, id_inspector, asegurado_codigo_identificador, lat, nivel_1, asegurado_nombre, pais, direccion, asegurador, fecha_ingreso, fecha_siniestro, nivel_1_texto, distance, nivel_4, perfil, asegurado_id, pais_texto, id_server, categoria, nivel_3, asegurado_correo, num_caso, lon, asegurado_tel_movil, nivel_5, tipo_tarea) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', insertarModelo_fila.fecha_tarea, insertarModelo_fila.id_inspeccion, insertarModelo_fila.id_asegurado, insertarModelo_fila.nivel_2, insertarModelo_fila.comentario_can_o_rech, insertarModelo_fila.asegurado_tel_fijo, insertarModelo_fila.estado_tarea, insertarModelo_fila.bono, insertarModelo_fila.evento, insertarModelo_fila.id_inspector, insertarModelo_fila.asegurado_codigo_identificador, insertarModelo_fila.lat, insertarModelo_fila.nivel_1, insertarModelo_fila.asegurado_nombre, insertarModelo_fila.pais, insertarModelo_fila.direccion, insertarModelo_fila.asegurador, insertarModelo_fila.fecha_ingreso, insertarModelo_fila.fecha_siniestro, insertarModelo_fila.nivel_1_, insertarModelo_fila.distancia, insertarModelo_fila.nivel_4, 'ubicacion', insertarModelo_fila.asegurado_id, insertarModelo_fila.pais, insertarModelo_fila.id, insertarModelo_fila.categoria, insertarModelo_fila.nivel_3, insertarModelo_fila.asegurado_correo, insertarModelo_fila.num_caso, insertarModelo_fila.lon, insertarModelo_fila.asegurado_tel_movil, insertarModelo_fila.nivel_5, insertarModelo_fila.tipo_tarea);
					});
					db_insertarModelo.execute('COMMIT');
					db_insertarModelo.close();
					db_insertarModelo = null;
					insertarModelo_m.trigger('change');
					if (Ti.App.deployType != 'production') console.log('cambiando obtenermistareas a false', {});
					require('vars')['obtenermistareas'] = L('x734881840_traducir', 'false');

					var ID_1241004686_trycatch = {
						error: function(e) {
							if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
						}
					};
					try {
						ID_1241004686_trycatch.error = function(evento) {};

						Alloy.Events.trigger('_refrescar_tareas_mistareas');

						Alloy.Events.trigger('_calcular_ruta');
					} catch (e) {
						ID_1241004686_trycatch.error(e);
					}
				}
				elemento = null, valor = null;
			};
			require('helper').ajaxUnico('consultarURL', '' + String.format(L('x2334609226', '%1$sobtenerMisTareas'), url_server.toString()) + '', 'POST', {
				id_inspector: inspector.id_server,
				lat: gps_latitud,
				lon: gps_longitud
			}, 15000, consultarURL);
			var consultarURL2 = {};

			consultarURL2.success = function(e) {
				var elemento = e,
					valor = e;
				if (elemento.error != 0 && elemento.error != '0') {
					var preguntarAlerta2_opts = [L('x1518866076_traducir', 'Aceptar')];
					var preguntarAlerta2 = Ti.UI.createAlertDialog({
						title: L('x57652245_traducir', 'Alerta'),
						message: L('x344160560_traducir', 'Error con el servidor'),
						buttonNames: preguntarAlerta2_opts
					});
					preguntarAlerta2.addEventListener('click', function(e) {
						var errori = preguntarAlerta2_opts[e.index];
						errori = null;

						e.source.removeEventListener("click", arguments.callee);
					});
					preguntarAlerta2.show();
				} else {
					var eliminarModelo2_i = Alloy.Collections.tareas_entrantes;
					var sql = "DELETE FROM " + eliminarModelo2_i.config.adapter.collection_name;
					var db = Ti.Database.open(eliminarModelo2_i.config.adapter.db_name);
					db.execute(sql);
					db.close();
					eliminarModelo2_i.trigger('remove');
					var elemento_critica = elemento.critica;
					var insertarModelo2_m = Alloy.Collections.tareas_entrantes;
					var db_insertarModelo2 = Ti.Database.open(insertarModelo2_m.config.adapter.db_name);
					db_insertarModelo2.execute('BEGIN');
					_.each(elemento_critica, function(insertarModelo2_fila, pos) {
						db_insertarModelo2.execute('INSERT INTO tareas_entrantes (fecha_tarea, id_inspeccion, id_asegurado, nivel_2, comentario_can_o_rech, asegurado_tel_fijo, estado_tarea, bono, evento, id_inspector, asegurado_codigo_identificador, lat, nivel_1, asegurado_nombre, pais, direccion, asegurador, fecha_ingreso, fecha_siniestro, nivel_1_texto, distance, nivel_4, perfil, asegurado_id, pais_texto, id_server, categoria, nivel_3, asegurado_correo, num_caso, lon, asegurado_tel_movil, tipo_tarea, nivel_5) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', insertarModelo2_fila.fecha_tarea, insertarModelo2_fila.id_inspeccion, insertarModelo2_fila.id_asegurado, insertarModelo2_fila.nivel_2, '-', insertarModelo2_fila.asegurado_tel_fijo, insertarModelo2_fila.estado_tarea, insertarModelo2_fila.bono, insertarModelo2_fila.evento, insertarModelo2_fila.id_inspector, insertarModelo2_fila.asegurado_codigo_identificador, insertarModelo2_fila.lat, insertarModelo2_fila.nivel_1, insertarModelo2_fila.asegurado_nombre, -1, insertarModelo2_fila.direccion, insertarModelo2_fila.asegurador, insertarModelo2_fila.fecha_ingreso, insertarModelo2_fila.fecha_siniestro, insertarModelo2_fila.nivel_1_, insertarModelo2_fila.distancia, insertarModelo2_fila.nivel_4, 'casa', insertarModelo2_fila.asegurado_id, insertarModelo2_fila.pais_, insertarModelo2_fila.id, insertarModelo2_fila.categoria, insertarModelo2_fila.nivel_3, insertarModelo2_fila.asegurado_correo, insertarModelo2_fila.num_caso, insertarModelo2_fila.lon, insertarModelo2_fila.asegurado_tel_movil, insertarModelo2_fila.tipo_tarea, insertarModelo2_fila.nivel_5);
					});
					db_insertarModelo2.execute('COMMIT');
					db_insertarModelo2.close();
					db_insertarModelo2 = null;
					insertarModelo2_m.trigger('change');
					var elemento_normal = elemento.normal;
					var insertarModelo3_m = Alloy.Collections.tareas_entrantes;
					var db_insertarModelo3 = Ti.Database.open(insertarModelo3_m.config.adapter.db_name);
					db_insertarModelo3.execute('BEGIN');
					_.each(elemento_normal, function(insertarModelo3_fila, pos) {
						db_insertarModelo3.execute('INSERT INTO tareas_entrantes (fecha_tarea, id_inspeccion, id_asegurado, nivel_2, comentario_can_o_rech, asegurado_tel_fijo, estado_tarea, bono, evento, id_inspector, asegurado_codigo_identificador, lat, nivel_1, asegurado_nombre, pais, direccion, asegurador, fecha_ingreso, fecha_siniestro, nivel_1_texto, distance, nivel_4, perfil, asegurado_id, pais_texto, id_server, categoria, nivel_3, asegurado_correo, num_caso, lon, asegurado_tel_movil, tipo_tarea, nivel_5) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', insertarModelo3_fila.fecha_tarea, insertarModelo3_fila.id_inspeccion, insertarModelo3_fila.id_asegurado, insertarModelo3_fila.nivel_2, '-', insertarModelo3_fila.asegurado_tel_fijo, insertarModelo3_fila.estado_tarea, insertarModelo3_fila.bono, insertarModelo3_fila.evento, insertarModelo3_fila.id_inspector, insertarModelo3_fila.asegurado_codigo_identificador, insertarModelo3_fila.lat, insertarModelo3_fila.nivel_1, insertarModelo3_fila.asegurado_nombre, -1, insertarModelo3_fila.direccion, insertarModelo3_fila.asegurador, insertarModelo3_fila.fecha_ingreso, insertarModelo3_fila.fecha_siniestro, insertarModelo3_fila.nivel_1_, insertarModelo3_fila.distancia, insertarModelo3_fila.nivel_4, 'casa', insertarModelo3_fila.asegurado_id, insertarModelo3_fila.pais_, insertarModelo3_fila.id, insertarModelo3_fila.categoria, insertarModelo3_fila.nivel_3, insertarModelo3_fila.asegurado_correo, insertarModelo3_fila.num_caso, insertarModelo3_fila.lon, insertarModelo3_fila.asegurado_tel_movil, insertarModelo3_fila.tipo_tarea, insertarModelo3_fila.nivel_5);
					});
					db_insertarModelo3.execute('COMMIT');
					db_insertarModelo3.close();
					db_insertarModelo3 = null;
					insertarModelo3_m.trigger('change');
					require('vars')['obtenerentrantes'] = L('x734881840_traducir', 'false');
					if (Ti.App.deployType != 'production') console.log('cambiando obtenerentrantes a false', {});

					Alloy.Events.trigger('_refrescar_tareas_entrantes');
				}
				elemento = null, valor = null;
			};

			consultarURL2.error = function(e) {
				var elemento = e,
					valor = e;
				var preguntarAlerta3_opts = [L('x1518866076_traducir', 'Aceptar')];
				var preguntarAlerta3 = Ti.UI.createAlertDialog({
					title: L('x57652245_traducir', 'Alerta'),
					message: L('x3389756565_traducir', 'No se pudo conectar con el servidor'),
					buttonNames: preguntarAlerta3_opts
				});
				preguntarAlerta3.addEventListener('click', function(e) {
					var errori = preguntarAlerta3_opts[e.index];
					errori = null;

					e.source.removeEventListener("click", arguments.callee);
				});
				preguntarAlerta3.show();
				elemento = null, valor = null;
			};
			require('helper').ajaxUnico('consultarURL2', '' + String.format(L('x1654875529', '%1$sobtenerTareasEntrantes'), url_server.toString()) + '', 'POST', {
				id_inspector: inspector.id_server,
				lat: gps_latitud,
				lon: gps_longitud
			}, 15000, consultarURL2);
			var consultarURL3 = {};

			consultarURL3.success = function(e) {
				var elemento = e,
					valor = e;
				var eliminarModelo3_i = Alloy.Collections.emergencia;
				var sql = "DELETE FROM " + eliminarModelo3_i.config.adapter.collection_name;
				var db = Ti.Database.open(eliminarModelo3_i.config.adapter.db_name);
				db.execute(sql);
				db.close();
				eliminarModelo3_i.trigger('remove');
				if (elemento.error != 0 && elemento.error != '0') {
					var preguntarAlerta4_opts = [L('x1518866076_traducir', 'Aceptar')];
					var preguntarAlerta4 = Ti.UI.createAlertDialog({
						title: L('x57652245_traducir', 'Alerta'),
						message: L('x344160560_traducir', 'Error con el servidor'),
						buttonNames: preguntarAlerta4_opts
					});
					preguntarAlerta4.addEventListener('click', function(e) {
						var errori = preguntarAlerta4_opts[e.index];
						errori = null;

						e.source.removeEventListener("click", arguments.callee);
					});
					preguntarAlerta4.show();
				} else {
					var elemento_perfil = elemento.perfil;
					var insertarModelo4_m = Alloy.Collections.emergencia;
					var db_insertarModelo4 = Ti.Database.open(insertarModelo4_m.config.adapter.db_name);
					db_insertarModelo4.execute('BEGIN');
					_.each(elemento_perfil, function(insertarModelo4_fila, pos) {
						db_insertarModelo4.execute('INSERT INTO emergencia (fecha_tarea, id_inspeccion, nivel_2, id_asegurado, comentario_can_o_rech, asegurado_tel_fijo, estado_tarea, bono, id_inspector, asegurado_codigo_identificador, lat, nivel_1, asegurado_nombre, pais, direccion, asegurador, fecha_ingreso, fecha_siniestro, nivel_1_texto, distance, nivel_4, perfil, asegurado_id, pais_texto, id_server, categoria, nivel_3, asegurado_correo, num_caso, lon, asegurado_tel_movil, distancia_2, nivel_5, tipo_tarea) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', insertarModelo4_fila.fecha_tarea, insertarModelo4_fila.id_inspeccion, insertarModelo4_fila.nivel_2, insertarModelo4_fila.id_asegurado, insertarModelo4_fila.comentario_can_o_rech, insertarModelo4_fila.asegurado_tel_fijo, insertarModelo4_fila.estado_tarea, insertarModelo4_fila.bono, insertarModelo4_fila.id_inspector, insertarModelo4_fila.asegurado_codigo_identificador, insertarModelo4_fila.lat, insertarModelo4_fila.nivel_1, insertarModelo4_fila.asegurado_nombre, insertarModelo4_fila.pais, insertarModelo4_fila.direccion, insertarModelo4_fila.asegurador, insertarModelo4_fila.fecha_ingreso, insertarModelo4_fila.fecha_siniestro, insertarModelo4_fila.nivel_1_google, insertarModelo4_fila.distancia, insertarModelo4_fila.nivel_4, 'casa', insertarModelo4_fila.asegurado_id, insertarModelo4_fila.pais_, insertarModelo4_fila.id, insertarModelo4_fila.categoria, insertarModelo4_fila.nivel_3, insertarModelo4_fila.asegurado_correo, insertarModelo4_fila.num_caso, insertarModelo4_fila.lon, insertarModelo4_fila.asegurado_tel_movil, insertarModelo4_fila.distancia_2, insertarModelo4_fila.nivel_5, insertarModelo4_fila.tipo_tarea);
					});
					db_insertarModelo4.execute('COMMIT');
					db_insertarModelo4.close();
					db_insertarModelo4 = null;
					insertarModelo4_m.trigger('change');
					var elemento_ubicacion = elemento.ubicacion;
					var insertarModelo5_m = Alloy.Collections.emergencia;
					var db_insertarModelo5 = Ti.Database.open(insertarModelo5_m.config.adapter.db_name);
					db_insertarModelo5.execute('BEGIN');
					_.each(elemento_ubicacion, function(insertarModelo5_fila, pos) {
						db_insertarModelo5.execute('INSERT INTO emergencia (fecha_tarea, id_inspeccion, id_asegurado, nivel_2, comentario_can_o_rech, asegurado_tel_fijo, estado_tarea, bono, id_inspector, asegurado_codigo_identificador, lat, nivel_1, asegurado_nombre, pais, direccion, asegurador, fecha_ingreso, fecha_siniestro, nivel_1_texto, distance, nivel_4, perfil, asegurado_id, pais_texto, id_server, categoria, nivel_3, asegurado_correo, num_caso, lon, asegurado_tel_movil, distancia_2, tipo_tarea, nivel_5) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', insertarModelo5_fila.fecha_tarea, insertarModelo5_fila.id_inspeccion, insertarModelo5_fila.id_asegurado, insertarModelo5_fila.nivel_2, insertarModelo5_fila.comentario_can_o_rech, insertarModelo5_fila.asegurado_tel_fijo, insertarModelo5_fila.estado_tarea, insertarModelo5_fila.bono, insertarModelo5_fila.id_inspector, insertarModelo5_fila.asegurado_codigo_identificador, insertarModelo5_fila.lat, insertarModelo5_fila.nivel_1, insertarModelo5_fila.asegurado_nombre, insertarModelo5_fila.pais, insertarModelo5_fila.direccion, insertarModelo5_fila.asegurador, insertarModelo5_fila.fecha_ingreso, insertarModelo5_fila.fecha_siniestro, insertarModelo5_fila.nivel_1_google, insertarModelo5_fila.distancia, insertarModelo5_fila.nivel_4, 'ubicacion', insertarModelo5_fila.asegurado_id, insertarModelo5_fila.pais_, insertarModelo5_fila.id, insertarModelo5_fila.categoria, insertarModelo5_fila.nivel_3, insertarModelo5_fila.asegurado_correo, insertarModelo5_fila.num_caso, insertarModelo5_fila.lon, insertarModelo5_fila.asegurado_tel_movil, insertarModelo5_fila.distancia_2, insertarModelo5_fila.tipo_tarea, insertarModelo5_fila.nivel_5);
					});
					db_insertarModelo5.execute('COMMIT');
					db_insertarModelo5.close();
					db_insertarModelo5 = null;
					insertarModelo5_m.trigger('change');
					require('vars')['obteneremergencias'] = L('x734881840_traducir', 'false');
					if (Ti.App.deployType != 'production') console.log('cambiando obteneremergencias a false', {});

					Alloy.Events.trigger('_refrescaremergencias');
				}
				elemento = null, valor = null;
			};

			consultarURL3.error = function(e) {
				var elemento = e,
					valor = e;
				if (Ti.App.deployType != 'production') console.log('Hubo un error', {});
				var preguntarAlerta5_opts = [L('x1518866076_traducir', 'Aceptar')];
				var preguntarAlerta5 = Ti.UI.createAlertDialog({
					title: L('x57652245_traducir', 'Alerta'),
					message: L('x3389756565_traducir', 'No se pudo conectar con el servidor'),
					buttonNames: preguntarAlerta5_opts
				});
				preguntarAlerta5.addEventListener('click', function(e) {
					var errori = preguntarAlerta5_opts[e.index];
					errori = null;

					e.source.removeEventListener("click", arguments.callee);
				});
				preguntarAlerta5.show();
				elemento = null, valor = null;
			};
			require('helper').ajaxUnico('consultarURL3', '' + String.format(L('x1578679327', '%1$sobtenerEmergencias'), url_server.toString()) + '', 'POST', {
				id_inspector: inspector.id_server,
				lat: gps_latitud,
				lon: gps_longitud
			}, 15000, consultarURL3);
		}
	}
	source = null, elemento = null;

}

var _activity;
if (OS_ANDROID) {
	_activity = $.Label3.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
if (OS_ANDROID) {
	$.Label3.addEventListener('open', function(e) {});
}


$.widgetSininternet.init({
	titulo: L('x2828751865_traducir', '¡ESTAS SIN CONEXION!'),
	__id: 'ALL374340032',
	mensaje: L('x3528440528_traducir', 'No puedes ver las tareas entrantes'),
	onon: on_widgetSininternet,
	onoff: Off_widgetSininternet
});

function on_widgetSininternet(e) {

	var evento = e;
	var vista_visible = true;

	if (vista_visible == 'si') {
		vista_visible = true;
	} else if (vista_visible == 'no') {
		vista_visible = false;
	}
	$.vista.setVisible(vista_visible);


	Alloy.Events.trigger('_refrescar_tareas_entrantes');

}

function Off_widgetSininternet(e) {

	var evento = e;
	var vista_visible = false;

	if (vista_visible == 'si') {
		vista_visible = true;
	} else if (vista_visible == 'no') {
		vista_visible = false;
	}
	$.vista.setVisible(vista_visible);


}

function Itemclick_listado(e) {

	e.cancelBubble = true;
	var objeto = e.section.getItemAt(e.itemIndex);
	var modelo = {},
		_modelo = [];
	var fila = {},
		fila_bak = {},
		info = {
			_template: objeto.template,
			_what: [],
			_seccion_ref: e.section.getHeaderTitle(),
			_model_id: -1
		},
		_tmp = {
			objmap: {}
		};
	if ('itemId' in e) {
		info._model_id = e.itemId;
		modelo._id = info._model_id;
		if (info._seccion_ref != '' && info._seccion_ref in _bind4section) {
			modelo._collection = _bind4section[info._seccion_ref];
			_tmp._coll = modelo._collection;
		}
	}
	if (info._seccion_ref != '' && info._seccion_ref in _bind4section) {
		if (Alloy.Collections[_tmp._coll].config.adapter.type == 'sql') {
			_tmp._inst = Alloy.Collections[_tmp._coll];
			_tmp._id = Alloy.Collections[_tmp._coll].config.adapter.idAttribute;
			_tmp._dbsql = 'SELECT * FROM ' + Alloy.Collections[_tmp._coll].config.adapter.collection_name + ' WHERE ' + _tmp._id + ' = ' + e.itemId;
			_tmp._db = Ti.Database.open(Alloy.Collections[_tmp._coll].config.adapter.db_name);
			_modelo = _tmp._db.execute(_tmp._dbsql);
			var values = [],
				fieldNames = [];
			var fieldCount = _.isFunction(_modelo.fieldCount) ? _modelo.fieldCount() : _modelo.fieldCount;
			var getField = _modelo.field;
			var i = 0;
			for (; fieldCount > i; i++) fieldNames.push(_modelo.fieldName(i));
			while (_modelo.isValidRow()) {
				var o = {};
				for (i = 0; fieldCount > i; i++) o[fieldNames[i]] = getField(i);
				values.push(o);
				_modelo.next();
			}
			_modelo = values;
			_tmp._db.close();
		} else {
			_tmp._search = {};
			_tmp._search[_tmp._id] = e.itemId + '';
			_modelo = Alloy.Collections[_tmp._coll].fetch(_tmp._search);
		}
	}
	var findVariables = require('fvariables');
	_.each(_list_templates[info._template], function(obj_id, id) {
		_.each(obj_id, function(valor, prop) {
			var llaves = findVariables(valor, '{', '}');
			_.each(llaves, function(llave) {
				_tmp.objmap[llave] = {
					id: id,
					prop: prop
				};
				fila[llave] = objeto[id][prop];
				if (id == e.bindId) info._what.push(llave);
			});
		});
	});
	info._what = info._what.join(',');
	fila_bak = JSON.parse(JSON.stringify(fila));
	/** 
	 * Creamos un flag para evitar que la pantalla se abra dos veces 
	 */
	var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
	if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
		require('vars')['var_abriendo'] = L('x4261170317', 'true');
		if ("menu" in Alloy.Globals) {
			Alloy.Globals["menu"].activeTab.open(Alloy.createController("tomartarea_index", {
				'_objeto': fila,
				'_id': fila.myid,
				'_tipo': 'entrantes',
				'__master_model': (typeof modelo !== 'undefined') ? modelo : {},
				'__modelo': (typeof _modelo !== 'undefined' && _modelo.length > 0) ? _modelo[0] : {}
			}).getView());
		} else {
			Alloy.Globals["menu"] = $.menu;
			Alloy.Globals["menu"].activeTab.open(Alloy.createController("tomartarea_index", {
				'_objeto': fila,
				'_id': fila.myid,
				'_tipo': 'entrantes',
				'__master_model': (typeof modelo !== 'undefined') ? modelo : {},
				'__modelo': (typeof _modelo !== 'undefined' && _modelo.length > 0) ? _modelo[0] : {}
			}).getView());
		}

	}
	_tmp.changed = false;
	_tmp.diff_keys = [];
	_.each(fila, function(value1, prop) {
		var had_samekey = false;
		_.each(fila_bak, function(value2, prop2) {
			if (prop == prop2 && value1 == value2) {
				had_samekey = true;
			} else if (!_.has(fila_bak, prop) || !_.has(fila, prop2)) {
				has_samekey = true;
			}
		});
		if (!had_samekey) _tmp.diff_keys.push(prop);
	});
	if (_tmp.diff_keys.length > 0) _tmp.changed = true;
	if (_tmp.changed == true) {
		_.each(_tmp.diff_keys, function(llave) {
			objeto[_tmp.objmap[llave].id][_tmp.objmap[llave].prop] = fila[llave];
		});
		e.section.updateItemAt(e.itemIndex, objeto);
	}

}


_my_events['_refrescar_tareas_entrantes,ID_1636319035'] = function(evento) {
	/** 
	 * Limpiamos el listado para ingresar las tareas refrescadas 
	 */
	var listado_borrar = false;

	var limpiarListado = function(animar) {
		var a_nimar = (typeof animar == 'undefined') ? false : animar;
		if (OS_IOS && a_nimar == true) {
			var s_ecciones = $.listado.getSections();
			_.each(s_ecciones, function(obj_id, pos) {
				$.listado.deleteSectionAt(0, {
					animated: true
				});
			});
		} else {
			$.listado.setSections([]);
		}
	};
	limpiarListado(listado_borrar);

	/** 
	 * Creamos las distintas secciones y header para indicar la fecha de la tarea 
	 */
	var seccionListadoHoli = Titanium.UI.createListSection({
		headerTitle: L('x2819214932_traducir', 'holi1')
	});
	var headerListado = Titanium.UI.createView({
		height: '25dp',
		width: Ti.UI.FILL
	});
	var vista20 = Titanium.UI.createView({
		height: '25dp',
		layout: 'composite',
		width: Ti.UI.FILL,
		backgroundColor: '#FFDCDC'
	});
	var CRITICAS = Titanium.UI.createLabel({
		text: L('x478055026_traducir', 'CRITICAS'),
		color: '#EE7F7E',
		touchEnabled: false,
		font: {
			fontFamily: 'Roboto-Medium',
			fontSize: '15dp'
		}

	});
	vista20.add(CRITICAS);

	headerListado.add(vista20);
	seccionListadoHoli.setHeaderView(headerListado);
	$.listado.appendSection(seccionListadoHoli);
	var seccionListadoHoyentrantes = Titanium.UI.createListSection({
		headerTitle: L('x3166667470', 'hoy_entrantes')
	});
	var headerListado2 = Titanium.UI.createView({
		height: '25dp',
		width: Ti.UI.FILL
	});
	var vista21 = Titanium.UI.createView({
		height: '25dp',
		layout: 'composite',
		width: Ti.UI.FILL,
		backgroundColor: '#F7F7F7'
	});
	var PARAHOY = Titanium.UI.createLabel({
		text: L('x1296057275_traducir', 'PARA HOY'),
		color: '#999999',
		touchEnabled: false,
		font: {
			fontFamily: 'Roboto-Medium',
			fontSize: '14dp'
		}

	});
	vista21.add(PARAHOY);

	headerListado2.add(vista21);
	seccionListadoHoyentrantes.setHeaderView(headerListado2);
	$.listado.appendSection(seccionListadoHoyentrantes);
	var seccionListadoMananaentrantes = Titanium.UI.createListSection({
		headerTitle: L('x3690817388', 'manana_entrantes')
	});
	var headerListado3 = Titanium.UI.createView({
		height: '25dp',
		width: Ti.UI.FILL
	});
	var vista22 = Titanium.UI.createView({
		height: '25dp',
		layout: 'composite',
		width: Ti.UI.FILL,
		backgroundColor: '#F7F7F7'
	});
	var MAANA = Titanium.UI.createLabel({
		text: L('x3380933310_traducir', 'MAÑANA'),
		color: '#999999',
		touchEnabled: false,
		font: {
			fontFamily: 'Roboto-Medium',
			fontSize: '14dp'
		}

	});
	vista22.add(MAANA);

	headerListado3.add(vista22);
	seccionListadoMananaentrantes.setHeaderView(headerListado3);
	$.listado.appendSection(seccionListadoMananaentrantes);
	/** 
	 * Consultamos la tabla de las tareas criticas y guardamos en variable criticas 
	 */
	var consultarModelo_i = Alloy.createCollection('tareas_entrantes');
	var consultarModelo_i_where = 'tipo_tarea=1 ORDER BY TIPO_TAREA ASC';
	consultarModelo_i.fetch({
		query: 'SELECT * FROM tareas_entrantes WHERE tipo_tarea=1 ORDER BY TIPO_TAREA ASC'
	});
	var criticas = require('helper').query2array(consultarModelo_i);
	if (Ti.App.deployType != 'production') console.log('tareas criticas existentes', {
		"criticas": criticas
	});
	var tarea_index = 0;
	_.each(criticas, function(tarea, tarea_pos, tarea_list) {
		tarea_index += 1;
		if ((_.isObject(tarea.nivel_2) || _.isString(tarea.nivel_2)) && _.isEmpty(tarea.nivel_2)) {
			/** 
			 * Revisamos cual es el ultimo nivel disponible y lo fijamos dentro de la variable tarea como ultimo_nivel 
			 */
			var tarea = _.extend(tarea, {
				ultimo_nivel: tarea.nivel_1
			});
		} else if ((_.isObject(tarea.nivel_3) || _.isString(tarea.nivel_3)) && _.isEmpty(tarea.nivel_3)) {
			var tarea = _.extend(tarea, {
				ultimo_nivel: tarea.nivel_2
			});
		} else if ((_.isObject(tarea.nivel_4) || _.isString(tarea.nivel_4)) && _.isEmpty(tarea.nivel_4)) {
			var tarea = _.extend(tarea, {
				ultimo_nivel: tarea.nivel_3
			});
		} else if ((_.isObject(tarea.nivel_5) || _.isString(tarea.nivel_5)) && _.isEmpty(tarea.nivel_5)) {
			var tarea = _.extend(tarea, {
				ultimo_nivel: tarea.nivel_4
			});
		} else {
			var tarea = _.extend(tarea, {
				ultimo_nivel: tarea.nivel_5
			});
		}
		/** 
		 * Insertamos la variable tarea con los atributos en el listado, seccion criticas 
		 */
		var itemListado = [{
			vista14: {},
			Label9: {
				text: String.format(L('x1487588533_traducir', '%1$s, %2$s'), (tarea.nivel_2) ? tarea.nivel_2.toString() : '', (tarea.pais_texto) ? tarea.pais_texto.toString() : '')
			},
			vista13: {},
			vista17: {},
			Label7: {
				text: tarea.direccion
			},
			vista19: {},
			vista16: {},
			Label8: {
				text: tarea.ultimo_nivel
			},
			vista15: {},
			vista18: {},
			template: 'criticas',
			Aubicacioncritica: {
				text: String.format(L('x1959850688_traducir', 'a %1$s km'), tarea.distance.toString())
			},
			vista11: {
				myid: tarea.id
			},
			vista12: {}

		}];
		var itemListado_secs = {};
		_.map($.listado.getSections(), function(itemListado_valor, itemListado_indice) {
			itemListado_secs[itemListado_valor.getHeaderTitle()] = itemListado_indice;
			return itemListado_valor;
		});
		if ('' + L('x2819214932_traducir', 'holi1') + '' in itemListado_secs) {
			$.listado.sections[itemListado_secs['' + L('x2819214932_traducir', 'holi1') + '']].appendItems(itemListado);
		} else {
			console.log('DEBUG:dynamic listItem points to a invalid section id: check attribute _seccion.');
		}
	});
	if (criticas && criticas.length) {
		/** 
		 * Revisamos si la variable criticas contiene tareas, siendo true, contamos el largo del arreglo y las definimos en el badge, si no tiene tareas, no mostramos el badge 
		 */
		var Label3_badge = criticas.length;

		var set_Badge = function(valor) {
			if (OS_IOS) {
				if (valor == 'null') {
					$.tab_Label3.setBadge(null);
				} else {
					$.tab_Label3.setBadge(valor);
				}
			} else if (OS_ANDROID) {
				if (valor == 'null') {
					$.tab_Label3.setIcon(null);
				} else if ('images' in require('a4w').styles && 'androidbadge' + Label3_badge in require('a4w').styles['images']) {
					$.tab_Label3.setIcon(require('a4w').styles['images']['androidbadge' + Label3_badge]);
					if (typeof WPATH != 'undefined') {
						require(WPATH('vars'))['_android_badge'] = Label3_badge;
					} else {
						require('vars')['_android_badge'] = Label3_badge;
					}
				}
			}
		};
		var get_Badge = function() {
			if (OS_IOS) {
				return $.tab_Label3.getBadge();
			} else if (OS_ANDROID) {
				if (typeof WPATH != 'undefined') {
					return require(WPATH('vars'))['_android_badge'];
				} else {
					return require('vars')['_android_badge'];
				}
			} else {
				return 0;
			}
		};
		set_Badge(Label3_badge);

	} else {
		var Label3_badge = 'null';

		var set_Badge = function(valor) {
			if (OS_IOS) {
				if (valor == 'null') {
					$.tab_Label3.setBadge(null);
				} else {
					$.tab_Label3.setBadge(valor);
				}
			} else if (OS_ANDROID) {
				if (valor == 'null') {
					$.tab_Label3.setIcon(null);
				} else if ('images' in require('a4w').styles && 'androidbadge' + Label3_badge in require('a4w').styles['images']) {
					$.tab_Label3.setIcon(require('a4w').styles['images']['androidbadge' + Label3_badge]);
					if (typeof WPATH != 'undefined') {
						require(WPATH('vars'))['_android_badge'] = Label3_badge;
					} else {
						require('vars')['_android_badge'] = Label3_badge;
					}
				}
			}
		};
		var get_Badge = function() {
			if (OS_IOS) {
				return $.tab_Label3.getBadge();
			} else if (OS_ANDROID) {
				if (typeof WPATH != 'undefined') {
					return require(WPATH('vars'))['_android_badge'];
				} else {
					return require('vars')['_android_badge'];
				}
			} else {
				return 0;
			}
		};
		set_Badge(Label3_badge);

	}
	/** 
	 * Consultamos la tabla de las tareas criticas y guardamos en variable normales 
	 */
	var consultarModelo2_i = Alloy.createCollection('tareas_entrantes');
	var consultarModelo2_i_where = 'tipo_tarea=0 ORDER BY FECHA_TAREA ASC';
	consultarModelo2_i.fetch({
		query: 'SELECT * FROM tareas_entrantes WHERE tipo_tarea=0 ORDER BY FECHA_TAREA ASC'
	});
	var normales = require('helper').query2array(consultarModelo2_i);
	if (normales && normales.length) {
		if (Ti.App.deployType != 'production') console.log('tareas normales existentes', {
			"normales": normales
		});
		/** 
		 * Formateamos fecha de hoy y manana para cargar la tarea dependiendo de la fecha de realizacion 
		 */
		var moment = require('alloy/moment');
		var fecha_hoy = moment(new Date()).format('YYYY-MM-DD');
		manana = new Date();
		manana.setDate(manana.getDate() + 1);;
		var moment = require('alloy/moment');
		var formatearFecha2 = manana;
		var fecha_manana = moment(formatearFecha2).format('YYYY-MM-DD');
		/** 
		 * Inicializamos la variable de ultima fecha en vacio para usarla en el recorrido de tareas 
		 */
		require('vars')['ultima_fecha'] = '';
		var tarea_index = 0;
		_.each(normales, function(tarea, tarea_pos, tarea_list) {
			tarea_index += 1;
			/** 
			 * Variable usada para poder filtrar las tareas segun la fecha y no se vayan todas a una sola seccion 
			 */
			var ultima_fecha = ('ultima_fecha' in require('vars')) ? require('vars')['ultima_fecha'] : '';
			if (ultima_fecha != tarea.fecha_tarea) {
				/** 
				 * Generamos una seccion nueva para las fechas que no sean de hoy, ni de manana 
				 */
				if (tarea.fecha_tarea == fecha_hoy) {} else if (tarea.fecha_tarea == fecha_manana) {} else {
					var moment = require('alloy/moment');
					var formatearFecha3 = tarea.fecha_tarea;
					if (typeof formatearFecha3 === 'string' || typeof formatearFecha3 === 'number') {
						var fecha_titulo = moment(formatearFecha3, 'YYYY-MM-DD').format('DD/MM/YYYY');
					} else {
						var fecha_titulo = moment(formatearFecha3).format('DD/MM/YYYY');
					}
					var seccionListadoFechaentrantes = Titanium.UI.createListSection({
						headerTitle: L('x2345059420', 'fecha_entrantes')
					});
					var headerListado4 = Titanium.UI.createView({
						height: '25dp',
						width: Ti.UI.FILL
					});
					var vista23 = Titanium.UI.createView({
						height: '25dp',
						layout: 'composite',
						width: Ti.UI.FILL,
						backgroundColor: '#F7F7F7'
					});
					var Fechatitulo = Titanium.UI.createLabel({
						text: fecha_titulo,
						color: '#999999',
						touchEnabled: false,
						font: {
							fontFamily: 'Roboto-Medium',
							fontSize: '14dp'
						}

					});
					vista23.add(Fechatitulo);

					headerListado4.add(vista23);
					seccionListadoFechaentrantes.setHeaderView(headerListado4);
					$.listado.appendSection(seccionListadoFechaentrantes);
				}
				/** 
				 * Actualizamos la variable con&#160;la ultima fecha de la tarea 
				 */
				require('vars')['ultima_fecha'] = tarea.fecha_tarea;
			}
			if ((_.isObject(tarea.nivel_2) || _.isString(tarea.nivel_2)) && _.isEmpty(tarea.nivel_2)) {
				/** 
				 * Revisamos cual es el ultimo nivel disponible y lo fijamos dentro de la variable tarea como ultimo_nivel 
				 */
				var tarea = _.extend(tarea, {
					ultimo_nivel: tarea.nivel_1
				});
			} else if ((_.isObject(tarea.nivel_3) || _.isString(tarea.nivel_3)) && _.isEmpty(tarea.nivel_3)) {
				var tarea = _.extend(tarea, {
					ultimo_nivel: tarea.nivel_2
				});
			} else if ((_.isObject(tarea.nivel_4) || _.isString(tarea.nivel_4)) && _.isEmpty(tarea.nivel_4)) {
				var tarea = _.extend(tarea, {
					ultimo_nivel: tarea.nivel_3
				});
			} else if ((_.isObject(tarea.nivel_5) || _.isString(tarea.nivel_5)) && _.isEmpty(tarea.nivel_5)) {
				var tarea = _.extend(tarea, {
					ultimo_nivel: tarea.nivel_4
				});
			} else {
				var tarea = _.extend(tarea, {
					ultimo_nivel: tarea.nivel_5
				});
			}
			if (tarea.fecha_tarea == fecha_hoy) {
				/** 
				 * Dependiendo de la fecha de la tarea, es donde apuntaremos a la seccion que corresponde (segun fecha) 
				 */
				var itemListado2 = [{
					vista6: {},
					Label4: {
						text: tarea.direccion
					},
					vista7: {},
					vista4: {},
					Label6: {
						text: String.format(L('x1487588533_traducir', '%1$s, %2$s'), (tarea.nivel_2) ? tarea.nivel_2.toString() : '', (tarea.pais_texto) ? tarea.pais_texto.toString() : '')
					},
					vista3: {},
					vista9: {},
					Label5: {
						text: tarea.ultimo_nivel
					},
					Aubicaciontarea: {
						text: String.format(L('x1959850688_traducir', 'a %1$s km'), tarea.distance.toString())
					},
					template: 'tareas',
					vista8: {},
					vista5: {},
					vista10: {},
					vista2: {
						myid: tarea.id
					}

				}];
				var itemListado2_secs = {};
				_.map($.listado.getSections(), function(itemListado2_valor, itemListado2_indice) {
					itemListado2_secs[itemListado2_valor.getHeaderTitle()] = itemListado2_indice;
					return itemListado2_valor;
				});
				if ('' + L('x3166667470', 'hoy_entrantes') + '' in itemListado2_secs) {
					$.listado.sections[itemListado2_secs['' + L('x3166667470', 'hoy_entrantes') + '']].appendItems(itemListado2);
				} else {
					console.log('DEBUG:dynamic listItem points to a invalid section id: check attribute _seccion.');
				}
			} else if (tarea.fecha_tarea == fecha_manana) {
				var itemListado3 = [{
					vista6: {},
					Label4: {
						text: tarea.direccion
					},
					vista7: {},
					vista4: {},
					Label6: {
						text: String.format(L('x1487588533_traducir', '%1$s, %2$s'), (tarea.nivel_2) ? tarea.nivel_2.toString() : '', (tarea.pais_texto) ? tarea.pais_texto.toString() : '')
					},
					vista3: {},
					vista9: {},
					Label5: {
						text: tarea.ultimo_nivel
					},
					Aubicaciontarea: {
						text: String.format(L('x1959850688_traducir', 'a %1$s km'), tarea.distance.toString())
					},
					template: 'tareas',
					vista8: {},
					vista5: {},
					vista10: {},
					vista2: {
						myid: tarea.id
					}

				}];
				var itemListado3_secs = {};
				_.map($.listado.getSections(), function(itemListado3_valor, itemListado3_indice) {
					itemListado3_secs[itemListado3_valor.getHeaderTitle()] = itemListado3_indice;
					return itemListado3_valor;
				});
				if ('' + L('x3690817388', 'manana_entrantes') + '' in itemListado3_secs) {
					$.listado.sections[itemListado3_secs['' + L('x3690817388', 'manana_entrantes') + '']].appendItems(itemListado3);
				} else {
					console.log('DEBUG:dynamic listItem points to a invalid section id: check attribute _seccion.');
				}
			} else {
				var itemListado4 = [{
					vista6: {},
					Label4: {
						text: tarea.direccion
					},
					vista7: {},
					vista4: {},
					Label6: {
						text: String.format(L('x1487588533_traducir', '%1$s, %2$s'), (tarea.nivel_2) ? tarea.nivel_2.toString() : '', (tarea.pais_texto) ? tarea.pais_texto.toString() : '')
					},
					vista3: {},
					vista9: {},
					Label5: {
						text: tarea.ultimo_nivel
					},
					Aubicaciontarea: {
						text: String.format(L('x1959850688_traducir', 'a %1$s km'), tarea.distance.toString())
					},
					template: 'tareas',
					vista8: {},
					vista5: {},
					vista10: {},
					vista2: {
						myid: tarea.id
					}

				}];
				var itemListado4_secs = {};
				_.map($.listado.getSections(), function(itemListado4_valor, itemListado4_indice) {
					itemListado4_secs[itemListado4_valor.getHeaderTitle()] = itemListado4_indice;
					return itemListado4_valor;
				});
				if ('' + L('x2345059420', 'fecha_entrantes') + '' in itemListado4_secs) {
					$.listado.sections[itemListado4_secs['' + L('x2345059420', 'fecha_entrantes') + '']].appendItems(itemListado4);
				} else {
					console.log('DEBUG:dynamic listItem points to a invalid section id: check attribute _seccion.');
				}
			}
		});
	}
};
Alloy.Events.on('_refrescar_tareas_entrantes', _my_events['_refrescar_tareas_entrantes,ID_1636319035']);

_my_events['_refrescar_tareas,ID_411683779'] = function(evento) {

	Alloy.Events.trigger('_refrescar_tareas_entrantes');
};
Alloy.Events.on('_refrescar_tareas', _my_events['_refrescar_tareas,ID_411683779']);
var ID_170851302_func = function() {

	Alloy.Events.trigger('_refrescar_tareas_entrantes');
};
var ID_170851302 = setTimeout(ID_170851302_func, 1000 * 0.2);

function Androidback_menu(e) {
	/** 
	 * Sin funcionalidad para evitar que el usuario cierre la pantalla 
	 */

	e.cancelBubble = true;
	var elemento = e.source;

}
//$.menu.open();
