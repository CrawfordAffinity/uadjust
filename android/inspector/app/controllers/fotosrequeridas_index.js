var _bind4section = {};
var _list_templates = {
	"elemento": {
		"vista2": {},
		"Label3": {
			"text": "{nombre}"
		},
		"Label2": {
			"text": "{id_server}"
		},
		"Label": {
			"text": "{id_segured}"
		},
		"vista": {}
	},
	"dano": {
		"Label2": {
			"text": "{id}"
		},
		"vista29": {},
		"Label": {
			"text": "{nombre}"
		}
	},
	"pborrar": {
		"vista5": {},
		"imagen": {},
		"vista3": {},
		"Label4": {
			"text": "{id}"
		},
		"Label3": {
			"text": "{nombre}"
		},
		"vista4": {},
		"vista22": {},
		"vista23": {},
		"vista24": {},
		"imagen2": {}
	},
	"contenido": {
		"vista2": {},
		"Label2": {
			"text": "{id}"
		},
		"Label": {
			"text": "{nombre}"
		}
	},
	"recinto": {
		"vista2": {},
		"Label": {
			"text": "{nombre}"
		},
		"Label2": {
			"text": "{id}"
		}
	},
	"nivel": {
		"Label2": {
			"text": "{id}"
		},
		"Label": {
			"text": "{nombre}"
		},
		"vista21": {}
	}
};
var $fotosr = $.fotosr.toJSON();

var _activity;
if (OS_ANDROID) {
	_activity = $.fotos_requeridas.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'fotos_requeridas';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.fotos_requeridas.addEventListener('open', function(e) {});
}
$.fotos_requeridas.orientationModes = [Titanium.UI.PORTRAIT];


var camaramax = Ti.UI.createView({
	height: Ti.UI.FILL,
	left: 0,
	layout: 'composite',
	width: Ti.UI.FILL,
	top: 0,
	tipo: 'imagen'
});
//$.fotos_requeridas.open();

function camaracamaramax() {
	require('com.skypanther.picatsize').showCamera({
		success: function(event) {
			if (event.mediaType == require('com.skypanther.picatsize').MEDIA_TYPE_PHOTO) {
				camaramax.fireEvent("success", {
					valor: {
						error: false,
						cancel: false,
						data: event.media,
						type: 'photo'
					}
				});
			} else if (event.mediaType == require('com.skypanther.picatsize').MEDIA_TYPE_VIDEO) {
				camaramax.fireEvent("success", {
					valor: {
						error: false,
						cancel: false,
						data: event.media,
						type: 'video'
					}
				});
			}
		},
		cancel: function() {
			camaramax.fireEvent("cancel", {
				valor: {
					error: false,
					cancel: true,
					data: '',
					reason: 'cancelled'
				}
			});
		},
		error: function(error) {
			camaramax.fireEvent("error", {
				valor: {
					error: true,
					cancel: false,
					data: error,
					reason: error.error,
					type: 'camera'
				}
			});
		},
		autohide: false,
		showControls: false,
		targetWidth: 480,
		targetHeight: 640,
		saveToPhotoGallery: false,
		overlay: camaramax,
	});
}
require('vars')['_estadoflash_'] = 'auto';
require('vars')['_tipocamara_'] = 'trasera';
require('vars')['_hayflash_'] = true;
if (Ti.Media.hasCameraPermissions()) {
	camaracamaramax();
} else {
	Ti.Media.requestCameraPermissions(function(ercp) {
		if (ercp.success) {
			camaracamaramax();
		} else {
			camaramax.fireEvent("error", {
				valor: {
					error: true,
					cancel: false,
					data: 'nopermission',
					type: 'permission',
					reason: 'camera doesnt have permissions'
				}
			});
		}
	});
}
var widgetBarra = Alloy.createWidget('barra');

function Cerrar_widgetBarra(e) {
	var evento = e;
	/** 
	 * Cerramos la vista, camara, y pantalla 
	 */
	var camaramax_esconder = 'camara';

	var esconderVistaCamara = function(tipo) {
		if (tipo == 'camara' || tipo == 'cam' || tipo == 'Camara') {
			require('com.skypanther.picatsize').hideCamera();
		} else if (tipo == 'vista' || tipo == 'view') {
			camaramax.hide();
		}
	};
	esconderVistaCamara(camaramax_esconder);

	var camaramax_esconder = 'vista';

	var esconderVistaCamara = function(tipo) {
		if (tipo == 'camara' || tipo == 'cam' || tipo == 'Camara') {
			require('com.skypanther.picatsize').hideCamera();
		} else if (tipo == 'vista' || tipo == 'view') {
			camaramax.hide();
		}
	};
	esconderVistaCamara(camaramax_esconder);

	$.fotos_requeridas.close();
}

function Presiono_widgetBarra(e) {
	var evento = e;
	/** 
	 * Recuperamos variable de el objeto para saber si las fotos minimas fueron capturadas, si no estan, avisamos al usuario que foto falta 
	 */
	var requeridas = ('requeridas' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['requeridas'] : '';
	if (requeridas.foto1 == true || requeridas.foto1 == 'true') {
		var preguntarAlerta_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta = Ti.UI.createAlertDialog({
			title: L('x57652245_traducir', 'Alerta'),
			message: L('x305784584_traducir', 'Falta la foto de fachada'),
			buttonNames: preguntarAlerta_opts
		});
		preguntarAlerta.addEventListener('click', function(e) {
			var pre = preguntarAlerta_opts[e.index];
			pre = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta.show();
	} else if (requeridas.foto2 == true || requeridas.foto2 == 'true') {
		var preguntarAlerta2_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta2 = Ti.UI.createAlertDialog({
			title: L('x57652245_traducir', 'Alerta'),
			message: L('x3294013631_traducir', 'Falta la foto del barrio'),
			buttonNames: preguntarAlerta2_opts
		});
		preguntarAlerta2.addEventListener('click', function(e) {
			var pre = preguntarAlerta2_opts[e.index];
			pre = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta2.show();
	} else if (requeridas.foto3 == true || requeridas.foto3 == 'true') {
		var preguntarAlerta3_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta3 = Ti.UI.createAlertDialog({
			title: L('x57652245_traducir', 'Alerta'),
			message: L('x2286900918_traducir', 'Falta la foto de número de domicilio'),
			buttonNames: preguntarAlerta3_opts
		});
		preguntarAlerta3.addEventListener('click', function(e) {
			var pre = preguntarAlerta3_opts[e.index];
			pre = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta3.show();
	} else {
		/** 
		 * Ocupamos esta variable para impedir que la pantalla siguente se cargue dos veces 
		 */
		var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
		if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
			require('vars')['var_abriendo'] = L('x4261170317', 'true');
			/** 
			 * Guardamos el modelo 
			 */
			Alloy.Collections[$.fotosr.config.adapter.collection_name].add($.fotosr);
			$.fotosr.save();
			Alloy.Collections[$.fotosr.config.adapter.collection_name].fetch();
			/** 
			 * Cerramos vista y camara 
			 */
			var camaramax_esconder = 'camara';

			var esconderVistaCamara = function(tipo) {
				if (tipo == 'camara' || tipo == 'cam' || tipo == 'Camara') {
					require('com.skypanther.picatsize').hideCamera();
				} else if (tipo == 'vista' || tipo == 'view') {
					camaramax.hide();
				}
			};
			esconderVistaCamara(camaramax_esconder);

			var camaramax_esconder = 'vista';

			var esconderVistaCamara = function(tipo) {
				if (tipo == 'camara' || tipo == 'cam' || tipo == 'Camara') {
					require('com.skypanther.picatsize').hideCamera();
				} else if (tipo == 'vista' || tipo == 'view') {
					camaramax.hide();
				}
			};
			esconderVistaCamara(camaramax_esconder);

			Alloy.createController("hayalguien_index", {}).getView().open();
		}
	}
}
widgetBarra.init({
	titulo: L('x810026296_traducir', 'FOTOS REQUERIDAS'),
	__id: 'ALL138747237',
	oncerrar: Cerrar_widgetBarra,
	continuar: L('', ''),
	cerrar: L('', ''),
	fondo: 'fondoazul',
	top: 0,
	onpresiono: Presiono_widgetBarra
});
camaramax.add(widgetBarra.getView());
var vista = Titanium.UI.createView({
	height: '73dp',
	bottom: '100dp',
	layout: 'composite',
	backgroundColor: '#FFFFFF'
});
var vista2 = Titanium.UI.createView({
	height: Ti.UI.FILL,
	layout: 'composite',
	width: '80dp',
	backgroundColor: '#F7F7F7',
	font: {
		fontSize: '12dp'
	}
});
var iconoCamara = Titanium.UI.createLabel({
	height: Ti.UI.SIZE,
	text: '\uE80c',
	color: '#FFFFFF',
	width: Ti.UI.SIZE,
	font: {
		fontFamily: 'beta1',
		fontSize: '36dp'
	}
});
vista2.add(iconoCamara);
vista.add(vista2);
var vista3 = Titanium.UI.createView({
	height: Ti.UI.FILL,
	visible: false,
	layout: 'composite',
	width: '80dp',
	backgroundColor: '#EE7F7E',
	font: {
		fontSize: '12dp'
	}
});
var iconoCamara2 = Titanium.UI.createLabel({
	height: Ti.UI.SIZE,
	text: '\uE80c',
	color: '#FFFFFF',
	width: Ti.UI.SIZE,
	font: {
		fontFamily: 'beta1',
		fontSize: '36dp'
	}
});
vista3.add(iconoCamara2);
vista3.addEventListener('click', function(e) {
	e.cancelBubble = true;
	var elemento = e.source;
	/** 
	 * Capturamos la foto 
	 */
	require('com.skypanther.picatsize').takePicture();
	/** 
	 * Escondemos la vista para prevenir que el usuario tome multiples fotos a la vez, y evitar que la aplicacion se cierre 
	 */
	var vista3_visible = false;

	if (vista3_visible == 'si') {
		vista3_visible = true;
	} else if (vista3_visible == 'no') {
		vista3_visible = false;
	}
	vista3.setVisible(vista3_visible);

});
vista.add(vista3);
var vista4 = Titanium.UI.createView({
	height: Ti.UI.FILL,
	layout: 'composite',
	width: '80dp',
	right: '0dp'
});
var iconoGirar_camara = Titanium.UI.createLabel({
	height: Ti.UI.SIZE,
	text: '\uE81d',
	color: '#2D9EDB',
	width: Ti.UI.SIZE,
	font: {
		fontFamily: 'beta1',
		fontSize: '24dp'
	}
});
vista4.add(iconoGirar_camara);
vista4.addEventListener('click', function(e) {
	e.cancelBubble = true;
	var elemento = e.source;
	/** 
	 * Revisamos el flag de posicion de camara (frontal o trasera) y cambiamos la camara 
	 */
	var camara = ('camara' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['camara'] : '';
	if (camara == L('x2010748274_traducir', 'trasera')) {
		/** 
		 * Flag para saber que tiene que cambiar a frontal 
		 */
		require('vars')[_var_scopekey]['camara'] = L('x2556550622_traducir', 'frontal');
		var camaramax_camara = 'frontal';

		if (camaramax_camara == 'front' || camaramax_camara == 'frontal' || camaramax_camara == 'normal') {
			camaramax_camara = require('com.skypanther.picatsize').CAMERA_FRONT;
			require('vars')['_hayflash_'] = false;
			require('vars')['_tipocamara_'] = 'frontal';
			require('vars')['_estadoflash_'] = 'auto';
		} else if (camaramax_camara == 'trasera' || camaramax_camara == 'back' || camaramax_camara == 'rear') {
			camaramax_camara = require('com.skypanther.picatsize').CAMERA_REAR;
			require('vars')['_hayflash_'] = true;
			require('vars')['_tipocamara_'] = 'trasera';
			require('vars')['_estadoflash_'] = 'auto';
		}
		require('com.skypanther.picatsize').switchCamera(camaramax_camara);

	}
	if (camara == L('x2556550622_traducir', 'frontal')) {
		require('vars')[_var_scopekey]['camara'] = L('x2010748274_traducir', 'trasera');
		var camaramax_camara = 'trasera';

		if (camaramax_camara == 'front' || camaramax_camara == 'frontal' || camaramax_camara == 'normal') {
			camaramax_camara = require('com.skypanther.picatsize').CAMERA_FRONT;
			require('vars')['_hayflash_'] = false;
			require('vars')['_tipocamara_'] = 'frontal';
			require('vars')['_estadoflash_'] = 'auto';
		} else if (camaramax_camara == 'trasera' || camaramax_camara == 'back' || camaramax_camara == 'rear') {
			camaramax_camara = require('com.skypanther.picatsize').CAMERA_REAR;
			require('vars')['_hayflash_'] = true;
			require('vars')['_tipocamara_'] = 'trasera';
			require('vars')['_estadoflash_'] = 'auto';
		}
		require('com.skypanther.picatsize').switchCamera(camaramax_camara);

	}
});
vista.add(vista4);
camaramax.add(vista);
var vista5 = Titanium.UI.createView({
	height: '100dp',
	bottom: '0dp',
	layout: 'vertical'
});
var widget4fotos = Alloy.createWidget('4fotos');

function Click_widget4fotos(e) {
	var evento = e;
	require('vars')[_var_scopekey]['foto'] = evento.pos;
	/** 
	 * Habilitamos boton tomar foto 
	 */
	var vista3_visible = true;

	if (vista3_visible == 'si') {
		vista3_visible = true;
	} else if (vista3_visible == 'no') {
		vista3_visible = false;
	}
	vista3.setVisible(vista3_visible);

}

function Listo_widget4fotos(e) {
	var evento = e;
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	if (_.isString(seltarea)) {
		/** 
		 * Definimos valor inventado id_server para testing 
		 */
		var seltarea = {
			id_server: 123
		};
	}
	var previos = [];
	var ID_587641544_f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
	if (ID_587641544_f.exists() == true) {
		previos = ID_587641544_f.getDirectoryListing();
	}
	if (Ti.App.deployType != 'production') console.log('archivos previos existentes en telefono', {
		"previos": previos
	});
	var requeridas = ('requeridas' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['requeridas'] : '';
	if (evento.pos == 1 || evento.pos == '1') {
		/** 
		 * Cambiamos estado de foto1 para avisar que tenemos foto capturada 
		 */
		var requeridas = _.extend(requeridas, {
			foto1: L('x734881840_traducir', 'false')
		});
		/** 
		 * Guardamos la foto en la memoria del celular 
		 */
		var ID_870492955_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
		if (ID_870492955_d.exists() == false) ID_870492955_d.createDirectory();
		var ID_870492955_f = Ti.Filesystem.getFile(ID_870492955_d.resolve(), 'foto1.jpg');
		if (ID_870492955_f.exists() == true) ID_870492955_f.deleteFile();
		ID_870492955_f.write(evento.nueva);
		ID_870492955_d = null;
		ID_870492955_f = null;
		/** 
		 * Actualizamos el modelo avisando que la foto de fachada, el archivo se llama foto1 
		 */
		$.fotosr.set({
			fachada: 'foto1.jpg'
		});
		if ('fotosr' in $) $fotosr = $.fotosr.toJSON();
	} else if (evento.pos == 2) {
		var requeridas = _.extend(requeridas, {
			foto2: L('x734881840_traducir', 'false')
		});
		var ID_1597663409_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
		if (ID_1597663409_d.exists() == false) ID_1597663409_d.createDirectory();
		var ID_1597663409_f = Ti.Filesystem.getFile(ID_1597663409_d.resolve(), 'foto2.jpg');
		if (ID_1597663409_f.exists() == true) ID_1597663409_f.deleteFile();
		ID_1597663409_f.write(evento.nueva);
		ID_1597663409_d = null;
		ID_1597663409_f = null;
		$.fotosr.set({
			barrio: 'foto2.jpg'
		});
		if ('fotosr' in $) $fotosr = $.fotosr.toJSON();
	} else if (evento.pos == 3) {
		var requeridas = _.extend(requeridas, {
			foto3: L('x734881840_traducir', 'false')
		});
		var ID_372013953_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
		if (ID_372013953_d.exists() == false) ID_372013953_d.createDirectory();
		var ID_372013953_f = Ti.Filesystem.getFile(ID_372013953_d.resolve(), 'foto3.jpg');
		if (ID_372013953_f.exists() == true) ID_372013953_f.deleteFile();
		ID_372013953_f.write(evento.nueva);
		ID_372013953_d = null;
		ID_372013953_f = null;
		$.fotosr.set({
			numero: 'foto3.jpg'
		});
		if ('fotosr' in $) $fotosr = $.fotosr.toJSON();
	} else if (evento.pos == 4) {
		var requeridas = _.extend(requeridas, {
			foto4: L('x734881840_traducir', 'false')
		});
		var ID_562700504_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
		if (ID_562700504_d.exists() == false) ID_562700504_d.createDirectory();
		var ID_562700504_f = Ti.Filesystem.getFile(ID_562700504_d.resolve(), 'foto4.jpg');
		if (ID_562700504_f.exists() == true) ID_562700504_f.deleteFile();
		ID_562700504_f.write(evento.nueva);
		ID_562700504_d = null;
		ID_562700504_f = null;
		$.fotosr.set({
			depto: 'foto4.jpg'
		});
		if ('fotosr' in $) $fotosr = $.fotosr.toJSON();
	}
	require('vars')[_var_scopekey]['requeridas'] = requeridas;
	/** 
	 * Habilitamos boton tomar foto 
	 */
	var vista3_visible = true;

	if (vista3_visible == 'si') {
		vista3_visible = true;
	} else if (vista3_visible == 'no') {
		vista3_visible = false;
	}
	vista3.setVisible(vista3_visible);

	require('vars')[_var_scopekey]['foto'] = L('x634125391_traducir', 'null');
	/** 
	 * Limpiamos memoria ram 
	 */
	evento = null;
}
widget4fotos.init({
	__id: 'ALL1189437781',
	onlisto: Listo_widget4fotos,
	opcional: L('x2052833569_traducir', 'Opcional'),
	label1: L('x724488137_traducir', 'Fachada'),
	label4: L('x676841594_traducir', 'Nº Depto'),
	label3: L('x4076266664_traducir', 'Numero'),
	onclick: Click_widget4fotos,
	label2: L('x2825989175_traducir', 'Barrio')
});
vista5.add(widget4fotos.getView());
camaramax.add(vista5);
camaramax.addEventListener('success', function(e) {
	e.cancelBubble = true;
	var elemento = e.valor;
	/** 
	 * Creamos alias para la foto recibida 
	 */
	var foto_original = elemento.data;
	var foto = ('foto' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['foto'] : '';
	var camara = ('camara' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['camara'] : '';
	if (foto == 1 || foto == '1') {
		widget4fotos.procesar({
			imagen1: foto_original,
			nueva: 640,
			camara: camara
		});
	} else if (foto == 2) {
		widget4fotos.procesar({
			imagen2: foto_original,
			nueva: 640,
			camara: camara
		});
	} else if (foto == 3) {
		widget4fotos.procesar({
			imagen3: foto_original,
			nueva: 640,
			camara: camara
		});
	} else if (foto == 4) {
		widget4fotos.procesar({
			imagen4: foto_original,
			nueva: 640,
			camara: camara
		});
	}
	/** 
	 * Limpiamos memoria ram 
	 */
	foto_original = comprimida = mini = elemento = null
});
camaramax.addEventListener('cancel', function(e) {
	e.cancelBubble = true;
	var elemento = e.valor;
	if (Ti.App.deployType != 'production') console.log('no voy a hacer nada. solo cancelar', {});
});


(function() {
	/** 
	 * Avisamos que se debe cerrar pantalla detalle de tarea 
	 */
	Alloy.Events.trigger('_cerrar_insp', {
		pantalla: 'detalle'
	});
	/** 
	 * Seteamos flag para saber que se inicia en la camara trasera 
	 */
	require('vars')[_var_scopekey]['camara'] = L('x2010748274_traducir', 'trasera');
	/** 
	 * Creamos objeto para poder saber si el minimo de fotos obligatorias existen 
	 */
	var requeridas = {
		foto1: L('x4261170317', 'true'),
		foto2: L('x4261170317', 'true'),
		foto3: L('x4261170317', 'true'),
		foto4: L('x734881840_traducir', 'false')
	};
	require('vars')[_var_scopekey]['requeridas'] = requeridas;
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
		/** 
		 * Verificamos que estemos en una inspeccion para poder modificar el id_inspeccion del modelo de fotos requeridas 
		 */
		$.fotosr.set({
			id_inspeccion: seltarea.id_server
		});
		if ('fotosr' in $) $fotosr = $.fotosr.toJSON();
	}
})();


/** 
 * Cerramos esta pantalla cuando es llamada en la pantalla siguente (hayalguien) 
 */
_my_events['_cerrar_insp,ID_231485652'] = function(evento) {
	if (!_.isUndefined(evento.pantalla)) {
		if (evento.pantalla == L('x2385878536_traducir', 'frequeridas')) {
			if (Ti.App.deployType != 'production') console.log('debug cerrando fotos requeridas', {});
			var ID_962933245_trycatch = {
				error: function(e) {
					if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
				}
			};
			try {
				ID_962933245_trycatch.error = function(evento) {
					if (Ti.App.deployType != 'production') console.log('error cerrando fotos requeridas', {});
				};
				if (Ti.App.deployType != 'production') console.log('cerrando fotos requeridas', {});
				var camaramax_esconder = 'camara';

				var esconderVistaCamara = function(tipo) {
					if (tipo == 'camara' || tipo == 'cam' || tipo == 'Camara') {
						require('com.skypanther.picatsize').hideCamera();
					} else if (tipo == 'vista' || tipo == 'view') {
						camaramax.hide();
					}
				};
				esconderVistaCamara(camaramax_esconder);

				var camaramax_esconder = 'vista';

				var esconderVistaCamara = function(tipo) {
					if (tipo == 'camara' || tipo == 'cam' || tipo == 'Camara') {
						require('com.skypanther.picatsize').hideCamera();
					} else if (tipo == 'vista' || tipo == 'view') {
						camaramax.hide();
					}
				};
				esconderVistaCamara(camaramax_esconder);

				$.fotos_requeridas.close();
				if (Ti.App.deployType != 'production') console.log('cerradas fotos requeridas', {});
			} catch (e) {
				ID_962933245_trycatch.error(e);
			}
		}
	} else {
		if (Ti.App.deployType != 'production') console.log('debug cerrando (todas) fotos requeridas', {});
		var ID_1962200830_trycatch = {
			error: function(e) {
				if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
			}
		};
		try {
			ID_1962200830_trycatch.error = function(evento) {
				if (Ti.App.deployType != 'production') console.log('error cerrando fotos requeridas', {});
			};
			$.fotos_requeridas.close();
		} catch (e) {
			ID_1962200830_trycatch.error(e);
		}
	}
};
Alloy.Events.on('_cerrar_insp', _my_events['_cerrar_insp,ID_231485652']);

if (OS_IOS || OS_ANDROID) {
	$.fotos_requeridas.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}
