var _bind4section = {};
var _list_templates = {};
var $contenido = $.contenido.toJSON();

var _activity;
if (OS_ANDROID) {
	_activity = $.editarcontenido.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'editarcontenido';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.editarcontenido.addEventListener('open', function(e) {});
}
$.editarcontenido.orientationModes = [Titanium.UI.PORTRAIT];


$.widgetBarra3.init({
	titulo: L('x1759675493_traducir', 'EDITAR CONTENIDO'),
	__id: 'ALL1035873505',
	oncerrar: Cerrar_widgetBarra3,
	textoderecha: L('x2943883035_traducir', 'Guardar'),
	fondo: 'fondoceleste',
	top: 0,
	modal: L('', ''),
	onpresiono: Presiono_widgetBarra3,
	colortextoderecha: 'blanco'
});

function Cerrar_widgetBarra3(e) {

	var evento = e;
	/** 
	 * Limpiamos memoria de widgets 
	 */
	$.widgetModal4.limpiar({});
	$.widgetModal5.limpiar({});
	$.widgetModal6.limpiar({});
	$.editarcontenido.close();

}

function Presiono_widgetBarra3(e) {

	var evento = e;
	/** 
	 * Obtenemos la fecha actual 
	 */

	var hoy = new Date();
	if ($contenido.fecha_compra > hoy == true || $contenido.fecha_compra > hoy == 'true') {
		/** 
		 * Validamos que los campos ingresados sean correctos 
		 */
		var preguntarAlerta14_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta14 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x2327084729_traducir', 'La fecha de compra no puede ser superior a hoy'),
			buttonNames: preguntarAlerta14_opts
		});
		preguntarAlerta14.addEventListener('click', function(e) {
			var nulo = preguntarAlerta14_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta14.show();
	} else if (_.isUndefined($contenido.nombre)) {
		var preguntarAlerta15_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta15 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x2224804335_traducir', 'Seleccione producto'),
			buttonNames: preguntarAlerta15_opts
		});
		preguntarAlerta15.addEventListener('click', function(e) {
			var nulo = preguntarAlerta15_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta15.show();
	} else if (_.isUndefined($contenido.id_marca)) {
		var preguntarAlerta16_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta16 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x3747506986_traducir', 'Seleccione marca del producto'),
			buttonNames: preguntarAlerta16_opts
		});
		preguntarAlerta16.addEventListener('click', function(e) {
			var nulo = preguntarAlerta16_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta16.show();
	} else if (_.isUndefined($contenido.id_recinto)) {
		var preguntarAlerta17_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta17 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x2405532746_traducir', 'Seleccione el recinto donde estaba el producto'),
			buttonNames: preguntarAlerta17_opts
		});
		preguntarAlerta17.addEventListener('click', function(e) {
			var nulo = preguntarAlerta17_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta17.show();
	} else if (_.isUndefined($contenido.cantidad)) {
		var preguntarAlerta18_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta18 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x4257958740_traducir', 'Ingrese cantidad de productos'),
			buttonNames: preguntarAlerta18_opts
		});
		preguntarAlerta18.addEventListener('click', function(e) {
			var nulo = preguntarAlerta18_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta18.show();
	} else if ((_.isObject($contenido.cantidad) || _.isString($contenido.cantidad)) && _.isEmpty($contenido.cantidad)) {
		var preguntarAlerta19_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta19 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x4257958740_traducir', 'Ingrese cantidad de productos'),
			buttonNames: preguntarAlerta19_opts
		});
		preguntarAlerta19.addEventListener('click', function(e) {
			var nulo = preguntarAlerta19_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta19.show();
	} else if (_.isUndefined($contenido.fecha_compra)) {
		var preguntarAlerta20_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta20 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x1056100694_traducir', 'Ingrese fecha de compra aproximada (año)'),
			buttonNames: preguntarAlerta20_opts
		});
		preguntarAlerta20.addEventListener('click', function(e) {
			var nulo = preguntarAlerta20_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta20.show();
	} else if (_.isUndefined($contenido.id_moneda)) {
		var preguntarAlerta21_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta21 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x2624129841_traducir', 'Seleccione el tipo de moneda del producto'),
			buttonNames: preguntarAlerta21_opts
		});
		preguntarAlerta21.addEventListener('click', function(e) {
			var nulo = preguntarAlerta21_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta21.show();
	} else if (_.isUndefined($contenido.valor)) {
		var preguntarAlerta22_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta22 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x1829790159_traducir', 'Ingrese valor del producto'),
			buttonNames: preguntarAlerta22_opts
		});
		preguntarAlerta22.addEventListener('click', function(e) {
			var nulo = preguntarAlerta22_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta22.show();
	} else if ((_.isObject($contenido.valor) || _.isString($contenido.valor)) && _.isEmpty($contenido.valor)) {
		var preguntarAlerta23_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta23 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x1829790159_traducir', 'Ingrese valor del producto'),
			buttonNames: preguntarAlerta23_opts
		});
		preguntarAlerta23.addEventListener('click', function(e) {
			var nulo = preguntarAlerta23_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta23.show();
	} else if (_.isUndefined($contenido.descripcion)) {
		var preguntarAlerta24_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta24 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x417344283_traducir', 'Describa el producto brevemente, con un mínimo de 30 caracteres'),
			buttonNames: preguntarAlerta24_opts
		});
		preguntarAlerta24.addEventListener('click', function(e) {
			var nulo = preguntarAlerta24_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta24.show();
	} else if (_.isNumber($contenido.descripcion.length) && _.isNumber(29) && $contenido.descripcion.length <= 29) {
		var preguntarAlerta25_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta25 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x417344283_traducir', 'Describa el producto brevemente, con un mínimo de 30 caracteres'),
			buttonNames: preguntarAlerta25_opts
		});
		preguntarAlerta25.addEventListener('click', function(e) {
			var nulo = preguntarAlerta25_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta25.show();
	} else {
		/** 
		 * Eliminamos modelo previo 
		 */
		/** 
		 * Eliminamos modelo previo 
		 */
		var eliminarModelo6_i = Alloy.Collections.insp_contenido;
		var sql = 'DELETE FROM ' + eliminarModelo6_i.config.adapter.collection_name + ' WHERE id=\'' + args._dato.id + '\'';
		var db = Ti.Database.open(eliminarModelo6_i.config.adapter.db_name);
		db.execute(sql);
		db.close();
		eliminarModelo6_i.trigger('remove');
		/** 
		 * Guardamos modelo nuevo 
		 */
		Alloy.Collections[$.contenido.config.adapter.collection_name].add($.contenido);
		$.contenido.save();
		Alloy.Collections[$.contenido.config.adapter.collection_name].fetch();
		/** 
		 * limpiamos widgets multiples (memoria ram) 
		 */
		$.widgetModal4.limpiar({});
		$.widgetModal5.limpiar({});
		$.widgetModal6.limpiar({});
		$.editarcontenido.close();
	}
}

function Click_vista38(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	/** 
	 * Ocupamos esta variable para impedir que la pantalla siguente se cargue dos veces 
	 */
	var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
	if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
		require('vars')['var_abriendo'] = L('x4261170317', 'true');
		Alloy.createController("listadocontenidos", {}).getView().open();
	}

}

$.widgetModal6.init({
	titulo: L('x1867465554_traducir', 'MARCAS'),
	cargando: L('x1740321226_traducir', 'cargando ..'),
	__id: 'ALL830800377',
	left: 0,
	onrespuesta: Respuesta_widgetModal6,
	campo: L('x3837495059_traducir', 'Marca del Item'),
	onabrir: Abrir_widgetModal6,
	color: 'celeste',
	right: 0,
	seleccione: L('x1021431138_traducir', 'seleccione marca'),
	activo: true,
	onafterinit: Afterinit_widgetModal6
});

function Abrir_widgetModal6(e) {

	var evento = e;

}

function Respuesta_widgetModal6(e) {

	var evento = e;
	$.widgetModal6.labels({
		valor: evento.valor
	});
	$.contenido.set({
		id_marca: evento.item.id_interno
	});
	if ('contenido' in $) $contenido = $.contenido.toJSON();
	if (Ti.App.deployType != 'production') console.log('datos recibidos de modal marca item', {
		"datos": evento
	});

}

function Afterinit_widgetModal6(e) {

	var evento = e;
	var ID_616239583_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		var consultarModelo6_i = Alloy.createCollection('insp_contenido');
		var consultarModelo6_i_where = 'id=\'' + args._dato.id + '\'';
		consultarModelo6_i.fetch({
			query: 'SELECT * FROM insp_contenido WHERE id=\'' + args._dato.id + '\''
		});
		var insp_dato = require('helper').query2array(consultarModelo6_i);
		if (insp_dato && insp_dato.length) {
			/** 
			 * obtenemos valor seleccionado previamente y lo mostramos en selector. 
			 */
			var consultarModelo7_i = Alloy.createCollection('marcas');
			var consultarModelo7_i_where = 'id_segured=\'' + insp_dato[0].id_marca + '\'';
			consultarModelo7_i.fetch({
				query: 'SELECT * FROM marcas WHERE id_segured=\'' + insp_dato[0].id_marca + '\''
			});
			var marca = require('helper').query2array(consultarModelo7_i);
			if (marca && marca.length) {
				var ID_1058169154_func = function() {
					$.widgetModal6.labels({
						valor: marca[0].nombre
					});
					if (Ti.App.deployType != 'production') console.log('el nombre de la partida es', {
						"datos": marca[0].nombre
					});
				};
				var ID_1058169154 = setTimeout(ID_1058169154_func, 1000 * 0.21);
			}
		}
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			var consultarModelo8_i = Alloy.createCollection('marcas');
			var consultarModelo8_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
			consultarModelo8_i.fetch({
				query: 'SELECT * FROM marcas WHERE pais_texto=\'' + pais[0].nombre + '\''
			});
			var marcas = require('helper').query2array(consultarModelo8_i);
			var datos = [];
			_.each(marcas, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'valor';
					if (llave == 'id_segured') newkey = 'id_interno';
					if (llave == 'id_server') newkey = 'id_gerardo';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
		} else {
			var eliminarModelo7_i = Alloy.Collections.marcas;
			var sql = "DELETE FROM " + eliminarModelo7_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo7_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo7_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo8_m = Alloy.Collections.marcas;
				var insertarModelo8_fila = Alloy.createModel('marcas', {
					nombre: String.format(L('x3638114596_traducir', 'Muro%1$s'), item.toString()),
					id_server: item,
					id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item.toString()),
					pais: 1
				});
				insertarModelo8_m.add(insertarModelo8_fila);
				insertarModelo8_fila.save();
			});
			var transformarModelo8_i = Alloy.createCollection('marcas');
			transformarModelo8_i.fetch();
			var transformarModelo8_src = require('helper').query2array(transformarModelo8_i);
			var datos = [];
			_.each(transformarModelo8_src, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'valor';
					if (llave == 'id_segured') newkey = 'id_interno';
					if (llave == 'id_server') newkey = 'id_gerardo';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
		}
		$.widgetModal6.data({
			data: datos
		});
	};
	var ID_616239583 = setTimeout(ID_616239583_func, 1000 * 0.2);

}

$.widgetFotochica4.init({
	caja: 55,
	__id: 'ALL165543940',
	onlisto: Listo_widgetFotochica4,
	left: 0,
	top: 0,
	onclick: Click_widgetFotochica4
});

function Click_widgetFotochica4(e) {

	var evento = e;
	/** 
	 * Definimos que estamos capturando foto en el primer item 
	 */
	require('vars')['cual_foto'] = L('x2212294583', '1');
	/** 
	 * Abrimos camara 
	 */
	$.widgetCamaralight2.disparar({});
	/** 
	 * Detenemos las animaciones de los widget 
	 */
	$.widgetFotochica4.detener({});
	$.widgetFotochica5.detener({});
	$.widgetFotochica6.detener({});

}

function Listo_widgetFotochica4(e) {

	var evento = e;
	/** 
	 * Recuperamos variables para poder definir la estructura de las carpetas donde se guardara la miniatura 
	 */
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	var nuevoid_f1 = ('nuevoid_f1' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['nuevoid_f1'] : '';
	if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
		var ID_627897085_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + seltarea.id_server);
		if (ID_627897085_d.exists() == false) ID_627897085_d.createDirectory();
		var ID_627897085_f = Ti.Filesystem.getFile(ID_627897085_d.resolve(), 'mini' + nuevoid_f1.id + '.jpg');
		if (ID_627897085_f.exists() == true) ID_627897085_f.deleteFile();
		ID_627897085_f.write(evento.mini);
		ID_627897085_d = null;
		ID_627897085_f = null;
	} else {
		var ID_954194152_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + 0);
		if (ID_954194152_d.exists() == false) ID_954194152_d.createDirectory();
		var ID_954194152_f = Ti.Filesystem.getFile(ID_954194152_d.resolve(), 'mini' + nuevoid_f1.id + '.jpg');
		if (ID_954194152_f.exists() == true) ID_954194152_f.deleteFile();
		ID_954194152_f.write(evento.mini);
		ID_954194152_d = null;
		ID_954194152_f = null;
	}
}

function Click_imagen2(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var evento = e;
	/** 
	 * Definimos que estamos capturando foto en el primer item 
	 */
	require('vars')['cual_foto'] = L('x2212294583', '1');
	/** 
	 * Abrimos camara 
	 */
	$.widgetCamaralight2.disparar({});
	/** 
	 * Detenemos las animaciones de los widget 
	 */
	$.widgetFotochica4.detener({});
	$.widgetFotochica5.detener({});
	$.widgetFotochica6.detener({});
	/** 
	 * Ocultamos la imagen obtenida desde la memoria del telefono para actualizar con la que acabamos de obtener 
	 */
	var vista50_visible = false;

	if (vista50_visible == 'si') {
		vista50_visible = true;
	} else if (vista50_visible == 'no') {
		vista50_visible = false;
	}
	$.vista50.setVisible(vista50_visible);


}

$.widgetFotochica5.init({
	caja: 55,
	__id: 'ALL1692328525',
	onlisto: Listo_widgetFotochica5,
	left: 5,
	top: 0,
	onclick: Click_widgetFotochica5
});

function Click_widgetFotochica5(e) {

	var evento = e;
	$.widgetFotochica5.detener({});
	$.widgetFotochica4.detener({});
	$.widgetFotochica6.detener({});
	require('vars')['cual_foto'] = 2;
	$.widgetCamaralight2.disparar({});

}

function Listo_widgetFotochica5(e) {

	var evento = e;
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	var nuevoid_f2 = ('nuevoid_f2' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['nuevoid_f2'] : '';
	if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
		var ID_41571261_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + seltarea.id_server);
		if (ID_41571261_d.exists() == false) ID_41571261_d.createDirectory();
		var ID_41571261_f = Ti.Filesystem.getFile(ID_41571261_d.resolve(), 'mini' + nuevoid_f2.id + '.jpg');
		if (ID_41571261_f.exists() == true) ID_41571261_f.deleteFile();
		ID_41571261_f.write(evento.mini);
		ID_41571261_d = null;
		ID_41571261_f = null;
	} else {
		var ID_1641740840_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + 0);
		if (ID_1641740840_d.exists() == false) ID_1641740840_d.createDirectory();
		var ID_1641740840_f = Ti.Filesystem.getFile(ID_1641740840_d.resolve(), 'mini' + nuevoid_f2.id + '.jpg');
		if (ID_1641740840_f.exists() == true) ID_1641740840_f.deleteFile();
		ID_1641740840_f.write(evento.mini);
		ID_1641740840_d = null;
		ID_1641740840_f = null;
	}
}

function Click_imagen3(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var evento = e;
	$.widgetFotochica4.detener({});
	$.widgetFotochica5.detener({});
	$.widgetFotochica6.detener({});
	require('vars')['cual_foto'] = 2;
	$.widgetCamaralight2.disparar({});
	var vista51_visible = false;

	if (vista51_visible == 'si') {
		vista51_visible = true;
	} else if (vista51_visible == 'no') {
		vista51_visible = false;
	}
	$.vista51.setVisible(vista51_visible);


}

$.widgetFotochica6.init({
	caja: 55,
	__id: 'ALL480946597',
	onlisto: Listo_widgetFotochica6,
	left: 5,
	top: 0,
	onclick: Click_widgetFotochica6
});

function Click_widgetFotochica6(e) {

	var evento = e;
	$.widgetFotochica6.detener({});
	$.widgetFotochica4.detener({});
	$.widgetFotochica5.detener({});
	require('vars')['cual_foto'] = 3;
	$.widgetCamaralight2.disparar({});

}

function Listo_widgetFotochica6(e) {

	var evento = e;
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	var nuevoid_f3 = ('nuevoid_f3' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['nuevoid_f3'] : '';
	if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
		var ID_1843631350_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + seltarea.id_server);
		if (ID_1843631350_d.exists() == false) ID_1843631350_d.createDirectory();
		var ID_1843631350_f = Ti.Filesystem.getFile(ID_1843631350_d.resolve(), 'mini' + nuevoid_f3.id + '.jpg');
		if (ID_1843631350_f.exists() == true) ID_1843631350_f.deleteFile();
		ID_1843631350_f.write(evento.mini);
		ID_1843631350_d = null;
		ID_1843631350_f = null;
	} else {
		var ID_1948120750_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + 0);
		if (ID_1948120750_d.exists() == false) ID_1948120750_d.createDirectory();
		var ID_1948120750_f = Ti.Filesystem.getFile(ID_1948120750_d.resolve(), 'mini' + nuevoid_f3.id + '.jpg');
		if (ID_1948120750_f.exists() == true) ID_1948120750_f.deleteFile();
		ID_1948120750_f.write(evento.mini);
		ID_1948120750_d = null;
		ID_1948120750_f = null;
	}
}

function Click_imagen4(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var evento = e;
	$.widgetFotochica4.detener({});
	$.widgetFotochica5.detener({});
	$.widgetFotochica6.detener({});
	require('vars')['cual_foto'] = 3;
	$.widgetCamaralight2.disparar({});
	var vista52_visible = false;

	if (vista52_visible == 'si') {
		vista52_visible = true;
	} else if (vista52_visible == 'no') {
		vista52_visible = false;
	}
	$.vista52.setVisible(vista52_visible);


}

$.widgetModal4.init({
	titulo: L('x767609104_traducir', 'RECINTOS'),
	cargando: L('x1831148736', 'cargando...'),
	__id: 'ALL1509904444',
	onrespuesta: Respuesta_widgetModal4,
	campo: L('x382177638_traducir', 'Ubicación'),
	onabrir: Abrir_widgetModal4,
	color: 'celeste',
	seleccione: L('x4060681104_traducir', 'recinto'),
	activo: true,
	onafterinit: Afterinit_widgetModal4
});

function Abrir_widgetModal4(e) {

	var evento = e;

}

function Respuesta_widgetModal4(e) {

	var evento = e;
	$.widgetModal4.labels({
		valor: evento.valor
	});
	$.contenido.set({
		id_recinto: evento.item.id_interno
	});
	if ('contenido' in $) $contenido = $.contenido.toJSON();
	if (Ti.App.deployType != 'production') console.log('datos recibidos de modal ubicacion', {
		"datos": evento
	});

}

function Afterinit_widgetModal4(e) {

	var evento = e;
	var ID_279917941_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		var consultarModelo9_i = Alloy.createCollection('insp_contenido');
		var consultarModelo9_i_where = 'id=\'' + args._dato.id + '\'';
		consultarModelo9_i.fetch({
			query: 'SELECT * FROM insp_contenido WHERE id=\'' + args._dato.id + '\''
		});
		var insp_dato = require('helper').query2array(consultarModelo9_i);
		if (insp_dato && insp_dato.length) {
			/** 
			 * obtenemos valor seleccionado previamente y lo mostramos en selector. 
			 */
			var ID_1090335190_func = function() {
				var consultarModelo10_i = Alloy.createCollection('insp_recintos');
				var consultarModelo10_i_where = 'id_recinto=\'' + insp_dato[0].id_recinto + '\'';
				consultarModelo10_i.fetch({
					query: 'SELECT * FROM insp_recintos WHERE id_recinto=\'' + insp_dato[0].id_recinto + '\''
				});
				var recintosb = require('helper').query2array(consultarModelo10_i);
				if (Ti.App.deployType != 'production') console.log('detalle de recintos', {
					"datos": recintosb
				});
				if (recintosb && recintosb.length) {
					$.widgetModal4.labels({
						valor: recintosb[0].nombre
					});
					if (Ti.App.deployType != 'production') console.log('el nombre de la partida es', {
						"datos": recintosb[0].nombre
					});
				}
			};
			var ID_1090335190 = setTimeout(ID_1090335190_func, 1000 * 0.21);
		}
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			var consultarModelo11_i = Alloy.createCollection('insp_recintos');
			var consultarModelo11_i_where = 'id_inspeccion=\'' + seltarea.id_server + '\'';
			consultarModelo11_i.fetch({
				query: 'SELECT * FROM insp_recintos WHERE id_inspeccion=\'' + seltarea.id_server + '\''
			});
			var recintos = require('helper').query2array(consultarModelo11_i);
			var datos = [];
			_.each(recintos, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'valor';
					if (llave == 'id_recinto') newkey = 'id_interno';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
		} else {
			var eliminarModelo8_i = Alloy.Collections.insp_recintos;
			var sql = "DELETE FROM " + eliminarModelo8_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo8_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo8_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo9_m = Alloy.Collections.insp_recintos;
				var insertarModelo9_fila = Alloy.createModel('insp_recintos', {
					nombre: String.format(L('x1101343128_traducir', 'Pieza%1$s'), item.toString()),
					id_recinto: item
				});
				insertarModelo9_m.add(insertarModelo9_fila);
				insertarModelo9_fila.save();
			});
			var transformarModelo10_i = Alloy.createCollection('insp_recintos');
			transformarModelo10_i.fetch();
			var transformarModelo10_src = require('helper').query2array(transformarModelo10_i);
			var datos = [];
			_.each(transformarModelo10_src, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'valor';
					if (llave == 'id_recinto') newkey = 'id_interno';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
		}
		$.widgetModal4.data({
			data: datos
		});
	};
	var ID_279917941 = setTimeout(ID_279917941_func, 1000 * 0.2);

}

function Change_campo3(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	$.contenido.set({
		cantidad: elemento
	});
	if ('contenido' in $) $contenido = $.contenido.toJSON();
	elemento = null, source = null;

}

function Click_vista55(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.DescribaelDao2.blur();
	$.campo4.blur();
	$.campo3.blur();
	$.widgetDpicker2.abrir({});

}

$.widgetModal5.init({
	titulo: L('x1629775439_traducir', 'MONEDAS'),
	cargando: L('x1831148736', 'cargando...'),
	__id: 'ALL1620626702',
	onrespuesta: Respuesta_widgetModal5,
	campo: L('x3081186843_traducir', 'Moneda'),
	onabrir: Abrir_widgetModal5,
	color: 'celeste',
	seleccione: L('x2547889144', '-'),
	activo: true,
	onafterinit: Afterinit_widgetModal5
});

function Afterinit_widgetModal5(e) {

	var evento = e;
	var ID_1876482487_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		var consultarModelo12_i = Alloy.createCollection('insp_contenido');
		var consultarModelo12_i_where = 'id=\'' + args._dato.id + '\'';
		consultarModelo12_i.fetch({
			query: 'SELECT * FROM insp_contenido WHERE id=\'' + args._dato.id + '\''
		});
		var insp_dato = require('helper').query2array(consultarModelo12_i);
		if (insp_dato && insp_dato.length) {
			var ID_706185240_func = function() {
				var consultarModelo13_i = Alloy.createCollection('monedas');
				var consultarModelo13_i_where = 'id_segured=\'' + insp_dato[0].id_moneda + '\'';
				consultarModelo13_i.fetch({
					query: 'SELECT * FROM monedas WHERE id_segured=\'' + insp_dato[0].id_moneda + '\''
				});
				var monedas = require('helper').query2array(consultarModelo13_i);
				if (monedas && monedas.length) {
					$.widgetModal5.labels({
						valor: monedas[0].nombre
					});
				}
			};
			var ID_706185240 = setTimeout(ID_706185240_func, 1000 * 0.21);
		}
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			/** 
			 * Obtenemos datos para selectores (solo los de este pais) 
			 */
			var consultarModelo14_i = Alloy.createCollection('monedas');
			var consultarModelo14_i_where = 'pais_texto=\'' + pais[0].nombre + '\'';
			consultarModelo14_i.fetch({
				query: 'SELECT * FROM monedas WHERE pais_texto=\'' + pais[0].nombre + '\''
			});
			var monedas = require('helper').query2array(consultarModelo14_i);
			var datos = [];
			_.each(monedas, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'valor';
					if (llave == 'id_segured') newkey = 'id_interno';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
		} else {
			/** 
			 * Insertamos dummies 
			 */
			/** 
			 * Insertamos dummies 
			 */
			var eliminarModelo9_i = Alloy.Collections.monedas;
			var sql = "DELETE FROM " + eliminarModelo9_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo9_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo9_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo10_m = Alloy.Collections.monedas;
				var insertarModelo10_fila = Alloy.createModel('monedas', {
					nombre: String.format(L('x4148455394_traducir', 'CLP%1$s'), item.toString()),
					id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item.toString())
				});
				insertarModelo10_m.add(insertarModelo10_fila);
				insertarModelo10_fila.save();
			});
			var transformarModelo12_i = Alloy.createCollection('monedas');
			transformarModelo12_i.fetch();
			var transformarModelo12_src = require('helper').query2array(transformarModelo12_i);
			var datos = [];
			_.each(transformarModelo12_src, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'valor';
					if (llave == 'id_segured') newkey = 'id_interno';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
		}
		$.widgetModal5.data({
			data: datos
		});
	};
	var ID_1876482487 = setTimeout(ID_1876482487_func, 1000 * 0.2);

}

function Abrir_widgetModal5(e) {

	var evento = e;
	$.widgetDpicker2.cerrar({});

}

function Respuesta_widgetModal5(e) {

	var evento = e;
	/** 
	 * Mostramos el valor seleccionado desde el widget 
	 */
	$.widgetModal5.labels({
		valor: evento.valor
	});
	/** 
	 * Actualizamos la tabla ingresando el id de moneda 
	 */
	$.contenido.set({
		id_moneda: evento.item.id_interno
	});
	if ('contenido' in $) $contenido = $.contenido.toJSON();

}

function Change_campo4(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	$.contenido.set({
		valor: elemento
	});
	if ('contenido' in $) $contenido = $.contenido.toJSON();
	elemento = null, source = null;

}

function Return_campo4(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	$.campo4.blur();
	elemento = null, source = null;

}

function Change_ID_1002331086(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	if (elemento == true || elemento == 'true') {
		/** 
		 * Dependiendo del estado del switch es el texto a mostrar y actualizacion a la tabla de contenido 
		 */
		var si = L('x3746555228_traducir', 'SI');
		$.NO9.setText(si);

		$.contenido.set({
			recupero: 1
		});
		if ('contenido' in $) $contenido = $.contenido.toJSON();
	} else {
		var no = L('x3376426101_traducir', 'NO');
		$.NO9.setText(no);

		$.contenido.set({
			recupero: 0
		});
		if ('contenido' in $) $contenido = $.contenido.toJSON();
	}
	elemento = null;

}

function Change_DescribaelDao2(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	$.contenido.set({
		descripcion: elemento
	});
	if ('contenido' in $) $contenido = $.contenido.toJSON();
	elemento = null, source = null;

}

$.widgetCamaralight2.init({
	onfotolista: Fotolista_widgetCamaralight2,
	__id: 'ALL1510471997'
});

function Fotolista_widgetCamaralight2(e) {

	var evento = e;
	/** 
	 * Recuperamos cual fue la foto seleccionada 
	 */
	var cual_foto = ('cual_foto' in require('vars')) ? require('vars')['cual_foto'] : '';
	if (cual_foto == 1 || cual_foto == '1') {
		var insertarModelo11_m = Alloy.Collections.numero_unico;
		var insertarModelo11_fila = Alloy.createModel('numero_unico', {
			comentario: 'nuevo danocontenido foto1'
		});
		insertarModelo11_m.add(insertarModelo11_fila);
		insertarModelo11_fila.save();
		var nuevoid_f1 = require('helper').model2object(insertarModelo11_m.last());
		/** 
		 * Guardamos en variable el detalle del modelo numero_unico para asignar informacion de foto1 del recinto 
		 */
		require('vars')[_var_scopekey]['nuevoid_f1'] = nuevoid_f1;
		/** 
		 * Recuperamos variable para saber si estamos haciendo una inspeccion o dummy y guardar la foto en la memoria del celular 
		 */
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var ID_11717236_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
			if (ID_11717236_d.exists() == false) ID_11717236_d.createDirectory();
			var ID_11717236_f = Ti.Filesystem.getFile(ID_11717236_d.resolve(), 'cap' + nuevoid_f1.id + '.jpg');
			if (ID_11717236_f.exists() == true) ID_11717236_f.deleteFile();
			ID_11717236_f.write(evento.foto);
			ID_11717236_d = null;
			ID_11717236_f = null;
		} else {
			var ID_1613466068_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + 0);
			if (ID_1613466068_d.exists() == false) ID_1613466068_d.createDirectory();
			var ID_1613466068_f = Ti.Filesystem.getFile(ID_1613466068_d.resolve(), 'cap' + nuevoid_f1.id + '.jpg');
			if (ID_1613466068_f.exists() == true) ID_1613466068_f.deleteFile();
			ID_1613466068_f.write(evento.foto);
			ID_1613466068_d = null;
			ID_1613466068_f = null;
		}
		/** 
		 * Actualizamos el modelo indicando cual es el nombre de la imagen 
		 */
		$.contenido.set({
			foto1: String.format(L('x1070475521', 'cap%1$s.jpg'), nuevoid_f1.id.toString())
		});
		if ('contenido' in $) $contenido = $.contenido.toJSON();
		/** 
		 * Enviamos la foto para que genere la miniatura (y tambien la guarde) y muestre en pantalla 
		 */
		$.widgetFotochica4.procesar({
			imagen: evento.foto,
			nueva: 640,
			calidad: 91,
			camara: 'trasera'
		});
		/** 
		 * Limpiamos memoria ram 
		 */
		evento = null;
	} else if (cual_foto == 2) {
		var insertarModelo12_m = Alloy.Collections.numero_unico;
		var insertarModelo12_fila = Alloy.createModel('numero_unico', {
			comentario: 'nuevo danocontenido foto2'
		});
		insertarModelo12_m.add(insertarModelo12_fila);
		insertarModelo12_fila.save();
		var nuevoid_f2 = require('helper').model2object(insertarModelo12_m.last());
		require('vars')[_var_scopekey]['nuevoid_f2'] = nuevoid_f2;
		if (Ti.App.deployType != 'production') console.log(String.format(L('x2429469022', 'imagen capturada y asignada como cap%1$s.jpg'), nuevoid_f2.id.toString()), {});
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var ID_1671957263_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
			if (ID_1671957263_d.exists() == false) ID_1671957263_d.createDirectory();
			var ID_1671957263_f = Ti.Filesystem.getFile(ID_1671957263_d.resolve(), 'cap' + nuevoid_f2.id + '.jpg');
			if (ID_1671957263_f.exists() == true) ID_1671957263_f.deleteFile();
			ID_1671957263_f.write(evento.foto);
			ID_1671957263_d = null;
			ID_1671957263_f = null;
		} else {
			var ID_156849599_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + 0);
			if (ID_156849599_d.exists() == false) ID_156849599_d.createDirectory();
			var ID_156849599_f = Ti.Filesystem.getFile(ID_156849599_d.resolve(), 'cap' + nuevoid_f2.id + '.jpg');
			if (ID_156849599_f.exists() == true) ID_156849599_f.deleteFile();
			ID_156849599_f.write(evento.foto);
			ID_156849599_d = null;
			ID_156849599_f = null;
		}
		$.contenido.set({
			foto2: String.format(L('x1070475521', 'cap%1$s.jpg'), nuevoid_f2.id.toString())
		});
		if ('contenido' in $) $contenido = $.contenido.toJSON();
		$.widgetFotochica5.procesar({
			imagen: evento.foto,
			nueva: 640,
			calidad: 91,
			camara: 'trasera'
		});
		evento = null;
	} else if (cual_foto == 3) {
		var insertarModelo13_m = Alloy.Collections.numero_unico;
		var insertarModelo13_fila = Alloy.createModel('numero_unico', {
			comentario: 'nuevo danocontenido foto3'
		});
		insertarModelo13_m.add(insertarModelo13_fila);
		insertarModelo13_fila.save();
		var nuevoid_f3 = require('helper').model2object(insertarModelo13_m.last());
		require('vars')[_var_scopekey]['nuevoid_f3'] = nuevoid_f3;
		if (Ti.App.deployType != 'production') console.log(String.format(L('x2429469022', 'imagen capturada y asignada como cap%1$s.jpg'), nuevoid_f3.id.toString()), {});
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var ID_1785670176_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
			if (ID_1785670176_d.exists() == false) ID_1785670176_d.createDirectory();
			var ID_1785670176_f = Ti.Filesystem.getFile(ID_1785670176_d.resolve(), 'cap' + nuevoid_f3.id + '.jpg');
			if (ID_1785670176_f.exists() == true) ID_1785670176_f.deleteFile();
			ID_1785670176_f.write(evento.foto);
			ID_1785670176_d = null;
			ID_1785670176_f = null;
		} else {
			var ID_1350886191_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + 0);
			if (ID_1350886191_d.exists() == false) ID_1350886191_d.createDirectory();
			var ID_1350886191_f = Ti.Filesystem.getFile(ID_1350886191_d.resolve(), 'cap' + nuevoid_f3.id + '.jpg');
			if (ID_1350886191_f.exists() == true) ID_1350886191_f.deleteFile();
			ID_1350886191_f.write(evento.foto);
			ID_1350886191_d = null;
			ID_1350886191_f = null;
		}
		$.contenido.set({
			foto3: String.format(L('x1070475521', 'cap%1$s.jpg'), nuevoid_f3.id.toString())
		});
		if ('contenido' in $) $contenido = $.contenido.toJSON();
		$.widgetFotochica6.procesar({
			imagen: evento.foto,
			nueva: 640,
			calidad: 91,
			camara: 'trasera'
		});
		evento = null;
	}
}

$.widgetDpicker2.init({
	__id: 'ALL713928716',
	aceptar: L('x1518866076_traducir', 'Aceptar'),
	cancelar: L('x2353348866_traducir', 'Cancelar'),
	onaceptar: Aceptar_widgetDpicker2,
	oncancelar: Cancelar_widgetDpicker2
});

function Aceptar_widgetDpicker2(e) {

	var evento = e;
	/** 
	 * Formateamos la fecha que responde el widget, mostramos en pantalla y actualizamos la fecha de compra 
	 */
	var moment = require('alloy/moment');
	var formatearFecha2 = evento.valor;
	var resp = moment(formatearFecha2).format('DD-MM-YYYY');
	$.Fecha5.setText(resp);

	var Fecha5_estilo = 'estilo12';

	var _tmp_a4w = require('a4w');
	if ((typeof Fecha5_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (Fecha5_estilo in _tmp_a4w.styles['classes'])) {
		try {
			Fecha5_estilo = _tmp_a4w.styles['classes'][Fecha5_estilo];
		} catch (st_val_err) {}
	}
	_tmp_a4w = null;
	$.Fecha5.applyProperties(Fecha5_estilo);

	$.contenido.set({
		fecha_compra: evento.valor
	});
	if ('contenido' in $) $contenido = $.contenido.toJSON();

}

function Cancelar_widgetDpicker2(e) {

	var evento = e;
	if (Ti.App.deployType != 'production') console.log('selector de fecha cancelado', {});

}

(function() {
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
		/** 
		 * Actualizamos el id de la inspeccion en la tabla de contenidos 
		 */
		$.contenido.set({
			id_inspeccion: seltarea.id_server
		});
		if ('contenido' in $) $contenido = $.contenido.toJSON();
	}
	/** 
	 * heredamos datos previos de nivel en modelo nuevo con binding de esta pantalla 
	 */
	var consultarModelo15_i = Alloy.createCollection('insp_contenido');
	var consultarModelo15_i_where = 'id=\'' + args._dato.id + '\'';
	consultarModelo15_i.fetch({
		query: 'SELECT * FROM insp_contenido WHERE id=\'' + args._dato.id + '\''
	});
	var insp_c = require('helper').query2array(consultarModelo15_i);
	/** 
	 * Cargamos los datos en la tabla 
	 */
	$.contenido.set({
		id_moneda: insp_c[0].id_moneda,
		fecha_compra: insp_c[0].fecha_compra,
		id_inspeccion: insp_c[0].id_inspeccion,
		nombre: insp_c[0].nombre,
		cantidad: insp_c[0].cantidad,
		foto1: insp_c[0].foto1,
		id_nombre: insp_c[0].id_nombre,
		foto2: insp_c[0].foto2,
		id_recinto: insp_c[0].id_recinto,
		id_marca: insp_c[0].id_marca,
		foto3: insp_c[0].foto3,
		recupero: insp_c[0].recupero,
		valor: insp_c[0].valor,
		descripcion: insp_c[0].descripcion
	});
	if ('contenido' in $) $contenido = $.contenido.toJSON();
	var moment = require('alloy/moment');
	var formatearFecha3 = insp_c[0].fecha_compra;
	var fecha_compra = moment(formatearFecha3).format('DD-MM-YYYY');
	/** 
	 * Cargamos textos en la pantalla 
	 */
	var Fecha5_estilo = 'estilo12';

	var _tmp_a4w = require('a4w');
	if ((typeof Fecha5_estilo == 'string' && 'styles' in _tmp_a4w && 'classes' in _tmp_a4w.styles) && (Fecha5_estilo in _tmp_a4w.styles['classes'])) {
		try {
			Fecha5_estilo = _tmp_a4w.styles['classes'][Fecha5_estilo];
		} catch (st_val_err) {}
	}
	_tmp_a4w = null;
	$.Fecha5.applyProperties(Fecha5_estilo);

	$.Fecha5.setText(fecha_compra);

	$.SeleccioneProducto2.setText(insp_c[0].nombre);

	$.SeleccioneProducto2.setColor('#000000');

	$.campo3.setValue(insp_c[0].cantidad);

	$.campo4.setValue(insp_c[0].valor);

	$.DescribaelDao2.setValue(insp_c[0].descripcion);

	if (insp_c[0].recupero == 1 || insp_c[0].recupero == '1') {
		/** 
		 * Modificamos el estado del switch 
		 */
		$.ID_1002331086.setValue(true);

		$.NO9.setText('SI');

	} else {
		$.ID_1002331086.setValue('false');

		$.NO9.setText('NO');

	}
	if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
		/** 
		 * Verificamos si las fotos estan en la carpeta de inspeccion o es un dummy 
		 */
		var ID_150567229_trycatch = {
			error: function(e) {
				if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
			}
		};
		try {
			ID_150567229_trycatch.error = function(evento) {};
			insp_c[0].foto1 = "mini" + insp_c[0].foto1.substring(3);
			var foto_1 = '';
			var ID_1235579640_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + seltarea.id_server);
			if (ID_1235579640_d.exists() == true) {
				var ID_1235579640_f = Ti.Filesystem.getFile(ID_1235579640_d.resolve(), insp_c[0].foto1);
				if (ID_1235579640_f.exists() == true) {
					foto_1 = ID_1235579640_f.read();
				}
				ID_1235579640_f = null;
			}
			ID_1235579640_d = null;
		} catch (e) {
			ID_150567229_trycatch.error(e);
		}
		var ID_587453623_trycatch = {
			error: function(e) {
				if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
			}
		};
		try {
			ID_587453623_trycatch.error = function(evento) {};
			insp_c[0].foto2 = "mini" + insp_c[0].foto2.substring(3);
			var foto_2 = '';
			var ID_1749644865_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + seltarea.id_server);
			if (ID_1749644865_d.exists() == true) {
				var ID_1749644865_f = Ti.Filesystem.getFile(ID_1749644865_d.resolve(), insp_c[0].foto2);
				if (ID_1749644865_f.exists() == true) {
					foto_2 = ID_1749644865_f.read();
				}
				ID_1749644865_f = null;
			}
			ID_1749644865_d = null;
		} catch (e) {
			ID_587453623_trycatch.error(e);
		}
		var ID_157007749_trycatch = {
			error: function(e) {
				if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
			}
		};
		try {
			ID_157007749_trycatch.error = function(evento) {};
			insp_c[0].foto3 = "mini" + insp_c[0].foto3.substring(3);
			var foto_3 = '';
			var ID_61377515_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + seltarea.id_server);
			if (ID_61377515_d.exists() == true) {
				var ID_61377515_f = Ti.Filesystem.getFile(ID_61377515_d.resolve(), insp_c[0].foto3);
				if (ID_61377515_f.exists() == true) {
					foto_3 = ID_61377515_f.read();
				}
				ID_61377515_f = null;
			}
			ID_61377515_d = null;
		} catch (e) {
			ID_157007749_trycatch.error(e);
		}
	} else {
		var ID_1956266938_trycatch = {
			error: function(e) {
				if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
			}
		};
		try {
			ID_1956266938_trycatch.error = function(evento) {};
			insp_c[0].foto1 = "mini" + insp_c[0].foto1.substring(3);
			var foto_1 = '';
			var ID_216147007_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + 0);
			if (ID_216147007_d.exists() == true) {
				var ID_216147007_f = Ti.Filesystem.getFile(ID_216147007_d.resolve(), insp_c[0].foto1);
				if (ID_216147007_f.exists() == true) {
					foto_1 = ID_216147007_f.read();
				}
				ID_216147007_f = null;
			}
			ID_216147007_d = null;
		} catch (e) {
			ID_1956266938_trycatch.error(e);
		}
		var ID_742064338_trycatch = {
			error: function(e) {
				if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
			}
		};
		try {
			ID_742064338_trycatch.error = function(evento) {};
			insp_c[0].foto2 = "mini" + insp_c[0].foto2.substring(3);
			var foto_2 = '';
			var ID_269412720_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + 0);
			if (ID_269412720_d.exists() == true) {
				var ID_269412720_f = Ti.Filesystem.getFile(ID_269412720_d.resolve(), insp_c[0].foto2);
				if (ID_269412720_f.exists() == true) {
					foto_2 = ID_269412720_f.read();
				}
				ID_269412720_f = null;
			}
			ID_269412720_d = null;
		} catch (e) {
			ID_742064338_trycatch.error(e);
		}
		var ID_1669864641_trycatch = {
			error: function(e) {
				if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
			}
		};
		try {
			ID_1669864641_trycatch.error = function(evento) {};
			insp_c[0].foto3 = "mini" + insp_c[0].foto3.substring(3);
			var foto_3 = '';
			var ID_1551657778_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + 0);
			if (ID_1551657778_d.exists() == true) {
				var ID_1551657778_f = Ti.Filesystem.getFile(ID_1551657778_d.resolve(), insp_c[0].foto3);
				if (ID_1551657778_f.exists() == true) {
					foto_3 = ID_1551657778_f.read();
				}
				ID_1551657778_f = null;
			}
			ID_1551657778_d = null;
		} catch (e) {
			ID_1669864641_trycatch.error(e);
		}
	}
	if ((_.isObject(foto_1) || (_.isString(foto_1)) && !_.isEmpty(foto_1)) || _.isNumber(foto_1) || _.isBoolean(foto_1)) {
		/** 
		 * Revisamos si las fotos existen, y modificamos la imagen en la vista para cargar la que esta en la memoria del equipo 
		 */
		var vista50_visible = true;

		if (vista50_visible == 'si') {
			vista50_visible = true;
		} else if (vista50_visible == 'no') {
			vista50_visible = false;
		}
		$.vista50.setVisible(vista50_visible);

		var imagen2_imagen = foto_1;

		if (typeof imagen2_imagen == 'string' && 'styles' in require('a4w') && imagen2_imagen in require('a4w').styles['images']) {
			imagen2_imagen = require('a4w').styles['images'][imagen2_imagen];
		}
		$.imagen2.setImage(imagen2_imagen);

		if ((Ti.Platform.manufacturer) == L('x3791547856_traducir', 'samsung')) {
			var vista50_rotar = 90;

			var setRotacion = function(angulo) {
				$.vista50.setTransform(Ti.UI.create2DMatrix().rotate(angulo));
			};
			setRotacion(vista50_rotar);

		}
	}
	if ((_.isObject(foto_2) || (_.isString(foto_2)) && !_.isEmpty(foto_2)) || _.isNumber(foto_2) || _.isBoolean(foto_2)) {
		var vista51_visible = true;

		if (vista51_visible == 'si') {
			vista51_visible = true;
		} else if (vista51_visible == 'no') {
			vista51_visible = false;
		}
		$.vista51.setVisible(vista51_visible);

		var imagen3_imagen = foto_2;

		if (typeof imagen3_imagen == 'string' && 'styles' in require('a4w') && imagen3_imagen in require('a4w').styles['images']) {
			imagen3_imagen = require('a4w').styles['images'][imagen3_imagen];
		}
		$.imagen3.setImage(imagen3_imagen);

		if ((Ti.Platform.manufacturer) == L('x3791547856_traducir', 'samsung')) {
			var vista51_rotar = 90;

			var setRotacion = function(angulo) {
				$.vista51.setTransform(Ti.UI.create2DMatrix().rotate(angulo));
			};
			setRotacion(vista51_rotar);

		}
	}
	if ((_.isObject(foto_3) || (_.isString(foto_3)) && !_.isEmpty(foto_3)) || _.isNumber(foto_3) || _.isBoolean(foto_3)) {
		var vista52_visible = true;

		if (vista52_visible == 'si') {
			vista52_visible = true;
		} else if (vista52_visible == 'no') {
			vista52_visible = false;
		}
		$.vista52.setVisible(vista52_visible);

		var imagen4_imagen = foto_3;

		if (typeof imagen4_imagen == 'string' && 'styles' in require('a4w') && imagen4_imagen in require('a4w').styles['images']) {
			imagen4_imagen = require('a4w').styles['images'][imagen4_imagen];
		}
		$.imagen4.setImage(imagen4_imagen);

		if ((Ti.Platform.manufacturer) == L('x3791547856_traducir', 'samsung')) {
			var vista52_rotar = 90;

			var setRotacion = function(angulo) {
				$.vista52.setTransform(Ti.UI.create2DMatrix().rotate(angulo));
			};
			setRotacion(vista52_rotar);

		}
	}
	/** 
	 * Limpiamos variables 
	 */
	foto_1 = null;
	foto_2 = null;
	foto_3 = null;
	/** 
	 * Dejamos el scroll en el top y desenfocamos los campos de texto 
	 */
	var ID_896487059_func = function() {
		$.scroll2.scrollToTop();
		$.DescribaelDao2.blur();
		$.campo4.blur();
		$.campo3.blur();
	};
	var ID_896487059 = setTimeout(ID_896487059_func, 1000 * 0.2);
	/** 
	 * Modificamos el statusbar 
	 */
	var ID_460312060_func = function() {
		var editarcontenido_statusbar = '#239EC4';

		var setearStatusColor = function(editarcontenido_statusbar) {
			if (OS_IOS) {
				if (editarcontenido_statusbar == 'light' || editarcontenido_statusbar == 'claro') {
					$.editarcontenido_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.LIGHT_CONTENT);
				} else if (editarcontenido_statusbar == 'grey' || editarcontenido_statusbar == 'gris' || editarcontenido_statusbar == 'gray') {
					$.editarcontenido_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.GREY);
				} else if (editarcontenido_statusbar == 'oscuro' || editarcontenido_statusbar == 'dark') {
					$.editarcontenido_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.DEFAULT);
				}
			} else if (OS_ANDROID) {
				abx.setStatusbarColor(editarcontenido_statusbar);
			}
		};
		setearStatusColor(editarcontenido_statusbar);

	};
	var ID_460312060 = setTimeout(ID_460312060_func, 1000 * 0.1);
	_my_events['resp_dato1,ID_149158092'] = function(evento) {
		if (Ti.App.deployType != 'production') console.log('detalle de la respuesta', {
			"datos": evento
		});
		$.SeleccioneProducto2.setText(evento.nombre);

		$.SeleccioneProducto2.setColor('#000000');

		/** 
		 * Asumimos que el valor mostrado seleccionado es el nombre de la fila dano, ya que no existe el campo nombre en esta pantalla. 
		 */
		$.contenido.set({
			nombre: evento.nombre,
			id_nombre: evento.id_segured
		});
		if ('contenido' in $) $contenido = $.contenido.toJSON();
	};
	Alloy.Events.on('resp_dato1', _my_events['resp_dato1,ID_149158092']);
})();

function Postlayout_editarcontenido(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var ID_575874735_func = function() {
		/** 
		 * Una vez ya cargada la tarea, dejamos la variable limpia para poder abrir otras pantallas 
		 */
		require('vars')['var_abriendo'] = '';
	};
	var ID_575874735 = setTimeout(ID_575874735_func, 1000 * 0.2);

}