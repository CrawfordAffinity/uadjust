var _bind4section = {};
var _list_templates = {
	"elemento": {
		"vista2": {},
		"Label3": {
			"text": "{nombre}"
		},
		"Label2": {
			"text": "{id_server}"
		},
		"Label": {
			"text": "{id_segured}"
		},
		"vista": {}
	}
};
var $danos1 = $.danos1.toJSON();

var _activity;
if (OS_ANDROID) {
	_activity = $.nuevodano.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'nuevodano';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.nuevodano.addEventListener('open', function(e) {});
}
$.nuevodano.orientationModes = [Titanium.UI.PORTRAIT];


$.widgetBarra.init({
	titulo: L('x2435611700_traducir', 'NUEVO DAÑO'),
	__id: 'ALL758725651',
	oncerrar: Cerrar_widgetBarra,
	textoderecha: L('x2943883035_traducir', 'Guardar'),
	fondo: 'fondomorado',
	top: 0,
	modal: L('', ''),
	onpresiono: Presiono_widgetBarra,
	colortextoderecha: 'blanco'
});

function Cerrar_widgetBarra(e) {

	var evento = e;
	$.nuevodano.close();
	/** 
	 * Limpiamos widget 
	 */
	$.widgetModal.limpiar({});
	$.widgetModal2.limpiar({});

}

function Presiono_widgetBarra(e) {

	var evento = e;
	if (_.isUndefined($danos1.id_partida)) {
		/** 
		 * Validamos que lo ingresado sea correcto 
		 */
		var preguntarAlerta_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x1349590523_traducir', 'Seleccione partida'),
			buttonNames: preguntarAlerta_opts
		});
		preguntarAlerta.addEventListener('click', function(e) {
			var nulo = preguntarAlerta_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta.show();
	} else if ((_.isObject($danos1.id_tipodano) || _.isString($danos1.id_tipodano)) && _.isEmpty($danos1.id_tipodano)) {
		require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
		var preguntarAlerta2_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta2 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x2702600033_traducir', 'Seleccione el tipo de daño'),
			buttonNames: preguntarAlerta2_opts
		});
		preguntarAlerta2.addEventListener('click', function(e) {
			var nulo = preguntarAlerta2_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta2.show();
	} else if ((_.isObject($danos1.id_unidadmedida) || _.isString($danos1.id_unidadmedida)) && _.isEmpty($danos1.id_unidadmedida)) {
		require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
		var preguntarAlerta3_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta3 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x4202932850_traducir', 'Seleccione la unidad de medida del daño'),
			buttonNames: preguntarAlerta3_opts
		});
		preguntarAlerta3.addEventListener('click', function(e) {
			var nulo = preguntarAlerta3_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta3.show();
	} else if (_.isUndefined($danos1.id_tipodano)) {
		var preguntarAlerta4_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta4 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x2702600033_traducir', 'Seleccione el tipo de daño'),
			buttonNames: preguntarAlerta4_opts
		});
		preguntarAlerta4.addEventListener('click', function(e) {
			var nulo = preguntarAlerta4_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta4.show();
	} else if (_.isUndefined($danos1.id_unidadmedida)) {
		var preguntarAlerta5_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta5 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x4202932850_traducir', 'Seleccione la unidad de medida del daño'),
			buttonNames: preguntarAlerta5_opts
		});
		preguntarAlerta5.addEventListener('click', function(e) {
			var nulo = preguntarAlerta5_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta5.show();
	} else if ((_.isObject($danos1.superficie) || _.isString($danos1.superficie)) && _.isEmpty($danos1.superficie)) {
		require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
		var preguntarAlerta6_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta6 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x1057642944_traducir', 'Ingrese la cubicación del daño'),
			buttonNames: preguntarAlerta6_opts
		});
		preguntarAlerta6.addEventListener('click', function(e) {
			var nulo = preguntarAlerta6_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta6.show();
	} else if ($danos1.superficie.length == 0 || $danos1.superficie.length == '0') {
		require('vars')[_var_scopekey]['todobien'] = L('x734881840_traducir', 'false');
		var preguntarAlerta7_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta7 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x1057642944_traducir', 'Ingrese la cubicación del daño'),
			buttonNames: preguntarAlerta7_opts
		});
		preguntarAlerta7.addEventListener('click', function(e) {
			var nulo = preguntarAlerta7_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta7.show();
	} else if (_.isUndefined($danos1.superficie)) {
		var preguntarAlerta8_opts = [L('x1518866076_traducir', 'Aceptar')];
		var preguntarAlerta8 = Ti.UI.createAlertDialog({
			title: L('x3237162386_traducir', 'Atencion'),
			message: L('x1057642944_traducir', 'Ingrese la cubicación del daño'),
			buttonNames: preguntarAlerta8_opts
		});
		preguntarAlerta8.addEventListener('click', function(e) {
			var nulo = preguntarAlerta8_opts[e.index];
			nulo = null;
			e.source.removeEventListener("click", arguments.callee);
		});
		preguntarAlerta8.show();
	} else {
		/** 
		 * Desenfocamos campos de texto 
		 */
		$.DescribaelDao.blur();
		var ID_1804013171_func = function() {
			/** 
			 * Guardamos en modelo el dano ingresado 
			 */
			Alloy.Collections[$.danos1.config.adapter.collection_name].add($.danos1);
			$.danos1.save();
			Alloy.Collections[$.danos1.config.adapter.collection_name].fetch();
			/** 
			 * Limpiamos widget 
			 */
			$.widgetModal.limpiar({});
			$.widgetModal2.limpiar({});
			$.nuevodano.close();
		};
		var ID_1804013171 = setTimeout(ID_1804013171_func, 1000 * 0.2);
	}
}

function Click_vista4(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	/** 
	 * Ocupamos esta variable para impedir que la pantalla siguente se cargue dos veces 
	 */
	var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
	if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
		require('vars')['var_abriendo'] = L('x4261170317', 'true');
		Alloy.createController("listadodanos_index", {}).getView().open();
	}

}

$.widgetModal2.init({
	titulo: L('x1384515306_traducir', 'TIPO DAÑO'),
	cargando: L('x1831148736', 'cargando...'),
	__id: 'ALL1413999243',
	left: 0,
	onrespuesta: Respuesta_widgetModal2,
	campo: L('x953397154_traducir', 'Tipo de Daño'),
	onabrir: Abrir_widgetModal2,
	color: 'morado',
	right: 0,
	seleccione: L('x4123108455_traducir', 'seleccione tipo'),
	activo: true,
	onafterinit: Afterinit_widgetModal2
});

function Abrir_widgetModal2(e) {

	var evento = e;

}

function Respuesta_widgetModal2(e) {

	var evento = e;
	/** 
	 * Mostramos el valor seleccionado desde el widget 
	 */
	$.widgetModal2.labels({
		valor: evento.valor
	});
	/** 
	 * Actualizamos el id_tipodano e id_unidadmedida 
	 */
	$.danos1.set({
		id_tipodano: evento.item.id_interno,
		id_unidadmedida: ''
	});
	if ('danos1' in $) $danos1 = $.danos1.toJSON();
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
		/** 
		 * Consultamos id_gerardo en tipo_accion, para obtener unidades de medida 
		 */
		var consultarModelo_i = Alloy.createCollection('tipo_accion');
		var consultarModelo_i_where = 'id_tipo_dano=\'' + evento.item.id_gerardo + '\'';
		consultarModelo_i.fetch({
			query: 'SELECT * FROM tipo_accion WHERE id_tipo_dano=\'' + evento.item.id_gerardo + '\''
		});
		var acciones = require('helper').query2array(consultarModelo_i);
		if (acciones && acciones.length) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			var consultarModelo2_i = Alloy.createCollection('unidad_medida');
			var consultarModelo2_i_where = 'pais_texto=\'' + pais[0].nombre + '\' AND id_accion=\'' + acciones[0].id_server + '\'';
			consultarModelo2_i.fetch({
				query: 'SELECT * FROM unidad_medida WHERE pais_texto=\'' + pais[0].nombre + '\' AND id_accion=\'' + acciones[0].id_server + '\''
			});
			var unidad = require('helper').query2array(consultarModelo2_i);
			var datos = [];
			_.each(unidad, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'valor';
					if (llave == 'id_segured') newkey = 'id_interno';
					if (llave == 'id_server') newkey = 'id_gerardo';
					if (llave == 'abrev') newkey = 'abrev';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
			$.widgetModal.data({
				data: datos
			});
		}
	}
	/** 
	 * Actualizamos texto de widget unidad de medida 
	 */
	$.widgetModal.labels({
		seleccione: 'unidad'
	});
	$.campo.setValue('');

	$.label.setText('-');


}

function Afterinit_widgetModal2(e) {

	var evento = e;
	var ID_1686389527_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			/** 
			 * Obtenemos datos para selectores (solo los de este pais) 
			 */
			var consultarModelo3_i = Alloy.createCollection('tipo_dano');
			var consultarModelo3_i_where = 'pais_texto=\'' + pais[0].nombre + '\' AND id_partida=0';
			consultarModelo3_i.fetch({
				query: 'SELECT * FROM tipo_dano WHERE pais_texto=\'' + pais[0].nombre + '\' AND id_partida=0'
			});
			var danos = require('helper').query2array(consultarModelo3_i);
			var datos = [];
			_.each(danos, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'valor';
					if (llave == 'id_segured') newkey = 'id_interno';
					if (llave == 'id_server') newkey = 'id_gerardo';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
		} else {
			/** 
			 * Insertamos dummies 
			 */
			/** 
			 * Insertamos dummies 
			 */
			var eliminarModelo_i = Alloy.Collections.tipo_dano;
			var sql = "DELETE FROM " + eliminarModelo_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo_m = Alloy.Collections.tipo_dano;
				var insertarModelo_fila = Alloy.createModel('tipo_dano', {
					nombre: String.format(L('x1478811929_traducir', 'Picada%1$s'), item.toString()),
					id_partida: 0,
					id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
					id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item.toString()),
					pais: 1
				});
				insertarModelo_m.add(insertarModelo_fila);
				insertarModelo_fila.save();
			});
			var transformarModelo3_i = Alloy.createCollection('tipo_dano');
			transformarModelo3_i.fetch();
			var transformarModelo3_src = require('helper').query2array(transformarModelo3_i);
			var datos = [];
			_.each(transformarModelo3_src, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'valor';
					if (llave == 'id_segured') newkey = 'id_interno';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
		}
		$.widgetModal2.data({
			data: datos
		});
	};
	var ID_1686389527 = setTimeout(ID_1686389527_func, 1000 * 0.2);

}

$.widgetFotochica.init({
	caja: 45,
	__id: 'ALL1070295433',
	onlisto: Listo_widgetFotochica,
	left: 0,
	top: 0,
	onclick: Click_widgetFotochica
});

function Click_widgetFotochica(e) {

	var evento = e;
	/** 
	 * Definimos que estamos capturando foto en el primer item 
	 */
	require('vars')[_var_scopekey]['cual_foto'] = L('x2212294583', '1');
	/** 
	 * Abrimos camara 
	 */
	$.widgetCamaralight.disparar({});
	/** 
	 * Detenemos las animaciones de los widget 
	 */
	$.widgetFotochica2.detener({});
	$.widgetFotochica3.detener({});
	$.widgetFotochica.detener({});

}

function Listo_widgetFotochica(e) {

	var evento = e;
	/** 
	 * Recuperamos variables para poder definir la estructura de las carpetas donde se guardara la miniatura 
	 */
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	var nuevoid_f1 = ('nuevoid_f1' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['nuevoid_f1'] : '';
	if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
		var ID_1482108514_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + seltarea.id_server);
		if (ID_1482108514_d.exists() == false) ID_1482108514_d.createDirectory();
		var ID_1482108514_f = Ti.Filesystem.getFile(ID_1482108514_d.resolve(), 'mini' + nuevoid_f1.id + '.jpg');
		if (ID_1482108514_f.exists() == true) ID_1482108514_f.deleteFile();
		ID_1482108514_f.write(evento.mini);
		ID_1482108514_d = null;
		ID_1482108514_f = null;
	} else {
		var ID_1791326058_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + 0);
		if (ID_1791326058_d.exists() == false) ID_1791326058_d.createDirectory();
		var ID_1791326058_f = Ti.Filesystem.getFile(ID_1791326058_d.resolve(), 'mini' + nuevoid_f1.id + '.jpg');
		if (ID_1791326058_f.exists() == true) ID_1791326058_f.deleteFile();
		ID_1791326058_f.write(evento.mini);
		ID_1791326058_d = null;
		ID_1791326058_f = null;
	}
}

$.widgetFotochica2.init({
	caja: 45,
	__id: 'ALL1464141874',
	onlisto: Listo_widgetFotochica2,
	left: 5,
	top: L('x4108050209', '0'),
	onclick: Click_widgetFotochica2
});

function Click_widgetFotochica2(e) {

	var evento = e;
	require('vars')[_var_scopekey]['cual_foto'] = 2;
	$.widgetCamaralight.disparar({});
	$.widgetFotochica.detener({});
	$.widgetFotochica3.detener({});
	$.widgetFotochica2.detener({});

}

function Listo_widgetFotochica2(e) {

	var evento = e;
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	var nuevoid_f2 = ('nuevoid_f2' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['nuevoid_f2'] : '';
	if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
		var ID_1070774099_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + seltarea.id_server);
		if (ID_1070774099_d.exists() == false) ID_1070774099_d.createDirectory();
		var ID_1070774099_f = Ti.Filesystem.getFile(ID_1070774099_d.resolve(), 'mini' + nuevoid_f2.id + '.jpg');
		if (ID_1070774099_f.exists() == true) ID_1070774099_f.deleteFile();
		ID_1070774099_f.write(evento.mini);
		ID_1070774099_d = null;
		ID_1070774099_f = null;
	} else {
		var ID_1933980157_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + 0);
		if (ID_1933980157_d.exists() == false) ID_1933980157_d.createDirectory();
		var ID_1933980157_f = Ti.Filesystem.getFile(ID_1933980157_d.resolve(), 'mini' + nuevoid_f2.id + '.jpg');
		if (ID_1933980157_f.exists() == true) ID_1933980157_f.deleteFile();
		ID_1933980157_f.write(evento.mini);
		ID_1933980157_d = null;
		ID_1933980157_f = null;
	}
}

$.widgetFotochica3.init({
	caja: L('x2889884971_traducir', '45'),
	__id: 'ALL437144584',
	onlisto: Listo_widgetFotochica3,
	left: 5,
	top: L('x4108050209', '0'),
	onclick: Click_widgetFotochica3
});

function Click_widgetFotochica3(e) {

	var evento = e;
	require('vars')[_var_scopekey]['cual_foto'] = 3;
	$.widgetCamaralight.disparar({});
	$.widgetFotochica.detener({});
	$.widgetFotochica2.detener({});
	$.widgetFotochica3.detener({});

}

function Listo_widgetFotochica3(e) {

	var evento = e;
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	var nuevoid_f3 = ('nuevoid_f3' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['nuevoid_f3'] : '';
	if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
		var ID_1536703765_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + seltarea.id_server);
		if (ID_1536703765_d.exists() == false) ID_1536703765_d.createDirectory();
		var ID_1536703765_f = Ti.Filesystem.getFile(ID_1536703765_d.resolve(), 'mini' + nuevoid_f3.id + '.jpg');
		if (ID_1536703765_f.exists() == true) ID_1536703765_f.deleteFile();
		ID_1536703765_f.write(evento.mini);
		ID_1536703765_d = null;
		ID_1536703765_f = null;
	} else {
		var ID_37771523_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'miniaturas' + 0);
		if (ID_37771523_d.exists() == false) ID_37771523_d.createDirectory();
		var ID_37771523_f = Ti.Filesystem.getFile(ID_37771523_d.resolve(), 'mini' + nuevoid_f3.id + '.jpg');
		if (ID_37771523_f.exists() == true) ID_37771523_f.deleteFile();
		ID_37771523_f.write(evento.mini);
		ID_37771523_d = null;
		ID_37771523_f = null;
	}
}

$.widgetModal.init({
	titulo: L('x52303785_traducir', 'UNIDAD'),
	cargando: L('x1831148736', 'cargando...'),
	__id: 'ALL1908023149',
	onrespuesta: Respuesta_widgetModal,
	campo: L('x2054670809_traducir', 'Unidad de medida'),
	onabrir: Abrir_widgetModal,
	color: 'morado',
	seleccione: L('x4091990063_traducir', 'unidad'),
	activo: true,
	onafterinit: Afterinit_widgetModal
});

function Abrir_widgetModal(e) {

	var evento = e;

}

function Respuesta_widgetModal(e) {

	var evento = e;
	/** 
	 * Mostramos el valor seleccionado desde el widget 
	 */
	$.widgetModal.labels({
		valor: evento.valor
	});
	/** 
	 * Actualizamos el id de la unidad de medida 
	 */
	$.danos1.set({
		id_unidadmedida: evento.item.id_interno
	});
	if ('danos1' in $) $danos1 = $.danos1.toJSON();
	/** 
	 * modifica el label con la abreviatura de la unidad de medida seleccionada. 
	 */
	$.label.setText(evento.item.abrev);


}

function Afterinit_widgetModal(e) {

	var evento = e;
	var ID_631716302_func = function() {
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			/** 
			 * Viene de login 
			 */
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			var consultarModelo4_i = Alloy.createCollection('unidad_medida');
			var consultarModelo4_i_where = 'pais_texto=\'' + pais[0].nombre + '\' AND id_accion=0';
			consultarModelo4_i.fetch({
				query: 'SELECT * FROM unidad_medida WHERE pais_texto=\'' + pais[0].nombre + '\' AND id_accion=0'
			});
			var unidad = require('helper').query2array(consultarModelo4_i);
			/** 
			 * obtenemos datos para selectores (solo los de este pais) 
			 */
			var datos = [];
			_.each(unidad, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'valor';
					if (llave == 'id_segured') newkey = 'id_interno';
					if (llave == 'id_server') newkey = 'id_gerardo';
					if (llave == 'abrev') newkey = 'abrev';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
		} else {
			/** 
			 * Insertamos dummies 
			 */
			/** 
			 * Insertamos dummies 
			 */
			var eliminarModelo2_i = Alloy.Collections.unidad_medida;
			var sql = "DELETE FROM " + eliminarModelo2_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo2_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo2_i.trigger('remove');
			var item_index = 0;
			_.each('1,2,3,4'.split(','), function(item, item_pos, item_list) {
				item_index += 1;
				var insertarModelo2_m = Alloy.Collections.unidad_medida;
				var insertarModelo2_fila = Alloy.createModel('unidad_medida', {
					nombre: String.format(L('x2542530260_traducir', 'Metro lineal%1$s'), item.toString()),
					id_server: String.format(L('x1290178835_traducir', '0%1$s'), item.toString()),
					id_segured: String.format(L('x1125471167_traducir', '10%1$s'), item.toString()),
					pais: 1,
					abrev: String.format(L('x339551322_traducir', 'ML%1$s'), item.toString())
				});
				insertarModelo2_m.add(insertarModelo2_fila);
				insertarModelo2_fila.save();
			});
			/** 
			 * Transformamos nombres de tablas 
			 */
			var transformarModelo5_i = Alloy.createCollection('unidad_medida');
			transformarModelo5_i.fetch();
			var transformarModelo5_src = require('helper').query2array(transformarModelo5_i);
			var datos = [];
			_.each(transformarModelo5_src, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'valor';
					if (llave == 'id_segured') newkey = 'id_interno';
					if (llave == 'id_server') newkey = 'id_gerardo';
					if (llave == 'abrev') newkey = 'abrev';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
		}
		/** 
		 * Cargamos el widget con los datos de tipo de unidad de medida 
		 */
		$.widgetModal.data({
			data: datos
		});
	};
	var ID_631716302 = setTimeout(ID_631716302_func, 1000 * 0.2);

}

function Change_campo(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	/** 
	 * Al escribir la descripcion, vamos actualizando en la tabla de danos la superficie 
	 */
	$.danos1.set({
		superficie: elemento
	});
	if ('danos1' in $) $danos1 = $.danos1.toJSON();
	elemento = null, source = null;

}

function Return_campo(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	/** 
	 * Al presionar enter en el teclado del telefono, desenfocamos el campo 
	 */
	$.campo.blur();
	elemento = null, source = null;

}

function Change_DescribaelDao(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	var source = e.source;
	/** 
	 * Al escribir la descripcion, vamos actualizando en la tabla de danos la descripcion del dano 
	 */
	$.danos1.set({
		descripcion_dano: elemento
	});
	if ('danos1' in $) $danos1 = $.danos1.toJSON();
	elemento = null, source = null;

}

$.widgetCamaralight.init({
	onfotolista: Fotolista_widgetCamaralight,
	__id: 'ALL143760440'
});

function Fotolista_widgetCamaralight(e) {

	var evento = e;
	if (Ti.App.deployType != 'production') console.log('funciono el evento', {
		"datos": evento.foto
	});
	/** 
	 * Revisamos en que widget se solicito la foto 
	 */
	var cual_foto = ('cual_foto' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['cual_foto'] : '';
	if (cual_foto == 1 || cual_foto == '1') {
		var insertarModelo3_m = Alloy.Collections.numero_unico;
		var insertarModelo3_fila = Alloy.createModel('numero_unico', {
			comentario: 'nuevo itemdano foto1'
		});
		insertarModelo3_m.add(insertarModelo3_fila);
		insertarModelo3_fila.save();
		var nuevoid_f1 = require('helper').model2object(insertarModelo3_m.last());
		/** 
		 * Guardamos en variable el detalle del modelo numero_unico para asignar informacion de foto1 del dano 
		 */
		require('vars')[_var_scopekey]['nuevoid_f1'] = nuevoid_f1;
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var ID_1673082978_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
			if (ID_1673082978_d.exists() == false) ID_1673082978_d.createDirectory();
			var ID_1673082978_f = Ti.Filesystem.getFile(ID_1673082978_d.resolve(), 'cap' + nuevoid_f1.id + '.jpg');
			if (ID_1673082978_f.exists() == true) ID_1673082978_f.deleteFile();
			ID_1673082978_f.write(evento.foto);
			ID_1673082978_d = null;
			ID_1673082978_f = null;
		} else {
			var ID_862806155_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + 0);
			if (ID_862806155_d.exists() == false) ID_862806155_d.createDirectory();
			var ID_862806155_f = Ti.Filesystem.getFile(ID_862806155_d.resolve(), 'cap' + nuevoid_f1.id + '.jpg');
			if (ID_862806155_f.exists() == true) ID_862806155_f.deleteFile();
			ID_862806155_f.write(evento.foto);
			ID_862806155_d = null;
			ID_862806155_f = null;
		}
		/** 
		 * Actualizamos el modelo indicando cual es el nombre de la imagen 
		 */
		$.danos1.set({
			foto1: String.format(L('x1070475521', 'cap%1$s.jpg'), nuevoid_f1.id.toString())
		});
		if ('danos1' in $) $danos1 = $.danos1.toJSON();
		/** 
		 * Enviamos la foto para que genere la miniatura (y tambien la guarde) y muestre en pantalla 
		 */
		$.widgetFotochica.procesar({
			imagen: evento.foto,
			nueva: 640,
			calidad: 91,
			camara: 'trasera'
		});
		/** 
		 * Limpiamos memoria ram 
		 */
		evento = null;
	} else if (cual_foto == 2) {
		var insertarModelo4_m = Alloy.Collections.numero_unico;
		var insertarModelo4_fila = Alloy.createModel('numero_unico', {
			comentario: 'nuevo itemdano foto2'
		});
		insertarModelo4_m.add(insertarModelo4_fila);
		insertarModelo4_fila.save();
		var nuevoid_f2 = require('helper').model2object(insertarModelo4_m.last());
		require('vars')[_var_scopekey]['nuevoid_f2'] = nuevoid_f2;
		if (Ti.App.deployType != 'production') console.log(String.format(L('x2429469022', 'imagen capturada y asignada como cap%1$s.jpg'), nuevoid_f2.id.toString()), {});
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var ID_984696000_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
			if (ID_984696000_d.exists() == false) ID_984696000_d.createDirectory();
			var ID_984696000_f = Ti.Filesystem.getFile(ID_984696000_d.resolve(), 'cap' + nuevoid_f2.id + '.jpg');
			if (ID_984696000_f.exists() == true) ID_984696000_f.deleteFile();
			ID_984696000_f.write(evento.foto);
			ID_984696000_d = null;
			ID_984696000_f = null;
		} else {
			var ID_1336465171_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + 0);
			if (ID_1336465171_d.exists() == false) ID_1336465171_d.createDirectory();
			var ID_1336465171_f = Ti.Filesystem.getFile(ID_1336465171_d.resolve(), 'cap' + nuevoid_f2.id + '.jpg');
			if (ID_1336465171_f.exists() == true) ID_1336465171_f.deleteFile();
			ID_1336465171_f.write(evento.foto);
			ID_1336465171_d = null;
			ID_1336465171_f = null;
		}
		$.danos1.set({
			foto2: String.format(L('x1070475521', 'cap%1$s.jpg'), nuevoid_f2.id.toString())
		});
		if ('danos1' in $) $danos1 = $.danos1.toJSON();
		$.widgetFotochica2.procesar({
			imagen: evento.foto,
			nueva: 640,
			calidad: 91,
			camara: 'trasera'
		});
		evento = null;
	} else if (cual_foto == 3) {
		var insertarModelo5_m = Alloy.Collections.numero_unico;
		var insertarModelo5_fila = Alloy.createModel('numero_unico', {
			comentario: 'nuevo itemdano foto3'
		});
		insertarModelo5_m.add(insertarModelo5_fila);
		insertarModelo5_fila.save();
		var nuevoid_f3 = require('helper').model2object(insertarModelo5_m.last());
		require('vars')[_var_scopekey]['nuevoid_f3'] = nuevoid_f3;
		if (Ti.App.deployType != 'production') console.log(String.format(L('x2429469022', 'imagen capturada y asignada como cap%1$s.jpg'), nuevoid_f3.id.toString()), {});
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var ID_1327392489_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + seltarea.id_server);
			if (ID_1327392489_d.exists() == false) ID_1327392489_d.createDirectory();
			var ID_1327392489_f = Ti.Filesystem.getFile(ID_1327392489_d.resolve(), 'cap' + nuevoid_f3.id + '.jpg');
			if (ID_1327392489_f.exists() == true) ID_1327392489_f.deleteFile();
			ID_1327392489_f.write(evento.foto);
			ID_1327392489_d = null;
			ID_1327392489_f = null;
		} else {
			var ID_1576027723_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + 0);
			if (ID_1576027723_d.exists() == false) ID_1576027723_d.createDirectory();
			var ID_1576027723_f = Ti.Filesystem.getFile(ID_1576027723_d.resolve(), 'cap' + nuevoid_f3.id + '.jpg');
			if (ID_1576027723_f.exists() == true) ID_1576027723_f.deleteFile();
			ID_1576027723_f.write(evento.foto);
			ID_1576027723_d = null;
			ID_1576027723_f = null;
		}
		$.danos1.set({
			foto3: String.format(L('x1070475521', 'cap%1$s.jpg'), nuevoid_f3.id.toString())
		});
		if ('danos1' in $) $danos1 = $.danos1.toJSON();
		$.widgetFotochica3.procesar({
			imagen: evento.foto,
			nueva: 640,
			calidad: 91,
			camara: 'trasera'
		});
		evento = null;
	}
}

(function() {
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
		/** 
		 * Modificamos la idinspeccion con el idserver de la tarea 
		 */
		$.danos1.set({
			id_inspeccion: seltarea.id_server
		});
		if ('danos1' in $) $danos1 = $.danos1.toJSON();
	}
	/** 
	 * Recuperamos la variable del id_recinto que tiene danos relacionados y actualizamos el modelo 
	 */
	var temp_idrecinto = ('temp_idrecinto' in require('vars')) ? require('vars')['temp_idrecinto'] : '';
	if ((_.isObject(temp_idrecinto) || (_.isString(temp_idrecinto)) && !_.isEmpty(temp_idrecinto)) || _.isNumber(temp_idrecinto) || _.isBoolean(temp_idrecinto)) {
		$.danos1.set({
			id_recinto: temp_idrecinto
		});
		if ('danos1' in $) $danos1 = $.danos1.toJSON();
	}
	/** 
	 * Desenfocamos todos los campos de texto en 0.2 segundos 
	 */
	var ID_268744191_func = function() {
		$.campo.blur();
		$.DescribaelDao.blur();
	};
	var ID_268744191 = setTimeout(ID_268744191_func, 1000 * 0.2);
	/** 
	 * Fijamos el scroll al inicio 
	 */
	$.scroll.scrollToTop();
	/** 
	 * Modificamos en 0.1 segundos el color del statusbar 
	 */
	var ID_475082989_func = function() {
		var nuevodano_statusbar = '#7E6EE0';

		var setearStatusColor = function(nuevodano_statusbar) {
			if (OS_IOS) {
				if (nuevodano_statusbar == 'light' || nuevodano_statusbar == 'claro') {
					$.nuevodano_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.LIGHT_CONTENT);
				} else if (nuevodano_statusbar == 'grey' || nuevodano_statusbar == 'gris' || nuevodano_statusbar == 'gray') {
					$.nuevodano_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.GREY);
				} else if (nuevodano_statusbar == 'oscuro' || nuevodano_statusbar == 'dark') {
					$.nuevodano_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.DEFAULT);
				}
			} else if (OS_ANDROID) {
				abx.setStatusbarColor(nuevodano_statusbar);
			}
		};
		setearStatusColor(nuevodano_statusbar);

	};
	var ID_475082989 = setTimeout(ID_475082989_func, 1000 * 0.1);
	_my_events['resp_dato,ID_1243902372'] = function(evento) {
		if (Ti.App.deployType != 'production') console.log('detalle de la respuesta', {
			"datos": evento
		});
		$.SeleccionePartida.setText(evento.nombre);

		$.SeleccionePartida.setColor('#000000');

		/** 
		 * Asumimos que el valor mostrado seleccionado es el nombre de la fila dano, ya que no existe el campo nombre en esta pantalla. 
		 */
		$.danos1.set({
			nombre: evento.nombre,
			id_partida: evento.id_partida,
			id_tipodano: '',
			id_unidadmedida: ''
		});
		if ('danos1' in $) $danos1 = $.danos1.toJSON();
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var pais = ('pais' in require('vars')) ? require('vars')['pais'] : '';
			var consultarModelo5_i = Alloy.createCollection('tipo_dano');
			var consultarModelo5_i_where = 'pais_texto=\'' + pais[0].nombre + '\' AND id_partida=\'' + evento.id_server + '\'';
			consultarModelo5_i.fetch({
				query: 'SELECT * FROM tipo_dano WHERE pais_texto=\'' + pais[0].nombre + '\' AND id_partida=\'' + evento.id_server + '\''
			});
			var danos = require('helper').query2array(consultarModelo5_i);
			if (Ti.App.deployType != 'production') console.log('predanos', {
				"datos": danos
			});
			/** 
			 * Obtenemos datos para selectores (solo los de este pais y este tipo de partida) 
			 */
			var datos = [];
			_.each(danos, function(fila, pos) {
				var new_row = {};
				_.each(fila, function(x, llave) {
					var newkey = '';
					if (llave == 'nombre') newkey = 'valor';
					if (llave == 'id_segured') newkey = 'id_interno';
					if (llave == 'id_server') newkey = 'id_gerardo';
					if (newkey != '') new_row[newkey] = fila[llave];
				});
				datos.push(new_row);
			});
			if (Ti.App.deployType != 'production') console.log('postdanos', {
				"datos": datos
			});
			$.widgetModal2.data({
				data: datos
			});
			if (Ti.App.deployType != 'production') console.log('termino de llamar', {});
		}
		/** 
		 * Editamos y limpiamos los campos que estan siendo filtrados con este campo 
		 */
		$.widgetModal2.labels({
			seleccione: 'seleccione tipo'
		});
		$.widgetModal.labels({
			seleccione: 'unidad'
		});
		$.campo.setValue('');

		$.label.setText('-');

	};
	Alloy.Events.on('resp_dato', _my_events['resp_dato,ID_1243902372']);
})();

function Postlayout_nuevodano(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var ID_1294585966_func = function() {
		/** 
		 * Una vez ya cargada la tarea, dejamos la variable limpia para poder abrir otras pantallas 
		 */
		require('vars')['var_abriendo'] = '';
	};
	var ID_1294585966 = setTimeout(ID_1294585966_func, 1000 * 0.2);

}
if (OS_IOS || OS_ANDROID) {
	$.nuevodano.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}
//$.nuevodano.open();
