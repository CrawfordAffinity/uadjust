var _bind4section = {};
var _list_templates = {
	"elemento": {
		"vista2": {},
		"Label3": {
			"text": "{nombre}"
		},
		"Label2": {
			"text": "{id_server}"
		},
		"Label": {
			"text": "{id_segured}"
		},
		"vista": {}
	},
	"dano": {
		"Label2": {
			"text": "{id}"
		},
		"vista29": {},
		"Label": {
			"text": "{nombre}"
		}
	},
	"pborrar": {
		"vista5": {},
		"imagen": {},
		"vista3": {},
		"Label4": {
			"text": "{id}"
		},
		"Label3": {
			"text": "{nombre}"
		},
		"vista4": {},
		"vista22": {},
		"vista23": {},
		"vista24": {},
		"imagen2": {}
	},
	"contenido": {
		"vista2": {},
		"Label2": {
			"text": "{id}"
		},
		"Label": {
			"text": "{nombre}"
		}
	},
	"recinto": {
		"vista2": {},
		"Label": {
			"text": "{nombre}"
		},
		"Label2": {
			"text": "{id}"
		}
	},
	"nivel": {
		"Label2": {
			"text": "{id}"
		},
		"Label": {
			"text": "{nombre}"
		},
		"vista21": {}
	},
	"tareas_mistareas": {
		"Label2": {
			"text": "{comuna}"
		},
		"vista4": {},
		"vista6": {},
		"vista2": {},
		"Adistancia": {
			"text": "a {distancia} km"
		},
		"vista8": {},
		"imagen": {},
		"vista10": {},
		"vista12": {
			"visible": "{seguirvisible}"
		},
		"Label": {
			"text": "{direccion}"
		},
		"vista3": {
			"idlocal": "{idlocal}"
		},
		"vista9": {},
		"Label3": {
			"text": "{ciudad}, {pais}"
		},
		"vista11": {},
		"vista5": {},
		"vista7": {}
	}
};

var _activity;
if (OS_ANDROID) {
	_activity = $.MISTAREAS.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'MISTAREAS';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.MISTAREAS.addEventListener('open', function(e) {});
}
$.MISTAREAS.orientationModes = [Titanium.UI.PORTRAIT];

function Load_imagen(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var evento = e;
	/** 
	 * Elemento es el objeto que llamo el evento, como es virtual no hay referencia de nodo para un modificar 
	 */
	elemento.start();

}

function Itemclick_listado(e) {

	e.cancelBubble = true;
	var objeto = e.section.getItemAt(e.itemIndex);
	var modelo = {},
		_modelo = [];
	var fila = {},
		fila_bak = {},
		info = {
			_template: objeto.template,
			_what: [],
			_seccion_ref: e.section.getHeaderTitle(),
			_model_id: -1
		},
		_tmp = {
			objmap: {}
		};
	if ('itemId' in e) {
		info._model_id = e.itemId;
		modelo._id = info._model_id;
		if (info._seccion_ref != '' && info._seccion_ref in _bind4section) {
			modelo._collection = _bind4section[info._seccion_ref];
			_tmp._coll = modelo._collection;
		}
	}
	if (info._seccion_ref != '' && info._seccion_ref in _bind4section) {
		if (Alloy.Collections[_tmp._coll].config.adapter.type == 'sql') {
			_tmp._inst = Alloy.Collections[_tmp._coll];
			_tmp._id = Alloy.Collections[_tmp._coll].config.adapter.idAttribute;
			_tmp._dbsql = 'SELECT * FROM ' + Alloy.Collections[_tmp._coll].config.adapter.collection_name + ' WHERE ' + _tmp._id + ' = ' + e.itemId;
			_tmp._db = Ti.Database.open(Alloy.Collections[_tmp._coll].config.adapter.db_name);
			_modelo = _tmp._db.execute(_tmp._dbsql);
			var values = [],
				fieldNames = [];
			var fieldCount = _.isFunction(_modelo.fieldCount) ? _modelo.fieldCount() : _modelo.fieldCount;
			var getField = _modelo.field;
			var i = 0;
			for (; fieldCount > i; i++) fieldNames.push(_modelo.fieldName(i));
			while (_modelo.isValidRow()) {
				var o = {};
				for (i = 0; fieldCount > i; i++) o[fieldNames[i]] = getField(i);
				values.push(o);
				_modelo.next();
			}
			_modelo = values;
			_tmp._db.close();
		} else {
			_tmp._search = {};
			_tmp._search[_tmp._id] = e.itemId + '';
			_modelo = Alloy.Collections[_tmp._coll].fetch(_tmp._search);
		}
	}
	var findVariables = require('fvariables');
	_.each(_list_templates[info._template], function(obj_id, id) {
		_.each(obj_id, function(valor, prop) {
			var llaves = findVariables(valor, '{', '}');
			_.each(llaves, function(llave) {
				_tmp.objmap[llave] = {
					id: id,
					prop: prop
				};
				fila[llave] = objeto[id][prop];
				if (id == e.bindId) info._what.push(llave);
			});
		});
	});
	info._what = info._what.join(',');
	fila_bak = JSON.parse(JSON.stringify(fila));
	/** 
	 * Usamos este flag para evitar que la pantalla se abra en mas de una oportunidad 
	 */
	var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
	if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
		require('vars')['var_abriendo'] = L('x4261170317', 'true');
		var nulo = Alloy.createController("detalletarea_index", {
			'_id': fila.idlocal,
			'__master_model': (typeof modelo !== 'undefined') ? modelo : {},
			'__modelo': (typeof _modelo !== 'undefined' && _modelo.length > 0) ? _modelo[0] : {}
		}).getView();
		nulo.open({
			modal: true
		});

		nulo = null;
	}
	_tmp.changed = false;
	_tmp.diff_keys = [];
	_.each(fila, function(value1, prop) {
		var had_samekey = false;
		_.each(fila_bak, function(value2, prop2) {
			if (prop == prop2 && value1 == value2) {
				had_samekey = true;
			} else if (!_.has(fila_bak, prop) || !_.has(fila, prop2)) {
				has_samekey = true;
			}
		});
		if (!had_samekey) _tmp.diff_keys.push(prop);
	});
	if (_tmp.diff_keys.length > 0) _tmp.changed = true;
	if (_tmp.changed == true) {
		_.each(_tmp.diff_keys, function(llave) {
			objeto[_tmp.objmap[llave].id][_tmp.objmap[llave].prop] = fila[llave];
		});
		e.section.updateItemAt(e.itemIndex, objeto);
	}

}

(function() {

	_my_events['_refrescar_tareas_mistareas,ID_199664348'] = function(evento) {

		var ID_521921285_trycatch = {
			error: function(e) {
				if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
			}
		};
		try {
			ID_521921285_trycatch.error = function(evento) {
				if (Ti.App.deployType != 'production') console.log('error refrescando mis tareas (pantalla mistareas)', {});
			};
			var inspeccion_encurso = ('inspeccion_encurso' in require('vars')) ? require('vars')['inspeccion_encurso'] : '';
			if (inspeccion_encurso == false || inspeccion_encurso == 'false') {
				/** 
				 * Si no hay inspeccion en curso, limpiamos la lista de tareas, crear secciones para hoy, manana y otro dia. Consultamos a la tabla e ingresamos las tareas segun fecha 
				 */
				var listado_borrar = false;

				var limpiarListado = function(animar) {
					var a_nimar = (typeof animar == 'undefined') ? false : animar;
					if (OS_IOS && a_nimar == true) {
						var s_ecciones = $.listado.getSections();
						_.each(s_ecciones, function(obj_id, pos) {
							$.listado.deleteSectionAt(0, {
								animated: true
							});
						});
					} else {
						$.listado.setSections([]);
					}
				};
				limpiarListado(listado_borrar);

				var seccionListadoHoy = Titanium.UI.createListSection({
					headerTitle: L('x1916403066_traducir', 'hoy')
				});
				var headerListado = Titanium.UI.createView({
					height: '25dp',
					width: Ti.UI.FILL
				});
				var vista13 = Titanium.UI.createView({
					height: '25dp',
					layout: 'composite',
					width: Ti.UI.FILL,
					backgroundColor: '#F7F7F7'
				});
				var PARAHOY = Titanium.UI.createLabel({
					text: L('x1296057275_traducir', 'PARA HOY'),
					color: '#999999',
					touchEnabled: false,
					font: {
						fontFamily: 'Roboto-Medium',
						fontSize: '14dp'
					}

				});
				vista13.add(PARAHOY);

				headerListado.add(vista13);
				seccionListadoHoy.setHeaderView(headerListado);
				$.listado.appendSection(seccionListadoHoy);
				var seccionListadoManana = Titanium.UI.createListSection({
					headerTitle: L('x4074624282_traducir', 'manana')
				});
				var headerListado2 = Titanium.UI.createView({
					height: '25dp',
					width: Ti.UI.FILL
				});
				var vista14 = Titanium.UI.createView({
					height: '25dp',
					layout: 'composite',
					width: Ti.UI.FILL,
					backgroundColor: '#F7F7F7'
				});
				var MAANA = Titanium.UI.createLabel({
					text: L('x3380933310_traducir', 'MAÑANA'),
					color: '#999999',
					touchEnabled: false,
					font: {
						fontFamily: 'Roboto-Medium',
						fontSize: '14dp'
					}

				});
				vista14.add(MAANA);

				headerListado2.add(vista14);
				seccionListadoManana.setHeaderView(headerListado2);
				$.listado.appendSection(seccionListadoManana);
				var consultarModelo_i = Alloy.createCollection('tareas');
				var consultarModelo_i_where = 'ORDER BY FECHA_TAREA ASC';
				consultarModelo_i.fetch({
					query: 'SELECT * FROM tareas ORDER BY FECHA_TAREA ASC'
				});
				var tareas = require('helper').query2array(consultarModelo_i);
				/** 
				 * Inicializamos la variable de ultima fecha en vacio para usarla en el recorrido de tareas 
				 */
				require('vars')['ultima_fecha'] = '';
				/** 
				 * Formateamos fecha de hoy y manana para cargar la tarea dependiendo de la fecha de realizacion 
				 */
				var moment = require('alloy/moment');
				var fecha_hoy = moment(new Date()).format('YYYY-MM-DD');
				var manana = new Date();
				manana.setDate(manana.getDate() + 1);;
				var moment = require('alloy/moment');
				var formatearFecha2 = manana;
				var fecha_manana = moment(formatearFecha2).format('YYYY-MM-DD');
				var tarea_index = 0;
				_.each(tareas, function(tarea, tarea_pos, tarea_list) {
					tarea_index += 1;
					if (tarea.estado_tarea == 4) {
						/** 
						 * Si estado_tarea es 4 seteamos variable para enviar_ubicacion de inspector 
						 */
						require('vars')['seguir_tarea'] = tarea.id_server;
					}
					/** 
					 * Sector de otras fechas 
					 */
					var ultima_fecha = ('ultima_fecha' in require('vars')) ? require('vars')['ultima_fecha'] : '';
					if (ultima_fecha != tarea.fecha_tarea) {
						/** 
						 * Generamos una seccion nueva para las fechas que no sean de hoy, ni de manana 
						 */
						if (tarea.fecha_tarea == fecha_hoy) {} else if (tarea.fecha_tarea == fecha_manana) {} else {
							var formatfecha = ('formatfecha' in require('vars')) ? require('vars')['formatfecha'] : '';
							var moment = require('alloy/moment');
							var formatearFecha3 = tarea.fecha_tarea;
							if (typeof formatearFecha3 === 'string' || typeof formatearFecha3 === 'number') {
								var fecha_titulo = moment(formatearFecha3, 'YYYY-MM-DD').format('DD//MM/YYYY');
							} else {
								var fecha_titulo = moment(formatearFecha3).format('DD//MM/YYYY');
							}
							var seccionListadoFecha = Titanium.UI.createListSection({
								headerTitle: L('x27834329_traducir', 'fecha')
							});
							var headerListado3 = Titanium.UI.createView({
								height: '25dp',
								width: Ti.UI.FILL
							});
							var vista15 = Titanium.UI.createView({
								height: '25dp',
								layout: 'composite',
								width: Ti.UI.FILL,
								backgroundColor: '#F7F7F7'
							});
							var Fechatitulo = Titanium.UI.createLabel({
								text: fecha_titulo,
								color: '#999999',
								touchEnabled: false,
								font: {
									fontFamily: 'Roboto-Medium',
									fontSize: '14dp'
								}

							});
							vista15.add(Fechatitulo);

							headerListado3.add(vista15);
							seccionListadoFecha.setHeaderView(headerListado3);
							$.listado.appendSection(seccionListadoFecha);
						}
						/** 
						 * Actualizamos la variable con&#160;&#160;la ultima fecha de la tarea 
						 */
						require('vars')['ultima_fecha'] = tarea.fecha_tarea;
					}
					if ((_.isObject(tarea.nivel_2) || _.isString(tarea.nivel_2)) && _.isEmpty(tarea.nivel_2)) {
						/** 
						 * Revisamos cual es el ultimo nivel disponible y lo fijamos dentro de la variable tarea como ultimo_nivel 
						 */
						var tarea = _.extend(tarea, {
							ultimo_nivel: tarea.nivel_1
						});
					} else if ((_.isObject(tarea.nivel_3) || _.isString(tarea.nivel_3)) && _.isEmpty(tarea.nivel_3)) {
						var tarea = _.extend(tarea, {
							ultimo_nivel: tarea.nivel_2
						});
					} else if ((_.isObject(tarea.nivel_4) || _.isString(tarea.nivel_4)) && _.isEmpty(tarea.nivel_4)) {
						var tarea = _.extend(tarea, {
							ultimo_nivel: tarea.nivel_3
						});
					} else if ((_.isObject(tarea.nivel_5) || _.isString(tarea.nivel_5)) && _.isEmpty(tarea.nivel_5)) {
						var tarea = _.extend(tarea, {
							ultimo_nivel: tarea.nivel_4
						});
					} else {
						var tarea = _.extend(tarea, {
							ultimo_nivel: tarea.nivel_5
						});
					}
					if (tarea.estado_tarea == 4) {
						/** 
						 * Preguntamos si esta en seguimiento (estado es 4) 
						 */
						require('vars')['seguimiento'] = L('x4261170317', 'true');
					} else {
						require('vars')['seguimiento'] = L('x734881840_traducir', 'false');
					}
					var seguimiento = ('seguimiento' in require('vars')) ? require('vars')['seguimiento'] : '';
					if (tarea.fecha_tarea == fecha_hoy) {
						/** 
						 * Dependiendo de la fecha de la tarea, es donde apuntaremos a la seccion que corresponde (segun fecha) 
						 */
						var itemListado = [{
							Label2: {
								text: tarea.ultimo_nivel
							},
							vista4: {},
							vista6: {},
							vista2: {},
							Adistancia: {
								text: String.format(L('x1959850688_traducir', 'a %1$s km'), tarea.distance.toString())
							},
							vista8: {},
							imagen: {},
							vista10: {},
							vista12: {
								visible: seguimiento
							},
							Label: {
								text: tarea.direccion
							},
							vista3: {
								idlocal: tarea.id
							},
							vista9: {},
							template: 'tareas_mistareas',
							Label3: {
								text: String.format(L('x1487588533_traducir', '%1$s, %2$s'), (tarea.nivel_2) ? tarea.nivel_2.toString() : '', (tarea.pais) ? tarea.pais.toString() : '')
							},
							vista11: {},
							vista5: {},
							vista7: {}

						}];
						var itemListado_secs = {};
						_.map($.listado.getSections(), function(itemListado_valor, itemListado_indice) {
							itemListado_secs[itemListado_valor.getHeaderTitle()] = itemListado_indice;
							return itemListado_valor;
						});
						if ('' + L('x1916403066_traducir', 'hoy') + '' in itemListado_secs) {
							$.listado.sections[itemListado_secs['' + L('x1916403066_traducir', 'hoy') + '']].appendItems(itemListado);
						} else {
							console.log('DEBUG:dynamic listItem points to a invalid section id: check attribute _seccion.');
						}
					} else if (tarea.fecha_tarea == fecha_manana) {
						var itemListado2 = [{
							Label2: {
								text: tarea.ultimo_nivel
							},
							vista4: {},
							vista6: {},
							vista2: {},
							Adistancia: {
								text: String.format(L('x1959850688_traducir', 'a %1$s km'), tarea.distance.toString())
							},
							vista8: {},
							imagen: {},
							vista10: {},
							vista12: {
								visible: seguimiento
							},
							Label: {
								text: tarea.direccion
							},
							vista3: {
								idlocal: tarea.id
							},
							vista9: {},
							template: 'tareas_mistareas',
							Label3: {
								text: String.format(L('x1487588533_traducir', '%1$s, %2$s'), (tarea.nivel_2) ? tarea.nivel_2.toString() : '', (tarea.pais) ? tarea.pais.toString() : '')
							},
							vista11: {},
							vista5: {},
							vista7: {}

						}];
						var itemListado2_secs = {};
						_.map($.listado.getSections(), function(itemListado2_valor, itemListado2_indice) {
							itemListado2_secs[itemListado2_valor.getHeaderTitle()] = itemListado2_indice;
							return itemListado2_valor;
						});
						if ('' + L('x4074624282_traducir', 'manana') + '' in itemListado2_secs) {
							$.listado.sections[itemListado2_secs['' + L('x4074624282_traducir', 'manana') + '']].appendItems(itemListado2);
						} else {
							console.log('DEBUG:dynamic listItem points to a invalid section id: check attribute _seccion.');
						}
					} else {
						var itemListado3 = [{
							Label2: {
								text: tarea.ultimo_nivel
							},
							vista4: {},
							vista6: {},
							vista2: {},
							Adistancia: {
								text: String.format(L('x1959850688_traducir', 'a %1$s km'), tarea.distance.toString())
							},
							vista8: {},
							imagen: {},
							vista10: {},
							vista12: {
								visible: seguimiento
							},
							Label: {
								text: tarea.direccion
							},
							vista3: {
								idlocal: tarea.id
							},
							vista9: {},
							template: 'tareas_mistareas',
							Label3: {
								text: String.format(L('x1487588533_traducir', '%1$s, %2$s'), (tarea.nivel_2) ? tarea.nivel_2.toString() : '', (tarea.pais) ? tarea.pais.toString() : '')
							},
							vista11: {},
							vista5: {},
							vista7: {}

						}];
						var itemListado3_secs = {};
						_.map($.listado.getSections(), function(itemListado3_valor, itemListado3_indice) {
							itemListado3_secs[itemListado3_valor.getHeaderTitle()] = itemListado3_indice;
							return itemListado3_valor;
						});
						if ('' + L('x27834329_traducir', 'fecha') + '' in itemListado3_secs) {
							$.listado.sections[itemListado3_secs['' + L('x27834329_traducir', 'fecha') + '']].appendItems(itemListado3);
						} else {
							console.log('DEBUG:dynamic listItem points to a invalid section id: check attribute _seccion.');
						}
					}
				});
				/** 
				 * Limpieza memoria 
				 */
				fecha_hoy = null, manana = null, tarea = null;
			}
		} catch (e) {
			ID_521921285_trycatch.error(e);
		}
	};
	Alloy.Events.on('_refrescar_tareas_mistareas', _my_events['_refrescar_tareas_mistareas,ID_199664348']);

	_my_events['_refrescar_tareas,ID_268924730'] = function(evento) {
		var inspeccion_encurso = ('inspeccion_encurso' in require('vars')) ? require('vars')['inspeccion_encurso'] : '';
		if (inspeccion_encurso == false || inspeccion_encurso == 'false') {

			Alloy.Events.trigger('_refrescar_tareas_mistareas');
		}
	};
	Alloy.Events.on('_refrescar_tareas', _my_events['_refrescar_tareas,ID_268924730']);
	/** 
	 * Esto se llama al logearse 
	 */
	var ID_1359791285_func = function() {

		Alloy.Events.trigger('_refrescar_tareas_mistareas');
	};
	var ID_1359791285 = setTimeout(ID_1359791285_func, 1000 * 0.1);
})();

function Androidback_MISTAREAS(e) {
	/** 
	 * Sin funcionalidad para evitar que el usuario cierre la pantalla 
	 */

	e.cancelBubble = true;
	var elemento = e.source;

}
if (OS_IOS || OS_ANDROID) {
	$.MISTAREAS.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}
//$.MISTAREAS.open();
