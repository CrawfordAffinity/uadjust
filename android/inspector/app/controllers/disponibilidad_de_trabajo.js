var _bind4section = {};
var _list_templates = {};

var _activity;
if (OS_ANDROID) {
	_activity = $.ENROLAMIENTO4.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'ENROLAMIENTO4';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.ENROLAMIENTO4.addEventListener('open', function(e) {
		abx.setBackgroundColor("white");
	});
}
$.ENROLAMIENTO4.orientationModes = [Titanium.UI.PORTRAIT];


$.widgetBarra4.init({
	titulo: L('x2146494644_traducir', 'ENROLAMIENTO'),
	__id: 'ALL1751136142',
	fondo: 'fondoblanco',
	colortitulo: 'negro',
	modal: L('', '')
});


$.widgetHeader4.init({
	titulo: L('x3752633607_traducir', 'PARTE 4: Disponibilidad de trabajo'),
	__id: 'ALL1222663545',
	avance: '4/6',
	onclick: Click_widgetHeader4
});

function Click_widgetHeader4(e) {

	var evento = e;

}


function Change_picker5(e) {

	e.cancelBubble = true;
	var elemento = e;
	var _columna = e.columnIndex;
	var columna = e.columnIndex + 1;
	var _fila = e.rowIndex;
	var fila = e.rowIndex + 1;
	if (Ti.App.deployType != 'production') console.log('seleccionado', {
		"fila": _fila
	});
	require('vars')['seleccionado'] = _fila;

}

function Change_ID_1568773770(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	if (elemento == true || elemento == 'true') {
		/** 
		 * Cambiamos el texto de la opcion y cambiamos el valor de la variable 
		 */
		$.NO.setText('SI');

		require('vars')['fueraciudad'] = L('x2212294583', '1');
	} else {
		$.NO.setText('NO');

		require('vars')['fueraciudad'] = L('x4108050209', '0');
	}
	elemento = null;

}

function Change_ID_952608080(e) {

	e.cancelBubble = true;
	var elemento = e.value;
	if (elemento == true || elemento == 'true') {
		/** 
		 * Cambiamos el texto de la opcion y cambiamos el valor de la variable 
		 */
		$.NO9.setText('SI');

		require('vars')['fuerapais'] = L('x2212294583', '1');
	} else {
		$.NO9.setText('NO');

		require('vars')['fuerapais'] = L('x4108050209', '0');
	}
	elemento = null;

}

$.widgetBotonlargo4.init({
	titulo: L('x1524107289_traducir', 'CONTINUAR'),
	__id: 'ALL218695237',
	onclick: Click_widgetBotonlargo4
});

function Click_widgetBotonlargo4(e) {

	var evento = e;
	/** 
	 * Recuperamos los valores de las variables, guardamos en la variable de registro 
	 */
	var seleccionado = ('seleccionado' in require('vars')) ? require('vars')['seleccionado'] : '';
	var fueraciudad = ('fueraciudad' in require('vars')) ? require('vars')['fueraciudad'] : '';
	var fuerapais = ('fuerapais' in require('vars')) ? require('vars')['fuerapais'] : '';
	var registro = ('registro' in require('vars')) ? require('vars')['registro'] : '';
	var registro = _.extend(registro, {
		disponibilidad: seleccionado,
		disp_viajar_ciudad: fueraciudad,
		disp_viajar_pais: fuerapais
	});
	require('vars')['registro'] = registro;
	if (seleccionado == 1 || seleccionado == '1') {
		/** 
		 * Dependiendo de la seleccion de disponibilidad de tiempo, se puede accionar distintas funciones 
		 */
		/** 
		 * Si se escoge fulltime, marcamos disponibilidad completa, guardamos en registro y continuamos a pantalla contacto 
		 */
		var registro = _.extend(registro, {
			disp_horas: '',
			d1: 1,
			d2: 1,
			d3: 1,
			d4: 1,
			d5: 1,
			d6: 1,
			d7: 1
		});
		require('vars')['registro'] = registro;
		Alloy.createController("contactos", {}).getView().open();
	} else {
		/** 
		 * Iniciamos animacion, despues de 0.3 segundos detenemos animacion y carga pantalla parttime 
		 */

		$.widgetBotonlargo4.iniciar_progreso({});
		var ID_738322479_func = function() {

			$.widgetBotonlargo4.detener_progreso({});
			Alloy.createController("part_time", {}).getView().open();
		};
		var ID_738322479 = setTimeout(ID_738322479_func, 1000 * 0.3);
	}
}

(function() {
	/** 
	 * Creamos variables seteadas en 0 
	 */
	require('vars')['fueraciudad'] = L('x4108050209', '0');
	require('vars')['fuerapais'] = L('x4108050209', '0');
	require('vars')['seleccionado'] = L('x4108050209', '0');
	/** 
	 * Creamos evento que al cerrar desde la ultima pantalla, esta tambi&#233;n se cierre. Evitamos el uso excesivo de memoria ram 
	 */

	_my_events['_close_enrolamiento,ID_1332238778'] = function(evento) {
		if (Ti.App.deployType != 'production') console.log('escuchando cerrar enrolamiento disp trabajao', {});
		$.ENROLAMIENTO4.close();
	};
	Alloy.Events.on('_close_enrolamiento', _my_events['_close_enrolamiento,ID_1332238778']);
})();

function Androidback_ENROLAMIENTO4(e) {
	/** 
	 * Dejamos esta accion vacia para que no pueda volver a la pantalla anterior 
	 */

	e.cancelBubble = true;
	var elemento = e.source;

}
if (OS_IOS || OS_ANDROID) {
	$.ENROLAMIENTO4.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}