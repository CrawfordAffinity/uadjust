var _bind4section = {};
var _list_templates = {
	"tarea_historia": {
		"Label3": {
			"text": "{comuna}"
		},
		"vista4": {},
		"vista6": {},
		"vista2": {},
		"Label2": {
			"text": "{hora_termino}"
		},
		"vista8": {},
		"vista13": {
			"visible": "{bt_enviartarea}"
		},
		"vista10": {},
		"Label": {
			"text": "{direccion}"
		},
		"vista3": {
			"idlocal": "{id}",
			"estado": "{estado_tarea}"
		},
		"vista9": {},
		"vista12": {},
		"Label4": {
			"text": "{ciudad}, {pais}"
		},
		"vista14": {
			"visible": "{enviando_tarea}"
		},
		"vista11": {},
		"vista5": {},
		"ENVIAR": {},
		"imagen": {},
		"vista7": {}
	}
};

var _activity;
if (OS_ANDROID) {
	_activity = $.PERFIL.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'PERFIL';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.PERFIL.addEventListener('open', function(e) {});
}
$.PERFIL.orientationModes = [Titanium.UI.PORTRAIT];

function Touchstart_imagen(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var evento = e;
	var imagen_imagen = 'historial_on';

	if (typeof imagen_imagen == 'string' && 'styles' in require('a4w') && imagen_imagen in require('a4w').styles['images']) {
		imagen_imagen = require('a4w').styles['images'][imagen_imagen];
	}
	$.imagen.setImage(imagen_imagen);


}

function Touchend_imagen(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var evento = e;
	var imagen_imagen = 'historial_off';

	if (typeof imagen_imagen == 'string' && 'styles' in require('a4w') && imagen_imagen in require('a4w').styles['images']) {
		imagen_imagen = require('a4w').styles['images'][imagen_imagen];
	}
	$.imagen.setImage(imagen_imagen);

	/** 
	 * Creamos un flag para evitar que la pantalla se abra dos veces 
	 */
	var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
	if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
		require('vars')['var_abriendo'] = L('x4261170317', 'true');
		Alloy.createController("historial_index", {}).getView().open();
	}

}

function Click_vista6(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	/** 
	 * Creamos un flag para evitar que la pantalla se abra dos veces 
	 */
	var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
	if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
		require('vars')['var_abriendo'] = L('x4261170317', 'true');
		Alloy.createController("editardomicilio_index", {}).getView().open();
	}

}

function Click_vista9(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	/** 
	 * Creamos un flag para evitar que la pantalla se abra dos veces 
	 */
	var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
	if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
		require('vars')['var_abriendo'] = L('x4261170317', 'true');
		Alloy.createController("editardisponibilidad_index", {}).getView().open();
	}

}

function Click_vista12(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	/** 
	 * Creamos un flag para evitar que la pantalla se abra dos veces 
	 */
	var var_abriendo = ('var_abriendo' in require('vars')) ? require('vars')['var_abriendo'] : '';
	if ((_.isObject(var_abriendo) || _.isString(var_abriendo)) && _.isEmpty(var_abriendo)) {
		require('vars')['var_abriendo'] = L('x4261170317', 'true');
		Alloy.createController("editarcontacto_index", {}).getView().open();
	}

}

var f_badge_historial = function(x_params) {
	var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
	if (_.isObject(inspector) && !_.isArray(inspector) && !_.isFunction(inspector)) {
		/** 
		 * Revisamos que la variable de inspector tenga datos 
		 */
		/** 
		 * Creamos una variable para el contador de inspecciones por mandar, consultamos el historial de tareas para incrementar el contador en caso de que se encuentren tareas por enviar 
		 */
		var cuantas = 0;
		var consultarModelo_i = Alloy.createCollection('historial_tareas');
		var consultarModelo_i_where = 'id_inspector=\'' + inspector.id_server + '\' AND estado_tarea=8';
		consultarModelo_i.fetch({
			query: 'SELECT * FROM historial_tareas WHERE id_inspector=\'' + inspector.id_server + '\' AND estado_tarea=8'
		});
		var inspa = require('helper').query2array(consultarModelo_i);
		var tarea_index = 0;
		_.each(inspa, function(tarea, tarea_pos, tarea_list) {
			tarea_index += 1;
			var fotos = [];
			var ID_1228384428_f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + tarea.id_server + '/');
			if (ID_1228384428_f.exists() == true) {
				fotos = ID_1228384428_f.getDirectoryListing();
			}
			if (fotos && fotos.length) {
				cuantas = cuantas + 1;
			}
		});
		var consultarModelo2_i = Alloy.createCollection('historial_tareas');
		var consultarModelo2_i_where = 'id_inspector=\'' + inspector.id_server + '\' AND estado_tarea=9';
		consultarModelo2_i.fetch({
			query: 'SELECT * FROM historial_tareas WHERE id_inspector=\'' + inspector.id_server + '\' AND estado_tarea=9'
		});
		var inspb = require('helper').query2array(consultarModelo2_i);
		var tarea_index = 0;
		_.each(inspb, function(tarea, tarea_pos, tarea_list) {
			tarea_index += 1;
			var fotos = [];
			var ID_399511280_f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + tarea.id_server + '/');
			if (ID_399511280_f.exists() == true) {
				fotos = ID_399511280_f.getDirectoryListing();
			}
			if (fotos && fotos.length) {
				cuantas = cuantas + 1;
			}
		});
		var insp_por_subir = L('x2539507736_traducir', 'INSP. POR SUBIR');
		$.INSPPORSUBIR.setText(String.format(L('x1045525933', '%1$s %2$s'), (cuantas) ? cuantas.toString() : '', (insp_por_subir) ? insp_por_subir.toString() : ''));

		if (cuantas == 0 || cuantas == '0') {
			/** 
			 * Si no hay tareas pendientes para ser enviadas, ocultamos el texto de las inspecciones pendientes por enviar 
			 */
			var INSPPORSUBIR_visible = false;

			if (INSPPORSUBIR_visible == 'si') {
				INSPPORSUBIR_visible = true;
			} else if (INSPPORSUBIR_visible == 'no') {
				INSPPORSUBIR_visible = false;
			}
			$.INSPPORSUBIR.setVisible(INSPPORSUBIR_visible);

		} else {
			var INSPPORSUBIR_visible = true;

			if (INSPPORSUBIR_visible == 'si') {
				INSPPORSUBIR_visible = true;
			} else if (INSPPORSUBIR_visible == 'no') {
				INSPPORSUBIR_visible = false;
			}
			$.INSPPORSUBIR.setVisible(INSPPORSUBIR_visible);

		}
		/** 
		 * Actualizamos datos mostrados de inspector 
		 */
		$.JohnAppleseed.setText(String.format(L('x1445533071', '%1$s %2$s %3$s'), (inspector.nombre) ? inspector.nombre.toString() : '', (inspector.apellido_paterno) ? inspector.apellido_paterno.toString() : '', (inspector.apellido_materno) ? inspector.apellido_materno.toString() : ''));

		$.label.setText(inspector.codigo_identificador);

		$.label2.setText(inspector.fecha_nacimiento);

		/** 
		 * Limpiamos la memoria 
		 */

		inspa = null, inspb = null
	}
	return null;
};
/** 
 * 26-feb-2018, cambiado de _refrescar_tareas_mistareas, para que cuando finalice inspeccion tambien se actualice el badge de historial. 
 */

_my_events['refrescar_historial,ID_324396855'] = function(evento) {
	var ID_836109156 = null;
	if ('badge_historial' in require('funciones')) {
		ID_836109156 = require('funciones').badge_historial({});
	} else {
		try {
			ID_836109156 = f_badge_historial({});
		} catch (ee) {}
	}
	if (Ti.App.deployType != 'production') console.log('escuche el evento refrescartareas en el perfil', {});
};
Alloy.Events.on('refrescar_historial', _my_events['refrescar_historial,ID_324396855']);


(function() {
	var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
	if ((_.isObject(inspector) || _.isString(inspector)) && _.isEmpty(inspector)) {
		/** 
		 * Revisamos que la variable del inspector (la que tiene el detalle del inspector) contenga datos, si esta vacio, hacemos una consulta a la tabla, cargamos los datos en la variable y editamos los textos en pantalla 
		 */
		var consultarModelo3_i = Alloy.createCollection('inspectores');
		var consultarModelo3_i_where = '';
		consultarModelo3_i.fetch();
		var inspector_list = require('helper').query2array(consultarModelo3_i);
		require('vars')['inspector'] = inspector_list[0];
		var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
		$.JohnAppleseed.setText(String.format(L('x1445533071', '%1$s %2$s %3$s'), (inspector.nombre) ? inspector.nombre.toString() : '', (inspector.apellido_paterno) ? inspector.apellido_paterno.toString() : '', (inspector.apellido_materno) ? inspector.apellido_materno.toString() : ''));

		$.label.setText(inspector.codigo_identificador);

		$.label2.setText(inspector.fecha_nacimiento);

	}
	var ID_1629163646 = null;
	if ('badge_historial' in require('funciones')) {
		ID_1629163646 = require('funciones').badge_historial({});
	} else {
		try {
			ID_1629163646 = f_badge_historial({});
		} catch (ee) {}
	}
})();

function Androidback_PERFIL(e) {

	e.cancelBubble = true;
	var elemento = e.source;

}
if (OS_IOS || OS_ANDROID) {
	$.PERFIL.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}
//$.PERFIL.open();
