var _bind4section = {};
var _list_templates = {
	"elemento": {
		"vista2": {},
		"Label3": {
			"text": "{nombre}"
		},
		"Label2": {
			"text": "{id_server}"
		},
		"Label": {
			"text": "{id_segured}"
		},
		"vista": {}
	},
	"dano": {
		"Label2": {
			"text": "{id}"
		},
		"vista29": {},
		"Label": {
			"text": "{nombre}"
		}
	},
	"pborrar": {
		"vista5": {},
		"imagen": {},
		"vista3": {},
		"Label4": {
			"text": "{id}"
		},
		"Label3": {
			"text": "{nombre}"
		},
		"vista4": {},
		"vista22": {},
		"vista23": {},
		"vista24": {},
		"imagen2": {}
	},
	"contenido": {
		"vista2": {},
		"Label2": {
			"text": "{id}"
		},
		"Label": {
			"text": "{nombre}"
		}
	},
	"recinto": {
		"vista2": {},
		"Label": {
			"text": "{nombre}"
		},
		"Label2": {
			"text": "{id}"
		}
	},
	"nivel": {
		"Label2": {
			"text": "{id}"
		},
		"Label": {
			"text": "{nombre}"
		},
		"vista21": {}
	}
};

var _activity;
if (OS_ANDROID) {
	_activity = $.inicio.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'inicio';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.inicio.addEventListener('open', function(e) {});
}
$.inicio.orientationModes = [Titanium.UI.PORTRAIT];


$.widgetBarra.init({
	titulo: L('x1489379407_traducir', '¿HAY ALGUIEN?'),
	__id: 'ALL138747237',
	fondo: 'fondoazul',
	top: 80
});


$.widgetPregunta.init({
	titulo: L('x1951816285_traducir', '¿PUEDE CONTINUAR CON LA INSPECCION?'),
	__id: 'ALL581285551',
	si: L('x369557195_traducir', 'SI puedo continuar'),
	texto: L('x2522333127_traducir', 'Si está el asegurado en el domicilio presione SI para continuar'),
	onno: no_widgetPregunta,
	onsi: si_widgetPregunta,
	no: L('x3892244486_traducir', 'NO se pudo realizar la inspección'),
	top: 80
});

function si_widgetPregunta(e) {

	var evento = e;
	Alloy.createController("datosbasicos_index", {}).getView().open();

}

function no_widgetPregunta(e) {

	var evento = e;
	var preguntarOpciones_opts = [L('x3481285698_traducir', 'Asegurado no puede seguir'), L('x3077268071_traducir', ' Se me acabo la bateria'), L('x479989047_traducir', ' Tuve un accidente'), L('x2376009830_traducir', ' Cancelar')];
	var preguntarOpciones = Ti.UI.createOptionDialog({
		title: L('x1670176792_traducir', 'RAZON PARA CANCELAR INSPECCION ACTUAL'),
		options: preguntarOpciones_opts
	});
	preguntarOpciones.addEventListener('click', function(e) {
		var resp = preguntarOpciones_opts[e.index];
		var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
		var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
		if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
			var razon = "";
			if (resp == L('x3481285698_traducir', 'Asegurado no puede seguir')) {
				/** 
				 * Esto parece redundante, pero es para escapar de i18n el texto 
				 */
				razon = "Asegurado no puede seguir";
			}
			if (resp == L('x1862794075_traducir', 'Se me acabo la bateria')) {
				razon = "Se me acabo la bateria";
			}
			if (resp == L('x4012805111_traducir', 'Tuve un accidente')) {
				razon = "Tuve un accidente";
			}
			if ((_.isObject(razon) || (_.isString(razon)) && !_.isEmpty(razon)) || _.isNumber(razon) || _.isBoolean(razon)) {
				require('vars')['insp_cancelada'] = L('x3958592860_traducir', 'recintos');
				var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
				/** 
				 * Mostramos popup avisando que se esta cancelando la inspeccion 
				 */
				var vista_visible = true;

				if (vista_visible == 'si') {
					vista_visible = true;
				} else if (vista_visible == 'no') {
					vista_visible = false;
				}
				$.vista.setVisible(vista_visible);

				var consultarURL = {};

				consultarURL.success = function(e) {
					var elemento = e,
						valor = e;
					/** 
					 * Ocultamos popup avisando que se esta cancelando la inspeccion 
					 */
					var vista_visible = false;

					if (vista_visible == 'si') {
						vista_visible = true;
					} else if (vista_visible == 'no') {
						vista_visible = false;
					}
					$.vista.setVisible(vista_visible);

					/** 
					 * Enviamos a firma 
					 */
					Alloy.createController("firma_index", {}).getView().open();
					var ID_860845987_func = function() {

						Alloy.Events.trigger('_cerrar_insp', {
							pantalla: 'hayalguien'
						});
					};
					var ID_860845987 = setTimeout(ID_860845987_func, 1000 * 0.5);
					elemento = null, valor = null;
				};

				consultarURL.error = function(e) {
					var elemento = e,
						valor = e;
					if (Ti.App.deployType != 'production') console.log('hubo error llamando cancelarTarea, la agregamos a cola de salida', {
						"elemento": elemento
					});
					if (Ti.App.deployType != 'production') console.log('agregando servicio cancelarTarea a cola', {});
					/** 
					 * En el caso que haya habido un problema llamando al servicio cancelarTarea, guardamos los datos en un objeto 
					 */
					var datos = {
						id_inspector: seltarea.id_inspector,
						codigo_identificador: inspector.codigo_identificador,
						id_server: seltarea.id_server,
						num_caso: seltarea.num_caso,
						razon: razon
					};
					var insertarModelo_m = Alloy.Collections.cola;
					var insertarModelo_fila = Alloy.createModel('cola', {
						data: JSON.stringify(datos),
						id_tarea: seltarea.id_server,
						tipo: 'cancelar'
					});
					insertarModelo_m.add(insertarModelo_fila);
					insertarModelo_fila.save();
					/** 
					 * Ocultamos popup avisando que se esta cancelando la inspeccion 
					 */
					var vista_visible = false;

					if (vista_visible == 'si') {
						vista_visible = true;
					} else if (vista_visible == 'no') {
						vista_visible = false;
					}
					$.vista.setVisible(vista_visible);

					/** 
					 * Enviamos a firma 
					 */
					Alloy.createController("firma_index", {}).getView().open();
					var ID_1869065227_func = function() {

						Alloy.Events.trigger('_cerrar_insp', {
							pantalla: 'hayalguien'
						});
					};
					var ID_1869065227 = setTimeout(ID_1869065227_func, 1000 * 0.5);
					elemento = null, valor = null;
				};
				require('helper').ajaxUnico('consultarURL', '' + String.format(L('x3244141284', '%1$scancelarTarea'), url_server.toString()) + '', 'POST', {
					id_inspector: seltarea.id_inspector,
					codigo_identificador: inspector.codigo_identificador,
					id_tarea: seltarea.id_server,
					num_caso: seltarea.num_caso,
					mensaje: razon,
					opcion: 0,
					tipo: 1
				}, 15000, consultarURL);
			}
		}
		resp = null;

		e.source.removeEventListener("click", arguments.callee);
	});
	preguntarOpciones.show();

}

function Postlayout_vista(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	$.progreso.show();

}

(function() {
	/** 
	 * Llamamos servicio iniciarInspeccion aqui, no hacemos nada con respuesta 
	 */
	var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	/** 
	 * Esto es para indicar en firma ult pantalla a cerrar en cancelada 
	 */
	require('vars')['insp_cancelada'] = '';
	/** 
	 * Desactivamos seguimiento de tarea 
	 */
	require('vars')['seguir_tarea'] = '';
	/** 
	 * api de iniciar tarea: solo si estamos en compilacion full 
	 */
	var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
	var datos = {
		id_inspector: inspector.id_server,
		codigo_identificador: inspector.codigo_identificador,
		id_server: seltarea.id_server,
		num_caso: seltarea.num_caso
	};
	require('vars')[_var_scopekey]['datos'] = datos;
	if ((_.isObject(url_server) || (_.isString(url_server)) && !_.isEmpty(url_server)) || _.isNumber(url_server) || _.isBoolean(url_server)) {
		/** 
		 * Revisamos si la variable url_server contiene datos para poder avisar al servidor que iniciaremos la inspeccion 
		 */
		/** 
		 * Inicializamos inspeccion 
		 */
		require('vars')['inspeccion_encurso'] = L('x4261170317', 'true');
		var moment = require('alloy/moment');
		var hora = moment(new Date()).format('HH:mm');
		var moment = require('alloy/moment');
		var fecha = moment(new Date()).format('DD-MM-YYYY');
		var insertarModelo2_m = Alloy.Collections.inspecciones;
		var insertarModelo2_fila = Alloy.createModel('inspecciones', {
			hora: hora,
			fecha: fecha,
			id_server: seltarea.id_server,
			fecha_inspeccion_inicio: new Date()
		});
		insertarModelo2_m.add(insertarModelo2_fila);
		insertarModelo2_fila.save();
		if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
			var consultarURL2 = {};

			consultarURL2.success = function(e) {
				var elemento = e,
					valor = e;
				datos = null;
				elemento = null, valor = null;
			};

			consultarURL2.error = function(e) {
				var elemento = e,
					valor = e;
				if (Ti.App.deployType != 'production') console.log('evento iniciar tarea se fue a la cola', {});
				var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
				var datos = ('datos' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['datos'] : '';
				var insertarModelo3_m = Alloy.Collections.cola;
				var insertarModelo3_fila = Alloy.createModel('cola', {
					data: JSON.stringify(datos),
					id_tarea: seltarea.id_server,
					tipo: 'iniciar'
				});
				insertarModelo3_m.add(insertarModelo3_fila);
				insertarModelo3_fila.save();
				datos = null;
				elemento = null, valor = null;
			};
			require('helper').ajaxUnico('consultarURL2', '' + String.format(L('x1627917957', '%1$siniciarTarea'), url_server.toString()) + '', 'POST', {
				id_inspector: inspector.id_server,
				codigo_identificador: inspector.codigo_identificador,
				id_tarea: seltarea.id_server,
				num_caso: seltarea.num_caso
			}, 15000, consultarURL2);
		} else {
			if (Ti.App.deployType != 'production') console.log('evento iniciar tarea se fue a la cola', {});
			var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
			var datos = ('datos' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['datos'] : '';
			var insertarModelo4_m = Alloy.Collections.cola;
			var insertarModelo4_fila = Alloy.createModel('cola', {
				data: JSON.stringify(datos),
				id_tarea: seltarea.id_server,
				tipo: 'iniciar'
			});
			insertarModelo4_m.add(insertarModelo4_fila);
			insertarModelo4_fila.save();
			datos = null;
		}
	}
	require('vars')['seguir_tarea'] = '';
	/** 
	 * Modificamos en 0.1 segundos el color del statusbar 
	 */
	var ID_583987458_func = function() {
		var inicio_statusbar = '#006C9B';

		var setearStatusColor = function(inicio_statusbar) {
			if (OS_IOS) {
				if (inicio_statusbar == 'light' || inicio_statusbar == 'claro') {
					$.inicio_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.LIGHT_CONTENT);
				} else if (inicio_statusbar == 'grey' || inicio_statusbar == 'gris' || inicio_statusbar == 'gray') {
					$.inicio_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.GREY);
				} else if (inicio_statusbar == 'oscuro' || inicio_statusbar == 'dark') {
					$.inicio_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.DEFAULT);
				}
			} else if (OS_ANDROID) {
				abx.setStatusbarColor(inicio_statusbar);
			}
		};
		setearStatusColor(inicio_statusbar);

	};
	var ID_583987458 = setTimeout(ID_583987458_func, 1000 * 0.1);
})();


/** 
 * Cerramos esta pantalla cuando es llamada en la pantalla siguente (datosbasicos) 
 */

_my_events['_cerrar_insp,ID_231485652'] = function(evento) {
	if (!_.isUndefined(evento.pantalla)) {
		if (evento.pantalla == L('x155149119_traducir', 'hayalguien')) {
			if (Ti.App.deployType != 'production') console.log('debug cerrando hayalguien', {});

			var ID_1436370236_trycatch = {
				error: function(e) {
					if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
				}
			};
			try {
				ID_1436370236_trycatch.error = function(evento) {
					if (Ti.App.deployType != 'production') console.log('error cerrando hayalguien', {});
				};
				$.inicio.close();
			} catch (e) {
				ID_1436370236_trycatch.error(e);
			}
		}
	} else {
		if (Ti.App.deployType != 'production') console.log('debug cerrando (todas) hayalguien', {});

		var ID_1962200830_trycatch = {
			error: function(e) {
				if (Ti.App.deployType != 'production') console.log('error en comando probar: recuerda poner evento ?error como primer hijo.', e);
			}
		};
		try {
			ID_1962200830_trycatch.error = function(evento) {
				if (Ti.App.deployType != 'production') console.log('error cerrando hayalguien', {});
			};
			$.inicio.close();
		} catch (e) {
			ID_1962200830_trycatch.error(e);
		}
	}
};
Alloy.Events.on('_cerrar_insp', _my_events['_cerrar_insp,ID_231485652']);

function Androidback_inicio(e) {

	e.cancelBubble = true;
	var elemento = e.source;

}

function Postlayout_inicio(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	var ID_1941683100_func = function() {
		/** 
		 * Una vez ya cargada la tarea, dejamos la variable limpia para poder abrir otras pantallas 
		 */
		require('vars')['var_abriendo'] = '';
	};
	var ID_1941683100 = setTimeout(ID_1941683100_func, 1000 * 0.2);

}
if (OS_IOS || OS_ANDROID) {
	$.inicio.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}
//$.inicio.open();
