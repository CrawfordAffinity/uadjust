var _bind4section = {};
var _list_templates = {};

var _activity;
if (OS_ANDROID) {
	_activity = $.inicio.activity;
	var abx = require('com.alcoapps.actionbarextras');
}
var _my_events = {},
	_out_vars = {},
	$item = {},
	args = arguments[0] || {};
if ('__args' in args && '__id' in args['__args']) args.__id = args['__args'].__id;
if ('__modelo' in args && _.keys(args.__modelo).length > 0 && 'item' in $) {
	$.item.set(args.__modelo);
	$item = $.item.toJSON();
}
var _var_scopekey = 'inicio';
require('vars')[_var_scopekey] = {};
if (OS_ANDROID) {
	$.inicio.addEventListener('open', function(e) {});
}
$.inicio.orientationModes = [Titanium.UI.PORTRAIT];


$.widgetBarra.init({
	titulo: L('x1452485313_traducir', 'FINALIZADO'),
	__id: 'ALL138747237',
	fondo: 'fondoazul',
	top: 0,
	modal: L('', '')
});


$.widgetMono.init({
	titulo: L('x542998687_traducir', '¡FELICIDADES EN TERMINAR CON EXITO TU INSPECCION!'),
	__id: 'ALL395419113',
	texto: L('x1072203252_traducir', 'Tu inspección será recibida por nosotros, gracias por tu trabajo.'),
	top: 40,
	tipo: '_info'
});


$.widgetBotonlargo.init({
	titulo: L('x1197985134_traducir', 'SALIR'),
	__id: 'ALL1012442242',
	color: 'verde',
	onclick: Click_widgetBotonlargo
});

function Click_widgetBotonlargo(e) {

	var evento = e;
	/** 
	 * Avisamos que ya no estamos en inspeccion 
	 */
	require('vars')['inspeccion_encurso'] = L('x734881840_traducir', 'false');
	var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
	var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	var eliminarModelo_i = Alloy.Collections.tareas;
	var sql = 'DELETE FROM ' + eliminarModelo_i.config.adapter.collection_name + ' WHERE id_server=\'' + seltarea.id_server + '\'';
	var db = Ti.Database.open(eliminarModelo_i.config.adapter.db_name);
	db.execute(sql);
	db.close();
	eliminarModelo_i.trigger('remove');
	if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
		var consultarURL = {};
		console.log('DEBUG WEB: requesting url:' + String.format(L('x4082712253', '%1$sobtenerHistorialTareas'), url_server.toString()) + ' with data:', {
			_method: 'POST',
			_params: {
				id_inspector: inspector.id_server
			},
			_timeout: '15000'
		});

		consultarURL.success = function(e) {
			var elemento = e,
				valor = e;
			var eliminarModelo2_i = Alloy.Collections.historial_tareas;
			var sql = "DELETE FROM " + eliminarModelo2_i.config.adapter.collection_name;
			var db = Ti.Database.open(eliminarModelo2_i.config.adapter.db_name);
			db.execute(sql);
			db.close();
			eliminarModelo2_i.trigger('remove');
			var elemento_historial_tareas = elemento.historial_tareas;
			var insertarModelo_m = Alloy.Collections.historial_tareas;
			var db_insertarModelo = Ti.Database.open(insertarModelo_m.config.adapter.db_name);
			db_insertarModelo.execute('BEGIN');
			_.each(elemento_historial_tareas, function(insertarModelo_fila, pos) {
				db_insertarModelo.execute('INSERT INTO historial_tareas (fecha_tarea, id_inspeccion, id_asegurado, nivel_2, comentario_can_o_rech, asegurado_tel_fijo, estado_tarea, bono, evento, id_inspector, asegurado_codigo_identificador, lat, nivel_1, asegurado_nombre, pais, direccion, asegurador, fecha_ingreso, fecha_siniestro, nivel_1_texto, distance, fecha_termino, nivel_4, perfil, asegurado_id, id_server, categoria, nivel_3, asegurado_correo, num_caso, lon, asegurado_tel_movil, nivel_5, tipo_tarea, hora_termino) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', insertarModelo_fila.fecha_tarea, insertarModelo_fila.id_inspeccion, insertarModelo_fila.id_asegurado, insertarModelo_fila.nivel_2, insertarModelo_fila.comentario_can_o_rech, insertarModelo_fila.asegurado_tel_fijo, insertarModelo_fila.estado_tarea, insertarModelo_fila.bono, insertarModelo_fila.evento, insertarModelo_fila.id_inspector, insertarModelo_fila.asegurado_codigo_identificador, insertarModelo_fila.lat, insertarModelo_fila.nivel_1, insertarModelo_fila.asegurado_nombre, insertarModelo_fila.pais, insertarModelo_fila.direccion, insertarModelo_fila.asegurador, insertarModelo_fila.fecha_ingreso, insertarModelo_fila.fecha_siniestro, insertarModelo_fila.nivel_1_, insertarModelo_fila.distancia, insertarModelo_fila.fecha_finalizacion, insertarModelo_fila.nivel_4, 'ubicacion', insertarModelo_fila.asegurado_id, insertarModelo_fila.id, insertarModelo_fila.categoria, insertarModelo_fila.nivel_3, insertarModelo_fila.asegurado_correo, insertarModelo_fila.num_caso, insertarModelo_fila.lon, insertarModelo_fila.asegurado_tel_movil, insertarModelo_fila.nivel_5, insertarModelo_fila.tipo_tarea, insertarModelo_fila.hora_finalizacion);
			});
			db_insertarModelo.execute('COMMIT');
			db_insertarModelo.close();
			db_insertarModelo = null;
			insertarModelo_m.trigger('change');
			/** 
			 * Llamo refresco tareas 
			 */

			Alloy.Events.trigger('refrescar_historial');
			var ID_983129480_func = function() {
				/** 
				 * Para refrescar mistareas y otras ventanas 
				 */

				Alloy.Events.trigger('_refrescar_tareas_mistareas');
				/** 
				 * Refrescar pantalla hoy 
				 */

				Alloy.Events.trigger('_calcular_ruta');
				if (Ti.App.deployType != 'production') console.log('ejecutado el delay refescar mistareas', {});
				if (Ti.App.deployType != 'production') console.log('con internet success cerrando pantalla', {});
				/** 
				 * Cerramos pantalla fin 
				 */
				$.inicio.close();
			};
			var ID_983129480 = setTimeout(ID_983129480_func, 1000 * 0.2);
			elemento = null, valor = null;
		};

		consultarURL.error = function(e) {
			var elemento = e,
				valor = e;
			var ID_1938634339_func = function() {
				/** 
				 * Para refrescar mistareas y otras ventanas 
				 */

				Alloy.Events.trigger('_refrescar_tareas_mistareas');
				/** 
				 * Refrescar pantalla hoy 
				 */

				Alloy.Events.trigger('_calcular_ruta');
				if (Ti.App.deployType != 'production') console.log('ejecutado el delay refescar mistareas', {});
				if (Ti.App.deployType != 'production') console.log('con internet error cerrando pantalla', {});
				/** 
				 * Cerramos pantalla fin 
				 */
				$.inicio.close();
			};
			var ID_1938634339 = setTimeout(ID_1938634339_func, 1000 * 0.2);
			elemento = null, valor = null;
		};
		require('helper').ajaxUnico('consultarURL', '' + String.format(L('x4082712253', '%1$sobtenerHistorialTareas'), url_server.toString()) + '', 'POST', {
			id_inspector: inspector.id_server
		}, 15000, consultarURL);
	} else {
		var ID_1195852335_func = function() {
			/** 
			 * Para refrescar mistareas y otras ventanas 
			 */

			Alloy.Events.trigger('_refrescar_tareas_mistareas');
			/** 
			 * Refrescar pantalla hoy 
			 */

			Alloy.Events.trigger('_calcular_ruta');
			if (Ti.App.deployType != 'production') console.log('ejecutado el delay refescar mistareas', {});
			if (Ti.App.deployType != 'production') console.log('sin internet cerrando pantalla', {});
			/** 
			 * Cerramos pantalla fin 
			 */
			$.inicio.close();
		};
		var ID_1195852335 = setTimeout(ID_1195852335_func, 1000 * 0.2);
	}
}

function Postlayout_inicio(e) {

	e.cancelBubble = true;
	var elemento = e.source;
	/** 
	 * Limpieza memoria 
	 */

	Alloy.Events.trigger('_cerrar_insp', {
		pantalla: 'firma'
	});

}

(function() {
	var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
	if (_.isObject(seltarea) && !_.isArray(seltarea) && !_.isFunction(seltarea)) {
		/** 
		 * Enviamos datos de inspeccion a servidor 
		 */
		/** 
		 * Enviamos datos 
		 */

		$.widgetBotonlargo.iniciar_progreso({});
		var consultando_datos = L('x4157762361_traducir', 'status: consultando datos...');
		$.StatusEsperando.setText(consultando_datos);

		/** 
		 * Consultamos todas las tablas de la inspeccion 
		 */
		var consultarModelo_i = Alloy.createCollection('insp_fotosrequeridas');
		var consultarModelo_i_where = 'id_inspeccion=\'' + seltarea.id_server + '\'';
		consultarModelo_i.fetch({
			query: 'SELECT * FROM insp_fotosrequeridas WHERE id_inspeccion=\'' + seltarea.id_server + '\''
		});
		var fotos = require('helper').query2array(consultarModelo_i);
		var consultarModelo2_i = Alloy.createCollection('insp_datosbasicos');
		var consultarModelo2_i_where = 'id_inspeccion=\'' + seltarea.id_server + '\'';
		consultarModelo2_i.fetch({
			query: 'SELECT * FROM insp_datosbasicos WHERE id_inspeccion=\'' + seltarea.id_server + '\''
		});
		var basicos = require('helper').query2array(consultarModelo2_i);
		var consultarModelo3_i = Alloy.createCollection('insp_caracteristicas');
		var consultarModelo3_i_where = 'id_inspeccion=\'' + seltarea.id_server + '\'';
		consultarModelo3_i.fetch({
			query: 'SELECT * FROM insp_caracteristicas WHERE id_inspeccion=\'' + seltarea.id_server + '\''
		});
		var cara = require('helper').query2array(consultarModelo3_i);
		var consultarModelo4_i = Alloy.createCollection('insp_niveles');
		var consultarModelo4_i_where = 'id_inspeccion=\'' + seltarea.id_server + '\'';
		consultarModelo4_i.fetch({
			query: 'SELECT * FROM insp_niveles WHERE id_inspeccion=\'' + seltarea.id_server + '\''
		});
		var niveles = require('helper').query2array(consultarModelo4_i);
		var consultarModelo5_i = Alloy.createCollection('insp_siniestro');
		var consultarModelo5_i_where = 'id_inspeccion=\'' + seltarea.id_server + '\'';
		consultarModelo5_i.fetch({
			query: 'SELECT * FROM insp_siniestro WHERE id_inspeccion=\'' + seltarea.id_server + '\''
		});
		var siniestro = require('helper').query2array(consultarModelo5_i);
		var consultarModelo6_i = Alloy.createCollection('insp_recintos');
		var consultarModelo6_i_where = 'id_inspeccion=\'' + seltarea.id_server + '\'';
		consultarModelo6_i.fetch({
			query: 'SELECT * FROM insp_recintos WHERE id_inspeccion=\'' + seltarea.id_server + '\''
		});
		var recintos = require('helper').query2array(consultarModelo6_i);
		var consultarModelo7_i = Alloy.createCollection('insp_itemdanos');
		var consultarModelo7_i_where = 'id_inspeccion=\'' + seltarea.id_server + '\'';
		consultarModelo7_i.fetch({
			query: 'SELECT * FROM insp_itemdanos WHERE id_inspeccion=\'' + seltarea.id_server + '\''
		});
		var danos = require('helper').query2array(consultarModelo7_i);
		var consultarModelo8_i = Alloy.createCollection('insp_contenido');
		var consultarModelo8_i_where = 'id_inspeccion=\'' + seltarea.id_server + '\'';
		consultarModelo8_i.fetch({
			query: 'SELECT * FROM insp_contenido WHERE id_inspeccion=\'' + seltarea.id_server + '\''
		});
		var contenidos = require('helper').query2array(consultarModelo8_i);
		var consultarModelo9_i = Alloy.createCollection('insp_documentos');
		var consultarModelo9_i_where = 'id_inspeccion=\'' + seltarea.id_server + '\'';
		consultarModelo9_i.fetch({
			query: 'SELECT * FROM insp_documentos WHERE id_inspeccion=\'' + seltarea.id_server + '\''
		});
		var documentos = require('helper').query2array(consultarModelo9_i);
		var consultarModelo10_i = Alloy.createCollection('insp_firma');
		var consultarModelo10_i_where = 'id_inspeccion=\'' + seltarea.id_server + '\'';
		consultarModelo10_i.fetch({
			query: 'SELECT * FROM insp_firma WHERE id_inspeccion=\'' + seltarea.id_server + '\''
		});
		var firma = require('helper').query2array(consultarModelo10_i);
		/** 
		 * Actualizamos fecha fin de inspeccion actual 
		 */
		var consultarModelo11_i = Alloy.createCollection('inspecciones');
		var consultarModelo11_i_where = 'id_server=\'' + seltarea.id_server + '\'';
		consultarModelo11_i.fetch({
			query: 'SELECT * FROM inspecciones WHERE id_server=\'' + seltarea.id_server + '\''
		});
		var nula = require('helper').query2array(consultarModelo11_i);
		var db = Ti.Database.open(consultarModelo11_i.config.adapter.db_name);
		if (consultarModelo11_i_where == '') {
			var sql = 'UPDATE ' + consultarModelo11_i.config.adapter.collection_name + ' SET fecha_inspeccion_fin=\'' + new Date() + '\'';
		} else {
			var sql = 'UPDATE ' + consultarModelo11_i.config.adapter.collection_name + ' SET fecha_inspeccion_fin=\'' + new Date() + '\' WHERE ' + consultarModelo11_i_where;
		}
		db.execute(sql);
		db.close();
		var consultarModelo12_i = Alloy.createCollection('inspecciones');
		var consultarModelo12_i_where = 'id_server=\'' + seltarea.id_server + '\'';
		consultarModelo12_i.fetch({
			query: 'SELECT * FROM inspecciones WHERE id_server=\'' + seltarea.id_server + '\''
		});
		var inspecciones = require('helper').query2array(consultarModelo12_i);
		var enviando = L('x3992720377_traducir', 'status: enviando...');
		$.StatusEsperando.setText(enviando);

		var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
		if ((_.isObject(url_server) || (_.isString(url_server)) && !_.isEmpty(url_server)) || _.isNumber(url_server) || _.isBoolean(url_server)) {
			/** 
			 * Se crea una estructura con los datos que iban a ser enviados al servidor, notificando que la inspeccion fue cancelada por algun motivo 
			 */
			var datos = {
				fotosrequeridas: fotos,
				datosbasicos: basicos,
				caracteristicas: cara,
				niveles: niveles,
				siniestro: siniestro,
				recintos: recintos,
				itemdanos: danos,
				contenido: contenidos,
				documentos: documentos,
				inspecciones: inspecciones,
				firma: firma
			};
			require('vars')[_var_scopekey]['datos'] = datos;
			if (Ti.App.deployType != 'production') console.log('no es un simulador EBH', {});
			if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
				var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
				var consultarURL2 = {};
				console.log('DEBUG WEB: requesting url:' + String.format(L('x3471857721', '%1$sfinalizarTarea'), url_server.toString()) + ' with JSON data:', {
					_method: 'POSTJSON',
					_params: {
						fotosrequeridas: fotos,
						datosbasicos: basicos,
						caracteristicas: cara,
						niveles: niveles,
						siniestro: siniestro,
						recintos: recintos,
						itemdanos: danos,
						contenido: contenidos,
						documentos: documentos,
						inspecciones: inspecciones,
						firma: firma,
						id_app: seltarea.id_server
					},
					_timeout: '15000'
				});

				consultarURL2.success = function(e) {
					var elemento = e,
						valor = e;
					if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
						/** 
						 * Llamamos funcion enviarFirmas 
						 */
						var ID_266553545 = null;
						if ('enviarfirmas' in require('funciones')) {
							ID_266553545 = require('funciones').enviarfirmas({});
						} else {
							try {
								ID_266553545 = f_enviarfirmas({});
							} catch (ee) {}
						}
					} else {}
					if (Ti.App.deployType != 'production') console.log('success de finalizar tarea EBH', {});
					var limp_temporales = L('x2911704577_traducir', 'status: limpiando temporales');
					var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
					$.StatusEsperando.setText(limp_temporales);

					var eliminarModelo3_i = Alloy.Collections.insp_datosbasicos;
					var sql = 'DELETE FROM ' + eliminarModelo3_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
					var db = Ti.Database.open(eliminarModelo3_i.config.adapter.db_name);
					db.execute(sql);
					db.close();
					eliminarModelo3_i.trigger('remove');
					var eliminarModelo4_i = Alloy.Collections.insp_caracteristicas;
					var sql = 'DELETE FROM ' + eliminarModelo4_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
					var db = Ti.Database.open(eliminarModelo4_i.config.adapter.db_name);
					db.execute(sql);
					db.close();
					eliminarModelo4_i.trigger('remove');
					var eliminarModelo5_i = Alloy.Collections.insp_niveles;
					var sql = 'DELETE FROM ' + eliminarModelo5_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
					var db = Ti.Database.open(eliminarModelo5_i.config.adapter.db_name);
					db.execute(sql);
					db.close();
					eliminarModelo5_i.trigger('remove');
					var eliminarModelo6_i = Alloy.Collections.insp_siniestro;
					var sql = 'DELETE FROM ' + eliminarModelo6_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
					var db = Ti.Database.open(eliminarModelo6_i.config.adapter.db_name);
					db.execute(sql);
					db.close();
					eliminarModelo6_i.trigger('remove');
					var eliminarModelo7_i = Alloy.Collections.insp_recintos;
					var sql = 'DELETE FROM ' + eliminarModelo7_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
					var db = Ti.Database.open(eliminarModelo7_i.config.adapter.db_name);
					db.execute(sql);
					db.close();
					eliminarModelo7_i.trigger('remove');
					var eliminarModelo8_i = Alloy.Collections.insp_itemdanos;
					var sql = 'DELETE FROM ' + eliminarModelo8_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
					var db = Ti.Database.open(eliminarModelo8_i.config.adapter.db_name);
					db.execute(sql);
					db.close();
					eliminarModelo8_i.trigger('remove');
					var eliminarModelo9_i = Alloy.Collections.insp_contenido;
					var sql = 'DELETE FROM ' + eliminarModelo9_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
					var db = Ti.Database.open(eliminarModelo9_i.config.adapter.db_name);
					db.execute(sql);
					db.close();
					eliminarModelo9_i.trigger('remove');
					var eliminarModelo10_i = Alloy.Collections.insp_documentos;
					var sql = 'DELETE FROM ' + eliminarModelo10_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
					var db = Ti.Database.open(eliminarModelo10_i.config.adapter.db_name);
					db.execute(sql);
					db.close();
					eliminarModelo10_i.trigger('remove');
					var eliminarModelo11_i = Alloy.Collections.inspecciones;
					var sql = 'DELETE FROM ' + eliminarModelo11_i.config.adapter.collection_name + ' WHERE id_server=\'' + seltarea.id_server + '\'';
					var db = Ti.Database.open(eliminarModelo11_i.config.adapter.db_name);
					db.execute(sql);
					db.close();
					eliminarModelo11_i.trigger('remove');
					/** 
					 * Eliminando datos enviados 
					 */
					/** 
					 * Eliminando datos enviados 
					 */

					var eliminarModelo12_i = Alloy.Collections.insp_fotosrequeridas;
					var sql = "DELETE FROM " + eliminarModelo12_i.config.adapter.collection_name;
					var db = Ti.Database.open(eliminarModelo12_i.config.adapter.db_name);
					db.execute(sql);
					db.close();
					eliminarModelo12_i.trigger('remove');
					if (Ti.App.deployType != 'production') console.log('eliminados todas las tareas EBH', {});
					/** 
					 * Limpieza de memoria 
					 */
					insp_fotosrequeridas = null, insp_datosbasicos = null, insp_caracteristicas = null, insp_niveles = null, insp_siniestro = null, insp_recintos = null, insp_itemdanos = null, insp_contenido = null, insp_documentos = null, insp_firma = null, inspecciones = null;
					if (Ti.App.deployType != 'production') console.log('limpieza variables EBH', {});
					var ID_1546591205_func = function() {
						if (Ti.App.deployType != 'production') console.log('ejecutado con desface... pero enviado EBH', {});
						var sta_listo = L('x145709933_traducir', 'status: listo');
						$.StatusEsperando.setText(sta_listo);


						$.widgetBotonlargo.detener_progreso({});
					};
					var ID_1546591205 = setTimeout(ID_1546591205_func, 1000 * 0.5);
					elemento = null, valor = null;
				};

				consultarURL2.error = function(e) {
					var elemento = e,
						valor = e;
					if (Ti.App.deployType != 'production') console.log('hubo error enviando inspeccion', {
						"elemento": elemento
					});
					if (Ti.App.deployType != 'production') console.log('agregando servicio finalizarTarea a cola', {});
					var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
					var datos = ('datos' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['datos'] : '';
					var insertarModelo2_m = Alloy.Collections.cola;
					var insertarModelo2_fila = Alloy.createModel('cola', {
						data: JSON.stringify(datos),
						id_tarea: seltarea.id_server,
						tipo: 'enviar'
					});
					insertarModelo2_m.add(insertarModelo2_fila);
					insertarModelo2_fila.save();
					var eliminarModelo13_i = Alloy.Collections.insp_datosbasicos;
					var sql = 'DELETE FROM ' + eliminarModelo13_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
					var db = Ti.Database.open(eliminarModelo13_i.config.adapter.db_name);
					db.execute(sql);
					db.close();
					eliminarModelo13_i.trigger('remove');
					var eliminarModelo14_i = Alloy.Collections.insp_caracteristicas;
					var sql = 'DELETE FROM ' + eliminarModelo14_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
					var db = Ti.Database.open(eliminarModelo14_i.config.adapter.db_name);
					db.execute(sql);
					db.close();
					eliminarModelo14_i.trigger('remove');
					var eliminarModelo15_i = Alloy.Collections.insp_niveles;
					var sql = 'DELETE FROM ' + eliminarModelo15_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
					var db = Ti.Database.open(eliminarModelo15_i.config.adapter.db_name);
					db.execute(sql);
					db.close();
					eliminarModelo15_i.trigger('remove');
					var eliminarModelo16_i = Alloy.Collections.insp_siniestro;
					var sql = 'DELETE FROM ' + eliminarModelo16_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
					var db = Ti.Database.open(eliminarModelo16_i.config.adapter.db_name);
					db.execute(sql);
					db.close();
					eliminarModelo16_i.trigger('remove');
					var eliminarModelo17_i = Alloy.Collections.insp_recintos;
					var sql = 'DELETE FROM ' + eliminarModelo17_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
					var db = Ti.Database.open(eliminarModelo17_i.config.adapter.db_name);
					db.execute(sql);
					db.close();
					eliminarModelo17_i.trigger('remove');
					var eliminarModelo18_i = Alloy.Collections.insp_itemdanos;
					var sql = 'DELETE FROM ' + eliminarModelo18_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
					var db = Ti.Database.open(eliminarModelo18_i.config.adapter.db_name);
					db.execute(sql);
					db.close();
					eliminarModelo18_i.trigger('remove');
					var eliminarModelo19_i = Alloy.Collections.insp_contenido;
					var sql = 'DELETE FROM ' + eliminarModelo19_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
					var db = Ti.Database.open(eliminarModelo19_i.config.adapter.db_name);
					db.execute(sql);
					db.close();
					eliminarModelo19_i.trigger('remove');
					var eliminarModelo20_i = Alloy.Collections.insp_documentos;
					var sql = 'DELETE FROM ' + eliminarModelo20_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
					var db = Ti.Database.open(eliminarModelo20_i.config.adapter.db_name);
					db.execute(sql);
					db.close();
					eliminarModelo20_i.trigger('remove');
					var eliminarModelo21_i = Alloy.Collections.inspecciones;
					var sql = 'DELETE FROM ' + eliminarModelo21_i.config.adapter.collection_name + ' WHERE id_server=\'' + seltarea.id_server + '\'';
					var db = Ti.Database.open(eliminarModelo21_i.config.adapter.db_name);
					db.execute(sql);
					db.close();
					eliminarModelo21_i.trigger('remove');
					/** 
					 * Limpieza de memoria 
					 */
					insp_fotosrequeridas = null, insp_datosbasicos = null, insp_caracteristicas = null, insp_niveles = null, insp_siniestro = null, insp_recintos = null, insp_itemdanos = null, insp_contenido = null, insp_documentos = null, insp_firma = null, inspecciones = null;
					var ID_1164055122_func = function() {
						var sta_sininternet = L('x461379474_traducir', 'status: sin internet, agregado a cola de salida');
						$.StatusEsperando.setText(sta_sininternet);


						$.widgetBotonlargo.detener_progreso({});
					};
					var ID_1164055122 = setTimeout(ID_1164055122_func, 1000 * 0.5);
					elemento = null, valor = null;
				};
				require('helper').ajaxUnico('consultarURL2', '' + String.format(L('x3471857721', '%1$sfinalizarTarea'), url_server.toString()) + '', 'POSTJSON', {
					fotosrequeridas: fotos,
					datosbasicos: basicos,
					caracteristicas: cara,
					niveles: niveles,
					siniestro: siniestro,
					recintos: recintos,
					itemdanos: danos,
					contenido: contenidos,
					documentos: documentos,
					inspecciones: inspecciones,
					firma: firma,
					id_app: seltarea.id_server
				}, 15000, consultarURL2);
			} else {
				if (Ti.App.deployType != 'production') console.log('El celular no detecto internet... agregando a la cola de envio', {});
				var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
				var datos = ('datos' in require('vars')[_var_scopekey]) ? require('vars')[_var_scopekey]['datos'] : '';
				var insertarModelo3_m = Alloy.Collections.cola;
				var insertarModelo3_fila = Alloy.createModel('cola', {
					data: JSON.stringify(datos),
					id_tarea: seltarea.id_server,
					tipo: 'enviar'
				});
				insertarModelo3_m.add(insertarModelo3_fila);
				insertarModelo3_fila.save();
				var eliminarModelo22_i = Alloy.Collections.insp_datosbasicos;
				var sql = 'DELETE FROM ' + eliminarModelo22_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
				var db = Ti.Database.open(eliminarModelo22_i.config.adapter.db_name);
				db.execute(sql);
				db.close();
				eliminarModelo22_i.trigger('remove');
				var eliminarModelo23_i = Alloy.Collections.insp_caracteristicas;
				var sql = 'DELETE FROM ' + eliminarModelo23_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
				var db = Ti.Database.open(eliminarModelo23_i.config.adapter.db_name);
				db.execute(sql);
				db.close();
				eliminarModelo23_i.trigger('remove');
				var eliminarModelo24_i = Alloy.Collections.insp_niveles;
				var sql = 'DELETE FROM ' + eliminarModelo24_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
				var db = Ti.Database.open(eliminarModelo24_i.config.adapter.db_name);
				db.execute(sql);
				db.close();
				eliminarModelo24_i.trigger('remove');
				var eliminarModelo25_i = Alloy.Collections.insp_siniestro;
				var sql = 'DELETE FROM ' + eliminarModelo25_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
				var db = Ti.Database.open(eliminarModelo25_i.config.adapter.db_name);
				db.execute(sql);
				db.close();
				eliminarModelo25_i.trigger('remove');
				var eliminarModelo26_i = Alloy.Collections.insp_recintos;
				var sql = 'DELETE FROM ' + eliminarModelo26_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
				var db = Ti.Database.open(eliminarModelo26_i.config.adapter.db_name);
				db.execute(sql);
				db.close();
				eliminarModelo26_i.trigger('remove');
				var eliminarModelo27_i = Alloy.Collections.insp_itemdanos;
				var sql = 'DELETE FROM ' + eliminarModelo27_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
				var db = Ti.Database.open(eliminarModelo27_i.config.adapter.db_name);
				db.execute(sql);
				db.close();
				eliminarModelo27_i.trigger('remove');
				var eliminarModelo28_i = Alloy.Collections.insp_contenido;
				var sql = 'DELETE FROM ' + eliminarModelo28_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
				var db = Ti.Database.open(eliminarModelo28_i.config.adapter.db_name);
				db.execute(sql);
				db.close();
				eliminarModelo28_i.trigger('remove');
				var eliminarModelo29_i = Alloy.Collections.insp_documentos;
				var sql = 'DELETE FROM ' + eliminarModelo29_i.config.adapter.collection_name + ' WHERE id_inspeccion=\'' + seltarea.id_server + '\'';
				var db = Ti.Database.open(eliminarModelo29_i.config.adapter.db_name);
				db.execute(sql);
				db.close();
				eliminarModelo29_i.trigger('remove');
				var eliminarModelo30_i = Alloy.Collections.inspecciones;
				var sql = 'DELETE FROM ' + eliminarModelo30_i.config.adapter.collection_name + ' WHERE id_server=\'' + seltarea.id_server + '\'';
				var db = Ti.Database.open(eliminarModelo30_i.config.adapter.db_name);
				db.execute(sql);
				db.close();
				eliminarModelo30_i.trigger('remove');
				/** 
				 * Limpieza de memoria 
				 */
				insp_fotosrequeridas = null, insp_datosbasicos = null, insp_caracteristicas = null, insp_niveles = null, insp_siniestro = null, insp_recintos = null, insp_itemdanos = null, insp_contenido = null, insp_documentos = null, insp_firma = null, inspecciones = null;
				var ID_93492704_func = function() {
					if (Ti.App.deployType != 'production') console.log('ejecute con desface para ver si lo soluciona', {});
					var sta_sininternet = L('x461379474_traducir', 'status: sin internet, agregado a cola de salida');
					$.StatusEsperando.setText(sta_sininternet);


					$.widgetBotonlargo.detener_progreso({});
				};
				var ID_93492704 = setTimeout(ID_93492704_func, 1000 * 0.5);
			}
		} else {
			$.StatusEsperando.setText('status: simulador ..');


			$.widgetBotonlargo.detener_progreso({});
		}
	} else {
		$.StatusEsperando.setText('status: simulador ..');

	}
	var ID_557059512_func = function() {
		var inicio_statusbar = '#006C9B';

		var setearStatusColor = function(inicio_statusbar) {
			if (OS_IOS) {
				if (inicio_statusbar == 'light' || inicio_statusbar == 'claro') {
					$.inicio_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.LIGHT_CONTENT);
				} else if (inicio_statusbar == 'grey' || inicio_statusbar == 'gris' || inicio_statusbar == 'gray') {
					$.inicio_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.GREY);
				} else if (inicio_statusbar == 'oscuro' || inicio_statusbar == 'dark') {
					$.inicio_window.setStatusBarStyle(Titanium.UI.iOS.StatusBar.DEFAULT);
				}
			} else if (OS_ANDROID) {
				abx.setStatusbarColor(inicio_statusbar);
			}
		};
		setearStatusColor(inicio_statusbar);

	};
	var ID_557059512 = setTimeout(ID_557059512_func, 1000 * 0.1);
})();

if (OS_IOS || OS_ANDROID) {
	$.inicio.addEventListener('close', function() {
		$.destroy(); // cleanup bindings
		$.off(); //remove backbone events of this controller
		var _ev_tmp = null,
			_ev_rem = null;
		if (_my_events) {
			for (_ev_tmp in _my_events) {
				try {
					if (_ev_tmp.indexOf('_web') != -1) {
						Ti.App.removeEventListener(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					} else {
						Alloy.Events.off(_ev_tmp.split(',')[0], _my_events[_ev_tmp]);
					}
				} catch (err10) {}
			}
			_my_events = null;
			//delete _my_events;
		}
		if (_out_vars) {
			for (_ev_tmp in _out_vars) {
				for (_ev_rem in _out_vars[_ev_tmp]._remove) {
					try {
						eval(_out_vars[_ev_tmp]._remove[_ev_rem]);
					} catch (_errt) {}
				}
				_out_vars[_ev_tmp] = null;
			}
			_ev_tmp = null;
			//delete _out_vars;
		}
	});
}
//$.inicio.open();
