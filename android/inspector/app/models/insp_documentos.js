exports.definition = {
	config: {
		columns: {
			"doc10": "INTEGER",
			"id_inspeccion": "INTEGER",
			"doc6": "INTEGER",
			"doc4": "INTEGER",
			"doc7": "INTEGER",
			"edificio": "INTEGER",
			"doc3": "INTEGER",
			"doc9": "INTEGER",
			"dias": "INTEGER",
			"doc1": "INTEGER",
			"contenidos": "INTEGER",
			"doc5": "INTEGER",
			"id": "INTEGER PRIMARY KEY AUTOINCREMENT",
			"otros": "TEXT",
			"doc8": "INTEGER",
			"doc2": "INTEGER",
		},
		adapter: {
			"type": "sql",
			"collection_name": "insp_documentos",
			"idAttribute": "id",
			"remoteBackup": "false",
		}
	},
	extendModel: function(Model) {
		_.extend(Model.prototype, {
			transform: function transform() {
				var transformed = this.toJSON();
				return transformed;
			}
		});
		return Model;
	},
	extendCollection: function(Collection) {
		// helper functions
		function S4() {
			return (0 | 65536 * (1 + Math.random())).toString(16).substring(1);
		}
		function guid() {
			return S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4();
		}
		// start extend
		_.extend(Collection.prototype, {
			deleteAll : function() {
				var collection = this;
				var sql = "DELETE FROM " + collection.config.adapter.collection_name;
				db = Ti.Database.open(collection.config.adapter.db_name);
				db.execute(sql);
				db.close();
				collection.trigger('sync');
			}
		});
		// end extend
		return Collection;
	}
};
