var funciones = {}
funciones.formatear_fecha = function(x_params) {
	var formato = x_params['formato'];
	var fecha = x_params['fecha'];
	var formato_entrada = x_params['formato_entrada'];
	var moment = require("alloy/moment");
	var fecha_era_texto = (typeof fecha === 'string' || typeof fecha === 'number') ? true : false;
	var nuevo = '';
	if (fecha_era_texto == true || fecha_era_texto == 'true') {
		nuevo = moment(fecha, formato_entrada).format(formato);
	} else {
		nuevo = moment(fecha).format(formato);
	}
	return nuevo;
};
funciones.consumir_cola = function(x_params) {
	var consultarModelo5_i = Alloy.createCollection('cola');
	var consultarModelo5_i_where = '';
	consultarModelo5_i.fetch();
	var lista_enviar = require('helper').query2array(consultarModelo5_i);
	if (lista_enviar && lista_enviar.length) {
		if (Ti.App.deployType != 'production') console.log('procesando cola de salida interna', {});
		var item_index = 0;
		_.each(lista_enviar, function(item, item_pos, item_list) {
			item_index += 1;
			var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
			if (item.tipo == L('x1966285652_traducir', 'cancelar')) {
				if (Ti.App.deployType != 'production') console.log('Voy a enviar un cancelar tarea', {
					"item": item
				});
				data = JSON.parse(item.data);
				var consultarURL7 = {};

				consultarURL7.success = function(e) {
					var elemento = e,
						valor = e;
					if (Ti.App.deployType != 'production') console.log('Respuesta de servidor de cancelar tarea', {
						"elemento": elemento
					});
					if (elemento.error == 0 || elemento.error == '0') {
						var eliminarModelo31_i = Alloy.Collections.cola;
						var sql = 'DELETE FROM ' + eliminarModelo31_i.config.adapter.collection_name + ' WHERE id=\'' + elemento.id_app + '\'';
						var db = Ti.Database.open(eliminarModelo31_i.config.adapter.db_name);
						db.execute(sql);
						db.close();
						eliminarModelo31_i.trigger('remove');
					}
					elemento = null, valor = null;
				};

				consultarURL7.error = function(e) {
					var elemento = e,
						valor = e;
					if (Ti.App.deployType != 'production') console.log('cancelarTarea fallo', {
						"asd": elemento
					});
					elemento = null, valor = null;
				};
				require('helper').ajaxUnico('consultarURL7', '' + String.format(L('x3244141284', '%1$scancelarTarea'), url_server.toString()) + '', 'POST', {
					id_inspector: data.id_inspector,
					codigo_identificador: data.codigo_identificador,
					id_tarea: data.id_server,
					num_caso: data.num_caso,
					mensaje: data.razon,
					opcion: 0,
					tipo: 1,
					id_app: item.id
				}, 15000, consultarURL7);
			} else if (item.tipo == L('x71742335_traducir', 'enviar')) {
				if (Ti.App.deployType != 'production') console.log('Voy a enviar un finalizar tarea', {
					"item": item
				});
				data = JSON.parse(item.data);
				var consultarURL8 = {};
				console.log('DEBUG WEB: requesting url:' + String.format(L('x3471857721', '%1$sfinalizarTarea'), url_server.toString()) + ' with JSON data:', {
					_method: 'POSTJSON',
					_params: {
						fotosrequeridas: data.fotosrequeridas,
						datosbasicos: data.datosbasicos,
						caracteristicas: data.caracteristicas,
						niveles: data.niveles,
						siniestro: data.siniestro,
						recintos: data.recintos,
						itemdanos: data.itemdanos,
						contenido: data.contenido,
						documentos: data.documentos,
						inspecciones: data.inspecciones,
						firma: data.firma,
						id_app: item.id
					},
					_timeout: '15000'
				});

				consultarURL8.success = function(e) {
					var elemento = e,
						valor = e;
					if (Ti.App.deployType != 'production') console.log('Respuesta de servidor de finalizar tarea (cola)', {
						"elemento": elemento
					});
					if (elemento.error == 0 || elemento.error == '0') {
						var ID_1296191031 = null;
						if ('enviarfirmas' in require('funciones')) {
							ID_1296191031 = require('funciones').enviarfirmas({});
						} else {
							try {
								ID_1296191031 = f_enviarfirmas({});
							} catch (ee) {}
						}
						var eliminarModelo32_i = Alloy.Collections.cola;
						var sql = 'DELETE FROM ' + eliminarModelo32_i.config.adapter.collection_name + ' WHERE id=\'' + elemento.id_app + '\'';
						var db = Ti.Database.open(eliminarModelo32_i.config.adapter.db_name);
						db.execute(sql);
						db.close();
						eliminarModelo32_i.trigger('remove');
						var inspector = ('inspector' in require('vars')) ? require('vars')['inspector'] : '';
						var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
						if ((_.isObject(inspector) || (_.isString(inspector)) && !_.isEmpty(inspector)) || _.isNumber(inspector) || _.isBoolean(inspector)) {
							var consultarURL9 = {};
							console.log('DEBUG WEB: requesting url:' + String.format(L('x4082712253', '%1$sobtenerHistorialTareas'), url_server.toString()) + ' with data:', {
								_method: 'POST',
								_params: {
									id_inspector: inspector.id_server
								},
								_timeout: '15000'
							});

							consultarURL9.success = function(e) {
								var elemento = e,
									valor = e;
								var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
								var eliminarModelo33_i = Alloy.Collections.tareas;
								var sql = 'DELETE FROM ' + eliminarModelo33_i.config.adapter.collection_name + ' WHERE id_server=\'' + seltarea.id_server + '\'';
								var db = Ti.Database.open(eliminarModelo33_i.config.adapter.db_name);
								db.execute(sql);
								db.close();
								eliminarModelo33_i.trigger('remove');
								var eliminarModelo34_i = Alloy.Collections.historial_tareas;
								var sql = "DELETE FROM " + eliminarModelo34_i.config.adapter.collection_name;
								var db = Ti.Database.open(eliminarModelo34_i.config.adapter.db_name);
								db.execute(sql);
								db.close();
								eliminarModelo34_i.trigger('remove');
								var elemento_historial_tareas = elemento.historial_tareas;
								var insertarModelo33_m = Alloy.Collections.historial_tareas;
								var db_insertarModelo33 = Ti.Database.open(insertarModelo33_m.config.adapter.db_name);
								db_insertarModelo33.execute('BEGIN');
								_.each(elemento_historial_tareas, function(insertarModelo33_fila, pos) {
									db_insertarModelo33.execute('INSERT INTO historial_tareas (fecha_tarea, id_inspeccion, id_asegurado, nivel_2, comentario_can_o_rech, asegurado_tel_fijo, estado_tarea, bono, evento, id_inspector, asegurado_codigo_identificador, lat, nivel_1, asegurado_nombre, pais, direccion, asegurador, fecha_ingreso, fecha_siniestro, nivel_1_texto, distance, fecha_termino, nivel_4, perfil, asegurado_id, id_server, categoria, nivel_3, asegurado_correo, num_caso, lon, asegurado_tel_movil, nivel_5, tipo_tarea, hora_termino) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', insertarModelo33_fila.fecha_tarea, insertarModelo33_fila.id_inspeccion, insertarModelo33_fila.id_asegurado, insertarModelo33_fila.nivel_2, insertarModelo33_fila.comentario_can_o_rech, insertarModelo33_fila.asegurado_tel_fijo, insertarModelo33_fila.estado_tarea, insertarModelo33_fila.bono, insertarModelo33_fila.evento, insertarModelo33_fila.id_inspector, insertarModelo33_fila.asegurado_codigo_identificador, insertarModelo33_fila.lat, insertarModelo33_fila.nivel_1, insertarModelo33_fila.asegurado_nombre, insertarModelo33_fila.pais, insertarModelo33_fila.direccion, insertarModelo33_fila.asegurador, insertarModelo33_fila.fecha_ingreso, insertarModelo33_fila.fecha_siniestro, insertarModelo33_fila.nivel_1_, insertarModelo33_fila.distancia, insertarModelo33_fila.fecha_finalizacion, insertarModelo33_fila.nivel_4, 'ubicacion', insertarModelo33_fila.asegurado_id, insertarModelo33_fila.id, insertarModelo33_fila.categoria, insertarModelo33_fila.nivel_3, insertarModelo33_fila.asegurado_correo, insertarModelo33_fila.num_caso, insertarModelo33_fila.lon, insertarModelo33_fila.asegurado_tel_movil, insertarModelo33_fila.nivel_5, insertarModelo33_fila.tipo_tarea, insertarModelo33_fila.hora_finalizacion);
								});
								db_insertarModelo33.execute('COMMIT');
								db_insertarModelo33.close();
								db_insertarModelo33 = null;
								insertarModelo33_m.trigger('change');
								/** 
								 * Llamo refresco tareas 
								 */

								Alloy.Events.trigger('refrescar_historial');
								elemento = null, valor = null;
							};

							consultarURL9.error = function(e) {
								var elemento = e,
									valor = e;
								elemento = null, valor = null;
							};
							require('helper').ajaxUnico('consultarURL9', '' + String.format(L('x4082712253', '%1$sobtenerHistorialTareas'), url_server.toString()) + '', 'POST', {
								id_inspector: inspector.id_server
							}, 15000, consultarURL9);
						}
					}
					elemento = null, valor = null;
				};

				consultarURL8.error = function(e) {
					var elemento = e,
						valor = e;
					elemento = null, valor = null;
				};
				require('helper').ajaxUnico('consultarURL8', '' + String.format(L('x3471857721', '%1$sfinalizarTarea'), url_server.toString()) + '', 'POSTJSON', {
					fotosrequeridas: data.fotosrequeridas,
					datosbasicos: data.datosbasicos,
					caracteristicas: data.caracteristicas,
					niveles: data.niveles,
					siniestro: data.siniestro,
					recintos: data.recintos,
					itemdanos: data.itemdanos,
					contenido: data.contenido,
					documentos: data.documentos,
					inspecciones: data.inspecciones,
					firma: data.firma,
					id_app: item.id
				}, 15000, consultarURL8);
			} else if (item.tipo == L('x4199404193', 'confirmar_ruta')) {
				if (Ti.App.deployType != 'production') console.log('Voy a confirmar ruta', {
					"item": item
				});
				data = JSON.parse(item.data);
				var consultarURL10 = {};

				consultarURL10.success = function(e) {
					var elemento = e,
						valor = e;
					if (elemento.error == 0 || elemento.error == '0') {
						var eliminarModelo35_i = Alloy.Collections.cola;
						var sql = 'DELETE FROM ' + eliminarModelo35_i.config.adapter.collection_name + ' WHERE id=\'' + elemento.id_app + '\'';
						var db = Ti.Database.open(eliminarModelo35_i.config.adapter.db_name);
						db.execute(sql);
						db.close();
						eliminarModelo35_i.trigger('remove');
					}
					elemento = null, valor = null;
				};

				consultarURL10.error = function(e) {
					var elemento = e,
						valor = e;
					if (Ti.App.deployType != 'production') console.log('confirmar ruta fallo', {
						"asd": elemento
					});
					elemento = null, valor = null;
				};
				require('helper').ajaxUnico('consultarURL10', '' + String.format(L('x2181838308', '%1$sconfirmarRuta'), url_server.toString()) + '', 'POST', {
					id_inspector: data.id_inspector,
					codigo_identificador: data.codigo_identificador,
					tareas: data.tareas,
					id_app: item.id
				}, 15000, consultarURL10);
			} else if (item.tipo == L('x2313572330_traducir', 'iniciar')) {
				if (Ti.App.deployType != 'production') console.log('Voy a enviar un iniciar tarea', {
					"item": item
				});
				data = JSON.parse(item.data);
				var consultarURL11 = {};

				consultarURL11.success = function(e) {
					var elemento = e,
						valor = e;
					if (Ti.App.deployType != 'production') console.log('Respuesta de servidor de iniciar tarea', {
						"elemento": elemento
					});
					if (elemento.error == 0 || elemento.error == '0') {
						var eliminarModelo36_i = Alloy.Collections.cola;
						var sql = 'DELETE FROM ' + eliminarModelo36_i.config.adapter.collection_name + ' WHERE id=\'' + elemento.id_app + '\'';
						var db = Ti.Database.open(eliminarModelo36_i.config.adapter.db_name);
						db.execute(sql);
						db.close();
						eliminarModelo36_i.trigger('remove');
					}
					elemento = null, valor = null;
				};

				consultarURL11.error = function(e) {
					var elemento = e,
						valor = e;
					if (Ti.App.deployType != 'production') console.log('iniciarTarea fallo', {
						"asd": elemento
					});
					elemento = null, valor = null;
				};
				require('helper').ajaxUnico('consultarURL11', '' + String.format(L('x1627917957', '%1$siniciarTarea'), url_server.toString()) + '', 'POST', {
					id_inspector: data.id_inspector,
					codigo_identificador: data.codigo_identificador,
					id_tarea: data.id_server,
					num_caso: data.num_caso,
					id_app: item.id
				}, 15000, consultarURL11);
			}
		});
	}
	return null;
};
funciones.enviarfirmas = function(x_params) {
	if (Ti.App.deployType != 'production') console.log('entre a enviarFirmas', {});
	var consultarModelo6_i = Alloy.createCollection('insp_firma');
	var consultarModelo6_i_where = '';
	consultarModelo6_i.fetch();
	var firmas = require('helper').query2array(consultarModelo6_i);
	var url_server = ('url_server' in require('vars')) ? require('vars')['url_server'] : '';
	if ((_.isObject(url_server) || (_.isString(url_server)) && !_.isEmpty(url_server)) || _.isNumber(url_server) || _.isBoolean(url_server)) {
		/** 
		 * enviamos la imagen de las firmas 
		 */
		if (firmas && firmas.length) {
			/** 
			 * Revisamos si el modelo de las firmas contiene firmas pendientes por enviar 
			 */
			var firma_index = 0;
			_.each(firmas, function(firma, firma_pos, firma_list) {
				firma_index += 1;
				if (Ti.App.deployType != 'production') console.log('enviando la firma', {
					"datos": firma.firma64
				});
				if (Ti.App.deployType != 'production') console.log(String.format(L('x1276337118_traducir', 'leyendo archivo: {data}/inspeccion%1$s/%2$s'), (firma.id_inspeccion) ? firma.id_inspeccion.toString() : '', (firma.firma64) ? firma.firma64.toString() : ''), {});
				/** 
				 * Leemos archivo de firma 
				 */

				var firmabin = '';
				var ID_1272411058_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + firma.id_inspeccion);
				if (ID_1272411058_d.exists() == true) {
					var ID_1272411058_f = Ti.Filesystem.getFile(ID_1272411058_d.resolve(), firma.firma64);
					if (ID_1272411058_f.exists() == true) {
						firmabin = ID_1272411058_f.read();
					}
					ID_1272411058_f = null;
				}
				ID_1272411058_d = null;
				var consultarURL12 = {};
				console.log('DEBUG WEB: requesting url:' + String.format(L('x757910167', '%1$ssubirImagenes'), url_server.toString()) + ' with data:', {
					_method: 'POST',
					_params: {
						id_tarea: firma.id_inspeccion,
						archivo: firma.firma64,
						imagen: firmabin,
						app_id: firma.firma64
					},
					_timeout: '20000'
				});

				consultarURL12.success = function(e) {
					var elemento = e,
						valor = e;
					if (elemento.error != 0 && elemento.error != '0') {} else if (!_.isUndefined(elemento.app_id)) {
						if (Ti.App.deployType != 'production') console.log('envio firma exitoso', {
							"elemento": elemento
						});
						var consultarModelo7_i = Alloy.createCollection('insp_firma');
						var consultarModelo7_i_where = 'firma64=\'' + elemento.app_id + '\'';
						consultarModelo7_i.fetch({
							query: 'SELECT * FROM insp_firma WHERE firma64=\'' + elemento.app_id + '\''
						});
						var datotarea = require('helper').query2array(consultarModelo7_i);
						if (datotarea && datotarea.length) {
							/** 
							 * borramos archivo que dijo fue exitoso 
							 */
							var ID_576056460_d = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'inspeccion' + datotarea[0].id_inspeccion);
							var ID_576056460_f = Ti.Filesystem.getFile(ID_576056460_d.resolve(), elemento.app_id);
							if (ID_576056460_f.exists() == true) ID_576056460_f.deleteFile();
							/** 
							 * Borramos registro de tabla firma 
							 */
							/** 
							 * Borramos registro de tabla firma 
							 */

							var eliminarModelo37_i = Alloy.Collections.insp_firma;
							var sql = 'DELETE FROM ' + eliminarModelo37_i.config.adapter.collection_name + ' WHERE firma64=\'' + elemento.app_id + '\'';
							var db = Ti.Database.open(eliminarModelo37_i.config.adapter.db_name);
							db.execute(sql);
							db.close();
							eliminarModelo37_i.trigger('remove');
							if (Ti.App.deployType != 'production') console.log(String.format(L('x480248166_traducir', 'firma %1$s borrada'), elemento.app_id.toString()), {});
						}
						/** 
						 * Limpiar memoria 
						 */
						datotarea = null;
					} else {
						/** 
						 * Borramos imagen local porque debe ser historia previa 
						 */
					}
					elemento = null, valor = null;
				};

				consultarURL12.error = function(e) {
					var elemento = e,
						valor = e;
					if (Ti.App.deployType != 'production') console.log('envio firma fallido', {
						"elemento": elemento
					});
					var seltarea = ('seltarea' in require('vars')) ? require('vars')['seltarea'] : '';
					elemento = null, valor = null;
				};
				require('helper').ajaxUnico('consultarURL12', '' + String.format(L('x757910167', '%1$ssubirImagenes'), url_server.toString()) + '', 'POST', {
					id_tarea: firma.id_inspeccion,
					archivo: firma.firma64,
					imagen: firmabin,
					app_id: firma.firma64
				}, 20000, consultarURL12);
			});
		}
	}
	return null;
};

module.exports = funciones;