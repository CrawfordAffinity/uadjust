var plantillas = {}
plantillas.fila_tarea = function(x_params, origen_nav) {
	var data = x_params['data'];
	var ciudad = x_params['ciudad'];
	var seguir = x_params['seguir'];
	var tipotarea = x_params['tipotarea'];
	var comuna = x_params['comuna'];
	var direccion = x_params['direccion'];
	var distancia = x_params['distancia'];
	var sectorTabla = Titanium.UI.createTableViewSection({});
	var filaTabla2 = Titanium.UI.createTableViewRow({
		className: 'hackfilaTabla2'
	});
	var vista112 = Titanium.UI.createView({
		height: '55dp',
		layout: 'composite',
		width: '100%'
	});
	var vista113 = Titanium.UI.createView({
		height: '55dp',
		layout: 'horizontal'
	});
	var vista114 = Titanium.UI.createView({
		height: '100%',
		layout: 'vertical',
		width: '1%'
	});
	vista113.add(vista114);
	var vista115 = Titanium.UI.createView({
		height: Ti.UI.SIZE,
		left: '5dp',
		layout: 'vertical',
		width: '87%'
	});
	var vista116 = Titanium.UI.createView({
		height: Ti.UI.SIZE,
		layout: 'horizontal',
		width: Ti.UI.FILL
	});
	var icono13 = Titanium.UI.createLabel({
		height: Ti.UI.SIZE,
		text: '\uE800',
		color: '#2D9EDB',
		width: Ti.UI.SIZE,
		ancho: '-',
		font: {
			fontFamily: 'beta1',
			fontSize: '15dp'
		}

	});
	vista116.add(icono13);
	var Direccion = Titanium.UI.createLabel({
		ellipsize: Titanium.UI.TEXT_ELLIPSIZE_TRUNCATE_END,
		left: 4,
		text: direccion,
		color: '#4d4d4d',
		wordWrap: false,
		touchEnabled: false,
		width: Ti.UI.FILL,
		textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
		font: {
			fontSize: '16dp'
		}

	});
	vista116.add(Direccion);

	vista115.add(vista116);
	var vista117 = Titanium.UI.createView({
		height: Ti.UI.SIZE,
		left: '0dp',
		layout: 'horizontal',
		width: '100%'
	});
	var vista118 = Titanium.UI.createView({
		height: '20dp',
		layout: 'composite',
		width: '26%',
		top: '2dp'
	});
	var iconoUbicacion12 = Titanium.UI.createLabel({
		height: Ti.UI.SIZE,
		text: '\uE829',
		left: '0dp',
		color: '#2D9EDB',
		width: Ti.UI.SIZE,
		font: {
			fontFamily: 'beta1',
			fontSize: '9dp'
		}

	});
	vista118.add(iconoUbicacion12);
	var Adistancia = Titanium.UI.createLabel({
		ellipsize: Titanium.UI.TEXT_ELLIPSIZE_TRUNCATE_END,
		height: '100%',
		left: 14,
		text: String.format(L('x1959850688_traducir', 'a %1$s km'), distancia.toString()),
		color: '#a0a1a3',
		wordWrap: false,
		touchEnabled: false,
		right: 0,
		font: {
			fontFamily: 'Roboto-Light',
			fontSize: '12dp'
		}

	});
	vista118.add(Adistancia);

	vista117.add(vista118);
	var vista119 = Titanium.UI.createView({
		height: '20dp',
		layout: 'composite',
		width: '35%',
		top: '2dp'
	});
	var iconoComuna12 = Titanium.UI.createLabel({
		height: Ti.UI.SIZE,
		text: '\uE811',
		left: '0dp',
		color: '#F8DA54',
		width: Ti.UI.SIZE,
		font: {
			fontFamily: 'beta1',
			fontSize: '9dp'
		}

	});
	vista119.add(iconoComuna12);
	var Comuna = Titanium.UI.createLabel({
		ellipsize: Titanium.UI.TEXT_ELLIPSIZE_TRUNCATE_END,
		height: '100%',
		left: 14,
		text: comuna,
		color: '#a0a1a3',
		wordWrap: false,
		touchEnabled: false,
		right: 0,
		font: {
			fontFamily: 'Roboto-Light',
			fontSize: '12dp'
		}

	});
	vista119.add(Comuna);

	vista117.add(vista119);
	var vista120 = Titanium.UI.createView({
		height: '20dp',
		layout: 'composite',
		width: '38%',
		top: '2dp'
	});
	var iconoCiudad12 = Titanium.UI.createLabel({
		height: Ti.UI.SIZE,
		text: '\uE810',
		left: '0dp',
		color: '#8CE5BD',
		width: Ti.UI.SIZE,
		font: {
			fontFamily: 'beta1',
			fontSize: '9dp'
		}

	});
	vista120.add(iconoCiudad12);
	var Ciudad = Titanium.UI.createLabel({
		ellipsize: Titanium.UI.TEXT_ELLIPSIZE_TRUNCATE_END,
		height: '100%',
		left: 14,
		text: ciudad,
		color: '#a0a1a3',
		wordWrap: false,
		touchEnabled: false,
		right: 0,
		font: {
			fontFamily: 'Roboto-Light',
			fontSize: '12dp'
		}

	});
	vista120.add(Ciudad);

	vista117.add(vista120);
	vista115.add(vista117);
	vista113.add(vista115);
	var vista121 = Titanium.UI.createView({
		height: '100%',
		layout: 'vertical',
		width: '10%'
	});
	var iconoEntrar12 = Titanium.UI.createLabel({
		height: Ti.UI.SIZE,
		text: '\uE81b',
		color: '#CCCCCC',
		width: Ti.UI.SIZE,
		top: '35%',
		right: '5dp',
		font: {
			fontFamily: 'beta1',
			fontSize: '14dp'
		}

	});
	vista121.add(iconoEntrar12);
	vista113.add(vista121);
	var icono13_icono = tipotarea;

	if ('fontello' in require('a4w') && 'adjust' in require('a4w').fontello && icono13_icono in require('a4w').fontello['adjust'].CODES) {
		icono13_icono = require('a4w').fontello['adjust'].CODES[icono13_icono];
	} else {
		console.log('live/modificar -> error setting new svg icon alias');
	}
	icono13.setText(icono13_icono);

	if (tipotarea == 1 || tipotarea == '1') {
		icono13.setColor('#2d9edb');

		vista114.setBackgroundColor('#2d9edb');
	} else if (tipotarea == 2) {
		icono13.setColor('#ee7f7e');

		vista114.setBackgroundColor('#ee7f7e');
	} else if (tipotarea == 3) {
		icono13.setColor('#b9aaf3');

		vista114.setBackgroundColor('#8383db');
	} else if (tipotarea == 4) {
		icono13.setColor('#fcbd83');

		vista114.setBackgroundColor('#fcbd83');
	} else if (tipotarea == 5) {
		icono13.setColor('#8ce5bd');

		vista114.setBackgroundColor('#8ce5bd');
	} else if (tipotarea == 6) {
		icono13.setColor('#f8da54');

		vista114.setBackgroundColor('#f8da54');
	} else if (tipotarea == 7) {
		icono13.setColor('#8383db');

		vista114.setBackgroundColor('#b9aaf3');
	} else if (tipotarea == 8) {
		icono13.setColor('#ffacaa');

		vista114.setBackgroundColor('#ffacaa');
	} else if (tipotarea == 9) {
		icono13.setColor('#8bc9e8');

		vista114.setBackgroundColor('#8bc9e8');
	} else if (tipotarea == 10) {
		icono13.setColor('#a5876d');

		vista114.setBackgroundColor('#a5876d');
	} else if (tipotarea == L('x3313283897_traducir', 'critico')) {
		icono13.setColor('#ee7f7e');

		vista114.setBackgroundColor('#ee7f7e');
	} else {
		icono13.setColor('#ee7f7e');

		Direccion.setLeft('0');

	}
	var fila;
	fila = filaTabla2;
	fila._data = data;
	vista112.add(vista113);
	var vista122 = Titanium.UI.createView({
		height: '35dp',
		visible: seguir,
		layout: 'vertical',
		width: '28dp',
		top: '9dp',
		right: '24dp'
	});
	if (OS_IOS) {
		var imagen4 = Ti.UI.createImageView({
			height: Ti.UI.FILL,
			duration: 50,
			images: ['/images/i4ECB85A2AE41ACB15653139F35BFA8C7.png', '/images/i4DB2E8AD733DFC95AF5E6A5115E117EE.png', '/images/iD4FE3BB66A3A734672CFBF9A4FE820B3.png', '/images/iD95E1371C4A9D4060E4C293FEBB2A371.png', '/images/i1846D1705B39A0DFD7CA331E2A624A7B.png', '/images/i1AAB9DCC791F6C8D5CBD86A69F2BD28A.png', '/images/iC18AD8B25C7E89544B26A56D72763ACF.png', '/images/i451D26CB7C617839FAAABBEE243EE91B.png', '/images/iBF02E0A9F57D34BA382B6B4851AEB549.png', '/images/iB810656D7547F7A2C967AC3DB0C7CAD4.png', '/images/iF173895CDE559CF4A789ECA310F324C1.png', '/images/iB13BBB34910D6E346D89A425DF9D9AA7.png', '/images/i09E757E6A96CDFD1A3071F75A86B9703.png', '/images/iB1B0726192E9AFA5B4393DD2789A8E73.png', '/images/i3BD062E17CB084738EC3C8544C4F562C.png', '/images/i0B25A07F8BA6A3D1155E5635855725E3.png', '/images/i51CD06F5D53967A813F0F7C4F8679F81.png', '/images/i57F0B2CBFC207F6540A08C8209B6461C.png', '/images/i6F4FBDF546B1311877A208695B53B1F5.png', '/images/i20546752F9D3EB6A38FD547BB45E2DA7.png', '/images/i997DFD2C59EEEC93E94CA8831CCFE464.png', '/images/i7A0053CD88CD6D72A30F33A67115B84F.png', '/images/i53F37CB31AA110AB8BF1BCA095F905FE.png', '/images/i6F4B0EAC06F1D5F21CF0ADB7E3036782.png', '/images/iD8AE92E691560F489DE642319547F8F9.png', '/images/iF8FD2CB3805AB4250C4E53D99093CA3C.png', '/images/iCA260B7119401A0D456373D95DAD7824.png', '/images/iC97BF367BA6481E3A5CEAEBFBD492506.png', '/images/iC9B3A576F9A65C4C9C208CFB562D3A8D.png', '/images/iCEF1D427EF5C147BC79A0D6848913C44.png'],
			id: imagen4,
			repeatCount: 0
		});
	} else {
		var imagen4 = Ti.UI.createImageView({
			height: Ti.UI.FILL,
			duration: 50,
			images: ['/images/i4ECB85A2AE41ACB15653139F35BFA8C7.png', '/images/i4DB2E8AD733DFC95AF5E6A5115E117EE.png', '/images/iD4FE3BB66A3A734672CFBF9A4FE820B3.png', '/images/iD95E1371C4A9D4060E4C293FEBB2A371.png', '/images/i1846D1705B39A0DFD7CA331E2A624A7B.png', '/images/i1AAB9DCC791F6C8D5CBD86A69F2BD28A.png', '/images/iC18AD8B25C7E89544B26A56D72763ACF.png', '/images/i451D26CB7C617839FAAABBEE243EE91B.png', '/images/iBF02E0A9F57D34BA382B6B4851AEB549.png', '/images/iB810656D7547F7A2C967AC3DB0C7CAD4.png', '/images/iF173895CDE559CF4A789ECA310F324C1.png', '/images/iB13BBB34910D6E346D89A425DF9D9AA7.png', '/images/i09E757E6A96CDFD1A3071F75A86B9703.png', '/images/iB1B0726192E9AFA5B4393DD2789A8E73.png', '/images/i3BD062E17CB084738EC3C8544C4F562C.png', '/images/i0B25A07F8BA6A3D1155E5635855725E3.png', '/images/i51CD06F5D53967A813F0F7C4F8679F81.png', '/images/i57F0B2CBFC207F6540A08C8209B6461C.png', '/images/i6F4FBDF546B1311877A208695B53B1F5.png', '/images/i20546752F9D3EB6A38FD547BB45E2DA7.png', '/images/i997DFD2C59EEEC93E94CA8831CCFE464.png', '/images/i7A0053CD88CD6D72A30F33A67115B84F.png', '/images/i53F37CB31AA110AB8BF1BCA095F905FE.png', '/images/i6F4B0EAC06F1D5F21CF0ADB7E3036782.png', '/images/iD8AE92E691560F489DE642319547F8F9.png', '/images/iF8FD2CB3805AB4250C4E53D99093CA3C.png', '/images/iCA260B7119401A0D456373D95DAD7824.png', '/images/iC97BF367BA6481E3A5CEAEBFBD492506.png', '/images/iC9B3A576F9A65C4C9C208CFB562D3A8D.png', '/images/iCEF1D427EF5C147BC79A0D6848913C44.png'],
			id: imagen4,
			repeatCount: 0
		});
	}
	require('vars')['_imagen4_original_'] = imagen4.getImage();
	require('vars')['_imagen4_filtro_'] = 'original';

	imagen4.addEventListener('load', function(e) {
		e.cancelBubble = true;
		var elemento = e.source;
		var evento = e;
		elemento.start();
	});
	vista122.add(imagen4);
	vista112.add(vista122);
	filaTabla2.add(vista112);
	sectorTabla.add(filaTabla2);
	return sectorTabla;
};

module.exports = plantillas;