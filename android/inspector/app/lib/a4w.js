exports.events = _.clone(Backbone.Events);
exports.ajaxkeys = {};
exports.variables = {};
exports.styles = {
	"images": {
		"id_164841395492": "/images/i7F73828276786D80F13715BB5BFB4CCC.png",
		"id_1245138369": "/images/iBD1E17BE427423F6FA053D07C27B494E.png",
		"id_164841395410": "/images/iE9355204576B1FD1BB3F1F0E435BFF6D.png",
		"id_164841395471": "/images/iDFEBE4EC019BCE94F6EC5853A3BFABF5.png",
		"id_164841395442": "/images/iE50DF32C64581FCE5856C9454B2D58E2.png",
		"id_12451383656": "/images/iC9AFD1E7FA9C0486C5424E816865BE8C.png",
		"androidbadge89": "/images/badge89.png",
		"id_164841395419": "/images/i988F7B1016AF1A4927CD7ED9AE8AB990.png",
		"id_164841395430": "/images/i9AC6E8C97F63FEB0A46CCD655AC44431.png",
		"id_17304885111": "/images/i07DBC5BDF16999EDD7FF32220DED028B.png",
		"id_164841395454": "/images/i20769EDB2A387AD7E63BFEE1ADB6E01A.png",
		"androidbadge37": "/images/badge37.png",
		"id_12451383619": "/images/i7F617027CD4EC4189C4E8CED725823AC.png",
		"id_806455898": "/images/iFF8B149DE516F182A0E44287CF224884.png",
		"androidbadge86": "/images/badge86.png",
		"id_38920584923": "/images/i53F37CB31AA110AB8BF1BCA095F905FE.png",
		"id_1227047425": "/images/iFF8B149DE516F182A0E44287CF224884.png",
		"id_1648413954115": "/images/i6AF96FB60F20F611E16F44E4A4C366B4.png",
		"id_1730488511": "/images/i8BA7BBB2B899B993343E33AEE2947F03.png",
		"androidbadge47": "/images/badge47.png",
		"id_164841395433": "/images/i388C5A5B6FEC1AAA9E0FED2BE6842A35.png",
		"id_172501864828": "/images/iC97BF367BA6481E3A5CEAEBFBD492506.png",
		"id_12451383639": "/images/i9CFC507846CDD4330DF7FDF1C0B0C31E.png",
		"id_3892058491": "/images/i4ECB85A2AE41ACB15653139F35BFA8C7.png",
		"mono_4": "/images/i8829E9B8FD973AE7CF14F5DF5B239967.png",
		"id_1648413954102": "/images/i47EFAC4DEC72BFED1EAD8D5621ECF913.png",
		"androidbadge12": "/images/badge12.png",
		"androidbadge76": "/images/badge76.png",
		"id_17304885161": "/images/iFD70798B325B49DF04F098F085C08364.png",
		"androidbadge44": "/images/badge44.png",
		"id_1648413954113": "/images/i7DF846739C1FD71098DE3902990DA8A9.png",
		"ubicacion_off": "/images/i4B9CE42B080904AF35CE20992C4B4C48.png",
		"id_164841395472": "/images/iAC27C6E0D5FAF36E89B0E75BD07AEB87.png",
		"ruta_on": "/images/iF412360E89F0E032391066B111FF2882.png",
		"id_16484139547": "/images/i77B96376A21E9E530BF99DEF0E191F24.png",
		"id_12451383615": "/images/iDD8E3DB6271506344EFC962A6F6C907E.png",
		"id_164841395414": "/images/i19F89DC20905F566DA7CCDBE900B1828.png",
		"id_1245138361": "/images/iB1F901AEFA79EE1F3A05C41650B368A4.png",
		"id_17304885159": "/images/i237B9312627FB83A97AD3AECE3B26610.png",
		"id_17304885116": "/images/i252ECAA10D65331F13AD2A83FD659115.png",
		"id_17304885127": "/images/i5C81EB6AB36733A02A5E68514D23976A.png",
		"id_12451383624": "/images/iFC47B0B749489B593F6282CEBE075C92.png",
		"id_16484139542": "/images/iEB558FE382408A66B62F79BCD08E87DE.png",
		"androidbadge97": "/images/badge97.png",
		"id_17304885162": "/images/iAF74778DC2264D8D603BB03FEB9BAE6A.png",
		"id_17304885148": "/images/iBF1B0916259A898898D73A289B45799B.png",
		"id_17304885156": "/images/i0D1C15FCA2F7073146F802BFB0B912B3.png",
		"androidbadge49": "/images/badge49.png",
		"id_17304885143": "/images/i3C81A2834695F313E5948B0BE6FB79D2.png",
		"androidbadge48": "/images/badge48.png",
		"id_12451383650": "/images/i9C794F568EA9A9383A579EBC118ECE1C.png",
		"androidbadge5": "/images/badge5.png",
		"id_172501864823": "/images/i53F37CB31AA110AB8BF1BCA095F905FE.png",
		"id_17304885139": "/images/i69C37C68EB212086D1F0C231FFAB90E9.png",
		"id_1245138364": "/images/i058F2A04E46A9135BF2007D00D84E610.png",
		"androidbadge67": "/images/badge67.png",
		"androidbadge43": "/images/badge43.png",
		"id_164841395478": "/images/i35A05BBCDD161E2A6C0C478F703901C0.png",
		"id_164841395436": "/images/i359C4AC08A6FB6D5912E5ECA8ABBB00A.png",
		"androidbadge31": "/images/badge31.png",
		"id_1730488517": "/images/iAFB1024EC559791C8AD5D79928EA088C.png",
		"id_38920584911": "/images/iF173895CDE559CF4A789ECA310F324C1.png",
		"androidbadge56": "/images/badge56.png",
		"id_164841395444": "/images/i89096AB8DBE818E384384C292C89811F.png",
		"id_17304885151": "/images/i8BA0FB795C77A5EA886096DF052FBF69.png",
		"androidbadge77": "/images/badge77.png",
		"id_1648413954103": "/images/i96D645D648D27F12B2E4F54E9503E8DF.png",
		"id_12451383630": "/images/i77B67FB15DAF2AA6C7DFAB3AEF96CC3D.png",
		"id_164841395467": "/images/iF3F5D57B22CF688DF19FE14CCD909402.png",
		"id_164841395468": "/images/i399EE8FDF222FDA603DF0C82E0BCCFAC.png",
		"id_164841395423": "/images/i42D15D48B2ED837F7903FE534A06494B.png",
		"id_17304885120": "/images/i2195901CE0F8A3F4362453AB02240F9F.png",
		"androidbadge35": "/images/badge35.png",
		"id_12451383636": "/images/iE8162669F36B070D6B7D99F53757E770.png",
		"id_38920584922": "/images/i7A0053CD88CD6D72A30F33A67115B84F.png",
		"id_164841395440": "/images/i585BBC1586441528C908AD41EB8EBDBD.png",
		"androidbadge21": "/images/badge21.png",
		"id_16484139549": "/images/i88E908455A225B9390E3678E9C19FBA2.png",
		"androidbadge64": "/images/badge64.png",
		"id_164841395415": "/images/i6755383116D1A5AD862D47B6D5554641.png",
		"androidbadge80": "/images/badge80.png",
		"androidbadge70": "/images/badge70.png",
		"id_164841395435": "/images/i10063715BD30D221415EF53335926168.png",
		"id_164841395488": "/images/iDAA2A0A70B3960D02C55DDF038216353.png",
		"id_17250186483": "/images/iD4FE3BB66A3A734672CFBF9A4FE820B3.png",
		"mono_5": "/images/i943F418A32B37EDCE50B3CB50D3CEBE8.png",
		"androidbadge94": "/images/badge94.png",
		"androidbadge42": "/images/badge42.png",
		"androidbadge51": "/images/badge51.png",
		"id_38920584918": "/images/i57F0B2CBFC207F6540A08C8209B6461C.png",
		"id_17304885165": "/images/i82E01BB1890D850A04D145E675940E7F.png",
		"id_164841395426": "/images/i077D23D9EA5866B0EF5E25614919665F.png",
		"id_17304885153": "/images/i96E6AEC1984C01E35E87A3D4D8529C92.png",
		"refresh": "/images/i3B4F386A89D1E313409443B38B63656D.png",
		"id_1245138368": "/images/i553359F66995CFAE34B27D5D0D4C95E0.png",
		"id_3892058498": "/images/i451D26CB7C617839FAAABBEE243EE91B.png",
		"androidbadge82": "/images/badge82.png",
		"id_1648413954111": "/images/i661E5F5296ABB7AB4B0E5D236E6C7630.png",
		"id_12451383645": "/images/i00F9DCB834689DD92A6F5679DF478CEC.png",
		"id_164841395421": "/images/i46769D88018418464C9F4B24D384CB8B.png",
		"androidbadge32": "/images/badge32.png",
		"androidbadge20": "/images/badge20.png",
		"androidbadge93": "/images/badge93.png",
		"id_1648413954116": "/images/iBBA0A82C21EA239F79E171AC51EA7B95.png",
		"id_1648413954119": "/images/iAE28EA1830BF41F55AD84813E8F1D056.png",
		"id_164841395449": "/images/iF714D356682197276705B90C40BA8A03.png",
		"id_17304885121": "/images/i68D6F827E1F992CD125964497C0F6E55.png",
		"androidbadge54": "/images/badge54.png",
		"id_3892058493": "/images/iD4FE3BB66A3A734672CFBF9A4FE820B3.png",
		"id_164841395499": "/images/i26C8BDFC1DEA0C67358C293A353C327B.png",
		"id_172501864819": "/images/i6F4FBDF546B1311877A208695B53B1F5.png",
		"pincho9": "/images/iA76649B47890BD38C12BC0A7922891CE.png",
		"id_38920584910": "/images/iB810656D7547F7A2C967AC3DB0C7CAD4.png",
		"id_1648413954105": "/images/i6CD686DC5A11FB94CBB2A09FC90DB6EF.png",
		"id_164841395495": "/images/i5A8E6E8D34ABB47533165D506B7E1462.png",
		"id_1648413954112": "/images/iCCB062F45E0D9D917C617FDB2EA1E357.png",
		"id_172501864821": "/images/i997DFD2C59EEEC93E94CA8831CCFE464.png",
		"id_12451383611": "/images/i889E7951CC79C107C7467F773D851F53.png",
		"id_164841395448": "/images/iBEB7E7D434C1667889663859EAE75D92.png",
		"id_164841395424": "/images/iBC7C1AA388608962A1F7B8C61BD6DA82.png",
		"androidbadge1": "/images/badge1.png",
		"mono_3": "/images/i8B282601EF9B7B696CDAC05451F12BC3.png",
		"id_1648413954114": "/images/iAD45960BC2841EC6E2D86283A2F33E96.png",
		"id_164841395457": "/images/i0DE060270629150E8A87173EDB8B79F7.png",
		"id_164841395417": "/images/i40A27D206CA23815C331AB71422629F2.png",
		"id_3892058496": "/images/i1AAB9DCC791F6C8D5CBD86A69F2BD28A.png",
		"androidbadge69": "/images/badge69.png",
		"id_164841395484": "/images/i7FDF4568F230AA5A4D3B701708D1E8D6.png",
		"id_1400926341": "/images/iFF8B149DE516F182A0E44287CF224884.png",
		"androidbadge14": "/images/badge14.png",
		"id_12451383646": "/images/i3310017A13BD47BA4E4549FD12A7788A.png",
		"androidbadge13": "/images/badge13.png",
		"id_12451383655": "/images/iFAD70BC931105FCA07FD8E0DAEAF4137.png",
		"androidbadge53": "/images/badge53.png",
		"id_17304885164": "/images/iFE5E804CABCEBF34AD79DF5E7E40CA2C.png",
		"androidbadge68": "/images/badge68.png",
		"androidbadge46": "/images/badge46.png",
		"id_12451383653": "/images/i129B9502415F1FFEF9E8936317CEFF50.png",
		"id_164841395475": "/images/i7E06C884017083155A6857A40DCF4DE3.png",
		"pincho8": "/images/i801CAD94710E3C73011A5EF0C6AF55E3.png",
		"id_17304885130": "/images/iA8164ACEC2C69CE2236E129E444F888A.png",
		"id_1648413954106": "/images/iEBD4EE843BEEB6E225C25387B7DCF30E.png",
		"id_164841395431": "/images/iE28ABABF9CABE7255711922BBF55B983.png",
		"id_17304885160": "/images/i90C0FC838365FD82F6AEEB884482F922.png",
		"androidbadge26": "/images/badge26.png",
		"id_12451383620": "/images/i0C1216BE3263895DD5C0DCCE921413F2.png",
		"id_172501864824": "/images/i6F4B0EAC06F1D5F21CF0ADB7E3036782.png",
		"id_12451383658": "/images/i487CD84F81C016C6569519B588DF619F.png",
		"id_16484139548": "/images/i1A8982F638B1931C2A37ED5E826C17C0.png",
		"distancia_on": "/images/i8C9E49EA4E040653B5840FC8302C77DE.png",
		"id_12451383649": "/images/iD68DDDF47F8B5A8C859DEE487442816E.png",
		"androidbadge84": "/images/badge84.png",
		"mono_7": "/images/iBB0FE490ADE01E0C5F7F04E1FEFC4E70.png",
		"id_164841395439": "/images/i9AE08E1C51AF160F89D84F868261FCFD.png",
		"androidbadge65": "/images/badge65.png",
		"androidbadge22": "/images/badge22.png",
		"id_17304885142": "/images/iF6D625C734C76E37B9240DE8C8E787FC.png",
		"androidbadge15": "/images/badge15.png",
		"id_172501864817": "/images/i51CD06F5D53967A813F0F7C4F8679F81.png",
		"androidbadge24": "/images/badge24.png",
		"id_164841395482": "/images/i8A0B707451CCBE122A5B28F640716AF1.png",
		"id_1361874287": "/images/iC8D04E2813709AD9C2488144189CDC65.png",
		"id_12451383622": "/images/iD51F4417351E1B84F7F9B808956CD544.png",
		"id_1245138362": "/images/iBD0F891E767949293A5E319C70468E61.png",
		"id_17304885136": "/images/i0FE7FE00BA11FC42B3C0AFD490C4CFA6.png",
		"id_3892058494": "/images/iD95E1371C4A9D4060E4C293FEBB2A371.png",
		"id_172501864822": "/images/i7A0053CD88CD6D72A30F33A67115B84F.png",
		"id_12451383631": "/images/i1352BFB08B32246214800F652EA481A9.png",
		"id_1730488515": "/images/i38C61C490C2D132A441BA4DA831A0B6B.png",
		"id_164841395447": "/images/i304ED678B67AD49378DB6228C609BE45.png",
		"id_38920584914": "/images/iB1B0726192E9AFA5B4393DD2789A8E73.png",
		"id_17304885119": "/images/iD963A16A7C9E33D13B943B513E0C6C61.png",
		"androidbadge73": "/images/badge73.png",
		"pincho6": "/images/iBFE4DC825CD3D239883F7A858B1B14D8.png",
		"androidbadge40": "/images/badge40.png",
		"id_164841395464": "/images/iAC8C1B1E912288D6EF52DC4C71B79944.png",
		"id_12451383628": "/images/i28054D3A888BD526A6B71FA0D72B495E.png",
		"id_38920584915": "/images/i3BD062E17CB084738EC3C8544C4F562C.png",
		"id_12451383627": "/images/i64703C2596335309216F0F02CBE47A0A.png",
		"id_12451383634": "/images/iB9E2226298DF133710B12402166BE94F.png",
		"id_12451383643": "/images/i8F4CE4FFC4978421CD759A09A64FD5DF.png",
		"androidbadge7": "/images/badge7.png",
		"androidbadge11": "/images/badge11.png",
		"id_164841395485": "/images/i69B8A04A79F91820581E4D3708B7D003.png",
		"id_164841395461": "/images/iB6A8B660E26087802754C713C196B739.png",
		"id_3892058492": "/images/i4DB2E8AD733DFC95AF5E6A5115E117EE.png",
		"id_164841395432": "/images/i0A41ED8B1E443429769FFB49137003C2.png",
		"id_164841395422": "/images/i3F6BAF33AC1E4011205917185EA98CAD.png",
		"androidbadge90": "/images/badge90.png",
		"id_164841395420": "/images/i405EC5CB58F536363712327BE6376BB1.png",
		"id_17304885135": "/images/i8830B078DE88248713D2F7C7DA553651.png",
		"id_172501864813": "/images/i09E757E6A96CDFD1A3071F75A86B9703.png",
		"logo": "/images/i81A079DFF4849D215FA8F346A2CFA375.png",
		"id_164841395469": "/images/i09C63C089B2E569672618ED5FC69504C.png",
		"id_16484139545": "/images/iC844F1FD37D49F099C8E19CDD3168A41.png",
		"historial_on": "/images/iEF1EA8FCAABA9EE2542F1B1BC93BF04C.png",
		"id_164841395496": "/images/i9AD38C8643DBCC036D0D226508F2B971.png",
		"id_17304885117": "/images/i3F8954A6250AC3AD770A65B1E182A34D.png",
		"id_1648413954107": "/images/iB0DCEF19023ADDE9B38C97AF31540D4D.png",
		"id_164841395455": "/images/iDAED2AF86C310E481C7409143758ECB6.png",
		"distancia_off": "/images/i8E93941EE90F038F87F33474BD829221.png",
		"id_164841395427": "/images/iA83DD447D387E7698AB46331B8A8D019.png",
		"androidbadge16": "/images/badge16.png",
		"id_12451383632": "/images/iDE34B298EE1EF399563C371BB1352EE0.png",
		"tiempo_on": "/images/iAFCE7B65D06E25A33905AA5DEED70BD0.png",
		"androidbadge79": "/images/badge79.png",
		"androidbadge30": "/images/badge30.png",
		"androidbadge96": "/images/badge96.png",
		"id_17304885146": "/images/iDAFCE8BEB03EBA6E5AE151A6D8498A43.png",
		"id_3892058495": "/images/i1846D1705B39A0DFD7CA331E2A624A7B.png",
		"androidbadge66": "/images/badge66.png",
		"id_38920584920": "/images/i20546752F9D3EB6A38FD547BB45E2DA7.png",
		"id_16484139543": "/images/iE4617B7080958F45BF2C192CC042382D.png",
		"id_1648413954109": "/images/i8143DE4F0A77BC1F4111FF6A02EED95E.png",
		"id_17304885170": "/images/i501CFFB92F510A66A56518AC101744C1.png",
		"id_17304885132": "/images/i11FFB0492F29D10E6FCE27A9D4057829.png",
		"androidbadge57": "/images/badge57.png",
		"id_17304885172": "/images/i54D9E2FB35E8CF50832C582618BF207C.png",
		"id_172501864812": "/images/iB13BBB34910D6E346D89A425DF9D9AA7.png",
		"androidbadge78": "/images/badge78.png",
		"pincho4": "/images/i07BD9F7C5EDAEC7FD998A077330516E5.png",
		"id_164841395490": "/images/i10CCB6F0BFED76841F1607FA34C062B1.png",
		"id_1754166454": "/images/iFF8B149DE516F182A0E44287CF224884.png",
		"id_164841395480": "/images/iE34B2EECFDC74FC20A5DEA25E204DB7A.png",
		"id_164841395450": "/images/iA1B6778946A3A0358E419143E8081D2B.png",
		"id_1792194859": "/images/iFF8B149DE516F182A0E44287CF224884.png",
		"id_1730488516": "/images/i6FB9FA73DDDE12DF8E86FFCDA892EC82.png",
		"id_164841395479": "/images/iCF6F841564C2B00839FA0338556EE25E.png",
		"id_38920584913": "/images/i09E757E6A96CDFD1A3071F75A86B9703.png",
		"androidbadge52": "/images/badge52.png",
		"androidbadge100": "/images/badge100.png",
		"id_12451383625": "/images/iC37FBC50BE131D22685223C641DA5C9B.png",
		"androidbadge19": "/images/badge19.png",
		"id_17250186488": "/images/i451D26CB7C617839FAAABBEE243EE91B.png",
		"androidbadge81": "/images/badge81.png",
		"id_12451383621": "/images/i52CB6FB175FB303E19F6C3CEA9C8D640.png",
		"id_38920584930": "/images/iCEF1D427EF5C147BC79A0D6848913C44.png",
		"androidbadge2": "/images/badge2.png",
		"androidbadge95": "/images/badge95.png",
		"id_17304885114": "/images/i874A37F48CC2C1995FC52C21369DDEB6.png",
		"androidbadge61": "/images/badge61.png",
		"androidbadge8": "/images/badge8.png",
		"id_17304885144": "/images/iAAA43281AA3CC03AB9B0FE147901460E.png",
		"id_17304885149": "/images/i874015A473ED5D0E79223B1DD2AF0A8D.png",
		"androidbadge62": "/images/badge62.png",
		"androidbadge92": "/images/badge92.png",
		"id_12451383648": "/images/iF70C2EE509A92FCB43AF685DDD2B2FA2.png",
		"id_38920584924": "/images/i6F4B0EAC06F1D5F21CF0ADB7E3036782.png",
		"id_17304885150": "/images/i272088853E72E862CA0E46BC9C1CB573.png",
		"androidbadge36": "/images/badge36.png",
		"id_12451383657": "/images/i2A3F902C97F8E816FBF43D6072C8EA38.png",
		"pincho10": "/images/i57157C0EED282EF313085642E8C9B543.png",
		"id_12451383654": "/images/i54B57D779036FF5C5902278D3A9A329A.png",
		"androidbadge87": "/images/badge87.png",
		"id_1648413954110": "/images/iDD2C93752E1169F8D71892A477C85A11.png",
		"androidbadge58": "/images/badge58.png",
		"androidbadge17": "/images/badge17.png",
		"id_17304885140": "/images/iEFF38CD8533035A66E0AF660B4E241AA.png",
		"id_17304885122": "/images/i30CD6C73987FC566A08353B551A359CC.png",
		"id_1648413954118": "/images/iE02AFE3817F8B969AEAACF7456E25AB5.png",
		"id_1648413954120": "/images/i870DC5E31CA825DAEE1AD00C6FB1D68A.png",
		"id_17304885138": "/images/iE8B07C574058E6BAA503A39BA330CE81.png",
		"id_12451383617": "/images/i29429A8CA3B30681CA2BB487FE2DDF26.png",
		"id_598255384": "/images/iFF8B149DE516F182A0E44287CF224884.png",
		"id_12451383644": "/images/i10280424F1EE424A6A514F0F16858345.png",
		"id_172501864818": "/images/i57F0B2CBFC207F6540A08C8209B6461C.png",
		"historial_off": "/images/iD3BAA8F5CE36949957FABB32FE5E7661.png",
		"id_1648413954100": "/images/iF086F6FE7A1868188FD09EE6FA03485B.png",
		"androidbadge91": "/images/badge91.png",
		"id_164841395411": "/images/i0E4650BFDDB114F81A51C600521AC2D7.png",
		"id_12451383635": "/images/iE89BCB17377B1DC1F77A2DE2512A28EC.png",
		"id_164841395441": "/images/i10BF85AEDE9F2E6087E8EECF917665A8.png",
		"androidbadge34": "/images/badge34.png",
		"id_164841395489": "/images/iECBF5EDE5417D57D71BD067290909C22.png",
		"id_12451383659": "/images/iC217A8FE8F59CC255970BBB562470CC2.png",
		"id_12451383651": "/images/i3D15430021DB8CCF5FC81650384162EA.png",
		"androidbadge88": "/images/badge88.png",
		"id_1648413954104": "/images/i6394513CA228CCB57ED602297C9422BC.png",
		"id_38920584926": "/images/iF8FD2CB3805AB4250C4E53D99093CA3C.png",
		"androidbadge98": "/images/badge98.png",
		"id_17304885152": "/images/i00DF7F2C7433E465EE49D8415C103CB0.png",
		"id_17304885141": "/images/i1B99D77A5103B23820B567FEC7972C68.png",
		"id_17304885169": "/images/iD12F88BE292586CBCD824A627FD1E84A.png",
		"androidbadge3": "/images/badge3.png",
		"id_3892058499": "/images/iBF02E0A9F57D34BA382B6B4851AEB549.png",
		"androidbadge75": "/images/badge75.png",
		"id_38920584928": "/images/iC97BF367BA6481E3A5CEAEBFBD492506.png",
		"mono_1": "/images/i20BA389E31A78BB6716B5CEE9C527BDA.png",
		"androidbadge99": "/images/badge99.png",
		"id_12451383626": "/images/i10257EAFE36B26C91213946BD6B916BE.png",
		"camara": "/images/iA05BE9D124AAD014095618F4F663E736.png",
		"id_12451383640": "/images/iE9825E0C72E7F6D3C9F3E8C359F44947.png",
		"androidbadge10": "/images/badge10.png",
		"id_164841395438": "/images/i54701C93E11CA7BAF12DF89AABF2AE04.png",
		"id_164841395497": "/images/iA7B0C6D62FD940727B9D5DBBD4165324.png",
		"id_172501864811": "/images/iF173895CDE559CF4A789ECA310F324C1.png",
		"id_17304885155": "/images/iB6C20BCB226B5B33CBDEC4DC14BFE599.png",
		"id_164841395445": "/images/i0BEBD7212FCEADD8E81C2812FD68C36D.png",
		"androidbadge29": "/images/badge29.png",
		"id_17304885129": "/images/i17E5146D6B946B06A625338800602C01.png",
		"pincho2": "/images/iA3D34032D37972198904AD753EA1DE26.png",
		"id_1737146733": "/images/iFF8B149DE516F182A0E44287CF224884.png",
		"id_164841395481": "/images/i15B71869941D9D056D2771715265227F.png",
		"id_164841395434": "/images/iBB18FBADFC2D527AD2709E8652AAFD5A.png",
		"id_172501864830": "/images/iCEF1D427EF5C147BC79A0D6848913C44.png",
		"androidbadge59": "/images/badge59.png",
		"id_164841395476": "/images/i691DBFC540A724998A2353C7D6DC199F.png",
		"id_3892058497": "/images/iC18AD8B25C7E89544B26A56D72763ACF.png",
		"id_17304885158": "/images/i6AC13900B0D450E5EB03B51FDAE9345E.png",
		"id_172501864815": "/images/i3BD062E17CB084738EC3C8544C4F562C.png",
		"id_164841395465": "/images/i396F93C0F37490C405609068F53808DE.png",
		"pincho5": "/images/iE3B5F64AE888BA3A54B0BC278814961A.png",
		"id_1648413954117": "/images/i2F569FBCA8D42EB735DED2831286DC66.png",
		"androidbadge74": "/images/badge74.png",
		"id_17304885125": "/images/iFB0ED3833FB97A5B95B692F9C8471E31.png",
		"pincho_normal": "/images/iA4447614C019AF8FFC6E718892E39183.png",
		"androidbadge6": "/images/badge6.png",
		"id_17304885166": "/images/iF4D52A4E116FB61ABEFF82D8A30CA9C9.png",
		"id_1245138363": "/images/iE18490E3CB42EE21FA2CA39791E6EE07.png",
		"id_164841395470": "/images/i6EA1D3660FE93F229A4DD0E034A728EB.png",
		"id_38920584925": "/images/iD8AE92E691560F489DE642319547F8F9.png",
		"id_38920584916": "/images/i0B25A07F8BA6A3D1155E5635855725E3.png",
		"androidbadge18": "/images/badge18.png",
		"id_164841395459": "/images/iA152F49A26EC92286520C265AABD8CFC.png",
		"mono_6": "/images/iF3FD4EEFB46CA0924F89BD43F0CEC7A4.png",
		"androidbadge60": "/images/badge60.png",
		"tip_extra": "/images/i943F418A32B37EDCE50B3CB50D3CEBE8.png",
		"id_38920584919": "/images/i6F4FBDF546B1311877A208695B53B1F5.png",
		"id_164841395429": "/images/i9BDA9EE2558DFE3F2125992BECA4278D.png",
		"id_172501864825": "/images/iD8AE92E691560F489DE642319547F8F9.png",
		"id_164841395498": "/images/i51313033EE92F99494C0CEF93F77F8D9.png",
		"id_16484139546": "/images/iD7DACAF52A6FE787F42E337F896ED7E5.png",
		"id_12451383616": "/images/i4BE78669A95AFE52AA53FBDD4361D3E4.png",
		"id_17250186486": "/images/i1AAB9DCC791F6C8D5CBD86A69F2BD28A.png",
		"androidbadge72": "/images/badge72.png",
		"id_17304885112": "/images/i9DE8C660239457D57CAF6FC50718367D.png",
		"id_17304885147": "/images/i64E25421653D939AB6C5F203BF125DA1.png",
		"id_17304885126": "/images/i908F7822EB301E7C21DA3300EA7E1E04.png",
		"androidbadge55": "/images/badge55.png",
		"id_17304885118": "/images/i19F42D46458DA832B6C5EA605D0FE30F.png",
		"id_164841395494": "/images/i18ACDDC89C81835BFB74D3A0E71D8C10.png",
		"androidbadge39": "/images/badge39.png",
		"androidbadge28": "/images/badge28.png",
		"id_172501864826": "/images/iF8FD2CB3805AB4250C4E53D99093CA3C.png",
		"id_12451383629": "/images/iE4678809AAB8130B87C7E9341736D608.png",
		"id_38920584917": "/images/i51CD06F5D53967A813F0F7C4F8679F81.png",
		"id_164841395460": "/images/i154CF2D0DF49584435B369E991104CD0.png",
		"id_164841395418": "/images/i38E6B9D378262B4EF35387DC47BBF31D.png",
		"id_164841395446": "/images/i1B954A4FDCFE0860F6D3EBC4D01D2F19.png",
		"ruta_off": "/images/iE5329B26B9AC4ACC443CAE77621EF265.png",
		"id_1648413954101": "/images/i9C4EAA87C9F69FE64BE240DAE158CF0E.png",
		"pincho1": "/images/iB8F4698ED4EF9DE4D073ADC090AF07A3.png",
		"id_1054467024": "/images/iFF8B149DE516F182A0E44287CF224884.png",
		"id_172501864829": "/images/iC9B3A576F9A65C4C9C208CFB562D3A8D.png",
		"id_17304885157": "/images/i2DB4323AB17635027E13AECA261FA3F1.png",
		"pincho3": "/images/iA7D179C87F7325F3FBDAB36A6CA3FC58.png",
		"androidbadge45": "/images/badge45.png",
		"id_17304885168": "/images/i526A700317F4A432B0CF89FBBF332C90.png",
		"id_164841395458": "/images/i380DF6EF85EB2185DE1A501FEE66371A.png",
		"id_17250186482": "/images/i4DB2E8AD733DFC95AF5E6A5115E117EE.png",
		"androidbadge71": "/images/badge71.png",
		"id_12451383637": "/images/i642B66133CEB6E403BB536FBADA0E681.png",
		"id_12451383610": "/images/iFE945B7620AFD4BCA1698FF05C97EEDF.png",
		"id_1648413954108": "/images/i5907CCF7C26ECC19EAD231C90A4FAB9D.png",
		"id_1245138367": "/images/i0337CBA53F3589380EFAC3F505EDAFF1.png",
		"androidbadge38": "/images/badge38.png",
		"id_17250186484": "/images/iD95E1371C4A9D4060E4C293FEBB2A371.png",
		"id_17304885131": "/images/i4E6EA4674A6697031167A09342BD8C1A.png",
		"id_164841395493": "/images/i68421A3D08C3B75227181E0EC9EBCE42.png",
		"id_17304885163": "/images/iC9F146C31C3B958AC4B25A55AF41AAF7.png",
		"id_38920584929": "/images/iC9B3A576F9A65C4C9C208CFB562D3A8D.png",
		"id_164841395477": "/images/iD182D4C677A7FEF7B7F9D2021F901A9C.png",
		"id_164841395491": "/images/iF8B6DC3340FEC020058F7C743ADA0098.png",
		"id_164841395456": "/images/i9B1B0BD5A88BDAB459D66F68B80309F5.png",
		"androidbadge63": "/images/badge63.png",
		"id_164841395483": "/images/i88CB92AA112DA9B4D3913374D31E5489.png",
		"id_12451383647": "/images/iE77D1DCF0D78AED4D64B7BE4DEC18855.png",
		"id_12451383614": "/images/i5CCD03CB61FDCAE6B11B30DC864BAE99.png",
		"id_164841395466": "/images/i28D1EA7CF68179295028D9F1DC787292.png",
		"id_17250186481": "/images/i4ECB85A2AE41ACB15653139F35BFA8C7.png",
		"id_1730488514": "/images/i799FBDEFBCE42FF6A29AE24DC5E1F31A.png",
		"id_164841395453": "/images/i3CD6AA1281B7B4ED5F37ED76FCC2A808.png",
		"id_12451383652": "/images/iA9CA15D575EC6434CD632DD4B0458E02.png",
		"id_17250186485": "/images/i1846D1705B39A0DFD7CA331E2A624A7B.png",
		"id_17304885110": "/images/iBD5503A54EC228A51A5CF21B87CBB563.png",
		"id_164841395425": "/images/iF09D8144E898AB6F35D2FF363EE98F9E.png",
		"id_164841395428": "/images/i21932839113ADBF836C5D8C8B4B11AEB.png",
		"id_17304885167": "/images/iE4CD8770DC370CD28752D781C9D9E0B4.png",
		"id_12451383638": "/images/i634ADF5364D3A8C7F50C71FE6DD90A80.png",
		"id_17304885123": "/images/i30912AAC660F7B0FCE4E0CED40C5AD71.png",
		"id_17304885154": "/images/i8DA6765DE5ECD4D3000F070754687D36.png",
		"id_12451383618": "/images/i69D7DC35EF1C27805FA3D2751325A855.png",
		"id_172501864810": "/images/iB810656D7547F7A2C967AC3DB0C7CAD4.png",
		"androidbadge85": "/images/badge85.png",
		"id_17304885145": "/images/i26D96BF2871E16844A9C2CD2E119BDE3.png",
		"id_17250186487": "/images/iC18AD8B25C7E89544B26A56D72763ACF.png",
		"androidbadge83": "/images/badge83.png",
		"id_17304885137": "/images/i74139C1CFBB319B4BD1A61CFB170FAF6.png",
		"id_164841395452": "/images/iBAAC1AF775CEE1B716B575455DB68FDC.png",
		"id_17304885115": "/images/i9A86D9B0C58DA850833187AAC8994213.png",
		"id_12451383612": "/images/i1DC1B37D94F0D03FB92F1569E2D04139.png",
		"id_172501864827": "/images/iCA260B7119401A0D456373D95DAD7824.png",
		"androidbadge23": "/images/badge23.png",
		"id_164841395487": "/images/i57C1B3043D0424531659720F9C32B1C2.png",
		"id_172501864814": "/images/iB1B0726192E9AFA5B4393DD2789A8E73.png",
		"id_164841395473": "/images/i273522DAAED644BC07219E8653954EE3.png",
		"id_17304885124": "/images/iAE4F6973296E3E5EF548497E2C016062.png",
		"id_164841395437": "/images/i0E59AE058F689EF3F4FAD9FDDF470974.png",
		"ubicacion_on": "/images/i84EB4847A130B3CF82CEF987C75D0FD8.png",
		"linea": "/images/i5B4F6784F2890644E0A60D366A74C0EE.png",
		"pinchocasa": "/images/i0752E598FC723F6708D472F7AB34311A.png",
		"id_1730488513": "/images/i411207B868639F6C00CAFAE11CBBC613.png",
		"id_164841395474": "/images/i86B7E95443CEBED206372766EAF4F59C.png",
		"id_12451383613": "/images/i69B0B3F2A0625ADD474DEFFDE5FB783D.png",
		"id_12451383633": "/images/iF78AD53B709F20115D8977BF9F612522.png",
		"id_17304885171": "/images/iF82DE8593E243C41CFDFB7FDC93D09FC.png",
		"id_164841395413": "/images/i2175C7E985066E9DBB5FD1E204BDED5D.png",
		"id_17250186489": "/images/iBF02E0A9F57D34BA382B6B4851AEB549.png",
		"domicilio_on": "/images/i42740106CF7009FDB11E094035603CB7.png",
		"id_1245138366": "/images/iC9EDBE23CD4A8C3ACACF6159FAE7896F.png",
		"androidbadge4": "/images/badge4.png",
		"id_1730488518": "/images/iAFC863B5EBE8E2882E28DB9CC3BDFBD8.png",
		"id_164841395463": "/images/iF0D35F95D8CF565D84F4B2EABC07C0C3.png",
		"id_38920584921": "/images/i997DFD2C59EEEC93E94CA8831CCFE464.png",
		"id_164841395412": "/images/i867483CAEDC8D77255A7F25A15625CA5.png",
		"domicilio_off": "/images/iCE5A9979520D84ABA851C287433E381B.png",
		"androidbadge25": "/images/badge25.png",
		"id_164841395416": "/images/i1391131C784CF99686D19D65AC20986E.png",
		"id_164841395443": "/images/iBFDCFF2749FF3ACE5A75BF05C2C1E25C.png",
		"id_1730488519": "/images/i302812C7698DB0EB8CD7A9C80E1020F7.png",
		"id_1730488512": "/images/iC96D37604F465E5687FC9DC62BB2CD62.png",
		"androidbadge33": "/images/badge33.png",
		"pincho7": "/images/i733093F68676D2D585523953DFE16DBF.png",
		"androidbadge50": "/images/badge50.png",
		"id_16484139541": "/images/i21578740E736331F7BDC9D452BD712C9.png",
		"id_38920584912": "/images/iB13BBB34910D6E346D89A425DF9D9AA7.png",
		"id_172501864820": "/images/i20546752F9D3EB6A38FD547BB45E2DA7.png",
		"id_17304885113": "/images/iDED8BD3A3FE286DAA4F9FAF6DD987704.png",
		"tiempo_off": "/images/iEC57153712AB88DA8D691D2EEABF34DE.png",
		"androidbadge27": "/images/badge27.png",
		"androidbadge41": "/images/badge41.png",
		"id_17304885128": "/images/i923949CCF1410E2BAAC0D3CD69BC9908.png",
		"id_172501864816": "/images/i0B25A07F8BA6A3D1155E5635855725E3.png",
		"id_12451383642": "/images/iC926655337EBEE90D344237131E954A2.png",
		"androidbadge9": "/images/badge9.png",
		"id_164841395451": "/images/i00760138B01D3550976723B1B225C943.png",
		"id_1282626517": "/images/iFF8B149DE516F182A0E44287CF224884.png",
		"id_38920584927": "/images/iCA260B7119401A0D456373D95DAD7824.png",
		"id_164841395486": "/images/i55288AB1A85C7038BCA8AD843ADF586F.png",
		"id_12451383660": "/images/iD730909ABD133F91A1EFBC1B1E5C70DA.png",
		"caminando": "/images/i73C793111A21DBA51C1900201A900DFE.png",
		"id_17304885133": "/images/i1B9D8C4796CE3F5DECE2F37AEAA7D9B2.png",
		"id_1245138365": "/images/iBC6A9F0C0BC8FF1BFAAE65D31F1594AB.png",
		"id_12451383623": "/images/i87AA4E2C8283824C531D6EA19313D656.png",
		"id_164841395462": "/images/iD0A8DD7F8257D87CADD4557D2DDE27A0.png",
		"id_333012614": "/images/iC8D04E2813709AD9C2488144189CDC65.png",
		"id_16484139544": "/images/i9290A0A895DCA701657AA3A2701BC5B1.png",
		"id_12451383641": "/images/i7E452776BF9CF2E5102B37DB75E66F36.png",
		"id_17304885134": "/images/iCD5BC07F526D4BAC0631D0A8D660CFEB.png"
	},
	"classes": {
		"#Largomt2": {
			"text": "L('x2558236558_traducir','Largo (mt)')"
		},
		"#ID_919650652": {
			"on": "L('x739438103','inspector.d5')"
		},
		"#iconoEntrar4": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "14dp"
			}
		},
		"#iconoUbicacion10": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#widgetHeader4": {
			"titulo": "L('x3752633607_traducir','PARTE 4: Disponibilidad de trabajo')"
		},
		"#DescribaelDao": {
			"hintText": "L('x397976520_traducir','Describa el daño')"
		},
		"#FechaSiniestro": {
			"text": "L('x3210826773_traducir','Fecha Siniestro')"
		},
		"#Definirde": {
			"text": "L('x1350052795_traducir','Definir % de daños')"
		},
		"#ID_1561283468": {
			"imagen": "L('x908735488','evento.foto')",
			"calidad": "L('x511942527_traducir','91')",
			"nueva": "L('x1112834923','640')",
			"camara": "L('x2899294158_traducir','trasera')"
		},
		"#Label10": {
			"title": "L('x4216663563_traducir','Emergencias')"
		},
		"#iconoFlecha4": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "6dp"
			}
		},
		"#ID_163921178": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#widgetBotonlargo3": {
			"titulo": "L('x1524107289_traducir','CONTINUAR')"
		},
		"fondolila": {
			"backgroundColor": "#8383db",
			"font": {
				"fontSize": "12dp"
			}
		},
		"#editar_pais": {
			"title": "L('x3206656629_traducir','editar pais')"
		},
		"#ID_1081611599": {
			"on": "L('x3038494637','inspector.d6')"
		},
		"#Escriturade": {
			"text": "L('x3916353779_traducir','Escritura de compraventa')"
		},
		"bg_progreso_celeste": {
			"backgroundColor": "#60bde2",
			"font": {
				"fontSize": "12dp"
			}
		},
		"btheader": {
			"color": "#2d9edb",
			"font": {
				"fontWeight": "bold",
				"fontSize": "18dp"
			}
		},
		"#ID_1181309813": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"estilo8": {
			"color": "#ee7f7e",
			"font": {
				"fontSize": "12dp"
			}
		},
		"#Piso": {
			"text": "L('x1951430381_traducir','Piso')"
		},
		"#ID_600103438": {
			"imagen": "L('x908735488','evento.foto')",
			"calidad": "L('x511942527_traducir','91')",
			"nueva": "L('x1112834923','640')",
			"camara": "L('x2899294158_traducir','trasera')"
		},
		"#seccionListadoFechaentrantes": {
			"headerTitle": "L('x2345059420','fecha_entrantes')"
		},
		"#FechadeNacimiento": {
			"text": "L('x2443017383_traducir','Fecha de nacimiento')"
		},
		"#imagen": {
			"images": ["/images/i8BA7BBB2B899B993343E33AEE2947F03.png", "/images/iC96D37604F465E5687FC9DC62BB2CD62.png", "/images/i411207B868639F6C00CAFAE11CBBC613.png", "/images/i799FBDEFBCE42FF6A29AE24DC5E1F31A.png", "/images/i38C61C490C2D132A441BA4DA831A0B6B.png", "/images/i6FB9FA73DDDE12DF8E86FFCDA892EC82.png", "/images/iAFB1024EC559791C8AD5D79928EA088C.png", "/images/iAFC863B5EBE8E2882E28DB9CC3BDFBD8.png", "/images/i302812C7698DB0EB8CD7A9C80E1020F7.png", "/images/iBD5503A54EC228A51A5CF21B87CBB563.png", "/images/i07DBC5BDF16999EDD7FF32220DED028B.png", "/images/i9DE8C660239457D57CAF6FC50718367D.png", "/images/iDED8BD3A3FE286DAA4F9FAF6DD987704.png", "/images/i874A37F48CC2C1995FC52C21369DDEB6.png", "/images/i9A86D9B0C58DA850833187AAC8994213.png", "/images/i252ECAA10D65331F13AD2A83FD659115.png", "/images/i3F8954A6250AC3AD770A65B1E182A34D.png", "/images/i19F42D46458DA832B6C5EA605D0FE30F.png", "/images/iD963A16A7C9E33D13B943B513E0C6C61.png", "/images/i2195901CE0F8A3F4362453AB02240F9F.png", "/images/i68D6F827E1F992CD125964497C0F6E55.png", "/images/i30CD6C73987FC566A08353B551A359CC.png", "/images/i30912AAC660F7B0FCE4E0CED40C5AD71.png", "/images/iAE4F6973296E3E5EF548497E2C016062.png", "/images/iFB0ED3833FB97A5B95B692F9C8471E31.png", "/images/i908F7822EB301E7C21DA3300EA7E1E04.png", "/images/i5C81EB6AB36733A02A5E68514D23976A.png", "/images/i923949CCF1410E2BAAC0D3CD69BC9908.png", "/images/i17E5146D6B946B06A625338800602C01.png", "/images/iA8164ACEC2C69CE2236E129E444F888A.png", "/images/i4E6EA4674A6697031167A09342BD8C1A.png", "/images/i11FFB0492F29D10E6FCE27A9D4057829.png", "/images/i1B9D8C4796CE3F5DECE2F37AEAA7D9B2.png", "/images/iCD5BC07F526D4BAC0631D0A8D660CFEB.png", "/images/i8830B078DE88248713D2F7C7DA553651.png", "/images/i0FE7FE00BA11FC42B3C0AFD490C4CFA6.png", "/images/i74139C1CFBB319B4BD1A61CFB170FAF6.png", "/images/iE8B07C574058E6BAA503A39BA330CE81.png", "/images/i69C37C68EB212086D1F0C231FFAB90E9.png", "/images/iEFF38CD8533035A66E0AF660B4E241AA.png", "/images/i1B99D77A5103B23820B567FEC7972C68.png", "/images/iF6D625C734C76E37B9240DE8C8E787FC.png", "/images/i3C81A2834695F313E5948B0BE6FB79D2.png", "/images/iAAA43281AA3CC03AB9B0FE147901460E.png", "/images/i26D96BF2871E16844A9C2CD2E119BDE3.png", "/images/iDAFCE8BEB03EBA6E5AE151A6D8498A43.png", "/images/i64E25421653D939AB6C5F203BF125DA1.png", "/images/iBF1B0916259A898898D73A289B45799B.png", "/images/i874015A473ED5D0E79223B1DD2AF0A8D.png", "/images/i272088853E72E862CA0E46BC9C1CB573.png", "/images/i8BA0FB795C77A5EA886096DF052FBF69.png", "/images/i00DF7F2C7433E465EE49D8415C103CB0.png", "/images/i96E6AEC1984C01E35E87A3D4D8529C92.png", "/images/i8DA6765DE5ECD4D3000F070754687D36.png", "/images/iB6C20BCB226B5B33CBDEC4DC14BFE599.png", "/images/i0D1C15FCA2F7073146F802BFB0B912B3.png", "/images/i2DB4323AB17635027E13AECA261FA3F1.png", "/images/i6AC13900B0D450E5EB03B51FDAE9345E.png", "/images/i237B9312627FB83A97AD3AECE3B26610.png", "/images/i90C0FC838365FD82F6AEEB884482F922.png", "/images/iFD70798B325B49DF04F098F085C08364.png", "/images/iAF74778DC2264D8D603BB03FEB9BAE6A.png", "/images/iC9F146C31C3B958AC4B25A55AF41AAF7.png", "/images/iFE5E804CABCEBF34AD79DF5E7E40CA2C.png", "/images/i82E01BB1890D850A04D145E675940E7F.png", "/images/iF4D52A4E116FB61ABEFF82D8A30CA9C9.png", "/images/iE4CD8770DC370CD28752D781C9D9E0B4.png", "/images/i526A700317F4A432B0CF89FBBF332C90.png", "/images/iD12F88BE292586CBCD824A627FD1E84A.png", "/images/i501CFFB92F510A66A56518AC101744C1.png", "/images/iF82DE8593E243C41CFDFB7FDC93D09FC.png", "/images/i54D9E2FB35E8CF50832C582618BF207C.png"]
		},
		"#NombreDelItem": {
			"text": "L('x3109184006_traducir','Nombre del item')"
		},
		"#ID_999748386": {
			"vertexto": "L('x2310355463','true')",
			"verprogreso": "L('x2001657581_traducir','false')"
		},
		"#SeIniciarel": {
			"text": "L('x3731894882_traducir','Se iniciará el proceso de inspección')"
		},
		"fondorosado": {
			"backgroundColor": "#ffacaa",
			"font": {
				"fontSize": "12dp"
			}
		},
		"#widgetMono": {
			"titulo": "L('x2349703141_traducir','NO TIENES TAREAS')",
			"texto": "L('x2279881512_traducir','Asegurate de tomar tareas para hoy y revisar tu ruta')"
		},
		"#ID_72807326": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#Definirde3": {
			"text": "L('x1350052795_traducir','Definir % de daños')"
		},
		"#ID_592273440": {
			"seleccione": "L('x3621251639_traducir','unidad')"
		},
		"#ID_1288158830": {
			"seleccione": "L('x3621251639_traducir','unidad')"
		},
		"#ID_611316667": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#ENROLAMIENTO3": {
			"title": "L('x2146494644_traducir','ENROLAMIENTO')"
		},
		"#ID_82327069": {
			"pantalla": "L('x1828862638_traducir','basicos')"
		},
		"#editar_domicilio": {
			"title": "L('x408858565_traducir','editar domicilio')"
		},
		"#widgetModal3": {
			"titulo": "L('x2059554513_traducir','ENTIDAD')",
			"cargando": "L('x1831148736','cargando...')",
			"campo": "L('x2538351238_traducir','Entidad Financiera')",
			"seleccione": "L('x3157783868_traducir','Seleccione entidad')"
		},
		"#AseguradorCorredor": {
			"text": "L('x2324757213_traducir','Asegurador/Corredor')"
		},
		"#niveles2": {
			"title": "L('x2201954602_traducir','niveles')"
		},
		"#NoCierrela": {
			"text": "L('x1187778027_traducir','No cierre la aplicacion y asegurese de estar conectado a internet')"
		},
		"#icono7": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "15dp"
			}
		},
		"#label2": {
			"title": "L('x2605545184_traducir','Mis tareas')",
			"text": "L('x1046761583_traducir','12-03-1989')"
		},
		"#iconoComuna3": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#Descripcin2": {
			"text": "L('x2847042974_traducir','Descripción del daño del contenido')"
		},
		"#icono13": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "15dp"
			}
		},
		"#NO3": {
			"text": "L('x3376426101_traducir','NO')"
		},
		"#iconoCaso": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "18dp"
			}
		},
		"#widgetPelota7": {
			"letra": "L('x2746444292','D')"
		},
		"#EstSeguro": {
			"text": "L('x1351122497_traducir','¿Está seguro?')"
		},
		"#editar_disponibilidad": {
			"title": "L('x125408045_traducir','editar disponibilidad')"
		},
		"#Superficiem": {
			"text": "L('x4117795806_traducir','Superficie (m2)')"
		},
		"#ID_1327135108": {
			"data": "L('x1872543855','data_region')"
		},
		"#MximoCaracteres": {
			"hintText": "L('x1483717589_traducir','máximo 140 caracteres')"
		},
		"#iconoFlecha": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "6dp"
			}
		},
		"#Altomt2": {
			"text": "L('x2760387473_traducir','Alto (mt)')"
		},
		"#RepitasuCorreo": {
			"hintText": "L('x2070103557_traducir','repita su correo')"
		},
		"#ID_450448435": {
			"imagen": "L('x908735488','evento.foto')",
			"calidad": "L('x511942527_traducir','91')",
			"nueva": "L('x1112834923','640')",
			"camara": "L('x2899294158_traducir','trasera')"
		},
		"#AoDelNivel": {
			"text": "L('x1895451025_traducir','Año del nivel')"
		},
		"#Fecha": {
			"text": "L('x3228145885_traducir','Fecha')"
		},
		"#ENROLAMIENTO5": {
			"title": "L('x2146494644_traducir','ENROLAMIENTO')"
		},
		"fondomorado": {
			"backgroundColor": "#b9aaf3",
			"font": {
				"fontSize": "12dp"
			}
		},
		"#ID_656202608": {
			"texto": "L('x2558119856_traducir','Guardar')"
		},
		"#SeleccionePartida": {
			"text": "L('x221750731_traducir','seleccione partida')"
		},
		"#NroCasoSistema": {
			"text": "L('x2151727540_traducir','Nro caso sistema')"
		},
		"#ID_1755646569": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#widgetPelota6": {
			"letra": "L('x543223747','S')"
		},
		"#Presupuesto": {
			"text": "L('x638343172_traducir','Presupuesto de reparaciones del edificio en formato excel, detallado por recintos a través de precios unitarios, según la siguiente descripción: Partida, unidad, cantidad, valor unitario y valor total, incluyendo adicionalmente los gastos generales, utilidad e IVA (Se entrega formato completo)')"
		},
		"#iconoUbicacion4": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#ID_201283538": {
			"valor": "L('x4264937424','evento.valor')"
		},
		"#ID_541145258": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"estilo9": {
			"font": {
				"fontFamily": "Roboto-Medium",
				"fontSize": "16dp"
			}
		},
		"#EscribaNumero": {
			"hintText": "L('x3183695581_traducir','Escriba numero de piso')"
		},
		"#FechaInspeccion": {
			"text": "L('x2793345159_traducir','Fecha inspeccion')"
		},
		"#ID_138241524": {
			"imagen2": "L('x1913188287','foto_original')",
			"nueva": "L('x1112834923','640')",
			"camara": "L('x1008027353_traducir','camara')"
		},
		"#ID_48701328": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#Disponibilidad": {
			"text": "L('x2249118711_traducir','Disponibilidad')"
		},
		"#ID_244368868": {
			"bono": "L('x1979562742','tareas[0].bono')"
		},
		"#ID_21955821": {
			"texto": "L('x1145120034','ini_aviso')"
		},
		"#widgetModalmultiple13": {
			"titulo": "L('x2266302645_traducir','Cubierta')",
			"cargando": "L('x1831148736','cargando...')",
			"hint": "L('x2134385782_traducir','Seleccione cubiertas')",
			"subtitulo": "L('x4011106049_traducir','Indique los tipos')"
		},
		"#TomarFotosDel": {
			"text": "L('x1885200639_traducir','Tomar fotos del daño')"
		},
		"#widgetModalmultiple3": {
			"titulo": "L('x591862035_traducir','Pavimentos')",
			"cargando": "L('x1831148736','cargando...')",
			"hint": "L('x2600368035_traducir','Seleccione pavimentos')",
			"subtitulo": "L('x4011106049_traducir','Indique los tipos')"
		},
		"#ID_1317893384": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#iconoUbicacion": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#seccionListadoOtra": {
			"headerTitle": "L('x910537343_traducir','otra')"
		},
		"#ID_488751613": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#iconoCiudad7": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#iconoCiudad12": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#SeRecomienda": {
			"text": "L('x1718106177_traducir','¿Se recomienda análisis estructural por especialista?')"
		},
		"#StatusEsperando": {
			"text": "L('x2451619465_traducir','status: esperando')"
		},
		"fondoverde": {
			"backgroundColor": "#8ce5bd",
			"font": {
				"fontSize": "12dp"
			}
		},
		"#EntraParaEditar": {
			"text": "L('x2434817046_traducir','Entra para editar tus datos')"
		},
		"#ConfirmaTus": {
			"text": "L('x22218330_traducir','Confirma tus tareas de hoy')"
		},
		"#iconoComuna12": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#editarrecinto": {
			"title": "L('x701446659_traducir','editarrecinto')"
		},
		"#ID_598086017": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#ID_241989500": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#inicio": {
			"title": "L('x3719714762_traducir','inicio')"
		},
		"fondocritica": {
			"backgroundColor": "#ffdcdc",
			"font": {
				"fontSize": "12dp"
			}
		},
		"#ID_697430": {
			"on": "L('x1528159873','inspector.d4')"
		},
		"#Piso2": {
			"text": "L('x1951430381_traducir','Piso')"
		},
		"#iconoRegistrarse": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "17dp"
			}
		},
		"#AodeConstruccion": {
			"text": "L('x2722748660_traducir','Año de construccion')"
		},
		"#icono4": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "15dp"
			}
		},
		"#ID_1471939697": {
			"nombre": "L('x1288856316','fila.nombre')",
			"id_segured": "L('x836317893','fila.id_segured')",
			"id_partida": "L('x836317893','fila.id_segured')",
			"id_server": "L('x1527202377','fila.id_server')"
		},
		"estilo8_2": {
			"color": "#ee7f7e",
			"font": {
				"fontFamily": "Roboto-Medium",
				"fontSize": "10dp"
			}
		},
		"#Recinto": {
			"text": "L('x1035217420_traducir','Recinto')"
		},
		"#iconoEntrar5": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "14dp"
			}
		},
		"#iconoUbicacion2": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#ID_1816801192": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#ID_1014684330": {
			"valor": "L('x1367393739','dato_n1[0].id_label')"
		},
		"#EnPresencia": {
			"text": "L('x2016219634_traducir','En presencia de')"
		},
		"#Partida": {
			"text": "L('x1719427472_traducir','Partida')"
		},
		"#NombreAsegurado": {
			"text": "L('x1960877763_traducir','NombreAsegurado')"
		},
		"#Correo": {
			"text": "L('x1890062079_traducir','Correo')"
		},
		"#EscribaNombre2": {
			"hintText": "L('x3550349657_traducir','Escriba nombre nivel')"
		},
		"#iconoCiudad2": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#ID_1540488274": {
			"valor": "L('x2602436437_traducir','Seleccione compañía')"
		},
		"#RUTDelPresente": {
			"text": "L('x3652084882_traducir','RUT del presente')"
		},
		"#firma": {
			"title": "L('x736965987_traducir','firma')"
		},
		"#widgetHeader3": {
			"titulo": "L('x233750493_traducir','PARTE 3: Domicilio')"
		},
		"#ID_960197216": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#ListadodeContenidos": {
			"text": "L('x2513329370_traducir','Listado de contenidos afectados valorizados, detallado por: Tipo de bien, marca, modelo, edad y valor estimado (Se entrega formato ejemplo)')"
		},
		"#widgetModal5": {
			"titulo": "L('x1629775439_traducir','MONEDAS')",
			"cargando": "L('x1831148736','cargando...')",
			"campo": "L('x3081186843_traducir','Moneda')",
			"seleccione": "L('x2547889144','-')"
		},
		"#RUTDelAsegurado": {
			"text": "L('x2270754326_traducir','RUT del asegurado')"
		},
		"#ID_1963341563": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#imagen4": {
			"images": ["/images/i4ECB85A2AE41ACB15653139F35BFA8C7.png", "/images/i4DB2E8AD733DFC95AF5E6A5115E117EE.png", "/images/iD4FE3BB66A3A734672CFBF9A4FE820B3.png", "/images/iD95E1371C4A9D4060E4C293FEBB2A371.png", "/images/i1846D1705B39A0DFD7CA331E2A624A7B.png", "/images/i1AAB9DCC791F6C8D5CBD86A69F2BD28A.png", "/images/iC18AD8B25C7E89544B26A56D72763ACF.png", "/images/i451D26CB7C617839FAAABBEE243EE91B.png", "/images/iBF02E0A9F57D34BA382B6B4851AEB549.png", "/images/iB810656D7547F7A2C967AC3DB0C7CAD4.png", "/images/iF173895CDE559CF4A789ECA310F324C1.png", "/images/iB13BBB34910D6E346D89A425DF9D9AA7.png", "/images/i09E757E6A96CDFD1A3071F75A86B9703.png", "/images/iB1B0726192E9AFA5B4393DD2789A8E73.png", "/images/i3BD062E17CB084738EC3C8544C4F562C.png", "/images/i0B25A07F8BA6A3D1155E5635855725E3.png", "/images/i51CD06F5D53967A813F0F7C4F8679F81.png", "/images/i57F0B2CBFC207F6540A08C8209B6461C.png", "/images/i6F4FBDF546B1311877A208695B53B1F5.png", "/images/i20546752F9D3EB6A38FD547BB45E2DA7.png", "/images/i997DFD2C59EEEC93E94CA8831CCFE464.png", "/images/i7A0053CD88CD6D72A30F33A67115B84F.png", "/images/i53F37CB31AA110AB8BF1BCA095F905FE.png", "/images/i6F4B0EAC06F1D5F21CF0ADB7E3036782.png", "/images/iD8AE92E691560F489DE642319547F8F9.png", "/images/iF8FD2CB3805AB4250C4E53D99093CA3C.png", "/images/iCA260B7119401A0D456373D95DAD7824.png", "/images/iC97BF367BA6481E3A5CEAEBFBD492506.png", "/images/iC9B3A576F9A65C4C9C208CFB562D3A8D.png", "/images/iCEF1D427EF5C147BC79A0D6848913C44.png"]
		},
		"#Usuario": {
			"text": "L('x3990391233_traducir','Usuario')"
		},
		"#Telfono": {
			"text": "L('x2354316101_traducir','Teléfono')"
		},
		"#Nombre3": {
			"text": "L('x1027380240_traducir','Nombre')"
		},
		"#ID_1248585493": {
			"imagen": "L('x908735488','evento.foto')",
			"calidad": "L('x511942527_traducir','91')",
			"nueva": "L('x1112834923','640')",
			"camara": "L('x2899294158_traducir','trasera')"
		},
		"#Escribir": {
			"hintText": "L('x714216034_traducir','escribir')"
		},
		"#ID_1393498435": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#widgetBoton": {
			"texto": "L('x990554165_traducir','PRESIONE PARA VERIFICAR')"
		},
		"#Especifique": {
			"hintText": "L('x1175748445_traducir','Especifique')"
		},
		"#AoDelNivel2": {
			"text": "L('x1895451025_traducir','Año del nivel')"
		},
		"#ID_364171459": {
			"texto": "L('x2558119856_traducir','Guardar')"
		},
		"#iconoEntrar6": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "14dp"
			}
		},
		"#ID_971982901": {
			"valor": "L('x392683181','monedas[0].nombre')"
		},
		"#ID_1852315924": {
			"texto": "L('x1629090833','insp_otrodia')"
		},
		"#ENVIANDOINSPECCIONES": {
			"text": "L('x2721945877_traducir','ENVIANDO INSPECCIONES')"
		},
		"#iconoUbicacion5": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#ApellidoMaterno": {
			"text": "L('x3313466056_traducir','Apellido Materno')"
		},
		"#nuevocontenido": {
			"title": "L('x2164432253_traducir','nuevocontenido')"
		},
		"#widgetModalmultiple4": {
			"titulo": "L('x1866523485_traducir','Estruct. cubierta')",
			"cargando": "L('x1831148736','cargando...')",
			"hint": "L('x2460890829_traducir','Seleccione e.cubiertas')",
			"subtitulo": "L('x4011106049_traducir','Indique los tipos')"
		},
		"#TomarFotosde": {
			"text": "L('x2601646010_traducir','Tomar fotos de Recinto')"
		},
		"#ID_673797912": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#iconoEntrar12": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "14dp"
			}
		},
		"#ID_247745777": {
			"pantalla": "L('x3681655494_traducir','caracteristicas')"
		},
		"#ID_1716162437": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#ID_70294074": {
			"imagen": "L('x908735488','evento.foto')",
			"calidad": "L('x511942527_traducir','91')",
			"nueva": "L('x1112834923','640')",
			"camara": "L('x2899294158_traducir','trasera')"
		},
		"#iconoFlecha2": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "6dp"
			}
		},
		"#filaPicker": {
			"title": "L('x3964605301_traducir','Part time')"
		},
		"#widgetBotonlargo5": {
			"titulo": "L('x1524107289_traducir','CONTINUAR')"
		},
		"#ID_747197978": {
			"monito": "L('x2310355463','true')"
		},
		"#ID_502441026": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#widgetBarra7": {
			"titulo": "L('x2146494644_traducir','ENROLAMIENTO')"
		},
		"#ID_1978741353": {
			"valor": "L('x4264937424','evento.valor')"
		},
		"#widget4fotos": {
			"opcional": "L('x2052833569_traducir','Opcional')",
			"label1": "L('x724488137_traducir','Fachada')",
			"label4": "L('x676841594_traducir','Nº Depto')",
			"label3": "L('x4076266664_traducir','Numero')",
			"label2": "L('x2825989175_traducir','Barrio')"
		},
		"#ID_308389788": {
			"valor": "L('x4264937424','evento.valor')"
		},
		"#SeleccioneProducto": {
			"text": "L('x2719983611_traducir','seleccione producto')"
		},
		"#ID_1576008367": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#ID_1746919605": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#icono5": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "15dp"
			}
		},
		"#iconoEntrar": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "14dp"
			}
		},
		"#ID_1950446904": {
			"valor": "L('x2320593836','recintosb[0].nombre')"
		},
		"#iconoEntrar3": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "16dp"
			}
		},
		"#SI": {
			"text": "L('x3746555228_traducir','SI')"
		},
		"#iconoUbicacion7": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#ID_1156891271": {
			"nueva": "L('x1112834923','640')",
			"camara": "L('x1008027353_traducir','camara')",
			"imagen4": "L('x1913188287','foto_original')"
		},
		"#editardano": {
			"title": "L('x2580589745_traducir','editardano')"
		},
		"#EditeCadaItem": {
			"text": "L('x2381264395_traducir','Edite cada item independientemente')"
		},
		"#widgetBotonlargo": {
			"titulo": "L('x1678967761_traducir','GUARDAR RECINTOS')"
		},
		"#nuevorecinto": {
			"title": "L('x3877276762_traducir','nuevorecinto')"
		},
		"#ID_1380094590": {
			"on": "L('x3256651579','inspector.d7')"
		},
		"#ID_25042256": {
			"imagen": "L('x908735488','evento.foto')",
			"calidad": "L('x511942527_traducir','91')",
			"nueva": "L('x1112834923','640')",
			"camara": "L('x2899294158_traducir','trasera')"
		},
		"estilo7": {
			"color": "#999999",
			"font": {
				"fontFamily": "Roboto-Medium",
				"fontSize": "14dp"
			}
		},
		"#icono3": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "15dp"
			}
		},
		"fondorojo": {
			"backgroundColor": "#ee7f7e",
			"font": {
				"fontSize": "12dp"
			}
		},
		"#ID_1607260411": {
			"imagen": "L('x908735488','evento.foto')",
			"calidad": "L('x511942527_traducir','91')",
			"nueva": "L('x1112834923','640')",
			"camara": "L('x2899294158_traducir','trasera')"
		},
		"#ID_440554575": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#ID_1955939486": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#ID_938801000": {
			"pantalla": "L('x3174879261_traducir','recintos')"
		},
		"#widgetEstadeacuerdo": {
			"titulo": "L('x404129330_traducir','¿EL ASEGURADO CONFIRMA ESTOS DATOS?')",
			"si": "L('x1723413441_traducir','SI, Están correctos')",
			"texto": "L('x2083765231_traducir','El asegurado debe confirmar que los datos de esta sección están correctos')",
			"pantalla": "L('x2851883104_traducir','¿ESTA DE ACUERDO?')",
			"no": "L('x55492959_traducir','NO, Hay que modificar algo')"
		},
		"#RepetirCorreo": {
			"text": "L('x3920081714_traducir','Repetir correo')"
		},
		"#ID_687145751": {
			"tipo": "L('x409735542','_sorry')"
		},
		"#widgetBarra6": {
			"titulo": "L('x2146494644_traducir','ENROLAMIENTO')",
			"fondo": "L('x84180205_traducir','fondoblanco')",
			"colortitulo": "L('x1631879960_traducir','negro')"
		},
		"#DocumentosSolicitados": {
			"text": "L('x3381833431_traducir','Documentos solicitados al asegurado')"
		},
		"titulo_recinto": {
			"color": "#8383db",
			"font": {
				"fontSize": "16dp"
			}
		},
		"#ID_1087169582": {
			"seleccione": "L('x2404293600_traducir','seleccione tipo')"
		},
		"#widgetPelota3": {
			"letra": "L('x185522819_traducir','MI')"
		},
		"estilo11": {
			"color": "#2d9edb",
			"font": {
				"fontWeight": "bold",
				"fontSize": "15dp"
			}
		},
		"#PRESIONAPARA": {
			"text": "L('x3327316334_traducir','PRESIONA PARA CONFIRMAR')"
		},
		"#Niveles": {
			"text": "L('x1283592374_traducir','Niveles')"
		},
		"#ENVIAR": {
			"text": "L('x4106122489_traducir','ENVIAR')"
		},
		"#Meses2": {
			"text": "L('x742992573_traducir','meses')"
		},
		"#ENROLAMIENTO7": {
			"title": "L('x2146494644_traducir','ENROLAMIENTO')"
		},
		"#ID_1991669706": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#Especificaciones": {
			"text": "L('x4145328961_traducir','Especificaciones técnicas de la construcción')"
		},
		"#widgetPelota4": {
			"letra": "L('x1141589763','J')"
		},
		"estilo11_1": {
			"color": "#fcbd83",
			"font": {
				"fontWeight": "bold",
				"fontSize": "15dp"
			}
		},
		"#iconoCiudad3": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#ASEGURATEDE": {
			"text": "L('x1820953783_traducir','ASEGURATE DE ENVIAR LA INSPECCION TERMINADA EN TU PERFIL')"
		},
		"#ID_1748478756": {
			"valor": "L('x4264937424','evento.valor')"
		},
		"#ID_1327360331": {
			"pantalla": "L('x3772634211_traducir','siniestro')"
		},
		"estilo5_1": {
			"color": "#c0c0c7",
			"font": {
				"fontSize": "17dp"
			}
		},
		"#NO5": {
			"text": "L('x3376426101_traducir','NO')"
		},
		"#ID_1787217865": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#ID_1907635340": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#SeleccioneProducto2": {
			"text": "L('x2719983611_traducir','seleccione producto')"
		},
		"#ID_969494301": {
			"imagen": "L('x908735488','evento.foto')",
			"calidad": "L('x511942527_traducir','91')",
			"nueva": "L('x1112834923','640')",
			"camara": "L('x2899294158_traducir','trasera')"
		},
		"#ID_104717595": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#iconoComuna10": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#Asegurado": {
			"text": "L('x2764946924_traducir','Asegurado')"
		},
		"fondoceleste": {
			"backgroundColor": "#8bc9e8",
			"font": {
				"fontSize": "12dp"
			}
		},
		"#EstsDispuesto2": {
			"text": "L('x529917922_traducir','¿Estás dispuesto a viajar fuera de tu país?')"
		},
		"#icono8": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "15dp"
			}
		},
		"#Respaldosde": {
			"text": "L('x230628023_traducir','Respaldos de adquisición de bienes afectados (Facturas, boletas, cartolas de casas comerciales, catálogos, fotos)')"
		},
		"estilo13": {
			"color": "#ffffff",
			"font": {
				"fontSize": "12dp"
			}
		},
		"#widgetModalmultiple9": {
			"titulo": "L('x1219835481_traducir','Muros / Tabiques')",
			"cargando": "L('x1831148736','cargando...')",
			"hint": "L('x2879998099_traducir','Seleccione muros')",
			"subtitulo": "L('x4011106049_traducir','Indique los tipos')"
		},
		"#ID_1893624963": {
			"on": "L('x729754126','inspector.d1')"
		},
		"#ID_1245606518": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#iconoCiudad10": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#Edificio": {
			"text": "L('x664979134_traducir','Edificio')"
		},
		"#ENROLAMIENTO": {
			"title": "L('x2146494644_traducir','ENROLAMIENTO')"
		},
		"#MEJORRUTA": {
			"text": "L('x1110261063_traducir','MEJOR RUTA')"
		},
		"#iconoEntrar7": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "14dp"
			}
		},
		"#ID_887108424": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#listadocontenidos": {
			"title": "L('x741972875_traducir','listadocontenidos')"
		},
		"#Aseguresede": {
			"text": "L('x3795083247_traducir','Asegurese de haber guardado todos los daños')"
		},
		"#ID_1639937371": {
			"pantalla": "L('x3294048930_traducir','documentos')"
		},
		"#ID_999750160": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#ID_1041491772": {
			"on": "L('x2994109364','inspector.d2')"
		},
		"#ID_1246965721": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#Inhabitable": {
			"text": "L('x2452838785_traducir','¿Inhabitable?')"
		},
		"#Superficie": {
			"text": "L('x2877523867_traducir','Superficie')"
		},
		"#ID_1675410232": {
			"texto": "L('x3105206052','presione_aceptar')",
			"estilo": "L('x2379090022_traducir','fondoverde')",
			"estado": "L('x2212294583','1')",
			"vertexto": "L('x2310355463','true')",
			"verprogreso": "L('x2001657581_traducir','false')"
		},
		"#ID_497149076": {
			"data": "L('x2347414356','data_oficios')"
		},
		"#iconoCiudad6": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#Asegratede": {
			"text": "L('x1626050066_traducir','Asegúrate de haber ingresado todos los recintos')"
		},
		"#IngreseUsuario": {
			"hintText": "L('x1935574480_traducir','ingrese usuario')"
		},
		"#ID_973667106": {
			"texto": "L('x1226161080_traducir','Siguiente')"
		},
		"#ASEGURADOSE": {
			"text": "L('x3998571287_traducir','ASEGURADO SE COMPROMETE EN ENVIAR LOS ANTECEDENTES SOLICITADOS EN')"
		},
		"#OtrosEspecificar": {
			"text": "L('x939625677_traducir','Otros (Especificar)')"
		},
		"bg_progreso_rosado": {
			"backgroundColor": "#ff9292",
			"font": {
				"fontSize": "12dp"
			}
		},
		"#Meses": {
			"hintText": "L('x742992573_traducir','meses')"
		},
		"#widgetPelota2": {
			"letra": "L('x3664761504','M')"
		},
		"#IngreseContrasea": {
			"hintText": "L('x2113698382_traducir','ingrese contraseña')"
		},
		"#iconoComuna7": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#ID_294305317": {
			"valor": "L('x2886025560','tipo_dano[0].nombre')"
		},
		"#Editar_PartTime": {
			"title": "L('x677283029_traducir','Editar PartTime')"
		},
		"#widgetPelota": {
			"letra": "L('x2909332022','L')"
		},
		"#CuntosDas": {
			"text": "L('x3307875748_traducir','¿Cuántos días?')"
		},
		"#IngresesuTelfono": {
			"hintText": "L('x93906838_traducir','ingrese su teléfono')"
		},
		"#PERFIL": {
			"title": "L('x1721610689_traducir','PERFIL')"
		},
		"#icono10": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "15dp"
			}
		},
		"#Superficiem2": {
			"text": "L('x4117795806_traducir','Superficie (m2)')"
		},
		"#widgetModalmultiple10": {
			"titulo": "L('x3327059844_traducir','Entrepisos')",
			"cargando": "L('x1831148736','cargando...')",
			"hint": "L('x2146928948_traducir','Seleccione entrepisos')",
			"subtitulo": "L('x4011106049_traducir','Indique los tipos')"
		},
		"#seccionListadoFecha": {
			"headerTitle": "L('x27834329_traducir','fecha')"
		},
		"#listadodanos": {
			"title": "L('x3510068842_traducir','listadodanos')"
		},
		"#AgregueRecintos": {
			"text": "L('x1886674359_traducir','Agregue recintos para indicar daños')"
		},
		"#ID_1747880333": {
			"seleccione": "L('x2404293600_traducir','seleccione tipo')"
		},
		"#DetallarExperiencia": {
			"text": "L('x2089188151_traducir','Detallar experiencia (Quedan 140 caracteres)')"
		},
		"#EditarContactos": {
			"text": "L('x574786063_traducir','Editar Contactos')"
		},
		"#ID_384484434": {
			"pantalla": "L('x3181120844_traducir','hayalguien')"
		},
		"#ID_252175981": {
			"texto": "L('x2034691121','confirmar_tarea')"
		},
		"#ID_1960576880": {
			"vertexto": "L('x2310355463','true')",
			"verprogreso": "L('x2001657581_traducir','false')"
		},
		"#icono6": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "15dp"
			}
		},
		"#RepitasuTelfono": {
			"hintText": "L('x1125245_traducir','repita su teléfono')"
		},
		"#PORTADA": {
			"title": "L('x2728236766_traducir','PORTADA')"
		},
		"#iconoTelefono": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "16dp"
			}
		},
		"#ID_1164248528": {
			"monito": "L('x2310355463','true')"
		},
		"titulo_niveles": {
			"color": "#8ce5bd",
			"font": {
				"fontSize": "16dp"
			}
		},
		"#Nombredela": {
			"hintText": "L('x293599003_traducir','Nombre de la persona')"
		},
		"fondooscuro": {
			"backgroundColor": "#878787",
			"font": {
				"fontSize": "12dp"
			}
		},
		"#itemListado": {
			"_section": "L('x2819214932_traducir','holi1')"
		},
		"#iconoCamara": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "36dp"
			}
		},
		"#ID_754196014": {
			"imagen1": "L('x1913188287','foto_original')",
			"nueva": "L('x1112834923','640')",
			"camara": "L('x1008027353_traducir','camara')"
		},
		"#ID_1234773562": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#ID_446946774": {
			"texto": "L('x1226161080_traducir','Siguiente')"
		},
		"#ID_851239848": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#icono1": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "15dp"
			}
		},
		"#iconoComuna8": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"fondoamarillo": {
			"backgroundColor": "#f8da54",
			"font": {
				"fontSize": "12dp"
			}
		},
		"#widgetBarra4": {
			"titulo": "L('x2146494644_traducir','ENROLAMIENTO')"
		},
		"#EscribaNumero2": {
			"hintText": "L('x3183695581_traducir','Escriba numero de piso')"
		},
		"#Label11": {
			"title": "L('x1249164186_traducir','Hoy')"
		},
		"#ID_343581434": {
			"tipo": "L('x409735542','_sorry')"
		},
		"#ID_278574846": {
			"valor": "L('x4264937424','evento.valor')"
		},
		"estilo7_1": {
			"color": "#ee7f7e",
			"font": {
				"fontFamily": "Roboto-Medium",
				"fontSize": "14dp"
			}
		},
		"#Nivel2": {
			"text": "L('x2028852218_traducir','nivel 3')"
		},
		"#ID_86543972": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#widgetModal6": {
			"titulo": "L('x1867465554_traducir','MARCAS')",
			"cargando": "L('x1740321226_traducir','cargando ..')",
			"campo": "L('x3837495059_traducir','Marca del Item')",
			"seleccione": "L('x1021431138_traducir','seleccione marca')"
		},
		"#label": {
			"text": "L('x1236848526','88.888.888-8')"
		},
		"#Fecha5": {
			"text": "L('x27834329_traducir','fecha')"
		},
		"#iconoEntrar10": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "14dp"
			}
		},
		"estilo1": {
			"color": "#4d4d4d",
			"font": {
				"fontSize": "15dp"
			}
		},
		"#imagen": {
			"images": ["/images/iB1F901AEFA79EE1F3A05C41650B368A4.png", "/images/iBD0F891E767949293A5E319C70468E61.png", "/images/iE18490E3CB42EE21FA2CA39791E6EE07.png", "/images/i058F2A04E46A9135BF2007D00D84E610.png", "/images/iBC6A9F0C0BC8FF1BFAAE65D31F1594AB.png", "/images/iC9EDBE23CD4A8C3ACACF6159FAE7896F.png", "/images/i0337CBA53F3589380EFAC3F505EDAFF1.png", "/images/i553359F66995CFAE34B27D5D0D4C95E0.png", "/images/iBD1E17BE427423F6FA053D07C27B494E.png", "/images/iFE945B7620AFD4BCA1698FF05C97EEDF.png", "/images/i889E7951CC79C107C7467F773D851F53.png", "/images/i1DC1B37D94F0D03FB92F1569E2D04139.png", "/images/i69B0B3F2A0625ADD474DEFFDE5FB783D.png", "/images/i5CCD03CB61FDCAE6B11B30DC864BAE99.png", "/images/iDD8E3DB6271506344EFC962A6F6C907E.png", "/images/i4BE78669A95AFE52AA53FBDD4361D3E4.png", "/images/i29429A8CA3B30681CA2BB487FE2DDF26.png", "/images/i69D7DC35EF1C27805FA3D2751325A855.png", "/images/i7F617027CD4EC4189C4E8CED725823AC.png", "/images/i0C1216BE3263895DD5C0DCCE921413F2.png", "/images/i52CB6FB175FB303E19F6C3CEA9C8D640.png", "/images/iD51F4417351E1B84F7F9B808956CD544.png", "/images/i87AA4E2C8283824C531D6EA19313D656.png", "/images/iFC47B0B749489B593F6282CEBE075C92.png", "/images/iC37FBC50BE131D22685223C641DA5C9B.png", "/images/i10257EAFE36B26C91213946BD6B916BE.png", "/images/i64703C2596335309216F0F02CBE47A0A.png", "/images/i28054D3A888BD526A6B71FA0D72B495E.png", "/images/iE4678809AAB8130B87C7E9341736D608.png", "/images/i77B67FB15DAF2AA6C7DFAB3AEF96CC3D.png", "/images/i1352BFB08B32246214800F652EA481A9.png", "/images/iDE34B298EE1EF399563C371BB1352EE0.png", "/images/iF78AD53B709F20115D8977BF9F612522.png", "/images/iB9E2226298DF133710B12402166BE94F.png", "/images/iE89BCB17377B1DC1F77A2DE2512A28EC.png", "/images/iE8162669F36B070D6B7D99F53757E770.png", "/images/i642B66133CEB6E403BB536FBADA0E681.png", "/images/i634ADF5364D3A8C7F50C71FE6DD90A80.png", "/images/i9CFC507846CDD4330DF7FDF1C0B0C31E.png", "/images/iE9825E0C72E7F6D3C9F3E8C359F44947.png", "/images/i7E452776BF9CF2E5102B37DB75E66F36.png", "/images/iC926655337EBEE90D344237131E954A2.png", "/images/i8F4CE4FFC4978421CD759A09A64FD5DF.png", "/images/i10280424F1EE424A6A514F0F16858345.png", "/images/i00F9DCB834689DD92A6F5679DF478CEC.png", "/images/i3310017A13BD47BA4E4549FD12A7788A.png", "/images/iE77D1DCF0D78AED4D64B7BE4DEC18855.png", "/images/iF70C2EE509A92FCB43AF685DDD2B2FA2.png", "/images/iD68DDDF47F8B5A8C859DEE487442816E.png", "/images/i9C794F568EA9A9383A579EBC118ECE1C.png", "/images/i3D15430021DB8CCF5FC81650384162EA.png", "/images/iA9CA15D575EC6434CD632DD4B0458E02.png", "/images/i129B9502415F1FFEF9E8936317CEFF50.png", "/images/i54B57D779036FF5C5902278D3A9A329A.png", "/images/iFAD70BC931105FCA07FD8E0DAEAF4137.png", "/images/iC9AFD1E7FA9C0486C5424E816865BE8C.png", "/images/i2A3F902C97F8E816FBF43D6072C8EA38.png", "/images/i487CD84F81C016C6569519B588DF619F.png", "/images/iC217A8FE8F59CC255970BBB562470CC2.png", "/images/iD730909ABD133F91A1EFBC1B1E5C70DA.png"]
		},
		"#Caso": {
			"text": "L('x2564648986_traducir','caso')"
		},
		"#iconoCiudad8": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#iconoUbicacion9": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#fotos_requeridas": {
			"title": "L('x2950435588','fotos_requeridas')"
		},
		"#ValorObjeto2": {
			"text": "L('x3330880180_traducir','Valor objeto')"
		},
		"#ID_955104357": {
			"on": "L('x3312536354','inspector.d3')"
		},
		"#FechadeNacimiento2": {
			"text": "L('x3071871667_traducir','fecha de nacimiento')"
		},
		"#EMail": {
			"text": "L('x123080099_traducir','E-Mail')"
		},
		"#iconoComuna2": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#editarnivel": {
			"title": "L('x1294449476_traducir','editarnivel')"
		},
		"#ApellidoMaterno2": {
			"hintText": "L('x503134249_traducir','apellido materno')"
		},
		"#PlanosyMemoria": {
			"text": "L('x298238517_traducir','Planos y memoria de cálculo estrutural')"
		},
		"#EscribaNombre": {
			"hintText": "L('x3378665810_traducir','escriba nombre nivel')"
		},
		"#DireccinRiesgo": {
			"text": "L('x4067701965_traducir','Dirección Riesgo')"
		},
		"#Certificado": {
			"text": "L('x1235487166_traducir','Certificado de dominio vigente actualizado')"
		},
		"#Contrasea": {
			"text": "L('x3861721694_traducir','Contraseña')"
		},
		"#imagen3": {
			"images": ["/images/i4ECB85A2AE41ACB15653139F35BFA8C7.png", "/images/i4DB2E8AD733DFC95AF5E6A5115E117EE.png", "/images/iD4FE3BB66A3A734672CFBF9A4FE820B3.png", "/images/iD95E1371C4A9D4060E4C293FEBB2A371.png", "/images/i1846D1705B39A0DFD7CA331E2A624A7B.png", "/images/i1AAB9DCC791F6C8D5CBD86A69F2BD28A.png", "/images/iC18AD8B25C7E89544B26A56D72763ACF.png", "/images/i451D26CB7C617839FAAABBEE243EE91B.png", "/images/iBF02E0A9F57D34BA382B6B4851AEB549.png", "/images/iB810656D7547F7A2C967AC3DB0C7CAD4.png", "/images/iF173895CDE559CF4A789ECA310F324C1.png", "/images/iB13BBB34910D6E346D89A425DF9D9AA7.png", "/images/i09E757E6A96CDFD1A3071F75A86B9703.png", "/images/iB1B0726192E9AFA5B4393DD2789A8E73.png", "/images/i3BD062E17CB084738EC3C8544C4F562C.png", "/images/i0B25A07F8BA6A3D1155E5635855725E3.png", "/images/i51CD06F5D53967A813F0F7C4F8679F81.png", "/images/i57F0B2CBFC207F6540A08C8209B6461C.png", "/images/i6F4FBDF546B1311877A208695B53B1F5.png", "/images/i20546752F9D3EB6A38FD547BB45E2DA7.png", "/images/i997DFD2C59EEEC93E94CA8831CCFE464.png", "/images/i7A0053CD88CD6D72A30F33A67115B84F.png", "/images/i53F37CB31AA110AB8BF1BCA095F905FE.png", "/images/i6F4B0EAC06F1D5F21CF0ADB7E3036782.png", "/images/iD8AE92E691560F489DE642319547F8F9.png", "/images/iF8FD2CB3805AB4250C4E53D99093CA3C.png", "/images/iCA260B7119401A0D456373D95DAD7824.png", "/images/iC97BF367BA6481E3A5CEAEBFBD492506.png", "/images/iC9B3A576F9A65C4C9C208CFB562D3A8D.png", "/images/iCEF1D427EF5C147BC79A0D6848913C44.png"]
		},
		"#ID_1448197759": {
			"texto": "L('x4133911346','cap_max_tareas')"
		},
		"#ID_289019737": {
			"imagen": "L('x908735488','evento.foto')",
			"calidad": "L('x511942527_traducir','91')",
			"nueva": "L('x1112834923','640')",
			"camara": "L('x2899294158_traducir','trasera')"
		},
		"#iconoUbicacion12": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#StatusEsperando": {
			"text": "L('x2451619465_traducir','status: esperando')"
		},
		"#ID_402705466": {
			"valor": "L('x4264937424','evento.valor')"
		},
		"#EscribirNombre": {
			"hintText": "L('x4026340113_traducir','escribir nombre')"
		},
		"estilo4": {
			"color": "#4d4d4d",
			"font": {
				"fontSize": "16dp"
			}
		},
		"#Altomt": {
			"text": "L('x2760387473_traducir','Alto (mt)')"
		},
		"#Descripcin": {
			"text": "L('x4003849308_traducir','Descripción del siniestro')"
		},
		"#ID_1660608118": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#widgetModal4": {
			"titulo": "L('x767609104_traducir','RECINTOS')",
			"cargando": "L('x1831148736','cargando...')",
			"campo": "L('x382177638_traducir','Ubicación')",
			"seleccione": "L('x4060681104_traducir','recinto')"
		},
		"#ID_1086442521": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#PlanosdeArquitectura": {
			"text": "L('x1295614616_traducir','Planos de arquitectura (Plantas, elevaciones y cortes)')"
		},
		"desc": {
			"color": "#838383",
			"font": {
				"fontFamily": "Roboto-Medium",
				"fontSize": "11dp"
			}
		},
		"#Anchomt2": {
			"text": "L('x4100297395_traducir','Ancho (mt)')"
		},
		"#JohnAppleseed": {
			"text": "L('x1848621184_traducir','John Appleseed Ramirez')"
		},
		"estilo17": {
			"color": "#838383",
			"font": {
				"fontFamily": "Roboto-Regular",
				"fontSize": "12dp"
			}
		},
		"#ID_1254098827": {
			"imagen": "L('x908735488','evento.foto')",
			"calidad": "L('x511942527_traducir','91')",
			"nueva": "L('x1112834923','640')",
			"camara": "L('x2899294158_traducir','trasera')"
		},
		"#ID_216610160": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#MISTAREAS": {
			"title": "L('x333584130_traducir','MISTAREAS')"
		},
		"#seccionListadoRef2": {
			"headerTitle": "L('x3424088795_traducir','ref1')"
		},
		"#seccionListadoHoli": {
			"headerTitle": "L('x2819214932_traducir','holi1')"
		},
		"#ENROLAMIENTO6": {
			"title": "L('x2146494644_traducir','ENROLAMIENTO')"
		},
		"#seccionListadoHoy": {
			"headerTitle": "L('x1916403066_traducir','hoy')"
		},
		"#ID_858231116": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#ID_368258369": {
			"pantalla": "L('x1805186499_traducir','contenidos')"
		},
		"#widgetDpicker2": {
			"aceptar": "L('x1518866076_traducir','Aceptar')",
			"cancelar": "L('x2353348866_traducir','Cancelar')"
		},
		"#filaPicker3": {
			"title": "L('x318795834_traducir','Full time')"
		},
		"#ID_102643216": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#ID_673659791": {
			"imagen": "L('x908735488','evento.foto')",
			"calidad": "L('x511942527_traducir','91')",
			"nueva": "L('x1112834923','640')",
			"camara": "L('x2899294158_traducir','trasera')"
		},
		"#ID_1775974358": {
			"imagen": "L('x908735488','evento.foto')",
			"calidad": "L('x511942527_traducir','91')",
			"nueva": "L('x1112834923','640')",
			"camara": "L('x2899294158_traducir','trasera')"
		},
		"#ID_434452781": {
			"vertexto": "L('x2310355463','true')",
			"verprogreso": "L('x2001657581_traducir','false')"
		},
		"#ID_807562049": {
			"valor": "L('x1992209902_traducir','Seleccione entidad')"
		},
		"estilo8_1": {
			"color": "#ee7f7e",
			"font": {
				"fontFamily": "Roboto-Medium",
				"fontSize": "15dp"
			}
		},
		"#ENROLAMIENTO2": {
			"title": "L('x2146494644_traducir','ENROLAMIENTO')"
		},
		"#PrimeroDebemos": {
			"text": "L('x1692519842_traducir','Primero debemos verificar tu disponibilidad')"
		},
		"#Certificado2": {
			"text": "L('x584718974_traducir','Certificado de hipotecas y gravámenes actualizado')"
		},
		"fondoplomo": {
			"backgroundColor": "#a3a3a3",
			"font": {
				"fontSize": "12dp"
			}
		},
		"bt_guardar": {
			"color": "#ffffff",
			"font": {
				"fontSize": "18dp"
			}
		},
		"#iconoNuevo_dano": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "18dp"
			}
		},
		"#iconoUbicacion8": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#iconoEntrar2": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "16dp"
			}
		},
		"#ApellidoPaterno2": {
			"hintText": "L('x469026087_traducir','apellido paterno')"
		},
		"#iconoEntrar11": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "14dp"
			}
		},
		"#Nivel": {
			"text": "L('x267051884_traducir','nivel 2')"
		},
		"#ID_355046063": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#iconoEnviar_insp": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "15dp"
			}
		},
		"#RecuperoMaterial2": {
			"text": "L('x1050264796_traducir','Recupero material')"
		},
		"#Contactos": {
			"text": "L('x3065475174_traducir','Contactos')"
		},
		"#iconoComuna5": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#ID_1257536581": {
			"valor": "L('x4264937424','evento.valor')"
		},
		"bg_progreso_verde": {
			"backgroundColor": "#33cc7f",
			"font": {
				"fontSize": "12dp"
			}
		},
		"#widgetModalmultiple12": {
			"titulo": "L('x1866523485_traducir','Estruct. cubierta')",
			"cargando": "L('x1831148736','cargando...')",
			"hint": "L('x2460890829_traducir','Seleccione e.cubiertas')",
			"subtitulo": "L('x4011106049_traducir','Indique los tipos')"
		},
		"#Ciudad": {
			"text": "L('x2301231272_traducir','Ciudad')"
		},
		"#FechadeCompra": {
			"text": "L('x234257621_traducir','Fecha de compra')"
		},
		"estilo_critica": {
			"color": "#ee7f7e",
			"font": {
				"fontSize": "14dp"
			}
		},
		"estilo6": {
			"color": "#4d4d4d",
			"font": {
				"fontFamily": "Roboto-Medium",
				"fontSize": "16dp"
			}
		},
		"fondoazul": {
			"backgroundColor": "#2d9edb",
			"font": {
				"fontSize": "12dp"
			}
		},
		"titulos_datos_firma": {
			"color": "#ffffff",
			"font": {
				"fontFamily": "Roboto-Medium",
				"fontSize": "18dp"
			}
		},
		"#iconoUbicacion3": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#Das": {
			"text": "L('x2854409003_traducir','días')"
		},
		"#Anchomt": {
			"text": "L('x4100297395_traducir','Ancho (mt)')"
		},
		"#EscribirNivel": {
			"hintText": "L('x3229948791_traducir','escribir nivel 3')"
		},
		"#ID_1447737581": {
			"valor": "L('x4264937424','evento.valor')"
		},
		"#widgetBotonlargo": {
			"titulo": "L('x1321529571_traducir','ENTRAR')"
		},
		"#Nivel4": {
			"text": "L('x3867756121_traducir','nivel 4')"
		},
		"#ID_211976969": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#Domicilio": {
			"text": "L('x426093682_traducir','Domicilio')"
		},
		"estilo12": {
			"color": "#000000",
			"font": {
				"fontSize": "18dp"
			}
		},
		"#ID_1256506288": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#iconoCiudad5": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#widgetModal2": {
			"titulo": "L('x1384515306_traducir','TIPO DAÑO')",
			"cargando": "L('x1831148736','cargando...')",
			"campo": "L('x953397154_traducir','Tipo de Daño')",
			"seleccione": "L('x4123108455_traducir','seleccione tipo')"
		},
		"#HoradeInspeccion": {
			"text": "L('x1045762689_traducir','Hora de inspeccion')"
		},
		"#ID_916677064": {
			"imagen": "L('x908735488','evento.foto')",
			"calidad": "L('x511942527_traducir','91')",
			"nueva": "L('x1112834923','640')",
			"camara": "L('x2899294158_traducir','trasera')"
		},
		"#widgetModalmultiple2": {
			"titulo": "L('x1975271086_traducir','Estructura Soportante')",
			"cargando": "L('x1831148736','cargando...')",
			"hint": "L('x2898603391_traducir','Seleccione estructura')",
			"subtitulo": "L('x4011106049_traducir','Indique los tipos')"
		},
		"#ID_216469142": {
			"valor": "L('x4264937424','evento.valor')"
		},
		"#ID_244739260": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#IngreseCorreo": {
			"hintText": "L('x2452733176_traducir','ingrese correo')"
		},
		"#widgetMapa": {
			"label_a": "L('x3904355907','a')",
			"_bono": "L('x2764662954_traducir','Bono adicional:')"
		},
		"#iconoUbicacion11": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#ID_1068163802": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#widgetModal": {
			"titulo": "L('x3180818344_traducir','SELECCIONE REGION')",
			"campo": "L('x147780672_traducir','Region')",
			"seleccione": "L('x677075728_traducir','seleccione region')",
			"cargando": "L('x1831148736','cargando...')"
		},
		"#FonoMovil": {
			"text": "L('x3999754113_traducir','Fono movil')"
		},
		"#iconoFlecha3": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "6dp"
			}
		},
		"#iconoComuna9": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"rojo": {
			"color": "#ee7f7e",
			"font": {
				"fontWeight": "bold",
				"fontFamily": "Roboto-Medium",
				"fontSize": "16dp"
			}
		},
		"#widgetDpicker": {
			"aceptar": "L('x1518866076_traducir','Aceptar')",
			"cancelar": "L('x2353348866_traducir','Cancelar')"
		},
		"#Estimacinde": {
			"text": "L('x2770200964_traducir','Estimación de Meses')"
		},
		"#ID_1552521694": {
			"imagen3": "L('x1913188287','foto_original')",
			"nueva": "L('x1112834923','640')",
			"camara": "L('x1008027353_traducir','camara')"
		},
		"titulo_niveles1": {
			"color": "#8ce5bd",
			"font": {
				"fontFamily": "Roboto-Medium",
				"fontSize": "16dp"
			}
		},
		"#EscribirNivel3": {
			"hintText": "L('x702987842_traducir','escribir nivel 5')"
		},
		"#ID_9621809": {
			"color": "L('x2372642490_traducir','morado')"
		},
		"#ID_3974519": {
			"pantalla": "L('x2627320454','insp_cancelada')"
		},
		"#ID_1282712073": {
			"pantalla": "L('x887781275_traducir','detalle')"
		},
		"#ENROLAMIENTO4": {
			"title": "L('x2146494644_traducir','ENROLAMIENTO')"
		},
		"#ID_437586889": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#ID_675248659": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#widgetModalmultiple11": {
			"titulo": "L('x591862035_traducir','Pavimentos')",
			"cargando": "L('x1831148736','cargando...')",
			"hint": "L('x2600368035_traducir','Seleccione pavimentos')",
			"subtitulo": "L('x4011106049_traducir','Indique los tipos')"
		},
		"#itemListado3": {
			"_section": "L('x3690817388','manana_entrantes')"
		},
		"#ID_1998547652": {
			"pantalla": "L('x3181120844_traducir','hayalguien')"
		},
		"#widgetFotochica2": {
			"top": "L('x4108050209','0')"
		},
		"#ID_1657448720": {
			"bono": "L('x1979562742','tareas[0].bono')"
		},
		"#ID_1997004832": {
			"valor": "L('x2196553667','insp_niveles[0].nombre')"
		},
		"#iconoCiudad9": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#ID_405426310": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#seccionListadoHoyentrantes": {
			"headerTitle": "L('x3166667470','hoy_entrantes')"
		},
		"#ID_203756117": {
			"imagen": "L('x908735488','evento.foto')",
			"calidad": "L('x511942527_traducir','91')",
			"nueva": "L('x1112834923','640')",
			"camara": "L('x2899294158_traducir','trasera')"
		},
		"#RepetirTelfono": {
			"text": "L('x3573009323_traducir','Repetir teléfono')"
		},
		"#ID_1014068730": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#iconoComuna11": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#ID_1506439673": {
			"desde": "L('x828196665','desde_horas[0]')",
			"hasta": "L('x2081216563','hasta_horas[0]')"
		},
		"#ID_615889807": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#seccionListadoManana": {
			"headerTitle": "L('x4074624282_traducir','manana')"
		},
		"#iconoComuna4": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#AgregueCada": {
			"text": "L('x246910460_traducir','Agregue cada contenido movible dañado')"
		},
		"#iconoCiudad": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#Label12": {
			"title": "L('x2445898609_traducir','Perfil')"
		},
		"#itemListado4": {
			"_section": "L('x2345059420','fecha_entrantes')"
		},
		"#widgetHeader": {
			"titulo": "L('x1574293037_traducir','Editar Disponibilidad')"
		},
		"#ID_1358966454": {
			"imagen": "L('x908735488','evento.foto')",
			"calidad": "L('x511942527_traducir','91')",
			"nueva": "L('x1112834923','640')",
			"camara": "L('x2899294158_traducir','trasera')"
		},
		"#ItemsdeDao": {
			"text": "L('x649484684_traducir','Items de daño')"
		},
		"#widgetModalmultiple": {
			"titulo": "L('x1313568614_traducir','Destino')",
			"hint": "L('x2017856269_traducir','Seleccione destino')",
			"subtitulo": "L('x1857779775_traducir','Indique los destinos')"
		},
		"#widgetBarra5": {
			"titulo": "L('x2146494644_traducir','ENROLAMIENTO')"
		},
		"#DATOS_BASICOS": {
			"title": "L('x3046757478_traducir','DATOS BASICOS')"
		},
		"estilo10": {
			"color": "#ffffff",
			"font": {
				"fontWeight": "bold",
				"fontSize": "16dp"
			}
		},
		"#seccionListadoAyer": {
			"headerTitle": "L('x1602196311_traducir','ayer')"
		},
		"#Evaluacinde": {
			"text": "L('x2945294809_traducir','Evaluación de daños generales')"
		},
		"#filaPicker2": {
			"title": "L('x3964605301_traducir','Part time')"
		},
		"#ID_439099973": {
			"valor": "L('x3644411302','marca[0].nombre')"
		},
		"#ID_1213709362": {
			"monito": "L('x2001657581_traducir','false')"
		},
		"#ID_1028762945": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#widgetPelota5": {
			"letra": "L('x1342839628','V')"
		},
		"fondoblanco": {
			"backgroundColor": "#ffffff",
			"font": {
				"fontSize": "12dp"
			}
		},
		"estilo8_3": {
			"color": "#ee7f7e",
			"font": {
				"fontFamily": "Roboto-Medium",
				"fontSize": "9dp"
			}
		},
		"#ID_238913056": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#Escriba": {
			"hintText": "L('x4236775069_traducir','escriba')"
		},
		"#widgetSininternet": {
			"titulo": "L('x2828751865_traducir','¡ESTAS SIN CONEXION!')",
			"mensaje": "L('x1855928898_traducir','No puedes ver las tareas de emergencias de hoy')"
		},
		"#ID_1029806223": {
			"imagen": "L('x908735488','evento.foto')",
			"calidad": "L('x511942527_traducir','91')",
			"nueva": "L('x1112834923','640')",
			"camara": "L('x2899294158_traducir','trasera')"
		},
		"#EscribirDireccion": {
			"hintText": "L('x3893799545_traducir','escribir direccion')"
		},
		"#IndiqueAo2": {
			"hintText": "L('x2340676069_traducir','Indique año')"
		},
		"#ID_376078675": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#campo": {
			"hintText": "L('x4108050209','0')"
		},
		"#ID_1319666298": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#ID_1726224660": {
			"pantalla": "L('x3681655494_traducir','caracteristicas')"
		},
		"#ID_582453330": {
			"pantalla": "L('x3181120844_traducir','hayalguien')"
		},
		"#ID_1522324099": {
			"imagen": "L('x908735488','evento.foto')",
			"calidad": "L('x511942527_traducir','91')",
			"nueva": "L('x1112834923','640')",
			"camara": "L('x2899294158_traducir','trasera')"
		},
		"#ValorObjeto": {
			"text": "L('x3330880180_traducir','Valor objeto')"
		},
		"#MANTENERPARA": {
			"text": "L('x2905032041_traducir','MANTENER PARA INICIAR')"
		},
		"#widgetModalmultiple8": {
			"titulo": "L('x1975271086_traducir','Estructura Soportante')",
			"cargando": "L('x1831148736','cargando...')",
			"hint": "L('x2898603391_traducir','Seleccione estructura')",
			"subtitulo": "L('x4011106049_traducir','Indique los tipos')"
		},
		"#iconoCamara2": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "36dp"
			}
		},
		"#ID_290035855": {
			"seleccione": "L('x3621251639_traducir','unidad')"
		},
		"#RecuperoMaterial": {
			"text": "L('x1050264796_traducir','Recupero material')"
		},
		"#ID_846484832": {
			"valor": "L('x1348260107','unidad_medida[0].nombre')"
		},
		"#SeleccioneDas": {
			"text": "L('x2509905523_traducir','Seleccione días disponibles')"
		},
		"#iconoEntrar9": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "14dp"
			}
		},
		"#icono2": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "15dp"
			}
		},
		"#ESTRUCTURASOPORTANTE": {
			"text": "L('x2268209570_traducir','ESTRUCTURA SOPORTANTE')"
		},
		"#Label3": {
			"text": "L('x1684325040','?')",
			"title": "L('x3567030427_traducir','TAREAS ENTRANTES')"
		},
		"#ID_875644790": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#editar_contacto": {
			"title": "L('x1044748843_traducir','editar contacto')"
		},
		"#NO": {
			"text": "L('x3376426101_traducir','NO')"
		},
		"#widgetModalmultiple5": {
			"titulo": "L('x2266302645_traducir','Cubierta')",
			"cargando": "L('x1831148736','cargando...')",
			"hint": "L('x2134385782_traducir','Seleccione cubiertas')",
			"subtitulo": "L('x4011106049_traducir','Indique los tipos')"
		},
		"#ID_688541926": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#Datos": {
			"text": "L('x2421821072_traducir','Datos')"
		},
		"#widgetBotonlargo2": {
			"titulo": "L('x1524107289_traducir','CONTINUAR')"
		},
		"#Definirde2": {
			"text": "L('x1350052795_traducir','Definir % de daños')"
		},
		"#ID_1197339644": {
			"valor": "L('x4264937424','evento.valor')"
		},
		"bg_progreso_naranjo": {
			"backgroundColor": "#f2a566",
			"font": {
				"fontSize": "12dp"
			}
		},
		"fondonaranjo": {
			"backgroundColor": "#fcbd83",
			"font": {
				"fontSize": "12dp"
			}
		},
		"#Ao": {
			"hintText": "L('x300188755_traducir','año')"
		},
		"#Version": {
			"text": "L('x1752567336_traducir','version: 07.05')"
		},
		"#ID_1732356249": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#CantidadItems": {
			"text": "L('x3754629878_traducir','Cantidad items')"
		},
		"#INSPPORSUBIR": {
			"text": "L('x2539507736_traducir','INSP. POR SUBIR')"
		},
		"#icono9": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "15dp"
			}
		},
		"#ApellidoPaterno": {
			"text": "L('x3279325126_traducir','Apellido Paterno')"
		},
		"#AgregaCadaNivel": {
			"text": "L('x1411564115_traducir','Agrega cada nivel')"
		},
		"#imagen2": {
			"images": ["/images/i21578740E736331F7BDC9D452BD712C9.png", "/images/iEB558FE382408A66B62F79BCD08E87DE.png", "/images/iE4617B7080958F45BF2C192CC042382D.png", "/images/i9290A0A895DCA701657AA3A2701BC5B1.png", "/images/iC844F1FD37D49F099C8E19CDD3168A41.png", "/images/iD7DACAF52A6FE787F42E337F896ED7E5.png", "/images/i77B96376A21E9E530BF99DEF0E191F24.png", "/images/i1A8982F638B1931C2A37ED5E826C17C0.png", "/images/i88E908455A225B9390E3678E9C19FBA2.png", "/images/iE9355204576B1FD1BB3F1F0E435BFF6D.png", "/images/i0E4650BFDDB114F81A51C600521AC2D7.png", "/images/i867483CAEDC8D77255A7F25A15625CA5.png", "/images/i2175C7E985066E9DBB5FD1E204BDED5D.png", "/images/i19F89DC20905F566DA7CCDBE900B1828.png", "/images/i6755383116D1A5AD862D47B6D5554641.png", "/images/i1391131C784CF99686D19D65AC20986E.png", "/images/i40A27D206CA23815C331AB71422629F2.png", "/images/i38E6B9D378262B4EF35387DC47BBF31D.png", "/images/i988F7B1016AF1A4927CD7ED9AE8AB990.png", "/images/i405EC5CB58F536363712327BE6376BB1.png", "/images/i46769D88018418464C9F4B24D384CB8B.png", "/images/i3F6BAF33AC1E4011205917185EA98CAD.png", "/images/i42D15D48B2ED837F7903FE534A06494B.png", "/images/iBC7C1AA388608962A1F7B8C61BD6DA82.png", "/images/iF09D8144E898AB6F35D2FF363EE98F9E.png", "/images/i077D23D9EA5866B0EF5E25614919665F.png", "/images/iA83DD447D387E7698AB46331B8A8D019.png", "/images/i21932839113ADBF836C5D8C8B4B11AEB.png", "/images/i9BDA9EE2558DFE3F2125992BECA4278D.png", "/images/i9AC6E8C97F63FEB0A46CCD655AC44431.png", "/images/iE28ABABF9CABE7255711922BBF55B983.png", "/images/i0A41ED8B1E443429769FFB49137003C2.png", "/images/i388C5A5B6FEC1AAA9E0FED2BE6842A35.png", "/images/iBB18FBADFC2D527AD2709E8652AAFD5A.png", "/images/i10063715BD30D221415EF53335926168.png", "/images/i359C4AC08A6FB6D5912E5ECA8ABBB00A.png", "/images/i0E59AE058F689EF3F4FAD9FDDF470974.png", "/images/i54701C93E11CA7BAF12DF89AABF2AE04.png", "/images/i9AE08E1C51AF160F89D84F868261FCFD.png", "/images/i585BBC1586441528C908AD41EB8EBDBD.png", "/images/i10BF85AEDE9F2E6087E8EECF917665A8.png", "/images/iE50DF32C64581FCE5856C9454B2D58E2.png", "/images/iBFDCFF2749FF3ACE5A75BF05C2C1E25C.png", "/images/i89096AB8DBE818E384384C292C89811F.png", "/images/i0BEBD7212FCEADD8E81C2812FD68C36D.png", "/images/i1B954A4FDCFE0860F6D3EBC4D01D2F19.png", "/images/i304ED678B67AD49378DB6228C609BE45.png", "/images/iBEB7E7D434C1667889663859EAE75D92.png", "/images/iF714D356682197276705B90C40BA8A03.png", "/images/iA1B6778946A3A0358E419143E8081D2B.png", "/images/i00760138B01D3550976723B1B225C943.png", "/images/iBAAC1AF775CEE1B716B575455DB68FDC.png", "/images/i3CD6AA1281B7B4ED5F37ED76FCC2A808.png", "/images/i20769EDB2A387AD7E63BFEE1ADB6E01A.png", "/images/iDAED2AF86C310E481C7409143758ECB6.png", "/images/i9B1B0BD5A88BDAB459D66F68B80309F5.png", "/images/i0DE060270629150E8A87173EDB8B79F7.png", "/images/i380DF6EF85EB2185DE1A501FEE66371A.png", "/images/iA152F49A26EC92286520C265AABD8CFC.png", "/images/i154CF2D0DF49584435B369E991104CD0.png", "/images/iB6A8B660E26087802754C713C196B739.png", "/images/iD0A8DD7F8257D87CADD4557D2DDE27A0.png", "/images/iF0D35F95D8CF565D84F4B2EABC07C0C3.png", "/images/iAC8C1B1E912288D6EF52DC4C71B79944.png", "/images/i396F93C0F37490C405609068F53808DE.png", "/images/i28D1EA7CF68179295028D9F1DC787292.png", "/images/iF3F5D57B22CF688DF19FE14CCD909402.png", "/images/i399EE8FDF222FDA603DF0C82E0BCCFAC.png", "/images/i09C63C089B2E569672618ED5FC69504C.png", "/images/i6EA1D3660FE93F229A4DD0E034A728EB.png", "/images/iDFEBE4EC019BCE94F6EC5853A3BFABF5.png", "/images/iAC27C6E0D5FAF36E89B0E75BD07AEB87.png", "/images/i273522DAAED644BC07219E8653954EE3.png", "/images/i86B7E95443CEBED206372766EAF4F59C.png", "/images/i7E06C884017083155A6857A40DCF4DE3.png", "/images/i691DBFC540A724998A2353C7D6DC199F.png", "/images/iD182D4C677A7FEF7B7F9D2021F901A9C.png", "/images/i35A05BBCDD161E2A6C0C478F703901C0.png", "/images/iCF6F841564C2B00839FA0338556EE25E.png", "/images/iE34B2EECFDC74FC20A5DEA25E204DB7A.png", "/images/i15B71869941D9D056D2771715265227F.png", "/images/i8A0B707451CCBE122A5B28F640716AF1.png", "/images/i88CB92AA112DA9B4D3913374D31E5489.png", "/images/i7FDF4568F230AA5A4D3B701708D1E8D6.png", "/images/i69B8A04A79F91820581E4D3708B7D003.png", "/images/i55288AB1A85C7038BCA8AD843ADF586F.png", "/images/i57C1B3043D0424531659720F9C32B1C2.png", "/images/iDAA2A0A70B3960D02C55DDF038216353.png", "/images/iECBF5EDE5417D57D71BD067290909C22.png", "/images/i10CCB6F0BFED76841F1607FA34C062B1.png", "/images/iF8B6DC3340FEC020058F7C743ADA0098.png", "/images/i7F73828276786D80F13715BB5BFB4CCC.png", "/images/i68421A3D08C3B75227181E0EC9EBCE42.png", "/images/i18ACDDC89C81835BFB74D3A0E71D8C10.png", "/images/i5A8E6E8D34ABB47533165D506B7E1462.png", "/images/i9AD38C8643DBCC036D0D226508F2B971.png", "/images/iA7B0C6D62FD940727B9D5DBBD4165324.png", "/images/i51313033EE92F99494C0CEF93F77F8D9.png", "/images/i26C8BDFC1DEA0C67358C293A353C327B.png", "/images/iF086F6FE7A1868188FD09EE6FA03485B.png", "/images/i9C4EAA87C9F69FE64BE240DAE158CF0E.png", "/images/i47EFAC4DEC72BFED1EAD8D5621ECF913.png", "/images/i96D645D648D27F12B2E4F54E9503E8DF.png", "/images/i6394513CA228CCB57ED602297C9422BC.png", "/images/i6CD686DC5A11FB94CBB2A09FC90DB6EF.png", "/images/iEBD4EE843BEEB6E225C25387B7DCF30E.png", "/images/iB0DCEF19023ADDE9B38C97AF31540D4D.png", "/images/i5907CCF7C26ECC19EAD231C90A4FAB9D.png", "/images/i8143DE4F0A77BC1F4111FF6A02EED95E.png", "/images/iDD2C93752E1169F8D71892A477C85A11.png", "/images/i661E5F5296ABB7AB4B0E5D236E6C7630.png", "/images/iCCB062F45E0D9D917C617FDB2EA1E357.png", "/images/i7DF846739C1FD71098DE3902990DA8A9.png", "/images/iAD45960BC2841EC6E2D86283A2F33E96.png", "/images/i6AF96FB60F20F611E16F44E4A4C366B4.png", "/images/iBBA0A82C21EA239F79E171AC51EA7B95.png", "/images/i2F569FBCA8D42EB735DED2831286DC66.png", "/images/iE02AFE3817F8B969AEAACF7456E25AB5.png", "/images/iAE28EA1830BF41F55AD84813E8F1D056.png", "/images/i870DC5E31CA825DAEE1AD00C6FB1D68A.png"]
		},
		"#widgetModalmultiple6": {
			"titulo": "L('x3327059844_traducir','Entrepisos')",
			"cargando": "L('x1831148736','cargando...')",
			"hint": "L('x2146928948_traducir','Seleccione entrepisos')",
			"subtitulo": "L('x4011106049_traducir','Indique los tipos')"
		},
		"#NO9": {
			"text": "L('x3376426101_traducir','NO')"
		},
		"#AceptaDesistir": {
			"text": "L('x4144438243_traducir','Acepta desistir la realización de esta inspección')"
		},
		"#HOY": {
			"title": "L('x3835609072_traducir','HOY')"
		},
		"#ID_1072199175": {
			"valor": "L('x4264937424','evento.valor')"
		},
		"#NO2": {
			"text": "L('x3376426101_traducir','NO')"
		},
		"#iconoCiudad11": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#INSTALACIONES": {
			"text": "L('x4060777286_traducir','INSTALACIONES')"
		},
		"progreso": {
			"color": "#ffffff",
			"font": {
				"fontWeight": "bold",
				"fontFamily": "Roboto-Medium",
				"fontSize": "14dp"
			}
		},
		"#ID_1298536493": {
			"pantalla": "L('x3294048930_traducir','documentos')"
		},
		"#iconoUbicacion6": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"bg_progreso_morado": {
			"backgroundColor": "#9e8ff2",
			"font": {
				"fontSize": "12dp"
			}
		},
		"#DescribaelSiniestro": {
			"hintText": "L('x2246046648_traducir','Describa el siniestro')"
		},
		"fondoplomo2": {
			"backgroundColor": "#e6e6e6",
			"font": {
				"fontSize": "12dp"
			}
		},
		"#EscribirNivel2": {
			"hintText": "L('x1591840468_traducir','escribir nivel 4')"
		},
		"#ID_1071782282": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#seccionListadoMananaentrantes": {
			"headerTitle": "L('x3690817388','manana_entrantes')"
		},
		"#EstsDispuesto": {
			"text": "L('x1110802308_traducir','¿Estás dispuesto a viajar fuera de tu ciudad?')"
		},
		"estilo8_4": {
			"color": "#a0a1a3",
			"font": {
				"fontFamily": "Roboto-Medium",
				"fontSize": "15dp"
			}
		},
		"estilo2": {
			"color": "#a0a1a3",
			"font": {
				"fontFamily": "Roboto-Light",
				"fontSize": "12dp"
			}
		},
		"#IndiqueAo": {
			"hintText": "L('x2340676069_traducir','Indique año')"
		},
		"#documentos": {
			"title": "L('x515385654_traducir','documentos')"
		},
		"#ID_1704506881": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#ID_327747054": {
			"texto": "L('x3334290504','tarea_inspeccionar')"
		},
		"#widgetBarra2": {
			"titulo": "L('x2578278336_traducir','NUEVO NIVEL')",
			"textoderecha": "L('x2943883035_traducir','Guardar')",
			"colortextoderecha": "L('x3992191548_traducir','azul')"
		},
		"#widgetHeader6": {
			"titulo": "L('x1291961897_traducir','PARTE 5: Contactos')"
		},
		"#Construccin": {
			"text": "L('x1380747035_traducir','Construcción Anexa')"
		},
		"titulo_contenido": {
			"color": "#8bc9e8",
			"font": {
				"fontSize": "16dp"
			}
		},
		"#nuevodano": {
			"title": "L('x1387087821_traducir','nuevodano')"
		},
		"#CantidadItems2": {
			"text": "L('x3754629878_traducir','Cantidad items')"
		},
		"estilo2_1": {
			"color": "#a0a1a3",
			"font": {
				"fontFamily": "Roboto-Light",
				"fontSize": "15dp"
			}
		},
		"desistirtexto": {
			"color": "#ffffff",
			"font": {
				"fontSize": "14dp"
			}
		},
		"#Direccion": {
			"text": "L('x4085563029_traducir','direccion')"
		},
		"#LaDireccion": {
			"text": "L('x1242435453_traducir','La direccion es correcta?')"
		},
		"#iconoPerfil": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "16dp"
			}
		},
		"#editarcontenido": {
			"title": "L('x3363414796_traducir','editarcontenido')"
		},
		"#FechadeCompra2": {
			"text": "L('x234257621_traducir','Fecha de compra')"
		},
		"#widgetBarra3": {
			"titulo": "L('x902724175_traducir','EDITAR NIVEL')",
			"textoderecha": "L('x2943883035_traducir','Guardar')"
		},
		"fondocafe": {
			"backgroundColor": "#a5876d",
			"font": {
				"fontSize": "12dp"
			}
		},
		"#widgetHeader5": {
			"titulo": "L('x3752633607_traducir','PARTE 4: Disponibilidad de trabajo')"
		},
		"#Contenido": {
			"text": "L('x1381415900_traducir','Contenido')"
		},
		"#TERMINACIONES": {
			"text": "L('x2474326379_traducir','TERMINACIONES')"
		},
		"#Largomt": {
			"text": "L('x2558236558_traducir','Largo (mt)')"
		},
		"#ID_1189960264": {
			"vertexto": "L('x2310355463','true')",
			"verprogreso": "L('x2001657581_traducir','false')"
		},
		"#ID_611022613": {
			"pantalla": "L('x1828862638_traducir','basicos')"
		},
		"#ID_861542679": {
			"nivel1": "L('x3264699902','pais.label_nivel1')"
		},
		"#ID_1252135805": {
			"ruta": "L('x2310355463','true')",
			"externo": "L('x2310355463','true')",
			"ciudad": "L('x1526661684','info.ciudad_pais')",
			"longitud": "L('x2465019777','tareas[0].lon')",
			"comuna": "L('x4190055040','info.comuna')",
			"latitud": "L('x4044153717','tareas[0].lat')",
			"direccion": "L('x1199566094','info.direccion')",
			"distancia": "L('x1784316920','info.distancia')",
			"tipo": "L('x2512664346_traducir','ubicacion')"
		},
		"#widgetBotonlargo7": {
			"titulo": "L('x4058536315_traducir','ENVIAR FORMULARIO')"
		},
		"#ID_1938929577": {
			"pantalla": "L('x3772634211_traducir','siniestro')"
		},
		"#widgetHeader7": {
			"titulo": "L('x4235921061_traducir','PARTE 6: Experiencia de trabajo')"
		},
		"#iconoCiudad4": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#REGISTRARSE": {
			"text": "L('x2745628043_traducir','REGISTRARSE')"
		},
		"#widgetFotochica3": {
			"caja": "L('x2889884971_traducir','45')",
			"top": "L('x4108050209','0')"
		},
		"#ID_1096398442": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#ID_793809866": {
			"color": "L('x563962669_traducir','rosado')"
		},
		"estilo6_1": {
			"color": "#848484",
			"font": {
				"fontFamily": "Roboto-Bold",
				"fontSize": "14dp"
			}
		},
		"#CancelandoInspeccin": {
			"text": "L('x1906634265_traducir','Cancelando Inspección, espere...')"
		},
		"#widgetPregunta": {
			"titulo": "L('x1951816285_traducir','¿PUEDE CONTINUAR CON LA INSPECCION?')",
			"si": "L('x369557195_traducir','SI puedo continuar')",
			"texto": "L('x2522333127_traducir','Si está el asegurado en el domicilio presione SI para continuar')",
			"no": "L('x3892244486_traducir','NO se pudo realizar la inspección')"
		},
		"#iconoComuna": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#widgetBarra": {
			"titulo": "L('x3782399344_traducir','CONTACTO')",
			"textoderecha": "L('x2943883035_traducir','Guardar')"
		},
		"#ID_1756236367": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#ElSeguroEst": {
			"text": "L('x4137765611_traducir','¿El seguro está asociado a un Crédito Hipotecario?')"
		},
		"estilo5": {
			"color": "#808080",
			"font": {
				"fontSize": "16dp"
			}
		},
		"#widgetHeader2": {
			"titulo": "L('x31606107_traducir','Editar Disponibilidad de trabajo')"
		},
		"#ID_237242880": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#iconoCorreo": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "12dp"
			}
		},
		"#TomarFotosde2": {
			"text": "L('x2601646010_traducir','Tomar fotos de Recinto')"
		},
		"#iconoEntrar8": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "14dp"
			}
		},
		"#FonoFijo": {
			"text": "L('x3136609939_traducir','Fono fijo')"
		},
		"desistir": {
			"backgroundColor": "#adadad",
			"opacity": 0.9,
			"font": {
				"fontSize": "12dp"
			}
		},
		"#ID_1395628503": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#ID_1877908171": {
			"ruta": "L('x2310355463','true')",
			"ciudad": "L('x1526661684','info.ciudad_pais')",
			"destino_longitud": "L('x2465019777','tareas[0].lon')",
			"origen_latitud": "L('x2063034443','inspector.lat_dir')",
			"destino_latitud": "L('x4044153717','tareas[0].lat')",
			"origen_longitud": "L('x1789756952','inspector.lon_dir')",
			"comuna": "L('x4190055040','info.comuna')",
			"direccion": "L('x1199566094','info.direccion')",
			"distancia": "L('x1784316920','info.distancia')",
			"tipo": "L('x2403206383_traducir','origen')"
		},
		"estilo3": {
			"color": "#838383",
			"font": {
				"fontFamily": "Roboto-Medium",
				"fontSize": "12dp"
			}
		},
		"#NO4": {
			"text": "L('x3376426101_traducir','NO')"
		},
		"#Telefono": {
			"text": "L('x3253144191_traducir','telefono')"
		},
		"#itemListado2": {
			"_section": "L('x3166667470','hoy_entrantes')"
		},
		"#widgetModalmultiple7": {
			"titulo": "L('x1219835481_traducir','Muros / Tabiques')",
			"cargando": "L('x1831148736','cargando...')",
			"hint": "L('x2879998099_traducir','Seleccione muros')",
			"subtitulo": "L('x4011106049_traducir','Indique los tipos')"
		},
		"#ID_1121553289": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#NombreDelItem2": {
			"text": "L('x3109184006_traducir','Nombre del item')"
		},
		"#Comuna": {
			"text": "L('x2043784063_traducir','Comuna')"
		},
		"#ID_324967777": {
			"seleccione": "L('x3621251639_traducir','unidad')"
		},
		"#widgetBotonlargo6": {
			"titulo": "L('x1524107289_traducir','CONTINUAR')"
		},
		"estilo11_2": {
			"color": "#ffacaa",
			"font": {
				"fontWeight": "bold",
				"fontSize": "15dp"
			}
		},
		"#ID_1621227243": {
			"color": "L('x902017363_traducir','azul')"
		},
		"#seccionListadoRef": {
			"headerTitle": "L('x3424088795_traducir','ref1')"
		},
		"#ID_1059959544": {
			"data": "L('x1369121172_traducir','datos')"
		},
		"#ExistenOtros": {
			"text": "L('x607758518_traducir','¿Existen otros seguros por la misma materia siniestrada?')"
		},
		"#DescribaelDao2": {
			"hintText": "L('x397976520_traducir','Describa el daño')"
		},
		"#RUT": {
			"hintText": "L('x1001594672_traducir','RUT')",
			"text": "L('x1001594672_traducir','RUT')"
		},
		"#ID_687238468": {
			"valor": "L('x4264937424','evento.valor')"
		},
		"#iconoComuna6": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "9dp"
			}
		},
		"#widgetBotonlargo4": {
			"titulo": "L('x1524107289_traducir','CONTINUAR')"
		},
		"#ID_1282535710": {
			"texto": "L('x987675317_traducir','cerrar')",
			"estilo": "L('x977382917_traducir','fondorojo')",
			"estado": "L('x450215437','2')",
			"vertexto": "L('x2310355463','true')",
			"verprogreso": "L('x2001657581_traducir','false')"
		},
		"#ID_288509584": {
			"vertexto": "L('x2310355463','true')",
			"verprogreso": "L('x2001657581_traducir','false')"
		},
		"#iconoCritico": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "14dp"
			}
		},
		"#Nivel6": {
			"text": "L('x2442032847_traducir','nivel 5')"
		},
		"#ID_789484083": {
			"texto": "L('x2695983055_traducir','PRESIONE PARA ACEPTAR')",
			"estilo": "L('x2379090022_traducir','fondoverde')",
			"estado": "L('x2212294583','1')",
			"vertexto": "L('x2310355463','true')",
			"verprogreso": "L('x2001657581_traducir','false')"
		},
		"#Nombre": {
			"text": "L('x1027380240_traducir','Nombre')"
		},
		"#iconoGirar_camara": {
			"font": {
				"fontFamily": "beta1",
				"fontSize": "24dp"
			}
		}
	}
};
exports.fontello = {
	"adjust": {
		"CODES": {
			"auto": "\uE80b",
			"entrar": "\uE81b",
			"emergencias": "\uE819",
			"registrarse": "\uE827",
			"continuar": "\uE812",
			"info": "\uE821",
			"cerrar": "\uE80e",
			"salir_insp": "\uE828",
			"hoy": "\uE820",
			"check": "\uE80f",
			"agregar_item": "\uE80a",
			"9": "\uE808",
			"girar_camara": "\uE81d",
			"llamar": "\uE822",
			"volver": "\uE82a",
			"cruz": "\uE814",
			"1": "\uE800",
			"hora": "\uE81f",
			"ciudad": "\uE810",
			"3": "\uE802",
			"flecha": "\uE82b",
			"7": "\uE806",
			"4": "\uE803",
			"perfil": "\uE817",
			"cancelar_tarea": "\uE80d",
			"6": "\uE805",
			"mistareas": "\uE823",
			"historial": "\uE81e",
			"8": "\uE807",
			"entradas": "\uE81a",
			"caso": "\uE815",
			"ubicacion": "\uE829",
			"2": "\uE801",
			"refresh": "\uE826",
			"enviar_insp": "\uE81c",
			"telefono": "\uE818",
			"nuevo_dano": "\uE824",
			"comuna": "\uE811",
			"critico": "\uE813",
			"5": "\uE804",
			"camara": "\uE80c",
			"10": "\uE809",
			"correo": "\uE816"
		},
		"POSTSCRIPT": "beta1",
		"TTF": "/Applications/CreadorOPEN/CreadorOPEN.app/Contents/MacOS/webapps/ROOT/WEB-INF/lucee/temp/_font/fontello-85cc3601/font/beta1.ttf"
	}
};