**Estructura Carpetas**
 
-   **/backend-node**  
    Contiene el código NodeJS que se encuentra actualmente en Amazon. Dentro de esa carpeta hay un archivo llamado "Manual Backend.pdf" que explica en detalle su contenido.  
      
    
-   **/android/cliente**  
    Contiene el código del proyecto Appcelerator Titanium correspondiente a la versión  Android de la aplicación U-Adjust SCL.  
    Dentro hay un archivo llamado "**Manual Android Cliente.pdf**" con sus referencias respectivas.  
      
    
-  **/ios/cliente**
    Contiene el código del proyecto Appcelerator Titanium correspondiente a la versión iPhone de la aplicación U-Adjust SCL.  
    Dentro hay un archivo llamado "**Manual iOS Cliente.pdf**" con sus referencias respectivas.  
      
    
-  **/android/inspector**  
    Contiene el código del proyecto Appcelerator Titanium correspondiente a la versión Android de la aplicación U-Adjust (inspector).  
    Dentro hay un archivo llamado "**Manual Android Inspector.pdf**" con sus referencias respectivas.  
      
    
-  **/ios/inspector**  
    Contiene el código del proyecto Appcelerator Titanium correspondiente a la versión iPhone de la aplicación U-Adjust (inspector).  
    Dentro hay un archivo llamado "**Manual iOS Inspector.pdf**" con sus referencias respectivas.