var express = require('express');
var router = express.Router();
    path = require("path");
// Se definen las rutas ocupadas para la documentacion.
router.get('/api/', function(req, res, next) {
  res.redirect('/');
});
router.get('/segured/', function(req, res, next) {
  res.redirect('/');
});
router.get(['/*'], function(req, res, next) {
  console.log('URL solicitada', req.url, 'pametros', req.query);
  switch (req.url) {
    case "/":
      res.sendFile(path.resolve(`doc/en_US/index.html`));
    break;
    case "":
      res.sendFile(path.resolve(`doc/en_US/index.html`));
    break;
    case "api":
      res.redirect('/');
    break;
    case "api/":
      res.redirect('/');
    break;
    case "segured":
      res.redirect('/');
    break;
    case "segured/":
      res.redirect('/');
    break;
    case '/api/resetearTareas?q=uadjust':
      console.log(req.query.q);
      require('wait.for').launchFiber(require('../models/v1').resetearTareas, req, res);
    break;
    default:
      res.sendFile(path.resolve(`doc/en_US/${req.url}`));
    break;
  }
});
module.exports = router;
