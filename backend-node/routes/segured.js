/**
 * Funciones de servicio de comunicacion entre segured y servicio backend u-adjust.
 *
 * @module segured
 * @requires moment
 * @requires wait.for
 * @requires async
 * @requires underscore
 */

var express = require('express'),
    wait = require('wait.for');
var router = express.Router();
var segured = require('../models/segured');

// Importante: al usar wait.launchFiber lo que se hace es crear una fibra para el usuario, esto es importante ya que node es 1 thread, al usar fibers,
// lo que hace es dividir ese thread en muchas fibras.

  /**
   * Consulta los paises con sus datos necesarios.
   *
   * @name consultarPaises
   * @method
   * @returns {object} resp = {error: [0,400,500], mensaje: "texto de respuesta"}, paises = [].
   */
  router.post('/consultarPaises', function(req, res, next){
    wait.launchFiber(segured.consultarPaises, req, res);
  });
  /**
   * Consulta el nivel 1 del pais mensionado
   *
   * @name consultarNivel1
   * @method
   * @param {string} pais - Pais a consultar.
   * @example
   *  {
   *    "pais":"Chile",
   *  }
   * @returns {object} resp = {error: [0,400,500], mensaje: "texto de respuesta"}, nivel_1 = [].
   */
  router.post('/consultarNivel1', function(req, res, next){
    wait.launchFiber(segured.consultarNivel1, req, res);
  });
  /**
   * Notificacion push puede enviar dos tipos, confirmacion y notificacion normal, la confirmacion no requiere mensaje.
   *
   * @name notificacionPush
   * @method
   * @param {number} tipo - Tipo de mensaje a enviar, 1 = notificacion normal, 2 = confirmacion.
   * @param {string} codigo_identificador - Codigo identificador del usuario.
   * @param {number} num_caso - Num_caso a enviar la notificacion push.
   * @param {number} [id_inspector] - id del inspector.
   * @param {string} [pais] - Pais a donde se enviara la notificacion push.
   * @param {string} [nivel_1] - Nivel a donde se enviara la notificacion push.
   * @param {string} [nivel_2] - Nivel a donde se enviara la notificacion push.
   * @param {string} [nivel_3] - Nivel a donde se enviara la notificacion push.
   * @param {string} [nivel_4] - Nivel a donde se enviara la notificacion push.
   * @param {string} [nivel_5] - Nivel a donde se enviara la notificacion push.
   * @param {string} [mensaje] - Mensaje a enviar si es de tipo 1 (notificacion).
   * @example
   *  {
   *    "tipo":1,
   *    "codigo_identificador":"99999999-9",
   *    "num_caso":5000,
   *    "mensaje":"Mensaje de la noticiacion push"
   *  }
   * @returns {object} resp = {error: [0,400,500], mensaje: "texto de respuesta"}.
   */
  router.post('/notificacionPush', function(req, res, next){
    wait.launchFiber(segured.notificacionPush, req, res);
  });
  /**
   * Notificacion push puede enviar dos tipos, confirmacion y notificacion normal.
   *
   * @name notificacionPushSolo
   * @method
   * @param {number} id_inspector - Id de u-adjust del inspector a cual enviar la notificacion.
   * @param {number} tipo - Tipo de mensaje a enviar, 1 = notificacion normal, 2 = confirmacion.
   * @param {string} [mensaje] - Mensaje a enviar si es de tipo 1 (notificacion).
   * @example
   *  {
   *    "id_inspector":157,
   *    "tipo":1,
   *    "mensaje":"Mensaje de la noticiacion push"
   *  }
   * @returns {object} resp = {error: [0,400,500], mensaje: "texto de respuesta"}.
   */
  router.post('/notificacionPushSolo', function(req, res, next){
    wait.launchFiber(segured.notificacionPushSolo, req, res);
  });
  /**
   * Notificacion push hacia el usuario de al app u-adjust-s.
   *
   * @name notificacionPush_s
   * @method
   * @param {string} device_token - Lista de tokens separados por coma de usuarios a enviar la notificacion push.
   * @param {string} mensaje - Mensaje para ser enviado a los usuarios de la lista de tokens.
   * @example
    {
      "mensaje":1,
      "mensaje":"Notificacion push para u-adjust scl!!!"
    }
   * @returns {object} resp = {error: [0,400,500], mensaje: "texto de respuesta"}.
   */
  router.post('/notificacionPush_s', function(req, res, next){
    wait.launchFiber(segured.notificacionPush_s, req, res);
  });
  /**
   * Activa o desactiva un inspector, si este es activado debe traer una contraseña
   *
   * @name bloquearActivarInspector
   * @method
   * @param {number} id_inspector - ID del inspector a bloquear/activar.
   * @param {string} [codigo_identificador] - Codigo identificador del usuario a bloquear/activar.
   * @param {string} [password] - Password del inspector (si activa).
   * @param {number} estado - Estado a aplicar al inspector.
   * @example
   {
    "id_inspector":1,
    "password":"TEST",
    "estado":1
   }
   * @returns {object} resp = {error: [0,400,500], mensaje: "texto de respuesta"}.
   */
  router.post('/bloquearActivarInspector', function(req, res, next){
    wait.launchFiber(segured.bloquearActivarInspector, req, res);
  });
  /**
   * Modifica la informacion del inspector, por este modulo se va arreglar la correccion de direccion.
   *
   * @name editarInspector
   * @method
   * @param {numeric} id_inspector - Id del inspector.
   * @param {numeric} [codigo_identificador] - Codigo identificador del inspector.
   * @param {numeric} tipo_cambio - Tipo de cambio a realizar.
   * @param {object} datos - Datos del inspector si tipo_cambio = 1 (domicilio).
   * @param {string} datos.pais - Pais del inspector.
   * @param {string} datos.nivel_1 - Nivel 1 del inspector.
   * @param {string} datos.nivel_2 - Nivel 2 del inspector.
   * @param {string} datos.nivel_3 - Nivel 3 del inspector.
   * @param {string} datos.nivel_4 - Nivel 4 del inspector.
   * @param {string} datos.nivel_5 - Nivel 5 del inspector.
   * @param {string} datos.direccion - Direccion del inspector.
   * @param {string} datos.lat_dir - Latitud de la direccion del inspector.
   * @param {string} datos.lon_dir - Longitud de la direccion del inspector.
   * @param {numeric} datos.correccion_direccion - Correccion de direccion del inspector.
   * @param {object} datos - Datos del inspector si tipo_cambio = 2 (disponibilidad).
   * @param {numeric} datos.disponibilidad - disponibilidad del inspector.
   * @param {numeric} datos.disponibilidad_viajar_pais - Disponibilidad de viajar fuera del pais.
   * @param {numeric} datos.disponibilidad_viajar_ciudad - Disponibilidad de viajar fuera de la ciudad.
   * @param {numeric} [datos.disponibilidad_horas] - disponibilidad_horas si su disponibilidad es part_time (0).
   * @param {numeric} [datos.d1] - d1 si su disponibilidad es part_time.
   * @param {numeric} [datos.d2] - d2 si su disponibilidad es part_time.
   * @param {numeric} [datos.d3] - d3 si su disponibilidad es part_time.
   * @param {numeric} [datos.d4] - d4 si su disponibilidad es part_time.
   * @param {numeric} [datos.d5] - d5 si su disponibilidad es part_time.
   * @param {numeric} [datos.d6] - d6 si su disponibilidad es part_time.
   * @param {numeric} [datos.d7] - d7 si su disponibilidad es part_time.
   * @param {object} datos - Datos del inspector si tipo_cambio = 3 (contacto).
   * @param {string} datos.correo - Correo del inspector.
   * @param {string} datos.telefono - Telefono del inspector.
   * @example
   *  {
   *  "codigo_identificador":"99999999-9",
   *  "tipo_cambio":1,
   *   "datos": {
   *   "pais":"Chile",
   *    "nivel_1":"Region Metropolitana",
   *    "nivel_2":"Santiago",
   *    "nivel_3":"Las Condes"
   *    "nivel_4":"",
   *    "nivel_5":"",
   *    "direccion":"Av. Las Condes 9460",
   *    "lat_dir":"10.000000",
   *    "lon_dir":"-10.000000",
   *    "correccion_direccion":0
   *  }
   * }
   * {
   *  "codigo_identificador":"99999999-9",
   *  "tipo_cambio":2,
   *  "datos": {
   *    "disponibilidad":0,
   *    "disponibilidad_viajar_pais":0,
   *    "disponibilidad_viajar_ciudad":0,
   *    "disponibilidad_horas":"12:00 23:00"
   *    "d1":1,
   *    "d2":1,
   *    "d3":0,
   *    "d4":1,
   *    "d5":1,
   *    "d6":0,
   *    "d7":0
   *  }
   * }
   * {
   *  "codigo_identificador":"99999999-9",
   *  "tipo_cambio":3,
   *  "datos": {
   *    "correo":"test@creador.cl",
   *    "telefono":2223334,
   *  }
   * }
   * @returns {object} resp = {error: [0,400,500], mensaje: "texto de respuesta"}.
   */
  router.post('/editarInspector', function(req, res, next){
   wait.launchFiber(segured.editarInspector, req, res);
  });
  /**
   * Registra las tareas dentro de la base de datos
   *
   * @name registrarTareas
   * @method
   * @param {array} tareas - Array de tareas, contiene toda la informacion necesaria.
   * @param {object|array} tareas.asegurado - Objeto asegurado, contiene la informacion del asegurado.
   * @param {string} tareas.asegurado.codigo_identificador - Codigo identificador del asegurado.
   * @param {string} tareas.asegurado.nombre - Nombre del asegurado.
   * @param {string} [tareas.asegurado.tel_fijo] - Telefono fijo del asegurado.
   * @param {string} [tareas.asegurado.tel_movil] - Telefono movil del asegurado.
   * @param {string} [tareas.asegurado.correo] - Correo del asegurado.
   * @param {string} tareas.asegurado.nombre - Nombre del asegurado.
   * @param {object|array} tareas.tarea - Objecto de tareas para ser ingresado.
   * @param {string} tareas.tarea.num_caso - Num_caso del caso de la tarea.
   * @param {numeric} [tareas.tarea.id_inspector] - Id del inspector al cual se le asigna la tarea (se asume que tiene fecha_tarea).
   * @param {string} tareas.tarea.asegurador - Asegurador del caso de la tarea.
   * @param {string} tareas.tarea.evento - Evento del caso de la tarea.
   * @param {string} tareas.tarea.fecha_tarea - Fecha_tarea del caso de la tarea.
   * @param {string} tareas.tarea.fecha_siniestro - Fecha_siniestro del caso de la tarea.
   * @param {string} tareas.tarea.lat - Latitud de la tarea.
   * @param {string} tareas.tarea.lon - Longitud de la tarea.
   * @param {numeric} tareas.tarea.categoria - Categoria del caso de la tarea.
   * @param {string} tareas.tarea.bono - Bono del caso de la tarea (flotante).
   * @param {numeric} tareas.tarea.tipo_tarea - tipo_tarea del caso de la tarea.
   * @example
   * {
   * "tareas":[
   *      {
   *         "asegurado":{
   *            "codigo_identificador":"14543231-6",
   *            "nombre":"Victor Navarro Bravo",
   *            "tel_fijo":"7987",
   *            "tel_movil":"85202147",
   *            "correo":"vnavarro@trassa.cl"
   *         },
   *         "tarea":{
   *            "num_caso":"5301",
   *            "asegurador":"HDI Seguros S.A.",
   *            "evento":"30",
   *            "fecha_tarea":"2017-06-21",
   *            "fecha_siniestro":"2017-06-01",
   *            "lon":"-70.6946254",
   *            "lat":"-33.42036180000001",
   *            "categoria":"0",
   *            "bono":"0",
   *            "tipo_tarea":0
   *         }
   *      }
   *   ]
   * }
   * @returns {object} resp = {error: [0,400,500], mensaje: "texto de respuesta"}.
   */
  router.post('/registrarTareas', function(req, res, next){
    wait.launchFiber(segured.registrarTareas, req, res);
  });
  /**
   * Consulta una tarea.
   *
   * @name consultarTarea
   * @method
   * @param {number} num_caso - Numero de caso asignado a la tarea.
   * @example
   * {
   *  "num_caso":5000
   * }
   * @returns {object|array}  resp = {error: [0,400,500], mensaje: "texto de respuesta"}, tarea = [array de tareeas].
   */
  router.post('/consultarTarea', function(req, res, next){
    wait.launchFiber(segured.consultarTarea, req, res);
  });
  /**
  * Modifica los datos de una tarea.
  *
  * @name modificarTarea
  * @method
  * @param {number} num_caso - Numero de caso asignado a la tarea.
  * @param {object} datos - Datos de la tarea a editar.
  * @param {string} datos.evento - Evento asociado a la tarea.
  * @param {string} datos.lat - Latitud de la tarea.
  * @param {string} datos.lon - Longitud de la tarea.
  * @param {numeric} datos.categoria - Categoria de la tarea (0=normal,1=emergencia)
  * @param {string} [datos.fecha_tarea] - Si su categoria es 1 (emergencia), este campo no es requerido.
  * @param {string} datos.fecha_siniestro - Fecha del siniestro.
  * @param {string} [datos.bono] - Bono de la tarea, si no es definido se asume 0.
  * @param {number} [datos.tipo_tarea] - Tipo de tarea (0=normal,1=critica), si es categoria 0 (normal), este aplica.
  * @example
  {
    "num_caso":5000,
    "datos": {
      "evento":30,
      "lat":"10.000000",
      "lon":"-10.000000",
      "categoria":0,
      "fecha_tarea":"2017-06-13",
      "fecha_siniestro":"2017-06-01",
      "bono":"10.5",
      "tipo_tarea":1
    }
  }
  {
    "num_caso":5000,
    "datos": {
      "evento":30,
      "lat":"10.000000",
      "lon":"-10.000000",
      "categoria":1,
      "fecha_siniestro":"2017-06-01",
      "bono":"1.5",
      "tipo_tarea":0
    }
  }
  * @returns {object|array}  resp = {error: [0,400,500], mensaje: "texto de respuesta"}, tarea = [array de tareeas].
  */
  router.post('/modificarTarea', function(req, res, next){
    wait.launchFiber(segured.modificarTarea, req, res);
  });
  /**
   * Cancela una tarea independiente del estado que tenga.
   *
   * @name cancelarTarea
   * @method
   * @param {string} num_tarea - Numero de tarea a desactivar.
   * @example
   * {
   *  "num_tarea": 5000
   * }
   * @returns {object} resp = {error: [0,400,500], mensaje: "texto de respuesta"}.
   */
  router.post('/cancelarTarea', function(req, res, next){
    wait.launchFiber(segured.cancelarTarea, req, res);
  });
  /**
   * Cancela una tarea independiente del estado que tenga.
   *
   * @name consultarInspector
   * @method
   * @param {numeric} [id_inspector] - Id del inspector a consultar.
   * @param {numeric} [codigo_identificador] - Codigo identificador del inspector a consultar.
   * @example
   * {
   *    "codigo_identificador": "99999999-9"
   *  }
   * @returns {object} resp = {error: [0,400,500], mensaje: "texto de respuesta"}, inspector = {}.
   */
  router.post('/consultarInspector', function(req, res, next){
    wait.launchFiber(segured.consultarInspector, req, res);
  });
  /**
   * Retorna un array de tareas tomadas entre las fechas especificadas.
   *
   * @name tareasTomadas
   * @method
   * @param {string} fecha_inicio - Fecha de inicio a consultar (YYYY-MM-DD).
   * @param {string} fecha_termino - Fecha de temrino a consultar (YYYY-MM-DD).
   * @example
   * {"fecha_inicio": "2017-05-12", "fecha_termino": "2017-05-15" }
   * @returns {object} resp = {error: [0,400,500], mensaje: "texto de respuesta"}, tareas = [].
   */
  router.post('/tareasTomadas', function(req, res, next){
    wait.launchFiber(segured.tareasTomadas, req, res);
  });
  /**
   * Retorna un array de tareas canceladas entre las fechas especificadas.
   *
   * @name tareasCanceladas
   * @method
   * @param {string} fecha_inicio - Fecha de inicio a consultar (YYYY-MM-DD).
   * @param {string} fecha_termino - Fecha de temrino a consultar (YYYY-MM-DD).
   * @example
   * {"fecha_inicio": "2017-05-12", "fecha_termino": "2017-05-15" }
   * @returns {object} resp = {error: [0,400,500], mensaje: "texto de respuesta"}.
   */
  router.post('/tareasCanceladas', function(req, res, next){
    wait.launchFiber(segured.tareasCanceladas, req, res);
  });
  /**
   * Retorna un array de tareas terminadas entre las fechas especificadas.
   *
   * @name tareasTerminadas
   * @method
   * @param {string} fecha_inicio - Fecha de inicio a consultar (YYYY-MM-DD).
   * @param {string} fecha_termino - Fecha de temrino a consultar (YYYY-MM-DD).
   * @example
   * {"fecha_inicio": "2017-05-12", "fecha_termino": "2017-05-15" }
   * @returns {object} resp = {error: [0,400,500], mensaje: "texto de respuesta"}.
   */
  router.post('/tareasTerminadas', function(req, res, next){
    wait.launchFiber(segured.tareasTerminadas, req, res);
  });
  /**
   * Registra partida con sus daños, accion y unidad de medida en la base de datos de U-Adjust.
   *
   * @name registrarPartida
   * @method
   * @param {object|array} datos - Array con datos de la partida.
   * @param {numeric} datos.id - Id de segured a registrar.
   * @param {string} datos.valor - Valor a registrar.
   * @param {string} datos.pais - Pais al que se registrara el dato.
   * @param {object|array} datos.tipo_dano - Array con tipos de daños a registrar.
   * @param {numeric} datos.tipo_dano.id - Id de segured a registrar.
   * @param {string} datos.tipo_dano.valor - Valor a registrar.
   * @param {object|array} datos.tipo_dano.accion - Array de acciones de tipo de daños.
   * @param {numeric} datos.tipo_dano.accion.id - Id de segured a registrar.
   * @param {string} datos.tipo_dano.accion.valor - Valor a registrar.
   * @param {object|array} datos.tipo_dano.accion.unidad_medida - Array con unidades de medida del tipo de daño.
   * @param {numeric} datos.tipo_dano.accion.unidad_medida.id - Id de segured a registrar.
   * @param {string} datos.tipo_dano.accion.unidad_medida.valor - Valor a registrar.
   * @param {string} datos.tipo_dano.accion.unidad_medida.valor_s - Valor corto a registrar.
   * @example
   * {
   *   "datos":[{
   *     "id":1,
   *     "valor":"TEXT",
   *     "pais":"Chile",
   *     "tipo_dano": [{
   *      "id":1,
   *      "valor":"TEXT",
   *      "accion":[{
   *        "id":1,
   *        "valor":"TEXT",
   *        "unidad_medida":[{
   *          "id":1,
   *          "valor":"TEXT",
   *          "valor_s":"TEX"
   *        }]
   *      }]
   *    }]
   *  }]
   * }
   * @returns {object} resp = {error: [0,400,500], mensaje: "texto de respuesta"}.
   */
  router.post('/registrarPartida', function(req, res, next){
    wait.launchFiber(segured.registrarPartida, req, res);
  });
  /**
   * Registra destinos en la base de datos de U-Adjust.
   *
   * @name registrarDestinos
   * @method
   * @param {object|array} destinos - Array con destinos a registrar.
   * @param {numeric} destinos.id - Id de segured a registrar.
   * @param {string} destinos.valor - Valor a registrar.
   * @param {string} destinos.pais - Pais al que se registrara el dato.
   * @example
   * {
   *   "destinos": [{
   *      "id":1,
   *      "valor":"TEXT",
   *      "pais":"CHILE"
   *    }]
   * }
   * @returns {object} resp = {error: [0,400,500], mensaje: "texto de respuesta"}.
   */
  router.post('/registrarDestinos', function(req, res, next){
    wait.launchFiber(segured.registrarDestinos, req, res);
  });
  /**
   * Registra companias en la base de datos de U-Adjust.
   *
   * @name registrarCompania
   * @method
   * @param {object|array} compania - Array con companias a registrar.
   * @param {numeric} compania.id - Id de segured a registrar.
   * @param {string} compania.valor - Valor a registrar.
   * @param {string} compania.pais - Pais al que se registrara el dato.
   * @example
   * {
   *   "compania": [{
   *      "id":1,
   *      "valor":"TEXT",
   *      "pais":"CHILE"
   *    }]
   * }
   * @returns {object} resp = {error: [0,400,500], mensaje: "texto de respuesta"}.
   */
  router.post('/registrarCompania', function(req, res, next){
    wait.launchFiber(segured.registrarCompania, req, res);
  });
  /**
   * Registra entidades financieras en la base de datos de U-Adjust.
   *
   * @name registrarEntidad_financiera
   * @method
   * @param {object|array} entidad_financiera - Array con entidades financieras a registrar.
   * @param {numeric} entidad_financiera.id - Id de segured a registrar.
   * @param {string} entidad_financiera.valor - Valor a registrar.
   * @param {string} entidad_financiera.pais - Pais al que se registrara el dato.
   * @example
   * {
   *   "entidad_financiera": [{
   *      "id":1,
   *      "valor":"TEXT",
   *      "pais":"CHILE"
   *    }]
   * }
   * @returns {object} resp = {error: [0,400,500], mensaje: "texto de respuesta"}.
   */
  router.post('/registrarEntidad_financiera', function(req, res, next){
    wait.launchFiber(segured.registrarEntidad_financiera, req, res);
  });
  /**
   * Registra tipo de siniestros en la base de datos de U-Adjust.
   *
   * @name registrarTipo_siniestro
   * @method
   * @param {object|array} tipo_siniestro - Array con tipos de siniestros a registrar.
   * @param {numeric} tipo_siniestro.id - Id de segured a registrar.
   * @param {string} tipo_siniestro.valor - Valor a registrar.
   * @param {string} tipo_siniestro.pais - Pais al que se registrara el dato.
   * @example
   * {
   *   "tipo_siniestro": [{
   *      "id":1,
   *      "valor":"TEXT",
   *      "pais":"CHILE"
   *    }]
   * }
   * @returns {object} resp = {error: [0,400,500], mensaje: "texto de respuesta"}.
   */
  router.post('/registrarTipo_siniestro', function(req, res, next){
    wait.launchFiber(segured.registrarTipo_siniestro, req, res);
  });
  /**
   * Registra estructuras soportantes en la base de datos de U-Adjust.
   *
   * @name registrarEstructura_soportante
   * @method
   * @param {object|array} estructura_soportante - Array con estructuras soportantes a registrar.
   * @param {numeric} estructura_soportante.id - Id de segured a registrar.
   * @param {string} estructura_soportante.valor - Valor a registrar.
   * @param {string} estructura_soportante.pais - Pais al que se registrara el dato.
   * @example
   * {
   *   "estructura_soportante": [{
   *      "id":1,
   *      "valor":"TEXT",
   *      "pais":"CHILE"
   *    }]
   * }
   * @returns {object} resp = {error: [0,400,500], mensaje: "texto de respuesta"}.
   */
  router.post('/registrarEstructura_soportante', function(req, res, next){
    wait.launchFiber(segured.registrarEstructura_soportante, req, res);
  });
  /**
   * Registra muros y tabiques en la base de datos de U-Adjust.
   *
   * @name registrarMuros_tabiques
   * @method
   * @param {object|array} muros_tabiques - Array con muros y tabiques a registrar.
   * @param {numeric} muros_tabiques.id - Id de segured a registrar.
   * @param {string} muros_tabiques.valor - Valor a registrar.
   * @param {string} muros_tabiques.pais - Pais al que se registrara el dato.
   * @example
   * {
   *   "muros_tabiques": [{
   *      "id":1,
   *      "valor":"TEXT",
   *      "pais":"CHILE"
   *    }]
   * }
   * @returns {object} resp = {error: [0,400,500], mensaje: "texto de respuesta"}.
   */
  router.post('/registrarMuros_tabiques', function(req, res, next){
    wait.launchFiber(segured.registrarMuros_tabiques, req, res);
  });
  /**
   * Registra entrepisos en la base de datos de U-Adjust.
   *
   * @name registrarEntrepisos
   * @method
   * @param {object|array} entrepisos - Array con entrepisos a registrar.
   * @param {numeric} entrepisos.id - Id de segured a registrar.
   * @param {string} entrepisos.valor - Valor a registrar.
   * @param {string} entrepisos.pais - Pais al que se registrara el dato.
   * @example
   * {
   *   "entrepisos": [{
   *      "id":1,
   *      "valor":"TEXT",
   *      "pais":"CHILE"
   *    }]
   * }
   * @returns {object} resp = {error: [0,400,500], mensaje: "texto de respuesta"}.
   */
  router.post('/registrarEntrepisos', function(req, res, next){
    wait.launchFiber(segured.registrarEntrepisos, req, res);
  });
  /**
   * Registra pavimentos en la base de datos de U-Adjust.
   *
   * @name registrarPavimento
   * @method
   * @param {object|array} pavimento - Array con pavimentos a registrar.
   * @param {numeric} pavimento.id - Id de segured a registrar.
   * @param {string} pavimento.valor - Valor a registrar.
   * @param {string} pavimento.pais - Pais al que se registrara el dato.
   * @example
   * {
   *   "pavimento": [{
   *      "id":1,
   *      "valor":"TEXT",
   *      "pais":"CHILE"
   *    }]
   * }
   * @returns {object} resp = {error: [0,400,500], mensaje: "texto de respuesta"}.
   */
  router.post('/registrarPavimento', function(req, res, next){
    wait.launchFiber(segured.registrarPavimento, req, res);
  });
  /**
   * Registra estructuras cubiertas en la base de datos de U-Adjust.
   *
   * @name registrarEstructura_cubierta
   * @method
   * @param {object|array} estructura_cubierta - Array con estructuras soportantes a registrar.
   * @param {numeric} estructura_cubierta.id - Id de segured a registrar.
   * @param {string} estructura_cubierta.valor - Valor a registrar.
   * @param {string} estructura_cubierta.pais - Pais al que se registrara el dato.
   * @example
   * {
   *   "estructura_cubierta": [{
   *      "id":1,
   *      "valor":"TEXT",
   *      "pais":"CHILE"
   *    }]
   * }
   * @returns {object} resp = {error: [0,400,500], mensaje: "texto de respuesta"}.
   */
  router.post('/registrarEstructura_cubierta', function(req, res, next){
    wait.launchFiber(segured.registrarEstructura_cubierta, req, res);
  });
  /**
   * Registra cubiertas en la base de datos de U-Adjust.
   *
   * @name registrarCubierta
   * @method
   * @param {object|array} cubierta - Array con cubiertas a registrar.
   * @param {numeric} cubierta.id - Id de segured a registrar.
   * @param {string} cubierta.valor - Valor a registrar.
   * @param {string} cubierta.pais - Pais al que se registrara el dato.
   * @example
   * {
   *   "cubierta": [{
   *      "id":1,
   *      "valor":"TEXT",
   *      "pais":"CHILE"
   *    }]
   * }
   * @returns {object} resp = {error: [0,400,500], mensaje: "texto de respuesta"}.
   */
  router.post('/registrarCubierta', function(req, res, next){
    wait.launchFiber(segured.registrarCubierta, req, res);
  });
  /**
   * Registra bienes en la base de datos de U-Adjust.
   *
   * @name bienes
   * @method
   * @param {object|array} bienes - Array con bienes a registrar.
   * @param {numeric} bienes.id - Id de segured a registrar.
   * @param {string} bienes.valor - Valor a registrar.
   * @param {string} bienes.pais - Pais al que se registrara el dato.
   * @example
   * {
   *   "bienes": [{
   *      "id":1,
   *      "valor":"TEXT",
   *      "pais":"Chile"
   *    }]
   * }
   * @todo terminar bienes...
   * @returns {object} resp = {error: [0,400,500], mensaje: "texto de respuesta"}.
   */
  router.post('/bienes', function(req, res, next){
    wait.launchFiber(segured.bienes, req, res);
  });
  /**
   * Registra marcas en la base de datos de U-Adjust.
   *
   * @name marcas
   * @method
   * @param {object|array} marcas - Array con bienes a registrar.
   * @param {numeric} marcas.id - Id de segured a registrar.
   * @param {string} marcas.valor - Valor a registrar.
   * @param {string} marcas.pais - Pais al que se registrara el dato.
   * @example
   * {
   *   "marcas": [{
   *      "id":1,
   *      "valor":"TEXT",
   *      "pais":"Chile"
   *    }]
   * }
   * @todo terminar bienes...
   * @returns {object} resp = {error: [0,400,500], mensaje: "texto de respuesta"}.
   */
  router.post('/marcas', function(req, res, next){
    wait.launchFiber(segured.marcas, req, res);
  });
  /**
   * Registra recintos en la base de datos de U-Adjust.
   *
   * @name recintos
   * @method
   * @param {object|array} recintos - Array con recintos a registrar.
   * @param {numeric} recintos.id - Id de segured a registrar.
   * @param {string} recintos.valor - Valor a registrar.
   * @param {string} recintos.pais - Pais al que se registrara el dato.
   * @example
   * {
   *   "recintos": [{
   *      "id":1,
   *      "valor":"TEXT",
   *      "pais":"Chile"
   *    }]
   * }
   * @todo terminar bienes...
   * @returns {object} resp = {error: [0,400,500], mensaje: "texto de respuesta"}.
   */
  router.post('/recintos', function(req, res, next){
    wait.launchFiber(segured.recintos, req, res);
  });
  /**
   * Edita el selector con una accion, modificar/eliminar
   *
   * @name editarSelectores
   * @method
   * @param {object|array} datos - Array de selectores a editar/eliminar.
   * @param {string} datos.accion - Accion a aplicar al selector especficado (modificar|eliminar).
   * @param {string} datos.selector - Selector a cual realizar la accion.
   * @param {numeric} datos.id - Id del selector.
   * @param {valor} datos.valor - Valor del selector.
   * @param {string} datos.pais - id del selector.
   * @example
   * {
   *   "datos": [{
   *      "accion":"editar",
   *      "selector":"entidad_financiera",
   *      "id":1,
   *      "valor":"TEXT",
   *      "pais":"Chile"
   *    }]
   * }
   * @returns {object} resp = {error: [0,400,500], mensaje: "texto de respuesta"}.
   */
  router.post('/editarSelectores', function(req, res, next){
    wait.launchFiber(segured.editarSelectores, req, res);
  });
  /**
   * Finaliza la inspección para borrar los datos como imagenes y otras cosas del servidor.
   *
   * @name finalizarInspeccion
   * @method
   * @param {number} num_caso - numero del caso a terminar.
   * @example
   * {
   *   "num_caso": 1203124
   * }
   * @returns {object} resp = {error: [0,400,500], mensaje: "texto de respuesta"}.
   */
  router.post('/finalizarInspeccion', function(req, res, next){
    wait.launchFiber(segured.finalizarInspeccion, req, res);
  });
  /**
   * Retorna la imagen requerida si es que existe.
   *
   * @name obtenerImagen
   * @method
   * @param {number} num_caso - numero del caso.
   * @param {number} id_imagen - Id de la imagen a obtener.
   * @example
   * {
   *   "num_caso": 1203124,
   *   "id_imagen": 12
   * }
   * @returns {object} resp = {error: [0,400,500], mensaje: "texto de respuesta"}, imagen = base64.
   */
  router.post('/obtenerImagen', function(req, res, next){
    wait.launchFiber(segured.obtenerImagen, req, res);
  });
  /**
   * Obtener la inspeccion ya realizada (retorna la misma estructura que se le envia a Segured).
   *
   * @name obtenerInspeccion
   * @method
   * @param {number} num_caso - numero del caso.
   * @example
   * {
   *   "num_caso": 1203124
   * }
   * @returns {object} resp = {error: [0,400,500], mensaje: "texto de respuesta"}, inspeccion = object.
   */
  router.post('/obtenerInspeccion', function(req, res, next){
    wait.launchFiber(segured.obtenerInspeccion, req, res);
  });

module.exports = router;
