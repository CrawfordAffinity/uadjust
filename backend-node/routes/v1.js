/**
 * Funciones de servicio de comunicacion entre aplicacion movil y servicio backend u-adjust.
 *
 * @module api
 * @requires moment
 * @requires wait.for
 * @requires async
 * @requires underscore
 */

var express = require('express'),
    wait = require('wait.for');
var router = express.Router();
var v1 = require('../models/v1');

// Importante: al usar wait.launchFiber lo que se hace es crear una fibra para el usuario, esto es importante ya que node es 1 thread, al usar fibers,
// lo que hace es dividir ese thread en muchas fibras.

  /**
   * retorna la configuracion de km de busqueda y cantidad de resultados para el filtro por distancia/resultados.
   *
   * @name config
   * @method
   * @private
   * @returns {Object} {limite_distancia: 0..n, limite_busqueda: 0..n}.
   */
    router.post('/dummy', function(req, res, next) {
       wait.launchFiber(v1.dummy, req, res);
    });
    /**
     * retorna los selectores con diferente fecha a la ultima modificacion del selector.
     *
     * @name selectores
     * @method
     * @param {Integer} pais - Id del pais a consultar.
     * @param {String} [destino] - Fecha a consultar, Ej: 2017-05-12.
     * @param {String} [compania] - Fecha a consultar, Ej: 2017-05-12.
     * @param {String} [entidad_financiera] - Fecha a consultar, Ej: 2017-05-12.
     * @param {String} [tipo_siniestro] - Fecha a consultar, Ej: 2017-05-12.
     * @param {String} [estructura_soportante] - Fecha a consultar, Ej: 2017-05-12.
     * @param {String} [muros_tabiques] - Fecha a consultar, Ej: 2017-05-12.
     * @param {String} [entrepisos] - Fecha a consultar, Ej: 2017-05-12.
     * @param {String} [pavimento] - Fecha a consultar, Ej: 2017-05-12.
     * @param {String} [estructura_cubierta] - Fecha a consultar, Ej: 2017-05-12.
     * @param {String} [cubierta] - Fecha a consultar, Ej: 2017-05-12.
     * @param {String} [partida] - Fecha a consultar, Ej: 2017-05-12.
     * @param {String} [tipo_dano] - Fecha a consultar, Ej: 2017-05-12.
     * @param {String} [bienes] - Fecha a consultar, Ej: 2017-05-12.
     * @param {String} [monedas] - Fecha a consultar, Ej: 2017-05-12.
     * @param {String} [recintos] - Fecha a consultar, Ej: 2017-05-12.
     * @returns {Object}
     */
    router.post('/selectores', function(req, res, next){
      wait.launchFiber(v1.selectores, req, res);
    });
    /**
     * Logea al inspector dentro de la aplicaicon, tambien entrega las tareas a realizar (tareas entrantes), emergencias (perfil,ubicacion), mis tareas, historial de tareas y selectores.
     *
     * @name login
     * @method
     * @param {string} correo - Correo del inspector.
     * @param {string} password - Password del inspector.
     * @param {string} uidd - Uidd del dispositivo.
     * @param {string} device - Tipo de dispositivo.
     * @param {string} device_token - Device token del dispositivo.
     * @param {string} lat - Latitud del inspector.
     * @param {string} lon - Longitud del inspector.
     * @param {string} fecha - fecha del dispositivo, para calcular la diferencia horaria.
     * @returns {object} resp = {[0,400,500,n], mensaje = [texto de respuesta],
     * @example
     *  tareas: {
     *      entrantes: { normal: [], critica: [] },
     *      emergencias: { ubicacion: [], perfil: []}
     *  },
     *  inspector: {},
     *  historial_tareas: [],
     *  mistareas: [] }.
     */
    router.post('/login', function(req, res, next){
      wait.launchFiber(v1.login, req, res);
    });
    /**
     * Obtiene las tareas de la pantalla tarea entrantes, filtradas segun se requiera.
     *
     * @name obtenerTareasEntrantes
     * @method
     * @param {number} id_inspector - Id del inspector a buscar sus tareas entrantes.
     * @returns {object} resp = {error: [0,400,500], mensaje: "texto de respuesta"}.
     */
    router.post('/obtenerTareasEntrantes', function(req,res,next){
      wait.launchFiber(v1.obtenerTareasEntrantes, req, res);
    });
    /**
     * Obtiene las tareas de la pantalla emergencia, filtradas segun se requiera.
     *
     * @name obtenerEmergencias
     * @method
     * @param {number} id_inspector - Id del inspector a buscar sus tareas entrantes.
     * @param {string} lat - Latitud del inspector actualmente.
     * @param {string} lon - Longitud del inspector actualmente.
     * @returns {object} Tareas filtradas
     */
    router.post('/obtenerEmergencias', function(req,res,next){
      wait.launchFiber(v1.obtenerEmergencias, req, res);
    });
    /**
     * Obtiene las tareas de la pantalla mis tareas, filtradas segun se requiera.
     *
     * @name obtenerMisTareas
     * @method
     * @param {number} id_inspector - Id del inspector a buscar sus tareas entrantes.
     * @param {string} lat - Latitud del inspector actualmente.
     * @param {string} lon - Longitud del inspector actualmente.
     * @returns {object} Tareas filtradas
     * @example
     *  {
     *      error: 0,
     *      mensaje: mensaje del error,
     *      mistareas: []
     *  },
     */
    router.post('/obtenerMisTareas', function(req,res,next){
      wait.launchFiber(v1.obtenerMisTareas, req, res);
    });
    /**
     * Obtiene el historial de tareas de la pantalla historial de tareas, ordenadas por fecha.
     *
     * @name obtenerHistorialTareas
     * @method
     * @param {number} id_inspector - Id del inspector a buscar sus tareas entrantes.
     * @returns {object} Tareas filtradas
     * @example
     *  {
     *      error: 0,
     *      mensaje: "",
     *      historial_tareas: []
     *  },
     */
    router.post('/obtenerHistorialTareas', function(req, res, next){
      wait.launchFiber(v1.obtenerHistorialTareas, req, res);
    });
    /**
     * Retorna la lista de paises, con regiones y experiencia de oficio correspondiente a cada pais ya activados.
     *
     * @name obtenerPais
     * @method
     * @returns {object}
     * @example
     * resp: {
     *   error:0,
     *   mensaje:"",
     *   paises:[],
     *   regiones:[]
     * }
     */
    router.post('/obtenerPais', function(req, res, next) {
       wait.launchFiber(v1.obtenerPais, req, res);
    });
    /**
     * Registra el inspector dentro de la base de datos y se le envia a Segured sobre el registro.
     *
     * @name registrarInspector
     * @method
     * @param {string} nombre - Nombre del inspector.
     * @param {string} apellido_paterno - Apellido paterno del inspector.
     * @param {string} apellido_materno - Apellido materno del inspector.
     * @param {string} codigo_identificador - Codigo identificador del inspector.
     * @param {string} fecha_nacimiento - Fecha de nacimiento del inspector Ej: 2017-05-12.
     * @param {number} pais - Id del pais.
     * @param {number} nivel_1 - Id del nivel_1 escogido por el inspector.
     * @param {string} [nivel_2] - Nivel_2 escrito por el inspector.
     * @param {string} [nivel_3] - Nivel_3 escrito por el inspector.
     * @param {string} [nivel_4] - Nivel_4 escrito por el inspector.
     * @param {string} [nivel_5] - Nivel_5 escrito por el inspector.
     * @param {string} [direccion] - Direccion del inspector (o arreglada por google).
     * @param {number} disponibilidad - Disponibilidad del inspector 0 = part time, 1 = full time.
     * @param {number} disp_viajar_ciudad - Disponibilidad de viajar fuera de la ciudad del inspector.
     * @param {number} disp_viajar_pais - Disponibilidad de viajar fuera del pais del inspector.
     * @param {number} [d1=0] - Disponibilidad de dia, si es part time aplica, si no, no es requerido.
     * @param {number} [d2=0] - Disponibilidad de dia, si es part time aplica, si no, no es requerido.
     * @param {number} [d3=0] - Disponibilidad de dia, si es part time aplica, si no, no es requerido.
     * @param {number} [d4=0] - Disponibilidad de dia, si es part time aplica, si no, no es requerido.
     * @param {number} [d5=0] - Disponibilidad de dia, si es part time aplica, si no, no es requerido.
     * @param {number} [d6=0] - Disponibilidad de dia, si es part time aplica, si no, no es requerido.
     * @param {number} [d7=0] - Disponibilidad de dia, si es part time aplica, si no, no es requerido.
     * @param {string} [disp_horas=00:00 23:59] - Disponibilidad de horas, si es part time aplica, si no, no es requerido.
     * @param {string} correo - Correo ingresado por el inspector.
     * @param {number} oficio - Id del oficio del inspector.
     * @param {string} experiencia - Experiencia del inspector, max 140 caracteres.
     * @param {string} uidd - uidd del inspector.
     * @param {string} telefono - telefono del inspector.
     * @param {number} correccion_direccion - Especifica si se le corrigio la direccion en la aplicacion/administrador, 1 = necesita correccion, 0 = corregida.
     * @param {string} device - Sistema operativo del inspector.
     * @param {string} latitude - Latitud de la direccion de la casa.
     * @param {string} longitude - Longitud de la direccion de la casa.
     * @param {object} google - Si el fue corregido por google, este objeto no vendra vacio y sera el que llenara los campos pais_google, direccion_google, nivel_1_google, etc.
     * @returns {object} resp = {[0,400,500,n], mensaje = [texto de respuesta]}.
     */
    router.post('/registrarInspector', function(req, res, next){
      wait.launchFiber(v1.registrarInspector, req, res);
    });
    /**
     * Valida el correo del inspector antes de registrarlo.
     *
     * @name validarCorreo
     * @method
     * @param {string} correo - Correo del inspector.
     * @returns {object} resp = {[0,400,500,n], mensaje = [texto de respuesta]}.
     */
    router.post('/validarCorreo', function(req, res, next){
      wait.launchFiber(v1.validarCorreo, req, res);
    });
    /**
     * Valida el codigo identificador del inspector en el pais antes de registrarlo.
     *
     * @name validarRut
     * @method
     * @param {string} codigo_identificador - Correo del inspector.
     * @param {string} pais - Pais del inspector.
     * @returns {object} resp = {[0,400,500,n], mensaje = [texto de respuesta]}.
     */
    router.post('/validarRut', function(req, res, next){
      wait.launchFiber(v1.validarRut, req, res);
    });
    /**
     * Obtiene la disponibilidad del inspector segun lo que diga Segured.
     *
     * @name dispInspector
     * @method
     * @param {number} id_inspector - Id del inspector para enviar a Segured.
     * @param {number} codigo_identificador - Codigo identificador del inspector para enviar a Segured.
     * @param {number} id_tarea - id de la tarea a aceptar.
     * @param {number} num_caso - Numero del caso de la tarea a aceptar.
     * @returns {object} resp = {error: [0,400,500], mensaje: "texto de respuesta"}, disponibilidad: [0,1]:booleano.
     */
    router.post('/dispInspector', function(req, res, next){
      wait.launchFiber(v1.dispInspector, req, res);
    });
    /**
     * Registra la tarea hacia el inspector.
     *
     * @name aceptarTarea
     * @method
     * @param {number} id_inspector - Id del inspector.
     * @param {number} codigo_identificador - Codigo identificador del inspector para enviar a Segured.
     * @param {number} id_tarea - id de la tarea a aceptar.
     * @param {number} num_caso - Numero del caso de la tarea a aceptar.
     * @param {number} categoria - (enterante 0 / emergencia 1) La cagetoria de la tarea, esto es por que si es una tarea de emergencias, se le actualiza la fecha_tarea a la de cuando se acepto.
     * @param {number} tipo_tarea - (normal 0 / critica 1) El tipo de tarea, si es critica, se asume que la fecha_tarea sera para hoy.
     * @returns {object} resp = {error: [0,400,500], mensaje: "texto de respuesta"}.
     */
    router.post('/aceptarTarea', function(req, res, next){
      wait.launchFiber(v1.aceptarTarea, req, res);
    });
    /**
     * Registra la cancelacion de una tarea y da aviso a Segured en el proceso.
     *
     * @name cancelarTarea
     * @method
     * @param {number} id_inspector - Id del inspector.
     * @param {string} codigo_identificador - Codigo identificador del inspector.
     * @param {number} id_tarea - id de la tarea a aceptar.
     * @param {number} num_caso - Numero del caso de la tarea a aceptar.
     * @param {string} mensaje - Texto descriptivo de por que se cancelo la tarea.
     * @param {number} [opcion] - Es un dato vacio (0) por el momento, pero puede que se reemplace el mensaje.
     * @param {number} [tipo=0] - Define si la tarea fue cancelada antes de iniciarse la inspeccion o ya iniciada (1).
     * @returns {object} resp = {error: [0,400,500], mensaje: "texto de respuesta"}.
     */
    // 16-11-17: Cancelar ya no va
    router.post('/cancelarTarea', function(req, res, next){
      wait.launchFiber(v1.cancelarTarea, req, res);
    });
    /**
     * Confirma la ruta del dia del inspector, esta funcion solo se utiliza cuando se le envia una notificacion push al inspector de tipo confirmación.
     *
     * @name confirmarRuta
     * @method
     * @param {number} id_inspector - Id del inspector.
     * @param {string} codigo_identificador - Codigo_identificador del inspector.
     * @param {number} [id_app] - Id para retornar a la app, mantiene el control de lo que se envia async.
     * @param {object} fecha - Fecha de la confirmación del inspector, esto por si el inspector confirma y se quedo sin internet de algun modo.
     * @returns {object} resp = {error: [0,400,500], mensaje: "texto de respuesta"}.
     */
    router.post('/confirmarRuta', function(req, res, next){
      wait.launchFiber(v1.confirmarRuta, req, res);
    });
    /**
     * Registra el inicio de una tarea, da aviso a Segured.
     *
     * @name iniciarTarea
     * @method
     * @param {number} id_inspector - Id del inspector.
     * @param {number} codigo_identificador - Codigo identificador del inspector para enviar a Segured.
     * @param {number} id_tarea - Id de la tarea a iniciar.
     * @param {number} num_caso - Numero del caso de la tarea a aceptar.
     * @returns {object} resp = {error: [0,400,500], mensaje: "texto de respuesta"}.
     */
    router.post('/iniciarTarea', function(req, res, next){
      wait.launchFiber(v1.iniciarTarea, req, res);
    });
    /**
     * Edita el domicio del inspector.
     *
     * @name editarPerfilTipo1
     * @method
     * @param {number} id_inspector - Id del inspector.
     * @param {number} pais - Id del pais.
     * @param {number} nivel_1 - Id del nivel_1.
     * @param {string} nivel_2 - Texto ingresado por el inspector.
     * @param {string} nivel_3 - Texto ingresado por el inspector.
     * @param {string} nivel_4 - Texto ingresado por el inspector.
     * @param {string} nivel_5 - Texto ingresado por el inspector.
     * @param {string} direccion - Direccion ingresada por el inspector.
     * @param {string} lat_dir - Latitud de la direccion del inspector resuelta por Google.
     * @param {string} lon_dir - Longitud de la direccion del inspector resulta por google.
     * @param {number} correccion_direccion - 1 = direccion necesita correccion, 2 = direccion no necesita correccion.
     * @param {object} google - objeto de google, si la direccion fue corregida este no estara vacio, y se editaran los campos del inspector pais_google, direccion_google, nivel_1_google.
     * @returns {object} resp = {error: [0,400,500], mensaje: "texto de respuesta"}.
     */
    router.post('/editarPerfilTipo1', function(req, res, next){
      wait.launchFiber(v1.editarPerfilTipo1, req, res);
    });
    /**
     * Edita la disponibilidad del inspector.
     *
     * @name editarPerfilTipo2
     * @method
     * @param {number} id_inspector - Id del inspector.
     * @param {number} disponibilidad - Disponibilidad del inspector 0 = parttime,  1 = fulltime.
     * @param {number} [d1=0] - Disponibilidad de dias, si su disponibilidad es 0 aplica.
     * @param {number} [d2=0] - Disponibilidad de dias, si su disponibilidad es 0 aplica.
     * @param {number} [d3=0] - Disponibilidad de dias, si su disponibilidad es 0 aplica.
     * @param {number} [d4=0] - Disponibilidad de dias, si su disponibilidad es 0 aplica.
     * @param {number} [d5=0] - Disponibilidad de dias, si su disponibilidad es 0 aplica.
     * @param {number} [d6=0] - Disponibilidad de dias, si su disponibilidad es 0 aplica.
     * @param {number} [d7=0] - Disponibilidad de dias, si su disponibilidad es 0 aplica.
     * @param {number} disponibilidad_viajar_pais - Si esta o no dispuesto a viajar fuera del pais.
     * @param {number} disponibilidad_viajar_ciudad - Si esta o no dispuesto a viajar fuera de su ciudad.
     * @param {string} [disponibilidad_horas=00:00 23:59] - Si su disponibilidad es 1 aplica.
     * @returns {object} resp = {error: [0,400,500], mensaje: "texto de respuesta"}.
     */
    router.post('/editarPerfilTipo2', function(req, res, next){
      wait.launchFiber(v1.editarPerfilTipo2, req, res);
    });
    /**
     * Edita los datos de contacto del inspector.
     *
     * @name editarPerfilTipo3
     * @method
     * @param {number} id_inspector - Id del inspector.
     * @param {string} correo - Correo del inspector.
     * @param {string} telefono - Telefono del inspector.
     * @returns {object} resp = {error: [0,400,500], mensaje: "texto de respuesta"}.
     */
    router.post('/editarPerfilTipo3', function(req, res, next){
      wait.launchFiber(v1.editarPerfilTipo3, req, res);
    });
    /**
     * Obtendra la tarea terminada, la guardara en la base de datos y lo agregara a la cola de salida para su envio.
     *
     * @name finalizarTarea
     * @method
     * @param {object} fotosrequeridas - objeto de fotos requeridas
     * @param {object} datosbasicos - objeto de datosbasicos
     * @param {object} niveles - objeto de niveles
     * @param {object} caracteristicas - objeto de caracteristicas
     * @param {object} siniestro - objeto de siniestro
     * @param {object} recintos - objeto de recintos
     * @param {object} itemdanos - objeto de itemdanos
     * @param {object} contenido - objeto de contenido
     * @param {object} documentos - objeto de documentos
     * @param {object} firma - objeto de firma
     * @returns {object} resp = {error: [0,400,500], mensaje: "texto de respuesta"}.
     */
    router.post('/finalizarTarea', function(req, res, next){
      wait.launchFiber(v1.finalizarTarea, req, res);
    });
    /**
     * Registra las imagenes dentro de la base de datos.
     *
     * @name subirImagenes
     * @method
     * @param {number} id_tarea - Id de la tarea.
     * @param {string} archivo - Nombre de la imagen.
     * @param {object} imagen - base64 de la imagen a subir.
     * @param {numeric} imagen_final - Booleano que indica si es la imagen final (true,false).
     * @returns {object} resp = {error: [0,400,500], mensaje: "texto de respuesta"}.
     */
     var multer  = require('multer');
     var upload = multer({ dest: 'uploads/' });
    router.post('/subirImagenes', upload.single('imagen'), function(req, res, next){
      wait.launchFiber(v1.subirImagenes, req, res);
    });
    /**
     * Envia la inspeccion cada x tiempo.
     *
     * @name enviarInspeccion
     * @method
     * @private
     */
    router.post('/enviarInspeccion', function(req, res, next) {
       wait.launchFiber(v1.enviarInspeccion, req, res);
    });
    /**
     * Envia las imagenes de la inspeccion cada x tiempo.
     *
     * @name enviarImagenes
     * @method
     * @private
     */
    router.post('/enviarImagenes', function(req, res, next){
      wait.launchFiber(v1.enviarImagenes, req, res);
    });
    /**
     * Avisa al inspector si tiene alguna confirmación pendiente.
     *
     * @name tengoNotificacion
     * @method
     * @param {number} ci_inspector - Codigo identificador del inspector a cual consultar las su ubicacion y compararla contra la tarea que tenga en estado.
     * @returns {object} (resp = {error: [0,400,500], mensaje: "texto de respuesta"}), push (boolean).
     */
    router.post('/tengoNotificacion', function(req, res, next){
      wait.launchFiber(v1.tengoNotificacion, req, res);
    });
    /**
     * Retorna la ultima ubicación del inspector con el tiempo estimado de llegada del inspector hasta la tarea.
     *
     * @name obtenerUbicacion
     * @method
     * @param {number} ci_inspector - Codigo identificador del inspector a cual consultar su ubicación.
     * @param {number} num_caso - Codigo unico de la tarea para ser ubicada dentro de la DB.
     * @returns {object} (resp = {error: [0,400,500], mensaje: "texto de respuesta"}), inspector [lat,lon], tarea[lat,lon].
     */
    router.post('/obtenerUbicacion', function(req, res, next){
      wait.launchFiber(v1.obtenerUbicacion, req, res);
    });
    /**
     * Guarda la ubicacion del inspector cada X tiempo dependiendo de la APP, esto es cuando inicia el seguimiento. Si tiene otra tarea con seguimiento (cambio la tarea a seguir), se dejara esas tareas en estado 2.
     *
     * @name guardarUbicacion
     * @method
     * @param {number} id_inspector - Id del inspector para guardar su ubicación.
     * @param {number} id_tarea - Id de la tarea del inspector.
     * @param {string} lat - Latitud.
     * @param {string} lon - Longitud.
     * @returns {object} (resp = {error: [0,400,500], mensaje: "texto de respuesta"}).
     */
    router.post('/guardarUbicacion', function(req, res, next){
      wait.launchFiber(v1.guardarUbicacion, req, res);
    });
    /**
     * Envia a segured cada X tiempo request que no dependan del usuario sobre las acciones requeridas para registrar usuario, aceptar tareas, confirmar rutas, etc.
     *
     * @name colaSalida
     * @method
     */
    router.post('/colaSalida', function(req, res, next){
      wait.launchFiber(v1.colaSalida, req, res);
    });

    // Resetear tareas
    router.post('/resetearTareas', function(req, res, next){
      wait.launchFiber(v1.resetearTareas, req, res);
    });
    router.get('/resetearTareas', function(req, res, next){
      wait.launchFiber(v1.resetearTareas, req, res);
    });
    //Dumps
      // router.post('/dump_tarea', function(req, res, next){
      //   wait.launchFiber(v1.dump_tarea, req, res);
      // });

    // timezone
      // router.post('/timezoneGet', function(req, res, next){
      //   wait.launchFiber(v1.timezoneGet, req, res);
      // });
    //Calling codes
      // router.post('/getCallingCodes', function(req, res, next){
      //   wait.launchFiber(v1.getCallingCodes, req, res);
      // });
    // Country Language
      // router.post('/getCountryLanguage', function(req, res, next){
      //   wait.launchFiber(v1.getCountryLanguage, req, res);
      // });

module.exports = router;
