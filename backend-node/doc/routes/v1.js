{
    "Funciones de servicio de comunicacion entre aplicacion movil y servicio backend u-adjust.": "Funciones de servicio de comunicacion entre aplicacion movil y servicio backend u-adjust.",
    "retorna la configuracion de km de busqueda y cantidad de resultados para el filtro por distancia/resultados.": "retorna la configuracion de km de busqueda y cantidad de resultados para el filtro por distancia/resultados.",
    "{limite_distancia: 0..n, limite_busqueda: 0..n}.": "{limite_distancia: 0..n, limite_busqueda: 0..n}.",
    "retorna los selectores con diferente fecha a la ultima modificacion del selector.": "retorna los selectores con diferente fecha a la ultima modificacion del selector.",
    "Id del pais a consultar.": "Id del pais a consultar.",
    "Fecha a consultar, Ej: 2017-05-12.": "Fecha a consultar, Ej: 2017-05-12.",
    "Logea al inspector dentro de la aplicaicon, tambien entrega las tareas a realizar (tareas entrantes), emergencias (perfil,ubicacion), mis tareas, historial de tareas y selectores.": "Logea al inspector dentro de la aplicaicon, tambien entrega las tareas a realizar (tareas entrantes), emergencias (perfil,ubicacion), mis tareas, historial de tareas y selectores.",
    "Correo del inspector.": "Correo del inspector.",
    "Password del inspector.": "Password del inspector.",
    "Uidd del dispositivo.": "Uidd del dispositivo.",
    "Tipo de dispositivo.": "Tipo de dispositivo.",
    "Device token del dispositivo.": "Device token del dispositivo.",
    "Latitud del inspector.": "Latitud del inspector.",
    "Longitud del inspector.": "Longitud del inspector.",
    "fecha del dispositivo, para calcular la diferencia horaria.": "fecha del dispositivo, para calcular la diferencia horaria.",
    "resp = {[0,400,500,n], mensaje = [texto de respuesta],": "resp = {[0,400,500,n], mensaje = [texto de respuesta],",
    "Obtiene las tareas de la pantalla tarea entrantes, filtradas segun se requiera.": "Obtiene las tareas de la pantalla tarea entrantes, filtradas segun se requiera.",
    "Id del inspector a buscar sus tareas entrantes.": "Id del inspector a buscar sus tareas entrantes.",
    "resp = {error: [0,400,500], mensaje: \"texto de respuesta\"}.": "resp = {error: [0,400,500], mensaje: \"texto de respuesta\"}.",
    "Obtiene las tareas de la pantalla emergencia, filtradas segun se requiera.": "Obtiene las tareas de la pantalla emergencia, filtradas segun se requiera.",
    "Latitud del inspector actualmente.": "Latitud del inspector actualmente.",
    "Longitud del inspector actualmente.": "Longitud del inspector actualmente.",
    "Tareas filtradas": "Tareas filtradas",
    "Obtiene las tareas de la pantalla mis tareas, filtradas segun se requiera.": "Obtiene las tareas de la pantalla mis tareas, filtradas segun se requiera.",
    "Obtiene el historial de tareas de la pantalla historial de tareas, ordenadas por fecha.": "Obtiene el historial de tareas de la pantalla historial de tareas, ordenadas por fecha.",
    "Retorna la lista de paises, con regiones y experiencia de oficio correspondiente a cada pais ya activados.": "Retorna la lista de paises, con regiones y experiencia de oficio correspondiente a cada pais ya activados.",
    "Registra el inspector dentro de la base de datos y se le envia a Segured sobre el registro.": "Registra el inspector dentro de la base de datos y se le envia a Segured sobre el registro.",
    "Nombre del inspector.": "Nombre del inspector.",
    "Apellido paterno del inspector.": "Apellido paterno del inspector.",
    "Apellido materno del inspector.": "Apellido materno del inspector.",
    "Codigo identificador del inspector.": "Codigo identificador del inspector.",
    "Fecha de nacimiento del inspector Ej: 2017-05-12.": "Fecha de nacimiento del inspector Ej: 2017-05-12.",
    "Id del pais.": "Id del pais.",
    "Id del nivel_1 escogido por el inspector.": "Id del nivel_1 escogido por el inspector.",
    "Nivel_2 escrito por el inspector.": "Nivel_2 escrito por el inspector.",
    "Nivel_3 escrito por el inspector.": "Nivel_3 escrito por el inspector.",
    "Nivel_4 escrito por el inspector.": "Nivel_4 escrito por el inspector.",
    "Nivel_5 escrito por el inspector.": "Nivel_5 escrito por el inspector.",
    "Direccion del inspector (o arreglada por google).": "Direccion del inspector (o arreglada por google).",
    "Disponibilidad del inspector 0 = part time, 1 = full time.": "Disponibilidad del inspector 0 = part time, 1 = full time.",
    "Disponibilidad de viajar fuera de la ciudad del inspector.": "Disponibilidad de viajar fuera de la ciudad del inspector.",
    "Disponibilidad de viajar fuera del pais del inspector.": "Disponibilidad de viajar fuera del pais del inspector.",
    "Disponibilidad de dia, si es part time aplica, si no, no es requerido.": "Disponibilidad de dia, si es part time aplica, si no, no es requerido.",
    "Disponibilidad de horas, si es part time aplica, si no, no es requerido.": "Disponibilidad de horas, si es part time aplica, si no, no es requerido.",
    "Correo ingresado por el inspector.": "Correo ingresado por el inspector.",
    "Id del oficio del inspector.": "Id del oficio del inspector.",
    "Experiencia del inspector, max 140 caracteres.": "Experiencia del inspector, max 140 caracteres.",
    "uidd del inspector.": "uidd del inspector.",
    "telefono del inspector.": "telefono del inspector.",
    "Especifica si se le corrigio la direccion en la aplicacion/administrador, 1 = necesita correccion, 0 = corregida.": "Especifica si se le corrigio la direccion en la aplicacion/administrador, 1 = necesita correccion, 0 = corregida.",
    "Sistema operativo del inspector.": "Sistema operativo del inspector.",
    "Latitud de la direccion de la casa.": "Latitud de la direccion de la casa.",
    "Longitud de la direccion de la casa.": "Longitud de la direccion de la casa.",
    "Si el fue corregido por google, este objeto no vendra vacio y sera el que llenara los campos pais_google, direccion_google, nivel_1_google, etc.": "Si el fue corregido por google, este objeto no vendra vacio y sera el que llenara los campos pais_google, direccion_google, nivel_1_google, etc.",
    "resp = {[0,400,500,n], mensaje = [texto de respuesta]}.": "resp = {[0,400,500,n], mensaje = [texto de respuesta]}.",
    "Valida el correo del inspector antes de registrarlo.": "Valida el correo del inspector antes de registrarlo.",
    "Valida el codigo identificador del inspector en el pais antes de registrarlo.": "Valida el codigo identificador del inspector en el pais antes de registrarlo.",
    "Pais del inspector.": "Pais del inspector.",
    "Obtiene la disponibilidad del inspector segun lo que diga Segured.": "Obtiene la disponibilidad del inspector segun lo que diga Segured.",
    "Id del inspector para enviar a Segured.": "Id del inspector para enviar a Segured.",
    "Codigo identificador del inspector para enviar a Segured.": "Codigo identificador del inspector para enviar a Segured.",
    "id de la tarea a aceptar.": "id de la tarea a aceptar.",
    "Numero del caso de la tarea a aceptar.": "Numero del caso de la tarea a aceptar.",
    "resp = {error: [0,400,500], mensaje: \"texto de respuesta\"}, disponibilidad: [0,1]:booleano.": "resp = {error: [0,400,500], mensaje: \"texto de respuesta\"}, disponibilidad: [0,1]:booleano.",
    "Registra la tarea hacia el inspector.": "Registra la tarea hacia el inspector.",
    "Id del inspector.": "Id del inspector.",
    "(enterante 0 / emergencia 1) La cagetoria de la tarea, esto es por que si es una tarea de emergencias, se le actualiza la fecha_tarea a la de cuando se acepto.": "(enterante 0 / emergencia 1) La cagetoria de la tarea, esto es por que si es una tarea de emergencias, se le actualiza la fecha_tarea a la de cuando se acepto.",
    "(normal 0 / critica 1) El tipo de tarea, si es critica, se asume que la fecha_tarea sera para hoy.": "(normal 0 / critica 1) El tipo de tarea, si es critica, se asume que la fecha_tarea sera para hoy.",
    "Registra la cancelacion de una tarea y da aviso a Segured en el proceso.": "Registra la cancelacion de una tarea y da aviso a Segured en el proceso.",
    "Texto descriptivo de por que se cancelo la tarea.": "Texto descriptivo de por que se cancelo la tarea.",
    "Es un dato vacio (0) por el momento, pero puede que se reemplace el mensaje.": "Es un dato vacio (0) por el momento, pero puede que se reemplace el mensaje.",
    "Define si la tarea fue cancelada antes de iniciarse la inspeccion o ya iniciada (1).": "Define si la tarea fue cancelada antes de iniciarse la inspeccion o ya iniciada (1).",
    "Confirma la ruta del dia del inspector, esta funcion solo se utiliza cuando se le envia una notificacion push al inspector de tipo confirmación.": "Confirma la ruta del dia del inspector, esta funcion solo se utiliza cuando se le envia una notificacion push al inspector de tipo confirmación.",
    "Codigo_identificador del inspector.": "Codigo_identificador del inspector.",
    "Id para retornar a la app, mantiene el control de lo que se envia async.": "Id para retornar a la app, mantiene el control de lo que se envia async.",
    "Fecha de la confirmación del inspector, esto por si el inspector confirma y se quedo sin internet de algun modo.": "Fecha de la confirmación del inspector, esto por si el inspector confirma y se quedo sin internet de algun modo.",
    "Registra el inicio de una tarea, da aviso a Segured.": "Registra el inicio de una tarea, da aviso a Segured.",
    "Id de la tarea a iniciar.": "Id de la tarea a iniciar.",
    "Edita el domicio del inspector.": "Edita el domicio del inspector.",
    "Id del nivel_1.": "Id del nivel_1.",
    "Texto ingresado por el inspector.": "Texto ingresado por el inspector.",
    "Direccion ingresada por el inspector.": "Direccion ingresada por el inspector.",
    "Latitud de la direccion del inspector resuelta por Google.": "Latitud de la direccion del inspector resuelta por Google.",
    "Longitud de la direccion del inspector resulta por google.": "Longitud de la direccion del inspector resulta por google.",
    "1 = direccion necesita correccion, 2 = direccion no necesita correccion.": "1 = direccion necesita correccion, 2 = direccion no necesita correccion.",
    "objeto de google, si la direccion fue corregida este no estara vacio, y se editaran los campos del inspector pais_google, direccion_google, nivel_1_google.": "objeto de google, si la direccion fue corregida este no estara vacio, y se editaran los campos del inspector pais_google, direccion_google, nivel_1_google.",
    "Edita la disponibilidad del inspector.": "Edita la disponibilidad del inspector.",
    "Disponibilidad del inspector 0 = parttime,  1 = fulltime.": "Disponibilidad del inspector 0 = parttime,  1 = fulltime.",
    "Disponibilidad de dias, si su disponibilidad es 0 aplica.": "Disponibilidad de dias, si su disponibilidad es 0 aplica.",
    "Si esta o no dispuesto a viajar fuera del pais.": "Si esta o no dispuesto a viajar fuera del pais.",
    "Si esta o no dispuesto a viajar fuera de su ciudad.": "Si esta o no dispuesto a viajar fuera de su ciudad.",
    "Si su disponibilidad es 1 aplica.": "Si su disponibilidad es 1 aplica.",
    "Edita los datos de contacto del inspector.": "Edita los datos de contacto del inspector.",
    "Telefono del inspector.": "Telefono del inspector.",
    "Obtendra la tarea terminada, la guardara en la base de datos y lo agregara a la cola de salida para su envio.": "Obtendra la tarea terminada, la guardara en la base de datos y lo agregara a la cola de salida para su envio.",
    "objeto de fotos requeridas": "objeto de fotos requeridas",
    "objeto de datosbasicos": "objeto de datosbasicos",
    "objeto de niveles": "objeto de niveles",
    "objeto de caracteristicas": "objeto de caracteristicas",
    "objeto de siniestro": "objeto de siniestro",
    "objeto de recintos": "objeto de recintos",
    "objeto de itemdanos": "objeto de itemdanos",
    "objeto de contenido": "objeto de contenido",
    "objeto de documentos": "objeto de documentos",
    "objeto de firma": "objeto de firma",
    "Registra las imagenes dentro de la base de datos.": "Registra las imagenes dentro de la base de datos.",
    "Id de la tarea.": "Id de la tarea.",
    "Nombre de la imagen.": "Nombre de la imagen.",
    "base64 de la imagen a subir.": "base64 de la imagen a subir.",
    "Booleano que indica si es la imagen final (true,false).": "Booleano que indica si es la imagen final (true,false).",
    "Envia la inspeccion cada x tiempo.": "Envia la inspeccion cada x tiempo.",
    "Envia las imagenes de la inspeccion cada x tiempo.": "Envia las imagenes de la inspeccion cada x tiempo.",
    "Avisa al inspector si tiene alguna confirmación pendiente.": "Avisa al inspector si tiene alguna confirmación pendiente.",
    "Codigo identificador del inspector a cual consultar las su ubicacion y compararla contra la tarea que tenga en estado.": "Codigo identificador del inspector a cual consultar las su ubicacion y compararla contra la tarea que tenga en estado.",
    "(resp = {error: [0,400,500], mensaje: \"texto de respuesta\"}), push (boolean).": "(resp = {error: [0,400,500], mensaje: \"texto de respuesta\"}), push (boolean).",
    "Retorna la ultima ubicación del inspector con el tiempo estimado de llegada del inspector hasta la tarea.": "Retorna la ultima ubicación del inspector con el tiempo estimado de llegada del inspector hasta la tarea.",
    "Codigo identificador del inspector a cual consultar su ubicación.": "Codigo identificador del inspector a cual consultar su ubicación.",
    "Codigo unico de la tarea para ser ubicada dentro de la DB.": "Codigo unico de la tarea para ser ubicada dentro de la DB.",
    "(resp = {error: [0,400,500], mensaje: \"texto de respuesta\"}), inspector [lat,lon], tarea[lat,lon].": "(resp = {error: [0,400,500], mensaje: \"texto de respuesta\"}), inspector [lat,lon], tarea[lat,lon].",
    "Guarda la ubicacion del inspector cada X tiempo dependiendo de la APP, esto es cuando inicia el seguimiento. Si tiene otra tarea con seguimiento (cambio la tarea a seguir), se dejara esas tareas en estado 2.": "Guarda la ubicacion del inspector cada X tiempo dependiendo de la APP, esto es cuando inicia el seguimiento. Si tiene otra tarea con seguimiento (cambio la tarea a seguir), se dejara esas tareas en estado 2.",
    "Id del inspector para guardar su ubicación.": "Id del inspector para guardar su ubicación.",
    "Id de la tarea del inspector.": "Id de la tarea del inspector.",
    "Latitud.": "Latitud.",
    "Longitud.": "Longitud.",
    "(resp = {error: [0,400,500], mensaje: \"texto de respuesta\"}).": "(resp = {error: [0,400,500], mensaje: \"texto de respuesta\"}).",
    "Envia a segured cada X tiempo request que no dependan del usuario sobre las acciones requeridas para registrar usuario, aceptar tareas, confirmar rutas, etc.": "Envia a segured cada X tiempo request que no dependan del usuario sobre las acciones requeridas para registrar usuario, aceptar tareas, confirmar rutas, etc."
}