// Instanciamiento de npm's requeridos para iniciar el servidor de node
var express = require('express'),
    session = require('express-session'),
    path = require('path'),
    favicon = require('serve-favicon'),
    logger = require('morgan'),
    cookieParser = require('cookie-parser'),
    bodyParser = require('body-parser'),
    // NodeGeocoder: es utilizado para realizar la geo decodificacion y codificacion de lat-lon o direccion.
    NodeGeocoder = require('node-geocoder'),
    // Mysql: es la instancia de mysql global.
    mysql = require('mysql'),
    helmet = require('helmet'),
    // Cluster: es para realziar un cluster de servidor conectados por express.
    cluster = require('express-cluster'),
    // schedule: es usado para crear crons.
    schedule = require('node-schedule'),
    // Request: es utilizado para realizar las llamadas get y post hacia otros servicios o servicios internos.
    request = require('request'),
    wait = require('wait.for'),
    // ArrowDB: se utiliza para enviar las notificaciones Push por el servicio de arrow.
    ArrowDB = require('arrowdb');
// Define en las variables del enviroment el TimeZone a utc.
process.env.TZ = 'utc';

cluster(function(worker) {
  var app = express();
  var port = process.env.APP_PORT;
  // Definicio de rutas.
  var index     = require('./routes/index');
  var api       = require('./routes/v1');
  var segured   = require('./routes/segured');

  //http://expressjs.com/es/advanced/best-practice-security.html
  app.use(helmet());
  app.disable('x-powered-by');
  // La definicion de la sesision es mas que nada para evitar la colision de sesiones en node
  app.use(session({
    secret: 'c-r-34605$ee$$$10nNn_api',
    resave: true,
    saveUninitialized: true
  }));
  // Se define la direccion de las vistas a utilizar.
  app.set('views', __dirname + '/views');
  // Se define el tipo de framework usado para las vistas
  app.set('view engine', 'ejs');

  // uncomment after placing your favicon in /public
  //app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
  app.use(logger('dev'));
  // El body parser es utilizado para limitar la cantidad de bite's que puede reconocer, esto nos permite manejar grandes Request enviados por la App.
  app.use(bodyParser.urlencoded({ extended: false,limit: '2gb' }));
  app.use(bodyParser.json({ extended: false,limit: '2gb' }));
  app.use(cookieParser());
  app.use(express.static(path.join(__dirname, 'public')));

  // Las rutas para llegar a los servicios, se definen antes por temas de orden requeridos por expressJs.
  app.use('/', index);
  app.use('/api/', api);
  app.use('/segured/', segured);

  // catch 404 and forward to error handler
  app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
  });
  // error handler
  app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = process.env.START_TYPE === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
  });
  process.env.UV_THREADPOOL_SIZE = 128;
  // Cronn para colaSalida, llamamos directamente desde aca para evitar usar otro servidor. mas info de como se utiliza el cron en https://www.npmjs.com/package/node-schedule
  wait.launchFiber(schedule.scheduleJob,'*/240 * * * * *',function(){//cada 2 minutos
      request({
      timeout: 20000,
      url: "http://127.0.0.1:9999/api/colaSalida",
      method: 'POST'
    }, function(error, response, body) {
      //console.log("=============================================");
      if ((!error || error === null) && response.statusCode == 200) {
        //console.log(body.ERROR);
        if (body === null) {
          console.log("ColaSalida: El servicio retorno null");
        } else {
        //  console.log(`ColaSalida: Enviado respondio => ${body}`);
        }
        //res.send(body);
      } else {
        console.log(`ColaSalida: ${error}`);
      };
    });
  });
  //Cronn para enviar inspecciones, llamamos directamente desde aca para evitar usar otro servidor. mas info de como se utiliza el cron en https://www.npmjs.com/package/node-schedule
  wait.launchFiber(schedule.scheduleJob,'*/1 * * * *',function(){// cada 10 minutos
      request({
      timeout: 20000,
      url: "http://127.0.0.1:9999/api/enviarInspeccion",
      method: 'POST'
    }, function(error, response, body) {
      //console.log("=============================================");
      if ((!error || error === null) && response.statusCode == 200) {
        //console.log(body.ERROR);
        if (body === null) {
          console.log("enviarInspeccion: El servicio retorno null");
        } else {
          // console.log(`enviarInspeccion: Enviado respondio => ${body}`);
        }
        //res.send(body);
      } else {
        console.log(`enviarInspeccion: error ${error}`);
      };
    });
  });
  // Cronn para enviar imagenes, llamamos directamente desde aca para evitar usar otro servidor. mas info de como se utiliza el cron en https://www.npmjs.com/package/node-schedule
  wait.launchFiber(schedule.scheduleJob,'*/1 * * * *',function(){// cada 1 minutos
      request({
      timeout: 200000,
      url: "http://127.0.0.1:9999/api/enviarImagenes",
      method: 'POST'
    }, function(error, response, body) {
      //console.log("=============================================");
      if ((!error || error === null) && response.statusCode == 200) {
        //console.log(body.ERROR);
        if (body === null) {
          console.log("enviarInspeccion: El servicio retorno null");
        } else {
          //console.log(`enviarInspeccion: Enviado respondio => ${body}`);
        }
        //res.send(body);
      } else {
        console.log(`enviarInspeccion: error ${error}`);
      };
    });
  });
  // Aca creamos el servidor, escuchando el puerto especificado en el package.json
  // Importante: Recordar que todo lo que diga process.env. es una variable definido en package.json, esto dado que en amazon podemos setearlos a gusto independiente del package.js
  return app.listen(port, function () {
    console.log(`T: ${new Date().toLocaleString()} | EXPRESS (${process.env.START_TYPE}): server listening on port ${port}`);
    console.log(`SERVIDOR INICIADO`);
    // Seteamos el link de segured, esto para tenerlo de forma "global", solo se puede utilizar cuando se realiza un llamado a los servicios.
    app.set('segured_link',process.env.SEGURED_LINK);
    // Seteamos el objeto de arrow para enviar notificaciones push.
    arrowDBApp = new ArrowDB(process.env.ARROW_KEY_INSPECTOR); // app inspector vieja en ARROW_KEY
    arrowDBApp_S = new ArrowDB(process.env.ARROW_KEY_SCL); // app servicio al cliente
    app.set('_push',arrowDBApp);
    app.set('_push_scl',arrowDBApp_S); // 22-3-18 separada arrow_key de inspector con app scl
    // Seteamos la conexion a la base de datos.
    var options = {
        host: process.env.RDS_HOSTNAME,
        port: process.env.RDS_PORT,
        user: process.env.RDS_USERNAME,
        password: process.env.RDS_PASSWORD,
        database: process.env.RDS_NAME
    };
    app.set('_conn',mysql.createConnection(options));
    // Seteamos la configuracion de u-adjust, es mas que nada para tener un orden. Tambien dentro de esta tenemos la google key si es necesaria.
    app.set('config',{
      nombre: "U-Adjust",
      google: { key : process.env.GOOGLE_KEY},
      push: {
        canal_uadjust: process.env.CANAL_UADJUST,
        canal_uadjust_s: process.env.CANAL_UADJUST_S
      }
    });
    // Seteamos el objeto geocoder para poder ser utilizado por los servicios.
    app.set('geocoder',NodeGeocoder({
      provider: 'google',
      // Optional depending on the providers
      httpAdapter: 'https', // Default
      apiKey: process.env.GOOGLE_KEY, // for Mapquest, OpenCage, Google Premier
      formatter: null // 'gpx', 'string', ...
    }));
  });
  // Al final creamos el cluster del servidor.
}, {count: process.env.CLUSTER});

// module.exports = app;
