# WEB SERVICE U-ADJUST PARA APP

Servicios de comunicación entre el servidor de U-Adjust/Segured y la aplicación.

## Descripción :
Funciones requeridas para el funcionamiento de la aplicación U-Adjust.

## Formato de los servicios

- Envian en formato JSON.
- Recibe en formato JSON
- Método POST.
- Protocolo x-www-form-urlencoded o RAW (aplication/json) en body.

## Tecnologia

- Los servicios son base a NODE.JS con motor V8 de Google.
- Express.js para la estructura y separacion de contenidos/instancias y generacion de rutas.
- wait.for para separar las instancias de cada usuario (fiber) y programar sin la necesidad de la logica async.
- async para generar llamadas paralelas.
- body-parser para parsear las llamadas (get/post) automaticamente.
- compare-lat-lon para comprarar entre dos latitudes y longitudes.
- connect-redis para guardar sesiones en el backend.
- cookie-parser para parsear las cookies de cada usuario.
- debug para realizar pruebas.
- ejs para mostrar el contenido.
- express-cluster para crear varias instancias de node.
- express-session para controlar las sessiones entre usuarios.
- helmet para agregarle mas seguridad al backend.
- moment para el manejo de fechas.
- mysql para la base de datos.
- node-geocoder para realizar geocoder y geodecoder de la direccion.
- node-pushnotifications para generar notificaciones push hacia la aplicacion.
- request y postman-request para generar llamadas afuera del servidor.
- serve-favicon, necesario para express.
- underscore, libreria mejorada para funciones basicas como for, each, etc. Con varias funcionalidades.

## Instalación

### Requerimientos:
- NODE.JS V4.* o mayor, preferible 7.10.*
- npm (instalado con NODE.JS)
- MariaDB 10.*

### Comandos:
- crear una base de datos llamada segured
- crear usuario u-adjust-node password vacio en la base de datos.
- colocarse en la carpeta principal y ejecutar el comando:
```sh
$ cd path/to/server/
$ npm install
$ npm run _dev_local
```
