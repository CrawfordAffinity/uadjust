var moment = require('moment'),
  wait = require('wait.for'),
  async = require('async'),
  _ = require('underscore');
var self = {
  consultarPaises: (req, res) => {
    var tmp = {
      resp: {
        error: 0,
        mensaje: "",
        paises: []
      }
    }
    // Obtenemos especificamente los datos de los paises necesarios para la App.
    tmp.resp.paises = wait.forMethod(req.app.settings._conn, 'query', `SELECT p.*, i.nombrecomun,i.codigoalfa2,i.codigoalfa3 FROM paises AS p INNER JOIN iso AS i ON i.id = p.idpais WHERE p.estado = 1`);
    // res.send() es para retornar la respuesta que queramos, es necesario que sea un json formado. Este ejemplo se repite varias veces, por tal no lo nombrare mas dentro de este archivo.
    res.send(tmp.resp);
    // Guadarmos en el log la respuesta enviada por si a caso. Este ejemplo se repite varias veces, por tal no lo nombrare mas dentro de este archivo.
    self.log("segured","consultarPaises",tmp.resp,res);
  },
  consultarNivel1: (req, res) => {
    var tmp = {
      resp: {
        error: 0,
        mensaje: "",
        datos: []
      },
      p: req.body.pais,
      paises: []
    }
    // Si tenemos el pais, obtenemos los datos del nivel 1 del pais activos.
    if (_.has(req.body, 'pais')) {
      tmp.paises = wait.forMethod(req.app.settings._conn, 'query', `SELECT * FROM paises AS p INNER JOIN iso AS i ON i.id = p.idpais AND p.id = ${tmp.p} WHERE estado = 1`);
    } else {
    // Si no tenemos el pais, obtenemos todos los niveles activos.
      tmp.paises = wait.forMethod(req.app.settings._conn, 'query', `SELECT * FROM paises AS p INNER JOIN iso AS i ON i.id = p.idpais WHERE estado = 1`);
    }
    let _pais = {};
    // Recorremos los piases
    _.each(tmp.paises, function(valor, id) {
      // A cada pais le asignamos una funcion para obtener los datos del nivel1. de forma paralela.
      _pais[valor.nombrecomun] = function(cb_paises) {
        req.app.settings._conn.query(`SELECT i.* FROM isosubdivision AS i INNER JOIN paises AS p ON p.idpais = i.id_pais AND i.id_pais = ${valor.idpais} WHERE p.estado = 1`, function(err, query) {
          cb_paises(err, query);
        });
      }
    });
    async.parallel(_pais,
      function(err, results) {
        if (err) {
          throw err;
        }
        tmp.resp.datos = results;
        res.send(tmp.resp);
        self.log("segured","consultarNivel1",tmp.resp,res);
      });
  },
  notificacionPush: (req, res) => {
    var tmp = {
      resp: {
        error: 0,
        mensaje: ""
      },
      tipo: req.body.tipo,
      codigo_identificador: req.body.codigo_identificador,
      id_inspector: req.body.id_inspector,
      num_caso: req.body.num_caso,
      pais: req.body.pais,
      nivel_1: req.body.nivel_1,
      nivel_2: req.body.nivel_2,
      nivel_3: req.body.nivel_3,
      nivel_4: req.body.nivel_4,
      nivel_5: req.body.nivel_5,
      mensaje: req.body.mensaje,
      ar_de_tokens: [],
      select: "i.id,i.device_token"
      //select: "i.id,i.device_token,i.device,i.nombre,i.apellido_paterno,i.apellido_materno,i.telefono"
    }
    try {
      // Validamos si el codigo identificador existe.
      if (_.has(req.body, 'codigo_identificador') && !_.isUndefined(req.body.codigo_identificador) && !_.isNull(req.body.codigo_identificador) && !_.isEmpty(req.body.codigo_identificador)) {
        // Validamos si el numero de caso existe.
        if (!_.has(req.body, 'num_caso') || _.isUndefined(req.body.num_caso) || _.isNull(req.body.num_caso)) {
          tmp.resp.error = 400;
          tmp.resp.mensaje = "num_caso no existe.";
        }
        // Si no existe algun error continuamos.
        if (tmp.resp.error == 0) {
          // Si el numero del caso es 0, obtenemos todos los inspectores, si no, obtenemos los que tengan el numero de caso.
          if (tmp.num_caso == 0) {
            tmp.inspectores = wait.forMethod(res.app.settings._conn, 'query', `
            SELECT ${tmp.select}
            FROM inspectores AS i
            WHERE i.codigo_identificador = ?
            AND i.device_token <> ""`,[ tmp.codigo_identificador ]);
          }else {
            tmp.inspectores = wait.forMethod(res.app.settings._conn, 'query', `
            SELECT ${tmp.select}
            FROM inspectores AS i
            INNER JOIN tareas AS t ON t.num_caso = ? AND i.codigo_identificador = ?
            AND i.device_token <> ""`,[ tmp.num_caso,tmp.codigo_identificador ]);
          }
          //Notificacion con mensaje
          // Validamos si el mensaje existe.
          if (!_.has(req.body, 'mensaje') || _.isUndefined(req.body.mensaje) || _.isNull(req.body.mensaje) || _.isEmpty(req.body.mensaje)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "mensaje no existe.";
          }
          // Si no existe algun error continuamos.
          if (tmp.resp.error == 0) {
            // Si el tipo existe, enviamos solo el mensaje de notificacion.
            if (_.has(req.body, 'tipo') && !_.isUndefined(req.body.tipo) && !_.isNull(req.body.tipo) && (req.body.tipo == "1" || req.body.tipo == 1)) {
              //console.log(" ================== solo mensaje... ==================");
              tmp.ar_de_tokens = _.pluck(tmp.inspectores,'device_token');
              //console.log(tmp.ar_de_tokens);
              // Enviamos la notificacion push.
              let enviado = wait.forMethod(self,'enviarPush',res,tmp.ar_de_tokens,res.app.settings.config.push.canal_uadjust,'U-Adjust',tmp.mensaje,false);
              tmp.resp = enviado;
              // ingresamos cada notificacion enviada al historial de notificaciones push.
              _.each(tmp.inspectores,function(valor,id){
                  tmp.inspectores = wait.forMethod(res.app.settings._conn, 'query', `
                  INSERT INTO push
                  (num_caso,id_inspector,tipo,texto,fecha)
                  VALUES
                  (?,?,?,?,now())`,[tmp.num_caso,valor.id,tmp.tipo,tmp.mensaje]);
              });
            // Si el tipo existe, tenemos que enviarle un mensaje para que confirme las tareas.
            } else {
              //console.log(" ================== mensaje por debajo... ==================");
              tmp.ar_de_tokens = _.pluck(tmp.inspectores,'device_token');
              //console.log(tmp.ar_de_tokens);
              // Enviamos la notificacion push.
              let enviado = wait.forMethod(self,'enviarPush',res,tmp.ar_de_tokens,res.app.settings.config.push.canal_uadjust,'U-Adjust',tmp.mensaje,true);
              tmp.resp = enviado;
              // ingresamos cada notificacion enviada al historial de notificaciones push.
              _.each(tmp.inspectores,function(valor,id){
                tmp.inspectores = wait.forMethod(res.app.settings._conn, 'query', `
                INSERT INTO push
                (num_caso,id_inspector,tipo,texto,fecha)
                VALUES
                (?,?,?,?,now())`,[tmp.num_caso,valor.id,tmp.tipo,tmp.mensaje]);
              });
            }
          }
        }
      // Si el codigo identificador no existe, verificamos si existe el id_inspector
      } else if (_.has(req.body, 'id_inspector') && !_.isUndefined(req.body.id_inspector) && !_.isNull(req.body.id_inspector)) {
        // Verificamos si el num_caso existe.
        if (!_.has(req.body, 'num_caso') || _.isUndefined(req.body.num_caso) || _.isNull(req.body.num_caso)) {
          tmp.resp.error = 400;
          tmp.resp.mensaje = "num_caso no existe.";
        }
        // Si no existen errores, continuamos
        if (tmp.resp.error == 0) {
          // Si el numero del caso es 0, obtenemos solamente los datos del inspector por su id, si no, buscamos por el id del inspector con el numero del caso.
          if (tmp.num_caso == 0) {
            tmp.inspectores = wait.forMethod(res.app.settings._conn, 'query', `SELECT
            ${tmp.select}
            FROM inspectores AS i
            WHERE i.id = ?
            AND i.device_token <> ""`,[tmp.id_inspector]);
          } else {
            tmp.inspectores = wait.forMethod(res.app.settings._conn, 'query', `SELECT
            ${tmp.select}
            FROM inspectores AS i
            LEFT JOIN tareas AS t ON t.id_inspector = i.id
            WHERE i.id = ?
            AND t.num_caso = ?
            AND t.id_inspector = ?
            AND t.estado_tarea BETWEEN 2 AND 4
            AND i.device_token <> ""`,[tmp.id_inspector,tmp.num_caso,tmp.id_inspector]);
          }
          //Notificacion con mensaje
          // Si el mensaje no existe
          if (!_.has(req.body, 'mensaje') || _.isUndefined(req.body.mensaje) || _.isNull(req.body.mensaje) || _.isEmpty(req.body.mensaje)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "mensaje no existe.";
          }
          // Si no existe ningun error continuamos
          if (tmp.resp.error == 0) {
            // Si el tipo existe, enviamos solo el mensaje de notificacion.
            if (_.has(req.body, 'tipo') && !_.isUndefined(req.body.tipo) && !_.isNull(req.body.tipo) && (req.body.tipo == 1 || req.body.tipo == "1")) {
              //selecciono todos los inspectores que cumplan estos datos,
              //ingreso y ejecuto la notificacion push
              // console.log(" ================== solo mensaje id_inspector... ==================");
              tmp.ar_de_tokens = _.pluck(tmp.inspectores,'device_token');
              // console.log(tmp.ar_de_tokens);
              // Enviamos la notificacion push.
              let enviado = wait.forMethod(self,'enviarPush',res,tmp.ar_de_tokens,res.app.settings.config.push.canal_uadjust,'U-Adjust',tmp.mensaje,false);
              tmp.resp = enviado;
              // ingresamos cada notificacion enviada al historial de notificaciones push.
              _.each(tmp.inspectores,function(valor,id){
                tmp.inspectores = wait.forMethod(res.app.settings._conn, 'query', `
                INSERT INTO push
                (num_caso,id_inspector,tipo,texto,fecha)
                VALUES
                (?,?,?,?,now())`,[tmp.num_caso,valor.id,tmp.tipo,tmp.mensaje]);
              });
            } else {
              // console.log(" ================== mensaje por debajo... ==================");
              // Creamos un array de device token de todos los usuarios a quienes se les envian las notificaciones push.
              tmp.ar_de_tokens = _.pluck(tmp.inspectores,'device_token');
              // console.log(tmp.ar_de_tokens);
              // Enviamos la notificacion push.
              let enviado = wait.forMethod(self,'enviarPush',res,tmp.ar_de_tokens,res.app.settings.config.push.canal_uadjust,'U-Adjust',tmp.mensaje,true);
              tmp.resp = enviado;
              // Ingresamos cada notificacion enviada al historial de notificaciones push
              _.each(tmp.inspectores,function(valor,id){
                tmp.inspectores = wait.forMethod(res.app.settings._conn, 'query', `
                INSERT INTO push
                (num_caso,id_inspector,tipo,texto,fecha)
                VALUES
                (?,?,?,?,now())`,[tmp.num_caso,valor.id,tmp.tipo,tmp.mensaje]);
              });
            }
          }
        }
      } else if (_.has(req.body, 'pais') && !_.isUndefined(req.body.pais) && !_.isNull(req.body.pais) && !_.isEmpty(req.body.pais)) {//Si es una notificacion por pais
        // Creamos la query dependiendo de los datos que tengamos
        let query = `SELECT
        ${tmp.select}
        FROM inspectores AS i
        WHERE i.pais_google = '${tmp.pais}' AND i.device_token <> ""`;
        // Agregamos a la query los AND niveles segun lo que se requiera
        query += (_.has(req.body, 'nivel_1') && !_.isUndefined(req.body.nivel_1) && !_.isNull(req.body.nivel_1) && !_.isEmpty(req.body.nivel_1)) ? `AND i.nivel_1_google = '${tmp.nivel_1}'`: "";
        query += (_.has(req.body, 'nivel_2') && !_.isUndefined(req.body.nivel_2) && !_.isNull(req.body.nivel_2) && !_.isEmpty(req.body.nivel_2)) ? `AND i.nivel_2_google = '${tmp.nivel_2}'`: "";
        query += (_.has(req.body, 'nivel_3') && !_.isUndefined(req.body.nivel_3) && !_.isNull(req.body.nivel_3) && !_.isEmpty(req.body.nivel_3)) ? `AND i.nivel_3_google = '${tmp.nivel_3}'`: "";
        query += (_.has(req.body, 'nivel_4') && !_.isUndefined(req.body.nivel_4) && !_.isNull(req.body.nivel_4) && !_.isEmpty(req.body.nivel_4)) ? `AND i.nivel_4_google = '${tmp.nivel_4}'`: "";
        query += (_.has(req.body, 'nivel_5') && !_.isUndefined(req.body.nivel_5) && !_.isNull(req.body.nivel_5) && !_.isEmpty(req.body.nivel_5)) ? `AND i.nivel_5_google = '${tmp.nivel_5}'`: "";
        if (tmp.resp.error == 0) {
          // Obtenemos los inspectores.
          tmp.inspectores = wait.forMethod(res.app.settings._conn, 'query', query);
          //Notificacion con mensaje, si no esta difinido se retorna error.
          if (!_.has(req.body, 'mensaje') || _.isUndefined(req.body.mensaje) || _.isNull(req.body.mensaje) ||  _.isEmpty(req.body.mensaje)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "mensaje no existe.";
          }
          if (tmp.resp.error == 0) {//Si no existe error se continua
            if (_.has(req.body, 'tipo') && !_.isUndefined(req.body.tipo) && !_.isNull(req.body.tipo) && (req.body.tipo == 1 || req.body.tipo == "1")) {//Si tiene tipo definido en 1, se envia solo el mensaje
              // console.log(" ================== solo mensaje... ==================");
              // Creamos el array de device_token de los inspectores seleccionados
              tmp.ar_de_tokens = _.pluck(tmp.inspectores,'device_token');
              // Enviamos las notificaciones push.
              let enviado = wait.forMethod(self,'enviarPush',res,tmp.ar_de_tokens,res.app.settings.config.push.canal_uadjust,'U-Adjust',tmp.mensaje,false);
              tmp.resp = enviado;
              // Ingresamos cada notificacion enviada al historial de notificaciones push
              _.each(tmp.inspectores,function(valor,id){
                tmp.inspectores = wait.forMethod(res.app.settings._conn, 'query', `
                INSERT INTO push
                (num_caso,id_inspector,tipo,pais,nivel_1,nivel_2,nivel_3,nivel_4,nivel_5,texto,fecha)
                VALUES
                (?,?,?,?,?,?,?,?,?,?,now())`,[tmp.num_caso,valor.id,tmp.tipo,tmp.pais,tmp.nivel_1,tmp.nivel_2,tmp.nivel_3,tmp.nivel_4,tmp.nivel_5,tmp.mensaje]);
              });
            } else {
              // console.log(" ================== mensaje por debajo... ==================");
              // Creamos el array de device_token de los inspectores seleccionados
              tmp.ar_de_tokens = _.pluck(tmp.inspectores,'device_token');
              // Enviamos las notificaciones push.
              let enviado = wait.forMethod(self,'enviarPush',res,tmp.ar_de_tokens,res.app.settings.config.push.canal_uadjust,'U-Adjust',tmp.mensaje,true);
              tmp.resp = enviado;
              // Ingresamos cada notificacion enviada al historial de notificaciones push
              _.each(tmp.inspectores,function(valor,id){
                tmp.inspectores = wait.forMethod(res.app.settings._conn, 'query', `
                INSERT INTO push
                (num_caso,id_inspector,tipo,pais,nivel_1,nivel_2,nivel_3,nivel_4,nivel_5,texto,fecha)
                VALUES
                (?,?,?,?,?,?,?,?,?,?,now())`,[tmp.num_caso,valor.id,tmp.tipo,tmp.pais,tmp.nivel_1,tmp.nivel_2,tmp.nivel_3,tmp.nivel_4,tmp.nivel_5,tmp.mensaje]);
              });
            }
          }
        }
      } else {
        tmp.resp.error = 400;
        tmp.resp.mensaje = "id_inspector no existe.";
      }
    } catch (e) {
      tmp.resp.error = 500;
      tmp.resp.mensaje = "Error al ingresar la notificacion push : " + e;
    } finally {
      res.send(tmp.resp);
      self.log("segured","notificacionPush",tmp.resp,res);
    }

  },
  notificacionPushSolo: (req, res) => {
    var tmp = {
      resp: {
        error: 0,
        mensaje: ""
      },
      id_inspector: req.body.id_inspector,
      tipo: req.body.tipo,
      mensaje: req.body.mensaje,
      ar_de_tokens: [],
      select: "id,device_token"
      //select: "i.id,i.device_token,i.device,i.nombre,i.apellido_paterno,i.apellido_materno,i.telefono"
    }
    // Validamos los recibidos, si no existe alguno, retornamos error 400 y mensaje.
    if (!_.has(req.body, 'id_inspector') || _.isUndefined(req.body.id_inspector) || _.isNull(req.body.id_inspector)) {
      tmp.resp.error = 400;
      tmp.resp.mensaje = "id_inspector no existe.";
    }
    if (!_.has(req.body, 'tipo') || _.isUndefined(req.body.tipo) || _.isNull(req.body.tipo)) {
      tmp.resp.error = 400;
      tmp.resp.mensaje = "tipo no existe.";
    }
    if (!_.has(req.body, 'mensaje') || _.isUndefined(req.body.mensaje) || _.isNull(req.body.mensaje) || _.isEmpty(req.body.mensaje)) {
      tmp.resp.error = 400;
      tmp.resp.mensaje = "mensaje no existe.";
    }
    if (tmp.resp.error == 0) { // Si no existe error continuamos
      // Obtenemos el inspector con el id enviado.
      tmp.inspectores = wait.forMethod(res.app.settings._conn, 'query', `SELECT ${tmp.select} FROM inspectores WHERE id = ?`,[tmp.id_inspector]);
      if (tmp.tipo == 1 || tmp.tipo == "1") {//Si es un mensaje de tipo 1
        // console.log(" ================== solo mensaje... ==================");
        // Creamos el array de device_token del usuario
        tmp.ar_de_tokens = _.pluck(tmp.inspectores,'device_token');
        // Enviamos la notificacion push
        let enviado = wait.forMethod(self,'enviarPush',res,tmp.ar_de_tokens,res.app.settings.config.push.canal_uadjust,'Notificacion',tmp.mensaje,false);
        tmp.resp = enviado;
        // Guardamos en la tabla push el registro del envio.
        _.each(tmp.inspectores,function(valor,id){
          tmp.inspectores = wait.forMethod(res.app.settings._conn, 'query', `
          INSERT INTO push
          (id_inspector,tipo,texto,fecha)
          VALUES
          (?,?,?,now())`,[tmp.id_inspector,tmp.tipo,tmp.mensaje]);
        });
      } else {//Si es un mensaje de tipo 0 (por debajo)
        // console.log(" ================== mensaje por debajo... ==================");
        // Creamos el array de device_token del usuario
        tmp.ar_de_tokens = _.pluck(tmp.inspectores,'device_token');
        // Enviamos la notificacion push
        let enviado = wait.forMethod(self,'enviarPush',res,tmp.ar_de_tokens,res.app.settings.config.push.canal_uadjust,'Notificacion',tmp.mensaje,true);
        tmp.resp = enviado;
        // Guardamos en la tabla push el registro del envio.
        _.each(tmp.inspectores,function(valor,id){
          tmp.inspectores = wait.forMethod(res.app.settings._conn, 'query', `
          INSERT INTO push
          (id_inspector,tipo,texto,fecha)
          VALUES
          (?,?,?,now())`,[tmp.id_inspector,tmp.tipo,tmp.mensaje]);
        });
      }
      res.send(tmp.resp);
      self.log("segured","notificacionPushSolo",tmp.resp,res);
    } else {
      res.send(tmp.resp);
      self.log("segured","notificacionPushSolo",tmp.resp,res);
    }
  },
  /**
  * realiza la accion de enviar el push al cliente
  *
  * @name enviarPush
  * @method
  * @param {String} device_token - Device token del usuario (telefono).
  * @param {String} canal - Canal del push.
  * @param {String} titulo - Titulo de la notificacion push.
  * @param {String} alerta - Mensaje de la notificacion push.
  * @param {Boolean} confirmacion - Si requiere confirmacion.
  * @param {function} cb - Callback.
  * @private
  */
  enviarPush: (res,device_token,canal,titulo,alerta,confirmacion,cb) => {
    // Creamos la estructura a enviar, esto existe en la documentacion de titanium para enviar notificaciones por medio de arrow.
    let datos = {}, tokens = "";
    tokens = device_token.join(",");
    datos = {
        to_tokens: tokens,
        channel: canal,
        payload: {
            "alert": alerta,
            "sound": "none",
            "title": titulo,
            "vibrate": true,
            "confirmacion": confirmacion
        }
    };
    // Enviamos la notificacion push.
    res.app.settings._push.pushNotificationsNotifyTokens(datos, function(err, result) {
        if (err) {//Si retorno un error, se realiza el callback correspondiente
          if (err.statusCode == 422) {
            cb(false,{error:500,mensaje:"Device token esta vacio."});
          } else {
            cb(false,{error:500,mensaje:err.message});
          }
        } else {//Si error no existe, el push se envio correctamente y se terorna los datos correspondiente.
          // console.log("Notificacion enviada" + JSON.stringify(result));
          if (result.statusCode == 200 && result.reason != "OK") {//Si no fue un error, pero nos retorna un statusCode 200 como error y la razon es diferente a ok, entregamos esa respuesta.
            cb(false,{error:500,mensaje:result.reason});
          } else {//Si no, la respuesta esta vacia, lo cual indica que se realizo sin problemas.
            cb(false,{error:0,mensaje:""});
          }
        }
    });
  },
  notificacionPush_s: (req, res) => {
    var tmp = {
      resp: {
        error: 0,
        mensaje: ""
      },
      device_token: req.body.device_token,
      mensaje: req.body.mensaje
    }
    // Validamos los datos entregados para enviar la notificacion push.
    if (!_.has(req.body, 'device_token') || _.isUndefined(req.body.device_token) || _.isNull(req.body.device_token) || _.isEmpty(req.body.device_token)) {
      tmp.resp.error = 400;
      tmp.resp.mensaje = "device_token no existe.";
    }
    if (!_.has(req.body, 'mensaje') || _.isUndefined(req.body.mensaje) || _.isNull(req.body.mensaje) || _.isEmpty(req.body.mensaje)) {
      tmp.resp.error = 400;
      tmp.resp.mensaje = "mensaje no existe.";
    }
    if (tmp.resp.error == 0) {//Si no existe error
      // Se envia la notificacion push.
      tmp.resp = wait.forMethod(self,'enviarPush_s',res,tmp.device_token,res.app.settings.config.push.canal_uadjust_s,'U-Adjust-S',tmp.mensaje);
      res.send(tmp.resp);
    } else {
      res.send(tmp.resp);
    }

  },
  /**
   * Enviar push a u-adjust-s.
   *
   * @name enviarPush_s
   * @method
   * @param {string} device_token - Device token a enviar push.
   * @param {string} canal - Canal del push.
   * @param {string} titulo - Titulo del push.
   * @param {string} alerta - Alerta del push.
   * @param {function} cb - Callback.
   * @example
    {
      "mensaje":1,
      "mensaje":"Notificacion push para u-adjust scl!!!"
    }
   * @returns {object} resp = [0,400,500], mensaje = [texto de respuesta].
   * @private
   */
  enviarPush_s: (res,device_token,canal,titulo,alerta,cb) => {
    // Creamos la estructura a enviar.
    datos = {
        to_tokens: device_token,
        channel: canal,
        payload: {
            "alert": alerta,
            "sound": "none",
            "title": titulo,
            "vibrate": true
        }
    }
    // enviamos la notificacion push.
    res.app.settings._push_scl.pushNotificationsNotifyTokens(datos, function(err, result) {
        if (err) {//Si existe algun error se retorna los callbacks correspondientes a los errores.
          if (err.statusCode == 422) {
            cb(false,{error:500,mensaje:"Device token esta vacio."});
          } else {
            cb(false,{error:500,mensaje:err.message});
          }
        } else {//Si no, se retorna el mensaje que corresponda al exito
          if (result.statusCode == 200 && result.reason != "OK") {//Si se envio pero nos retorno un statusCode 200, quiere decir que hubo un error y este se retorna como corresponde.
            cb(false,{error:500,mensaje:result.reason});
          } else {
            cb(false,{error:0,mensaje:""});
          }
        }
    });
  },
  bloquearActivarInspector: (req, res) => {
    //TODO: Enviar correo??? - 11-05-2017 : ellos envian el correo.
    var tmp = {
      resp: {
        error: 0,
        mensaje: ""
      },
      i: req.body.id_inspector,
      c: req.body.codigo_identificador,
      p: req.body.password,
      e: req.body.estado,
      helper: {}
    }
    // if (tmp.i == undefined || tmp.i == "" ) {
    //   tmp.resp.error = 400;
    //   tmp.resp.mensaje = "El id no existe";
    // }
    // Validamos que el estado exista.
    if (!_.has(req.body, 'estado') || _.isUndefined(req.body.estado) || _.isNull(req.body.estado)) {
      tmp.resp.error = 400;
      tmp.resp.mensaje = "estado no existe.";
    }
    if (tmp.resp.error == 0) { //Si no existe error alguno, se continua.
      if (tmp.e == "1") {//Si el estado es igual a 1, preguntamos si la password existe.
        if (!_.has(req.body, 'password') || _.isUndefined(req.body.password) || _.isNull(req.body.password) || _.isEmpty(req.body.password)) {
          tmp.resp.error = 400;
          tmp.resp.mensaje = "password no existe.";
        }
        if (tmp.resp.error == 0) {//Si no existe error alguno, continuamos
          // Validamos si el id del inspector existe, si es asi actualizamos su estado y su password.
          if (_.has(req.body, 'id_inspector') && !_.isUndefined(req.body.id_inspector) && !_.isNull(req.body.id_inspector)) {
            tmp.helper = wait.forMethod(req.app.settings._conn, 'query', `UPDATE inspectores SET estado = 1, password = ? WHERE id = ?`,[tmp.p,tmp.i]);
          } else if (_.has(req.body, 'codigo_identificador') && !_.isUndefined(req.body.codigo_identificador) && !_.isNull(req.body.codigo_identificador) && !_.isEmpty(req.body.codigo_identificador)) {
            // Si no existe el id_inspector, deberia existir el codigo_identificador, con esto actualizamos el estado y la password del inspector.
            tmp.helper = wait.forMethod(req.app.settings._conn, 'query', `UPDATE inspectores SET estado = 1, password = ? WHERE codigo_identificador = ?`,[tmp.p,tmp.c]);
          } else { //Si no existe ninguno, retornamos el error correspondiente
            tmp.resp.error = 400;
            tmp.resp.mensaje = "id_inspector y codigo_identificador no existen para el estado 1.";
          }
        }
      } else {//Si es del tipo 2, preguntamos si existe el id del inspector o el codigo identificador
        if (_.has(req.body, 'id_inspector') && !_.isUndefined(req.body.id_inspector) && !_.isNull(req.body.id_inspector)) {
          tmp.helper = wait.forMethod(req.app.settings._conn, 'query', `UPDATE inspectores SET estado = 2 WHERE id = ?`,[tmp.i]);
        } else if (_.has(req.body, 'codigo_identificador') && !_.isUndefined(req.body.codigo_identificador) && !_.isNull(req.body.codigo_identificador) && !_.isEmpty(req.body.codigo_identificador)) {
          // Si no existe el id_inspector, deberia existir el codigo_identificador, con esto actualizamos el estado y la password del inspector.
          tmp.helper = wait.forMethod(req.app.settings._conn, 'query', `UPDATE inspectores SET estado = 2 WHERE codigo_identificador = ?`,[tmp.c]);
        } else {
          tmp.resp.error = 400;
          tmp.resp.mensaje = "id_inspector y codigo_identificador no existen para el estado 2 ó 0.";
        }
      }
      if (tmp.helper.affectedRows == 0) {
        tmp.resp.error = 500;
        tmp.resp.mensaje = "El inspector no sufrio cambios.";
      }
      res.send(tmp.resp);
      self.log("segured","bloquearActivarInspector",tmp.resp,res);
    } else {
      res.send(tmp.resp);
      self.log("segured","bloquearActivarInspector",tmp.resp,res);
    }
  },
  editarInspector: (req, res) => {
    var tmp = {
      resp: {
        error: 0,
        mensaje: ""
      },
      id_inspector: req.body.id_inspector,
      codigo_identificador: req.body.codigo_identificador,
      tipo_cambio: req.body.tipo_cambio,
      datos: req.body.datos
    };
    // Validamos que los datos vengan definidos
    if (!_.has(req.body, 'tipo_cambio') || _.isUndefined(req.body.tipo_cambio) || _.isNull(req.body.tipo_cambio)) {
      tmp.resp.error = 400;
      tmp.resp.mensaje = "tipo_cambio no existe.";
    }
    if (!_.has(req.body, 'datos') || _.isUndefined(req.body.datos) || _.isNull(req.body.datos) ||  _.isEmpty(req.body.datos)) {
      tmp.resp.error = 400;
      tmp.resp.mensaje = "datos no existe.";
    }
    try {
      if (tmp.resp.error == 0) {//Si no existe ningun error
        switch (tmp.tipo_cambio) {//Validamos el tipo de cambio a realizar (1= domicilio, 2= disponibilidad)
          case "1": //domicilio
            if (!_.has(req.body, 'pais') || _.isUndefined(req.body.pais) || _.isNull(req.body.pais) ||  _.isEmpty(req.body.pais)) {
              tmp.resp.error = 400;
              tmp.resp.mensaje = "pais no existe.";
            }
            if (!_.has(req.body, 'nivel_1') || _.isUndefined(req.body.nivel_1) || _.isNull(req.body.nivel_1) ||  _.isEmpty(req.body.nivel_1)) {
              tmp.resp.error = 400;
              tmp.resp.mensaje = "nivel_1 no existe.";
            }
            if (!_.has(req.body, 'nivel_2') || _.isUndefined(req.body.nivel_2) || _.isNull(req.body.nivel_2) ||  _.isEmpty(req.body.nivel_2)) {
              tmp.resp.error = 400;
              tmp.resp.mensaje = "nivel_2 no existe.";
            }
            if (!_.has(req.body, 'nivel_3') || _.isUndefined(req.body.nivel_3) || _.isNull(req.body.nivel_3) ||  _.isEmpty(req.body.nivel_3)) {
              tmp.resp.error = 400;
              tmp.resp.mensaje = "nivel_3 no existe.";
            }
            if (!_.has(req.body, 'nivel_4') || _.isUndefined(req.body.nivel_4) || _.isNull(req.body.nivel_4) ||  _.isEmpty(req.body.nivel_4)) {
              tmp.resp.error = 400;
              tmp.resp.mensaje = "nivel_4 no existe.";
            }
            if (!_.has(req.body, 'nivel_5') || _.isUndefined(req.body.nivel_5) || _.isNull(req.body.nivel_5) ||  _.isEmpty(req.body.nivel_5)) {
              tmp.resp.error = 400;
              tmp.resp.mensaje = "nivel_5 no existe.";
            }
            if (!_.has(req.body, 'direccion') || _.isUndefined(req.body.direccion) || _.isNull(req.body.direccion) ||  _.isEmpty(req.body.direccion)) {
              tmp.resp.error = 400;
              tmp.resp.mensaje = "direccion no existe.";
            }
            if (!_.has(req.body, 'lat_dir') || _.isUndefined(req.body.lat_dir) || _.isNull(req.body.lat_dir) ||  _.isEmpty(req.body.lat_dir)) {
              tmp.resp.error = 400;
              tmp.resp.mensaje = "lat_dir no existe.";
            }
            if (!_.has(req.body, 'lon_dir') || _.isUndefined(req.body.lon_dir) || _.isNull(req.body.lon_dir) ||  _.isEmpty(req.body.lon_dir)) {
              tmp.resp.error = 400;
              tmp.resp.mensaje = "lon_dir no existe.";
            }
            if (!_.has(req.body, 'correccion_direccion') || _.isUndefined(req.body.correccion_direccion) || _.isNull(req.body.correccion_direccion)) {
              tmp.resp.error = 400;
              tmp.resp.mensaje = "correccion_direccion no existe.";
            }
            if (!_.has(req.body, 'id_inspector') || _.isUndefined(req.body.id_inspector) || _.isNull(req.body.id_inspector)) {
              if (tmp.resp.error == 0) {
                let domi = wait.forMethod(res.app.settings._conn, 'query',
                  `UPDATE inspectores SET
                      pais = ?,
                      nivel_1 = ?,
                      nivel_2 = ?,
                      nivel_3 = ?,
                      nivel_4 = ?,
                      nivel_5 = ?,
                      direccion = ?,
                      lat_dir = ?,
                      lon_dir = ?,
                      correccion_direccion = ?
                      WHERE id = ?`,[tmp.datos.pais,tmp.datos.nivel_1,tmp.datos.nivel_2,tmp.datos.nivel_3,tmp.datos.nivel_4,tmp.datos.nivel_5,tmp.datos.direccion,tmp.datos.lat_dir,tmp.datos.lon_dir,tmp.datos.correccion_direccion,tmp.id_inspector]);
                if (domi.affectedRows <= 0) {
                  throw ('Error al ingresar los cambios a domicilio del id_inspector ' + tmp.id_inspector);
                }
              }
            }else if (!_.has(req.body, 'codigo_identificador') || _.isUndefined(req.body.codigo_identificador) || _.isNull(req.body.codigo_identificador)) {
              if (tmp.resp.error == 0) {
                let domi = wait.forMethod(res.app.settings._conn, 'query',
                  `UPDATE inspectores SET
                      pais = ?,
                      nivel_1 = ?,
                      nivel_2 = ?,
                      nivel_3 = ?,
                      nivel_4 = ?,
                      nivel_5 = ?,
                      direccion = ?,
                      lat_dir = ?,
                      lon_dir = ?,
                      correccion_direccion = ?
                      WHERE codigo_identificador = ?`,[tmp.datos.pais,tmp.datos.nivel_1,tmp.datos.nivel_2,tmp.datos.nivel_3,tmp.datos.nivel_4,tmp.datos.nivel_5,tmp.datos.direccion,tmp.datos.lat_dir,tmp.datos.lon_dir,tmp.datos.correccion_direccion,tmp.codigo_identificador]);
                if (domi.affectedRows <= 0) {
                  throw ('Error al ingresar los cambios a domicilio del codigo_identificador ' + tmp.codigo_identificador);
                }
              }
            }else {
              throw("Tiene que existir id_inspector o codigo_identificador no existe.");
            }
            break;
          case "2": //disponibilidad
            //En los datos debe venir disponibilidad (0:part time, 1:full time)
            if (!_.has(req.body, 'disponibilidad') || _.isUndefined(req.body.disponibilidad) || _.isNull(req.body.disponibilidad)) {
              tmp.resp.error = 400;
              tmp.resp.mensaje = "disponibilidad no existe.";
            }
            if (!_.has(req.body, 'disponibilidad_viajar_pais') || _.isUndefined(req.body.disponibilidad_viajar_pais) || _.isNull(req.body.disponibilidad_viajar_pais)) {
              tmp.resp.error = 400;
              tmp.resp.mensaje = "disponibilidad_viajar_pais no existe.";
            }
            if (!_.has(req.body, 'disponibilidad_viajar_ciudad') || _.isUndefined(req.body.disponibilidad_viajar_ciudad) || _.isNull(req.body.disponibilidad_viajar_ciudad)) {
              tmp.resp.error = 400;
              tmp.resp.mensaje = "disponibilidad_viajar_ciudad no existe.";
            }
            if (tmp.resp.error == 0) {
              if (tmp.datos.disponibilidad == '0') {
                if (!_.has(req.body, 'disponibilidad_horas') || _.isUndefined(req.body.disponibilidad_horas) || _.isNull(req.body.disponibilidad_horas)) {
                  tmp.resp.error = 400;
                  tmp.resp.mensaje = "disponibilidad_horas no existe.";
                }
                if (!_.has(req.body, 'd1') || _.isUndefined(req.body.d1) || _.isNull(req.body.d1)) {
                  tmp.resp.error = 400;
                  tmp.resp.mensaje = "d1 no existe.";
                }
                if (!_.has(req.body, 'd2') || _.isUndefined(req.body.d2) || _.isNull(req.body.d2)) {
                  tmp.resp.error = 400;
                  tmp.resp.mensaje = "d2 no existe.";
                }
                if (!_.has(req.body, 'd3') || _.isUndefined(req.body.d3) || _.isNull(req.body.d3)) {
                  tmp.resp.error = 400;
                  tmp.resp.mensaje = "d3 no existe.";
                }
                if (!_.has(req.body, 'd4') || _.isUndefined(req.body.d4) || _.isNull(req.body.d4)) {
                  tmp.resp.error = 400;
                  tmp.resp.mensaje = "d4 no existe.";
                }
                if (!_.has(req.body, 'd5') || _.isUndefined(req.body.d5) || _.isNull(req.body.d5)) {
                  tmp.resp.error = 400;
                  tmp.resp.mensaje = "d5 no existe.";
                }
                if (!_.has(req.body, 'd6') || _.isUndefined(req.body.d6) || _.isNull(req.body.d6)) {
                  tmp.resp.error = 400;
                  tmp.resp.mensaje = "d6 no existe.";
                }
                if (!_.has(req.body, 'd7') || _.isUndefined(req.body.d7) || _.isNull(req.body.d7)) {
                  tmp.resp.error = 400;
                  tmp.resp.mensaje = "d7 no existe.";
                }
                if (!_.has(req.body, 'id_inspector') || _.isUndefined(req.body.id_inspector) || _.isNull(req.body.id_inspector)) {
                  if (tmp.resp.error == 0) {
                    //full time : id_inspector, disponibilidad, disponibilidad_viajar_pais, disponibilidad_viajar_ciudad
                    let disp = wait.forMethod(res.app.settings._conn, 'query',
                      `UPDATE inspectores SET
                        disponibilidad = ?,
                        d1 = ?,
                        d2 = ?,
                        d3 = ?,
                        d4 = ?,
                        d5 = ?,
                        d6 = ?,
                        d7 = ?,
                        disponibilidad_horas = ?,
                        disponibilidad_viajar_pais = ?,
                        disponibilidad_viajar_ciudad = ?
                        WHERE id = ?`,[tmp.datos.disponibilidad,tmp.datos.d1,tmp.datos.d2,tmp.datos.d3,tmp.datos.d4,tmp.datos.d5,tmp.datos.d6,tmp.datos.d7,tmp.datos.disponibilidad_horas,tmp.datos.disponibilidad_viajar_pais,tmp.datos.disponibilidad_viajar_ciudad,tmp.id_inspector]);
                    if (disp.affectedRows <= 0) {
                      throw ('Error al actualizar los datos de disponibilidad del id_inspector ' + tmp.id_inspector);
                    }
                  }
                }else if (!_.has(req.body, 'codigo_identificador') || _.isUndefined(req.body.codigo_identificador) || _.isNull(req.body.codigo_identificador)) {
                  if (tmp.resp.error == 0) {
                    //full time : id_inspector, disponibilidad, disponibilidad_viajar_pais, disponibilidad_viajar_ciudad
                    let disp = wait.forMethod(res.app.settings._conn, 'query', `UPDATE inspectores SET
                                                                    disponibilidad = ?,
                                                                    d1 = ?,
                                                                    d2 = ?,
                                                                    d3 = ?,
                                                                    d4 = ?,
                                                                    d5 = ?,
                                                                    d6 = ?,
                                                                    d7 = ?,
                                                                    disponibilidad_horas = ?,
                                                                    disponibilidad_viajar_pais = ?,
                                                                    disponibilidad_viajar_ciudad = ?
                                                                    WHERE codigo_identificador = ?`,[tmp.datos.disponibilidad,tmp.datos.d1,tmp.datos.d2,tmp.datos.d3,tmp.datos.d4,tmp.datos.d5,tmp.datos.d6,tmp.datos.d7,tmp.datos.disponibilidad_horas,tmp.datos.disponibilidad_viajar_pais,tmp.datos.disponibilidad_viajar_ciudad,tmp.codigo_identificador]);
                    if (disp.affectedRows <= 0) {
                      throw ('Error al actualizar los datos de disponibilidad del codigo_identificador ' + tmp.codigo_identificador);
                    }
                  }
                }else {
                  throw("Tiene que existir id_inspector o codigo_identificador no existe.");
                }

              } else {
                if (!_.has(req.body, 'id_inspector') || _.isUndefined(req.body.id_inspector) || _.isNull(req.body.id_inspector)) {
                  //part time : disponibilidad_fechas[1,2,3,4,5,6,7],disponibilidad_horas (10:00 a 23:00), disponibilidad_viajar_pais, disponibilidad_viajar_ciudad
                  let disp = wait.forMethod(res.app.settings._conn, 'query', `UPDATE inspectores SET
                                                                  disponibilidad = ?,
                                                                  d1 = 1,
                                                                  d2 = 1,
                                                                  d3 = 1,
                                                                  d4 = 1,
                                                                  d5 = 1,
                                                                  d6 = 1,
                                                                  d7 = 1,
                                                                  disponibilidad_horas ?,
                                                                  disponibilidad_viajar_pais = ?,
                                                                  disponibilidad_viajar_ciudad = ?
                                                                  WHERE id = ?`,[tmp.datos.disponibilidad,'00:00 23:00',tmp.datos.disponibilidad_viajar_pais,tmp.datos.disponibilidad_viajar_ciudad,tmp.id_inspector]);
                  if (disp.affectedRows <= 0) {
                    throw ('Error al actualizar los datos de disponibilidad del id_inspector ' + tmp.id_inspector);
                  }
                }else if (!_.has(req.body, 'codigo_identificador') || _.isUndefined(req.body.codigo_identificador) || _.isNull(req.body.codigo_identificador)) {
                  //part time : disponibilidad_fechas[1,2,3,4,5,6,7],disponibilidad_horas (10:00 a 23:00), disponibilidad_viajar_pais, disponibilidad_viajar_ciudad
                  let disp = wait.forMethod(res.app.settings._conn, 'query', `UPDATE inspectores SET
                                                                  disponibilidad = ?,
                                                                  d1 = 1,
                                                                  d2 = 1,
                                                                  d3 = 1,
                                                                  d4 = 1,
                                                                  d5 = 1,
                                                                  d6 = 1,
                                                                  d7 = 1,
                                                                  disponibilidad_horas = ?,
                                                                  disponibilidad_viajar_pais = ?,
                                                                  disponibilidad_viajar_ciudad = ?
                                                                  WHERE codigo_identificador = ?`,[tmp.datos.disponibilidad,'00:00 23:00',tmp.datos.disponibilidad_viajar_pais,tmp.datos.disponibilidad_viajar_ciudad,tmp.codigo_identificador]);
                  if (disp.affectedRows <= 0) {
                    throw ('Error al actualizar los datos de disponibilidad del codigo_identificador ' + tmp.codigo_identificador);
                  }
                }else {
                  throw("Tiene que existir id_inspector o codigo_identificador no existe.");
                }

              }
            }
            break;
          case "3": //contactos
            //Se espera : id_inspector, correo, telefono
            if (!_.has(req.body, 'correo') || _.isUndefined(req.body.correo) || _.isNull(req.body.correo) ||  _.isEmpty(req.body.correo)) {
              tmp.resp.error = 400;
              tmp.resp.mensaje = "correo no existe.";
            }
            if (!_.has(req.body, 'telefono') || _.isUndefined(req.body.telefono) || _.isNull(req.body.telefono) ||  _.isEmpty(req.body.telefono)) {
              tmp.resp.error = 400;
              tmp.resp.mensaje = "telefono no existe.";
            }
            if (!_.has(req.body, 'id_inspector') || _.isUndefined(req.body.id_inspector) || _.isNull(req.body.id_inspector)) {
              if (tmp.resp.error == 0) {
                let conta = wait.forMethod(res.app.settings._conn, 'query', `UPDATE inspectores SET
                                                                correo = ?,
                                                                telefono = ?
                                                                WHERE id = ?`,[tmp.datos.correo,tmp.datos.telefono,tmp.id_inspector]);
                if (conta.affectedRows <= 0) {
                  throw ('Error al actualizar los datos de contacto del id_inspector ' + tmp.id_inspector);
                }
              }
            }else if (!_.has(req.body, 'codigo_identificador') || _.isUndefined(req.body.codigo_identificador) || _.isNull(req.body.codigo_identificador)) {
              if (tmp.resp.error == 0) {
                let conta = wait.forMethod(res.app.settings._conn, 'query', `UPDATE inspectores SET
                                                                correo = ?,
                                                                telefono = ?
                                                                WHERE codigo_identificador = ?`,tmp.datos.correo,tmp.datos.telefono,tmp.codigo_identificador);
                if (conta.affectedRows <= 0) {
                  throw ('Error al actualizar los datos de contacto del codigo_identificador ' + tmp.codigo_identificador);
                }
              }
            }else {
              throw("Tiene que existir id_inspector o codigo_identificador no existe.");
            }
            break;
          default:
            throw ('No existe este tipo de cambio.');
            break;
        }
      }
    } catch (e) {
      //console.log(`error : ${e}`);
      tmp.resp.error = 500;
      tmp.resp.mensaje = "Error al editar el perfil del inspector : " + e;
    } finally {
      res.send(tmp.resp);
      self.log("segured","editarInspector",tmp.resp,res);
    }
  },
  registrarTareas: (req, res) => {
    //TODO: Hay un problema aca, dado que necesito insertar el asegurado, no puedo validar todo altiro, podria, pero que significaria para segured?, tiempo de consumo...
    var tmp = {
      resp: {
        error: 0,
        mensaje: ""
      },
      t: req.body.tareas,
      asegurado: {},
      datos_pais: {}
    }
    if (!_.has(req.body, 'tareas') ||  tmp.t == "") {
      tmp.resp.error = 400;
      tmp.resp.mensaje = "Tareas no existe";
    }
    try {
      if (tmp.resp.error == 0) {
        _.each(tmp.t, function(tareas, elid) {
          if (!_.has(tareas, 'tarea') || _.isUndefined(tareas.tarea) || _.isNull(tareas.tarea) || _.isEmpty(tareas.tarea)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "tarea no existe.";
          }
          if (!_.has(tareas, 'asegurado') || _.isUndefined(tareas.asegurado) || _.isNull(tareas.asegurado) || _.isEmpty(tareas.asegurado)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "asegurado no existe.";
          }
          if (tmp.resp.error == 0) {
            //Hay que validar que tenga los datos dentro (Como es un array...)
            //Existe el asegurado, lo ingresamos.
            if (!_.has(tareas.asegurado, 'codigo_identificador') || _.isUndefined(tareas.asegurado.codigo_identificador) || _.isNull(tareas.asegurado.codigo_identificador) || _.isEmpty(tareas.asegurado.codigo_identificador)) {
              tmp.resp.error = 400;
              tmp.resp.mensaje = "codigo_identificador no existe.";
            }
            if (!_.has(tareas.asegurado, 'nombre') || _.isUndefined(tareas.asegurado.nombre) || _.isNull(tareas.asegurado.nombre) || _.isEmpty(tareas.asegurado.nombre)) {
              tmp.resp.error = 400;
              tmp.resp.mensaje = "nombre no existe.";
            }
            //
            if (_.has(tareas.asegurado, 'tel_fijo') && (_.isUndefined(tareas.asegurado.tel_fijo) || _.isNull(tareas.asegurado.tel_fijo) || _.isEmpty(tareas.asegurado.tel_fijo))) {
              tareas.asegurado.tel_fijo = "";
            }
            if (_.has(tareas.asegurado, 'tel_movil') && (_.isUndefined(tareas.asegurado.tel_movil) || _.isNull(tareas.asegurado.tel_movil) || _.isEmpty(tareas.asegurado.tel_movil))) {
              tareas.asegurado.tel_movil = "";
            }
            if (_.has(tareas.asegurado, 'correo') && (_.isUndefined(tareas.asegurado.correo) || _.isNull(tareas.asegurado.correo) || _.isEmpty(tareas.asegurado.correo))) {
              tareas.asegurado.correo = "";
            }
            if (tmp.resp.error == 0) {
              // 02-10-2017: Validamos si existe algun asegurado, si no existe, registramos uno y guardamos el insertId, si no, asignamos esa variable al tmp.asegurado
              let helper_asegurado = wait.forMethod(res.app.settings._conn,'query',
              `SELECT id FROM asegurado
              WHERE codigo_identificador = ?
              AND nombre = ?
              AND tel_fijo = ?
              AND tel_movil = ?
              AND correo = ?`,[tareas.asegurado.codigo_identificador,tareas.asegurado.nombre,tareas.asegurado.tel_fijo,tareas.asegurado.tel_movil,tareas.asegurado.correo]);
              if (helper_asegurado.length >= 1) {
                tmp.asegurado = { insertId: helper_asegurado[0].id };
              } else {
                tmp.asegurado = wait.forMethod(res.app.settings._conn, 'query', `
                    INSERT INTO asegurado
                    (codigo_identificador,nombre,tel_fijo,tel_movil,correo)
                    VALUES
                    (?,?,?,?,?)`,
                    [tareas.asegurado.codigo_identificador,
                    tareas.asegurado.nombre,
                    tareas.asegurado.tel_fijo,
                    tareas.asegurado.tel_movil,
                    tareas.asegurado.correo]);
                //console.log(tmp.asegurado);
                if (tmp.asegurado.affectedRows <= 0) {
                  tmp.resp.error = 500;
                  tmp.resp.error = "No se logro ingresar los datos del asegurado.";
                }
              }
              if (tmp.resp.error == 0) {
                if (!_.has(tmp.asegurado, 'insertId') || _.isUndefined(tmp.asegurado.insertId) || _.isNull(tmp.asegurado.insertId)) {
                  tmp.resp.error = 400;
                  tmp.resp.mensaje = "Inserted ID no existe (no se logro ingresar el asegurado).";
                }
                if (!_.has(tareas.tarea, 'num_caso') || _.isUndefined(tareas.tarea.num_caso) || _.isNull(tareas.tarea.num_caso)) {
                  tmp.resp.error = 400;
                  tmp.resp.mensaje = "num_caso no existe.";
                }
                if (!_.has(tareas.tarea, 'asegurador') || _.isUndefined(tareas.tarea.asegurador) || _.isNull(tareas.tarea.asegurador) ||  _.isEmpty(tareas.tarea.asegurador)) {
                  tmp.resp.error = 400;
                  tmp.resp.mensaje = "asegurador no existe.";
                }
                if (!_.has(tareas.tarea, 'evento') || _.isUndefined(tareas.tarea.evento) || _.isNull(tareas.tarea.evento)) {
                  tmp.resp.error = 400;
                  tmp.resp.mensaje = "evento no existe.";
                }
                if (!_.has(tareas.tarea, 'lat') || _.isUndefined(tareas.tarea.lat) || _.isNull(tareas.tarea.lat) || _.isEmpty(tareas.tarea.lat)) {
                  tmp.resp.error = 400;
                  tmp.resp.mensaje = "lat no existe.";
                }
                if (!_.has(tareas.tarea, 'lon') || _.isUndefined(tareas.tarea.lon) || _.isNull(tareas.tarea.lon) || _.isEmpty(tareas.tarea.lon)) {
                  tmp.resp.error = 400;
                  tmp.resp.mensaje = "lon no existe.";
                }
                if (!_.has(tareas.tarea, 'categoria') || _.isUndefined(tareas.tarea.categoria) || _.isNull(tareas.tarea.categoria)) {
                  tmp.resp.error = 400;
                  tmp.resp.mensaje = "categoria no existe.";
                } else if (tareas.tarea.categoria == "0") {
                  if (!_.has(tareas.tarea, 'fecha_tarea') || _.isUndefined(tareas.tarea.fecha_tarea) || _.isNull(tareas.tarea.fecha_tarea) || _.isEmpty(tareas.tarea.fecha_tarea)) {
                    tmp.resp.error = 400;
                    tmp.resp.mensaje = "fecha_tarea no existe.";
                  } else if (tareas.tarea.fecha_tarea == "") {
                    tmp.resp.error = 400;
                    tmp.resp.mensaje = "fecha_tarea debe existir para categoria 1.";
                  }
                }
                if (!_.has(tareas.tarea, 'fecha_siniestro') || _.isUndefined(tareas.tarea.fecha_siniestro) || _.isNull(tareas.tarea.fecha_siniestro) || _.isEmpty(tareas.tarea.fecha_siniestro)) {
                  tmp.resp.error = 400;
                  tmp.resp.mensaje = "fecha_siniestro no existe.";
                }
                if (!_.has(tareas.tarea, 'bono') || _.isUndefined(tareas.tarea.bono) || _.isNull(tareas.tarea.bono) || _.isEmpty(tareas.tarea.bono)) {
                  tareas.tarea.bono = 0;
                }
                if (!_.has(tareas.tarea, 'tipo_tarea') || _.isUndefined(tareas.tarea.tipo_tarea) || _.isNull(tareas.tarea.tipo_tarea)) {
                  tmp.resp.error = 400;
                  tmp.resp.mensaje = "tipo_tarea no existe.";
                }
                if (tmp.resp.error == 0) {
                  // tmp.datos_pais = wait.forMethod(res.app.settings._conn,'query',`SELECT i.nombrecomun,i2.subdivision_name
                  // FROM iso AS i
                  // INNER JOIN paises AS p ON i.id = p.idpais
                  // INNER JOIN isosubdivision AS i2 ON i2.id_pais = i.id
                  // WHERE p.id = ${tareas.tarea.pais}
                  // AND i2.id = ${tareas.tarea.nivel_1}`);

                  let help_google = [];
                  //console.log(`${tmp.datos_pais[0].nombrecomun}, ${tmp.datos_pais[0].subdivision_name}, ${tareas.tarea.direccion}`);
                  help_google = wait.forMethod(res.app.settings.geocoder, 'reverse', { lat: tareas.tarea.lat, lon: tareas.tarea.lon });
                  tareas.tarea.pais = (_.has(help_google[0], 'country') && help_google[0].country != "") ? help_google[0].country : "";
                  tareas.tarea.nivel_1 = (_.has(help_google[0].administrativeLevels, 'level1long') && help_google[0].administrativeLevels.level1long != "") ? help_google[0].administrativeLevels.level1long : "";
                  tareas.tarea.nivel_2 = (_.has(help_google[0].administrativeLevels, 'level2long') && help_google[0].administrativeLevels.level2long != "") ? help_google[0].administrativeLevels.level2long : "";
                  tareas.tarea.nivel_3 = (_.has(help_google[0].administrativeLevels, 'level3long') && help_google[0].administrativeLevels.level3long != "") ? help_google[0].administrativeLevels.level3long : "";
                  tareas.tarea.nivel_4 = (_.has(help_google[0].administrativeLevels, 'level4long') && help_google[0].administrativeLevels.level4long != "") ? help_google[0].administrativeLevels.level4long : "";
                  tareas.tarea.nivel_5 = (_.has(help_google[0].administrativeLevels, 'level5long') && help_google[0].administrativeLevels.level5long != "") ? help_google[0].administrativeLevels.level5long : "";
                  tareas.tarea.direccion = (_.has(help_google[0], 'formattedAddress') && help_google[0].formattedAddress != "") ? help_google[0].formattedAddress.split(",")[0] : "";

                  //Agrega dia a la tarea d1,d2,d3,d4,d5,d6,d7 segun fecha_tarea.
                  let lista_dias = "";
                  //2017-10-10: No me sirve de nada calcular la diferencia horaria de la fecha_tarea
                  // dado que la fecha de la tarea sera entregada a futuro, y no ahora.
                  //let dif_horaria = moment(tareas.tarea.fecha_tarea).diff(new Date(),'hours');
                  let fecha_tarea = new Date(tareas.tarea.fecha_tarea);
                  lista_dias = _.map([1, 2, 3, 4, 5, 6, 7], function(num) { return (num == (fecha_tarea.getDay() + 1)) ? (num == 7) ? "1" : "1," : (num == 7) ? "0" : "0,"; }).join("");
                  let helper = {};
                  // 20-10-2017: Las tareas podran ser registradas asignadas. 21-3-2018 victor pide que inspector pueda ser 0 como omision de campo.
                  if ((_.has(tareas.tarea, 'id_inspector') && parseInt(tareas.tarea.id_inspector)!=0) || (!_.isUndefined(tareas.tarea.id_inspector) && parseInt(tareas.tarea.id_inspector)!=0) || (!_.isNull(tareas.tarea.id_inspector) && parseInt(tareas.tarea.id_inspector)!=0)) {
                    helper = wait.forMethod(res.app.settings._conn, 'query', `INSERT INTO tareas (
                      num_caso,id_inspector,id_asegurado,asegurador,evento,fecha_tarea,fecha_siniestro,fecha_ingreso,direccion,lat,lon,pais,nivel_1,nivel_2,nivel_3,nivel_4,nivel_5,categoria,bono,tipo_tarea,estado_tarea,d1,d2,d3,d4,d5,d6,d7
                      )VALUES(
                      ?,?,?,?,?,CAST(? AS DATETIME),CAST(? AS DATETIME),date_format(now(),'%Y-%m-%d'),?,?,?,?,?,?,?,?,?,?,?,?,?,${lista_dias})`,[
                            tareas.tarea.num_caso,tareas.tarea.id_inspector,tmp.asegurado.insertId,tareas.tarea.asegurador,tareas.tarea.evento,
                            tareas.tarea.fecha_tarea,tareas.tarea.fecha_siniestro,
                            tareas.tarea.direccion,tareas.tarea.lat,tareas.tarea.lon,tareas.tarea.pais,
                            tareas.tarea.nivel_1,tareas.tarea.nivel_2,tareas.tarea.nivel_3,tareas.tarea.nivel_4,tareas.tarea.nivel_5,
                            tareas.tarea.categoria,tareas.tarea.bono,tareas.tarea.tipo_tarea,2
                          ]);
                  } else {
                    if (tareas.tarea.categoria == "1" || tareas.tarea.categoria == 1) {
                      helper = wait.forMethod(res.app.settings._conn, 'query', `
                            INSERT INTO tareas (
                              num_caso,id_asegurado,asegurador,evento,
                              fecha_siniestro,fecha_ingreso,
                              direccion,lat,lon,pais,
                              nivel_1,nivel_2,nivel_3,nivel_4,nivel_5,
                              categoria,bono,tipo_tarea,estado_tarea,
                              d1,d2,d3,d4,d5,d6,d7
                            )VALUES(
                              ?, ?, ?, ?,
                              CAST(? AS DATETIME), date_format(now(),'%Y-%m-%d'),
                              ?, ?, ?, ?,
                              ?, ?, ?, ?, ?,
                              ?, ?, ?, ?,
                              ${lista_dias}
                            )`,[
                              tareas.tarea.num_caso,tmp.asegurado.insertId,tareas.tarea.asegurador,tareas.tarea.evento,
                              tareas.tarea.fecha_siniestro,
                              tareas.tarea.direccion,tareas.tarea.lat,tareas.tarea.lon,tareas.tarea.pais,
                              tareas.tarea.nivel_1,tareas.tarea.nivel_2,tareas.tarea.nivel_3,tareas.tarea.nivel_4,tareas.tarea.nivel_5,
                              tareas.tarea.categoria,tareas.tarea.bono,tareas.tarea.tipo_tarea,1
                            ]);
                    } else {
                      if (tareas.tarea.tipo_tarea == 0) {//Normal
                        helper = wait.forMethod(res.app.settings._conn, 'query', `INSERT INTO tareas (
                          num_caso,id_asegurado,asegurador,evento,fecha_tarea,fecha_siniestro,fecha_ingreso,direccion,lat,lon,pais,nivel_1,nivel_2,nivel_3,nivel_4,nivel_5,categoria,bono,tipo_tarea,estado_tarea,d1,d2,d3,d4,d5,d6,d7
                          )VALUES(
                          ?,?,?,?,CAST(? AS DATETIME),CAST(? AS DATETIME),date_format(now(),'%Y-%m-%d'),?,?,?,?,?,?,?,?,?,?,?,?,?,${lista_dias})`,[
                                tareas.tarea.num_caso,tmp.asegurado.insertId,tareas.tarea.asegurador,tareas.tarea.evento,
                                tareas.tarea.fecha_tarea,tareas.tarea.fecha_siniestro,
                                tareas.tarea.direccion,tareas.tarea.lat,tareas.tarea.lon,tareas.tarea.pais,
                                tareas.tarea.nivel_1,tareas.tarea.nivel_2,tareas.tarea.nivel_3,tareas.tarea.nivel_4,tareas.tarea.nivel_5,
                                tareas.tarea.categoria,tareas.tarea.bono,tareas.tarea.tipo_tarea,1
                              ]);
                      } else {//Critica
                        helper = wait.forMethod(res.app.settings._conn, 'query', `INSERT INTO tareas (
                          num_caso,id_asegurado,asegurador,evento,fecha_siniestro,fecha_ingreso,direccion,lat,lon,pais,nivel_1,nivel_2,nivel_3,nivel_4,nivel_5,categoria,bono,tipo_tarea,estado_tarea,d1,d2,d3,d4,d5,d6,d7
                        )VALUES(
                          ?,?,?,?,CAST(? AS DATETIME),date_format(now(),'%Y-%m-%d'),?,?,?,?,?,?,?,?,?,?,?,?,?,${lista_dias})`,[
                                tareas.tarea.num_caso,tmp.asegurado.insertId,tareas.tarea.asegurador,tareas.tarea.evento,
                                tareas.tarea.fecha_siniestro,
                                tareas.tarea.direccion,tareas.tarea.lat,tareas.tarea.lon,tareas.tarea.pais,
                                tareas.tarea.nivel_1,tareas.tarea.nivel_2,tareas.tarea.nivel_3,tareas.tarea.nivel_4,tareas.tarea.nivel_5,
                                tareas.tarea.categoria,tareas.tarea.bono,tareas.tarea.tipo_tarea,1
                              ]);
                      }
                    }
                  }
                  if (helper.affectedRows == 0) {
                    tmp.resp.error = 500;
                    tmp.resp.error = "No se logro ingresar los datos de la tarea.";
                  }
                }
                //console.log(tmp.t[tareas]['asegurado']);
              }// Error
            }// Error
          }
        });
      }
    } catch (e) {
      console.log('Error => ',e);
      tmp.resp.error = 500;
      tmp.resp.mensaje = "Error al registrar la(s) tarea(s).";
    } finally {
      res.send(tmp.resp);
      self.log("segured","registrarTareas",tmp.resp,res);
    }
  },
  consultarTarea: (req, res) => {
    var tmp = {
      resp: {
        error: 0,
        mensaje: "",
        tarea: []
      },
      num_caso: req.body.num_caso
    }
    if (!_.has(req.body, 'num_caso') || _.isUndefined(req.body.num_caso) || _.isNull(req.body.num_caso)) {
      tmp.resp.error = 400;
      tmp.resp.mensaje = "num_caso no existe.";
    }
    if (tmp.resp.error == 0) {
      tmp.resp.tarea = wait.forMethod(res.app.settings._conn, 'query', `SELECT t.id,t.num_caso,t.id_inspeccion,a.id AS asegurado_id, a.codigo_identificador AS asegurado_codigo_identificador, a.nombre AS asegurado_nombre, a.tel_fijo AS asegurado_tel_fijo, a.tel_movil AS asegurado_tel_movil, a.correo AS asegurado_correo,t.asegurador,t.id_inspector,date_format(t.fecha_tarea,'%Y-%m-%d') AS fecha_tarea,date_format(t.fecha_ingreso,'%Y-%m-%d') AS fecha_ingreso, date_format(t.fecha_siniestro,'%Y-%m-%d') AS fecha_siniestro, t.direccion, t.pais, t.nivel_1, t.nivel_2, t.nivel_3, t.nivel_4, t.nivel_5, t.categoria, t.bono, t.lat, t.lon, t.tipo_tarea, t.estado_tarea, t.comentario_can_o_rech, t.d1, t.d2, t.d3, t.d4, t.d5, t.d6, t.d7, t.evento FROM tareas AS t LEFT JOIN asegurado AS a ON a.id = t.id_asegurado WHERE t.num_caso = ${tmp.num_caso}`);
      res.send(tmp.resp);
      self.log("segured","consultarTarea",tmp.resp,res);
    } else {
      res.send(tmp.resp);
      self.log("segured","consultarTarea",tmp.resp,res);
    }
  },
  modificarTarea: (req, res) => {
    var tmp = {
      resp: {
        error: 0,
        mensaje: ""
      },
      n: req.body.num_caso,
      d: req.body.datos
    }
    if (!_.has(req.body, 'num_caso') || _.isUndefined(req.body.num_caso) || _.isNull(req.body.num_caso)) {
      tmp.resp.error = 400;
      tmp.resp.mensaje = "num_caso no existe";
    }
    if (!_.has(req.body, 'datos') || _.isUndefined(req.body.datos) || _.isNull(req.body.datos) || _.isEmpty(req.body.datos)) {
      tmp.resp.error = 400;
      tmp.resp.mensaje = "datos no existe";
    }
    try {
      if (tmp.resp.error == 0) {
          if (!_.has(tmp.d, 'evento') || _.isUndefined(tmp.d.evento) || _.isNull(tmp.d.evento)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "evento no existe.";
          }
          if (!_.has(tmp.d, 'lat') || _.isUndefined(tmp.d.lat) || _.isNull(tmp.d.lat) || _.isEmpty(tmp.d.lat)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "lat no existe.";
          }
          if (!_.has(tmp.d, 'lon') || _.isUndefined(tmp.d.lon) || _.isNull(tmp.d.lon) || _.isEmpty(tmp.d.lon)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "lon no existe.";
          }
          if (!_.has(tmp.d, 'categoria') || _.isUndefined(tmp.d.categoria) || _.isNull(tmp.d.categoria)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "categoria no existe.";
          }else if (tmp.d.categoria == "1") {//Si es emergencia se deja en 0 el tipo_tarea
            tmp.d.tipo_tarea = "0";
          }else if (!_.has(tmp.d, 'tipo_tarea') || _.isUndefined(tmp.d.tipo_tarea) || _.isNull(tmp.d.tipo_tarea)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "tipo_tarea no existe.";
          }
          if (!_.has(tmp.d, 'fecha_tarea') || _.isUndefined(tmp.d.fecha_tarea) || _.isNull(tmp.d.fecha_tarea) || _.isEmpty(tmp.d.fecha_tarea)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "fecha_tarea no existe.";
          }
          if (!_.has(tmp.d, 'fecha_siniestro') || _.isUndefined(tmp.d.fecha_siniestro) || _.isNull(tmp.d.fecha_siniestro) || _.isEmpty(tmp.d.fecha_siniestro)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "fecha_siniestro no existe.";
          }
          if (!_.has(tmp.d, 'bono') || _.isUndefined(tmp.d.bono) || _.isNull(tmp.d.bono) || _.isEmpty(tmp.d.bono)) {
            tmp.d.bono = "0";
          }

          if (tmp.d.categoria == "1") {
            tmp.d.tipo_tarea = 0;
          }
          if (tmp.resp.error == 0) {
            let help_google = [];
            //console.log(`${tmp.datos_pais[0].nombrecomun}, ${tmp.datos_pais[0].subdivision_name}, ${tareas.tarea.direccion}`);
            help_google = wait.forMethod(res.app.settings.geocoder, 'reverse', { lat: tareas.tarea.lat, lon: tareas.tarea.lon });
            tareas.tarea.pais = (_.has(help_google[0], 'country') && help_google[0].country != "") ? help_google[0].country : "";
            tareas.tarea.nivel_1 = (_.has(help_google[0].administrativeLevels, 'level1long') && help_google[0].administrativeLevels.level1long != "") ? help_google[0].administrativeLevels.level1long : "";
            tareas.tarea.nivel_2 = (_.has(help_google[0].administrativeLevels, 'level2long') && help_google[0].administrativeLevels.level2long != "") ? help_google[0].administrativeLevels.level2long : "";
            tareas.tarea.nivel_3 = (_.has(help_google[0].administrativeLevels, 'level3long') && help_google[0].administrativeLevels.level3long != "") ? help_google[0].administrativeLevels.level3long : "";
            tareas.tarea.nivel_4 = (_.has(help_google[0].administrativeLevels, 'level4long') && help_google[0].administrativeLevels.level4long != "") ? help_google[0].administrativeLevels.level4long : "";
            tareas.tarea.nivel_5 = (_.has(help_google[0].administrativeLevels, 'level5long') && help_google[0].administrativeLevels.level5long != "") ? help_google[0].administrativeLevels.level5long : "";
            tareas.tarea.direccion = (_.has(help_google[0], 'formattedAddress') && help_google[0].formattedAddress != "") ? help_google[0].formattedAddress.split(",")[0] : "";

            //Agrega dia a la tarea d1,d2,d3,d4,d5,d6,d7 segun fecha_tarea.
            let lista_dias = "";
            // let dif_horaria = moment(tareas.tarea.fecha_tarea).diff(new Date(),'hours');
            let fecha_tarea = new Date(tareas.tarea.fecha_tarea);
            lista_dias = _.map([1, 2, 3, 4, 5, 6, 7], function(num) { return (num == (fecha_tarea.getDay() + 1)) ? (num == 7) ? "1" : "1," : (num == 7) ? "0" : "0,"; }).join("");
            let helper = {};
            if (tareas.tarea.categoria == 1) {
              helper = wait.forMethod(res.app.settings._conn, 'query', `UPDATE tareas
                      SET evento = ?,fecha_siniestro = CAST(? AS DATETIME),fecha_ingreso = CAST(? AS DATETIME),direccion = ?,
                      lat = ?,lon = ?,pais = ?,nivel_1 = ?,nivel_2 = ?,
                      nivel_3 = ?,nivel_4 = ?,nivel_5 = ?,categoria = ?,
                      bono = ?,tipo_tarea = ?,estado_tarea = ?,
                      lista_dias
                      WHERE num_caso = ?`,[tmp.d.evento,tmp.d.fecha_siniestro,tmp.d.fecha_ingreso,tmp.d.direccion,tmp.d.lat,tmp.d.lon,tmp.d.pais,tmp.d.nivel_1,
                        tmp.d.nivel_2,tmp.d.nivel_3,tmp.d.nivel_4,tmp.d.nivel_5,tmp.d.categoria,tmp.d.bono,tmp.d.tipo_tarea,tmp.d.estado_tarea,tmp.n]);
            } else {
              helper = wait.forMethod(res.app.settings._conn, 'query', `UPDATE tareas
                      SET evento = ?,fecha_tarea = CAST(? AS DATETIME),fecha_siniestro = CAST(? AS DATETIME),fecha_ingreso = CAST(? AS DATETIME),direccion = ?,
                      lat = ?,lon = ?,pais = ?,nivel_1 = ?,nivel_2 = ?,
                      nivel_3 = ?,nivel_4 = ?,nivel_5 = ?,categoria = ?,
                      bono = ?,tipo_tarea = ?,estado_tarea = ?,
                      lista_dias
                      WHERE num_caso = ?`,[tmp.d.evento,tmp.d.fecha_tarea,tmp.d.fecha_siniestro,tmp.d.fecha_ingreso,tmp.d.direccion,tmp.d.lat,tmp.d.lon,tmp.d.pais,tmp.d.nivel_1,
                        tmp.d.nivel_2,tmp.d.nivel_3,tmp.d.nivel_4,tmp.d.nivel_5,tmp.d.categoria,tmp.d.bono,tmp.d.tipo_tarea,tmp.d.estado_tarea,tmp.n]);
            }
            if (helper.affectedRows == 0) {
              tmp.resp.error = 500;
              tmp.resp.error = "No se logro ingresar los datos de la tarea.";
            }
          }
        //console.log(tmp.t[tareas]['asegurado']);
      }
    } catch (e) {
      //console.log(e);
      tmp.resp.error = 500;
      tmp.resp.mensaje = "Error al registrar la(s) tarea(s).";
    } finally {
      res.send(tmp.resp);
      self.log("segured","modificarTarea",tmp.resp,res);
    }
  },
  cancelarTarea: (req, res) => {
    var tmp = {
      resp: {
        error: 0,
        mensaje: ""
      },
      n: req.body.num_tarea
    }
    if (!_.has(req.body, 'num_tarea') || _.isUndefined(req.body.num_tarea) || _.isNull(req.body.num_tarea)) {
      tmp.resp.error = 400;
      tmp.resp.mensaje = "num_tarea no existe.";
    }
    try {
      if (tmp.resp.error == 0) {
        tmp.h = wait.forMethod(res.app.settings._conn, 'query', `UPDATE tareas SET estado_tarea = 7 WHERE num_caso = ${tmp.n}`); //Podria dejarla en 0, no se aun
        if (tmp.h.affectedRows == 0) {
          tmp.resp.error = 500;
          tmp.resp.mensaje = "No se actualizo ninguna tarea.";
        }
      }
    } catch (e) {
      //console.log(e);
      tmp.resp.error = 500;
      tmp.resp.mensaje = "Error al cancelar tarea : " + e;
    } finally {
      res.send(tmp.resp);
      self.log("segured","cancelarTarea",tmp.resp,res);
    }
  },
  consultarInspector: (req, res) => {
    var tmp = {
      resp: {
        error: 0,
        mensaje: "",
        inspector: {}
      },
      select: `SELECT i.id, i.codigo_identificador, i.uidd, i.device_token, i.telefono, i.correo, i.nombre, i.apellido_paterno, i.apellido_materno, i.password, i1.nombrecomun AS pais, i2.subdivision_name AS nivel_1, i.nivel_2, i.nivel_3, i.nivel_4, i.nivel_5, i.direccion, i.lat_dir, i.lon_dir, i.lat_inspector, i.lon_inspector, date_format(i.fecha_nacimiento,'%Y-%m-%d') AS fecha_nacimiento, i.disponibilidad, i.d1,i.d2,i.d3,i.d4,i.d5,i.d6,i.d7 , i.disponibilidad_horas, i.disponibilidad_viajar_ciudad, i.disponibilidad_viajar_pais, e.nombre AS experiencia_oficio, i.experiencia_detalle, i.estado, i.correccion_direccion, i.pais_google, i.direccion_google, i.nivel_1_google, i.nivel_2_google, i.nivel_3_google, i.nivel_4_google, i.nivel_5_google`,
      i: req.body.id_inspector,
      c: req.body.codigo_identificador
    }
    try {
      if (_.has(req.body, 'id_inspector') || _.has(req.body, 'codigo_identificador') || tmp.i != "" || tmp.c != "") {
        if (_.has(req.body, 'id_inspector') || tmp.i != "") {
          tmp.resp.inspector = wait.forMethod(res.app.settings._conn, 'query', `
          ${tmp.select}
          FROM inspectores AS i
          LEFT JOIN paises AS p ON p.id = i.pais
          LEFT JOIN iso AS i1 ON i1.id = p.idpais
          LEFT JOIN isosubdivision AS i2 ON i2.id = i.nivel_1
          LEFT JOIN experiencia_oficio AS e ON e.id = i.id_experiencia_oficio
          WHERE i.id = ${tmp.i}
          `);
          tmp.resp.inspector[0].historial_tareas = wait.forMethod(res.app.settings._conn, 'query', `SELECT t.num_caso FROM tareas AS t WHERE t.id_inspector = ${tmp.i} AND t.estado_tarea >= 6`);
        } else if (_.has(req.body, 'codigo_identificador') || tmp.c != "") {
          tmp.resp.inspector = wait.forMethod(res.app.settings._conn, 'query', `
          ${tmp.select}
          FROM inspectores AS i
          LEFT JOIN paises AS p ON p.id = i.pais
          LEFT JOIN iso AS i1 ON i1.id = p.idpais
          LEFT JOIN isosubdivision AS i2 ON i2.id = i.nivel_1
          LEFT JOIN experiencia_oficio AS e ON e.id = i.id_experiencia_oficio
          WHERE i.codigo_identificador = ${tmp.c}
          `);
          tmp.resp.inspector[0].historial_tareas = wait.forMethod(res.app.settings._conn, 'query', `
          SELECT t.num_caso FROM tareas AS t
          INNER JOIN inspectores AS i ON t.id_inspector = i.id AND i.codigo_identificador = ${tmp.c}
          `);
        }
      } else {
        tmp.resp.inspector = wait.forMethod(res.app.settings._conn, 'query', `
        ${tmp.select}
        FROM inspectores AS i
        LEFT JOIN paises AS p ON p.id = i.pais
        LEFT JOIN iso AS i1 ON i1.id = p.idpais
        LEFT JOIN isosubdivision AS i2 ON i2.id = i.nivel_1
        LEFT JOIN experiencia_oficio AS e ON e.id = i.id_experiencia_oficio
        `);
        _.each(tmp.resp.inspector, function(x, elid) {
          x.historial_tareas = wait.forMethod(res.app.settings._conn, 'query', `SELECT t.num_caso FROM tareas AS t WHERE t.id_inspector = ${x.id}`);
        });
      }
    } catch (e) {
      //console.log(e);
      tmp.resp.error = 500;
      tmp.resp.mensaje = "Error al consultar el inspector";
    } finally {
      res.send(tmp.resp);
      self.log("segured","consultarInspector",tmp.resp,res);
    }
  },
  tareasTomadas: (req, res) => {
    var tmp = {
      resp: {
        error: 0,
        mensaje: "",
        tareas: []
      },
      fecha_inicio: req.body.fecha_inicio,
      fecha_termino: req.body.fecha_termino
    }
    if (!_.has(req.body, 'fecha_inicio') || _.isUndefined(req.body.fecha_inicio) || _.isNull(req.body.fecha_inicio) || _.isEmpty(req.body.fecha_inicio)) {
      tmp.resp.error = 400;
      tmp.resp.mensaje = "fecha_inicio no existe.";
    }
    if (!_.has(req.body, 'fecha_termino') || _.isUndefined(req.body.fecha_termino) || _.isNull(req.body.fecha_termino) || _.isEmpty(req.body.fecha_termino)) {
      tmp.resp.error = 400;
      tmp.resp.mensaje = "fecha_termino no existe.";
    }
    try {
      if (tmp.resp.error == 0) {
        //Sera necesario pasarle los datos del inspector por este servicio tambien ??
        tmp.resp.tareas = wait.forMethod(res.app.settings._conn, 'query', `
        SELECT t.id, t.num_caso, a.id AS asegurado_id, a.codigo_identificador AS asegurado_codigo_identificador,
        a.nombre AS asegurado_nombre, a.tel_fijo AS asegurado_tel_fijo, a.tel_movil AS asegurado_tel_movil,
        a.correo AS asegurado_correo,
        t.asegurador, t.id_inspector,
        date_format(t.fecha_tarea,'%Y-%m-%d') AS fecha_tarea,
        date_format(t.fecha_ingreso,'%Y-%m-%d') AS fecha_ingreso,
        date_format(t.fecha_siniestro,'%Y-%m-%d') AS fecha_siniestro,
        t.direccion, i.nombrecomun AS pais, i2.subdivision_name AS nivel_1,
        t.nivel_2, t.nivel_3, t.nivel_4, t.nivel_5, t.categoria, t.bono, t.lat, t.lon, t.tipo_tarea, t.estado_tarea, t.comentario_can_o_rech, t.evento
        FROM tareas AS t
        INNER JOIN paises AS p ON p.id = t.pais
        INNER JOIN iso AS i ON i.id = p.idpais
        INNER JOIN isosubdivision AS i2 ON i2.id = t.nivel_1
        LEFT JOIN asegurado AS a ON a.id = t.id_asegurado
        WHERE t.fecha_tarea between CAST(? AS DATETIME) AND CAST(? AS DATETIME)
        AND t.estado_tarea between 2 AND 4`,[tmp.fecha_inicio,tmp.fecha_termino]);
        //console.log(tmp.resp.tareas.length);
        // _.each(tmp.resp.tareas,function(x,elid){
        //   x.asegurado = wait.forMethod(res.app.settings._conn,'query',`SELECT * FROM asegurado WHERE id = ${x.id_asegurado}`);
        // });
      }
    } catch (e) {
      //console.log(e);
      tmp.resp.error = 500;
      tmp.resp.mensaje = "Error al obtener tareas tomadas";
    } finally {
      res.send(tmp.resp);
      self.log("segured","tareasTomadas",tmp.resp,res);
    }

  },
  tareasCanceladas: (req, res) => {
    var tmp = {
      resp: {
        error: 0,
        mensaje: "",
        tareas: []
      },
      fecha_inicio: req.body.fecha_inicio,
      fecha_termino: req.body.fecha_termino
    }
    if (!_.has(req.body, 'fecha_inicio') || _.isUndefined(req.body.fecha_inicio) || _.isNull(req.body.fecha_inicio) || _.isEmpty(req.body.fecha_inicio)) {
      tmp.resp.error = 400;
      tmp.resp.mensaje = "fecha_inicio no existe.";
    }
    if (!_.has(req.body, 'fecha_termino') || _.isUndefined(req.body.fecha_termino) || _.isNull(req.body.fecha_termino) || _.isEmpty(req.body.fecha_termino)) {
      tmp.resp.error = 400;
      tmp.resp.mensaje = "fecha_termino no existe.";
    }
    try {
      if (tmp.resp.error == 0) {
        //Sera necesario pasarle los datos del inspector por este servicio tambien ??
        tmp.resp.tareas = wait.forMethod(res.app.settings._conn, 'query', `
        SELECT t.id, t.num_caso, a.id AS asegurado_id,
        a.codigo_identificador AS asegurado_codigo_identificador,
        a.nombre AS asegurado_nombre, a.tel_fijo AS asegurado_tel_fijo,
        a.tel_movil AS asegurado_tel_movil, a.correo AS asegurado_correo,
        t.asegurador, t.id_inspector,
        date_format(t.fecha_tarea,'%Y-%m-%d') AS fecha_tarea,
        date_format(t.fecha_ingreso,'%Y-%m-%d') AS fecha_ingreso,
        date_format(t.fecha_siniestro,'%Y-%m-%d') AS fecha_siniestro,
        t.direccion, i.nombrecomun AS pais, i2.subdivision_name AS nivel_1,
        t.nivel_2, t.nivel_3, t.nivel_4, t.nivel_5, t.categoria, t.bono, t.lat, t.lon, t.tipo_tarea, t.estado_tarea, t.comentario_can_o_rech, t.evento,
        t.d1, t.d2, t.d3, t.d4, t.d5, t.d6, t.d7
        FROM tareas AS t
        INNER JOIN paises AS p ON p.id = t.pais
        INNER JOIN iso AS i ON i.id = p.idpais
        INNER JOIN isosubdivision AS i2 ON i2.id = t.nivel_1
        LEFT JOIN asegurado AS a ON a.id = t.id_asegurado
        WHERE t.fecha_tarea between CAST('${tmp.fecha_inicio}' AS DATETIME) AND CAST('${tmp.fecha_termino}' AS DATETIME)
        AND t.estado_tarea = 7`);
        // _.each(tmp.resp.tareas,function(x,elid){
        //   x.asegurado = wait.forMethod(res.app.settings._conn,'query',`SELECT * FROM asegurado WHERE id = ${x.id_asegurado}`);
        // });
      }
    } catch (e) {
      //console.log(e);
      tmp.resp.error = 500;
      tmp.resp.mensaje = "Error al obtener tareas canceladas";
    } finally {
      res.send(tmp.resp);
      self.log("segured","tareasCanceladas",tmp.resp,res);
    }
  },
  tareasTerminadas: (req, res) => {
    var tmp = {
      resp: {
        error: 0,
        mensaje: "",
        tareas: []
      },
      fecha_inicio: req.body.fecha_inicio,
      fecha_termino: req.body.fecha_termino
    }
    if (!_.has(req.body, 'fecha_inicio') || _.isUndefined(req.body.fecha_inicio) || _.isNull(req.body.fecha_inicio) || _.isEmpty(req.body.fecha_inicio)) {
      tmp.resp.error = 400;
      tmp.resp.mensaje = "fecha_inicio no existe.";
    }
    if (!_.has(req.body, 'fecha_termino') || _.isUndefined(req.body.fecha_termino) || _.isNull(req.body.fecha_termino) || _.isEmpty(req.body.fecha_termino)) {
      tmp.resp.error = 400;
      tmp.resp.mensaje = "fecha_termino no existe.";
    }
    try {
      if (tmp.resp.error == 0) {
        //Sera necesario pasarle los datos del inspector por este servicio tambien ??
        tmp.resp.tareas = wait.forMethod(res.app.settings._conn, 'query', `
        SELECT t.id, t.num_caso, a.id AS asegurado_id,
        a.codigo_identificador AS asegurado_codigo_identificador,
        a.nombre AS asegurado_nombre, a.tel_fijo AS asegurado_tel_fijo,
        a.tel_movil AS asegurado_tel_movil, a.correo AS asegurado_correo,
        t.asegurador, t.id_inspector,
        date_format(t.fecha_tarea,'%Y-%m-%d') AS fecha_tarea,
        date_format(t.fecha_ingreso,'%Y-%m-%d') AS fecha_ingreso,
        date_format(t.fecha_siniestro,'%Y-%m-%d') AS fecha_siniestro,
        t.direccion, i.nombrecomun AS pais, i2.subdivision_name AS nivel_1,
        t.nivel_2, t.nivel_3, t.nivel_4, t.nivel_5, t.categoria, t.bono, t.lat, t.lon, t.tipo_tarea, t.estado_tarea, t.comentario_can_o_rech, t.evento,
        t.d1, t.d2, t.d3, t.d4, t.d5, t.d6, t.d7
        FROM tareas AS t
        INNER JOIN paises AS p ON p.id = t.pais
        INNER JOIN iso AS i ON i.id = p.idpais
        INNER JOIN isosubdivision AS i2 ON i2.id = t.nivel_1
        LEFT JOIN asegurado AS a ON a.id = t.id_asegurado
        WHERE t.fecha_tarea between CAST('${tmp.fecha_inicio}' AS DATETIME) AND CAST('${tmp.fecha_termino}' AS DATETIME)
        AND t.estado_tarea >= 8`);
        // _.each(tmp.resp.tareas,function(x,elid){
        //   x.asegurado = wait.forMethod(res.app.settings._conn,'query',`SELECT * FROM asegurado WHERE id = ${x.id_asegurado}`);
        // });
      }
    } catch (e) {
      //console.log(e);
      tmp.resp.error = 500;
      tmp.resp.mensaje = "Error al obtener tareas terminadas.";
    } finally {
      res.send(tmp.resp);
      self.log("segured","tareasTerminadas",tmp.resp,res);
    }
  },
  registrarPartida: (req, res) => {
    var tmp = {
      resp: {
        error: 0,
        mensaje: ""
      },
      datos: req.body.datos,
      h: [],
      h2: []
    }
    if (!_.has(req.body, 'datos') || _.isUndefined(req.body.datos) || _.isNull(req.body.datos) || _.isEmpty(req.body.datos)) {
      tmp.resp.error = 400;
      tmp.resp.mensaje = "datos no existe.";
    }
    try {
      if (tmp.resp.error == 0) {
        if (tmp.datos.length > 0) {
          _.each(tmp.datos, function(_x, id1) {
            if (!_.has(_x, 'id') || _.isUndefined(_x.id) || _.isNull(_x.id)) {
              tmp.resp.error = 400;
              tmp.resp.mensaje = "id no existe.";
            }
            if (!_.has(_x, 'valor') || _.isUndefined(_x.valor) || _.isNull(_x.valor) ||  _.isEmpty(_x.valor)) {
              tmp.resp.error = 400;
              tmp.resp.mensaje = "valor no existe.";
            }
            if (!_.has(_x, 'pais') || _.isUndefined(_x.pais) || _.isNull(_x.pais)) {
              tmp.resp.error = 400;
              tmp.resp.mensaje = "pais no existe.";
            }
            if (!_.has(_x, 'tipo_dano') || _.isUndefined(_x.tipo_dano) || _.isNull(_x.tipo_dano) || _.isEmpty(_x.tipo_dano)) {
              tmp.resp.error = 400;
              tmp.resp.mensaje = "tipo_dano no existe.";
            }
            if (tmp.resp.error == 0) {
              tmp.h = wait.forMethod(res.app.settings._conn, 'query', `
              INSERT INTO partida (id_segured,valor,pais_texto,fecha)VALUES(?,
                ?,?,now())`, [_x.id, _x.valor, _x.pais]);
              if (tmp.h.affectedRows <= 0) {
                tmp.resp.error = 500;
                tmp.resp.mensaje = "No se logro guardar la partida";
              } else {
                if (_x.tipo_dano.length > 0) {
                  if (!_.has(tmp.h, 'insertId') || _.isUndefined(tmp.h.insertId) || _.isNull(tmp.h.insertId)) {
                    tmp.resp.error = 400;
                    tmp.resp.mensaje = "Partida no se logro guardar";
                  }
                  if (tmp.resp.error == 0) {
                    _.each(_x.tipo_dano, function(_y, id2) {
                      if (!_.has(_y, 'id') || _.isUndefined(_y.id) || _.isNull(_y.id)) {
                        tmp.resp.error = 400;
                        tmp.resp.mensaje = "id no existe.";
                      }
                      if (!_.has(_y, 'valor') || _.isUndefined(_y.valor) || _.isNull(_y.valor) ||  _.isEmpty(_y.valor)) {
                        tmp.resp.error = 400;
                        tmp.resp.mensaje = "valor no existe.";
                      }
                      if (!_.has(_y, 'accion') || _.isUndefined(_y.accion) || _.isNull(_y.accion) ||  _.isEmpty(_y.accion)) {
                        tmp.resp.error = 400;
                        tmp.resp.mensaje = "accion no existe.";
                      }
                      if (tmp.resp.error == 0) {
                        tmp.tipo_dano = wait.forMethod(res.app.settings._conn, 'query', `
                        INSERT INTO tipo_dano (
                          id_segured,id_partida,valor,pais_texto,fecha
                        )VALUES(
                          ?,?,?,?,now())`, [_y.id, tmp.h.insertId, _y.valor, _x.pais]);
                          // hasta aca llegue
                        if (tmp.tipo_dano.affectedRows <= 0) {
                          tmp.resp.error = 500;
                          tmp.resp.mensaje = "Tipo daño no se logro guardar.";
                        } else {
                          if (!_.has(tmp.tipo_dano, 'insertId') || _.isUndefined(tmp.tipo_dano.insertId) || _.isNull(tmp.tipo_dano.insertId)) {
                            tmp.resp.error = 400;
                            tmp.resp.mensaje = "Tipo daño no se logro guardar";
                          }
                          if (tmp.resp.error == 0) {
                            _.each(_y.accion,function(_a,id3){
                              if (!_.has(_a, 'id') || _.isUndefined(_a.id) || _.isNull(_a.id)) {
                                tmp.resp.error = 400;
                                tmp.resp.mensaje = "id de accion no existe.";
                              }
                              if (!_.has(_a, 'valor') || _.isUndefined(_a.valor) || _.isNull(_a.valor) ||  _.isEmpty(_a.valor)) {
                                tmp.resp.error = 400;
                                tmp.resp.mensaje = "valor de accion no existe.";
                              }
                              if (!_.has(_a, 'unidad_medida') || _.isUndefined(_a.unidad_medida) || _.isNull(_a.unidad_medida) ||  _.isEmpty(_a.unidad_medida)) {
                                tmp.resp.error = 400;
                                tmp.resp.mensaje = "unidad_medida no existe.";
                              }
                              if (tmp.resp.error == 0) {
                                tmp.accion = wait.forMethod(res.app.settings._conn, 'query', `INSERT accion (id_segured,id_tipo_dano,valor,pais_texto,fecha)values(?,?,?,?,now())`,[_a.id,tmp.tipo_dano.insertId,_a.valor,_x.pais]);
                                if (tmp.accion.affectedRows <= 0) {
                                  tmp.resp.error = 400;
                                  tmp.resp.mensaje = "No se logro guardar la accion."
                                }else {
                                  if (!_.has(tmp.accion, 'insertId') || _.isUndefined(tmp.accion.insertId) || _.isNull(tmp.accion.insertId)) {
                                    tmp.resp.error = 400;
                                    tmp.resp.mensaje = "La accion no se logro guardar";
                                  }
                                  if (tmp.resp.error == 0) {
                                    _.each(_a.unidad_medida,function(_b,id4){
                                      if (!_.has(_b,'id') || _.isUndefined(_b.id) || _.isNull(_b.id)) {
                                        tmp.resp.error = 400;
                                        tmp.resp.mensaje = "id de unidad_medida no existe.";
                                      }
                                      if (!_.has(_b,'valor') || _.isUndefined(_b.valor) || _.isNull(_b.valor) || _.isEmpty(_b.valor)) {
                                        tmp.resp.error = 400;
                                        tmp.resp.mensaje = "valor de unidad_medida no existe.";
                                      }
                                      if (!_.has(_b,'valor_s') || _.isUndefined(_b.valor_s) || _.isNull(_b.valor_s) || _.isEmpty(_b.valor_s)) {
                                        tmp.resp.error = 400;
                                        tmp.resp.mensaje = "valor_s de unidad_medida no existe.";
                                      }
                                      if (tmp.resp.error == 0) {
                                        tmp.unidad_medida = wait.forMethod(res.app.settings._conn, 'query', `INSERT unidad_medida (id_segured,id_accion,valor,valor_s,pais_texto,fecha)values(?,?,?,?,?,now())`,[_b.id,tmp.accion.insertId,_b.valor,_b.valor_s,_x.pais]);
                                        if (tmp.accion.affectedRows <= 0) {
                                          tmp.resp.error = 400;
                                          tmp.resp.mensaje = "No se logro guardar la unidad de medida."
                                        }
                                      }
                                    });
                                  }
                                }
                              }
                            });

                            //Actualizamos la fecha de actualizacion TODO: por ahora solo actualizamos el tipo_partida, el tipo_dano sera despues
                            tmp.actualizar_selectores = wait.forMethod(res.app.settings._conn, 'query', `UPDATE selectores SET fecha = now() WHERE selector = ? AND pais_texto = ? AND ID>0`,["partida",_x.pais]);
                            if (tmp.actualizar_selectores.affectedRows <= 0) {
                              wait.forMethod(res.app.settings._conn, 'query', `INSERT INTO selectores (selector,pais_texto,fecha)values(?,?,now())`, ["partida", _x.pais]);
                            }
                          }else {
                            throw "Error al ingresar los tipos de daños.";
                          }
                        }
                      }
                    });
                  }else {
                    throw "Error al ingresar la partida.";
                  }
                } else {
                  tmp.resp.error = 400;
                  tmp.resp.mensaje = "Tipo partida esta vacio."
                }
              }
            }
          });
        } else {
          tmp.resp.error = 400;
          tmp.resp.mensaje = "Partida esta vacio."
        }
      }
    } catch (e) {
      console.log('Error => ',e);
      tmp.resp.error = 500;
      tmp.resp.mensaje = "Error al registrar la partida: " + e;
    } finally {
      res.send(tmp.resp);
      self.log("segured","registrarPartida",tmp.resp,res);
    }
  },
  registrarDestinos: (req, res) => {
    var tmp = {
      resp: {
        error: 0,
        mensaje: ""
      },
      datos: req.body.destinos,
      insertado: [],
      nom_db: "destino",
      nom_log: "Los destinos"
    }
    if (!_.has(req.body, 'destinos') || _.isUndefined(req.body.destinos) || _.isNull(req.body.destinos) ||  _.isEmpty(req.body.destinos) || req.body.destinos.length <= 0) {
      tmp.resp.error = 400;
      tmp.resp.mensaje = "destinos no existe.";
    }
    try {
      if (tmp.resp.error == 0) {
        _.each(tmp.datos, function(_valor, _id) {
          if (!_.has(_valor, 'id') || _.isUndefined(_valor.id) || _.isNull(_valor.id)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "id no existe.";
          }
          if (!_.has(_valor, 'valor') || _.isUndefined(_valor.valor) || _.isNull(_valor.valor) || _.isEmpty(_valor.valor)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "valor no existe.";
          }
          if (!_.has(_valor, 'pais') || _.isUndefined(_valor.pais) || _.isNull(_valor.pais) || _.isEmpty(_valor.pais)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "pais no existe.";
          }
          if (tmp.resp.error == 0) {
            tmp.h = wait.forMethod(res.app.settings._conn, 'query', `
            INSERT INTO ${tmp.nom_db} (
              id_segured,
              valor,
              pais_texto,
              fecha
            )VALUES(?,?,?,now())`, [_valor.id,
              _valor.valor,
              _valor.pais
            ]);
            if (tmp.h.affectedRows <= 0) {
              tmp.resp.error = 500;
              tmp.resp.mensaje = `${tmp.nom_log} no se logro registrar.`;
            } else {
              tmp.actualizar_selectores = wait.forMethod(res.app.settings._conn, 'query', `UPDATE selectores SET fecha = now() WHERE selector = "${tmp.nom_db}" AND pais_texto = '${_valor.pais}'`);
              if (tmp.actualizar_selectores.affectedRows <= 0) {
                wait.forMethod(res.app.settings._conn, 'query', `INSERT INTO selectores (selector,pais_texto,fecha)values(?,?,now())`, [tmp.nom_db, _valor.pais]);
              }
            }
          }
        });
      }
    } catch (e) {
      tmp.resp.error = 500;
      tmp.resp.mensaje = `Error al registrar ${tmp.nom_log} : ${e}`;
    } finally {
      res.send(tmp.resp);
      self.log("segured","registrarDestinos",tmp.resp,res);
    }
  },
  registrarCompania: (req, res) => {
    var tmp = {
      resp: {
        error: 0,
        mensaje: ""
      },
      datos: req.body.compania,
      insertado: [],
      nom_db: "compania",
      nom_log: "Las companias"
    }
    if (!_.has(req.body, 'compania') || _.isUndefined(req.body.compania) || _.isNull(req.body.compania) ||  _.isEmpty(req.body.compania) || req.body.compania.length <= 0) {
      tmp.resp.error = 400;
      tmp.resp.mensaje = "compania no existe.";
    }
    try {
      if (tmp.resp.error == 0) {
        _.each(tmp.datos, function(_valor, _id) {
          if (!_.has(_valor, 'id') || _.isUndefined(_valor.id) || _.isNull(_valor.id)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "id no existe.";
          }
          if (!_.has(_valor, 'valor') || _.isUndefined(_valor.valor) || _.isNull(_valor.valor) || _.isEmpty(_valor.valor)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "valor no existe.";
          }
          if (!_.has(_valor, 'pais') || _.isUndefined(_valor.pais) || _.isNull(_valor.pais) || _.isEmpty(_valor.pais)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "pais no existe.";
          }
          if (tmp.resp.error == 0) {
            tmp.h = wait.forMethod(res.app.settings._conn, 'query', `
            INSERT INTO ${tmp.nom_db} (
              id_segured,
              valor,
              pais_texto,
              fecha
            )VALUES(?,?,?,now())`, [_valor.id,
              _valor.valor,
              _valor.pais
            ]);
            if (tmp.h.affectedRows <= 0) {
              tmp.resp.error = 500;
              tmp.resp.mensaje = `${tmp.nom_log} no se logro registrar.`;
            } else {
              tmp.actualizar_selectores = wait.forMethod(res.app.settings._conn, 'query', `UPDATE selectores SET fecha = now() WHERE selector = "${tmp.nom_db}" AND pais_texto = '${_valor.pais}'`);
              if (tmp.actualizar_selectores.affectedRows <= 0) {
                wait.forMethod(res.app.settings._conn, 'query', `INSERT INTO selectores (selector,pais_texto,fecha)values(?,?,now())`, [tmp.nom_db, _valor.pais]);
              }
            }
          }
        });
      }
    } catch (e) {
      tmp.resp.error = 500;
      tmp.resp.mensaje = `Error al registrar ${tmp.nom_log} : ${e}`;
    } finally {
      res.send(tmp.resp);
      self.log("segured","registrarCompania",tmp.resp,res);
    }
  },
  registrarEntidad_financiera: (req, res) => {
    var tmp = {
      resp: {
        error: 0,
        mensaje: ""
      },
      datos: req.body.entidad_financiera,
      insertado: [],
      nom_db: "entidad_financiera",
      nom_log: "Las entidades financieras"
    }
    if (!_.has(req.body, 'entidad_financiera') || _.isUndefined(req.body.entidad_financiera) || _.isNull(req.body.entidad_financiera) || _.isEmpty(req.body.entidad_financiera) || req.body.entidad_financiera.length <= 0) {
      tmp.resp.error = 400;
      tmp.resp.mensaje = "entidad_financiera no existe.";
    }
    try {
      if (tmp.resp.error == 0) {
        _.each(tmp.datos, function(_valor, _id) {
          if (!_.has(_valor, 'id') || _.isUndefined(_valor.id) || _.isNull(_valor.id)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "id no existe.";
          }
          if (!_.has(_valor, 'valor') || _.isUndefined(_valor.valor) || _.isNull(_valor.valor) || _.isEmpty(_valor.valor)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "valor no existe.";
          }
          if (!_.has(_valor, 'pais') || _.isUndefined(_valor.pais) || _.isNull(_valor.pais) || _.isEmpty(_valor.pais)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "pais no existe.";
          }
          if (tmp.resp.error == 0) {
            tmp.h = wait.forMethod(res.app.settings._conn, 'query', `
            INSERT INTO ${tmp.nom_db} (
              id_segured,
              valor,
              pais_texto,
              fecha
            )VALUES(?,?,?,now())`, [_valor.id,
              _valor.valor,
              _valor.pais
            ]);
            if (tmp.h.affectedRows <= 0) {
              tmp.resp.error = 500;
              tmp.resp.mensaje = `${tmp.nom_log} no se logro registrar.`;
            } else {
              tmp.actualizar_selectores = wait.forMethod(res.app.settings._conn, 'query', `UPDATE selectores SET fecha = now() WHERE selector = "${tmp.nom_db}" AND pais_texto = '${_valor.pais}'`);
              if (tmp.actualizar_selectores.affectedRows <= 0) {
                wait.forMethod(res.app.settings._conn, 'query', `INSERT INTO selectores (selector,pais_texto,fecha)values(?,?,now())`, [tmp.nom_db, _valor.pais]);
              }
            }
          }
        });
      }
    } catch (e) {
      tmp.resp.error = 500;
      tmp.resp.mensaje = `Error al registrar ${tmp.nom_log} : ${e}`;
    } finally {
      res.send(tmp.resp);
      self.log("segured","registrarEntidad_financiera",tmp.resp,res);
    }
  },
  registrarTipo_siniestro: (req, res) => {
    var tmp = {
      resp: {
        error: 0,
        mensaje: ""
      },
      datos: req.body.tipo_siniestro,
      insertado: [],
      nom_db: "tipo_siniestro",
      nom_log: "Los tipo de siniestros"
    }
    if (!_.has(req.body, 'tipo_siniestro') || _.isUndefined(req.body.tipo_siniestro) || _.isNull(req.body.tipo_siniestro) || _.isEmpty(req.body.tipo_siniestro) || req.body.tipo_siniestro.length <= 0) {
      tmp.resp.error = 400;
      tmp.resp.mensaje = "tipo_siniestro no existe.";
    }
    try {
      if (tmp.resp.error == 0) {
        _.each(tmp.datos, function(_valor, _id) {
          if (!_.has(_valor, 'id') || _.isUndefined(_valor.id) || _.isNull(_valor.id)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "id no existe.";
          }
          if (!_.has(_valor, 'valor') || _.isUndefined(_valor.valor) || _.isNull(_valor.valor) || _.isEmpty(_valor.valor)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "valor no existe.";
          }
          if (!_.has(_valor, 'pais') || _.isUndefined(_valor.pais) || _.isNull(_valor.pais) || _.isEmpty(_valor.pais)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "pais no existe.";
          }
          if (tmp.resp.error == 0) {
            tmp.h = wait.forMethod(res.app.settings._conn, 'query', `
            INSERT INTO ${tmp.nom_db} (
              id_segured,
              valor,
              pais_texto,
              fecha
            )VALUES(?,?,?,now())`, [_valor.id,
              _valor.valor,
              _valor.pais
            ]);
            if (tmp.h.affectedRows <= 0) {
              tmp.resp.error = 500;
              tmp.resp.mensaje = `${tmp.nom_log} no se logro registrar.`;
            } else {
              tmp.actualizar_selectores = wait.forMethod(res.app.settings._conn, 'query', `UPDATE selectores SET fecha = now() WHERE selector = "${tmp.nom_db}" AND pais_texto = '${_valor.pais}'`);
              if (tmp.actualizar_selectores.affectedRows <= 0) {
                wait.forMethod(res.app.settings._conn, 'query', `INSERT INTO selectores (selector,pais_texto,fecha)values(?,?,now())`, [tmp.nom_db, _valor.pais]);
              }
            }
          }
        });
      }
    } catch (e) {
      tmp.resp.error = 500;
      tmp.resp.mensaje = `Error al registrar ${tmp.nom_log} : ${e}`;
    } finally {
      res.send(tmp.resp);
      self.log("segured","registrarTipo_siniestro",tmp.resp,res);
    }
  },
  registrarEstructura_soportante: (req, res) => {
    var tmp = {
      resp: {
        error: 0,
        mensaje: ""
      },
      datos: req.body.estructura_soportante,
      insertado: [],
      nom_db: "estructura_soportante",
      nom_log: "Las estructuras soportantes"
    }
    if (!_.has(req.body, 'estructura_soportante') || _.isUndefined(req.body.estructura_soportante) || _.isNull(req.body.estructura_soportante) || _.isEmpty(req.body.estructura_soportante) || req.body.estructura_soportante.length <= 0) {
      tmp.resp.error = 400;
      tmp.resp.mensaje = "estructura_soportante no existe.";
    }
    try {
      if (tmp.resp.error == 0) {
        _.each(tmp.datos, function(_valor, _id) {
          if (!_.has(_valor, 'id') || _.isUndefined(_valor.id) || _.isNull(_valor.id)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "id no existe.";
          }
          if (!_.has(_valor, 'valor') || _.isUndefined(_valor.valor) || _.isNull(_valor.valor) || _.isEmpty(_valor.valor)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "valor no existe.";
          }
          if (!_.has(_valor, 'pais') || _.isUndefined(_valor.pais) || _.isNull(_valor.pais) || _.isEmpty(_valor.pais)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "pais no existe.";
          }
          if (tmp.resp.error == 0) {
            tmp.h = wait.forMethod(res.app.settings._conn, 'query', `
            INSERT INTO ${tmp.nom_db} (
              id_segured,
              valor,
              pais_texto,
              fecha
            )VALUES(?,?,?,now())`, [_valor.id,
              _valor.valor,
              _valor.pais
            ]);
            if (tmp.h.affectedRows <= 0) {
              tmp.resp.error = 500;
              tmp.resp.mensaje = `${tmp.nom_log} no se logro registrar.`;
            } else {
              tmp.actualizar_selectores = wait.forMethod(res.app.settings._conn, 'query', `UPDATE selectores SET fecha = now() WHERE selector = "${tmp.nom_db}" AND pais_texto = '${_valor.pais}'`);
              if (tmp.actualizar_selectores.affectedRows <= 0) {
                wait.forMethod(res.app.settings._conn, 'query', `INSERT INTO selectores (selector,pais_texto,fecha)values(?,?,now())`, [tmp.nom_db, _valor.pais]);
              }
            }
          }
        });
      }
    } catch (e) {
      tmp.resp.error = 500;
      tmp.resp.mensaje = `Error al registrar ${tmp.nom_log} : ${e}`;
    } finally {
      res.send(tmp.resp);
      self.log("segured","registrarEstructura_soportante",tmp.resp,res);
    }
  },
  registrarMuros_tabiques: (req, res) => {
    var tmp = {
      resp: {
        error: 0,
        mensaje: ""
      },
      datos: req.body.muros_tabiques,
      insertado: [],
      nom_db: "muros_tabiques",
      nom_log: "Los muros y tabiques"
    }
    if (!_.has(req.body, 'muros_tabiques') || _.isUndefined(req.body.muros_tabiques) || _.isNull(req.body.muros_tabiques) || _.isEmpty(req.body.muros_tabiques) || req.body.muros_tabiques.length <= 0) {
      tmp.resp.error = 400;
      tmp.resp.mensaje = "muros_tabiques no existe.";
    }
    try {
      if (tmp.resp.error == 0) {
        _.each(tmp.datos, function(_valor, _id) {
          if (!_.has(_valor, 'id') || _.isUndefined(_valor.id) || _.isNull(_valor.id)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "id no existe.";
          }
          if (!_.has(_valor, 'valor') || _.isUndefined(_valor.valor) || _.isNull(_valor.valor) || _.isEmpty(_valor.valor)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "valor no existe.";
          }
          if (!_.has(_valor, 'pais') || _.isUndefined(_valor.pais) || _.isNull(_valor.pais) || _.isEmpty(_valor.pais)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "pais no existe.";
          }
          if (tmp.resp.error == 0) {
            tmp.h = wait.forMethod(res.app.settings._conn, 'query', `
            INSERT INTO ${tmp.nom_db} (
              id_segured,
              valor,
              pais_texto,
              fecha
            )VALUES(?,?,?,now())`, [_valor.id,
              _valor.valor,
              _valor.pais
            ]);
            if (tmp.h.affectedRows <= 0) {
              tmp.resp.error = 500;
              tmp.resp.mensaje = `${tmp.nom_log} no se logro registrar.`;
            } else {
              tmp.actualizar_selectores = wait.forMethod(res.app.settings._conn, 'query', `UPDATE selectores SET fecha = now() WHERE selector = "${tmp.nom_db}" AND pais_texto = '${_valor.pais}'`);
              if (tmp.actualizar_selectores.affectedRows <= 0) {
                wait.forMethod(res.app.settings._conn, 'query', `INSERT INTO selectores (selector,pais_texto,fecha)values(?,?,now())`, [tmp.nom_db, _valor.pais]);
              }
            }
          }
        });
      }
    } catch (e) {
      tmp.resp.error = 500;
      tmp.resp.mensaje = `Error al registrar ${tmp.nom_log} : ${e}`;
    } finally {
      res.send(tmp.resp);
      self.log("segured","registrarMuros_tabiques",tmp.resp,res);
    }
  },
  registrarEntrepisos: (req, res) => {
    var tmp = {
      resp: {
        error: 0,
        mensaje: ""
      },
      datos: req.body.entrepisos,
      insertado: [],
      nom_db: "entrepisos",
      nom_log: "Los entrepisos"
    }
    if (!_.has(req.body, 'entrepisos') || _.isUndefined(req.body.entrepisos) || _.isNull(req.body.entrepisos) || _.isEmpty(req.body.entrepisos) || req.body.entrepisos.length <= 0) {
      tmp.resp.error = 400;
      tmp.resp.mensaje = "entrepisos no existe.";
    }
    try {
      if (tmp.resp.error == 0) {
        _.each(tmp.datos, function(_valor, _id) {
          if (!_.has(_valor, 'id') || _.isUndefined(_valor.id) || _.isNull(_valor.id)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "id no existe.";
          }
          if (!_.has(_valor, 'valor') || _.isUndefined(_valor.valor) || _.isNull(_valor.valor) || _.isEmpty(_valor.valor)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "valor no existe.";
          }
          if (!_.has(_valor, 'pais') || _.isUndefined(_valor.pais) || _.isNull(_valor.pais) || _.isEmpty(_valor.pais)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "pais no existe.";
          }
          if (tmp.resp.error == 0) {
            tmp.h = wait.forMethod(res.app.settings._conn, 'query', `
            INSERT INTO ${tmp.nom_db} (
              id_segured,
              valor,
              pais_texto,
              fecha
            )VALUES(?,?,?,now())`, [_valor.id,
              _valor.valor,
              _valor.pais
            ]);
            if (tmp.h.affectedRows <= 0) {
              tmp.resp.error = 500;
              tmp.resp.mensaje = `${tmp.nom_log} no se logro registrar.`;
            } else {
              tmp.actualizar_selectores = wait.forMethod(res.app.settings._conn, 'query', `UPDATE selectores SET fecha = now() WHERE selector = "${tmp.nom_db}" AND pais_texto = '${_valor.pais}'`);
              if (tmp.actualizar_selectores.affectedRows <= 0) {
                wait.forMethod(res.app.settings._conn, 'query', `INSERT INTO selectores (selector,pais_texto,fecha)values(?,?,now())`, [tmp.nom_db, _valor.pais]);
              }
            }
          }
        });
      }
    } catch (e) {
      tmp.resp.error = 500;
      tmp.resp.mensaje = `Error al registrar ${tmp.nom_log} : ${e}`;
    } finally {
      res.send(tmp.resp);
      self.log("segured","registrarEntrepisos",tmp.resp,res);
    }
  },
  registrarPavimento: (req, res) => {
    var tmp = {
      resp: {
        error: 0,
        mensaje: ""
      },
      datos: req.body.pavimento,
      insertado: [],
      nom_db: "pavimento",
      nom_log: "Los pavimentos"
    }
    if (!_.has(req.body, 'pavimento') || _.isUndefined(req.body.pavimento) || _.isNull(req.body.pavimento) ||  _.isEmpty(req.body.pavimento) || req.body.pavimento.length <= 0) {
      tmp.resp.error = 400;
      tmp.resp.mensaje = "pavimento no existe.";
    }
    try {
      if (tmp.resp.error == 0) {
        _.each(tmp.datos, function(_valor, _id) {
          if (!_.has(_valor, 'id') || _.isUndefined(_valor.id) || _.isNull(_valor.id)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "id no existe.";
          }
          if (!_.has(_valor, 'valor') || _.isUndefined(_valor.valor) || _.isNull(_valor.valor) || _.isEmpty(_valor.valor)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "valor no existe.";
          }
          if (!_.has(_valor, 'pais') || _.isUndefined(_valor.pais) || _.isNull(_valor.pais) || _.isEmpty(_valor.pais)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "pais no existe.";
          }
          if (tmp.resp.error == 0) {
            tmp.h = wait.forMethod(res.app.settings._conn, 'query', `
            INSERT INTO ${tmp.nom_db} (
              id_segured,
              valor,
              pais_texto,
              fecha
            )VALUES(?,?,?,now())`, [_valor.id,
              _valor.valor,
              _valor.pais
            ]);
            if (tmp.h.affectedRows <= 0) {
              tmp.resp.error = 500;
              tmp.resp.mensaje = `${tmp.nom_log} no se logro registrar.`;
            } else {
              tmp.actualizar_selectores = wait.forMethod(res.app.settings._conn, 'query', `UPDATE selectores SET fecha = now() WHERE selector = "${tmp.nom_db}" AND pais_texto = '${_valor.pais}'`);
              if (tmp.actualizar_selectores.affectedRows <= 0) {
                wait.forMethod(res.app.settings._conn, 'query', `INSERT INTO selectores (selector,pais_texto,fecha)values(?,?,now())`, [tmp.nom_db, _valor.pais]);
              }
            }
          }
        });
      }
    } catch (e) {
      tmp.resp.error = 500;
      tmp.resp.mensaje = `Error al registrar ${tmp.nom_log} : ${e}`;
    } finally {
      res.send(tmp.resp);
      self.log("segured","registrarPavimento",tmp.resp,res);
    }
  },
  registrarEstructura_cubierta: (req, res) =>  {
    var tmp = {
      resp: {
        error: 0,
        mensaje: ""
      },
      datos: req.body.estructura_cubierta,
      insertado: [],
      nom_db: "estructura_cubierta",
      nom_log: "Las estructuras cubiertas"
    }
    if (!_.has(req.body, 'estructura_cubierta') || _.isUndefined(req.body.estructura_cubierta) || _.isNull(req.body.estructura_cubierta) || _.isEmpty(req.body.estructura_cubierta) || req.body.estructura_cubierta.length <= 0) {
      tmp.resp.error = 400;
      tmp.resp.mensaje = "estructura_cubierta no existe.";
    }
    try {
      if (tmp.resp.error == 0) {
        _.each(tmp.datos, function(_valor, _id) {
          if (!_.has(_valor, 'id') || _.isUndefined(_valor.id) || _.isNull(_valor.id)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "id no existe.";
          }
          if (!_.has(_valor, 'valor') || _.isUndefined(_valor.valor) || _.isNull(_valor.valor) || _.isEmpty(_valor.valor)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "valor no existe.";
          }
          if (!_.has(_valor, 'pais') || _.isUndefined(_valor.pais) || _.isNull(_valor.pais) || _.isEmpty(_valor.pais)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "pais no existe.";
          }
          if (tmp.resp.error == 0) {
            tmp.h = wait.forMethod(res.app.settings._conn, 'query', `
            INSERT INTO ${tmp.nom_db} (
              id_segured,
              valor,
              pais_texto,
              fecha
            )VALUES(?,?,?,now())`, [_valor.id,
              _valor.valor,
              _valor.pais
            ]);
            if (tmp.h.affectedRows <= 0) {
              tmp.resp.error = 500;
              tmp.resp.mensaje = `${tmp.nom_log} no se logro registrar.`;
            } else {
              tmp.actualizar_selectores = wait.forMethod(res.app.settings._conn, 'query', `UPDATE selectores SET fecha = now() WHERE selector = "${tmp.nom_db}" AND pais_texto = '${_valor.pais}'`);
              if (tmp.actualizar_selectores.affectedRows <= 0) {
                wait.forMethod(res.app.settings._conn, 'query', `INSERT INTO selectores (selector,pais_texto,fecha)values(?,?,now())`, [tmp.nom_db, _valor.pais]);
              }
            }
          }
        });
      }
    } catch (e) {
      tmp.resp.error = 500;
      tmp.resp.mensaje = `Error al registrar ${tmp.nom_log} : ${e}`;
    } finally {
      res.send(tmp.resp);
      self.log("segured","registrarEstructura_cubierta",tmp.resp,res);
    }
  },
  registrarCubierta: (req, res) => {
    var tmp = {
      resp: {
        error: 0,
        mensaje: ""
      },
      datos: req.body.cubierta,
      insertado: [],
      nom_db: "cubierta",
      nom_log: "La cubierta"
    }
    if (!_.has(req.body, 'cubierta') || _.isUndefined(req.body.cubierta) || _.isNull(req.body.cubierta) || _.isEmpty(req.body.cubierta) || req.body.cubierta.length <= 0) {
      tmp.resp.error = 400;
      tmp.resp.mensaje = "cubierta no existe.";
    }
    try {
      if (tmp.resp.error == 0) {
        _.each(tmp.datos, function(_valor, _id) {
          if (!_.has(_valor, 'id') || _.isUndefined(_valor.id) || _.isNull(_valor.id)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "id no existe.";
          }
          if (!_.has(_valor, 'valor') || _.isUndefined(_valor.valor) || _.isNull(_valor.valor) || _.isEmpty(_valor.valor)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "valor no existe.";
          }
          if (!_.has(_valor, 'pais') || _.isUndefined(_valor.pais) || _.isNull(_valor.pais) || _.isEmpty(_valor.pais)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "pais no existe.";
          }
          if (tmp.resp.error == 0) {
            tmp.h = wait.forMethod(res.app.settings._conn, 'query', `
            INSERT INTO ${tmp.nom_db} (
              id_segured,
              valor,
              pais_texto,
              fecha
            )VALUES(?,?,?,now())`, [_valor.id,
              _valor.valor,
              _valor.pais
            ]);
            if (tmp.h.affectedRows <= 0) {
              tmp.resp.error = 500;
              tmp.resp.mensaje = `${tmp.nom_log} no se logro registrar.`;
            } else {
              tmp.actualizar_selectores = wait.forMethod(res.app.settings._conn, 'query', `UPDATE selectores SET fecha = now() WHERE selector = "${tmp.nom_db}" AND pais_texto = '${_valor.pais}'`);
              if (tmp.actualizar_selectores.affectedRows <= 0) {
                wait.forMethod(res.app.settings._conn, 'query', `INSERT INTO selectores (selector,pais_texto,fecha)values(?,?,now())`, [tmp.nom_db, _valor.pais]);
              }
            }
          }
        });
      }
    } catch (e) {
      tmp.resp.error = 500;
      tmp.resp.mensaje = `Error al registrar ${tmp.nom_log} : ${e}`;
    } finally {
      res.send(tmp.resp);
      self.log("segured","registrarCubierta",tmp.resp,res);
    }
  },
  bienes: (req, res) => {
    var tmp = {
      resp: {
        error: 0,
        mensaje: ""
      },
      datos: req.body.bienes,
      insertado: {},
      nom_db: "bienes",
      nom_log: "Los bienes"
    }
    if (!_.has(req.body, 'bienes') || _.isUndefined(req.body.bienes) || _.isNull(req.body.bienes) || _.isEmpty(req.body.bienes) || req.body.bienes.length <= 0) {
      tmp.resp.error = 400;
      tmp.resp.mensaje = "bienes no existe.";
    }
    try {
      if (tmp.resp.error == 0) {
        _.each(tmp.datos, function(_valor, _id) {
          if (!_.has(_valor, 'id') || _.isUndefined(_valor.id) || _.isNull(_valor.id)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "id no existe.";
          }
          if (!_.has(_valor, 'valor') || _.isUndefined(_valor.valor) || _.isNull(_valor.valor) || _.isEmpty(_valor.valor)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "valor no existe.";
          }
          if (!_.has(_valor, 'pais') || _.isUndefined(_valor.pais) || _.isNull(_valor.pais) || _.isEmpty(_valor.pais)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "pais no existe.";
          }
          if (tmp.resp.error == 0) {
            tmp.h = wait.forMethod(res.app.settings._conn, 'query', `
            INSERT INTO ${tmp.nom_db} (
              id_segured,
              valor,
              pais_texto,
              fecha
            )VALUES(?,?,?,now())`, [_valor.id,
              _valor.valor,
              _valor.pais
            ]);
            if (tmp.h.affectedRows <= 0) {
              tmp.resp.error = 500;
              tmp.resp.mensaje = `${tmp.nom_log} no se logro registrar.`;
            } else {
              tmp.actualizar_selectores = wait.forMethod(res.app.settings._conn, 'query', `UPDATE selectores SET fecha = now() WHERE selector = "${tmp.nom_db}" AND pais_texto = '${_valor.pais}'`);
              if (tmp.actualizar_selectores.affectedRows <= 0) {
                wait.forMethod(res.app.settings._conn, 'query', `INSERT INTO selectores (selector,pais_texto,fecha)values(?,?,now())`, [tmp.nom_db, _valor.pais]);
              }
            }
          }
        });
      }
    } catch (e) {
      tmp.resp.error = 500;
      tmp.resp.mensaje = `Error al registrar ${tmp.nom_log} : ${e}`;
    } finally {
      res.send(tmp.resp);
      self.log("segured","bienes",tmp.resp,res);
    }
  },
  marcas: (req, res) => {
    var tmp = {
      resp: {
        error: 0,
        mensaje: ""
      },
      datos: req.body.marcas,
      insertado: {},
      nom_db: "marcas",
      nom_log: "Las marcas"
    }
    if (!_.has(req.body, 'marcas') || _.isUndefined(req.body.marcas) || _.isNull(req.body.marcas) || _.isEmpty(req.body.marcas) || req.body.marcas.length <= 0) {
      tmp.resp.error = 400;
      tmp.resp.mensaje = "marcas no existe.";
    }
    try {
      if (tmp.resp.error == 0) {
        _.each(tmp.datos, function(_valor, _id) {
          if (!_.has(_valor, 'id') || _.isUndefined(_valor.id) || _.isNull(_valor.id)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "id no existe.";
          }
          if (!_.has(_valor, 'valor') || _.isUndefined(_valor.valor) || _.isNull(_valor.valor) || _.isEmpty(_valor.valor)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "valor no existe.";
          }
          if (!_.has(_valor, 'pais') || _.isUndefined(_valor.pais) || _.isNull(_valor.pais) || _.isEmpty(_valor.pais)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "pais no existe.";
          }
          if (tmp.resp.error == 0) {
            tmp.h = wait.forMethod(res.app.settings._conn, 'query', `
            INSERT INTO ${tmp.nom_db} (
              id_segured,
              valor,
              pais_texto,
              fecha
            )VALUES(?,?,?,now())`, [_valor.id,
              _valor.valor,
              _valor.pais
            ]);
            if (tmp.h.affectedRows <= 0) {
              tmp.resp.error = 500;
              tmp.resp.mensaje = `${tmp.nom_log} no se logro registrar.`;
            } else {
              tmp.actualizar_selectores = wait.forMethod(res.app.settings._conn, 'query', `UPDATE selectores SET fecha = now() WHERE selector = "${tmp.nom_db}" AND pais_texto = '${_valor.pais}'`);
              if (tmp.actualizar_selectores.affectedRows <= 0) {
                wait.forMethod(res.app.settings._conn, 'query', `INSERT INTO selectores (selector,pais_texto,fecha)values(?,?,now())`, [tmp.nom_db, _valor.pais]);
              }
            }
          }
        });
      }
    } catch (e) {
      tmp.resp.error = 500;
      tmp.resp.mensaje = `Error al registrar ${tmp.nom_log} : ${e}`;
    } finally {
      res.send(tmp.resp);
      self.log("segured","marcas",tmp.resp,res);
    }

  },
  recintos: (req, res) => {
    var tmp = {
      resp: {
        error: 0,
        mensaje: ""
      },
      datos: req.body.recintos,
      insertado: {},
      nom_db: "recintos",
      nom_log: "Los recintos"
    }
    if (!_.has(req.body, 'recintos') || _.isUndefined(req.body.recintos) || _.isNull(req.body.recintos) || _.isEmpty(req.body.recintos) || req.body.recintos.length <= 0) {
      tmp.resp.error = 400;
      tmp.resp.mensaje = "recintos no existe.";
    }
    try {
      if (tmp.resp.error == 0) {
        _.each(tmp.datos, function(_valor, _id) {
          if (!_.has(_valor, 'id') || _.isUndefined(_valor.id) || _.isNull(_valor.id)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "id no existe.";
          }
          if (!_.has(_valor, 'valor') || _.isUndefined(_valor.valor) || _.isNull(_valor.valor) || _.isEmpty(_valor.valor)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "valor no existe.";
          }
          if (!_.has(_valor, 'pais') || _.isUndefined(_valor.pais) || _.isNull(_valor.pais) || _.isEmpty(_valor.pais)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "pais no existe.";
          }
          if (tmp.resp.error == 0) {
            tmp.h = wait.forMethod(res.app.settings._conn, 'query', `
            INSERT INTO ${tmp.nom_db} (
              id_segured,
              valor,
              pais_texto,
              fecha
            )VALUES(?,?,?,now())`, [_valor.id,
              _valor.valor,
              _valor.pais
            ]);
            if (tmp.h.affectedRows <= 0) {
              tmp.resp.error = 500;
              tmp.resp.mensaje = `${tmp.nom_log} no se logro registrar.`;
            } else {
              tmp.actualizar_selectores = wait.forMethod(res.app.settings._conn, 'query', `UPDATE selectores SET fecha = now() WHERE selector = "${tmp.nom_db}" AND pais_texto = '${_valor.pais}'`);
              if (tmp.actualizar_selectores.affectedRows <= 0) {
                wait.forMethod(res.app.settings._conn, 'query', `INSERT INTO selectores (selector,pais_texto,fecha)values(?,?,now())`, [tmp.nom_db, _valor.pais]);
              }
            }
          }
        });
      }
    } catch (e) {
      tmp.resp.error = 500;
      tmp.resp.mensaje = `Error al registrar ${tmp.nom_log} : ${e}`;
    } finally {
      res.send(tmp.resp);
      self.log("segured","recintos",tmp.resp,res);
    }

  },
  editarSelectores: (req, res) => {
    var tmp = {
      resp: {
        error: 0,
        mensaje: ""
      },
      datos: req.body.datos,
      selectores: ["destino","compania","entidad_financiera","tipo_siniestro","estructura_soportante","muros_tabiques","entrepisos","pavimento",
                  "estructura_cubierta","cubierta","partida","tipo_dano","accion","unidad_medida","bienes","marcas","recintos"]
    }
    if (!_.has(req.body, 'datos') || _.isUndefined(req.body.datos) || _.isNull(req.body.datos) || _.isEmpty(req.body.datos)) {
      tmp.resp.error = 400;
      tmp.resp.mensaje = "datos no existe.";
    }
    if (tmp.resp.error == 0) {
      _.each(tmp.datos, function(_x, _id) {
        //console.log(_x);
        if (!_.has(_x, 'accion') || _.isUndefined(_x.accion) || _.isNull(_x.accion) || _.isEmpty(_x.accion)) {
          tmp.resp.error = 400;
          tmp.resp.mensaje = "accion no existe.";
        }
        if (!_.has(_x, 'selector') || _.isUndefined(_x.selector) || _.isNull(_x.selector) || _.isEmpty(_x.selector)) {
          tmp.resp.error = 400;
          tmp.resp.mensaje = "selector no existe.";
        }else if (!_.contains(tmp.selectores,_x.selector)) {
          tmp.resp.error = 400;
          tmp.resp.mensaje = "selector no es requerido.";
        }
        if (tmp.resp.error == 0) {
          switch (_x.accion) {
            case "editar":
              if (!_.has(_x, 'id') || _.isUndefined(_x.id) || _.isNull(_x.id)) {
                tmp.resp.error = 400;
                tmp.resp.mensaje = "id no existe.";
              }
              if (!_.has(_x, 'valor') || _.isUndefined(_x.valor) || _.isNull(_x.valor) || _.isEmpty(_x.valor)) {
                tmp.resp.error = 400;
                tmp.resp.mensaje = "valor no existe.";
              }
              if (!_.has(_x, 'pais') || _.isUndefined(_x.pais) || _.isNull(_x.pais) || _.isEmpty(_x.pais)) {
                tmp.resp.error = 400;
                tmp.resp.mensaje = "pais no existe.";
              }
              if (tmp.resp.error == 0) {
                if (_x.selector == "unidad_medida") {
                  if (!_.has(_x, 'valor_s') || _.isUndefined(_x.valor_s) || _.isNull(_x.valor_s) || _.isEmpty(_x.valor_s)) {
                    tmp.resp.error = 400;
                    tmp.resp.mensaje = "valor_s no existe.";
                  }
                  if (tmp.resp.error == 0) {
                    tmp.h = wait.forMethod(res.app.settings._conn, 'query', `
                    UPDATE ${_x.selector} SET
                      valor = ?,
                      valor_s = ?,
                      fecha = now()
                      WHERE id_segured = ?`, [_x.valor,
                        _x.valor_s,
                      _x.pais,
                      _x.id
                    ]);
                  } else {
                    res.send(tmp.resp);
                  }
                } else {
                  tmp.h = wait.forMethod(res.app.settings._conn, 'query', `
                  UPDATE ${_x.selector} SET
                    valor = ?,
                    pais_texto = ?,
                    fecha = now()
                    WHERE id_segured = ?`, [_x.valor,
                    _x.pais,
                    _x.id
                  ]);
                }

                if (tmp.h.affectedRows <= 0) {
                  tmp.resp.error = 500;
                  tmp.resp.mensaje = `El ${_x.selector} no se logro actualizar.`;
                } else {
                  if (_x.selector != "tipo_dano" || _x.selector != "accion" || _x.selector != "unidad_medida" || _x.selector != "marcas") {
                    tmp.actualizar_selectores = wait.forMethod(res.app.settings._conn, 'query', `UPDATE selectores SET fecha = now() WHERE selector = ? AND pais_texto = ?`,[_x.selector,_x.pais]);
                    if (tmp.actualizar_selectores.affectedRows <= 0) {
                      wait.forMethod(res.app.settings._conn, 'query', `INSERT INTO selectores (selector,pais_texto,fecha)values(?,?,now())`, [_x.selector, _x.pais]);
                    }
                  } else {
                    tmp.actualizar_selectores = wait.forMethod(res.app.settings._conn, 'query', `UPDATE selectores SET fecha = now() WHERE selector = "partida" AND pais_texto = ?`,[_x.pais]);
                    if (tmp.actualizar_selectores.affectedRows <= 0) {
                      wait.forMethod(res.app.settings._conn, 'query', `INSERT INTO selectores (selector,pais_texto,fecha)values(?,?,now())`, ["partida", _x.pais]);
                    }
                  }
                }
              } else {
                res.send(tmp.resp);
              }
              break;
            case "eliminar":
              if (!_.has(_x, 'id') || _.isUndefined(_x.id) || _.isNull(_x.id)) {
                tmp.resp.error = 400;
                tmp.resp.mensaje = "id no existe.";
              }
              if (tmp.resp.error == 0) {
                //Tiene que enviar como selecctor accion
                tmp.h = wait.forMethod(res.app.settings._conn, 'query', `
                DELETE FROM ${_x.selector}
                  WHERE id_segured = ?`, [_x.id]);
                if (tmp.h.affectedRows <= 0) {
                  tmp.resp.error = 500;
                  tmp.resp.mensaje = `El ${_x.selector} no se logro actualizar.`;
                } else {
                  if (_x.selector != "tipo_dano" || _x.selector != "accion" || _x.selector != "unidad_medida" || _x.selector != "marcas") {
                    tmp.actualizar_selectores = wait.forMethod(res.app.settings._conn, 'query', `UPDATE selectores SET fecha = now() WHERE selector = ? AND pais_texto = ?`,[_x.selector,_x.pais]);
                    if (tmp.actualizar_selectores.affectedRows <= 0) {
                      wait.forMethod(res.app.settings._conn, 'query', `INSERT INTO selectores (selector,pais,fecha)values(?,?,now())`, [_x.selector, _x.pais]);
                    }
                  } else {
                    tmp.actualizar_selectores = wait.forMethod(res.app.settings._conn, 'query', `UPDATE selectores SET fecha = now() WHERE selector = "partida" AND pais_texto = ?`,[_x.pais]);
                    if (tmp.actualizar_selectores.affectedRows <= 0) {
                      wait.forMethod(res.app.settings._conn, 'query', `INSERT INTO selectores (selector,pais_texto,fecha)values(?,?,now())`, ["partida", _x.pais]);
                    }
                  }
                }
              } else {
                res.send(tmp.resp);
              }
              break;
            default:
              tmp.resp.error = 400;
              tmp.resp.mensaje = `La accion ${_x.accion} en el selector ${_x.selector} no existe.`;
              break;
          }
        } else {
          res.send(tmp.resp);
          self.log("segured","editarSelectores",tmp.resp,res);
        }
        if (_id >= tmp.datos.length - 1) {
          res.send(tmp.resp);
          self.log("segured","editarSelectores",tmp.resp,res);
        }
      });
    } else {
      res.send(tmp.resp);
      self.log("segured","editarSelectores",tmp.resp,res);
    }
  },
  finalizarInspeccion: (req, res) => {
    var tmp = {
      resp: {
        error: 0,
        mensaje: ""
      },
      num_caso: req.body.num_caso
    }
    if (!_.has(req.body, 'num_caso') || _.isUndefined(req.body.num_caso) || _.isNull(req.body.num_caso) || _.isEmpty(req.body.num_caso)) {
      tmp.resp.error = 400;
      tmp.resp.mensaje = "num_caso no existe.";
    }
    if (tmp.resp.error == 0) {
      // TODO: BORRAR LOS OTROS DATOS DE INSPECCION
      // Obtenemos la tarea por que no quiero hacer un join en cada consulta
      let tarea = wait.forMethod(res.app.settings._conn, 'query',`SELECT id FROM tareas WHERE num_caso = ? AND id_inspeccion <> 0 AND estado_tarea >= 6`,[tmp.num_caso]);
      let borrar_inspeccion = wait.forMethod(res.app.settings._conn, 'query',`DELETE FROM inspecciones WHERE id_tarea = ?`,[tarea[0].id]);
      wait.forMethod(res.app.settings._conn, 'query',`UPDATE tareas SET estado_tarea = 10 WHERE id = ?`,[tarea[0].id]);
      if (borrar_inspeccion.affectedRows <= 0) {
        tmp.resp.error = 500;
        tmp.resp.mensaje = "Error al borrar las inspecciones.";
        res.send(tmp.resp);
        self.log("segured","finalizarInspeccion",tmp.resp,res);
      } else {
        try {
          wait.forMethod(res.app.settings._conn, 'query',`DELETE FROM inspeccion_caracteristicas WHERE id_tarea = ?`,[tarea[0].id]);
          wait.forMethod(res.app.settings._conn, 'query',`DELETE FROM inspeccion_contenidos WHERE id_tarea = ?`,[tarea[0].id]);
          wait.forMethod(res.app.settings._conn, 'query',`DELETE FROM inspeccion_datosbasicos WHERE id_tarea = ?`,[tarea[0].id]);
          wait.forMethod(res.app.settings._conn, 'query',`DELETE FROM inspeccion_documentos WHERE id_tarea = ?`,[tarea[0].id]);
          wait.forMethod(res.app.settings._conn, 'query',`DELETE FROM inspeccion_firma WHERE id_tarea = ?`,[tarea[0].id]);
          wait.forMethod(res.app.settings._conn, 'query',`DELETE FROM inspeccion_fotosrequeridas WHERE id_tarea = ?`,[tarea[0].id]);
          wait.forMethod(res.app.settings._conn, 'query',`DELETE FROM inspeccion_itemdanos WHERE id_tarea = ?`,[tarea[0].id]);
          wait.forMethod(res.app.settings._conn, 'query',`DELETE FROM inspeccion_niveles WHERE id_tarea = ?`,[tarea[0].id]);
          wait.forMethod(res.app.settings._conn, 'query',`DELETE FROM inspeccion_recintos WHERE id_tarea = ?`,[tarea[0].id]);
          wait.forMethod(res.app.settings._conn, 'query',`DELETE FROM inspeccion_siniestro WHERE id_tarea = ?`,[tarea[0].id]);
          wait.forMethod(res.app.settings._conn, 'query',`DELETE FROM imagenes WHERE id_tarea = ?`,[tarea[0].id]);
        } catch (e) {
          tmp.resp.error = 500;
          tmp.resp.mensaje = "Error al borrar las inspecciones.";
        }
        res.send(tmp.resp);
        self.log("segured","finalizarInspeccion",tmp.resp,res);
      }
    } else {
      res.send(tmp.resp);
      self.log("segured","finalizarInspeccion",tmp.resp,res);
    }
  },
  obtenerImagen: (req, res) => {
    var tmp = {
      resp: {
        error: 0,
        mensaje: "",
        imagen: ""
      },
      num_caso: req.body.num_caso,
      id_imagen: req.body.id_imagen
    }
    if (!_.has(req.body, 'num_caso') || _.isUndefined(req.body.num_caso) || _.isNull(req.body.num_caso) || _.isEmpty(req.body.num_caso)) {
      tmp.resp.error = 400;
      tmp.resp.mensaje = "num_caso no existe.";
    }
    if (!_.has(req.body, 'id_imagen') || _.isUndefined(req.body.id_imagen) || _.isNull(req.body.id_imagen) || _.isEmpty(req.body.id_imagen)) {
      tmp.resp.error = 400;
      tmp.resp.mensaje = "id_imagen no existe.";
    }
    if (tmp.resp.error == 0) {
      //
      tmp.resp.imagen = wait.forMethod(res.app.settings._conn, 'query',
      `SELECT i.imagen FROM imagenes AS i
      INNER join tareas AS t ON t.id = i.id_tarea
      WHERE t.num_caso = ? AND i.id = ?
      limit 1`,[tmp.num_caso,tmp.id_imagen]);
      tmp.resp.imagen = tmp.resp.imagen[0].imagen.toString();
      // tmp.resp.imagen = self.base64_encode(tmp.resp.imagen[0].imagen);
      res.send(tmp.resp);
      self.log("segured","obtenerImagen",tmp.resp,res);
    }else {
      res.send(tmp.resp);
      self.log("segured","obtenerImagen",tmp.resp,res);
    }
  },
  obtenerInspeccion: (req, res) => {
    var tmp = {
      resp: {
        error: 0,
        mensaje: "",
        inspeccion: ""
      },
      num_caso: req.body.num_caso
    }
    if (!_.has(req.body, 'num_caso') || _.isUndefined(req.body.num_caso) || _.isNull(req.body.num_caso) || _.isEmpty(req.body.num_caso)) {
      tmp.resp.error = 400;
      tmp.resp.mensaje = "num_caso no existe.";
    }
    if (tmp.resp.error == 0) {
      tmp.resp.inspeccion = wait.forMethod(res.app.settings._conn, 'query', `SELECT i.json FROM inspecciones AS i INNER JOIN tareas AS t ON t.id = i.id_tarea WHERE t.num_caso = ?`,[tmp.num_caso])[0].json;
      res.send(tmp.resp);
      self.log("segured","obtenerInspeccion",tmp.resp,res);
    }else {
      res.send(tmp.resp);
      self.log("segured","obtenerInspeccion",tmp.resp,res);
    }
  },
  /**
   * Registra las respuestas fallidas del sistema.
   *
   * @name log
   * @method
   * @param {string} entidad - entidad (segured/u-adjust).
   * @param {string} funcion - nombre de la funcion.
   * @param {string} mensaje - mensaje que se le entrego al usuario.
   * @private
   */
  log: (entidad,funcion,mensaje,res) => {
    try {
      wait.forMethod(res.app.settings._conn, 'query', `INSERT INTO log_salida (entidad,funcion,mensaje,fecha)VALUES(?,?,?,now())`,[entidad,funcion,JSON.stringify(mensaje)]);
    } catch (e) {
    }
  },
}

module.exports = self;
