var moment = require('moment'),
    wait = require('wait.for'),
    async = require('async'),
    _ = require('underscore'),
    fs = require('fs'),
    path = require('path');
// TODO 12-05-17: Quitar todos los TryCatch (hace mas lento todo)
var self = {
    // Funcion Dummy para realizar pruebas de respuesta sin ninguna accion, esta se ocupa en la App cuando solo quieren probar funcionalidad.
    dummy: (req, res, next) => {
      res.send({error:0,mensaje:""});
    },
    config: (res) => {
      // Obtenemos los datos de configuracion, solo el ultimo registrado.
      var helper = wait.forMethod(res.app.settings._conn, 'query', `SELECT * FROM segured.configuracion order by id DESC limit 1`);
      //console.log(helper);
      return { limite_distancia: helper[0].km_busqueda, limite_busqueda: helper[0].cantidad_de_resultados }
    },
    selectores: (req, res) => {
      // Traspasamos los datos de req.body al temporal, esto nos permite poder modificarlos si es necesario.
        var tmp = {
            resp: {
                error: 0,
                mensaje: "",
                destino: [],
                compania: [],
                entidad_financiera: [],
                tipo_siniestro: [],
                estructura_soportante: [],
                muros_tabiques: [],
                entrepisos: [],
                pavimento: [],
                estructura_cubierta: [],
                cubierta: [],
                partida: [],
                tipo_dano: [],
                accion: [],
                unidad_medida: [],
                bienes: [],
                marcas: [],
                monedas: [],
                selectores: [],
                recintos: []
            },
            pais: req.body.pais,
            destino: req.body.destino,
            compania: req.body.compania,
            entidad_financiera: req.body.entidad_financiera,
            tipo_siniestro: req.body.tipo_siniestro,
            estructura_soportante: req.body.estructura_soportante,
            muros_tabiques: req.body.muros_tabiques,
            entrepisos: req.body.entrepisos,
            pavimento: req.body.pavimento,
            estructura_cubierta: req.body.estructura_cubierta,
            cubierta: req.body.cubierta,
            partida: req.body.partida,
            tipo_dano: req.body.tipo_dano,
            accion: req.body.accion,
            unidad_medida: req.body.unidad_medida,
            bienes: req.body.bienes,
            marcas: req.body.marcas,
            monedas: req.body.monedas,
            recintos: req.body.recintos
        };
        if (!_.has(req.body, 'pais')) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "El pais no existe.";
        }
        if (tmp.resp.error == 0) {
            // Si lo requerido esta difinido, obtenemos la diferencia horaria entre la app y el ultimo cambio en el servidor, si es diferente le entregamos el dato requerido.
            if (_.has(req.body, 'destino')) {
                f_destino = wait.forMethod(res.app.settings._conn, 'query', `SELECT * FROM selectores WHERE selector = "destino" AND pais_texto = ? AND fecha = date_format(CAST(? AS DATETIME),'%Y-%m-%dT%H-%i-%S')`, [ tmp.pais, tmp.destino ]);
                if (f_destino.length <= 0) { tmp.resp.destino = wait.forMethod(res.app.settings._conn, 'query', 'SELECT * FROM destino WHERE pais_texto = ?', [tmp.pais]); }
            }
            if (_.has(req.body, 'compania')) {
                f_compania = wait.forMethod(res.app.settings._conn, 'query', `SELECT * FROM selectores WHERE selector = "compania" AND pais_texto = ? AND fecha = date_format(CAST(? AS DATETIME),'%Y-%m-%dT%H-%i-%S')`, [ tmp.pais, tmp.compania ]);
                if (f_compania.length <= 0) { tmp.resp.compania = wait.forMethod(res.app.settings._conn, 'query', 'SELECT * FROM compania'); }
            }
            if (_.has(req.body, 'entidad_financiera')) {
                f_entidad_financiera = wait.forMethod(res.app.settings._conn, 'query', `SELECT * FROM selectores WHERE selector = "entidad_financiera" AND pais_texto = ? AND fecha = date_format(CAST(? AS DATETIME),'%Y-%m-%dT%H-%i-%S')`, [ tmp.pais, tmp.entidad_financiera ]);
                if (f_entidad_financiera.length <= 0) { tmp.resp.entidad_financiera = wait.forMethod(res.app.settings._conn, 'query', 'SELECT * FROM entidad_financiera'); }
            }
            if (_.has(req.body, 'tipo_siniestro')) {
                f_tipo_siniestro = wait.forMethod(res.app.settings._conn, 'query', `SELECT * FROM selectores WHERE selector = "tipo_siniestro" AND pais_texto = ? AND fecha = date_format(CAST(? AS DATETIME),'%Y-%m-%dT%H-%i-%S')`, [ tmp.pais, tmp.tipo_siniestro ]);
                if (f_tipo_siniestro.length <= 0) { tmp.resp.tipo_siniestro = wait.forMethod(res.app.settings._conn, 'query', 'SELECT * FROM tipo_siniestro WHERE pais_texto = ?', [ tmp.pais ]); }
            }
            if (_.has(req.body, 'estructura_soportante')) {
                f_estructura_soportante = wait.forMethod(res.app.settings._conn, 'query', `SELECT * FROM selectores WHERE selector = "estructura_soportante" AND pais_texto = ? AND fecha = date_format(CAST(? AS DATETIME),'%Y-%m-%dT%H-%i-%S')`, [ tmp.pais, tmp.estructura_soportante ]);
                if (f_estructura_soportante.length <= 0) { tmp.resp.estructura_soportante = wait.forMethod(res.app.settings._conn, 'query', 'SELECT * FROM estructura_soportante WHERE pais_texto = ?', [ tmp.pais ]); }
            }
            if (_.has(req.body, 'muros_tabiques')) {
                f_muros_tabiques = wait.forMethod(res.app.settings._conn, 'query', `SELECT * FROM selectores WHERE selector = "muros_tabiques" AND pais_texto = ? AND fecha = date_format(CAST(? AS DATETIME),'%Y-%m-%dT%H-%i-%S')`, [ tmp.pais, tmp.muros_tabiques ]);
                if (f_muros_tabiques.length <= 0) { tmp.resp.muros_tabiques = wait.forMethod(res.app.settings._conn, 'query', 'SELECT * FROM muros_tabiques WHERE pais_texto = ?', [ tmp.pais ]); }
            }
            if (_.has(req.body, 'entrepisos')) { f_entrepisos = wait.forMethod(res.app.settings._conn, 'query', `SELECT * FROM selectores WHERE selector = "entrepisos" AND pais_texto = ? AND fecha = date_format(CAST(? AS DATETIME),'%Y-%m-%dT%H-%i-%S')`, [ tmp.pais, tmp.entrepisos ]);
                if (f_entrepisos.length <= 0) { tmp.resp.entrepisos = wait.forMethod(res.app.settings._conn, 'query', 'SELECT * FROM entrepisos WHERE pais_texto = ?', [ tmp.pais ]); }
            }
            if (_.has(req.body, 'pavimento')) { f_pavimento = wait.forMethod(res.app.settings._conn, 'query', `SELECT * FROM selectores WHERE selector = "pavimento" AND pais_texto = ? AND fecha = date_format(CAST(? AS DATETIME),'%Y-%m-%dT%H-%i-%S')`, [ tmp.pais, tmp.entrepisos ]);
                if (f_pavimento.length <= 0) { tmp.resp.pavimento = wait.forMethod(res.app.settings._conn, 'query', 'SELECT * FROM pavimento WHERE pais_texto = ?', [tmp.pais ]); }
            }
            if (_.has(req.body, 'estructura_cubierta')) {
                f_estructura_cubierta = wait.forMethod(res.app.settings._conn, 'query', `SELECT * FROM selectores WHERE selector = "estructura_cubierta" AND pais_texto = ? AND fecha = date_format(CAST(? AS DATETIME),'%Y-%m-%dT%H-%i-%S')`, [ tmp.pais, tmp.estructura_cubierta ]);
                if (f_estructura_cubierta.length <= 0) { tmp.resp.estructura_cubierta = wait.forMethod(res.app.settings._conn, 'query', 'SELECT * FROM estructura_cubierta WHERE pais_texto = ?', [ tmp.pais ]); }
            }
            if (_.has(req.body, 'cubierta')) {
                f_cubierta = wait.forMethod(res.app.settings._conn, 'query', `SELECT * FROM selectores WHERE selector = "cubierta" AND pais_texto = ? AND fecha = date_format(CAST(? AS DATETIME),'%Y-%m-%dT%H-%i-%S')`, [ tmp.pais, tmp.cubierta ]);
                if (f_cubierta.length <= 0) { tmp.resp.cubierta = wait.forMethod(res.app.settings._conn, 'query', 'SELECT * FROM cubierta WHERE pais_texto = ?', [tmp.pais] ); }
            }
            if (_.has(req.body, 'partida')) {
                f_partida = wait.forMethod(res.app.settings._conn, 'query',  `SELECT * FROM selectores WHERE selector = "partida" AND pais_texto = ? AND fecha = date_format(CAST(? AS DATETIME),'%Y-%m-%dT%H-%i-%S')`, [ tmp.pais, tmp.tipo_partida ]);
                if (f_partida.length <= 0) {
                    tmp.resp.partida = wait.forMethod(res.app.settings._conn, 'query', `SELECT * FROM partida WHERE pais_texto = ?`, [tmp.pais]);
                    tmp.resp.tipo_dano = wait.forMethod(res.app.settings._conn, 'query', `SELECT * FROM tipo_dano WHERE pais_texto = ?`, [tmp.pais ]);
                    tmp.resp.accion = wait.forMethod(res.app.settings._conn, 'query', `SELECT * FROM accion WHERE pais_texto = ?`, [tmp.pais]);
                    tmp.resp.unidad_medida = wait.forMethod(res.app.settings._conn, 'query', `SELECT * FROM unidad_medida WHERE pais_texto = ?`, [ tmp.pais ]);
                }
            }
            if (_.has(req.body, 'bienes')) {
                f_bienes = wait.forMethod(res.app.settings._conn, 'query', `SELECT * FROM selectores WHERE selector = "bienes" AND pais_texto = ? AND fecha = date_format(CAST(? AS DATETIME),'%Y-%m-%dT%H-%i-%S')`, [ tmp.pais, tmp.bienes ]);
                if (f_bienes.length <= 0) { tmp.resp.bienes = wait.forMethod(res.app.settings._conn, 'query', 'SELECT * FROM bienes WHERE pais_texto = ?', [tmp.pais]); }
            }
            if (_.has(req.body, 'marcas')) {
                f_marcas = wait.forMethod(res.app.settings._conn, 'query', `SELECT * FROM selectores WHERE selector = "marcas" AND pais_texto = ? AND fecha = date_format(CAST(? AS DATETIME),'%Y-%m-%dT%H-%i-%S')`, [ tmp.pais, tmp.marcas ]);
                if (f_marcas.length <= 0) { tmp.resp.marcas = wait.forMethod(res.app.settings._conn, 'query', 'SELECT * FROM marcas WHERE pais_texto = ?', [tmp.pais]); }
            }
            if (_.has(req.body, 'monedas')) {
                f_monedas = wait.forMethod(res.app.settings._conn, 'query', `SELECT * FROM selectores WHERE selector = "monedas" AND pais_texto = ? AND fecha = date_format(CAST(? AS DATETIME),'%Y-%m-%dT%H-%i-%S')`, [ tmp.pais, tmp.monedas ]);
                if (f_monedas.length <= 0) { tmp.resp.monedas = wait.forMethod(res.app.settings._conn, 'query', 'SELECT * FROM monedas WHERE pais_texto = ?', [tmp.pais]); }
            }
            if (_.has(req.body, 'recintos')) {
                f_recintos = wait.forMethod(res.app.settings._conn, 'query', `SELECT * FROM selectores WHERE selector = "recintos" AND pais_texto = ? AND fecha = date_format(CAST(? AS DATETIME),'%Y-%m-%dT%H-%i-%S')`, [ tmp.pais, tmp.bienes ]);
                if (f_recintos.length <= 0) { tmp.resp.recintos = wait.forMethod(res.app.settings._conn, 'query', 'SELECT * FROM recintos WHERE pais_texto = ?', [tmp.pais]); }
            }
            tmp.resp.selectores = wait.forMethod(res.app.settings._conn, 'query', `SELECT id,selector,pais,date_format(fecha,'%Y-%m-%dT%H-%i-%S') AS fecha,pais_texto FROM selectores WHERE pais_texto = ?`, [ tmp.pais ]);
            res.send(tmp.resp);
            self.log("u-adjust", "selectores", tmp.resp, res);
        } else {
            res.send(tmp.resp);
            self.log("u-adjust", "selectores", tmp.resp, res);
        }

    },
    login: (req, res) => {
      // Definimos el objeto temporal, este tiene lo que queremos selectores para las base de datos de tareas, simplemente para no entregarle informacion que no necesite la App.
        var tmp = {
            select: `t.id,t.num_caso,t.id_inspeccion,a.id AS asegurado_id, a.codigo_identificador AS asegurado_codigo_identificador, a.nombre AS asegurado_nombre, a.tel_fijo AS asegurado_tel_fijo, a.tel_movil AS asegurado_tel_movil, a.correo AS asegurado_correo,t.asegurador,t.id_inspector,date_format(t.fecha_tarea,'%Y-%m-%d') AS fecha_tarea,date_format(t.fecha_ingreso,'%Y-%m-%d') AS fecha_ingreso, date_format(t.fecha_siniestro,'%Y-%m-%d') AS fecha_siniestro, t.direccion, t.pais, t.pais AS pais_, t.nivel_1, t.nivel_1 AS nivel_1_, t.nivel_2, t.nivel_3, t.nivel_4, t.nivel_5, t.categoria, t.bono, t.lat, t.lon, t.tipo_tarea, t.estado_tarea, t.comentario_can_o_rech, t.d1, t.d2, t.d3, t.d4, t.d5, t.d6, t.d7, t.evento`,
            select_historial: `t.id,t.num_caso,t.id_inspeccion,a.id AS asegurado_id, a.codigo_identificador AS asegurado_codigo_identificador, a.nombre AS asegurado_nombre, a.tel_fijo AS asegurado_tel_fijo, a.tel_movil AS asegurado_tel_movil, a.correo AS asegurado_correo,t.asegurador,t.id_inspector,date_format(t.fecha_tarea,'%Y-%m-%d') AS fecha_tarea,date_format(t.fecha_ingreso,'%Y-%m-%d') AS fecha_ingreso, date_format(t.fecha_siniestro,'%Y-%m-%d') AS fecha_siniestro, date_format(t.fecha_termino,'%Y-%m-%d') AS fecha_finalizacion, date_format(t.fecha_termino,'%H:%i:%S') AS hora_finalizacion, t.direccion, t.pais, t.pais AS pais_, t.nivel_1, t.nivel_1 AS nivel_1_, t.nivel_2, t.nivel_3, t.nivel_4, t.nivel_5, t.categoria, t.bono, t.lat, t.lon, t.tipo_tarea, t.estado_tarea, t.comentario_can_o_rech, t.d1, t.d2, t.d3, t.d4, t.d5, t.d6, t.d7, t.evento`,
            resp: {
                error: 0,
                mensaje: "",
                tareas: {
                    entrantes: {
                        normal: [],
                        critica: []
                    },
                    emergencias: {
                        ubicacion: [],
                        perfil: []
                    },
                    historial_tareas: [],
                    mistareas: []
                },
                inspector: {}
            },
            correo: req.body.correo,
            uidd: req.body.uidd,
            password: req.body.password,
            device: req.body.device,
            device_token: req.body.device_token,
            lat: req.body.lat,
            lon: req.body.lon,
            fecha: req.body.fecha,
            limites: self.config(res)
        };
        if (!_.has(req.body, 'correo') || _.isUndefined(req.body.correo) || _.isNull(req.body.correo) || _.isEmpty(req.body.correo)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "correo no existe.";
        }
        if (!_.has(req.body, 'password') || _.isUndefined(req.body.password) || _.isNull(req.body.password) || _.isEmpty(req.body.password)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "password no existe.";
        }
        if (!_.has(req.body, 'uidd') || _.isUndefined(req.body.uidd) || _.isNull(req.body.uidd) || _.isEmpty(req.body.uidd)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "uidd no existe.";
        }
        if (!_.has(req.body, 'device') || _.isUndefined(req.body.device) || _.isNull(req.body.device) || _.isEmpty(req.body.device)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "device no existe.";
        }
        if (!_.has(req.body, 'lat') || _.isUndefined(req.body.lat) || _.isNull(req.body.lat) || _.isEmpty(req.body.lat)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "lat no existe.";
        }
        if (!_.has(req.body, 'lon') || _.isUndefined(req.body.lon) || _.isNull(req.body.lon) || _.isEmpty(req.body.lon)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "lon no existe.";
        }
        if (!_.has(req.body, 'device_token') || _.isUndefined(req.body.device_token) || _.isNull(req.body.device_token) || _.isEmpty(req.body.device_token)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "device_token no existe.";
        }
        if (!_.has(req.body, 'fecha') || _.isUndefined(req.body.fecha) || _.isNull(req.body.fecha) || _.isEmpty(req.body.fecha)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "fecha no existe.";
        }
        //console.log(req.body);
        if (tmp.resp.error == 0) {
            //console.log(`Login de inspector : ${tmp.correo}, ${tmp.password}`);
            // Retorna los datos de inspector, tareas y historial de tareas si esta validado, si no, retorna vacio
            tmp.inspectores = wait.forMethod(res.app.settings._conn, 'query',
                `SELECT *
                  FROM inspectores
                  WHERE correo = ?
                  AND password = ?`,[tmp.correo,tmp.password]);
            //console.log(tmp.inspectores);
            if (tmp.inspectores.length >= 1) { //Si el inspector existe
              tmp.dif_horaria = +tmp.fecha; // Guardamos la diferencia horaria del usuario para aplicarla a las tareas.
              // Insertamos el log del login.
              wait.forMethod(res.app.settings._conn, 'query',
                  `INSERT INTO log_login (id_inspector,correo,password,uidd,device,lat,lon,device_token,dif_horaria,fecha_ingreso)
                  VALUES
                  (?,?,?,?,?,?,?,?,?,now())`,
                  [tmp.inspectores[0].id, tmp.correo, tmp.password, tmp.uidd, tmp.device, tmp.lat, tmp.lon, tmp.device_token,tmp.dif_horaria]);
                if (tmp.inspectores[0].estado == 0) {
                    tmp.resp.error = 401;
                    tmp.resp.mensaje = 'El inspector no esta activo.';
                } else if (tmp.inspectores[0].estado == 1) {
                  // El inspector existe y la password corresponde
                  // Actualizo el dispositivo en el login...
                  //28-04-17 : LOGIN y registrarToken son los unicos que tienen DEVICE_TOKEN en el paylo
                  //console.log(tmp.paylo);
                  //9-05-17 : Actualizar el uidd segun el login.
                  //8-10-17: Actualizamos fecha y diferencia horaria del usuario.
                  wait.forMethod(res.app.settings._conn, 'query',
                      `UPDATE inspectores
                        SET uidd = ?, device = ?, device_token = ?, lat_inspector = ?, lon_inspector = ?, dif_horaria = ?
                        WHERE id = ?`, [ tmp.uidd, tmp.device, tmp.device_token, tmp.lat, tmp.lon, tmp.fecha, tmp.inspectores[0].id ]);
                    let tmp_2 = {
                        tareas: {
                            entrantes: {
                                normal: [],
                                critica: []
                            },
                            emergencias: {
                                ubicacion: [],
                                perfil: []
                            }
                        },
                        historial_tareas: [],
                        mistareas: []
                    }
                    // Volvemos a obtener la información del inspector, dado que puede haberse logeado en otro telefono, esto cambiaria su información.
                    // 2017-12-29: Cambiamos los datos directos para evitar hacer otra llamada a la base de datos.
                    tmp.inspectores[0].uidd = tmp.uidd;
                    tmp.inspectores[0].device = tmp.device;
                    tmp.inspectores[0].device_token = tmp.device_token;
                    tmp.inspectores[0].lat_inspector = tmp.lat;
                    tmp.inspectores[0].lon_inspector = tmp.lon;
                    tmp.inspectores[0].dif_horaria = tmp.fecha;
                    tmp.resp.inspector = tmp.inspectores[0];
                    // tmp.resp.inspector = wait.forMethod(res.app.settings._conn, 'query',
                    //     `SELECT *
                    //       FROM inspectores
                    //       WHERE correo = ?
                    //       AND password = ?`,[tmp.correo,tmp.password])[0];
                    // Formatemos la fecha de nacimiento del inspector.
                    tmp.resp.inspector.fecha_nacimiento = moment(tmp.resp.inspector.fecha_nacimiento).format('YYYY-MM-DD');
                    // Creamos la lista de dias segun el formato requerido para el select, t.d1 = 1, t.d2 = 0, etc
                    let lista_dias = [];
                    _.select([tmp.resp.inspector.d1, tmp.resp.inspector.d2, tmp.resp.inspector.d3, tmp.resp.inspector.d4, tmp.resp.inspector.d5, tmp.resp.inspector.d6, tmp.resp.inspector.d7], function(num, index) {
                        if (num == 1) { lista_dias.push(`t.d${index+1} = 1`); }
                        return false;
                    });
                    // Agregamos And al principio y todo lo demas con OR AND (t.d1 = 1 OR t.d2 = 0, etc)
                    if (lista_dias.length > 0) {
                      lista_dias = `AND (${lista_dias.join(" OR ")})`;
                    } else {
                      lista_dias = "";
                    }
                    //console.log("A buscar por perfil inspector : ",typeof lista_dias,lista_dias.join(" OR "));
                    // EMERGENCIAS: PERFIL
                      if (tmp.resp.inspector.disponibilidad_viajar_pais == 1) {// Esta dispuesto a viajar fuera del pais?
                          tmp_2.tareas.emergencias.perfil = wait.forMethod(res.app.settings._conn, 'query',
                              `SELECT ${tmp.select}
                                FROM tareas AS t
                                LEFT JOIN asegurado AS a ON a.id = t.id_asegurado
                                WHERE (t.id_inspector = 0 OR t.id_inspector = null)
                                AND t.estado_tarea = 1
                                AND t.categoria = 1
                                ${lista_dias}
                                AND t.lat <> "" AND t.lon <> ""`);
                      } else {//No esta dispuesto a viajar fuera del pais
                          if (tmp.resp.inspector.disponibilidad_viajar_ciudad) {//dispuesto a viajar fuera de la ciudad pero no del pais
                              tmp_2.tareas.emergencias.perfil = wait.forMethod(res.app.settings._conn, 'query',
                                  `SELECT ${tmp.select}
                                    FROM tareas AS t
                                    LEFT JOIN asegurado AS a ON a.id = t.id_asegurado
                                    WHERE (t.id_inspector = 0 OR t.id_inspector = null)
                                    AND t.estado_tarea = 1
                                    AND t.categoria = 1
                                    AND t.pais = ?
                                    AND t.nivel_1 = ?
                                    ${lista_dias}
                                    AND t.lat <> "" AND t.lon <> ""`, [ tmp.resp.inspector.pais_google, tmp.resp.inspector.nivel_1_google ]);
                          } else {//no esta dispuesto a viajar fuera de la ciudad
                              tmp_2.tareas.emergencias.perfil = wait.forMethod(res.app.settings._conn, 'query',
                                  `SELECT ${tmp.select}
                                    FROM tareas AS t
                                    LEFT JOIN asegurado AS a ON a.id = t.id_asegurado
                                    WHERE (t.id_inspector = 0 OR t.id_inspector = null)
                                    AND t.estado_tarea = 1
                                    AND t.categoria = 1
                                    AND t.pais = ?
                                    AND t.nivel_1 = ?
                                    AND t.nivel_2 = ?
                                    ${lista_dias}
                                    AND t.lat <> "" AND t.lon <> ""`, [ tmp.resp.inspector.pais_google, tmp.resp.inspector.nivel_1_google, tmp.resp.inspector.nivel_2_google ]);
                          }
                      }
                      //Aplico distancia, los ordeno por la misma y aplico limite de busqueda en km y rango de busqueda (cantidad de tareas entregadas hasta el rango).
                      tmp.resp.tareas.emergencias.perfil = wait.forMethod(self, 'busquedaTareas', tmp_2.tareas.emergencias.perfil, tmp.resp.inspector, tmp.limites, "emergencia");
                      //tmp.resp.tareas.emergencias.perfil = self.busquedaTareas(tmp_2.tareas.emergencias.perfil,[],tmp.resp.inspector,tmp.limites,tmp.limites.limite_distancia);

                    //EMERGENCIAS: DISTANCIA
                      tmp_2.tareas.emergencias.ubicacion = wait.forMethod(res.app.settings._conn, 'query',
                          `SELECT ${tmp.select}
                            FROM tareas AS t
                            LEFT JOIN asegurado AS a ON a.id = t.id_asegurado
                            WHERE t.id_inspector = 0
                            AND t.estado_tarea = 1
                            AND t.categoria = 1
                            ${lista_dias}
                            AND t.lat <> "" AND t.lon <> ""`);
                      //Aplico distancia, los ordeno por la misma y aplico limite de busqueda en km y rango de busqueda (cantidad de tareas entregadas hasta el rango).
                      tmp.resp.tareas.emergencias.ubicacion = wait.forMethod(self, 'busquedaTareas', tmp_2.tareas.emergencias.ubicacion, { lat_dir: tmp.lat, lon_dir: tmp.lon }, tmp.limites, "emergencia");

                      // Todas las tareas de hoy / todas las tareas de a cuerdo al perfil del inspector
                    //ENTRANTES: NORMAL
                      tmp_2.tareas.entrantes.normal = wait.forMethod(res.app.settings._conn, 'query',
                          `SELECT ${tmp.select}
                            FROM tareas AS t
                            LEFT JOIN asegurado AS a ON a.id = t.id_asegurado
                            WHERE t.id_inspector = 0
                            AND t.estado_tarea = 1
                            AND t.tipo_tarea = 0
                            AND t.categoria = 0
                            AND t.pais = ?
                            AND t.nivel_1 = ?
                            AND t.nivel_2 = ?
                            ${lista_dias}
                            AND t.lat <> "" AND t.lon <> ""
                            AND date_format(t.fecha_tarea,'%Y-%m-%d') >= date_format(DATE_ADD(now(), INTERVAL ? HOUR),'%Y-%m-%d')`, [ tmp.resp.inspector.pais_google, tmp.resp.inspector.nivel_1_google, tmp.resp.inspector.nivel_2_google, tmp.resp.inspector.dif_horaria]);
                      //Aplico distancia, los ordeno por la misma y aplico limite de busqueda en km y rango de busqueda (cantidad de tareas entregadas hasta el rango).
                      tmp.resp.tareas.entrantes.normal = wait.forMethod(self, 'busquedaTareas', tmp_2.tareas.entrantes.normal, tmp.resp.inspector, tmp.limites, "entrantes");
                      // ENTRANTES: CRITICA
                      tmp_2.tareas.entrantes.critica = wait.forMethod(res.app.settings._conn, 'query',
                          `SELECT ${tmp.select}
                            FROM tareas AS t
                            LEFT JOIN asegurado AS a ON a.id = t.id_asegurado
                            WHERE t.id_inspector = 0
                            AND t.estado_tarea = 1
                            AND t.tipo_tarea = 1
                            AND t.categoria = 0
                            AND t.pais = ?
                            AND t.nivel_1 = ?
                            AND t.nivel_2 = ?
                            ${lista_dias}
                            AND t.lat <> "" AND t.lon <> ""`, [ tmp.resp.inspector.pais_google, tmp.resp.inspector.nivel_1_google, tmp.resp.inspector.nivel_2_google]);
                      //Aplico distancia, los ordeno por la misma y aplico limite de busqueda en km y rango de busqueda (cantidad de tareas entregadas hasta el rango).
                      tmp.resp.tareas.entrantes.critica = wait.forMethod(self, 'busquedaTareas', tmp_2.tareas.entrantes.critica, tmp.resp.inspector, tmp.limites, "entrantes");
                    //HISTORIAL
                      tmp.resp.tareas.historial_tareas = wait.forMethod(res.app.settings._conn, 'query',
                          `SELECT
                            ${tmp.select_historial}
                            FROM tareas AS t
                            LEFT JOIN asegurado AS a ON a.id = t.id_asegurado
                            WHERE t.id_inspector = ?
                            AND t.estado_tarea >= 6
                            ORDER BY t.fecha_termino ASC`, [ tmp.resp.inspector.id ]);
                    //MISTAREAS
                      // mis tareas (se entregan dado que puede logearse en otro disp y tener sus tareas): 23-04-2017, necesita la distancia?????
                      //tmp.resp.tareas.historial_tareas = wait.forMethod(self,'busquedaTareas',tmp_2.historial_tareas,tmp.resp.inspector,tmp.limites);
                      tmp_2.mistareas = wait.forMethod(res.app.settings._conn, 'query', `SELECT ${tmp.select}
                        FROM tareas AS t
                        LEFT JOIN asegurado AS a ON a.id = t.id_asegurado
                        WHERE t.id_inspector = ?
                        AND t.estado_tarea BETWEEN 2 AND 4
                        AND date_format(t.fecha_tarea,'%Y-%m-%d') >= date_format(DATE_ADD(now(), INTERVAL ? HOUR),'%Y-%m-%d')`, [ tmp.resp.inspector.id, tmp.resp.inspector.dif_horaria ]);
                      // 6/10/2017: Pablo solicita que las mis tareas sean desde la ubicacion del inspector
                      //Aplico distancia, los ordeno por la misma y aplico limite de busqueda en km y rango de busqueda (cantidad de tareas entregadas hasta el rango).
                      tmp.resp.tareas.mistareas = wait.forMethod(self, 'busquedaTareas', tmp_2.mistareas, { lat_dir: tmp.lat, lon_dir: tmp.lon }, tmp.limites, "mistareas");
                    res.send(tmp.resp);
                    self.log("u-adjust", "login", tmp.resp, res);
                } else if (tmp.resp.inspector.estado == 2) {
                    tmp.resp.error = 402;
                    tmp.resp.mensaje = 'El inspector esta bloqueado';
                    res.send(tmp.resp);
                    self.log("u-adjust", "login", tmp.resp, res);
                }
            } else {
                tmp.resp.error = 401;
                tmp.resp.mensaje = "El inspector no existe o lo ingresado no corresponde.";
                res.send(tmp.resp);
                self.log("u-adjust", "login", tmp.resp, res);
            }
        } else {
            res.send(tmp.resp);
            self.log("u-adjust", "login", tmp.resp, res);
        }
    }, //login
    obtenerTareasEntrantes: (req, res) => {
      // Creanis el objeto temporal con el selector a usar
        var tmp = {
            select: `t.id,t.num_caso,t.id_inspeccion,a.id AS asegurado_id, a.codigo_identificador AS asegurado_codigo_identificador, a.nombre AS asegurado_nombre, a.tel_fijo AS asegurado_tel_fijo, a.tel_movil AS asegurado_tel_movil, a.correo AS asegurado_correo,t.asegurador,t.id_inspector,date_format(t.fecha_tarea,'%Y-%m-%d') AS fecha_tarea,date_format(t.fecha_ingreso,'%Y-%m-%d') AS fecha_ingreso, date_format(t.fecha_siniestro,'%Y-%m-%d') AS fecha_siniestro, t.direccion, t.pais, t.pais AS pais_, t.nivel_1, t.nivel_1 AS nivel_1_, t.nivel_2, t.nivel_3, t.nivel_4, t.nivel_5, t.categoria, t.bono, t.lat, t.lon, t.tipo_tarea, t.estado_tarea, t.comentario_can_o_rech, t.d1, t.d2, t.d3, t.d4, t.d5, t.d6, t.d7, t.evento`,
            resp: {
                error: 0,
                mensaje: "",
                critica: [],
                normal: [],
                inspector: {}
            },
            id_inspector: req.body.id_inspector,
            limites: self.config(res)
        };
        if (!_.has(req.body, 'id_inspector') || _.isUndefined(req.body.id_inspector) || _.isNull(req.body.id_inspector) || _.isEmpty(req.body.id_inspector)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "id_inspector no existe.";
        }
        if (tmp.resp.error == 0) {
          // Obtenemos los datos del inspector
          tmp.inspectores = wait.forMethod(res.app.settings._conn, 'query', `SELECT * FROM inspectores WHERE id = ?`,[tmp.id_inspector]);
          if (tmp.inspectores.length >= 1) { //Si el inspector existe
              let tmp_2 = { normal: [], critica: [] }
              tmp.resp.inspector = tmp.inspectores[0];//Traspasamos los datos del inspector a la respuesta para la App.
              tmp.resp.inspector.fecha_nacimiento = moment(tmp.resp.inspector.fecha_nacimiento).format('YYYY-MM-DD'); //Formatemaos la fecha de nacimiento segun se requiere.
              // Creamos la lista de dias segun el formato requerido para el select, t.d1 = 1, t.d2 = 0, etc
              let lista_dias = [];
              _.select([tmp.inspectores[0].d1, tmp.inspectores[0].d2, tmp.inspectores[0].d3, tmp.inspectores[0].d4, tmp.inspectores[0].d5, tmp.inspectores[0].d6, tmp.inspectores[0].d7], function(num, index) {
                  if (num == 1) { lista_dias.push(`t.d${index+1} = 1`); }
                  return false;
              });
              // Agregamos And al principio y todo lo demas con OR AND (t.d1 = 1 OR t.d2 = 0, etc)
              if (lista_dias.length > 0) {
                lista_dias = `AND (${lista_dias.join(" OR ")})`;
              } else {
                lista_dias = "";
              }
              //console.log("A buscar por perfil inspector : ",typeof lista_dias,lista_dias.join(" OR "));
              // Obtengo los dias que trabaja, y con eso creo un string para buscar
              // ENTRANTES: NORMAL
                // todas las tareas de a cuerdo al perfil del inspector
                tmp_2.normal = wait.forMethod(res.app.settings._conn, 'query',
                    `SELECT ${tmp.select}
                      FROM tareas AS t
                      LEFT JOIN asegurado AS a ON a.id = t.id_asegurado
                      WHERE t.id_inspector = 0
                      AND t.estado_tarea = 1
                      AND t.tipo_tarea = 0
                      AND t.categoria = 0
                      AND t.pais = ?
                      AND t.nivel_1 = ?
                      AND t.nivel_2 = ?
                      ${lista_dias}
                      AND t.lat <> "" AND t.lon <> ""
                      AND date_format(t.fecha_tarea,'%Y-%m-%d') >= date_format(DATE_ADD(now(), INTERVAL ? HOUR),'%Y-%m-%d')
                      ORDER BY t.fecha_tarea DESC`, [ tmp.resp.inspector.pais_google, tmp.resp.inspector.nivel_1_google, tmp.resp.inspector.nivel_2_google, tmp.inspectores[0].dif_horaria]);
                //Aplico distancia, los ordeno por la misma y aplico limite de busqueda en km y rango de busqueda (cantidad de tareas entregadas hasta el rango).
                tmp.resp.normal = wait.forMethod(self, 'busquedaTareas', tmp_2.normal, tmp.inspectores[0], tmp.limites, "entrantes");
              // ENTRANTES: CRITICAS
                tmp_2.critica = wait.forMethod(res.app.settings._conn, 'query',
                    `SELECT ${tmp.select}
                      FROM tareas AS t
                      LEFT JOIN asegurado AS a ON a.id = t.id_asegurado
                      WHERE t.id_inspector = 0
                      AND t.estado_tarea = 1
                      AND t.tipo_tarea = 1
                      AND t.categoria = 0
                      AND t.pais = ?
                      AND t.nivel_1 = ?
                      AND t.nivel_2 = ?
                      ${lista_dias}
                      AND t.lat <> "" AND t.lon <> ""
                      ORDER BY t.fecha_tarea DESC`, [ tmp.resp.inspector.pais_google, tmp.resp.inspector.nivel_1_google, tmp.resp.inspector.nivel_2_google]);
                //Aplico distancia, los ordeno por la misma y aplico limite de busqueda en km y rango de busqueda (cantidad de tareas entregadas hasta el rango).
                tmp.resp.critica = wait.forMethod(self, 'busquedaTareas', tmp_2.critica, tmp.inspectores[0], tmp.limites, "entrantes");
              res.send(tmp.resp);
              self.log("u-adjust", "obtenerTareasEntrantes", tmp.resp, res);
          } else {
              tmp.resp.error = 400;
              tmp.resp.mensaje = "El inspector no existe o lo ingresado no corresponde.";
              res.send(tmp.resp);
              self.log("u-adjust", "obtenerTareasEntrantes", tmp.resp, res);
          }
        } else {
            res.send(tmp.resp);
            self.log("u-adjust", "obtenerTareasEntrantes", tmp.resp, res);
        }
    },
    obtenerEmergencias: (req, res) => {
      // Creanis el objeto temporal con el selector a usar
      var tmp = {
          select: `t.id,t.num_caso,t.id_inspeccion,a.id AS asegurado_id, a.codigo_identificador AS asegurado_codigo_identificador, a.nombre AS asegurado_nombre, a.tel_fijo AS asegurado_tel_fijo, a.tel_movil AS asegurado_tel_movil, a.correo AS asegurado_correo,t.asegurador,t.id_inspector,date_format(t.fecha_tarea,'%Y-%m-%d') AS fecha_tarea,date_format(t.fecha_ingreso,'%Y-%m-%d') AS fecha_ingreso, date_format(t.fecha_siniestro,'%Y-%m-%d') AS fecha_siniestro, t.direccion, t.pais, t.pais AS pais_, t.nivel_1, t.nivel_1 AS nivel_1_, t.nivel_2, t.nivel_3, t.nivel_4, t.nivel_5, t.categoria, t.bono, t.lat, t.lon, t.tipo_tarea, t.estado_tarea, t.comentario_can_o_rech, t.d1, t.d2, t.d3, t.d4, t.d5, t.d6, t.d7, t.evento`,
          resp: {
              error: 0,
              mensaje: "",
              perfil: [],
              ubicacion: [],
              inspector: {}
          },
          id_inspector: req.body.id_inspector,
          lat: req.body.lat,
          lon: req.body.lon,
          limites: self.config(res)
      }
      if (!_.has(req.body, 'id_inspector') || _.isUndefined(req.body.id_inspector) || _.isNull(req.body.id_inspector) || _.isEmpty(req.body.id_inspector)) {
          tmp.resp.error = 400;
          tmp.resp.mensaje = "id_inspector no existe.";
      }
      if (!_.has(req.body, 'lat') || _.isUndefined(req.body.lat) || _.isNull(req.body.lat) || _.isEmpty(req.body.lat)) {
          tmp.resp.error = 400;
          tmp.resp.mensaje = "lat no existe.";
      }
      if (!_.has(req.body, 'lon') || _.isUndefined(req.body.lon) || _.isNull(req.body.lon) || _.isEmpty(req.body.lon)) {
          tmp.resp.error = 400;
          tmp.resp.mensaje = "lon no existe.";
      }
      //console.log('obtenerEmergencias => ', req.body);
      if (tmp.resp.error == 0) {

            // Obtenemos los datos del inspector
            tmp.inspectores = wait.forMethod(res.app.settings._conn, 'query', `SELECT * FROM inspectores WHERE id = ?`,[tmp.id_inspector]);
            if (tmp.inspectores.length >= 1) {
                let tmp_2 = { ubicacion: [], perfil: [] }
                // Actualizamos la lat y lon del inspector
                wait.forMethod(req.app.settings._conn, 'query', `UPDATE inspectores SET lat_inspector = ?, lon_inspector = ? WHERE id = ?`,[tmp.lat,tmp.lon,tmp.id_inspector]);
                tmp.inspectores[0].lat_inspector = tmp.lat;
                tmp.inspectores[0].lon_inspector = tmp.lon;
                tmp.resp.inspector = tmp.inspectores[0];
                //Le damos formato a la fecha de nacimiento
                tmp.resp.inspector.fecha_nacimiento = moment(tmp.resp.inspector.fecha_nacimiento).format('YYYY-MM-DD');
                // Creamos la lista de dias segun el formato requerido para el select, t.d1 = 1, t.d2 = 0, etc
                let lista_dias = [];
                _.select([tmp.inspectores[0].d1, tmp.inspectores[0].d2, tmp.inspectores[0].d3, tmp.inspectores[0].d4, tmp.inspectores[0].d5, tmp.inspectores[0].d6, tmp.inspectores[0].d7], function(num, index) {
                    if (num == 1) { lista_dias.push(`t.d${index+1} = 1`); }
                    return false;
                });
                // Agregamos And al principio y todo lo demas con OR AND (t.d1 = 1 OR t.d2 = 0, etc)
                if (lista_dias.length > 0) {
                  lista_dias = `AND (${lista_dias.join(" OR ")})`;
                } else {
                  lista_dias = "";
                }
                //console.log("A buscar por perfil inspector : ",typeof lista_dias,lista_dias.join(" OR "));
                // EMERGENCIAS: PERFIL
                  //console.log(`Nivel_1 : ${tmp.inspectores[0].nivel_1}, nivel_2: ${tmp.inspectores[0].nivel_2}, limite_busqueda: ${self.config(res).limite_busqueda}, limite_distancia: ${self.config(res).limite_distancia}`);
                  if (tmp.resp.inspector.disponibilidad_viajar_pais == 1) {// Esta dispuesto a viajar fuera del pais?
                      tmp_2.perfil = wait.forMethod(res.app.settings._conn, 'query',
                          `SELECT ${tmp.select}
                            FROM tareas AS t
                            LEFT JOIN asegurado AS a ON a.id = t.id_asegurado
                            WHERE (t.id_inspector = 0 OR t.id_inspector = null)
                            AND t.estado_tarea = 1
                            AND t.categoria = 1
                            ${lista_dias}
                            AND t.lat <> "" AND t.lon <> ""
                            ORDER BY t.fecha_ingreso DESC`);
                  } else {//No esta dispuesto a viajar fuera del pais
                      if (tmp.resp.inspector.disponibilidad_viajar_ciudad) {//dispuesto a viajar fuera de la ciudad pero no del pais
                          tmp_2.perfil = wait.forMethod(res.app.settings._conn, 'query',
                              `SELECT ${tmp.select}
                                FROM tareas AS t
                                LEFT JOIN asegurado AS a ON a.id = t.id_asegurado
                                WHERE (t.id_inspector = 0 OR t.id_inspector = null)
                                AND t.estado_tarea = 1
                                AND t.categoria = 1
                                AND t.pais = ?
                                AND t.nivel_1 = ?
                                ${lista_dias}
                                AND t.lat <> "" AND t.lon <> ""
                                ORDER BY t.fecha_ingreso DESC`, [ tmp.resp.inspector.pais_google, tmp.resp.inspector.nivel_1_google ]);
                      } else {//no esta dispuesto a viajar fuera de la ciudad
                          tmp_2.perfil = wait.forMethod(res.app.settings._conn, 'query',
                              `SELECT ${tmp.select}
                                FROM tareas AS t
                                LEFT JOIN asegurado AS a ON a.id = t.id_asegurado
                                WHERE (t.id_inspector = 0 OR t.id_inspector = null)
                                AND t.estado_tarea = 1
                                AND t.categoria = 1
                                AND t.pais = ?
                                AND t.nivel_1 = ?
                                AND t.nivel_2 = ?
                                ${lista_dias}
                                AND t.lat <> "" AND t.lon <> ""
                                ORDER BY t.fecha_ingreso DESC`, [ tmp.resp.inspector.pais_google, tmp.resp.inspector.nivel_1_google, tmp.resp.inspector.nivel_2_google ]);
                      }
                  }
                //Aplico distancia, los ordeno por la misma y aplico limite de busqueda en km y rango de busqueda (cantidad de tareas entregadas hasta el rango).
                tmp.resp.perfil = wait.forMethod(self, 'busquedaTareas', tmp_2.perfil, tmp.inspectores[0], tmp.limites, "emergencia");
                // EMERGENCIAS: UBICACION
                  tmp_2.ubicacion = wait.forMethod(res.app.settings._conn, 'query',
                      `SELECT ${tmp.select}
                        FROM tareas AS t
                        LEFT JOIN asegurado AS a ON a.id = t.id_asegurado
                        WHERE t.id_inspector = 0
                        AND t.estado_tarea = 1
                        AND t.categoria = 1
                        ${lista_dias}
                        AND t.lat <> "" AND t.lon <> ""
                        ORDER BY t.fecha_ingreso DESC`);
                  //Aplico distancia, los ordeno por la misma y aplico limite de busqueda en km y rango de busqueda (cantidad de tareas entregadas hasta el rango).
                  tmp.resp.ubicacion = wait.forMethod(self, 'busquedaTareas', tmp_2.ubicacion, { lat_dir: tmp.lat, lon_dir: tmp.lon }, tmp.limites, "emergencia");
                res.send(tmp.resp);
                self.log("u-adjust", "obtenerEmergencias", tmp.resp, res);
            } else {
                tmp.resp.error = 400;
                tmp.resp.mensaje = "El inspector no existe o lo ingresado no corresponde.";
                res.send(tmp.resp);
                self.log("u-adjust", "obtenerEmergencias", tmp.resp, res);
            }

      } else {
          res.send(tmp.resp);
          self.log("u-adjust", "obtenerEmergencias", tmp.resp, res);
      }
    },
    obtenerMisTareas: (req, res) => {
      var tmp = {
          select: `t.id,t.num_caso,t.id_inspeccion,a.id AS asegurado_id, a.codigo_identificador AS asegurado_codigo_identificador, a.nombre AS asegurado_nombre, a.tel_fijo AS asegurado_tel_fijo, a.tel_movil AS asegurado_tel_movil, a.correo AS asegurado_correo,t.asegurador,t.id_inspector,date_format(t.fecha_tarea,'%Y-%m-%d') AS fecha_tarea,date_format(t.fecha_ingreso,'%Y-%m-%d') AS fecha_ingreso, date_format(t.fecha_siniestro,'%Y-%m-%d') AS fecha_siniestro, t.direccion, t.pais, t.pais AS pais_, t.nivel_1, t.nivel_1 AS nivel_1_, t.nivel_2, t.nivel_3, t.nivel_4, t.nivel_5, t.categoria, t.bono, t.lat, t.lon, t.tipo_tarea, t.estado_tarea, t.comentario_can_o_rech, t.d1, t.d2, t.d3, t.d4, t.d5, t.d6, t.d7, t.evento`,
          resp: {
              error: 0,
              mensaje: "",
              mistareas: [],
              inspector: {}
          },
          id_inspector: req.body.id_inspector,
          lat: req.body.lat,
          lon: req.body.lon,
          limites: self.config(res)
      }
      if (!_.has(req.body, 'id_inspector') || _.isUndefined(req.body.id_inspector) || _.isNull(req.body.id_inspector) || _.isEmpty(req.body.id_inspector)) {
          tmp.resp.error = 400;
          tmp.resp.mensaje = "id_inspector no existe.";
      }
      if (!_.has(req.body, 'lat') || _.isUndefined(req.body.lat) || _.isNull(req.body.lat) || _.isEmpty(req.body.lat)) {
          tmp.resp.error = 400;
          tmp.resp.mensaje = "lat no existe.";
      }
      if (!_.has(req.body, 'lon') || _.isUndefined(req.body.lon) || _.isNull(req.body.lon) || _.isEmpty(req.body.lon)) {
          tmp.resp.error = 400;
          tmp.resp.mensaje = "lon no existe.";
      }
      // console.log('obtenerMisTareas => ', req.body);
      if (tmp.resp.error == 0) {
          // Obtenemos los datos del inspector si es que existe.
          tmp.inspectores = wait.forMethod(res.app.settings._conn, 'query', `SELECT * FROM inspectores WHERE id = ?`,[tmp.id_inspector]);
          if (tmp.inspectores.length >= 1) {//Si el inspector existe
            // Actualizamos la latitud y longitud del inspector.
            wait.forMethod(req.app.settings._conn, 'query', `UPDATE inspectores SET lat_inspector = ?, lon_inspector = ? WHERE id = ?`,[tmp.lat,tmp.lon,tmp.id_inspector]);
            tmp.inspectores[0].lat_inspector = tmp.lat;
            tmp.inspectores[0].lon_inspector = tmp.lon;
            tmp.resp.inspector = tmp.inspectores[0];
            // Obtenemos las tareas del inspector.
            let mistareas = wait.forMethod(res.app.settings._conn, 'query', `SELECT ${tmp.select}
              FROM tareas AS t
              LEFT JOIN asegurado AS a ON a.id = t.id_asegurado
              WHERE t.id_inspector = ?
              AND t.estado_tarea BETWEEN 2 AND 4
              AND date_format(t.fecha_tarea,'%Y-%m-%d') >= date_format(DATE_ADD(now(), INTERVAL ? HOUR),'%Y-%m-%d')`, [ tmp.inspectores[0].id, tmp.inspectores[0].dif_horaria ]);
            //Aplico distancia, los ordeno por la misma y aplico limite de busqueda en km y rango de busqueda (cantidad de tareas entregadas hasta el rango).
            tmp.resp.mistareas = wait.forMethod(self, 'busquedaTareas', mistareas, { lat_dir: tmp.lat, lon_dir: tmp.lon }, tmp.limites, "mistareas");
            res.send(tmp.resp);
            self.log("u-adjust", "obtenerMisTareas", tmp.resp, res);
          } else {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "El inspector no existe o lo ingresado no corresponde.";
            res.send(tmp.resp);
            self.log("u-adjust", "obtenerMisTareas", tmp.resp, res);
          }
      } else {
          res.send(tmp.resp);
          self.log("u-adjust", "obtenerMisTareas", tmp.resp, res);
      }
    },
    obtenerHistorialTareas: (req, res) => {
      var tmp = {
          select: `t.id,t.num_caso,t.id_inspeccion,a.id AS asegurado_id, a.codigo_identificador AS asegurado_codigo_identificador, a.nombre AS asegurado_nombre, a.tel_fijo AS asegurado_tel_fijo, a.tel_movil AS asegurado_tel_movil, a.correo AS asegurado_correo,t.asegurador,t.id_inspector,date_format(t.fecha_tarea,'%Y-%m-%d') AS fecha_tarea,date_format(t.fecha_ingreso,'%Y-%m-%d') AS fecha_ingreso, date_format(t.fecha_siniestro,'%Y-%m-%d') AS fecha_siniestro, date_format(t.fecha_termino,'%Y-%m-%d') AS fecha_finalizacion, date_format(t.fecha_termino,'%H:%i:%S') AS hora_finalizacion, t.direccion, t.pais, t.pais AS pais_, t.nivel_1, t.nivel_1 AS nivel_1_, t.nivel_2, t.nivel_3, t.nivel_4, t.nivel_5, t.categoria, t.bono, t.lat, t.lon, t.tipo_tarea, t.estado_tarea, t.comentario_can_o_rech, t.d1, t.d2, t.d3, t.d4, t.d5, t.d6, t.d7, t.evento`,
          resp: {
              error: 0,
              mensaje: "",
              historial_tareas: [],
              inspector: {}
          },
          id_inspector: req.body.id_inspector
      }
      if (!_.has(req.body, 'id_inspector') || _.isUndefined(req.body.id_inspector) || _.isNull(req.body.id_inspector) || _.isEmpty(req.body.id_inspector)) {
          tmp.resp.error = 400;
          tmp.resp.mensaje = "id_inspector no existe.";
      }
      if (tmp.resp.error == 0) {
          // Obtenemos los datos del inspector si es que existe.
          tmp.inspectores = wait.forMethod(res.app.settings._conn, 'query', `SELECT * FROM inspectores WHERE id = ?`,[tmp.id_inspector]);
          if (tmp.inspectores.length >= 1) {//Si el inspector existe
            tmp.resp.inspector = tmp.inspectores[0];
            // Obtenemos el historial de tareas del inspector.
            tmp.resp.historial_tareas = wait.forMethod(res.app.settings._conn, 'query',
                `SELECT
                  ${tmp.select}
                  FROM tareas AS t
                  LEFT JOIN asegurado AS a ON a.id = t.id_asegurado
                  WHERE t.id_inspector = ?
                  AND t.estado_tarea >= 6
                  ORDER BY t.fecha_termino ASC
                  LIMIT 100`, [ tmp.resp.inspector.id ]);
            res.send(tmp.resp);
            self.log("u-adjust", "obtenerHistorialTareas", tmp.resp, res);
          } else {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "El inspector no existe o lo ingresado no corresponde.";
            res.send(tmp.resp);
            self.log("u-adjust", "obtenerHistorialTareas", tmp.resp, res);
          }
      } else {
          res.send(tmp.resp);
          self.log("u-adjust", "obtenerHistorialTareas", tmp.resp, res);
      }
    },
    /**
     * Filtra las tareas por distancia y limite segun corresponda.
     *
     * @name busquedaTareas
     * @method
     * @param {array} tareas - Tareas a filtrar (ya filtradas por su perfil).
     * @param {object} inspector - Datos del inspector.
     * @param {object} limites - Configuracion a aplicar de distancia y limite de busqueda.
     * @param {function} cb - Callback.
     * @private
     * @returns {object} Tareas filtradas.
     */
    busquedaTareas: (tareas, inspector, limites, donde, cb) => {
        //console.log(donde);
        var retorna = [];
        if (tareas) {
            if (tareas.length != 0) {
                //Mapeo la funcion para obtener las distancias...
                var mapa = _.map(tareas, function(x) {
                  // var helper = {};
                  // 6/10/2017: Error al intepretar las distancias.
                  // if (_.has(inspector,'lat_inspector') && _.has(inspector,'lon_inspector') && inspector.lat_inspector != "" && inspector.lon_inspector != "") {
                  //   helper = self.dist(inspector.lat_inspector, inspector.lon_inspector, x.lat, x.lon);
                  // } else {
                  //   helper = self.dist(inspector.lat_dir, inspector.lon_dir, x.lat, x.lon);
                  // }
                  var helper = self.dist(inspector.lat_dir, inspector.lon_dir, x.lat, x.lon);
                  x.distancia = (helper.distancia == undefined ||  helper.distancia == null || helper.distancia == "") ? 0 : helper.distancia;
                  x.distancia_2 = (helper.distancia_2 == undefined ||  helper.distancia_2 == null || helper.distancia_2 == "") ? 0 : helper.distancia_2;
                  return x;
                });
                //El retorna filtra las tareas menores al limite de distancia, luego las ordena y obtiene las primeras del limite de busqueda.
                if (donde == "emergencia") {
                    retorna = _.first(_.sortBy(_.filter(mapa, function(y) { 
                                                                if (y.distancia <= limites.limite_distancia) { 
                                                                    return y; 
                                                                } 
                                                              }), 'distancia_2'), limites.limite_busqueda);
                } else {
                    retorna = _.first(_.sortBy(_.filter(mapa,   function(y) { 
                                                                    if (y.distancia <= limites.limite_distancia) { 
                                                                        return y; 
                                                                    } 
                                                                }
                                                        ), 
                                                function(tarea) { 
                                                    return (new Date(tarea.fecha_tarea).getTime()); 
                                                }
                                        ), limites.limite_busqueda);
                }

                //Si el tamaño es menor al limite de busqueda esperado, retornamos las tareas sin filtrarlas por limite de distancia.
                //console.log(tmp.resp.tareas.emergencias.perfil.length);
                if (typeof(retorna) == "undefined") {
                    console.log('psb retorna undefined: debug busquedaTareas (donde='+donde+') mapa.length='+mapa.length+' y mapa=',mapa);
                }
                if (typeof(retorna) != "undefined" && retorna.length && retorna.length < limites.limite_busqueda) {
                    //console.log('Los resultados son menores a los esperados : ', retorna.length);
                    if (donde == "emergencia") {
                        retorna = _.first(_.sortBy(mapa, 'distancia_2'), limites.limite_busqueda);
                    } else {
                        retorna = _.first(_.sortBy(mapa, function(tarea) { return (new Date(tarea.fecha_tarea).getTime()); }), limites.limite_busqueda);
                    }
                    cb(false, retorna);
                } else if (typeof(retorna) != "undefined" && retorna.length >= limites.limite_busqueda) {
                    cb(false, retorna);
                } else {
                    // 20-4-18 error length no existe con usuario vnavarro.
                    console.log('error vnavarro usa, retorno esta undefined');
                    cb(false, []);
                }
            } else {
                cb(false, []);
            }
        } else {
            cb(false, []);
        }
    },
    /**
     * Calcula la distancia en metros entre un punto de lat/lon a otro punto de lat/lon.
     *
     * @name dist
     * @method
     * @param {string} lat1 - Latitud de inicio.
     * @param {string} lon1 - Longitud de inicio.
     * @param {string} lat2 - Latitud de termino.
     * @param {string} lon2 - Longitud de termino.
     * @private
     * @returns {float} Distancia en Metros.
     */
    dist: (lat1, lon1, lat2, lon2) => {
        var R = 6371; // Radius of the earth in km
        var dLat = self.deg2rad(lat2 - lat1); // deg2rad below
        var dLon = self.deg2rad(lon2 - lon1);
        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(self.deg2rad(
            lat1)) * Math.cos(self.deg2rad(lat2)) * Math.sin(dLon / 2) * Math.sin(
            dLon / 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = R * c; // Distance in km
        return { "distancia": Math.ceil(Math.round(d * 1e2) / 1e2), "distancia_2": Math.ceil(Math.round(d * 1e2))};
    },
    /**
     * Transforma deg a radians.
     *
     * @name deg2rad
     * @method
     * @param {float} deg - El numero a transformar.
     * @private
     * @returns {float} radian.
     */
    deg2rad: (deg) => {return deg * (Math.PI / 180)},
    obtenerPais: (req, res) => {
        //console.log(res.app.settings._conn);
        var tmp = {
            resp: {
                error: 0,
                mensaje: "",
                paises: [],
                regiones: []
            }
        }
        tmp.resp.paises = wait.forMethod(res.app.settings._conn, 'query',
            `SELECT p.id, p.idpais, i.nombrecomun AS nombre, i.lenguaje, i.codigoalfa2 AS iso, p.nivel_1, lower(p.nivel_2) AS nivel_2, lower(p.nivel_3) AS nivel_3, lower(p.nivel_4) AS nivel_4, lower(p.nivel_5) AS nivel_5, p.label_codigo_identificador, p.sis_metrico, p.estado
              FROM paises AS p
              LEFT JOIN iso AS i ON i.id = p.idpais
              WHERE p.estado = 1`);
        tmp.resp.regiones = wait.forMethod(res.app.settings._conn, 'query',
            `SELECT i2.id, i2.id_pais, i2.codigoalfa2, i2.subdivision_name, i2.codigoalfa3
              FROM isosubdivision AS i2
              INNER JOIN iso AS i ON i.id = i2.id_pais
              INNER JOIN paises AS p ON p.idpais = i.id
              WHERE p.estado = 1`);
        tmp.resp.experiencia_oficio = wait.forMethod(res.app.settings._conn,'query',
            `SELECT eo.id, eo.idpais, eo.nombre
            FROM experiencia_oficio AS eo
            INNER JOIN paises AS p ON eo.idpais = p.id
            WHERE p.estado = 1`);
        res.send(tmp.resp);
        self.log("u-adjust", "obtenerPais", tmp.resp, res);
    }, //End obtenerPais
    registrarInspector: (req, res) => {
        var tmp = {
            resp: {
                error: 0,
                mensaje: ""
            },
            pais: req.body.pais, //*
            nombre: req.body.nombre, //*
            apellido_paterno: req.body.apellido_paterno, //*
            apellido_materno: req.body.apellido_materno, //No requerdo
            codigo_identificador: req.body.codigo_identificador, //*
            fecha_nacimiento: req.body.fecha_nacimiento, //*
            nivel_1: req.body.nivel_1, //*
            nivel_2: req.body.nivel_2, //no requerido
            nivel_3: req.body.nivel_3,
            nivel_4: req.body.nivel_4,
            nivel_5: req.body.nivel_5,
            direccion: req.body.direccion, //*
            disponibilidad: req.body.disponibilidad, //
            disp_viajar_ciudad: req.body.disp_viajar_ciudad, //
            disp_viajar_pais: req.body.disp_viajar_pais, //
            //disp_dias: "1,2,3,4,5,6,7",//no requerido, pero se ingresa igual
            // d1: "07002359"
            d1: req.body.d1,
            d2: req.body.d2,
            d3: req.body.d3,
            d4: req.body.d4,
            d5: req.body.d5,
            d6: req.body.d6,
            d7: req.body.d7,
            disp_horas: req.body.disp_horas, //no requerido, pero se ingresa igual
            correo: req.body.correo, //*
            oficio: req.body.oficio, //*
            experiencia: req.body.experiencia, //*
            uidd: req.body.uidd, //*
            telefono: req.body.telefono, //*
            correccion_direccion: req.body.correccion_direccion, //*
            device: req.body.device, //*
            latitude: req.body.latitude, //
            longitude: req.body.longitude, //
            google: req.body.google, //
            pais_google: "", //
            direccion_google: "", //
            nivel_1_google: "", //
            nivel_2_google: "", //
            nivel_3_google: "", //
            nivel_4_google: "", //
            nivel_5_google: "" //
        };
        //console.log(tmp);
        // if (!_.has(req.body,'pais')) {
        //
        // }
        if (!_.has(req.body, 'pais') || _.isUndefined(req.body.pais) || _.isNull(req.body.pais)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "pais no existe.";
        }
        if (!_.has(req.body, 'nombre') || _.isUndefined(req.body.nombre) || _.isNull(req.body.nombre) || _.isEmpty(req.body.nombre)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "nombre no existe.";
        }
        if (!_.has(req.body, 'apellido_paterno') || _.isUndefined(req.body.apellido_paterno) || _.isNull(req.body.apellido_paterno) || _.isEmpty(req.body.apellido_paterno)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "apellido_paterno no existe.";
        }
        if (!_.has(req.body, 'codigo_identificador') || _.isUndefined(req.body.codigo_identificador) || _.isNull(req.body.codigo_identificador) || _.isEmpty(req.body.codigo_identificador)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "codigo_identificador no existe.";
        }
        if (!_.has(req.body, 'fecha_nacimiento') || _.isUndefined(req.body.fecha_nacimiento) || _.isNull(req.body.fecha_nacimiento) || _.isEmpty(req.body.fecha_nacimiento)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "fecha_nacimiento no existe.";
        } else {
          tmp.fecha_nacimiento = moment(tmp.fecha_nacimiento).format('YYYY-MM-DD');
        }
        if (!_.has(req.body, 'nivel_1') || _.isUndefined(req.body.nivel_1) || _.isNull(req.body.nivel_1)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "nivel_1 no existe.";
        }
        if (!_.has(req.body, 'direccion') || _.isUndefined(req.body.direccion) || _.isNull(req.body.direccion) || _.isEmpty(req.body.direccion)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "direccion no existe.";
        }
        if (!_.has(req.body, 'correo') || _.isUndefined(req.body.correo) || _.isNull(req.body.correo) || _.isEmpty(req.body.correo)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "correo no existe.";
        }
        if (!_.has(req.body, 'oficio') || _.isUndefined(req.body.oficio) || _.isNull(req.body.oficio)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "oficio no existe.";
        }
        if (!_.has(req.body, 'experiencia') || _.isUndefined(req.body.experiencia) || _.isNull(req.body.experiencia) || _.isEmpty(req.body.experiencia)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "experiencia no existe.";
        }
        if (!_.has(req.body, 'uidd') || _.isUndefined(req.body.uidd) || _.isNull(req.body.uidd) || _.isEmpty(req.body.uidd)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "uidd no existe.";
        }
        if (!_.has(req.body, 'telefono') || _.isUndefined(req.body.telefono) || _.isNull(req.body.telefono) || _.isEmpty(req.body.telefono)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "telefono no existe.";
        }
        if (!_.has(req.body, 'correccion_direccion') || _.isUndefined(req.body.correccion_direccion) ||_.isNull(req.body.correccion_direccion) || _.isEmpty(req.body.correccion_direccion)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "correccion_direccion no existe.";
        }
        if (!_.has(req.body, 'device') || _.isUndefined(req.body.device) || _.isNull(req.body.device) || _.isEmpty(req.body.device)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "device no existe.";
        }
        // if (_.has(req.body,'disp_dias')) {
        //   tmp.disp_dias = req.body.disp_dias;
        // }
        // console.log(req.body);
        // console.log(tmp);
        /* ERRORES:
                1020 : Correo ya existe,
                1040 : Codigo_identificador ya existe en el pais
        */
        // Validar el inspector
        if (tmp.resp.error == 0) {
            tmp.disponibilidad = (req.body.disponibilidad == "") ? 0 : req.body.disponibilidad;
            tmp.disp_viajar_ciudad = (req.body.disp_viajar_ciudad == "") ? 0 : req.body.disp_viajar_ciudad;
            tmp.disp_viajar_pais = (req.body.disp_viajar_pais == "") ? 0 : req.body.disp_viajar_pais;
            tmp.disp_horas = (req.body.disp_horas == "") ? "00:00 23:00" : req.body.disp_horas;
            tmp.d1 = (!_.has(req.body, 'd1') || _.isUndefined(req.body.d1) || _.isNull( req.body.d1) || _.isEmpty(req.body.d1) && req.body.d1 == "") ? 0 : req.body.d1;
            tmp.d2 = (!_.has(req.body, 'd2') || _.isUndefined(req.body.d2) || _.isNull( req.body.d2) || _.isEmpty(req.body.d2) && req.body.d2 == "") ? 0 : req.body.d2;
            tmp.d3 = (!_.has(req.body, 'd3') || _.isUndefined(req.body.d3) || _.isNull( req.body.d3) || _.isEmpty(req.body.d3) && req.body.d3 == "") ? 0 : req.body.d3;
            tmp.d4 = (!_.has(req.body, 'd4') || _.isUndefined(req.body.d4) || _.isNull( req.body.d4) || _.isEmpty(req.body.d4) && req.body.d4 == "") ? 0 : req.body.d4;
            tmp.d5 = (!_.has(req.body, 'd5') || _.isUndefined(req.body.d5) || _.isNull( req.body.d5) || _.isEmpty(req.body.d5) && req.body.d5 == "") ? 0 : req.body.d5;
            tmp.d6 = (!_.has(req.body, 'd6') || _.isUndefined(req.body.d6) || _.isNull( req.body.d6) || _.isEmpty(req.body.d6) && req.body.d6 == "") ? 0 : req.body.d6;
            tmp.d7 = (!_.has(req.body, 'd7') || _.isUndefined(req.body.d7) || _.isNull( req.body.d7) || _.isEmpty(req.body.d7) && req.body.d7 == "") ? 0 : req.body.d7;
            if (tmp.disponbilidad == "1" || tmp.disponibilidad == 1) { tmp.disp_horas = "00:00 23:00"; tmp.d1 = 1; tmp.d2 = 1; tmp.d3 = 1; tmp.d4 = 1; tmp.d5 = 1; tmp.d6 = 1; tmp.d7 = 1; }
            tmp.inspector = wait.forMethod(res.app.settings._conn, 'query', `SELECT 1 FROM inspectores WHERE correo = ?`,[tmp.correo]);
            if (tmp.inspector.length <= 0) {
                tmp.inspector = wait.forMethod(res.app.settings._conn, 'query', `SELECT 1 FROM inspectores WHERE pais = ? AND codigo_identificador = ?`,[tmp.pais,tmp.codigo_identificador]);
                if (tmp.inspector.length <= 0) {
                    //console.log(tmp);
                    if (_.has(req.body, 'google') && !_.isUndefined(req.body.google) && !_.isNull(req.body.google) && !_.isEmpty(req.body.google)) {
                        //console.log(req.body.google);
                        tmp.google = JSON.parse(tmp.google);
                        _.find(tmp.google, function(google) { if (google.types[0] == 'country') { tmp.pais_google = google.long_name; return true; } });
                        _.find(tmp.google, function(google) { if (google.types[0] == 'street_number') { tmp.numero_direccion_google = google.long_name; return true; } });
                        _.find(tmp.google, function(google) { if (google.types[0] == 'route') { tmp.direccion_google = google.short_name + " " + tmp.numero_direccion_google; tmp.direccion = tmp.direccion_google; return true; } });
                        _.find(tmp.google, function(google) { if (google.types[0] == 'administrative_area_level_1') { tmp.nivel_1_google = google.long_name; return true; } });
                        _.find(tmp.google, function(google) { if (google.types[0] == 'administrative_area_level_2') { tmp.nivel_2_google = google.long_name; tmp.nivel_2 = tmp.nivel_2_google; return true; } });
                        _.find(tmp.google, function(google) { if (google.types[0] == 'administrative_area_level_3') { tmp.nivel_3_google = google.long_name; tmp.nivel_3 = tmp.nivel_3_google; return true; } });
                        _.find(tmp.google, function(google) { if (google.types[0] == 'administrative_area_level_4') { tmp.nivel_4_google = google.long_name; tmp.nivel_4 = tmp.nivel_4_google; return true; } });
                        _.find(tmp.google, function(google) { if (google.types[0] == 'administrative_area_level_5') { tmp.nivel_5_google = google.long_name; tmp.nivel_5 = tmp.nivel_5_google; return true; } });
                    }
                    tmp.insert =
                        `INSERT INTO inspectores
                        (codigo_identificador,uidd,telefono,correo,nombre,apellido_paterno,
                        apellido_materno,pais,nivel_1,nivel_2,nivel_3,nivel_4,nivel_5,
                        direccion,lat_dir,lon_dir,fecha_nacimiento,disponibilidad,
                        disponibilidad_viajar_ciudad,disponibilidad_viajar_pais,
                        disponibilidad_horas,id_experiencia_oficio,experiencia_detalle,
                        estado,correccion_direccion,device,d1,d2,d3,d4,d5,d6,d7,
                        registrado_segured,fecha_ingreso,pais_google,direccion_google,
                        nivel_1_google,nivel_2_google,nivel_3_google,nivel_4_google,
                        nivel_5_google)VALUES(
                        ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,CAST(? AS DATETIME),?,?,?,?,?,?,0,
                        ?,?,?,?,?,?,?,?,?,0,Now(),?,?,?,?,?,?,?)`;
                    //console.log(tmp.insert);
                    let insertado = wait.forMethod(res.app.settings._conn, 'query',
                        tmp.insert, [tmp.codigo_identificador,
                            tmp.uidd,
                            tmp.telefono,
                            tmp.correo,
                            tmp.nombre,
                            tmp.apellido_paterno,
                            tmp.apellido_materno,
                            tmp.pais,
                            tmp.nivel_1,
                            tmp.nivel_2,
                            tmp.nivel_3,
                            tmp.nivel_4,
                            tmp.nivel_5,
                            tmp.direccion,
                            tmp.latitude,
                            tmp.longitude,
                            tmp.fecha_nacimiento,
                            tmp.disponibilidad,
                            tmp.disp_viajar_ciudad,
                            tmp.disp_viajar_pais,
                            tmp.disp_horas,
                            tmp.oficio,
                            tmp.experiencia,
                            tmp.correccion_direccion,
                            tmp.device,
                            tmp.d1,
                            tmp.d2,
                            tmp.d3,
                            tmp.d4,
                            tmp.d5,
                            tmp.d6,
                            tmp.d7,
                            tmp.pais_google,
                            tmp.direccion_google,
                            tmp.nivel_1_google,
                            tmp.nivel_2_google,
                            tmp.nivel_3_google,
                            tmp.nivel_4_google,
                            tmp.nivel_5_google
                        ]);
                    //console.log(insertado);
                    if (insertado.affectedRows <= 0) {
                        tmp.resp.error = 500;
                        tmp.resp.mensaje = 'El inspector no se logro registrar';
                        res.send(tmp.resp);
                        self.log("u-adjust", "registrarInspector", tmp.resp, res);
                    } else {
                        //Enviamos informacion a Segured
                        var experiencia_oficio = wait.forMethod(res.app.settings._conn, 'query', `SELECT * FROM experiencia_oficio WHERE id = ?`, [ tmp.oficio ]);
                        tmp.oficio = experiencia_oficio[0].nombre;
                        if (_.has(req.body, 'google') && !_.isUndefined(req.body.google) && !_.isNull(req.body.google) && !_.isEmpty(req.body.google)) {
                            tmp.pais = tmp.pais_google;
                            tmp.nivel_1 = tmp.nivel_1_google;
                            tmp.nivel_2 = tmp.nivel_2_google;
                            tmp.nivel_3 = tmp.nivel_3_google;
                            tmp.nivel_4 = tmp.nivel_4_google;
                            tmp.nivel_5 = tmp.nivel_5_google;
                        } else {
                            var pais = wait.forMethod(res.app.settings._conn, 'query',`select i.* from paises AS p INNER JOIN iso AS i ON i.id = p.idpais WHERE p.id = ?`, [tmp.pais]);
                            var iso = wait.forMethod(res.app.settings._conn, 'query',`select i2.* from isosubdivision AS i2 WHERE i2.id = ?`, [tmp.nivel_1]);
                            tmp.pais = pais[0].nombrecomun;
                            tmp.nivel_1 = iso[0].subdivision_name;
                        }

                        var json = {
                                "id_inspector": insertado.insertId,
                                "codigo_identificador": tmp.codigo_identificador,
                                "uidd": tmp.uidd,
                                "device_token": "",
                                "device": "",
                                "telefono": tmp.telefono,
                                "correo": tmp.correo,
                                "password": "",
                                "nombre": tmp.nombre,
                                "apellido_paterno": tmp.apellido_paterno,
                                "apellido_materno": tmp.apellido_materno,
                                "pais": tmp.pais,
                                "nivel_1": tmp.nivel_1,
                                "nivel_2": tmp.nivel_2,
                                "nivel_3": tmp.nivel_3,
                                "nivel_4": tmp.nivel_4,
                                "nivel_5": tmp.nivel_5,
                                "direccion": tmp.direccion,
                                "correccion_direccion": tmp.correccion_direccion,
                                "lat_dir": tmp.latitude,
                                "lon_dir": tmp.longitude,
                                // "lat_inspector": tmp.lat_inspector,
                                // "lon_inspector": tmp.lon_inspector,
                                "fecha_nacimiento": tmp.fecha_nacimiento,
                                "disponibilidad": tmp.disponibilidad,
                                "d1": tmp.d1,
                                "d2": tmp.d2,
                                "d3": tmp.d3,
                                "d4": tmp.d4,
                                "d5": tmp.d5,
                                "d6": tmp.d6,
                                "d7": tmp.d7,
                                "disponibilidad_horas": tmp.disp_horas,
                                "disponibilidad_viajar_ciudad": tmp.disp_viajar_ciudad,
                                "disponibilidad_viajar_pais": tmp.disp_viajar_pais,
                                "experiencia_oficio": tmp.oficio, //TODO: Este puede cambiar a futuro
                                "experiencia_detalle": tmp.experiencia,
                                "estado": 0, //TODO: INFORMAR A SEGURED
                                "fecha_envio": moment(new Date()).format('YYYY-MM-DDTHH:MM:SS')
                            }
                          self.guardarColaSalida(res, 'registrarInspector', '/registrarInspector/', json, (rer,rre) => {
                            if (rer) {
                              wait.forMethod(res.app.settings._conn, 'query', `DELETE FROM inspectores WHERE id = ?`,[insertado.insertId]);
                              tmp.resp.error = 500;
                              tmp.resp.mensaje = "Error al guardar la cola de salida.";
                              res.send(tmp.resp);
                              self.log("u-adjust", "registrarInspector", tmp.resp, res);
                            } else {
                              res.send(tmp.resp);
                              self.log("u-adjust", "registrarInspector", tmp.resp, res);
                            }
                          });
                    }
                } else {
                    tmp.resp.error = 1040;
                    tmp.resp.mensaje = 'Codigo identificador ya existe en el pais';
                    res.send(tmp.resp);
                    self.log("u-adjust", "registrarInspector", tmp.resp, res);
                }
            } else {
                tmp.resp.error = 1020;
                tmp.resp.mensaje = 'Correo ya existe.';
                res.send(tmp.resp);
                self.log("u-adjust", "registrarInspector", tmp.resp, res);
            }
        } else {
            res.send(tmp.resp);
            self.log("u-adjust", "registrarInspector", tmp.resp, res);
        }

    },
    validarCorreo: (req, res) => {
      var tmp = {
          resp: {
              error: 0,
              mensaje: ""
          },
          correo: req.body.correo,
          inspector: []
      }
      tmp.inspector = wait.forMethod(res.app.settings._conn, 'query', `SELECT 1 FROM inspectores WHERE correo = ?`,[tmp.correo]);
      if (tmp.inspector.length > 0) {
        tmp.resp.error = 1020;
        tmp.resp.mensaje = "";
      }
      res.send(tmp.resp);
      self.log("u-adjust", "validarCorreo", tmp.resp, res);
    },
    validarRut: (req, res) => {
      var tmp = {
          resp: {
              error: 0,
              mensaje: ""
          },
          codigo_identificador: req.body.codigo_identificador,
          pais: req.body.pais,
          inspector: []
      }
      tmp.inspector = wait.forMethod(res.app.settings._conn, 'query', `SELECT 1 FROM inspectores WHERE pais = ? AND codigo_identificador = ?`,[tmp.pais,tmp.codigo_identificador]);
      if (tmp.inspector.length > 0) {
          tmp.resp.error = 1040;
          tmp.resp.mensaje = "";
      }
      res.send(tmp.resp);
      self.log("u-adjust", "validarRut", tmp.resp, res);
    },
    dispInspector: (req, res) => {
        var tmp = {
            resp: {
                error: 0,
                mensaje: "",
                disponibilidad: 0
            },
            id_inspector: req.body.id_inspector,
            codigo_identificador: req.body.codigo_identificador,
            id_tarea: req.body.id_tarea,
            num_caso: req.body.num_caso
        };
        if (!_.has(req.body, 'id_inspector') || _.isUndefined(req.body.id_inspector) || _.isNull(req.body.id_inspector) || _.isEmpty(req.body.id_inspector)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "id_inspector no existe.";
        }
        if (!_.has(req.body, 'codigo_identificador') || _.isUndefined(req.body.codigo_identificador) || _.isNull(req.body.codigo_identificador) || _.isEmpty(req.body.codigo_identificador)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "codigo_identificador no existe.";
        }
        if (!_.has(req.body, 'id_tarea') || _.isUndefined(req.body.id_tarea) || _.isNull(req.body.id_tarea)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "id_tarea no existe.";
        }
        if (!_.has(req.body, 'num_caso') || _.isUndefined(req.body.num_caso) || _.isNull(req.body.num_caso)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "num_caso no existe.";
        }
        if (tmp.resp.error == 0) {
          var json = {
                  "id_inspector": tmp.id_inspector,
                  "codigo_identificador": tmp.codigo_identificador,
                  "id_tarea": tmp.id_tarea,
                  "num_caso": tmp.num_caso,
                  "fecha_disponibilidad​": moment(new Date()).format('YYYY-MM-DDTHH:MM:SS')
              }
            // console.log(json);
            tmp.disponibilidad = wait.forMethod(self, 'enviarSegured', res.app.settings.segured_link + '/dispInspector/', json);
            // console.log(tmp.disponibilidad);
            if (_.has(tmp.disponibilidad,'GLOSA_ERROR') && _.isString(tmp.disponibilidad.GLOSA_ERROR) && tmp.disponibilidad.GLOSA_ERROR !== "") {
              if (tmp.disponibilidad.GLOSA_ERROR != "INSPECTOR DISPONIBLE") {
                  tmp.resp.disponibilidad = 0;
                  res.send(tmp.resp);
                  self.log("u-adjust", "dispInspector", tmp.resp, res);
              } else {
                  tmp.resp.disponibilidad = 1;
                  res.send(tmp.resp);
                  self.log("u-adjust", "dispInspector", tmp.resp, res);
              }
            } else if (_.isNumber(tmp.disponibilidad.error) && tmp.disponibilidad.error !== 0) {
              tmp.resp = {
                  error: tmp.disponibilidad.error,
                  mensaje: tmp.disponibilidad.mensaje
              }
              res.send(tmp.resp);
              self.log("u-adjust", "dispInspector", tmp.resp, res);
            } else {
              res.send(tmp.resp);
              self.log("u-adjust", "dispInspector", tmp.resp, res);
            }
        } else {
            res.send(tmp.resp);
            self.log("u-adjust", "dispInspector", tmp.resp, res);
        }
    },
    aceptarTarea: (req, res) => {
      //console.log(req.body);
        var tmp = {
            resp: {
                error: 0,
                mensaje: ""
            },
            id_inspector: req.body.id_inspector,
            codigo_identificador: req.body.codigo_identificador,
            id_tarea: req.body.id_tarea,
            num_caso: req.body.num_caso,
            categoria: req.body.categoria,
            tipo_tarea: req.body.tipo_tarea,
            tareas: [],
            inspector: []
        };
        if (!_.has(req.body, 'id_inspector') || _.isUndefined(req.body.id_inspector) || _.isNull(req.body.id_inspector) || _.isEmpty(req.body.id_inspector)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "id_inspector no existe.";
        }
        if (!_.has(req.body, 'codigo_identificador') || _.isUndefined(req.body.codigo_identificador) || _.isNull(req.body.codigo_identificador) || _.isEmpty(req.body.codigo_identificador)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "codigo_identificador no existe.";
        }
        if (!_.has(req.body, 'id_tarea') || _.isUndefined(req.body.id_tarea) || _.isNull(req.body.id_tarea)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "id_tarea no existe.";
        }
        if (!_.has(req.body, 'num_caso') || _.isUndefined(req.body.num_caso) || _.isNull(req.body.num_caso)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "num_caso no existe.";
        }
        if (!_.has(req.body, 'categoria') || _.isUndefined(req.body.categoria) || _.isNull(req.body.categoria)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "categoria no existe.";
        }
        if (!_.has(req.body, 'tipo_tarea') || _.isUndefined(req.body.tipo_tarea) || _.isNull(req.body.tipo_tarea)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "tipo_tarea no existe.";
        }
        if (tmp.resp.error == 0) {
          tmp.inspector = wait.forMethod(res.app.settings._conn, 'query', `SELECT * FROM inspectores WHERE id = ?`,[tmp.id_inspector]);
          if (tmp.inspector.length >= 1) {
            if (tmp.categoria == '0') {
              if (tmp.tipo_tarea == '0') {//Normal
                tmp.tareas = wait.forMethod(res.app.settings._conn, 'query',
                    `UPDATE tareas
                      SET id_inspector = ?,
                      estado_tarea = 2
                      WHERE id = ?
                      AND id_inspector = 0
                      AND categoria = 0
                      AND tipo_tarea = 0
                      AND estado_tarea = 1`, [ tmp.id_inspector, tmp.id_tarea ]);
              } else {//Critica
                tmp.tareas = wait.forMethod(res.app.settings._conn, 'query',
                    `UPDATE tareas
                      SET id_inspector = ?,
                      estado_tarea = 2,
                      fecha_tarea = date_format(DATE_ADD(now(), INTERVAL ? HOUR),'%Y-%m-%d')
                      WHERE id = ?
                      AND id_inspector = 0
                      AND categoria = 0
                      AND tipo_tarea = 1
                      AND estado_tarea = 1`, [ tmp.id_inspector, tmp.inspector[0].dif_horaria, tmp.id_tarea ]);
              }
            } else {//Emergencia
                tmp.tareas = wait.forMethod(res.app.settings._conn, 'query',
                    `UPDATE tareas
                      SET id_inspector = ?,
                      estado_tarea = 2,
                      fecha_tarea = date_format(DATE_ADD(now(), INTERVAL ? HOUR),'%Y-%m-%d')
                      WHERE id = ?
                      AND categoria = 1
                      AND id_inspector = 0
                      AND estado_tarea = 1`, [ tmp.id_inspector, tmp.inspector[0].dif_horaria, tmp.id_tarea ]);
            }
            if (tmp.tareas.affectedRows > 0) {
                var json = {
                    "id_inspector": tmp.id_inspector,
                    "codigo_identificador": tmp.codigo_identificador,
                    "num_caso": tmp.num_caso,
                    "fecha_aceptar_tarea​": moment(new Date()).format('YYYY-MM-DDTHH:MM:SS')
                }
                self.guardarColaSalida(res, 'aceptarTarea', '/aceptarTarea/', json, (cer,cre) => {
                  if (cer) {
                    tmp.resp.error = 500;
                    tmp.resp.mensaje = "Error al guardar la cola de salida";
                    res.send(tmp.resp);
                    self.log("u-adjust", "aceptarTarea", tmp.resp, res);
                  } else {
                    res.send(tmp.resp);
                    self.log("u-adjust", "aceptarTarea", tmp.resp, res);
                  }
                });
            } else {
                tmp.resp.error = 500;
                tmp.resp.mensaje = "Error al actualizar la tarea";
                res.send(tmp.resp);
                self.log("u-adjust", "aceptarTarea", tmp.resp, res);
            }
          } else {
            tmp.resp.error = 500;
            tmp.resp.mensaje = "El inspector no existe.";
            res.send(tmp.resp);
            self.log("u-adjust", "aceptarTarea", tmp.resp, res);
          }

        } else {
            res.send(tmp.resp);
            self.log("u-adjust", "aceptarTarea", tmp.resp, res);
        }
    },
    cancelarTarea: (req, res) => {
        var tmp = {
            resp: {
                error: 0,
                mensaje: "",
                id_app: 0
            },
            id_inspector: req.body.id_inspector,
            codigo_identificador: req.body.codigo_identificador,
            id_tarea: req.body.id_tarea,
            num_caso: req.body.num_caso,
            mensaje: req.body.mensaje,
            opcion: req.body.opcion,
            tipo: 0,
            update_inspector: {},
            cancela_tarea: {}
        }
        if (_.has(req.body, 'id_app') || !_.isUndefined(req.body.id_app) || !_.isNull(req.body.id_app)) {
            tmp.resp.id_app = req.body.id_app;
        }
        if (!_.has(req.body, 'id_inspector') || _.isUndefined(req.body.id_inspector) || _.isNull(req.body.id_inspector) || _.isEmpty(req.body.id_inspector) || req.body.id_inspector == 0) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "id_inspector no existe.";
        }
        if (!_.has(req.body, 'codigo_identificador') || _.isUndefined(req.body.codigo_identificador) || _.isNull(req.body.codigo_identificador) || _.isEmpty(req.body.codigo_identificador)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "codigo_identificador no existe.";
        }
        if (!_.has(req.body, 'id_tarea') || _.isUndefined(req.body.id_tarea) || _.isNull(req.body.id_tarea)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "id_tarea no existe.";
        }
        if (!_.has(req.body, 'num_caso') || _.isUndefined(req.body.num_caso) || _.isNull(req.body.num_caso)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "num_caso no existe.";
        }
        if (_.has(req.body, 'tipo') && !_.isUndefined(req.body.tipo) && !_.isNull(req.body.tipo) && !_.isEmpty(req.body.tipo)) {
            tmp.tipo = 1;
        }
        if (tmp.resp.error == 0) {
            var json = {
                "id_inspector": tmp.id_inspector,
                "codigo_identificador": tmp.codigo_identificador,
                "num_caso": tmp.num_caso,
                "mensaje": tmp.mensaje,
                "opcion": 0, //2017-08-18 A pedido de victor, pero queda para la version 2
                "tipo": tmp.tipo,
                "fecha_cancelacion​": moment(new Date()).format('YYYY-MM-DDTHH:MM:SS')
            }
            self.guardarColaSalida(res, 'cancelarTarea', '/registrarCancelacion/', json, (cer,cre) => {
              if (cer) {
                tmp.resp.error = 500;
                tmp.resp.mensaje = "Error al guardar la cola de salida";
                res.send(tmp.resp);
                self.log("u-adjust", "cancelarTarea", tmp.resp, res);
              } else {
                if (tmp.tipo) {
                    wait.forMethod(res.app.settings._conn, 'query',
                        `UPDATE
                          tareas
                          SET estado_tarea = 6,
                          comentario_can_o_rech = ?,
                          fecha_termino = now(),
                          opcion = 0
                          WHERE id = ? AND id_inspector = ?`, [ tmp.mensaje, tmp.id_tarea, tmp.id_inspector ]);
                } else {
                    wait.forMethod(res.app.settings._conn, 'query',
                        `UPDATE
                          tareas
                          SET estado_tarea = 7,
                          comentario_can_o_rech = ?,
                          fecha_termino = now(),
                          opcion = 0
                          WHERE id = ? AND id_inspector = ?`, [ tmp.mensaje, tmp.id_tarea, tmp.id_inspector ]);
                }
                res.send(tmp.resp);
                self.log("u-adjust", "cancelarTarea", tmp.resp, res);
              }
            });
            //tmp.colaSalida = wait.forMethod(self,'guardarColaSalida', res, 'cancelarTarea', 'http://qa-serviciosproflow.crawfordaffinity.com/ServiceApp.svc/registrarCancelacion/', json);

        } else {
            res.send(tmp.resp);
            self.log("u-adjust", "cancelarTarea", tmp.resp, res);
        }
    },
    confirmarRuta: (req, res) => {
        var tmp = {
            resp: {
                error: 0,
                mensaje: "",
                id_app: 0
            },
            id_inspector: req.body.id_inspector,
            codigo_identificador: req.body.codigo_identificador,
            id_app: req.body.id_app,
            fecha: req.body.fecha,
            tareas: []
        };
        if (!_.has(req.body, 'id_inspector') || _.isUndefined(req.body.id_inspector) || _.isNull(req.body.id_inspector)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "id_inspector no existe.";
        }
        if (!_.has(req.body, 'codigo_identificador') || _.isUndefined(req.body.codigo_identificador) || _.isNull(req.body.codigo_identificador)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "codigo_identificador no existe.";
        }
        if (!_.has(req.body, 'fecha') || _.isUndefined(req.body.fecha) || _.isNull(req.body.fecha) || _.isEmpty(req.body.fecha)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "fecha no existe.";
        }
        if (_.has(req.body, 'id_app') || !_.isUndefined(req.body.id_app) || !_.isNull(req.body.id_app)) {
            tmp.resp.id_app = req.body.id_app;
        }
        if (tmp.resp.error == 0) {
          tmp.inspectores = wait.forMethod(res.app.settings._conn, 'query',
            `SELECT id,dif_horaria
              FROM inspectores
              WHERE id = ?`,[tmp.id_inspector]);
          if (tmp.inspectores.length > 0) {
            var contador = 0;
            tmp.tareas = wait.forMethod(res.app.settings._conn, 'query',
              `SELECT num_caso
              FROM tareas
              WHERE id_inspector = ?
              AND date_format(fecha_tarea,'%Y-%m-%d') = date_format(DATE_ADD(now(), INTERVAL ? HOUR),'%Y-%m-%d')
              AND (estado_tarea = 2 OR estado_tarea = 3)`, [tmp.id_inspector,tmp.inspectores[0].dif_horaria]);
            if (tmp.tareas.length > 0) {
                _.each(tmp.tareas, (valor) => {
                    contador++;
                    var json = {
                        "id_inspector": tmp.id_inspector,
                        "codigo_identificador": tmp.codigo_identificador,
                        "num_caso": valor.num_caso,
                        "fecha_confirmacion": tmp.fecha
                        // "fecha_confirmacion": moment(new Date()).format('YYYY-MM-DDTHH:MM:SS')
                    }
                    self.guardarColaSalida(res, 'confirmarRuta', '/confirmacionTarea/', json, (cer,cre) => {
                      if (cer) {
                        tmp.resp.error = 500;
                        tmp.resp.mensaje = "Error al guardar la cola de salida";
                        res.send(tmp.resp);
                        self.log("u-adjust", "confirmarRuta", tmp.resp, res);
                      } else {
                        wait.forMethod(res.app.settings._conn, 'query',
                            `UPDATE push
                              SET fecha_confirmado = now(), estado = 1, fecha_telefono = ?
                              WHERE id_inspector = ?
                              AND num_caso = ?
                              AND (fecha_confirmado = null OR fecha_confirmado = "")
                              AND estado = 0`, [ moment(tmp.fecha).format('YYYY-MM-DDTHH:MM:SS'), tmp.id_inspector, valor ]);
                      }
                    });
                    // tmp.confirmar = wait.forMethod(self, 'enviarSegured', "http://qa-serviciosproflow.crawfordaffinity.com/ServiceApp.svc/confirmacionTarea/", json);
                    // if (_.isNumber(tmp.confirmar.error) && tmp.confirmar.error !== 0) {
                    //     tmp.resp.error = tmp.confirmar.error;
                    //     tmp.resp.mensaje = tmp.confirmar.mensaje;
                    //     res.send(tmp.resp);
                    //     self.log("u-adjust", "confirmarRuta", tmp.resp, res);
                    // } else if (_.isString(tmp.confirmar.GLOSA_ERROR) && tmp.confirmar.GLOSA_ERROR !== "") {
                    //     if (tmp.confirmar.GLOSA_ERROR != "TAREA CONFIRMADA") {
                    //         tmp.resp.error = 500;
                    //         tmp.resp.mensaje = tmp.confirmar.GLOSA_ERROR;
                    //         res.send(tmp.resp);
                    //         self.log("u-adjust", "confirmarRuta", tmp.resp, res);
                    //     } else {
                    //         let historial_push = wait.forMethod(res.app.settings._conn, 'query',
                    //             `UPDATE push
                    //               SET fecha_confirmado = now(), estado = 1, fecha_telefono = ?
                    //               WHERE id_inspector = ?
                    //               AND num_caso = ?
                    //               AND (fecha_confirmado = null OR fecha_confirmado = "")
                    //               AND estado = 0`, [ moment(tmp.fecha).format('YYYY-MM-DDTHH:MM:SS'), tmp.id_inspector, valor ]);
                    //             // console.log(historial_push);
                    //     }
                    // }
                    if (contador == tmp.tareas.length) {
                        res.send(tmp.resp);
                        self.log("u-adjust", "confirmarRuta", tmp.resp, res);
                    }
                });
            } else {
                res.send(tmp.resp);
                self.log("u-adjust", "confirmarRuta", tmp.resp, res);
            }
          } else {
            tmp.resp.error = 500;
            tmp.resp.mensaje = 'Inspector no existe.';
            res.send(tmp.resp);
            self.log("u-adjust", "confirmarRuta", tmp.resp, res);
          }
        } else {
            res.send(tmp.resp);
            self.log("u-adjust", "confirmarRuta", tmp.resp, res);
        }
    },
    iniciarTarea: (req, res) => {
        // Falta la funcion para dar aviso a Segured
        var tmp = {
            resp: {
                error: 0,
                mensaje: "",
                id_app: 0
            },
            id_inspector: req.body.id_inspector,
            codigo_identificador: req.body.codigo_identificador,
            id_tarea: req.body.id_tarea,
            num_caso: req.body.num_caso,
            tareas: [],
            iniciar_tarea: {}
        };
        if (!_.has(req.body, 'id_inspector') || _.isUndefined(req.body.id_inspector) || _.isNull(req.body.id_inspector) || _.isEmpty(req.body.id_inspector)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "id_inspector no existe.";
        }
        if (!_.has(req.body, 'codigo_identificador') || _.isUndefined(req.body.codigo_identificador) || _.isNull(req.body.codigo_identificador) || _.isEmpty(req.body.codigo_identificador)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "codigo_identificador no existe.";
        }
        if (!_.has(req.body, 'id_tarea') || _.isUndefined(req.body.id_tarea) || _.isNull(req.body.id_tarea)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "id_tarea no existe.";
        }
        if (!_.has(req.body, 'num_caso') || _.isUndefined(req.body.num_caso) || _.isNull(req.body.num_caso)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "num_caso no existe.";
        }
        if (_.has(req.body, 'id_app') || !_.isUndefined(req.body.id_app) || !_.isNull(req.body.id_app)) {
            tmp.resp.id_app = req.body.id_app;
        }
        //9-05-17 : No necesito paylo en todos lados
        if (tmp.resp.error == 0) {
            tmp.tareas = wait.forMethod(res.app.settings._conn, 'query',
                `UPDATE tareas
                  SET estado_tarea = 5
                  WHERE id = ?
                  AND id_inspector = ?
                  AND estado_tarea BETWEEN 2 AND 4`, [ tmp.id_tarea, tmp.id_inspector ]);
            if (tmp.tareas.affectedRows == 0) {
                tmp.resp.error = 500;
                tmp.resp.mensaje = "No se logro actualizar la tarea..";
                res.send(tmp.resp);
                self.log("u-adjust", "iniciarTarea", tmp.resp, res);
            } else {
                var json = {
                    "id_inspector": tmp.id_inspector,
                    "codigo_identificador": tmp.codigo_identificador,
                    "id_tarea": tmp.id_tarea,
                    "numero_caso": tmp.num_caso,
                    "mensaje": tmp.mensaje,
                    "opcion": 0,
                    "fecha_inicio_tarea": moment(new Date()).format('YYYY-MM-DDTHH:MM:SS')
                }
                self.guardarColaSalida(res, 'iniciarTarea', '/registrarInicioTarea/', json, (cer,cre) => {
                  if (cer) {
                    tmp.resp.error = 500;
                    tmp.resp.mensaje = "Error al guardar la cola de salida";
                    res.send(tmp.resp);
                    self.log("u-adjust", "iniciarTarea", tmp.resp, res);
                  } else {
                    res.send(tmp.resp);
                    self.log("u-adjust", "iniciarTarea", tmp.resp, res);
                  }
                });
            }
        } else {
            res.send(tmp.resp);
            self.log("u-adjust", "iniciarTarea", tmp.resp, res);
        }

    },
    editarPerfilTipo1: (req, res) => { //Domicilio
        var tmp = {
            resp: {
                error: 0,
                mensaje: ""
            },
            id_inspector: req.body.id_inspector,
            pais: req.body.pais,
            nivel_1: req.body.nivel_1,
            nivel_2: req.body.nivel_2,
            nivel_3: req.body.nivel_3,
            nivel_4: req.body.nivel_4,
            nivel_5: req.body.nivel_5,
            direccion: req.body.direccion,
            lat_dir: req.body.lat_dir,
            lon_dir: req.body.lon_dir,
            correccion_direccion: req.body.correccion_direccion,
            google: req.body.google,
            codigo_identificador: "",
            pais_google: "",
            direccion_google: "",
            nivel_1_google: "",
            nivel_2_google: "",
            nivel_3_google: "",
            nivel_4_google: "",
            nivel_5_google: ""
        }
        if (!_.has(req.body, 'id_inspector') || _.isUndefined(req.body.id_inspector) || _.isNull(req.body.id_inspector)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "id_inspector no existe";
        }
        if (!_.has(req.body, 'pais') || _.isUndefined(req.body.pais) || _.isNull(req.body.pais)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "pais no existe";
        }
        if (!_.has(req.body, 'nivel_1') || _.isUndefined(req.body.nivel_1) || _.isNull(req.body.nivel_1)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "nivel_1 no existe";
        }
        if (!_.has(req.body, 'direccion') || _.isUndefined(req.body.direccion) || _.isNull(req.body.direccion)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "direccion no existe";
        }
        if (!_.has(req.body, 'lat_dir') || _.isUndefined(req.body.lat_dir) || _.isNull(req.body.lat_dir)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "lat_dir no existe";
        }
        if (!_.has(req.body, 'lon_dir') || _.isUndefined(req.body.lon_dir) || _.isNull(req.body.lon_dir)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "lon_dir no existe";
        }
        if (!_.has(req.body, 'correccion_direccion') || _.isUndefined(req.body.correccion_direccion) || _.isNull(req.body.correccion_direccion)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "correccion_direccion no existe";
        }
        if (tmp.resp.error == 0) {
            if (_.has(req.body, 'google') && !_.isUndefined(req.body.google) && !_.isNull(req.body.google) && !_.isEmpty(req.body.google)) {
                //console.log(req.body.google);
                tmp.google = JSON.parse(tmp.google);
                _.find(tmp.google, function(google) { if (google.types[0] == 'country') { tmp.pais_google = google.long_name; return true; }
                });
                _.find(tmp.google, function(google) {
                    if (google.types[0] == 'street_number') { tmp.numero_direccion_google = google.long_name; return true; }
                });
                _.find(tmp.google, function(google) {
                    if (google.types[0] == 'route') { tmp.direccion_google = google.short_name + " " + tmp.numero_direccion_google; tmp.direccion = tmp.direccion_google; return true; }
                });
                _.find(tmp.google, function(google) {
                    if (google.types[0] == 'administrative_area_level_1') {tmp.nivel_1_google = google.long_name; return true; }
                });
                _.find(tmp.google, function(google) {
                    if (google.types[0] == 'administrative_area_level_2') {tmp.nivel_2_google = google.long_name; tmp.nivel_2 = tmp.nivel_2_google; return true; }
                });
                _.find(tmp.google, function(google) {
                    if (google.types[0] == 'administrative_area_level_3') { tmp.nivel_3_google = google.long_name; tmp.nivel_3 = tmp.nivel_3_google; return true; }
                });
                _.find(tmp.google, function(google) {
                    if (google.types[0] == 'administrative_area_level_4') { tmp.nivel_4_google = google.long_name; tmp.nivel_4 = tmp.nivel_4_google; return true; }
                });
                _.find(tmp.google, function(google) {
                    if (google.types[0] == 'administrative_area_level_5') { tmp.nivel_5_google = google.long_name; tmp.nivel_5 = tmp.nivel_5_google; return true; }
                });
            }
            let domicilio = wait.forMethod(res.app.settings._conn, 'query',
                `UPDATE inspectores SET
                    pais = ?,
                    nivel_1 = ?,
                    nivel_2 = ?,
                    nivel_3 = ?,
                    nivel_4 = ?,
                    nivel_5 = ?,
                    direccion = ?,
                    lat_dir = ?,
                    lon_dir = ?,
                    correccion_direccion = ?,
                    pais_google = ?,
                    direccion = ?,
                    direccion_google = ?,
                    nivel_1_google = ?,
                    nivel_2_google = ?,
                    nivel_3_google = ?,
                    nivel_4_google = ?,
                    nivel_5_google = ?
                    WHERE id = ?`, [
                    tmp.pais,
                    tmp.nivel_1,
                    tmp.nivel_2,
                    tmp.nivel_3,
                    tmp.nivel_4,
                    tmp.nivel_5,
                    tmp.direccion,
                    tmp.lat_dir,
                    tmp.lon_dir,
                    tmp.correccion_direccion,
                    tmp.pais_google,
                    tmp.direccion,
                    tmp.direccion_google,
                    tmp.nivel_1_google,
                    tmp.nivel_2_google,
                    tmp.nivel_3_google,
                    tmp.nivel_4_google,
                    tmp.nivel_5_google,
                    tmp.id_inspector
                ]);
            if (domicilio.affectedRows <= 0) {
                tmp.resp.error = 500;
                tmp.resp.mensaje = "Error al ingresar los cambios a domicilio del id_inspector : " + tmp.id_inspector;
                res.send(tmp.resp);
                self.log("u-adjust", "editarPerfilTipo1", tmp.resp, res);
            } else {
                tmp.codigo_identificador = wait.forMethod(res.app.settings._conn,'query',`SELECT codigo_identificador FROM inspectores WHERE id = ?`, [tmp.id_inspector]);
                var json = {
                    "id_inspector": tmp.id_inspector,
                    "codigo_identificador": tmp.codigo_identificador[0].codigo_identificador,
                    "tipo_cambio": 1,
                    "datos": {
                        "pais": tmp.pais,
                        "nivel_1": tmp.nivel_1,
                        "nivel_2": tmp.nivel_2,
                        "nivel_3": tmp.nivel_3,
                        "nivel_4": tmp.nivel_4,
                        "nivel_5": tmp.nivel_5,
                        "direccion": tmp.direccion,
                        "lat_dir": tmp.lat_dir,
                        "lon_dir": tmp.lon_dir,
                        "fecha_modificacion​": moment(new Date()).format('YYYY-MM-DDTHH:MM:SS')
                    }
                }
                self.guardarColaSalida(res, 'editarPerfilTipo1', '/editarInspector/', json, (cer,cre) => {
                  if (cer) {
                    tmp.resp.error = 500;
                    tmp.resp.mensaje = "Error al guardar la cola de salida";
                    res.send(tmp.resp);
                    self.log("u-adjust", "editarPerfilTipo1", tmp.resp, res);
                  } else {
                    res.send(tmp.resp);
                    self.log("u-adjust", "editarPerfilTipo1", tmp.resp, res);
                  }
                });
                // tmp.editarInspector = wait.forMethod(self, 'enviarSegured',"http://qa-serviciosproflow.crawfordaffinity.com/ServiceApp.svc/editarInspector/",json);
                // if (_.isNumber(tmp.editarInspector.error) && tmp.editarInspector.error !== 0) {
                //     tmp.resp = {
                //         error: tmp.editarInspector.error,
                //         mensaje: tmp.editarInspector.mensaje
                //     }
                //     res.send(tmp.resp);
                //     self.log("u-adjust", "editarPerfilTipo1", tmp.resp, res);
                // } else if (_.isString(tmp.editarInspector.GLOSA_ERROR) && tmp.editarInspector
                //     .GLOSA_ERROR !== "") {
                //     if (tmp.editarInspector.GLOSA_ERROR != "DOMICILIO ACTUALIZADO") {
                //         tmp.resp.error = 500;
                //         tmp.resp.mensaje = "No se logro registrar el cambio de domicilio en segured.";
                //         res.send(tmp.resp);
                //         self.log("u-adjust", "editarPerfilTipo1", tmp.resp, res);
                //     } else {
                //         res.send(tmp.resp);
                //         self.log("u-adjust", "registrarInspector", tmp.resp, res);
                //     }
                // } else {
                //     res.send(tmp.resp);
                //     self.log("u-adjust", "editarPerfilTipo1", tmp.resp, res);
                // }
            }
        } else {
            res.send(tmp.resp);
            self.log("u-adjust", "editarPerfilTipo1", tmp.resp, res);
        }
    },
    editarPerfilTipo2: (req, res) => { //Disponibilidad
        var tmp = {
            resp: {
                error: 0,
                mensaje: ""
            },
            id_inspector: req.body.id_inspector,
            disponibilidad: req.body.disponibilidad,
            d1: 0,
            d2: 0,
            d3: 0,
            d4: 0,
            d5: 0,
            d6: 0,
            d7: 0,
            disponibilidad_viajar_pais: req.body.disponibilidad_viajar_pais,
            disponibilidad_viajar_ciudad: req.body.disponibilidad_viajar_ciudad,
            disponibilidad_horas: req.body.disponibilidad_horas,
        }
        if (!_.has(req.body, 'id_inspector') || _.isUndefined(req.body.id_inspector) || _.isNull(req.body.id_inspector) || _.isEmpty(req.body.id_inspector)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "id_inspector no existe.";
        }
        if (tmp.resp.error == 0) {
            //console.log(tmp);
            tmp.disponibilidad = (req.body.disponibilidad == "" ||  _.isUndefined(req.body.disponibilidad) || _.isNull(req.body.disponibilidad) || _.isEmpty(req.body.disponibilidad)) ? 0 : req.body.disponibilidad;
            tmp.disp_viajar_ciudad = (req.body.disp_viajar_ciudad == "" ||  _.isUndefined(req.body.disp_viajar_ciudad) || _.isNull(req.body.disp_viajar_ciudad) || _.isEmpty(req.body.disp_viajar_ciudad)) ? 0 : req.body.disp_viajar_ciudad;
            tmp.disp_viajar_pais = (req.body.disp_viajar_pais == "" || _.isUndefined(req.body.disp_viajar_pais) || _.isNull(req.body.disp_viajar_pais) || _.isEmpty(req.body.disp_viajar_pais)) ? 0 : req.body.disp_viajar_pais;
            tmp.disp_horas = (req.body.disponibilidad_horas == "" || _.isUndefined(req.body.disponibilidad_horas) || _.isNull(req.body.disponibilidad_horas) || _.isEmpty(req.body.disponibilidad_horas)) ? "00:00 23:00" : req.body.disponibilidad_horas;
            tmp.d1 = (!_.has(req.body, 'd1') ||  _.isUndefined(req.body.d1) || _.isNull(req.body.d1) || _.isEmpty(req.body.d1) && req.body.d1 == "") ? 0 :req.body.d1;
            tmp.d2 = (!_.has(req.body, 'd2') || _.isUndefined(req.body.d2) || _.isNull(req.body.d2) || _.isEmpty(req.body.d2) && req.body.d2 == "") ? 0 :req.body.d2;
            tmp.d3 = (!_.has(req.body, 'd3') || _.isUndefined(req.body.d3) || _.isNull(req.body.d3) || _.isEmpty(req.body.d3) && req.body.d3 == "") ? 0 :req.body.d3;
            tmp.d4 = (!_.has(req.body, 'd4') || _.isUndefined(req.body.d4) || _.isNull(req.body.d4) || _.isEmpty(req.body.d4) && req.body.d4 == "") ? 0 :req.body.d4;
            tmp.d5 = (!_.has(req.body, 'd5') || _.isUndefined(req.body.d5) || _.isNull(req.body.d5) || _.isEmpty(req.body.d5) && req.body.d5 == "") ? 0 :req.body.d5;
            tmp.d6 = (!_.has(req.body, 'd6') || _.isUndefined(req.body.d6) || _.isNull(req.body.d6) || _.isEmpty(req.body.d6) && req.body.d6 == "") ? 0 :req.body.d6;
            tmp.d7 = (!_.has(req.body, 'd7') || _.isUndefined(req.body.d7) || _.isNull(req.body.d7) || _.isEmpty(req.body.d7) && req.body.d7 == "") ? 0 :req.body.d7;
            if (tmp.disponbilidad == "1" || tmp.disponibilidad == 1) {tmp.d1 = 1; tmp.d2 = 1; tmp.d3 = 1; tmp.d4 = 1; tmp.d5 = 1; tmp.d6 = 1; tmp.d7 = 1;}
            let disponibilidad = wait.forMethod(res.app.settings._conn, 'query',
                `UPDATE inspectores SET
                    disponibilidad = ?,
                    d1 = ?,
                    d2 = ?,
                    d3 = ?,
                    d4 = ?,
                    d5 = ?,
                    d6 = ?,
                    d7 = ?,
                    disponibilidad_horas = ?,
                    disponibilidad_viajar_pais = ?,
                    disponibilidad_viajar_ciudad = ?
                    WHERE id = ?`, [
                    tmp.disponibilidad,
                    tmp.d1,
                    tmp.d2,
                    tmp.d3,
                    tmp.d4,
                    tmp.d5,
                    tmp.d6,
                    tmp.d7,
                    tmp.disponibilidad_horas,
                    tmp.disponibilidad_viajar_pais,
                    tmp.disponibilidad_viajar_ciudad,
                    tmp.id_inspector
                ]);
            if (disponibilidad.affectedRows <= 0) {
                tmp.resp.error = 500;
                tmp.resp.mensaje = "Error al actualizar los datos de disponibilidad del id_inspector : " + mp.id_inspector;
                res.send(tmp.resp);
                self.log("u-adjust", "editarPerfilTipo2", tmp.resp, res);
            } else {
                tmp.codigo_identificador = wait.forMethod(res.app.settings._conn, 'query',
                    `SELECT codigo_identificador FROM inspectores WHERE id = ?`, [tmp.id_inspector]);
                var json = {
                    "id_inspector": tmp.id_inspector,
                    "codigo_identificador": tmp.codigo_identificador[0].codigo_identificador,
                    "tipo_cambio": 2,
                    "datos": {
                        "disponibilidad": tmp.disponibilidad,
                        "disponibilidad_viajar_pais": tmp.disponibilidad_viajar_pais,
                        "disponibilidad_viajar_ciudad": tmp.disponibilidad_viajar_ciudad,
                        "disponibilidad_fechas": [tmp.d1, tmp.d2, tmp.d3, tmp.d4, tmp.d5, tmp.d6, tmp.d7].join(","),
                        "disponibilidad_horas": tmp.disponibilidad_horas,
                        "fecha_modificacion​": moment(new Date()).format('YYYY-MM-DDTHH:MM:SS')
                    }
                }
                self.guardarColaSalida(res, 'editarPerfilTipo2', '/editarInspector/', json, (cer,cre) => {
                  if (cer) {
                    tmp.resp.error = 500;
                    tmp.resp.mensaje = "Error al guardar la cola de salida";
                    res.send(tmp.resp);
                    self.log("u-adjust", "editarPerfilTipo2", tmp.resp, res);
                  } else {
                    res.send(tmp.resp);
                    self.log("u-adjust", "editarPerfilTipo2", tmp.resp, res);
                  }
                });
                // tmp.editarInspector = wait.forMethod(self, 'enviarSegured', "http://qa-serviciosproflow.crawfordaffinity.com/ServiceApp.svc/editarInspector/", json);
                // if (_.isNumber(tmp.editarInspector.error) && tmp.editarInspector.error !==0) {
                //     tmp.resp = {
                //         error: tmp.editarInspector.error,
                //         mensaje: tmp.editarInspector.mensaje
                //     }
                //     res.send(tmp.resp);
                //     self.log("u-adjust", "editarPerfilTipo2", tmp.resp, res);
                // } else if (_.isString(tmp.editarInspector.GLOSA_ERROR) && tmp.editarInspector.GLOSA_ERROR !== "") {
                //     if (tmp.editarInspector.GLOSA_ERROR !=
                //         "DISPONIBILIDAD ACTUALIZADA") {
                //         tmp.resp.error = 500;
                //         tmp.resp.mensaje = "No se logro registrar el cambio en la disponibilidad en segured.";
                //         res.send(tmp.resp);
                //         self.log("u-adjust", "editarPerfilTipo2", tmp.resp, res);
                //     } else {
                //         res.send(tmp.resp);
                //         self.log("u-adjust", "editarPerfilTipo2", tmp.resp, res);
                //     }
                // } else {
                //     res.send(tmp.resp);
                //     self.log("u-adjust", "editarPerfilTipo2", tmp.resp, res);
                // }
            }
        } else {
            res.send(tmp.resp);
            self.log("u-adjust", "editarPerfilTipo2", tmp.resp, res);
        }
    },
    editarPerfilTipo3: (req, res) => { //contactos
        var tmp = {
            resp: {
                error: 0,
                mensaje: ""
            },
            id_inspector: req.body.id_inspector,
            correo: req.body.correo,
            telefono: req.body.telefono
        }
        if (!_.has(req.body, 'id_inspector') || _.isUndefined(req.body.id_inspector) || _.isNull(req.body.id_inspector) || _.isEmpty(req.body.id_inspector)) {
            tmp.resp.error = 0;
            tmp.resp.mensaje = "id_inspector no existe.";
        }
        if (!_.has(req.body, 'correo') || _.isUndefined(req.body.correo) || _.isNull(req.body.correo) || _.isEmpty(req.body.correo)) {
            tmp.resp.error = 0;
            tmp.resp.mensaje = "correo no existe.";
        }
        if (!_.has(req.body, 'telefono') || _.isUndefined(req.body.telefono) || _.isNull(req.body.telefono) || _.isEmpty(req.body.telefono)) {
            tmp.resp.error = 0;
            tmp.resp.mensaje = "telefono no existe.";
        }
        if (tmp.resp.error == 0) {
            let conta = wait.forMethod(res.app.settings._conn, 'query',
                `UPDATE inspectores SET correo = ?, telefono = ? WHERE id = ?`, [tmp.correo, tmp.telefono, tmp.id_inspector]);
            if (conta.affectedRows <= 0) {
                tmp.resp.error = 500;
                tmp.resp.mensaje = "Error al actualizar los datos de contacto del id_inspector : " + tmp.id_inspector;
                res.send(tmp.resp);
            } else {
                tmp.codigo_identificador = wait.forMethod(res.app.settings._conn, 'query', `SELECT codigo_identificador FROM inspectores WHERE id = ?`, [tmp.id_inspector]);
                var json = {
                    "id_inspector": tmp.id_inspector,
                    "codigo_identificador": tmp.codigo_identificador[0].codigo_identificador,
                    "tipo_cambio": 3,
                    "datos": {
                        "correo": tmp.correo,
                        "telefono": tmp.telefono,
                        "fecha_modificacion​": moment(new Date()).format(
                            'YYYY-MM-DDTHH:MM:SS')
                    }
                }
                self.guardarColaSalida(res, 'editarPerfilTipo3', '/editarInspector/', json, (cer,cre) => {
                  if (cer) {
                    tmp.resp.error = 500;
                    tmp.resp.mensaje = "Error al guardar la cola de salida";
                    res.send(tmp.resp);
                    self.log("u-adjust", "editarPerfilTipo3", tmp.resp, res);
                  } else {
                    res.send(tmp.resp);
                    self.log("u-adjust", "editarPerfilTipo3", tmp.resp, res);
                  }
                });
                // tmp.editarInspector = wait.forMethod(self, 'enviarSegured',"http://qa-serviciosproflow.crawfordaffinity.com/ServiceApp.svc/editarInspector/",json);
                // if (_.isNumber(tmp.editarInspector.error) && tmp.editarInspector.error !== 0) {
                //     tmp.resp = { error: tmp.editarInspector.error, mensaje: tmp.editarInspector.mensaje};
                //     res.send(tmp.resp);
                //     self.log("u-adjust", "editarPerfilTipo3", tmp.resp, res);
                // } else if (_.isString(tmp.editarInspector.GLOSA_ERROR) && tmp.editarInspector.GLOSA_ERROR !== "") {
                //     if (tmp.editarInspector.GLOSA_ERROR != "CONTACTO ACTUALIZADO") {
                //         tmp.resp.error = 500;
                //         tmp.resp.mensaje = "No se logro registrar el cambio en los datos de contacto en segured.";
                //         res.send(tmp.resp);
                //         self.log("u-adjust", "editarPerfilTipo3", tmp.resp, res);
                //     } else {
                //         res.send(tmp.resp);
                //         self.log("u-adjust", "editarPerfilTipo3", tmp.resp, res);
                //     }
                // } else {
                //     res.send(tmp.resp);
                //     self.log("u-adjust", "editarPerfilTipo3", tmp.resp, res);
                // }
            }
        } else {
            res.send(tmp.resp);
            self.log("u-adjust", "editarPerfilTipo3", tmp.resp, res);
        }
    },
    finalizarTarea: (req, res) => {
        var tmp = {
            resp: {
                error: 0,
                mensaje: "",
                id_app: 0
            },
            inspecciones: req.body.inspecciones,
            fotosrequeridas: req.body.fotosrequeridas,
            datosbasicos: req.body.datosbasicos,
            niveles: req.body.niveles,
            caracteristicas: req.body.caracteristicas,
            siniestro: req.body.siniestro,
            recintos: req.body.recintos,
            itemdanos: req.body.itemdanos,
            contenidos: req.body.contenido,
            documentos: req.body.documentos,
            firma: req.body.firma
        };
        // wait.forMethod(res.app.settings._conn, 'query',`INSERT INTO reg_finalizar_tarea_andres (json)VALUES(?)`,[JSON.stringify(tmp.inspeccion)]);
        if (!_.has(req.body, 'inspecciones') || _.isUndefined(req.body.inspecciones) || _.isNull(req.body.inspecciones)) {
          tmp.resp.error = 400;
          tmp.resp.mensaje = "inspecciones no existe.";
        } else if (_.isArray(tmp.inspecciones) && tmp.inspecciones.length == 0) {
          tmp.resp.error = 400;
          tmp.resp.mensaje = "campo inspecciones no tiene datos y es obligatorio.";
        }
        if (!_.has(req.body, 'fotosrequeridas') || _.isUndefined(req.body.fotosrequeridas) || _.isNull(req.body.fotosrequeridas)) {
          tmp.resp.error = 400;
          tmp.resp.mensaje = "fotosrequeridas no existe.";
        }
        if (!_.has(req.body, 'datosbasicos') || _.isUndefined(req.body.datosbasicos) || _.isNull(req.body.datosbasicos)) {
          tmp.resp.error = 400;
          tmp.resp.mensaje = "datosbasicos no existe.";
        }
        if (!_.has(req.body, 'niveles') || _.isUndefined(req.body.niveles) || _.isNull(req.body.niveles)) {
          tmp.resp.error = 400;
          tmp.resp.mensaje = "niveles no existe.";
        }
        if (!_.has(req.body, 'caracteristicas') || _.isUndefined(req.body.caracteristicas) || _.isNull(req.body.caracteristicas)) {
          tmp.resp.error = 400;
          tmp.resp.mensaje = "caracteristicas no existe.";
        }
        if (!_.has(req.body, 'siniestro') || _.isUndefined(req.body.siniestro) || _.isNull(req.body.siniestro)) {
          tmp.resp.error = 400;
          tmp.resp.mensaje = "siniestro no existe.";
        }
        if (!_.has(req.body, 'recintos') || _.isUndefined(req.body.recintos) || _.isNull(req.body.recintos)) {
          tmp.resp.error = 400;
          tmp.resp.mensaje = "recintos no existe.";
        }
        if (!_.has(req.body, 'itemdanos') || _.isUndefined(req.body.itemdanos) || _.isNull(req.body.itemdanos)) {
          tmp.resp.error = 400;
          tmp.resp.mensaje = "itemdanos no existe.";
        }
        if (!_.has(req.body, 'contenido') || _.isUndefined(req.body.contenido) || _.isNull(req.body.contenido)) {
          tmp.resp.error = 400;
          tmp.resp.mensaje = "contenido no existe.";
        }
        if (!_.has(req.body, 'documentos') || _.isUndefined(req.body.documentos) || _.isNull(req.body.documentos)) {
          tmp.resp.error = 400;
          tmp.resp.mensaje = "documentos no existe.";
        }
        if (!_.has(req.body, 'firma') || _.isUndefined(req.body.firma) || _.isNull(req.body.firma)) {
          tmp.resp.error = 400;
          tmp.resp.mensaje = "firma no existe.";
        }
        if (_.has(req.body, 'id_app') || !_.isUndefined(req.body.id_app) || !_.isNull(req.body.id_app)) {
            tmp.resp.id_app = req.body.id_app;
        }
        if (tmp.resp.error == 0) {
          // TODO: no voy a armar el json aca, voy a crear una funcion que lo haga al enviar la inspeccion.
          // Comentario desde APP: internamente el id_server para las tablas temporales de inspeccion se llama id_inspeccion (osea, es id_tarea)
          // Obtengo los datos de la tarea antes que nada
          var fallo = false;
          var id_insp = [];
          console.log('PSB DEBUG: INSPECCION RECIBIDO DUMP',tmp);
          try {
            if (_.isArray(tmp.inspecciones) && tmp.inspecciones.length >= 1) {
              _.each(tmp.inspecciones, (i) => {
                try {
                    wait.forMethod(res.app.settings._conn, 'query',
                    `INSERT INTO inspecciones (id_tarea,fecha_inicio,fecha_finalizacion,fecha)values(?,CAST(? AS DATETIME),CAST(? AS DATETIME),now())`, [
                        i.id_server,
                        (!_.isNull(i.fecha_inspeccion_inicio)) ? moment(i.fecha_inspeccion_inicio).format("YYYY-MM-DD HH:mm:ss") : i.fecha_inspeccion_inicio,
                        (!_.isNull(i.fecha_inspeccion_fin)) ? moment(i.fecha_inspeccion_fin).format("YYYY-MM-DD HH:mm:ss") : i.fecha_inspeccion_fin
                    ]);
                    id_insp.push(i.id_server);
                } catch(eeee) {
                    console.log('Error al insertar la tarea en tabla inspecciones',{ catch:eeee, i:i });
                    // por ahora omitiremos marcar error, fallo=true.
                }
              });
            }
            if (_.isArray(tmp.fotosrequeridas) && tmp.fotosrequeridas.length >= 1) {
              _.each(tmp.fotosrequeridas, (fr) => {
                let foto1,foto2,foto3,foto4;
                foto1 = (_.isNull(fr.fachada)) ? {insertId:0} : wait.forMethod(res.app.settings._conn,'query',
                  `INSERT INTO imagenes
                  (id_tarea,archivo,fecha) VALUES (?,?,now())`,
                  [fr.id_inspeccion,fr.fachada]
                );
                foto2 = (_.isNull(fr.barrio)) ? {insertId:0} : wait.forMethod(res.app.settings._conn,'query',
                  `INSERT INTO imagenes
                  (id_tarea,archivo,fecha) VALUES (?,?,now())`,
                  [fr.id_inspeccion,fr.barrio]
                );
                foto3 = (_.isNull(fr.numero)) ? {insertId:0} : wait.forMethod(res.app.settings._conn,'query',
                  `INSERT INTO imagenes
                  (id_tarea,archivo,fecha) VALUES (?,?,now())`,
                  [fr.id_inspeccion,fr.numero]
                );
                foto4 = (_.isNull(fr.depto)) ? {insertId:0} : wait.forMethod(res.app.settings._conn,'query',
                  `INSERT INTO imagenes
                  (id_tarea,archivo,fecha) VALUES (?,?,now())`,
                  [fr.id_inspeccion,fr.depto]
                );
                wait.forMethod(res.app.settings._conn, 'query',
                  `INSERT INTO inspeccion_fotosrequeridas (
                    id_tarea,fachada,barrio,numero,n_depto
                  )values(
                    ?,?,?,?,?
                  )`, [
                      fr.id_inspeccion,
                      foto1.insertId,
                      foto2.insertId,
                      foto3.insertId,
                      foto4.insertId
                  ]);
              });
            }
            if (_.isArray(tmp.datosbasicos) && tmp.datosbasicos.length >= 1) {
              _.each(tmp.datosbasicos, (dt) => {
                // wait.forMethod(res.app.settings._conn, 'query',
                //     `UPDATE inspecciones SET fecha_finalizacion = ? WHERE id_tarea = ?`, [+tmp.datosbasicos.fecha_full_inspeccion,+dt.id_inspeccion]);
                wait.forMethod(res.app.settings._conn, 'query',
                    `INSERT INTO inspeccion_datosbasicos (
                      id_tarea,hora_inspeccion,
                      fono_movil,presente_nombre,
                      fono_fijo,rut_asegurado,
                      fecha_inspeccion,direccion_riesgo,
                      direccion_correcta,nivel_2,
                      nivel_3,nivel_4,
                      nivel_5,fecha_full_inspeccion,
                      asegurador,nro_caso,
                      asegurado,fecha_siniestro,
                      presente_rut,email
                    )values(
                      ?,?,
                      ?,?,
                      ?,?,
                      CAST(? AS DATETIME),?,
                      ?,?,
                      ?,?,
                      ?,?,
                      ?,?,
                      ?,CAST(? AS DATETIME),
                      ?,?)`, [
                      dt.id_inspeccion, dt.hora_inspeccion,
                      dt.fono_movil, dt.presente_nombre,
                      dt.fono_fijo, dt.rut_asegurado,
                      moment(dt.fecha_inspeccion,"DD-MM-YYYY").format("YYYY-MM-DD"), dt.direccion_riesgo,
                      dt.direccion_correcta, (_.has(dt,'ciudad') && !_.isNull(dt.ciudad) && dt.ciudad != "") ? dt.ciudad : "",
                      (_.has(dt,'comuna') && !_.isNull(dt.comuna) && dt.comuna != "") ? dt.comuna : "","",
                      "", (!_.isNull(dt.fecha_full_inspeccion)) ? moment(dt.fecha_full_inspeccion).format("YYYY-MM-DD HH:mm:ss") : dt.fecha_full_inspeccion,
                      dt.asegurador, dt.nro_caso,
                      dt.asegurado, moment(dt.fecha_siniestro).format("YYYY-MM-DD"),
                      dt.presente_rut,dt.email
                    ]);
              });
            }
            if (_.isArray(tmp.caracteristicas) && tmp.caracteristicas.length >= 1) {
              _.each(tmp.caracteristicas, (cr) => {
                wait.forMethod(res.app.settings._conn, 'query',
                    `INSERT INTO inspeccion_caracteristicas (
                      id_tarea,destinos,construccion_ano,
                      construccion_anexa,otros_seguros_enlugar,id_compania,
                      asociado_hipotecario,id_entidad_financiera,inhabitable,
                      estimacion_meses
                    )values(
                      ?,?,?,
                      ?,?,?,
                      ?,?,?,
                      ?
                    )`, [
                        cr.id_inspeccion,cr.destinos,(_.isNull(cr.construccion_ano)) ? 0 : cr.construccion_ano,
                        (_.isNull(cr.construccion_anexa)) ? 0 : cr.construccion_anexa,(_.isNull(cr.otros_seguros_enlugar)) ? 0 : cr.otros_seguros_enlugar,(_.isNull(cr.id_compania)) ? 0 : cr.id_compania,
                        cr.asociado_hipotecario,(_.isNull(cr.id_entidad_financiera)) ? 0 : cr.id_entidad_financiera,(_.isNull(cr.inhabitable)) ? 0 : cr.inhabitable,
                        (_.isNull(cr.estimacion_meses)) ? 0 : cr.estimacion_meses
                    ]);
              });
            }
            if (_.isArray(tmp.niveles) && tmp.niveles.length >= 1) {
              _.each(tmp.niveles, (v) => {
                wait.forMethod(res.app.settings._conn, 'query',
                    `INSERT INTO inspeccion_niveles (
                      id_tarea,id_nivel,nombre,piso,ano,largo,
                      ancho,alto,superficie,ids_estructuras_soportantes,ids_muros,
                      ids_entrepisos,ids_pavimentos,ids_estructura_cubierta,ids_cubierta
                    )values(
                      ?,?,?,?,?,?,
                      ?,?,?,?,?,
                      ?,?,?,?)`, [
                      v.id_inspeccion,v.id,v.nombre,v.piso,v.ano,v.largo,
                      v.ancho,v.alto,v.superficie,v.ids_estructuras_soportantes,v.ids_muros,
                      v.ids_entrepisos,v.ids_pavimentos,v.ids_estructura_cubiera,v.ids_cubierta
                ]);
              });
            }
            if (_.isArray(tmp.siniestro) && tmp.siniestro.length >= 1) {
              _.each(tmp.siniestro, (s) => {
                wait.forMethod(res.app.settings._conn, 'query',
                  `INSERT INTO inspeccion_siniestro (
                    id_tarea,id_tipo_siniestro,descripcion,analisis_especialista,
                    porcentaje_danos_estructura,porcentaje_danos_terminaciones,porcentaje_danos_instalaciones
                  ) VALUES (
                    ?,?,?,?,
                    ?,?,?
                  )
                  `,[
                    s.id_inspeccion,s.id_tipo_siniestro,s.descripcion,s.analisis_especialista,
                    (_.isNull(s.porcentaje_danos_estructura)) ? 0 : s.porcentaje_danos_estructura,(_.isNull(s.porcentaje_danos_terminaciones)) ? 0 : s.porcentaje_danos_terminaciones,(_.isNull(s.porcentaje_danos_instalaciones)) ? 0 : s.porcentaje_danos_instalaciones
                  ]
                );
              });
            }
            if (_.isArray(tmp.recintos) && tmp.recintos.length >= 1) {
              _.each(tmp.recintos, (v) => {
                let foto1,foto2,foto3;
                foto1 = (_.isNull(v.foto1)) ? {insertId:0} : wait.forMethod(res.app.settings._conn,'query',
                  `INSERT INTO imagenes
                  (id_tarea,archivo,fecha) VALUES (?,?,now())`,
                  [v.id_inspeccion,v.foto1]
                );
                foto2 = (_.isNull(v.foto2)) ? {insertId:0} : wait.forMethod(res.app.settings._conn,'query',
                  `INSERT INTO imagenes
                  (id_tarea,archivo,fecha) VALUES (?,?,now())`,
                  [v.id_inspeccion,v.foto2]
                );
                foto3 = (_.isNull(v.foto3)) ? {insertId:0} : wait.forMethod(res.app.settings._conn,'query',
                  `INSERT INTO imagenes
                  (id_tarea,archivo,fecha) VALUES (?,?,now())`,
                  [v.id_inspeccion,v.foto3]
                );
                wait.forMethod(res.app.settings._conn,'query',
                  `INSERT INTO inspeccion_recintos
                  (
                    id_tarea,id_recinto,nombre,foto1,foto2,
                    foto3,id_nivel,largo,ancho,alto,
                    superficie,ids_estructuras_soportantes,ids_muros,ids_entrepisos,ids_pavimentos,ids_estructura_cubierta,ids_cubierta
                  )VALUES(
                    ?,?,?,?,?,
                    ?,?,?,?,?,
                    ?,?,?,?,?,?,?
                  )`,[
                    v.id_inspeccion,v.id_recinto,v.nombre,foto1.insertId,foto2.insertId,
                    foto3.insertId,v.id_nivel,v.largo,v.ancho,v.alto,
                    v.superficie,v.ids_estructuras_soportantes,v.ids_muros,v.ids_entrepisos,v.ids_pavimentos,
                    v.ids_estructura_cubiera,v.ids_cubierta
                  ]
                );
              });
            }
            if (_.isArray(tmp.itemdanos) && tmp.itemdanos.length >= 1) {
              _.each(tmp.itemdanos, (v) => {
                let foto1,foto2,foto3;
                foto1 = (_.isNull(v.foto1)) ? {insertId:0} : wait.forMethod(res.app.settings._conn,'query',
                  `INSERT INTO imagenes
                  (id_tarea,archivo,fecha) VALUES (?,?,now())`,
                  [v.id_inspeccion,v.foto1]
                );
                foto2 = (_.isNull(v.foto2)) ? {insertId:0} : wait.forMethod(res.app.settings._conn,'query',
                  `INSERT INTO imagenes
                  (id_tarea,archivo,fecha) VALUES (?,?,now())`,
                  [v.id_inspeccion,v.foto2]
                );
                foto3 = (_.isNull(v.foto3)) ? {insertId:0} : wait.forMethod(res.app.settings._conn,'query',
                  `INSERT INTO imagenes
                  (id_tarea,archivo,fecha) VALUES (?,?,now())`,
                  [v.id_inspeccion,v.foto3]
                );
                wait.forMethod(res.app.settings._conn,'query',
                  `INSERT INTO inspeccion_itemdanos
                  (
                    id_tarea,id_recinto,id_partida,id_tipodano,foto1,
                    foto2,foto3,id_unidadmedida,superficie,descripcion_dano
                  ) VALUES (
                    ?,?,?,?,?,
                    ?,?,?,?,?
                  )`,[
                    v.id_inspeccion,v.id_recinto,v.id_partida,v.id_tipodano,foto1.insertId,foto2.insertId,
                    foto3.insertId,v.id_unidadmedida,v.superficie,v.descripcion_dano
                  ]
                );
              });
            }
            if (_.isArray(tmp.contenidos) && tmp.contenidos.length >= 1) {
              _.each(tmp.contenidos, (v) => {
                let foto1,foto2,foto3;
                foto1 = (_.isNull(v.foto1)) ? {insertId:0} : wait.forMethod(res.app.settings._conn,'query',
                  `INSERT INTO imagenes
                  (id_tarea,archivo,fecha) VALUES (?,?,now())`,
                  [v.id_inspeccion,v.foto1]
                );
                foto2 = (_.isNull(v.foto2)) ? {insertId:0} : wait.forMethod(res.app.settings._conn,'query',
                  `INSERT INTO imagenes
                  (id_tarea,archivo,fecha) VALUES (?,?,now())`,
                  [v.id_inspeccion,v.foto2]
                );
                foto3 = (_.isNull(v.foto3)) ? {insertId:0} : wait.forMethod(res.app.settings._conn,'query',
                  `INSERT INTO imagenes
                  (id_tarea,archivo,fecha) VALUES (?,?,now())`,
                  [v.id_inspeccion,v.foto3]
                );
                wait.forMethod(res.app.settings._conn,'query',
                  `INSERT INTO inspeccion_contenidos
                  (
                    id_tarea,id_nombre,id_marca,foto1,foto2,
                    foto3,id_recinto,cantidad,fecha_compra,id_moneda,
                    valor,recupero,descripcion
                  ) VALUES (
                    ?,?,?,?,?,
                    ?,?,?,?,?,
                    ?,?,?
                  )`,[
                    v.id_inspeccion,v.id_nombre,v.id_marca,foto1.insertId,foto2.insertId,
                    foto3.insertId,v.id_recinto,v.cantidad,(!_.isNull(v.fecha_compra)) ? moment(v.fecha_compra).format("YYYY-MM-DD HH:mm:ss") : v.fecha_compra,v.id_moneda,
                    v.valor,v.recupero,v.descripcion
                  ]
                );
              });
            }
            // en v.fecha_compra arriba decia: moment.unix(v_fecha_compra) 9mar18 psb
            if (_.isArray(tmp.documentos) && tmp.documentos.length >= 1) {
              _.each(tmp.documentos, (doc) => {
                wait.forMethod(res.app.settings._conn, 'query',
                  `INSERT INTO inspeccion_documentos
                  (
                    id_tarea,doc1,doc2,doc3,doc4,
                    doc5,doc6,doc7,doc8,doc9,
                    doc10,edificio,contenidos,otros,dias
                  )
                  VALUES
                  (
                    ?,?,?,?,?,
                    ?,?,?,?,?,
                    ?,?,?,?,?
                  )`,[
                    doc.id_inspeccion,(_.isNull(doc.doc1)) ? 0 : doc.doc1, (_.isNull(doc.doc2)) ? 0 : doc.doc2, (_.isNull(doc.doc3)) ? 0 : doc.doc3, (_.isNull(doc.doc4)) ? 0 : doc.doc4,
                    (_.isNull(doc.doc5)) ? 0 : doc.doc5,(_.isNull(doc.doc6)) ? 0 : doc.doc6,(_.isNull(doc.doc7)) ? 0 : doc.doc7,(_.isNull(doc.doc8)) ? 0 : doc.doc8,(_.isNull(doc.doc9)) ? 0 : doc.doc9,
                    (_.isNull(doc.doc10)) ? 0 : doc.doc10,(_.isNull(doc.edificio)) ? 0 : doc.edificio,(_.isNull(doc.contenidos)) ? 0 : doc.contenidos,doc.otros,(_.isNull(doc.dias)) ? 0 : doc.dias
                  ]
                );
              });
            }
            if (_.isArray(tmp.firma) && tmp.firma.length >= 1) {
              _.each(tmp.firma, (f) => {
                let foto, id_insp_firma=f.id_inspeccion;
                try {
                    // psb normalizacion de id_tarea en firma
                    if (tmp.firma.length==1 && id_insp.length==1) {
                        // psb: si inspecciones recibidas es 1, y firma largo es 1, asignamos id_inspeccion como el recibido en array inspecciones
                        id_insp_firma = id_insp[0];
                        console.log('PSB DEBUG: FORZANDO FIX ID FIRMA', id_insp_firma);
                    }
                    foto = (_.isNull(f.firma64)) ? {insertId:0} : wait.forMethod(res.app.settings._conn,'query',
                      `INSERT INTO imagenes
                      (id_tarea,archivo,fecha) VALUES (?,?,now())`,
                      [id_insp_firma,f.firma64]
                    );
                    wait.forMethod(res.app.settings._conn, 'query',
                      `INSERT INTO inspeccion_firma (id_tarea,firma) VALUES (?,?)`,[id_insp_firma,foto.insertId]
                    );
                } catch(efirm) {
                    console.log('PSB DEBUG: ERROR INSERTANDO DEF DE FIRMA',efirm);
                }
              });
            }
            console.log('PSB DEBUG: FIRMAS PROCESADAS',tmp.firma);

          } catch (e) {
            console.log('Error al insertar la tarea',e);
            fallo = true;
          }
          if (fallo) {
            tmp.resp.error = 500;
            tmp.resp.mensaje = "Error al ingresar la tarea.";
            res.send(tmp.resp);
            self.log("u-adjust", "finalizarTarea", tmp.resp, res);
          } else {
            // @TODO: Actualizar la inspecion en tabla inspeccion a estado 1 para que se envie.
            // let actualiza_inspeccion = wait.forMethod(`UPDATE inspecciones SET estado = 1 WHERE id = ?`,[finaliza.insertId]);
            try {
              _.each(tmp.inspecciones, (v,l) => {
                console.log('INSPECCIONES REGISTRADAS => ',v.id_server);
                if (tmp.resp.error == 0) {
                  var inspecciones = wait.forMethod(res.app.settings._conn, 'query',
                  `SELECT * FROM inspecciones WHERE id_tarea = ?`, [+v.id_server]);
                  var actualiza_inspecciones = wait.forMethod(res.app.settings._conn, 'query',
                  `UPDATE inspecciones SET estado = 1 WHERE id_tarea = ?`, [+v.id_server]);
                  let actualiza_tarea = wait.forMethod(res.app.settings._conn,'query',
                  `UPDATE tareas SET estado_tarea = 8, id_inspeccion = ?, fecha_termino = now() WHERE id = ?`, [inspecciones[0].id, v.id_server]);
                  if (actualiza_inspecciones.affectedRows <= 0 && actualiza_tarea.affectedRows <= 0) {
                      tmp.resp.error = 500;
                      tmp.resp.mensaje = `Error al ingresar la tarea ${v.id_server}`;
                  }
                }
                if (l == tmp.inspecciones.length-1) {
                  res.send(tmp.resp);
                  self.log("u-adjust", "finalizarTarea", tmp.resp, res);
                }
              });
            } catch (e) {
                console.log('Error db',e);
            }

          }
        } else {
          res.send(tmp.resp);
          self.log("u-adjust", "finalizarTarea", tmp.resp, res);
        }
        // try {
        //     if (tmp.resp.error == 0) {
        //         tmp.inspeccion = JSON.parse(req.body.inspeccion);
        //         if (!_.has(tmp.inspeccion, 'imagen_firma') || _.isUndefined(tmp.inspeccion.imagen_firma) || _.isNull(tmp.inspeccion.imagen_firma) || _.isEmpty(tmp.inspeccion.imagen_firma)) {
        //             tmp.resp.error = 400;
        //             tmp.resp.mensaje = "imagen_firma no existe.";
        //         }
        //         if (!_.has(tmp.inspeccion, 'datosInspeccion') || _.isUndefined(tmp.inspeccion.datosInspeccion) || _.isNull(tmp.inspeccion.datosInspeccion) || _.isEmpty(tmp.inspeccion.datosInspeccion)) {
        //             tmp.resp.error = 400;
        //             tmp.resp.mensaje = "datosInspeccion no existe.";
        //         } else {
        //             if (!_.has(tmp.inspeccion.datosInspeccion, 'id_tarea') || _.isUndefined(tmp.inspeccion.datosInspeccion.id_tarea) || _.isNull(tmp.inspeccion.datosInspeccion.id_tarea)) {
        //                 tmp.resp.error = 400;
        //                 tmp.resp.mensaje = "id_tarea no existe.";
        //             }
        //             if (!_.has(tmp.inspeccion.datosInspeccion, 'num_caso') || _.isUndefined(tmp.inspeccion.datosInspeccion.num_caso) || _.isNull(tmp.inspeccion.datosInspeccion.num_caso)) {
        //                 tmp.resp.error = 400;
        //                 tmp.resp.mensaje = "num_caso no existe.";
        //             }
        //             if (!_.has(tmp.inspeccion.datosInspeccion, 'id_inspector') || _.isUndefined(tmp.inspeccion.datosInspeccion.id_inspector) || _.isNull(tmp.inspeccion.datosInspeccion.id_inspector)) {
        //                 tmp.resp.error = 400;
        //                 tmp.resp.mensaje = "id_inspector no existe.";
        //             }
        //             if (!_.has(tmp.inspeccion.datosInspeccion,'fecha_finalizacion_inspeccion') || _.isUndefined(tmp.inspeccion.datosInspeccion.fecha_finalizacion_inspeccion) || _.isNull(tmp.inspeccion.datosInspeccion.fecha_finalizacion_inspeccion) || _.isEmpty(tmp.inspeccion.datosInspeccion.fecha_finalizacion_inspeccion)) {
        //                 tmp.resp.error = 400;
        //                 tmp.resp.mensaje = "fecha_finalizacion_inspeccion no existe.";
        //             }
        //             if (!_.has(tmp.inspeccion.datosInspeccion, 'fecha_inspeccion') || _.isUndefined(tmp.inspeccion.datosInspeccion.fecha_inspeccion) || _.isNull(tmp.inspeccion.datosInspeccion.fecha_inspeccion) || _.isEmpty(tmp.inspeccion.datosInspeccion.fecha_inspeccion)) {
        //                 tmp.resp.error = 400;
        //                 tmp.resp.mensaje = "fecha_finalizacion_inspeccion no existe.";
        //             } else {
        //                 tmp.inspeccion.datosInspeccion.fecha_inspeccion = moment(new Date(tmp.inspeccion.datosInspeccion.fecha_inspeccion)).format('YYYY-MM-DD HH:MM:SS')
        //             }
        //         }
        //         if (tmp.resp.error == 0) {
        //             tmp.id_tarea = tmp.inspeccion.datosInspeccion.id_tarea;
        //             delete tmp.inspeccion.datosInspeccion.id_tarea;//Borra de la estructura el dato, para no enviarselo a segured
        //             let finaliza = wait.forMethod(res.app.settings._conn, 'query',
        //                 `INSERT INTO inspecciones (id_tarea,num_caso,id_inspector,json,fecha_finalizacion,fecha)values(?,?,?,?,CAST(? AS DATETIME),now())`, [
        //                     tmp.id_tarea,
        //                     tmp.inspeccion.datosInspeccion.num_caso,
        //                     tmp.inspeccion.datosInspeccion.id_inspector,
        //                     JSON.stringify(tmp.inspeccion),
        //                     tmp.inspeccion.datosInspeccion.fecha_finalizacion_inspeccion
        //                 ]);
        //             if (finaliza.affectedRows <= 0) {
        //                 tmp.resp.error = 500;
        //                 tmp.resp.mensaje = "Error al ingresar la tarea.";
        //                 res.send(tmp.resp);
        //                 self.log("u-adjust", "finalizarTarea", tmp.resp, res);
        //             } else {
        //                 let actualiza_inspeccion = wait.forMethod(res.app.settings._conn,'query',`UPDATE tareas SET estado_tarea = 8, id_inspeccion = ?, fecha_termino = now() WHERE id = ?`, [finaliza.insertId, tmp.id_tarea]);
        //                 if (actualiza_inspeccion.affectedRows <= 0) {
        //                     tmp.resp.error = 500;
        //                     tmp.resp.mensaje = "Error al ingresar la tarea.";
        //                     res.send(tmp.resp);
        //                     self.log("u-adjust", "finalizarTarea", tmp.resp, res);
        //                 } else {
        //                     res.send(tmp.resp);
        //                     self.log("u-adjust", "finalizarTarea", tmp.resp, res);
        //                 }
        //             }
        //         } else {
        //             res.send(tmp.resp);
        //             self.log("u-adjust", "finalizarTarea", tmp.resp, res);
        //         }
        //     } else {
        //         res.send(tmp.resp);
        //         self.log("u-adjust", "finalizarTarea", tmp.resp, res);
        //     }
        // } catch (e) {
        //     console.log("Error => " + e);
        // }
    },
    subirImagenes: (req, res) => {
        var tmp = {
            resp: {
                error: 0,
                mensaje: "",
                app_id: ""
            },
            id_tarea: req.body.id_tarea,
            archivo: req.body.archivo,
            imagen: "",
            app_id: req.body.app_id
        };
        if (_.has(req.body,'app_id') && !_.isUndefined(req.body.app_id) && !_.isNull(req.body.app_id)) {
          tmp.resp.app_id = tmp.app_id;
        }
        if (!_.has(req.body, 'archivo') || _.isUndefined(req.body.archivo) || _.isNull(req.body.archivo) || _.isEmpty(req.body.archivo)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "archivo no existe.";
        }
        if (!_.has(req, 'file') || _.isUndefined(req.file) || _.isNull(req.file) || _.isEmpty(req.file)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "imagen no existe.";
        } else {
          // tmp.imagen = req.file.path;
          tmp.imagen = self.base64_encode(req.file.path)
        }
        if (!_.has(req.body, 'id_tarea') || _.isUndefined(req.body.id_tarea) || _.isNull(req.body.id_tarea)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "id_tarea no existe.";
        }
        if (tmp.resp.error == 0) {
          var contador = wait.forMethod(res.app.settings._conn, 'query', 'SELECT count(id) AS contador FROM imagenes WHERE id_tarea = ? AND obtenida = 0',[tmp.id_tarea]);//Necesito saber cuantas imagenes me faltan
          try {
              let imagen_db = wait.forMethod(res.app.settings._conn, 'query',
              `UPDATE imagenes SET imagen = ?, obtenida = 1 WHERE id_tarea = ? AND archivo = ? AND obtenida = 0`,
              [tmp.imagen, tmp.id_tarea, tmp.archivo]);
              if (imagen_db.affectedRows <= 0) {
                  //Si no se logro
                  tmp.resp.error = 500;
                  tmp.resp.mensaje = "Error al insertar la imagen en la base de datos.";
                  res.send(tmp.resp);
                  self.log("u-adjust", "subirImagenes", tmp.resp, res);
              } else {
                  fs.unlink(req.file.path, () => {});
                  res.send(tmp.resp);
                  self.log("u-adjust", "subirImagenes", tmp.resp, res);
              }
          } catch (e) {
              console.log("Error => ",e);
              tmp.resp.error = 500;
              tmp.resp.mensaje = `Error al insertar la imagen en la base de datos. ${e}`;
              res.send(tmp.resp);
              self.log("u-adjust", "subirImagenes", tmp.resp, res);
          }
          if (contador[0].contador <= 1) { // Es la ultima imagen
            if (tmp.resp.error == 0) {//Si se inserto con exito la imagen, actualizamos las imagenes listas
              try {
                  let actualiza_fin = wait.forMethod(res.app.settings._conn, 'query',`UPDATE inspecciones SET imagenes_listas = 1 WHERE id_tarea = ?`, [tmp.id_tarea]);
                  // console.log("------"+JSON.stringify(actualiza_fin));
                  if (actualiza_fin.affectedRows <= 0) {
                      tmp.resp.error = 500;
                      tmp.resp.mensaje = "Error al actualizar la inspeccion imagenes listas.";
                      //res.send(tmp.resp); //9-mar-18 comentado pq no se puede usar res.send luego de otro res.send previo
                      self.log("u-adjust", "subirImagenes", tmp.resp, res);
                  } else {
                      //res.send(tmp.resp); //9-mar-18 comentado pq no se puede usar res.send luego de otro res.send previo
                      self.log("u-adjust", "subirImagenes", tmp.resp, res);
                  }
              } catch (e) {
                  tmp.resp.error = 500;
                  tmp.resp.mensaje = "Error al actualizar la inspeccion imagenes listas.";
                  //res.send(tmp.resp); //9-mar-18 comentado pq no se puede usar res.send luego de otro res.send previo
                  self.log("u-adjust", "subirImagenes", tmp.resp, res);
              }
            }
          }
          fs.unlink(req.file.path, () => {});
        } else {
            res.send(tmp.resp);
            self.log("u-adjust", "subirImagenes", tmp.resp, res);
        }
    },
    base64_encode: (file) => {
      // read binary data
      try {
        // convert binary data to base64 encoded string
        return fs.readFileSync(file, 'base64').toString('base64');
      } catch (e) {
        // convert binary data to base64 encoded string
        return "";
      }
    },
    armarJsonInspeccion: (res,inspeccion) => {
      let tarea,fotosrequeridas,datosbasicos,caracteristicas,
      niveles,siniestro,recintos,items_danos = [],danos_contenidos,contenidos,documentos,firma,json;
        tarea = wait.forMethod(res.app.settings._conn, 'query',`SELECT * FROM tareas WHERE id = ?`,[inspeccion.id_tarea]);
        firma = wait.forMethod(res.app.settings._conn, 'query',
        `SELECT i.imagen FROM imagenes AS i
        INNER JOIN inspeccion_firma AS i2 ON i2.firma = i.id
        WHERE i.id_tarea = ? AND i.obtenida = 1 limit 1`,[inspeccion.id_tarea]);
        // console.log('firma:>',firma,firma.length);
        console.log('inspeccion=>',inspeccion.id_tarea,'Firma=>',firma.length);
        if (firma.length >= 1) {//Si la firma existe (la envio la App)
        // if (firma.length <= 1) {
          fotosrequeridas = wait.forMethod(res.app.settings._conn, 'query',`SELECT * FROM inspeccion_fotosrequeridas WHERE id_tarea = ?`,[inspeccion.id_tarea]);
          datosbasicos = wait.forMethod(res.app.settings._conn, 'query',`SELECT * FROM inspeccion_datosbasicos WHERE id_tarea = ?`,[inspeccion.id_tarea]);
          caracteristicas = wait.forMethod(res.app.settings._conn, 'query',`SELECT * FROM inspeccion_caracteristicas WHERE id_tarea = ?`,[inspeccion.id_tarea]);
          niveles = wait.forMethod(res.app.settings._conn, 'query',`SELECT * FROM inspeccion_niveles WHERE id_tarea = ?`,[inspeccion.id_tarea]);
          siniestro = wait.forMethod(res.app.settings._conn, 'query',`SELECT * FROM inspeccion_siniestro WHERE id_tarea = ?`,[inspeccion.id_tarea]);
          //recintos = wait.forMethod(res.app.settings._conn, 'query',`SELECT * FROM inspeccion_recintos WHERE id_tarea = ?`,[inspeccion.id_tarea]);
          // _.each(recintos, (v) => {
          //     items_danos.push(wait.forMethod(res.app.settings._conn, 'query',`SELECT * FROM inspeccion_itemdanos WHERE id_recinto = ? AND id_tarea = ?`,[v.id_recinto,inspeccion.id_tarea]));
          // });
          // console.log('itemdanos',items_danos);
          contenidos = wait.forMethod(res.app.settings._conn, 'query',`SELECT * FROM inspeccion_contenidos WHERE id_tarea = ?`,[inspeccion.id_tarea]);
          documentos = wait.forMethod(res.app.settings._conn, 'query',`SELECT * FROM inspeccion_documentos WHERE id_tarea = ?`,[inspeccion.id_tarea]);

          // =========================================== JSON ===========================================
          // 9-mar-18 victor me pide agregar prefijo data:image/jpeg;base64 en firma
          json = {
             "imagen_firma": (_.isUndefined(firma[0])) ? "" : "data:image/jpeg;base64,"+firma[0].imagen.toString(),
             "datosInspeccion":{
                "num_caso": (_.isUndefined(tarea[0])) ? 0 : tarea[0].num_caso,
                "id_inspector": (_.isUndefined(tarea[0])) ? 0 : tarea[0].id_inspector,
                "imagen_fachada": (_.isUndefined(fotosrequeridas[0]) || _.isNull(fotosrequeridas[0].fachada)) ? "" : fotosrequeridas[0].fachada,
                "imagen_barrio": (_.isUndefined(fotosrequeridas[0]) || _.isNull(fotosrequeridas[0].barrio)) ? "" : fotosrequeridas[0].barrio,
                "imagen_numero": (_.isUndefined(fotosrequeridas[0]) || _.isNull(fotosrequeridas[0].numero)) ? "" : fotosrequeridas[0].numero,
                "imagen_n_depto": (_.isUndefined(fotosrequeridas[0]) || _.isNull(fotosrequeridas[0].n_depto)) ? "" : fotosrequeridas[0].n_depto,
                "fecha_inspeccion": (_.isNull(inspeccion.fecha_inicio)) ? null : moment(inspeccion.fecha_inicio).format("YYYY-MM-DD HH:MM:SS"),
                "fecha_finalizacion_inspeccion": (_.isNull(inspeccion.fecha_finalizacion)) ? null : moment(inspeccion.fecha_finalizacion).format("YYYY-MM-DD HH:MM:SS"),
                "datos_basicos":{
                  "fono_fijo": (_.isUndefined(datosbasicos[0]) || _.isNull(datosbasicos[0].fono_fijo)) ? "" : datosbasicos[0].fono_fijo,
                  "fono_movil": (_.isUndefined(datosbasicos[0]) || _.isNull(datosbasicos[0].fono_movil)) ? "" : datosbasicos[0].fono_movil,
                  "email": (_.isUndefined(datosbasicos[0]) || _.isNull(datosbasicos[0].email)) ? "" : datosbasicos[0].email,
                  "en_presencia": (_.isUndefined(datosbasicos[0]) || _.isNull(datosbasicos[0].presente_nombre)) ? "" : datosbasicos[0].presente_nombre,
                  "rut_presente": (_.isUndefined(datosbasicos[0]) || _.isNull(datosbasicos[0].presente_rut)) ? "" : datosbasicos[0].presente_rut,
                  "direccion_correcta": (_.isUndefined(datosbasicos[0]) || _.isNull(datosbasicos[0].direccion_correcta)) ? 0 : datosbasicos[0].direccion_correcta,
                  "direccion": (_.isUndefined(datosbasicos[0]) || _.isNull(datosbasicos[0].direccion_riesgo)) ? "" : datosbasicos[0].direccion_riesgo,
                  "nivel_2": (_.isUndefined(datosbasicos[0]) || _.isNull(datosbasicos[0].nivel_2)) ? "" : datosbasicos[0].nivel_2,
                  "nivel_3": (_.isUndefined(datosbasicos[0]) || _.isNull(datosbasicos[0].nivel_3)) ? "" : datosbasicos[0].nivel_3
                },
                "caracteristicas":{
                   "destino": (_.isUndefined(caracteristicas[0]) || _.isNull(caracteristicas[0].destinos) || _.isEmpty(caracteristicas[0].destinos)) ? "" : caracteristicas[0].destinos.split(",").map(parseFloat),
                   "ano_construccion": (_.isUndefined(caracteristicas[0]) || _.isNull(caracteristicas[0].construccion_ano)) ? 0 : caracteristicas[0].construccion_ano,
                   "construcciones_anexas": (_.isUndefined(caracteristicas[0])) ? 0 : caracteristicas[0].construccion_anexa,
                   "existen_otros_seguros": (_.isUndefined(caracteristicas[0])) ? 0 : caracteristicas[0].otros_seguros_enlugar,
                   "compania": (_.isUndefined(caracteristicas[0])) ? 0 : caracteristicas[0].id_compania,
                   "credito_hipotecario": (_.isUndefined(caracteristicas[0])) ? 0 : caracteristicas[0].asociado_hipotecario,
                   "entidad_financiera": (_.isUndefined(caracteristicas[0])) ? 0 : caracteristicas[0].id_entidad_financiera,
                   "inhabitable": (_.isUndefined(caracteristicas[0])) ? 0 : caracteristicas[0].inhabitable,
                   "meses": (_.isUndefined(caracteristicas[0])) ? 0 : caracteristicas[0].estimacion_meses,
                   "niveles":[]
                },
                "siniestro":{
                   "tipo_siniestro": (_.isUndefined(siniestro[0]) || _.isNull(siniestro[0].id_tipo_siniestro)) ? 0 : siniestro[0].id_tipo_siniestro,
                   "descripcion": (_.isUndefined(siniestro[0]) || _.isNull(siniestro[0].descripcion)) ? "" : siniestro[0].descripcion,
                   "estructura_soportante_porcentaje_dano": (_.isUndefined(siniestro[0]) || _.isNull(siniestro[0].porcentaje_danos_estructura)) ? 0 : siniestro[0].porcentaje_danos_estructura,
                   "recomienda_analisis": (_.isUndefined(siniestro[0]) || _.isNull(siniestro[0].analisis_especialista)) ? 0 : siniestro[0].analisis_especialista,
                   "terminacion_porcentaje_dano": (_.isUndefined(siniestro[0]) || _.isNull(siniestro[0].porcentaje_danos_terminaciones)) ? 0 : siniestro[0].porcentaje_danos_terminaciones,
                   "instalaciones_porcentaje_dano": (_.isUndefined(siniestro[0]) || _.isNull(siniestro[0].porcentaje_dano_instalaciones)) ? 0 : siniestro[0].porcentaje_dano_instalaciones
                },
                "documentos":{
                   "propuesta_reparaciones": (_.isUndefined(documentos[0]) || _.isNull(documentos[0].doc1)) ? 0 : documentos[0].doc1,
                   "escritura_compraventa": (_.isUndefined(documentos[0]) || _.isNull(documentos[0].doc2)) ? 0 : documentos[0].doc2,
                   "certificado_dominio_vigente_actualizado": (_.isUndefined(documentos[0]) || _.isNull(documentos[0].doc3)) ? 0 : documentos[0].doc3,
                   "certificado_hipotecas_gravamenes_actualizado": (_.isUndefined(documentos[0]) || _.isNull(documentos[0].doc4)) ? 0 : documentos[0].doc4,
                   "lista_contenidos_afectados_valorizados": (_.isUndefined(documentos[0]) || _.isNull(documentos[0].doc5)) ? 0 : documentos[0].doc5,
                   "respaldos_adquisicion_bienes": (_.isUndefined(documentos[0]) || _.isNull(documentos[0].doc6)) ? 0 : documentos[0].doc6,
                   "planos_arquitectura": (_.isUndefined(documentos[0]) || _.isNull(documentos[0].doc7)) ? 0 : documentos[0].doc7,
                   "planos_memoria": (_.isUndefined(documentos[0]) || _.isNull(documentos[0].doc8)) ? 0 : documentos[0].doc8,
                   "especificaciones_tecnicas_construccion": (_.isUndefined(documentos[0]) || _.isNull(documentos[0].doc9)) ? 0 : documentos[0].doc9,
                   "informes_tecnicos_danos": (_.isUndefined(documentos[0]) || _.isNull(documentos[0].doc10)) ? 0 : documentos[0].doc10,
                   "edificio": (_.isUndefined(documentos[0]) || _.isNull(documentos[0].edificio)) ? 0 : documentos[0].edificio,
                   "contenidos": (_.isUndefined(documentos[0]) || _.isNull(documentos[0].contenidos)) ? 0 : documentos[0].contenidos,
                   "otros": (_.isUndefined(documentos[0]) || _.isNull(documentos[0].otros)) ? "" : documentos[0].otros,
                   "dias": (_.isUndefined(documentos[0]) || _.isNull(documentos[0].dias)) ? 0 : documentos[0].dias
                }
             }
          }
          if (niveles.length >= 1) {
            _.each(niveles, (n,nl) => {
              recintos = wait.forMethod(res.app.settings._conn, 'query',`SELECT * FROM inspeccion_recintos WHERE id_tarea = ? AND id_nivel = ?`,[n.id_tarea,n.id_nivel]);
              json.datosInspeccion.caracteristicas.niveles.push({
                "nombre": n.nombre,
                "piso": n.piso,
                "ano_construccion": n.ano,
                "largo": n.largo,
                "ancho": n.ancho,
                "alto": n.alto,
                "superficie": n.superficie,
                "estructura_soportante": (_.isUndefined(n.ids_estructuras_soportantes) || _.isNull(n.ids_estructuras_soportantes)) ? [] : n.ids_estructuras_soportantes.split(',').map(parseFloat),
                "muros_tabiques": (_.isUndefined(n.ids_muros) || _.isNull(n.ids_muros)) ? [] : n.ids_muros.split(',').map(parseFloat),
                "entrepisos": (_.isUndefined(n.ids_entrepisos) || _.isNull(n.ids_entrepisos)) ? [] : n.ids_entrepisos.split(',').map(parseFloat),
                "pavimento": (_.isUndefined(n.ids_pavimentos) || _.isNull(n.ids_pavimentos)) ? [] : n.ids_pavimentos.split(',').map(parseFloat),
                "estructura_cubierta": (_.isUndefined(n.ids_estructura_cubierta) || _.isNull(n.ids_estructura_cubierta)) ? [] : n.ids_estructura_cubierta.split(',').map(parseFloat),
                "cubierta": (_.isUndefined(n.ids_cubierta) || _.isNull(n.ids_cubierta)) ? [] : n.ids_cubierta.split(',').map(parseFloat),
                "recinto": _.map(recintos, (r) => {
                  //console.log('info nivel en recinto -> this.n',this);
                  //if (r.id_nivel == this.n.id_nivel) {
                    items_danos = wait.forMethod(res.app.settings._conn, 'query',`SELECT * FROM inspeccion_itemdanos WHERE id_recinto = ? AND id_tarea = ?`,[r.id_recinto,inspeccion.id_tarea]);
                    danos_contenido = wait.forMethod(res.app.settings._conn, 'query',`SELECT * FROM inspeccion_contenidos WHERE id_recinto = ? AND id_tarea = ?`,[r.id_recinto,inspeccion.id_tarea]);
                    return {
                       "nombre": r.nombre,
                       "imagenes": _.map(_.without([r.foto1,r.foto2,r.foto3], 0), (foto) => { if(foto != 0) { return {"id_imagen":foto}; } }),
                       "largo": r.largo,
                       "ancho": r.ancho,
                       "alto": r.alto,
                       "superficie": r.superficie,
                       "estructura_soportante": (_.isUndefined(r.ids_estructuras_soportantes) || _.isNull(r.ids_estructuras_soportantes)) ? [] : r.ids_estructuras_soportantes.split(',').map(parseFloat),
                       "muros_tabiques": (_.isUndefined(r.ids_muros) || _.isNull(r.ids_muros)) ? [] : r.ids_muros.split(',').map(parseFloat),
                       "entrepisos": (_.isUndefined(r.ids_entrepisos) || _.isNull(r.ids_entrepisos)) ? [] : r.ids_entrepisos.split(',').map(parseFloat),
                       "pavimento": (_.isUndefined(r.ids_pavimentos) || _.isNull(r.ids_pavimentos)) ? [] : r.ids_pavimentos.split(',').map(parseFloat),
                       "estructura_cubierta": (_.isUndefined(r.ids_estructura_cubierta) || _.isNull(r.ids_estructura_cubierta)) ? [] : r.ids_estructura_cubierta.split(',').map(parseFloat),
                       "cubierta": (_.isUndefined(r.ids_cubierta) || _.isNull(r.ids_cubierta)) ? [] : r.ids_cubierta.split(',').map(parseFloat),
                       "contiene_danos": (items_danos.length >= 1) ? 1 : 0,
                       "items_danos": _.map(items_danos, (itda) => {
                         //if (itda.id_recinto == this.r.id_recinto) {
                           var accion = wait.forMethod(res.app.settings._conn, 'query',
                           `SELECT a.id_segured FROM accion AS a
                            LEFT JOIN tipo_dano AS td ON td.id = a.id_tipo_dano
                            LEFT JOIN partida AS p ON p.id = td.id_partida
                            WHERE td.id_segured = ? AND p.id_segured = ?`,[itda.id_tipodano,itda.id_partida]);
                           return {
                              "tipo_partida": itda.id_partida,
                              "tipo_dano": itda.id_tipodano,
                              "accion": (_.isUndefined(accion[0])) ? 0 : accion[0].id_segured,
                              "unidad_medida": itda.id_unidadmedida,
                              "superficie": itda.superficie,
                              "descripcion": (_.isNull(itda.descripcion_dano)) ? "" : itda.descripcion_dano,
                              "imagenes": _.map(_.without([itda.foto1,itda.foto2,itda.foto3], 0), (foto) => { if(foto != 0) { return {"id_imagen":foto}; } })
                           };
                         //}
                       }),
                       "danos_contenido": _.map(danos_contenido, (con) => {
                        //if (con.id_recinto == this.r.id_recinto) {
                          return {
                             "bienes": con.id_nombre,
                             "marca": con.id_marca,
                             "imagenes": _.map(_.without([con.foto1,con.foto2,con.foto3], 0), (foto) => { if(foto != 0) { return {"id_imagen":foto}; } }),
                             "cantidad_items": con.cantidad,
                             "fecha_compra": moment(con.fecha_compra).format("YYYY-MM-DD"),
                             "moneda": con.id_moneda,
                             "valor_del_objeto": con.valor,
                             "recupero_material": con.cupero,
                             "descripcion": con.descripcion
                          };
                        //}
                       })
                      //
                    };
                  //}
                })
              });
              if (nl == niveles.length-1) {
                  wait.forMethod(res.app.settings._conn, 'query', `UPDATE inspecciones SET json = ?, estado = 2 WHERE id = ?`,[JSON.stringify(json),inspeccion.id]);
              }
            });
          } else {
            wait.forMethod(res.app.settings._conn, 'query', `UPDATE inspecciones SET json = ?, estado = 2 WHERE id = ?`,[JSON.stringify(json),inspeccion.id]);
          }
        }
    },
    enviarInspeccion: (req, res) => {
        var tmp = {
            resp: []
        }
        let ins_armar = wait.forMethod(res.app.settings._conn, 'query', `SELECT * FROM inspecciones WHERE enviado_segured = 0 AND estado = 1`);//Estado 1 es registrada en base de datos
        if (ins_armar.length >= 1) {
            _.each(ins_armar, (v) => {
              self.armarJsonInspeccion(res,v);
            });
        }
        let inspecciones = wait.forMethod(res.app.settings._conn, 'query',
            `SELECT * FROM inspecciones WHERE enviado_segured = 0 AND estado = 2`);//estado 2 es que el json esta creado y listo
        if (inspecciones.length >= 1) {
          console.log(` ============= enviarInspeccion: ${inspecciones.length} inspecciones por enviar ============= `);
            _.each(inspecciones, function(valor, l) {
                valor.json = JSON.parse(valor.json);
                let envio_segured = wait.forMethod(self, 'enviarSegured', res.app.settings.segured_link + "/finalizaInspeccion/", valor.json);
                if (_.isNumber(envio_segured.error)) {
                    if (envio_segured.error==501) {
                        // 20-mar-2018 victor pide asumir error 501 como enviado para no continuar insistiendo (indicamos en enviado_segured=2 para saber que hubo una respuesta 501).
                        wait.forMethod(res.app.settings._conn, 'query',
                            `UPDATE inspecciones SET enviado_segured = 2, fecha_enviado_segured = now() WHERE id = ?`, [valor.id]);
                        wait.forMethod(res.app.settings._conn, 'query',
                            `UPDATE tareas SET estado_tarea = 9 WHERE id = ?`, [valor.id_tarea]);
                        tmp.resp.push({
                            error: envio_segured.error,
                            mensaje: `id => ${valor.id} - id_tarea => ${valor.id_tarea} - respuesta 501 => ${envio_segured.mensaje}`
                        });
                    } else {
                        wait.forMethod(res.app.settings._conn, 'query',
                          `UPDATE inspecciones SET repeticiones = ? WHERE id = ?`, [valor.repeticiones + 1, valor.id]);
                        tmp.resp.push({
                            error: envio_segured.error,
                            mensaje: `id => ${valor.id} - id_tarea => ${valor.id_tarea} - respuesta => ${envio_segured.mensaje}`
                        });
                    }
                } else {
                    if (envio_segured.error != "NO") {
                        wait.forMethod(res.app.settings._conn, 'query',
                            `UPDATE inspecciones SET repeticiones = ? WHERE id = ?`, [valor.repeticiones + 1, valor.id]);
                        tmp.resp.push({
                            error: envio_segured.error,
                            mensaje: `Error ${envio_segured.error}: id => ${valor.id} - id_tarea => ${valor.id_tarea} - respuesta => ${JSON.stringify(envio_segured)}`
                        });
                    } else {
                        wait.forMethod(res.app.settings._conn, 'query',
                            `UPDATE inspecciones SET enviado_segured = 1, fecha_enviado_segured = now() WHERE id = ?`, [valor.id]);
                        wait.forMethod(res.app.settings._conn, 'query',
                            `UPDATE tareas SET estado_tarea = 9 WHERE id = ?`, [valor.id_tarea]);
                        tmp.resp.push({
                            error: 0,
                            mensaje: `Enviado : id => ${valor.id} - id_tarea => ${valor.id_tarea} - respuesta => ${JSON.stringify(envio_segured.GLOSA_ERROR)}`
                        });
                    }
                }
                if (l == inspecciones.length-1) {
                    res.send(tmp.resp);
                    if (tmp.resp.length != 0) {
                      self.log("segured", "enviarInspeccion", tmp.resp, res);
                    }
                }
            });
        } else {
            tmp.resp={};
            tmp.resp.error = 500;
            tmp.resp.mensaje = `No existen inspecciones para enviar`;
            res.send(tmp.resp);
            // self.log("segured", "enviarInspeccion", tmp.resp, res);
        }
    },
    enviarImagenes: (req, res) => {
        var tmp = {
            resp: []
        }
        // Busca todas las imagenes a enviar MENOS las firmas (10may18 y solo de las inspecciones ya recibidas por segured)
        let imagenes = wait.forMethod(res.app.settings._conn, 'query',
            `SELECT i.id,t.num_caso,i.imagen,i.repeticiones
              FROM imagenes AS i
              INNER JOIN tareas AS t ON t.id = i.id_tarea
              INNER JOIN inspecciones AS i2 ON i2.id_tarea = i.id_tarea
              WHERE i.enviado_segured = 0
              AND i.obtenida = 1
              AND i2.enviado_segured <> 0
              AND NOT EXISTS (
              SELECT * FROM inspeccion_firma s
              WHERE i.id = s.firma)`
        );
        if (imagenes.length >= 1) {
          console.log(` ============= enviarImagenes: ${imagenes.length} imagenes por enviar ============= `);
            let contador = 0;
            _.each(imagenes, function(valor, id, fila) {
                contador++;
                var json = {
                      "numero_caso": valor.num_caso,
                      "id_imagen": valor.id,
                      "imagen": "data:image/jpeg;base64,"+valor.imagen.toString(),
                      "fecha_envio": moment(new Date()).format('YYYY-MM-DD HH:MM:SS')
                    }
                    // res.send(tmp.json);
                //console.log('JSON siendo enviado:',json);
                let envio_segured = wait.forMethod(self, 'enviarSegured', res.app.settings.segured_link + "/imagenesInspeccion/",json);
                console.log('imagenes: envio_segured dice:',{ respuesta:envio_segured, num_caso:valor.num_caso, id_imagen:valor.id });
                if (_.isNumber(envio_segured.error)) {
                    if (envio_segured.error==501) {
                        // consideramos error 501 como OK para no seguir insistiendo si la imagen ya habia sido recibida (ej por pruebas 10-abr-18)
                        wait.forMethod(res.app.settings._conn, 'query',`UPDATE imagenes SET enviado_segured = 1, fecha_enviado_segured = now() WHERE id = ?`, [valor.id]);
                        // wait.forMethod(res.app.settings._conn, 'query', `UPDATE tareas SET estado_tarea = 10 WHERE id = ?`,[valor.id_tarea]);
                        tmp.resp.push({
                            error: 501,
                            mensaje: `Enviado : id => ${valor.id} - id_imagen => ${valor.id} - num_caso => ${Math.round(valor.num_caso)} - respuesta => ${JSON.stringify(envio_segured)}`
                        });
                    } else {
                        wait.forMethod(res.app.settings._conn, 'query',`UPDATE imagenes SET repeticiones = ? WHERE id = ?`, [valor.repeticiones + 1, valor.id]);
                        tmp.resp.push({
                            error: envio_segured.error,
                            mensaje: `id => ${valor.id} - id_imagen => ${valor.id} - num_caso => ${Math.round(valor.num_caso)} - respuesta => ${envio_segured.mensaje}`
                        });
                    }
                } else {
                    if (envio_segured.error != "NO") {
                        wait.forMethod(res.app.settings._conn, 'query',`UPDATE imagenes SET repeticiones = ? WHERE id = ?`, [valor.repeticiones + 1, valor.id]);
                        tmp.resp.push({
                            error: 500,
                            mensaje: `Error : id => ${valor.id} - id_imagen => ${valor.id} - num_caso => ${Math.round(valor.num_caso)} - respuesta => ${JSON.stringify(envio_segured)}`
                        });
                    } else {
                        wait.forMethod(res.app.settings._conn, 'query',`UPDATE imagenes SET enviado_segured = 1, fecha_enviado_segured = now() WHERE id = ?`, [valor.id]);
                        // wait.forMethod(res.app.settings._conn, 'query', `UPDATE tareas SET estado_tarea = 10 WHERE id = ?`,[valor.id_tarea]);
                        tmp.resp.push({
                            error: 0,
                            mensaje: `Enviado : id => ${valor.id} - id_imagen => ${valor.id} - num_caso => ${Math.round(valor.num_caso)} - respuesta => ${JSON.stringify(envio_segured)}`
                        });
                    }
                }
                if (contador == imagenes.length) {
                    res.send(tmp.resp);
                    if (tmp.resp.length != 0) {
                      self.log("segured", "enviarImagenes", tmp.resp, res);
                    }
                }
            });
        } else {
            tmp.resp.error = 500;
            tmp.resp.mensaje = `No existen imagenes para enviar`;
            res.send(tmp.resp);
            // self.log("segured", "enviarImagenes", tmp.resp, res);
        }
    },
    /**
     * Guarda en la cola de salida los datos a ser enviados.
     *
     * @name colaSalida
     * @method
     * @private
     */
    guardarColaSalida: (res, funcion, url, json, cb) => {
      let resp = wait.forMethod(res.app.settings._conn, 'query',
          `INSERT INTO cola_salida (funcion,url,json,fecha)VALUES(?,?,?,now())`,[funcion,url,JSON.stringify(json)]
      );
      cb((resp.affectedRows == 0) ? true:false,true);
    },
    colaSalida: (req, res) => {
      var tmp = {
          resp: []
      }
      let salida = wait.forMethod(res.app.settings._conn, 'query',
          `SELECT * FROM cola_salida WHERE estado = 0`
      );
      if (salida.length >= 1) {
          console.log(` ============= colaSalida: ${salida.length} funciones por enviar ============= `);
          let contador = 0;
          _.each(salida, function(valor, id, fila) {
            try {
            valor.json = JSON.parse(valor.json);
            } catch (e) {
             console.log('Error en JSON a enviar', e);
            }
              contador++;
              let envio_segured = wait.forMethod(self,'enviarSegured', res.app.settings.segured_link + valor.url, valor.json);
              // console.log(envio_segured);
              switch (valor.funcion) {
                case 'registrarInspector':
                  try {
                    // var inspector = JSON.parse(valor.json);
                    if (_.has(envio_segured,'error') && _.isNumber(envio_segured.error) && envio_segured.error != 0) {
                        tmp.resp.push({error: envio_segured.error,mensaje: `id (${valor.id}) - ${valor.funcion} - respuesta : ${envio_segured.mensaje}`});
                        wait.forMethod(res.app.settings._conn, 'query', `DELETE FROM inspectores WHERE id = ?`,[valor.id_inspector]);
                    } else if (_.isString(envio_segured.GLOSA_ERROR) && envio_segured.GLOSA_ERROR != "") {
                        if (envio_segured.GLOSA_ERROR == "codigo_identificador YA FUE INFORMADO POR U-ADJUST") {
                          tmp.resp.push({error: 500,mensaje: `id (${valor.id}) - ${valor.funcion} - Registro fallido : ${envio_segured.mensaje}`});
                          wait.forMethod(res.app.settings._conn, 'query', `UPDATE inspectores SET registrado_segured = 1 WHERE id = ?`,[valor.id_inspector]);
                          wait.forMethod(res.app.settings._conn, 'query', `UPDATE cola_salida SET estado = 1, fecha_envio = now() WHERE id = ?`,[valor.id]);
                        } else if (envio_segured.GLOSA_ERROR == "id_inspector YA FUE INFORMADO POR U-ADJUST") {
                          tmp.resp.push({error: 500,mensaje: `id (${valor.id}) - ${valor.funcion} - Registro fallido : ${envio_segured.mensaje}`});
                          wait.forMethod(res.app.settings._conn, 'query', `UPDATE inspectores SET registrado_segured = 1 WHERE id = ?`,[valor.id_inspector]);
                          wait.forMethod(res.app.settings._conn, 'query', `UPDATE cola_salida SET estado = 1, fecha_envio = now() WHERE id = ?`,[valor.id]);
                        } else if (envio_segured.GLOSA_ERROR != "Datos grabados correctamente") {
                            tmp.resp.push({error: 500,mensaje: `id (${valor.id}) - ${valor.funcion} - Llamada a segured fallo, se procede a eliminar el inspector : ${envio_segured.mensaje}`});
                            wait.forMethod(res.app.settings._conn, 'query', `DELETE FROM inspectores WHERE id = ?`, [valor.id_inspector]);
                            wait.forMethod(res.app.settings._conn, 'query', `UPDATE cola_salida SET estado = 1, fecha_envio = now() WHERE id = ?`,[valor.id]);
                        } else {
                          wait.forMethod(res.app.settings._conn, 'query', `UPDATE inspectores SET registrado_segured = 1 WHERE id = ?`,[valor.id_inspector]);
                          wait.forMethod(res.app.settings._conn, 'query',`UPDATE cola_salida SET estado = 1, fecha_envio = now() WHERE id = ?`,[valor.id]);
                        }
                    } else {
                        wait.forMethod(res.app.settings._conn, 'query', `UPDATE inspectores SET registrado_segured = 1 WHERE id = ?`,[valor.id_inspector]);
                        wait.forMethod(res.app.settings._conn, 'query', `UPDATE cola_salida SET estado = 1, fecha_envio = now() WHERE id = ?`,[valor.id]);
                    }
                  } catch (e) {
                    console.log('Error al registrarInspector',e);
                  }
                break;
                case 'aceptarTarea':
                  try {
                    if (_.isNumber(envio_segured.error) && envio_segured.error !== 0) {
                      tmp.resp.push({error: envio_segured.error,mensaje: `id (${valor.id}) - ${valor.funcion} - respuesta : ${envio_segured.mensaje}`});
                    } else if (_.isString(envio_segured.GLOSA_ERROR) && envio_segured.GLOSA_ERROR !== "") {
                        if (envio_segured.GLOSA_ERROR != "TAREA ACEPTADA") {
                          tmp.resp.push({error: 500,mensaje: `id (${valor.id}) - ${valor.funcion} - Tarea no puede ser aceptada por el inspector : ${envio_segured.mensaje}`});
                        } else {
                          wait.forMethod(res.app.settings._conn, 'query',`UPDATE cola_salida SET estado = 1, fecha_envio = now() WHERE id = ?`,[valor.id]);
                        }
                    } else {
                      wait.forMethod(res.app.settings._conn, 'query',`UPDATE cola_salida SET estado = 1, fecha_envio = now() WHERE id = ?`,[valor.id]);
                    }
                  } catch (e) {
                    console.log('Error al aceptarTarea',e);
                  }
                break;
                case 'cancelarTarea'://16-10-17: Ya no va cancelar tarea, no se elimina codigo por si quisieran retormar a futuro esta funcionalidad.
                  try {
                    if (_.isNumber(envio_segured.error) && envio_segured.error !== 0) {
                        tmp.resp.push({
                            error: envio_segured.error,
                            mensaje: `id (${valor.id}) - ${valor.funcion} - respuesta : ${envio_segured.mensaje}`
                        });
                    } else if (_.isString(envio_segured.GLOSA_ERROR) && envio_segured.GLOSA_ERROR !== "") {
                        if (envio_segured.GLOSA_ERROR != "TAREA CANCELADA") {
                          tmp.resp.push({
                              error: 500,
                              mensaje: `id (${valor.id}) - ${valor.funcion} : No se logro registrar la cancelacion en segured : ${envio_segured.GLOSA_ERROR}.`
                          });
                        } else {
                          wait.forMethod(res.app.settings._conn, 'query',
                              `UPDATE cola_salida SET estado = 1, fecha_envio = now() WHERE id = ?`,[valor.id]);
                        }
                    }
                  } catch (e) {
                    console.log('Error al cancelarTarea',e);
                  }
                break;
                case 'confirmarRuta':
                  try {
                    if (_.isNumber(envio_segured.error) && envio_segured.error !== 0) {
                      tmp.resp.push({
                          error: envio_segured.error,
                          mensaje: `id (${valor.id}) - ${valor.funcion} - respuesta : ${envio_segured.mensaje}`
                      });
                    } else if (_.isString(envio_segured.GLOSA_ERROR) && envio_segured.GLOSA_ERROR !== "") {
                        if (envio_segured.GLOSA_ERROR != "TAREA CONFIRMADA") {
                            tmp.resp.push({
                                error: 500,
                                mensaje: `id (${valor.id}) - ${valor.funcion} : No se logro registrar la confirmacion de la ruta.`
                            });
                        } else {
                          wait.forMethod(res.app.settings._conn, 'query',
                              `UPDATE cola_salida SET estado = 1, fecha_envio = now() WHERE id = ?`,[valor.id]);
                        }
                    }
                  } catch (e) {
                    console.log('Error al confirmarRuta',e);
                  }
                break;
                case 'iniciarTarea':
                  try {
                    if (_.isNumber(envio_segured.error) && envio_segured.error !== 0) {
                        tmp.resp.push({
                            error: envio_segured.error,
                            mensaje: `id (${valor.id}) - ${valor.funcion} - respuesta : ${envio_segured.mensaje}`
                        });
                    } else if (_.isString(envio_segured.GLOSA_ERROR) && envio_segured.GLOSA_ERROR !== "") {
                        if (envio_segured.GLOSA_ERROR != "TAREA INICIADA") {
                            tmp.resp.push({
                                error: 500,
                                mensaje: `id (${valor.id}) - ${valor.funcion} : No se logro registrar el inicio de tarea en segured.`
                            });
                        } else {
                          wait.forMethod(res.app.settings._conn, 'query',
                              `UPDATE cola_salida SET estado = 1, fecha_envio = now() WHERE id = ?`,[valor.id]);
                        }
                    } else {
                      wait.forMethod(res.app.settings._conn, 'query',
                          `UPDATE cola_salida SET estado = 1, fecha_envio = now() WHERE id = ?`,[valor.id]);
                    }
                  } catch (e) {
                    console.log('Error al iniciarTarea',{error:e, fecha:new Date()});
                  }
                break;
                case 'editarPerfilTipo1':
                  try {
                    if (_.isNumber(envio_segured.error) && envio_segured.error !== 0) {
                      tmp.resp.push({
                          error: envio_segured.error,
                          mensaje: `id (${valor.id}) - ${valor.funcion} - respuesta : ${envio_segured.mensaje}`
                      });
                    } else if (_.isString(envio_segured.GLOSA_ERROR) && envio_segured.GLOSA_ERROR !== "") {
                        if (tmp.editarInspector.GLOSA_ERROR != "DOMICILIO ACTUALIZADO") {
                          tmp.resp.push({
                              error: 500,
                              mensaje: `id (${valor.id}) - ${valor.funcion} : No se logro registrar el cambio de domicilio en segured.`
                          });
                        } else {
                          wait.forMethod(res.app.settings._conn, 'query',
                              `UPDATE cola_salida SET estado = 1, fecha_envio = now() WHERE id = ?`,[valor.id]);
                        }
                    } else {
                      wait.forMethod(res.app.settings._conn, 'query',
                          `UPDATE cola_salida SET estado = 1, fecha_envio = now() WHERE id = ?`,[valor.id]);
                    }
                  } catch (e) {
                    console.log('Error al iniciarTarea',e);
                  }
                break;
                case 'editarPerfilTipo2':
                  try {
                    if (_.isNumber(envio_segured.error) && envio_segured.error !==0) {
                      tmp.resp.push({
                          error: envio_segured.error,
                          mensaje: `id (${valor.id}) - ${valor.funcion} - respuesta : ${envio_segured.mensaje}`
                      });
                    } else if (_.isString(envio_segured.GLOSA_ERROR) && envio_segured.GLOSA_ERROR !== "") {
                        if (envio_segured.GLOSA_ERROR != "DISPONIBILIDAD ACTUALIZADA") {
                          tmp.resp.push({
                              error: 500,
                              mensaje: `id (${valor.id}) - ${valor.funcion} : No se logro registrar el cambio en la disponibilidad en segured.`
                          });
                        } else {
                          wait.forMethod(res.app.settings._conn, 'query',
                              `UPDATE cola_salida SET estado = 1, fecha_envio = now() WHERE id = ?`,[valor.id]);
                        }
                    } else {
                      wait.forMethod(res.app.settings._conn, 'query',
                          `UPDATE cola_salida SET estado = 1, fecha_envio = now() WHERE id = ?`,[valor.id]);
                    }
                  } catch (e) {
                    console.log('Error al editarPerfilTipo2',e);
                  }
                break;
                case 'editarPerfilTipo3':
                  try {
                    if (_.isNumber(envio_segured.error) && envio_segured.error !== 0) {
                      tmp.resp.push({
                          error: envio_segured.error,
                          mensaje: `id (${valor.id}) - ${valor.funcion} - respuesta : ${envio_segured.mensaje}`
                      });
                    } else if (_.isString(envio_segured.GLOSA_ERROR) && envio_segured.GLOSA_ERROR !== "") {
                        if (envio_segured.GLOSA_ERROR != "CONTACTO ACTUALIZADO") {
                          tmp.resp.push({
                              error: 500,
                              mensaje: `id (${valor.id}) - ${valor.funcion} : No se logro registrar el cambio en los datos de contacto en segured.`
                          });
                        } else {
                          wait.forMethod(res.app.settings._conn, 'query',
                              `UPDATE cola_salida SET estado = 1, fecha_envio = now() WHERE id = ?`,[valor.id]);
                        }
                    } else {
                      wait.forMethod(res.app.settings._conn, 'query',
                          `UPDATE cola_salida SET estado = 1, fecha_envio = now() WHERE id = ?`,[valor.id]);
                    }
                  } catch (e) {
                    console.log('Error al editarPerfilTipo2',e);
                  }
                break;
                default:
                  tmp.resp.push({
                      error: 500,
                      mensaje: `id (${valor.id}) funcion => ${valor.funcion} : no existe para el envio`
                  });
                break;
              }
              if (contador == salida.length) {
                  res.send(tmp.resp);
                  if (tmp.resp.length != 0) {
                    self.log("u-adjust", "colaSalida", tmp.resp, res);
                  }
              }
          });
      } else {
          tmp.resp.error = 500;
          tmp.resp.mensaje = `No hay cola de salida.`;
          res.send(tmp.resp);
          // self.log("u-adjust", "colaSalida", tmp.resp, res);
      }
    },
    /**
     * Envia un request a Segured con la informacion requerida, devuelve un Callback al terminar.
     *
     * @name enviarSegured
     * @method
     * @param {string} _url - Url a ser consultada.
     * @param {object} _json - JSON a ser enviado.
     * @param {function} cb - Callback.
     * @private
     * @returns {object} (resp = [0,400,500], mensaje = [texto de respuesta]).
     */
    enviarSegured: (_url, _json, cb) => {
        var request = require('postman-request');
        request({
            method: 'POST',
            uri: _url,
            json: true,
            timeout: 200000,
            body: _json
        }, function(error, response, body) {
            // console.log(error,response,body);
            if ((!error || error === null) && response.statusCode == 200) {
                //console.log(body.ERROR);
                if (body === null) {
                    cb(false, {error: 500, mensaje: "Envio a Segured fallo => retorno NULL (error de servicio)", GLOSA_ERROR: "retorno NULL (error de servicio)"});
                } else if (_.has(body, 'ERROR') && body.ERROR != "NO") {
                    cb(false, {error: !isNaN(parseInt(body.ERROR))?parseInt(body.ERROR):body.ERROR, mensaje: `Envio a Segured fallo => ${body.GLOSA_ERROR}`, "GLOSA_ERROR": body.GLOSA_ERROR});
                } else {
                    cb(false, _.extend({ GLOSA_ERROR:"", error:body.ERROR },body));
                }
                //res.send(body);
            } else {
                // console.log(`error envio a segured => ${error}`);
                cb(false, {error: 500,mensaje: `Envio a Segured fallo => ${error}`, GLOSA_ERROR:error});
            };
        });
    },
    tengoNotificacion: (req, res) => {
        tmp = {
            resp: {
                error: 0,
                mensaje: "",
                push: false
            },
            id_inspector: req.body.id_inspector,
            push: {}
        }
        if (!_.has(req.body, 'id_inspector') || _.isUndefined(req.body.id_inspector) || _.isNull(req.body.id_inspector)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "id_inspector no existe.";
        }
        if (tmp.resp.error == 0) {
            tmp.push = wait.forMethod(res.app.settings._conn, 'query',`SELECT id_inspector FROM push WHERE id_inspector = ? AND estado = 0 LIMIT 1`, [tmp.id_inspector]);
            if (tmp.push.length >= 1) {
                tmp.resp.push = true;
                res.send(tmp.resp);
                self.log("segured", "tengoNotificacion", tmp.resp, res);
                wait.forMethod(res.app.settings._conn, 'query',`UPDATE push SET estado = 1 WHERE id_inspector = ?`, [tmp.push[0].id_inspector]);
            } else {
                res.send(tmp.resp);
            }
        } else {
            res.send(tmp.resp);
            self.log("segured", "tengoNotificacion", tmp.resp, res);
        }
    },
    obtenerUbicacion: (req, res) => {
        var tmp = {
            resp: {
                error: 0,
                mensaje: "",
                inspector: "",
                tarea: "",
                duracion: "",
                duracion_seg: "",
                distancia: "",
                polyline: ""
            },
            inspector: {},
            tarea: {},
            google: {},
            ci_inspector: req.body.ci_inspector,
            num_caso: req.body.num_caso
        };
        if (!_.has(req.body, 'ci_inspector') || _.isUndefined(req.body.ci_inspector) || _.isNull(req.body.ci_inspector) || _.isEmpty(req.body.ci_inspector)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "ci_inspector no existe.";
        }
        if (!_.has(req.body, 'num_caso') || _.isUndefined(req.body.num_caso) || _.isNull(req.body.num_caso)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "num_caso no existe.";
        }
        if (tmp.resp.error == 0) {
            // Obtengo la LAT y LON del inspector
            tmp.inspector = wait.forMethod(res.app.settings._conn, 'query',
                `SELECT lat_inspector, lon_inspector FROM inspectores WHERE codigo_identificador = ?`,   [tmp.ci_inspector]);
            if (tmp.inspector.length > 0) {
                tmp.tarea = wait.forMethod(res.app.settings._conn, 'query',`SELECT t.lat,t.lon FROM tareas AS t LEFT JOIN inspectores AS i ON i.codigo_identificador = ? WHERE t.estado_tarea = 4 AND t.num_caso = ?`, [tmp.ci_inspector, tmp.num_caso]);
                if (tmp.tarea.length > 0 && tmp.inspector.length > 0) {
                    tmp.tarea = tmp.tarea[0].lat + ',' + tmp.tarea[0].lon;
                    tmp.inspector = tmp.inspector[0].lat_inspector + ',' + tmp.inspector[0].lon_inspector;
                    var request = require('postman-request');
                    var google_url = `https://maps.googleapis.com/maps/api/directions/json?origin=${tmp.inspector}&destination=${tmp.tarea}&mode=transit&key=${res.app.settings.config.google.key}`;
                    // console.log(google_url);
                    request({method: 'GET', uri: google_url, timeout: 50000 }, function(error, response, body) {
                        if ((!error || error === null) && response.statusCode == 200) {
                            if (body !== null) {
                                try {
                                    tmp.google = JSON.parse(body);
                                    // console.log(tmp.google);
                                } catch (e) {}
                                if (_.has(tmp.google, 'routes')) {
                                    if (_.has(tmp.google.routes[0], 'legs')) {
                                        tmp.resp.duracion = (_.has(tmp.google.routes[0].legs[0], 'duration')) ? moment.utc(tmp.google.routes[0].legs[0].duration.value * 1000).format('HH[h]:mm[m]') : "";
                                        tmp.resp.duracion_seg = (_.has(tmp.google.routes[0].legs[0], 'duration')) ? tmp.google.routes[0].legs[0].duration.value : "";
                                        tmp.resp.distancia = (_.has(tmp.google.routes[0].legs[0], 'distance')) ? tmp.google.routes[0].legs[0].distance.text : "";
                                        // Si tengo distancia y duracion agrego los datos del inspector y la tarea
                                        tmp.resp.tarea = tmp.tarea;
                                        tmp.resp.inspector = tmp.inspector;
                                    }
                                    tmp.resp.polyline = (_.has(tmp.google.routes[0], 'overview_polyline')) ? tmp.google.routes[0].overview_polyline.points : "";
                                }
                                res.send(tmp.resp);
                                self.log("u-adjust", "obtenerUbicacion", tmp.resp, res);
                            } else {
                                res.send(tmp.resp);
                                self.log("u-adjust", "obtenerUbicacion", tmp.resp, res);
                            }
                        } else {
                            res.send(tmp.resp);
                            self.log("u-adjust", "obtenerUbicacion", tmp.resp, res);
                        };
                    });
                } else {
                    res.send(tmp.resp);
                    self.log("u-adjust", "obtenerUbicacion", tmp.resp, res);
                }
            } else {
                res.send(tmp.resp);
                self.log("u-adjust", "obtenerUbicacion", tmp.resp, res);
            }
        } else {
            res.send(tmp.resp);
            self.log("u-adjust", "obtenerUbicacion", tmp.resp, res);
        }
    },
    guardarUbicacion: (req, res) => {
        var tmp = {
            resp: {
                error: 0,
                mensaje: "",
            },
            id_inspector: req.body.id_inspector,
            id_tarea: req.body.id_tarea,
            lat: req.body.lat,
            lon: req.body.lon,
            tareas: []
        };
        if (!_.has(req.body, 'id_inspector') || _.isUndefined(req.body.id_inspector) || _.isNull(req.body.id_inspector) || _.isEmpty(req.body.id_inspector)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "id_inspector no existe.";
        }
        if (!_.has(req.body, 'lat') || _.isUndefined(req.body.lat) || _.isNull(req.body.lat) || _.isEmpty(req.body.lat)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "lat no existe.";
        }
        if (!_.has(req.body, 'lon') || _.isUndefined(req.body.lon) || _.isNull(req.body.lon) || _.isEmpty(req.body.lon)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "lon no existe.";
        }
        if (!_.has(req.body, 'id_tarea') || _.isUndefined(req.body.id_tarea) || _.isNull(req.body.id_tarea) || _.isEmpty(req.body.id_tarea)) {
            tmp.resp.error = 400;
            tmp.resp.mensaje = "id_tarea no existe.";
        }
        if (tmp.resp.error == 0) {
            // Obtengo la LAT y LON del inspector
            // Tiene alguna otra tarea con estado_tarea en 4 (seguimiento)?
            tmp.tareas = wait.forMethod(res.app.settings._conn, 'query', `SELECT id FROM tareas WHERE id_inspector = ? AND estado_tarea = 4 AND id <> ?`, [tmp.id_inspector, tmp.id_tarea]);
            // console.log(tmp.tareas);
            if (tmp.tareas.length > 0) {
                //Tiene otra tarea en estado 4
                //Hay que desactivar su seguimiento (volver a 3)
                _.each(tmp.tareas, (valor) => { wait.forMethod(res.app.settings._conn, 'query',`UPDATE tareas SET estado_tarea = 3 WHERE id = ?`, [valor.id]); });
                tmp.tareas = wait.forMethod(res.app.settings._conn, 'query', `SELECT id FROM tareas WHERE id_inspector = ? AND estado_tarea BETWEEN 2 AND 3 AND id = ?`, [tmp.id_inspector, tmp.id_tarea]);
                if (tmp.tareas.length > 0) {
                  wait.forMethod(res.app.settings._conn, 'query', `UPDATE tareas SET estado_tarea = 4 WHERE id = ? AND id_inspector = ?`, [tmp.id_tarea, tmp.id_inspector]);
                }
            } else {
              tmp.tareas = wait.forMethod(res.app.settings._conn, 'query', `SELECT id FROM tareas WHERE id_inspector = ? AND estado_tarea BETWEEN 2 AND 3 AND id = ?`, [tmp.id_inspector, tmp.id_tarea]);
              if (tmp.tareas.length > 0) {
                wait.forMethod(res.app.settings._conn, 'query', `UPDATE tareas SET estado_tarea = 4 WHERE id = ? AND id_inspector = ?`, [tmp.id_tarea, tmp.id_inspector]);
              }
            }
            res.send(tmp.resp);
            tmp.guardado_2 = wait.forMethod(res.app.settings._conn, 'query', `UPDATE inspectores SET lat_inspector = ?, lon_inspector = ? WHERE id = ?`, [tmp.lat, tmp.lon, tmp.id_inspector]);
            // 2017-09-27: Pablo solicita guardar en una tabla aparte los cambios de ubicacion del inspector.
            tmp.guardado_3 = wait.forMethod(res.app.settings._conn, 'query', `INSERT INTO ubicacionInspectores (lat,lon,id_inspector,id_tarea,fecha)VALUES(?,?,?,?,now())`,[tmp.lat,tmp.lon,tmp.id_inspector,tmp.id_tarea]);
            self.log("u-adjust", "guardarUbicacion", tmp.resp, res);
        } else {
            res.send(tmp.resp);
            self.log("u-adjust", "guardarUbicacion", tmp.resp, res);
        }
    },
    resetearTareas: (req, res, next) => {
      if ((_.has(req.body,'q') && req.body.q == "uadjust") || (_.has(req.query,'q') && req.query.q == "uadjust")) {
        try {
            wait.forMethod(res.app.settings._conn, 'query',
                `call actualizar_fechas_criticas();`);
             wait.forMethod(res.app.settings._conn, 'query',
                 `call actualizar_fechas_emergencias();`);
            wait.forMethod(res.app.settings._conn, 'query',
                `call actualizar_fechas_normales(5);`);
        } catch (e) {
          console.log("error",e);
        }
        res.send(true);
      } else {
        res.send(false);
      }

    },
    /**
     * Registra las respuestas fallidas del sistema.
     *
     * @name log
     * @method
     * @param {string} entidad - entidad (segured/u-adjust).
     * @param {string} funcion - nombre de la funcion.
     * @param {string} mensaje - mensaje que se le entrego al usuario.
     * @private
     */
    log: (entidad, funcion, mensaje, res) => {
        try {
            wait.forMethod(res.app.settings._conn, 'query',
                `INSERT INTO log_salida (entidad,funcion,mensaje,fecha)VALUES(?,?,?,now())`, [entidad, funcion, JSON.stringify(mensaje)]);
        } catch (e) {}
    },

    /**
     * Genera los time zone por pais y actualiza la db de paises con la diferencia horaria.
     *
     * @name getTimezone
     * @method
     * @private
     */
    getTimezone: (req, res) => {
        var tz = require("moment-timezone");
        var ctz = require('country-tz');
        var now_server = new Date();
        var now = moment(now_server);
        //console.log("Fecha servidor",moment.utc(now_server).format("YYYY-MM-DD HH:mm:ss"),"|","Prueba, darle offset a la fecha actual -0400",moment.utc(now_server).utcOffset("-0400").format("YYYY-MM-DD HH:mm:ss"));
        var paises = [];
        paises = wait.forMethod(res.app.settings._conn, 'query', `SELECT i2.* FROM paises AS p RIGHT JOIN iso AS i ON i.id = p.idpais RIGHT JOIN isosubdivision AS i2 ON i2.id_pais = i.id`);
        _.each(paises, function(_valor, _id) {
            let _timezone = ctz.getTimeZonesByCode(_valor.codigoalfa2);
            let _tz = "";
            _.each(_timezone, function(_valor2, _id2) {
                _tz = now.tz(_valor2).format('Z');
                console.log(_id, "||", _id2, "||", _valor.codigoalfa2, "||",_valor.codigoalfa2, "|", _valor2, "|", _tz, "|","Fecha actual: ", moment.utc(now_server).format("YYYY-MM-DD HH:mm:ss"), "|", 'Hora del pais: ',moment.utc(now_server).utcOffset(_tz).format("YYYY-MM-DD HH:mm:ss"));
                // if (_.has(req.body,'actualizar') && !_.isNull(req.body.actualizar) && req.body.actualizar == "1") {
                //   console.log(`VOY A ACTUALIZAR EL UTC DE ${_valor.nombrecomun} con id ${_valor.id} a ${_tz}...`);
                //   wait.forMethod(res.app.settings._conn,'query',`UPDATE iso SET utc = '${_tz}' WHERE id = ${_valor.id}`);
                // }
                if (_.has(req.body, 'insertar') && !_.isNull(req.body.insertar) && req.body.insertar == "1") {
                    wait.forMethod(res.app.settings._conn, 'query',`INSERT INTO iso_utc (id_iso,tz,diff)values(${_valor.id},'${_valor2}','${_tz}')`);
                }
            });
        });
        res.send(true);
    },

    /**
     * Genera los codigos de llamado por pais y actualiza la db.
     *
     * @name getCallingCodes
     * @method
     * @private
     */
    getCallingCodes: (req, res) => {
        var ci = require('countryinfo'),
            arPaises = [],
            callCode = [];
        paises = wait.forMethod(res.app.settings._conn, 'query', `SELECT i.* FROM iso AS i`);
        arPaises = _.map(paises, function(valor) {
            callCode = ci.data(valor.codigoalfa2);
            valor.callingCode = (_.has(callCode, 'callingCodes') && !_.isUndefined(callCode.callingCodes[0]) && !_.isNull(callCode.callingCodes[0]) && !_.isEmpty(callCode.callingCodes[0])) ? callCode.callingCodes[0].replace(/\+|\s/g, "") : "";
            wait.forMethod(res.app.settings._conn, 'query', `UPDATE iso SET callingCode = ? WHERE id = ?`, [valor.callingCode,valor.id]);
            return valor;
        });
        // _.each(paises,function(valor,index){
        //   var callCode = ci.data(valor.codigoalfa2);
        //   var callCode2 = (_.has(callCode,'callingCodes') && !_.isUndefined(callCode.callingCodes[0]) && !_.isNull(callCode.callingCodes[0]) && !_.isEmpty(callCode.callingCodes[0])) ? callCode.callingCodes[0].replace(/\+|\s/g,"") : "";
        //   console.log(valor.codigoalfa2,callCode2);
        //   wait.forMethod(res.app.settings._conn, 'query', `UPDATE iso SET callingCode = ? WHERE id = ?`,[callCode2,valor.id]);
        // });
        res.send(arPaises);
    },

    /**
     * Genera los lenguajes por pais y actualiza la db.
     *
     * @name getCountryLanguage
     * @method
     * @private
     */
    getCountryLanguage: (req, res) => {
        var c = require('countryinfo');
        paises = wait.forMethod(res.app.settings._conn, 'query',`SELECT i.* FROM iso AS i`);
        _.each(paises, (valor, index) => {
            try {
                // console.log(valor.codigoalfa2,c.data(valor.codigoalfa2).languages[0].alpha2);
                var lenguaje = c.data(valor.codigoalfa2).languages[0].alpha2.toLowerCase() + '-' + valor.codigoalfa2.toUpperCase();
                wait.forMethod(req.app.settings._conn, 'query',`UPDATE iso SET lenguaje = ? WHERE id = ?`, [lenguaje, valor.id]);
            } catch (e) {
                console.log('error => ', valor.codigoalfa2);
            }
        });
        res.send(true);
    },
}

module.exports = self;
